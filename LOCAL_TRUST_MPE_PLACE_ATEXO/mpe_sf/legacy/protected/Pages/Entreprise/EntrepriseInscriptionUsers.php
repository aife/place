<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEntrepriseInsee;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_InscritHistorique;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_SearchLucene;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Insee\Atexo_Insee_Entreprise;
use Application\Service\Atexo\Insee\Atexo_Insee_EntrepriseVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseInscriptionUsers extends MpeTPage
{
    private $_company;
    private $_etablissement;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        if ((!Atexo_Module::isEnabled('InscriptionLibreEntreprise')
        || Atexo_Module::isEnabled('SocleExternePpp'))) {
            if ($this->User->IsGuest) { // bloquer l'accès pour les guest lorsque la création de compte n'est pas autorisée ici
                $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
            }
        }
        $this->inscription->visible = true;
        $this->demandeInscription->visible = false;

        $this->PanelInscrit->setPostBack($this->IsPostBack);
        $this->panelRegionProvince->setPostBack($this->IsPostBack);
        if ('ChangingUser' == $_GET['action']) {
            if ($this->User->IsGuest) {
                $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
            }
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='none';</script>";
            $this->panelMonEntreprise->setVisible(false);
            $this->panelMessage->setVisible(false);
            $this->panelMessageErreur->setVisible(false);
            $this->buttonRetour->setVisible(false);

            $this->panelSiege->setCalledFor(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
            $this->panelSiege->setVisible(false);
        } elseif ('ChangingCompany' == $_GET['action']) {
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_CurrentUser::getIdEntreprise());
            if ($entreprise) {
                if (($entreprise->getPaysenregistrement() != Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) && ('' != $entreprise->getSirenEtranger())) {
                    $this->_company = $this->fillEntrepriseEtrangerObject($entreprise);
                } else {
                    $this->panelRegionProvince->setDisplay(true);
                    $this->_company = $this->fillEntrepriseBdeObject($entreprise);
                }

                $this->panelSiege->setVisible(true);
                $this->panelSiege->fill($this->_company);
                if (Atexo_CurrentUser::isATES()) {
                    $this->panelUserCompte->setVisible(false);
                    $this->panelMonEntreprise->setVisible(true);
                    $this->buttonRetour->setVisible(false);
                    $this->panelMessage->setVisible(false);
                    $this->panelMessageErreur->setVisible(false);

                    //Desactiver les validateurs de creation compte inscrit
                    $this->PanelInscrit->emailCreationValidator->SetEnabled(false);
                    $this->PanelInscrit->emailModificationValidator->SetEnabled(false);
                    $this->PanelInscrit->loginCreationValidator->SetEnabled(false);
                    $this->PanelInscrit->loginModificationValidator->SetEnabled(false);
                    $this->PanelInscrit->certifModificationValidator->SetEnabled(false);
                    $this->PanelInscrit->certifCreationValidator->SetEnabled(false);

                    if (!$this->IsPostBack) {
                        $this->displayCompany();
                        $this->chargerComboFormeJuridique();
                    }
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
                }
            } else {
                $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
            }
        } elseif ('creation' == $_GET['action']) {
            $this->buttonRetour->setVisible(false);
            $this->panelMessage->setVisible(false);
            $this->panelMessageErreur->setVisible(false);

            if (isset($_GET['siren'])) {
                $this->cpSiegeSocial->setMaxLength('5');
                $this->PanelInscrit->cpEtablissement->setMaxLength('5');
                $this->PanelInscrit->nic->setMaxLength('5');
                $this->panelSiege->setCalledFor(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
                $this->panelSiege->setSiren(Atexo_Util::atexoHtmlEntities($_GET['siren']));
                $this->panelNumRc->setDisplay('None');
                $this->panelSiren->setDisplay('None');
                $this->validatorSiren->SetEnabled(false);

                if (Atexo_Module::isEnabled('CompteEntrepriseRcnum')) {
                    //$this->panelNumRc->setDisplay("Dynamic");
                    $this->panelSiren->setVisible(false);
                    $this->validatorSiren->SetEnabled(false);
                }

                if (Atexo_Module::isEnabled('CompteEntrepriseSiren')) {
                    $this->panelSiren->setDisplay('Dynamic');
                    $this->validatorSiren->SetEnabled(true);
                }

                if (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                    $this->panelSiren->setDisplay('Dynamic');
                    $this->validatorSiren->SetEnabled(false);
                    $this->codeEtablissement->setDisplay('None');
                    $this->siren->setCssClass('input-185');
                    $this->infoBulle->setDisplay('None');
                    $this->labelIdentifiantUnique->setDisplay('Dynamic');
                    $this->labelSiren->setDisplay('None');
                    $this->labelSiret->setDisplay('None');
                    $this->labelSiegeSocial->setDisplay('None');
                }
                $this->panelIdNational->setVisible(false);
                if (!$this->isPostBack) {
                    $this->siren->Text = Atexo_Util::atexoHtmlEntities($_GET['siren']);
                }
                if ($_GET['siret']) {
                    $this->PanelInscrit->nic->Text = Atexo_Util::atexoHtmlEntities($_GET['siret']);
                }
                if ($_GET['siret']) {
                    $this->codeEtablissement->Text = Atexo_Util::atexoHtmlEntities($_GET['siret']);
                }
                $this->panelSiege->setLieuEtablissement(Prado::localize('DEFINE_FRANCE'));
                $this->pays->Text = Atexo_Util::encodeToHttp(Prado::localize('DEFINE_FRANCE'));
                $this->PanelInscrit->paysEtablissement->Text = Atexo_Util::encodeToHttp(Prado::localize('DEFINE_FRANCE'));
                $this->_company = self::retrieveCompany(Atexo_Util::atexoHtmlEntities($_GET['siren']));
                $this->PanelInscrit->setCompany($this->_company);
                if (Atexo_Module::isEnabled('CompteInscritMsgAtes') && false == is_object($this->_company)) {
                    $this->PanelInscrit->msgInscrit->Text = Prado::localize('TEXT_COMPTE_ADMINISTRATEUR');
                } else {
                    $this->PanelInscrit->msgInscrit->Text = Prado::localize('TEXT_MON_COMPTE_PERSONNEL');
                }
                if ($_GET['siret']) {
                    $siren = '';
                    if (is_object($this->_company)) {
                        $siren = $this->_company->getSiren();
                    } else {
                        $siren = Atexo_Util::atexoHtmlEntities($_GET['siren']);
                    }
                    if ('' != $siren) {
                        $this->_etablissement = $this->retreiveEtablissement($siren, Atexo_Util::atexoHtmlEntities($_GET['siret']));
                        if (!$this->IsPostBack) {
                            $this->displayEtablissement();
                        }
                    }
                } else {
                    $this->_etablissement = $this->_company;
                    if (!$this->IsPostBack) {
                        $this->displayEtablissement();
                    }
                }
            } elseif (isset($_GET['idNational']) && isset($_GET['pays'])) {
                $this->cpSiegeSocial->setMaxLength('10');
                $this->PanelInscrit->cpEtablissement->setMaxLength('10');
                $this->panelSiege->setCalledFor('Etrangere');
                $this->panelSiege->setIdNational(Atexo_Util::atexoHtmlEntities($_GET['idNational']));
                $this->panelSiege->setLieuEtablissement(Atexo_Util::atexoHtmlEntities($_GET['pays']));
                $this->pays->Text = Atexo_Util::encodeToHttp(Atexo_Util::atexoHtmlEntities($_GET['pays']));
                $this->PanelInscrit->paysEtablissement->Text = Atexo_Util::encodeToHttp(Atexo_Util::atexoHtmlEntities($_GET['pays']));
                $this->panelSiren->setVisible(false);
                $this->panelIdNational->setVisible(true);
                $this->idNational->Text = Atexo_Util::atexoHtmlEntities($_GET['idNational']);
                $this->_company = self::retrieveCompany(Atexo_Util::atexoHtmlEntities($_GET['idNational']));
                $this->PanelInscrit->setCompany($this->_company);
            } else {
                $this->inscription->visible = false;
                $this->demandeInscription->visible = true;
                $this->panelDemandeAuthentification->setMessage(Prado::localize('TEXT_IDENTIFICATION_POUR_ACCEDER_FONCTIONNALITE'));
                $this->panelDemandeAuthentification->setVisible(true);
            }

            if (!$this->IsPostBack) {
                $this->chargerComboFormeJuridique();
                if (Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
                    $atexoReferentiel = new Atexo_Ref();
                    $atexoReferentiel->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NACE_BAS_NIVEAU_CONFIG'));
                    $this->atexoReferentiel->afficherReferentiel($atexoReferentiel);
                }
                $this->displayCompany();
            }
        }
        $this->customizeForm();
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            '-',
            date('Y-m-d H:i:s')
        );
    }

    /**
     * retourne un objet Entreprise selon le l'identifiant du $_GET (SIREN ou IDENTIFIAN NATIONNAL).
     */
    public function retrieveCompany($identifiant)
    {
        $company = false;
        if (isset($_GET['siren'])) {
            // recherche dans la Base BDE par SIREN
            $company = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren($identifiant);
            if ($company instanceof Entreprise) {
                $company = $this->fillEntrepriseBdeObject($company);
            }
            if ((!is_object($company)) && (Atexo_Module::isEnabled('BaseQualifieeEntrepriseInsee', null))) {
                // Recherche de l'entreprise dans la base qualifiee
                $company = (new Atexo_Insee_Entreprise())->retreiveCompanyInsee($identifiant);
                if ($company instanceof CommonEntrepriseInsee) {
                    $company = $this->fillEntrepriseInseeObject($company);
                }
            }
        } else {
            // recherche dans la Base BDE par IDENTIFIAN NATIONNAL
            $company = (new Atexo_Entreprise())->retrieveCompanyByIdNational($identifiant, Atexo_Util::atexoHtmlEntities($_GET['pays']));
            if ($company instanceof Entreprise) {
                $company = $this->fillEntrepriseEtrangerObject($company);
            }
        }

        return $company;
    }

    public function retreiveEtablissement($siren, $nic)
    {
        //if (true) {
        $etablissement = false;
        if (Atexo_Module::isEnabled('BaseQualifieeEntrepriseInsee', null)) {
            $etablissement = (new Atexo_Insee_Entreprise())->retreiveEtablissementInsee($siren, $nic);
            if (is_object($etablissement)) {
                $etablissement = $this->fillEntrepriseInseeObject($etablissement);
            } else {
                $etablissement = $this->_company;
            }
        } else {
            $etablissement = $this->_company;
        }

        return $etablissement;
        //}
    }

    /**
     * alimenter les champs du bloc entreprise.
     */
    public function displayCompany()
    {
        $company = $this->_company;
        if ($company instanceof Atexo_Insee_EntrepriseVo) {
            if (('BDE' == $company->getTypeObject()) && ('creation' == $_GET['action'])) {
                $this->panelMonEntreprise->SetEnabled(false); //Si l'entreprise exite dans la base BDE
            } else {
                $this->panelMonEntreprise->SetEnabled(true);
            }
            if ($company) {
                $this->panelSiege->panelRaisonSociale->setVisible(true);
                $this->panelSiege->fill($company);
                if (($company->getPaysenregistrement() != Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) && ('' != $company->getSirenEtranger())) {
                    $this->panelIdNational->setVisible(true);
                    $this->panelSiren->setVisible(false);
                    $this->idNational->Text = $company->getSirenetranger();
                    $this->idNational->setEnabled(false);
                } else {
                    $this->panelIdNational->setVisible(false);
                    $this->panelSiren->setVisible(true);
                    $this->siren->Text = $company->getSiren();
                    if ('' != $company->getNicsiege()) {
                        $this->codeEtablissement->Text = $company->getNicsiege();
                    }
                    /*
                     * Rendre possible la modification de siren/siret par une entreprise à la première inscription
                     */
                    //$this->siren->setEnabled(false);
                    //$this->codeEtablissement->setEnabled(false);
                }
                $this->displayCompanyBde($company);
            } else {
                $this->panelSiege->panelRaisonSociale->setVisible(false);
            }
        } else {
            $this->panelPays->setVisible(false);
        }
    }

    /**
     * Charger la liste des formes juridiques.
     */
    public function chargerComboFormeJuridique()
    {
        $formeJuredique = (new Atexo_Entreprise())->retierveFormeJuredique();
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER');
        if ($formeJuredique) {
            $langue = Atexo_CurrentUser::readFromSession('lang');
            $getLibelleFormejuridique = 'getLibelleFormejuridique'.ucwords($langue);
            foreach ($formeJuredique as $formJurid) {
                if ($formJurid->$getLibelleFormejuridique()) {
                    $data[$formJurid->getFormejuridique()] = $formJurid->$getLibelleFormejuridique();
                } elseif (!$formJurid->getLibelleFormejuridique() && !$formJurid->$getLibelleFormejuridique()) {
                    $data[$formJurid->getFormejuridique()] = $formJurid->getFormejuridique();
                } elseif (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) && $formJurid->getLibelleFormejuridique()) {
                    $data[$formJurid->getFormejuridique()] = $formJurid->getLibelleFormejuridique();
                }
            }
        }
        $this->formeJuridique->DataSource = $data;
        $this->formeJuridique->dataBind();
    }

    /**
     * ajouter Nouveau compte inscrit et entreprise si n'existe pas.
     */
    public function addCompte()
    {
        $newCompany = null;
        $created = false;
        if ($this->isValid) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if ($this->_company) {
                if ('BDE' == $this->_company->getTypeObject()) {
                    $isCompany = true;
                } else {
                    $isCompany = false;
                }
            } else {
                $isCompany = false;
            }
            if ($isCompany) {
                $newInscrit = $this->PanelInscrit->setNewInscrit();
                $newInscrit->setEntrepriseid($this->_company->getId());
                if (Atexo_Module::isEnabled('EntrepriseValidationEmailInscription')) {
                    $newInscrit->setBloque(Atexo_Config::getParameter('ETAT_COMPTE_INSCRIT_INVALIDE'));
                    $newInscrit->setUid((new Atexo_Message())->getUniqueId());
                }
                $res = $newInscrit->save($connexion);
                //on met à jour la date de modification de l'entreprise si inscrit cree
                // TODO enregistremement de la date de modification d'une entreprise si inscrit cree
                //$this->_company->setDateModification(date('Y-m-d H:i:s'));
                //$this->_company->save($connexion);
                $entreprise = $this->_company;
            } else {
                $newCompany = new Atexo_Entreprise_EntrepriseVo();
                if ($_GET['siren']) {
                    $newCompany->setSiren(Atexo_Util::atexoHtmlEntities($this->siren->Text));
                    $newCompany->setNicSiege(Atexo_Util::atexoHtmlEntities($this->codeEtablissement->Text));
                    $newCompany->setPaysenregistrement(Atexo_Util::encodeToHttp(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')));
                    $newCompany->setPaysadresse(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
                    $newCompany->setAcronymePays(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
                } elseif ($_GET['idNational']) {
                    $newCompany->setSiren('');
                    $newCompany->setSirenetranger(Atexo_Util::atexoHtmlEntities($_GET['idNational']));
                    $newCompany->setPaysadresse(Atexo_Util::encodeToHttp(Atexo_Util::atexoHtmlEntities($_GET['pays'])));
                    $newCompany->setPaysenregistrement(Atexo_Util::encodeToHttp((new Atexo_Geolocalisation_GeolocalisationN2())->retrieveAccronymeCountryByDenomination(Atexo_Util::atexoHtmlEntities($_GET['pays']))));
                    $newCompany->setAcronymePays((new Atexo_Geolocalisation_GeolocalisationN2())->retrieveAccronymeCountryByDenomination(Atexo_Util::atexoHtmlEntities($_GET['pays'])));
                }
                $newCompany->setNom(($this->raisonSociale->Text));
                $newCompany->setSirenetranger(($this->panelSiege->idNational->Text));
                $newCompany->setAdresse(($this->addressSiegeSocial->Text));
                $newCompany->setAdresse2(($this->address2SiegeSocial->Text));
                $newCompany->setCodepostal(($this->cpSiegeSocial->Text));
                $newCompany->setVilleadresse(($this->villeSiegeSocial->Text));
                $newCompany->setPaysadresse(Atexo_Util::encodeToHttp(($this->pays->Text)));
                $newCompany->setTelephone(($this->telEntreprise->Text));
                $newCompany->setFax(($this->faxEntreprise->Text));
                // Statut Entreprise
                if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
                    if ($this->entrepriseEA->checked) {
                        $newCompany->setEntrepriseEA('1');
                    }
                    if ($this->entrepriseSIAE->checked) {
                        $newCompany->setEntrepriseSIAE('1');
                    }
                }
                if (Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) {
                    $newCompany->setCodeape(($this->apeNafNace1->Text).($this->apeNafNace2->Text));
                }
                if (Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
                    $newCompany->setCodeape($this->atexoReferentiel->codesRefSec->Value);
                }
                if (('0' != $this->formeJuridique->getSelectedValue()) && ($this->formeJuridique->getSelectedValue())) {
                    $newCompany->setFormejuridique($this->formeJuridique->SelectedValue);
                } else {
                    return false;
                }
                $newInscrit = $this->PanelInscrit->setNewInscrit();
                if (Atexo_Module::isEnabled('EntrepriseValidationEmailInscription')) {
                    $newInscrit->setBloque(Atexo_Config::getParameter('ETAT_COMPTE_INSCRIT_INVALIDE'));
                    $newInscrit->setUid((new Atexo_Message())->getUniqueId());
                }
                $newCompany->addCommonInscrit($newInscrit);
                $newCompany->setDateCreation(date('Y-m-d H:i:s'));
                $newCompany->setDateModification(date('Y-m-d H:i:s'));
                if (('0' != $this->panelRegionProvince->regionSiegeSocial->getSelectedValue()) && ($this->panelRegionProvince->regionSiegeSocial->getSelectedValue())) {
                    $newCompany->setRegion($this->panelRegionProvince->regionSiegeSocial->getSelectedValue());
                }
                if (('0' != $this->panelRegionProvince->provinceSiegeSocial->getSelectedValue()) && ($this->panelRegionProvince->provinceSiegeSocial->getSelectedValue())) {
                    $newCompany->setProvince($this->panelRegionProvince->provinceSiegeSocial->getSelectedValue());
                }
                $newCompany->setTelephone2(($this->telEntreprise2->Text));
                $newCompany->setTelephone3(($this->telEntreprise3->Text));
                $newCompany->setCnss(($this->idcnss->Text));
                $newCompany->setCapitalSocial(($this->idcapital->Text));
                $newCompany->setNumTax($this->idtaxeprof->Text);
                $newCompany->setRcNum(($this->numRc->Text));
                $newCompany->setRcVille(($this->villeRc->Text));
                if ('' != $this->domaineActivite->idsDomaines->Text) {
                    $newCompany->setDomainesActivites('#'.($this->domaineActivite->idsDomaines->Text));
                }
                if (Atexo_Module::isEnabled('CompteEntrepriseActivationInscriptionParAgent')) {
                    $newCompany->setCompteActif('0');
                } else {
                    $newCompany->setCompteActif('1');
                }
                $newCompany->setIfu(($this->ifu->Text));
                $entreprise = (new Atexo_Entreprise())->save($newCompany);
                $created = true;
                // Ajoute l'entreprise a l'index Entreprise de Lucene
                if (Atexo_Module::isEnabled('UtiliserLucene')) {
                    $searchLucene = new Atexo_Entreprise_SearchLucene();
                    $searchLucene->addEntreprise($entreprise);
                }
            }
            //Sauvegarde de l'historique
            $inscrit1 = $newInscrit->getPrenom().' '.$newInscrit->getNom();
            $mail1 = $newInscrit->getEmail();
            $idInscrit1 = $newInscrit->getId();
            $idEnt = $newInscrit->getEntrepriseId();
            $this->saveHistorique($newInscrit, $inscrit1, $mail1, $idInscrit1, $idEnt);

            self::EnvoiMailConfirmationInscrit($newInscrit, $entreprise, $created);
            if ($newCompany) {
                return $newInscrit;
            }

            return false;
        }
    }

    /**
     * Si nouveau compte creer redirection vers la page d'accueil authentifie.
     */
    public function actionBouttonEnregister($sender, $param)
    {
        if ($this->isValid) {
            // Mode Creation Compte entreprise et Compte Inscrit
            if ('creation' == $_GET['action']) {
                $inscritCreated = $this->addCompte();
                if ($inscritCreated) {
                    $login = $inscritCreated->getLogin();
                    $mdp = $inscritCreated->getMdp();
                    $userVo = new Atexo_UserVo();
                    $userVo->setLogin($login);
                    $userVo->setPassword($mdp);
                    $userVo->setType('entreprise');
                    if (Atexo_Module::isEnabled('EntrepriseValidationEmailInscription')) {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.ConfirmationCreationInscrit&uid='.$inscritCreated->getUid());
                    } elseif (Atexo_Module::isEnabled('SocleExterneEntreprise') && $login && ($this->getApplication()->getModule('auth')->login($userVo, null))) {
                        if ($_GET['goto'] && Atexo_Util::urlIsAllow($_GET['goto'])) {
                            $this->response->redirect(urldecode(Atexo_Util::atexoHtmlEntities($_GET['goto'])));
                        } else {
                            $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie');
                        }
                    } elseif ($login && $mdp
                    && ($this->getApplication()->getModule('auth')->login($userVo, null))) {
                        if ($_GET['goto'] && Atexo_Util::urlIsAllow($_GET['goto'])) {
                            $this->response->redirect(urldecode(Atexo_Util::atexoHtmlEntities($_GET['goto'])));
                        } else {
                            $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie');
                        }
                    } else {
                        $userVo->setCertificat('__CERT__');
                        $userVo->setPassword('__CERT__');
                        if ($this->getApplication()->getModule('auth')->login($userVo, null)) {
                            if ($_GET['goto'] && Atexo_Util::urlIsAllow($_GET['goto'])) {
                                $this->response->redirect(urldecode(Atexo_Util::atexoHtmlEntities($_GET['goto'])));
                            } else {
                                $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie');
                            }
                        }
                    }
                }
            }

            // Mode Modification Compte Utilisateur
            elseif ('ChangingUser' == $_GET['action']) {
                $result = self::updateUserCompte();
                if ($result >= 0) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie&action=ChangingUserSuccess');
                } elseif (-1 == $result) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie&action=ChangingUserfailur');
                    /*$this->panelMessageErreur->setVisible(true);
                     $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_MODIFICATION_COMPTE'));// Erreur enregistrement modifie*/
                }
            }

            // Mode Modification Compte Entreprise
            elseif ('ChangingCompany' == $_GET['action']) {
                $result = self::udateCompteCompany();
                $this->panelDeclarationHonneur->setVisible(false);
                if ($result) {
                    //                  $this->_company = $companyUpdated;
                    $this->panelMessage->setVisible(true);
                    $this->panelMessage->setMessage(Prado::localize('TEXT_COMPTE_MODIFIER_AVEC_SUCCE')); //enregistrement modifie
                    $this->buttonRetour->setVisible(true);
                    $this->buttonAnnuler->setVisible(false);
                    $this->buttonEnregistrer->setVisible(false);
                    $this->panelMonEntreprise->setVisible(false);
                } elseif (-1 == $result) {
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_MODIFICATION_COMPTE')); // Erreur enregistrement modifie
                }
            }
        }
    }

    public function actionBouttonAnnuler($sender, $param)
    {
        if ('creation' == $_GET['action']) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
        } elseif (('ChangingUser' == $_GET['action']) || ('ChangingCompany' == $_GET['action'])) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie');
        }
    }

    public function actionBouttonRetour($sender, $param)
    {
        $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie');
    }

    /**
     * Modifier compte Inscrit de l'utilisateur courant.
     *
     * @return -1 si erreur, >=0 si OK
     */
    public function updateUserCompte()
    {
        if ($this->IsValid) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $inscrit = CommonInscritPeer::retrieveByPK(Atexo_CurrentUser::getIdInscrit(), $connexion);
            $this->PanelInscrit->saveEtablissements();
            if ($inscrit) {
                $inscritUpdeted = $this->PanelInscrit->updateCompte($inscrit);
                if ($inscritUpdeted) {
                    $this->User->setNom($inscritUpdeted->getNom());
                    $this->User->setPrenom($inscritUpdeted->getPrenom());
                    $this->getApplication()->getModule('auth')->updateSessionUser($this->User);
                    //Sauvegarde de l'historique
                    $this->updateHistorique($inscritUpdeted);

                    //on met a jour la date de modification de l'entreprise si inscrit mis a jour
                    //$this->_company->setDateModification(date('Y-m-d H:i:s'));
                    //$this->_company->save($connexion);

                    try {
                        return $inscritUpdeted->Save($connexion);
                    } catch (Exception $e) {
                        Prado::log('Erreur Modification Compte Inscrit '.$e->__toString().'-', TLogger::FATAL, 'Fatal');

                        return -1;
                    }
                }
            }
        }
    }

    /**
     * Modifier compte entreprise.
     */
    public function udateCompteCompany()
    {
        if ($this->IsValid) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $company = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_CurrentUser::getIdEntreprise());
            if ($company) {
                if ((null !== $company->getSiren()) && (null == $company->getSirenetranger())) {
                    $company->setSiren(($this->siren->Text));
                    $company->setNicSiege(($this->codeEtablissement->Text));
                    $company->setPaysenregistrement(Atexo_Util::encodeToHttp(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')));
                    $company->setPaysadresse(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
                } elseif ((null !== $company->getSirenetranger()) && (null == $company->getSiren())) {
                    $company->setSiren('');
                    $company->setSirenetranger(Atexo_Util::atexoHtmlEntities($_GET['idNational']));
                    $company->setPaysenregistrement('ETRANGER');
                }
                if (!Atexo_Module::isEnabled('GestionEntrepriseParAgent') || 'ChangingCompany' == !$_GET['action']) {
                    $company->setNom(($this->raisonSociale->Text));
                }
                $company->setSirenetranger(($this->panelSiege->idNational->Text));
                $company->setAdresse(($this->addressSiegeSocial->Text));
                $company->setAdresse2(($this->address2SiegeSocial->Text));
                $company->setCodepostal(($this->cpSiegeSocial->Text));
                $company->setVilleadresse(($this->villeSiegeSocial->Text));
                $company->setPaysadresse(Atexo_Util::encodeToHttp(($this->pays->Text)));
                $company->setTelephone(($this->telEntreprise->Text));
                $company->setFax(($this->faxEntreprise->Text));
                if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
                    if ($this->entrepriseEA->checked) {
                        $company->setEntrepriseEA('1');
                    } else {
                        $company->setEntrepriseEA('0');
                    }
                    if ($this->entrepriseSIAE->checked) {
                        $company->setEntrepriseSIAE('1');
                    } else {
                        $company->setEntrepriseSIAE('0');
                    }
                }
                if (Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) {
                    $company->setCodeape(($this->apeNafNace1->Text.$this->apeNafNace2->Text));
                }
                if (Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
                    $company->setCodeape($this->atexoReferentiel->codesRefSec->Value);
                }
                $company->setFormejuridique($this->formeJuridique->getSelectedValue());
                $company->setDateModification(date('Y-m-d H:i:s'));
                if (('0' != $this->panelRegionProvince->regionSiegeSocial->getSelectedValue()) && ($this->panelRegionProvince->regionSiegeSocial->getSelectedValue())) {
                    $company->setRegion($this->panelRegionProvince->regionSiegeSocial->getSelectedValue());
                }
                if (('0' != $this->panelRegionProvince->provinceSiegeSocial->getSelectedValue()) && ($this->panelRegionProvince->provinceSiegeSocial->getSelectedValue())) {
                    $company->setProvince($this->panelRegionProvince->provinceSiegeSocial->getSelectedValue());
                }
                $company->setTelephone2(($this->telEntreprise2->Text));
                $company->setTelephone3(($this->telEntreprise3->Text));

                $company->setCnss(($this->idcnss->Text));
                $company->setCapitalSocial(($this->idcapital->Text));
                $company->setNumTax(($this->idtaxeprof->Text));
                $company->setRcNum(($this->numRc->Text));
                $company->setRcVille(($this->villeRc->Text));
                $company->setDomainesActivites(($this->domaineActivite->idsDomaines->Text));
                $company->setIfu(($this->ifu->Text));
                try {
                    $company = (new Atexo_Entreprise())->Save($company);
                    if (is_object($company)) {
                        if (Atexo_Module::isEnabled('UtiliserLucene')) {
                            $searchLucene = new Atexo_Entreprise_SearchLucene();
                            $searchLucene->addEntreprise($company);
                            // Mise a jour de l'index Entreprise de Lucene
                        }

                        return $company;
                    }

                    return false;
                } catch (Exception) {
                    return -1;
                }
            }
        }
    }

    /**
     * alimenter le bloc mon etablissement.
     */
    public function displayEtablissement()
    {
        $etablissement = $this->_etablissement;
        if (is_object($etablissement)) {
            $this->PanelInscrit->displayChampsEtablissement($etablissement);
        }
    }

    public function displayCompanyBde($company)
    {
        $this->pays->Text = Atexo_Util::encodeToHttp($company->getPaysadresse());
        $this->raisonSociale->Text = $company->getNom();
        $this->addressSiegeSocial->Text = $company->getAdresse();
        $this->address2SiegeSocial->Text = $company->getAdresse2();
        $this->cpSiegeSocial->Text = $company->getCodepostal();
        $this->villeSiegeSocial->Text = $company->getVilleadresse();
        $this->telEntreprise->Text = $company->getTelephone();
        $this->faxEntreprise->Text = $company->getfax();
        if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
            if ($company->getEntrepriseEA()) {
                $this->entrepriseEA->checked = true;
            } else {
                $this->entrepriseEA->checked = false;
            }
            if ($company->getEntrepriseSIAE()) {
                $this->entrepriseSIAE->checked = true;
            } else {
                $this->entrepriseSIAE->checked = false;
            }
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) {
            $this->apeNafNace1->Text = substr($company->getCodeape(), 0, 4);
            $this->apeNafNace2->Text = substr($company->getCodeape(), -1);
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
            $atexoRef = new Atexo_Ref();
            $atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NACE_BAS_NIVEAU_CONFIG'));
            $atexoRef->setSecondaires($company->getCodeApe());
            if ('creation' == $_GET['action']) {
                $this->atexoReferentiel->setCas('cas9');
            }
            $this->atexoReferentiel->afficherReferentiel($atexoRef);
        }
        if ($company->getFormejuridique()) {
            $this->formeJuridique->SelectedValue = $company->getFormejuridique();
        }
        if ($company->getRegion()) {
            $this->panelRegionProvince->regionSiegeSocial->SelectedValue = $company->getRegion();
        }
        if ($company->getProvince()) {
            $this->panelRegionProvince->provinceSiegeSocial->SelectedValue = $company->getProvince();
        }
        $this->telEntreprise2->Text = $company->getTelephone2();
        $this->telEntreprise3->Text = $company->getTelephone3();
        $this->idtaxeprof->Text = $company->getNumTax();
        $this->numRc->Text = $company->getRcNum();
        $this->villeRc->Text = $company->getRcVille();
        $this->domaineActivite->idsDomaines->Text = $company->getDomainesActivites();
        $this->idcnss->Text = $company->getCnss();
        $this->idcapital->Text = $company->getCapitalSocial();
        $this->ifu->Text = $company->getIfu();
    }

    /**
     * faire une correspondance de la forme
     * juridique recuperer par web service et celle en base.
     */
    public function getNewFormeJuridique($formJur)
    {
        $formJur = utf8_decode($formJur);
        if (0 == strcmp($formJur, 'Société par actions simplifiée (SAS)')) {
            return 'SAS';
        } elseif (0 == strcmp($formJur, 'Syndicat de copropriété')) {
            return 'Autre';
        } elseif (0 == strcmp($formJur, 'Autre société à responsabilité limitée')) {
            return 'SARL';
        } elseif (0 == strcmp($formJur, 'Société civile immobiliére')) {
            return 'AURL';
        } else {
            return '0';
        }
    }

    /**
     * Juste pour avoir un les memes get et set des deux objets.
     */
    public function fillEntrepriseInseeObject($companyInsee)
    {
        $entreprise = new Atexo_Insee_EntrepriseVo();
        $entreprise->setNom($companyInsee->getRaisonsociale());
        $entreprise->setSiren($companyInsee->getSiren());
        $entreprise->setAdresse($companyInsee->getAdretpost2());
        //$entreprise->setAdresse2($companyInsee->getAdretpost3());
        $entreprise->setVilleadresse($companyInsee->getAdrEtComLib());
        $entreprise->setCodeape($companyInsee->getApetsiege());
        $entreprise->setCodepostal($companyInsee->getAdretcodepost());
        $entreprise->setFormejuridique($this->getNewFormeJuridique($companyInsee->getCjLib()));
        $entreprise->setDescriptionActivite($companyInsee->getApenlib);
        if ('00000' != $companyInsee->getNic()) {
            $entreprise->setNic($companyInsee->getNic());
        } else {
            $entreprise->setNic('');
        }
        if ('00000' != $companyInsee->getNicsiege()) {
            $entreprise->setNicsiege($companyInsee->getNicsiege());
        } else {
            $entreprise->setNicsiege('');
        }
        $entreprise->setPaysenregistrement(Atexo_Util::encodeToHttp(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')));
        $entreprise->setPaysadresse(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
        $entreprise->setTypeObject('INSEE');

        return $entreprise;
    }

    /**
     * Juste pour avoir un les memes get et set des deux objets.
     */
    public function fillEntrepriseBdeObject($companyBde)
    {
        $entreprise = new Atexo_Insee_EntrepriseVo();
        $entreprise->setId($companyBde->getId());
        $entreprise->setNom($companyBde->getNom());
        $entreprise->setSiren($companyBde->getSiren());
        $entreprise->setAdresse($companyBde->getAdresse());
        $entreprise->setAdresse2($companyBde->getAdresse2());
        $entreprise->setVilleadresse($companyBde->getVilleadresse());
        $entreprise->setCodeape($companyBde->getCodeape());
        $entreprise->setCodepostal($companyBde->getCodepostal());
        $entreprise->setFormejuridique($companyBde->getFormejuridique());
        $entreprise->setDescriptionActivite($companyBde->getDescriptionActivite());
        $entreprise->setNicsiege($companyBde->getNicsiege());
        $entreprise->setPaysenregistrement(Atexo_Util::encodeToHttp(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')));
        $entreprise->setPaysadresse(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
        $entreprise->setTypeObject('BDE');
        $entreprise->setTelephone($companyBde->getTelephone());
        $entreprise->setFax($companyBde->getFax());
        $entreprise->setRegion($companyBde->getRegion());
        $entreprise->setProvince($companyBde->getProvince());
        $entreprise->setTelephone2($companyBde->getTelephone2());
        $entreprise->setTelephone3($companyBde->getTelephone3());
        $entreprise->setCnss($companyBde->getCnss());
        $entreprise->setCapitalSocial($companyBde->getCapitalSocial());
        $entreprise->setNumTax($companyBde->getNumTax());
        $entreprise->setRcNum($companyBde->getRcNum());
        $entreprise->setRcVille($companyBde->getRcVille());
        $entreprise->setDomainesActivites($companyBde->getDomainesActivites());
        $entreprise->setCompteActif($companyBde->getCompteActif());
        $entreprise->setIfu($companyBde->getIfu());
        $entreprise->setEntrepriseEA($companyBde->getEntrepriseEA());
        $entreprise->setEntrepriseSIAE($companyBde->getEntrepriseSIAE());

        return $entreprise;
    }

    /**
     * Juste pour avoir un les memes get et set des deux objets.
     */
    public function fillEntrepriseEtrangerObject($companyBde)
    {
        $entreprise = new Atexo_Insee_EntrepriseVo();
        $entreprise->setId($companyBde->getId());
        $entreprise->setNom($companyBde->getNom());
        $entreprise->setSirenetranger($companyBde->getSirenetranger());
        $entreprise->setAdresse($companyBde->getAdresse());
        $entreprise->setAdresse2($companyBde->getAdresse2());
        $entreprise->setVilleadresse($companyBde->getVilleadresse());
        $entreprise->setCodeape($companyBde->getCodeape());
        $entreprise->setCodepostal($companyBde->getCodepostal());
        $entreprise->setFormejuridique($companyBde->getFormejuridique());
        $entreprise->setDescriptionActivite($companyBde->getDescriptionActivite());
        $entreprise->setNicsiege($companyBde->getNicsiege());
        $entreprise->setPaysenregistrement(Atexo_Util::encodeToHttp($companyBde->getPaysenregistrement()));
        $entreprise->setPaysadresse(Atexo_Util::encodeToHttp($companyBde->getPaysadresse()));
        $entreprise->setTypeObject('BDE');
        $entreprise->setTelephone($companyBde->getTelephone());
        $entreprise->setTelephone2($companyBde->getTelephone2());
        $entreprise->setTelephone3($companyBde->getTelephone3());
        $entreprise->setFax($companyBde->getFax());
        $entreprise->setDomainesActivites($companyBde->getDomainesActivites());
        $entreprise->setCompteActif($companyBde->getCompteActif());
        $entreprise->setEntrepriseEA($companyBde->getEntrepriseEA());
        $entreprise->setEntrepriseSIAE($companyBde->getEntrepriseSIAE());

        return $entreprise;
    }

    public function EnvoiMailConfirmationInscrit($inscrit, $entreprise, $creation)
    {
        if ($creation && Atexo_Module::isEnabled('MailActivationCompteInscritEntreprise')) {
            $msg = Atexo_Message::getMessageConfirmationInscritWithCompanyInfo($inscrit, $entreprise);
        } else {
            $msg = Atexo_Message::getMessageConfirmationInscrit($inscrit, $entreprise);
        }

        $objet = Prado::Localize('DEFINE_TITRE_MAIL_CREATION_COMPTE_UTILISATEUR');
        $msg = '<p>'.str_replace("\n", '</p><p>', $msg).'</p>';
        (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise($inscrit, $objet, $msg);
        //Envoi mail de notification a l'administrateur
        $allAdmin = (new Atexo_Entreprise_Inscrit())->retrieveAllAdmins($inscrit->getEntrepriseId());
        if (is_array($allAdmin) && count($allAdmin)) {
            $profilInscrit = (new Atexo_Entreprise_Inscrit())->correspondanceIdProfilLibelle($inscrit->getProfil());
            $msgCorpsPersonaliseAdmin = str_replace('[__NOM_PRENOM_UTILISATEUR__]', $inscrit->getPrenom().' '.$inscrit->getNom(), Prado::Localize('DEFINE_CORPS_MAIL_CREATION_COMPTE_UTILISATEUR_TO_ADMIN'));
            $msgCorpsPersonaliseAdmin = str_replace('[__PROFIL__]', $profilInscrit, $msgCorpsPersonaliseAdmin);
            $msgCorpsPersonaliseAdmin = str_replace('[__PF__]', Atexo_Config::getParameter('PF_SHORT_NAME'), $msgCorpsPersonaliseAdmin);
            $msgCorpsPersonaliseAdmin = str_replace('[__NOM_SOCIETE__]', $entreprise->getNom(), $msgCorpsPersonaliseAdmin);
            foreach ($allAdmin as $admin) {
                if ($admin->getId() != $inscrit->getId()) {
                    (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise($admin, $objet, $msgCorpsPersonaliseAdmin);
                }
            }
        }
        //      Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'),$inscrit->getEmail(),Prado::localize('TEXT_CONFIRMATION_CREATION'),$msg);
    }

    /**
     * Adapter le formulaire pour un affichage qui correspond a MPE MAROC.
     */
    public function customizeForm()
    {
        $this->panelPays->setVisible(true);
        if (Atexo_Module::isEnabled('CompteEntrepriseTelephone2')) {
            $this->panelTelEntreprise2->setDisplay('Dynamic');
        } else {
            $this->panelTelEntreprise2->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseTelephone3')) {
            $this->panelTelEntreprise3->setDisplay('Dynamic');
        } else {
            $this->panelTelEntreprise3->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
            $this->panelStatutCompteEntreprise->setDisplay('Dynamic');
        } else {
            $this->panelStatutCompteEntreprise->setDisplay('None');
        } // création d'une entreprise locale || modification d'une entreprise locale
        if (isset($_GET['siren']) || ('ChangingCompany' == $_GET['action'] && $this->_company->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'))) {
            if (Atexo_Module::isEnabled('CompteEntrepriseCnss')) {
                $this->panelCnss->setDisplay('Dynamic');
            } else {
                $this->panelCnss->setDisplay('None');
            }
            $this->panelNumRc->setDisplay('None');
            $this->panelSiren->setDisplay('None');
            $this->validatorSiren->SetEnabled(false);
            if (Atexo_Module::isEnabled('CompteEntrepriseRcnum')) { //$this->panelNumRc->setDisplay("Dynamic");
                $this->panelSiren->setVisible(false);
                $this->validatorSiren->SetEnabled(false);
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseSiren')) {
                $this->panelSiren->setDisplay('Dynamic');
                $this->validatorSiren->SetEnabled(true);
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                $this->panelSiren->setDisplay('Dynamic');
                $this->validatorSiren->SetEnabled(false);
                $this->codeEtablissement->setDisplay('None');
                $this->siren->setCssClass('input-185');
                $this->infoBulle->setDisplay('None');
                $this->labelIdentifiantUnique->setDisplay('Dynamic');
                $this->labelSiren->setDisplay('None');
                $this->labelSiret->setDisplay('None');
                $this->labelSiegeSocial->setDisplay('None');
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseTaxProf')) {
                $this->panelIdTaxProf->setDisplay('Dynamic');
            } else {
                $this->panelIdTaxProf->setDisplay('None');
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseIfu')) {
                $this->panelIfu->setDisplay('Dynamic');
            } else {
                $this->panelIfu->setDisplay('None');
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseCapitalSocial')) {
                $this->panelCapital->setDisplay('Dynamic');
            } else {
                $this->panelCapital->setDisplay('None');
            }
        }
        if (!Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) {
            $this->panelCodeApe->setDisplay('None');
            $this->validatorApe->SetEnabled(false);
        } else {
            $this->panelCodeApe->setDisplay('Dynamic');
            $this->validatorApe->SetEnabled(true);
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite')) {
            $this->entrDomaineActivite->setDisplay('Dynamic');
            $this->domaineActivite->validatorDomainesActivites->SetEnabled(true);
            $this->domaineActivite->setAllLevel(true);
        } else {
            $this->entrDomaineActivite->setDisplay('None');
            $this->domaineActivite->validatorDomainesActivites->SetEnabled(false);
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseDeclarationHonneur')) {
            $this->panelDeclarationHonneur->setVisible(true);
        } else {
            $this->panelDeclarationHonneur->setVisible(false);
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseCpObligatoire')) {
            $this->validatorCpSiegeSocial->SetEnabled(true);
            $this->etoileCpSiegeSocial->setDisplay('Dynamic');
        } else {
            $this->validatorCpSiegeSocial->SetEnabled(false);
            $this->etoileCpSiegeSocial->setDisplay('None');
        } // pour que les champs soient non modifiable dans le cas d'une entreprise créer par agent
        if (Atexo_Module::isEnabled('GestionEntrepriseParAgent') && 'ChangingCompany' == $_GET['action']) {
            $this->panelRaisonSocial->SetEnabled(false);
            $this->panelInfoBulleRc->setDisplay('None');
        } //Pour l'affichage du code NACE en accedant au referentiel commun de gestion des compte entreprise
        if (!Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
            $this->panelCodeNaceRef->setDisplay('None');
            $this->validatorNaceRef->SetEnabled(false);
        } else {
            $this->panelCodeNaceRef->setDisplay('Dynamic');
            $this->validatorNaceRef->SetEnabled(true);
        }
    }

    /**
     * rafrechir le panel des domaines d'activites.
     */
    public function onCallBackDomaineActivites($sender, $param)
    {
        //echo "here";exit;
        //$this->entrDomaineActivite->render($param->NewWriter);
    }

    /**
     * rafrechir le panel des domaines d'activites.
     */
    public function refreshComposants($sender, $param)
    {
        $this->domaineActivite->panelDomaineActivite->render($param->NewWriter);
    }

    /**
     * Permet de sauvegarder l'historique d'un nouvel utilisateur inscrit.
     *
     * @param CommonInscrit $inscrit: objet inscrit
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistorique($inscrit, $inscrit1 = null, $mail1 = null, $idInscrit1 = null, $idEnt = null)
    {
        if ($inscrit instanceof CommonInscrit) {
            if ($inscrit->getProfil() == Atexo_Config::getParameter('PROFIL_INSCRIT_ATES')) {
                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_ADMIN');
            } else {
                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_UES');
            }
            $inscrit2 = $inscrit->getPrenom().' '.$inscrit->getNom();
            $email2 = $inscrit->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique($inscrit2, $email2, $action, $inscrit1, $mail1, $idInscrit1, $idEnt);
        }
    }

    /**
     * Permet de mettre à jour l'historique.
     *
     * @param CommonInscrit $inscrit: objet inscrit
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function updateHistorique($inscrit)
    {
        if ($inscrit instanceof CommonInscrit) {
            $action = Atexo_Config::getParameter('EVENEMENT_ADMIN_UPDATE');
            $inscrit2 = $inscrit->getPrenom().' '.$inscrit->getNom();
            $email2 = $inscrit->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique($inscrit2, $email2, $action);
        }
    }

    /**
     * Permet de valider la selection de l'etablissement
     * Verifie si au moins 1 etablissement a ete selectionne
     * Sinon affiche un message d'erreur.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function validerListeEtablissements($sender, $param)
    {
        $siretMonEtablissement = $this->PanelInscrit->etablissements->getSiretMonEtablissement();
        if (null == $siretMonEtablissement || empty($siretMonEtablissement)) {
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->validateurListeEtablissements->ErrorMessage = Prado::localize('MESSAGE_VOUS_DEVEZ_SELECTIONNER_AU_MOINS_UN_ETABLISSEMENT');

            return;
        }
    }
}
