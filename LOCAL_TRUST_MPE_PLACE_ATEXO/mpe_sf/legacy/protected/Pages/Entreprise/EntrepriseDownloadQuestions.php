<?php

namespace Application\Pages\Entreprise;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Register;

/**
 * Permet de télécharger le AVIS.
 *
 * @author mouslim MITALI <mouslim.Mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDownloadQuestions extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        if ($consultation) {
            $question = Atexo_Consultation_Register::retrieveQuestionByIdInscritAndRefCons(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit(), false, 0, 0, [], Atexo_Util::atexoHtmlEntities($_GET['idQuestion']));
            if ($question[0] instanceof CommonQuestionsDce) {
                $this->_idFichier = $question[0]->getIdFichier();
                $this->_nomFichier = $question[0]->getNomFichier();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            }
        }
    }
}
