<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Menu;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Complements;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CoffreFort;
use Application\Service\Atexo\Menu\Atexo_Menu_MenuVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_FormatLibreVo;
use Prado\Prado;

/**
 * Page detail d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDetailConsultation extends MpeTPage
{
    private $_consultation;
    private $_organismeAcronyme;
    private bool $_showDownloadRg = false;
    private bool $_showDownloadDce = false;
    private bool $_showDownloadComplement = false;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (isset($_GET['refConsultation'])) {
            $serviceSF = Atexo_Util::getSfService('app.mapping.consultation');
            if ($serviceSF) {
                $id = Atexo_Util::atexoHtmlEntities($_GET['refConsultation']);
                $org = $_GET['orgAcronyme'] ?? null;
                $code = $_GET['code'] ?? null;
                $idConsultation = $serviceSF->getConsultationId($id, $org, $code);
                $_GET['id'] = $idConsultation;
                if (-1 == $idConsultation) {
                    $this->mainPart->setVisible(false);
                    $this->errorPart->setVisible(true);
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize('MESSAGE_ERREUR_DEUX_CONSULTATIONS_EN_COLLISION'));

                    return '';
                }
            }
        }
        if ($_GET['id'] && $_GET['orgAcronyme'] && $_GET['code']) { //acces details consultation restreinte
            Atexo_CurrentUser::deleteFromSession('organisme');
            Atexo_CurrentUser::deleteFromSession('reference');
            Atexo_CurrentUser::deleteFromSession('codeAcces');
            $this->_consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            Atexo_CurrentUser::writeToSession('organisme', Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            if ($this->_consultation instanceof CommonConsultation &&
                $this->_consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                Atexo_CurrentUser::writeToSession('reference', $this->_consultation->getReferenceUtilisateur());
                Atexo_CurrentUser::writeToSession('codeAcces', $_GET['code']);
            }
        }

        //Redirection vers la nouvelle page de details de la consultation
        if (Atexo_Module::isEnabled('PanierEntreprise')) {
            if ($_GET['orgAcronyme'] && 0 == strcmp($_GET['id'], (int) $_GET['id'])) {
                $this->_consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                $mobile = '';
                $questions = '';
                if (isset($_GET['mobile'])) {
                    $mobile = '&mobile='.Atexo_Util::atexoHtmlEntities($_GET['mobile']);
                }
                if (isset($_GET['questions'])) {
                    $questions = '&questions='.Atexo_Util::atexoHtmlEntities($_GET['questions']);
                }
                if ($this->_consultation instanceof CommonConsultation) {
                    if ($_GET['code']) {
                        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
                        $extras = 'orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&code='.$_GET['code'].$mobile.$questions;
                        $url = Atexo_MpeSf::getUrlDetailsConsultation($consultationId, $extras);
                        $this->response->redirect($url);
                    } else {
                        $url = Atexo_MpeSf::getUrlDetailsConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), 'orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).$mobile.$questions);
                        $this->response->redirect($url);
                    }
                }
            }
        }//Affichage d'un message d'erreur lorsque l'acronyme de l'organisme n'est pas renseigne
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        $arrayOrg = [];
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                $arrayOrg[] = $uneOrganisme->getAcronyme();
            }
        }
        if (!$_GET['orgAcronyme']) {
            $this->mainPart->setVisible(false);
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE'));
        } elseif (!in_array($_GET['orgAcronyme'], $arrayOrg)) {
            echo Prado::localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            exit;
        } else {
            $dateDebAction = date('Y-m-d H:i:s');
            $this->panelMessage->setVisible(false);
            if (isset($_GET['Success'])) {
                $this->panelMessage->setVisible(true);
                $this->panelMessage->setMessage(Prado::localize('TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION'));
            } elseif (isset($_GET['Failed'])) {
                $this->panelMessageErreur->setVisible(true);
                $this->errorPart->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_MODIFICATION'));
            }
            if (0 == strcmp($_GET['id'], (int) $_GET['id'])) {
                $this->_consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                if (!$this->_consultation) {
                    $this->mainPart->setVisible(false);
                    $this->errorPart->setVisible(true);
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize('TEXT_AUCUNE_CONSULTATION'));
                } else {
                    $this->displayConsultation($this->_consultation);
                    if ($this->_consultation->getIdTypeAvis() != Atexo_Config::getParameter(TYPE_AVIS_CONSULTATION)) {
                        //$this->detailConsultation->setVisible(false);
                        $this->panelPieces->setVisible(false);
                        $this->panelActionsQuestion->setVisible(false);
                        $this->panelActionsRepondre->setVisible(false);
                        if (2 == $this->_consultation->getIdTypeAvis()) {
                            $this->linkRetourBas->NavigateUrl = 'index.php?page=Entreprise.EntrepriseAdvancedSearch&AvisInformation';
                            $this->linkNewSearch->NavigateUrl = 'index.php?page=Entreprise.EntrepriseAdvancedSearch&AllAnn';
                            $this->linkRetourBas2->NavigateUrl = 'index.php?page=Entreprise.EntrepriseAdvancedSearch&AvisInformation';
                            $this->linkNewSearch2->NavigateUrl = 'index.php?page=Entreprise.EntrepriseAdvancedSearch&AllAnn';
                        } elseif (4 == $this->_consultation->getIdTypeAvis()) {
                            $this->linkRetourBas->NavigateUrl = 'index.php?page=Entreprise.EntrepriseAdvancedSearch&AvisAttribution';
                            $this->linkNewSearch->NavigateUrl = 'index.php?page=Entreprise.EntrepriseAdvancedSearch&AllAnn';
                            $this->linkRetourBas2->NavigateUrl = 'index.php?page=Entreprise.EntrepriseAdvancedSearch&AvisAttribution';
                            $this->linkNewSearch2->NavigateUrl = 'index.php?page=Entreprise.EntrepriseAdvancedSearch&AllAnn';
                        }
                    } else {
                        //$this->linkRetourBas->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AllCons";
                        $dateFin = $this->_consultation->getDatefin();
                        //$dateFinPoursuivreAffichage = Atexo_Util::dateDansFutur ($dateFin, 0, $this->_consultation->getPoursuivreAffichage());
                        if (Atexo_Module::isEnabled('EntrepriseRepondreConsultationApresCloture') || (Atexo_Util::cmpIsoDateTime(date('Y-m-d H:i:s'), $dateFin) <= 0)) {
                            if (1 == $this->_consultation->getAutoriserReponseElectronique()) {
                                if (Atexo_Module::isEnabled('AssocierDocumentsCfeConsultation')) {
                                    $this->panelDocsCoffreFort->setVisible(true);
                                }
                                if (Atexo_Module::isEnabled('ReponsePasAPas')) {
                                    $this->labelReponseElectronique->Text = Prado::localize('SI_REPONSE_CLIQUEZ_BOUTON_FINALE');
                                    $this->buttonRepondreConsultation->setVisible(false);
                                    $this->reponsePasAPas->setVisible(true);
                                } else {
                                    $this->labelReponseElectronique->Text = Prado::localize('SI_REPONSE_CLIQUEZ_BOUTON').'.';
                                    $this->buttonRepondreConsultation->setVisible(true);
                                    $this->reponsePasAPas->setVisible(false);
                                }
                            } elseif (2 == $this->_consultation->getAutoriserReponseElectronique()) {
                                $this->panelDocsCoffreFort->setVisible(false);
                                $this->reponsePasAPas->setVisible(false);
                                $this->buttonRepondreConsultation->setVisible(false);
                                $this->labelReponseElectroniqueDehorsPF->Text = Prado::localize('TEXT_REPONSE_ELECTRONIQUE_EN_DEHORS_PLATE_FORME').'.';
                            } else {
                                //$this->buttonRepondreConsultationExpress->setVisible(false);
                                //$this->buttonSignerLesPieces->setVisible(false);
                                //$this->buttonEnvoyerMaReponse->setVisible(false);
                                $this->reponsePasAPas->setVisible(false);
                                $this->buttonRepondreConsultation->setVisible(false);
                                $this->labelReponseElectronique->Text = Prado::localize('TEXT_REPONSE_ELECTRONIQUE_REFUSEE').'.';
                            }
                        } else {
                            $this->panelActionsQuestion->setVisible(false);
                            $this->panelActionsRepondre->setVisible(false);
                        }
                    }
                    $this->displayAvisPubliciteFormatLibre($this->_consultation->getId(), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                    self::showHideTextesDocumentsJoints();
                    $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION4', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
                    $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
                    $arrayDonnees = [];
                    $arrayDonnees['ref'] = $this->_consultation->getId();
                    $arrayDonnees['org'] = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                    $arrayDonnees['IdDescritpion'] = $IdDesciption;
                    $arrayDonnees['afficher'] = true;
                    $arrayDonnees['logApplet'] = '';
                    $arrayDonnees['description'] = $description;
                    Atexo_InscritOperationsTracker::trackingOperations(
                        Atexo_CurrentUser::getIdInscrit(),
                        Atexo_CurrentUser::getIdEntreprise(),
                        $_SERVER['REMOTE_ADDR'],
                        date('Y-m-d'),
                        $dateDebAction,
                        substr($_SERVER['QUERY_STRING'], 0),
                        '-',
                        date('Y-m-d H:i:s'),
                        $arrayDonnees
                    );
                }
            } else {
                $this->mainPart->setVisible(false);
                $this->errorPart->setVisible(true);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_AUCUNE_CONSULTATION'));
            }
        }
    }

    public function displayConsultation($consultation)
    {
        $this->_organismeAcronyme = $consultation->getOrganisme();
        if ($_GET['orgAcronyme']) {
            $organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        } else {
            $organisme = $consultation->getOrganisme();
        }
        $this->onClickDownloadDce($organisme);
        $this->setDownlodReglementNavigateUrl($organisme);
        $this->setDownlodComplementNavigateUrl($organisme);
    }

    /**
     * Retourne la taille du dossier DCE.
     */
    public function setTailDceFile($Org)
    {
        $dce = (new Atexo_Consultation_Dce())->getDce($this->_consultation->getId(), $Org);

        if ($dce) {
            $tailleDce = (new Atexo_Consultation_Dce())->getTailleDCE($dce, $_GET['orgAcronyme']);
            $this->linkDownloadDce->Text = Prado::localize('DEFINE_DOSSIER_CONSULTATION').' - '.$tailleDce;
            //$this->tailleDce->NavigateUrl .= $this->linkDownloadDce->NavigateUrl;
        }
    }

    /**
     * Retourne la taille du dossier Regelement.
     */
    public function setTailleComplement($complement, $organisme)
    {
        if ($complement) {
            $tailleComplement = Atexo_Util::arrondirSizeFile((new Atexo_Blob())->getTailFile($complement->getComplement(), $organisme) / 1024);
            $this->linkDownloadComplement->Text = Prado::localize('DEFINE_SAVOIR_PLUS_CONSULTATION').' - '.$tailleComplement;
            //$this->tailleComplement->NavigateUrl .= $this->linkDownloadComplement->NavigateUrl;
        }
    }

    public function setTailReglement($organisme)
    {
        $rg = (new Atexo_Consultation_Rg())->getReglement($this->_consultation->getId(), $organisme);
        if ($rg) {
            $tailleRg = Atexo_Util::arrondirSizeFile((new Atexo_Blob())->getTailFile($rg->getRg(), $organisme) / 1024);
            $this->linkDownloadReglement->Text = Prado::localize('REGELEMENT_CONS').' - '.$tailleRg;
            //$this->tailleReglement->NavigateUrl .= $this->linkDownloadReglement->NavigateUrl;
        }
    }

    /**
     * Cree le lien de telechargement du dossier DCE.
     */
    public function onClickDownloadDce($Org)
    {
        $dce = '';
        $dce = (new Atexo_Consultation_Dce())->getDce(Atexo_Util::atexoHtmlEntities($_GET['id']), $Org);

        if ($dce) {
            if (0 != $dce->getDce()) {
                $this->linkDownloadDce->NavigateUrl = 'index.php?page=Entreprise.EntrepriseDemandeTelechargementDce&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                $this->setTailDceFile($Org);
                $this->_showDownloadDce = true;
            } else {
                $this->_showDownloadDce = false;
            }
        } else {
            $this->_showDownloadDce = false;
        }
    }

    /**
     * Cree le lien de dossier Reglement de la consultation.
     */
    public function setDownlodReglementNavigateUrl($organisme)
    {
        $rg = '';
        $rg = (new Atexo_Consultation_Rg())->getReglement($this->_consultation->getId(), $organisme);
        if ($rg) {
            if (0 != $rg->getRg()) {
                $this->linkDownloadReglement->NavigateUrl = 'index.php?page=Entreprise.EntrepriseDownloadReglement&id='.
                                                           base64_encode($this->_consultation->getId()).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                $this->setTailReglement($organisme);
                $this->_showDownloadRg = true;
            } else {
                $this->_showDownloadRg = false;
            }
        } else {
            $this->_showDownloadRg = false;
        }
    }

    public function setDownlodComplementNavigateUrl($organisme)
    {
        $complement = '';
        $complement = (new Atexo_Consultation_Complements())->getComplement($this->_consultation->getId(), $organisme);
        if ($complement) {
            if (0 != $complement->getComplement()) {
                $this->linkDownloadComplement->NavigateUrl = 'index.php?page=Entreprise.EntrepriseDownloadComplement&id='.
                                                            base64_encode($this->_consultation->getId()).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                $this->setTailleComplement($complement, $organisme);
                $this->_showDownloadComplement = true;
            } else {
                $this->_showDownloadComplement = false;
            }
        } else {
            $this->_showDownloadComplement = false;
        }
    }

    public function setUrlLots($consultation)
    {
        $arrayLots = $consultation->getAllLots();

        if (is_array($arrayLots) && count($arrayLots) > 0) {
            $this->linkDetailLots->NavigateUrl = "javascript:popUp('".Atexo_Config::getParameter('URL_MPE_ORGANISME').'/'.$consultation->getOrganisme().
                                                 '/detail_lots.php?org='.$consultation->getOrganisme().'&cons_ref='.$consultation->getId()."','yes')";
        } else {
            $this->linkDetailLots->Text = '-';
        }
    }

    public function getOrganismeAcronyme()
    {
        return $this->_organismeAcronyme;
    }

    public function repondreConsultation($sender, $param)
    {
        $this->response->redirect('index.php?page=Entreprise.FormulaireReponse&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&org='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));

        $menuVo = new Atexo_Menu_MenuVo();
        $menuVo->setCmd('accept_explication_send_offers');
        $menuVo->setOrg(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        $menuVo->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $menuVo->setRefParamName('refMarche');

        (new Atexo_Menu())->activateMenuEntreprise($menuVo);
    }

    public function signerPiecesReponseConsultation($sender, $param)
    {
        $this->response->redirect('index.php?page=Entreprise.FormulaireSignatureReponse&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&org='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));

        $menuVo = new Atexo_Menu_MenuVo();
        $menuVo->setCmd('accept_explication_send_offers');
        $menuVo->setOrg(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        $menuVo->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $menuVo->setRefParamName('refMarche');

        (new Atexo_Menu())->activateMenuEntreprise($menuVo);
    }

    public function envoyerPiecesReponseConsultation($sender, $param)
    {
        $this->response->redirect('index.php?page=Entreprise.FormulaireReponsePasAPas&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&org='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));

        $menuVo = new Atexo_Menu_MenuVo();
        $menuVo->setCmd('accept_explication_send_offers');
        $menuVo->setOrg(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        $menuVo->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $menuVo->setRefParamName('refMarche');

        (new Atexo_Menu())->activateMenuEntreprise($menuVo);
    }

    public function poserQuestion($sender, $param)
    {
        if (Atexo_Module::isEnabled('PoserQuestionNecessiteAuthentification') && !Atexo_CurrentUser::getIdInscrit()) {
            $this->response->redirect('index.php?page=Entreprise.EntrepriseHome&goto='.
            urlencode('?page=Entreprise.EntrepriseFormulairePoserQuestion&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme'])));
        }

        $this->response->redirect('index.php?page=Entreprise.EntrepriseFormulairePoserQuestion&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
    }

    public function displayAvisPubliciteFormatLibre($consultationId, $organisme)
    {
        $listeFormAvis = (new Atexo_Publicite_Avis())->retreiveListAvisEnvoye($consultationId, $organisme);
        $dataSource = [];
        $index = 0;
        $atexoBlob = new Atexo_Blob();
        foreach ($listeFormAvis as $oneAvis) {
            $newFormLibre = new Atexo_Publicite_FormatLibreVo();
            if ($oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT') || $oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_AVIS_IMPORTES')) {
                if ($oneAvis->getTypeDocGenere() == Atexo_Config::getParameter('TYPE_DOC_GENERE_AVIS')) {
                    if ($oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_AVIS_IMPORTES')) {
                        $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_PUB'));
                    } else {
                        $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_PUBLICITE').' - '.strtoupper($oneAvis->getLangue()));
                    }
                } else {
                    $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_COMPLEMENTAIRE'));
                }
                $newFormLibre->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
                $taille = Atexo_Util::arrondirSizeFile($atexoBlob->getTailFile($oneAvis->getAvis(), $organisme) / 1024);
                $newFormLibre->setTaille($taille);
            } else {
                $newFormLibre->setLibelleType(Prado::localize('TEXT_URL_ACCES_DIRECT'));
                $newFormLibre->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_URL'));
                $newFormLibre->setUrl($oneAvis->getUrl());
            }
            $newFormLibre->setId($oneAvis->getId());
            $newFormLibre->setDatePub(Atexo_Util::iso2frnDateTime($oneAvis->getDateCreation(), false));
            $newFormLibre->setStatut($oneAvis->getStatut());
            $dataSource[$index] = $newFormLibre;
            ++$index;
        }
        // Ajouter les liens PDF au BOAMP dans le data source
        $listLienPdfBoamp = (new Atexo_Publicite_Destinataire())->retrieveAnnonceByRefCons($consultationId, $organisme);
        $dataSourceBoamp = [];
        $indexX = 0;
        foreach ($listLienPdfBoamp as $oneAnnonce) {
            if ('' != $oneAnnonce->getLienBoamp()) {
                $newFormxml = new Atexo_Publicite_FormatLibreVo();
                $newFormxml->setLibelleType(Prado::localize('NOM_LIEN_AVIS_BOAMP'));
                $newFormxml->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_URL_BOAMP'));
                $newFormxml->setUrl($oneAnnonce->getLienBoamp());
                $newFormxml->setStatut($oneAnnonce->getStatutDestinataire());
                $dataSourceBoamp[$indexX] = $newFormxml;
                ++$indexX;
            }
        }

        $this->repeaterAvis->DataSource = [...$dataSource, ...$dataSourceBoamp];
        $this->repeaterAvis->DataBind();
    }

    public function getToolTip($idEntreprise, $consultationId, $orgConsultation)
    {
        $piecesEntreprises = (new Atexo_Entreprise_CoffreFort())->retrievePiecesConsultationDocumentCfeByIdEntreprise($idEntreprise, $consultationId, $orgConsultation);
        if (!is_array($piecesEntreprises)) {
            $piecesEntreprises = [];
        }

        return Prado::localize('TEXT_AJOUTER_DOCUMENTS_COFFRE_FORT').' : '.count($piecesEntreprises).' '.Prado::localize('DOCUMENTS_AJOUTES');
    }

    public function showHideTextesDocumentsJoints()
    {
        $dce = (new Atexo_Consultation_Dce())->getDce(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        $rg = (new Atexo_Consultation_Rg())->getReglement($this->_consultation->getId(), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        $complement = (new Atexo_Consultation_Complements())->getComplement($this->_consultation->getId(), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        if (!$dce && !$rg && !$complement) {
            $this->divPiecesConsultation->setVisible(false);
            $this->divNoPiecesConsultation->setVisible(true);
        } else {
            $this->divPiecesConsultation->setVisible(true);
            $this->divNoPiecesConsultation->setVisible(false);
        }
    }

    public function showDownloadRg()
    {
        return $this->_showDownloadRg;
    }

    public function showDownloadDce()
    {
        return $this->_showDownloadDce;
    }

    public function showDownloadComplement()
    {
        return $this->_showDownloadComplement;
    }

    public function getIdTypeAvisConsultation()
    {
        if ($this->_consultation) {
            return $this->_consultation->getIdTypeAvis();
        }
    }

    public function getTitleBoutonReponseExpress()
    {
        if ($this->_consultation) {
            if ('1' == $this->_consultation->getSignatureOffre()) {
                return Prado::localize('SIGNER_ET_ENVOYER_MA_REPONSE');
            } else {
                return Prado::localize('ENVOYER_MA_REPONSE');
            }
        }
    }
}
