<?php

namespace Application\Pages\Entreprise;

use App\Service\ApiGateway\WebServicesSynchro;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\Entreprise\EntrepriseAPIService;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_HistorisationMotDePasse;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_InscritHistorique;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_SearchLucene;
use DateTime;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Permet de gerer la creation d'un nouvel compte entreprise.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.9.0
 *
 * @copyright Atexo 2015
 */
class FormulaireCompteEntreprise extends MpeTPage
{
    private $entreprise = null;
    public string $pageTitle = 'TITRE';

    /**
     * Modifie la valeur de [entreprise].
     *
     * @param Atexo_Entreprise_EntrepriseVo $value la valeur a mettre
     *
     * @return Atexo_Entreprise_EntrepriseVo
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setEntreprise($value)
    {
        $this->setViewState('entreprise', $value);
        $this->entreprise = $value;

        return $value;
    }

    /**
     * Recupere la valeur du [entreprise].
     *
     * @return Atexo_Entreprise_EntrepriseVo la valeur courante [entreprise]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEntreprise()
    {
        return $this->getViewState('entreprise');
    }

    /**
     * Initialisation.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function onInit($param)
    {
        $this->pageTitle = 'TITRE_MES_COORDONNEES';
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Chargement de la page.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function onLoad($param)
    {
        $this->panelInfosEntreprise->setPaysEtablissement($this->getPaysEtablissement());
        if (!$this->IsPostBack) {
            if ('creation' == $_GET['action']) {
                if (!$this->User->IsGuest && !Atexo_CurrentUser::isAgent() && !Atexo_CurrentUser::isAgentSocle()) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie');
                }
                $this->etablissements->setMode(3);
                $this->setEntreprise($this->retrieveCompany());
                $this->panelInfosEntreprise->setEntreprise($this->getEntreprise());
                $this->panelInfosEntreprise->setIdentifiantUnique($this->getIdentifiantUnique());
            } elseif (('ChangingCompany' == $_GET['action'] && Atexo_CurrentUser::isATES()) || 'ChangingUser' == $_GET['action']) {
                $this->etablissements->setMode(3);
                $entreprise = EntreprisePeer::retrieveByPK(Atexo_CurrentUser::getIdEntreprise());
                if ($entreprise) {
                    $company = (new Atexo_Entreprise())->getEntrepriseVoFromEntreprise($entreprise);
                    if ($company) {
                        $this->setEntreprise($company);
                        if ($this->isEntrepriseLocale()) {
                            $typeEntreprise = ($this->isEntrepriseLocale()) ? '1' : '2';
                            $mode = null;
                            Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($this->getSiren(), $typeEntreprise, $mode);
                            $this->setModeSaisie($mode);
                        }
                        $this->panelStatutEntreprise->setEntreprise($this->getEntreprise());
                    } else {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
                    }
                }
            } else {
                $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
            }
            $this->chargerEtablissement();
            $this->setViewState('isEntrepriselocale', $this->isEntrepriseLocale());
            $this->panelInfosEntreprise->chargerInformations();
            $this->panelInfosEntreprise->setIsEntrepriselocale($this->isEntrepriseLocale());
            $this->customizeForm();
            $this->gererAffichageMessageAvertissement();
            $this->panelMessageConfirmation->visible = false;
        }
    }

    /**
     * Permet de gerer l'affichage des elements du formulaire
     * Gere l'activation/desactivation des validateurs
     * Rempli des textes au chargement de la page.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function customizeForm()
    {
        if ('creation' == $_GET['action']) {
            $this->PanelInscrit->setVisible(true);
            $this->panelStatutEntreprise->setVisible(false);
            $this->validateurListeEtablissements->enabled = true;
            $this->texteSymboleChampsObligatoires->visible = true;
            $this->texteSymboleChampsObligatoires->Text = Prado::localize('TEXT_LE_SYMBOLE').'<span class="champ-oblig"> * </span>'.Prado::localize('TEXT_INDIQUE_CHAMPS_OBLIGATOIRES');
            $this->labelPageFilAriane->Text = Prado::localize('INSCRIPTION');
        } elseif ('ChangingCompany' == $_GET['action']) {
            $this->PanelInscrit->setVisible(false);
            $this->panelStatutEntreprise->setVisible(true);
            $this->validateurListeEtablissements->enabled = false;
            $this->texteSymboleChampsObligatoires->visible = false;
            $this->labelPageFilAriane->Text = Prado::localize('TEXT_MY_ENTREPRISE');
        } elseif ('ChangingUser' == $_GET['action']) {
            $this->PanelInscrit->setVisible(true);
            $this->panelStatutEntreprise->setVisible(false);
            $this->panelInfosEntreprise->setVisible(true);
            $this->validateurListeEtablissements->enabled = false;
            $this->texteSymboleChampsObligatoires->visible = false;
        }
    }

    /**
     * Permet de gerer le chargement des etablissements.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function chargerEtablissement()
    {
        $listeEtablissements = null;
        $entrepriseExiste = false;
        if ($this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo) {
            $codeMonEtablissement = null;
            $sirenMonEtablissement = null;
            if ('creation' == $_GET['action']) {
                $listeEtablissements = $this->getEntreprise()->getListeEtablissements();
                $codeMonEtablissement = $_GET['siret'];
                $sirenMonEtablissement = $_GET['siren'];
            } elseif ('ChangingCompany' == $_GET['action'] || 'ChangingUser' == $_GET['action']) {
                $listeEtablissements = (new Atexo_Entreprise_Etablissement())->retreiveEtablissementsByIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
                $listeEtablissements = (new Atexo_Entreprise_Etablissement())->fillEtablissementsVoArray($listeEtablissements);
                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
                $codeMonEtablissement = $inscrit->getCodeEtablissement();
                $sirenMonEtablissement = $this->getEntreprise()->getSiren();
            }
            $this->etablissements->setSiretMonEtablissement($codeMonEtablissement);
            $this->etablissements->setSirenEntreprise($sirenMonEtablissement);
            if ($this->isEntrepriseLocale()) {
                $this->etablissements->setIsEntrepriseLocale(true);
            }
            if ($this->getEtablissementSiege() instanceof Atexo_Entreprise_EtablissementVo) {
                $this->panelInfosEntreprise->setEtablissementSiege($this->getEtablissementSiege());
            }
            $this->etablissements->setEtablissementsList($listeEtablissements);
            $this->etablissements->setSiretSiegeSocial(substr($this->getEntreprise()->getSiretSiegeSocial(), -5));
            if ($this->getEntreprise()->getId()) {
                $entrepriseExiste = true;
            }
        } else {
            if ($this->isEntrepriseLocale()) {
                $this->etablissements->setIsEntrepriseLocale(true);
            }
            $this->etablissements->setSirenEntreprise(Atexo_Util::atexoHtmlEntities($_GET['siren']));
        }
        $modeDegradee = true;
        if ('mode3' == $this->getModeSaisie()) {
            $modeDegradee = false;
        } else {
            $this->etablissements->setMode(4);
        }
        $this->etablissements->setAjoutManuel($modeDegradee);
        $this->etablissements->setEntrepriseExiste($entrepriseExiste);
        $this->etablissements->initComposant();
    }

    /**
     * Permet de tester si une entreprise est locale ou etrangere.
     *
     * @return bool : true si l'entreprise est locale false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function isEntrepriseLocale()
    {
        if ($this->getViewState('isEntrepriselocale')) {
            return $this->getViewState('isEntrepriselocale');
        }
        $isEntrepriselocale = false;
        if (!isset($_GET['siren']) && !isset($_GET['siret']) && !isset($_GET['idNational']) && $this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo) {
            if ($this->getEntreprise()->getSiren()) {
                $isEntrepriselocale = true;
            } elseif ($this->getEntreprise()->getSirenetranger()) {
                $isEntrepriselocale = false;
            }
        } elseif (isset($_GET['siren']) || isset($_GET['siret'])) {
            $isEntrepriselocale = true;
        } elseif (isset($_GET['idNational'])) {
            $isEntrepriselocale = false;
        }
        $this->setViewState('isEntrepriselocale', $isEntrepriselocale);

        return $isEntrepriselocale;
    }

    /**
     * Permet le lieu d'etablissement de l'entreprise (pays).
     *
     * @return string : pays etablissement
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getPaysEtablissement()
    {
        $pays = null;
        if ($this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo) {
            $pays = $this->getEntreprise()->getPaysenregistrement();
        } else {
            if ($this->isEntrepriseLocale()) {
                $pays = Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE');
            } elseif (isset($_GET['pays'])) {
                $pays = Atexo_Util::atexoHtmlEntities($_GET['pays']);
            }
        }
        $this->setViewState('paysEtablissement', $pays);

        return $pays;
    }

    /**
     * Permet de gerer l'affichage du message d'avertissement pour la disponiblite des informations d'identifications automatique des entreprises.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function gererAffichageMessageAvertissement()
    {
        $this->panelMessageAvertissement->visible = false;
        $this->panelMessageConfirmationDisponibiliteInfosInsee->visible = false;
        if ('mode2' == $this->getModeSaisie()) {//Mode degradee avec message d'avertissement
            $this->panelMessageAvertissement->visible = true;
            $this->messageAvertissement->setMessage(Prado::localize('AVERTISSEMENT_DISPONIBLITE_IDENTIFICATION_AUTOMATIQUE_ENTREPRISE'));
        } elseif ('mode3' == $this->getModeSaisie()) {//Mode normal
            $this->panelMessageConfirmationDisponibiliteInfosInsee->visible = true;
        } elseif ('mode4' == $this->getModeSaisie()) {
            $this->panelMessageAvertissement->visible = true;
            $this->messageAvertissement->setMessage(Prado::localize('AVERTISSEMENT_SIRET_INVALIDE'));
        }
    }

    /**
     * Permet d'enregistrer les données de la creation d'un nouvel entreprise et/ou etablissements.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function save($sender, $param)
    {
        if ($this->isValid) {
            // Mode Creation Compte entreprise et Compte Inscrit
            if ('creation' == $_GET['action']) {
                $inscritCreated = $this->creerEntreprise();
                if ($inscritCreated) {
                    $login = $inscritCreated->getLogin();
                    $mdp = $inscritCreated->getMdp();
                    $userVo = new Atexo_UserVo();
                    $userVo->setLogin($login);
                    $userVo->setPassword($mdp);
                    $userVo->setType('entreprise');
                    $flashbag = Atexo_Util::getSfService("mpe.session");
                    if (Atexo_Module::isEnabled('EntrepriseValidationEmailInscription')) {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.ConfirmationCreationInscrit&uid='.$inscritCreated->getUid());
                    } elseif (Atexo_Module::isEnabled('SocleExterneEntreprise') && $login) {
                        $flashbag->getFlashBag()->add('success', 'USER_CREATED_MESSAGE');
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL') . 'entreprise/login');
                    } elseif ($login && $mdp) {
                        $flashbag->getFlashBag()->add('success', 'USER_CREATED_MESSAGE');
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL') . 'entreprise');
                    } else {
                        $userVo->setCertificat('__CERT__');
                        $userVo->setPassword('__CERT__');
                        $flashbag->getFlashBag()->add('success', 'USER_CREATED_MESSAGE');
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL') . 'entreprise');
                    }
                }
            } elseif ('ChangingCompany' == $_GET['action']) {
                $this->modifierEntreprise();
                $this->panelStatutEntreprise->chargertInformation($this->getEntreprise());
            } elseif ('ChangingUser' == $_GET['action']) {
                if (Atexo_Module::isEnabled('entrepriseMotsDePasseHistorises') && $this->checkRecentPassword()) {
                    $this->Page->divValidationSummary->addServerError(
                        Prado::localize(
                            'HISTORISATION_MESSAGE_ERROR',
                            ['nb' => Atexo_Module::retrieveModules()->getEntrepriseMotsDePasseHistorises()]
                        ),
                        false
                    );
                    $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";

                    return true;
                }

                $result = self::updateUserCompte();
                if ($result) {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseAccueilAuthentifie&action=ChangingUserSuccess');
                } else {
                    $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseAccueilAuthentifie&action=ChangingUserfailur');
                }
            }
        }
        $this->scriptJs->text = "<script>J('" . $this->buttonEnregistrer->getClientId() . "').show();</script>";
    }

    private function checkRecentPassword()
    {
        $email = $this->PanelInscrit->emailPersonnel->SafeText;
        $nouveauMotDePasse = $this->PanelInscrit->password->SafeText;

        return (new Atexo_HistorisationMotDePasse())->checkRecentPassword($email, $nouveauMotDePasse);
    }

    /**
     * Permet de creer un nouveau compte entreprise.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function creerEntreprise()
    {
        $connexion = null;
        if (null == $this->getEntreprise()) {
            $company = $this->panelInfosEntreprise->chargerObjetEntreprise();
            $company->setDateCreation(date('Y-m-d H:i:s'));
            $company->setDateModification(date('Y-m-d H:i:s'));
            if (false === $company) {
                return false;
            }
        } else {
            $company = $this->getEntreprise();
            if ($this->isEntrepriseLocale()) {
                $company->setPaysenregistrement(Atexo_Util::encodeToHttp(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')));
                $company->setPaysadresse(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
                $company->setAcronymePays(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
            }
        }
        if (!$this->isEntrepriseLocale()) {
            $company->setSaisieManuelle(1);
        }
        $newInscrit = $this->PanelInscrit->setNewInscrit($this->getEntreprise());

        /**
         * Bloc save RGPD
         */

        $dateValidationRgpd = new \DateTime();
        $rgpdCommunicationPlace = $this->PanelInscrit->communicationPlaceRgpd->Checked;
        $enqueteRgpd = $this->PanelInscrit->enqueteRgpd->Checked;
        $rgpdCommunication = $this->PanelInscrit->communicationRgpd->Checked;


        $newInscrit->setDateValidationRgpd($dateValidationRgpd);
        $newInscrit->setRgpdCommunicationPlace($rgpdCommunicationPlace);
        $newInscrit->setRgpdEnquete($enqueteRgpd);
        $newInscrit->setRgpdCommunication($rgpdCommunication);

        if (Atexo_Module::isEnabled('EntrepriseValidationEmailInscription')) {
            $newInscrit->setBloque(Atexo_Config::getParameter('ETAT_COMPTE_INSCRIT_INVALIDE'));
            $newInscrit->setUid((new Atexo_Message())->getUniqueId());
        }

        //Enregistrement de la liste des etablissements a l'entreprise
        $listeEtablissements = $this->etablissements->getEtablissementsList();
        $company->setListeEtablissements($listeEtablissements);
        //On stocke la valeur du code de l'etablissement car apres la sauvegarde de l'entreprise on perd cette valeur
        $codeMonEtablissement = $this->etablissements->getSiretMonEtablissement();

        //On stocke le code de l'etablissement de l'inscrit à l'objet $inscrit
        if ($this->isEntrepriseLocale()) {
            $newInscrit->setSiret($codeMonEtablissement);
        }

        $companyCreated = false;
        try {
            //Enregistrement de l'entreprise
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $connexion->beginTransaction();
            $company->setNicsiege($this->etablissements->getSiretSiegeSocial());

            $entreprise = (new Atexo_Entreprise())->save($company, $connexion);
            //supression des établissements
            $etablissementsToDelate = $this->etablissements->getListeIdsEtabToDelete();
            (new Atexo_Entreprise_Etablissement())->deleteEtablissements($etablissementsToDelate, $connexion);
            $connexion->commit();
            // metre à jour le id_etablissement pour l'inscrit
            (new Atexo_Entreprise_Inscrit())->setIdEtablissementForInscrit($newInscrit, $codeMonEtablissement, $entreprise->getId(), $connexion);
            $newInscrit->setEntrepriseId($entreprise->getId());
            $newInscrit->save($connexion);
            $companyCreated = true;
            // Ajoute l'entreprise a l'index Entreprise de Lucene
            if (Atexo_Module::isEnabled('UtiliserLucene') && $entreprise) {
                $searchLucene = new Atexo_Entreprise_SearchLucene();
                $searchLucene->addEntreprise($entreprise);
            }

            //Sauvegarde de l'historique
            $inscrit1 = $newInscrit->getPrenom().' '.$newInscrit->getNom();
            $mail1 = $newInscrit->getEmail();
            $idInscrit1 = $newInscrit->getId();
            $idEnt = $newInscrit->getEntrepriseId();
            $this->saveHistorique($newInscrit, $inscrit1, $mail1, $idInscrit1, $idEnt);

            self::EnvoiMailConfirmationInscrit($newInscrit, $entreprise, $companyCreated);
            if ($entreprise) {
                return $newInscrit;
            }
        } catch (Exception $e) {
            $connexion->rollback();
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur survenue lors de l'enregistrement de l'entreprise : ".$e->getMessage().$e->getTraceAsString());
        }

        return false;
    }

    /**
     * Permet de recuperer l'entreprise par le siren/siret
     * Stocke le mode de saisie dans la session de la page
     * Valeurs possibles du mode de saisie :
     *     mode1 => Mode degradee sans message d'avertissement
     *     mode2 => Mode degradee avec message d'avertissement
     *     mode3 => Mode automatique utilisant le web service SGMAP.
     *
     * @return Atexo_Entreprise_EntrepriseVo : objet entreprise
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveCompany()
    {
        $etablissementSiege = null;
        $company = $this->recupererEntrepriseByIdentifiant();
        $typeEntreprise = ($this->isEntrepriseLocale()) ? '1' : '2';
        $mode = null;

        /** @var EntrepriseAPIService $entrepriseAPIService */
        $entrepriseAPIService = Atexo_Util::getSfService(EntrepriseAPIService::class);
        /** @var AtexoEtablissement $atexoEtablissement */
        $atexoEtablissement = Atexo_Util::getSfService(AtexoEtablissement::class);
        /** @var AtexoEntreprise $atexoEntrepriseService */
        $atexoEntrepriseService = Atexo_Util::getSfService(AtexoEntreprise::class);

        if ($company instanceof Entreprise) {
            $company = (new Atexo_Entreprise())->getEntrepriseVoFromEntreprise($company);
            $listeEtablissements = (new Atexo_Entreprise_Etablissement())->retreiveEtablissementsByIdEntreprise($company->getId());
            $etablissementsVo = [];
            if (is_array($listeEtablissements) && count($listeEtablissements)) {
                foreach ($listeEtablissements as $oneEtab) {
                    if ($oneEtab instanceof CommonTEtablissement) {
                        $etabVo = $oneEtab->getEtablissementVo();
                        $etablissementsVo[$etabVo->getCodeEtablissement()] = $etabVo;
                    }
                }
            }
            $codeMonEtablissement = $_GET['siret'];
            if (!isset($etablissementsVo[$codeMonEtablissement])) {
                $siret = $company->getSiren().$codeMonEtablissement;
                if (Atexo_Util::isSiretValide($siret)) {
                    $establishmentFromApi = $entrepriseAPIService->callEstablishment(
                        $siret
                    );
                    if (!empty($establishmentFromApi)) {
                        /** @var AtexoEtablissement $atexoEtablissementService */
                        $atexoEtablissementService = Atexo_Util::getSfService(AtexoEtablissement::class);

                        $establishment = $atexoEtablissementService->loadFromApiGouvEntreprise(
                            $establishmentFromApi['data']
                        );
                        $establishment->setIdEntreprise($company->getId());
                        $establishment = $atexoEtablissementService->createOrUpdateEntity($establishment, 'create');
                        $etablissementsVo[$establishment->getCodeEtablissement()] = (new Atexo_Entreprise_Etablissement())->getEtablissementVoFromEntreprise($company, $siret);
                    }
                }
            }
            $company->setListeEtablissements($etablissementsVo);
            if ($this->isEntrepriseLocale()) {
                //Recuperation du mode de saisie
                if (Atexo_Module::isEnabled('SynchronisationSgmap')) {
                    if (Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_SGMAP_LORS_DE_CREATION_ENTREPRISE')) {
                        $entrepriseSF = $atexoEntrepriseService->getEntrepriseBySiren($company->getSiren());
                        $lastUpdate = new DateTime($entrepriseSF->getUpdateDateApiGouv());
                        $oneMonthAgo = new DateTime('-1 month');
                        if ($lastUpdate < $oneMonthAgo) {
                            $entrepriseFromAPI = $entrepriseAPIService->callEntreprise($this->getSiren());
                            $entrepriseUpdated = $atexoEntrepriseService->loadFromApiGouvEntreprise($entrepriseFromAPI['data'], $entrepriseSF);
                            $atexoEntrepriseService->createOrUpdateEntity($entrepriseUpdated, 'update');
                        }
                    }
                    $mode = 'mode3'; //Mode automatique
                    // Si la synchronisation lord de l'authentification est active et l'entreprise est française et le module de synchronisation avec Api Gouv Entreprise est activé on fait la
                    // synchronisation de l'etablissement et l'entreprise ratachés à l'inscrit
                }
            }
        } elseif ($this->isEntrepriseLocale()) {
            // verification du siren
            if (!(Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) && !Atexo_Util::isSirenValide($this->getSiren())) {
                $mode = 'mode4';
            } else {
                $entrepriseFromAPI = $entrepriseAPIService->callEntreprise($this->getSiren());
                if (!empty($entrepriseFromAPI)) {
                    $entreprise = $atexoEntrepriseService->loadFromApiGouvEntreprise($entrepriseFromAPI['data']);
                    $entreprise = $atexoEntrepriseService->createOrUpdateEntity($entreprise, 'create');
                    $establishment = $atexoEtablissement->loadFromApiGouvEntreprise($entrepriseFromAPI['data']);
                    $establishment->setIdEntreprise($entreprise->getId());
                    $atexoEtablissement->createOrUpdateEntity($establishment, 'create');
                } else {
                    $mode = 'mode2';
                }
                $company = Atexo_Entreprise::retrieveCompanyBySiren($this->getSiren());

                if (!empty($company)) {
                    $company = (new Atexo_Entreprise())->getEntrepriseVoFromEntreprise($company);
                }
                if ($company instanceof Atexo_Entreprise_EntrepriseVo) {
                    $company->setEtablissementSiege((new Atexo_Entreprise_Etablissement())->getEtablissementVoFromEntreprise($company));
                    $this->modifierEntrepriseAvecEtablissementSiege($company);

                    //Ajout de l'etablissement siege a la liste des etablissements
                    $codeEtab = substr($company->getSiretSiegeSocial(), -5);
                    $listeEtablissement = $company->getEtablissements();
                    if ($listeEtablissement[$codeEtab] instanceof Atexo_Entreprise_EtablissementVo) {
                        $etablissementSiege = $listeEtablissement[$codeEtab];
                    } elseif ($company->getEtablissementSiege() instanceof Atexo_Entreprise_EtablissementVo) {
                        $etablissementSiege = $company->getEtablissementSiege();
                    }
                    $listeEtablissement[$codeEtab] = $etablissementSiege;
                    $this->setEtablissementSiege($etablissementSiege);

                    //recuperation de l'etablissement dont le siret est passé en param si ce n'est pas le siege (on l'a deja)
                    if (isset($_GET['siren']) && isset($_GET['siret']) && $_GET['siret'] != $codeEtab) {
                        $siret = Atexo_Util::atexoHtmlEntities($_GET['siren']).Atexo_Util::atexoHtmlEntities($_GET['siret']);
                        $establishmentFromApi = $entrepriseAPIService->callEstablishment(
                            $siret
                        );
                        if (!empty($establishmentFromApi)) {
                            /** @var AtexoEtablissement $atexoEtablissementService */
                            $atexoEtablissementService = Atexo_Util::getSfService(AtexoEtablissement::class);

                            $establishment = $atexoEtablissementService->loadFromApiGouvEntreprise(
                                $establishmentFromApi['data']
                            );
                            $establishment->setIdEntreprise($entreprise->getId());
                            $establishment = $atexoEtablissementService->createOrUpdateEntity($establishment, 'create');
                            $listeEtablissement[$establishment->getCodeEtablissement()] = (new Atexo_Entreprise_Etablissement())->getEtablissementVoFromEntreprise($company, $siret);
                        }
                    }
                    $company->setListeEtablissements($listeEtablissement);
                }
            }
        }

        if (
            isset($_GET['siren'])
            && isset($_GET['siret'])
            && ($siren = $_GET['siren'])
            && ($nic = $_GET['siret'])
        ) {
            // Synchronisation WS Exercice pour alimenter Sourcing
            /** @var WebServicesSynchro $webServicesSynchro */
            $webServicesSynchro = Atexo_Util::getSfService(WebServicesSynchro::class);
            $webServicesSynchro->synchroApiGatewayExercices($siren, $nic);
        }

        $this->setModeSaisie($mode);

        return $company;
    }

    /**
     * Action du bouton "Annuler", permet de faire une redirection.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function actionBouttonAnnuler($sender, $param)
    {
        if ('creation' == $_GET['action']) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
        } elseif (('ChangingUser' == $_GET['action']) || ('ChangingCompany' == $_GET['action'])) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie');
        }
    }

    /**
     * Permet de sauvegarder l'historique d'un nouvel utilisateur inscrit.
     *
     * @param CommonInscrit $inscrit:  objet inscrit
     * @param CommonInscrit $inscrit1: objet inscrit
     *                                 $param string $mail1 : Email de l'inscrit
     *                                 $param string $idInscrit1 : identifiant de l'inscrit
     * @param string        $idEnt     : identifiant de l'entreprise
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistorique($inscrit, $inscrit1 = null, $mail1 = null, $idInscrit1 = null, $idEnt = null)
    {
        if ($inscrit instanceof CommonInscrit) {
            if ($inscrit->getProfil() == Atexo_Config::getParameter('PROFIL_INSCRIT_ATES')) {
                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_ADMIN');
            } else {
                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_UES');
            }
            $inscrit2 = $inscrit->getPrenom().' '.$inscrit->getNom();
            $email2 = $inscrit->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique($inscrit2, $email2, $action, $inscrit1, $mail1, $idInscrit1, $idEnt);
        }
    }

    /**
     * Permet d'envoyer l'email de confirmation pour les modifications et suppression de compte inscrit entreprise.
     *
     * @param CommonInscrit $inscrit: objet inscrit
     * @param $entreprise : objet entreprise
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function EnvoiMailConfirmationInscrit($inscrit, $entreprise, $creation)
    {
        if ($creation && Atexo_Module::isEnabled('MailActivationCompteInscritEntreprise')) {
            $msg = Atexo_Message::getMessageConfirmationInscritWithCompanyInfo($inscrit, $entreprise);
        } else {
            $msg = Atexo_Message::getMessageConfirmationInscrit($inscrit, $entreprise);
        }

        $objet = Prado::Localize('DEFINE_TITRE_MAIL_CREATION_COMPTE_UTILISATEUR');
        $msg = '<p>'.str_replace("\n", '</p><p>', $msg).'</p>';
        (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise($inscrit, $objet, $msg);
        //Envoi mail de notification a l'administrateur
        $allAdmin = (new Atexo_Entreprise_Inscrit())->retrieveAllAdmins($inscrit->getEntrepriseId());
        if (is_array($allAdmin) && count($allAdmin)) {
            $profilInscrit = (new Atexo_Entreprise_Inscrit())->correspondanceIdProfilLibelle($inscrit->getProfil());
            $msgCorpsPersonaliseAdmin = str_replace('[__NOM_PRENOM_UTILISATEUR__]', $inscrit->getPrenom().' '.$inscrit->getNom(), Prado::Localize('DEFINE_CORPS_MAIL_CREATION_COMPTE_UTILISATEUR_TO_ADMIN'));
            $msgCorpsPersonaliseAdmin = str_replace('[__PROFIL__]', $profilInscrit, $msgCorpsPersonaliseAdmin);
            $msgCorpsPersonaliseAdmin = str_replace('[__PF__]', Atexo_Config::getParameter('PF_SHORT_NAME'), $msgCorpsPersonaliseAdmin);
            $msgCorpsPersonaliseAdmin = str_replace('[__NOM_SOCIETE__]', $entreprise->getNom(), $msgCorpsPersonaliseAdmin);
            foreach ($allAdmin as $admin) {
                if ($admin->getId() != $inscrit->getId()) {
                    (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise($admin, $objet, $msgCorpsPersonaliseAdmin);
                }
            }
        }
    }

    /**
     * Permet de recuperer le siren a partir de l'url.
     *
     * @return string siren
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getSiren()
    {
        $siren = null;
        if (isset($_GET['siren'])) {
            $siren = Atexo_Util::atexoHtmlEntities($_GET['siren']);
        } elseif (!isset($_GET['siren']) && !isset($_GET['siret']) && !isset($_GET['idNational'])
            && $this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo && $this->getEntreprise()->getSiren()) {
            $siren = $this->getEntreprise()->getSiren();
        }

        return $siren;
    }

    /**
     * Permet de recuperer l'identifiant unique a partir de l'url
     *  identifiant unique => siren pour une entreprise locale
     *  identifiant unique => identifiant nationale pour une entreprise etrangere.
     *
     * @return string identifiant unique
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIdentifiantUnique()
    {
        $identifiantUnique = null;
        if (isset($_GET['idNational'])) {
            $identifiantUnique = Atexo_Util::atexoHtmlEntities($_GET['idNational']);
        } elseif (isset($_GET['siren'])) {
            $identifiantUnique = $this->getSiren();
        }

        return $identifiantUnique;
    }

    /**
     * Stocke le mode de saisie dans le viewState de la page
     * Valeurs possibles du mode de saisie :
     *     mode1 => Mode degradee sans message d'avertissement
     *     mode2 => Mode degradee avec message d'avertissement
     *     mode3 => Mode automatique utilisant le web service SGMAP.
     *
     * @param string $value la valeur a mettre
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setModeSaisie($value)
    {
        $this->setViewState('modeSaisie', $value);
    }

    /**
     * Recupere la valeur du mode de saisie stockée dans le viewState
     * Valeurs possibles du mode de saisie :
     *     mode1 => Mode degradee sans message d'avertissement
     *     mode2 => Mode degradee avec message d'avertissement
     *     mode3 => Mode automatique utilisant le web service SGMAP.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getModeSaisie()
    {
        return $this->getViewState('modeSaisie');
    }

    /**
     * Stocke l'etablissement siege dans le viewState de la page.
     *
     * @param Atexo_Entreprise_EtablissementVo $value la valeur a mettre
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setEtablissementSiege(Atexo_Entreprise_EtablissementVo $value)
    {
        $this->setViewState('etablissementSiege', $value);
    }

    /**
     * Recupere l'etablissement siege dans le viewState de la page.
     *
     * @return Atexo_Entreprise_EtablissementVo
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEtablissementSiege()
    {
        return $this->getViewState('etablissementSiege');
    }

    /**
     * Permet de recuperer l'entreprise et ses etablissements lies en base de donnees.
     *
     * @return Entreprise: objet entreprise
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function recupererEntrepriseByIdentifiant()
    {
        $company = null;
        if (isset($_GET['siren']) || isset($_GET['siret'])) {
            $company = Atexo_Entreprise::retrieveCompanyBySiren(Atexo_Util::atexoHtmlEntities($_GET['siren']));
        } elseif (isset($_GET['idNational'])) {
            $company = (new Atexo_Entreprise())->retrieveCompanyByIdNational(Atexo_Util::atexoHtmlEntities($_GET['idNational']), Atexo_Util::atexoHtmlEntities($_GET['pays']));
        }

        return $company;
    }

    /**
     * Permet de valider la selection de l'etablissement
     * Verifie si au moins 1 etablissement a ete selectionne
     * Sinon affiche un message d'erreur.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function validerListeEtablissements($sender, $param)
    {
        $siretMonEtablissement = $this->etablissements->getSiretMonEtablissement();
        if (null == $siretMonEtablissement || empty($siretMonEtablissement)) {
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->validateurListeEtablissements->ErrorMessage = Prado::localize('MESSAGE_VOUS_DEVEZ_SELECTIONNER_AU_MOINS_UN_ETABLISSEMENT');

            return;
        }
    }

    /**
     * Permet de modifier l'objet entreprise passé par reference par des informations du siege social.
     *
     * @param Atexo_Entreprise_EntrepriseVo : objet entreprise
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function modifierEntrepriseAvecEtablissementSiege(Atexo_Entreprise_EntrepriseVo &$entreprise)
    {
        $etablissements = $entreprise->getEtablissements();
        $codeEtab = substr($entreprise->getSiretSiegeSocial(), -5);
        $siegeSocial = $etablissements[$codeEtab];
        if ($siegeSocial instanceof Atexo_Entreprise_EtablissementVo) {
            $entreprise->setCodeape($siegeSocial->getCodeape());
            $entreprise->setLibelleApe($siegeSocial->getLibelleApe());
            $pays = $siegeSocial->getPays();
            if (empty($pays)) {
                $pays = Atexo_Util::encodeToHttp(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
            }
            $entreprise->setPaysenregistrement($pays);
            $entreprise->setPaysAdresse($pays);
            $entreprise->setAdresse($siegeSocial->getAdresse());
            $entreprise->setVilleenregistrement($siegeSocial->getVille());
        }
    }

    /**
     * Permet de modifier une entreprise deja existante
     * Enregistre l'entreprise en base de donnees.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function modifierEntreprise()
    {
        $connexion = null;
        $company = $this->getEntreprise();
        $company->setDateModification(date('Y-m-d H:i:s'));
        $this->panelStatutEntreprise->chargertInformation($company);
        //Enregistrement de la liste des etablissements a l'entreprise
        $listeEtablissements = $this->etablissements->getEtablissementsList();
        $company->setListeEtablissements($listeEtablissements);
        //On stocke la valeur du code de l'etablissement car apres la sauvegarde de l'entreprise on perd cette valeur
        $codeMonEtablissement = $this->etablissements->getSiretMonEtablissement();

        $entrepriseModifie = false;
        try {
            //Enregistrement de l'entreprise
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $connexion->beginTransaction();
            $entreprise = (new Atexo_Entreprise())->save($company, $connexion);
            //supression des établissements
            $etablissementsToDelate = $this->etablissements->getListeIdsEtabToDelete();
            (new Atexo_Entreprise_Etablissement())->deleteEtablissements($etablissementsToDelate, $connexion);

            // metre à jour le id_etablissement pour l'inscrit
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
            (new Atexo_Entreprise_Inscrit())->setIdEtablissementForInscrit($inscrit, $codeMonEtablissement, $entreprise->getId(), $connexion);
            $inscrit->save($connexion);

            $connexion->commit();
            $entrepriseModifie = true;
        } catch (Exception $e) {
            $connexion->rollback();
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur survenue lors de l'enregistrement de l'entreprise : ".$e->getMessage());
        }
        if ($entrepriseModifie) {
            $this->panelMessageConfirmation->visible = true;
            $this->messageConfirmation->setMessage(Prado::localize('TEXT_COMPTE_MODIFIER_AVEC_SUCCE')); //enregistrement modifie
            $this->buttonRetour->setVisible(true);
            $this->buttonEnregistrer->setVisible(false);
            $this->buttonAnnuler->setVisible(false);
            $this->panelMonEntreprise->visible = false;
        }
    }

    /**
     * Permet de gerer l'action du bouton retour
     * Fait une redirection.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function actionBouttonRetour($sender, $param)
    {
        $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseAccueilAuthentifie');
    }

    /**
     * Modifier compte Inscrit de l'utilisateur courant.
     *
     * @return -1 si erreur, >=0 si OK
     */
    public function updateUserCompte()
    {
        if ($this->IsValid) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $inscrit = CommonInscritPeer::retrieveByPK(Atexo_CurrentUser::getIdInscrit(), $connexion);
            $this->saveEtablissements();
            if ($inscrit) {
                $inscritUpdated = $this->PanelInscrit->updateCompte($inscrit);
                if ($inscritUpdated) {
                    $this->User->setNom($inscritUpdated->getNom());
                    $this->User->setPrenom($inscritUpdated->getPrenom());
                    $this->getApplication()->getModule('auth')->login($this->User, $inscritUpdated->getMdp());
                    //Sauvegarde de l'historique
                    $this->updateHistorique($inscritUpdated);
                    try {
                        $inscritUpdated->save($connexion);
                    } catch (\Exception $e) {
                        Prado::log('Erreur Modification Compte Inscrit '.$e->__toString().'-', TLogger::FATAL, 'Fatal');

                        return -1;
                    }
                }
            }
            return true;
        }
    }

    public function saveEtablissements()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $etablissementsVo = $this->etablissements->getEtablissementsList();
        (new Atexo_Entreprise_Etablissement())->saveEtablissements($etablissementsVo, Atexo_CurrentUser::getIdEntreprise());
        $etablissementsToDelate = $this->etablissements->getListeIdsEtabToDelete();
        (new Atexo_Entreprise_Etablissement())->deleteEtablissements($etablissementsToDelate, $connexion);
    }

    /**
     * Permet de mettre à jour l'historique.
     *
     * @param CommonInscrit $inscrit : objet inscrit
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function updateHistorique($inscrit)
    {
        if ($inscrit instanceof CommonInscrit) {
            $action = Atexo_Config::getParameter('EVENEMENT_ADMIN_UPDATE');
            $inscrit2 = $inscrit->getPrenom().' '.$inscrit->getNom();
            $email2 = $inscrit->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique($inscrit2, $email2, $action);
        }
    }
}
