<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe EntrepriseVisualiserEntiteAchatsArborescence.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseVisualiserEntiteAchatsArborescence extends MpeTPage
{
    protected $_orgAcr;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->_orgAcr = Atexo_Util::atexoHtmlEntities($_GET['acrOrg']);
        $ArrayOrganismes = (new Atexo_Organismes())->retrieveActiveOrganismes(false, true, true);
        if ($ArrayOrganismes[$this->_orgAcr]) {
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($this->_orgAcr);
            $this->sigleorganisme->Text = $organismeO->getSigle();
            $this->libelleorganisme->Text = $organismeO->getDenominationOrg();
            $this->TreeServices->Text = self::displayEntitiesTree($this->_orgAcr);
            $this->idretour->NavigateUrl = '?page=Entreprise.EntrepriseVisualiserEntitesAchatsRecherche';
        } else {
            exit;
        }
    }

    /**
     * @param $acronymeOrg
     *
     * @return string
     */
    public function displayEntitiesTree($acronymeOrg)
    {
        $serviceChildren = Atexo_EntityPurchase::getLevelOneChilds(0, $acronymeOrg);

        return $this->getHtmlArboServices($serviceChildren, $acronymeOrg, true);
    }

    /**
     * @param array  $services
     * @param string $org
     * @param bool   $firstLevel
     *
     * @return string
     */
    public function getHtmlArboServices($services, $org, $firstLevel = false)
    {
        $html = '';
        if (is_array($services)) {
            $services = Atexo_Util::sortArrayOfObjects($services, 'sigle');
            foreach ($services as $service) {
                if ($service instanceof CommonService) {
                    $hrefService = '';
                    $hrefService .= '<a href="?page=Entreprise.EntrepriseDetailEntiteAchat&idEntite='.$service->getId().
                        '&orgAcr='.$org.'" class="pull-left" title="Cliquez pour accéder au détail">';
                    $hrefService .= '<strong>'.$service->getSigle().'</strong>'.' - '.$service->getLibelle();
                    $hrefService .= '</a>';

                    $serviceChildren = Atexo_EntityPurchase::getLevelOneChilds($service->getId(), $org);
                    if (is_array($serviceChildren) && count($serviceChildren) > 0) {
                        $idCollapse = 'collapseEntitie'.$service->getId();
                        $html .= '<div class="panel panel-default">';
                        $html .= '<div class="panel-heading collapsed" role="tab" id="heading'.
                            $idCollapse.'" data-toggle="collapse" data-target="#'.$idCollapse.
                            '" aria-expanded="false" aria-controls="'.$idCollapse.'">';
                        $html .= '<h4 class="panel-title">';
                        $html .= '<div class="clearfix">';
                        $html .= $hrefService;
                        $html .= '</div>';
                        $html .= '</h4> ';
                        $html .= '</div>';
                        $html .= '<div id="'.$idCollapse.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'.$idCollapse.'">';
                        $html .= '<div class="panel-body">';
                        $html .= $this->getHtmlArboServices($serviceChildren, $org);
                        $html .= '</div>';
                        $html .= '</div>';
                        $html .= '</div>';
                    } elseif ($firstLevel) {
                        $html .= '<div class="panel panel-default">';
                        $html .= '<div class="panel-heading collapsed " >';
                        $html .= '<h4 class="panel-title">';
                        $html .= '<div class="clearfix">';
                        $html .= $hrefService;
                        $html .= '</div>';
                        $html .= '</h4> ';
                        $html .= '</div>';
                        $html .= '</div>';
                    } else {
                        $html .= '<div class="p-b-1">'.str_replace('class="pull-left"', '', $hrefService).'</div>';
                    }
                }
            }
        }

        return $html;
    }
}
