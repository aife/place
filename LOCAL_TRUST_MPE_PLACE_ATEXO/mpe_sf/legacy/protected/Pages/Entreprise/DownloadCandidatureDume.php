<?php

namespace Application\Pages\Entreprise;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use ZipArchive;

/*
 * permet de recuperer le fichier CandidatureDume d'une offre
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 * @version 1.0
 * @since 2017-esr
 * @copyright Atexo 2018
 * @package pages/agent
 */
class DownloadCandidatureDume extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Cette methode permet telecharger la candidature Dume lie a l'offre passee en param(url).
     *
     * @param void
     *
     * @return stream candidatureDume
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function onLoad($param)
    {
        $reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $organisme = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['organisme']));
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setAvecConsClotureesSansPoursuivreAffichage(true);
        $consultationFound = (new Atexo_Consultation())->retrieveConsultationForCompagny($reference, $organisme, true, $critere);
        if ($consultationFound instanceof CommonConsultation) {
            $idOffre = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idOffre']));
            $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, $organisme);
            $candidature = Atexo_Dume_AtexoDume::retrieveCandidatureDume($offre);
            if ($candidature instanceof CommonTCandidature) {
                $tDumeNumeros = Atexo_Dume_AtexoDume::getTDumeNumerosEntreprise($candidature->getIdDumeContexte());
                if (is_array($tDumeNumeros) && !empty($tDumeNumeros)) {
                    if (1 == count($tDumeNumeros)) {
                        if ($tDumeNumeros[0] instanceof CommonTDumeNumero) {
                            $this->downloadFiles($tDumeNumeros[0]->getBlobId(), $tDumeNumeros[0]->getNumeroDumeNational().'.pdf', $consultationFound->getOrganisme());
                        }
                    } else {
                        $directory = Atexo_Config::getParameter('COMMON_TMP').'DUME'.session_name().session_id().time().md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
                        mkdir($directory);
                        $zip = new ZipArchive();
                        $zipFileName = 'Dume_EL_'.$offre->getNumeroReponse().'.zip';
                        $res = $zip->open($directory.$zipFileName, ZipArchive::CREATE);
                        if (true === $res) {
                            $atexoBlob = new Atexo_Blob();
                            foreach ($tDumeNumeros as $dumeNumero) {
                                if ($dumeNumero instanceof CommonTDumeNumero) {
                                    $blobresource = $atexoBlob->acquire_lock_on_blob($dumeNumero->getBlobId(), $organisme);
                                    $zip->addFile($blobresource['blob']['pointer'], $dumeNumero->getNumeroDumeNational().'.pdf');
                                }
                            }
                            $zip->close();
                            static::downloadFileContent($zipFileName, file_get_contents($directory.$zipFileName));
                        }
                    }
                }
            }
        }
        exit;
    }
}
