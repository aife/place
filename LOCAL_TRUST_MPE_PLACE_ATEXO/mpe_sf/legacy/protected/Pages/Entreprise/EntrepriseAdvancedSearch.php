<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Recherches;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Page de recherche avancée.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseAdvancedSearch extends MpeTPage
{
    protected $selectedOrg = false;
    public string $pageTitle = 'TITRE';

    public function onInit($param)
    {
        $this->pageTitle = 'TITRE_RECHERCHE_AVANCEE';
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        Atexo_CurrentUser::writeToSession('userType', 'Entreprise');
        if (!Atexo_CurrentUser::isConnected() && isset($_GET['panierEntreprise'])) {
            $this->response->redirect('?page=Entreprise.EntrepriseHome&goto='.urlencode($_SERVER['REQUEST_URI']));
        }
        $dateDebAction = date('Y-m-d H:i:s');
        $lanceSearch = true;
        if (!$this->IsPostBack) {
            $this->rechercheAvancee->setVisible(true);
            $this->panelResult->setVisible(false);
            if (isset($_GET['ref']) || isset($_GET['refConsultation'])) {
                $serviceSF = Atexo_Util::getSfService('app.mapping.consultation');
                if ($serviceSF) {
                    $id = (isset($_GET['ref'])) ? Atexo_Util::atexoHtmlEntities($_GET['ref']) : Atexo_Util::atexoHtmlEntities($_GET['refConsultation']);
                    $org = $_GET['orgAcronyme'] ?? null;
                    $idConsultation = $serviceSF->getConsultationId($id, $org);
                    $_GET['id'] = $idConsultation;
                    if (-1 == $idConsultation) {
                        $lanceSearch = false;
                        $this->rechercheAvancee->setVisible(false);
                        $this->panelResult->setVisible(false);
                        $this->afficherMonPanier->setVisible(false);
                        $this->msgErreur->setVisible(true);
                        $this->panelErreur->setMessage(Prado::localize('MESSAGE_ERREUR_DEUX_CONSULTATIONS_EN_COLLISION'));
                    }
                }
            }
            if (isset($_GET['selectedorg'])) {
                $this->selectedOrg = Atexo_Util::atexoHtmlEntities($_GET['selectedorg']);
            } elseif (isset($_COOKIE['selectedorg']) && !isset($_GET['orgTest'])) {
                $this->selectedOrg = Atexo_Util::atexoHtmlEntities($_COOKIE['selectedorg']);
            }

            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setEmailInscrit(Atexo_CurrentUser::getEmailInscrit());
            if (isset($_GET['orgTest'])) {
                $criteriaVo->setOrganismeTest(true);
            }
            if ($lanceSearch) {
                if (isset($_GET['keyWord']) && (0 == strcmp($_GET['keyWord'], $_GET['keyWord']))) {
                    if (isset($this->selectedOrg)) {
                        $criteriaVo->setAcronymeOrganisme($this->selectedOrg);
                    }
                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);
                    $criteriaVo->setKeyWordAdvancedSearch(Atexo_Util::atexoHtmlEntities($_GET['keyWord']));
                    if (isset($_GET['categorie'])) {
                        $criteriaVo->setCategorieConsultation($_GET['categorie']);
                    }
                    if (isset($_GET['localisations'])) {
                        $criteriaVo->setLieuxexecution(trim(self::getIdsSelectedGeoN2($_GET['localisations'])));
                    }
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                } elseif (isset($_GET['AllCons'])) {
                    if (isset($this->selectedOrg)) {
                        $criteriaVo->setAcronymeOrganisme($this->selectedOrg);
                    }

                    if (isset($_GET['id'])) {
                        $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    }

                    if (isset($_GET['id']) && isset($_GET['orgAcronyme'])) {
                        $criteriaVo->setAcronymeOrganisme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                        $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    }

                    if (isset($_GET['EnCours'])) {
                        $criteriaVo->setDateFinStart(date('d/m/Y'));
                    }
                    if (isset($_GET['domaineActivite']) && $_GET['domaineActivite']) {
                        $criteriaVo->setDomaineActivite(Atexo_Util::atexoHtmlEntities($_GET['domaineActivite']));
                    }

                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);

                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);

                    //pour lister les consultations annulées
                    if (isset($_GET['consAnnulee']) && (1 == $_GET['consAnnulee'])) {
                        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_DECISION'));
                        $criteriaVo->setConsultationAnnulee(true);
                    }

                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $nombreElement = $this->dataForSearchResult($criteriaVo);
                    if (isset($_GET['id']) && isset($_GET['orgAcronyme'])) {
                        if (empty($nombreElement)) {
                            $this->rechercheAvancee->setVisible(false);
                            $this->panelResult->setVisible(false);
                            $this->afficherMonPanier->setVisible(false);
                            $this->msgErreur->setVisible(true);
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                            $consultation = CommonConsultationPeer::retrieveByPK($_GET['id'], $connexion);
                            $msg = Prado::localize('TEXT_AUCUNE_CONSULTATION');
                            if ($consultation instanceof CommonConsultation) {
                                $msg = str_replace(['{_ref_consultation_}', '{_intitule_consultation_}'], [$consultation->getReferenceUtilisateur(), $consultation->getIntituleTraduit()], Prado::localize('MSG_CONSULTATION_N_EST_PAS_EN_LIGNE'));
                            }
                            $this->panelErreur->setMessage($msg);
                        }
                    }
                } elseif (isset($_GET['AvisAttribution'])) {
                    if (isset($this->selectedOrg)) {
                        $criteriaVo->setAcronymeOrganisme($this->selectedOrg);
                    }
                    if (isset($_GET['id']) && isset($_GET['orgAcronyme'])) {
                        $criteriaVo->setAcronymeOrganisme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                        $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    }
                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'));
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                } elseif (isset($_GET['AvisInformation'])) {
                    if (isset($this->selectedOrg)) {
                        $criteriaVo->setAcronymeOrganisme($this->selectedOrg);
                    }
                    if (isset($_GET['id']) && isset($_GET['orgAcronyme'])) {
                        $criteriaVo->setAcronymeOrganisme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                        $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    }
                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'));
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                } elseif (isset($_GET['AvisExtraitPV'])) {
                    if (isset($this->selectedOrg)) {
                        $criteriaVo->setAcronymeOrganisme($this->selectedOrg);
                    }
                    if (isset($_GET['id']) && isset($_GET['orgAcronyme'])) {
                        $criteriaVo->setAcronymeOrganisme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                        $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    }
                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_EXTRAIT_PV'));
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                } elseif (isset($_GET['AvisRapportAchevement'])) {
                    if (isset($this->selectedOrg)) {
                        $criteriaVo->setAcronymeOrganisme($this->selectedOrg);
                    }
                    if (isset($_GET['id']) && isset($_GET['orgAcronyme'])) {
                        $criteriaVo->setAcronymeOrganisme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                        $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    }
                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_RAPPORT_ACHEVEMENT'));
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                } elseif (isset($_GET['AvisdecisionResiliation'])) {
                    if (isset($this->selectedOrg)) {
                        $criteriaVo->setAcronymeOrganisme($this->selectedOrg);
                    }
                    if (isset($_GET['id']) && isset($_GET['orgAcronyme'])) {
                        $criteriaVo->setAcronymeOrganisme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                        $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    }
                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_DECISION_RESILIATION'));
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                } elseif (isset($_GET['AvisProjetAchat'])) {
                    if (isset($this->selectedOrg)) {
                        $criteriaVo->setAcronymeOrganisme($this->selectedOrg);
                    }
                    if (isset($_GET['id']) && isset($_GET['orgAcronyme'])) {
                        $criteriaVo->setAcronymeOrganisme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                        $criteriaVo->setIdReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    }
                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_PROJET_ACHAT'));
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                } elseif (isset($_GET['id']) && isset($_GET['orgAcronyme'])) {
                    $this->AdvancedSearch->displayInfoRechercheProcedureRestreinte(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                } elseif (isset($_GET['idRecherche']) && Atexo_CurrentUser::getIdInscrit()) {
                    $RechercheObj = (new Atexo_Entreprise_Recherches())->retreiveRecherchesByIdIdCreateur($_GET['idRecherche'], Atexo_CurrentUser::getIdInscrit());
                    if ($RechercheObj) {
                        $criteriaVo = (new Atexo_Entreprise_Recherches())->getCriteriaFromObject($RechercheObj);
                        $criteriaVo->setcalledFromPortail(true);
                        $this->setViewState('CriteriaVo', $criteriaVo);
                        Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                        if (isset($_GET['search'])) {
                            $this->AdvancedSearch->displayCriteria();
                        } else {
                            $this->rechercheAvancee->setVisible(false);
                            $this->panelResult->setVisible(true);
                            $this->dataForSearchResult($criteriaVo);
                        }
                    } else {
                        $this->response->redirect('?page=Entreprise.EntrepriseHome');
                    }
                }
                if (Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']) && !isset($_GET['searchAnnCons'])) {
                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);
                    $criteriaVo->setForPanierEntreprise(true);
                    $criteriaVo->setIdInscrit(Atexo_CurrentUser::getIdInscrit());
                    $criteriaVo->setIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
                    //$criteriaVo->setAcronymeOrganisme($_GET['orgAcronyme']);
                    if (isset($_GET['dateFinEnd'])) {
                        //$criteriaVo->setDateTimeFin(date('Y-m-d H:i:s'));
                        $criteriaVo->setAvecConsClotureesSansPoursuivreAffichage(true);
                    } else {
                        $criteriaVo->setDateFinStart(date('d/m/Y'));
                        //Avec retraits, questions, dépots, messages échangés
                        if (isset($_GET['retraits'])) {
                            $criteriaVo->setAvecRetrait(true);
                        }
                        if (isset($_GET['questions'])) {
                            $criteriaVo->setAvecQuestion(true);
                        }
                        if (isset($_GET['depots'])) {
                            $criteriaVo->setAvecDepot(true);
                        }
                        if (isset($_GET['echanges'])) {
                            $criteriaVo->setAvecEchange(true);
                        }
                        //Sans retraits, questions, dépots, messages échangés
                        if (isset($_GET['sansRetraits'])) {
                            $criteriaVo->setSansRetrait(true);
                        }
                        if (isset($_GET['sansQuestions'])) {
                            $criteriaVo->setSansQuestion(true);
                        }
                        if (isset($_GET['sansDepots'])) {
                            $criteriaVo->setSansDepot(true);
                        }
                        if (isset($_GET['sansEchanges'])) {
                            $criteriaVo->setSansEchange(true);
                        }

                        if (isset($_GET['consClotures'])) {
                            $criteriaVo->setAvecConsCloturees(true);
                        }
                    }
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                }
            }
            if (isset($_GET['WithCritere'])) {
                $this->rechercheAvancee->setVisible(false);
                $this->panelResult->setVisible(true);
                $criteriaVo = Atexo_CurrentUser::readFromSession('criteriaCons');
                if ($criteriaVo instanceof Atexo_Consultation_CriteriaVo) {
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    //aller à la page X après recherche
                    if ($criteriaVo->getLimit() > 0 && $criteriaVo->getOffset() > 0) {
                        $size = $criteriaVo->getLimit();
                        $indexPage = $criteriaVo->getOffset() / $criteriaVo->getLimit() + 1;
                        $this->dataForSearchResult($criteriaVo);
                        $this->resultSearch->updatePagination($size, $indexPage);
                        $this->resultSearch->populateData($criteriaVo);
                    } else {
                        $this->dataForSearchResult($criteriaVo);
                    }
                } else {
                    $criteriaVo = new Atexo_Consultation_CriteriaVo();
                    $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
                    $this->rechercheAvancee->setVisible(false);
                    $this->panelResult->setVisible(true);
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    $criteriaVo->setcalledFromPortail(true);
                    $this->setViewState('CriteriaVo', $criteriaVo);
                    Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
                    $this->dataForSearchResult($criteriaVo);
                }
            }
        }

        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    /**
     * Fonction qui appel la fonction du template pour rechercher puis alimenter le tableau de bord.
     *
     * @param : $criteriaVo : objet contenant les critères de recherches
     */
    public function dataForSearchResult(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        return $this->resultSearch->fillRepeaterWithDataForSearchResult($criteriaVo);
    }

    public function Trier($sender, $param)
    {
        $this->resultSearch->Trier($param->CommandName);
    }

    public function getUrlConsultationExterne($url)
    {
        return $url;
    }

    public function returnPictoReponseConsultation($consultation)
    {
        //$consultation = new CommonConsultation();
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                if ('2' == $consultation->getAutoriserReponseElectronique()) {
                    return 'reponse-elec-inconnu.gif';
                }
            }
            if (!$consultation->getAutoriserReponseElectronique()) {
                return 'reponse-elec-non.gif';
            }
            if (!$consultation->getAutoriserReponseElectronique()) {
                return 'reponse-elec-non.gif';
            } elseif (($consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-oblig-avec-signature.gif';
            } elseif (($consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-avec-signature.gif';
            } elseif ((!$consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-oblig.gif';
            } elseif ((!$consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec.gif';
            }
        } else {
            return '';
        }
    }

    public function returnTitlePictoReponseConsultation($consultation)
    {
        //$consultation = new CommonConsultation();
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                if ('2' == $consultation->getAutoriserReponseElectronique()) {
                    return Prado::localize('TEXT_STATUT_INCONNUE_REPONSE_CONSULTATION');
                }
            }
            if (!$consultation->getAutoriserReponseElectronique()) {
                return Prado::localize('TEXT_PAS_REPONSE_POUR_CONSULTATION');
            } elseif (($consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE');
            } elseif (($consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE');
            } elseif ((!$consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE');
            } elseif ((!$consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE');
            }
        } else {
            return '';
        }
    }

    public function refreshComposants($sender, $param)
    {
        $this->AdvancedSearch->domaineActivite->panelDomaineActivite->render($param->NewWriter);
    }

    public function afficherMonPanier()
    {
        if (Atexo_Module::isEnabled('PanierEntreprise')) {
            $this->response->redirect('?page=Entreprise.EntrepriseAdvancedSearch&panierEntreprise');
        }
    }

    /**
     * @return mixed
     */
    public function isSiretAlert()
    {
        $isSiretAlert = (bool) Atexo_CurrentUser::readFromSession('siretAlert');
        Atexo_CurrentUser::writeToSession('siretAlert', null);

        return $isSiretAlert;
    }
    public function getIdsSelectedGeoN2(string $localisations): string
    {
        $localisations = explode(',', $localisations);
        $arrayIdGeolocalisation = [];
        if (is_array($localisations)) {
            foreach ($localisations as $denominationN2) {
                $denominationN2 = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeoN2ByDenomination2($denominationN2);
                if ($denominationN2 instanceof CommonGeolocalisationN2) {
                    $arrayIdGeolocalisation[] = $denominationN2->getId();
                }
            }
        }
        if (count($arrayIdGeolocalisation)) {
            return implode(',', $arrayIdGeolocalisation);
        } else {
            return '';
        }
    }
}
