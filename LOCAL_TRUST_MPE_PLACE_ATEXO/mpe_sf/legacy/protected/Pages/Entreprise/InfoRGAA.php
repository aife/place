<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;

class InfoRGAA extends MpeTPage
{
    public function onLoad($param)
    {
        $url = Atexo_Config::getParameter('URL_LT_MPE_SF').'/entreprise/footer/accessibilite';
        $this->response->redirect($url);
    }
}
