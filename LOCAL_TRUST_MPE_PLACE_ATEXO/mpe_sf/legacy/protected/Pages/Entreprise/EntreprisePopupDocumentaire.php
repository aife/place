<?php

namespace Application\Pages\Entreprise;

use App\Utils\Encryption;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonDocumentsAttachesPeer;
use Application\Propel\Mpe\CommonJustificatifs;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Documents;
use Prado\Prado;

/**
 * popup de d'ajout et modification des documents entreprise.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntreprisePopupDocumentaire extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        self::customizeForm();
        $encrypt = Atexo_Util::getSfService(Encryption::class);
        $idDocument =  $encrypt->decryptId($_GET['id']);
        if ($idDocument) {
            if (in_array($idDocument, (new Atexo_Entreprise_Documents())->retirieveArrayIdsDocs(Atexo_CurrentUser::getIdEntreprise()))) {
                $this->actionBoutton->value = 1;
                $this->docType->setEnabled(false);
                if (!$this->IsPostBack) {
                    $this->displayDoc(Atexo_Util::atexoHtmlEntities($idDocument));
                }
            } else {
                $this->panelDoc->setVisible(false);
                $this->labelClose->Text = Prado::localize('TEXT_ERREUR');
            }
            $this->contenuDocAttacheValidator->setEnabled(false);
        }
        if (!$idDocument) {
            $this->contenuDocAttacheValidator->setEnabled(true);
        }
        if (!$this->IsPostBack) {
            $this->displayDocumentsType();
        }
        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    /**
     * affiche les types des documents de la table Justificatif.
     */
    public function displayDocumentsType()
    {
        $docsType = (new Atexo_Entreprise_Documents())->retieveDocumentType();
        $data = [];
        if ($docsType) {
            $langue = Atexo_CurrentUser::readFromSession('lang');
            $nomDocumentTraduit = 'getNomDocument'.ucwords($langue);
            foreach ($docsType as $oneDoc) {
                if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                    || !$oneDoc->$nomDocumentTraduit()) {
                    $data[$oneDoc->getIdDocument()] = $oneDoc->getNomDocument();
                } else {
                    $data[$oneDoc->getIdDocument()] = $oneDoc->$nomDocumentTraduit();
                }
            }
        }
        $this->docType->DataSource = $data;
        $this->docType->DataBind();
    }

    /**
     * en cas de modification afficher le type de document selectionné.
     */
    public function displayDoc($idDocument)
    {


        $document = (new Atexo_Entreprise_Documents())->retrieveDocumentById($idDocument, Atexo_CurrentUser::getIdEntreprise());
        if ($document) {
            $this->docType->SelectedValue = $document[0]->getCommonDocumentsAttaches()->getIdDocument();
            $this->datemiseFinValidite->text = Atexo_Util::iso2frnDateTime($document[0]->getDateFinValidite());
            if ($document[0]->getVisibleParAgents()) {
                $this->fichierAccessibleOui->checked = true;
            } else {
                $this->fichierAccessibleNon->checked = true;
            }
        }
    }

    /**
     *ajouter un document dans la base.
     */
    public function addDocument($sender, $param)
    {
        if ($this->IsValid) {
            if ($this->docFile->HasFile) {
                $docFile = new CommonJustificatifs();
                $infile = Atexo_Config::getParameter('COMMON_TMP').'docAttaches'.session_id().time();
                if (move_uploaded_file($this->docFile->LocalName, $infile)) {
                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $atexoBlob = new Atexo_Blob();
                    $docIdBlob = $atexoBlob->insert_blob($this->docFile->FileName, $infile, Atexo_Config::getParameter('COMMON_BLOB'));
                    $docFile->setJustificatif($docIdBlob);
                    $docFile->setIdDocument($this->docType->SelectedValue);
                    $docFile->setIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
                    $docFile->setIntituleJustificatif($this->docFile->FileName);
                    $documentAttache = CommonDocumentsAttachesPeer::retrieveByPK($this->docType->SelectedItem->getValue(), $connexionCom);
                    if ($documentAttache) {
                        $languesActive = Atexo_Languages::retrieveActiveLangages();
                        foreach ($languesActive as $langue) {
                            $setNomTraduit = 'setNom'.ucwords($langue->getLangue());
                            $getNomDocumentTraduit = 'getNomDocument'.ucwords($langue->getLangue());

                            $docFile->$setNomTraduit($documentAttache->$getNomDocumentTraduit());
                        }
                        $docFile->setNom($documentAttache->getNomDocument());
                    }
                    $docFile->setStatut(0);
                    $docFile->setTaille($this->docFile->FileSize);
                    if (Atexo_Util::isDate($this->datemiseFinValidite->text)) {
                        $docFile->setDateFinValidite(Atexo_Util::frnDate2iso($this->datemiseFinValidite->text));
                    }
                    if (true == $this->fichierAccessibleOui->Checked) {
                        $docFile->setVisibleParAgents(1);
                    } elseif (true == $this->fichierAccessibleNon->Checked) {
                        $docFile->setVisibleParAgents(0);
                    }
                    $docFile->save($connexionCom);
                } else {
                    echo 'erreur';
                    exit;
                }
            }
            //$this->labelClose->Text = "<script>window.close();</script>";
            $this->labelClose->Text = "<script>window.opener.document.getElementById('ctl0_CONTENU_PAGE_buttonDisplayDoc').click();window.close();</script>";
        }
    }

    /**
     * modification d'un document.
     */
    public function updateDocument($idDocument)
    {
        $encrypt = Atexo_Util::getSfService(Encryption::class);
        $idDocument =  $encrypt->decryptId($idDocument);
        if ($this->IsValid) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $document = (new Atexo_Entreprise_Documents())->retrieveDocumentByPk($idDocument);
            if ($document instanceof CommonJustificatifs) {
                if ($this->docFile->HasFile) {
                    $infile = Atexo_Config::getParameter('COMMON_TMP').'docAttaches'.session_id().time();
                    if (move_uploaded_file($this->docFile->LocalName, $infile)) {
                        $atexoBlob = new Atexo_Blob();
                        $docIdBlob = $atexoBlob->insert_blob($this->docFile->FileName, $infile, Atexo_Config::getParameter('COMMON_BLOB'));
                        $document->setJustificatif($docIdBlob);
                        $document->setIntituleJustificatif($this->docFile->FileName);
                        $document->setTaille($this->docFile->FileSize);
                    }
                }
                if (Atexo_Util::isDate($this->datemiseFinValidite->text)) {
                    $document->setDateFinValidite(Atexo_Util::frnDate2iso($this->datemiseFinValidite->text));
                }
                if (true == $this->fichierAccessibleOui->Checked) {
                    $document->setVisibleParAgents(1);
                } elseif (true == $this->fichierAccessibleNon->Checked) {
                    $document->setVisibleParAgents(0);
                }
                $document->save($connexionCom);
            }
            $this->labelClose->Text = "<script>window.opener.document.getElementById('ctl0_CONTENU_PAGE_buttonDisplayDoc').click();window.close();</script>";
        }
    }

    /**
     * appel au fonctions de validation.
     */
    public function verifyDocAttache($sender, $param)
    {
        self::verifyDocTailleCreation($sender, $param);
        if (0 == $this->actionBoutton->value) {
            self::verifyTypeDocCreation($sender, $param);
        }
    }

    /**
     * Verifier la taille maximum du document à ajouter
     * ( 2Mo maximum).
     */
    public function verifyDocTailleCreation($sender, $param)
    {
        $sizedocAttache = $this->docFile->FileSize;
        if (0 == $sizedocAttache) {
            $param->IsValid = false;
            $this->docAttacheValidator->ErrorMessage = '<br/>'.Prado::localize('TEXT_FICHIER_VIDE').'<br/>';
        } elseif ($sizedocAttache >= 2 * 1024 * 1024) {
            $param->IsValid = false;
            $this->docAttacheValidator->ErrorMessage = '<br/>'.Prado::localize('TEXT_TAILLE_DOC', ['docAttache' => $this->docFile->FileName]).'<br/>';
        }
    }

    /**
     * Verifier que le type du document n'existe pas déjà.
     */
    public function verifyTypeDocCreation($sender, $param)
    {
        $allDocType = (new Atexo_Entreprise_Documents())->retrieveDocuments(Atexo_CurrentUser::getIdEntreprise());
        $intitule = $this->docType->getSelectedItem()->getText();
        if (!$allDocType) {
            return;
        }
        foreach ($allDocType as $oneDoc) {
            if ($oneDoc->getCommonDocumentsAttaches()->getNomDocument() === $intitule) {
                $param->IsValid = false;
                $this->docAttacheValidator->ErrorMessage = '<br/>'.Prado::localize('TEXT_TYPE_DOC_EXISTE_DEJA', ['docAttache' => $this->docFile->FileName]).'<br/>';

                return;
            }
        }
    }

    /**
     * action sur le boutton ,ajout  ou modification d'un document.
     */
    public function onValider($sender, $param)
    {
        //Début scan antivirus
        if ($this->docFile->HasFile) {
            $msg = Atexo_ScanAntivirus::startScan($this->docFile->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.' "'.$this->docFile->FileName.'"');

                return;
            }
        }
        //Fin scan antivirus

        if (0 == $this->actionBoutton->value) {
            self::addDocument($sender, $param);
        } else {
            self::updateDocument(Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
    }

    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CfeDateFinValiditeObligatoire')) {
            $this->validateurDateFinValiditeObligatoire->setEnabled(true);
            $this->labelEtoileDateFinValidite->setDisplay('Dynamic');
        } else {
            $this->validateurDateFinValiditeObligatoire->setEnabled(false);
            $this->labelEtoileDateFinValidite->setDisplay('None');
        }

        if (!Atexo_Module::isEnabled('PradoValidateurFormatDate')) {
            $this->controlDatemiseFinValidite->Enabled = false;
        }
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }
}
