<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Permet de télécharger le DCE.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseFormulaireDemande extends MpeTPage
{
    public $_reference;
    public $_parentPage;
    public $_organisme;
    public $lieuExecution;
    private bool $_postBack = false;

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function onLoad($param)
    {
        $this->pays->setPostBack($this->_postBack);
        $this->customizeForm();
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);

        if (!$this->_postBack) {
            if ('EntrepriseDemandeTelechargementDce' == $this->getParentPage()) {
                $this->getConditionGeneralesTelechargementDce();
            } elseif ('EntrepriseFormulairePoserQuestion' == $this->getParentPage()) {
                $this->getConditionGeneralesPoserQuestion();
            }
        }
    }

    /**
     * import les données entreprises.
     */
    public function getCoordonnees()
    {
        $etablissementVo = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_CurrentUser::getIdEntreprise());
        $inscrit = CommonInscritPeer::retrieveByPk(Atexo_CurrentUser::getIdInscrit(), $connexionCom);
        $etablissementQuery = new CommonTEtablissementQuery();
        $etablissement = $etablissementQuery->getEtabissementbyIdEseAndIdEtab(Atexo_CurrentUser::getIdEtablissement(), Atexo_CurrentUser::getIdEntreprise());
        if ($etablissement instanceof CommonTEtablissement) {
            $etablissementVo = $etablissement->getEtablissementVo();
        }
        if ($inscrit) {
            $this->nom->Text = $inscrit->getNom();
            $this->prenom->Text = $inscrit->getPrenom();

            if ($entreprise->getNom()) {
                $this->raisonSocial->Text = $entreprise->getNom();
            }
            if ($entreprise->getSiren()) {
                $this->france->checked = true;
                if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                    $arraySiren = explode(Atexo_Config::getParameter('SEPARATEUR_VILLE_RC'), $entreprise->getSiren());
                    $this->RcVille->SelectedValue = $arraySiren['0'];
                    $this->RcNumero->Text = $arraySiren['1'];
                } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                    $this->identifiantUnique->Text = $entreprise->getSiren();
                } else {
                    $this->siren->Text = $entreprise->getSiren();
                }
            } else {
                $this->idNational->Text = $entreprise->getSirenEtranger();
                $this->etranger->checked = true;

                $this->pays->setPostBack(true);
                $this->pays->fillDataSource();
                $this->pays->SelectedValue = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdCountry($entreprise->getPaysAdresse());
                $this->script->Text = "<script>document.getElementById('nonEtablieFrance').style.display='';</script>";
            }
            if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
                $this->siret->Text = $etablissementVo->getCodeEtablissement();
                $this->address->Text = $etablissementVo->getAdresse();
                $this->address2->Text = $etablissementVo->getAdresse2();
                $this->cp->Text = $etablissementVo->getCodePostal();
                $this->ville->Text = $etablissementVo->getVille();
            }
            $this->tel->Text = $inscrit->getTelephone();
            $this->fax->Text = $inscrit->getFax();
            $this->email->Text = $inscrit->getEmail();
        }
    }

    /**
     * mettre l'url des lots.
     */
    public function setUrlLots($consultation)
    {
        $arrayLots = $consultation->getAllLots();

        if (is_array($arrayLots) && count($arrayLots) > 2) {
            $url = "javascript:popUp('".Atexo_Config::getParameter('URL_MPE_ORGANISME').'/'.$consultation->getOrganisme().'/detail_lots.php?org='.$consultation->getOrganisme()
             .'&cons_ref='.$consultation->getId()."','yes')";
            $this->linkDetailLots->NavigateUrl = $url;
        } else {
            $this->linkDetailLots->Text = '-';
        }
    }

    public function truncate($texte, $lieu)
    {
        $truncatedText = null;
        if ($lieu) {
            $maximumNumberOfCaracterToReturn = 60;
            $arrayText = explode(',', $texte);
        } else {
            $maximumNumberOfCaracterToReturn = 65;
            $arrayText = explode(' ', $texte);
        }
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            if ($lieu) {
                $truncatedText .= ','.$arrayText[$indexArrayText];
            } else {
                $truncatedText .= ' '.$arrayText[$indexArrayText];
            }
        }

        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 60) ? '' : 'display:none';
    }

    public function getLieuxExecution()
    {
        return $this->lieuExecution;
    }

    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setParentPage($value)
    {
        $this->_parentPage = $value;
    }

    /**
     * Permet de determiner les conditions d'acces a la fonctionnalite de telechargement de DCE.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getConditionGeneralesTelechargementDce()
    {
        if (Atexo_Module::isEnabled('TelechargerDceSansIdentification') || Atexo_Module::isEnabled('TelechargerDceAvecAuthentification')) {
            $this->descriptionConditions->Text = prado::localize('DEFINE_CONDITIONS_FORMULAIRE_DEMANDE_PART_1');
            $this->descriptionAnonyme->setDisplay('Dynamic');
            if ($this->User->IsGuest && Atexo_Module::isEnabled('TelechargerDceAvecAuthentification')) {
                $this->textInscriptionAuthentification->Text = prado::localize('MESSAGE_JE_SOUHAITE_IDENTIFIER_INSCRIRE');
                $this->blocInscriptionAuthentification->setDisplay('Dynamic');
                $this->inscriptionAuthentification->checked = true;
            } else {
                $this->blocInscriptionAuthentification->setDisplay('None');
                $this->choixTelechargement->checked = true;
            }
            $this->script->Text .= '<script>gererBlocCoordonnees();</script>';
            $this->completerFormulaireDemande->Text = prado::localize('COMPLETER_FORMULAIRE_DEMANDE');
        } else {
            $this->descriptionConditions->Text = prado::localize('DEFINE_CONDITIONS_FORMULAIRE_DEMANDE');
            $this->descriptionAnonyme->setDisplay('None');
            $this->choixTelechargement->checked = true;
        }
        $this->titreFormulaire->Text = prado::localize('DEFINE_FORMULAIRE_DEMANDE');
    }

    /**
     * Permet de determiner les conditions d'acces a la fonctionnalite de Poser une Question.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getConditionGeneralesPoserQuestion()
    {
        if (Atexo_Module::isEnabled('PoserQuestionNecessiteAuthentification') && !Atexo_CurrentUser::getIdInscrit()) {
            $url = '?page=Entreprise.EntrepriseFormulairePoserQuestion&id='.Atexo_Util::atexoHtmlEntities($_GET['id']);
            $url .= '&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
            $this->response->redirect('index.php?page=Entreprise.EntrepriseHome&goto='.urlencode($url));

            return;
        }
        if (!Atexo_CurrentUser::getIdInscrit()) {
            $this->textInscriptionAuthentification->Text = prado::localize('MESSAGE_JE_SOUHAITE_IDENTIFIER_INSCRIRE');
            $this->blocInscriptionAuthentification->setDisplay('Dynamic');
            $this->blocChoixTelechargement->setDisplay('Dynamic');
            $this->inscriptionAuthentification->checked = true;
        } else {
            $this->blocChoixTelechargement->setDisplay('None');
            $this->blocInscriptionAuthentification->setDisplay('None');
            $this->choixTelechargement->checked = true;
        }
        $this->libelleConditionsFormulaireDemandePart2->setDisplay('None');
        $this->blocChoixAnonyme->setDisplay('None');
        $this->script->Text .= '<script>gererBlocCoordonnees();</script>';
        $this->completerFormulaireDemande->Text = prado::localize('COMPLETER_FORMULAIRE_DEMANDE');
        $this->descriptionConditions->Text = prado::localize('DEFINE_CONDITIONS_GENERALES_UTILISATION');
        $this->titreFormulaire->Text = prado::localize('DEFINE_FORMULAIRE_IDENTIFICATION_QUESTION');
    }

    public function getDenominationOrg()
    {
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($this->_organisme);
        if ($organisme) {
            $nomOrganisme = $organisme->getDenominationOrg();
        }

        return $nomOrganisme;
    }

    public function displayVilleRc()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $data['99'] = Prado::localize('PAS_DE_RC');

        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->RcVille->DataSource = $data;
        $this->RcVille->DataBind();
    }

    /**
     * Adapter le formulaire pour un affichage qui correspond a MPE MAROC.
     */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
            //$this->panelLabelSiren->setCssClass("intitule-150");
            $this->siren->SetMaxLength(Atexo_Config::getParameter('IFU_MAX_LENGTH'));
            $this->siret->setDisplay('None');
            $this->validatorSiren->SetEnabled(false);
            $this->labelInfoTel->setVisible(false);
            $this->labelInfoFax->setVisible(false);
            $this->panelRc->setVisible(true);
            $this->panelSiren->setVisible(false);
            $this->panelIdentifiantUnique->setVisible(false);
            if (!$this->getPostBack()) {
                $this->displayVilleRc();
            }
        } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
            $this->panelIdentifiantUnique->setVisible(true);
            $this->panelRc->setVisible(false);
            $this->panelSiren->setVisible(false);
            $this->validatorSiren->SetEnabled(false);
        } else {
            $this->panelRc->setVisible(false);
            $this->panelSiren->setVisible(true);
            $this->panelIdentifiantUnique->setVisible(false);
        }
    }
}
