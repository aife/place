<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonBlobFile;
use Application\Propel\Mpe\CommonBlobFilePeer;
use Application\Propel\Mpe\CommonCG76DonneeComplementaireDomaine;
use Application\Propel\Mpe\CommonCG76DonneeComplementaireEntreprise;
use Application\Propel\Mpe\CommonCG76PieceJointe;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\DomainesActivites\Atexo_DomainesActivites_Domaines;
use Application\Service\Atexo\DonneesComplementairesEntreprise\Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise;
use Exception;
use Prado\Prado;

/**
 * Page des données complémentaires des entreprises*/
class DonneesComplementaires extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseDonneesComplementaires') && Atexo_CurrentUser::isATES()) {
            if (!$this->IsPostBack) {
                $this->DomainesActivites->displayDomaines();
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

                $donneesComplementaires = (new Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise())->retrieveDonneComplementaireByIdEntreprise(Atexo_CurrentUser::getIdEntreprise());

                if ($donneesComplementaires) {
                    $this->email->Text = $donneesComplementaires->getEmail();
                    $this->typeFormation->Text = $donneesComplementaires->getTypeFormation();
                    $this->coutMoyen->Text = $donneesComplementaires->getCoutMoyenJournee();
                    if ('1' == $donneesComplementaires->getCollaborationFpt()) {
                        $this->collaborationFPT_oui->Checked = true;
                    } else {
                        $this->collaborationFPT_non->Checked = true;
                    }
                    if ('1' == $donneesComplementaires->getCollaborationFpe()) {
                        $this->collaborationFPE_oui->Checked = true;
                    } else {
                        $this->collaborationFPE_non->Checked = true;
                    }
                    if ('1' == $donneesComplementaires->getCentreDocumentation()) {
                        $this->centreDocumentation_oui->Checked = true;
                    } else {
                        $this->centreDocumentation_non->Checked = true;
                    }
                    if ('1' == $donneesComplementaires->getServiceReprographie()) {
                        $this->serviceReprographie_oui->Checked = true;
                    } else {
                        $this->serviceReprographie_non->Checked = true;
                    }
                    if ('1' == $donneesComplementaires->getSalleInfo()) {
                        $this->salleInformatique_oui->Checked = true;
                    } else {
                        $this->salleInformatique_non->Checked = true;
                    }
                    if ('1' == $donneesComplementaires->getSalleCours()) {
                        $this->salleCours_oui->Checked = true;
                    } else {
                        $this->salleCours_non->Checked = true;
                    }
                    $this->aireGeoIntervention->Text = $donneesComplementaires->getAireGeoInter();

                    //$idDonneeComplementaire = $donneesComplementaires->getRef();

                    /*Liste Pièces jointe*/
                    $infos = (new Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise())->retrievePieceJointeByIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
                    $files = [];
                    $chaineFiles = '';
                    if ($infos) {
                        foreach ($infos as $info) {
                            if ($info instanceof CommonCG76PieceJointe) {
                                $blobFile = null;
                                $c = new Criteria();
                                $c->add(CommonBlobFilePeer::ID, $info->getIdpj());
                                $blobFile = CommonBlobFilePeer::doSelectOne($c, $connexionCom);
                                if ($blobFile instanceof CommonBlobFile) {
                                    $infos['name'] = $blobFile->getName();
                                    $infos['id'] = $blobFile->getId();
                                    $files[] = $infos;
                                    $chaineFiles .= $blobFile->getId().',';
                                }
                            }
                        }
                    }
                    $this->repeaterPj->DataSource = $files;
                    $this->repeaterPj->DataBind();
                    $this->listFiles->Value = $chaineFiles;

                    $domaineChoisis = (new Atexo_DomainesActivites_Domaines())->retrieveDomaineByIdDonneeCompelementaire($donneesComplementaires->getRef());
                    $liste = [];
                    if (is_array($domaineChoisis)) {
                        foreach ($domaineChoisis as $domaine) {
                            $liste[$domaine->getIddomaine()] = $domaine->getIddomaine();
                        }
                    }
                    $this->DomainesActivites->checkDomaines($liste);
                }
            }
        } else {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreurModule->setMessage(Prado::localize('DEFINE_ERREUR_NO_HABILITE_PAGE'));
            $this->mainPart->setVisible(false);
        }
    }

    /**
     * creer une données complémentaires*/
    public function createDonneeComplementaire()
    {
        if ($this->IsValid) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexionCom->beginTransaction();
            try {
                $IdInscrit = Atexo_CurrentUser::getIdInscrit();
                $IdEntreprise = Atexo_CurrentUser::getIdEntreprise();
                $donneesComplementaires = (new Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise())->retrieveDonneComplementaireByIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
                if (!$donneesComplementaires) {
                    $donneesComplementaires = new CommonCG76DonneeComplementaireEntreprise();
                }
                $donneesComplementaires->setIdinscrit($IdInscrit);
                $donneesComplementaires->setIdentreprise($IdEntreprise);
                $donneesComplementaires->setEmail(trim(trim($this->email->Text)));
                $donneesComplementaires->setTypeFormation($this->typeFormation->Text);
                $donneesComplementaires->setCoutMoyenJournee($this->coutMoyen->Text);
                if ($this->collaborationFPT_oui->Checked) {
                    $donneesComplementaires->setCollaborationFpt(1);
                } else {
                    $donneesComplementaires->setCollaborationFpt(0);
                }
                if ($this->collaborationFPE_oui->Checked) {
                    $donneesComplementaires->setCollaborationFpe(1);
                } else {
                    $donneesComplementaires->setCollaborationFpe(0);
                }
                if ($this->centreDocumentation_oui->Checked) {
                    $donneesComplementaires->setCentreDocumentation(1);
                } else {
                    $donneesComplementaires->setCentreDocumentation(0);
                }
                if ($this->serviceReprographie_oui->Checked) {
                    $donneesComplementaires->setServiceReprographie(1);
                } else {
                    $donneesComplementaires->setServiceReprographie(0);
                }
                if ($this->salleInformatique_oui->Checked) {
                    $donneesComplementaires->setSalleInfo(1);
                } else {
                    $donneesComplementaires->setSalleInfo(0);
                }
                if ($this->salleCours_oui->Checked) {
                    $donneesComplementaires->setSalleCours(1);
                } else {
                    $donneesComplementaires->setSalleCours(0);
                }
                $donneesComplementaires->setAireGeoInter($this->aireGeoIntervention->Text);
                $donneesComplementaires->save($connexionCom);
                $IdDonneeComplementaire = $donneesComplementaires->getRef();
                //Insertion pièce Jointe
                $IdEntreprise = Atexo_CurrentUser::getIdEntreprise();
                $listeIds = $this->listFiles->Value;

                (new Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise())->deletePieceJointeByIdEntreprise($IdEntreprise);
                if ('' != $listeIds) {
                    $arrayIds = explode(',', $listeIds);
                    foreach ($arrayIds as $idBlob) {
                        if (trim($idBlob)) {
                            $pieceJointe = new CommonCG76PieceJointe();
                            $pieceJointe->setIdentreprise($IdEntreprise);
                            $pieceJointe->setIdpj($idBlob);
                            $pieceJointe->save($connexionCom);
                        }
                    }
                }
                //Insertion Liste des domaines
                if ($donneesComplementaires) {
                    (new Atexo_DomainesActivites_Domaines())->deleteDomainesByIdDonneeComplementaire($donneesComplementaires->getRef());
                }
                $listeDomainesChoisis = $this->DomainesActivites->getCheckedDomaines();
                if (is_array($listeDomainesChoisis)) {
                    foreach ($listeDomainesChoisis as $IdDomaine) {
                        $domaine = new CommonCG76DonneeComplementaireDomaine();
                        $domaine->setIddonneecomlementaire($IdDonneeComplementaire);
                        $domaine->setIddomaine($IdDomaine);
                        $domaine->save($connexionCom);
                    }
                }
                $connexionCom->commit();
                $this->response->redirect('?page=Entreprise.EntrepriseAccueilAuthentifie&action=orgF');
            } catch (Exception) {
                $connexionCom->rollback();
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage("Erreur lors de la sauvegarde de l'organisme de formation.");
            }
        }
    }

    public function refreshListePj($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $listeIds = $this->listFiles->Value;
        $arrayIds = explode(',', $listeIds);
        $files = [];
        foreach ($arrayIds as $idBlob) {
            if (trim($idBlob)) {
                $blobFile = null;
                $c = new Criteria();
                $c->add(CommonBlobFilePeer::ID, $idBlob);
                $blobFile = CommonBlobFilePeer::doSelectOne($c, $connexionCom);
                if ($blobFile instanceof CommonBlobFile) {
                    $infos['name'] = $blobFile->getName();
                    $infos['id'] = $blobFile->getId();
                    $files[] = $infos;
                }
            }
        }
        $this->repeaterPj->DataSource = $files;
        $this->repeaterPj->DataBind();

        $this->files->render($param->NewWriter);
    }

    public function downloadBlob($sender, $param)
    {
        $idBlob = $param->CommandName;
        $nomBlob = $param->CommandParameter;
        $atexoBlob = new Atexo_Blob();
        $atexoBlob->sendBlobToClient($idBlob, $nomBlob, Atexo_Config::getParameter('COMMON_BLOB'));
        exit;
    }

    public function returnToAccueil()
    {
        $this->response->redirect('?page=Entreprise.EntrepriseAccueilAuthentifie');
    }
}
