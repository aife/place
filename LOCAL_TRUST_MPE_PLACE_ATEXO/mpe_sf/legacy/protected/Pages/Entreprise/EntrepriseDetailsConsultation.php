<?php

namespace Application\Pages\Entreprise;

use App\Service\Publicite\EchangeConcentrateur;
use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonComplement;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonRelationEchange;
use Application\Propel\Mpe\CommonTBourseCotraitanceQuery;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTComplementFormulairePeer;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Propel\Mpe\CommonTEditionFormulairePeer;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\CommonTSupportPublicationQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_BourseCotraitance;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Menu;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Complements;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Register;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CoffreFort;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_ComplementFormulaire;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_CriteriaVo;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition;
use Application\Service\Atexo\Menu\Atexo_Menu_MenuVo;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_FormatLibreVo;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Page details d'une consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class EntrepriseDetailsConsultation extends DownloadFile
{
    private $_consultation;
    private $_organismeAcronyme;
    private bool $_showDownloadRg = false;
    private bool $_showDownloadDce = false;
    private bool $_showDownloadComplement = false;
    private array $_arrayTriTelech = [];
    private array $_arrayTriQuestion = [];
    private array $_arrayTriMessagerie = [];
    protected array $arrayTypeAR = [];
    protected array $arrayFormat = [];
    //si TypeAvis = 3 => consultation
    protected ?bool $_isConsultation = null;

    public int $currentTab = 1;
    private $_idConsultation;

    /**
     * recupere la valeur de [isConsultation] dans le viewstate de la page.
     *
     * @return bool la valeur courante [isConsultation] dans le viewstate de la page
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIsConsultation()
    {
        return $this->_isConsultation;
    }

    /**
     * modifie la valeur de [isConsultation] dans le viewstate de la page.
     *
     * @param bool $isConsultation la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setIsConsultation($isConsultation)
    {
        $this->_isConsultation = $isConsultation;
    }

    public function onPreInit($param)
    {
        if (isset($_GET['refConsultation']) && isset($_GET['orgAcronyme'])) {
            $id = Atexo_Util::atexoHtmlEntities($_GET['refConsultation']);
            $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
            $serviceSF = Atexo_Util::getSfService('app.mapping.consultation');
            $this->_idConsultation = $serviceSF->getConsultationId($id, $org);
        } elseif (isset($_GET['id'])) {
            $this->_idConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);
        }

        if ($this->_idConsultation && -1 != $this->_idConsultation) {
            $this->_consultation = $this->getCriteriaConsultation($this->_idConsultation);
            $_GET['id'] = $this->_idConsultation;

            if ($this->_consultation instanceof CommonConsultation
                && $this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')
                && !isset($_GET['clotures'])
            ) {
                $url = Atexo_MpeSf::getUrlDetailConsultationMpeSf(
                    $this->_consultation->getId(),
                    Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme'])
                );
                $this->response->redirect($url);
            }
        }
    }

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = null;
        if (-1 == $this->_idConsultation) {
            $this->mainPart->setVisible(false);
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('MESSAGE_ERREUR_DEUX_CONSULTATIONS_EN_COLLISION'));

            return;
        }
        if (!$this->_consultation) {
            $this->mainPart->setVisible(false);
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_AUCUN_RESULTAT'));

            return;
        }

        if (isset($_GET['retraits'])) {
            $this->currentTab = 1;
        }
        if (isset($_GET['questions'])) {
            $this->currentTab = 2;
        }
        if (isset($_GET['depots'])) {
            $this->currentTab = 3;
        }
        if (isset($_GET['echanges'])) {
            $this->currentTab = 4;
        }

        if ('succes' == $_GET['action']) {
            $this->panelMessageActionOk->setVisible(true);
            $this->panelMessageActionOk->setMessage(Prado::localize('TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION'));
        }
        if (isset($_GET['inscription'])) {
            $this->panelMessageDejaInscrit->setVisible(true);
            $this->panelMessageDejaInscrit->setMessage(Prado::localize('DEFINE_DEJA_INSCRIT_COTRAITANCE'));
        }
        if (Atexo_Module::isEnabled('SocleExternePpp') && !Atexo_CurrentUser::getIdInscrit()) {
            $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
            if (isset($headersArray['EXTERNALID']) && isset($headersArray['USERTYPE']) && 'EMPLOYEE' == $headersArray['USERTYPE']) {
                self::authenticateViaPPPSocleEntreprise($headersArray['EXTERNALID']);
            }
        }

        if ((Atexo_CurrentUser::isAgent() || Atexo_CurrentUser::isAgentSocle()) && Atexo_CurrentUser::isConnected()) {
            $this->getApplication()->getModule('auth')->logout();
        }
        //Debut acces depuis le mail de notification aux entreprises suite aux demandes de complements
        if (isset($_GET['clotures']) && isset($_GET['depots']) && isset($_GET['orgAcronyme']) && isset($_GET['id']) && !Atexo_CurrentUser::isConnected()) {
            $id = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);

            $url = Atexo_Config::getParameter("URL_LT_MPE_SF") . '/consultation/' . $id;
            $this->response->redirect($url);
        }
        //Fin acces depuis le mail de notification aux entreprises suite aux demandes de complements
        $this->AtexoFormulaire->setCalledFrom('entreprise');
            if ($_GET['id'] && $_GET['orgAcronyme'] && $_GET['code']) {
            //acces details consultation restreinte
            Atexo_CurrentUser::deleteFromSession('organisme');
            Atexo_CurrentUser::deleteFromSession('reference');
            Atexo_CurrentUser::deleteFromSession('codeAcces');
            Atexo_CurrentUser::writeToSession('organisme', Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            $this->_consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            Atexo_CurrentUser::writeToSession('reference', $this->_consultation->getReferenceUtilisateur());
            if ($this->_consultation instanceof CommonConsultation &&
                $this->_consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                Atexo_CurrentUser::writeToSession('codeAcces', str_replace(" ", "+", $_GET['code']));
            }
        }
        if (!$_GET['orgAcronyme']) {
            $this->mainPart->setVisible(false);
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE'));
        } elseif (!Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']))) {
            echo Prado::localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            exit;
        } else {
            $dateDebAction = date('Y-m-d H:i:s');
            $this->panelMessage->setVisible(false);
            if (isset($_GET['Success'])) {
                $this->panelMessage->setVisible(true);
                $this->panelMessage->setMessage(Prado::localize('TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION'));
            } elseif (isset($_GET['Failed'])) {
                $this->panelMessageErreur->setVisible(true);
                $this->errorPart->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_MODIFICATION'));
            }

            if (0 == strcmp($_GET['id'], (int) $_GET['id'])) {
                $critere = new Atexo_Consultation_CriteriaVo();
                //Acces aux details de la consultation depuis le menu "Panier entreprise" en cliquant sur les liens (
                //registre des retraits, questions, depots, messages): autoriser les consultations cloturees du panier
                if (Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['clotures'])
                    && (isset($_GET['retraits']) || isset($_GET['questions']) || isset($_GET['depots']) || isset($_GET['echanges']))
                    && (new Atexo_Consultation())->verifierConsultationDansPanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']))) {
                    $critere->setAvecConsClotureesSansPoursuivreAffichage(true);
                }
                $this->_consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), true, $critere);
                if (!$this->_consultation) {
                    $this->mainPart->setVisible(false);
                    $this->errorPart->setVisible(true);
                    $this->panelMessageErreur->setVisible(true);
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                    $consultation = CommonConsultationPeer::retrieveByPK($_GET['id'], $connexion);
                    $msg = Prado::localize('TEXT_AUCUNE_CONSULTATION');
                    if ($consultation instanceof CommonConsultation) {
                        if ($consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                            $msg = Prado::localize('CONSULTATAION_RESTREINTE_HORS_LIGNE');
                        } else {
                            $msg = str_replace(['{_ref_consultation_}', '{_intitule_consultation_}'], [$consultation->getReferenceUtilisateur(), $consultation->getIntituleTraduit()], Prado::localize('MSG_CONSULTATION_N_EST_PAS_EN_LIGNE'));
                        }
                    }
                    $this->panelMessageErreur->setMessage($msg);
                } else {
                    if ($this->_consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                        $this->setIsConsultation(true);
                    } else {
                        $this->setIsConsultation(false);
                    }
                    $this->displayConsultation($this->_consultation);
                    if ($this->_consultation->getIdTypeAvis() != Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                        //$this->detailConsultation->setVisible(false);
                        $this->panelPieces->setVisible(false);
                        $this->panelActionsQuestion->setVisible(false);
                        $this->panelActionsRepondre->setVisible(false);
                        $this->panelRepondreConsultationMPS->setVisible(false);
                    // MPE-5984
                        /*if ($this->_consultation->getIdTypeAvis() == 2) {
                        // SDR : avec la nouvelle charte ce lien a ete supprime donc j'ai désactive l'apl sur le code
                            //  $this->linkRetourBas->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AvisInformation";
                            //$this->linkNewSearch->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AllAnn";
                            //$this->linkRetourBas2->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AvisInformation";
                            //$this->linkNewSearch2->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AllAnn";
                        }
                        elseif($this->_consultation->getIdTypeAvis() == 4){
                          //  $this->linkRetourBas->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AvisAttribution";
                          //  $this->linkNewSearch->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AllAnn";
                         //   $this->linkRetourBas2->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AvisAttribution";
                          //  $this->linkNewSearch2->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AllAnn";
                        }*/
                    } else {
                        //$this->linkRetourBas->NavigateUrl= "index.php?page=Entreprise.EntrepriseAdvancedSearch&AllCons";
                        $dateFin = $this->_consultation->getDatefin();
                        $dateFinPoursuivreAffichage = Atexo_Util::dateDansFutur($dateFin, 0, $this->_consultation->getPoursuivreAffichage());
                        if (((Atexo_Module::isEnabled('EntrepriseRepondreConsultationApresCloture') && $dateFinPoursuivreAffichage >= date('Y-m-d H:i:s'))
                                || (Atexo_Util::cmpIsoDateTime(date('Y-m-d H:i:s'), $dateFin) <= 0)) && !$this->_consultation->getConsultationAnnulee()) {
                            if (1 == $this->_consultation->getAutoriserReponseElectronique()) {
                                $this->buttonRepondreConsultation->setVisible(true);
                                $this->buttonRepondreConsultationVertical->setVisible(true);

                                if (Atexo_Module::isEnabled('MarchePublicSimplifie', $this->_consultation->getOrganisme()) && 1 == $this->_consultation->getMarchePublicSimplifie()) {
                                    $this->panelRepondreConsultationMPS->setVisible(true);
                                    $this->labelReponseElectroniqueMPS->Text = Prado::localize('TEXT_REPONSE_ELECTRONIQUE_MPS');
                                    $this->labelReponseElectronique->Text = Prado::localize('TEXT_REPONSE_ELECTRONIQUE_STANDARD');
                                } else {
                                    $this->labelReponseElectronique->Text = Prado::localize('SI_REPONSE_CLIQUEZ_BOUTON').'.';
                                    $this->panelRepondreConsultationMPS->setVisible(false);
                                }

                                if (Atexo_Module::isEnabled('InterfaceModuleSub') && Atexo_CurrentUser::isConnected()) {
                                    $this->listeFormulairesSub->Visible = true;
                                }
                            } else {
                                $this->panelRepondreConsultationMPS->setVisible(false);
                                $this->buttonRepondreConsultation->setVisible(false);
                                $this->buttonRepondreConsultationVertical->setVisible(false);
                                $this->labelReponseElectronique->Text = Prado::localize('TEXT_REPONSE_ELECTRONIQUE_REFUSEE').'.';
                                $this->buttonRepondreConsultationMPS->setVisible(false);
                            }
                        } else {
                            $this->panelRepondreConsultationMPS->setVisible(false);
                            $this->panelActionsQuestion->setVisible(false);
                            $this->panelActionsRepondre->setVisible(false);
                            //$this->panelPieces->setVisible(false);
                        }
                    }
                    //$this->panelOnglet4->setStyle('display:block;');
                    $this->displayAvisPubliciteFormatLibre($this->_consultation->getId(), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                    self::showHideTextesDocumentsJoints();

                    //Remplissage des contenus des onglets
                    self::remplirListeTelechargement(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                    self::remplirListeQuestion(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                    self::remplirListeDepot(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities($_GET['id']));
                    $this->remplirListeCandidatureCoTraitant(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities($_GET['id']));
                    $this->arrayTypeAR = Atexo_Message::getAllTypesAR(Atexo_CurrentUser::getCurrentOrganism());
                    $this->arrayFormat = Atexo_Message::getAllFormatsEchange(Atexo_CurrentUser::getCurrentOrganism(), true);
                    self::remplirListeMessagerie(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities($_GET['id']));
                    if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                        if (isset($_GET['dossierNonVal'])) {
                            $this->afficherMessageErreur(Prado::localize('DEFINE_MESSAGE_ERREUR_ENVOI_COMPLEMENT_SUB_DOSSIERS_NON_VALIDES'));
                        } elseif (isset($_GET['editionNonGen'])) {
                            $this->afficherMessageErreur(Prado::localize('DEFINE_MESSAGE_ERREUR_ENVOI_COMPLEMENT_SUB_EDITIONS_NON_GENEREES'));
                        } elseif (isset($_GET['instructionKO'])) {
                            $this->afficherMessageErreur(Prado::localize('DEFINE_MESSAGE_ERREUR_PASSAGE_INSTRUCTION_SUB_DOSSIERS_KO'));
                        } elseif (isset($_GET['idCmp']) && '' != $_GET['idCmp']) {
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                            $complement = CommonTComplementFormulairePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idCmp'])), $connexion);
                            if ($complement instanceof CommonTComplementFormulaire) {
                                $numeroComplement = $complement->getNumeroComplement();
                                $this->panelMessage->setVisible(true);
                                $this->panelMessage->setMessage(str_replace('[__numero_complement__]', $numeroComplement, Prado::localize('MESSAGE_CONFIRMATION_ENVOI_DEMANDE_COMPLEMENT')));
                                $_GET['idCmp'] = null;
                            }
                        }
                    }
                    if (Atexo_Module::isEnabled('InterfaceModuleSub') && Atexo_CurrentUser::isConnected()) {
                        $this->listeFormulairesSub->setConsultation($this->getConsultation());
                        $this->setViewState('consultation', $this->_consultation);
                        if (!$this->IsCallback) {
                            $this->chargerRepeaterDemandeComplements();
                            $this->chargerRepeaterDemandeComplementsTransmis();
                        }
                    }
                }
            }
        }

        if (!$this->isPostBack) {
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION4', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            if ($this->_consultation instanceof CommonConsultation) {
                $description = str_replace('[__ref_utilisateur__]', $this->_consultation->getReferenceUtilisateur(), $description);
            }
            $this->traceOperationsInsrctis($IdDesciption, $dateDebAction, '', Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), $description);
        }
        //Affichage des onglets
        self::customizeTab();
        //Chargement du formulaire SUB
    }

    public function displayConsultation($consultation)
    {
        $this->_organismeAcronyme = $consultation->getOrganisme();
        if ($_GET['orgAcronyme']) {
            $organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        } else {
            $organisme = $consultation->getOrganisme();
        }
        $this->onClickDownloadDce($organisme);
        $this->setDownlodReglementNavigateUrl($organisme);
        $this->displayLinkDownlodComplement();
    }

    /**
     * Retourne la taille du dossier DCE.
     */
    public function setTailDceFile($Org)
    {
        $dce = (new Atexo_Consultation_Dce())->getDce($this->_consultation->getId(), $Org);

        if ($dce) {
            $tailleDce = (new Atexo_Consultation_Dce())->getTailleDCE($dce, $_GET['orgAcronyme']);
            $this->linkDownloadDce->Text = Prado::localize('DEFINE_DOSSIER_CONSULTATION').' - '.$tailleDce;
            //$this->tailleDce->NavigateUrl .= $this->linkDownloadDce->NavigateUrl;
        }
    }

    /**
     * Retourne la taille du dossier Regelement.
     */
    public function setTailleComplement($complement, $organisme)
    {
        if ($complement) {
            $tailleComplement = Atexo_Util::arrondirSizeFile((new Atexo_Blob())->getTailFile($complement->getComplement(), $organisme) / 1024);
            $this->linkDownloadComplement->Text = Prado::localize('DEFINE_SAVOIR_PLUS_CONSULTATION').' - '.$tailleComplement;
            //$this->tailleComplement->NavigateUrl .= $this->linkDownloadComplement->NavigateUrl;
        }
    }

    public function setTailReglement($organisme)
    {
        $rg = (new Atexo_Consultation_Rg())->getReglement($this->_consultation->getId(), $organisme);
        if ($rg) {
            $tailleRg = Atexo_Util::arrondirSizeFile((new Atexo_Blob())->getTailFile($rg->getRg(), $organisme) / 1024);
            $this->linkDownloadReglement->Text = Prado::localize('REGELEMENT_CONS').' - '.$tailleRg;
            //$this->tailleReglement->NavigateUrl .= $this->linkDownloadReglement->NavigateUrl;
        }
    }

    /**
     * Cree le lien de telechargement du dossier DCE.
     */
    public function onClickDownloadDce($Org)
    {
        $dce = '';
        $dce = (new Atexo_Consultation_Dce())->getDce(Atexo_Util::atexoHtmlEntities($_GET['id']), $Org);

        if ($dce && 0 != $dce->getDce()) {
            $this->linkDownloadDce->NavigateUrl = 'index.php?page=Entreprise.EntrepriseDemandeTelechargementDce&id='.
                Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
            $this->setTailDceFile($Org);
            $this->_showDownloadDce = true;
        } else {
            $this->_showDownloadDce = false;
        }
    }

    /**
     * Cree le lien de dossier Reglement de la consultation.
     */
    public function setDownlodReglementNavigateUrl($organisme)
    {
        $rg = '';
        $rg = (new Atexo_Consultation_Rg())->getReglement($this->_consultation->getId(), $organisme);
        if ($rg && 0 != $rg->getRg()) {
            $this->linkDownloadReglement->NavigateUrl = 'index.php?page=Entreprise.EntrepriseDownloadReglement&id='.
                base64_encode($this->_consultation->getId()).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
            $this->setTailReglement($organisme);
            $this->_showDownloadRg = true;
        } else {
            $this->_showDownloadRg = false;
        }
    }

    // SUPPRESSION DE LA FONCTION setUrlLots($consultation) qui ne semble pas être utilisé (apparait en gris sur PHPSTORM + aucun appel dans le projet

    public function getOrganismeAcronyme()
    {
        return $this->_organismeAcronyme;
    }

    public function repondreConsultation($sender, $param)
    {
        if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
            if (!Atexo_CurrentUser::isConnected()) {
                $url = 'index.php?page=Entreprise.EntrepriseHome&goto=';
                $url .= urlencode('?page=Entreprise.EntrepriseDetailsConsultation&id='
                    .Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&depots');
                $this->response->redirect($url);
            }
            //Verifier si la reponse contient au moins un dossier candidature et un dossier d'offres
            $dossierOffreTrouve = false;
            $this->verifierSiReponseContientUnDossierOffre($dossierOffreTrouve);
            if (!$dossierOffreTrouve) {
                $this->afficherMessageErreur(Prado::localize('DEFINE_MESSAGE_ERREUR_ACCES_FORMULAIRE_REPONSE_SANS_DOSSIER_OFFRE'));

                return;
            }

            //Verifier si toutes les editions sont deja generees
            $toutesEditionGenerees = true;
            $tousDossiersValidees = true;
            $this->verifierSiToutesEditionsReponseGenerees($tousDossiersValidees, $toutesEditionGenerees);
            if (!$tousDossiersValidees || !$toutesEditionGenerees) {
                if (!$tousDossiersValidees) {
                    $this->afficherMessageErreur(Prado::localize('DEFINE_MESSAGE_ERREUR_ACCES_FORMULAIRE_REPONSE_DOSSIERS_NON_VALIDES'));
                } elseif (!$toutesEditionGenerees) {
                    $this->afficherMessageErreur(Prado::localize('DEFINE_MESSAGE_ERREUR_ACCES_FORMULAIRE_REPONSE_EDITIONS_NON_GENEREES'));
                }
                $_GET['depots'] = 'depots';

                return;
            }
        }
        $url = 'index.php?page=Entreprise.FormulaireReponseConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id'])
            .'&org='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $this->response->redirect($url);

        $menuVo = new Atexo_Menu_MenuVo();
        $menuVo->setCmd('accept_explication_send_offers');
        $menuVo->setOrg(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        $menuVo->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $menuVo->setRefParamName('refMarche');

        (new Atexo_Menu())->activateMenuEntreprise($menuVo);
    }

    /**
     * Permet d'acceder au formulaire de poser une question.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function poserQuestion($sender, $param)
    {
        $this->response->redirect('index.php?page=Entreprise.EntrepriseFormulairePoserQuestion&id='
            .Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&questions');
    }

    public function displayAvisPubliciteFormatLibre($consultationId, $organisme)
    {
        $listeFormAvis = (new Atexo_Publicite_Avis())->retreiveListAvisEnvoye($consultationId, $organisme);
        $consultation = (new CommonConsultationQuery())->findOneById($consultationId);
        $dataSource = [];
        $index = 0;
        $atexoBlob = new Atexo_Blob();
        foreach ($listeFormAvis as $oneAvis) {
            $newFormLibre = new Atexo_Publicite_FormatLibreVo();
            if ($oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT') || $oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED')
                || $oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_AVIS_IMPORTES') || $oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED_HORS_PF')) {
                if ($oneAvis->getTypeDocGenere() == Atexo_Config::getParameter('TYPE_DOC_GENERE_AVIS')) {
                    if (Atexo_Module::isEnabled('PubliciteOpoce')) {
                        if ($oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED')) {
                            $newFormLibre->setLibelleType(Prado::localize('TEXT_AVIS_EUROPEEN_FICHIER_JOINT'));
                        } elseif ($oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_AVIS_IMPORTES')) {
                            $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_PUB'));
                        } else {
                            $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_PRESSE'));
                        }
                    } else {
                        $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_PUBLICITE').' - '.strtoupper($oneAvis->getLangue()));
                    }
                } else {
                    if ($oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED_HORS_PF')) {
                        $newFormLibre->setLibelleType(Prado::localize('TEXT_AVIS_EUROPEEN_FICHIER_JOINT'));
                    } else {
                        $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_COMPLEMENTAIRE'));
                    }
                }
                $newFormLibre->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
                $taille = Atexo_Util::arrondirSizeFile($atexoBlob->getTailFile($oneAvis->getAvis(), $organisme) / 1024);
                $newFormLibre->setTaille($taille);
                $url = '/entreprise/consultation/'
                    . $oneAvis->getConsultationId()
                    . '/avis/' . $oneAvis->getId()
                    . '/telechargement/'
                    //Passage du type avis
                    . $consultation->getIdTypeAvis();
                $newFormLibre->setUrlPieceLibre($url);
            } else {
                $newFormLibre->setLibelleType(Prado::localize('TEXT_URL_ACCES_DIRECT'));
                $newFormLibre->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_URL'));
                $newFormLibre->setUrl($oneAvis->getUrl());

            }
            $newFormLibre->setId($oneAvis->getId());
            $newFormLibre->setDatePub(Atexo_Util::iso2frnDateTime($oneAvis->getDateCreation(), false));
            $newFormLibre->setStatut($oneAvis->getStatut());
            $dataSource[$index] = $newFormLibre;
            ++$index;
        }
        // Ajouter les liens PDF au BOAMP dans le data source
        $listLienPdfBoamp = (new Atexo_Publicite_Destinataire())->retrieveAnnonceByRefCons($consultationId, $organisme);
        $dataSourceBoamp = [];
        $indexX = 0;
        foreach ($listLienPdfBoamp as $oneAnnonce) {
            if ('' != $oneAnnonce->getLienBoamp()) {
                $dataSourceBoamp[$indexX] = $this->getObjectFormatLibreVo(
                    Prado::localize('NOM_LIEN_AVIS_BOAMP'),
                    Atexo_Config::getParameter('TYPE_FORMAT_URL_BOAMP'),
                    $oneAnnonce->getLienBoamp(),
                    $oneAnnonce->getStatutDestinataire()
                );
                ++$indexX;
            }
        }
        // Ajouter des liens de publication de l'OPOCE
        $listeLiensAvisPub = (new Atexo_Publicite_AvisPub())->retrieveFormXmlDestPubByRefCons($consultationId, $organisme);
        $dataLiensAvisPub = [];
        $defaultValueLiensAvisPub = new Atexo_Publicite_FormatLibreVo();
        $dataLiensAvisPub[0] = $defaultValueLiensAvisPub;
        $increment = 0;
        foreach ($listeLiensAvisPub as $lienPub) {
            $dataLiensAvisPub[$increment] = $this->getObjectFormatLibreVo(
                Prado::localize('NOM_LIEN_AVIS_TED'),
                Atexo_Config::getParameter('TYPE_FORMAT_URL_TED'),
                $lienPub['lien_pub'],
                $lienPub['statut_destinataire']
            );
            ++$increment;
        }
        // publicite SUB BOAMP
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $dataSourceBoampSUB = [];
        $supportPubBoamp = CommonTSupportPublicationQuery::create()->findOneByCode(Atexo_Config::getParameter('CODE_SUPPORT_PUBLICATION_BOAMP'));
        if ($supportPubBoamp instanceof CommonTSupportPublication) {
            $listeAnnoncesSub = Atexo_Publicite_AnnonceSub::getSupportAnnonceByRefConsAndSupportPub($consultationId, $supportPubBoamp->getId(), Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE'), $connexion);
            $i = 0;
            if (is_array($listeAnnoncesSub) && count($listeAnnoncesSub)) {
                foreach ($listeAnnoncesSub as $annonceSub) {
                    if ($annonceSub instanceof CommonTSupportAnnonceConsultation) {
                        $dataSourceBoampSUB[$i] = $this->getObjectFormatLibreVo(
                            Prado::localize('NOM_LIEN_AVIS_BOAMP'),
                            Atexo_Config::getParameter('TYPE_FORMAT_URL_BOAMP'),
                            Atexo_Config::getParameter('URL_SUPPORT_PUBLICATION_BOAMP').$annonceSub->getNumeroAvis()
                        );
                        ++$i;
                    }
                }
            }
        }


        //Pub sur le concenctreur
        $listePubs = $this->getLienPubInfo($consultationId);
        $dataPubConcentrateur = [];
        /**
         * Règle repris du template :
         * mpe_sf/templates/consultation/consultation-pub-telechargements.html.twig
         */
        foreach ($listePubs as $pub) {
            if ($pub['lienTelechargement'] != null) {
                $dataPubConcentrateur[] = $this->getObjectFormatLibreVo(
                    Prado::localize('TEXT_FICHIER_JOINT_AVIS_COMPLEMENTAIRE'),
                    Atexo_Config::getParameter('TYPE_FORMAT_URL_TED'),
                    $pub['lienTelechargement'],
                    $pub['statut']
                );
            }
            if ($pub['lienPublicationTed'] != null) {
                $dataPubConcentrateur[] = $this->getObjectFormatLibreVo(
                    Prado::localize('NOM_LIEN_AVIS_TED'),
                    Atexo_Config::getParameter('TYPE_FORMAT_URL_TED'),
                    $pub['lienPublicationTed'],
                    $pub['statut']
                );
            }
            if ($pub["support"]["code"] == 'ENOTICES' && $pub['lienPublication'] != null)  {
                $dataPubConcentrateur[] = $this->getObjectFormatLibreVo(
                    Prado::localize('NOM_LIEN_AVIS_TED'),
                    Atexo_Config::getParameter('TYPE_FORMAT_URL_TED'),
                    $pub['lienPublication'],
                    $pub['statut']
                );
            }
        }

        $this->repeaterAvis->DataSource = array_merge($dataSource, $dataSourceBoamp, $dataLiensAvisPub, $dataSourceBoampSUB, $dataPubConcentrateur);
        $this->repeaterAvis->DataBind();
    }

    private function getLienPubInfo($consultationId): array
    {
        $listeAvis = [];
        if (Atexo_Module::isEnabled('Publicite')) {
            try {
                $echangeConcentrateur = Atexo_Util::getSfService(EchangeConcentrateur::class);
                $accessToken = $echangeConcentrateur->getToken();
                if (!empty($accessToken)) {
                    $listeAvis = $echangeConcentrateur->getConsultationAll($accessToken, $consultationId);
                }
            } catch (\Exception $e) {
                Prado::log('Annonce -> Appel au concentrateur KO'.$e->getMessage().'-'.$e->getTraceAsString(), TLogger::ERROR, 'ERROR');
            }
        }
        return $listeAvis;
    }

    public function getObjectFormatLibreVo($libelle, $typeFormat, $url, $statut = '')
    {
        $newFormxml = new Atexo_Publicite_FormatLibreVo();
        $newFormxml->setLibelleType($libelle);
        $newFormxml->setTypeFormat($typeFormat);
        $newFormxml->setUrl($url);
        $newFormxml->setStatut($statut);

        return $newFormxml;
    }

    public function getToolTip($idEntreprise, $consultationId, $orgConsultation)
    {
        $piecesEntreprises = (new Atexo_Entreprise_CoffreFort())->retrievePiecesConsultationDocumentCfeByIdEntreprise($idEntreprise, $consultationId, $orgConsultation);
        if (!is_array($piecesEntreprises)) {
            $piecesEntreprises = [];
        }

        return Prado::localize('TEXT_AJOUTER_DOCUMENTS_COFFRE_FORT').' : '.count($piecesEntreprises).' '.Prado::localize('DOCUMENTS_AJOUTES');
    }

    public function showHideTextesDocumentsJoints()
    {
        $dce = (new Atexo_Consultation_Dce())->getDce(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        $rg = (new Atexo_Consultation_Rg())->getReglement($this->_consultation->getId(), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        $complement = (new Atexo_Consultation_Complements())->getComplement($this->_consultation->getId(), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        if (!$dce && !$rg && !$complement) {
            $this->divPiecesConsultation->setVisible(false);
            $this->divNoPiecesConsultation->setVisible(true);
        } else {
            $this->divPiecesConsultation->setVisible(true);
            $this->divNoPiecesConsultation->setVisible(false);
        }
    }

    public function showDownloadRg()
    {
        return $this->_showDownloadRg;
    }

    public function showDownloadDce()
    {
        return $this->_showDownloadDce;
    }

    public function showDownloadComplement()
    {
        return $this->_showDownloadComplement;
    }

    public function getIdTypeAvisConsultation()
    {
        return $this->_consultation->getIdTypeAvis();
    }

    //----------------------------------------------------- Debut tableau telechargement --------------------------------------------------------------

    public function remplirListeTelechargement($organisme, $limit = null, $offset = null, $arrayTri = [])
    {
        $nombreElementTotal = Atexo_Consultation_Register::retrieveTelechargementByIdInscritAndRefCons(
            $this->_consultation->getId(),
            Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']),
            Atexo_CurrentUser::getIdEntreprise(),
            Atexo_CurrentUser::getIdInscrit(),
            true
        );
        if ($nombreElementTotal >= 1) {
            $listeTelechargement = Atexo_Consultation_Register::retrieveTelechargementByIdInscritAndRefCons(
                $this->_consultation->getId(),
                Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']),
                Atexo_CurrentUser::getIdEntreprise(),
                Atexo_CurrentUser::getIdInscrit(),
                false,
                $limit,
                $offset,
                $arrayTri
            );
            $this->panelMoreThanOneElementFoundTelechargement->setStyle('display:block');
            $this->panelNoElementFoundTelechargement->setStyle('display:none');
            $this->nombreElementTelechargement->Text = $nombreElementTotal;

            $this->PagerBottomTelechargement->setVisible(true);
            $this->PagerTopTelechargement->setVisible(true);

            $this->setViewState('nombreElement', $nombreElementTotal);
            $this->setViewState('listeTelechargements', $listeTelechargement);

            $this->nombrePageTopTelechargement->Text = ceil($nombreElementTotal / $this->tableauTelechargement->PageSize);
            $this->nombrePageBottomTelechargement->Text = ceil($nombreElementTotal / $this->tableauTelechargement->PageSize);

            $this->tableauTelechargement->setVirtualItemCount($nombreElementTotal);
            $this->tableauTelechargement->dataSource = $listeTelechargement;
            $this->tableauTelechargement->dataBind();
        } else {
            $this->panelMoreThanOneElementFoundTelechargement->setStyle('display:none');
            $this->panelNoElementFoundTelechargement->setStyle('display:block');

            $this->PagerBottomTelechargement->setVisible(false);
            $this->PagerTopTelechargement->setVisible(false);
        }
    }

    public function pageChangedTelechargement($sender, $param)
    {
        $this->tableauTelechargement->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageTopTelechargement->Text = $param->NewPageIndex + 1;
        $this->numPageBottomTelechargement->Text = $param->NewPageIndex + 1;
        //$script = '<script language="JavaScript" type="text/JavaScript">document.getElementById("ctl0_CONTENU_PAGE_refreshRepeaterTelechargement").click();return false;</script>';
        //$this->javascriptTelechargement->Text = $script;
        $this->populateData();
    }

    public function goToPageTelechargement($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTopTelechargement':
                $numPage = $this->numPageTopTelechargement->Text;
                break;
            case 'DefaultButtonBottomTelechargement':
                $numPage = $this->numPageBottomTelechargement->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTopTelechargement->Text) {
                $numPage = $this->nombrePageTopTelechargement->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauTelechargement->CurrentPageIndex = $numPage - 1;
            $this->numPageBottomTelechargement->Text = $numPage;
            $this->numPageTopTelechargement->Text = $numPage;
            $this->populateData();
        } else {
            $this->numPageTopTelechargement->Text = $this->tableauTelechargement->CurrentPageIndex + 1;
            $this->numPageBottomTelechargement->Text = $this->tableauTelechargement->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenghtTelechargement($sender, $param)
    {
        switch ($sender->ID) {
            case 'listePageSizeTopTelechargement':
                $pageSize = $this->listePageSizeTopTelechargement->getSelectedValue();
                $this->listePageSizeBottomTelechargement->setSelectedValue($pageSize);
                break;
            case 'listePageSizeBottomTelechargement':
                $pageSize = $this->listePageSizeBottomTelechargement->getSelectedValue();
                $this->listePageSizeTopTelechargement->setSelectedValue($pageSize);
                break;
        }

        $this->tableauTelechargement->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTopTelechargement->Text = ceil($nombreElement / $this->tableauTelechargement->PageSize);
        $this->nombrePageBottomTelechargement->Text = ceil($nombreElement / $this->tableauTelechargement->PageSize);
        $this->tableauTelechargement->setCurrentPageIndex(0);
        $this->populateData();
    }

    public function populateData()
    {
        $offset = $this->tableauTelechargement->CurrentPageIndex * $this->tableauTelechargement->PageSize;
        $limit = $this->tableauTelechargement->PageSize;
        if ($offset + $limit > $this->tableauTelechargement->getVirtualItemCount()) {
            $limit = $this->tableauTelechargement->getVirtualItemCount() - $offset;
        }
        $this->setViewState('offSet', $offset);
        $this->setViewState('limit', $limit);
        $this->remplirListeTelechargement(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), $limit, $offset, $this->_arrayTriTelech);
    }

    public function rafraichirListeTelechargement($sender, $param)
    {
        $this->remplirDataSourceTelechargement();
        $this->panelMoreThanOneElementFoundTelechargement->render($param->getNewWriter());
        $this->panelNoElementFoundTelechargement->render($param->getNewWriter());
        $this->DefaultButtonTopTelechargement->render($param->getNewWriter());
        $this->DefaultButtonBottomTelechargement->render($param->getNewWriter());
    }

    public function onCallBackRefreshRepeaterTelechargement($sender, $param)
    {
        $this->panelMoreThanOneElementFoundTelechargement->render($param->NewWriter);
        $this->remplirDataSourceTelechargement();
    }

    public function remplirDataSourceTelechargement()
    {
        $this->setViewState('offSet', 0);
        $this->setViewState('limit', $this->tableauTelechargement->PageSize);
        $this->populateData();
        $this->numPageTopTelechargement->Text = $this->tableauTelechargement->CurrentPageIndex + 1;
        $this->numPageBottomTelechargement->Text = $this->tableauTelechargement->CurrentPageIndex + 1;
    }

    public function critereTriTelechargement($sender, $param)
    {
        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriDefaut', 'DESC');

        $sensTri = $this->getViewState('sensTri', 'DESC');

        if ($tri) {
            if ('ASC' == $sensTri) {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ('ASC' == $sensTriDefaut) {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTri', $sensTri);
        $this->setViewState('sensTriDefaut', $sensTriDefaut);
        $this->setViewState('tri', $tri);
        $this->setViewState('commandeName', $param->CommandName);

        $this->_arrayTriTelech['sensTri'] = $sensTri;
        $this->_arrayTriTelech['commandeName'] = $param->CommandName;
    }

    public function TrierTelechargement($sender, $param)
    {
        $this->remplirDataSourceTelechargement();
        $this->panelMoreThanOneElementFoundTelechargement->render($param->NewWriter);
    }

    //---------------------------------------------------------------- debut poser question ------------------------------------------------------------------------
    public function remplirListeQuestion($organisme, $limit = null, $offset = null, $arrayTri = [])
    {
        $nombreElementTotal = Atexo_Consultation_Register::retrieveQuestionByIdInscritAndRefCons(
            $this->_consultation->getId(),
            Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']),
            Atexo_CurrentUser::getIdEntreprise(),
            Atexo_CurrentUser::getIdInscrit(),
            true
        );
        if ($nombreElementTotal >= 1) {
            $listeQuestion = Atexo_Consultation_Register::retrieveQuestionByIdInscritAndRefCons(
                $this->_consultation->getId(),
                Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']),
                Atexo_CurrentUser::getIdEntreprise(),
                Atexo_CurrentUser::getIdInscrit(),
                false,
                $limit,
                $offset,
                $arrayTri
            );
            $this->panelMoreThanOneElementFoundQuestion->setStyle('display:block');
            $this->panelNoElementFoundQuestion->setStyle('display:none');
            $this->nombreElementQuestion->Text = $nombreElementTotal;

            $this->PagerBottomQuestion->setVisible(true);
            $this->PagerTopQuestion->setVisible(true);

            $this->setViewState('nombreElement', $nombreElementTotal);
            $this->setViewState('listeQuestions', $listeQuestion);

            $this->nombrePageTopQuestion->Text = ceil($nombreElementTotal / $this->tableauQuestion->PageSize);
            $this->nombrePageBottomQuestion->Text = ceil($nombreElementTotal / $this->tableauQuestion->PageSize);

            $this->tableauQuestion->setVirtualItemCount($nombreElementTotal);
            $this->tableauQuestion->dataSource = $listeQuestion;
            $this->tableauQuestion->dataBind();
        } else {
            $this->panelMoreThanOneElementFoundQuestion->setStyle('display:none');
            $this->panelNoElementFoundQuestion->setStyle('display:block');

            $this->PagerBottomQuestion->setVisible(false);
            $this->PagerTopQuestion->setVisible(false);
        }
    }

    public function pageChangedQuestion($sender, $param)
    {
        $this->tableauQuestion->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageTopQuestion->Text = $param->NewPageIndex + 1;
        $this->numPageBottomQuestion->Text = $param->NewPageIndex + 1;
        $this->populateDataQuestions();
    }

    public function goToPageQuestion($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTopQuestion':
                $numPage = $this->numPageTopQuestion->Text;
                break;
            case 'DefaultButtonBottomQuestion':
                $numPage = $this->numPageBottomQuestion->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTopQuestion->Text) {
                $numPage = $this->nombrePageTopQuestion->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauQuestion->CurrentPageIndex = $numPage - 1;
            $this->numPageBottomQuestion->Text = $numPage;
            $this->numPageTopQuestion->Text = $numPage;
            $this->populateDataQuestions();
        } else {
            $this->numPageTopQuestion->Text = $this->tableauQuestion->CurrentPageIndex + 1;
            $this->numPageBottomQuestion->Text = $this->tableauQuestion->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenghtQuestion($sender, $param)
    {
        switch ($sender->ID) {
            case 'listePageSizeTopQuestion':
                $pageSize = $this->listePageSizeTopQuestion->getSelectedValue();
                $this->listePageSizeBottomQuestion->setSelectedValue($pageSize);
                break;
            case 'listePageSizeBottomQuestion':
                $pageSize = $this->listePageSizeBottomQuestion->getSelectedValue();
                $this->listePageSizeTopQuestion->setSelectedValue($pageSize);
                break;
        }

        $this->tableauQuestion->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTopQuestion->Text = ceil($nombreElement / $this->tableauQuestion->PageSize);
        $this->nombrePageBottomQuestion->Text = ceil($nombreElement / $this->tableauQuestion->PageSize);
        $this->tableauQuestion->setCurrentPageIndex(0);
        $this->populateDataQuestions();
    }

    public function populateDataQuestions()
    {
        $offset = $this->tableauQuestion->CurrentPageIndex * $this->tableauQuestion->PageSize;
        $limit = $this->tableauQuestion->PageSize;
        if ($offset + $limit > $this->tableauQuestion->getVirtualItemCount()) {
            $limit = $this->tableauQuestion->getVirtualItemCount() - $offset;
        }
        $this->setViewState('offSet', $offset);
        $this->setViewState('limit', $limit);
        $this->remplirListeQuestion(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), $limit, $offset, $this->_arrayTriQuestion);
    }

    public function rafraichirListeQuestion($sender, $param)
    {
        $this->remplirDataSourceQuestion();
        $this->panelMoreThanOneElementFoundQuestion->render($param->getNewWriter());
        $this->panelNoElementFoundQuestion->render($param->getNewWriter());
        $this->DefaultButtonBottomQuestion->render($param->getNewWriter());
        $this->DefaultButtonTopQuestion->render($param->getNewWriter());
    }

    public function remplirDataSourceQuestion()
    {
        $this->setViewState('offSet', 0);
        $this->setViewState('limit', $this->tableauQuestion->PageSize);
        $this->populateDataQuestions();
        $this->numPageTopQuestion->Text = $this->tableauQuestion->CurrentPageIndex + 1;
        $this->numPageBottomQuestion->Text = $this->tableauQuestion->CurrentPageIndex + 1;
    }

    public function critereTriQuestions($sender, $param)
    {
        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriDefaut', 'DESC');

        $sensTri = $this->getViewState('sensTri', 'DESC');

        if ($tri) {
            if ('ASC' == $sensTri) {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ('ASC' == $sensTriDefaut) {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTri', $sensTri);
        $this->setViewState('sensTriDefaut', $sensTriDefaut);
        $this->setViewState('tri', $tri);
        $this->setViewState('commandeName', $param->CommandName);

        $this->_arrayTriQuestion['sensTri'] = $sensTri;
        $this->_arrayTriQuestion['commandeName'] = $param->CommandName;
    }

    public function TrierQuestions($sender, $param)
    {
        $this->remplirDataSourceQuestion();
        $this->panelMoreThanOneElementFoundQuestion->render($param->NewWriter);
    }

    public function getdateReponse($idQuestion, $org)
    {
        $relationEchange = (new Atexo_Message_RelationEchange())->getRelationEchangeByIdExterneAndTypeRelation($idQuestion, Atexo_Config::getParameter('TYPE_RELATION_REPONSE_QUESTION'), $org);
        if ($relationEchange[0] instanceof CommonRelationEchange) {
            return $relationEchange[0]->getDateEnvoi();
        }

        return '';
    }

    //---------------------------------------------------------------- Fin poser question ------------------------------------------------------------------------

    public function customizeTab()
    {
        $this->onglet1->setCssClass('tab-on');
        $this->onglet2->setCssClass('tab');
        $this->onglet3->setCssClass('tab');
        $this->onglet4->setCssClass('tab');
    }

    //-------------------------------------------------------------- Debut depot ---------------------------------------------------------------------------------

    /**
     * Remplissage de la liste des reponses deposees.
     *
     * @param  $organisme
     * @param  $refCons
     */
    public function remplirListeDepot($organisme, $refCons)
    {
        $listeFichiers = (new Atexo_Entreprise_Reponses())->getListeFichierDepot($organisme, $refCons, Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit());
        if (is_countable($listeFichiers) ? count($listeFichiers) : 0) {
            $this->idPanelAucunResultatDepotsOffres->visible = false;
        } else {
            $this->idPanelAucunResultatDepotsOffres->visible = true;
        }
        $this->idRepeaterDepotsOffres->DataSource = $listeFichiers;
        $this->idRepeaterDepotsOffres->dataBind();
    }

    public function downloadSignature($sender, $param)
    {
        $arrayParametre = explode('|', $param->CommandName);
        $content = base64_decode(trim($arrayParametre[1]));
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            date('Y-m-d H:i:s'),
            substr($_SERVER['QUERY_STRING'], 0),
            '-',
            date('Y-m-d H:i:s')
        );
        DownloadFile::downloadFileContent($arrayParametre[0].' - '.str_replace(':', '', str_replace(
            ' ',
            '',
            str_replace('-', '', $arrayParametre[2])
        )).Prado::localize('DEFINE_TEXT_SIGNATURE_1_XML'), $content);
        exit;
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function getDateDepotOffre($idOffre, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $offre = CommonOffresPeer::retrieveByPK($idOffre, $organisme, $connexion);
        if ($offre instanceof CommonOffres) {
            return Atexo_Util::iso2frnDateTime($offre->getUntrusteddate(), false, true);
        }
    }

    //-------------------------------------------------------------- Debut messagerie securisee ---------------------------------------------------------------------------------

    public function remplirListeMessagerie($organisme, $consultationId, $limit = null, $offset = null, $arrayTri = [])
    {
        if (Atexo_CurrentUser::getIdInscrit()) {
            $arrayMessages = [];
            $arrayTri['limit'] = $limit ?: '10';
            $arrayTri['offSet'] = $offset ?: '0';

            $nombreElementTotal = Atexo_Message::getAllEchangesDestinataireByIdRef($consultationId, $organisme, [], false, false, Atexo_CurrentUser::getIdInscrit(), true);

            if ($nombreElementTotal >= 1) {
                $listeMessages = Atexo_Message::getAllEchangesDestinataireByIdRef($consultationId, $organisme, $arrayTri, true, true, Atexo_CurrentUser::getIdInscrit(), true);

                foreach ($listeMessages as $destinataire) {
                    $PJ = '';
                    $Result = [];
                    //$objetEchange = $destinataire->getEchange();
                    if ($destinataire['FORMAT'] != Atexo_Config::getParameter('ECHANGE_PLATE_FORME')) {
                        $Pjs = Atexo_Message::getPjByIdEchange($destinataire['ID_ECHANGE'], $organisme);
                        if (is_array($Pjs)) {
                            foreach ($Pjs as $pj) {
                                $Result[] = '<a href="?page=Agent.DownloadPjEchange&idPj='.$pj->getPiece().'">'.$pj->getNomFichier().'</a>';
                                $nomPjs[] = $pj->getNomFichier();
                            }
                        }
                        if (is_array($Result) && 0 != count($Result)) {
                            $PJ = '<br>'.implode('<br>', $Result);
                        }
                        if (is_array($nomPjs) && 0 != count($nomPjs)) {
                            $nomPJs = implode('\n', $nomPjs);
                        }
                    }
                    // echo "test kbe ". $destinataire['TYPE_AR']. "</br>";
                    if ($destinataire['TYPE_AR'] == Atexo_Config::getParameter('ID_TYPE_AR_DATE_HEURE')) {
                        $destinataire['TypeAccuse'] = Atexo_Util::iso2frnDateTime($destinataire['DATE_AR']);
                    } elseif ($destinataire['TYPE_AR'] == Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE')) {
                        $destinataire['TypeAccuse'] = $this->arrayTypeAR[Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE')];
                    } else {
                        $destinataire['TypeAccuse'] = '-';
                    }
                    if ($destinataire['FORMAT'] != Atexo_Config::getParameter('ECHANGE_PLATE_FORME')) {
                        $destinataire['PJ'] = $PJ;
                        $destinataire['nomPjs'] = $nomPJs;
                    } else {
                        $destinataire['PJ'] = '<br><a href="index.php?page=Entreprise.EntrepriseDetailEchangeMail&num_ar='.$destinataire['UID'].'&orgAcronyme='.$organisme.'">'.
                            Prado::localize('DEFINE_TEXT_VISUALISATION_MESSAGE').'</a>';
                    }
                    $destinataire['FormatText'] = $this->arrayFormat[$destinataire['FORMAT']];
                    $arrayMessages[] = $destinataire;
                }

                $this->panelMoreThanOneElementFoundMessagerie->setStyle('display:block');
                $this->panelNoElementFoundMessagerie->setStyle('display:none');
                $this->nombreElementMessagerie->Text = $nombreElementTotal;

                $this->PagerBottomMessagerie->setVisible(true);
                $this->PagerTopMessagerie->setVisible(true);

                $this->setViewState('nombreElement', $nombreElementTotal);
                $this->setViewState('listeMessages', $arrayMessages);

                $this->nombrePageTopMessagerie->Text = ceil($nombreElementTotal / $this->tableauMessagerie->PageSize);
                $this->nombrePageBottomMessagerie->Text = ceil($nombreElementTotal / $this->tableauMessagerie->PageSize);

                $this->tableauMessagerie->setVirtualItemCount($nombreElementTotal);
                $this->tableauMessagerie->dataSource = $arrayMessages;
                $this->tableauMessagerie->dataBind();
            } else {
                $this->panelMoreThanOneElementFoundMessagerie->setStyle('display:none');
                $this->panelNoElementFoundMessagerie->setStyle('display:block');

                $this->PagerBottomMessagerie->setVisible(false);
                $this->PagerTopMessagerie->setVisible(false);
            }
        }
    }

    public function pageChangedMessagerie($sender, $param)
    {
        $this->tableauMessagerie->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageTopMessagerie->Text = $param->NewPageIndex + 1;
        $this->numPageBottomMessagerie->Text = $param->NewPageIndex + 1;
        $this->populateDataMessages();
    }

    public function goToPageMessagerie($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTopMessagerie':
                $numPage = $this->numPageTopMessagerie->Text;
                break;
            case 'DefaultButtonBottomMessagerie':
                $numPage = $this->numPageBottomMessagerie->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTopMessagerie->Text) {
                $numPage = $this->nombrePageTopMessagerie->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauMessagerie->CurrentPageIndex = $numPage - 1;
            $this->numPageBottomMessagerie->Text = $numPage;
            $this->numPageTopMessagerie->Text = $numPage;
            $this->populateDataMessages();
        } else {
            $this->numPageTopMessagerie->Text = $this->tableauMessagerie->CurrentPageIndex + 1;
            $this->numPageBottomMessagerie->Text = $this->tableauMessagerie->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenghtMessagerie($sender, $param)
    {
        switch ($sender->ID) {
            case 'listePageSizeTopMessagerie':
                $pageSize = $this->listePageSizeTopMessagerie->getSelectedValue();
                $this->listePageSizeBottomMessagerie->setSelectedValue($pageSize);
                break;
            case 'listePageSizeBottomMessagerie':
                $pageSize = $this->listePageSizeBottomMessagerie->getSelectedValue();
                $this->listePageSizeTopMessagerie->setSelectedValue($pageSize);
                break;
        }

        $this->tableauMessagerie->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTopMessagerie->Text = ceil($nombreElement / $this->tableauMessagerie->PageSize);
        $this->nombrePageBottomMessagerie->Text = ceil($nombreElement / $this->tableauMessagerie->PageSize);
        $this->tableauMessagerie->setCurrentPageIndex(0);
        $this->populateDataMessages();
    }

    public function populateDataMessages()
    {
        $offset = $this->tableauMessagerie->CurrentPageIndex * $this->tableauMessagerie->PageSize;
        $limit = $this->tableauMessagerie->PageSize;
        if ($offset + $limit > $this->tableauMessagerie->getVirtualItemCount()) {
            $limit = $this->tableauMessagerie->getVirtualItemCount() - $offset;
        }
        $this->setViewState('offSet', $offset);
        $this->setViewState('limit', $limit);
        $this->remplirListeMessagerie(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities($_GET['id']), $limit, $offset, $this->_arrayTriMessagerie);
    }

    public function rafraichirListeMessagerie($sender, $param)
    {
        $this->remplirDataSourceMessagerie();
        $this->panelMoreThanOneElementFoundMessagerie->render($param->getNewWriter());
        $this->panelNoElementFoundMessagerie->render($param->getNewWriter());
        $this->DefaultButtonBottomMessagerie->render($param->getNewWriter());
        $this->DefaultButtonTopMessagerie->render($param->getNewWriter());
    }

    public function remplirDataSourceMessagerie()
    {
        $this->setViewState('offSet', 0);
        $this->setViewState('limit', $this->tableauMessagerie->PageSize);
        $this->populateDataMessages();
        $this->numPageTopMessagerie->Text = $this->tableauMessagerie->CurrentPageIndex + 1;
        $this->numPageBottomMessagerie->Text = $this->tableauMessagerie->CurrentPageIndex + 1;
    }

    public function critereTriMessagerie($sender, $param)
    {
        $tri = $param->CommandName;
        $sensTriDefaut = $this->getViewState('sensTriDefaut', 'DESC');

        $sensTri = $this->getViewState('sensTri', 'DESC');

        if ($tri) {
            if ('ASC' == $sensTri) {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ('ASC' == $sensTriDefaut) {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTri', $sensTri);
        $this->setViewState('sensTriDefaut', $sensTriDefaut);
        $this->setViewState('tri', $tri);
        $this->setViewState('commandeName', $param->CommandName);
        if ('expediteur' == $param->CommandName) {
            $this->_arrayTriMessagerie['De'] = $sensTri;
        }
        if ('date_heure_messagerie' == $param->CommandName) {
            $this->_arrayTriMessagerie['DATE'] = $sensTri;
        }
        if ('destinataire' == $param->CommandName) {
            $this->_arrayTriMessagerie['A'] = $sensTri;
        }
        $this->_arrayTriMessagerie['commandeName'] = $param->CommandName;
    }

    public function TrierMessagerie($sender, $param)
    {
        $this->remplirDataSourceMessagerie();
        $this->panelMoreThanOneElementFoundMessagerie->render($param->NewWriter);
    }

    public function getActiveTab()
    {
        if (isset($_GET['retraits'])) {
            return 1;
        } elseif (isset($_GET['questions'])) {
            return 2;
        } elseif (isset($_GET['depots'])) {
            return 3;
        } elseif (isset($_GET['echanges'])) {
            return 4;
        } else {
            return 1;
        }
    }

    public function getTitleBoutonReponseExpress()
    {
        if ('1' == $this->_consultation->getSignatureOffre()) {
            return Prado::localize('SIGNER_ET_ENVOYER_MA_REPONSE');
        } else {
            return Prado::localize('ENVOYER_MA_REPONSE');
        }
    }

    public function formaterNomsPiecesDce($piecesDce)
    {
        if (0 == strcmp($piecesDce, Prado::localize('DCE_INTEGRAL'))) {
            return $piecesDce;
        }
        $tableauDce = explode('#', $piecesDce);
        unset($tableauDce[0]);
        unset($tableauDce[count($tableauDce)]);

        return implode(' <b>|</b> ', $tableauDce);
    }

    public function getUrlAjoutPanier($reference, $organisme)
    {
        if (Atexo_CurrentUser::isConnected()) {
            return "javascript:popUpSetSize('index.php?page=Entreprise.popUpGestionPanier&id=".base64_encode($reference).'&orgAcronyme='.$organisme."&action=ajout','700px','80px','yes');";
        } else {
            return 'index.php?page=Entreprise.EntrepriseHome&goto='.urlencode('?page=Entreprise.EntrepriseAccueilAuthentifie');
        }
    }

    public function getUrlDetailsConsultation($consultationId, $orgAcronyme)
    {
        return Atexo_MpeSf::getUrlDetailsConsultation($consultationId, $orgAcronyme);
    }

    public function afficherMonPanier()
    {
        if (Atexo_Module::isEnabled('PanierEntreprise')) {
            $this->response->redirect('?page=Entreprise.EntrepriseAdvancedSearch&panierEntreprise');
        }
    }

    public function authenticateViaPPPSocleEntreprise($idExterne)
    {
        // Coquille vide car authentification captée par SF
    }

    /**
     * Permet de charger le tableau des demandes de complements.
     */
    public function chargerRepeaterDemandeComplements()
    {
        $consultationId = $this->getConsultation()->getId();
        $org = $this->getConsultation()->getOrganisme();
        $demandesComplements = (new Atexo_FormulaireSub_Dossier())->recupererDemandesComplementsOF($consultationId, $org, Atexo_CurrentUser::getIdInscrit());
        $this->idRepeaterDemandeComplements->DataSource = $demandesComplements;
        $this->idRepeaterDemandeComplements->DataBind();
    }

    /**
     * Permet de charger le tableau des demandes de complements deja transmis par l'entreprise.
     */
    public function chargerRepeaterDemandeComplementsTransmis()
    {
        $criteriaVo = new Atexo_FormulaireSub_CriteriaVo();
        $criteriaVo->setTypeReponseDossier(Atexo_Config::getParameter('TYPE_DOSSIER_DEMANDE_COMPLEMENT'));
        $criteriaVo->setIdInscrit(Atexo_CurrentUser::getIdInscrit());
        $criteriaVo->setStatutDemande(Atexo_Config::getParameter('STATUT_DEMANDE_COMPLEMENT_ENVOYEE'));
        $criteriaVo->setDateReference(date('Y-m-d H:i:s'));
        $criteriaVo->setControlWithDateRemettre(false);
        $criteriaVo->setRefCons($this->getConsultation()->getId());
        $criteriaVo->setOrganisme($this->getConsultation()->getOrganisme());
        $demandesComplements = (new Atexo_FormulaireSub_ComplementFormulaire())->recupererDemandesComplement($criteriaVo);
        $this->idRepeaterDemandeComplementsTransmis->DataSource = $demandesComplements;
        $this->idRepeaterDemandeComplementsTransmis->DataBind();
    }

    /*
     * Raffraichissement du bloc des dossiers
     */
    public function raffraichirBlocDemandesComplement($sender, $param)
    {
        $this->setViewState('consultation', $this->_consultation);
        $this->chargerRepeaterDemandeComplements();
        foreach ($this->idRepeaterDemandeComplements->getItems() as $item) {
            $item->idAtexoDemandeComplementsSub->actionsApresCallBack();
        }
        $this->blocDemandesComplement->render($param->getNewWriter());
    }

    /**
     * Permet de verifier si toutes les editions sont generees une reponse.
     *
     * @param $editionsGenerees: passes en reference, cette variable sera remplie a true si toutes editions deja generees, false sinon
     */
    public function verifierSiToutesEditionsReponseGenerees(&$dossiersValidees, &$editionsGenerees)
    {
        //Recuperer la reponse electronique formulaire
        $reponseElect = (new Atexo_FormulaireSub_Dossier())->recupererReponseElectFormulaire(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_CurrentUser::getIdInscrit());
        if ($reponseElect) {
            //Recuperer les dossiers de cette reponse formulaire
            $dossiers = (new Atexo_FormulaireSub_Dossier())->getDossierByIdReponseElectFormulaire($reponseElect->getIdReponseElecFormulaire(), Atexo_Config::getParameter('TYPE_DOSSIER_REPONSE_PRINCIPALE'));
            if (is_array($dossiers)) {
                if (!count($dossiers)) {
                    $dossiersValidees = false;

                    return;
                } else {
                    foreach ($dossiers as $unDossier) {
                        if ($unDossier instanceof CommonTDossierFormulaire) {
                            if ($unDossier->getStatutValidation() != Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE')) {
                                $dossiersValidees = false;

                                return;
                            }
                            //Recuperer les editions de ce dossier
                            $editions = (new Atexo_FormulaireSub_Dossier())->recupererEditionsFormulaire($unDossier->getIdDossierFormulaire());
                            if (is_array($editions) && count($editions)) {
                                foreach ($editions as $uneEdition) {
                                    $cas = (new Atexo_FormulaireSub_Edition())->gestionActionEdition($uneEdition, $unDossier);
                                    if ('cas3' != $cas) {
                                        $editionsGenerees = false;

                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $dossiersValidees = false;

            return;
        }
    }

    /**
     * Permet de verifier si la reponse contient au moins un dossier d'offres.
     *
     * @param bool $dossierOffreTrouve: passes en reference, cette variable sera remplie a true si toutes editions deja generees, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function verifierSiReponseContientUnDossierOffre(&$dossierOffreTrouve)
    {
        $reponseElect = (new Atexo_FormulaireSub_Dossier())->recupererReponseElectFormulaire(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_CurrentUser::getIdInscrit());
        if ($reponseElect) {
            $dossiers = (new Atexo_FormulaireSub_Dossier())->getDossierByIdReponseElectFormulaire($reponseElect->getIdReponseElecFormulaire(), Atexo_Config::getParameter('TYPE_DOSSIER_REPONSE_PRINCIPALE'), Atexo_Config::getParameter('TYPE_ENV_OFFRE'), 0);
            if (is_array($dossiers) && count($dossiers)) {
                $dossierOffreTrouve = true;
            }
        }
    }

    /**
     * Permet d'afficher un message d'erreur.
     *
     * @param $message: le message a afficher
     */
    public function afficherMessageErreur($message)
    {
        $this->mainPart->setVisible(true);
        $this->filArianeWithErrorMessage->setVisible(false);
        $this->errorPart->setVisible(true);
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($message);
    }

    /*
     * Permet de tracer l'acces de l'inscrit a la PF
     */
    public function traceOperationsInsrctis($IdDesciption, $dateDebAction, $details = '', $ref = '', $org = '', $description = null)
    {
        $arrayDonnees = [];
        $arrayDonnees['ref'] = $ref;
        $arrayDonnees['org'] = $org;
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        $arrayDonnees['logApplet'] = '';
        $arrayDonnees['description'] = empty($description) ? (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true) : $description;
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            '-',
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
    }

    /**
     * Permet de generer une edition SUB.
     */
    public function genererEdition($sender, $param)
    {
        $arrayParam = explode('_', $param->CallbackParameter);
        $placeFunctionCallBack = $arrayParam[0];
        $callFrom = $arrayParam[1];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($this->Page->getViewState('idEditionGeneration')) {
            $edition = CommonTEditionFormulairePeer::retrieveByPK($this->Page->getViewState('idEditionGeneration'), $connexion);
            //Generation de l'edition
            (new Atexo_FormulaireSub_Edition())->genererEdition($edition);
            //Chargement et raffraichissement
            $this->scriptJsPopInGenerationEdition->Text = '';
            $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($edition->getIdDossierFormulaire());
            if ($dossier instanceof CommonTDossierFormulaire) {
                if ('page' == $placeFunctionCallBack) {
                    if ('demandeComplement' == $callFrom) {
                        $this->Page->raffraichirBlocDemandesComplement($sender, $param);
                    } elseif ('formulairePreparation' == $callFrom) {
                        $this->listeFormulairesSub->raffraichirBlocDossiersFormulaire($sender, $param);
                    }
                } elseif ('parent' == $placeFunctionCallBack) {
                    //faire le traitement ici
                }
            }
        }
    }

    /**
     * Permet de gerer l'affichage du lien de telechargement du complement de la consultation.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function displayLinkDownlodComplement()
    {
        if ($this->_consultation instanceof CommonConsultation) {
            $complement = (new Atexo_Consultation_Complements())->getComplement($this->_consultation->getId(), $this->_consultation->getOrganisme());
            if ($complement instanceof CommonComplement && 0 != $complement->getComplement()) {
                $this->setTailleComplement($complement, $this->_consultation->getOrganisme());
                $this->_showDownloadComplement = true;
            } else {
                $this->_showDownloadComplement = false;
            }
        } else {
            $this->_showDownloadComplement = false;
        }
    }

    /**
     * Permet de telecharger le complement de la consultation.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function DownlodComplement()
    {
        if ($this->_consultation instanceof CommonConsultation) {
            $consultationId = $this->_consultation->getId();
            $organisme = $this->_consultation->getOrganisme();
            $complement = (new Atexo_Consultation_Complements())->getComplement($consultationId, $organisme);
            if ($complement instanceof CommonComplement) {
                $this->downloadFiles($complement->getComplement(), $complement->getNomFichier(), $organisme);
                exit;
            }
        }
    }

    /**
     * Permet de telecharger un avis de publicite de la consultation.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function DownloadAvis($sender, $param)
    {
        if ($this->_consultation instanceof CommonConsultation) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $avis = (new Atexo_Publicite_Avis())->retreiveAvisByIdAndRefCons($param->CommandParameter, $this->_consultation->getId(), $this->_consultation->getOrganisme(), $connexion);
            if ($avis instanceof CommonAVIS) {
                //Nom du fichier joint
                $nomFichier = $avis->getNomFichier();
                if (Atexo_Module::isEnabled('PubliciteOpoce')) {
                    //Début récuperation de l'identifiant de l'avis global
                    $avisPub = (new Atexo_Publicite_AvisPub())->getAvisByIdAvisPortailAndRefConsultation($avis->getId(), $this->_consultation->getId(), $this->_consultation->getOrganisme(), $avis->getType());
                    $idAvisPub = '';
                    if ($avisPub instanceof CommonAvisPub) {
                        $idAvisPub = $avisPub->getId();
                    }
                    if ($avis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED')) {
                        $nomFichier = 'Avis_TED_'.(new Atexo_Publicite_AvisPub())->getNomFichierAnnoncePresse($this->_consultation->getId(), $idAvisPub).'.pdf';
                    } elseif ($avis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT')) {
                        $nomFichier = 'Avis_presse_'.(new Atexo_Publicite_AvisPub())->getNomFichierAnnoncePresse($this->_consultation->getId(), $idAvisPub).'.pdf';
                    }
                }
                $this->downloadFiles($avis->getAvis(), $nomFichier, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            }
        }
    }

    /**
     * Permet de retourner le message selon le cas.
     *
     * @return string
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getMessageInscriptionBourseCotraitance()
    {
        $nomEse = null;
        if ($this->showBourse()) {
            if (0 != Atexo_CurrentUser::getIdEntreprise()) {
                $entreprise = CommonEntrepriseQuery::create()->filterById(Atexo_CurrentUser::getIdEntreprise())->findOne();
                if ($entreprise instanceof Entreprise) {
                    $nomEse = $entreprise->getNom();
                }
            }
            $groupement = Atexo_BourseCotraitance::getCasGroupement($_GET['id']);
            switch ($groupement['cas']) {
                case 'cas1':
                    return Prado::localize('DEFINE_AUCUNE_ESE_INSCRITE_BOURSE_COTRAITANCE_CONSULTATION');
                case 'cas2':
                    return '<strong class="m-l-1 m-r-1">'.$nomEse.' </strong>'.Prado::localize('DEFINE_SEUL_MON_ESE_INSCRITE_BOURSE_COTRAITANCE_CONSULTATION');
                case 'cas3':
                    $message = '<span title="'.Prado::localize('TEXT_SINSCRIRE_POUR_VISUALISER_DETAIL').'">
                                                <strong class="m-l-1">'.
                        $groupement['count'].' '.Prado::localize('DEFINE_ESE_INSCRITE').
                        '</strong>
                                            </span>';
                    if (Atexo_CurrentUser::isConnected()) {
                        $message .= '<div class="m-l-4"><strong class="m-r-1">'.$nomEse.' </strong>'.Prado::localize('DEFINE_MON_ESE_NON_INSCRITE_BOURSE_COTRAITANCE_CONSULTATION').'</div>';
                    }

                    return $message;
                case 'cas4':
                    if (Atexo_CurrentUser::isConnected()) {
                        $message = '<a data-toggle="tooltip" title="Voir le d&eacute;tail" href="?page=Entreprise.EntrepriseRechercheBourseCotraitance&id='.$_GET['id'].'&orgAcronyme='.$_GET['orgAcronyme'].'&GME">
                                                <strong class="m-l-1">'.
                            $groupement['count'].' '.Prado::localize('TEXT_ENTREPRISES_INSCRITES').
                            '</strong>
                                            </a>'.
                            '<span class="m-l-1">'.Prado::localize('DEFINE_INSCRITE_BOURSE_COTRAITANCE_Y_COMPRIS_MON_ESE').'</span>'.
                            '<strong class="m-l-1"> '.$nomEse.'</strong>';
                    } else {
                        $message = '<span title="'.Prado::localize('TEXT_SINSCRIRE_POUR_VISUALISER_DETAIL').'">
                                                            <strong class="m-l-1">'.
                            $groupement['count'].' '.Prado::localize('DEFINE_ESE_INSCRITE').
                            '</strong>
                                                        </span>';
                    }

                    return $message;
            }
        } else {
            return 'Module non actif';
        }
    }

    /**
     * Permet de retourner le nom du picto selon le cas.
     *
     * @return string
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getPictoBourseCotraitance()
    {
        if ($this->showBourse()) {
            $groupement = Atexo_BourseCotraitance::getCasGroupement($_GET['id']);
            switch ($groupement['cas']) {
                case 'cas1':
                    return 'icone-cotraitance-1.png';
                case 'cas2':
                    return 'icone-cotraitance-2.png';
                case 'cas3':
                    return 'icone-cotraitance-3.png';
                case 'cas4':
                    return 'icone-cotraitance-4.png';
            }
        } else {
            return 'icone-cotraitance-pas-actif.png';
        }
    }

    /**
     * Permet de se desinscrire de la bourse à la cotraitance d'une Consultation.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function unsubscribe($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        CommonTBourseCotraitanceQuery::create()->filterByReferenceConsultation($_GET['id'])->filterByIdEntreprise(Atexo_CurrentUser::getIdEntreprise())->delete($connexion);
        Atexo_BourseCotraitance::initCachedCasGroupemen($_GET['id'], Atexo_CurrentUser::getIdEntreprise());
        $this->response->redirect('?page=Entreprise.EntrepriseDetailsConsultation&id='.$_GET['id'].'&orgAcronyme='.$_GET['orgAcronyme'].'&action=succes');
    }

    public function isCurrentTab($index)
    {
        if (intval($index) === $this->currentTab) {
            return ' active ';
        }

        return '';
    }

    public function showBourse()
    {
        if (Atexo_Module::isEnabled('BourseCotraitance')) {
            return true;
        }

        return false;
    }

    /**
     * @return CommonConsultation
     */
    private function  getCriteriaConsultation(?int $idConsultation = null)
    {
        if (null === $idConsultation && null !== $_GET['id']) {
            $idConsultation = $_GET['id'];
        }

        if ($_GET['orgAcronyme'] && $idConsultation && $_GET['code']) {
            // Accès details consultation restreinte
            Atexo_CurrentUser::deleteFromSession('organisme');
            Atexo_CurrentUser::deleteFromSession('reference');
            Atexo_CurrentUser::deleteFromSession('codeAcces');
            $this->_consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($idConsultation), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            Atexo_CurrentUser::writeToSession('organisme', Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            Atexo_CurrentUser::writeToSession('reference', $this->_consultation->getReferenceUtilisateur());
            Atexo_CurrentUser::writeToSession('codeAcces', str_replace(" ", "+", $_GET['code']));
        }

        $critere = new Atexo_Consultation_CriteriaVo();
        //Acces aux details de la consultation depuis le menu "Panier entreprise" en cliquant sur les liens (
        //registre des retraits, questions, depots, messages): autoriser les consultations cloturees du panier
        if (Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['clotures'])
            && (isset($_GET['retraits']) || isset($_GET['questions']) || isset($_GET['depots']) || isset($_GET['echanges']))
            && (new Atexo_Consultation())->verifierConsultationDansPanierEntreprise(Atexo_Util::atexoHtmlEntities($idConsultation), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']))) {
            $critere->setAvecConsClotureesSansPoursuivreAffichage(true);
        }
        $this->_consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($idConsultation), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), true, $critere);

        return $this->_consultation;
    }

    /**
     * Remplissage de la liste des candidatures.
     *
     * @param  $organisme
     * @param  $refCons
     */
    public function remplirListeCandidatureCoTraitant($organisme, $refCons)
    {
        $listeCandidatures = Atexo_Dume_AtexoDume::getCandidaturseCoTraitantDumeOnline($refCons, $organisme, Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit(), 2, Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_VALIDE'));
        $this->idRepeaterCandidaturesCoTraitant->DataSource = $listeCandidatures;
        $this->idRepeaterCandidaturesCoTraitant->dataBind();
    }
}
