<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFormulaire;
use Application\Propel\Mpe\Om\BaseCommonConsultationFormulairePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/*
 * Created on 20 oct. 2011
 *
 * by BOUKATAYA Fatima Zahra
 */

class PopUpDetailQuestionnaireEntreprise extends MpeTPage
{
    public ?\Application\Propel\Mpe\CommonConsultationFormulaire $_formCons = null;
    public $_idReponse;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        $this->errorPart->setVisible(false);
        $this->panelMessageErreur->setVisible(false);
        $messageErreur = '';
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->_formCons = BaseCommonConsultationFormulairePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idFormCons']), $connexion);
        if ($this->_formCons instanceof CommonConsultationFormulaire) {
            if (Atexo_CurrentUser::getIdInscrit()) {
                $this->idEntrepriseConsultationSummary->setReference($this->_formCons->getConsultationId());
                $this->idEntrepriseConsultationSummary->setOrganisme($this->_formCons->getOrganisme());

                $commonConsultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_formCons->getConsultationId(), $this->_formCons->getOrganisme());
                if ($commonConsultation instanceof CommonConsultation) {
                    if (!$this->isPostBack) {
                        if (isset($_GET['idFormCons'])) {
                            $formReps = (new Atexo_FormConsultation())->retreiveReponseInscritformulaireCons(Atexo_Util::atexoHtmlEntities($_GET['idFormCons']), Atexo_CurrentUser::getIdInscrit());
                            if ($formReps) {
                                $this->_idReponse = $formReps->getId();
                            }
                        }
                    }
                } else {
                    $messageErreur = Prado::localize('TEXT_AUCUNE_CONSULTATION');
                }
            } else {
                $messageErreur = Prado::localize('NON_AUTORISER_DEPOT_REPONSE');
            }
        } else {
            $messageErreur = Prado::localize('NON_AUTORISER_DEPOT_REPONSE');
        }
        if ($messageErreur) {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($messageErreur);
            $this->container->setVisible(false);
        }
    }

    public function getDescriptionEnveloppe()
    {
        $descLot = '';
        if ($this->_formCons instanceof CommonConsultationFormulaire) {
            if ('0' != $this->_formCons->getLot()) {
                $descLot = (new Atexo_FormConsultation())->getDescriptionLot($this->_formCons->getLot(), $this->_formCons->getOrganisme(), $this->_formCons->getConsultationId()).'- ';
            }

            return $descLot.$this->_formCons->getLibelleTypeEnveloppe();
        }

        return $descLot;
    }
}
