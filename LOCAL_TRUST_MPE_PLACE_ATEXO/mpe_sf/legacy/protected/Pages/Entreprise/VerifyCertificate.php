<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * commentaires.
 *
 * @author Fatima Zahra BOUKATAYA <fatima.boukatayabi@atexo.com>
 * @copyright Atexo 2010
 */
class VerifyCertificate extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ('' != $_POST['strCertificat']) {
            $stringToResend = '';
            $liste = explode('#_', Atexo_Util::atexoHtmlEntities($_POST['strCertificat'])); // le séparateur doit être changer du cote de l'applet #_
            foreach ($liste as $un) {
                $lst = explode('#', $un);
                if (null != $lst[2]) {
                    $chaineVerification = (new Atexo_Crypto())->verifierCertificatDeSignature(trim(trim($lst[2])), false, 'XML', true);
                    if ($chaineVerification) {
                        $stringToResend .= $lst[0].'#'.$lst[1].'#'.$chaineVerification[0].'|'.$chaineVerification[1].'|'.$chaineVerification[2].'#&';
                    } else {
                        $stringToResend .= $lst[0].'#'.$lst[1].'#'.'ERR'.'#&';
                    }
                }
            }
            echo $stringToResend;
        } else {
            echo 'ERR';
        }
        exit;
    }
}
