<?php

namespace Application\Pages\Entreprise;

use App\Service\PlateformeVirtuelle\Context;
use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Permet de telecharger le DCE.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseFormulairePoserQuestion extends MpeTPage
{
    public $_reference;
    public $_parentPage;
    public $_organisme;
    private $_pays = null;
    public string $pageTitle = 'TITRE';

    public function onInit($param)
    {
        if ($this->isPostBack) {
            $this->pageTitle = 'TITRE_POSER_QUESTION';
        } else {
            $this->pageTitle = 'TITRE_FORMULAIRE_POSER_QUESTION';
        }
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $this->EntrepriseFormulaireDemande->setPostBack($this->isPostBack);
        if (!$this->isPostBack) {
            $this->EntrepriseFormulaireDemande->getCoordonnees();
        }
        $this->_pays = $this->EntrepriseFormulaireDemande->pays->SelectedValue;
        $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);

        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        if (!$consultation) {
            $this->EntrepriseFormulaireDemande->setVisible(false);
            $this->validateButton->setVisible(false);
            $this->annulerButtonQuestion->setVisible(false);
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_AUCUNE_CONSULTATION'));
        }
        if (!$this->isPostBack) {
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION9', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            $this->traceOperationsInsrctis($IdDesciption, $dateDebAction, '', $this->_reference, $this->_organisme);
        }
    }

    /**
     * Enregistre les donnees relatives a l'inscrit dans la table Question.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function saveInscritQuestion($sender, $param)
    {
        $pays = null;
        $acronymePays = null;
        $dateDebAction = date('Y-m-d H:i:s');
        $questionCommon = new CommonQuestionsDce();

        $questionCommon->setAdresse($this->EntrepriseFormulaireDemande->address->Text);
        $questionCommon->setAdresse2($this->EntrepriseFormulaireDemande->address2->Text);
        $questionCommon->setConsultationId($this->_reference);
        $questionCommon->setCp($this->EntrepriseFormulaireDemande->cp->Text);
        $questionCommon->setDateDepot(date('Y-m-d H:i:s'));
        $questionCommon->setEmail(trim($this->EntrepriseFormulaireDemande->email->Text));
        $questionCommon->setEntreprise($this->EntrepriseFormulaireDemande->raisonSocial->Text);
        $questionCommon->setFax($this->EntrepriseFormulaireDemande->fax->Text);
        $questionCommon->setVille($this->EntrepriseFormulaireDemande->ville->Text);
        $questionCommon->setTel($this->EntrepriseFormulaireDemande->tel->Text);
        $questionCommon->setNom($this->EntrepriseFormulaireDemande->nom->Text);
        $questionCommon->setPrenom($this->EntrepriseFormulaireDemande->prenom->Text);

        /** @var Context $pfvContextService */
        $pfvContextService = $this->getSfService(Context::class);
        $pfvId = $pfvContextService->getCurrentPlateformeVirtuelleId();
        $questionCommon->setPlateformeVirtuelleId($pfvId);

        if ($this->EntrepriseFormulaireDemande->inscriptionAuthentification->Checked && !Atexo_CurrentUser::getIdInscrit()) {
            $url = '?page=Entreprise.EntrepriseFormulairePoserQuestion&id='.Atexo_Util::atexoHtmlEntities($_GET['id']);
            $url .= '&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
            if (Atexo_Module::isEnabled('SocleExterneEntreprise')) {
                $this->response->redirect('index.php?page=Entreprise.EntrepriseAccueilAuthentifie&goto='.urlencode($url));
            } else {
                $this->response->redirect('index.php?page=Entreprise.EntrepriseHome&goto='.urlencode($url));
            }

            return;
        } elseif ($this->EntrepriseFormulaireDemande->france->checked) {
            $questionCommon->setAcronymePays(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
            if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                $questionCommon->setSiret($this->EntrepriseFormulaireDemande->RcVille->SelectedValue.Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$this->EntrepriseFormulaireDemande->RcNumero->Text);
            } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                $questionCommon->setSiret($this->EntrepriseFormulaireDemande->identifiantUnique->Text);
            } else {
                $questionCommon->setSiret($this->EntrepriseFormulaireDemande->siren->Text.$this->EntrepriseFormulaireDemande->siret->Text);
            }
        } else {
            if (null != $this->_pays) {
                $pays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveCountryById($this->_pays);
                $acronymePays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveAccronymeCountryById($this->_pays);
            }

            $questionCommon->setPays($pays);
            $questionCommon->setAcronymePays($acronymePays);
            $questionCommon->setIdentifiantNational($this->EntrepriseFormulaireDemande->idNational->Text);
        }
        $questionCommon->setIdInscrit(empty(Atexo_CurrentUser::getIdInscrit()) ? null : Atexo_CurrentUser::getIdInscrit());
        $questionCommon->setIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
        $questionCommon->setOrganisme($this->_organisme);

        $this->setViewState('Question', $questionCommon);
        $this->EntrepriseFormulaireDemande->setVisible(false);
        $this->validateButton->setVisible(false);
        $this->annulerButtonQuestion->setVisible(false);
        $this->enteteDossierConsultation->setVisible(false);
        $this->formulairePoserQuestion->setVisible(true);
        $this->formulairePoserQuestion->setQuestion($questionCommon);
        $this->errorPart->setVisible(false);
        $this->panelMessageErreur->setVisible(false);
        $this->panelMessageErreur->setMessage('');
        $details = 'Depot de la Question.';
        $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION10', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $this->traceOperationsInsrctis($IdDesciption, $dateDebAction, $details, $this->_reference, $this->_organisme);
    }

    /**
     * retourner a la page Detail de la consultation.
     */
    public function goToDetailConsultation($sender, $param)
    {
        $codeAccess = Atexo_CurrentUser::readFromSessionSf("contexte_authentification_" . $_GET['id'])['codeAcces']
            ? '&code=' . Atexo_CurrentUser::readFromSessionSf("contexte_authentification_" . $_GET['id'])['codeAcces']
            : '';
        $url = 'index.php?page=Entreprise.EntrepriseDetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']) . $codeAccess;
        if (isset($_GET['questions'])) {
            $url .= '&questions';
        }
        $this->response->redirect($url);
    }

    /*
     * Permet de tracer l'acces de l'inscrit a la PF
     */
    public function traceOperationsInsrctis($IdDesciption, $dateDebAction, $details = '', $ref = '', $org = '')
    {
        $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $arrayDonnees = [];
        $arrayDonnees['ref'] = $ref;
        $arrayDonnees['org'] = $org;
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        $arrayDonnees['logApplet'] = '';
        $arrayDonnees['description'] = $description;
        if (!$details) {
            $details = '-';
        }
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            $details,
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
    }
}
