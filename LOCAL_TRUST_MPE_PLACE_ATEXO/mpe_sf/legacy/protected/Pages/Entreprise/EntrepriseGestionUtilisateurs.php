<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_InscritHistorique;
use Prado\Prado;

/**
 * Page de gestion des utilisateurs.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseGestionUtilisateurs extends MpeTPage
{
    protected $critereTri = '';
    protected $triAscDesc = '';

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        if (!$this->IsPostBack) {
            $this->displyUtilisateurs();
        }
        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    /**
     * affiche la liste des utilisateurs d'une entreprise.
     */
    public function displyUtilisateurs()
    {
        $dataSource = (new Atexo_Entreprise_Inscrit())->retrieveInscritSansCurruenUser(Atexo_CurrentUser::getIdEntreprise());
        Atexo_CurrentUser::writeToSession('cachedUES', $dataSource);
        $this->listeUtilisateurs->DataSource = $dataSource;
        $this->listeUtilisateurs->DataBind();
    }

    /**
     * actions sur les utilisateurs.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function actionUser($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ('Delete' === $param->CommandName) {
            $inscrit = CommonInscritPeer::retrieveByPK($param->CommandParameter, $connexionCom);
            $nom = $inscrit->getPrenom().' '.$inscrit->getNom();
            $mail = $inscrit->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique($nom, $mail, Atexo_Config::getParameter('EVENEMENT_ADMIN_DELETE'));

            /**
             * Anonymisation
             */
            $idUser = $inscrit->getId();
            $idEntrepise = $inscrit->getEntrepriseId();
            $idEtablissement = $inscrit->getIdEtablissement();
            $idRandom = 1 + rand() * 1000;
            $inscritOldData = $inscrit;
            $inscrit->setBloque('1');
            $inscrit->setDeleted(true);
            $inscrit->setDeletedAt(new \DateTime());
            $inscrit->setLogin('login_' . $idUser);
            $inscrit->setMdp('mdp_' . $idUser);
            $inscrit->setTentativesMdp(666);
            $inscrit->setNom('nom_inscrit_' . $idUser);
            $inscrit->setPrenom('prenom_inscrit_' . $idUser);
            $inscrit->setAdresse('adresse_inscrit_' . $idUser);
            $inscrit->setEmail('inscrit_'
                . $idUser
                . '-'
                . $idEntrepise
                . '-'
                . $idEtablissement
                . $idRandom
                . "@atexo-anonymise.com");
            $inscrit->save($connexionCom);

            //Envoi mail de notification aux admins et a l'utilisateur supprime
            $msgCorpsPersonaliseUser = Prado::localize('DEFINE_CORPS_MAIL_SUPPRESSION_COMPTE_UTILISATEUR_TO_INSCRIT');
            $objet = Prado::localize('DEFINE_TITRE_MAIL_SUPPRESSION_COMPTE_UTILISATEUR');
            (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise($inscritOldData, $objet, $msgCorpsPersonaliseUser);
        } elseif ('Lock' === $param->CommandName) {
            $inscrit = CommonInscritPeer::retrieveByPK($param->CommandParameter, $connexionCom);
            if ($inscrit) {
                $inscrit->setBloque('0');
                if ($inscrit->save($connexionCom) >= 0) {
                    $nom = $inscrit->getPrenom().' '.$inscrit->getNom();
                    $mail = $inscrit->getEmail();
                    (new Atexo_Entreprise_InscritHistorique())->setHistorique($nom, $mail, Atexo_Config::getParameter('EVENEMENT_ADMIN_LOCK'));
                }
            }
        } elseif ('UnLock' === $param->CommandName) {
            $inscrit = CommonInscritPeer::retrieveByPK($param->CommandParameter, $connexionCom);
            if ($inscrit) {
                $inscrit->setBloque('1');
                if ($inscrit->save($connexionCom) >= 0) {
                    $nom = $inscrit->getPrenom().' '.$inscrit->getNom();
                    $mail = $inscrit->getEmail();
                    (new Atexo_Entreprise_InscritHistorique())->setHistorique($nom, $mail, Atexo_Config::getParameter('EVENEMENT_ADMIN_UNLOCK'));
                }
            }
        }
    }

    /**
     * Actualiser le tableau des utilisateurs dans le cas d'une nouvelle action.
     */
    public function refreshRepeater($sender, $param)
    {
        $dataSource = (new Atexo_Entreprise_Inscrit())->retrieveInscritSansCurruenUser(Atexo_CurrentUser::getIdEntreprise());
        $this->listeUtilisateurs->DataSource = $dataSource;
        $this->listeUtilisateurs->DataBind();
        $this->panelUsers->render($param->NewWriter);
    }

    /**
     * Trier le tableau des utlisateurs.
     *
     * @param sender
     * @param param
     */
    public function sortRepeater($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $cachedUES = Atexo_CurrentUser::readFromSession('cachedUES');

        if ('Nom' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedUES, 'Nom', '');
        } elseif ('Coordonnees' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedUES, 'AdresseEtablissement', '');
        } elseif ('CodeEtablissement' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedUES, 'CodeEtablissement', '');
        } else {
            return;
        }
        // Mise dans la session de la liste des invités triée
        Atexo_CurrentUser::deleteFromSession('cachedUES');
        Atexo_CurrentUser::writeToSession('cachedUES', $dataSourceTriee[0]);

        //Mise en viewState des critères de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        //Remplissage du tabeau des Rpas
        $this->listeUtilisateurs->DataSource = $dataSourceTriee[0];
        $this->listeUtilisateurs->DataBind();

        // Re-affichage du TActivePanel
        $this->panelUsers->render($param->getNewWriter());
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, $typeCritere);
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }
}
