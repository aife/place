<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\WebService\Atexo_WebService_InterfacesAnnonceMarchesHTTPS;

/*
 * Created on 30 août 2012
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class InterfaceAnnonceMarchesHTTPS extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($_POST['xml_request']) {
            $xmlAnnoncesMarches = base64_decode($_POST['xml_request']);
            $xmlreturn = (new Atexo_WebService_InterfacesAnnonceMarchesHTTPS())->processXml($xmlAnnoncesMarches);
            print_r($xmlreturn);
            exit;
        }
    }
}
