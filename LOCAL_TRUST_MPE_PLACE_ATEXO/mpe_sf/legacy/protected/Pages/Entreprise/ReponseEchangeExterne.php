<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Message\Atexo_Message_EchangeExterne;

/**
 * Classe ReponseEchangeExterne.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class ReponseEchangeExterne extends MpeTPage
{
    public string $url = '';

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $codeLien = Atexo_Util::atexoHtmlEntities($_GET['codelien']);
        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
        $idEntreprise = $inscrit->getEntrepriseId();
        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise);
        $echangeExterne = new Atexo_Message_EchangeExterne();
        $token = $echangeExterne->intialiseEchangeRep($codeLien, $inscrit, $entreprise);
        $url = Atexo_Config::getParameter('URL_MESSAGERIE_SECURISE_REDACTION').$token;
        $this->url = $url;
    }
}
