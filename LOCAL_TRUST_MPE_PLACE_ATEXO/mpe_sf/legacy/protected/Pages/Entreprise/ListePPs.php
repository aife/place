<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieINSEEPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Prado\Prado;

class ListePPs extends MpeTPage
{
    public $langue;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->tableauPP->setPostBack($this->IsPostBack);
        $this->tableauPP->panelListePPs->setDisplay('None');
        if (!$this->isPostBack) {
            $this->formRecherchePPs->setVisible(true);
            $this->listePPs->setVisible(false);
            $this->displayOrganismes();
            $this->fillClassification();
            $this->fillAnnee();
        }
    }

    public function displayOrganismes()
    {
        if (isset($_COOKIE['selectedorg'])) {
            $ArrayOrganismes = [];
            $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($this->langue);
            $ArrayOrganismesO = Atexo_Organismes::retrieveOrganismeByAcronyme($_COOKIE['selectedorg']);
            if (0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$ArrayOrganismesO->$getDenominationOrg()) {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->getDenominationOrg();
            } else {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->$getDenominationOrg();
            }
            $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($ArrayOrganismesO->getAcronyme(), null, $this->langue);
            $this->entityPurchaseNames->dataSource = $listEntityPurchase;
            $this->entityPurchaseNames->DataBind();
        } else {
            $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $this->langue);
            array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');
        }

        $this->organismesNames->dataSource = $ArrayOrganismes;
        $this->organismesNames->DataBind();
    }

    public function displayEntityPurchase($sender, $param)
    {
        // script pour afficher liste des services
        if ('0' != $this->organismesNames->SelectedValue) {
            $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = '';</script>";
            $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($this->organismesNames->SelectedValue, true, $this->langue);
            $this->entityPurchaseNames->dataSource = $listEntityPurchase;
            $this->entityPurchaseNames->DataBind();
        } else {
            $this->initialiserEntityPurchase();
        }
        $this->panelSearchInPP->render($param->NewWriter);
    }

    public function onSearchClick()
    {
        $this->tableauPP->panelListePPs->setDisplay('Dynamic');
        $this->tableauPP->setClassification($this->classification->getSelectedValue());
        $this->tableauPP->setOrganisme($this->organismesNames->getSelectedValue());
        $this->tableauPP->setServiceId($this->entityPurchaseNames->getSelectedValue());
        $this->tableauPP->setAnnee($this->anneePP->getSelectedValue());

        $this->tableauPP->displayProgrammePrevisionnel();
        $this->listePPs->setVisible(true);
    }

    public function displaySearchPPs()
    {
        $this->Page->formRecherchePPs->setVisible(true);
        $this->Page->listePPs->setVisible(false);
        $this->response->redirect('?page=Entreprise.ListePPs');
    }

    /*
     * replis la liste des classification
     */
    public function fillClassification()
    {
        $criteria = new Criteria();
        $codesInsee = CommonCategorieINSEEPeer::doSelect($criteria, Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));
        $codesInsee = is_array($codesInsee) ? $codesInsee : [];
        $dataSource = ['0' => '--- '.Prado::localize('TOUTES_LES_CLASSIFICATIONS').' ---'];
        foreach ($codesInsee as $insee) {
            $dataSource[$insee->getId()] = $insee->getLibelle();
        }
        $this->classification->setDataSource($dataSource);
        $this->classification->DataBind();
    }

    public function choixClassification($sender, $param)
    {
        $idClassification = $this->classification->getSelectedValue();
        $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $this->langue, true, $idClassification);
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');

        $this->organismesNames->dataSource = $ArrayOrganismes;
        $this->organismesNames->DataBind();

        $this->initialiserEntityPurchase();

        $this->panelSearchInPP->render($param->NewWriter);
    }

    public function fillAnnee()
    {
        $listesAnnees = [];
        $listesAnnees[0] = '---'.Prado::localize('TEXT_SELECTIONNER').'---';
        $listesAnnees[date('Y') - 1] = date('Y') - 1;
        $listesAnnees[date('Y')] = date('Y');
        $listesAnnees[date('Y') + 1] = date('Y') + 1;
        $this->anneePP->dataSource = $listesAnnees;
        $this->anneePP->dataBind();
    }

    /**
     * Permet d'effacer les critaires de la recherche.
     *
     * @param TActiveButton          $sender
     * @param TCommandEventParameter $param
     *
     * @author LEZ <loubna.ezziani@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function cleareCritaire($sender, $param)
    {
        $this->classification->setSelectedValue(0);
        $this->organismesNames->setSelectedValue(0);
        $this->initialiserEntityPurchase();
        $this->anneePP->setSelectedValue(0);
    }

    /**
     * Permet rafraichir le panel de la recherche.
     *
     * @param TActiveButton          $sender
     * @param TCommandEventParameter $param
     *
     * @author LEZ <loubna.ezziani@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function refreshCritaire($sender, $param)
    {
        $this->panelSearchInPP->render($param->getNewWriter());
    }

    /**
     * Permet de chacher et d'initialiser la liste des entitypurchaseNames.
     *
     * @author LEZ <loubna.ezziani@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function initialiserEntityPurchase()
    {
        $this->entityPurchaseNames->dataSource = [];
        $this->entityPurchaseNames->DataBind();
        $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = 'none';</script>";
    }
}
