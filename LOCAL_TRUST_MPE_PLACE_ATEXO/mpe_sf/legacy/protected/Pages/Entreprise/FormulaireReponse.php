<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_ActeDEngagement;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_CategorieLot;
use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class FormulaireReponse extends MpeTPage
{
    public ?string $_avecChiffrement = null;
    public ?string $_avecSignature = null;
    public $_uid;
    public ?bool $_acteEngagement = null;
    public bool $_variantesAutorisees = false;
    public $_consultation = null;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $uid = null;
        $dateDebAction = date('Y-m-d H:i:s');
        $this->errorPart->setVisible(false);
        $this->panelMessageErreur->setVisible(false);
        $this->confirmationPart->setVisible(false);
        $messageErreur = '';
        if (Atexo_CurrentUser::getIdInscrit()) {
            $this->detailConsultation->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->detailConsultation->setOrganisme(Atexo_Util::atexoHtmlEntities($_GET['org']));
            if (!$this->IsPostBack) {
                $uid = uniqid(random_int(0, mt_getrandmax()), true);
                $this->setViewState('uid', $uid);
            }

            $commonConsultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']));
            $this->_consultation = $commonConsultation;
            if ($commonConsultation instanceof CommonConsultation &&
                   (Atexo_Module::isEnabled('EntrepriseRepondreConsultationApresCloture') || Atexo_Util::cmpIsoDateTime(date('Y-m-d H:i:s'), $commonConsultation->getDatefin()) <= 0)) {
                $this->alloti->value = $commonConsultation->getAlloti();
                $allotisement = $commonConsultation->getAlloti();
                $varianteCalcule = '0';
                $donnesComplementaitre = $commonConsultation->getDonneComplementaire();
                if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
                    $varianteCalcule = $donnesComplementaitre->getVarianteCalcule();
                }
                if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Module::isEnabled('DonneesComplementaires', Atexo_Util::atexoHtmlEntities($_GET['org'])) && '1' == $varianteCalcule) {
                    $this->_variantesAutorisees = true;
                    $this->panelVariante->setDisplay('Dynamic');
                    if (isset($_GET['var'])) {
                        $this->offreVariante->checked = true;
                    } else {
                        $this->offreBase->checked = true;
                    }
                } else {
                    //$this->offreBase->checked=true;
                    $this->panelVariante->setDisplay('None');
                }
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $c = new Criteria();
                $c->add(CommonConsultationPeer::ID, $commonConsultation->getId());
                $c->add(CommonConsultationPeer::ORGANISME, Atexo_Util::atexoHtmlEntities($_GET['org']));
                $consultation = CommonConsultationPeer::doSelectOne($c, $connexionCom);

                $this->_acteEngagement = ('1' == $consultation->getSignatureActeEngagement());
                $this->setViewState('refConsultation', $consultation->getId());
                $this->setViewState('organisme', $commonConsultation->getOrganisme());
                $this->setViewState('acteEngagement', $consultation->getSignatureActeEngagement());
                $this->setViewState('serviceId', $consultation->getServiceId());

                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritAndEntreprise(Atexo_CurrentUser::getIdInscrit());
                if (is_array($inscrit) && 1 == count($inscrit)) {
                    $inscrit = array_shift($inscrit);
                    if ($inscrit instanceof CommonInscrit && ($inscrit->getEntreprise() instanceof Entreprise)) {
                        Prado::log('Acces page reponse: Ref: {'.Atexo_Util::atexoHtmlEntities($_GET['id'])."} [{$commonConsultation->getReferenceutilisateur()}] - Org: {".
                                       Atexo_Util::atexoHtmlEntities($_GET['org']).'} - Id enteprise: '.Atexo_CurrentUser::getIdEntreprise().' - Id Inscrit: '.Atexo_CurrentUser::getIdInscrit()
                                       .' INFOS: '.gethostbyaddr($_SERVER['REMOTE_ADDR']).
                                       " - {$inscrit->getPrenom()} {$inscrit->getNom()} - {$inscrit->getEmail()} ({$inscrit->getEntreprise()->getNom()}) - UID: $uid", TLogger::INFO, 'Atexo.pages.entreprise.FormulaireReponse.php');
                        if (!$this->IsPostBack) {
                            $this->tempRestant->Text = Atexo_Util::diffDatesMoisJoursHeuresMinutes(date('Y-m-d H:i'), $commonConsultation->getDatefin());
                            $this->nomEntreprise->Text = $inscrit->getEntreprise()->getNom();
                            $this->nomInscrit->Text = $inscrit->getNom();
                            $this->prenomInscrit->Text = $inscrit->getPrenom();
                            $this->mailInscrit->Text = $inscrit->getEmail();
                            $this->adresseAR->Text = $inscrit->getEmail();
                        }

                        $this->setViewState('nomEntreprise', $inscrit->getEntreprise()->getNom());
                        $this->setViewState('nomInscrit', $inscrit->getNom());
                        $this->setViewState('prenomInscrit', $inscrit->getPrenom());

                        $certificatChifrement = [];

                        if ('0' != $consultation->getEnvCandidature()) {
                            $this->panelCandidature->setVisible(true);
                            $certificatChifrement = (new Atexo_Certificat())->retrieveCertificatChiffrement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), Atexo_Util::atexoHtmlEntities($_GET['org']));
                            if (is_array($certificatChifrement)) {
                                $this->repeaterCertificatCandidature->DataSource = $certificatChifrement;
                                $this->repeaterCertificatCandidature->DataBind();
                            } else {
                                throw new Atexo_Exception('Expected function to return an array');
                            }
                        } else {
                            $this->panelCandidature->setVisible(false);
                        }

                        if ($allotisement
                           && ('0' != $consultation->getEnvOffre() || '0' != $consultation->getEnvAnonymat() || '0' != $consultation->getEnvOffreTechnique())) {
                            $lots = $consultation->getAllLots();
                            if (is_array($lots) && count($lots) >= 1) {
                                $this->RepeaterLot->DataSource = $lots;
                                $this->RepeaterLot->DataBind();
                            }
                        }

                        if ('0' != $consultation->getEnvOffre()) {
                            if (!$allotisement) {
                                $this->panelOffre->setVisible(true);
                            } else {
                                $this->panelOffre->setVisible(false);
                            }
                            $certificatChifrement = (new Atexo_Certificat())->retrieveCertificatChiffrement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('TYPE_ENV_OFFRE'), Atexo_Util::atexoHtmlEntities($_GET['org']));
                            $this->repeaterCertificatOffre->DataSource = $certificatChifrement;
                            $this->repeaterCertificatOffre->DataBind();
                        } else {
                            $this->panelOffre->setVisible(false);
                        }

                        if ('0' != $consultation->getEnvAnonymat()) {
                            $certificatChifrement = (new Atexo_Certificat())->retrieveCertificatChiffrement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'), Atexo_Util::atexoHtmlEntities($_GET['org']));
                            $this->repeaterCertificatAnonymat->DataSource = $certificatChifrement;
                            $this->repeaterCertificatAnonymat->DataBind();
                            if (!$allotisement) {
                                $this->panelAnonymat->setVisible(true);
                            } else {
                                $this->panelAnonymat->setVisible(false);
                            }
                        } else {
                            $this->panelAnonymat->setVisible(false);
                        }

                        if ('0' != $consultation->getEnvOffreTechnique()) {
                            if (!$allotisement) {
                                $this->panelOffreTechnique->setVisible(true);
                            } else {
                                $this->panelOffreTechnique->setVisible(false);
                            }
                            $certificatChifrement = (new Atexo_Certificat())->retrieveCertificatChiffrement(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'), Atexo_Util::atexoHtmlEntities($_GET['org']));
                            $this->repeaterCertificatOffreTechnique->DataSource = $certificatChifrement;
                            $this->repeaterCertificatOffreTechnique->DataBind();
                        } else {
                            $this->panelOffreTechnique->setVisible(false);
                        }

                        if (1 == $consultation->getSignatureOffre()) {
                            $this->signatureElectroniqueRequise->Text = Prado::Localize('OUI_SINATURE_SUR_MON_POSTE');
                            $this->_avecSignature = '1';
                        } else {
                            $this->signatureElectroniqueRequise->Text = Prado::Localize('DEFINE_NON');
                            $this->_avecSignature = '0';
                        }

                        if (1 == $consultation->getChiffrementOffre()) {
                            $this->chiffremetPlis->Text = Prado::Localize('OUI_SUR_MON_POSTE');
                            $this->_avecChiffrement = '1';
                        } else {
                            $this->chiffremetPlis->Text = Prado::Localize('DEFINE_NON');
                            $this->_avecChiffrement = '0';
                        }

                        if (Atexo_Module::isEnabled('AutoriserUneSeuleReponsePrincipaleParEntreprise')) {
                            if (('1' == $commonConsultation->getVarianteCalcule() && !isset($_GET['var'])) || '0' == $commonConsultation->getVarianteCalcule()) {
                                if ('0' == $commonConsultation->getVarianteCalcule()) {
                                    $reponsesEntreprise = (new Atexo_Consultation_Responses())->retrieveReponsesEntreprise(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_CurrentUser::getIdInscrit());
                                    if (is_array($reponsesEntreprise) && !empty($reponsesEntreprise)) {
                                        $messageErreur = Prado::localize('DEPOT_PLUSIERS_REPONSES_INTERDIT');
                                    }
                                } else {
                                    $reponsesEntreprise = (new Atexo_Consultation_Responses())->retrieveReponsesDeBaseEntreprise(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_CurrentUser::getIdInscrit());
                                    if (is_array($reponsesEntreprise) && !empty($reponsesEntreprise)) {
                                        $this->redirectToFormulaireVariante();
                                    }
                                }
                            } elseif ('1' == $commonConsultation->getVarianteCalcule() && isset($_GET['var'])) {
                                $reponsesEntreprise = (new Atexo_Consultation_Responses())->retrieveReponsesDeBaseEntreprise(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_CurrentUser::getIdInscrit());
                                if (is_array($reponsesEntreprise) && !empty($reponsesEntreprise)) {
                                    $this->msgErreur->setVisible(true);
                                    $this->msgErreur->setMessage(Prado::localize('DEPOT_PLUSIERS_REPONSES_INTERDIT_VARIANTES_OK'));
                                    $this->offreVariante->checked = true;
                                    $this->offreBase->checked = false;
                                    $this->offreBase->setEnabled(false);
                                }
                            }
                        }
                    } else {
                        $messageErreur = Prado::localize('NON_AUTORISER_DEPOT_REPONSE');
                    }
                } else {
                    $messageErreur = Prado::localize('NON_AUTORISER_DEPOT_REPONSE');
                }
            } else {
                $messageErreur = Prado::localize('TEXT_AUCUNE_CONSULTATION');
            }
        } else {
            $messageErreur = Prado::localize('NON_AUTORISER_DEPOT_REPONSE');
        }
        if ($messageErreur) {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($messageErreur);
            $this->mainPart->setVisible(false);
        }

        if ((isset($_POST['xmlString']) && $_POST['xmlString'])) { /////////
            $this->detailConsultationConfirmation->setReference(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->detailConsultationConfirmation->setOrganisme(Atexo_Util::atexoHtmlEntities($_GET['org']));
            $this->errorPart->setVisible(false);
            $this->panelMessageErreur->setVisible(false);
            $this->mainPart->setVisible(false);
            $this->confirmationPart->setVisible(true);
        }
        $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION12', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $this->traceOperationsInsrctis($IdDesciption, $dateDebAction, '', Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']));
    }

    public function reponseReceived($sender, $param)
    {
        $params = [];
        //Ajout de la consultation au panier de l'entreprise
        if (Atexo_Module::isEnabled('PanierEntreprise')) {
            if (!Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise(
                Atexo_Util::atexoHtmlEntities($_GET['org']),
                Atexo_Util::atexoHtmlEntities($_GET['id']),
                Atexo_CurrentUser::getIdEntreprise(),
                Atexo_CurrentUser::getIdInscrit()
            )) {
                Atexo_Entreprise_PaniersEntreprises::AddConsultationToPanierEntreprise(
                    Atexo_Util::atexoHtmlEntities($_GET['org']),
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    Atexo_CurrentUser::getIdEntreprise(),
                    Atexo_CurrentUser::getIdInscrit()
                );
            }
        }
        $dateDebAction = date('Y-m-d H:i:s');
        try {
            $objetConsultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), false);
            if ($objetConsultation instanceof CommonConsultation) {
                $offreVariante = null;
                $varianteCalcule = '0';
                $donnesComplementaitre = $objetConsultation->getDonneComplementaire();
                if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
                    $varianteCalcule = $donnesComplementaitre->getVarianteCalcule();
                }
                if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Module::isEnabled('DonneesComplementaires', $_GET['org']) && '1' == $varianteCalcule) {
                    if ($this->offreVariante->checked) {
                        $offreVariante = '1';
                    } else {
                        $offreVariante = '0';
                    }
                }
                $arrayReturned = (new Atexo_Entreprise_Reponses())->reponseReceieved($_POST['xmlString'], $this->getViewState('uid'), Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), $this->adresseAR->Text, Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit(), $offreVariante, false, false, Atexo_CurrentUser::getIdEtablissement());
                //affichage des messages de confirmation de reception
                //$dateHeuresDepot=explode(" ", Atexo_Util::iso2frnDateTime($arrayReturned['dateDepot']));
                /*$messageConfirmation="";
                $messageConfirmation.='<div class="line"><div class="intitule-auto">'.Prado::localize('REPONSE_BIEN_ENREGISTRE').' : </div> </div>';
                $messageConfirmation.='<div class="line"><div class="intitule-auto">'.Prado::localize('HORODATAGE_DEPOT').' : </div> <span id="horodatage">'.
                 Atexo_Util::iso2frnDateTime($arrayReturned['dateDepot']) .'</span> </div>';

                $messageConfirmation.='<div class="line float-left"><div class="intitule-auto">'.Prado::localize('CONTENU_TRANSMIS'). ' : </div></div>';
                $messageConfirmation.='<div class="line float-left"><div class="intitule-auto"> <p><span id="fichiersDeposes">'.
                Atexo_Entreprise_Reponses::getNomsFichiersFromArray($arrayReturned,$this->getViewState('acteEngagement') ,'<br />',true ,Atexo_Util::atexoHtmlEntities($_GET['org'])) .
                '</span></p></div></div>';
                $messageConfirmation.='<div class="spacer-small"></div>';
                $messageConfirmation.='<div class="breaker"></div>';
                $this->panelMessage->setMessage($messageConfirmation);*/

                //----------Integration nouvelle maquette de reponse---------
                $this->panelConfirmation->labelHorodatage->Text = Atexo_Util::iso2frnDateTime($arrayReturned['dateDepot'], false, true);
                $detailReponse = (new Atexo_Entreprise_Reponses())->getNomsFichiersFromArray($arrayReturned, $this->getViewState('acteEngagement'), '<br />', true, Atexo_Util::atexoHtmlEntities($_GET['org']));

                $detail = 'Response files :';
                foreach ($detailReponse as $one) {
                    $detail .= $one['nomFichier'].'/ ';
                }
                $details = 'ref utilistauer consultation :'.$objetConsultation->getReferenceUtilisateur().': '.$detail;
                $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION14', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);

                $this->traceOperationsInsrctis($IdDesciption, $dateDebAction, $details, Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']));

                $this->panelConfirmation->repeaterConfirmationDepotPli->DataSource = $detailReponse;
                $this->panelConfirmation->repeaterConfirmationDepotPli->DataBind();
                //----------------Fin d'integration-------------------------

                //envoi du mail de notification aux agent concerne
                $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']));
                $listeIdsAgents = (new Atexo_Consultation_Guests())->getIdsAgentFromArrayGuestsVo($guests);
                $agents = (new Atexo_Agent())->retrieveAgentsAvecAlerteReponseElectronique($listeIdsAgents);
                //Determination de la reference utilisateur de la consultation

                $params['reponseReceived'] = true;

                $listeAgents = Atexo_Consultation_Guests::listAgentConsultationFavoris(Atexo_Util::atexoHtmlEntities($_GET['id']), $params);
                $refUtilisateur = $objetConsultation->getReferenceUtilisateur();

	    		if($agents && is_array($agents)) {
	    			//Selection de la langue de la plateforme pour l'envoie du mail a l'agent de la plateforme
					$globalization = Prado::getApplication()->Modules["globalization"];
					$globalization->setTranslationCatalogue("messages.".Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'));
					//Recuperation du corps du mail
	    			$corpsMail=(new Atexo_Entreprise_Reponses())->getCorpsMessageReponseRecuePourAgent(Atexo_Util::atexoHtmlEntities($_GET['id']),
	    						Atexo_Util::atexoHtmlEntities($_GET['org']), $arrayReturned);
	    			foreach($agents as $agent) {
	    				if(
                            $agent->getEmail()
                            && in_array($agent->getId(), $listeAgents) !== false
                        ) {
	    					Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $agent->getEmail(),
	    					Prado::localize('RECEPTION_REPONSE_ELECRONIQUE').' - '.Prado::localize('TEXT_REF').' : '.$refUtilisateur, $corpsMail, $cc = "", $return_path = "");
	    				}
	    			}
	    			$this->onInit();
	    		}
	    		$corpsMail=Atexo_Entreprise_Reponses::getCorpsMessageReponseRecuePourEntreprise(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']),
			    			$arrayReturned, $this->getViewState('nomEntreprise'), $this->getViewState('nomInscrit'), $this->getViewState('prenomInscrit'),
			    			$this->adresseAR->Text, $this->getViewState('acteEngagement'));
				if(Atexo_Config::getParameter('SIGNATURE_MAIL') == "1") {
					$signed = true;
				}
				else {
					$signed = false;
				}

                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), Atexo_Util::atexoHtmlEntities($this->adresseAR->Text), Prado::localize('ACCUSE_RECEPTION_REPONSE_ELECTRONIQUE').
                                           ' - '.Prado::localize('TEXT_REF').' : '.$refUtilisateur, $corpsMail, $cc = '', $return_path = '', $signed);
            } else {
                echo 'Erreur consultation cloturee '.date('Y-m-d H:i:s');
                exit;
            }
        } catch (\Exception $e) {
            //echo base64_decode($_POST['xmlString']);echo $e;
            Prado::log('Erreur lors du depot de la reponse entreprise : Ref: {'.Atexo_Util::atexoHtmlEntities($_GET['id']).'}- Org: {'.Atexo_Util::atexoHtmlEntities($_GET['org']).'} - Id enteprise: '.
                   Atexo_CurrentUser::getIdEntreprise().' - Id Inscrit: '.Atexo_CurrentUser::getIdInscrit()."\n".$e, TLogger::ERROR, 'Atexo.pages.entreprise.FormulaireReponse.php');
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->mainPart->setVisible(false);
            $this->confirmationPart->setVisible(false);
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_DEPOT'));
        }
    }

    public function redirectToDetail()
    {
        $this->response->redirect('index.php?page=Entreprise.EntrepriseDetailConsultation&id='.
                                   Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['org']));
    }

    public function redirectToFormulaireVariante()
    {
        $this->response->redirect('index.php?page=Entreprise.FormulaireReponse&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&org='.Atexo_Util::atexoHtmlEntities($_GET['org']).'&var');
    }

    public function redirectToNormalFormulaire()
    {
        $this->response->redirect('index.php?page=Entreprise.FormulaireReponse&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
    }

    /**
     * indique si le lot doit etre retire du formulaire de reponse.
     *
     * @param $objet Consultation | CategorieLot
     *
     * @return bool
     */
    public function isEnveloppeVisibleInCaseOfVariante($objet)
    {
        if ($objet instanceof CommonCategorieLot) {// dans le cas  alloti, l'enveloppe apparait si accepte les variantes
            $variante = '';
            $donnesComplementaire = $objet->getDonneComplementaire();
            if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
                $variante = $donnesComplementaire->getVariantes();
            }
            if ('0' === $variante) {
                return true;
            } else {
                return false;
            }
        } elseif ($objet instanceof CommonConsultation) { // dans le cas non alloti, l'enveloppe apparait tjs
            return false;
        }
    }

    public function getXmlAE($idLot)
    {
        $lotObject = null;
        $inscritObject = (new Atexo_Entreprise_Inscrit())->retrieveInscritAndEntreprise(Atexo_CurrentUser::getIdInscrit());
        $inscritObject = array_shift($inscritObject);
        $entrepriseObject = $inscritObject->getEntreprise();
        $consultationObject = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']));
        $typeProcedure = $consultationObject->getTypeProcedure();
        if ('0' != $idLot) {
            $connectionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $lotObject = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero(Atexo_Util::atexoHtmlEntities($_GET['org']), $consultationObject->getId(), $idLot);
            //$lotObject = CommonCategorieLotPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['org']),$consultationObject->getId(), $idLot, $connectionCom);
        }

        return (new Atexo_ActeDEngagement())->preRemplirXmlAe(Atexo_Config::getParameter('PATH_XML_ACTE_D_ENGAGEMENT'), $consultationObject, $inscritObject, $entrepriseObject, $typeProcedure, $lotObject);
    }

    public function getUrlCondUtilisation()
    {
        //return Atexo_Util::getAbsoluteUrl() . "index.php?page=Commun.ConditionsUtilisation&calledFrom=entreprise";
        return '';
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    /*
     * Permet de tracer l'acces de l'inscrit a la PF
     */
    public function traceOperationsInsrctis($IdDesciption, $dateDebAction, $details = '', $ref = '', $org = '')
    {
        $arrayDonnees = [];
        $arrayDonnees['ref'] = $ref;
        $arrayDonnees['org'] = $org;
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        if (!$details) {
            $details = '-';
        }
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            $details,
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
    }

    public function traceHistorique($sender, $param)
    {
        $this->traceOperationsInsrctis('DESCRIPTION17');
    }
}
