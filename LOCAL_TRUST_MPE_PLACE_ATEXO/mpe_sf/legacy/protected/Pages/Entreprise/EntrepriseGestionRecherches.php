<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;

/**
 * permet de gerer les recherches favorites sauvegardées par un agent.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 * @version 1.0
 *
 * @since 4.8.0
 */
class EntrepriseGestionRecherches extends MpeTPage
{
    /**
     * permet d'initialiser la page.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * permet de charger la page.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        $this->rechercheFavorites->IdCreateur = Atexo_CurrentUser::getIdInscrit();
        $this->rechercheFavorites->TypeCreateur = Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE');
        $this->rechercheFavorites->setCallFrom('entreprise');
        $this->rechercheFavorites->setTypeAvisConsultation('all');
        $this->rechercheFavorites->initConposant();
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            date('Y-m-d H:i:s'),
            substr($_SERVER['QUERY_STRING'], 0),
            '-',
            date('Y-m-d H:i:s')
        );
    }
}
