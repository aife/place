<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonContactEntreprise;
use Application\Propel\Mpe\CommonEntrepriseInfoExercice;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_ContactEntreprise;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Leaders;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Prestation;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_SearchLucene;
use Prado\Prado;

class EntrepriseGestionCompteActivite extends MpeTPage
{
    private $_idEntreprise;
    private $_entreprise;
    public ?string $_compteEntrepriseDonneesFinancieres = null;
    public ?string $_compteEntrepriseChiffreAffaireProductionBiensServices = null;
    public string $pageTitle = 'TITRE';

    public function onLoad($param)
    {
        $this->_compteEntrepriseDonneesFinancieres = 'none';
        $this->_compteEntrepriseChiffreAffaireProductionBiensServices = '';
        $this->customizeForm();
        $this->remplirTableauPrestations(Atexo_CurrentUser::getIdEntreprise());
        $this->agrements->setPostBack($this->IsPostBack);
        $this->qualification->setPostBack($this->IsPostBack);
        if (!Atexo_CurrentUser::isATES()) {
            $this->response->redirect('?page=Entreprise.EntrepriseHome');
        }
        if (Atexo_CurrentUser::getIdEntreprise()) {
            $this->_idEntreprise = Atexo_CurrentUser::getIdEntreprise();
        } else {
            $this->response->redirect('?page=Entreprise.EntrepriseHome');
        }
        $this->remplirTableauDirigeants($this->_idEntreprise);
        if (!$this->isPostBack) {   // Contact entreprise
            $this->remplirActiviteEntreprise($this->_idEntreprise);
            if (Atexo_Module::isEnabled('BourseALaSousTraitance')) {
                if ('1' == $this->_entreprise->getVisibleBourse()) {
                    $this->RechercheCollaborateur->rechercheCollaborateurOui->Checked = true;
                    $this->RechercheCollaborateur->rechercheCollaborateurNon->Checked = false;
                } else {
                    $this->RechercheCollaborateur->rechercheCollaborateurOui->Checked = false;
                    $this->RechercheCollaborateur->rechercheCollaborateurNon->Checked = true;
                    $this->RechercheCollaborateur->panel_rechercheCollaborateur->setStyle('display:none');
                }
                $contact = (new Atexo_ContactEntreprise())->retrieveContactByIdEntreprise($this->_entreprise->getId());
                if ($contact) {
                    $this->RechercheCollaborateur->nomContactGroupement->Text = $contact->getNom();
                    $this->RechercheCollaborateur->prenomContactGroupement->Text = $contact->getPrenom();
                    $this->RechercheCollaborateur->addresseContactGroupement->Text = $contact->getAdresse();
                    $this->RechercheCollaborateur->addressSuiteContactGroupement->Text = $contact->getAdresseSuite();
                    $this->RechercheCollaborateur->cpContactGroupement->Text = $contact->getCodepostal();
                    $this->RechercheCollaborateur->villeContactGroupement->Text = $contact->getVille();
                    $this->RechercheCollaborateur->fonction->Text = $contact->getFonction();
                    $this->RechercheCollaborateur->email->Text = $contact->getEmail();
                    $this->RechercheCollaborateur->tel->Text = $contact->getTelephone();
                }
                $this->RechercheCollaborateur->fillRepeaterTypeCollaboration();
                $typeCollaboration = $this->_entreprise->getTypeCollaboration();
                if ($typeCollaboration) {
                    $arrayType = explode('#', $typeCollaboration);
                    for ($i = 0; $i < count($arrayType); ++$i) {
                        foreach ($this->RechercheCollaborateur->repeaterTypeCollaboration->getItems() as $item) {
                            if ($item->idType->getValue() == $arrayType[$i]) {
                                $item->type->checked = true;
                            }
                        }
                    }
                }
                if ($this->_entreprise->getVisibleBourse()) {
                    $this->RechercheCollaborateur->rechercheCollaborateurOui->Checked;
                } else {
                    $this->RechercheCollaborateur->rechercheCollaborateurNon->Checked;
                }
            }
            $this->siteInternet->Text = $this->_entreprise->getSiteInternet();
            $this->descriptionActivite->Text = $this->_entreprise->getDescriptionActivite();
            $this->activiteDefense->Text = $this->_entreprise->getActiviteDomaineDefense();
            $this->emailDevis->Text = $this->_entreprise->getAdressesElectroniques();
            $this->labelleDocCommercial->Text = $this->_entreprise->getIntituleDocumentsCommerciaux();
            $this->sizeDocCommercial->Text = Atexo_Util::arrondirSizeFile($this->_entreprise->getTailleDocumentsCommerciaux() / 1024);
            if ($this->_entreprise->getIntituleDocumentsCommerciaux()) {
                $this->panelPJDocCommercial->setDisplay('Dynamic');
            }
            $this->qualification->idsQualification->Text = base64_encode($this->_entreprise->getQualification());
            $this->qualification->libelleQualif->Text = base64_encode((new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($this->_entreprise->getQualification()));
            $this->agrements->idsSelectedAgrements->Value = $this->_entreprise->getAgrement();
            $this->moyensTechniques->Text = $this->_entreprise->getMoyensTechnique();
            $this->moyensHumains->Text = $this->_entreprise->getMoyensHumains();
            // Selection des domaines d'activités LT-Ref
            if (Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel')) {
                $atexoDomainesActivitesRef = new Atexo_Ref();
                $atexoDomainesActivitesRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_DOMAINES_ACTIVITES_CONFIG'));
                if ($this->_entreprise->getDomainesActivites()) {
                    $atexoDomainesActivitesRef->setSecondaires($this->_entreprise->getDomainesActivites());
                }
                $this->idAtexoRefDomaineActivite->afficherReferentiel($atexoDomainesActivitesRef);
            }// Séléction des certificats des comptes entreprises Lt-Ref
            if (Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) {
                $atexoCertificatCompteEtpRef = new Atexo_Ref();
                $atexoCertificatCompteEtpRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CERTIFICAT_COMPTE_ENTREPRISE_CONFIG'));
                if ($this->_entreprise->getAgrement()) {
                    $atexoCertificatCompteEtpRef->setSecondaires($this->_entreprise->getAgrement());
                }
                $this->idAtexoCertificatCompteEtpRef->afficherReferentiel($atexoCertificatCompteEtpRef);
            }// Séléction et affichage des qualifications des entreprises en utilisant LT-REF
            if (Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) {
                $atexoQualificationLtRef = new Atexo_Ref();
                $atexoQualificationLtRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_QUALIFICATION_CONFIG'));
                if ($this->_entreprise->getQualification()) {
                    $atexoQualificationLtRef->setSecondaires($this->_entreprise->getQualification());
                }
                $this->idAtexoRefQualification->afficherReferentiel($atexoQualificationLtRef);
            }
            $this->ltrefEntr->afficherReferentielEntreprise(Atexo_CurrentUser::getIdEntreprise());
        }
        $this->remplirTroisDernieresAnnees();
    }

    public function onInit($param)
    {
        $this->pageTitle = 'TITRE_DESCRIPTION_ACTIVITE';
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function remplirTableauDirigeants($idEntreprise)
    {
        $leaders = Atexo_Entreprise_Leaders::retrieveLeaders($idEntreprise);
        if (is_array($leaders)) {
            $this->repeaterDirigeants->dataSource = $leaders;
            $this->repeaterDirigeants->dataBind();
        } else {
            $this->panelDirigeants->setVisible(false);
            $this->panelAvertissement->setVisible(true);
            $this->panelAvertissement->setMessage(Prado::localize('DEFINE_LISTE_DIRIGEANT_VIDE'));
        }
    }

    public function remplirTroisDernieresAnnees()
    {
        $annees = Atexo_Util::getListeTroisDernieresAnnees();
        $this->anneeClotureExercice1->Text = $annees['1'];
        $this->anneeClotureExercice2->Text = $annees['2'];
        $this->anneeClotureExercice3->Text = $annees['3'];
    }

    public function remplirActiviteEntreprise($idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $this->_entreprise = Atexo_Entreprise::getInfoEntreprise($idEntreprise, $connexionCom);
        if ($this->_entreprise) {
            if ($this->_entreprise->getSiren()) {
                $this->PanelSiege->setCalledFor(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
                $this->PanelSiege->setSiren($this->_entreprise->getSiren());
            } else {
                $this->PanelSiege->setCalledFor('Etrangere');
                $this->PanelSiege->setIdNational($this->_entreprise->getSirenEtranger());
            }
            $this->PanelSiege->setRaisonSociale($this->_entreprise->getNom());
            $this->PanelSiege->setLieuEtablissement($this->_entreprise->getPaysAdresse());
        }
        self::fillListePme();

        $annee = Atexo_Util::getlistetroisdernieresannees();
        $EntrepriseInfoExercice1 = CommonEntrepriseInfoExercice::getCommonEntrepriseInfoExercice($idEntreprise, $annee[1], $connexionCom);
        if ($EntrepriseInfoExercice1) {
            $this->anneeClotureExercice1->Text = $EntrepriseInfoExercice1->getAnneeClotureExercice();
            $this->debutExercice1->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice1->getDebutExerciceGlob());
            $this->finExercice1->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice1->getFinExerciceGlob());
            $this->vente1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getVentesGlob(), 2);
            $this->bien1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getBiensGlob(), 2);
            $this->service1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getServicesGlob(), 2);
            $this->total1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getTotalGlob(), 2);
            $this->effectifMoyen1->Text = $EntrepriseInfoExercice1->getEffectifMoyen();
            $this->effectifEncadrement1->Text = $EntrepriseInfoExercice1->getEffectifEncadrement();
            $pme = $EntrepriseInfoExercice1->getPme();
            if ('1' === $pme || '0' === $pme) {
                $this->pme1->setSelectedValue($pme);
            }
            $this->caA1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getChiffreAffaires(), 2);
            $this->bA1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getBesoinExcedentFinancement(), 2);
            $this->cfA1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getCashFlow(), 2);
            $this->ceA1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getCapaciteEndettement(), 2);
        } else {
            $this->debutExercice1->Text = '01/01/'.$annee[1];
            $this->finExercice1->Text = '31/12/'.$annee[1];
        }
        $EntrepriseInfoExercice2 = CommonEntrepriseInfoExercice::getCommonEntrepriseInfoExercice($idEntreprise, $annee[2], $connexionCom);
        if ($EntrepriseInfoExercice2) {
            $this->anneeClotureExercice2->Text = $EntrepriseInfoExercice2->getAnneeClotureExercice();
            $this->debutExercice2->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice2->getDebutExerciceGlob());
            $this->finExercice2->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice2->getFinExerciceGlob());
            $this->vente2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getVentesGlob(), 2);
            $this->bien2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getBiensGlob(), 2);
            $this->service2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getServicesGlob(), 2);
            $this->total2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getTotalGlob(), 2);
            $this->effectifMoyen2->Text = $EntrepriseInfoExercice2->getEffectifMoyen();
            $this->effectifEncadrement2->Text = $EntrepriseInfoExercice2->getEffectifEncadrement();
            $pme = $EntrepriseInfoExercice2->getPme();
            if ('1' === $pme || '0' === $pme) {
                $this->pme2->setSelectedValue($pme);
            }
            $this->caA2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getChiffreAffaires(), 2);
            $this->bA2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getBesoinExcedentFinancement(), 2);
            $this->cfA2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getCashFlow(), 2);
            $this->ceA2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getCapaciteEndettement(), 2);
        } else {
            $this->debutExercice2->Text = '01/01/'.$annee[2];
            $this->finExercice2->Text = '31/12/'.$annee[2];
        }
        $EntrepriseInfoExercice3 = CommonEntrepriseInfoExercice::getCommonEntrepriseInfoExercice($idEntreprise, $annee[3], $connexionCom);
        if ($EntrepriseInfoExercice3) {
            $this->anneeClotureExercice3->Text = $EntrepriseInfoExercice3->getAnneeClotureExercice();
            $this->debutExercice3->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice3->getDebutExerciceGlob());
            $this->finExercice3->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice3->getFinExerciceGlob());
            $this->vente3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getVentesGlob(), 2);
            $this->bien3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getBiensGlob(), 2);
            $this->service3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getServicesGlob(), 2);
            $this->total3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getTotalGlob(), 2);
            $this->effectifMoyen3->Text = $EntrepriseInfoExercice3->getEffectifMoyen();
            $this->effectifEncadrement3->Text = $EntrepriseInfoExercice3->getEffectifEncadrement();
            $pme = $EntrepriseInfoExercice3->getPme();
            if ('1' === $pme || '0' === $pme) {
                $this->pme3->setSelectedValue($pme);
            }
            $this->caA3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getChiffreAffaires(), 2);
            $this->bA3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getBesoinExcedentFinancement(), 2);
            $this->cfA3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getCashFlow(), 2);
            $this->ceA3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getCapaciteEndettement(), 2);
        } else {
            $this->debutExercice3->Text = '01/01/'.$annee[3];
            $this->finExercice3->Text = '31/12/'.$annee[3];
        }
    }

    /**
     * retourne le libellé corréspond à l'état de l'entreprise.
     */
    public function getEtatEntreprise($pme)
    {
        if (null == $pme) {
            return Prado::localize('NON_RENSEIGNE');
        } elseif (0 == $pme) {
            return Prado::localize('DEFINE_NON');
        } elseif (1 == $pme) {
            return Prado::localize('DEFINE_OUI');
        }
    }

    /**
     * rafraichit le tableau des dirigeants.
     */
    public function refreshEntrepriseInfos($sender, $param)
    {
        $this->remplirTableauDirigeants($this->_idEntreprise);
        $this->panelDirigeants->render($param->NewWriter);
    }

    /**
     * supprime un dirigeant.
     */
    public function onDeleteClick($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idDirigeant = $param->CommandParameter;
        Atexo_Entreprise_Leaders::delete($idDirigeant, $this->_idEntreprise, $connexionCom);
        $this->remplirTableauDirigeants($this->_idEntreprise);
    }

    /**
     * met à jour les informations relatives à une entreprise.
     */
    public function update()
    {
        $typeCollaboration = null;
        if ($this->IsValid) {
            //Début scan antivirus
            if ($this->docCommercial->HasFile) {
                $msg = Atexo_ScanAntivirus::startScan($this->docCommercial->LocalName);
                if ($msg) {
                    $this->afficherErreur($msg.' "'.$this->docCommercial->FileName.'"');

                    return;
                }
            }
            //Fin scan antivirus
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $entreprise = EntreprisePeer::retrieveByPk($this->_idEntreprise, $connexionCom);
            $entreprise->setSiteInternet($this->siteInternet->Text);
            $entreprise->setDescriptionActivite($this->descriptionActivite->Text);
            $entreprise->setActiviteDomaineDefense($this->activiteDefense->Text);
            $entreprise->setDateModification(date('Y-m-d H:i:s'));

            if ($this->docCommercial->HasFile) {
                $infile = Atexo_Config::getParameter('COMMON_TMP').'docCommercial'.session_id().time();
                if (move_uploaded_file($this->docCommercial->LocalName, $infile)) {
                    $atexoBlob = new Atexo_Blob();
                    $docIdBlob = $atexoBlob->insert_blob($this->docCommercial->FileName, $infile, Atexo_Config::getParameter('COMMON_BLOB'));
                    $entreprise->setDocumentsCommerciaux($docIdBlob);
                    $entreprise->setIntituleDocumentsCommerciaux($this->docCommercial->FileName);
                    $entreprise->setTailleDocumentsCommerciaux($this->docCommercial->FileSize);
                }
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseQualification')) {
                $entreprise->setQualification('#'.base64_decode($this->qualification->idsQualification->Text));
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseAgrement')) {
                $entreprise->setAgrement($this->agrements->idsSelectedAgrements->Value);
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) {
                $entreprise->setAgrement($this->idAtexoCertificatCompteEtpRef->codesRefSec->value);
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) {
                $entreprise->setQualification($this->idAtexoRefQualification->codesRefSec->value);
            }
            $entreprise->setMoyensTechnique($this->moyensTechniques->Text);
            $entreprise->setMoyensHumains($this->moyensHumains->Text);

            if (Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel')) {
                $entreprise->setDomainesActivites($this->idAtexoRefDomaineActivite->codesRefSec->value);
            }

            if (Atexo_Module::isEnabled('BourseALaSousTraitance')) {
                if ($this->RechercheCollaborateur->rechercheCollaborateurOui->Checked) {
                    $contact = (new Atexo_ContactEntreprise())->retrieveContactByIdEntreprise($this->_idEntreprise);
                    if (!$contact) {
                        $contact = new CommonContactEntreprise();
                        $contact->setIdEntreprise($this->_idEntreprise);
                    }

                    $contact->setNom($this->RechercheCollaborateur->nomContactGroupement->Text);
                    $contact->setPrenom($this->RechercheCollaborateur->prenomContactGroupement->Text);
                    $contact->setAdresse($this->RechercheCollaborateur->addresseContactGroupement->Text);
                    $contact->setAdresseSuite($this->RechercheCollaborateur->addressSuiteContactGroupement->Text);
                    $contact->setCodepostal($this->RechercheCollaborateur->cpContactGroupement->Text);
                    $contact->setVille($this->RechercheCollaborateur->villeContactGroupement->Text);
                    $contact->setFonction($this->RechercheCollaborateur->fonction->Text);
                    $contact->setEmail(trim($this->RechercheCollaborateur->email->Text));
                    $contact->setTelephone($this->RechercheCollaborateur->tel->Text);
                    $contact->save($connexionCom);
                    foreach ($this->RechercheCollaborateur->repeaterTypeCollaboration->getItems() as $item) {
                        if ($item->type->checked) {
                            $typeCollaboration .= '#'.$item->idType->getValue();
                        }
                    }
                    if ($typeCollaboration) {
                        $typeCollaboration .= '#';
                        $entreprise->setTypeCollaboration($typeCollaboration);
                    }
                    $entreprise->setVisibleBourse('1');
                } else {
                    $entreprise->setVisibleBourse('0');
                }
            }
            $entreprise->setAdressesElectroniques($this->emailDevis->Text);
            $entreprise->save($connexionCom);
            $this->ltrefEntr->saveReferentielEntreprise(Atexo_CurrentUser::getIdEntreprise());

            // Mise a jour de l'index de Lucene
            if (Atexo_Module::isEnabled('UtiliserLucene')) {
                $searchLucene = new Atexo_Entreprise_SearchLucene();
                $searchLucene->addEntreprise($entreprise);
            }
            if (Atexo_Module::isEnabled('UtiliserLucene')) {
                $searchLucene = new Atexo_Entreprise_SearchLucene();
                $searchLucene->addEntreprise($entreprise);
            }

            //Les informations cles des derniers exercices
            $idEntreprise = $entreprise->getId();
            self::saveExercicesEntreprise($idEntreprise, $connexionCom);
            $this->response->redirect('?page=Entreprise.EntrepriseAccueilAuthentifie');
        }
    }

    /**
     * retourner vers la page d'Accueil.
     */
    public function retour($sender, $param)
    {
        $this->response->redirect('?page=Entreprise.EntrepriseAccueilAuthentifie');
    }

    /***
     * afficher les champs qui sont propre à mpe Maroc
     */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseDocumentsCommerciaux')) {
            $this->panelDocumentsComerciaux->setDisplay('Dynamic');
        } else {
            $this->panelDocumentsComerciaux->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseQualification')) {
            $this->panelQualification->setDisplay('Dynamic');
        } else {
            $this->panelQualification->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseAgrement')) {
            $this->panelAgrements->setDisplay('Dynamic');
        } else {
            $this->panelAgrements->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseMoyensTechniques')) {
            $this->panelMoyensTechnique->setDisplay('Dynamic');
        } else {
            $this->panelMoyensTechnique->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseMoyensHumains')) {
            $this->panelMoyensHumains->setDisplay('Dynamic');
        } else {
            $this->panelMoyensHumains->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntreprisePrestationsRealisees')) {
            $this->panelPrestation->setDisplay('Dynamic');
        } else {
            $this->panelPrestation->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseDeclarationHonneur')) {
            $this->panelDeclarationSurHonneur->validateurDeclaration->setValidationGroup('validateInfosEntrperise');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseActiviteDomaineDefense')) {
            $this->panelActiviteDomaineDefense->setDisplay('Dynamic');
        } else {
            $this->panelActiviteDomaineDefense->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseDonneesFinancieres')) {
            $this->_compteEntrepriseDonneesFinancieres = '';
        } else {
            $this->_compteEntrepriseDonneesFinancieres = 'none';
        }
        if (!Atexo_Module::isEnabled('CompteEntrepriseChiffreAffaireProductionBiensServices')) {
            $this->_compteEntrepriseChiffreAffaireProductionBiensServices = 'none';
        } else {
            $this->_compteEntrepriseChiffreAffaireProductionBiensServices = '';
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseDeclarationHonneur')) {
            $this->panelDeclarationSurHonneur->setVisible(true);
        } else {
            $this->panelDeclarationSurHonneur->setVisible(false);
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) {
            $this->panelcertification->setDisplay('Dynamic');
        } else {
            $this->panelcertification->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) {
            $this->panelQualificationLtRef->setDisplay('Dynamic');
        } else {
            $this->panelQualificationLtRef->setDisplay('None');
        }
    }

    /***
     * rempplir le tableau des préstation d'une entreprise
     *
     */
    public function remplirTableauPrestations($idEntreprise)
    {
        $prestations = Atexo_Entreprise_Prestation::retrievePrestations($idEntreprise);
        if (is_array($prestations)) {
            $this->prestationEntreprise->repeaterPrestations->dataSource = $prestations;
            $this->prestationEntreprise->repeaterPrestations->dataBind();
        }
    }

    /**
     * rafraichit le tableau des prestation d'une entreprise.
     */
    public function refreshEntreprisePrestations($sender, $param)
    {
        $this->remplirTableauPrestations(Atexo_CurrentUser::getIdEntreprise());
        $this->panelPrestation->render($param->NewWriter);
    }

    /**
     * supprime une prestation.
     */
    public function deletePrestation($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idPrestation = $param->CommandParameter;
        Atexo_Entreprise_Prestation::delete($idPrestation, $connexionCom);
        $this->remplirTableauPrestations($this->_idEntreprise);
    }

    /**
     * supprimer les documments commerciaux (ecrase le document déja ajouté).
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function deleteDocumentCommercial($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $entreprise = EntreprisePeer::retrieveByPk($this->_idEntreprise, $connexionCom);
        $entreprise->setDocumentsCommerciaux(null);
        $entreprise->setIntituleDocumentsCommerciaux(null);
        $entreprise->setTailleDocumentsCommerciaux(null);
        $entreprise->save($connexionCom);
    }

    public function refreshPanelDocumentCommercial($sender, $param)
    {
        $this->panelPJDocCommercial->setDisplay('None');
    }

    public function refreshComposants($sender, $param)
    {
        $this->panelQualification->render($param->NewWriter);
        $this->panelAgrements->render($param->NewWriter);
        $this->panelDirigeants->render($param->NewWriter);
        $this->panelActiviteEntreprise->render($param->NewWriter);
    }

    public function downloadPiece($sender, $param)
    {
        $nomFichier = $this->_entreprise->getIntituleDocumentsCommerciaux();
        $idFichier = $this->_entreprise->getdocumentsCommerciaux();

        //$connexion=Propel::getConnection(Atexo_Config::getParameter("COMMON_DB").Atexo_Config::getParameter('CONST_READ_WRITE'));
        $atexoBlob = new Atexo_Blob();
        $tailleFichier = $atexoBlob->getTailFile($idFichier, Atexo_Config::getParameter('COMMON_BLOB'));
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        if ('.zip' == strrchr($nomFichier, '.')) {
            header('Content-Type: application/zip');
        } else {
            header('Content-Type: application/octet-stream');
        }
        $nomFichier = str_replace('\\', '', $nomFichier);
        header('Content-Disposition: attachment; filename="'.$nomFichier.'";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.$tailleFichier);

        $atexoBlob->send_blob_to_client($idFichier, Atexo_Config::getParameter('COMMON_BLOB'));
        exit;
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }

    public function checkMultipleEmailAdresse($sender, $param)
    {
        if ('' != $this->emailDevis->Text) {
            $emailsErrone = Atexo_Util::checkMultipleEmail($this->emailDevis->Text);
            if (is_array($emailsErrone)) {
                $this->emailValidator->ErrorMessage = Prado::localize('TEXT_ADRESSES_ELECTRONIQUE').' : '.implode(',', $emailsErrone);
                $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
            } else {
                $param->IsValid = true;
            }
        } else {
            $param->IsValid = true;
        }
    }

    /**
     * Permet de remplir les liste des pme.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function fillListePme()
    {
        $dataSource = ['2' => Prado::Localize('NON_RENSEIGNE'), '1' => Prado::Localize('DEFINE_OUI'), '0' => Prado::Localize('DEFINE_NON')];
        $this->pme1->Datasource = $dataSource;
        $this->pme1->dataBind();
        $this->pme1->setSelectedValue('2');
        $this->pme2->Datasource = $dataSource;
        $this->pme2->dataBind();
        $this->pme2->setSelectedValue('2');
        $this->pme3->Datasource = $dataSource;
        $this->pme3->dataBind();
        $this->pme3->setSelectedValue('2');
    }

    /**
     * Permet de sauvgarder les exercices de l'entreprise.
     *
     * @param int $idEntreprise l'id de l'entreprise,Objet $connexionCom de la connexion
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function saveExercicesEntreprise($idEntreprise, $connexionCom)
    {
        $annees = Atexo_Util::getlistetroisdernieresannees();
        foreach ($annees as $key => $annee) {
            $entrepriseInfoExercice = CommonEntrepriseInfoExercice::retrieveEntrepriseInfoExerciceByIdEntreprise($idEntreprise, $annee, $connexionCom);
            if (!$entrepriseInfoExercice) {
                $entrepriseInfoExercice = new CommonEntrepriseInfoExercice();
                $entrepriseInfoExercice->setIdEntreprise($idEntreprise);
            }
            self::fillEntrepriseExerciceByAnnee($entrepriseInfoExercice, $annee, $key);
            $entrepriseInfoExercice->save($connexionCom);
        }
    }

    /**
     * Permet de remplir l'exercice de l'entreprise par les indos.
     *
     * @param Objet  CommonEntrepriseInfoExercice $entrepriseInfoExercice l'exercice de l'entreprise,String $annee l'annee de l'exercice,Integer $numElement le numero d'element
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function fillEntrepriseExerciceByAnnee(&$entrepriseInfoExercice, $annee, $numElement)
    {
        $entrepriseInfoExercice->setAnneeClotureExercice($annee);
        $debutExercice = 'debutExercice'.$numElement;
        $entrepriseInfoExercice->setDebutexerciceglob(Atexo_Util::frnDate2iso($this->$debutExercice->SafeText));
        $finExercice = 'finExercice'.$numElement;
        $entrepriseInfoExercice->setFinexerciceglob(Atexo_Util::frnDate2iso($this->$finExercice->SafeText));
        if (Atexo_Module::isEnabled('CompteEntrepriseChiffreAffaireProductionBiensServices')) {
            $vente = 'vente'.$numElement;
            $entrepriseInfoExercice->setVentesGlob($this->$vente->SafeText);
            $bien = 'bien'.$numElement;
            $entrepriseInfoExercice->setBiensGlob($this->$bien->SafeText);
            $service = 'service'.$numElement;
            $entrepriseInfoExercice->setServicesGlob($this->$service->SafeText);
            $total = 'total'.$numElement;
            $entrepriseInfoExercice->setTotalGlob($this->$total->SafeText);

            $pme = 'pme'.$numElement;
            if ('0' === $this->$pme->getSelectedValue() || '1' === $this->$pme->getSelectedValue()) {
                $entrepriseInfoExercice->setPme($this->$pme->getSelectedValue());
            } else {
                $entrepriseInfoExercice->setPme('');
            }
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseDonneesFinancieres')) {
            $caA = 'caA'.$numElement;
            $entrepriseInfoExercice->setChiffreAffaires($this->$caA->SafeText);
            $bA = 'bA'.$numElement;
            $entrepriseInfoExercice->setBesoinExcedentFinancement($this->$bA->SafeText);
            $cfA = 'cfA'.$numElement;
            $entrepriseInfoExercice->setCashFlow($this->$cfA->SafeText);
            $ceA = 'ceA'.$numElement;
            $entrepriseInfoExercice->setCapaciteEndettement($this->$ceA->SafeText);
        }
        $effectifMoyen = 'effectifMoyen'.$numElement;
        $entrepriseInfoExercice->setEffectifMoyen($this->$effectifMoyen->SafeText);
        $effectifEncadrement = 'effectifEncadrement'.$numElement;
        $entrepriseInfoExercice->setEffectifEncadrement($this->$effectifEncadrement->SafeText);
    }

    public function getInfoSaisiManuelle()
    {
        $return = false;
        if (!$this->_entreprise) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $this->_entreprise = Atexo_Entreprise::getInfoEntreprise($this->_idEntreprise, $connexionCom);
        }
        if ($this->_entreprise->getSaisieManuelle()) {
            $return = true;
        }

        return $return;
    }
}
