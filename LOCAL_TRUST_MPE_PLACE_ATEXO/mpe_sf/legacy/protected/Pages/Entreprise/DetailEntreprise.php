<?php

namespace Application\Pages\Entreprise;

use Application\Pages\Commun\DetailEntrepriseMere;
use Application\Propel\Mpe\Entreprise;

/**
 * Composant Detail de l'Entreprise.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailEntreprise extends DetailEntrepriseMere
{
}
