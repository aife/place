<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonResponsableengagement;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Leaders;

class EntrepriseAjoutDirigeant extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        if (!$this->IsPostBack && isset($_GET['id'])) {
            $leader = (new Atexo_Entreprise_Leaders())->retrieveLeaderById(Atexo_Util::atexoHtmlEntities($_GET['id']));
            if ($leader instanceof CommonResponsableengagement && $leader->getEntrepriseId() == Atexo_CurrentUser::getIdEntreprise()) {
                $this->erreur->setVisible(false);

                $this->nom->Text = $leader->getNom();
                $this->prenom->Text = $leader->getPrenom();
                $this->qualite->Text = $leader->getQualite();
                $this->email->Text = $leader->getEmail();
                $this->telephone->Text = $leader->getTelephone();
                $this->fax->Text = $leader->getFax();
            } else {
                $this->erreur->setVisible(true);
                $this->panelMessageErreur->setMessage("Vous n'êtes as autorisé à modifier ce dirigeant.");
                $this->noErreur->setVisible(false);
            }
        } else {
            $this->erreur->setVisible(false);
        }
        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    public function ajouterDirigeant($sender, $param)
    {
        if (!Atexo_CurrentUser::isConnected()) {
            $this->response->redirect('?page=Entreprise.EntrepriseHome');
        }
        if (isset($_GET['id'])) {
            $dirigeant = (new Atexo_Entreprise_Leaders())->retrieveLeaderById(Atexo_Util::atexoHtmlEntities($_GET['id']));
        } else {
            $dirigeant = new CommonResponsableengagement();
            $idEntreprise = Atexo_CurrentUser::getIdEntreprise();
            $dirigeant->setEntrepriseId($idEntreprise);
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $dirigeant->setNom(Atexo_Util::atexoHtmlEntities($this->nom->Text));
        $dirigeant->setPrenom($this->prenom->Text);
        $dirigeant->setQualite($this->qualite->Text);
        $dirigeant->setEmail(trim($this->email->Text));
        $dirigeant->setTelephone($this->telephone->Text);
        $dirigeant->setFax($this->fax->Text);
        $dirigeant->save($connexionCom);

        $this->scriptAddAndClose->Text = '<script>'.(isset($_GET['callback']) ? "opener.document.getElementById('ctl0_CONTENU_PAGE_".Atexo_Util::atexoHtmlEntities($_GET['callback'])."').click();" : '')."opener.document.getElementById('ctl0_CONTENU_PAGE_refreshEntrepriseInformations').click();window.close();</script>";
    }
}
