<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDestinataireAnnonceJAL;
use Application\Propel\Mpe\CommonDestinataireCentralePub;
use Application\Propel\Mpe\CommonRelationEchange;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTComplementFormulairePeer;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_ComplementFormulaire;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Echange;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use Application\Service\Atexo\Publicite\Atexo_Publicite_CentralePublication;
use Application\Service\Atexo\Publicite\Atexo_Publicite_DestinataireJAL;
use Prado\Prado;

/**
 * Classe EntrepriseDetailEchangeMail.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDetailEchangeMail extends MpeTPage
{
    protected $orgAcronyme;
    protected $idMsg;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $objetMessage = null;
        $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $logger = Atexo_LoggerManager::getLogger('messec');
        $this->orgAcronyme = $org;
        $this->accusereception->_acronymeOrg = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $this->accusereception->_numAccuseReception = Atexo_Util::atexoHtmlEntities($_GET['num_ar']);

        $echangeDest = Atexo_Message::getechangeDestinataireByUid(Atexo_Util::atexoHtmlEntities($_GET['num_ar']), $org);
        if ($echangeDest && $_GET['num_ar']) {
            if ($echangeDest->getIdEchange()) {
                $objetMessage = Atexo_Message::retrieveMessageById($echangeDest->getIdEchange(), $org);
            }
            $this->accusereception->_refConsultation = $objetMessage->getConsultationId();
            if (!$this->IsPostBack) {
                $this->messageconfirm->setMessage(Prado::localize('DEFINE_TEXT_MESSAGE_CONFIRMATION'));
                if ($echangeDest) {
                    $this->idMsg = $echangeDest->getIdEchange();
                    if ('0' != $echangeDest->getTypeAr() && $echangeDest->getAr() != Atexo_Config::getParameter('ACCUSE_RECEPTION_VALIDE')) {
                        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                        $logger->info('set date AR num ar : '.$_GET['num_ar'].' org '.$org);
                        $dateAr = date('Y-m-d H:i:s');
                        $echangeDest->setAr(Atexo_Config::getParameter('ACCUSE_RECEPTION_VALIDE'));
                        $echangeDest->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_DATE_HEURE'));
                        $echangeDest->setDateAr($dateAr);
                        $echangeDest->save($connexionCom);
                        $logger->info('save echangeDest '.$echangeDest->getId().' avec la date '.$dateAr);
                        //Dans le cas de l'utilisation de l'interface SUB, on met a jour la date de premier accuse de reception
                        //On fait une mise a jour des dossiers par web service
                        if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                            try {
                                $typeRelation = Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMPLEMENT_FORMULAIRE');
                                $relationEchange = (new Atexo_Message_RelationEchange())->getRelationByIdEchangeAndTypeRelation($echangeDest->getIdEchange(), $typeRelation, $org);
                                if ($relationEchange instanceof CommonRelationEchange) {
                                    $complement = CommonTComplementFormulairePeer::retrieveByPK($relationEchange->getIdExterne(), $connexionCom);
                                    if ($complement instanceof CommonTComplementFormulaire) {
                                        (new Atexo_FormulaireSub_ComplementFormulaire())->enregistrerDatePremierARDemandeComplements($complement);
                                        $arrayDate = explode(' ', $echangeDest->getDateAr());
                                        $arrayInfos = [];
                                        $arrayInfos[0]['binding']['type'] = 'dateType';
                                        $arrayInfos[0]['binding']['nom'] = 'AOF_Candidature_Date_A_R';
                                        $arrayInfos[0]['binding']['valeur'] = Atexo_Util::iso2frnDate($arrayDate[0]);
                                        $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($complement->getIdDossierFormulaire());
                                        if ($dossier instanceof CommonTDossierFormulaire) {
                                            (new Atexo_FormulaireSub_Echange())->updateDossierInterfaceSub($dossier->getCleExterneDossier(), $arrayInfos);
                                        }
                                    }
                                }
                            } catch (\Exception $e) {
                                $logger = Atexo_LoggerManager::getLogger('publicite');
                                $logger->error("Erreur lors de l'envoi de la requete a l'interface SUB : ".$e->getMessage());
                            }
                        }
                    }
                    $this->contenuEchange->setVisible(true);
                    $this->accusereception->remplirInfoEchange($echangeDest, $org);
                    $this->updateAR($this->getTableToUpByEchangeDestinataire($echangeDest, $org), $org);
                }
                $this->idretour->NavigateUrl = 'index.php?page=Entreprise.EntrepriseHome';
                $consultation = CommonConsultationPeer::retrieveByPK($objetMessage->getConsultationId(), $connexionCom);
                if (
                    Atexo_Module::isEnabled('PanierEntreprise')
                    && $consultation->getTypeAcces() != Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')
                ) {
                    $url = Atexo_MpeSf::getUrlDetailsConsultation($objetMessage->getConsultationId(), "&orgAcronyme=$org");
                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    if ($consultation instanceof CommonConsultation
                        && Atexo_Consultation::getStatus($consultation, true) >= Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                    ) {
                        $url .= '&clotures';
                    }
                    $this->idretourConsultation->NavigateUrl = $url;
                    $this->idretourConsultation->visible = true;
                }
            }
        } else {
            $logger->error('Echange destinataire non trouve num_ar : '.$_GET['num_ar'].' org '.$org);
            $this->errorMsg->setVisible(true);
            $this->errorMsg->setMessage(Prado::localize('TEXT_ERREUR_URL'));
            $this->contenuEchange->setVisible(false);
        }
    }

    public function getTableToUpByEchangeDestinataire($echangeDest, $org)
    {
        $desObject = null;
        if ($echangeDest) {
            $desCentralePub = (new Atexo_Publicite_CentralePublication())->retrieveDestinataireCentralPubByEchangeDestinataire($echangeDest, $org);
            $desAnnonceJal = (new Atexo_Publicite_DestinataireJAL())->retrieveDestinataireAnnonceJalByEchangeDestinataire($echangeDest, $org);

            if ($desCentralePub) {
                $desObject = $desCentralePub;
            } elseif ($desAnnonceJal) {
                $desObject = $desAnnonceJal;
            }
            if ($desObject) {
                return $desObject;
            } else {
                return false;
            }
        }
    }

    public function updateAR($obj, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($obj instanceof CommonDestinataireCentralePub || $obj instanceof CommonDestinataireAnnonceJAL) {
            $obj->setAccuse(1);
            $obj->setdateAr(date('Y-m-d H:i:s'));
            $obj->save($connexion);
        }
    }
}
