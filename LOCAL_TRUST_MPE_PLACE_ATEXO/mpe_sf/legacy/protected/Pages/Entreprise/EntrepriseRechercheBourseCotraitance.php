<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_BourseCotraitance;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Created by PhpStorm.
 * User: AsoAtexo
 * Date: 10/08/2015
 * Time: 12:32.
 */
class EntrepriseRechercheBourseCotraitance extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $groupement = Atexo_BourseCotraitance::getCasGroupement($_GET['id'], Atexo_CurrentUser::getIdEntreprise());
        $eseInscrite = false;
        if ('cas2' == $groupement['cas'] || 'cas4' == $groupement['cas']) {
            $eseInscrite = true;
        }
        if (!Atexo_Module::isEnabled('BourseCotraitance')) {
            $this->response->redirect('?page=Entreprise.EntrepriseHome');
        } elseif (!$eseInscrite) {
            $url = urlencode('?page=Entreprise.EntrepriseRechercheBourseCotraitance'.
                             '&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).
                             '&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&GME');
            $this->response->redirect('?page=Entreprise.EntrepriseHome&goto='.$url);
        }
        if (!$this->getIsPostBack() && !$this->getIsCallback()) {
            if (Atexo_CurrentUser::isConnected() && Atexo_CurrentUser::getIdInscrit()) {
                if (isset($_GET['GME'])) {
                    $criteresGME = [
                        'refCons' => $_GET['id'],
                        'mandataire' => '',
                        'solidaire' => '',
                        'conjoint' => '',
                        'clauseSocial' => '',
                        'eseAdaptee' => '',
                        'limit' => '10',
                        'offset' => '0',
                        'keyWord' => '',
                    ];
                    $this->AtexoResultCotraitant->setCriteresSearch($criteresGME);
                    $countResult = $this->AtexoResultCotraitant->getlisteCotraitant($criteresGME, true);
                    $this->AtexoResultCotraitant->setResultCount($countResult);
                    if ($countResult) {
                        $this->AtexoResultCotraitant->displayCotraitant();
                    }
                    $this->blocSearchCotraitant->style = 'display:none;';
                    $this->blocResultCotraitant->style = 'display:block;';
                    $this->AtexoSearchCotraitant->visible = false;
                    $this->AtexoResultCotraitant->visible = true;
                } else {
                    $this->blocSearchCotraitant->style = 'display:block;';
                    $this->blocResultCotraitant->style = 'display:none;';
                    $this->AtexoSearchCotraitant->visible = true;
                    $this->AtexoResultCotraitant->visible = false;
                }
                $this->AtexoSearchCotraitant->initComposant();
            } else {
                $inscription = '';
                $url = urlencode('?page=Entreprise.EntrepriseRechercheBourseCotraitance'.
                                 '&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).
                                 '&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).$inscription);
                $this->response->redirect('?page=Entreprise.EntrepriseHome&goto='.$url);
            }
        }
    }

    public function search($sender, $param)
    {
        try {
            $criteresCotraitants = $this->AtexoSearchCotraitant->getCriteresCotraitant();
            $this->setViewState('criteresRecherche', $criteresCotraitants);
            $criteresCotraitants['offset'] = 0;
            $criteresCotraitants['limit'] = $this->AtexoResultCotraitant->pageSize;
            $this->AtexoResultCotraitant->setCriteresSearch($criteresCotraitants);
            $countResult = $this->AtexoResultCotraitant->getlisteCotraitant($criteresCotraitants, true);
            $this->AtexoResultCotraitant->setResultCount($countResult);
            $this->blocSearchCotraitant->style = 'display:none;';
            $this->blocResultCotraitant->style = 'display:block;';
            $this->AtexoSearchCotraitant->visible = false;
            $this->AtexoResultCotraitant->visible = true;
            if ($countResult) {
                $this->AtexoResultCotraitant->displayCotraitant();
            } else {
                $this->AtexoResultCotraitant->hideComponent($sender, $param);
            }
            $this->activerTypesGroupements($criteresCotraitants);
            $this->blocSearchCotraitant->render($param->getNewWriter());
            $this->blocResultCotraitant->render($param->getNewWriter());
        } catch (Exception $e) {
            Prado::log(' Erreur : '.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'EntrepriseRechercheBourseCotraitance.php');
        }
    }

    public function backForSearch($sender, $param)
    {
        $this->blocSearchCotraitant->style = 'display:block;';
        $this->blocResultCotraitant->style = 'display:none;';
        $this->scriptJs->Text = '<script language="JavaScript" type="text/JavaScript">J("html").animate({ "scrollTop": 0 })</script>';
        $this->AtexoSearchCotraitant->visible = true;
        $this->AtexoResultCotraitant->visible = false;
        $this->blocSearchCotraitant->render($param->getNewWriter());
        $this->blocResultCotraitant->render($param->getNewWriter());
    }

    /**
     * Permet de reprendre les criteres "types de groupements" saisis dans le moteur de recherche.
     *
     * @param array $criteres : tableau des criteres
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function activerTypesGroupements($criteres)
    {
        if (!empty($criteres)) {
            $script = $this->desactiverTypesGroupements();
            if ('1' == $criteres['mandataire']) {
                $script .= 'J("#ctl0_CONTENU_PAGE_AtexoResultCotraitant_mandataire").click();';
            }
            if ('1' == $criteres['conjoint']) {
                $script .= 'J("#ctl0_CONTENU_PAGE_AtexoResultCotraitant_conjoint").click();';
            }
            if ('1' == $criteres['solidaire']) {
                $script .= 'J("#ctl0_CONTENU_PAGE_AtexoResultCotraitant_solidaire").click();';
            }
            if ('1' == $criteres['sousTraitant']) {
                $script .= 'J("#ctl0_CONTENU_PAGE_AtexoResultCotraitant_sousTraitant").click();';
            }
            $this->AtexoResultCotraitant->filtreMotsCles->Text = $criteres['keyWord'];
            $this->scriptJs->Text = '<script language="JavaScript" type="text/JavaScript">'.$script.'</script>';
        }
    }

    /**
     * Permet de reprendre les criteres "types de groupements" saisis dans le moteur de recherche avec pour action de desactiver les types de groupements choisis.
     *
     * @return string : javascript
     */
    public function desactiverTypesGroupements()
    {
        $script = 'document.getElementById("ctl0_CONTENU_PAGE_AtexoResultCotraitant_mandataire").checked=false;';
        $script .= 'document.getElementById("ctl0_CONTENU_PAGE_AtexoResultCotraitant_conjoint").checked=false;';
        $script .= 'document.getElementById("ctl0_CONTENU_PAGE_AtexoResultCotraitant_solidaire").checked=false;';

        return $script;
    }
}
