<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEnchereEntreprisePmi;
use Application\Propel\Mpe\CommonEnchereEntreprisePmiQuery;
use Application\Propel\Mpe\CommonEnchereOffre;
use Application\Propel\Mpe\CommonEnchereOffrePeer;
use Application\Propel\Mpe\CommonEnchereOffreReference;
use Application\Propel\Mpe\CommonEncherePmi;
use Application\Propel\Mpe\CommonEncherePmiPeer;
use Application\Propel\Mpe\CommonEnchereReferencePeer;
use Application\Propel\Mpe\CommonEnchereReferenceQuery;
use Application\Propel\Mpe\CommonEnchereValeursInitialesPeer;
use Application\Propel\Mpe\CommonEnchereValeursInitialesQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;
use Prado\TPropertyValue;
use Prado\Util\TLogger;

/**
 * Class pour la participation d'une entreprise a une enchere.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseParticipationEnchere extends MpeTPage
{
    private $_connexionCom;

    public function getConnexionCom()
    {
        if (null == $this->_connexionCom) {
            $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }

        return $this->_connexionCom;
    }

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $organismesArray = [];
        $dateDebAction = date('Y-m-d H:i:s');
        if (!Atexo_CurrentUser::isConnected()) {
            // TODO
            //dd($this->getApplication()->getModule("auth")->getReturnUrl());
            //return $this->response->redirect("index.php?page=Entreprise.EntrepriseHome");
        }

        if (!$this->isPostBack) {
            $this->etapeConnection();

            $organismes = Atexo_Organismes::retrieveOrganismes(true);
            $organismesArray[0] = '--- '.Prado::localize('ENTITE_PUBLIQUE').' ---';
            foreach ($organismes as $oneOrg) {
                $organismesArray[$oneOrg->getAcronyme()] = $oneOrg->getDenominationOrgTraduit();
            }
            $this->organismeAcronyme->setDataSource($organismesArray);
            $this->organismeAcronyme->dataBind();

            // Si le lien est celui du mail, on preselectionne l'organisme
            if (isset($_GET['orgAcronym'])) {
                $this->organismeAcronyme->setSelectedValue(Atexo_Util::atexoHtmlEntities($_GET['orgAcronym']));
            }

            $detail = 'Etape de Connexion.';
            Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), $detail, date('Y-m-d H:i:s'));
        }
    }

    public function etapeConnection()
    {
        $this->etapeConnection->setVisible(true);
        $this->etapeParticipation->setVisible(false);
        $this->etapeSuivi->setVisible(false);
        $this->pageNonAccessible->setVisible(false);
    }

    public function loginEntreprise($sender, $param)
    {
        $ref = $this->refEnchere->Text;
        $pass = $this->passwordEnchere->Text;
        $organismeAcronyme = $this->organismeAcronyme->getSelectedValue();

        $entreprise = $this->getEntreprise($ref, $pass, $organismeAcronyme);
        if (null == $entreprise) {
            $param->IsValid = false;
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";

            return false;
        }
    }

    /**
     * Permet de chercher une entreprise participant à l'enchère par des critères bien donnés.
     *
     * @param string    $refUtilisateur: référence utilisateur de l'enchère
     * @param organisme $organisme:      l'organisme concerné
     * @param $passEntreprise $organisme: mot de passe de connexion de l'entreprise
     * @return: retourne l'entreprise correspondant aux critères
     */
    public function getEntreprise($refUtilisateur, $passEntreprise, $arcronymeOrganisme)
    {
        $enchereEntreprisePmiQuery = new CommonEnchereEntreprisePmiQuery(Atexo_Config::getParameter('COMMON_DB'));
        $entreprises = $enchereEntreprisePmiQuery->recupererEntrepriseEnchere($refUtilisateur, $arcronymeOrganisme, $passEntreprise);
        if (is_array($entreprises) && count($entreprises) && $entreprises[0] instanceof CommonEnchereEntreprisePmi) {
            return $entreprises[0];
        }

        return null;
    }

    public function etapeParticipation()
    {
        if ($this->passwordEnchereValid->IsValid) {
            // Recuperation des informations de l'enchere / Connection de l'entreprise
            $ref = $this->refEnchere->Text;
            $pass = $this->passwordEnchere->Text;
            $organismeAcronyme = $this->organismeAcronyme->getSelectedValue();
            $this->setViewState('acronymeOrganisme', $organismeAcronyme);

            $entreprise = $this->getEntreprise($ref, $pass, $organismeAcronyme);
            $enchere = $entreprise->getCommonEncherePmi($this->getConnexionCom());

            if (null == $enchere) {
                $this->etapeConnection();
                $this->etapeParticipation->setVisible(false);
                $this->etapeSuivi->setVisible(false);
                $this->pageNonAccessible->setVisible(false);

                return;
            }

            if (!$enchere->isAccessible()) {
                $this->etapeConnection->setVisible(false);
                $this->etapeParticipation->setVisible(false);
                $this->etapeSuivi->setVisible(false);
                $this->pageNonAccessible->setVisible(true);

                $this->refEnchereNonAccessible->Text = $enchere->getReferenceutilisateur();

                return;
            }

            $this->setViewState('enchere', $enchere);
            $this->setViewState('entreprise', $entreprise);

            $this->etapeConnection->setVisible(false);
            $this->etapeParticipation->setVisible(true);
            $this->etapeSuivi->setVisible(false);
            $this->pageNonAccessible->setVisible(false);

            // Affichage des informations de l'enchere
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $enchereUpdated = CommonEncherePmiPeer::retrieveByPK($enchere->getId(), $connexionCom);

            $status = $enchereUpdated->getStatusEnchere();
            $this->statusLabel->Text = $status['text'];
            $this->statusLabel->CssClass = $status['css'];

            $tempsRestant = $enchereUpdated->getTsTempsRestant();
            $this->timeLeft->Text = $tempsRestant['text'];
            $this->timeLeft->CssClass = $tempsRestant['css'];
            $this->dateDebut->Text = $enchereUpdated->getDatedebut();
            $this->dateFinPrev->Text = $enchereUpdated->getDatefin();
        }
    }

    public function etapeSuivi()
    {
        $enchere = $this->getEnchere();

        $this->etapeConnection->setVisible(false);
        $this->etapeParticipation->setVisible(false);
        $this->etapeSuivi->setVisible(true);
        $this->pageNonAccessible->setVisible(false);
        $this->erreurEnvoieEncherePanel->setVisible(true);

        // Liste des references de l'enchere
        $references = $enchere->getCommonEnchereReferences(null, $this->getConnexionCom());
        $this->setViewState('references', $references);

        $this->repeaterReferenceMonOffre->setDataSource($references);
        $this->repeaterReferenceMonOffre->dataBind();

        // Si l'entreprise connectee n'a pas encore fait d'offre, on force la premiere
        // selon les valeurs initiales indiquees lors de la creation de l'enchere...
        $entreprise = $this->getViewState('entreprise');

        $this->updateTableauSuivi($enchere, $references);
    }

    public function getObjetEnchere()
    {
        $enchere = $this->getViewState('enchere');

        return $enchere->getObjet();
    }

    public function getRefEnchere()
    {
        $enchere = $this->getViewState('enchere');

        return $enchere->getCommonReferenceutilisateur();
    }

    public function getIdEntrepriseConnectee()
    {
        $entreprise = $this->getViewState('entreprise');

        return $entreprise->getId();
    }

    /**
     * Enter description here...
     *
     * @return CommonEncherePmi
     */
    public function getEnchere()
    {
        $enchere = $this->getViewState('enchere');

        return $enchere;
    }

    public function isListeCandidats()
    {
        $enchere = $this->getViewState('enchere');
        // TEST :
        //$enchere = CommonEncherePmiPeer::retrieveByPK($enchere->getId());
        if ('1' == $enchere->getListecandidatsvisible()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateTableauSuivi(CommonEncherePmi $enchere, $references)
    {
        $lastOffre = null;
        // TEST :
        if ('1' == $enchere->getMeilleurnotehaute()) {
            $tri = 'DESC';
        } else {
            $tri = 'ASC';
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        // Recuperations des dernieres offres faites par les entreprises
        if ('1' == $enchere->getListecandidatsvisible() && '1' == $enchere->getRangvisible()) {
            $lastOffre = CommonEnchereOffre::getLastOffreOrdered($enchere->getId(), $tri, $enchere->getOrganisme());
        } elseif ('1' == $enchere->getListecandidatsvisible() && '0' == $enchere->getRangvisible()) {
            $lastOffre = CommonEnchereOffre::getLastOffreUnOrdered($enchere->getId(), $enchere->getOrganisme());
        } elseif ('0' == $enchere->getListecandidatsvisible() && '1' == $enchere->getMeilleureoffrevisible()) {
            $lastOffre = CommonEnchereOffre::getBestAndMyOffre($enchere->getId(), $this->getIdEntrepriseConnectee(), '1' == $enchere->getRangvisible(), $tri, $enchere->getOrganisme());
        } elseif ('0' == $enchere->getListecandidatsvisible() && '0' == $enchere->getMeilleureoffrevisible()) {
            $lastOffre = CommonEnchereOffre::getMyOffre($enchere->getId(), $this->getIdEntrepriseConnectee(), '1' == $enchere->getRangvisible(), $enchere->getOrganisme(), $tri);
        }

        if ((is_countable($lastOffre) ? count($lastOffre) : 0) < 1) {
            $this->aucuneOffre->setVisible(true);
        } else {
            $this->aucuneOffre->setVisible(false);
        }

        $this->repeaterOffres->setDataSource($lastOffre);
		$this->repeaterOffres->dataBind();


        //$this->repeaterReferenceOffre->setDataSource($references);
        //$this->repeaterReferenceOffre->dataBind();



    }

    public function updateStatus($enchere)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $enchereUpdated = CommonEncherePmiPeer::retrieveByPK($enchere->getId(), $connexionCom);

        // Affichage des informations de l'enchere
        $this->dateDebutSuivi->Text = date('d/m/Y H:i:s', strtotime($enchereUpdated->getDatedebut()));
        $this->dateFinPrevSuivi->Text = date('d/m/Y H:i:s', strtotime($enchereUpdated->getDatefin()));

        $status = $enchereUpdated->getStatusEnchere();
        $this->statusLabelSuivi->Text = $status['text'];
        $this->statusLabelSuivi->CssClass = $status['css'];

        $tempsRestant = $enchereUpdated->getTsTempsRestant();
        $this->timeLeftSuivi->Text = $tempsRestant['text'];
        $this->timeLeftSuivi->CssClass = $tempsRestant['css'];

        $this->envoyerMonOffre->setEnabled($enchereUpdated->isBidAllowed());

        $isCloturee = $enchereUpdated->isCloturee();
        //$this->chiffrerMonOffre->setVisible($isCloturee);
        $this->envoyerMonOffre->setVisible(!$isCloturee);

        $enchereUpdated->displayDateAccessible() ? $this->infosAccessiblilite->setDisplay('Dynamic') : $this->infosAccessiblilite->setDisplay('None');
        $this->dateLimiteAccessible->Text = $enchereUpdated->getDateLimiteAccessible();
        $this->msgAcheteur->Text = (new Atexo_Config())->toPfEncoding($enchereUpdated->getCommentaire());
    }

    public function updateStatusEntreprise(CommonEnchereEntreprisePmi $entreprise)
    {
        $isoDateNow = strftime('%Y-%m-%d %H:%M:%S', time());
        $entreprise->setDateping($isoDateNow);
        $entreprise->setNomagentconnecte(Atexo_CurrentUser::getFirstName().' '.Atexo_CurrentUser::getLastName());
        $entreprise->save($this->getConnexionCom());
    }

    public function getNbReferences()
    {
        $enchere = $this->getViewState('enchere');

        return $enchere->countCommonEnchereReferences(null, false, $this->getConnexionCom());
    }

    public function simulerNote($sender, $param)
    {
        $references = $this->getViewState('references');
        $entreprise = $this->getViewState('entreprise');
        $enchere = $this->getViewState('enchere');

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        //$this->getViewState("acronymeOrganisme");
        $enchereOffreReferences = [];
        foreach ($this->repeaterReferenceMonOffre->getItems() as $offreReference) {
            $valeurRef = str_ireplace(',', '.', $offreReference->valeurRef->Text);
            $enchereOffreReference = new CommonEnchereOffreReference();
            $c = new Criteria();
            $c->add(CommonEnchereReferencePeer::ID, $offreReference->idRef->Value, Criteria::EQUAL);
            $c->add(CommonEnchereReferencePeer::ORGANISME, $this->getViewState('acronymeOrganisme'), Criteria::EQUAL);
            $enchereReferences = CommonEnchereReferencePeer::doSelect($c, $this->getConnexionCom()); //$enchere->getCommonEnchereReferences($c, $connexionCom);

            $valeurRef = str_ireplace(',', '.', $offreReference->valeurRef->Text);
            $enchereOffreReference->setCommonEnchereReference($enchereReferences[0]);
            $enchereOffreReference->setValeur($valeurRef);
            $enchereOffreReference->setOrganisme($this->getViewState("acronymeOrganisme"));
            $enchereOffreReferences[] = $enchereOffreReference;
        }

        // Calcul des notes
        $totalIndicatifCandidat = 0;
        $totalCandidat = 0;
        $noteEnchereTotalCandidat = 0;
        $noteGlobaleCandidat = 0;
        foreach ($enchereOffreReferences as $enchereOffreReference) {
            $reference = $enchereOffreReference->getCommonEnchereReference($connexionCom);
            $valeurReferenceANoterCandidat = $enchereOffreReference->getValeur() * $reference->getQuantite();

            // Calcul total Candidat (que les montants, a comparer au montant de reserve...)
            if (0 == strcmp($reference->getIsmontant(), '1')) {
                $totalCandidat += $valeurReferenceANoterCandidat;
            }

            // Calcul total indicatif Candidat
            $totalIndicatifCandidat += $valeurReferenceANoterCandidat;

            // Bareme applicable a la reference
            // Si bareme prix reference
            if (strcmp($reference->getTypebaremereference(),
                    Atexo_Config::getParameter('BAREME_REFERENCE_PRIX_REFERENCE')) == 0) {
                $noteReferenceCandidat = $valeurReferenceANoterCandidat;
            } // Si bareme relatif
            else if (strcmp($reference->getTypebaremereference(),
                    Atexo_Config::getParameter('BAREME_REFERENCE_RELATIF')) == 0) {
                $valeurReferenceMinimal = $reference->getValeurReferenceMinimal($entreprise->getId(),
                    $entreprise->getOrganisme());

                // Si la valeur de l'offre sur la reference est meilleur (ie plus petite) || premiere offre
                // Alors c'est cette offre qui doit obtenir la noteMaxBaremeRelatif
                if ($enchereOffreReference->getValeur() < $valeurReferenceMinimal || null == $valeurReferenceMinimal) {
                    $noteReferenceCandidat = $reference->getNotemaxbaremerelatif();
                }
                // Sinon la note est calculer par rapport a la meilleure
                else {
                    $noteReferenceCandidat =
                        ($reference->getNotemaxbaremerelatif() * $valeurReferenceMinimal) / $enchereOffreReference->getValeur();
                }

            }

            // Si bareme par tranche
            else if (strcmp($reference->getTypebaremereference(),
                    Atexo_Config::getParameter('BAREME_REFERENCE_TRANCHES')) == 0){
                $tranches = $reference->getCommonEncheretranchebaremereferences(null, $connexionCom);
                foreach ($tranches as $tranche) {
                    if ($tranche->getBorneinf() < $valeurReferenceANoterCandidat && $valeurReferenceANoterCandidat <= $tranche->getBornesup()) {
                        $noteReferenceCandidat = $tranche->getNote();
                    }
                }
            }

            // Calcul de la noteEnchereTotalCandidat dans le cas somme ponderee (sinon c'est apres en dehors de la boucle)
            if (0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_SOMME_PONDEREE'))) {
                $noteEnchereTotalCandidat += $noteReferenceCandidat * $reference->getPonderationnotereference();
            }
        } /* fin foreach ($enchereOffreReferences as $enchereOffreReference) */

        // Calcul de la noteEnchereTotalCandidat dans les autres cas (base sur le TIC...):
        // Si Bareme prix reference
        if (0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_PRIX_REFERENCE'))) {
            $noteEnchereTotalCandidat = $totalIndicatifCandidat;
        }

        // Si Bareme relatif :
        elseif (0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_RELATIF'))) {
            $TICmin = CommonEnchereOffre::getMinTIC($enchere->getId(), $entreprise->getId(), $enchere->getOrganisme());
            $TICmin = min($TICmin, $totalIndicatifCandidat);
            $noteEnchereTotalCandidat = $enchere->getNotemaxbaremerelatif() * $TICmin / $totalIndicatifCandidat;
        }

        // Si Bareme par tranche :
        elseif (0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES'))) {
            $tranches = $enchere->getCommonEnchereTranchesBaremeNETCs(null, $connexionCom);
            foreach ($tranches as $tranche) {
                if ($tranche->getBorneinf() < $totalIndicatifCandidat && $totalIndicatifCandidat <= $tranche->getBornesup()) {
                    $noteEnchereTotalCandidat = $tranche->getNote();
                }
            }
        }

        // Calcul de la note globales :
        // Si Bareme somme ponderee (par coeff a et b) des 2 notes NETC et Note Technique du Candidat
        if (0 == strcmp($enchere->getTypebaremeenchereglobale(), Atexo_Config::getParameter('BAREME_GLOBAL_SOMME_PONDEREE'))) {
            $noteGlobaleCandidat = $noteEnchereTotalCandidat * $enchere->getCoeffa() + $entreprise->getNotetechnique() * $enchere->getCoeffb();
        }
        // Si Bareme corrige par note technique
        elseif (0 == strcmp($enchere->getTypebaremeenchereglobale(), Atexo_Config::getParameter('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE'))) {
            $noteGlobaleCandidat = $noteEnchereTotalCandidat * (1 - $enchere->getCoeffc() * $entreprise->getNotetechnique());
        } else {
            $noteGlobaleCandidat = null;
        }

        if ($noteGlobaleCandidat != null) {
            $this->noteSimulee->Text = number_format($noteGlobaleCandidat, 2, ',', ' ');
        } else {
            $this->noteSimulee->Text = 'Erreur';
        }
        $this->updateTableauSuivi($enchere, $references);
    }

    public function envoyerOffre($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')); //$this->getViewState("acronymeOrganisme")
        $this->CallbackClient->callClientFunction('hideValidationSummary');

        $references = $this->getViewState('references');
        $entreprise = $this->getViewState('entreprise');
        $enchereBase = $this->getViewState('enchere');

        if ('1' == $enchereBase->getMeilleurnotehaute()) {
            $tri = 'DESC';
        } else {
            $tri = 'ASC';
        }

        $enchere = CommonEncherePmiPeer::retrieveByPK($enchereBase->getId(), $connexionCom);

        Prado::log('Enchere offre soumise par entreprise id= '.$entreprise->getId(), TLogger::INFO, 'Atexo.pages.entreprise.EntrepriseParticipationEnchere.php');

        // On verifie le status de l'enchere
        if (!$enchere->isBidAllowed()) {
            $this->erreurEnvoieEnchere->Text = Prado::localize('IMPOSSIBLE_ENVOYER_OFFRE');
            $this->CallbackClient->callClientFunction('displayErrorEnchere', true);
            Prado::log('Enchere offre soumise par entreprise id= '.$entreprise->getId().' Enchere non autorisee', TLogger::INFO, 'Atexo.pages.entreprise.EntrepriseParticipationEnchere.php');

            return;
        }

        $enchereOffreReferences = [];
        //$champsVides = false;
        foreach($this->repeaterReferenceMonOffre->getItems() as $offreReference){
            /*// Si l'un des champs est vide, on arrete tout et on reviens a l'ecran precedent
            if ($offreReference->valeurRef->Text == "") {
                $this->erreurEnvoieEnchere->Text = Atexo_Util::toUtf8(Prado::localize('VOUS_DEVEZ_ENTRER_TOUTES_VALEURS'));
	            $this->CallbackClient->callClientFunction('displayErrorEnchere', true);
	            return;
            }*/
            $valeurRef = str_ireplace(' ', '', $offreReference->valeurRef->Text);
            $valeurRef = str_ireplace(',', '.', $valeurRef);
            $value = TPropertyValue::ensureFloat($valeurRef);
            $enchereOffreReference = new CommonEnchereOffreReference();

            $c = new Criteria();
            $c->add(CommonEnchereReferencePeer::ID, $offreReference->idRef->Value, Criteria::EQUAL);
            $c->add(CommonEnchereReferencePeer::ORGANISME, $this->getViewState("acronymeOrganisme"), Criteria::EQUAL);
            $enchereReferences = CommonEnchereReferencePeer::doSelect($c,
                $this->getConnexionCom());//$enchere->getCommonEncherereferences($c, $connexionCom);
            $enchereReference = $enchereReferences[0];

            //Si lib1 est vide, on affiche un message d'erreur
            if ($valeurRef == "") {
                $this->erreurEnvoieEnchere->Text = Prado::localize('MESSAGE_ERROR_REFERENCE_VIDE') .
                    ' ' .
                    $enchereReference->getLibelle();
	            $this->CallbackClient->callClientFunction('displayErrorEnchere', true);
	            return;
            }
            //Si le champ lib1 n'est pas decimal, on affiche un message d'erreur
            if (!is_numeric($valeurRef)) {
                $this->erreurEnvoieEnchere->Text = $enchereReference->getLibelle() .
                    ' '.
                    Prado::localize('MESSAGE_ERROR_NOMBRE_DECIMAL');
                $this->CallbackClient->callClientFunction('displayErrorEnchere', true);

                return;
            }

            // Verifier que le pas min/max est respecte
            $bestEnchereOffre = CommonEnchereOffre::getBestOffre($enchere->getId(), $tri, $enchere->getOrganisme());

            if ($bestEnchereOffre) {
                $c = new Criteria();
                $bestOffreReference = $bestEnchereOffre->getCommonEnchereOffreReferences($c, $this->getConnexionCom());
                if (isset($bestOffreReference[0])) {
                    $bestValue = $bestOffreReference[0]->getValeur();
                    if ($bestValue != $value) {
                        if (abs($bestValue - $value) < $enchereReference->getPasmin()) {
                            $this->erreurEnvoieEnchere->Text = Prado::localize('PAS_MINIMUM_NON_RESPECTE').' '.$enchereReference->getLibelle();
                            $this->CallbackClient->callClientFunction('displayErrorEnchere', true);
                            Prado::log('Enchere offre soumise par entreprise id= '.$entreprise->getId().' Meilleur valeur ='.$bestValue.' Valeur soumise = '.$value.' Pas min = '.$enchereReference->getPasmin(), TLogger::INFO, 'Atexo.pages.entreprise.EntrepriseParticipationEnchere.php');

                            return;
                        }
                        if (0 != $enchereReference->getPasmax() && abs($bestValue - $value) > $enchereReference->getPasmax()) {
                            $this->erreurEnvoieEnchere->Text = Prado::localize('PAS_MAXIMUM_NON_RESPECTE').' '.$enchereReference->getLibelle();
                            $this->CallbackClient->callClientFunction('displayErrorEnchere', true);
                            Prado::log('Enchere offre soumise par entreprise id= '.$entreprise->getId().' Meilleur valeur ='.$bestValue.' Valeur soumise = '.$value.' Pas max = '.$enchereReference->getPasmax(), TLogger::INFO, 'Atexo.pages.entreprise.EntrepriseParticipationEnchere.php');

                            return;
                        }
                    } else {
                        $this->erreurEnvoieEnchere->Text = Prado::localize('PAS_MINIMUM_NON_RESPECTE').' '.$enchereReference->getLibelle();
                        $this->CallbackClient->callClientFunction('displayErrorEnchere', true);

                        return;
                    }
                }
            } else {
                // OK c'est la premiere offre...
                // On se base alors sur la valeur de depart de reference afin de verifier le pas min/max
                if (1 == $enchereReference->getValeurdepartcommune()) {
                    $valeurDepart = $enchereReference->getValeurdepart();
                } else {
                    $c = new Criteria();
                    $c->add(CommonEnchereValeursInitialesPeer::IDENCHEREENTREPRISE, $entreprise->getId(), Criteria::EQUAL);
                    $c->add(CommonEnchereValeursInitialesPeer::ORGANISME, $entreprise->getOrganisme(), Criteria::EQUAL);
                    $c->add(CommonEnchereValeursInitialesPeer::IDENCHEREREFERENCE, $offreReference->idRef->Value, Criteria::EQUAL);
                    $valeurDepart = CommonEnchereValeursInitialesPeer::doSelectOne($c, $this->getConnexionCom());
                    $valeurDepart = $valeurDepart->getValeur();
                }
                if (abs($valeurDepart - $value) < $enchereReference->getPasmin()) {
                    $this->erreurEnvoieEnchere->Text = Prado::localize('PAS_MINIMUM_NON_RESPECTE').' '.$enchereReference->getLibelle();
                    $this->CallbackClient->callClientFunction('displayErrorEnchere', true);

                    return;
                }
                if (0 != $enchereReference->getPasmax() && abs($valeurDepart - $value) > $enchereReference->getPasmax()) {
                    $this->erreurEnvoieEnchere->Text = Prado::localize('PAS_MAXIMUM_NON_RESPECTE').' '.$enchereReference->getLibelle();
                    $this->CallbackClient->callClientFunction('displayErrorEnchere', true);

                    return;
                }
            }

            $enchereOffreReference->setCommonEnchereReference($enchereReference);
            $enchereOffreReference->setValeur($value);
            $enchereOffreReference->setOrganisme($this->getViewState('acronymeOrganisme'));
            $enchereOffreReferences[$enchereOffreReference->getCommonEnchereReference($this->getConnexionCom())->getId()] = $enchereOffreReference;
        }

        // Calcul des notes
        $totalIndicatifCandidat = 0;
        $totalCandidat = 0;
        $noteEnchereTotalCandidat = 0;
        $noteGlobaleCandidat = 0;
        $updateNotesNeeded = false;
        $valeureMin = [];
        foreach ($enchereOffreReferences as $enchereOffreReference) {
            $reference = $enchereOffreReference->getCommonEnchereReference($connexionCom);
            $valeurReferenceANoterCandidat = $enchereOffreReference->getValeur() * $reference->getQuantite();

            // Calcul total Candidat (que les montants, a comparer au montant de reserve...)
            if (0 == strcmp($reference->getIsmontant(), '1')) {
                $totalCandidat += $valeurReferenceANoterCandidat;
            }

            // Calcul total indicatif Candidat
            $totalIndicatifCandidat += $valeurReferenceANoterCandidat;

            // Bareme applicable a la reference
            // Si bareme prix reference
            if (0 == strcmp($reference->getTypebaremereference(), Atexo_Config::getParameter('BAREME_REFERENCE_PRIX_REFERENCE'))) {
                $noteReferenceCandidat = $valeurReferenceANoterCandidat;
            }

            // Si bareme relatif
            elseif (0 == strcmp($reference->getTypebaremereference(), Atexo_Config::getParameter('BAREME_REFERENCE_RELATIF'))) {
                $valeurReferenceMinimal = $reference->getValeurReferenceMinimal($entreprise->getId(), $entreprise->getOrganisme());

                // Si la valeur de l'offre sur la reference est meilleur (ie plus petite)
                // Alors c'est cette offre qui doit obtenir la noteMaxBaremeRelatif
                // Et on doit mettre a jours toutes les notes ($noteReferenceCandidat) des autres candidats
                if ($enchereOffreReference->getValeur() < $valeurReferenceMinimal || null == $valeurReferenceMinimal) {
                    $noteReferenceCandidat = $reference->getNotemaxbaremerelatif();
                    $updateNotesNeeded = true;
                    $valeureMin[$reference->getId()] = $enchereOffreReference->getValeur();
                //EnchereOffrePeer::updateRelativesNotesReferences($enchere, $tri, $entreprise->getId(), $enchereOffreReference->getValeur(), $connexionCom);
                }

                // Sinon la note est calculer par rapport a la meilleure et pas besoins d'updater les notes
                else {
                    $noteReferenceCandidat = $reference->getNotemaxbaremerelatif() * $valeurReferenceMinimal / $enchereOffreReference->getValeur();
                    $valeureMin[$reference->getId()] = $valeurReferenceMinimal;
                }
            }

            // Si bareme par tranche
            elseif (0 == strcmp($reference->getTypebaremereference(), Atexo_Config::getParameter('BAREME_REFERENCE_TRANCHES'))) {
                $tranches = $reference->getCommonEnchereTrancheBaremeReferences(null, $connexionCom);
                foreach ($tranches as $tranche) {
                    if ($tranche->getBorneinf() < $valeurReferenceANoterCandidat && $valeurReferenceANoterCandidat <= $tranche->getBornesup()) {
                        $noteReferenceCandidat = $tranche->getNote();
                    }
                }
            }

            // Calcul de la noteEnchereTotalCandidat dans le cas somme ponderee
            // (sinon c'est apres en dehors de la boucle)
            if (0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_SOMME_PONDEREE'))) {
                $noteEnchereTotalCandidat += $noteReferenceCandidat * $reference->getPonderationnotereference();
            }
        } /* fin foreach ($enchereOffreReferences as $enchereOffreReference) */

        // Calcul de la noteEnchereTotalCandidat dans les autres cas (base sur le TIC...):
        // Si Bareme prix reference
        if (0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_PRIX_REFERENCE'))) {
            $noteEnchereTotalCandidat = $totalIndicatifCandidat;
        }

        // Si Bareme relatif :
        elseif (0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_RELATIF'))) {
            $TICmin = CommonEnchereOffre::getMinTIC($enchere->getId(), $entreprise->getId(), $enchere->getOrganisme(), $connexionCom);
            $TICmin = min($TICmin, $totalIndicatifCandidat);
            $noteEnchereTotalCandidat = $enchere->getNotemaxbaremerelatif() * $TICmin / $totalIndicatifCandidat;
        }

        // Si Bareme par tranche :
        elseif (0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES'))) {
            $tranches = $enchere->getCommonEnchereTranchesBaremeNETCs(null, $connexionCom);
            foreach ($tranches as $tranche) {
                if ($tranche->getBorneinf() <= $totalIndicatifCandidat && $totalIndicatifCandidat <= $tranche->getBornesup()) {
                    $noteEnchereTotalCandidat = $tranche->getNote();
                }
            }
        }

        // Calcul de la note globales :
        // Si Bareme somme ponderee (par coeff a et b) des 2 notes NETC et Note Technique du Candidat
        if (0 == strcmp($enchere->getTypebaremeenchereglobale(), Atexo_Config::getParameter('BAREME_GLOBAL_SOMME_PONDEREE'))) {
            $noteGlobaleCandidat = $noteEnchereTotalCandidat * $enchere->getCoeffa() + $entreprise->getNotetechnique() * $enchere->getCoeffb();
        }
        // Si Bareme corrige par note technique
        elseif (0 == strcmp($enchere->getTypebaremeenchereglobale(), Atexo_Config::getParameter('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE'))) {
            $noteGlobaleCandidat = $noteEnchereTotalCandidat * (1 - $enchere->getCoeffc() * $entreprise->getNotetechnique());
        } else {
            $noteGlobaleCandidat = null;
        }

        // TEST
//        echo "<br> TC = " . $totalCandidat;
//        echo "<br> TIC = " . $totalIndicatifCandidat;
//        echo "<br> NETC = " . $noteEnchereTotalCandidat;
//        echo "<br> NGC = " . $noteGlobaleCandidat;

        try {
            if ($updateNotesNeeded && 0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_SOMME_PONDEREE'))) {
                CommonEnchereOffre::updateRelativesNotesReferences($enchere, $tri, $entreprise->getId(), $noteGlobaleCandidat, $valeureMin, $entreprise->getOrganisme(), $connexionCom);
            }

            if ($enchere->getMeilleureenchereobligatoire()) {
                $bestOffre = CommonEnchereOffre::getBestOffre($enchere->getId(), $tri, $enchere->getOrganisme(), $connexionCom);
                if (null != $bestOffre) {
                    $bestNgc = $bestOffre->getValeurngc();
                    // Si la meilleure enchere est meilleure , on ne prend pas en compte ce bid
                    // Depend du type de classement !
                    if ('1' == $enchere->getMeilleurnotehaute()) {
                        $meilleureOffre = -1;
                    } else {
                        $meilleureOffre = 1;
                    }
                    if ($meilleureOffre * $bestNgc < $meilleureOffre * $noteGlobaleCandidat) {
                        $connexionCom->rollBack();
                        throw new Atexo_Exception(Prado::localize('MEILLEUR_OFFRE_OBLIGATOIRE'));
                    }
                } else {
                    // C'est la premiere donc on continue...
                }
            }

            // Calcul du rang :
            // $rang = EnchereoffrePeer::getSimuRang($entreprise->getId(), $enchere->getId(), $noteGlobaleCandidat, $connexionCom);

            // Si valide, on sauvegarde en base les objets EnchereOffreReference et l'objet EnchereOffre

            $enchereOffre = new CommonEnchereOffre();
            $enchereOffre->setOrganisme($this->getViewState('acronymeOrganisme'));
            $enchereOffre->setDate(strftime('%Y-%m-%d %H:%M:%S'));
            $enchereOffre->setValeurnetc($noteEnchereTotalCandidat);
            $enchereOffre->setValeurtic($totalIndicatifCandidat);
            $enchereOffre->setValeurtc($totalCandidat);
            $enchereOffre->setValeurngc($noteGlobaleCandidat);
            //         $enchereOffre->setRang($rang);
            $enchereOffre->setCommonEncherePmi($enchere);
            $enchereOffre->setCommonEnchereEntreprisePmi($entreprise);
            $enchereOffre->save($connexionCom);

            foreach ($enchereOffreReferences as $enchereOffreReference) {
                $enchereOffreReference->setOrganisme($this->getViewState('acronymeOrganisme'));
                $enchereOffreReference->setCommonEnchereOffre($enchereOffre);
                $enchereOffreReference->save($connexionCom);
            }

            // Si delai de prolongation, et l'enchere interviens "tard" on la prolonge :
            if (null != $enchere->getDelaiprolongation() && 0 != $enchere->getDelaiprolongation()) {
                $tsNow = time();
                $tsDateFin = strtotime($enchere->getDatefin());
                if (($tsDateFin - $tsNow) < ($enchere->getDelaiprolongation() * 60)) {
                    $newTsDateFin = $tsNow + $enchere->getDelaiprolongation() * 60;
                    $enchere->setDatefin(strftime('%Y-%m-%d %H:%M:%S', $newTsDateFin));
                    $enchere->setOrganisme($this->getViewState('acronymeOrganisme'));
                    $enchere->save($connexionCom);
                }
            }

            // Si Bareme relatif pour l'enchere : il faut mettre a jour toutes
            // les notes Global des Candidats (et netc) pour que le classement soit correct
            if (0 == strcmp($enchere->getTypebaremenetc(), Atexo_Config::getParameter('BAREME_ENCHERE_RELATIF'))) {
                $TICmin = CommonEnchereOffre::getMinTIC($enchere->getId(), $entreprise->getId(), $enchere->getOrganisme(), $connexionCom);
                // Si le TIC est plus petit que le minimum des autres offres, il faut mettre a jour toutes
                // les notes Global des Candidats (et netc) pour que le classement soit correct
                if ($totalIndicatifCandidat < $TICmin) {
                    $TICmin = $totalIndicatifCandidat;
                    CommonEnchereOffre::updateRelativesNotes($enchere, $entreprise->getId(), $TICmin, $entreprise->getOrganisme(), $connexionCom);
                }
            }
        } catch (Atexo_Exception $e) {
            $connexionCom->rollback();
            $this->erreurEnvoieEnchere->Text = $e->getMessage();
            $this->CallbackClient->callClientFunction('displayErrorEnchere', true);

            return;
            //echo "erreur<br>";
        } catch (\Exception $e) {
            $connexionCom->rollback();
            throw $e;
        }

        // Mise a jour de la vue et de la note simulee
        $this->updateTableauSuivi($enchere, $references);
        if ($noteGlobaleCandidat != null) {
            $this->noteSimulee->Text = number_format(
                $noteGlobaleCandidat,
                2,
                ',',
                ' '
            );
        } else {
            $this->noteSimulee->Text = 'Erreur';
        }

        if ($param) {
            $this->apRepeaterOffres->render($param->getNewWriter());
            $this->apEnvoyerOffre->render($param->getNewWriter());
        }

        $this->CallbackClient->callClientFunction('displayErrorEnchere', false);
    }

    public function updateEnchere($sender, $param)
    {
        $references = $this->getViewState('references');
        $enchere = $this->getViewState('enchere');
        $entreprise = $this->getViewState('entreprise');

        $this->updateTableauSuivi($enchere, $references);
        $this->updateStatus($enchere);
        $this->updateStatusEntreprise($entreprise);

        $this->infosEnchere->render($param->getNewWriter());
        $this->apRepeaterOffres->render($param->getNewWriter());
        $this->apEnvoyerOffre->render($param->getNewWriter());
    }

    public function actionBouttonRetour()
    {
        $this->response->redirect('?page=Entreprise.EntrepriseAccueilAuthentifie');
    }

    public function getLibelleEntiteAssocie()
    {
        $service = $this->getEnchere()->getService($this->Page->getConnexionCom());
        if ($service instanceof CommonService) {
            return $service->getLibelle();
        } else {
            $AcronymeOrganisme = $this->getViewState('acronymeOrganisme');
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($AcronymeOrganisme);
            if ($organisme instanceof CommonOrganisme) {
                return $organisme->getDenominationOrg();
            }
        }
    }

    public function getValeurInitialOffre($idRef)
    {
        $valeurRef = '';
        $entreprise = $this->getViewState('entreprise');
        if ($this->getEnchere()->isBidAllowed()) {
            $reference = CommonEnchereReferenceQuery::create()
                ->filterByOrganisme($this->getViewState('acronymeOrganisme'))
                ->filterById($idRef)
                ->findOne($this->getConnexionCom());

            //Cas des valeurs initiales communes a chaque entreprise :
            if ('1' == $reference->getValeurdepartcommune() && null != $reference->getValeurdepart() && 0 != $reference->getValeurdepart()) {
                $valeurRef = $reference->getValeurdepart();
            } else {
                // Cas des valeurs propres a chaque entreprise
                $valeurInitiale = CommonEnchereValeursInitialesQuery::create()
                    ->filterByOrganisme($this->getViewState('acronymeOrganisme'))
                    ->filterByIdencherereference($idRef)
                    ->filterByIdenchereentreprise($entreprise->getId())
                    ->findOne($this->getConnexionCom());

                $valeurRef = $valeurInitiale->getValeur();
            }
        }
        return $valeurRef;
    }
}
