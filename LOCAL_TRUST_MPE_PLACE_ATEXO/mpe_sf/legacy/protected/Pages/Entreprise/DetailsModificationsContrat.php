<?php

namespace Application\Pages\Entreprise;

use App\Service\WebServicesSourcing;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonModificationContratPeer;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Class DetailsModificationsContrat.
 */
class DetailsModificationsContrat extends MpeTPage
{
    protected $connexion;
    protected $consultation;
    protected $contrat;
    public bool $_calledFrom = false;

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        $messageErreur = null;
        $logger = Atexo_LoggerManager::getLogger('contrat');

        if (isset($_GET['idContratEtalab'])) {
            $modifications = $this->getContratPublishedEtalabByUID(Atexo_Util::atexoHtmlEntities($_GET['idContratEtalab']));
            $this->fillEtalabRepeater($modifications);
        } elseif (isset($_GET['idContrat'])) {
            $idContrat = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['idContrat']));
            $this->connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $this->contrat = $contratQuery->getTContratTitulaireById($idContrat, $this->connexion);

            if ($this->contrat instanceof CommonTContratTitulaire) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $c = new Criteria();
                $c->add(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, $idContrat);
                $modifications = CommonModificationContratPeer::doSelect($c, $connexion);
                $this->fillRepeater($modifications);
                $this->setViewState('criteria', $c);
            } else {
                $logger->error("Le contrat dont l'id = ".$idContrat." n'existe pas");
                $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
            }
        } else {
            $logger->error("Erreur d'accès à la page DetailsModificationsContrat sans le parametre idContrat");
            $messageErreur = Prado::Localize('DEFINE_VOUS_N_AVEZ_PAS_DROIT_ACCEDER_PAGE');
        }
    }

    /**
     * @param null $pageSize
     */
    public function fillRepeater(array $modifications, bool $firstCalled = true, $pageSize = null): void
    {
        $nombreElement = count($modifications);

        if ($nombreElement >= 1) {
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);

            if ($firstCalled) {
                $this->nbrResultats->Text = $nombreElement;
                $this->setViewState('nombreElement', $nombreElement);
                $this->listeModificationContrat->setVirtualItemCount($nombreElement);
                $this->listeModificationContrat->setCurrentPageIndex(0);
                $this->nombrePageTop->Text = ceil($this->getViewState('nombreElement') / $this->listeModificationContrat->PageSize);
                $this->nombrePageBottom->Text = ceil($this->getViewState('nombreElement') / $this->listeModificationContrat->PageSize);
            }

            if (!is_null($pageSize)) {
                $this->nombrePageTop->Text = ceil($this->getViewState('nombreElement') / $pageSize);
                $this->nombrePageBottom->Text = ceil($this->getViewState('nombreElement') / $pageSize);
            }

            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->listeModificationContrat->DataSource = $modifications;
            $this->listeModificationContrat->DataBind();
        } else {
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
        }
    }

    /**
     * @param $modifications
     */
    public function fillEtalabRepeater($modifications)
    {
        $nombreElement = is_countable($modifications) ? count($modifications) : 0;
        $pagerVisibilty = false;
        if ($nombreElement >= 1) {
            $this->nbrResultats->Text = $nombreElement;
            $pagerVisibilty = true;

            $this->setViewState('nombreElement', $nombreElement);

            $this->nombrePageTop->Text = ceil($nombreElement / $this->listeModificationContratEtalab->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->listeModificationContratEtalab->PageSize);

            $this->listeModificationContratEtalab->setVirtualItemCount($nombreElement);
            $this->listeModificationContratEtalab->setCurrentPageIndex(0);

            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);

            $this->listeModificationContratEtalab->DataSource = $modifications;
            $this->listeModificationContratEtalab->DataBind();
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
        }

        $this->PagerBottom->setVisible($pagerVisibilty);
        $this->PagerTop->setVisible($pagerVisibilty);
    }

    /**
     * @param $idEtablissement
     *
     * @return Entreprise
     *
     * @throws PropelException
     */
    public function getNomAttributaire($idEtablissement)
    {
        $etablissementQuery = new CommonTEtablissementQuery();
        $etablissement = $etablissementQuery->findOneByIdEtablissement($idEtablissement);

        $entrepriseQuery = new CommonEntrepriseQuery();

        return $entrepriseQuery->findOneById($etablissement->getIdEntreprise())->getNom();
    }

    /**
     * @param $idEtablissement
     *
     * @return string
     *
     * @throws PropelException
     */
    public function getSiretEntreprise($idEtablissement)
    {
        $etablissementQuery = new CommonTEtablissementQuery();
        $etablissement = $etablissementQuery->findOneByIdEtablissement($idEtablissement);

        $entrepriseQuery = new CommonEntrepriseQuery();
        $entreprise = $entrepriseQuery->findOneById($etablissement->getIdEntreprise());

        return $entreprise->getSiren().$etablissement->getCodeEtablissement();
    }

    private function getContratPublishedEtalabByUID($numeroIdentificationUnique)
    {
        /** @var WebServicesSourcing $servicesSourcing */
        $servicesSourcing = Atexo_Util::getSfContainer()->get(WebServicesSourcing::class);
        $result = $servicesSourcing->getContent('getDecpContract', $numeroIdentificationUnique);

        return $result->modifications;
    }

    public function pageChanged($sender, $param)
    {
        $this->pagination('pageChanged', $sender, $param);
    }

    public function goToPage($sender, $param)
    {
        $this->pagination('goTo', $sender, $param);
    }

    public function changePagerLength($sender, $param)
    {
        $this->pagination('changePagerLength', $sender, $param);
    }

    public function pagination($type, $sender , $param)
    {
        $numPage = null;
        $pageSize = null;
        if ($type == 'goTo') {
            switch ($sender->ID) {
                case 'DefaultButtonTop':
                    $numPage = $this->numPageTop->Text;
                    break;
                case 'DefaultButtonBottom':
                    $numPage = $this->numPageBottom->Text;
                    break;
            }
            if (Atexo_Util::isEntier($numPage)) {
                if ($numPage >= $this->nombrePageTop->Text) {
                    $numPage = $this->nombrePageTop->Text;
                } elseif ($numPage <= 0) {
                    $numPage = 1;
                }
            }
            $gotToPage = $numPage;
            $this->listeModificationContrat->CurrentPageIndex = $numPage - 1;
            $this->setViewState('currentPageIndex', $this->listeModificationContrat->CurrentPageIndex);
        } elseif ($type == 'pageChanged') {
            $this->listeModificationContrat->CurrentPageIndex = $param->NewPageIndex;
            $this->setViewState('currentPageIndex', $this->listeModificationContrat->CurrentPageIndex);
        }

        if ($type == 'changePagerLength') {
            switch ($sender->ID) {
                case 'nombreResultatAfficherBottom':
                    $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                    break;
                case 'nombreResultatAfficherTop':
                    $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                    break;
            }
            $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
            $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
            $this->listeModificationContrat->PageSize = $pageSize;
        }

        $criteria = $this->getViewState('criteria');

        $offset = $this->getViewState('currentPageIndex') * $this->listeModificationContrat->PageSize;
        $limit = $this->listeModificationContrat->PageSize;

        if ($offset + $limit > $this->listeModificationContrat->VirtualItemCount) {
            $limit = $this->listeModificationContrat->VirtualItemCount - $offset;
        }

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $criteria->setOffset($offset);
        $criteria->setLimit($limit);
        $modifications = CommonModificationContratPeer::doSelect($criteria, $connexion);

        if ($type == 'goTo') {
            $this->numPageTop->Text = $gotToPage;
            $this->numPageBottom->Text = $gotToPage;
        } else {
            $this->numPageTop->Text = $this->getViewState('currentPageIndex') + 1;
            $this->numPageBottom->Text = $this->getViewState('currentPageIndex') + 1;
        }

        $this->fillRepeater($modifications, false, $pageSize);
    }
}
