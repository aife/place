<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_HistorisationMotDePasse;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_InscritHistorique;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Page de modification du compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntreprisePopupUpdateUser extends MpeTPage
{
    private $_inscrit;
    private ?\Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo $entreprise = null;

    /**
     * Modifie la valeur de [entreprise].
     *
     * @param Atexo_Entreprise_EntrepriseVo $value la valeur a mettre
     *
     * @return Atexo_Entreprise_EntrepriseVo
     *
     * @author Salwa DRISSI <salwa.drissi@atexo.com>
     * @copyright Atexo 2017
     */
    public function setEntreprise($value)
    {
        $this->setViewState('entreprise', $value);
        $this->entreprise = $value;

        return $value;
    }

    /**
     * Recupere la valeur du [entreprise].
     *
     * @return Atexo_Entreprise_EntrepriseVo la valeur courante [entreprise]
     *
     * @author Salwa DRISSI <salwa.drissi@atexo.com>
     * @copyright Atexo 2017
     */
    public function getEntreprise()
    {
        return $this->getViewState('entreprise');
    }

    public function setInscrit($value)
    {
        $this->setViewState('inscrit', $value);
        $this->_inscrit = $value;
    }

    public function getInscrit()
    {
        return $this->getViewState('inscrit');
    }

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    /**
     * Stocke le mode de saisie dans le viewState de la page
     * Valeurs possibles du mode de saisie :
     *     mode1 => Mode degradee sans message d'avertissement
     *     mode2 => Mode degradee avec message d'avertissement
     *     mode3 => Mode automatique utilisant le web service SGMAP.
     *
     * @param string $value la valeur a mettre
     *
     * @return void
     *
     * @author Salwa DRISSI <salwa.drissi@atexo.com>
     * @copyright Atexo 2017
     */
    public function setModeSaisie($value)
    {
        $this->setViewState('modeSaisie', $value);
    }

    /**
     * Recupere la valeur du mode de saisie stockée dans le viewState
     * Valeurs possibles du mode de saisie :
     *     mode1 => Mode degradee sans message d'avertissement
     *     mode2 => Mode degradee avec message d'avertissement
     *     mode3 => Mode automatique utilisant le web service SGMAP.
     *
     * @return string
     *
     * @author Salwa DRISSI <salwa.drissi@atexo.com>
     * @copyright Atexo 2017
     */
    public function getModeSaisie()
    {
        return $this->getViewState('modeSaisie');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $dateDebAction = date('Y-m-d H:i:s');
            $this->panelMessageErreur->setVisible(false);
            $this->PanelInscrit->setPostBack($this->IsPostBack);
            $this->PanelInscrit->CaseLogin->Checked = true;
            //$this->PanelInscrit->inscritSimple->Checked=true;
            $this->etablissements->setMode(3);
            $company = (new Atexo_Entreprise())->getEntrepriseVoFromEntreprise(EntreprisePeer::retrieveByPK(Atexo_CurrentUser::getIdEntreprise()));
            if ($company) {
                $this->setEntreprise($company);
                if ($this->isEntrepriseLocale()) {
                    $typeEntreprise = ($this->isEntrepriseLocale()) ? '1' : '2';
                    $mode = null;
                    Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($this->getSiren(), $typeEntreprise, $mode);
                    $this->setModeSaisie($mode);
                }
            }
            if ('ChangingUser' == $_GET['action']) {
                $isOurInscrit = false;
                $usersCurrentCompany = (new Atexo_Entreprise_Inscrit())->retrieveInscritByIdCompnay(Atexo_CurrentUser::getIdEntreprise());
                if ($usersCurrentCompany) {
                    foreach ($usersCurrentCompany as $oneInscrit) {
                        if ($oneInscrit->getId() == $_GET['id']) {
                            $isOurInscrit = true;
                        }
                    }
                    if (true === $isOurInscrit) {
                        $this->setInscrit((new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_Util::atexoHtmlEntities($_GET['id'])));
                        $this->PanelInscrit->setInscrit($this->getInscrit());
                    } else {
                        $this->PanelInscrit->setVisible(false);
                        $this->panelMessageErreur->setVisible(true);
                        $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_ERREUR_ACCEE'));
                    }
                }
            } elseif ('creationByATES' == $_GET['action']) {
                $company = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_CurrentUser::getIdEntreprise());
                if (!$this->IsPostBack) {
                    $entrepriseInsUser = new EntrepriseInscriptionUsers();
                    $companyVo = $entrepriseInsUser->fillEntrepriseBdeObject($company);
                }
            }
            $this->chargerEtablissement();
            $this->setViewState('isEntrepriselocale', $this->isEntrepriseLocale());
            Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
        }
        $this->customizeForm();
    }

    public function customizeForm()
    {
        $this->PanelInscrit->setVisible(true);
        $this->validateurListeEtablissements->enabled = false;
    }

    public function doUpdate()
    {
        $nom = null;
        $mail = null;
        if ($this->IsValid) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $inscrit = CommonInscritPeer::retrieveByPK($this->getInscrit()->getId(), $connexion);
            $this->setViewState('profilInit', $inscrit->getProfil());
            $action = 0;
            if ($inscrit) {
                $inscritUpdeted = $this->PanelInscrit->updateCompte($inscrit);

                if ($inscritUpdeted) {
                    try {
                        $result = $inscritUpdeted->save($connexion);

                        if ($result >= 0) {
                            $nom = $inscritUpdeted->getPrenom().' '.$inscritUpdeted->getNom();
                            $mail = $inscritUpdeted->getEmail();
                            (new Atexo_Entreprise_InscritHistorique())->setHistorique($nom, $mail, Atexo_Config::getParameter('EVENEMENT_ADMIN_UPDATE'));
                        }
                        $profilInit = $this->getViewState('profilInit');
                        $newProfil = $inscritUpdeted->getProfil();
                        if ($profilInit != $newProfil) {
                            if ('2' == $newProfil) {
                                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_ADMIN');
                            } else {
                                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_UES');
                            }
                            (new Atexo_Entreprise_InscritHistorique())->setHistorique($nom, $mail, $action);
                        }

                        return $result;
                    } catch (\Exception $e) {
                        Prado::log('Erreur Modification Compte Inscrit '.$e->__toString().'-', TLogger::FATAL, 'Fatal');

                        return -3;
                    }
                } else {
                    return -3;
                }
            } else {
                return -3;
            }
        }

        return -1;
    }

    public function doAddInscrit()
    {
        if ($this->IsValid) {
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_CurrentUser::getIdEntreprise());
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $newInscrit = $this->PanelInscrit->setNewInscrit($entreprise);
            $newInscrit->setEntrepriseId(Atexo_CurrentUser::getIdEntreprise());
            if ($this->PanelInscrit->adminEntreprise->Checked) {
                $newInscrit->setProfil('2');
            } else {
                $newInscrit->setProfil('1');
            }

            $msg = Atexo_Message::getMessageConfirmationInscrit($newInscrit, $entreprise);

            $objet = Prado::Localize('DEFINE_TITRE_MAIL_CREATION_COMPTE_UTILISATEUR');
            $msg = '<p>'.str_replace("\n", '</p><p>', $msg).'</p>';
            (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise($newInscrit, $objet, $msg);

            $result = $newInscrit->save($connexion);

            //Sauvegarde de l'historique
            $this->saveHistorique($newInscrit);

            if ($result >= 0) {
                return $result;
            }

            return -2;
        }

        return -1;
    }

    /**
     * Action Boutton Enregister.
     */
    public function saveChange($sender, $param)
    {
        $result = null;
        $this->saveEtablissements();

        if ('ChangingUser' == $_GET['action'] && Atexo_Module::isEnabled('entrepriseMotsDePasseHistorises') && $this->checkRecentPassword()) {
            $result = -4;
        } elseif ('ChangingUser' == $_GET['action']) {
            $result = self::doUpdate();
        } elseif ('creationByATES' == $_GET['action']) {
            $result = self::doAddInscrit();
        }
        if ($result >= 0) {
            $this->labelClose->Text = '<script>closePopUpUpdateUser();</script>';
        } else {
            if (-4 == $result) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(
                    Prado::localize(
                        'HISTORISATION_MESSAGE_ERROR',
                        ['nb' => Atexo_Module::retrieveModules()->getEntrepriseMotsDePasseHistorises()]
                    )
                );
            }
            if (-3 == $result) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_MODIFICATION_COMPTE'));
            }
            if (-2 == $result) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_CREATION_COMPTE_INSCRIT'));
            }
            if (-1 == $result) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_CREATION_COMPTE_INSCRIT'));
            }
        }
    }

    private function checkRecentPassword()
    {
        $email = $this->PanelInscrit->emailPersonnel->SafeText;
        $nouveauMotDePasse = $this->PanelInscrit->password->SafeText;

        return (new Atexo_HistorisationMotDePasse())->checkRecentPassword($email, $nouveauMotDePasse);
    }

    /**
     * Permet de sauvegarder l'historique d'un nouvel utilisateur inscrit.
     *
     * @param CommonInscrit $inscrit : objet inscrit
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistorique($inscrit, $inscrit1 = null, $mail1 = null, $idInscrit1 = null, $idEnt = null)
    {
        if ($inscrit instanceof CommonInscrit) {
            if ($inscrit->getProfil() == Atexo_Config::getParameter('PROFIL_INSCRIT_ATES')) {
                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_ADMIN');
            } else {
                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_UES');
            }
            $inscrit2 = $inscrit->getPrenom().' '.$inscrit->getNom();
            $email2 = $inscrit->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique($inscrit2, $email2, $action, $inscrit1, $mail1, $idInscrit1, $idEnt);
        }
    }

    /**
     * Permet de mettre à jour l'historique.
     *
     * @param CommonInscrit $inscrit : objet inscrit
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function updateHistorique($inscrit)
    {
        if ($inscrit instanceof CommonInscrit) {
            $action = Atexo_Config::getParameter('EVENEMENT_ADMIN_UPDATE');
            $inscrit2 = $inscrit->getPrenom().' '.$inscrit->getNom();
            $email2 = $inscrit->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique($inscrit2, $email2, $action);
        }
    }

    /**
     * Permet de valider la selection de l'etablissement
     * Verifie si au moins 1 etablissement a ete selectionne
     * Sinon affiche un message d'erreur.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function validerListeEtablissements($sender, $param)
    {
        $siretMonEtablissement = $this->etablissements->getSiretMonEtablissement();
        if (null == $siretMonEtablissement || empty($siretMonEtablissement)) {
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummaryPopUp').style.display='';</script>";
            $param->IsValid = false;
            $this->validateurListeEtablissements->ErrorMessage = Prado::localize('MESSAGE_VOUS_DEVEZ_SELECTIONNER_AU_MOINS_UN_ETABLISSEMENT');

            return;
        }
    }

    public function chargerEtablissement()
    {
        $entrepriseExiste = false;
        if ($this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo) {
            $codeMonEtablissement = null;
            $sirenMonEtablissement = null;
            $listeEtablissements = (new Atexo_Entreprise_Etablissement())->retreiveEtablissementsByIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
            $listeEtablissements = (new Atexo_Entreprise_Etablissement())->fillEtablissementsVoArray($listeEtablissements);
            $idInscrit = $this->getInscrit() ? $this->getInscrit()->getId() : Atexo_CurrentUser::getIdInscrit();
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($idInscrit);
            $codeMonEtablissement = $inscrit->getCodeEtablissement();
            $sirenMonEtablissement = $this->getEntreprise()->getSiren();
            $this->etablissements->setSiretMonEtablissement($codeMonEtablissement);
            $this->etablissements->setSirenEntreprise($sirenMonEtablissement);
            if ($this->isEntrepriseLocale()) {
                $this->etablissements->setIsEntrepriseLocale(true);
            }
            if ($this->getEtablissementSiege() instanceof Atexo_Entreprise_EtablissementVo) {
                $this->panelInfosEntreprise->setEtablissementSiege($this->getEtablissementSiege());
            }
            $this->etablissements->setEtablissementsList($listeEtablissements);
            $this->etablissements->setSiretSiegeSocial(substr($this->getEntreprise()->getSiretSiegeSocial(), -5));
            if ($this->getEntreprise()->getId()) {
                $entrepriseExiste = true;
            }
        } else {
            if ($this->isEntrepriseLocale()) {
                $this->etablissements->setIsEntrepriseLocale(true);
            }
            $this->etablissements->setSirenEntreprise(Atexo_Util::atexoHtmlEntities($_GET['siren']));
        }
        $modeDegradee = true;
        if ('mode3' == $this->getModeSaisie()) {
            $modeDegradee = false;
        } else {
            $this->etablissements->setMode(4);
        }
        $this->etablissements->setAjoutManuel($modeDegradee);
        $this->etablissements->setEntrepriseExiste($entrepriseExiste);
        $this->etablissements->initComposant();
    }

    public function isEntrepriseLocale()
    {
        if ($this->getViewState('isEntrepriselocale')) {
            return $this->getViewState('isEntrepriselocale');
        }
        $isEntrepriselocale = false;
        if (!isset($_GET['siren']) && !isset($_GET['siret']) && !isset($_GET['idNational']) && $this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo) {
            if ($this->getEntreprise()->getSiren()) {
                $isEntrepriselocale = true;
            } elseif ($this->getEntreprise()->getSirenetranger()) {
                $isEntrepriselocale = false;
            }
        } elseif (isset($_GET['siren']) || isset($_GET['siret'])) {
            $isEntrepriselocale = true;
        } elseif (isset($_GET['idNational'])) {
            $isEntrepriselocale = false;
        }
        $this->setViewState('isEntrepriselocale', $isEntrepriselocale);

        return $isEntrepriselocale;
    }

    /**
     * Permet de recuperer le siren a partir de l'url.
     *
     * @return string siren
     *
     * @author Salwa DRISSI <salwa.drissi@atexo.com>
     * @copyright Atexo 2017
     */
    public function getSiren()
    {
        $siren = null;
        if (isset($_GET['siren'])) {
            $siren = Atexo_Util::atexoHtmlEntities($_GET['siren']);
        } elseif (!isset($_GET['siren']) && !isset($_GET['siret']) && !isset($_GET['idNational'])
            && $this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo && $this->getEntreprise()->getSiren()
        ) {
            $siren = $this->getEntreprise()->getSiren();
        }

        return $siren;
    }

    /**
     * Recupere l'etablissement siege dans le viewState de la page.
     *
     * @return Atexo_Entreprise_EtablissementVo
     *
     * @author Salwa DRISSI <salwa.drissi@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEtablissementSiege()
    {
        return $this->getViewState('etablissementSiege');
    }

    public function saveEtablissements()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $etablissementsVo = $this->etablissements->getEtablissementsList();
        (new Atexo_Entreprise_Etablissement())->saveEtablissements($etablissementsVo, Atexo_CurrentUser::getIdEntreprise());
        $etablissementsToDelate = $this->etablissements->getListeIdsEtabToDelete();
        (new Atexo_Entreprise_Etablissement())->deleteEtablissements($etablissementsToDelate, $connexion);
    }
}
