<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_ValeursReferentielles;

/**
 * page de diagnostic (applet, java, Os...).
 *
 * @author adil el kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DiagnosticPoste extends MpeTPage
{
    public function onLoad($param)
    {
        $url = Atexo_Config::getParameter('URL_LT_MPE_SF').'/entreprise/footer/diagnostic-poste';
        $this->response->redirect($url);
    }

    /*
    * Permet d'enregister la trace de diagnoqtique de post
    */
    public function saveTraceDiagInscrit($sender, $param)
    {
        $logApplet = '';
        $description = '';
        $dateDebAction = date('Y-m-d H:i:s');
        if ('1' == $param->CallbackParameter) {
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION2', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        } else {
            $logApplet = $this->logApplet->Value;
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION3', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        }
        $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $arrayDonnees = [];
        $arrayDonnees['ref'] = '';
        $arrayDonnees['org'] = '';
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        $arrayDonnees['logApplet'] = $logApplet;
        $arrayDonnees['description'] = $description;

        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            $this->resDiag->value,
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
        $this->PanelButtons->render($param->NewWriter);
    }
}
