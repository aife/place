<?php

namespace Application\Pages\Entreprise;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;

/**
 * Permet de télécharger le DCE.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDownloadCompleteDce extends DownloadFile
{
    private $_reference;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dce = '';
        $organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronym']);
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, $organisme);
        Atexo_Db::closeCommon();
        if ($consultation) {
            $dce = (new Atexo_Consultation_Dce())->getDce($consultation->getId(), $organisme);
            if (0 != $dce->getDce()) {
                $this->_idFichier = $dce->getDce();
                $this->_nomFichier = $dce->getNomDce();
                //$details = "Complete DCE Download.";
                //$IdDesciption = Atexo_ValeursReferentielles::retrieveByPk("DESCRIPTION7",Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'),true);
                //                $this->traceOperationsInsrctis($IdDesciption,$dateDebAction,$details,$this->_reference,$organisme);
                //Téléchargement du DCE
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, $organisme);
            }

            // Mise à jour du nombre de téléchargement du DCE
            (new Atexo_Consultation())->updateNbrDownloadDce($this->_reference, $organisme);
        }
    }

    /*
    * Permet de tracer l'acces de l'inscrit à la PF
    */
  /* public function traceOperationsInsrctis($IdDesciption,$dateDebAction,$details = '',$ref = '',$org = '')
    {
       $description = Atexo_ValeursReferentielles::retrieveByIdValAndIdRef($IdDesciption,Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'),true);
       $arrayDonnees = array();
       $arrayDonnees['ref'] = $ref;
       $arrayDonnees['org'] = $org;
       $arrayDonnees['IdDescritpion'] = $IdDesciption;
       $arrayDonnees['afficher'] = true;
       $arrayDonnees['logApplet'] = '';
       $arrayDonnees['description'] = $description;
       Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(),Atexo_CurrentUser::getIdEntreprise(),
       $_SERVER["REMOTE_ADDR"],date('Y-m-d'),$dateDebAction,substr($_SERVER["QUERY_STRING"], 0),"-",date('Y-m-d H:i:s'),$arrayDonnees);
    }*/
}
