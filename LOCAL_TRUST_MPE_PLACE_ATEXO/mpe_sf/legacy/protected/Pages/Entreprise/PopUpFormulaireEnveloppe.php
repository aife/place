<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Propel\Mpe\CommonTDossierFormulairePeer;
use Application\Propel\Mpe\CommonTEditionFormulaire;
use Application\Propel\Mpe\CommonTEditionFormulairePeer;
use Application\Propel\Mpe\CommonTEnveloppeDossierFormulaire;
use Application\Propel\Mpe\CommonTParamDossierFormulaire;
use Application\Propel\Mpe\CommonTReponseElecFormulaire;
use Application\Propel\Mpe\CommonTReponseElecFormulairePeer;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_ComplementFormulaire;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Echange;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Parametrage;
use AtexoCrypto\Dto\Offre;
use Exception;
use Prado\Prado;

/*
 * Created on 11 juin. 2013
 *
 * by Khalid BENAMAR
 */
class PopUpFormulaireEnveloppe extends MpeTPage
{
    public ?int $_cleExterneDispositif = null;
    public $_dossier;
    public $_dossierPere;
    public $_consultation = null;
    public $_sso;
    public $_lot;
    public $_cleExterneDossier;
    public ?\Application\Propel\Mpe\CommonInscrit $_inscrit = null;
    public ?\Application\Propel\Mpe\Entreprise $_entreprise = null;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        $this->errorPart->setVisible(false);
        $this->panelMessageErreur->setVisible(false);
        $messageErreur = '';
        // vérifier que l'inscrit est connecté
        if (Atexo_CurrentUser::getIdInscrit()) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            //Récuperation de l'inscrit et de l'entreprise
            $this->recupererInscrit();
            $this->recupererEntreprise();
            //$this->_formCons = "";
            $commonConsultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            if ($commonConsultation instanceof CommonConsultation) {
                $this->_consultation = $commonConsultation;
                $numLot = '';
                $this->idEntrepriseConsultationSummary->setReference($this->_consultation->getId());
                $this->idEntrepriseConsultationSummary->setOrganisme($this->_consultation->getOrganisme());
                if ($this->_consultation->getAlloti() && $_GET['id'] && $_GET['orgAcronyme'] && $_GET['lot']) {
                    $lot = (new Atexo_Consultation_Lots())->retrieveCategorieLot(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['lot']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                    if ($lot instanceof CommonCategorieLot) {
                        $this->_lot = $lot;
                    }
                }
                // envoie des infos de l'inscrit et de l'entreprise WS SUB
                if (!$this->isPostBack) {
                    $echangeEntreprise = new Atexo_FormulaireSub_Echange();
                    $echangeEntreprise->sendEntrepriseToSUB(Atexo_CurrentUser::getIdEntreprise());
                }
                // reccuperer le disposifif selon la consultation & le type d'enveloppe
                $paramDossier = (new Atexo_FormulaireSub_Parametrage())->retrieveParametrageEnveloppe($this->_consultation->getId(), $this->_consultation->getOrganisme(), Atexo_Util::atexoHtmlEntities($_GET['typeEnv']));
                if ($paramDossier instanceof CommonTParamDossierFormulaire) {
                    $this->_cleExterneDispositif = $paramDossier->getCleExterneDispositif();
                }
                $this->gestionDossiersOffres();
                // génération SSo entreprise
                //$this->_sso = $this->_sso = "5294cad42758a";
                $this->_sso = (new Atexo_Entreprise_Inscrit())->getSsoValideInscrit(Atexo_CurrentUser::getIdInscrit());
                // envoi de l'id de dossier si ça existe
                if (isset($_GET['idDossier']) && $_GET['idDossier']) {
                    if (isset($_GET['idEnv']) && $_GET['idEnv']) {
                        $idEnv = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idEnv']));
                        $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                        $this->_dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireByIdEnveloppeAndOrg($idEnv, $org);
                    } else {
                        $this->_dossier = CommonTDossierFormulairePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['idDossier']), $connexion);
                    }
                } elseif ($paramDossier) {
                    $this->_dossier = $this->isDossierExiste(
                        $commonConsultation->getId(),
                        $commonConsultation->getOrganisme(),
                        Atexo_CurrentUser::getIdInscrit(),
                        Atexo_CurrentUser::getIdEntreprise(),
                        $this->_lot
                    );
                    if (!$this->_dossier) {
                        $cleExterneInitialisationDossier = $this->getCleExterne(); // => demande une Cle Externe du WB
                        if ($cleExterneInitialisationDossier) {
                            $this->_cleExterneDossier = $cleExterneInitialisationDossier;
                            $this->_dossier = $this->updateInfosDossier($commonConsultation, $this->_cleExterneDossier, $connexion);
                            $this->refrechRepeaterBlocDossier($this->_dossier->getTypeReponse(), $this->_dossier->getStatutValidation());
                        } else {
                            $messageErreur = Prado::localize('TEXT_ERREUR_PARAMETRAGE_AOF');
                        }
                    }
                }
                if (!$paramDossier) {
                    $messageErreur = Prado::localize('TEXT_ERREUR_PARAMETRAGE_AOF');
                } elseif ($this->_dossier instanceof CommonTDossierFormulaire || $this->_dossier instanceof CommonTEnveloppeDossierFormulaire) {
                    $this->setViewState('dossier', $this->_dossier);
                    $this->setMessageAlerte();
                } else {
                    $messageErreur = Prado::localize('TEXT_AUCUN_DOSSIER');
                }
            } else {
                $messageErreur = Prado::localize('TEXT_AUCUNE_CONSULTATION');
            }
        } else {
            $messageErreur = Prado::localize('NON_AUTORISER_DEPOT_REPONSE');
        }
        if ($messageErreur) {
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($messageErreur);
            $this->container->setVisible(false);
        }
    }

    /**
     * Permet de retourner la Clé externe de SUB.
     */
    public function getCleExterne()
    {
        $cleExterneDossier = '';
        if (isset($_GET['typeEnv']) && $_GET['typeEnv'] == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {//candidature
            if (!($this->_dossier instanceof CommonTDossierFormulaire) && !($this->_dossier instanceof CommonTEnveloppeDossierFormulaire) && !$this->_cleExterneDossier) {
                $xml = $this->createXmlMetaDonneesCandidatures();
                $cleExterneDossier = (new Atexo_FormulaireSub_Echange())->CreerDossierOffresSub($xml);
            }
        } elseif (isset($_GET['typeEnv']) && $_GET['typeEnv'] == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {   //Offre
            if (!($this->_dossier instanceof CommonTDossierFormulaire) && !($this->_dossier instanceof CommonTEnveloppeDossierFormulaire) && !$this->_cleExterneDossier) {
                $xml = $this->createXmlMetaDonneesOffres();
                $cleExterneDossier = (new Atexo_FormulaireSub_Echange())->CreerDossierOffresSub($xml);
            }
        }

        return $cleExterneDossier;
    }

    /**
     * Recupère l'url de l'iframe SUB.
     */
    public function getUrlFrame()
    {
        $cleExterneDossier = null;
        if ($this->_cleExterneDossier) {
            $cleExterneDossier = $this->_cleExterneDossier;
        } elseif ($this->_dossier instanceof CommonTDossierFormulaire || $this->_dossier instanceof CommonTEnveloppeDossierFormulaire) {
            $cleExterneDossier = $this->_dossier->getCleExterneDossier();
        }
        $url = Atexo_Config::getParameter('URL_SUB_FORMULAIRE').'?sso='.$this->_sso.'&idDispositif='.$this->_cleExterneDispositif;
        if ($this->_dossierPere instanceof CommonTDossierFormulaire || $this->_dossierPere instanceof CommonTEnveloppeDossierFormulaire) {
            $url .= '&idDossierPere='.$this->_dossierPere->getCleExterneDossier();
        }
        if ($this->_dossier instanceof CommonTDossierFormulaire || $this->_dossier instanceof CommonTEnveloppeDossierFormulaire || $cleExterneDossier) {
            $url .= '&idDossier='.$cleExterneDossier;
            /// Vérifier que l'entreprise a déjà deposé une réponse pour cette consultation et que le dossier n'est pas en demande de complément
            if (isset($_GET['idEnv']) && $_GET['idEnv'] || isset($_GET['complementEnvoye']) && $_GET['complementEnvoye']) {
                $url .= '&idFormulaire='.$this->_dossier->getCleExterneFormulaire();
            }
        }

        return $url;
    }

    /**
     * Permet de mettre à jour le statut d'une formulaire.
     *
     * @param
     */
    public function mettreAJourDossier($sender, $param)
    {
        $connexion = null;
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $dossier = $this->updateInfosDossier($this->_consultation, $this->cleExterneDossier->Value, $connexion);
            if ($dossier instanceof CommonTDossierFormulaire) {
                if ('' == $this->statutFormulaire->Value) {
                    $stautGenration = Atexo_Config::getParameter('STATUT_EDITION_EXCLU_ROUTINE');
                } else {
                    $stautGenration = Atexo_Config::getParameter('STATUT_EDITION_A_GENERER');
                }
                //Insertion des éditions
                $listeEditions = (new Atexo_FormulaireSub_Edition())->recupererListeEditionsDepuisSub($dossier->getIdDossierFormulaire(), $dossier->getCleExterneDossier());
                $connexion->beginTransaction();
                //Suppression des anciennes éditions du dossier
                $arrayIdEdition = [];
                if (false !== $listeEditions && is_array($listeEditions) && count($listeEditions)) {
                    foreach ($listeEditions as $edition) {
                        if ($edition instanceof CommonTEditionFormulaire) {
                            $statutGenerationEdition = '';
                            if (($edition->getType() == Atexo_Config::getParameter('TYPE_DOCUMENT_PJ'))
                               && ('' != $edition->getPath()) && (null != $edition->getPath())) {
                                $statutGenerationEdition = Atexo_Config::getParameter('STATUT_EDITION_GENERER');
                            } else {
                                $statutGenerationEdition = $stautGenration;
                            }
                            //Sauvegarde des éditions
                            $edition->setStatutGeneration($statutGenerationEdition);
                            $edition->save($connexion);
                            $arrayIdEdition[] = $edition->getIdEditionFormulaire();
                        }
                    }
                    $cDelete = new Criteria();
                    $cDelete->add(CommonTEditionFormulairePeer::ID_EDITION_FORMULAIRE, $arrayIdEdition, Criteria::NOT_IN);
                    $cDelete->add(CommonTEditionFormulairePeer::ID_DOSSIER_FORMULAIRE, $dossier->getIdDossierFormulaire(), Criteria::EQUAL);
                    CommonTEditionFormulairePeer::doDelete($cDelete, $connexion);
                }

                $connexion->commit();
                $this->refrechRepeaterBlocDossier($dossier->getTypeReponse(), $dossier->getStatutValidation());
            }
        } catch (\Exception $e) {
            $connexion->rollback();
            throw $e;
        }
        if ($this->cleExterneDossier->value) {
            $this->_cleExterneDossier = $this->cleExterneDossier->value;
        }
        $this->_dossier = $dossier;
        $this->setViewState('dossier', $dossier);
    }

    /**
     * Construction du récapitulatif.
     */
    public function getRecap($typeEnv, $numLot)
    {
        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            return Prado::localize('CANDIDATURE');
        } elseif ($numLot) {
            $lot = (new Atexo_Consultation_Lots())->retrieveCategorieLot(Atexo_Util::atexoHtmlEntities($_GET['id']), $numLot, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            if ($lot) {
                return Prado::localize('TEXT_LOT').' '.$lot->getLot().' - '.$lot->getDescriptionTraduite().' - '.Prado::localize('OFFRE');
            }
        } else {
            return Prado::localize('OFFRE');
        }
    }

    /**
     * Permet de construire l'xml contenant les méta-données lors de la création du dossier d'offres.
     *
     * @return $xml: l'xml construit
     */
    public function createXmlMetaDonneesOffres()
    {
        $xml = '';
        $idDispositif = $this->_cleExterneDispositif;
        if ($this->_consultation instanceof CommonConsultation) {
            $infosLot = '';
            if ($this->_lot instanceof CommonCategorieLot) {
                $infosLot = preg_replace('#"#', '\"', Prado::localize('TEXT_LOT').' '.$this->_lot->getLot().' - '.Atexo_Util::toUtf8($this->_lot->getDescriptionTraduite()));
            }
            if ($this->_dossierPere instanceof CommonTDossierFormulaire) {
                $idDossierPere = $this->_dossierPere->getCleExterneDossier();
                if ($this->_inscrit instanceof CommonInscrit) {
                    $loginTier = $this->_inscrit->getLogin();
                    $xml .= '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
                    $xml .= '<dossiercreation idDossierPere="'.$idDossierPere.'" loginTiers="'.Atexo_Util::toUtf8($loginTier).'" idDispositif="'.$idDispositif.'">';
                    if ($infosLot) {
                        $xml .= '<dossier_datas>';
                        $xml .= '<data type="stringType" nom="AOF_Offre_Caracteristiques_Principales_Lot">';
                        $xml .= '<valeur><![CDATA['.Atexo_Util::toUtf8($infosLot).']]></valeur>';
                        $xml .= '</data>';
                        $xml .= '</dossier_datas>';
                    }
                    $xml .= '</dossiercreation>';
                }
            }
        }

        return $xml;
    }

    /**
     * Permet de construire l'xml contenant les méta-données lors de la création du dossier de candidature.
     *
     * @return $xml: l'xml construit
     */
    public function createXmlMetaDonneesCandidatures()
    {
        $xml = '';
        $idDispositif = $this->_cleExterneDispositif;
        if ($this->_consultation instanceof CommonConsultation) {
            if ($this->_inscrit instanceof CommonInscrit) {
                $loginTier = $this->_inscrit->getLogin();
                $xml .= '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
                $xml .= '<dossiercreation loginTiers="'.Atexo_Util::toUtf8($loginTier).'" idDispositif="'.$idDispositif.'">';
                if ($this->_entreprise instanceof Entreprise) {
                    $xml .= '<dossier_datas>';
                    //Raison sociale
                    $xml .= '<data type="stringType" nom="AOF_Candidature_raison_sociale">';
                    $xml .= '<valeur><![CDATA['.Atexo_Util::toUtf8(preg_replace('#"#', '\"', $this->_entreprise->getNom())).']]></valeur>';
                    $xml .= '</data>';
                    $xml .= '<data type="stringType" valeur="'.$this->_entreprise->getSiren().'" nom="AOF_Candidature_SIREN"/>';
                    $xml .= '<data type="stringType" valeur="'.$this->_entreprise->getSiretSiegeSocial().'" nom="AOF_Candidature_NIC_sieg_sociale"/>';
                    $nic = (new Atexo_Entreprise_Etablissement())->getCodeEtablissemntByIdEtab(Atexo_CurrentUser::getIdEtablissement());
                    $xml .= '<data type="stringType" valeur="'.$nic.'" nom="AOF_Candidature_NIC"/> ';
                    $xml .= '</dossier_datas>';
                }
                $xml .= '</dossiercreation>';
            }
        }

        return $xml;
    }

    /**
     * Gestion du cas des dossiers d'offres.
     */
    public function gestionDossiersOffres()
    {
        if (isset($_GET['typeEnv']) && $_GET['typeEnv'] == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
            $idInscrit = Atexo_CurrentUser::getIdInscrit();
            if (isset($_GET['idDossierPere']) && $_GET['idDossierPere']) {
                $idDossierPere = Atexo_Util::atexoHtmlEntities($_GET['idDossierPere']);
                $dossierFormulaire = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($idDossierPere);
                if ($dossierFormulaire) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                    $reponseElecFormulaire = CommonTReponseElecFormulairePeer::retrieveByPK($dossierFormulaire->getIdReponseElecFormulaire(), $connexion);
                    if (($reponseElecFormulaire instanceof CommonTReponseElecFormulaire)
                       && $reponseElecFormulaire->getOrganisme() == $org
                       && $reponseElecFormulaire->getConsultationId() == $this->_consultation->getId()
                       && $reponseElecFormulaire->getIdInscrit() == $idInscrit) {
                        $this->_dossierPere = $dossierFormulaire;
                    } else {
                        $messageError = date('Y-m-d H:i:s')." l'idDossierPere = ".$idDossierPere.' ne corresspond pas à un id valide 
	        						pour la consultation dont le référence est  = '.$this->_consultation->getId()." l'organisme = ".$org.' idInscrit = '.$idInscrit;
                        Atexo_Util::writeLogFile(
                            Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'),
                            Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log',
                            $messageError,
                            'a'
                        );
                    }
                }
            }
        }
    }

    /**
     * Permet de recuperer l'inscrit.
     */
    public function recupererInscrit()
    {
        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
        if ($inscrit instanceof CommonInscrit) {
            $this->_inscrit = $inscrit;
        }
    }

    /**
     * Permet de recuperer l'entreprise lié à l'inscrit.
     */
    public function recupererEntreprise()
    {
        if ($this->_inscrit instanceof CommonInscrit) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $entreprise = EntreprisePeer::retrieveByPK($this->_inscrit->getEntrepriseId(), $connexion);
            if ($entreprise instanceof Entreprise) {
                $this->_entreprise = $entreprise;
            }
        }
    }

    /**
     * Permet de creer ou mette à jour le dossier
     * Param Consultation,cle externe ,connexion.
     */
    public function updateInfosDossier($consultation, $cleExterne, $connexion)
    {
        try {
            $reponseElecFormulaire = (new Atexo_FormulaireSub_Dossier())->recupererReponseElectFormulaire($consultation->getId(), $consultation->getOrganisme(), Atexo_CurrentUser::getIdInscrit());
            if (!$reponseElecFormulaire) {
                $reponseElecFormulaire = new CommonTReponseElecFormulaire();
            }
            $reponseElecFormulaire->setConsultationId($consultation->getId());
            $reponseElecFormulaire->setOrganisme($consultation->getOrganisme());
            $reponseElecFormulaire->setIdInscrit(Atexo_CurrentUser::getIdInscrit());
            $reponseElecFormulaire->setIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
            $reponseElecFormulaire->save($connexion);
            $this->_dossier = $this->getViewState('dossier');
            if (!$this->_dossier instanceof CommonTDossierFormulaire) {
                $this->_dossier = new CommonTDossierFormulaire();
            }
            $this->_dossier->setIdReponseElecFormulaire($reponseElecFormulaire->getIdReponseElecFormulaire());
            $this->_dossier->setDateCreation(date('Y-m-d H:i:s'));
            $this->_dossier->setDateModif(date('Y-m-d H:i:s'));
            $this->_dossier->setCleExterneDispositif(Atexo_Util::atexoHtmlEntities($_GET['idDispositif']));
            $this->_dossier->setCleExterneDossier($cleExterne);
            $this->_dossier->setIdLot(Atexo_Util::atexoHtmlEntities($_GET['lot']));
            $this->_dossier->setTypeEnveloppe(Atexo_Util::atexoHtmlEntities($_GET['typeEnv']));
            if ('' == $this->statutFormulaire->Value) {
                $statut = Atexo_Config::getParameter('STATUT_DOSSIER_BROUILLON');
            } else {
                $statut = Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE');
            }
            if ($this->cleExterneDispositif->Value) {
                $this->_dossier->setCleExterneDispositif($this->cleExterneDispositif->Value);
            }
            $this->_dossier->setStatutValidation($statut);
            $this->_dossier->setIdDossierPere(Atexo_Util::atexoHtmlEntities($_GET['idDossierPere']));
            $this->_dossier->save($connexion);

            return $this->_dossier;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Permet de faire le refresh de repeater des editions
     * Param $typeReponse dossier int
     * Param $statut dossier string.
     */
    public function refrechRepeaterBlocDossier($typeReponse, $statut)
    {
        if ($typeReponse == Atexo_Config::getParameter('TYPE_DOSSIER_DEMANDE_COMPLEMENT')) {
            $script = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_buttonRefreshBlocDossier').click();";
        } else {
            $script = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_listeFormulairesSub_buttonRefreshBlocDossier').click();";
        }
        if ($statut == Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE')) {
            $script .= 'window.close();';
        }
        $script .= '</script>';
        $this->scriptSaveClose->Text = $script;
    }

    /**
     * Permet verifier si le dossier existe deja
     * si existe le retourner
     * Param $typeReponse dossier int
     * Param $statut dossier string.
     */
    public function isDossierExiste($consultationId, $org, $idInscrit, $idEntreprise, $lot)
    {
        $dossier = '';
        $reponseElecFormulaire = (new Atexo_FormulaireSub_Dossier())->recupererReponseElectFormulaire($consultationId, $org, $idInscrit, $idEntreprise);
        if ($reponseElecFormulaire  instanceof CommonTReponseElecFormulaire) {
            if ($lot instanceof CommonCategorieLot) {
                $numLot = $lot->getLot();
            } else {
                $numLot = 0;
            }
            if (isset($_GET['typeEnv'])) {
                $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierByIdReponseElectFormulaireAndTypeEnveloppeAndNumLot($reponseElecFormulaire->getIdReponseElecFormulaire(), Atexo_Util::atexoHtmlEntities($_GET['typeEnv']), $numLot);
            }
        }

        return $dossier;
    }

    /**
     * Permet de remplir le message d'alerte selon le type et le statut de dossier
     * Mantis 29980.
     */
    public function setMessageAlerte()
    {
        $depotDossier = 0;
        $afficher = true;
        if ($this->_dossier instanceof CommonTDossierFormulaire) {
            $depotDossier = $this->_dossier->getFormulaireDepose();
        }
        if ($this->_dossier->getTypeReponse() == Atexo_Config::getParameter('TYPE_DOSSIER_REPONSE_PRINCIPALE')
        && ($depotDossier == Atexo_Config::getParameter('FORMULAIRE_DEPOSE_DOSSIER_PREPARATION'))
           ) {
            if ($this->_dossier->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                $this->panelMessageAvertissement->setMessage(Prado::localize('TEXT_DEPOT_CANDIDATURE_AOF'));
            } else {
                $this->panelMessageAvertissement->setMessage(Prado::localize('TEXT_DEPOT_OFFRE_AOF'));
            }
        } elseif ($this->_dossier->getTypeReponse() == Atexo_Config::getParameter('TYPE_DOSSIER_DEMANDE_COMPLEMENT')) {
            $complement = (new Atexo_FormulaireSub_ComplementFormulaire())->getComplementFormulaireByIdDossier($this->_dossier->getIdDossierFormulaire());
            if ($complement instanceof CommonTComplementFormulaire) {
                if ($complement->getStatutDemande() != Atexo_Config::getParameter('STATUT_DEMANDE_COMPLEMENT_ENVOYEE')) {
                    if ($this->_dossier->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                        $this->panelMessageAvertissement->setMessage(Prado::localize('TEXT_REOUVERTURE_CANDIDATURE_AOF'));
                    } else {
                        $this->panelMessageAvertissement->setMessage(Prado::localize('TEXT_REOUVERTURE_OFFRE_AOF'));
                    }
                } else {
                    $afficher = false;
                }
            } else {
                $afficher = false;
            }
        } else {
            $afficher = false;
        }
        $this->panelMessageAvertissement->setVisible($afficher);
    }
}
