<?php

namespace Application\Pages\Entreprise;

use App\Utils\Encryption;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Documents;
use Prado\Prado;

/**
 * page compte entreprise , espace document.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseEspaceDocumentaire extends MpeTPage
{
    public $_docCompany;
    public string $pageTitle = 'TITRE';

    public function onInit($param)
    {
        $this->pageTitle = 'TITRE_COFFRE_FORT_ENTREPRISE';
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        if (!$this->IsPostBack) {
            $this->_docCompany = (new Atexo_Entreprise_Documents())->retrieveDocuments(Atexo_CurrentUser::getIdEntreprise());
            $this->displayDocuments();
        }
        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    /**
     * affichage des documents de l'entrperise.
     */
    public function displayDocuments()
    {
        $dataSource = $this->_docCompany;
        if ($dataSource) {
            $this->repeaterDocs->DataSource = $dataSource;
        } else {
            $this->repeaterDocs->DataSource = [];
        }
        $this->repeaterDocs->DataBind();
    }

    public function refreshRepeater($sender, $param)
    {
        $this->repeaterDocs->DataSource = (new Atexo_Entreprise_Documents())->retrieveDocuments(Atexo_CurrentUser::getIdEntreprise());
        $this->repeaterDocs->DataBind();
        $this->panelRepeater->render($param->NewWriter);
    }

    /**
     * actions sur les documents.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function actionDocument($sender, $param)
    {
        $doc = null;
        if ('Delete' === $param->CommandName) {
            $encrypt = Atexo_Util::getSfService(Encryption::class);
            (new Atexo_Entreprise_Documents())->deleteDocument($encrypt->decryptId($param->CommandParameter));
        } elseif ('Visu' === $param->CommandName) {
            if ($doc) {
                $util = new Atexo_Util();
                $util->createHeaderByData($doc->getFichier(), $doc->getNom());
            }
        }
    }

    public function getImageVisibleOrNotByAgent($idDocument)
    {
        $encrypt = Atexo_Util::getSfService(Encryption::class);
        $document = (new Atexo_Entreprise_Documents())
            ->retrieveDocumentById(
                $encrypt->decryptId($idDocument),
                Atexo_CurrentUser::getIdEntreprise()
            );
        if ($document) {
            if ($document[0]->getVisibleParAgents()) {
                return '/images/picto-visible.gif';
            } else {
                return '/images/picto-visible-inactive.gif';
            }
        }
    }

    public static function getTextAccessibiliteAgent($idDocument)
    {
        $encrypt = Atexo_Util::getSfService(Encryption::class);
        $document = (new Atexo_Entreprise_Documents())
            ->retrieveDocumentById(
                $encrypt->decryptId($idDocument),
                Atexo_CurrentUser::getIdEntreprise()
            );
        if ($document) {
            if ($document[0]->getVisibleParAgents()) {
                return Prado::localize('TEXT_FICHIER_TOUJOURS_ACCESSIBLE_ACHETEURS_PUBLICS');
            } else {
                return Prado::localize('TEXT_FICHIER_NON_ACCESSIBLE_ACHETEURS_PUBLICS');
            }
        }
    }
}
