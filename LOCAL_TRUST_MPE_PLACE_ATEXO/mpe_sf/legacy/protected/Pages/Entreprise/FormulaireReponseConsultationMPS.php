<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CandidatureMPS;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Exercice;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use AtexoPdf\PdfGeneratorClient;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Le formulaire de a reponse MPS.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.7.0
 *
 * @copyright Atexo 2014
 */
class FormulaireReponseConsultationMPS extends MpeTPage
{
    public $_consultation;
    public $_entreprise;
    public $_candidatureMPS;
    protected $_monEtablissement;

    /**
     * Get the value of [_consultation] column.
     *
     * @return CommonConsultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getConsultation()
    {
        if (!$this->_consultation instanceof CommonConsultation) {
            $this->_consultation = $this->getViewState('consultation');
        }

        return $this->_consultation;
    }

    /**
     * Set the value of [_consultation] .
     *
     * @param CommonConsultation $consultation new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    /**
     * Get the value of [_monEtablissement] column.
     *
     * @return EtablissementVo
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getMonEtablissement()
    {
        if (!$this->_monEtablissement) {
            $this->_monEtablissement = $this->getViewState('monEtablissement');
        }

        return $this->_monEtablissement;
    }

    /**
     * Set the value of [_monEtablissement] .
     *
     * @param Etablissement $monEtablissement new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setMonEtablissement($monEtablissement)
    {
        $this->setViewState('monEtablissement', $monEtablissement);
    }

    /**
     * Get the value of [_candidatureMPS] column.
     *
     * @return CommonTCandidatureMps
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCandidatureMPS()
    {
        if (!$this->_candidatureMPS instanceof Atexo_Entreprise_CandidatureMPS) {
            $this->_candidatureMPS = $this->getViewState('candidatureMPS');
        }

        return $this->_candidatureMPS;
    }

    /**
     * Set the value of [_candidatureMPS] .
     *
     * @param CommonTCandidatureMps $candidatureMPS new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setCandidatureMPS($candidatureMPS)
    {
        $this->_candidatureMPS = $candidatureMPS;
    }

    /**
     * Set the value of [_entreprise] .
     *
     * @param Entreprise $entreprise new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setEntreprise($entreprise)
    {
        $this->setViewState('entreprise', $entreprise);
    }

    /**
     * Get the value of [_entreprise] column.
     *
     * @return Entreprise
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEntreprise()
    {
        return $this->getViewState('entreprise');
    }

    public function onInit($param)
    {

        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $messageErreur = null;
        if (!$this->isPostBack) {
            $this->blocIdentiteEntreprise->setVisible(true);
            $this->panelFormulaireMPS->setVisible(true);
            $this->panelRecapitulationFormulaireMPS->setVisible(false);
            $this->legende->setVisible(false);

            if (Atexo_CurrentUser::isConnected() && Atexo_CurrentUser::getIdInscrit()) {
                $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']));

                if ($consultation instanceof CommonConsultation &&
                       (
                           Atexo_Module::isEnabled('EntrepriseRepondreConsultationApresCloture') ||
                           Atexo_Util::cmpIsoDateTime(date('Y-m-d H:i:s'), $consultation->getDatefin()) <= 0
                       )
                   ) {
                    if (Atexo_Module::isEnabled('MarchePublicSimplifie', $consultation->getOrganisme()) &&
                           $consultation->getMarchePublicSimplifie()
                       ) {
                        $this->setViewState('consultation', $consultation);
                        $this->setConsultation($consultation);
                        //Chargement du Bloc Recap Consultation
                        $this->chargerBlocRecap($consultation);

                        $this->objetCandidature->setMode(1);
                        $this->objetCandidature->chargerInfoLots($consultation, $_GET['lots']);

                        //Chargement du Bloc Info Inscrit
                        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
                        // La synchronisation avec Api Gouv Entreprise
                        $idEntreprise = Atexo_CurrentUser::getIdEntreprise();
                        $idCodeEffectif = (new Atexo_Entreprise())->getIdCodeEffectifById($idEntreprise);

                        if (Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_SGMAP_MPS') &&
                               (new Atexo_Entreprise())->isEntrepriseFrancaise($inscrit->getEntrepriseId()) &&
                               Atexo_Module::isEnabled('SynchronisationSGMAP')
                           ) {
                            $forcerSynchronisation = false;

                            if (!$idCodeEffectif) {
                                $forcerSynchronisation = true;
                            }

                            $sirenInscrit = $inscrit->getSirenEntrepriseInscrit();
                            Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($sirenInscrit, '1', $modeSynchro, $forcerSynchronisation);
                            Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($sirenInscrit.$inscrit->getSiretEtablissementInscrit(), '1', $modeSynchro);
                            $sunchroniserExercices = (new Atexo_Entreprise_Exercice())->exerciceVideExiste($idEntreprise);

                            if ($sunchroniserExercices) {
                                (new Atexo_Entreprise_Exercice())->synchroExerciceAvecSGMAP($sirenInscrit);
                            }
                        }

                        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise);

                        if ($entreprise && ($inscrit instanceof CommonInscrit)) {
                            if ($entreprise->getSiren()) {
                                $this->blocDeclaration->setIdEntreprise($entreprise->getId());
                                $capacitesData = (new Atexo_Entreprise())->initialiserCapacite();
                                (new Atexo_Entreprise())->fillCapaciteFormEntreprise($capacitesData, $entreprise->getId());

                                $montEtablissement = $inscrit->getCommonTEtablissement();

                                if ($montEtablissement) {
                                    $montEtablissementVo = $montEtablissement->getEtablissementVo();
                                    $this->setViewState('SiretMonEtablissement', $montEtablissement->getCodeEtablissement());
                                    $this->setViewState('monEtablissement', $montEtablissementVo);
                                }

                                $this->blocCapacite->setCapacites($capacitesData);

                                if ($entreprise->getIdCodeEffectif()) {
                                    $this->blocCapacite->setIdTrancheEffectifsEntreprise($entreprise->getIdCodeEffectif());
                                }

                                $this->blocCapacite->setConsultationAlloti($consultation->getAlloti());
                                $this->blocIdentite->setCompany($entreprise);
                                $this->setEntreprise($entreprise);
                                $this->blocIdentite->chargerInfoEntreprise();
                            } else {
                                $messageErreur = Prado::localize('MODE_MPS_NON_AUTORISER');
                            }
                        }
                    } else {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL').'index.php?page=Entreprise.FormulaireReponseConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id'])
                        .'&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
                    }
                } else {
                    $messageErreur = Prado::localize('TEXT_AUCUNE_CONSULTATION');
                }
            } else {
                $messageErreur = Prado::localize('NON_AUTORISER_DEPOT_REPONSE');
            }

            if ($messageErreur) {
                $this->blocIdentiteEntreprise->setVisible(false);
                $this->panelFormulaireMPS->setVisible(false);
                $this->detailConsultation->setVisible(false);
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage($messageErreur);
            } else {
                $this->blocIdentiteEntreprise->setVisible(true);
                $this->panelFormulaireMPS->setVisible(true);
                $this->panelMessageErreur->setVisible(false);
            }
        }
    }

    /**
     * Permet de charger le bloc de recapulation de la consultation.
     *
     * @param CommonConsultation $consultation
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function chargerBlocRecap($consultation)
    {
        if ($consultation instanceof CommonConsultation) {
            $this->detailConsultation->setReference($consultation->getId());
            $this->detailConsultation->setOrganisme($consultation->getOrganisme());
        }
    }

    /**
     * Valider les informations de la candidature MPS.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function validerInformations()
    {
        self::saveInfosCandidatureMps();
        self::chargerRecap();
    }

    /**
     * Remplire la candidature MPS par les donnees de la consultation.
     *
     * @param CommonTCandidatureMps $candidatureMPS la candidature MPS
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillCandidatureByDataConsultation(&$candidatureMPS)
    {
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            $candidatureMPS->setRefConsultation($consultation->getId());
            $candidatureMPS->setOrganisme($consultation->getOrganisme());
            $candidatureMPS->setReferenceUtilisateur($consultation->getReferenceUtilisateur());
            $candidatureMPS->setObjetConsultation($consultation->getObjet());
            $candidatureMPS->setDateLimiteRemise($consultation->getDateFin());
        }
    }

    /**
     * Remplire le formulaire de recapitulation.
     *
     * @param CommonTCandidatureMps $candidatureMPS la candidature MPS
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function chargerInfoRecapitulation($candidatureMPS)
    {
        //$this->blocIdentite->setCompany($candidatureMPS->getEntreprise());
        //$this->blocIdentite->chargerInfoEntreprise();
        $this->blocConformiteRecapitulation->chargerInfoConformite();
        $this->blocCapaciteRecapitulation->setCapacites($candidatureMPS->getCapacites());
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            $this->blocCapaciteRecapitulation->setConsultationAlloti($consultation->getAlloti());
        }
        $this->blocCapaciteRecapitulation->chargerInfoCapacites();
        $this->blocDeclarationRecapitulation->chargerInfoDeclarationsRecap();
        $this->objetCandidature->setMode(0);
        $this->objetCandidature->chargerInfoLots($this->getConsultation(), $candidatureMPS->getListeLots());
    }

    /**
     * Modifier les informations de la candidature MPS.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function modifierInformations()
    {
        $this->scriptJs->Text = "<script>displayOneOptionPanel(document.getElementById('ctl0_CONTENU_PAGE_blocDeclaration_representantLegal'),'1',
					'ctl0_CONTENU_PAGE_blocDeclaration_panel_representantAutre');
					</script>";
        $this->panelMessageErreur->setVisible(false);
        $this->panelFormulaireMPS->setVisible(true);
        $this->panelRecapitulationFormulaireMPS->setVisible(false);
        $this->legende->setVisible(false);
        $this->objetCandidature->setMode(1);
        $candidature = $this->getCandidatureMPS();
        $idsLots = ($candidature instanceof Atexo_Entreprise_CandidatureMPS) ? $candidature->getListeLots() : null;
        $this->objetCandidature->chargerInfoLots($this->getConsultation(), $idsLots);
    }

    /**
     * Permet de telecharger la version brouillon de la candidature MPS.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function imprimerCandidatureMPS($sender, $param)
    {
        self::saveInfosCandidatureMps();
        $brouillon = $sender->CommandParameter;
        $candidatureMPS = $this->getCandidatureMPS();
        if ($candidatureMPS instanceof Atexo_Entreprise_CandidatureMPS) {
            $consultation = $this->getConsultation();
            $pathFile = (new Atexo_MarchePublicSimplifie())->generateOdtCandidatureMPS($candidatureMPS, $consultation, $brouillon);
            if ($pathFile) {
                $pdfContent = (new PdfGeneratorClient())->genererPdf($pathFile);
                @unlink($pathFile);
                DownloadFile::downloadFileContent(Prado::localize('DEFINE_CANDIDATURE_MPS'), $pdfContent);
            }
        }
    }

    /**
     * Enregistrer les informations de la candidature MPS.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function saveCandidatureMPS()
    {
        try {
            $candidatureMPS = $this->getViewState('candidatureMPS');
            $consultation = $this->getConsultation();

            if ($candidatureMPS instanceof Atexo_Entreprise_CandidatureMPS) {
                $pathFile = (new Atexo_MarchePublicSimplifie())->generateOdtCandidatureMPS($candidatureMPS, $consultation);
                $filePdf = $this->generationPdf($pathFile);
                $fileName = Prado::localize('DEFINE_CANDIDATURE_MPS');

                if ($filePdf) {
                    $connexion = Propel::getConnection(
                        Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
                    );
                    $tCandidatureMPS = new CommonTCandidatureMps();
                    $tCandidatureMPS->setConsultationId($candidatureMPS->getRefConsultation());
                    $tCandidatureMPS->setOrganisme($candidatureMPS->getOrganisme());
                    $tCandidatureMPS->setIdInscrit($candidatureMPS->getIdInscrit());
                    $listeLots = $this->objetCandidature->getSelectedLots();

                    if (!empty($listeLots)) {
                        $tCandidatureMPS->setListeLots($listeLots);
                    }

                    $entreprise = $candidatureMPS->getEntreprise();

                    if ($entreprise instanceof Atexo_Entreprise_EntrepriseVo) {
                        $tCandidatureMPS->setIdEntreprise($entreprise->getId());
                    }

                    $atexoBlob = new Atexo_Blob();
                    $arrayHorodatage = (new Atexo_Crypto())->getArrayHorodatage($filePdf, true);

                    if (is_array($arrayHorodatage)) {
                        $tCandidatureMPS->setHorodatage($arrayHorodatage['horodatage']);
                        $tCandidatureMPS->setUntrustedDate($arrayHorodatage['untrustedDate']);
                        $tCandidatureMPS->setUntrustedSerial($arrayHorodatage['untrustedSerial']);
                        $idBlob = $atexoBlob->insert_blob($fileName, $filePdf, $candidatureMPS->getOrganisme());

                        if ($idBlob) {
                            $tCandidatureMPS->setIdBlob($idBlob);
                            $sizeFile = $atexoBlob->getTailFile($idBlob, $candidatureMPS->getOrganisme());
                            $tCandidatureMPS->setTailleFichier($sizeFile);
                            $tCandidatureMPS->save($connexion);

                            $this->response->redirect(
                                Atexo_Config::getParameter('PF_URL').
                                'index.php?page=Entreprise.FormulaireReponseConsultation&id='.
                                $candidatureMPS->getRefConsultation().
                                '&org='.$candidatureMPS->getOrganisme().
                                '&idCandidatureMps='.
                                $tCandidatureMPS->getIdCandidature()
                            );
                        } else {
                            Prado::log(
                                "Erreur lors du depot de la candidature MPS  : impossible d'enregistrer le fichier  Ref: ".
                                Atexo_Util::atexoHtmlEntities($_GET['id']).'- Org: '.
                                Atexo_Util::atexoHtmlEntities($_GET['org']).' - Id enteprise: '.
                                Atexo_CurrentUser::getIdEntreprise().' - Id Inscrit: '.
                                Atexo_CurrentUser::getIdInscrit()."\n",
                                TLogger::ERROR,
                                'Atexo.pages.entreprise.FormulaireReponseConsultationMPS.php'
                            );
                            $this->panelMessageErreur->setVisible(true);
                            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_DEPOT_CANDIDATURE_MPS'));
                        }
                    } else {
                        Prado::log(
                            "Erreur lors du depot de la candidature MPS  :problème d'horodatage du fichier Ref: ".
                            Atexo_Util::atexoHtmlEntities($_GET['id']).'- Org: '.
                            Atexo_Util::atexoHtmlEntities($_GET['org']).' - Id enteprise: '.
                            Atexo_CurrentUser::getIdEntreprise().' - Id Inscrit: '.
                            Atexo_CurrentUser::getIdInscrit()."\n",
                            TLogger::ERROR,
                            'Atexo.pages.entreprise.FormulaireReponseConsultationMPS.php'
                        );
                        $this->panelMessageErreur->setVisible(true);
                        $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_DEPOT_CANDIDATURE_MPS'));
                    }

                    @unlink($pathFile);
                    @unlink($filePdf);
                }
            }
        } catch (\Exception $e) {
            Prado::log(
                'Erreur lors du depot de la candidature MPS  : Ref: '.
                Atexo_Util::atexoHtmlEntities($_GET['id']).'- Org: '.Atexo_Util::atexoHtmlEntities($_GET['org']).
                ' - Id enteprise: '.Atexo_CurrentUser::getIdEntreprise().' - Id Inscrit: '.
                Atexo_CurrentUser::getIdInscrit().' Message erreur : '.$e->getMessage()." \n",
                TLogger::ERROR,
                'Atexo.pages.entreprise.FormulaireReponseConsultationMPS.php'
            );

            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_DEPOT_CANDIDATURE_MPS'));
            $this->objetCandidature->chargerInfoLots(
                $this->getConsultation(),
                ($this->getCandidatureMPS() instanceof Atexo_Entreprise_CandidatureMPS) ? $this->getCandidatureMPS()->getListeLots() : null
            );
        }
    }

    /**
     * Enregistre les informations de la candidature MPS.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function saveInfosCandidatureMps()
    {
        $candidatureMPS = new Atexo_Entreprise_CandidatureMPS();
        $this->fillCandidatureByDataConsultation($candidatureMPS);
        $this->fillCandidatureByDataEntreprise($candidatureMPS);
        $this->fillCandidatureByDataEtablissement($candidatureMPS);
        $this->blocCapacite->fillCandidatureByDataCapacites($candidatureMPS);
        $this->blocConformite->fillCandidatureByDataConformite($candidatureMPS);
        $this->blocDeclaration->fillCandidatureByDataDeclaration($candidatureMPS);
        $candidatureMPS->setIdInscrit(Atexo_CurrentUser::getIdInscrit());
        $listeLots = $this->objetCandidature->getSelectedLots();
        if (!empty($listeLots)) {
            $candidatureMPS->setListeLots($listeLots);
        }
        $this->Page->setViewState('candidatureMPS', $candidatureMPS);
    }

    /**
     * charge le tableau recaputilatif des informations de la candidature MPS.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function chargerRecap()
    {
        $candidatureMPS = $this->Page->getViewState('candidatureMPS');
        $this->chargerInfoRecapitulation($candidatureMPS);
        $this->panelFormulaireMPS->setVisible(false);
        $this->panelRecapitulationFormulaireMPS->setVisible(true);
        $this->legende->setVisible(true);
    }

    /**
     * Remplire la candidature MPS par les donnees de l'entreprise.
     *
     * @param CommonTCandidatureMps $candidatureMPS la candidature MPS
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillCandidatureByDataEntreprise(&$candidatureMPS)
    {
        $entrepriseVo = $this->getEntreprise();
        if ($entrepriseVo) {
            $entrepriseVo->setListeEtablissements($this->getMonEtablissement());
        }
        $candidatureMPS->setEntreprise($entrepriseVo);
    }

    /**
     * Remplire la candidature MPS par les donnees de l'etablissement.
     *
     * @param CommonTCandidatureMps $candidatureMPS la candidature MPS
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillCandidatureByDataEtablissement(&$candidatureMPS)
    {
        $etablissement = $this->getMonEtablissement();
        $candidatureMPS->setEtablissement($etablissement);
    }

    /**
     * @param $pathFile
     *
     * @return string | Excerpion si pas de génération
     *
     * @throws Exception
     */
    public function generationPdf($pathFile)
    {
        try {
            $i = 0;
            $fileName = Prado::localize('DEFINE_CANDIDATURE_MPS');
            $filePdf = Atexo_Config::getParameter('COMMON_TMP').'CandidatureMPS_'.session_id().time().$fileName;
            while ($i < 2) {
                ++$i;
                $pdfContent = (new PdfGeneratorClient())->genererPdf($pathFile);
                Atexo_Util::write_file($filePdf, $pdfContent);
                if (is_file($filePdf) && filesize($filePdf) > 20) {
                    return $filePdf;
                }
            }

            throw new Exception('Erreur génération PDF ');
        } catch (Exception $e) {
            throw $e;
        }
    }
}
