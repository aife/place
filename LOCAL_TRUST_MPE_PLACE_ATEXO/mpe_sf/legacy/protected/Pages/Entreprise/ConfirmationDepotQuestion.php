<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;

class ConfirmationDepotQuestion extends MpeTPage
{
    public $_reference;
    public $_parentPage;
    public $_organisme;
    public string $pageTitle = 'TITRE';

    public function onInit($param)
    {
        $this->pageTitle = 'TITRE_CONFIRMATION_QUESTION';
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION11', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $arrayDonnees = [];
        $arrayDonnees['ref'] = $this->_reference;
        $arrayDonnees['org'] = $this->_organisme;
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        $arrayDonnees['logApplet'] = '';
        $arrayDonnees['description'] = $description;
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            '-',
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
    }

    public function retour()
    {
        $this->response->redirect('index.php?page=Entreprise.EntrepriseDetailConsultation&id='.$this->_reference.'&orgAcronyme='.$this->_organisme);
    }
}
