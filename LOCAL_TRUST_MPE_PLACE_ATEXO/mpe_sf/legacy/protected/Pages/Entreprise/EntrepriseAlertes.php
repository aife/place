<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAlerte;
use Application\Propel\Mpe\CommonAlertePeer;
use Application\Propel\Mpe\CommonCategorieINSEEPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LieuxExecution;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Alertes;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_ReferentielVo;
use Exception;
use Prado\Prado;
use SimpleXMLElement;

/**
 * Page de recherche avancee.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseAlertes extends MpeTPage
{
    private string $_idAlerte = '';

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $sessionLang = Atexo_CurrentUser::readFromSession('lang');

        if (Atexo_Module::isEnabled('MultiLinguismeEntreprise') && isset($sessionLang)) {
            $this->linkLieuExe1->NavigateUrl = $this->getLieuExecutionUrl();
            //$this->linkLieuExe1->NavigateUrl = "javascript:popUpSetSize('index.php?page=Commun.LieuxExecution','1050px','450px','yes');";
        }
        $this->_idAlerte = Atexo_Util::atexoHtmlEntities($_GET['idAlerte']);
        $this->panelMessageErreur->setVisible(false);
        //<!-- module referentiel CPV
        if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
            $atexo_ref = new Atexo_Ref();
            $atexo_ref->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_CONFIG'));
            $this->idAtexoRef->afficherReferentiel($atexo_ref);
        }
        //-->
        // Debut codes nuts
        if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
            $atexoCodesNutsRef = new Atexo_Ref();
            $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
            $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
        }
        // Fin codes nuts
        if (!$this->IsPostBack) {
            $this->displayOrganismes();
            $this->displayCategory();
            $this->displayClauses();
            if ('' != $this->_idAlerte) {
                $this->displayAlerte();
            } else {
                $this->desactiver->checked = true;
                $this->ltRefCons->afficherReferentielConsultation();
                $this->idReferentielZoneText->afficherTextConsultation();
            }
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                $this->panelPurchaseNames->setDisplay('None');
            }
            if (Atexo_Module::isEnabled('RechercheAvanceeParTypeOrg')) {
                $this->panelSearchByClassification->setVisible(true);
                $this->fillClassification();
            }

            if (Atexo_Module::isEnabled('RechercheAvanceeParDomainesActivite')) {
                $this->panelDomainesActivites->setVisible(true);
            }

            if (Atexo_Module::isEnabled('RechercheAvanceeParQualification')) {
                $this->panelQualifications->setVisible(true);
            }

            if (Atexo_Module::isEnabled('RechercheAvanceeParAgrement')) {
                $this->panelAgrements->setVisible(true);
            }
        }

        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    public function displayAlerte()
    {
        $tabRefZoneTexteVo = [];
        if ($this->_idAlerte) {
            $alerte = (new Atexo_Entreprise_Alertes())->retreiveAlertById($this->_idAlerte);
            $this->description->Text = $alerte->getDenomination();
            $xmlCritaria = $alerte->getXmlcriteria();
            if ($xmlCritaria) {
                $xml = new SimpleXMLElement($xmlCritaria);
                if (((string) $xml->organisme) != 'NULL') {
                    $this->ministereName->SelectedValue = (string) $xml->organisme;
                }
                if ('NULL' != (string) $xml->entiteAchat) {
                    $this->displayEntityPurchaseModifyAlerte();
                    $this->entiteAchat->SelectedValue = (string) $xml->entiteAchat;
                    if ('oui' == (string) $xml->descendant) {
                        $this->inclureDescendances->checked = true;
                    }
                    if ('non' == (string) $xml->descendant) {
                        $this->entiteSeule->checked = true;
                    }
                }
                if ('NULL' != (string) $xml->categorie) {
                    $this->categorie->SelectedValue = (string) $xml->categorie;
                }
                if (Atexo_Module::isEnabled('LieuxExecution')) {
                    if ('NULL' != (string) $xml->lieuExecution) {
                        $this->idsSelectedGeoN2->Value = (string) $xml->lieuExecution;
                        $this->refreshDisplaySelectedGeoN2();
                    }
                }

                //Codes nuts
                if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
                    if ('NULL' != (string) $xml->codesNuts) {
                        $atexoCodesNutsRef = new Atexo_Ref();
                        $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
                        $atexoCodesNutsRef->setSecondaires((string) $xml->codesNuts);
                        $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
                    }
                }
                if (Atexo_Module::isEnabled('ConsultationClause')) {
                    if ((string) $xml->clauseSociale && 'NULL' != (string) $xml->clauseSociale) {
                        $this->clauseSociales->setSelectedValue((string) $xml->clauseSociale);
                    }
                    if ((string) $xml->atelierProtege && 'NULL' != (string) $xml->atelierProtege) {
                        $this->ateliersProteges->setSelectedValue((string) $xml->atelierProtege);
                    }
                    if ((string) $xml->clauseEnv && 'NULL' != (string) $xml->clauseEnv) {
                        $this->clauseEnvironnementale->setSelectedValue((string) $xml->clauseEnv);
                    }
                }
                /*if (((string)$xml->cpv1!='NULL')||((string)$xml->cpv2!='NULL')
                 ||((string)$xml->cpv3!='NULL')||((string)$xml->cpv4!='NULL')) {
                 $consulationPrincipale = array();
                 $consulationSecondaires = array();
                 if ((string)$xml->cpv1 != 'NULL') {
                 $consulationSecondaires[] = array(0=>(string)$xml->cpv1, 1=>(utf8_decode((string)$xml->libelle_cpv1)));
                 }
                 if ((string)$xml->cpv2 != 'NULL')
                 {
                 $consulationSecondaires[] = array(0=>(string)$xml->cpv2, 1=>(utf8_decode((string)$xml->libelle_cpv2)));

                 }
                 if ((string)$xml->cpv3 != 'NULL')
                 {
                 $consulationSecondaires[] = array(0=>(string)$xml->cpv3, 1=>(utf8_decode((string)$xml->libelle_cpv3)));
                 }
                 if ((string)$xml->cpv4 != 'NULL')
                 {
                 $consulationSecondaires[] = array(0=>(string)$xml->cpv4, 1=>(utf8_decode((string)$xml->libelle_cpv4)));
                 }
                 if(Atexo_Module::isEnabled('AffichageCodeCpv')) {
                 $this->idAtexoCPV->setIdElement("");
                 $this->idAtexoCPV->setPrincipal($consulationPrincipale);
                 $this->idAtexoCPV->setSecondaire($consulationSecondaires);
                 $this->idAtexoCPV->afficherCPV();
                 }
                 }*/
                //<!-- module referentiel CPV
                if (('NULL' != (string) $xml->cpv2)) {
                    if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                        $atexoRef = new Atexo_Ref();
                        $atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_CONFIG'));
                        //demandePrincipal =false
                        //$atexoRef->setPrincipal((string)$xml->cpv1);
                        $atexoRef->setSecondaires((string) $xml->cpv2);
                        $this->idAtexoRef->afficherReferentiel($atexoRef);
                    }
                }
                //-->

                if (Atexo_Module::isEnabled('RechercheAvanceeParDomainesActivite')) {
                    $this->panelDomainesActivites->setVisible(true);
                    $this->domaineActivite->idsDomaines->Text = (string) $xml->domainesActivite;
                }

                if (Atexo_Module::isEnabled('RechercheAvanceeParQualification')) {
                    $this->panelQualifications->setVisible(true);
                    $this->qualification->setIdsQualification(base64_encode((string) $xml->qualifications));
                }

                if (Atexo_Module::isEnabled('RechercheAvanceeParAgrement')) {
                    $this->panelAgrements->setVisible(true);
                    $this->agrements->setIdsSelectedAgrements((string) $xml->agrements);
                }
                if ('NULL' != (string) $xml->motCle) {
                    $this->keywordSearch->Text = utf8_decode((string) $xml->motCle);
                }

                $allLtRef = (new Atexo_Referentiel_Referentiel())->getAllLtReferentiel();
                $tabRefVo = null;
                foreach ($allLtRef as $ltRef) {
                    $balise = (new Atexo_Referentiel_Referentiel())->getNomAttribue($ltRef->getCodeLibelle());
                    $value = utf8_decode((string) $xml->$balise);
                    if ($value && '' != $value && 'NULL' != $value) {
                        $refVo = new Atexo_Referentiel_ReferentielVo();
                        if (0 == $ltRef->getType()) {
                            $refVo->setValueForReferentielVo($ltRef->getId(), '', $value, $ltRef->getTypeSearch());
                            $tabRefVo[$ltRef->getId()] = $refVo;
                        } else {
                            $refVo->setValueForReferentielVo($ltRef->getId(), $value, '', $ltRef->getTypeSearch(), $ltRef->getType());
                            $tabRefZoneTexteVo[$ltRef->getId()] = $refVo;
                        }
                    }
                }
                //              print_r($tabRefVo);exit;
                $this->ltRefCons->afficherReferentielConsultation(null, null, null, $tabRefVo);
                $this->idReferentielZoneText->afficherTextConsultation(null, null, null, $tabRefZoneTexteVo);
            }
            $periodicite = $alerte->getPeriodicite();
            if ($periodicite == Atexo_Config::getParameter('PERIODICITE_DESACTIVER')) {
                $this->desactiver->checked = true;
            } elseif ($periodicite == Atexo_Config::getParameter('PERIODICITE_TOUJOURS')) {
                $this->tousLesJours->checked = true;
            //echo 'ici';
            } elseif ($periodicite == Atexo_Config::getParameter('PERIODICITE_TOUTES_SEMAINES')) {
                $this->toutesLesSemaines->checked = true;
            }

            if ('1' == $alerte->getFormat()) {
                $this->formatHtml->checked = true;
            } elseif ('0' == $alerte->getFormat()) {
                $this->formatTexte->checked = true;
            }
        }
    }

    /**
     * affichage de toutes les ministeres.
     */
    public function displayOrganismes()
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $ArrayOrganismes = [];
        if (isset($_COOKIE['selectedorg'])) {
            $ArrayOrganismesO = null;
            if ('' !== $this->_idAlerte) {
                $alerte = (new Atexo_Entreprise_Alertes())->retreiveAlertById($this->_idAlerte);
                if (null != $alerte) {
                    $xmlCritaria = $alerte->getXmlcriteria();
                    if ($xmlCritaria) {
                        $xml = new SimpleXMLElement($xmlCritaria);
                        if (((string) $xml->organisme) != 'NULL') {
                            $acronyme = (string) $xml->organisme;
                            $ArrayOrganismesO = Atexo_Organismes::retrieveOrganismeByAcronyme($acronyme);
                        }
                    }
                }
            }
            if (null === $ArrayOrganismesO) {
                $ArrayOrganismesO = Atexo_Organismes::retrieveOrganismeByAcronyme($_COOKIE['selectedorg']);
            }
            $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->getDenominationOrgTraduit();
            //$this->organismesNames->SetEnabled(false);
            //$this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = '';</script>";

            $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($ArrayOrganismesO->getAcronyme(), null, $langue);
            $this->entiteAchat->dataSource = $listEntityPurchase;
            $this->entiteAchat->DataBind();
        } else {
            $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $langue);
            array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');
        }
        //Recherche multi-criteres
        $this->ministereName->dataSource = $ArrayOrganismes;
        $this->ministereName->DataBind();
    }

    /**
     * affiche la liste des categories.
     */
    public function displayCategory()
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $categorie = new Atexo_Consultation_Category();
        $CategorieObj = $categorie->retrieveCategories(true, true, $langue);
        $listCategorie = $categorie->retrieveDataCategories($CategorieObj, $langue);
        $arrayCategorie = Atexo_Util::arrayUnshift($listCategorie, '--- '.Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUTES_LES_CATEGORIES').' ---');
        $this->categorie->DataSource = $arrayCategorie;
        $this->categorie->DataBind();
    }

    /**
     * Permet de remplire les listes des clauses.
     */
    public function displayClauses()
    {
        $arrayClausesSociales = [Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER') => Prado::localize('DEFINE_OUI'), Atexo_Config::getParameter('CLAUSES_SOCIALES_DESACTIVER') => Prado::localize('DEFINE_NON')];
        $arrayClausesSociales = Atexo_Util::arrayUnshift($arrayClausesSociales, Prado::localize('TEXT_INDIFFERENT'));
        $this->clauseSociales->DataSource = $arrayClausesSociales;
        $this->clauseSociales->DataBind();

        $this->ateliersProteges->DataSource = $arrayClausesSociales;
        $this->ateliersProteges->DataBind();

        $arrayClausesEnv = [Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER') => Prado::localize('DEFINE_OUI'), Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_DESACTIVER') => Prado::localize('DEFINE_NON')];
        $arrayClausesEnv = Atexo_Util::arrayUnshift($arrayClausesEnv, Prado::localize('TEXT_INDIFFERENT'));
        $this->clauseEnvironnementale->DataSource = $arrayClausesEnv;
        $this->clauseEnvironnementale->DataBind();
    }

    /**
     * action du boutton annuler:retour vers la page de
     * gestion des alertes.
     */
    public function onClickAnnuler($sender, $param)
    {
        $this->Response->Redirect('?page=Entreprise.EntrepriseGestionAlertes');
    }

    /**
     * affichage de toutes les entites d'achats.
     */
    public function displaySerEntityPurchase($sender, $param)
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        // script pour afficher liste des services
        $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = '';</script>";
        //echo $this->ministereName->SelectedValue;
        $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($this->ministereName->SelectedValue, true, $langue);
        //print_r($listEntityPurchase);exit;
        $this->entiteAchat->dataSource = $listEntityPurchase;
        $this->entiteAchat->DataBind();
        $this->panelPurchaseNames->render($param->NewWriter);
    }

    /**
     * affiche les denominations des lieux d'executions.
     */
    public function displaySelectedGeoN2($sender, $param)
    {
        $lieuxExecutionsType1 = null;
        $ids = $this->idsSelectedGeoN2->Value;
        $arIds = explode(',', $ids);
        $list = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
        if ($list) {
            foreach ($list as $oneGeoN2) {
                $lieuxExecutionsType1 .= $oneGeoN2->getDenomination1Traduit().', ';
            }
            if ($lieuxExecutionsType1) {
                $this->denominationGeo2T->Text = Atexo_Util::toUtf8(substr($lieuxExecutionsType1, 0, -2));
            }
        }

        // Re-affichage du TActivePanel pour cause de probleme d'accent
    }

    /**
     * affiche les denominations des lieux d'executions.
     */
    public function refreshDisplaySelectedGeoN2()
    {
        $lieuxExecutionsType1 = null;
        $ids = $this->idsSelectedGeoN2->Value;
        $arIds = explode(',', $ids);
        $list = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
        if ($list) {
            foreach ($list as $oneGeoN2) {
                $lieuxExecutionsType1 .= ($oneGeoN2->getDenomination1Traduit()).', ';
            }
            if ($lieuxExecutionsType1) {
                if ($this->IsPostBack) {
                    $this->denominationGeo2T->Text = Atexo_Util::toUtf8(substr($lieuxExecutionsType1, 0, -2));
                } else {
                    $this->denominationGeo2T->Text = substr($lieuxExecutionsType1, 0, -2);
                }
            }
        } else {
            $this->denominationGeo2T->Text = '';
        }
    }

    /**
     * generer le fichier xml qui contient les criteres de recherche.
     */
    public function generateXml()
    {
        if (file_exists(Atexo_Config::getParameter('XML_ALERTE_PATH'))) {
            $xml = simplexml_load_file(Atexo_Config::getParameter('XML_ALERTE_PATH'));
            if ('0' != $this->ministereName->getSelectedValue()) {
                $xml->organisme = $this->ministereName->getSelectedValue();
                $xml->entiteAchat = 'NULL';
                $xml->descendant = 'non';
                if (('0' != $this->entiteAchat->getSelectedValue()) && ($this->entiteAchat->getSelectedValue())) {
                    $xml->entiteAchat = $this->entiteAchat->getSelectedValue();
                    if (true == $this->inclureDescendances->Checked) {
                        $xml->descendant = 'oui';
                    }
                }
            } else {
                $xml->organisme = 'NULL';
                $xml->entiteAchat = 'NULL';
            }

            $xml->typeAnnonce = Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION');
            $xml->typeProcedure = 'NULL';
            if ('0' != $this->categorie->getSelectedValue()) {
                $xml->categorie = $this->categorie->SelectedValue;
            } else {
                $xml->categorie = 'NULL';
            }
            if (Atexo_Module::isEnabled('LieuxExecution')) {
                if (trim($this->idsSelectedGeoN2->Value)) {
                    $xml->lieuExecution = trim($this->idsSelectedGeoN2->Value);
                } else {
                    $xml->lieuExecution = 'NULL';
                }
            }

            //Codes nuts
            if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
                if ($this->idAtexoRefNuts->codesRefSec->value) {
                    $xml->codesNuts = $this->idAtexoRefNuts->codesRefSec->value;
                }
            }
            if (Atexo_Module::isEnabled('ConsultationClause')) {
                if ('0' != $this->clauseSociales->getSelectedValue()) {
                    $xml->clauseSociale = $this->clauseSociales->getSelectedValue();
                } else {
                    $xml->clauseSociale = 'NULL';
                }
                if ('0' != $this->ateliersProteges->getSelectedValue()) {
                    $xml->atelierProtege = $this->ateliersProteges->getSelectedValue();
                } else {
                    $xml->atelierProtege = 'NULL';
                }
                if ('0' != $this->clauseEnvironnementale->getSelectedValue()) {
                    $xml->clauseEnv = $this->clauseEnvironnementale->getSelectedValue();
                } else {
                    $xml->clauseEnv = 'NULL';
                }
            }

            /*if(Atexo_Module::isEnabled('AffichageCodeCpv')) {
             if ($this->idAtexoCPV->code_cpv1->Text!='') {
             $xml->cpv1 = $this->idAtexoCPV->code_cpv1->Text;
             $xml->libelle_cpv1 = utf8_encode($this->idAtexoCPV->code_cpv_libelle1->Text);
             }
             else {
             $xml->cpv1 = 'NULL';
             $xml->libelle_cpv1 = 'NULL';
             }
             if ($this->idAtexoCPV->code_cpv2->Text!='') {
             $xml->cpv2 = $this->idAtexoCPV->code_cpv2->Text;
             $xml->libelle_cpv2 = utf8_encode($this->idAtexoCPV->code_cpv_libelle2->Text);
             }
             else {
             $xml->cpv2 = 'NULL';
             $xml->libelle_cpv2 = 'NULL';
             }
             if ($this->idAtexoCPV->code_cpv3->Text!='') {
             $xml->cpv3 = $this->idAtexoCPV->code_cpv3->Text;
             $xml->libelle_cpv3 = utf8_encode($this->idAtexoCPV->code_cpv_libelle3->Text);
             }
             else {
             $xml->cpv3 = 'NULL';
             $xml->libelle_cpv3 = 'NULL';
             }
             if ($this->idAtexoCPV->code_cpv4->Text!='') {
             $xml->cpv4 = $this->idAtexoCPV->code_cpv4->Text;
             $xml->libelle_cpv4 = utf8_encode($this->idAtexoCPV->code_cpv_libelle4->Text);
             }
             else {
             $xml->cpv4 = 'NULL';
             $xml->libelle_cpv4 = 'NULL';
             }
             }*/
            //<!-- Module referentiel CPV
            if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                //demandePrincipal = false
                /*if ($this->idAtexoRef->codeRefPrinc->Value!='') {
                    $xml->cpv1 = $this->idAtexoRef->codeRefPrinc->Value;
                    }
                    else {
                    $xml->cpv1 = 'NULL';
                    }*/
                if ('' != $this->idAtexoRef->codesRefSec->Value) {
                    $xml->cpv2 = $this->idAtexoRef->codesRefSec->Value;
                } else {
                    $xml->cpv2 = 'NULL';
                }
            }
            //-->

            if ('0' != $this->keywordSearch->Text) {
                $motcle = utf8_encode(str_replace('&', '&amp;', $this->keywordSearch->Text));
                $xml->motCle = $motcle;
            } else {
                $xml->motCle = 'NULL';
            }

            if (Atexo_Module::isEnabled('RechercheAvanceeParDomainesActivite')) {
                if ($this->domaineActivite->idsDomaines->Text) {
                    $xml->domainesActivite = $this->domaineActivite->idsDomaines->Text;
                } else {
                    $xml->domainesActivite = 'NULL';
                }
            } else {
                $xml->domainesActivite = 'NULL';
            }

            if (Atexo_Module::isEnabled('RechercheAvanceeParQualification')) {
                if ($this->qualification->idsQualification->Text) {
                    $xml->qualifications = base64_decode($this->qualification->idsQualification->Text);
                } else {
                    $xml->qualifications = 'NULL';
                }
            } else {
                $xml->qualifications = 'NULL';
            }

            if (Atexo_Module::isEnabled('RechercheAvanceeParAgrement')) {
                if ($this->agrements->idsSelectedAgrements->Value) {
                    $xml->agrements = $this->agrements->idsSelectedAgrements->Value;
                } else {
                    $xml->agrements = 'NULL';
                }
            } else {
                $xml->agrements = 'NULL';
            }

            $referentiels = $this->ltRefCons->getValeurForXml();
            if (is_array($referentiels)) {
                foreach ($referentiels as $balise => $value) {
                    $xml->addChild($balise, $value);
                }
            }

            $referentielsTexte = $this->idReferentielZoneText->getValeurForXml();
            if (is_array($referentielsTexte)) {
                foreach ($referentielsTexte as $balise => $value) {
                    $xml->addChild($balise, $value);
                }
            }
            //print_r($xml);exit;
            if ($xml->asXML()) {
                return $xml->asXML();
            }
        }
    }

    public function saveAlerte()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($this->_idAlerte) {
            $newAlerte = CommonAlertePeer::retrieveByPK($this->_idAlerte, $connexionCom);
        } else {
            $newAlerte = new CommonAlerte();
        }
        $newAlerte->setIdInscrit(Atexo_CurrentUser::getIdInscrit());
        $newAlerte->setDenomination($this->description->Text);
        $newAlerte->setXmlcriteria(self::generateXml());
        if (true == $this->tousLesJours->checked) {
            $newAlerte->setPeriodicite(Atexo_Config::getParameter('PERIODICITE_TOUJOURS'));
        } elseif (true == $this->toutesLesSemaines->checked) {
            $newAlerte->setPeriodicite(Atexo_Config::getParameter('PERIODICITE_TOUTES_SEMAINES'));
        } elseif (true == $this->desactiver->checked) {
            $newAlerte->setPeriodicite(Atexo_Config::getParameter('PERIODICITE_DESACTIVER'));
        }
        if ('0' != $this->categorie->getSelectedValue()) {
            $newAlerte->setCategorie($this->categorie->SelectedValue);
        } else {
            $newAlerte->setCategorie(null);
        }
        if (true == $this->formatHtml->checked) {
            $newAlerte->setFormat('1');
        } elseif (true == $this->formatTexte->checked) {
            $newAlerte->setFormat('0');
        }
        $newAlerte->save($connexionCom);
        try {
            return $newAlerte->save($connexionCom);
        } catch (Exception) {
            return -1;
        }
    }

    public function actionButtonSave($sender, $param)
    {
        $resultSave = self::saveAlerte();
        if ($resultSave >= 0) {
            $this->response->redirect('index.php?page=Entreprise.EntrepriseGestionAlertes');
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_MESSAGE_ERREUR_SAVE_ALERT'));
        }
    }

    public function displayEntityPurchaseModifyAlerte()
    {
        // script pour afficher liste des services
        $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = '';</script>";
        $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($this->ministereName->SelectedValue);
        $this->entiteAchat->dataSource = $listEntityPurchase;
        $this->entiteAchat->DataBind();
    }

    public function getLieuExecutionUrl()
    {
        return Atexo_LieuxExecution::getLieuExecutionUrl();
    }

    public function fillClassification()
    {
        $codesInsee = CommonCategorieINSEEPeer::doSelect(new Criteria(), Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));
        $codesInsee = is_array($codesInsee) ? $codesInsee : [];
        $dataSource = ['0' => '--- '.Prado::localize('TOUTES_LES_CLASSIFICATIONS').' ---'];
        foreach ($codesInsee as $insee) {
            $dataSource[$insee->getId()] = $insee->getLibelle();
        }
        $this->classification->setDataSource($dataSource);
        $this->classification->DataBind();
    }

    public function choixClassification($sender, $param)
    {
        $idClassification = $this->classification->getSelectedValue();

        $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, Atexo_CurrentUser::readFromSession('lang'), true, $idClassification);
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');

        $this->ministereName->dataSource = $ArrayOrganismes;
        $this->ministereName->DataBind();

        $this->panelPurchaseNames->render($param->NewWriter);
    }

    public function refreshComposants($sender, $param)
    {
        $this->domaineActivite->panelDomaineActivite->render($param->NewWriter);
    }
}
