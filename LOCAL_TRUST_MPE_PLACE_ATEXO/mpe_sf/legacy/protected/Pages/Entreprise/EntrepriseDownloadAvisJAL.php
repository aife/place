<?php

namespace Application\Pages\Entreprise;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;

/**
 * Permet de télécharger le AVIS.
 *
 * @author mouslim MITALI <mouslim.Mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDownloadAvisJAL extends DownloadFile
{
    private $_idAvis;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->_idAvis = Atexo_Util::atexoHtmlEntities($_GET['idAvis']);
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        if ($consultation) {
            $avis = (new Atexo_Publicite_Avis())->retreiveAvisByIdAndRefCons($this->_idAvis, $consultation->getId(), $consultation->getOrganisme(), $connexion);
            if ($avis) {
                //Nom du fichier joint
                $nomFichier = $avis->getNomFichier();
                if (Atexo_Module::isEnabled('PubliciteOpoce')) {
                    //Début récuperation de l'identifiant de l'avis global
                    $avisPub = (new Atexo_Publicite_AvisPub())->getAvisByIdAvisPortailAndRefConsultation($avis->getId(), $consultation->getId(), $consultation->getOrganisme(), $avis->getType());
                    $idavisPub = '';
                    if ($avisPub instanceof CommonAvisPub) {
                        $idavisPub = $avisPub->getId();
                    }
                    if ($avis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED')) {
                        $nomFichier = 'Avis_TED_'.(new Atexo_Publicite_AvisPub())->getNomFichierAnnoncePresse($consultation->getId(), $idavisPub).'.pdf';
                    } elseif ($avis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT')) {
                        $nomFichier = 'Avis_presse_'.(new Atexo_Publicite_AvisPub())->getNomFichierAnnoncePresse($consultation->getId(), $idavisPub).'.pdf';
                    }
                }
                $this->_idFichier = $avis->getAvis();
                $this->_nomFichier = $nomFichier;
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            }
        }
    }
}
