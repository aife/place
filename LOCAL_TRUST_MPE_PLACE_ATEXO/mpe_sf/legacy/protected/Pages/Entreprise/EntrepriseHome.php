<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonTMessageAccueil;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Page d'acceuil coté entreprise identification ou inscription.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseHome extends MpeTPage
{
    public string $lblMsgAccueilTruncate = '';
    public string $pageTitle = 'TITRE';
    public $messageClassName = 'info';

    public function onInit($param)
    {
        $this->pageTitle = 'TITRE_CONNEXION_INSCRIPTION';
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        Atexo_Util::setNonav(false);
    }

    public function onLoad($param)
    {
        $this->customizeForm();
        if (!$this->User->IsGuest && !Atexo_CurrentUser::isAgent() && !Atexo_CurrentUser::isAgentSocle()) {
            $this->response->redirect(
                Atexo_Config::getParameter('PF_URL') . "?page=Entreprise.EntrepriseAccueilAuthentifie"
            );
        } elseif (
            (Atexo_CurrentUser::isAgent()
                || Atexo_CurrentUser::isAgentSocle()) && Atexo_CurrentUser::isConnected()
        ) {
            $this->getApplication()->getModule("auth")->logout();
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . "?page=Entreprise.EntrepriseHome");
        }

        if (Atexo_Module::isEnabled('SocleExternePpp') && !Atexo_CurrentUser::getIdInscrit()) {
            $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
            if (isset($headersArray['EXTERNALID']) && isset($headersArray['USERTYPE']) && 'EMPLOYEE' == $headersArray['USERTYPE']) {
                self::authenticateViaPPPSocleEntreprise($headersArray['EXTERNALID']);
            } else {
                if ($_GET['goto'] && Atexo_Util::urlIsAllow($_GET['goto'])) {
                    $this->response->redirect(Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGIN'));
                }
            }
        }

        $this->displayBlocIdantification();
        $this->hideMessage();
        $this->panelMessageWarningChrome->setVisible(true);
        $this->panelMessageWarningChrome->setMessage(Prado::localize('TEXT_WARNING_BROWSER_CHROME_ESE'));
        $this->WarningChrome->initiComposant(Prado::localize('TEXT_WARNING_BROWSER_CHROME_ESE'));

        $message = Atexo_Message::retrieveMessageAccueilByDestinataire('entreprise', null);
        $cssClass = 'bloc-message form-bloc-conf msg-';
        if ($message instanceof CommonTMessageAccueil) {
            if (!empty($message->getContenu())) {
                $this->labelAccueilPonctuel->setCssClass($cssClass . $message->getTypeMessage());
                $this->lblMsgAccueilTruncate = Atexo_Util::truncateHTMLContents($message->getContenu(), 200);
                $this->messageClassName = strtr($message->getTypeMessage(), [
                    'info' => 'info',
                    'avertissement' => 'warning',
                    'erreur' => 'danger'
                ]);
                $this->lblMsgAccueil->setText(html_entity_decode($message->getContenu()));
                $this->labelAccueilPonctuel->setVisible(true);
            } else {
                $this->labelAccueilPonctuel->setVisible(false);
            }
        } else {
            if ('MSG_ACCUEIL_PONCTUEL' != Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_ENTREPRISE')) {
                $this->lblMsgAccueilTruncate = Atexo_Util::truncateHTMLContents(
                    (new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_ENTREPRISE')),
                    200
                );
                $this->lblMsgAccueil->setText(
                    html_entity_decode(
                        (new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_ENTREPRISE'))
                    )
                );
                $this->labelAccueilPonctuel->setCssClass(
                    $cssClass . Atexo_Config::getParameter('MSG_ACCUEIL_PONCTUEL_ENTREPRISE_TYPE')
                );
                $this->labelAccueilPonctuel->setVisible(true);
            } else {
                $this->labelAccueilPonctuel->setVisible(false);
            }
        }
    }

    /**
     * Affiche bloc d'identification selon la valeur du champs
     * AuthenticateInscritByCert dans la table moduleCommun.
     */
    public function displayBlocIdantification()
    {
        if ($_GET['goto'] && Atexo_Util::urlIsAllow($_GET['goto'])) {
            if (Atexo_Module::isEnabled('SocleExterneEntreprise')) {
                $this->response->redirect(Atexo_Util::atexoHtmlEntities($_GET['goto']));
            }
            $this->panelDemandeAuthentification->setVisible(false);
            $this->messageAccueil->setVisible(false);
        } else {
            $this->panelDemandeAuthentification->setVisible(false);
            $this->messageAccueil->setVisible(true);
            Atexo_Util::setBodyClass('entreprise_home accueil_entreprise_personnalise');
            if (Atexo_Module::isEnabled('AccueilEntreprisePersonnalise')) {
                $this->response->redirect('entreprise');
            }
        }
    }

    /*
     * Redirige vers la page InscriptionUtilisateur
     * @param siren
     * @param siret
     */
    public function siretOkButton()
    {
        $siren = $this->siren->Text;
        $siret = $this->siret->Text;
        $url = Atexo_Config::getParameter(
            'PF_URL'
        ) . '?page=Entreprise.FormulaireCompteEntreprise&action=creation&siren=' . $siren;
        if ($_GET['goto']) {
            $url .= '&goto=' . urlencode(Atexo_Util::atexoHtmlEntities($_GET['goto']));
        }
        if ('' != $siren && '' == $siret) {
            $this->response->redirect($url);
        } elseif ('' != $siren && '' != $siret) {
            $this->response->redirect($url . '&siret=' . $siret);
        }
    }

    public function idNationalOkPressed()
    {
        $idNational = $this->idNational->Text;
        if ((0 != $this->listPays->SelectedIndex) && '' != $idNational) {
            $url = Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.FormulaireCompteEntreprise&action=creation&idNational='
            . $idNational . '&pays=' . substr($this->listPays->getSelectedItem()->getText(), 0, -6);
            if ($_GET['goto']) {
                $url .= '&goto=' . urlencode(Atexo_Util::atexoHtmlEntities($_GET['goto']));
            }
            $this->response->redirect($url);
        }
    }

    /**
     * @return mixed
     */
    public function isSiretAlertEntrepriseHomePage()
    {
        return (new Atexo_CurrentUser())->isSiretAlert();
    }

    public function VerifyAuthLogin()
    {
        //@todo : [TAG] : Diff
        // Coquille vide car authentification captée par SF
    }

    public function VerifyAuthCertif($sender, $param)
    {
        // Coquille vide car authentification captée par SF
    }

    public function displayMessage($text)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($text);
    }

    public function hideMessage()
    {
        $this->panelMessageErreur->setVisible(false);
    }

    public function displayVilleRc()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER') . ' ...';
        $data['99'] = Prado::localize('JE_PAS_DE_RC');
        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->RcVille->DataSource = $data;
        $this->RcVille->DataBind();
    }

    public function RcOk()
    {
        $villeRc = $this->RcVille->getSelectedValue();
        $numeroRc = $this->RcNumero->Text;
        if ('' != $villeRc && '' != $numeroRc) {
            $rc = $villeRc . Atexo_Config::getParameter('SEPARATEUR_VILLE_RC') . $numeroRc;
            $url = Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.FormulaireCompteEntreprise&action=creation&siren=' . $rc;
            if ($_GET['goto']) {
                $url .= '&goto=' . urlencode(Atexo_Util::atexoHtmlEntities($_GET['goto']));
            }
            $this->response->redirect($url);
        }
    }

    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
            $this->panelSiren->setVisible(false);
            $this->panelRc->setVisible(true);
            $this->SirenValidator->SetEnabled(false);
            $this->panelIdentifiantUnique->setVisible(false);
            $this->displayVilleRc();
        } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
            $this->panelIdentifiantUnique->setVisible(true);
            $this->panelSiren->setVisible(false);
            $this->panelRc->setVisible(false);
            $this->SirenValidator->SetEnabled(false);
        } else {
            $this->panelSiren->setVisible(true);
            $this->panelRc->setVisible(false);
            $this->SirenValidator->SetEnabled(true);
            $this->panelIdentifiantUnique->setVisible(false);
        }
    }

    /*
     * Redirige vers la page InscriptionUtilisateur
     * @param siret
     */
    public function identifiantUniqueOkPressed()
    {
        $identifiantUnique = $this->identifiantUnique->Text;
        $url = Atexo_Config::getParameter(
            'PF_URL'
        ) . '?page=Entreprise.FormulaireCompteEntreprise&action=creation&siren=' . $identifiantUnique;
        if ($_GET['goto']) {
            $url .= '&goto=' . urlencode(Atexo_Util::atexoHtmlEntities($_GET['goto']));
        }
        if ('' != $identifiantUnique) {
            $this->response->redirect($url);
        }
    }

    public function refreshListPays($sender, $param)
    {
        $this->listPays->fillDataSource();
        //$this->listPays->render($param->getNewWriter());
    }

    public function authenticateViaPPPSocleEntreprise($idExterne)
    {
        // Coquille vide car authentification captée par SF
    }
}
