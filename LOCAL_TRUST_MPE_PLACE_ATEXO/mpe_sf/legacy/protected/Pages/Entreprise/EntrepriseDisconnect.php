<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;

/**
 * Page de deconnexion.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDisconnect extends MpeTPage
{
    public function onLoad($param)
    {
        $isFCUser = Atexo_CurrentUser::isAuthenticatedWithFc() ? true : false;

        $this->getApplication()->getModule('auth')->logout();
        // Récupération du nom de session du concentrateur fs_fc, pour dire à la fonction detruireCookies() de ne pas supprimer le cookie de la session passée en paramètre. Dans notre cas '$fcSessionName'
        // Ce test est fait seulement quand il s'agit d'une deconnexion de FranceConnect. dans le cas du concentrateur fs_fc nous laissons le soins au serveur de detruire la session au moment opportun,
        // pas au client de détruire son cookie.
        if ($isFCUser) {
            Atexo_Util::detruireCookies([Atexo_Config::getParameter('CONCENTRATEUR_FC_NOM_SESSION')]);
            $this->response->redirect(Atexo_Config::getParameter('URL_FRANCECONNECT_CONCENTRATEUR_LOGOUT'));
        } else {
            Atexo_Util::detruireCookies();
        }

        //Eventuellement deconnecter l'utilisateur dans la session mpe symfony
        if (isset($_GET['callFromMpeSf']) && 1 == Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
            $this->response->redirect(Atexo_Config::getParameter('URL_LT_MPE_SF_DECONNEXION'));
        }

        if (Atexo_Module::isEnabled('SocleExterneEntreprise')) {
            $this->response->redirect(Atexo_Config::getParameter('URL_INDEX_ENTREPRISE_DISCONECT'));
        } elseif (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->response->redirect(Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGOUT'));
        } else {
            $this->response->redirect('?page=Entreprise.EntrepriseHome');
        }
    }
}
