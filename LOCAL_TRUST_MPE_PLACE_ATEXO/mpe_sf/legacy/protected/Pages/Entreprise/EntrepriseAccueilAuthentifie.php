<?php

namespace Application\Pages\Entreprise;

use App\Service\AtexoMenu;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEntreprisePeer;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_InscritHistorique;
use Prado\Prado;
use Application\Service\Atexo\Atexo_Util;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseAccueilAuthentifie extends MpeTPage
{
    public string $pageTitle = 'TITRE';

    public function onInit($param)
    {
        $this->pageTitle = 'TITRE_ACCUEIL_AUTHENTIFIE';
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($this->User->IsGuest) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
        }
        if ($_GET['goto'] && Atexo_Util::urlIsAllow($_GET['goto']) && Atexo_Module::isEnabled('SocleExterneEntreprise')) {
            $this->response->redirect(Atexo_Util::atexoHtmlEntities($_GET['goto']));
        }
        $this->scriptJs->Text = '';
        $dateDebAction = date('Y-m-d H:i:s');
        $this->cutomizePage();

        if (!$this->IsPostBack) {
            if (
                Atexo_Util::getSfService(AtexoMenu::class)->isDossierVolumineux() &&
                $this->isConnected()
            ) {
                $this->linkDossierVolumineux->setVisible(true);
            }

            $this->panelMessageErreur->setVisible(false);
            $this->panelMessage->setVisible(false);
            if (isset($_GET['action'])) {
                if (0 == strcmp($_GET['action'], 'ChangingUserSuccess')) {
                    $this->panelMessage->setVisible(true);
                    $this->panelMessage->setMessage(Prado::localize('TEXT_COMPTE_MODIFIER_AVEC_SUCCE'));
                } elseif (0 == strcmp($_GET['action'], 'ChangingUserfailur')) {
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_MODIFICATION_COMPTE'));
                } elseif (0 == strcmp($_GET['action'], 'orgF')) {
                    $this->panelMessage->setVisible(true);
                    $this->panelMessage->setMessage(Prado::localize('CG76_DC_ACEC_SUCCESS'));
                }
            }
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION1', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            $arrayDonnees = [];
            $arrayDonnees['ref'] = '';
            $arrayDonnees['org'] = '';
            $arrayDonnees['IdDescritpion'] = $IdDesciption;
            $arrayDonnees['description'] = $description;
            $arrayDonnees['afficher'] = true;
            $arrayDonnees['logApplet'] = '';
            Atexo_InscritOperationsTracker::trackingOperations(
                Atexo_CurrentUser::getIdInscrit(),
                Atexo_CurrentUser::getIdEntreprise(),
                $_SERVER['REMOTE_ADDR'],
                date('Y-m-d'),
                $dateDebAction,
                substr($_SERVER['QUERY_STRING'], 0),
                '-',
                date('Y-m-d H:i:s'),
                $arrayDonnees
            );
        }
    }

    /**
     * activer le lien si l'utilisateur authentifie est un ATES.
     */
    public function goToChangingCompnay()
    {
        if (Atexo_CurrentUser::isATES()) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.FormulaireCompteEntreprise&action=ChangingCompany');
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_DROIT_ACCES_MODIFIER_ENTREPRISE_INVALIDE'));
        }
    }

    public function goToDescriptionActivity()
    {
        if (Atexo_CurrentUser::isATES()) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseGestionCompteActivite');
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_DROIT_ACCES_MODIFIER_ENTREPRISE_INVALIDE'));
        }
    }

    public function gotoEspaceDocumentaire()
    {
        if (Atexo_CurrentUser::isATES()) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseEspaceDocumentaire');
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_DROIT_ACCES_MODIFIER_ENTREPRISE_INVALIDE'));
        }
    }

    /**
     * Permet de supprimer un compte utilisateur (simple ou administrateur).
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function deleteCompteInscrit()
    {
        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveAllAdmins(Atexo_CurrentUser::getIdEntreprise());
        if (
            !(is_countable($inscrit) ? count($inscrit) : 0)
            || (1 == (is_countable($inscrit) ? count($inscrit) : 0) && ($inscrit[0]->getId() == Atexo_CurrentUser::getIdInscrit()))
        ) {
            $message = Prado::localize('ERREUR_SUPPRESSION_COMPTE_ATES');
            if ($this->visibiliteComplementMessageSuppressionCompte()) {
                $message .= '<br/>' . Prado::localize('TEXTE_COMPLEMENT_MESSAGE_SUPPRESSION_COMPTE_PERSONNEL_INSCRIT');
            }
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage($message);

            return;
        }
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB')
            . Atexo_Config::getParameter('CONST_READ_WRITE')
        );
        $user = CommonInscritPeer::retrieveByPK(Atexo_CurrentUser::getIdInscrit(), $connexion);
        if ($user) {
            //Tracer l'action (nous avons tracé avant de supprimer pour ne pas perdre l'info sur le profil)
            $nom = $user->getPrenom() . ' ' . $user->getNom();
            $mail = $user->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique(
                $nom,
                $mail,
                Atexo_Config::getParameter('EVENEMENT_ADMIN_DELETE')
            );

            $userOldData = $user;
            $this->suppressionLogiqueInscrit($user, $connexion);

            //Envoi de mail
            $msgCorpsPersonaliseAdmin = Prado::localize('DEFINE_CORPS_MAIL_SUPPRESSION_COMPTE_UTILISATEUR_TO_ADMIN');
            $msgCorpsPersonaliseAdmin = str_replace(
                '[__NOM_PRENOM_UTILISATEUR__]',
                $userOldData->getPrenom() . ' ' . $userOldData->getNom(),
                $msgCorpsPersonaliseAdmin
            );
            $msgCorpsPersonaliseUser = Prado::localize('DEFINE_CORPS_MAIL_SUPPRESSION_COMPTE_UTILISATEUR_TO_INSCRIT');
            $objet = Prado::localize('DEFINE_TITRE_MAIL_SUPPRESSION_COMPTE_UTILISATEUR');
            $profilInscrit = (new Atexo_Entreprise_Inscrit())->getProfilByIdInscrit(Atexo_CurrentUser::getIdInscrit());
            if ($profilInscrit == Atexo_Config::getParameter('PROFIL_INSCRIT_ATES')) {
                (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise(
                    $userOldData,
                    $objet,
                    $msgCorpsPersonaliseUser
                );
            } else {
                //Envoi mail de notification aux admins et a l'utilisateur supprime
                (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise(
                    $userOldData,
                    $objet,
                    $msgCorpsPersonaliseUser
                );
                //Envoi mail de notification a l'administrateur
                $allAdmin = (new Atexo_Entreprise_Inscrit())->retrieveAllAdmins(Atexo_CurrentUser::getIdEntreprise());
                if (is_array($allAdmin) && count($allAdmin)) {
                    foreach ($allAdmin as $admin) {
                        (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise(
                            $admin,
                            $objet,
                            $msgCorpsPersonaliseAdmin
                        );
                    }
                }
            }

            // On détruit les cookies avant de rediriger
            Atexo_Util::detruireCookies();

            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseDisconnect');
        }
    }

    public function gotoGestionUsers()
    {
        if (Atexo_CurrentUser::isATES()) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . 'index.php?page=Entreprise.EntrepriseGestionUtilisateurs');
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_DROIT_ACCES_MODIFIER_ENTREPRISE_INVALIDE'));
        }
    }

    public function gotoDonneeComplementaire()
    {
        if (Atexo_CurrentUser::isATES()) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . 'index.php?page=Entreprise.DonneesComplementaires');
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_DROIT_ACCES_MODIFIER_ENTREPRISE_INVALIDE'));
        }
    }

    public function gotoUpdateProfile()
    {
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->response->redirect(Atexo_Config::getParameter('URL_PPP_MON_COMPTE_ENTREPRISE'));
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.FormulaireCompteEntreprise&action=ChangingUser');
        }
    }

    /**
     * @return mixed
     */
    public function isSiretAlertEntrepriseAccueilAuthentifie()
    {
        $isSiretAlert = (bool)Atexo_CurrentUser::readFromSession('siretAlert');
        Atexo_CurrentUser::writeToSession('siretAlert', null);
        return $isSiretAlert;
    }

    public function cutomizePage()
    {
        $this->name->Text = ' ' . Atexo_CurrentUser::getFirstNameInscrit() . ' ' . Atexo_CurrentUser::getLastNameInscrit() . '';
        $this->titreBlocMonCompte->Text = Prado::localize('TEXT_MON_COMPTE_PERSONNEL_MIN');
        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
        $adresse = '';
        $codeEtablissement = '';
        if ($inscrit instanceof CommonInscrit) {
            if (0 != $inscrit->getIdEtablissement()) {
                $etab = CommonTEtablissementPeer::retrieveByPK($inscrit->getIdEtablissement());
                if ('' != $etab->getCodeEtablissement()) {
                    $codeEtablissement = ' ' . $etab->getCodeEtablissement() . ', ';
                }
                $adresse1 = $etab->getAdresse();
                if ('' != $etab->getAdresse2()) {
                    $adresse1 .= ' ' . $etab->getAdresse2();
                }

                if (!empty($adresse1)) {
                    $adresse = ' ' . $adresse1 . ', ';
                }
                if ('' != $etab->getCodePostal()) {
                    $adresse .= $etab->getCodePostal() . ' ';
                }
                if ('' != $etab->getVille()) {
                    $adresse .= $etab->getVille();
                }
            }
            if (0 != $inscrit->getEntrepriseId()) {
                $entreprise = CommonEntreprisePeer::retrieveByPK($inscrit->getEntrepriseId());
                $this->nameEntreprise->Text = ' ' . $entreprise->getNom() . '.';
            }
        }

        $this->adressEtablissement->Text = $adresse . '.';
        $this->codeEtablissement->Text = $codeEtablissement;





        if (Atexo_CurrentUser::isATES()) {
            $this->profil->Text = ' ' . Prado::localize('TEXT_ADMINISTRATEUR_ACCUEIL') . ' ';
        } else {
            $this->profil->Text = ' ' . Prado::localize('TEXT_UTILISATEUR_ACCUEIL') . ' ';
        }
        if (!Atexo_CurrentUser::isATES()) {
            $this->panelCompanyCompte->setVisible(false);
            $this->breakerServicesSePreparerARepondre->setVisible(true);
        } else {
            $this->panelCompanyCompte->setVisible(true);
            $this->breakerServicesSePreparerARepondre->setVisible(false);
        }
    }

    /**
     * Permet de gerer la visibilite du complement de message affiche lors de la suppression d'un compte utilisateur (simple ou admin).
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @return bool: true si message à afficher, false sinon
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function visibiliteComplementMessageSuppressionCompte()
    {
        $libelleMsgSuppression = 'TEXTE_COMPLEMENT_MESSAGE_SUPPRESSION_COMPTE_PERSONNEL_INSCRIT';
        if (Atexo_CurrentUser::isATES() && 0 != strcmp(Prado::localize($libelleMsgSuppression), $libelleMsgSuppression)) {
            return true;
        }

        return false;
    }

    public function gotoEvenementsAdmin()
    {
        $this->response->redirect(Atexo_Config::getParameter('PF_URL') . 'index.php?page=Entreprise.EntrepriseEvenementsAdmin');
    }

    private function suppressionLogiqueInscrit(CommonInscrit $inscrit, $connexion)
    {
        /**
         * Anonymisation
         */
        $idUser = $inscrit->getId();
        $idEntrepise = $inscrit->getEntrepriseId();
        $idEtablissement = $inscrit->getIdEtablissement();
        $idRandom = 1 + rand() * 1000;
        $inscrit->setBloque('1');
        $inscrit->setDeleted(true);
        $inscrit->setDeletedAt(new \DateTime());
        $inscrit->setLogin('login_' . $idUser);
        $inscrit->setMdp('mdp_' . $idUser);
        $inscrit->setTentativesMdp(666);
        $inscrit->setNom('nom_inscrit_' . $idUser);
        $inscrit->setPrenom('prenom_inscrit_' . $idUser);
        $inscrit->setAdresse('adresse_inscrit_' . $idUser);
        $inscrit->setEmail('inscrit_'
            . $idUser
            . '-'
            . $idEntrepise
            . '-'
            . $idEtablissement
            . $idRandom
            . "@atexo-anonymise.com");
        $inscrit->save($connexion);
    }
}
