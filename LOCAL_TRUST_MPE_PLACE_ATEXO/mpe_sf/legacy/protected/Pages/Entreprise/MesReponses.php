<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class MesReponses extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $this->panelMessage->setVisible(false);
        if (isset($_GET['Success'])) {
            $this->panelMessage->setVisible(true);
            $this->panelMessage->setMessage(Prado::localize('TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION'));
        }
        if (!$this->IsPostBack) {
            $nombreReponses = (new Atexo_Consultation_Responses())->retrieveInscritResponses(Atexo_CurrentUser::getIdInscrit(), 0, 0, '', '', true);
            if ($nombreReponses > 0) {
                $this->panelAucunResultat->setVisible(false);
                $this->nombreResultatTop->Text = $nombreReponses;
                $this->nombreResultatBottom->Text = $nombreReponses;
                $this->numeroPageBottom->Text = ceil($nombreReponses / $this->mesReponses->PageSize);
                $this->numeroPageTop->Text = ceil($nombreReponses / $this->mesReponses->PageSize);
                $this->pageNumberBottom->Text = '1';
                $this->pageNumberTop->Text = '1';
                $this->setViewState('nombreElement', $nombreReponses);
                $this->setViewState('nombrePages', ceil($nombreReponses / $this->mesReponses->PageSize));
                $this->setViewState('numeroPage', 1);
                $this->mesReponses->setVirtualItemCount($nombreReponses);
                $this->mesReponses->setCurrentPageIndex(0);
                $this->setViewState('sensTri', 'DESC');
                $this->setViewState('trierPar', 'Untrusteddate');
                $this->populateData();
            } else {
                $this->panelResultat1->setVisible(false);
                $this->panelResultat2->setVisible(false);
                $this->panelAucunResultat->setVisible(true);
                $this->panelMesReponses->setVisible(true);
            }
        }
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            '-',
            date('Y-m-d H:i:s')
        );
    }

    public function populateData()
    {
        $offset = $this->mesReponses->CurrentPageIndex * $this->mesReponses->PageSize;
        $limit = $this->mesReponses->PageSize;
        if ($offset + $limit > $this->mesReponses->getVirtualItemCount()) {
            $limit = $this->mesReponses->getVirtualItemCount() - $offset;
        }

        $sensTri = $this->getViewState('sensTri', []);
        $trierPar = $this->getViewState('trierPar', '');

        $this->mesReponses->DataSource = (new Atexo_Consultation_Responses())->retrieveInscritResponses(Atexo_CurrentUser::getIdInscrit(), $limit, $offset, $trierPar, $sensTri);
        $this->mesReponses->DataBind();
    }

    public function truncate($texte)
    {
        $truncatedText = null;
        $maximumNumberOfCaracterToReturn = 200;
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 200) ? '' : 'display:none';
    }

    public function getAcronymeAndNomOrganisme($acronyme)
    {
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronyme);
        if ($organisme instanceof CommonOrganisme) {
            return $organisme->getDenominationOrg().' / '.$organisme->getSigle();
        }
    }

    public function goToPageBottom($sender, $param)
    {
        $this->gotoPage($this->pageNumberBottom->Text);
        $this->pageNumberTop->Text = $this->pageNumberBottom->Text;
        $this->panelMesReponses->render($param->NewWriter);
    }

    public function goToPageTop($sender, $param)
    {
        $this->gotoPage($this->pageNumberTop->Text);
        $this->pageNumberBottom->Text = $this->pageNumberTop->Text;
        $this->panelMesReponses->render($param->NewWriter);
    }

    public function gotoPage($numPage)
    {
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage > $this->numeroPageTop->Text) {
                $numPage = $this->numeroPageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->mesReponses->CurrentPageIndex = $numPage - 1;
            $this->setViewState('numeroPage', $numPage);

            $this->populateData();
        } else {
            $this->numPageTop->Text = $this->mesReponses->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->mesReponses->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenghtTop($sender, $param)
    {
        $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
        $this->changePagerLenght($pageSize);
        $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
        $this->panelMesReponses->render($param->NewWriter);
    }

    public function changePagerLenghtBottom($sender, $param)
    {
        $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
        $this->changePagerLenght($pageSize);
        $this->nombreResultatAfficherTop->getSelectedValue($pageSize);
        $this->panelMesReponses->render($param->NewWriter);
    }

    public function changePagerLenght($pageSize)
    {
        $this->mesReponses->PageSize = $pageSize;
        $nombreReponses = $this->getViewState('nombreElement');
        $this->setViewState('numeroPage', 1);
        $this->numeroPageBottom->Text = ceil($nombreReponses / $this->mesReponses->PageSize);
        $this->numeroPageTop->Text = ceil($nombreReponses / $this->mesReponses->PageSize);
        $this->mesReponses->setCurrentPageIndex(0);
        $this->pageNumberBottom->Text = '1';
        $this->pageNumberTop->Text = '1';
        $this->populateData();
    }

    public function gotoFirst($sender, $param)
    {
        $numeroPage = 1;
        $this->gotoPage($numeroPage);
        $this->pageNumberTop->Text = $numeroPage;
        $this->pageNumberBottom->Text = $numeroPage;
        $this->panelMesReponses->render($param->NewWriter);
    }

    public function gotoPrevious($sender, $param)
    {
        $numeroPage = $this->pageNumberTop->Text - 1;
        $this->gotoPage($numeroPage);

        if ($numeroPage > $this->numeroPageTop->Text) {
            $numeroPage = $this->numeroPageTop->Text;
        } elseif ($numeroPage <= 0) {
            $numeroPage = 1;
        }

        $this->pageNumberTop->Text = $numeroPage;
        $this->pageNumberBottom->Text = $numeroPage;
        $this->panelMesReponses->render($param->NewWriter);
    }

    public function gotoNext($sender, $param)
    {
        $numeroPage = $this->pageNumberTop->Text + 1;
        $this->gotoPage($numeroPage);
        if ($numeroPage > $this->numeroPageTop->Text) {
            $numeroPage = $this->numeroPageTop->Text;
        } elseif ($numeroPage <= 0) {
            $numeroPage = 1;
        }

        $this->pageNumberTop->Text = $numeroPage;
        $this->pageNumberBottom->Text = $numeroPage;
        $this->panelMesReponses->render($param->NewWriter);
    }

    public function gotoLast($sender, $param)
    {
        $numeroPage = $this->getViewState('nombrePages');
        $this->gotoPage($numeroPage);
        $this->pageNumberTop->Text = $numeroPage;
        $this->pageNumberBottom->Text = $numeroPage;
        $this->panelMesReponses->render($param->NewWriter);
    }

    public function insertBrWhenTextTooLong($texte, $everyHowManyCaracter)
    {
        return wordwrap($texte, $everyHowManyCaracter, '<br>', true);
    }

    public function sortReponses($sender, $param)
    {
        $sortBy = $sender->ID;
        $arraySensTri = $this->getViewState('sensTri');
        if (!is_array($arraySensTri)) {
            $arraySensTri = [];
        }
        $arraySensTri[$sortBy] = ('ASC' == $arraySensTri[$sortBy]) ? 'DESC' : 'ASC';
        $this->setViewState('sensTri', $arraySensTri);
        $this->setViewState('trierPar', $sortBy);

        $this->pageNumberBottom->Text = '1';
        $this->pageNumberTop->Text = '1';
        $this->mesReponses->CurrentPageIndex = 0;
        $this->populateData();
        $this->panelMesReponses->render($param->NewWriter);
    }

    public function addStrongTag($nomChamps, $typeTag)
    {
        if ($nomChamps == $this->getViewState('trierPar')) {
            return ('open' == $typeTag) ? '<strong>' : '</strong>';
        }

        return '';
    }

    public function showAnnulerAction($datefin, $depotAnnule, $idOffre, $organisme)
    {
        return $datefin > date('Y-m-d H:i:s') && '0' == $depotAnnule && !(new Atexo_Consultation_Responses())->verifierSiUnPliOuvert($idOffre, $organisme);
    }

    // ToDo
    // move this function to Atexo_util

    public function getDateCalender($_date)
    {
        $date = [];
        $date['date'] = $_date;
        $date['num_month'] = date('n', strtotime($_date));
        $date['day'] = date('j', strtotime($_date));
        $date['month'] = Atexo_Util::getMonth()[$date['num_month']];
        $date['month_abbreviation'] = Atexo_Util::getMonthAbbreviation()[$date['num_month']];
        $date['year'] = date('Y', strtotime($_date));
        $date['heure'] = date('H', strtotime($_date));
        $date['minutes'] = date('i', strtotime($_date));
        $date['heure_minutes'] = $date['heure'].':'.$date['minutes'];

        return $date;
    }
}
