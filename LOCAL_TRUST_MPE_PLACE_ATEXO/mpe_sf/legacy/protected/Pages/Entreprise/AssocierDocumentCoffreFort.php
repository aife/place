<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationDocumentCfe;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Documents;
use Exception;

/**
 * Page d'ajout de documents au coffre-fort.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AssocierDocumentCoffreFort extends MpeTPage
{
    private ?\Application\Propel\Mpe\CommonConsultation $_consultation = null;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        if (0 == strcmp($_GET['id'], (int) $_GET['id'])) {
            $this->_consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        }
        if (!$this->IsPostBack) {
            $this->remplirTableauDocumentsCoffreFort(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getIdEntreprise());
        }

        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    public function remplirTableauDocumentsCoffreFort($consultationId, $idEntreprise)
    {
        $dataSource = (new Atexo_Entreprise_Documents())->retrieveDocumentJoinCfe($consultationId, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), $idEntreprise);
        $this->repeaterDocumentCoffreFort->dataSource = $dataSource;
        $this->repeaterDocumentCoffreFort->dataBind();
    }

    public function valider()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $connexionCom->beginTransaction();
        try {
            (new Atexo_Entreprise_Documents())->supprimerConsultationDocumentCfe(Atexo_CurrentUser::getIdEntreprise(), Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), $connexionCom);
            $atexoBlob = new Atexo_Blob();
            $atexoCrypto = new Atexo_Crypto();
            foreach ($this->repeaterDocumentCoffreFort->Items as $item) {
                $idDuplicata = $atexoBlob->duplicateBlob($item->id_blob->value, Atexo_Config::getParameter('COMMON_BLOB'));
                if ($item->case_a_cocher->checked) {
                    $consultationDocCfe = new CommonConsultationDocumentCfe();
                    $consultationDocCfe->setIdEntreprise($item->idEntreprise->value);
                    $consultationDocCfe->setConsultationId($item->consultation_id->value);
                    $consultationDocCfe->setOrganismeConsultation($item->organisme_consultation->value);
                    $consultationDocCfe->setIdJustificatif($item->id_justificatif->value);
                    $consultationDocCfe->setNomFichier($item->intitule_justificatif->value);
                    $consultationDocCfe->setTailleDocument($item->taille->value);
                    $consultationDocCfe->setIdBlob($idDuplicata);
                    $consultationDocCfe->setTypeDocument($item->nom_justificatif->value);
                    $consultationDocCfe->setDateFinValidite($item->date_fin_validite->value);

                    //Horodatage du fichier
                    $blobRessource = $atexoBlob->acquire_lock_on_blob($idDuplicata, Atexo_Config::getParameter('COMMON_BLOB'));
                    if ($blobRessource) {
                        $arrayTimeStampDocCfe = $atexoCrypto->timeStampFile($blobRessource['blob']['pointer']);
                        $consultationDocCfe->setHorodatage($arrayTimeStampDocCfe['horodatage']);
                        $consultationDocCfe->setUntrusteddate($arrayTimeStampDocCfe['untrustedDate']);
                    }
                    //Insertion dans consultationDocumentCfe
                    $consultationDocCfe->save($connexionCom);
                }
                $connexionCom->commit();
            }
        } catch (Exception) {
            $connexionCom->rollback();
            $this->response->redirect('index.php?page=Entreprise.EntrepriseDetailConsultation&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&Failed');
        }
        $this->response->redirect('index.php?page=Entreprise.EntrepriseDetailConsultation&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&Success');
    }

    public function retour()
    {
        $this->response->redirect('index.php?page=Entreprise.EntrepriseDetailConsultation&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
    }
}
