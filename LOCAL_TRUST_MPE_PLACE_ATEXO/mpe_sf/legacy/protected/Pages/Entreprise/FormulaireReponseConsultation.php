<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Attestation;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\Signature\Atexo_Signature_FichierVo;
use Application\Service\Atexo\Signature\Atexo_Signature_Gestion;
use Application\Service\Atexo\Signature\Atexo_Signature_InfosVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class FormulaireReponseConsultation extends MpeTPage
{
    public $_consultation;
    public $_uid;
    public ?string $_avecChiffrement = null;
    public ?string $_avecSignature = null;
    public ?bool $_acteEngagement = null;

    /**
     * Recupère la consultation.
     */
    public function getConsultation()
    {
        if (!$this->_consultation instanceof CommonConsultation) {
            $this->_consultation = $this->getViewState('consultation');
        }

        return $this->_consultation;
    }

    /**
     * Affecte la consultation.
     */
    public function setConsultation($value)
    {
        $this->_consultation = $value;
    }

    public function onPreInit($param)
    {
        if (false !== $this->CheckCryptoServerActivationState(Atexo_Util::atexoHtmlEntities($_GET['org']))) {
            $idCandidatureMps = null;
            $typeCandidature = 0; // 0 : aucune, 1: interne, 2 : externe
            if (isset($_GET['idCandidatureMps'])) {
                $typeCandidature = 1;
                $idCandidatureMps = Atexo_Util::atexoHtmlEntities($_GET['idCandidatureMps']);
            } elseif (isset($_GET['candidature_id'])) {
                $idCandidatureMps = Atexo_Util::atexoHtmlEntities($_GET['candidature_id']);
                $typeCandidature = 2;
            }
            $contexteComplementaire = [];
            $contexteComplementaire['id_candidature_mps'] = $idCandidatureMps;
            $contexteComplementaire['type_candidature_mps'] = $typeCandidature;
            $url = Atexo_MpeSf::getUrlDepotMpeSf(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), $contexteComplementaire);
            $this->response->redirect($url);
        } else {
            parent::onPreInit($param);
        }
    }

    /**
     * Initialisation de la page.
     */
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        //echo "DEBUT OnLoad : " . date('Y-m-d H:i:s') . "\n";
        $this->confirmationPart->setDisplay('None');
        $messageErreur = '';
        if (Atexo_CurrentUser::isConnected() && Atexo_CurrentUser::getIdInscrit()) {
            if ($_GET['id'] && $_GET['org']) {
                $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']));
                $this->setConsultation($consultation);
                $this->chargerBlocRecap($consultation);
                if (!$this->IsPostBack) {
                    if ($consultation instanceof CommonConsultation &&
                        (Atexo_Module::isEnabled('EntrepriseRepondreConsultationApresCloture') || Atexo_Util::cmpIsoDateTime(date('Y-m-d H:i:s'), $consultation->getDatefin()) <= 0)
                    ) {
                        if (Atexo_Module::isEnabled('MarchePublicSimplifie', $consultation->getOrganisme()) && $consultation->getMarchePublicSimplifie() && isset($_GET['idCandidatureMps'])) {
                            $candidatureMps = (new Atexo_MarchePublicSimplifie())->retrieveCandidatureMpsSansOffre(Atexo_Util::atexoHtmlEntities($_GET['idCandidatureMps']), $consultation->getId(), $consultation->getOrganisme(), Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise());
                            if (!$candidatureMps instanceof CommonTCandidatureMps) {
                                $this->panelMessageAvertissement->setVisible(true);
                                $this->panelMessageAvertissement->setMessage(Prado::Localize('CANDIDATURE_MPS_N_EXISTE_PAS'));
                            }
                        }
                        //Chargement du Bloc Recap Consultation
                        $this->alloti->value = $consultation->getAlloti();
                        $this->setViewState('consultation', $consultation);

                        $this->_acteEngagement = ('1' == $consultation->getSignatureActeEngagement());
                        $this->setViewState('refConsultation', $consultation->getId());
                        $this->setViewState('organisme', $consultation->getOrganisme());
                        $this->setViewState('acteEngagement', $consultation->getSignatureActeEngagement());
                        if (Atexo_Module::isEnabled('Groupement')) {
                            $this->blocGroupementEntreprise->setModeReadOnly(false);
                            $this->blocGroupementEntreprise->setIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
                            $this->blocGroupementEntreprise->setIdEtablissement(Atexo_CurrentUser::getIdEtablissement());
                            $this->blocGroupementEntreprise->initialiserComposant();
                        }

                        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritAndEntreprise(Atexo_CurrentUser::getIdInscrit());
                        //Effectue les actions qui dependent de  l'inscrit
                        $this->getActionInscrit($inscrit, $consultation);
                        if (1 == $consultation->getSignatureOffre()) {
                            $this->panelLegendSignature->visible = true;
                            $this->panelBoutonHautSigner->visible = true;
                            $this->panelBoutonBasSigner->visible = true;
                            $this->isSignatureRequis->value = 1;
                        }
                        $this->gererAffichageBlocVariantesConsultation($consultation);
                        $this->afficherBlockErreur($messageErreur);
                    } else {
                        $this->afficherBlockErreur(Prado::localize('TEXT_AUCUNE_CONSULTATION'));
                    }
                }
            } else {
                $this->afficherBlockErreur(Prado::localize('NON_AUTORISER_DEPOT_REPONSE'));
            }
        } else {
            $this->afficherBlockErreur(Prado::localize('NON_AUTORISER_DEPOT_REPONSE'));
        }
        if (!$this->IsPostBack && !$this->IsCallback) {
            $this->saveTraceDiagInscrit('DESCRIPTION12');
        }
    }

    /**
     * Permet de charger la liste des lots.
     */
    public function chargerListeLots($consultation)
    {
        $dataSource = [];
        $lots = $consultation->getAllLots();
        if (is_array($lots) && count($lots) >= 1) {
            $dataSource = $lots;
        } else {
            $dataSource = [$this->getConsultation()];
        }
        $this->listeOffres->DataSource = $dataSource;
        $this->listeOffres->DataBind();
    }

    /**
     * Permet de recuperer le numéro du lot.
     */
    public function getNumeroLot($lot)
    {
        $numlot = '0';
        if (($this->getConsultation() instanceof CommonConsultation) && ($this->getConsultation()->getAlloti()) && ($lot instanceof CommonCategorieLot)) {
            $numlot = $lot->getLot();
        }

        return $numlot;
    }

    /**
     * Action bouton annuler.
     */
    public function redirectToDetail()
    {
        $this->saveTraceDiagInscrit('DESCRIPTION19');
        $this->response->redirect('index.php?page=Entreprise.EntrepriseDetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).
            '&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['org']));
    }

    /**
     * Permet de gerer la visibilité du bloc des offres techniques.
     */
    public function showPanelOffresTechnique($consultation)
    {
        if ('0' != $consultation->getEnvOffreTechnique()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de gerer la visibilité du bloc des offres.
     */
    public function showPanelOffres($consultation)
    {
        if ('0' != $consultation->getEnvOffre()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de gerer la visibilité du bloc des offres de type anonymat.
     */
    public function showPanelOffresAnonymat($consultation)
    {
        if ('0' != $consultation->getEnvAnonymat()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de remplir les tableaux des certificats de chiffrement pour les différentes enveloppes.
     */
    public function afficherInfosSignatureChiffrement($consultation)
    {
        if (1 == $consultation->getSignatureOffre()) {
            $this->signatureElectroniqueRequise->Text = Prado::Localize('OUI_SINATURE_SUR_MON_POSTE');
            $this->_avecSignature = '1';
        } else {
            $this->signatureElectroniqueRequise->Text = Prado::Localize('DEFINE_NON');
            $this->_avecSignature = '0';
        }

        if (1 == $consultation->getChiffrementOffre()) {
            $this->chiffremetPlis->Text = Prado::Localize('OUI_SUR_MON_POSTE');
            $this->_avecChiffrement = '1';
        } else {
            $this->chiffremetPlis->Text = Prado::Localize('DEFINE_NON');
            $this->_avecChiffrement = '0';
            $this->panelChiffrement->visible = false;
        }
    }

    /**
     * Permet de charger l'enveloppe de candidature.
     */
    public function setElementsEnveloppeCandidature()
    {
        $dataSource = [];
        $dataSource[] = ['typeEnv' => '1', 'lot' => '0'];
        $this->candidature->DataSource = $dataSource;
        $this->candidature->DataBind();
    }

    public function SignerPieces($sender, $param)
    {
        $this->functionSignature->Text = '';
        $listPiecesCochees = $this->recupererToutesPiecesCochees();
        if (!$listPiecesCochees) {
            $scriptJsAlertFichierNonSelectionne = '<script>';
            $scriptJsAlertFichierNonSelectionne .= "bootbox.alert('".Atexo_Util::toUtf8(Prado::localize('MESSAGE_AUCUN_FICHIER_SELECTIONNE_SIGNATURE_REPONSE'))."');";
            $scriptJsAlertFichierNonSelectionne .= 'return;';
            $scriptJsAlertFichierNonSelectionne .= '</script>';
            $this->functionSignature->Text = $scriptJsAlertFichierNonSelectionne;
        }
        if (is_array($listPiecesCochees) && count($listPiecesCochees)) {
            $functionSignerFichiersSelectionnes = '<script>';
            $functionSignerFichiersSelectionnes .= "document.MpeChiffrementApplet.setMethodeJavascriptRenvoiResultat('renvoiResultatSignature');";
            $functionSignerFichiersSelectionnes .= "document.MpeChiffrementApplet.setMethodeJavascriptRecuperationLog('logAppletSignature');";
            $functionSignerFichiersSelectionnes .= "document.MpeChiffrementApplet.initialiser(0, null, null, 1, 0, 0, '".Atexo_Config::getParameter('URL_MODULE_VALIDATION_SIGNATURE')."',1);";
            foreach ($listPiecesCochees as $fichier) {
                if ($fichier instanceof Atexo_Signature_FichierVo) {
                    if ($fichier->getTypeFichier() == Atexo_Config::getParameter('TYPE_DOCUMENT_HASH')) {
                        $functionSignerFichiersSelectionnes .= 'document.MpeChiffrementApplet.ajouterHashFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.
                            $fichier->getIdentifiant().", '".$fichier->getIdentifiant()."', '".strtoupper($fichier->getHashFichier())."', '".addslashes(utf8_encode($fichier->getNomFichier())).
                            "', 'SEC', '".addslashes($fichier->getOrgineFichier())."', null);";
                    } elseif ($fichier->getTypeFichier() == Atexo_Config::getParameter('TYPE_DOCUMENT_FICHIER')) {
                        if ($fichier->getOrgineFichier() == Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_TYPEE')) {
                            if ($fichier->getFichierAeAjoute()) {
                                if (Atexo_Module::isEnabled('GenererActeDengagement')) {
                                    if ($fichier->getCheminFichier()) {
                                        $functionSignerFichiersSelectionnes .= 'document.MpeChiffrementApplet.ajouterFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.
                                            $fichier->getIdentifiant().", '".$fichier->getIdentifiant()."', '".addslashes($fichier->getCheminFichier())."', 'SEC', '".
                                            addslashes($fichier->getOrgineFichier())."', null, 1);";
                                    }
                                    if ($fichier->getCheminFichierXmlAE()) {
                                        $functionSignerFichiersSelectionnes .= 'document.MpeChiffrementApplet.ajouterFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.
                                            $fichier->getIdentifiant().", '".$fichier->getIdentifiant().'_'.$fichier->getIdentifiant()."', '".addslashes($fichier->getCheminFichierXmlAE())."', 'SEC', '".
                                            addslashes($fichier->getOrgineFichier())."', null, 1);";
                                    }
                                } else {
                                    $functionSignerFichiersSelectionnes .= 'document.MpeChiffrementApplet.ajouterFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.
                                        $fichier->getIdentifiant().", '".$fichier->getIdentifiant()."', '".addslashes($fichier->getCheminFichier())."', 'SEC', '".
                                        addslashes($fichier->getOrgineFichier())."', null, 1);";
                                }
                            }
                        } elseif ($fichier->getOrgineFichier() == Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_LIBRE')) {
                            $functionSignerFichiersSelectionnes .= 'document.MpeChiffrementApplet.ajouterFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.
                                $fichier->getIdentifiant().", '".$fichier->getIdentifiant()."', '".addslashes($fichier->getCheminFichier())."', 'SEC', '".
                                addslashes($fichier->getOrgineFichier())."', null, 1);";
                        }
                    }
                }
            }
            $functionSignerFichiersSelectionnes .= 'document.MpeChiffrementApplet.ajouterRestrictionTypeCertificatSignatureElectronique();';
            $functionSignerFichiersSelectionnes .= 'document.MpeChiffrementApplet.executer();';
            $functionSignerFichiersSelectionnes .= '</script>';
            //Nous allons donc le decoder afin que l'applet puisse l'interpreter
            if ('utf-8' != strtolower(Atexo_Config::getParameter('HTTP_ENCODING'))) {
                $functionSignerFichiersSelectionnes = utf8_decode($functionSignerFichiersSelectionnes);
            }
            $this->functionSignature->Text = $functionSignerFichiersSelectionnes;
            if ($this->logAppletSignature->Value) {
                $this->saveTraceDiagInscrit('DESCRIPTION21', $this->logAppletSignature->Value);
            }
        }
    }

    public function recupererJsonSignature($sender, $param)
    {
        $json = $this->retourJsonSignature->Value;
        //La fonction json_decode doit recevoir un json encodé en utf8
        $json = Atexo_Util::httpEncodingToUtf8($json);
        $dataSourceNew = (new Atexo_Signature_Gestion())->transformeJson($json);
        $listeSignedFiles = self::getListeSignedFiles($dataSourceNew);
        $this->saveTraceDiagInscrit('DESCRIPTION17', false, '', '', $listeSignedFiles);
        $dataSource = (new Atexo_Signature_Gestion())->merge($this->Page->getViewState('dataSourcePieces'), $dataSourceNew);
        $this->Page->setViewState('dataSourcePieces', $dataSource);
        $this->raffraichirListeFormulaires($sender, $param);
    }

    /**
     * Permet de recuperer la liste des fichiers signes.
     *
     * @param array $dataSource
     *
     * @return string $listeSignedFiles
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getListeSignedFiles($dataSource)
    {
        $arraySignedFiles = [];
        $listeSignedFiles = '';
        foreach ($dataSource as $listFiles) {
            if (is_array($listFiles) && count($listFiles)) {
                foreach ($listFiles as $typePieces) {
                    if (is_array($typePieces) && count($typePieces)) {
                        foreach ($typePieces as $fichierVoObject) {
                            $cheminFicher = $fichierVoObject->getCheminFichier();
                            $arrayCheminFichier = explode('\\', $cheminFicher);
                            if (is_array($arrayCheminFichier) && count($arrayCheminFichier)) {
                                $arraySignedFiles[] = $arrayCheminFichier[count($arrayCheminFichier) - 1];
                            }
                        }
                    }
                }
            }
        }
        if (count($arraySignedFiles)) {
            $listeSignedFiles = '<br/>- '.Atexo_Util::utf8ToIso(implode('<br/> - ', $arraySignedFiles));
        }

        return $listeSignedFiles;
    }

    public function getLastIdFichier()
    {
        $max = (int) $this->Page->getViewState('lastIdFichier');
        $this->Page->setViewState('lastIdFichier', $max + 1);

        return $this->Page->getViewState('lastIdFichier');
    }

    public function envoyerReponse($sender, $param)
    {
        $fichiersSansSignature = null;
        $fichiersAvecSignatureIncertaine = null;
        $fichiersAvecSignatureInvalide = null;
        $fichiersAvecSignatureValide = null;
        $this->functionEnvoiReponse->Text = '';
        $this->functionSignature->Text = '';
        $this->javascriptErreur->Text = '';
        $dataSources = $this->Page->getViewState('dataSourcePieces');
        $urlDepotReponse = Atexo_Entreprise_Reponses::getUrlDepotReponse(Atexo_Util::atexoHtmlEntities($_GET['org']));
        $uid = $this->getViewState('uid');
        $consultation = $this->getConsultation();
        $this->setConsultation($consultation);
        $chiffrementRequis = $consultation->getChiffrementOffre();
        $signatureRequise = (int) $consultation->getSignatureOffre();
        $fichierAjouteReponse = false;
        $functionEnvoiReponse = '';
        if (is_array($dataSources) && count($dataSources)) {
            $functionEnvoiReponse = "document.MpeChiffrementApplet.setMethodeJavascriptRenvoiResultat('renvoiResultatEnvoiReponse');";
            $functionEnvoiReponse .= "document.MpeChiffrementApplet.setMethodeJavascriptRecuperationLog('logAppletDepotReponse');";
            $functionEnvoiReponse .= 'document.MpeChiffrementApplet.initialiser('.$chiffrementRequis.", '".$urlDepotReponse."', '".$uid."', ".$signatureRequise.', 0, '.$signatureRequise.", '".
                Atexo_Config::getParameter('URL_MODULE_VALIDATION_SIGNATURE')."',1);";
            $numOrdreFichier = 1;
            $relationNumOrdreFichier = [];
            $messageConfirmSignature = '';
            $msgSignatureValide = [];
            $msgFichiersSansSignature = [];
            $msgSignatureIncertaine = [];
            $msgSignatureInvalide = [];
            $messageFichiersVides = [];

            if ($chiffrementRequis) {
                $this->ConstruireAppelAppletChiffrement($consultation, $functionEnvoiReponse);
            }

            foreach ($dataSources as $indexEnveloppe => $enveloppe) {
                //Editions SUB
                if (Atexo_Module::isEnabled('InterfaceModuleSub') && is_array($enveloppe['pieceEdition']) && count($enveloppe['pieceEdition']) > 0) {
                    $arrayFichier = $enveloppe['pieceEdition'];
                    $fichierAjouteReponse = true;
                    foreach ($arrayFichier as $identifiant => $fichier) {
                        $functionEnvoiReponse .= 'document.MpeChiffrementApplet.ajouterHashFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.$numOrdreFichier.
                            ", '".$fichier->getIdentifiant()."', '".strtoupper($fichier->getHashFichier())."', '".addslashes(utf8_encode($fichier->getNomFichier()))."', 'PRI', '".
                            addslashes($fichier->getOrgineFichier())."', '".$fichier->getContenuSignatureXML()."');";
                        $relationNumOrdreFichier[$numOrdreFichier] = $fichier;
                        ++$numOrdreFichier;
                        $this->getMessageConfirmSignature($fichier, $msgFichiersSansSignature, $msgSignatureIncertaine, $msgSignatureInvalide, $msgSignatureValide);
                    }
                } //Pièces libres
                if (!Atexo_Module::isEnabled('InterfaceModuleSub') && is_array($enveloppe['pieceLibre']) && count($enveloppe['pieceLibre']) > 0) {
                    $arrayFichier = $enveloppe['pieceLibre'];
                    $fichierAjouteReponse = true;
                    foreach ($arrayFichier as $identifiant => $fichier) {
                        if ($fichier->getCheminSignatureXML()) {
                            $generationXadesNecessaire = '1';
                        } else {
                            $generationXadesNecessaire = '0';
                        }//ajouterFichier(typeEnveloppe, numeroLot, indexFichier, identifiantFichier, cheminFichierSelectionne, typeFichier, cheminFichierSignatureXML);
                        $functionEnvoiReponse .= 'document.MpeChiffrementApplet.ajouterFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.$numOrdreFichier.", '".
                            $fichier->getIdentifiant()."', '".addslashes($fichier->getCheminFichier())."', 'PRI', '".addslashes($fichier->getOrgineFichier())."', '".
                            addslashes($fichier->getCheminSignatureXML())."', ".$generationXadesNecessaire.');';
                        $relationNumOrdreFichier[$numOrdreFichier] = $fichier;
                        ++$numOrdreFichier;
                        $this->getMessageConfirmSignature($fichier, $msgFichiersSansSignature, $msgSignatureIncertaine, $msgSignatureInvalide, $msgSignatureValide);
                        $this->ajoutMessageFichierVide($fichier, $messageFichiersVides);
                    }
                } //Pièces typées
                if (!Atexo_Module::isEnabled('InterfaceModuleSub') && is_array($enveloppe['pieceTypees']) && count($enveloppe['pieceTypees']) > 0) {
                    $arrayFichier = $enveloppe['pieceTypees'];
                    foreach ($arrayFichier as $identifiant => $fichier) {
                        if ($fichier->getFichierAeAjoute()) {
                            $fichierAjouteReponse = true;
                            if ($fichier->getCheminSignatureXML()) {
                                $generationXadesNecessaire = '1';
                            } else {
                                $generationXadesNecessaire = '0';
                            }//ajouterFichier(typeEnveloppe, numeroLot, indexFichier, identifiantFichier, cheminFichierSelectionne, typeFichier, cheminFichierSignatureXML);
                            if (Atexo_Module::isEnabled('GenererActeDengagement')) {
                                if (false === $this->verifierActeEngagement($fichier, $consultation)) {
                                    return;
                                }
                                if ($fichier->getCheminFichierXmlAE()) {
                                    $functionEnvoiReponse .= 'document.MpeChiffrementApplet.ajouterFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.$numOrdreFichier.
                                        ", '".$fichier->getIdentifiant()."', '".addslashes($fichier->getCheminFichierXmlAE())."', 'ACE', '".addslashes($fichier->getOrgineFichier()).
                                        "', '".addslashes($fichier->getCheminFichierSignatureXmlAE())."', ".$generationXadesNecessaire.');';
                                    ++$numOrdreFichier;
                                }
                                if ($fichier->getCheminFichier()) {
                                    $functionEnvoiReponse .= 'document.MpeChiffrementApplet.ajouterFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.$numOrdreFichier.
                                        ", '".$fichier->getIdentifiant()."', '".addslashes($fichier->getCheminFichier())."', 'ACE', '".addslashes($fichier->getOrgineFichier()).
                                        "', '".addslashes($fichier->getCheminSignatureXML())."', ".$generationXadesNecessaire.');';
                                }
                            } else {
                                $functionEnvoiReponse .= 'document.MpeChiffrementApplet.ajouterFichier('.$fichier->getTypeEnveloppe().', '.$fichier->getNumeroLot().', '.$numOrdreFichier.
                                    ", '".$fichier->getIdentifiant()."', '".addslashes($fichier->getCheminFichier())."', 'ACE', '".addslashes($fichier->getOrgineFichier()).
                                    "', '".addslashes($fichier->getCheminSignatureXML())."', ".$generationXadesNecessaire.');';
                            }
                            $relationNumOrdreFichier[$numOrdreFichier] = $fichier;
                            ++$numOrdreFichier;
                            $this->getMessageConfirmSignature($fichier, $msgFichiersSansSignature, $msgSignatureIncertaine, $msgSignatureInvalide, $msgSignatureValide);
                            $this->ajoutMessageFichierVide($fichier, $messageFichiersVides);
                        } elseif ($this->getConditionGenerationAE($consultation) && $fichier instanceof Atexo_Signature_FichierVo && $fichier->getOrgineFichier() == Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_TYPEE')) {
                            $this->javascriptErreur->Text = "<script>bootbox.alert('".Prado::Localize('DEFINE_MESSAGE_ERREUR_ACTE_ENGAGEMENT_NON_GENERE_OU_SINE')."');</script>";

                            return;
                        }
                    }
                }
            }
            $functionEnvoiReponse .= 'document.MpeChiffrementApplet.executer();';
            $i = 0;
            if ($consultation->getSignatureOffre()) {
                if ($msgFichiersSansSignature) {//sans signature
                    $messageConfirmSignature[$i]['message'] = Prado::localize('DEFINE_MESSAGE_PROBLEME_AUCUNE_SIGNATURE_DEPOT_REPONSE');
                    $messageConfirmSignature[$i++]['nameFiles'] = $msgFichiersSansSignature;
                }
                if ($msgSignatureIncertaine) {//Signature incertaine
                    $messageConfirmSignature[$i]['message'] = Prado::localize('DEFINE_MESSAGE_PROBLEME_SIGNATURE_INCERTAINE_DEPOT_REPONSE');
                    $messageConfirmSignature[$i++]['nameFiles'] = $msgSignatureIncertaine;
                }
                if ($msgSignatureInvalide) {//Signature invalide
                    $messageConfirmSignature[$i]['message'] = Prado::localize('DEFINE_MESSAGE_PROBLEME_SIGNATURE_INVALIDE_DEPOT_REPONSE');
                    $messageConfirmSignature[$i++]['nameFiles'] = $msgSignatureInvalide;
                }
            }
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION18', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            $nbrSansSignature = is_countable($msgFichiersSansSignature) ? count($msgFichiersSansSignature) : 0;
            if ($nbrSansSignature > 1) {
                $msgSansSignature = Prado::localize('PLRS_PICES_SANS_SIGNATURES');
            } else {
                $msgSansSignature = Prado::localize('PICE_SANS_SIGNATURE');
            }
            if ($nbrSansSignature) {
                $fichiersSansSignature = '<br/>- '.Atexo_Util::utf8ToIso(implode('<br/> - ', $msgFichiersSansSignature));
            }
            $description .= "<br/><strong>$nbrSansSignature $msgSansSignature</strong>".$fichiersSansSignature;
            $nbrIncertaine = is_countable($msgSignatureIncertaine) ? count($msgSignatureIncertaine) : 0;
            if ($nbrIncertaine > 1) {
                $msgAvecSignatureIncertaine = Prado::localize('PLRS_PICES_AVEC_SIGNATURES_INCERTAINES');
            } else {
                $msgAvecSignatureIncertaine = Prado::localize('PICE_AVEC_SIGNATURE_INCERTAINE');
            }
            if ($nbrIncertaine) {
                $fichiersAvecSignatureIncertaine = '<br/>- '.Atexo_Util::utf8ToIso(implode('<br/> - ', $msgSignatureIncertaine));
            }
            $description .= "<br/><strong>$nbrIncertaine $msgAvecSignatureIncertaine</strong>".$fichiersAvecSignatureIncertaine;
            $nbrSignatureInvalide = is_countable($msgSignatureInvalide) ? count($msgSignatureInvalide) : 0;

            if ($nbrSignatureInvalide > 1) {
                $msgAvecSignatureInvalide = Prado::localize('PLRS_PICES_AVEC_SIGNATURES_INVALIDES');
            } else {
                $msgAvecSignatureInvalide = Prado::localize('PICE_AVEC_SIGNATURE_INVALIDE');
            }
            if ($nbrSignatureInvalide) {
                $fichiersAvecSignatureInvalide = '<br/>- '.Atexo_Util::utf8ToIso(implode('<br/> - ', $msgSignatureInvalide));
            }
            $description .= "<br/><strong>$nbrSignatureInvalide $msgAvecSignatureInvalide</strong>".$fichiersAvecSignatureInvalide;
            $nbrSignatureValide = is_countable($msgSignatureValide) ? count($msgSignatureValide) : 0;
            if ($nbrSignatureValide > 1) {
                $msgAvecSignatureValide = Prado::localize('PLRS_PICES_AVEC_SIGNATURES_VALIDES');
            } else {
                $msgAvecSignatureValide = Prado::localize('PICE_AVEC_SIGNATURE_VALIDE');
            }
            if ($nbrSignatureValide) {
                $fichiersAvecSignatureValide = '<br/>- '.Atexo_Util::utf8ToIso(implode('<br/> - ', $msgSignatureValide)).'<br/>';
            }
            $description .= "<br/><strong>$nbrSignatureValide $msgAvecSignatureValide</strong>".$fichiersAvecSignatureValide;
            $this->saveTraceDiagInscrit('DESCRIPTION18', false, $description, $IdDesciption);

            if ($fichierAjouteReponse) {
                if ($messageConfirmSignature) {
                    $this->repeaterMessagePbSignature->DataSource = $messageConfirmSignature;
                    $this->repeaterMessagePbSignature->DataBind();
                    $this->repeaterMessagePbFichier->DataSource = $messageFichiersVides;
                    $this->repeaterMessagePbFichier->DataBind();

                    $this->labelEnvoiReponse->Text = base64_encode($functionEnvoiReponse);
                    $this->panelMessagePbSignature->render($param->getNewWriter());
                    $this->saveTraceDiagInscrit('DESCRIPTION25');
                    $scriptEnvoiReponse = "<script>openModal('modal-confirmation-depot','popup-800',document.getElementById('ctl0_CONTENU_PAGE_titrePopin'));</script>";
                } else {
                    $scriptEnvoiReponse = '<script>'.$functionEnvoiReponse.'</script>';
                }
            } else {
                $scriptEnvoiReponse = "<script> bootbox.alert('".Atexo_Util::toUtf8(Prado::localize('DEFINE_MESSAGE_DEVEZ_RENSIEGNER_AU_MOINS_UN_DOCUMENT'))."'); </script>";
            }
            //Lors de l'ajout des fichiers au formulaire, nous avons encodé le json en utf8 dans le cas des plateformes ISO.Nous allons donc le decoder afin que l'applet puisse l'interpreter
            if ('utf-8' != strtolower(Atexo_Config::getParameter('HTTP_ENCODING'))) {
                $scriptEnvoiReponse = utf8_decode($scriptEnvoiReponse);
            }
            $this->functionEnvoiReponse->Text = $scriptEnvoiReponse;

            $this->setViewState('relationNumOrdreFichier', $relationNumOrdreFichier);
            if ($this->logAppletdepotReponse->value) {
                $this->saveTraceDiagInscrit('DESCRIPTION24', $this->logAppletdepotReponse->value);
            }
        }
    }

    public function reponseReceived($sender, $param)
    {
        $idOffre = null;
        $this->functionEnvoiReponse->Text = '';
        $this->labelEnvoiReponse->Text = '';
        $this->functionSignature->Text = '';
        $json = $this->retourJsonEnvoiReponse->Value;
        $this->retourJsonEnvoiReponse->Value = '';
        //La fonction json_decode doit recevoir un json encodé en utf8
        $json = Atexo_Util::httpEncodingToUtf8($json);
        $res = json_decode(Atexo_Util::toUtf8($json), true, 512, JSON_THROW_ON_ERROR);
        if (isset($res['contenuReponseAnnonceXML'])) {
            $xmlReponseAnnonce = $res['contenuReponseAnnonceXML'];
            //Ajout de la consultation au panier de l'entreprise
            if (Atexo_Module::isEnabled('PanierEntreprise')) {
                if (!Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit())) {
                    Atexo_Entreprise_PaniersEntreprises::AddConsultationToPanierEntreprise(
                        Atexo_Util::atexoHtmlEntities($_GET['org']),
                        Atexo_Util::atexoHtmlEntities($_GET['id']),
                        Atexo_CurrentUser::getIdEntreprise(),
                        Atexo_CurrentUser::getIdInscrit()
                    );
                }
            }
            $dateDebAction = date('Y-m-d H:i:s');
            try {
                $objetConsultation = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), false);
                if ($objetConsultation instanceof CommonConsultation) {
                    $offreVariante = null;
                    $varianteCalcule = $objetConsultation->getVarianteCalcule();
                    if (!$varianteCalcule) {
                        $varianteCalcule = '0';
                    }
                    if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && 0 == strcmp($varianteCalcule, '1')) {
                        if ($this->offreVariante->checked) {
                            $offreVariante = '1';
                        } else {
                            $offreVariante = '0';
                        }
                    }
                    $idCandidatureMPS = self::getIdCandidatureMPS($objetConsultation);
                    $arrayReturned = (new Atexo_Entreprise_Reponses())->reponseReceieved($xmlReponseAnnonce, $this->getViewState('uid'), Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), $this->adresseAR->Text, Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit(), $offreVariante, true, $idCandidatureMPS, Atexo_CurrentUser::getIdEtablissement());
                    //affichage des messages de confirmation de reception
                    //----------Integration nouvelle maquette de réponse---------
                    $detailReponse = (new Atexo_Entreprise_Reponses())->getNomsFichiersFromArray($arrayReturned, $this->getViewState('acteEngagement'), '<br />', true, $_GET['org']);
                    $detail = 'Response files :';
                    foreach ($detailReponse as $one) {
                        $detail .= $one['nomFichier'].'/ ';
                    }
                    $details = 'ref utilistauer consultation :'.$objetConsultation->getReferenceUtilisateur().': '.$detail;
                    Atexo_InscritOperationsTracker::trackingOperations(
                        Atexo_CurrentUser::getIdInscrit(),
                        Atexo_CurrentUser::getIdEntreprise(),
                        $_SERVER['REMOTE_ADDR'],
                        date('Y-m-d'),
                        $dateDebAction,
                        substr($_SERVER['QUERY_STRING'], 0),
                        $details,
                        date('Y-m-d H:i:s')
                    );

                    //----------------Debut message de confirmation ---------------
                    $idEtp = Atexo_CurrentUser::getIdEntreprise();
                    $idInscrit = Atexo_CurrentUser::getIdInscrit();
                    $listeFichiers = (new Atexo_Entreprise_Reponses())->getListeFichierDepot(Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_Util::atexoHtmlEntities($_GET['id']), $idEtp, $idInscrit);
                    $reponsesInscrit = (new Atexo_Consultation_Responses())->retrieveReponsesEntreprise(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), $idInscrit);
                    $offre = null;
                    if (is_array($reponsesInscrit) && count($reponsesInscrit)) {
                        $offre = $reponsesInscrit[count($reponsesInscrit) - 1];
                        $idOffre = $offre->getId();
                        $listeFichiersDernierOffre = $listeFichiers[$idOffre];
                    }
                    if (Atexo_Module::isEnabled('Groupement')) {
                        $this->blocGroupementEntreprise->saveGroupementEntreprise($idOffre);
                    }
                    $this->idRepeaterDepotsOffres->DataSource = [$idOffre => $listeFichiersDernierOffre];
                    $this->idRepeaterDepotsOffres->dataBind();
                    $this->blocMessageConfirmation->visible = true;
                    $this->labelHorodatage->Text = Atexo_Util::iso2frnDateTime($arrayReturned['dateDepot'], false, true);
                    //----------------Fin message de confirmation ---------------

                    //----------------Fin d'intégration-------------------------
                    //Envoi de mail
                    $idBlobPjRecapMail = $this->sendMail($objetConsultation, $arrayReturned);
                    $lienDownload = 'index.php?page=Agent.DownloadRecapReponse&idFile='.$idBlobPjRecapMail.'&orgAcronyme='.$objetConsultation->getOrganisme();
                    $this->saveTraceDiagInscrit('DESCRIPTION20', false, '', '', null, $lienDownload);
                    if ($offre instanceof CommonOffres) {
                        $offre->setIdPdfEchangeAccuse($idBlobPjRecapMail);
                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                        $offre->save($connexion);
                    }
                    //Début fermettre de l'alerte d'attente de l'applet
                    $javascript = '<script >';
                    $javascript .= 'el = document.getElementById("pageLoader");if(el) {document.body.removeChild(el);}';
                    $javascript .= '</script>';
                    //Fin fermettre de l'alerte d'attente de l'applet
                    $this->jsAlerteAttenteApplet->Text = $javascript;
                    $this->confirmationPart->setDisplay('Dynamic');
                    $this->main_part->setDisplay('None');
                    $this->errorPart->setDisplay('None');
                    $this->panelMessageErreur->setDisplay('None');
                //$this->confirmationPart->render($param->getNewWriter());
                } else {
                    echo " problème d'accès à la consultation ";
                    exit;
                }
                //TODO : A tester apres correction probleme aplet

                (new Atexo_Entreprise_Attestation())->synchroniserTypesDocumentsEntreprise(Atexo_CurrentUser::getIdEntreprise(), true, $objetConsultation->getReferenceUtilisateur());
                (new Atexo_Entreprise_Attestation())->synchroniserTypesDocumentsEntreprise(Atexo_CurrentUser::getIdEtablissement(), false, $objetConsultation->getReferenceUtilisateur());
            } catch (\Exception $e) {
                Prado::log('Erreur lors du depot de la reponse entreprise : Ref: '.Atexo_Util::atexoHtmlEntities($_GET['id']).'- Org: '.Atexo_Util::atexoHtmlEntities($_GET['org']).' - Id enteprise: '.Atexo_CurrentUser::getIdEntreprise().
                    ' - Id Inscrit: '.Atexo_CurrentUser::getIdInscrit()."\n".$e, TLogger::ERROR, 'Atexo.pages.entreprise.FormulaireReponse.php');
                $this->afficherBlockErreur(Prado::localize('ERREUR_DEPOT'));
            }
        }
    }

    public function getMessageConfirmSignature($fichier, &$msgFichiersSansSignature, &$msgSignatureIncertaine, &$msgSignatureInvalide, &$msgSignatureValide = null)
    {
        $presenceSignature = null;
        $statutResultatSignature = null;
        $nomFichier = (new Atexo_Util())->toHttpEncoding($fichier->getNomFichier());
        $infoSignature = $fichier->getInfosSignature();
        if ((!$fichier->getContenuSignatureXML() && !$fichier->getCheminSignatureXML()) || !($infoSignature instanceof Atexo_Signature_InfosVo)) {
            $presenceSignature = false;
        } else {
            $presenceSignature = true;
            $statutResultatSignature = $fichier->getResultatValiditeSignatureWithMessage();
        }
        if (true == $presenceSignature) {
            if ($statutResultatSignature['statut'] == Atexo_Config::getParameter('NOK')) {
                $msgSignatureInvalide[] = $nomFichier;
            }
            if ($statutResultatSignature['statut'] == Atexo_Config::getParameter('UNKNOWN')) {
                $msgSignatureIncertaine[] = $nomFichier;
            }
            if ($statutResultatSignature['statut'] == Atexo_Config::getParameter('OK')) {
                $msgSignatureValide[] = $nomFichier;
            }
        } else {
            $msgFichiersSansSignature[] = $nomFichier;
        }
    }

    /**
     * Permet de raffraichir les formulaires de candidature et offres.
     */
    public function raffraichirListeFormulaires($sender, $param)
    {
        //Chargement des composants
        $this->chargerComposants($sender, $param);

        //Raffraichissement candidature + offres
        $this->Page->panelCandidature->render($param->getNewWriter());
        $this->Page->panelLots->render($param->getNewWriter());
    }

    /**
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function chargerComposants($sender, $param)
    {
        //Chargement candidature
        if ('0' != $this->getConsultation()->getEnvCandidature()) {
            foreach ($this->candidature->getItems() as $itemCandidature) {
                if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                    $itemCandidature->atexoReponseEnveloppeCandidature->atexoReponseFormulaireSub->remplirEditionsFormulaire();
                } else {
                    $itemCandidature->atexoReponseEnveloppeCandidature->atexoReponsePiecesLibres->chargerPieceLibre();
                    if ($this->isConsultationHasPiecesTypees()) {
                        $itemCandidature->atexoReponseEnveloppeCandidature->atexoReponsePiecesTypees->refresh($sender, $param);
                    }
                }
            }
        }
        //Chargement offres
        foreach ($this->listeOffres->getItems() as $itemOffre) {
            //Offres
            if ('0' != $this->getConsultation()->getEnvOffre()) {
                if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                    $itemOffre->atexoReponseEnveloppeOffre->atexoReponseFormulaireSub->remplirEditionsFormulaire();
                } else {
                    $itemOffre->atexoReponseEnveloppeOffre->atexoReponsePiecesLibres->chargerPieceLibre();
                    if ($this->isConsultationHasPiecesTypees()) {
                        $itemOffre->atexoReponseEnveloppeOffre->atexoReponsePiecesTypees->refresh($sender, $param);
                    }
                }
            }
            //Offres techniques
            if ('0' != $this->getConsultation()->getEnvOffreTechnique()) {
                if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                    $itemOffre->atexoReponseEnveloppeOffreTech->atexoReponseFormulaireSub->remplirEditionsFormulaire();
                } else {
                    $itemOffre->atexoReponseEnveloppeOffreTech->atexoReponsePiecesLibres->chargerPieceLibre();
                    if ($this->isConsultationHasPiecesTypees()) {
                        $itemOffre->atexoReponseEnveloppeOffreTech->atexoReponsePiecesTypees->refresh($sender, $param);
                    }
                }
            }
            //Anonymat
            if ('0' != $this->getConsultation()->getEnvAnonymat()) {
                if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                    $itemOffre->atexoReponseEnveloppeOffreAnonymat->atexoReponseFormulaireSub->remplirEditionsFormulaire();
                } else {
                    $itemOffre->atexoReponseEnveloppeOffreAnonymat->atexoReponsePiecesLibres->chargerPieceLibre();
                    if ($this->isConsultationHasPiecesTypees()) {
                        $itemOffre->atexoReponseEnveloppeOffreAnonymat->atexoReponsePiecesTypees->refresh($sender, $param);
                    }
                }
            }
        }
    }

    /**
     * Permet de recuperer toutes les pièces cochées.
     *
     * @sender
     *
     * @param
     * @return: le tableau des pièces cochées
     */
    public function recupererToutesPiecesCochees()
    {
        $listPiecesCochees = [];
        if ('0' != $this->getConsultation()->getEnvCandidature()) {
            foreach ($this->candidature->getItems() as $itemCandidature) {
                //Candidature
                if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                    $itemCandidature->atexoReponseEnveloppeCandidature->atexoReponseFormulaireSub->recupererPiecesCochees($listPiecesCochees);
                } else {
                    $itemCandidature->atexoReponseEnveloppeCandidature->atexoReponsePiecesLibres->recupererPiecesCochees($listPiecesCochees);
                    $itemCandidature->atexoReponseEnveloppeCandidature->atexoReponsePiecesTypees->recupererPiecesCochees($listPiecesCochees);
                }
            }
        }
        foreach ($this->listeOffres->getItems() as $itemOffre) {
            //Offres
            if ('0' != $this->getConsultation()->getEnvOffre()) {
                if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                    $itemOffre->atexoReponseEnveloppeOffre->atexoReponseFormulaireSub->recupererPiecesCochees($listPiecesCochees);
                } else {
                    $itemOffre->atexoReponseEnveloppeOffre->atexoReponsePiecesLibres->recupererPiecesCochees($listPiecesCochees);
                    $itemOffre->atexoReponseEnveloppeOffre->atexoReponsePiecesTypees->recupererPiecesCochees($listPiecesCochees);
                }
            }
            //Offres techniques
            if ('0' != $this->getConsultation()->getEnvOffreTechnique()) {
                if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                    $itemOffre->atexoReponseEnveloppeOffreTech->atexoReponseFormulaireSub->recupererPiecesCochees($listPiecesCochees);
                } else {
                    $itemOffre->atexoReponseEnveloppeOffreTech->atexoReponsePiecesLibres->recupererPiecesCochees($listPiecesCochees);
                    $itemOffre->atexoReponseEnveloppeOffreTech->atexoReponsePiecesTypees->recupererPiecesCochees($listPiecesCochees);
                }
            }
            //Anonymat
            if ('0' != $this->getConsultation()->getEnvAnonymat()) {
                if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                    $itemOffre->atexoReponseEnveloppeOffreAnonymat->atexoReponseFormulaireSub->recupererPiecesCochees($listPiecesCochees);
                } else {
                    $itemOffre->atexoReponseEnveloppeOffreAnonymat->atexoReponsePiecesLibres->recupererPiecesCochees($listPiecesCochees);
                    $itemOffre->atexoReponseEnveloppeOffreAnonymat->atexoReponsePiecesTypees->recupererPiecesCochees($listPiecesCochees);
                }
            }
        }
        if (is_array($listPiecesCochees) && count($listPiecesCochees)) {
            return $listPiecesCochees;
        }

        return false;
    }

    /**
     * Permet de gerer la visibilité des dossiers.
     */
    public function getVisibiliteDossier($typeEnv, $numLot)
    {
        if (!Atexo_Module::isEnabled('InterfaceModuleSub')) {
            return true;
        }
        $dossier = $this->getDossier($typeEnv, $numLot);
        if ($dossier) {
            return true;
        }

        return false;
    }

    /**
     * Permet de recuperer le dossier formulaire SUB
     * Est utilisé ici pour tester si le lot possède un dossier ou pas.
     */
    public function getDossier($typeEnv, $numLot)
    {
        $dossier = false;
        $reponseElectFormulaire = (new Atexo_FormulaireSub_Dossier())->recupererReponseElectFormulaire($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme(), Atexo_CurrentUser::getIdInscrit());
        if ($reponseElectFormulaire) {
            $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierByIdReponseElectFormulaireAndTypeEnveloppeAndNumLot($reponseElectFormulaire->getIdReponseElecFormulaire(), $typeEnv, $numLot);
        }

        return $dossier;
    }

    /*
     * Methode d'affectation des parametres à l'applet de chiffrement
     */
    public function getParamsMpeChiffrementApplet()
    {
        $modeRestriction = 'false';
        if (Atexo_Module::isEnabled('ModeRestrictionRGS')) {
            $modeRestriction = 'true';
        }

        return ['origineContexteMetier' => 'MPE_depot',
            'originePlateforme' => Atexo_Config::getParameter('ORIGINE_PLATEFORME'),
            'origineOrganisme' => Atexo_Config::getParameter('ORIGINE_ORGANISME'),
            'origineItem' => Atexo_Config::getParameter('ORIGINE_ITEM'),
            'typeHash' => Atexo_Config::getParameter('TYPE_ALGORITHM_HASH'),
            'urlPkcs11LibRefXml' => Atexo_Config::getParameter('URL_LIBRARY_PKCS11'),
            'methodeJavascriptDebutAttente' => 'showLoader',
            'methodeJavascriptFinAttente' => 'hideLoader',
            'methodeJavascriptRecuperationLog' => 'logAppletChargement',
            'modeRestrictionRGS' => $modeRestriction, ];
    }

    /*
     * Methode d'affectation des parametres à l'applet de sélection de fichiers
     */
    public function getParamsMpeSelectionFichierApplet()
    {
        return ['methodeJavascriptDebutAttente' => 'showLoader', 'methodeJavascriptFinAttente' => 'hideLoader', 'methodeJavascriptRenvoiResultat' => 'selectionEffectueeAppletDepotReponse',
            'methodeJavascriptFinTraitement' => 'finTraitementAppletSelectionFichiers', 'methodeJavascriptRecuperationLog' => 'logAppletChargement', ];
    }

    /*
     * Methode d'affectation des parametres à l'applet de génération de l'acte d'engagement
     */
    public function getParamsMpeAppletGenerationAE()
    {
        $modeRestriction = 'false';
        if (Atexo_Module::isEnabled('ModeRestrictionRGS')) {
            $modeRestriction = 'true';
        }

        return ['origineContexteMetier' => 'MPE_depot',
            'originePlateforme' => Atexo_Config::getParameter('ORIGINE_PLATEFORME'),
            'origineOrganisme' => Atexo_Config::getParameter('ORIGINE_ORGANISME'),
            'origineItem' => Atexo_Config::getParameter('ORIGINE_ITEM'),
            'typeHash' => Atexo_Config::getParameter('TYPE_ALGORITHM_HASH'),
            'urlPkcs11LibRefXml' => Atexo_Config::getParameter('URL_LIBRARY_PKCS11'),
            'methodeJavascriptDebutAttente' => 'showLoader',
            'methodeJavascriptFinAttente' => 'hideLoader',
            'methodeJavascriptRenvoiResultatGeneration' => 'renvoiResultatGeneration',
            'methodeJavascriptRecuperationLog' => 'logAppletChargement',
            'modeRestrictionRGS' => $modeRestriction, ];
    }

    /**
     * Permet d'afficher les informations de l'entreprise.
     */
    public function afficherInfosEntreprise($inscrit)
    {
        if ($inscrit instanceof CommonInscrit) {
            $this->nomEntreprise->Text = $inscrit->getEntreprise()->getNom();
            $this->nomInscrit->Text = $inscrit->getNom();
            $this->prenomInscrit->Text = $inscrit->getPrenom();
            $this->mailInscrit->Text = $inscrit->getEmail();
            $this->adresseAR->Text = $inscrit->getEmail();
        }
    }

    /**
     * Permet de charger le bloc Récap.
     */
    public function chargerBlocRecap($consultation)
    {
        if ($consultation instanceof CommonConsultation) {
            $this->detailConsultation->setReference($consultation->getId());
            $this->detailConsultation->setOrganisme($consultation->getOrganisme());
            $this->detailConsultationConfirmation->setReference($consultation->getId());
            $this->detailConsultationConfirmation->setOrganisme($consultation->getOrganisme());
        }
    }

    /**
     * Effectue les actions qui dependent de l'inscrit.
     *
     * @param CommonInscrit      $inscrit
     * @param CommonConsultation $consultation
     */
    public function getActionInscrit($inscrit, $consultation)
    {
        $uid = null;
        if (is_array($inscrit) && 1 == count($inscrit)) {
            $inscrit = array_shift($inscrit);
            if ($inscrit instanceof CommonInscrit && ($inscrit->getCommonEntreprise() instanceof Entreprise)) {
                $msgErreur = 'Acces page reponse: Ref: {'.Atexo_Util::atexoHtmlEntities($_GET['id']).'} [{'.$consultation->getReferenceutilisateur().'}] -
					Org: {'.Atexo_Util::atexoHtmlEntities($_GET['org']).'} -
					Id enteprise: '.Atexo_CurrentUser::getIdEntreprise().' -
					Id Inscrit: '.Atexo_CurrentUser::getIdInscrit().
                    ' INFOS: '.gethostbyaddr($_SERVER['REMOTE_ADDR'])." - {$inscrit->getPrenom()} {$inscrit->getNom()} - {$inscrit->getEmail()} ({$inscrit->getEntreprise()->getNom()}) - UID: ".$uid;
                Prado::log($msgErreur, TLogger::INFO, 'Atexo.pages.entreprise.FormulaireReponse.php');
                if (!$this->IsPostBack) {
                    $uid = uniqid(random_int(0, mt_getrandmax()), true);
                    $this->setViewState('uid', $uid);
                    if ('0' != $consultation->getEnvCandidature()) {
                        $this->panelCandidature->setVisible(true);
                        $this->setElementsEnveloppeCandidature();
                    } else {
                        $this->panelCandidature->setVisible(false);
                    }
                    if ('0' != $consultation->getEnvOffre() || '0' != $consultation->getEnvAnonymat() || '0' != $consultation->getEnvOffreTechnique()) {
                        $this->chargerListeLots($consultation);
                    }
                    $this->tempRestant->Text = Atexo_Util::diffDatesMoisJoursHeuresMinutes(date('Y-m-d H:i'), $consultation->getDatefin());
                    $this->afficherInfosEntreprise($inscrit);
                    $this->setViewState('nomEntreprise', $inscrit->getEntreprise()->getNom());
                    $this->setViewState('nomInscrit', $inscrit->getNom());
                    $this->setViewState('prenomInscrit', $inscrit->getPrenom());
                    //afficher les infos de signature chiffrement et transmission
                    $this->afficherInfosSignatureChiffrement($consultation);
                }
                if (Atexo_Module::isEnabled('AutoriserUneSeuleReponsePrincipaleParEntreprise')) {
                    if ((0 == strcmp($consultation->getVarianteCalcule(), '1') && !isset($_GET['var']))
                        || 0 == strcmp($consultation->getVarianteCalcule(), '0')) {
                        if (0 == strcmp($consultation->getVarianteCalcule(), '0')) {
                            if ($this->depotEntrepriseTrouve()) {
                                $this->afficherBlockErreur(Prado::localize('DEPOT_PLUSIERS_REPONSES_INTERDIT'));
                            }
                        } else {
                            if ($this->depotEntrepriseTrouve()) {
                                $this->redirectToFormWithVariante();
                            }
                        }
                    } elseif (0 == strcmp($consultation->getVarianteCalcule(), '1') && isset($_GET['var'])) {
                        if ($this->depotEntrepriseTrouve()) {
                            $this->afficherMessageErreurSansMasquerContenuPage(Prado::localize('DEPOT_PLUSIERS_REPONSES_INTERDIT_VARIANTES_OK'));
                            $this->offreVariante->checked = true;
                            $this->offreBase->checked = false;
                            $this->offreBase->setEnabled(false);
                        }
                    }
                }
            } else {
                $this->afficherBlockErreur(Prado::localize('NON_AUTORISER_DEPOT_REPONSE'));
            }
        } else {
            $this->afficherBlockErreur(Prado::localize('NON_AUTORISER_DEPOT_REPONSE'));
        }
    }

    /**
     * Envoi du mail à l'inscrit et l'agent.
     */
    public function sendMail($objetConsultation, $arrayReturned)
    {
        $params = [];
        $arrayInfosInscrit = [];
        $idBlobPjRecapMail = null;
        try {
            if (!$objetConsultation instanceof CommonConsultation) {
                $objetConsultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']));
            }
            $refCons = $objetConsultation->getConsultationId();
            $org = $objetConsultation->getOrganisme();
            $refUtilisateur = $objetConsultation->getReferenceUtilisateur();
            //envoi du mail de notification aux agent concerné
            $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($_GET['id'], $_GET['org']);
            $listeIdsAgents = (new Atexo_Consultation_Guests())->getIdsAgentFromArrayGuestsVo($guests);
            $agents = (new Atexo_Agent())->retrieveAgentsAvecAlerteReponseElectronique($listeIdsAgents);
            //Determination de la reference utilisateur de la consultation

            $params['reponseReceived'] = true;
            $listeAgents = Atexo_Consultation_Guests::listAgentConsultationFavoris(Atexo_Util::atexoHtmlEntities($_GET['id']), $params);

            if ($agents && is_array($agents)) {
                //Selection de la langue de la plateforme pour l'envoie du mail à l'agent de la plateforme
                $globalization = Prado::getApplication()->Modules['globalization'];
                $globalization->setTranslationCatalogue('messages.'.Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'));
                //Récuperation du corps du mail
                foreach($agents as $agent) {
                    if(
                        $agent->getEmail()
                        && in_array($agent->getId(), $listeAgents) !== false
                    ) {
                        $corpsMail = (new Atexo_Message())->getContentMailAgentForOffreAR(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), $arrayReturned,$agent);
                        Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $agent->getEmail(), Prado::localize('RECEPTION_REPONSE_ELECRONIQUE').' - '.
                            Prado::localize('TEXT_REF').' : '.$refUtilisateur, $corpsMail, $cc = '', $return_path = '', false, true);
                    }
                }
                $this->onInit();
            }
            $arrayReturned['errorSign'] = Atexo_Util::atexoHtmlEntities($_POST['erreurSignature']);
            $arrayInfosInscrit['nomInscrit'] = $this->getViewState('nomInscrit');
            $arrayInfosInscrit['prenomInscrit'] = $this->getViewState('prenomInscrit');
            $arrayInfosInscrit['mailInscrit'] = $this->adresseAR->Text;
            $arrayInfosInscrit['nomEse'] = $this->getViewState('nomEntreprise');

            $corpsMail = (new Atexo_Message())->getContentMailEseForOffreAR($refCons, $org, $arrayReturned, $arrayInfosInscrit, $this->getViewState('acteEngagement'));
            $corpsMail = str_replace('HTTP_ENCODING_MAIL', Atexo_Config::getParameter('HTTP_ENCODING'), $corpsMail);
            $dateDepot = str_replace(['-', ':', ' '], ['', '', '_'], $arrayReturned['dateDepot']);
            $namePdfFile = $refUtilisateur.'_'.$dateDepot.'.pdf';
            $nameHtmlFile = $refUtilisateur.'_'.$dateDepot.'.html';
            $pathPdfFile = Atexo_Config::getParameter('COMMON_TMP').'/'.$namePdfFile;
            $pathHtmlToFormat = Atexo_Config::getParameter('COMMON_TMP').'/'.$nameHtmlFile;
            Atexo_Util::writeFile($pathHtmlToFormat, $corpsMail);
            $pdfContent = null;
            if (is_file($pathHtmlToFormat)) {
                $pathFileLogGenerationPdf = Atexo_Config::getParameter('COMMON_TMP').'/logPrintMailReponses';
                if (!is_file($pathFileLogGenerationPdf)) {
                    Atexo_Util::writeFile($pathFileLogGenerationPdf, '');
                }
                $cmd = Atexo_Config::getParameter('PATH_BIN_WKHTMLPDF').' '.$pathHtmlToFormat.' '.$pathPdfFile.' >> '.$pathFileLogGenerationPdf.' 2>&1 ';
                system($cmd);
                if (is_file($pathPdfFile) && filesize($pathPdfFile) > 0) {
                    $pdfContent = file_get_contents($pathPdfFile);
                    $atexoBlob = new Atexo_Blob();
                    $idBlobPjRecapMail = $atexoBlob->insert_blob($namePdfFile, $pathPdfFile, $org);
                }
                unlink($pathHtmlToFormat);
            }
            //FIXME signature Mail ??
            $signed = false;
            if ('1' == Atexo_Config::getParameter('SIGNATURE_MAIL')) {
                $signed = true;
            }
            $from = Atexo_Config::getParameter('PF_MAIL_FROM');
            $to = $this->adresseAR->Text;
            $subject = Prado::localize('ACCUSE_RECEPTION_REPONSE_ELECTRONIQUE').' - '.Prado::localize('TEXT_REF').' : '.$refUtilisateur;
            (new Atexo_Message())->mailWithAttachement($from, $to, $subject, $corpsMail, '', $pdfContent, $namePdfFile, '', true, $signed);

            return $idBlobPjRecapMail;
        } catch (Exception $ex) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error($ex->getMessage());
        }
    }

    /**
     * Permet de sauvgarder l'historiques de navigation de l'inscrits sur
     * le formulaire de reponse.
     */
    public function saveTraceDiagInscrit($descriptionCle, $appeltLog = false, $variable = '', $IdDesciption = '', $listeSignedFile = null, $lienDownload = null)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $log = '';
        if (!$IdDesciption) {
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk($descriptionCle, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        }
        if (!$variable) {
            $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        } else {
            $description = $variable;
        }
        if ($listeSignedFile) {
            $description .= $listeSignedFile;
        }
        if ($appeltLog) {
            $log = $appeltLog; //$this->erreurApplet->value; TODO
        }
        $arrayDonnees = [];
        $arrayDonnees['ref'] = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $arrayDonnees['org'] = Atexo_Util::atexoHtmlEntities($_GET['org']);
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        $arrayDonnees['logApplet'] = $log;
        $arrayDonnees['description'] = $description;
        $arrayDonnees['lienDownload'] = $lienDownload;
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            '-',
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
    }

    /**
     * Permet de sauvgarder les erreurs de l'appler au chagement
     * de pa page.
     */
    public function saveErreurApplet($sender, $param)
    {
        if ($this->erreurAppletChargement->value) {
            $this->saveTraceDiagInscrit('DESCRIPTION22', $this->erreurAppletChargement->value);
        }
    }

    /*
     * Permet d'envoi des reponses à l'applet pour le chiffrement
     */
    public function poursuivreEnvoieReponse($sender, $param)
    {
        //Lors de l'ajout des fichiers au formulaire, nous avons encodé le json en utf8 dans le cas des plateformes ISO.
        //Nous allons donc le decoder afin que l'applet puisse l'interpreter
        $scriptEnvoiReponse = base64_decode($this->labelEnvoiReponse->Text);
        if ('utf-8' != strtolower(Atexo_Config::getParameter('HTTP_ENCODING'))) {
            $scriptEnvoiReponse = utf8_decode($scriptEnvoiReponse);
        }
        $this->functionEnvoiReponse->Text = '<script>'.$scriptEnvoiReponse.'</script>';
    }

    /**
     * Permet de préciser si la consultation contient
     * un DC3 (Acte engagement).
     *
     * @return true: si oui, false sinon
     */
    public function isConsultationHasPiecesTypees()
    {
        if ($this->getConsultation() instanceof CommonConsultation && '1' == $this->getConsultation()->getSignatureActeEngagement()) {
            return true;
        }

        return false;
    }

    public function afficherBlockErreur($messageErreur)
    {
        if ($messageErreur) {
            $this->errorPart->setDisplay('Dynamic');
            $this->panelMessageErreur->setDisplay('Dynamic');
            $this->panelMessageErreur->setMessage($messageErreur);
            $this->main_part->setDisplay('None');
            $this->confirmationPart->setDisplay('None');
        }
    }

    /**
     * Permet de gerer l'affichage du bloc des variantes.
     *
     * @param CommonConsultation $consultation; objet consultation
     */
    public function gererAffichageBlocVariantesConsultation($consultation)
    {
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && 0 == strcmp($consultation->getVarianteCalcule(), '1')) {
            $this->panelVariante->setDisplay('Dynamic');
            if (isset($_GET['var'])) {
                $this->offreVariante->checked = true;
            } else {
                $this->offreBase->checked = true;
            }
        } else {
            $this->panelVariante->setDisplay('None');
        }
    }

    /**
     * Permet de rediriger vers le formulaire de réponse avec l'utilisation de l'offre variante.
     */
    public function redirectToFormWithVariante()
    {
        $this->response->redirect('index.php?page=Entreprise.FormulaireReponseConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&org='.Atexo_Util::atexoHtmlEntities($_GET['org']).'&var');
    }

    /**
     * Permet de rediriger vers le formulaire de réponse normal (sans offre variante).
     */
    public function redirectToFormWithoutVariante()
    {
        $this->response->redirect('index.php?page=Entreprise.FormulaireReponseConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&org='.Atexo_Util::atexoHtmlEntities($_GET['org']));
    }

    /**
     * Permet de verifier si l'entreprise a deja deposé une reponse.
     *
     * @return true si depot trouvé, false sinon
     */
    public function depotEntrepriseTrouve()
    {
        $reponsesEntreprise = (new Atexo_Consultation_Responses())->retrieveReponsesEntreprise(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']), Atexo_CurrentUser::getIdInscrit());
        if (is_array($reponsesEntreprise) && !empty($reponsesEntreprise)) {
            return true;
        }

        return false;
    }

    /**
     * Permet d'afficher le message d'erreur sans masquer le contenu de la page
     * Différent de la fonction "afficherBlockErreur" qui en plus d'afficher le message d'erreur masque le contenu de la page.
     *
     * @param string $messageErreur: message d'erreur
     */
    public function afficherMessageErreurSansMasquerContenuPage($messageErreur)
    {
        $this->errorPart->setDisplay('Dynamic');
        $this->panelMessageErreur->setDisplay('Dynamic');
        $this->panelMessageErreur->setMessage($messageErreur);
    }

    /**
     * Permet de retourner l'id de la candidature MPS.
     *
     * @param CommonConsultation $consultation la consultation concernee,
     *
     * @return int l'id de la candidature
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getIdCandidatureMPS($consultation)
    {
        if (Atexo_Module::isEnabled('MarchePublicSimplifie', $consultation->getOrganisme()) && $consultation->getMarchePublicSimplifie() && isset($_GET['idCandidatureMps'])) {
            return Atexo_Util::atexoHtmlEntities($_GET['idCandidatureMps']);
        }

        return false;
    }

    /**
     * Permet de retourner si un fichier avec le meme nom existe deja.
     *
     * @param int    $typeEnveloppe le type d'enveloppe
     * @param int    $lot           le lot
     * @param string $nomFichier    le nom de fichier
     *
     * @return bool true si le fichier avec le meme nom existe si non false
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function isNomFichierExiste($typeEnveloppe, $lot, $nomFichier)
    {
        $dataSource = $this->getViewState('dataSourcePieces');
        if (is_array($dataSource)) {
            $listePieces = $dataSource[$typeEnveloppe.'_'.$lot];
            if (is_array($listePieces)) {
                foreach ($listePieces as $key => $pieces) {
                    if (is_array($pieces)) {
                        foreach ($pieces as $piece) {
                            if ($piece instanceof Atexo_Signature_FichierVo) {
                                if ($piece->getNomFichier() == $nomFichier) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Permet d'afficher le message d'erreur dans le cas d'ajout des fichiers deja presetent.
     *
     * @param array $nomFichiers les noms des fichiers
     * @param $param
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function afficherMessageErreurAjoutFile($nomFichiers, $param)
    {
        $message = Prado::Localize('PIECE_DEJA_PRESENTE');
        if (is_array($nomFichiers)) {
            $msg = '<ul><li>'.implode('</li><li>', $nomFichiers).'</li></ul>';
            $message .= (new Atexo_Util())->toHttpEncoding($msg);
        }
        $this->panelErreur->setMessage($message);
        $this->panelMessageAjoutFile->render($param->getNewWriter());
        $this->javascriptErreur->Text = "<script>openModal('erreur-ajout-fichier','popup-small2',document.getElementById('ctl0_CONTENU_PAGE_titrePopinErreur'));</script>";
    }

    /**
     * Permet de verifier que l'acte d'engagement est genere et signe
     * Affiche un message d'erreur dans le cas contraire.
     *
     * @param Atexo_Signature_FichierVo $fichier      : objet representant le fichier
     * @param CommonConsultation        $consultation : objet representant la consultation
     *
     * @return bool : true si acte d'engagement genere et signe, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function verifierActeEngagement($fichier, $consultation)
    {
        if ($this->getConditionGenerationAE($consultation) && $fichier instanceof Atexo_Signature_FichierVo
            && $fichier->getOrgineFichier() == Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_TYPEE') && (!$fichier->getCheminFichierXmlAE() || !$fichier->getCheminFichierSignatureXmlAE())) {
            $this->javascriptErreur->Text = "<script>bootbox.alert('".Prado::Localize('DEFINE_MESSAGE_ERREUR_ACTE_ENGAGEMENT_NON_GENERE_OU_SINE')."');</script>";

            return false;
        }

        return true;
    }

    /**
     * Verifie si la consultation verifie les criteres de generation de l'acte d'engagement.
     *
     * @param CommonConsultation $consultation : objet consultation
     *
     * @return bool : true si acte d'engagement a generer, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function getConditionGenerationAE($consultation)
    {
        return Atexo_Module::isEnabled('GenererActeDengagement') && $consultation instanceof CommonConsultation && $consultation->getSignatureOffre() && $consultation->getSignatureActeEngagement();
    }

    /**
     * Permet d'ajouter le nom des fichier vides dans messageFichiersVides.
     *
     * @param Atexo_Signature_FichierVo $fichier              le fichier
     * @param array                     $messageFichiersVides
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2016
     */
    public function ajoutMessageFichierVide($fichier, &$messageFichiersVides)
    {
        if ($fichier instanceof Atexo_Signature_FichierVo) {
            if (!$fichier->getTailleFichier()) {
                $messageFichiersVides[] = $fichier->getNomFichier();
            }
        }
    }

    /**
     * construit les appels ajouterCertificat par type d'enveloppe.
     *
     * @param $consultation
     * @param $functionEnvoiReponse
     *
     * @throws Exception
     */
    public function ConstruireAppelAppletChiffrement($consultation, &$functionEnvoiReponse)
    {
        $certificatChifrement = [];
        try {
            if ($consultation->getEnvAnonymat()) {
                $certificatChifrement[Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')] = (new Atexo_Certificat())->retrieveCertificatChiffrement($consultation->getId(), Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'), $consultation->getOrganisme());
            }
            if ($consultation->getEnvCandidature()) {
                $certificatChifrement[Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')] = (new Atexo_Certificat())->retrieveCertificatChiffrement($consultation->getId(), Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), $consultation->getOrganisme());
            }
            if ($consultation->getEnvOffre()) {
                $certificatChifrement[Atexo_Config::getParameter('TYPE_ENV_OFFRE')] = (new Atexo_Certificat())->retrieveCertificatChiffrement($consultation->getId(), Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $consultation->getOrganisme());
            }
            if ($consultation->getEnvOffreTechnique()) {
                $certificatChifrement[Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')] = (new Atexo_Certificat())->retrieveCertificatChiffrement($consultation->getId(), Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'), $consultation->getOrganisme());
            }
            $indexCertificat = 1;
            if (is_array($certificatChifrement)) {
                foreach ($certificatChifrement as $typeEnv => $listOneCertif) {
                    foreach ($listOneCertif as $oneCertif) {
                        $functionEnvoiReponse .= 'document.MpeChiffrementApplet.ajouterCertificat('.$typeEnv.", '".str_replace("\r", '', str_replace("\n", '', $oneCertif->getCertificat()))."');".PHP_EOL;
                        ++$indexCertificat;
                    }
                }
            }
        } catch (Exception $e) {
            throw $e;
            Prado::log('Erreur au niveau de la fonction ConstruireAppelAppletChiffrement : '.$e->getMessage(), TLogger::ERROR, 'FormulaireReponseConsultation.php');
        }
    }
}
