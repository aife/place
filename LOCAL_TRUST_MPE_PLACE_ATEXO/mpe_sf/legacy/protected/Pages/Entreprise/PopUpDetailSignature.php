<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

class PopUpDetailSignature extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->afficherDetailsignature();
        }
    }

    public function afficherDetailsignature()
    {
        //$id=$_POST["dce_item_$index"];$_POST['accepte' . $line->getId()];ctl0_CONTENU_PAGE_detailSignataire
        if (isset($_GET['cand'])) {
            $idInputSignataire = 'ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_signataire'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputEmisPar = 'ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_emisPar'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);

            $idInputDateValideFrom = 'ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideFrom'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputDateValideTo = 'ctl0_CONTENU_PAGE_PiecesCand_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideTo'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
        } elseif (isset($_GET['ae'])) {
            $idInputSignataire = 'ctl0_CONTENU_PAGE_repeaterOffres_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_signataire'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputEmisPar = 'ctl0_CONTENU_PAGE_repeaterOffres_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_emisPar'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);

            $idInputDateValideFrom = 'ctl0_CONTENU_PAGE_repeaterOffres_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideFrom'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputDateValideTo = 'ctl0_CONTENU_PAGE_repeaterOffres_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideTo'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
        } elseif ('Offre' == $_GET['typeEnv']) {
            $idInputSignataire = 'ctl0_CONTENU_PAGE_repeaterOffres_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesOffre_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_signataire'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputEmisPar = 'ctl0_CONTENU_PAGE_repeaterOffres_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesOffre_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_emisPar'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);

            $idInputDateValideFrom = 'ctl0_CONTENU_PAGE_repeaterOffres_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesOffre_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideFrom'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputDateValideTo = 'ctl0_CONTENU_PAGE_repeaterOffres_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesOffre_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideTo'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
        } elseif ('Technique' == $_GET['typeEnv']) {
            $idInputSignataire = 'ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesTechniques_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_signataire'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputEmisPar = 'ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesTechniques_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_emisPar'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);

            $idInputDateValideFrom = 'ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesTechniques_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideFrom'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputDateValideTo = 'ctl0_CONTENU_PAGE_repeaterOffresTechnique_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesTechniques_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideTo'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
        } elseif ('Anonymat' == $_GET['typeEnv']) {
            $idInputSignataire = 'ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesAnonymat_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_signataire'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputEmisPar = 'ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesAnonymat_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_emisPar'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);

            $idInputDateValideFrom = 'ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesAnonymat_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideFrom'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputDateValideTo = 'ctl0_CONTENU_PAGE_repeaterAnonymat_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idLot']).'_PiecesAnonymat_listePiecesRepeater_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideTo'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
        } else {
            $idInputSignataire = 'ctl0_CONTENU_PAGE_listePiecesASigner_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_signataire'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputEmisPar = 'ctl0_CONTENU_PAGE_listePiecesASigner_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_emisPar'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);

            $idInputDateValideFrom = 'ctl0_CONTENU_PAGE_listePiecesASigner_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideFrom'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
            $idInputDateValideTo = 'ctl0_CONTENU_PAGE_listePiecesASigner_ctl'.Atexo_Util::atexoHtmlEntities($_GET['idPiece']).'_dateValideTo'.Atexo_Util::atexoHtmlEntities($_GET['idSign']);
        }

        $this->jsDetailSignataire->Text = "<script>infoSignataireEmisPar('ctl0_CONTENU_PAGE_detailSignataire','".$idInputSignataire."');</script>";

        $this->jsDetailEmisPar->Text = "<script>infoSignataireEmisPar('ctl0_CONTENU_PAGE_detailEmisPar','".$idInputEmisPar."');</script>";

        $this->jsDateValideFrom->Text = "<script>infoSignataireEmisPar('ctl0_CONTENU_PAGE_dateValideFrom','".$idInputDateValideFrom."');</script>";

        $this->jsDateValideTo->Text = "<script>infoSignataireEmisPar('ctl0_CONTENU_PAGE_dateValideTo','".$idInputDateValideTo."');</script>";

        if ('0' == $_GET['statut']) {
            $this->signature->setStatut(Atexo_Config::getParameter('OK'));
        } elseif ('3' == $_GET['statut']) {
            $this->signature->setStatut(Atexo_Config::getParameter('UNKNOWN'));
        } else {
            $this->signature->setStatut(Atexo_Config::getParameter('NOK'));
        }

        //if($_GET['expired']=="1") {//verification signature
        //if($_GET['statut']=="3") {
        if ('0' == $_GET['expired']) {
            $this->periodeValidite->setStatut(Atexo_Config::getParameter('OK'));
        } elseif ('2' == $_GET['expired']) {
            $this->periodeValidite->setStatut(Atexo_Config::getParameter('NOK'));
        } else {
            $this->periodeValidite->setStatut(Atexo_Config::getParameter('UNKNOWN'));
        }

        if ('0' == $_GET['chaine']) {
            $this->chaine->setStatut(Atexo_Config::getParameter('OK'));
        } elseif ('2' == $_GET['chaine']) {
            $this->chaine->setStatut(Atexo_Config::getParameter('NOK'));
        } else {
            $this->chaine->setStatut(Atexo_Config::getParameter('UNKNOWN'));
        }

        if ('0' == $_GET['crl']) {
            $this->CRL->setStatut(Atexo_Config::getParameter('OK'));
        } elseif ('2' == $_GET['chaine']) {
            $this->CRL->setStatut(Atexo_Config::getParameter('NOK'));
        } else {
            $this->CRL->setStatut(Atexo_Config::getParameter('UNKNOWN'));
        }
    }
}
