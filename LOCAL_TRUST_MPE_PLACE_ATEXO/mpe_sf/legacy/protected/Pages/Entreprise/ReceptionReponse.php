<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ReceptionReponse extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $res = (new Atexo_Entreprise_Reponses())->recieveBloc(Atexo_Util::atexoHtmlEntities($_POST['uidOffre']), Atexo_Util::atexoHtmlEntities($_POST['bloc']), Atexo_Util::atexoHtmlEntities($_POST['typeEnv']), Atexo_Util::atexoHtmlEntities($_POST['sousPli']), Atexo_Util::atexoHtmlEntities($_POST['typeFichier']), Atexo_Util::atexoHtmlEntities($_POST['numOrdreFichier']), Atexo_Util::atexoHtmlEntities($_POST['numOrdreBloc']), Atexo_Util::atexoHtmlEntities($_GET['org']));
        if ($res > 0) {
            echo 'OK';
        } else {
            echo 'ERR';
        }
        exit;
    }
}
