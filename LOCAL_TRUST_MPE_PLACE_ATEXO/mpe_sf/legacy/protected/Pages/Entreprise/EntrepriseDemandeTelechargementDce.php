<?php

namespace Application\Pages\Entreprise;

use App\Service\PlateformeVirtuelle\Context;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Prado\Exceptions\TException;
use Prado\Prado;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDemandeTelechargementDce extends MpeTPage
{
    public $_reference;
    public $_organisme;

    public function onInit($param)
    {

        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        MessageStatusCheckerUtil::initEntryPoints('hinclude');
    }

    public function onLoad($param)
    {
        $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        if (empty($_GET['id']) && !isset($_GET['id'])) {
            throw new TException('Cette page n\'existe pas.');
        }

        $dateDebAction = date('Y-m-d H:i:s');
        //Debut affichage du bloc mes coordonnees
        if (Atexo_Module::isEnabled('TelechargerDceAvecAuthentification') && $this->User->IsGuest) {
            //On verifie si l'inscrit est authentifie, sinon on redirige vers la page d'authentification
            $this->EntrepriseFormulaireDemande->blocMesCoordonnees->setStyle('display:none');
            $this->activerValidateursCoordonnees();
        }
        //Fin affichage du bloc mes coordonnees
        $this->EntrepriseFormulaireDemande->setPostBack($this->isPostBack);
        if (!$this->isPostBack) {
            $this->EntrepriseFormulaireDemande->getCoordonnees();
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION5', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            $this->traceOperationsInsrctis($IdDesciption, $dateDebAction, '', $this->_reference, $this->_organisme);
        }

        $code = Atexo_Util::atexoHtmlEntities($_GET['code']);

        if (!Atexo_Module::isEnabled('moduleSourcing', $this->_organisme)) {
            $this->panelSourcingFragment->setVisible(false);
        }

        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, $this->_organisme, false);
        if(!empty($consultation) && $consultation->getCodeDceRestreint() && !($code === $consultation->getCodeDceRestreint())) {
            $urlRedirect = 'app.php/entreprise/consultation/'
                . $this->_reference
                . '?orgAcronyme='  . $this->_organisme
                . '&code=' . $code
            ;

            return $this->response->redirect($urlRedirect);
        }
    }

    /**
     * Enregistre les donnees relatives a l'inscrit dans la table Telechargement.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function saveInscritTelechargement($sender, $param)
    {
        $pays = null;
        $acronymePays = null;
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, $this->_organisme);
        if ($consultation) {
            $dateDebAction = date('Y-m-d H:i:s');
            if ($this->EntrepriseFormulaireDemande->inscriptionAuthentification->Checked
                    && Atexo_Module::isEnabled('TelechargerDceAvecAuthentification') && $this->User->IsGuest
            ) {
                $url = urlencode('?page=Entreprise.EntrepriseDemandeTelechargementDce&id='.
                        Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                $this->response->redirect('?page=Entreprise.EntrepriseHome&goto='.$url);
            } elseif (($this->EntrepriseFormulaireDemande->choixTelechargement->Checked) || (!Atexo_Module::isEnabled('TelechargerDceSansIdentification') && !Atexo_Module::isEnabled('TelechargerDceAvecAuthentification'))) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $telechargementCom = new CommonTelechargement();
                //->ici on ajoute le controle avec php sur les champs obligatoire si le java script est desactive sur le navigateur
                if ('' == trim($this->EntrepriseFormulaireDemande->nom->Text) || '' == trim($this->EntrepriseFormulaireDemande->prenom->Text)
                        || '' == trim($this->EntrepriseFormulaireDemande->email->Text) || false == $this->EntrepriseFormulaireDemande->accepterConditions->Checked
                ) {
                    $this->response->redirect('index.php?page=Entreprise.EntrepriseAccueilAuthentifie');
                }
                $telechargementCom->setAdresse($this->EntrepriseFormulaireDemande->address->Text);
                $telechargementCom->setCodepostal($this->EntrepriseFormulaireDemande->cp->Text);
                $telechargementCom->setConsultationId($this->_reference);
                $telechargementCom->setEmail(trim($this->EntrepriseFormulaireDemande->email->Text));
                $telechargementCom->setTelephone($this->EntrepriseFormulaireDemande->tel->Text);
                $telechargementCom->setFax($this->EntrepriseFormulaireDemande->fax->Text);
                $telechargementCom->setVille($this->EntrepriseFormulaireDemande->ville->Text);
                $telechargementCom->setEntreprise($this->EntrepriseFormulaireDemande->raisonSocial->Text);
                $telechargementCom->setIdInscrit(!empty(Atexo_CurrentUser::getIdInscrit()) ? Atexo_CurrentUser::getIdInscrit() : null);
                $telechargementCom->setIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
                $telechargementCom->setEntreprise($this->EntrepriseFormulaireDemande->raisonSocial->Text);
                $telechargementCom->setNom($this->EntrepriseFormulaireDemande->nom->Text);
                $telechargementCom->setPrenom($this->EntrepriseFormulaireDemande->prenom->Text);
                $telechargementCom->setConsultationId($this->_reference);
                $telechargementCom->setOrganisme($this->_organisme);
                $telechargementCom->setDatetelechargement(date('Y-m-d H:i:s'));

                /** @var Context $pfvContextService */
                $pfvContextService = Atexo_Util::getSfService(Context::class);
                $pfvId = $pfvContextService->getCurrentPlateformeVirtuelleId();
                $telechargementCom->setPlateformeVirtuelleId($pfvId);

                if ($this->EntrepriseFormulaireDemande->france->checked) {
                    $telechargementCom->setPays(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
                    $telechargementCom->setAcronymePays(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
                    if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                        $telechargementCom->setSiret($this->EntrepriseFormulaireDemande->RcVille->SelectedValue.
                                Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$this->EntrepriseFormulaireDemande->RcNumero->Text);
                    } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                        $telechargementCom->setSiret($this->EntrepriseFormulaireDemande->identifiantUnique->Text);
                    } else {
                        $telechargementCom->setSiret($this->EntrepriseFormulaireDemande->siren->Text.
                                $this->EntrepriseFormulaireDemande->siret->Text);
                    }
                } else {
                    if (null != $this->EntrepriseFormulaireDemande->pays->getSelectedValue()) {
                        $pays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveCountryById($this->EntrepriseFormulaireDemande->pays->getSelectedValue());
                        $acronymePays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveAccronymeCountryById($this->EntrepriseFormulaireDemande->pays->getSelectedValue());
                    }
                    $telechargementCom->setPays($pays);
                    $telechargementCom->setAcronymePays($acronymePays);
                    $telechargementCom->setSirenetranger($this->EntrepriseFormulaireDemande->idNational->Text);
                }
                $telechargementCom->setDatetelechargement(date('Y-m-d H:i:s'));
                $telechargementCom->setAdresse2($this->EntrepriseFormulaireDemande->address2->Text);
                $this->setViewstate('telechergement', $telechargementCom);
            } else {
                $this->setViewstate('telechergement', '-1');
                if ($this->EntrepriseFormulaireDemande->choixAnonyme->checked) {
                    $this->EntrepriseDownloadDce->panelEnvoiPostauComplementaires->setVisible(false);
                }
            }
            $this->panelEntrepriseFormulaireDemande->setvisible(false);
            $this->panelEntrepriseDownloadDce->setvisible(true);
            $details = Prado::localize('DEFINE_FORMULAIRE_DEMANDE');
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION6', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            $this->traceOperationsInsrctis($IdDesciption, $dateDebAction, $details, Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        }
    }

    /**
     * retourner a la page Detail de la consultation.
     */
    public function goToDetailConsultation($sender, $param)
    {
        $codeAccess = Atexo_CurrentUser::readFromSessionSf("contexte_authentification_" . $_GET['id'])['codeAcces']
            ? '&code=' . Atexo_CurrentUser::readFromSessionSf("contexte_authentification_" . $_GET['id'])['codeAcces']
            : '';
        $this->response->redirect('entreprise/consultation/' . Atexo_Util::atexoHtmlEntities($_GET['id'])
            . '?orgAcronyme=' . Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme'])) . $codeAccess ;
    }

    /*
     * desactive les validateurs se trouvant dans le bloc "Mes coordonnees" dans le formulaire
     */
    public function desactiverValidateursCoordonnees()
    {
        $this->EntrepriseFormulaireDemande->nomValidator->setEnabled(false);
        $this->EntrepriseFormulaireDemande->prenomValidator->setEnabled(false);
        $this->EntrepriseFormulaireDemande->emailValidator->setEnabled(false);
        $this->EntrepriseFormulaireDemande->validatorSiren->setEnabled(false);
        $this->EntrepriseFormulaireDemande->rcValidator->setEnabled(false);
    }

    /**
     * Permet d'activer les validateurs du bloc "Mes coordonnees".
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function activerValidateursCoordonnees()
    {
        $this->EntrepriseFormulaireDemande->nomValidator->setEnabled(true);
        $this->EntrepriseFormulaireDemande->prenomValidator->setEnabled(true);
        $this->EntrepriseFormulaireDemande->emailValidator->setEnabled(true);
        $this->EntrepriseFormulaireDemande->validatorSiren->setEnabled(true);
        $this->EntrepriseFormulaireDemande->rcValidator->setEnabled(true);
    }

    /*
     * Permet de tracer l'acces de l'inscrit a la PF
     */
    public function traceOperationsInsrctis($IdDesciption, $dateDebAction, $details = '', $ref = '', $org = '')
    {
        $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $arrayDonnees = [];
        $arrayDonnees['ref'] = $ref;
        $arrayDonnees['org'] = $org;
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        $arrayDonnees['logApplet'] = '';
        $arrayDonnees['description'] = $description;
        if (!$details) {
            $details = '-';
        }
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            $details,
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
    }
}
