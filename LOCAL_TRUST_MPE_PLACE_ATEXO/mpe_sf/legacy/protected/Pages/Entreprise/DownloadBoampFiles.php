<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Pages\Agent\download;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;

/**
 * Class Interne pour le récuparation des files des boamp (xml/html).
 *
 * @author khadija CHOUIKA  <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 2.2
 *
 * @since MPE-4.0
 */
class DownloadBoampFiles extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($_GET['id'] && $_GET['orgAcronyme']) {
            $file = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_BOAMP_FICHIER_LOGS').'FichierHtmlXmlBoamp.csv';
            if (file_exists($file)) {
                $fileHandle = fopen($file, 'r+');
                while (($data = fgetcsv($fileHandle, 100, '#')) !== false) {
                    if ((base64_decode($_GET['id']) == $data[3]) && ($_GET['orgAcronyme'] == $data[2])) {
                        if (isset($_GET['xml'])) {
                            $fichierXml = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_BOAMP_XML_TRAITES').$data[4];
                            (new download())->createHeader($fichierXml, $data[4]);
                            break;
                        } elseif (isset($_GET['html'])) {
                            $fichierXml = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_BOAMP_HTML_TRAITES').$data[5];
                            (new download())->createHeader($fichierXml, $data[5]);
                            break;
                        }
                    }
                }
                fclose($fileHandle);

                return false;
            } else {
                echo "Le fichier de correspondance n'existe pas \n";
            }
        }
    }
}
