<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe ReponseEntreprise.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ReponseEntreprise extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        //Le cas d'une consultation restreinte.
        $consultationObj = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['acronymeOrg']));
        if ($consultationObj) {
            if ($consultationObj->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                Atexo_CurrentUser::writeToSession('organisme', Atexo_Util::atexoHtmlEntities($_GET['acronymeOrg']));
                Atexo_CurrentUser::writeToSession('reference', $consultationObj->getReferenceUtilisateur());
                Atexo_CurrentUser::writeToSession('codeAcces', $consultationObj->getCodeProcedure());
            }
        }

        // Pour EntrepriseConsultationSummary
        $_GET['id'] = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $_GET['orgAcronyme'] = Atexo_Util::atexoHtmlEntities($_GET['acronymeOrg']);
	   	$this->TemplateEnvoiCourrierElectronique->idMsgModifCons = Atexo_Config::getParameter('ID_MESSAGE_COURRIER_LIBRE');
        $this->TemplateEnvoiCourrierElectronique->panelTypeMessage->setVisible(false);
        $this->TemplateEnvoiCourrierElectronique->panelDestinataire->setVisible(false);

        $from = Atexo_Config::getParameter('PF_MAIL_FROM');

        if ($consultationObj instanceof CommonConsultation
            && $consultationObj->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
            $plateformeVirtuelle = $consultationObj->getCommonPlateformeVirtuelle();
            $from = $plateformeVirtuelle->getNoReply();
        }

        $this->TemplateEnvoiCourrierElectronique->destinataires->Text = $from;

        $this->TemplateEnvoiCourrierElectronique->panelCorpsAR->setVisible(true);
        $this->TemplateEnvoiCourrierElectronique->panelOptionEnvoi->setVisible(false);
        $this->TemplateEnvoiCourrierElectronique->setTailleMaxPj(Atexo_Config::getParameter('ATACHEMENT_MAX_UPLOAD_FILE_SIZE'));
        $this->TemplateEnvoiCourrierElectronique->setConfirmationReponseEntreprise(true);

        $this->TemplateEnvoiCourrierElectronique->setPostBack($this->IsPostBack);
    }

    public function RedirectReturn()
    {
        $this->response->redirect('?page=Agent.DetailConsultation&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
    }
}
