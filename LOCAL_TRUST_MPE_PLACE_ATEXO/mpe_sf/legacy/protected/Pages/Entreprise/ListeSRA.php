<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieINSEEPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Prado\Prado;

/*
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class ListeSRA extends MpeTPage
{
    public $langue;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->tableauSRA->setPostBack($this->IsPostBack);
        $this->tableauSRA->panelListeSRA->setDisplay('None');
        if (!$this->isPostBack) {
            $this->formRechercheSRA->setVisible(true);
            $this->listeSRA->setVisible(false);
            self::displayOrganismes();
            self::fillClassification();
            self::fillAnneeSRA();
        }
    }

    public function displayOrganismes()
    {
        if (isset($_COOKIE['selectedorg'])) {
            $ArrayOrganismes = [];
            $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($this->langue);
            $ArrayOrganismesO = Atexo_Organismes::retrieveOrganismeByAcronyme($_COOKIE['selectedorg']);
            if (0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$ArrayOrganismesO->$getDenominationOrg()) {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->getDenominationOrg();
            } else {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->$getDenominationOrg();
            }
        } else {
            $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $this->langue);
            array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');
        }

        $this->organismesNames->dataSource = $ArrayOrganismes;
        $this->organismesNames->DataBind();
    }

    public function onSearchClick()
    {
        $this->tableauSRA->panelListeSRA->setDisplay('Dynamic');
        $this->tableauSRA->setClassification($this->classification->getSelectedValue());
        $this->tableauSRA->setOrganisme($this->organismesNames->getSelectedValue());
        $this->tableauSRA->setAnnee($this->anneeSRA->getSelectedValue());

        $this->tableauSRA->displaySyntheseRapportAudit();
        $this->listeSRA->setVisible(true);
    }

    public function displaySearchSRA()
    {
        $this->Page->formRechercheSRA->setVisible(true);
        $this->Page->listeSRA->setVisible(false);
        $this->response->redirect('?page=Entreprise.ListeSRA');
    }

    /*
     * replis la liste des classification
     */
    public function fillClassification()
    {
        $criteria = new Criteria();
        $codesInsee = CommonCategorieINSEEPeer::doSelect($criteria, Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));
        $codesInsee = is_array($codesInsee) ? $codesInsee : [];
        $dataSource = ['0' => '--- '.Prado::localize('TOUTES_LES_CLASSIFICATIONS').' ---'];
        foreach ($codesInsee as $insee) {
            $dataSource[$insee->getId()] = $insee->getLibelle();
        }
        $this->classification->setDataSource($dataSource);
        $this->classification->DataBind();
    }

    public function choixClassification($sender, $param)
    {
        $idClassification = $this->classification->getSelectedValue();
        $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $this->langue, true, $idClassification);
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');

        $this->organismesNames->dataSource = $ArrayOrganismes;
        $this->organismesNames->DataBind();

        $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = 'none';</script>";

        $this->panelSearchInSRA->render($param->NewWriter);
    }

    public function fillAnneeSRA()
    {
        $listesAnnees = [];
        $listesAnnees[0] = '---'.Prado::localize('TEXT_SELECTIONNER').'---';
        for ($i = Atexo_Config::getParameter('NBRS_ANNEE_ANTERIEUR'); $i >= 0; --$i) {
            $listesAnnees[date('Y') - $i] = date('Y') - $i;
        }
        $this->anneeSRA->dataSource = $listesAnnees;
        $this->anneeSRA->dataBind();
    }
}
