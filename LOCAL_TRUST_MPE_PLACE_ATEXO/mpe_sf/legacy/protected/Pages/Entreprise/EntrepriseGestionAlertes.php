<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Alertes;
use Prado\Prado;

/**
 * Page de recherche avancee.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseGestionAlertes extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $this->panelMessageErreur->setVisible(false);
        if (!$this->IsPostBack) {
            $this->displayAlertes();
        }
        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    /**
     * afficher le tableau des alertes.
     */
    public function displayAlertes()
    {
        $this->repeaterAlertes->DataSource = (new Atexo_Entreprise_Alertes())->retreiveAlertes(Atexo_CurrentUser::getIdInscrit());
        $this->repeaterAlertes->DataBind();
    }

    /**
     * Actualiser le tableau des Alertes dans le cas d'une nouvelle action.
     */
    public function refreshRepeater($sender, $param)
    {
        $this->repeaterAlertes->DataSource = (new Atexo_Entreprise_Alertes())->retreiveAlertes(Atexo_CurrentUser::getIdInscrit());
        $this->repeaterAlertes->DataBind();
        $this->panelAlertes->render($param->NewWriter);
    }

    /**
     * actions sur les Alertes.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function actionAlerte($sender, $param)
    {
        if ('Delete' === $param->CommandName) {
            (new Atexo_Entreprise_Alertes())->deleteAlerte($param->CommandParameter);
        } elseif ('Search' === $param->CommandName) {
            $alerte = (new Atexo_Entreprise_Alertes())->retreiveAlertById($param->CommandParameter);
            if ($alerte) {
                $this->Response->Redirect('index.php?page=Entreprise.EntrepriseAdvancedSearch&idAlerte='.$alerte->getId());
            } else {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ALERTE_DESACTIVER'));
            }
        }
    }
}
