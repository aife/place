<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use Prado\Prado;

/**
 * Page detail d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AnnulerDepot extends MpeTPage
{
    public ?string $_sha1AnnulationString = null;
    private $_xmlString;
    private $_signatureOffre;
    private bool $_isValid = false;
    private ?string $_texteConstitutionDossierReponse = null;
    private ?string $_typeSignature = null;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $messageError = null;
        $this->errorPart->setVisible(false);

        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $idOffre = Atexo_Util::atexoHtmlEntities($_GET['idO']);
        // bloc à supprimer
        $corpsMail = (new Atexo_Entreprise_Reponses())->corpsMailAnnulation(Atexo_CurrentUser::getIdInscrit(), $consultationId, $idOffre, $organisme, 'mohamed.blal@gmail.com', 'Ceci est un text');
        Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), 'mohamed.blal@gmail.com', Prado::localize('ACCUSE_RECEPTION_ANNULATION'), $corpsMail, '', '', false, true);
        // fin bloc à supprimer
        $commonConsultation = (new Atexo_Consultation())->retrieveCommonConsultation($consultationId, $organisme);
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
        $pliOuvert = (new Atexo_Consultation_Responses())->verifierSiUnPliOuvert($idOffre, $organisme);

        if (!$pliOuvert && $commonConsultation instanceof CommonConsultation && $commonConsultation->getDatefin() > date('Y-m-d H:i:s')) {
            $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, $organisme);

            if ($offre instanceof CommonOffres && $offre->getInscritId() == Atexo_CurrentUser::getIdInscrit()) {
                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritAndEntreprise(Atexo_CurrentUser::getIdInscrit());

                if (is_array($inscrit) && 1 == count($inscrit)) {
                    $inscrit = array_shift($inscrit);
                    if ($inscrit instanceof CommonInscrit && ($inscrit->getEntreprise() instanceof Entreprise)) {
                        $this->nomEntreprise->Text = $inscrit->getEntreprise()->getNom();
                        $this->nomInscrit->Text = $inscrit->getNom();
                        $this->prenomInscrit->Text = $inscrit->getPrenom();
                        $this->mailInscrit->Text = $inscrit->getEmail();
                        $this->adresseAR->Text = $inscrit->getEmail();
                    }
                }

                if ('0' != $offre->getDepotAnnule()) {
                    $messageError = Prado::localize('DEPOT_DEJA_ANNULE');
                } else {
                    $this->horodatage->Text = Atexo_Util::iso2frnDateTime($offre->getUntrusteddate());
                    $arrayEnveloppes = (new Atexo_Entreprise_Reponses())->createArrayFromXmlOffre($offre->getXmlString());
                    $arrayEnveloppes['dateDepot'] = $offre->getUntrusteddate();
                    $this->_texteConstitutionDossierReponse = (new Atexo_Entreprise_Reponses())->getNomsFichiersFromArray($arrayEnveloppes, $consultation->getSignatureActeEngagement(), '<br />');
                    $this->constitutionDossierReponse->Text = $this->_texteConstitutionDossierReponse;
                    $this->_sha1AnnulationString = sha1($offre->getXmlString());

                    //Recuperation de la signature a partir de l'xml de l'offre
                    $this->_signatureOffre = (new Atexo_Entreprise_Reponses())->retrieveSignatureOffreFromXml($offre->getXmlString());
                    //utilisation de l'objet propel Fichierenveloppe afin de profiter des fcts sur le certificat (getCn,...)
                    $fichierEnveloppe = new CommonFichierEnveloppe();

                    $this->emailSubject->Text = $fichierEnveloppe->getEmailSubject($commonConsultation->getSignatureOffre());
                    $this->cnSubject->Text = $fichierEnveloppe->getCnSubject($commonConsultation->getSignatureOffre());
                    $this->ouSubject->Text = $fichierEnveloppe->getOuSubject($commonConsultation->getSignatureOffre());
                    $this->oSubject->Text = $fichierEnveloppe->getOSubject($commonConsultation->getSignatureOffre());
                    $this->cSubject->Text = $fichierEnveloppe->getCObject($commonConsultation->getSignatureOffre());

                    $this->cnEmetteur->Text = $fichierEnveloppe->getCnEmmeteur($commonConsultation->getSignatureOffre());
                    $this->ouEmetteur->Text = $fichierEnveloppe->getOuEmetteur($commonConsultation->getSignatureOffre());
                    $this->oEmetteur->Text = $fichierEnveloppe->getOEmetteur($commonConsultation->getSignatureOffre());
                    $this->cEmetteur->Text = $fichierEnveloppe->getCEmetteur($commonConsultation->getSignatureOffre());

                    $this->_isValid = true;
                }
            } else {
                $messageError .= Prado::localize('NON_AUTORISE_A_ANNULER');
            }
        } else {
            $messageError = Prado::localize('ANNULATION_IMPOSSIBLE');
        }

        if ($messageError) {
            $this->panelMessageErreur->setVisible(true);
            $this->errorPart->setVisible(true);
            $this->panelMessageErreur->setMessage($messageError);
            $this->mainPart->setVisible(false);
        }
    }

    public function signatureRecieved()
    {
        if ($this->_isValid) {
            $organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
            $idOffre = Atexo_Util::atexoHtmlEntities($_GET['idO']);
            $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
            $signature = Atexo_Util::atexoHtmlEntities($_POST['signature']);

            $atexoCrypto = new Atexo_Crypto();

            $signatureAnnulationPkcs7 = "-----BEGIN PKCS7-----\n".trim($signature)."\n-----END PKCS7-----";

            $signatureDepot = base64_decode(trim($this->_signatureOffre));
            $pos = strpos($signatureDepot, 'ds:SignedInfo');
            if (false !== $pos) {
                $this->_typeSignature = 'XML';
            } else {
                $signatureDepot = "-----BEGIN PKCS7-----\n".trim($this->_signatureOffre)."\n-----END PKCS7-----";
            }
            $cerificatFileNameAnnulation = ($atexoCrypto->getCertificatFromSignature($signatureAnnulationPkcs7));
            $serialCertAnnulation = (new Atexo_Crypto_Certificat())->getSerialFromCertif($cerificatFileNameAnnulation);
            $cnSubjectCertAnnulation = (new Atexo_Crypto_Certificat())->getCnCertificatFromFile($cerificatFileNameAnnulation);
            $cnIssuerCertAnnulation = (new Atexo_Crypto_Certificat())->getCnEmetteurCertificatFromFile($cerificatFileNameAnnulation);
            $dateExprertAnnulation = (new Atexo_Crypto_Certificat())->getDateExpiration(file_get_contents($cerificatFileNameAnnulation));

            $certificatFileNameDepot = ($atexoCrypto->getCertificatFromSignature($signatureDepot, true, true, $this->_typeSignature));
            $serialCertDepot = (new Atexo_Crypto_Certificat())->getSerialFromCertif($certificatFileNameDepot);
            $cnSubjectCertDepot = (new Atexo_Crypto_Certificat())->getCnCertificatFromFile($certificatFileNameDepot);
            $cnIssuerCertDepot = (new Atexo_Crypto_Certificat())->getCnEmetteurCertificatFromFile($certificatFileNameDepot);
            $dateExprertDepot = (new Atexo_Crypto_Certificat())->getDateExpiration(file_get_contents($certificatFileNameDepot));

            if ($serialCertAnnulation != $serialCertDepot || $cnSubjectCertAnnulation != $cnSubjectCertDepot || $cnIssuerCertAnnulation != $cnIssuerCertDepot || $dateExprertAnnulation != $dateExprertDepot) {
                $this->panelErreurAnnulation->setVisible(true);
                $this->panelErreurAnnulation->setMessage(Prado::localize('CERTIFICAT_ANNULATION_DIFFERENT'));

                return;
            } else {
                $verificationAnnulation = $atexoCrypto->verifierCertificatDeSignature($signatureAnnulationPkcs7, false);

                $fichierEnveloppe = (new Atexo_Entreprise_Reponses())->retrieveUnFichierEnveloppeByIdOffre($idOffre, $organisme);
                $verificationDepot = $fichierEnveloppe->getVerificationCertificat();

                //Verifie si le certificat n'etait pas revoque au moment du depot et est revoque au moment de l'annulation
                if ('0' == $verificationDepot[1] && '1' == $verificationAnnulation[1]) {
                    $this->panelErreurAnnulation->setVisible(true);
                    $this->panelErreurAnnulation->setMessage(Prado::localize('ANNULATION_IMPOSSIBLE_CERTIFICAT_REVOQUE'));

                    return;
                } else {
                    $timeStampVars = $atexoCrypto->timeStampData($signature);

                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $offre = CommonOffresPeer::retrieveByPK($idOffre, $organisme, $connexionCom);
                    $offre->setDepotAnnule('1');
                    $offre->setHorodatageAnnulation($timeStampVars['horodatage']);
                    $offre->setDateAnnulation($timeStampVars['untrustedDate']);
                    $offre->setSignatureAnnulation($signature);
                    $offre->setStringAnnulation($this->_xmlString);
                    $offre->setVerificationCertificatAnnulation($verificationAnnulation);
                    $offre->save($connexionCom);

                    (new Atexo_Entreprise_Reponses())->deleteBlobsReponse($idOffre, $organisme);

                    $corpsMail = (new Atexo_Entreprise_Reponses())->corpsMailAnnulation(Atexo_CurrentUser::getIdInscrit(), $consultationId, $idOffre, $organisme, $this->adresseAR->Text, $this->_texteConstitutionDossierReponse);
                    Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $this->adresseAR->Text, Prado::localize('ACCUSE_RECEPTION_ANNULATION'), $corpsMail, '', '', ('1' == Atexo_Config::getParameter('SIGNATURE_MAIL')));

                    self::gotoMesReponses();
                }
            }
        }
    }

    public function gotoMesReponses()
    {
        $this->response->redirect('index.php?page=Entreprise.MesReponses&Success');
    }

    /*
     * Permet d'avoir les parametres pour l'applet SignaturePkcs7Applet
     */
    public function getParamsSignaturePkcs7Applet()
    {
        return ['methodeJavascriptDebutAttente' => 'afficherVeuillezPatienter', 'methodeJavascriptFinAttente' => 'masquerVeuillezPatienter', 'methodeJavascriptFinTraitement' => 'finTraitementSignaturePkcs7Applet', 'methodeJavascriptRenvoiResultat' => 'renvoiResultatSignaturePkcs7Applet'];
    }
}
