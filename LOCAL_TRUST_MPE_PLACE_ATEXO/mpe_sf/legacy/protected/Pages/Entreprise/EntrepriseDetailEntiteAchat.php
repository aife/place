<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe EntrepriseDetailEntiteAchat.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDetailEntiteAchat extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $ArrayOrganismes = (new Atexo_Organismes())->retrieveActiveOrganismes(false, true, true);
        if (isset($_GET['idEntite']) && isset($_GET['orgAcr']) && $ArrayOrganismes[$_GET['orgAcr']]) {
            $this->DetailEntiteAchat->_IdEntite = Atexo_Util::atexoHtmlEntities($_GET['idEntite']);
            $this->DetailEntiteAchat->_org = Atexo_Util::atexoHtmlEntities($_GET['orgAcr']);
            $this->idretour->NavigateUrl = '?page=Entreprise.EntrepriseVisualiserEntiteAchatsArborescence&acrOrg='.Atexo_Util::atexoHtmlEntities($_GET['orgAcr']);
        } else {
            exit;
        }
    }
}
