<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Web\UI\TPageStateFormatter;

class Key extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        Atexo_Util::setNonav(true);
    }

    public function onLoad($param)
    {
        $key = TPageStateFormatter::serialize($this->page, []);

        echo $key;
        exit;
    }
}
