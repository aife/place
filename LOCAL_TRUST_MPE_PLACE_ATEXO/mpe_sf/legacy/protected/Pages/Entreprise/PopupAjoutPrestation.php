<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonPrestation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Prestation;
use Prado\Prado;

class PopupAjoutPrestation extends MpeTPage
{
    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        if (!$this->IsPostBack) {
            $this->displayProcedureType(true);
            if ($_GET['id']) {
                $this->displayPrestation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            }
        }

        $this->customizeForm();
    }

    public function displayProcedureType($calledFromPrtail)
    {
        $typeProc = new Atexo_Consultation_ProcedureType();
        $listTypeProc = $typeProc->retrieveProcedureType($calledFromPrtail, false, null, Atexo_CurrentUser::readFromSession('lang'));
        $arrayTypeProc = Atexo_Util::arrayUnshift($listTypeProc, '--- '.Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUS_TYPES_PROCEDURES').' ---');
        $this->procedureType->DataSource = $arrayTypeProc;
        $this->procedureType->DataBind();
    }

    public function savePrestation($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idEntreprise = Atexo_CurrentUser::getIdEntreprise();
        if (isset($_GET['id'])) {
            $prestation = (new Atexo_Entreprise_Prestation())->retrievePrestationById(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getIdEntreprise());
        } else {
            $prestation = new CommonPrestation();
            $prestation->setIdEntreprise($idEntreprise);
        }

        $prestation->setNumMarche($this->numMarche->Text);
        $prestation->setTypeProcedure($this->procedureType->selectedValue);
        $prestation->setObjet($this->objetMarche->Text);
        $prestation->setMontant($this->montant->Text);
        $prestation->setMaitreOuvrage($this->maitreOuvrage->Text);
        $prestation->setDateDebutExecution(Atexo_Util::frnDate2Iso($this->dateDebutExecution->Text));
        $prestation->setDateFinExecution(Atexo_Util::frnDate2Iso($this->dateFinExecution->Text));

        $prestation->save($connexionCom);

        $this->scriptAddAndClose->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refreshEntreprisePrestations').click();window.close();</script>";
    }

    public function displayPrestation($id)
    {
        $prestation = (new Atexo_Entreprise_Prestation())->retrievePrestationById($id, Atexo_CurrentUser::getIdEntreprise());
        if ($prestation) {
            $this->numMarche->Text = $prestation->getNumMarche();
            $this->procedureType->selectedValue = $prestation->getTypeProcedure();
            $this->objetMarche->Text = $prestation->getObjet();
            $this->montant->Text = $prestation->getMontant();
            $this->maitreOuvrage->Text = $prestation->getMaitreOuvrage();
            $this->dateDebutExecution->Text = Atexo_Util::iso2frnDate($prestation->getDateDebutExecution());
            $this->dateFinExecution->Text = Atexo_Util::iso2frnDate($prestation->getDateFinExecution());
        }
    }

    public function customizeForm()
    {
        if (!Atexo_Module::isEnabled('PradoValidateurFormatDate')) {
            $this->validateurDateDebutExecution->Enabled = false;
            $this->validateurDateFinExecution->Enabled = false;
        }
    }
}
