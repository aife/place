<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCG76PieceJointePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Permet de télécharger un fichier.
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class downloadCG76File extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $ref = Atexo_CurrentUser::getIdEntreprise();
        $idBlob = Atexo_Util::atexoHtmlEntities($_GET['idBlob']);
        $nomBlob = Atexo_Util::atexoHtmlEntities($_GET['nameBlob']);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCG76PieceJointePeer::IDPJ, $idBlob);
        $c->add(CommonCG76PieceJointePeer::IDENTREPRISE, $ref);
        $PieceJointe = CommonCG76PieceJointePeer::doSelectOne($c, $connexionCom);

        if ($PieceJointe) {
            $atexoBlob = new Atexo_Blob();
            $atexoBlob->sendBlobToClient($idBlob, $nomBlob, Atexo_Config::getParameter('COMMON_BLOB'));
        } else {
            echo Prado::localize('DEFINE_ERREUR_NO_HABILITE_FICHIER');
        }
        exit;
    }
}
