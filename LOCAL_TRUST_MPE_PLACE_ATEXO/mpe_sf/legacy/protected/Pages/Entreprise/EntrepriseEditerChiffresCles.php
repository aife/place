<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEntrepriseInfoExercice;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class EntrepriseEditerChiffresCles extends MpeTPage
{
    private $_pme;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        if (!$this->isPostBack) {
            $this->getListeExercices();
            $this->onYearSelectedChanged();
            $this->getListePme();

            $this->customizeForm();
        }
        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    public function entregistrerInfoExercice($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $idEntreprise = Atexo_CurrentUser::getIdEntreprise();

        $comExercEntreprise = CommonEntrepriseInfoExercice::retrieveInfoExercicebyIdEntrepriseAndAnnee($idEntreprise, $this->anneeCloture->getSelectedItem()->getText(), $connexionCom);
        $comExercEntreprise->setIdEntreprise($idEntreprise);
        $comExercEntreprise->setAnneeClotureExercice($this->anneeCloture->getSelectedItem()->getText());
        $comExercEntreprise->setDebutexerciceglob(Atexo_Util::frnDate2iso($this->debutExercice->Text));
        $comExercEntreprise->setFinexerciceglob(Atexo_Util::frnDate2iso($this->finExercice->Text));
        $comExercEntreprise->setVentesGlob($this->caVenteMarchandise->Text);
        $comExercEntreprise->setBiensGlob($this->biens->Text);
        $comExercEntreprise->setServicesGlob($this->services->Text);
        $comExercEntreprise->setTotalGlob($this->caTotal->Text);
        if ('1' == $this->pmeReglementEuro->SelectedValue) {
            $comExercEntreprise->setPme('');
        } elseif ('2' == $this->pmeReglementEuro->SelectedValue) {
            $comExercEntreprise->setPme('1');
        } elseif ('3' == $this->pmeReglementEuro->SelectedValue) {
            $comExercEntreprise->setPme('0');
        }
        $comExercEntreprise->setChiffreAffaires($this->ca->Text);
        $comExercEntreprise->setBesoinExcedentFinancement($this->besoinExcedent->Text);
        $comExercEntreprise->setCashFlow($this->cashFlow->Text);
        $comExercEntreprise->setCapaciteEndettement($this->capaciteEndettement->Text);

        $comExercEntreprise->setEffectifMoyen($this->effectifMoyen->Text);
        $comExercEntreprise->setEffectifEncadrement($this->dontEncadrement->Text);
        $comExercEntreprise->save($connexionCom);

        $this->script->Text = "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_refreshEntrepriseInformations').click();window.close();</script>";
    }

    /**
     * retourne les années des exercices.
     */
    public function getListeExercices()
    {
        $anneeAnneesClotureExercices = Atexo_Util::getListeTroisDernieresAnnees();
        $this->setViewState('anneeAnneesClotureExercices', $anneeAnneesClotureExercices);
        $this->anneeCloture->DataSource = $anneeAnneesClotureExercices;
        $this->anneeCloture->dataBind();
    }

    /**
     * retourne les possiblités d'une pme.
     */
    public function getListePme()
    {
        $anneeCloturesExercies = ['1' => (Prado::localize('NON_RENSEIGNE')), '2' => Prado::localize('DEFINE_OUI'), '3' => Prado::localize('DEFINE_NON')];
        $this->pmeReglementEuro->DataSource = $anneeCloturesExercies;
        $this->pmeReglementEuro->dataBind();
    }

    /**
     * Lors du choix de la liste des années.
     */
    public function onPmeChanged()
    {
        $this->_pme = $this->pmeReglementEuro->SelectedValue;
    }

    public function onYearSelectedChanged2()
    {
        $anneeExercice = $this->anneeCloture->SelectedValue;
        $this->getListePme();
        if ('' == $anneeExercice) {
            $anneeExercice = 1;
        }
        $this->setViewState('anneeExercice', $anneeExercice);
        //$idEntreprise = Atexo_CurrentUser::getIdEntreprise();
    }

    /**
     * Lors du choix de la liste des années.
     */
    public function onYearSelectedChanged()
    {
        $anneeExercice = $this->anneeCloture->SelectedValue;

        $this->getListePme();
        $annees = $this->getViewState('anneeAnneesClotureExercices');

        if ('' == $anneeExercice) {
            $anneeExercice = 1;
        }
        $anneeCloture = $annees[$anneeExercice];

        $this->setViewState('anneeExercice', $anneeExercice);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $idEntreprise = Atexo_CurrentUser::getIdEntreprise();
        if ($idEntreprise) {
            $EntrepriseInfoExercice = CommonEntrepriseInfoExercice::retrieveInfoExercicebyIdEntrepriseAndAnnee($idEntreprise, $anneeCloture, $connexionCom);
        } else {
            $EntrepriseInfoExercice = new CommonEntrepriseInfoExercice();
        }

        if (null != $EntrepriseInfoExercice->getDebutexerciceglob()) {
            $this->debutExercice->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice->getDebutexerciceglob());
        }
        if (null != $EntrepriseInfoExercice->getFinexerciceglob()) {
            $this->finExercice->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice->getFinexerciceglob());
        }
        if (null != $EntrepriseInfoExercice->getVentesGlob()) {
            $this->caVenteMarchandise->Text = $EntrepriseInfoExercice->getVentesGlob();
        }
        if (null != $EntrepriseInfoExercice->getBiensGlob()) {
            $this->biens->Text = $EntrepriseInfoExercice->getBiensGlob();
        }
        if (null != $EntrepriseInfoExercice->getServicesGlob()) {
            $this->services->Text = $EntrepriseInfoExercice->getServicesGlob();
        }
        if (null != $EntrepriseInfoExercice->getTotalGlob()) {
            $this->caTotal->Text = $EntrepriseInfoExercice->getTotalGlob();
        }
        if ('' == $EntrepriseInfoExercice->getPme()) {
            $this->pmeReglementEuro->SelectedValue = '1';
        } elseif ('1' == $EntrepriseInfoExercice->getPme()) {
            $this->pmeReglementEuro->SelectedValue = '2';
        } elseif ('0' == $EntrepriseInfoExercice->getPme()) {
            $this->pmeReglementEuro->SelectedValue = '3';
        }

        if (null != $EntrepriseInfoExercice->getChiffreAffaires()) {
            $this->ca->Text = $EntrepriseInfoExercice->getChiffreAffaires();
        }
        if (null != $EntrepriseInfoExercice->getBesoinExcedentFinancement()) {
            $this->besoinExcedent->Text = $EntrepriseInfoExercice->getBesoinExcedentFinancement();
        }
        if (null != $EntrepriseInfoExercice->getCashFlow()) {
            $this->cashFlow->Text = $EntrepriseInfoExercice->getCashFlow();
        }
        if (null != $EntrepriseInfoExercice->getCapaciteEndettement()) {
            $this->capaciteEndettement->Text = $EntrepriseInfoExercice->getCapaciteEndettement();
        }
        if (null != $EntrepriseInfoExercice->getEffectifMoyen()) {
            $this->effectifMoyen->Text = $EntrepriseInfoExercice->getEffectifMoyen();
        }
        if (null != $EntrepriseInfoExercice->getEffectifEncadrement()) {
            $this->dontEncadrement->Text = $EntrepriseInfoExercice->getEffectifEncadrement();
        }
    }

    /***
     * Afficher les champs nécessaire pour les modules activés
     */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseDonneesFinancieres')) {
            $this->panelDonneesFinancieres->setDisplay('Dynamic');
        } else {
            $this->panelDonneesFinancieres->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseChiffreAffaireProductionBiensServices')) {
            $this->panelVenteMarchandise->setDisplay('Dynamic');
            $this->panelProductionVendu->setDisplay('Dynamic');
            $this->panelQuestion->setDisplay('Dynamic');
        } else {
            $this->panelVenteMarchandise->setDisplay('None');
            $this->panelProductionVendu->setDisplay('None');
            $this->panelQuestion->setDisplay('None');
        }
    }
}
