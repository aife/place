<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonTBourseCotraitance;
use Application\Propel\Mpe\CommonTBourseCotraitanceQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_BourseCotraitance;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Consultation;
use Exception;
use Laminas\Http\Client;
use Laminas\Http\Request;

class EntrepriseInscriptionBourseCotraitance extends MpeTPage
{
    public string $pageTitle = 'TITRE';

    public function onInit($param)
    {
        $this->pageTitle = 'TITRE_INSCRIPTION_BOURSE_COTRAITANCE';
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!Atexo_Module::isEnabled('BourseCotraitance')) {
            $this->response->redirect('?page=Entreprise.EntrepriseHome');
        }
        if (!$this->isPostBack && !$this->isCallBack) {
            if (Atexo_CurrentUser::isConnected() && Atexo_CurrentUser::getIdInscrit()) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $monInscriptionBourse = CommonTBourseCotraitanceQuery::create()->filterByReferenceConsultation($_GET['id'])->filterByIdEntreprise(Atexo_CurrentUser::getIdEntreprise())->findOne($connexion);
                if ($monInscriptionBourse instanceof CommonTBourseCotraitance) {
                    if (isset($_GET['inscription'])) {
                        $url = Atexo_MpeSf::getUrlDetailsConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), 'orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&inscription');

                        $this->response->redirect($url);
                    }
                    $this->setViewState('NewInscription', false);
                    self::fillInfosBourse($monInscriptionBourse);
                } else {
                    $this->setViewState('NewInscription', true);
                    self::fillInfosInscrit();
                }
            } else {
                $inscription = '';
                if ($_GET['orgAcronyme']) {
                    $inscription = '&inscription';
                }
                $url = urlencode('?page=Entreprise.EntrepriseInscriptionBourseCotraitance'.
                                 '&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).
                                 '&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).$inscription);
                $this->response->redirect('?page=Entreprise.EntrepriseHome&goto='.$url);
            }
        }
    }

    /**
     * Permet de preremplir le formulaire d'inscription a la bourse depuis les donnees de l'inscrit en cas de nouvelle inscription.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillInfosInscrit()
    {
        //$entreprise = CommonEntrepriseQuery::create()->filterById(Atexo_CurrentUser::getIdEntreprise())->findOne();
        $inscrit = CommonInscritQuery::create()->filterById(Atexo_CurrentUser::getId())->findOne();
        if ($inscrit instanceof CommonInscrit) {
            $this->nomContactGroupement->Text = $inscrit->getNom();
            $this->prenomContactGroupement->Text = $inscrit->getPrenom();
            $this->email->Text = $inscrit->getEmail();
            $this->tel->Text = $inscrit->getTelephone();
            //Chargement des informations a partir de l'etablissement
            $etablissement = $inscrit->getCommonTEtablissement();
            if ($etablissement instanceof CommonTEtablissement) {
                $this->addresseContactGroupement->Text = $etablissement->getAdresse();
                $this->addressSuiteContactGroupement->Text = $etablissement->getAdresse2();
                $this->cpContactGroupement->Text = $etablissement->getCodePostal();
                $this->villeContactGroupement->Text = $etablissement->getVille();
            }
            //Chargement a partir de l'entreprise
            $entreprise = $inscrit->getCommonEntreprise();
            if ($entreprise instanceof Entreprise) {
                $this->paysContactGroupement->Text = $entreprise->getPaysadresse();
            }
        }
    }

    /**
     * Permet d'annuler l inscription à la bourse à la cotraitance et rediriger vers details de la consultation.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function annuler()
    {
        $this->response->redirect(Atexo_MpeSf::getUrlDetailsConsultation($_GET['id'], '&orgAcronyme='.$_GET['orgAcronyme']));
    }

    /**
     * Permet de s'inscrire la bourse à la cotraitance d'une Consultation.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function Enregistrer()
    {
        $idEntreprise = null;
        $idEtablissement = null;
        $referenceConsultation = null;
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $referenceConsultation = $_GET['id'];
            $idEntreprise = Atexo_CurrentUser::getIdEntreprise();
            $idEtablissement = Atexo_CurrentUser::getIdEtablissement();
            $bourseCotraitanceObject = CommonTBourseCotraitanceQuery::create()->filterByReferenceConsultation($referenceConsultation)->filterByIdEntreprise($idEntreprise)->findOne($connexion);
            if (!($bourseCotraitanceObject instanceof CommonTBourseCotraitance)) {
                $bourseCotraitanceObject = new CommonTBourseCotraitance();
            }
            $bourseCotraitanceObject->setReferenceConsultation($referenceConsultation);
            $bourseCotraitanceObject->setIdEntreprise((int) $idEntreprise);
            $bourseCotraitanceObject->setIdEtablissementInscrite((int) $idEtablissement);
            $mandataire = $solidaire = $conjoint = $sousTraitant = $clauseSociale = $enrepriseAdapte = '0';
            if (true == $this->collaborateurMandataire->checked) {
                $mandataire = '1';
            }
            if (true == $this->collaborateurCotraitantSolidaire->checked) {
                $solidaire = '1';
            }
            if (true == $this->collaborateurCotraitantConjoint->checked) {
                $conjoint = '1';
            }
            if (true == $this->collaborateurSoustraitant->checked) {
                $sousTraitant = '1';
            }
            if (true == $this->clauseSocialeOui->checked) {
                $clauseSociale = '1';
            }
            if (true == $this->clauseEAOui->checked) {
                $enrepriseAdapte = '1';
            }
            $bourseCotraitanceObject->setMandataireGroupement($mandataire);
            $bourseCotraitanceObject->setCotraitantSolidaire($solidaire);
            $bourseCotraitanceObject->setCotraitantConjoint($conjoint);
            $bourseCotraitanceObject->setSousTraitant($sousTraitant);
            $bourseCotraitanceObject->setClauseSocial($clauseSociale);
            $bourseCotraitanceObject->setEntrepriseAdapte($enrepriseAdapte);
            $bourseCotraitanceObject->setDescTypeCotraitanceRecherche($this->descTypeCotraitanceRecherche->Text);
            $bourseCotraitanceObject->setDescMonApportMarche($this->descMonApportMarche->Text);
            $bourseCotraitanceObject->setNomInscrit($this->nomContactGroupement->Text);
            $bourseCotraitanceObject->setPrenomInscrit($this->prenomContactGroupement->Text);
            $bourseCotraitanceObject->setAdresseInscrit($this->addresseContactGroupement->Text);
            $bourseCotraitanceObject->setAdresse2Incsrit($this->addressSuiteContactGroupement->Text);
            $bourseCotraitanceObject->setCpInscrit($this->cpContactGroupement->Text);
            $bourseCotraitanceObject->setVilleInscrit($this->villeContactGroupement->Text);
            $bourseCotraitanceObject->setPaysInscrit($this->paysContactGroupement->Text);
            $bourseCotraitanceObject->setFonctionInscrit($this->fonction->Text);
            $bourseCotraitanceObject->setEmailInscrit($this->email->Text);
            $bourseCotraitanceObject->setTelMobileInscrit($this->tel->Text);
            $bourseCotraitanceObject->save($connexion);
            self::addCoordonnees($bourseCotraitanceObject, $connexion);
            Atexo_BourseCotraitance::initCachedCasGroupemen($referenceConsultation, $idEntreprise);
            $url = Atexo_MpeSf::getUrlDetailsConsultation($referenceConsultation, 'orgAcronyme='.$_GET['orgAcronyme'].'&action=succes');
            $this->response->redirect($url);
        } catch (Exception $e) {
            $msg = <<<MSG
Erreur lors de l'inscription de la bourse à la cotraitance.
idEntreprise = $idEntreprise
idEtablissement = $idEtablissement
referenceCons = $referenceConsultation
MSG;
            self::logErreur($msg.PHP_EOL.$e->getMessage());
        }
    }

    /**
     * Permet de preremplir le formulaire d'inscription a la bourse depuis un objet CommonTBourseCotraitance en cas de MAJ.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillInfosBourse($monInscriptionBourse)
    {
        $this->nomContactGroupement->Text = $monInscriptionBourse->getNomInscrit();
        $this->prenomContactGroupement->Text = $monInscriptionBourse->getPrenomInscrit();
        $this->addresseContactGroupement->Text = $monInscriptionBourse->getAdresseInscrit();
        $this->addressSuiteContactGroupement->Text = $monInscriptionBourse->getAdresse2Incsrit();
        $this->cpContactGroupement->Text = $monInscriptionBourse->getCpInscrit();
        $this->villeContactGroupement->Text = $monInscriptionBourse->getVilleInscrit();
        $this->paysContactGroupement->Text = $monInscriptionBourse->getPaysInscrit();
        $this->email->Text = $monInscriptionBourse->getEmailInscrit();
        $this->tel->Text = $monInscriptionBourse->getTelMobileInscrit();
        $this->fonction->Text = $monInscriptionBourse->getFonctionInscrit();
        $this->descTypeCotraitanceRecherche->Text = $monInscriptionBourse->getDescTypeCotraitanceRecherche();
        $this->descMonApportMarche->Text = $monInscriptionBourse->getDescMonApportMarche();
        $mandataire = $solidaire = $conjoint = $sousTraitant = false;
        if ('1' == $monInscriptionBourse->getMandataireGroupement($mandataire)) {
            $mandataire = true;
        }
        if ('1' == $monInscriptionBourse->getCotraitantSolidaire($solidaire)) {
            $solidaire = true;
        }
        if ('1' == $monInscriptionBourse->getCotraitantConjoint()) {
            $conjoint = true;
        }
        if ('1' == $monInscriptionBourse->getSousTraitant()) {
            $sousTraitant = true;
        }
        $this->collaborateurCotraitantConjoint->checked = $conjoint;
        $this->collaborateurCotraitantSolidaire->checked = $solidaire;
        $this->collaborateurMandataire->checked = $mandataire;
        $this->collaborateurSoustraitant->checked = $sousTraitant;
        if ('1' == $monInscriptionBourse->getClauseSocial()) {
            $this->clauseSocialeOui->checked = true;
        }
        if ('1' == $monInscriptionBourse->getEntrepriseAdapte()) {
            $this->clauseEAOui->checked = true;
        }
    }

    /**
     * Permet de construire l'url du ws.
     *
     * @param string $city       : ville
     * @param string $postalcode : code postal
     * @param string $country    : pays
     *
     * @return string $url
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getUrl($country = null, $city = null, $postalcode = null)
    {
        $url = Atexo_Config::getParameter('GEOLOC_SEARCH_URL');
        $urlParam = [];
        if (!empty($city)) {
            $urlParam[] = 'city='.urlencode(utf8_encode(preg_replace('#cedex#i', '', $city)));
        }
        if (!empty($country)) {
            $urlParam[] = 'country='.urlencode(utf8_encode($country));
        }
        if (!empty($postalcode)) {
            $urlParam[] = 'postalcode='.urlencode(utf8_encode($postalcode));
        }
        $urlParam[] = 'limit=1';
        $urlParam[] = 'format=json';
        $url .= '?'.implode('&', $urlParam);

        return $url;
    }

    /**
     * Permet de recuperer les coordonnees geolocalisation de l'inscrit.
     *
     * @param string $url : url du ws
     *
     * @return string
     *
     * @author SOUID AHMED Ayoub <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function recupererCoordonneesGeoViaNominatim($url)
    {
        try {
            $client = new Client();
            $client->setUri($url);
            $client->setMethod(Request::METHOD_GET);
            $resultat = $client->send();

            return $resultat;
        } catch (Exception $e) {
            self::logErreur("\t Erreur lors du retour du ws NOMINATIM ,url = ".$url.$e->getMessage());
        }
    }

    /**
     * Permet de valider le retour du ws.
     *
     * @param string $jsonResultat
     *
     * @return bool : true si valide, false sinon
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function retourWsValide($jsonResultat)
    {
        try {
            $resultat = json_decode($jsonResultat, null, 512, JSON_THROW_ON_ERROR);
            if (!empty($resultat) && is_object($resultat[0]) && $resultat[0]->lat && $resultat[0]->lon) {
                self::logInfos("\t Validation du retour du ws: OK");

                return true;
            }
            self::logInfos("\t Validation du retour du ws: KO");

            return false;
        } catch (Exception $e) {
            self::logErreur('Erreur lors de la validation du retour du ws: '.$e->getMessage());
        }
    }

    /**
     * Permet de parser le retour du ws NOMINATIM pour recuperer la latitude et la longitude.
     *
     * @param string $jsonResultat : le retour du ws en encode en json
     *
     * @return array : tableau unidimensionnel contenant la latitude et la longitude
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function parserResultat($jsonResultat)
    {
        try {
            $resultat = json_decode($jsonResultat, null, 512, JSON_THROW_ON_ERROR);
            $coordonneesGeoloc = [];
            if (!empty($resultat) && is_object($resultat[0])) {
                $coordonneesGeoloc['lat'] = $resultat[0]->lat;
                $coordonneesGeoloc['long'] = $resultat[0]->lon;
                self::logInfos("\t Parse des donnees effectue avec succes ");
            }

            return $coordonneesGeoloc;
        } catch (Exception $e) {
            self::logErreur('Erreur lors du parse du json: '.$e->getMessage());
        }
    }

    /**
     * Permet d'ajouter les coordoonees de l'inscrit a la bourse a la cotraitance.
     *
     * @param pdo $connexion
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function addCoordonnees(CommonTBourseCotraitance $bourseCotraitanceObject, $connexion)
    {
        try {
            $url = self::getUrl($bourseCotraitanceObject->getPaysInscrit(), $bourseCotraitanceObject->getVilleInscrit(), $bourseCotraitanceObject->getCpInscrit());
            $jsonCoordonneesGeoEtab = self::recupererCoordonneesGeoViaNominatim($url);
            if (!self::retourWsValide($jsonCoordonneesGeoEtab)) {
                $url = self::getUrl($bourseCotraitanceObject->getPaysInscrit(), $bourseCotraitanceObject->getVilleInscrit());
                $jsonCoordonneesGeoEtab = self::recupererCoordonneesGeoViaNominatim($url);
            }
            $coordonneesGeoEtab = self::parserResultat($jsonCoordonneesGeoEtab);
            if ($coordonneesGeoEtab['lat'] && $coordonneesGeoEtab['long']) {
                $bourseCotraitanceObject->setLat($coordonneesGeoEtab['lat']);
                $bourseCotraitanceObject->setLong($coordonneesGeoEtab['long']);
                $bourseCotraitanceObject->setMajLongLat(date('Y-m-d H:i:s'));
                $bourseCotraitanceObject->save($connexion);
            }
        } catch (Exception $e) {
            $msg = "Erreur lors de l'ajout des coordonnees à la bourse à la cotraitance.".PHP_EOL.
                    'idEntreprise = '.$bourseCotraitanceObject->getIdEntreprise().PHP_EOL.
                    'idEtablissement = '.$bourseCotraitanceObject->getIdEtablissementInscrite().PHP_EOL.
                    'referenceCons = '.$bourseCotraitanceObject->getReferenceConsultation();
            self::logErreur($msg.PHP_EOL.$e->getMessage());
        }
    }

    /**
     * Permet de logger les infos.
     *
     * @param string $infos
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function logInfos($infos)
    {
        $logger = Atexo_LoggerManager::getLogger();
        $logger->info($infos);
    }

    /**
     * Permet de logger les erreurs.
     *
     * @param string $erreur
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function logErreur($erreur)
    {
        $logger = Atexo_LoggerManager::getLogger();
        $logger->error($erreur);
    }
}
