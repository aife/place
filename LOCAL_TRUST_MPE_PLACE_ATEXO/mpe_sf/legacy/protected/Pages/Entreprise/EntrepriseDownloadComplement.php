<?php

namespace Application\Pages\Entreprise;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Complements;
use Prado\Prado;

/**
 * Permet de télécharger le DCE.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDownloadComplement extends DownloadFile
{
    private $_reference;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $complement = '';
        $this->_reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        if ($consultation instanceof CommonConsultation) {
            $complement = (new Atexo_Consultation_Complements())->getComplement($this->_reference, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            if ($complement) {
                $this->_idFichier = $complement->getComplement();
                $this->_nomFichier = $complement->getNomFichier();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
                exit;
            } else {
                $this->errorPart->setVisible(true);
                $this->messageErreur->setVisible(true);
                $this->messageErreur->setMessage(Prado::localize('DOCUMENT_NON_DISPONIBLE'));
            }
        } else {
            $this->errorPart->setVisible(true);
            $this->messageErreur->setVisible(true);
            $this->messageErreur->setMessage(Prado::localize('DOCUMENT_NON_DISPONIBLE'));
        }
    }
}
