<?php

namespace Application\Pages\Entreprise;

use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet de télécharger les Pj
 * classe EntrepriseDownloadPjEchange.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDownloadPjEchange extends DownloadFile
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $idPj = Atexo_Util::atexoHtmlEntities($_GET['idPj']);
        $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $uid = Atexo_Util::atexoHtmlEntities($_GET['uid']);
        $idEchange = Atexo_Util::atexoHtmlEntities($_GET['idMsg']);
        $pj = Atexo_Message::getPjByIdPiece($idPj, $org, $idEchange);
        $echangeDestinataire = Atexo_Message::getEchangeDestinataireByUid($uid, $org);

        if ($pj && $echangeDestinataire) {
            if ($echangeDestinataire->getIdEchange() == $pj->getIdMessage()) {
                $this->_idFichier = $idPj;
                $this->_nomFichier = $pj->getNomFichier();
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, $org);
            }
        }
    }
}
