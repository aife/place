<?php

namespace Application\Pages\Entreprise;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonRG;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Prado\Prado;

/**
 * Permet de télécharger le DCE.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */ class EntrepriseDownloadReglement extends DownloadFile
{
    private $_reference;

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $this->_reference = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        if ($consultation instanceof CommonConsultation) {
            $rg = (new Atexo_Consultation_Rg())->getReglement($this->_reference, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            if ($rg instanceof CommonRG) {
                $this->_idFichier = $rg->getRg();
                $this->_nomFichier = $rg->getNomFichier();
                $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION8', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
                $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
                $arrayDonnees = [];
                $arrayDonnees['ref'] = $this->_reference;
                $arrayDonnees['org'] = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                $arrayDonnees['IdDescritpion'] = $IdDesciption;
                $arrayDonnees['afficher'] = true;
                $arrayDonnees['logApplet'] = '';
                $arrayDonnees['description'] = $description;

                Atexo_InscritOperationsTracker::trackingOperations(
                    Atexo_CurrentUser::getIdInscrit(),
                    Atexo_CurrentUser::getIdEntreprise(),
                    $_SERVER['REMOTE_ADDR'],
                    date('Y-m-d'),
                    $dateDebAction,
                    substr($_SERVER['QUERY_STRING'], 0),
                    '-',
                    date('Y-m-d H:i:s'),
                    $arrayDonnees
                );
                $this->downloadFiles($this->_idFichier, $this->_nomFichier, Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            }
        } else {
            $this->errorPart->setVisible(true);
            $this->messageErreur->setVisible(true);
            $this->messageErreur->setMessage(Prado::localize('TEXT_AUCUNE_CONSULTATION'));
        }
    }
}
