<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Prado\Prado;

class ConfirmationCreationInscrit extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $inscritCreate = null;
        if ($_GET['uid']) {
            $inscritCreate = (new Atexo_Entreprise_Inscrit())->retrieveInscritByUid(Atexo_Util::atexoHtmlEntities($_GET['uid']));
        }
        if ($inscritCreate) {
            $message = Prado::localize('MESSAGE_CONFIRMATION_INSCRIPTION_INSCRIT_1').'<br/>';
            $message .= str_replace('[__EMAIL__]', "'".$inscritCreate->getEmail()."'", Prado::localize('MESSAGE_CONFIRMATION_INSCRIPTION_INSCRIT_2'));
            $this->messageConfirmation->setVisible(true);
            $this->messageConfirmation->setMessage($message);
        }
    }
}
