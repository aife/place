<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Controls\TableauAffichageSocietesExclus;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_SocietesExclues;

/**
 * Classe de recherches des sociétés exclues.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseRechercherSocietesExclues extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($_GET['search']) {
            $this->tableauAffichageSocietesExclues->visible = false;
            $this->tableauRechercheSocietesExclues->visible = true;
            $this->labelTextResultat->visible = false;
            $this->labelTextRecherche->visible = true;
        }
    }

    public function lancerRechercheWithCritere($critere)
    {
        (new TableauAffichageSocietesExclus())->fillRepeaterWithSearchData($critere);
    }

    public function action($sender, $param)
    {
        if ('Delete' === $param->CommandName) {
            (new Atexo_SocietesExclues())->deleteSocieteExclues($param->CommandParameter);
        }
    }

    public function refreshRepeater($sender, $param)
    {
        $societesExclues = (new TableauAffichageSocietesExclus())->fillRepeaterWithSearchData($this->Page->getViewState('critere'), false, null);
        $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->DataSource = $societesExclues;
        $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->DataBind();
        $this->Page->tableauAffichageSocietesExclues->panelRepeater->render($param->NewWriter);
    }
}
