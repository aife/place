<?php

namespace Application\Pages\Entreprise;

use App\Service\ConfigurationOrganismeService;
use App\Service\Crypto\WebServicesCrypto;
use App\Service\Routing\RouteBuilderInterface;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Service;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class SignDocument extends MpeTPage
{
    public function onInit($param)
    {
        // Redirect to SF Page, we do not use this Prado Page anymore
        $sfPath = Atexo_Util::getSfService(RouteBuilderInterface::class)->getRoute('entreprise_signer_document');
        $this->response->redirect($sfPath);


        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);

        /** @var ConfigurationOrganismeService $service */
        $service = Atexo_Util::getSfService(ConfigurationOrganismeService::class);
        if(!$service->isModuleDisabledForOneOrganisme('activerMonAssistantMarchesPublics')){
            MessageStatusCheckerUtil::initEntryPoints('assistance-launch');
            MessageStatusCheckerUtil::initEntryPoints('assistance-status-checker');
        }
    }

    public function sendSignature($sender, $param)
    {
        $file = null;
        $signature = $this->signature->Value;
        $error = false;
        if ($signature) {
            $file = Atexo_Config::getParameter('COMMON_TMP').'/signature_document'.session_name().session_id().time();
            if (!Atexo_Util::write_file($file, $signature)) {
                $error = true;
            }
        } else {
            $error = true;
        }

        if ($error) {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('ERREUR_LORS_DE_LA_SIGNATURE'));
        } else {
            $this->panelMessageErreur->setVisible(false);
            $cheminFichierSigner = $this->cheminDocument->Value;
            $cheminEtNomFichier = explode('\\', $cheminFichierSigner);
            $nomFichierSignature = $cheminEtNomFichier[count($cheminEtNomFichier) - 1];
            $fp = fopen($file, 'r');
            $data = fread($fp, filesize($file));
            fclose($fp);
            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: private', false);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.$nomFichierSignature.'.p7s'.'";');
            header('Content-Transfer-Encoding: binary');
            echo $data;
            exit;
        }
    }

    /**
     * Map Offres to dechiffrement.
     *
     * @param array $offres
     * @param int   $idAgent
     * @param array $params
     *
     * @return Dehiffrement
     */
    public function getJnlpToSignDocument()
    {
        $logger = Atexo_LoggerManager::getLogger('crypto');

        $service = new Atexo_Crypto_Service(
            Atexo_Config::getParameter('URL_CRYPTO'),
            $logger,
            Atexo_Config::getParameter('ENABLE_SIGNATURE_SERVER_V2'),
            null,
            Atexo_Util::getSfService(WebServicesCrypto::class)
        );

        try {
            return $service->jnlpSignature(Atexo_Config::getParameter('URL_CRYPTO'));
        } catch (AtexoCrypto\Exception\ServiceException $e) {
            $logger->error("Erreur Lors d'appel au ws de dechiffrement".$e->errorCode.' - '.$e->etatObjet.' - '.$e->message);
        }

        return false;
    }

    public function lancerJWS()
    {
        $resultat = self::getJnlpToSignDocument();
        if ($resultat) {
            $this->panelMessageErreur->setDisplay('None');
            header('Content-Length: '.strlen($resultat));
            header('Content-disposition: attachment; filename="'.Atexo_Config::getParameter('NOM_FICHIER_WEBSTART_SIGNATURE').'"');
            header('Content-type: application/x-java-jnlp-file');
            echo $resultat;
            exit;
        } else {
            $this->panelMessageErreur->setDisplay('Dynamic');
            $this->panelMessageErreur->setMessage(Prado::localize('SERVICE_CRYPTO_ACTUELLEMENT_INDISPONIBLE'));
        }
    }
}
