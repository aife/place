<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;

/**
 * Affichage de la liste des certificats valides par le gouvernement
 * en ce basant sur le fichier de reference gouvernemental :
 * https://references.modernisation.gouv.fr/sites/default/files/TSL-FR.xml.
 *
 * @since 4.6.0
 *
 * @author Gregory CHEVRET<gregory.chevret@atexo.com>
 * @author Loubna  EZZIANI<loubna.ezziani@atexo.com>
 * @copyright ATEXO 2014
 */
class ListeAcRGS extends MpeTPage
{
    public function onLoad($param)
    {
        $url = Atexo_Config::getParameter('URL_LT_MPE_SF').'/entreprise/footer/info-acrgs';
        $this->response->redirect($url);
    }
}
