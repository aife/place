<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;

/*
 * Created on 8 juin 2012
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */

class NosPartenaires extends MpeTPage
{
    public function onLoad($param)
    {
        $url = Atexo_Config::getParameter('URL_LT_MPE_SF').'/entreprise/footer/nos-partenaires';
        $this->response->redirect($url);
    }
}
