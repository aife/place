<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Organismes;
use PDO;
use Prado\Prado;

/**
 * Classe EntrepriseVisualiserEntitesAchatsRecherche.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseVisualiserEntitesAchatsRecherche extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->Page->IsPostBack) {
            $this->remplirListEntitePublic();
        }
    }

    public function remplirListEntitePublic()
    {
        $active = true;
        $affichageEntite = true;
        $ArrayOrganismes = (new Atexo_Organismes())->retrieveActiveOrganismes(false, $active, $affichageEntite);
        $this->entitePublique->dataSource = $ArrayOrganismes;
        $this->entitePublique->DataBind();
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');
        $this->entitePubliqueChoix->dataSource = $ArrayOrganismes;
        $this->entitePubliqueChoix->DataBind();
    }

    public function remplissageChamps()
    {
        $this->recherche->setVisible(true);
        $this->resultat->setVisible(false);
        $this->javascript->Text = "<script>showHideLayer('ongletLayer2',ctl0_CONTENU_PAGE_onglet2,2);</script>";
    }

    public function retour()
    {
        $this->recherche->setVisible(true);
        $this->resultat->setVisible(false);
        $this->onEffaceClick();
        $this->javascript->Text = "<script>showHideLayer('ongletLayer2',ctl0_CONTENU_PAGE_onglet2,2);</script>";
    }

    public function retourDetail()
    {
        $this->onRechercheClick();
        $this->detailEntite->setVisible(false);
    }

    public function onClickSearch()
    {
        $this->response->redirect('index.php?page=Entreprise.EntrepriseVisualiserEntiteAchatsArborescence&acrOrg='.$this->entitePublique->getSelectedValue());
    }

    public function onEffaceClick()
    {
        $this->entitePubliqueChoix->SelectedValue = 0;
        $this->entiteAchat->Text = '';
        $this->adresse1->Text = '';
        $this->cp->Text = '';
        $this->ville->Text = '';
        $this->pays->Text = '';
        $this->keywordSearch->Text = '';
        $this->javascript->Text = "<script>showHideLayer('ongletLayer2',ctl0_CONTENU_PAGE_onglet2,2);</script>";
    }

    public function onRechercheClick()
    {
        $resultatRecherche = [];
        $params = [];
        $sql = "SELECT id, sigle, libelle, ville, pays  
            FROM Service  WHERE 1=1
                            and affichage_service = '1'
                            AND organisme='".$this->entitePubliqueChoix->SelectedValue."'";

        if ('' != $this->keywordSearch->Text) {
            $sql .= 'AND (libelle LIKE :champlibelle '.
                    'OR adresse LIKE :champadresse '.
                    'OR cp LIKE :champcp '.
                    'OR ville LIKE :champville '.
                    'OR pays  LIKE :champpays)';
            $params = $params + [':champlibelle' => addslashes('%'.htmlentities($this->keywordSearch->Text).'%')];
            $params = $params + [':champadresse' => addslashes('%'.htmlentities($this->keywordSearch->Text).'%')];
            $params = $params + [':champcp' => addslashes(htmlentities($this->keywordSearch->Text).'%')];
            $params = $params + [':champville' => addslashes('%'.htmlentities($this->keywordSearch->Text).'%')];
            $params = $params + [':champpays' => addslashes('%'.htmlentities($this->keywordSearch->Text).'%')];
        }

        if ('' != $this->entiteAchat->Text) {
            $sql .= 'AND libelle LIKE :libelle ';
            $params = $params + [':libelle' => addslashes('%'.htmlentities($this->entiteAchat->Text).'%')];
        }
        if ('' != $this->adresse1->Text) {
            $sql .= 'AND adresse LIKE :adresse ';
            $params = $params + [':adresse' => addslashes('%'.htmlentities($this->adresse1->Text).'%')];
        }
        if ('' != $this->cp->Text) {
            $sql .= 'AND cp LIKE :cp ';
            $params = $params + [':cp' => $this->cp->Text.'%'];
        }
        if ('' != $this->ville->Text) {
            $sql .= 'AND ville LIKE :ville ';
            $params = $params + [':ville' => addslashes('%'.htmlentities($this->ville->Text).'%')];
        }
        if ('' != $this->pays->Text) {
            $sql .= 'AND pays LIKE :pays ';
            $params = $params + [':pays' => addslashes('%'.htmlentities($this->pays->Text).'%')];
        }

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute($params);

        $organismeObject = Atexo_Organismes::retrieveOrganismeByAcronyme($this->entitePubliqueChoix->SelectedValue);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $resultatRecherche[] = ['id' => $row['id'], 'sigle' => $row['sigle'], 'libelle' => $row['libelle'],
                                        'ville' => $row['ville'], 'pays' => $row['pays'], 'acronyme' => $this->entitePubliqueChoix->SelectedValue,
                                        'sigleorg' => $organismeObject->getSigle(), 'denominationorg' => $organismeObject->getDenominationOrg(), ];
        }

        $this->EntrepriseResultSearchEntity->remplirTableauResultat($resultatRecherche);
        $this->recherche->setVisible(false);
        $this->resultat->setVisible(true);
    }
}
