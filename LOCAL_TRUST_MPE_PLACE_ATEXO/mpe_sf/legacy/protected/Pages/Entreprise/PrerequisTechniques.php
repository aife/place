<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;

class PrerequisTechniques extends MpeTPage
{
    public string $pageTitle = 'TITRE';

    public function onLoad($param)
    {
        $url = Atexo_Config::getParameter('URL_LT_MPE_SF').'/entreprise/footer/prerequis-techniques';
        $this->response->redirect($url);
    }

    public function onInit($param)
    {
        $this->pageTitle = 'TITRE_PREREQUIS_TECHNIQUES';
        $dateDebAction = date('Y-m-d H:i:s');
        $this->Master->setCalledFrom(Atexo_Util::atexoHtmlEntities($_GET['calledFrom']));
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
        $details = 'Pré-requis techniques';
        $idInscrit = Atexo_CurrentUser::getIdInscrit();
        $idEntreprise = Atexo_CurrentUser::getIdEntreprise();
        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date('Y-m-d');
        $action = substr($_SERVER['QUERY_STRING'], 0);
        $dateFinAction = date('Y-m-d H:i:s');
        Atexo_InscritOperationsTracker::trackingOperations($idInscrit, $idEntreprise, $ip, $date, $dateDebAction, $action, $details, $dateFinAction);
    }

    public function getModeServeurCrypto()
    {
        if (!Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')
            || ($this->isAgent() && Atexo_CurrentUser::getId() && Atexo_Module::isEnabled('ModeApplet'))
        ) {
            return 0;
        } else {
            return 1;
        }
    }
}
