<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Pages\Commun\DownloadFile;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use AtexoPdf\PdfGeneratorClient;

/*
 * Created on 21 nov. 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class GetMailFormatPdf extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if ($_GET['idAlerte']) {
            $fichier = Atexo_Config::getParameter('CHEMIN_REP_SENT_MAIL_ALERTE').'/'.Atexo_Util::atexoHtmlEntities($_GET['idAlerte']);
            if (Atexo_Util::isSafeFileName($fichier) && is_file($fichier)) {
                $pdfContent = (new PdfGeneratorClient())->genererPdf($fichier);
                @unlink($fichier);
                DownloadFile::downloadFileContent(Atexo_Config::getParameter('NOM_FICHIER_MAIL_ALERTE_PDF').'.pdf', $pdfContent);
            }
        }
    }
}
