<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Exception;
use Prado\Prado;

class FormulaireActeDEngagement extends MpeTPage
{
    public bool $label = false;
    public $array_values = [];

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');
        //Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            $this->LoadData();
            $this->remplirListaeCase();
        } else {
            //$this->array_values = $_POST;
        }
    }

    public function setValues(array $values)
    {
        $this->array_values = $values;
    }

    public function isAConsulter()
    {
        $this->label = true;
    }

    public function remplirListaeCase()
    {
        $arrayData = [
                            '0' => ['value' => 'aeCase1',
                                        'text' => Prado::localize('PARAGRAPHE_ENGAGEMENT_2'),
                                        'enabled' => 'true',
                                        'checked' => 'false',
                                        ],
                            '1' => ['value' => 'aeCase2',
                                        'text' => Prado::localize('PARAGRAPHE_ENGAGEMENT_3'),
                                        'enabled' => 'true',
                                        'checked' => 'false',
                                        ],
        ];
        $this->aeCase->setListData($arrayData);
    }

    public function AddEchange()
    {
        $post = $_POST;
        unset($post['PRADO_PAGESTATE']);
        unset($post['PRADO_POSTBACK_TARGET']);
        unset($post['PRADO_POSTBACK_PARAMETER']);
        //$post = array_splice ($post, 0, $post->length-1);
        $post2 = [];
        foreach ($post as $key => $value) {
            $post2[str_replace('$', '_', $key)] = $value;
        }
        $this->array_values = $post2;
        $post2 = serialize($post2);
        Atexo_CurrentUser::writeToSession('post', $post2);
    }

    public function LoadData()
    {
        $post = Atexo_CurrentUser::readFromSession('post');

        if (isset($post)) {
            $_post = unserialize($post);
            $this->array_values = $_post;
        }
    }
}
