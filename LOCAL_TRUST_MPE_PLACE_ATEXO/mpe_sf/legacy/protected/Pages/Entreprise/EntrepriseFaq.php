<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonFaqEntreprise;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_FoireAuxQuestions;

/**
 * Classe de "Foire aux questions".
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseFaq extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        if (Atexo_Config::getParameter('UTILISER_FAQ') || Atexo_Config::getParameter('UTILISER_UTAH')) {
			$this->response->redirect('/entreprise');
        }
        $this->fillRepeaters();
    }

    /**
     * Permet de retourner la valeur traduit d'un element donnee.
     *
     * @param CommonFaqEntreprise $valueName   l'objet faq entreprise
     * @param string              $methodeName le nom de la methode de l'element qu'on veux recuperer
     *
     * @return string la valeur de l'element
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function writeValue(CommonFaqEntreprise $valueName, $methodeName)
    {
        $langues = Atexo_Languages::retrieveArrayActiveLangages();
        $value = '';
        if (is_array($langues)) {
            foreach ($langues as $langue) {
                if (strtolower($langue) == strtolower(Atexo_CurrentUser::readFromSession('lang'))) {
                    if (strtolower($langue) == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
                        $valueLangue = 'get'.$methodeName;
                        $value = $valueName->$valueLangue();
                    } else {
                        $valueLangue = 'get'.$methodeName.strtoupper(substr($langue, 0, 1)).strtolower(substr($langue, 1));
                        $value = $valueName->$valueLangue();
                    }
                }
            }

            return $value;
        }
    }

    /**
     * Permet de remplire les repeater des donnees.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function fillRepeaters()
    {
        $dataSourceMonCompte = (new Atexo_Entreprise_FoireAuxQuestions())->retreiveInfoFoire('THEME_MON_COMPTE');
        $this->repeaterMonCompte->DataSource = $dataSourceMonCompte;
        $this->repeaterMonCompte->DataBind();
        $dataSourceConsRep = (new Atexo_Entreprise_FoireAuxQuestions())->retreiveInfoFoire('THEME_CONSULTATIONS_ET_REPONSES');
        $this->repeaterConsultationsReponses->DataSource = $dataSourceConsRep;
        $this->repeaterConsultationsReponses->DataBind();
        $dataSourceConf = (new Atexo_Entreprise_FoireAuxQuestions())->retreiveInfoFoire('THEME_CONFIGURATION');
        $this->repeaterConfiguration->DataSource = $dataSourceConf;
        $this->repeaterConfiguration->DataBind();
    }
}
