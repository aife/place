<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_FluxRss;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;

class FluxRss extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $this->panelMessageErreur->setVisible(false);
        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
        self::displayFluxRss();
    }

    public function displayMessage($text)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($text);
    }

    /*
     * permet de remplir le repeater des flux rss par les flux à afficher
     */
    public function displayFluxRss()
    {
        $dataSource = (new Atexo_FluxRss())->getFluxRssAAfficher();
        $this->repeaterFluxRss->DataSource = $dataSource;
        $this->repeaterFluxRss->DataBind();
    }
}
