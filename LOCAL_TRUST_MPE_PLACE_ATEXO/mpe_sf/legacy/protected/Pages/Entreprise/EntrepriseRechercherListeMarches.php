<?php

namespace Application\Pages\Entreprise;

use App\Entity\MarchePublie;
use App\Service\ConfigurationOrganismeService;
use App\Service\CpvService;
use App\Service\WebServicesSourcing;
use Application\Controls\MpeTPage;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionQuery;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonMarchePublie;
use Application\Propel\Mpe\CommonMarchePubliePeer;
use Application\Propel\Mpe\CommonModificationContratQuery;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\CommonTypeProcedurePivotQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ListeMarches;
use Application\Service\Atexo\Atexo_Marche;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Application\Service\Atexo\Contrat\Atexo_Contrat_FormePrix;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Marche\Atexo_Marche_Annee;
use Prado\Prado;
use Prado\Util\TLogger;
use Prado\Web\UI\WebControls\TLinkButton;
use Prado\Web\UI\WebControls\TPager;

/**
 * Page de publication de la liste des marchés - Article 133.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseRechercherListeMarches extends MpeTPage
{
    private $_annee;
    private $_idService;
    private string $_calledFrom = '';
    private $langue;
    protected $arrayFormePrix;
    public bool $expandEtalabInfo = false;
    public bool $expandInfo = false;

    public function getOrganismeAcronyme()
    {
        return $this->getViewState('acronymeOrganisme');
    }

    public function getAnnee()
    {
        if (null == $this->_annee) {
            $this->_annee = $this->getViewState('annee');
        }

        return $this->_annee;
    }

    public function getIdServiceAgent()
    {
        if (null == $this->_idService) {
            $this->_idService = $this->getViewState('idService');
        }

        return $this->_idService;
    }

    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $organismesArray = [];
        $this->referentielCPV->setCodeCpvObligatoire(false);
        $this->referentielCPV->setInfoBulleDisplayed(true);

        $this->arrayFormePrix = Atexo_Contrat_FormePrix::getFormes();
        $categories = Atexo_Consultation_Category::retrieveCategories(true);
        $this->categorie->setDataSource(Atexo_Consultation_Category::retrieveDataCategories($categories));
        $this->categorie->dataBind();

        if (!$this->Page->IsPostBack) {
            $this->inclureDescendances->checked = true;
            $this->referentielCPV->onInitComposant();
        }

        if (!Atexo_Module::isEnabled('Article133UploadFichier') && !Atexo_Module::isEnabled('Article133GenerationPf')) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL') . '?page=Entreprise.EntrepriseHome');
        }

        if (Atexo_Module::isEnabled('Article133UploadFichier') && !Atexo_Module::isEnabled('Article133GenerationPf')) {
            $_GET['download'] = '';
        } elseif (!Atexo_Module::isEnabled('Article133UploadFichier') && Atexo_Module::isEnabled('Article133GenerationPf')) {
            $_GET['search'] = '';
        }

        $this->langue = Atexo_CurrentUser::readFromSession('lang');
        $this->tableauListeMarches->setPostBack($this->IsPostBack);
        $this->tableauListeMarches->panelListeMarches->setDisplay('None');

        if (!$this->isPostBack) {
            $this->formulaireRecherche->setVisible(true);
            $this->etapeResultatsPanel->setVisible(false);
            $this->messageErreur->setVisible(false);
            $this->suiteRecherche->setDisplay('None');
            $this->concessionnaireLabel->setDisplay('None');

            if (isset($_COOKIE['selectedorg'])) {
                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($_COOKIE['selectedorg']);
                $organismesArray[$_COOKIE['selectedorg']] = $organisme->getDenominationOrg();
            } else {
                $organismes = Atexo_Organismes::retrieveOrganismes(true);
                $organismesArray[0] = '--- ' . Prado::localize('ENTITE_PUBLIQUE') . ' ---';

                foreach ($organismes as $oneOrg) {
                    $organismesArray[$oneOrg->getAcronyme()] = $oneOrg->getDenominationOrg();
                }
            }

            $this->organismeAcronyme->setDataSource($organismesArray);
            $this->organismeAcronyme->dataBind();

            if (isset($_COOKIE['selectedorg'])) {
                $this->organismeAcronyme->setSelectedValue($_COOKIE['selectedorg']);

                if (Atexo_Module::isEnabled('Article133UploadFichier') && isset($_GET['download'])) {
                    $this->tableauListeMarches->panelListeMarches->setDisplay('Dynamic');
                    $this->tableauListeMarches->setOrganisme($this->organismeAcronyme->getSelectedValue());
                    $this->tableauListeMarches->displayListeMarche();
                }
            }
            if (Atexo_Module::isEnabled('Article133GenerationPf') && isset($_GET['search'])) {
                $this->updateListes();
            }
        }
    }

    public function etapeRecherche()
    {
        $this->formulaireRecherche->setVisible(true);
        $this->etapeResultatsPanel->setVisible(false);
    }

    public function etapeNouvelleRecherche()
    {
        $this->formulaireRecherche->setVisible(true);
        $this->etapeResultatsPanel->setVisible(false);

        $this->attributaire->Text = '';
        $this->naturePrestations->setSelectedValue('0');
        $this->categorie->setSelectedValue('0');
        $this->categorie->setSelectedValue('0');
		$this->montantMin->Text = '';
        $this->montantMax->Text = '';
        $this->dateNotificationStart->Text = '';
        $this->dateNotificationEnd->Text = '';
        $this->keywordSearch->Text = '';
    }

    public function updateListes()
    {
        $organismeAcronyme = $this->organismeAcronyme->getSelectedValue();

        if (!$organismeAcronyme || '0' == $organismeAcronyme) {
            $this->suiteRecherche->setDisplay('None');
            $this->panelBouttonSearch->setDisplay('None');

            return;
        }

        $this->setViewState('acronymeOrganisme', $organismeAcronyme);

        $entities = Atexo_EntityPurchase::getEntityPurchase($organismeAcronyme, true);
        if (is_array($entities)) {
            $this->entiteAchat->DataSource = $entities;
            $this->entiteAchat->DataBind();
        }

        $categories = Atexo_Consultation_Category::retrieveCategories(true);
        $this->naturePrestations->setDataSource(Atexo_Consultation_Category::retrieveDataCategories($categories));
        $this->naturePrestations->dataBind();

        $listeAnnes = (new Atexo_Marche_Annee())->getYearToDisplay();
        $this->annee->setDataSource($listeAnnes);
        $this->annee->dataBind();
        $arrayKeys = array_keys($listeAnnes);

        $annee = array_shift($arrayKeys);

        //      $this->naturePrestations->setDataSource(CommonCategorieConsultationPeer::getCategories($organismeAcronyme));
        $this->naturePrestations->dataBind();

        $this->suiteRecherche->setDisplay('Dynamic');
        $this->panelBouttonSearch->setDisplay('Dynamic');
    }

    public function callBackUpdateListes($sender, $param)
    {
        if (Atexo_Module::isEnabled('Article133UploadFichier') && isset($_GET['download'])) {
            $this->tableauListeMarches->panelListeMarches->render($param->getNewWriter());
        }
        if (Atexo_Module::isEnabled('Article133GenerationPf') && isset($_GET['search'])) {
            $this->panelSearchInEntreprise->render($param->getNewWriter());
        }
    }

    /**
     * @throws PropelException
     */
    public function etapeResultats()
    {
        $this->messageErreur->setVisible(false);

        // On sauvegarde l'annee selectionnée
        $this->setViewState('annee', $this->annee->getSelectedValue());
        $this->setViewState('idService', $this->entiteAchat->getSelectedValue());

        // On check pour l'année en cours s'il existe des données MarchePublie
        $this->checkMarchePublieCurrentYear(
            $this->annee->getSelectedValue(),
            $this->getViewState('acronymeOrganisme'),
            $this->entiteAchat->getSelectedValue()
        );

        $listesDesMarchesPublies = $this->getServicesAvecAnneeMarchePublie();
        //si le tableau retourné n'est pas vide on fait la recherche des marches avec les services concernes
        // et qui ont publie leurs marches
        if ($listesDesMarchesPublies && is_array($listesDesMarchesPublies)) {
            $arrayIdsServices = [];
            foreach ($listesDesMarchesPublies as $marchePublie) {
                $arrayIdsServices[] = $marchePublie->getServiceId();
            }
            $this->formulaireRecherche->setVisible(false);
            $this->etapeResultatsPanel->setVisible(true);
            $this->afficherResultats($arrayIdsServices);
        }
        //sinon aucun des services/sous-services n'a publie
        else {
            $this->messageErreur->setVisible(true);
            $this->messageErreur->setMessage(Prado::localize('LISTE_MARCHES_NON_PUBLIEES'));

            return;
        }
    }

    /**
     * @param TPager $sender
     * @param $param
     */
    public function pageChanged($sender, $param)
    {
        if ('PagerBottom' == $sender->getID()) {
            $this->repeaterContrats->CurrentPageIndex = $param->NewPageIndex;
            $this->numPageBottom->Text = $param->NewPageIndex + 1;
            $this->expandInfo = true;
        }
        if ('EtalabPagerBottom' == $sender->getID()) {
            $this->repeaterEtalabContrats->CurrentPageIndex = $param->NewPageIndex;
            $this->numEtalabPageBottom->Text = $param->NewPageIndex + 1;
            $this->expandEtalabInfo = true;
        }
        $criteriaVo = $this->getViewState('criteriaVo');
        $this->populateData($criteriaVo);
        $this->populateEtalabData($criteriaVo);
    }

    public function changePagerLenght($sender, $param)
    {
        if ('nombreResultatAfficherBottom' == $sender->getID()) {
            $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
            $this->repeaterContrats->PageSize = $pageSize;
            $this->repeaterContrats->setCurrentPageIndex(0);
            $this->numPageBottom->Text = 1;
            $this->expandInfo = true;
        }

        if ('nombreEtalabResultatAfficherBottom' == $sender->getID()) {
            $pageSize = $this->nombreEtalabResultatAfficherBottom->getSelectedValue();
            $this->repeaterEtalabContrats->PageSize = $pageSize;
            $this->repeaterEtalabContrats->setCurrentPageIndex(0);
            $this->numEtalabPageBottom->Text = 1;
            $this->expandEtalabInfo = true;
        }
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterContrats->PageSize);

        $nombreElement = $this->getViewState('nombreEtalabElement');
        $this->nombreEtalabPageBottom->Text = ceil($nombreElement / $this->repeaterEtalabContrats->PageSize);

        $criteriaVo = $this->getViewState('criteriaVo');
        $this->populateData($criteriaVo);
        $this->populateEtalabData($criteriaVo);
    }

    /**
     * Afficher les informations des contrats.
     */
    private function refreshPageInfo(Atexo_Contrat_CriteriaVo $criteriaVo): void
    {
        $criteriaVo->setCount(true);
        $contratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $nombreElement = $contratTitulaireQuery->getContratByCriteria($criteriaVo);
        $criteriaVo->setCount(false);
        $this->nbrResultats->Text = Prado::Localize('DEFINE_RESULTAT_RECHERCHE_CONTRATS', ['nbrResultats' => $nombreElement]);

        $nombreElementEtalab = $this->getContratPublishedEtalab($criteriaVo, true);
        $this->nbrEtalabResultats->Text = Prado::Localize('DEFINE_RESULTAT_ETALAB_CONTRATS', ['nbrResultats' => $nombreElementEtalab]);

        if ($nombreElement >= 1) {
            $this->PagerBottom->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterContrats->PageSize);
            $this->repeaterContrats->setVirtualItemCount($nombreElement);
            $this->repeaterContrats->setCurrentPageIndex(0);

            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
        }

        $this->refreshPageInfoEtalab($criteriaVo, $nombreElementEtalab);
        $this->loadSourceBlocByDataSource($criteriaVo);
    }

    /**
     * Afficher les informations des contrats etalab.
     *
     * @param int|mixed $nombreElementEtalab
     */
    private function refreshPageInfoEtalab(Atexo_Contrat_CriteriaVo $criteriaVo, $nombreElementEtalab): void
    {
        if (!$this->isSiretAcheteursExists($criteriaVo)) {
            $this->panelEtalabNoSiretFound->setVisible(true);
            $this->nbrEtalabResultats->Text = Prado::Localize('LISTE_MARCHES_SIRET_MANQUANT');

            $this->panelEtalabMoreThanOneElementFound->setVisible(false);
            $this->panelEtalabNoElementFound->setVisible(false);
            $this->EtalabPagerBottom->setVisible(false);
        } elseif ($nombreElementEtalab >= 1) {
            $this->EtalabPagerBottom->setVisible(true);
            $this->setViewState('nombreEtalabElement', $nombreElementEtalab);
            $this->nombreEtalabPageBottom->Text = ceil($nombreElementEtalab / $this->repeaterEtalabContrats->PageSize);
            $this->repeaterEtalabContrats->setVirtualItemCount($nombreElementEtalab);
            $this->repeaterEtalabContrats->setCurrentPageIndex(0);
            $this->panelEtalabMoreThanOneElementFound->setVisible(true);

            $this->panelEtalabNoElementFound->setVisible(false);
            $this->panelEtalabNoSiretFound->setVisible(false);
        } else {
            $this->panelEtalabNoElementFound->setVisible(true);

            $this->panelEtalabMoreThanOneElementFound->setVisible(false);
            $this->EtalabPagerBottom->setVisible(false);
            $this->panelEtalabNoSiretFound->setVisible(false);
        }
    }

    /**
     * @throws PropelException
     */
    private function loadSourceBlocByDataSource(Atexo_Contrat_CriteriaVo $criteriaVo): void
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $organisme = $criteriaVo->getOrganisme();
        $serviceId = null;

        if ($this->isOrganismeCentralisee($organisme)) {
            $serviceId = 0;
        } elseif (($service = $criteriaVo->getServiceId()) && isset($service[0])) {
            $serviceId = $service[0];
        }

        if (
            null !== $serviceId
            && $organisme
            && ($year = $criteriaVo->getAnneeNotification())
        ) {
            $marchePublie = (new Atexo_Marche())->getMarchePublieByYearAndOrganisme($organisme, $serviceId, $year, $connexion);

            // init visible panel
            $this->panelRefresh->setVisible(true);
            $this->panelEtalabRefresh->setVisible(true);

            if ($marchePublie) {
                if ($marchePublie->getDatasource() === MarchePublie::DATA_GOUV) {
                    $this->panelRefresh->setVisible(false);
                }

                if ($marchePublie->getDatasource() === MarchePublie::DATA_DEFAULT) {
                    $this->panelEtalabRefresh->setVisible(false);
                }
            }
        }
    }

    private function populateData($criteriaVo)
    {
        $offset = $this->repeaterContrats->CurrentPageIndex * $this->repeaterContrats->PageSize;
        $limit = $this->repeaterContrats->PageSize;
        if ($offset + $limit > $this->repeaterContrats->getVirtualItemCount()) {
            $limit = $this->repeaterContrats->getVirtualItemCount() - $offset;
        }

        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);

        $contratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $arrayContratsTitulaire = $contratTitulaireQuery->getContratByCriteria($criteriaVo);

        $contratsTitulaire = [];

        foreach ($arrayContratsTitulaire as $contrat) {
            $modificationContratQuery = new CommonModificationContratQuery();
            $donneesAnnuellesConcessionQuery = new CommonDonneesAnnuellesConcessionQuery();
            $modifications = $modificationContratQuery->findByIdContratTitulaire($contrat->getIdContratTitulaire());

            if (1 == $criteriaVo->getSearchModule()) {
                $donneesAnnuellesConcession = $donneesAnnuellesConcessionQuery->getDonneesAnnuellesConcession($contrat->getIdContratTitulaire());
                $data = [
                    'contrat' => $contrat,
                    'modifications' => $modifications,
                    'donneesAnnuellesConcession' => $donneesAnnuellesConcession,
                ];
            } else {
                $data = [
                    'contrat' => $contrat,
                    'modifications' => $modifications,
                ];
            }
            $contratsTitulaire[] = $data;
        }

        $this->repeaterContrats->DataSource = $contratsTitulaire;
        $this->repeaterContrats->DataBind();
    }

    private function populateEtalabData(Atexo_Contrat_CriteriaVo $criteriaVo)
    {
        $offset = $this->repeaterEtalabContrats->CurrentPageIndex * $this->repeaterEtalabContrats->PageSize;
        $limit = $this->repeaterEtalabContrats->PageSize;
        if ($offset + $limit > $this->repeaterEtalabContrats->getVirtualItemCount()) {
            $limit = $this->repeaterEtalabContrats->getVirtualItemCount() - $offset;
        }

        $criteriaVo->setOffset($this->repeaterEtalabContrats->CurrentPageIndex);
        $criteriaVo->setLimit($limit);

        $contratsEtalab = $this->getContratPublishedEtalab($criteriaVo);

        $this->repeaterEtalabContrats->DataSource = $contratsEtalab->content;
        $this->repeaterEtalabContrats->DataBind();
    }

    public function goToPage($sender, $param)
    {
        $numPage = $this->numPageBottom->Text;
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->numPageBottom->Text) {
                $numPage = $this->numPageBottom->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->repeaterContrats->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $criteriaVo = $this->getViewState('criteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageBottom->Text = $this->repeaterContrats->CurrentPageIndex + 1;
        }
    }

    /**
     * @param TLinkButton $sender
     * @param mixed       $param
     */
    public function Trier($sender, $param)
    {
        $criteriaVo = $this->getViewState('criteriaVo');
        $champsOrderBy = $param->getCommandName();
        if (str_contains($sender->getParent()->getUniqueID(), 'repeaterContrats')) {
            $this->setViewState('sortByElement', $champsOrderBy);
            $criteriaVo->setChampOrderBy($champsOrderBy);
            $arraySensTri = $this->getViewState('sensTriArray', []);
            $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
            $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
            $this->setViewState('sensTriArray', $arraySensTri);
            $this->setViewState('criteriaVo', $criteriaVo);
            $this->repeaterContrats->setCurrentPageIndex(0);
            $this->numPageBottom->Text = 1;
            $this->expandInfo = true;
        }
        if (str_contains($sender->getParent()->getUniqueID(), 'repeaterEtalabContrats')) {
            $this->setViewState('sortEtalabByElement', $champsOrderBy);
            $criteriaVo->setChampEtalabOrderBy($champsOrderBy);
            $arraySensTri = $this->getViewState('sensEtalabTriArray', []);
            $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensEtalabOrderBy()) ? 'DESC' : 'ASC';
            $criteriaVo->setSensEtalabOrderBy($arraySensTri[$champsOrderBy]);
            $this->setViewState('sensEtalabTriArray', $arraySensTri);
            $this->setViewState('criteriaVo', $criteriaVo);
            $this->repeaterEtalabContrats->setCurrentPageIndex(0);
            $this->numEtalabPageBottom->Text = 1;
            $this->expandEtalabInfo = true;
        }
        $this->populateData($criteriaVo);
        $this->populateEtalabData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    private function afficherResultats($arrayIdsServices)
    {
        $contratCriteriaVo = new Atexo_Contrat_CriteriaVo();

        // pour eviter le cas 1 qui est par defaut
        $contratCriteriaVo->setContratOrganismeInvite('0');
        $contratCriteriaVo->setOrganisme($this->getViewState('acronymeOrganisme'));
        $this->ws_organisme->setValue($contratCriteriaVo->getOrganisme());
        $this->ws_organisme->dataBind();


        $this->ws_inclureDescendances->setValue($this->inclureDescendances->Checked);
        $this->ws_inclureDescendances->dataBind();

        $contratCriteriaVo->setAnneeNotification($this->getViewState('annee'));
        $this->ws_anneeNotification->setValue($contratCriteriaVo->getAnneeNotification());
        $this->ws_anneeNotification->dataBind();

        if (trim($this->attributaire->Text)) {
            $contratCriteriaVo->setNomAttributaire(trim($this->attributaire->getSafeText()));
            $this->ws_nomAttributaire->setValue($contratCriteriaVo->getNomAttributaire());
            $this->ws_nomAttributaire->dataBind();
        }

        if ('0' != $this->naturePrestations->getSelectedValue()) {
            $contratCriteriaVo->setCategorie($this->naturePrestations->getSelectedValue());
            $this->ws_categorie->setValue($contratCriteriaVo->getCategorie());
            $this->ws_categorie->dataBind();
        }

        if (trim($this->montantMin->Text)) {
            $contratCriteriaVo->setMontantMin(trim($this->montantMin->getSafeText()));
            $this->ws_montantMin->setValue($contratCriteriaVo->getMontantMin());
            $this->ws_montantMin->dataBind();
        }

        if (trim($this->montantMax->Text)) {
            $contratCriteriaVo->setMontantMax(trim($this->montantMax->getSafeText()));
            $this->ws_montantMax->setValue($contratCriteriaVo->getMontantMax());
            $this->ws_montantMax->dataBind();
        }

        if (trim($this->referentielCPV->getCpvPrincipale())) {
            $contratCriteriaVo->setCodeCpv($this->referentielCPV->getCpvPrincipale() . $this->referentielCPV->getCpvSecondaires());
            $this->ws_codeCPV->setValue($contratCriteriaVo->getCodeCpv());
            $this->ws_codeCPV->dataBind();
        }

        if (trim($this->dateNotificationStart->Text)) {
            $contratCriteriaVo->setDateNotificationStart(Atexo_Util::frnDate2iso(trim($this->dateNotificationStart->getSafeText())));
            $this->ws_dateNotifStart->setValue($contratCriteriaVo->getDateNotificationStart());
            $this->ws_dateNotifStart->dataBind();
        }

        if (trim($this->dateNotificationEnd->Text)) {
            $contratCriteriaVo->setDateNotificationEnd(Atexo_Util::frnDate2iso(trim($this->dateNotificationEnd->getSafeText())));
            $this->ws_dateNotifEnd->setValue($contratCriteriaVo->getDateNotificationEnd());
            $this->ws_dateNotifEnd->dataBind();
        }

        if (trim($this->keywordSearch->Text)) {
            $contratCriteriaVo->setMotsCles(trim($this->keywordSearch->getSafeText()));
            $this->ws_keywordSearch->setValue($contratCriteriaVo->getMotsCles());
            $this->ws_keywordSearch->dataBind();
        }

        $contratCriteriaVo->setContratClassKey('1');
        $contratCriteriaVo->setPublicationContrat('0'); // 0 => Contrat publie
        $contratCriteriaVo->setDateNotificationNotNull(true);

        $concession = 0;
        $contratCriteriaVo->setSearchModule(1);
        if ('1' == $this->searchConcession->getValue()) {
            $concession = 1;
        }

        $contratCriteriaVo->setConcession($concession);
        $this->ws_concession->setValue($concession);

        $contratCriteriaVo->setChampOrderBy([CommonTContratTitulaire::class . '.IdTypeContrat']);
        $this->setViewState('criteriaVo', $contratCriteriaVo);

        $siretAcheteurs = [];
        if (!empty($contratCriteriaVo->getServiceId())) {
            foreach ($contratCriteriaVo->getServiceId() as $service) {
                $siretAcheteurs[] = $this->getSiretOrganismeService($contratCriteriaVo->getOrganisme(), $service, true);
            }
        } else {
            $siretAcheteurs[] = $this->getSiretOrganismeService($contratCriteriaVo->getOrganisme(), null, true);
        }

        $contratCriteriaVo->setSiretAcheteurs($siretAcheteurs);
        $this->refreshPageInfo($contratCriteriaVo);
        $this->populateData($contratCriteriaVo);
        $this->populateEtalabData($contratCriteriaVo);
    }

    /**
     * @return array|mixed
     * @throws PropelException
     */
    private function getServicesAvecAnneeMarchePublie()
    {
        $listeChilds = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idService = $this->getViewState('idService') !== '0' ? $this->getViewState('idService') : null;
        $acronymeOrganisme = $this->getViewState('acronymeOrganisme');
        $inclureDescendances = $this->inclureDescendances->Checked;
        $anneSelectionnee = $this->annee->getSelectedValue();

        if ($this->isOrganismeCentralisee($acronymeOrganisme)) {
            // En mode organisme centralisée, il faut récupérer les services correspondant à l'organisme
            $marchePublieServiceCentralisee = (new Atexo_Marche())->getMarchePublieByYearAndOrganisme($acronymeOrganisme, MarchePublie::ID_SERVICE_CENTRALISEE, $anneSelectionnee, $connexionCom);

            if (
                $marchePublieServiceCentralisee
                && $marchePublieServiceCentralisee->getIspubliee() == MarchePublie::PUBLICATION_NO
            ) {
                return [];
            }

            $listeChilds = Atexo_EntityPurchase::getEntityPurchaseById($acronymeOrganisme);

            $marchesPublieCentralisee = [];

            // Set service Id parent
            $marchePublieParent = new CommonMarchePublie();
            $marchePublieParent->setOrganisme($acronymeOrganisme);
            $marchePublieParent->setServiceId(MarchePublie::ID_SERVICE_CENTRALISEE);
            $marchePublieParent->setNumeromarcheannee($anneSelectionnee);
            $marchePublieParent->setIspubliee(MarchePublie::PUBLICATION_YES);
            $marchesPublieCentralisee[] = $marchePublieParent;

            // Set services enfant
            foreach ($listeChilds as $child) {
                $marchePublie = new CommonMarchePublie();
                $marchePublie->setOrganisme($acronymeOrganisme);
                $marchePublie->setServiceId($child);
                $marchePublie->setNumeromarcheannee($anneSelectionnee);
                $marchePublie->setIspubliee(MarchePublie::PUBLICATION_YES);

                $marchesPublieCentralisee[] = $marchePublie;
            }

            return $marchesPublieCentralisee;
        } else {
            // On verifie que l'entite d'achat a bien publie sa liste des marche
            $c = new Criteria();
            $c->add(CommonMarchePubliePeer::ORGANISME, $acronymeOrganisme);
            $c->add(CommonMarchePubliePeer::NUMEROMARCHEANNEE, $anneSelectionnee);
            $c->add(CommonMarchePubliePeer::ISPUBLIEE, 1, Criteria::EQUAL);

            if ($inclureDescendances) {
                if ($idService) {
                    //cas 4 : service <> 0 (niveau entite) et les descendants, il faut rechercher les marches avec serviceId selectionne ou incluant ceux des descendants
                    $listeChilds = Atexo_EntityPurchase::getSubServices($idService, $acronymeOrganisme, true, false);
                    $listeChilds[] = $idService;
                    $c->add(CommonMarchePubliePeer::SERVICE_ID, $listeChilds, Criteria::IN);
                } else {
                    //cas 2 : service = 0 (niveau ministere) et les descendants, il faut rechercher les marches avec serviceId = NULL  ou incluant ceux des descendants
                    $listeChilds = Atexo_EntityPurchase::getEntityPurchaseById($acronymeOrganisme);
                    $crit0 = $c->getNewCriterion(CommonMarchePubliePeer::SERVICE_ID, $listeChilds, Criteria::IN);
                    $crit1 = $c->getNewCriterion(CommonMarchePubliePeer::SERVICE_ID, null, Criteria::ISNULL);
                    $crit0->addOR($crit1);
                    $c->add($crit0);
                }
            } else {
                //cas 3 : service <> 0 OR service==0 (niveau entite) et pas les descendants, il faut rechercher les marches avec serviceId sélectionne
                $listeChilds[] = $idService;
                $c->add(CommonMarchePubliePeer::SERVICE_ID, $listeChilds, Criteria::IN);
            }

            return CommonMarchePubliePeer::doSelect($c, $connexionCom);
        }
    }

    public function doSearch($sender, $param)
    {
        $this->attributaireLabel->setDisplay('Dynamic');
        $this->concessionnaireLabel->setDisplay('None');
        $concession = $this->searchConcession->getValue();
        if ('1' == $concession) {
            $this->attributaireLabel->setDisplay('None');
            $this->concessionnaireLabel->setDisplay('Dynamic');
        }

        if (Atexo_Module::isEnabled('Article133UploadFichier') && isset($_GET['download'])) {
            $this->tableauListeMarches->panelListeMarches->setDisplay('Dynamic');
            $this->tableauListeMarches->setCalledFromAgent(false);
            $this->tableauListeMarches->setOrganisme($this->organismeAcronyme->getSelectedValue());
            $this->tableauListeMarches->displayListeMarche();
        }
        if (Atexo_Module::isEnabled('Article133GenerationPf') && isset($_GET['search'])) {
            $this->updateListes();
        }
    }

    /**
     * @return string
     */
    public function getRawFavoritedLieux()
    {
        $lieux = '';

        if ('agent' == $this->_calledFrom) {
            $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());

            if ($agent) {
                $lieux = $agent->getLieuExecution();
            }
        }

        return $lieux;
    }

    /**
     * @return string
     */
    public function getFavoritedLieux()
    {
        $buffer = $this->getRawFavoritedLieux();
        $arrayId = explode(',', $buffer);
        //Suppression des elements nuls du tableau
        $arrayId = array_filter($arrayId);
        $new_arrayId = [];

        $geoN2 = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayId);

        if ($geoN2) {
            foreach ($geoN2 as $oneN2) {
                $new_arrayId[] = $oneN2->getDenomination2();
            }
        }

        return implode('_', $new_arrayId);
    }

    /**
     * afficher les denominations des lieux d'executions.
     */
    public function displaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeoN2->Text = $this->getSelectedGeoN2($ids);
    }

    /**
     * recuperer les denominations des lieux d'execution.
     */
    public function getSelectedGeoN2($ids)
    {
        $text = '';
        $arIds = explode(',', $ids);
        $list = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
        $getDenomination1 = 'getDenomination1' . Atexo_Languages::getLanguageAbbreviation($this->langue);

        if ($list) {
            $lieuxExecutionsType1 = '';

            foreach ($list as $oneGeoN2) {
                if (
                    0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) ||
                    !$oneGeoN2->$getDenomination1()
                ) {
                    $lieuxExecutionsType1 .= $oneGeoN2->getDenomination1() . ', ';
                } else {
                    $lieuxExecutionsType1 .= $oneGeoN2->$getDenomination1() . ', ';
                }
            }

            if ($lieuxExecutionsType1) {
                $text = substr($lieuxExecutionsType1, 0, -2);
            }
        }

        return $text;
    }

    /**
     * @param $accronymeOrganisme
     *
     * @return string
     */
    public function getSiretOrganisme($accronymeOrganisme)
    {
        $organismeQuery = new CommonOrganismeQuery();
        $organisme = $organismeQuery->findOneByAcronyme($accronymeOrganisme);

        return is_object($organisme) ? $organisme->getSiren() . ' - ' . $organisme->getComplement() : '';
    }

    /**
     * @param $accronymeOrganisme
     *
     * @return string
     */
    public function getOrganisme($accronymeOrganisme)
    {
        $organismeQuery = new CommonOrganismeQuery();

        $organisme = $organismeQuery->findOneByAcronyme($accronymeOrganisme);

        return $organisme->getDenominationOrg();
    }

    /**
     * @param $idEntreprise
     * @param $idEtablissement
     *
     * @return string
     *
     * @throws PropelException
     */
    public function getSiretEntreprise($idEntreprise, $idEtablissement)
    {
        $entrepriseQuery = new CommonEntrepriseQuery();
        $entreprise = $entrepriseQuery->findOneById($idEntreprise);

        $etablissementQuery = new CommonTEtablissementQuery();
        $etablissement = $etablissementQuery->findOneByIdEtablissement($idEtablissement);

        return is_object($entreprise) ? $entreprise->getSiren() . '-' .
            (is_object($etablissement) ? $etablissement->getCodeEtablissement() : '') : '';
    }

    /**
     * @param $idProcedurePivot
     *
     * @return mixed|string
     */
    public function getProcedurePivot($idProcedurePivot)
    {
        $procedurePivotQuery = new CommonTypeProcedurePivotQuery();
        $procedurePivot = $procedurePivotQuery->findOneById($idProcedurePivot);

        if (!is_object($procedurePivot)) {
            return '';
        }

        return Prado::localize($procedurePivot->getLibelle());
    }

    /**
     * Envoyer les paramètres de recherche au WS contrats symfony pour exporter les résultats au format XML.
     */
    public function exportAsXml($sender, $param)
    {
        $lieuxExecution = null;
        $organisme = $this->ws_organisme->Value ?: 'false';
        $servicesId = $this->ws_services->Value ?: '0';
        $inclureDescendance = $this->ws_inclureDescendances->Value;
        $anneeNotif = $this->ws_anneeNotification->Value ?: 'false';
        $nomAttributaire = $this->ws_nomAttributaire->Value ?: 'false';
        $categorie = $this->ws_categorie->Value ?: 'false';
        $montantMin = $this->ws_montantMin->Value ?: 'false';
        $montantMax = $this->ws_montantMax->Value ?: 'false';
        $lieuxExecution = 'false';
        $codesCpv = $this->ws_codeCPV->Value ?: 'false';
        $codesCpv = str_replace('#', ',', $codesCpv);
        $dateNotifStart = $this->ws_dateNotifStart->Value ?: 'false';
        $dateNotifEnd = $this->ws_dateNotifEnd->Value ?: 'false';
        $keyword = $this->ws_keywordSearch->Value ?: 'false';
        $concession = $this->ws_concession->Value ?: 'false';

        $url = Atexo_Config::getParameter('PF_URL') . "app.php/api/v1/donnees-essentielles/contrat/xml-extraire-criteres/$organisme/$servicesId/$inclureDescendance/$anneeNotif/$nomAttributaire/$categorie/$montantMin/$montantMax/$lieuxExecution/$codesCpv/$dateNotifStart/$dateNotifEnd/$keyword";
        if ('1' == $concession) {
            $url = $url . '/1';
        }

        $this->response->redirect($url);
    }

    /**
     * Envoyer les paramètres de recherche au WS contrats symfony pour exporter les résultats au format XML.
     */
    public function exportEtalabAsXml($sender, $param)
    {
        $criteriaVo = $this->getViewState('criteriaVo');
        $champsOrderBy = $param->getCommandName();
        if (str_contains($sender->getParent()->getUniqueID(), 'repeaterEtalabContrats')) {
            $this->setViewState('sortEtalabByElement', $champsOrderBy);
            $criteriaVo->setChampEtalabOrderBy($champsOrderBy);
            $arraySensTri = $this->getViewState('sensEtalabTriArray', []);
            $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensEtalabOrderBy()) ? 'DESC' : 'ASC';
            $criteriaVo->setSensEtalabOrderBy($arraySensTri[$champsOrderBy]);
            $this->setViewState('sensEtalabTriArray', $arraySensTri);
            $this->setViewState('criteriaVo', $criteriaVo);
        }
        $servicesSourcing = Atexo_Util::getSfContainer()->get(WebServicesSourcing::class);
        try {
            header('Content-type: text/xml');
            header('Content-Disposition: attachment; filename="marches.xml"');
            echo $servicesSourcing->getContent('getDonneesEssentiellesEtalabFormatXml', $criteriaVo);
            exit();
        } catch (\Exception $e) {
            Prado::log(
                'Erreur Récupération fichier xml depuis sourcing' .
                $e->getMessage() . $e->getTraceAsString(),
                TLogger::ERROR,
                'EntrepriseRechercherListeMarches.php'
            );
        }
    }

    /**
     * retour le code postal et la ville de l'attributaire.
     *
     * @param $idEntreprise
     * @param $idEtablissement
     *
     * @return string
     *
     * @throws PropelException
     */
    public function getCodePostalEtVille($idEntreprise)
    {
        $entrepriseQuery = new CommonEntrepriseQuery();
        $entreprise = $entrepriseQuery->findOneById($idEntreprise);

        return is_object($entreprise) ? '(' . $entreprise->getCodePostal() . ' - ' . $entreprise->getVilleadresse() . ')' : '';
    }

    /**
     * Methode qui nous permet d'afficher le label du code CPV.
     *
     * @param $cpvPrincipal
     *
     * @return string
     *
     * @throws Atexo_Config_Exception
     */
    public function getLabelCodeCpv($cpvPrincipal)
    {
        $atexoRef = new Atexo_Ref();
        $atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_CONFIG'));
        $atexoRef->setPrincipal($cpvPrincipal);

        return $this->afficherReferentiel($atexoRef);
    }

    /**
     * @param $cpvPrincipal
     *
     * @return string
     *
     * @throws Atexo_Config_Exception
     */
    public function getCategorieByCodeCpv($cpvPrincipal)
    {
        /** @var CpvService $cpvService */
        $cpvService = Atexo_Util::getSfContainer()->get(CpvService::class);
        $categorie = $cpvService->getCategorieByCpv($cpvPrincipal);

        return $categorie;
    }

    /**
     * @param $atexoRef
     *
     * @return string
     *
     * @throws Atexo_Config_Exception
     */
    public function afficherReferentiel($atexoRef)
    {
        $dataRefFile = null;
        $consulationPrincipal = null;
        $sessionLang = Atexo_CurrentUser::readFromSession('lang');

        if (isset($sessionLang)) {
            $lang = $sessionLang;
        } else {
            $lang = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }

        if ($atexoRef instanceof Atexo_Ref) {
            //l'identifiant
            $atexoRef->setClef($this->ClientId);
            // la langue
            if ('su' == $lang) {
                $lang = 'sv';
            }

            $atexoRef->setLocale($lang);
            //fichier de données
            $arrayConfigXml = $atexoRef->getArrayConfigXml();
            if ($arrayConfigXml) {
                $dataRefFile = Atexo_Config::getParameter('APP_BASE_ROOT_DIR') . $arrayConfigXml['cheminData'] . $arrayConfigXml['fichierDonnee'] . $lang . '.csv';
            }

            //code principal
            $consulationPrincipal = '';
            if ($atexoRef->getPrincipal()) {
                if (is_file($dataRefFile)) {
                    $consulationPrincipal = $atexoRef->getLibelleRef($atexoRef->getPrincipal(), $dataRefFile);
                }
            }
        }

        return $consulationPrincipal;
    }

    /**
     *  Retourne le label de l'organisme ou du service.
     *
     * @param $organisme
     * @param $idService
     *
     * @return string
     */
    public function getLabelOrganismeService($organisme, $idService)
    {
        return (new Atexo_Consultation())->getOrganismeDenominationByService($organisme, $idService);
    }

    /**
     * Retourne le siret de l'organisme ou du service.
     *
     * @param $organisme
     * @param $idService
     * @param $trim
     *
     * @return string
     */
    public function getSiretOrganismeService($organisme, $idService, $trim = false)
    {
        $siret = (new Atexo_Consultation())->getSiretOrganismeByService($organisme, $idService);

        return $trim ? str_replace(' - ', '', $siret) : $siret;
    }

    public function isConcession()
    {
        $bool = false;
        if ('1' == $this->searchConcession->getValue()) {
            $bool = true;
        }

        return $bool;
    }

    private function getContratPublishedEtalab($criteriaVo, $isCount = false)
    {
        $resultCount = 0;
        $result = 0;
        /** @var ConfigurationOrganismeService $configurationOrganismeService */
        $configurationOrganismeService = Atexo_Util::getSfContainer()->get(ConfigurationOrganismeService::class);
        if ($configurationOrganismeService->isModuleEnabledForOneOrganisme('moduleSourcing')) {
            /** @var WebServicesSourcing $servicesSourcing */
            $servicesSourcing = Atexo_Util::getSfContainer()->get(WebServicesSourcing::class);
            $result = $servicesSourcing->getContent('searchDecpContracts', $criteriaVo);
            if (!empty($result)) {
                $resultCount = $result->totalElements;
            }
        }

        return $isCount ? $resultCount : $result;
    }

    private function isSiretAcheteursExists(Atexo_Contrat_CriteriaVo $criteriaVo): bool
    {
        return !empty($criteriaVo->getSiretAcheteurs()) && !empty($criteriaVo->getSiretAcheteurs()[0]);
    }

    /**
     * Check pour l'année en cours si les données d'accès aux données essentielles existent
     *
     *
     * @throws PropelException
     */
    private function checkMarchePublieCurrentYear(string $year, string $acronymeOrganisme, string $serviceId): void
    {
        if ($year === date("Y")) {
            $previousYear = (int) $year - 1;

            if (is_int($previousYear)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $connexion->beginTransaction();

                $serviceId =
                    $this->isOrganismeCentralisee($acronymeOrganisme)
                        ?
                        Atexo_ListeMarches::SERVICE_ORGANISME_CENTRALISEE
                        : $serviceId
                ;

                $this->setMarchePublieData($acronymeOrganisme, $serviceId, $year, $previousYear, $connexion);

                $connexion->commit();
            }
        }
    }

    /**
     * @param mixed $connexion
     *
     * @throws PropelException
     */
    private function setMarchePublieData(
        string $acronymeOrganisme,
        ?string $serviceId,
        string $year,
        int $previousYear,
        $connexion
    ): void {
        $marchePubliePreviousYear = (new Atexo_Marche())->getMarchePublieByYearAndOrganisme($acronymeOrganisme, $serviceId, $previousYear, $connexion);

        $marchePublie = (new Atexo_Marche())->getMarchePublieByYearAndOrganisme($acronymeOrganisme, $serviceId, $year, $connexion);

        if (!($marchePublie instanceof CommonMarchePublie)) {
            $marchePublie = new CommonMarchePublie();
            $marchePublie->setOrganisme($acronymeOrganisme);
            $marchePublie->setServiceId($serviceId);
            $marchePublie->setNumeromarcheannee($year);

            // set les nouveaux à 1 par défaut
            $marchePublie->setIspubliee(MarchePublie::PUBLICATION_YES);

            // set Data source
            $dataSource = MarchePublie::DATA_BOTH;
            if ($marchePubliePreviousYear) {
                $dataSource = $marchePubliePreviousYear->getDatasource();
            }
            $marchePublie->setDatasource($dataSource);

            $marchePublie->save($connexion);
        }
    }

    private function isOrganismeCentralisee(string $acronymeOrganisme): bool
    {
        return Atexo_Module::isEnabled('OrganisationCentralisee', $acronymeOrganisme);
    }
}
