<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;

class FormulaireSignatureReponse extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('entreprise');
        Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);
        if (!$this->isPostBack) {
            $this->fillRepeaterPiecesReponse();
            $this->traceHistorique('DESCRIPTION15');
        }
    }

    public function fillRepeaterPiecesReponse()
    {
        $dataPieces = $this->getDataPieces();
        $indexPiece = is_countable($dataPieces) ? count($dataPieces) : 0;

        $nbrePiecesArajouter = 1;
        if (0 == $indexPiece) {
            $nbrePiecesArajouter = 1;
        }
        for ($i = 0; $i < $nbrePiecesArajouter; ++$i) {
            $dataPieces[$indexPiece]['pieceText'] = 'N° ';
            $dataPieces[$indexPiece]['intitulePiece'] = '';
            $dataPieces[$indexPiece]['numPiece'] = $indexPiece + 1;
            ++$indexPiece;
        }
        if ($nbrePiecesArajouter) {
            $this->remplirPieces($dataPieces);
        }
    }

    public function addPieceASigner($sender, $param)
    {
        $dataPieces = $this->getDataPieces();
        $indexPiece = is_countable($dataPieces) ? count($dataPieces) : 0;
        $nbrePiecesArajouter = 1;
        //$dataPieces[]['intitulePiece'] = '';
        for ($i = 0; $i < $nbrePiecesArajouter; ++$i) {
            $dataPieces[$indexPiece]['pieceText'] = 'N° ';
            $dataPieces[$indexPiece]['intitulePiece'] = '';
            $dataPieces[$indexPiece]['numPiece'] = $indexPiece + 1;
            //$dataPieces[$indexPiece]['deleteBtn'] = true ;
            ++$indexPiece;
        }

        $this->traceHistorique('DESCRIPTION16');
        $this->remplirPieces($dataPieces);
        $this->panelPiecesASigner->render($param->NewWriter);
    }

    public function getDataPieces()
    {
        $indexPiece = 0;
        $dataPieces = [];

        foreach ($this->listePiecesASigner->getItems() as $item) {
            $dataPieces[$item->getItemIndex()]['pieceText'] = Atexo_Util::utf8ToIso($item->pieceText->Text);
            $dataPieces[$item->getItemIndex()]['intitulePiece'] = Atexo_Util::utf8ToIso($item->intitulePiece->Text);
            $dataPieces[$item->getItemIndex()]['numPiece'] = $item->numPiece->Text;
            //$dataPieces[$item->getItemIndex()]['deleteBtn'] = $item->deleteBtn->Visible;
            ++$indexPiece;
        }//print_r($dataPieces);exit;

        return $dataPieces;
    }

    public function remplirPieces($dataPieces)
    {
        $this->listePiecesASigner->dataSource = $dataPieces;
        $this->listePiecesASigner->dataBind();
        $indexPiece = 0;
        foreach ($this->listePiecesASigner->getItems() as $item) {
            $item->pieceText->Text = $dataPieces[$indexPiece]['pieceText'];
            $item->intitulePiece->Text = $dataPieces[$indexPiece]['intitulePiece'];
            $item->numPiece->Text = $dataPieces[$indexPiece]['numPiece'];
            //$item->deleteBtn->Visible = $dataPieces[$indexPiece]['deleteBtn'] ;
            ++$indexPiece;
        }
        //$nombrePieces = count($dataPieces);
    }

    public function deleteItemPiece($sender, $param)
    {
        $finalData = [];
        $i = 0;
        if ('delete' == $param->CommandName) {
            $dataPieces = $this->getDataPieces();
            unset($dataPieces[$param->Item->ItemIndex]); //print_r($dataPieces);exit;
            foreach ($dataPieces as $un) {
                $finalData[$i]['pieceText'] = $un['pieceText'];
                $finalData[$i]['intitulePiece'] = $un['intitulePiece'];
                $finalData[$i]['numPiece'] = $i + 1;
                //$finalData[$i]['deleteBtn'] = $un['deleteBtn'];
                ++$i;
            }
            /*if($param->Item->ItemIndex = count($dataPieces)+1){
                $dataPieces[$param->Item->ItemIndex+1]['intitulePiece']='';
            }*/
            $this->remplirPieces($finalData);
        }
    }

    public function activeRefreshPieces($sender, $param)
    {
        $this->panelPiecesASigner->render($param->NewWriter);
    }

    public function RedirectPreRequis()
    {
        $url = Atexo_Util::getAbsoluteUrl().'index.php?page=Entreprise.PrerequisTechniques&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['org']).'&calledFrom=entreprise#rubrique_2_paragraphe_6';
        $this->response->redirect($url);
    }

    public function traceHistorique($description, $details = '')
    {
        $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $organisme = Atexo_Util::atexoHtmlEntities($_GET['org']);
        $dateDebAction = date('Y-m-d H:i:s');
        $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk($description, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $arrayDonnees = [];
        $arrayDonnees['ref'] = $ref;
        $arrayDonnees['org'] = $organisme;
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        if (!$details) {
            $details = '-';
        }
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            $details,
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
    }
}
