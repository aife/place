<?php

namespace Application\Pages\Entreprise;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Languages;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <anis.abid@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseModal extends MpeTPage
{
    public $controls = null;
    public $action = null;
    public $etablissement;

    public function onPreInit($param)
    {
        $this->controls = $_GET['controls'];
        $this->action = $_GET['action'];
    }

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('entreprise');

        $this->controls = $_GET['controls'];
        $this->action = $_GET['action'];
    }

    public function onLoad($param)
    {
        $this->controls = $_GET['controls'];

        $this->action = $_GET['action'];

        switch ($this->controls) {
            case 'CAtexoModalUserEtablissement':
                $idEtablissement = $_GET['idEtablissement'];

                if ('update' == $this->action and null != $idEtablissement) {
                    $this->AtexoModalUserEtablissement->setIdEtablissement($_GET['idEtablissement']);
                    $this->AtexoModalUserEtablissement->setVisible(true);
                    $this->AtexoModalUserEtablissement->updateAction();
                }

                if ('add' == $this->action and null != $idEtablissement) {
                    $this->AtexoModalUserEtablissement->setVisible(true);
                    $this->AtexoModalUserEtablissement->addAction();
                }

                if ('submit' == $this->action and null !== $idEtablissement) {
                    $this->AtexoModalUserEtablissement->setIdEtablissement($_GET['idEtablissement']);
                    $this->AtexoModalUserEtablissement->setVisible(false);
                    $this->AtexoModalUserEtablissement->submitAction();
                }

                break;

            case 'CAtexoModalLieuxExecutionCarte':
                if ('update' == $this->action) {
                    $this->AtexoModalLieuxExecutionCarte->setVisible(true);
                    $this->AtexoModalLieuxExecutionCarte->updateAction();
                }

                break;

            case 'CAtexoModalMyCart':
                if ('add' == $this->action) {
                    $this->AtexoModalMyCart->setVisible(true);
                    $this->AtexoModalMyCart->addAction();
                }

                break;

            case 'CAtexoModalDetailEntreprise':
                if ('submit' == $this->action) {
                    $this->AtexoModalDetailEntreprise->setVisible(true);
                    $this->AtexoModalDetailEntreprise->submitAction();
                }

                break;

            case 'CAtexoModalGestionUtilisateurs':
                if ('update' == $this->action) {
                    $this->AtexoModalGestionUtilisateurs->setVisible(true);
                    $this->AtexoModalGestionUtilisateurs->updateAction();
                }

                break;

            case 'CAtexoModalGestionPanier':
                if ('delete' == $this->action) {
                    $this->AtexoModalGestionPanier->setVisible(true);
                    $this->AtexoModalGestionPanier->deleteAction(false);
                }

                if ('deleteConfirmation' == $this->action) {
                    $this->AtexoModalGestionPanier->setVisible(false);
                    $this->AtexoModalGestionPanier->deleteAction(true);
                }

                break;
        }
    }

    public function isControls($controls)
    {
        if ($this->controls == $controls) {
            return true;
        }

        return false;
    }

    public function isView($controls, $action)
    {
        if ($this->controls == $controls and $this->action = $action) {
            return true;
        }

        return false;
    }
}
