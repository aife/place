<?php

namespace Application\Pages\Cron;

use Application\Controls\MpeTPage;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Prado\Prado;

/**
 * Page d'envoi de mail concernant les consultations clôturé.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class CronConsultationCloture extends MpeTPage
{
    public function onInit($param)
    {
        $this->Master->setCalledFrom('agent');
        Atexo_Languages::setLanguageCatalogue('agent');
    }

    public function onLoad($param)
    {
        $this->getConsultationCloture();
    }

    public function getConsultationCloture()
    {

		$commonConsultationQuery = new CommonConsultationQuery();
		$commonConsultations = $commonConsultationQuery->getConsultationsCloturees();
		if(is_array($commonConsultations)) {
			foreach($commonConsultations as $commonConsultation) {
				if($commonConsultation instanceof CommonConsultation) {
					$corpsMail = Atexo_Message::getContentMailForSynthesisAchievedConsultation($commonConsultation);
					$guests=Atexo_Consultation_Guests::getAllInvitedAgents($commonConsultation->getId(), $commonConsultation->getOrganisme());
					$params['consultationCloture'] = true;
                    $listIdAgents = Atexo_Consultation_Guests::listAgentConsultationFavoris($commonConsultation->getId(), $params);

					if(is_array($guests)) {
						 foreach($guests as $guest) {
							if(
                                $guest->getAlerteClotureConsultation() && $guest->getMailAgent()
                                && array_search($guest->getId(), $listIdAgents) !== false
                            ) {
								$replaced = array("VALEUR_NOM_DEST","VALEUR_PRENOM_DEST");
								$replacedBy = array($guest->getLastName(),$guest->getFirstName());
								$mail  = str_replace($replaced, $replacedBy,$corpsMail);
								$from = Atexo_Config::getParameter('PF_MAIL_FROM', $commonConsultation->getOrganisme());
								$pfName = null;

                                if ($commonConsultation instanceof CommonConsultation
                                    && $commonConsultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                                    $plateformeVirtuelle = $commonConsultation->getCommonPlateformeVirtuelle();
                                    $from = $plateformeVirtuelle->getNoReply();
                                    $pfName = $plateformeVirtuelle->getFromPfName();
                                }

                                $to = $guest->getMailAgent();
                                $subject = Prado::localize('DEFINE_TEXT_OBJET_MAIL_CONSULTATION_CLOTURE');
                                Atexo_Message::simpleMail(
                                    $from,
                                    $to,
                                    $subject,
                                    $mail,
                                    '',
                                    '',
                                    false,
                                    true,
                                    true,
                                    $pfName
                                );
                            }
                        }
                    }
                }
            }
        }

        exit;
    }
}
