-- FMA MPE-4959
CREATE TABLE IF NOT EXISTS `code_retour` (
  `code` INT NOT NULL ,
  `description` TEXT NOT NULL ,
  PRIMARY KEY (`code`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `echanges_interfaces`(
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code_retour` INT(11) NOT NULL,
  `service` VARCHAR(255) NOT NULL,
  `nom_batch` VARCHAR(255) NOT NULL,
  `variables_entree` LONGTEXT NULL DEFAULT NULL,
  `resultat` LONGTEXT NULL DEFAULT NULL,
  `type_flux` VARCHAR(255) DEFAULT NULL,
  `poids` INT(11) DEFAULT NULL,
  `information_metier` LONGTEXT NULL DEFAULT NULL,
  `nb_flux` INT(11) DEFAULT NULL,
  `debut_execution` DATETIME NOT NULL,
  `fin_execution` DATETIME NULL,
  `id_echanges_interfaces` INT(11) DEFAULT NULL,
  PRIMARY KEY(`id`),
  KEY `fk_code_retour`(`code_retour`),
  KEY `id_echanges_interfaces`(`id_echanges_interfaces`),
  CONSTRAINT `fi_id_echanges_interfaces` FOREIGN KEY(`id_echanges_interfaces`) REFERENCES `echanges_interfaces`(`id`),
  CONSTRAINT `fk_code_retour` FOREIGN KEY(`code_retour`) REFERENCES `code_retour`(`code`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = INNODB;

-- APE MPE-712
ALTER TABLE `t_etablissement` ADD COLUMN `tva_intracommunautaire` VARCHAR(20) NULL DEFAULT NULL AFTER `maj_long_lat`;

-- LEZ MPE-5421
ALTER TABLE `configuration_plateforme` ADD COLUMN `plateforme_editeur` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT 'Configuration RÉDAC permettant de définir l''environnement comme Lecteur du clausier éditeur (0) ou éditeur du clausier éditeur (1).';
-- LEZ MPE-5413
ALTER TABLE `HabilitationProfil`	ADD COLUMN `activer_version_clausier` ENUM('0','1') NULL DEFAULT '0';
ALTER TABLE `HabilitationAgent`	ADD COLUMN `activer_version_clausier` ENUM('0','1') NULL DEFAULT '0' ;

ALTER TABLE `echanges_interfaces` ADD COLUMN `nom_interface` VARCHAR(255) NULL DEFAULT NULL ;



-- FMA MPE-5618
ALTER TABLE `consultation` ADD COLUMN `marche_insertion` BOOLEAN NULL DEFAULT 0 AFTER `type_procedure_dume`;
ALTER TABLE `consultation` ADD COLUMN `clause_specification_technique` VARCHAR(255) NULL DEFAULT '0' AFTER `marche_insertion`;

CREATE TABLE IF NOT EXISTS `categories_considerations_sociales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `consultation` CHANGE `clause_sociale_condition_execution` `clause_sociale_condition_execution` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Le marché comprend des clauses sociales d\'insertion comme condition d\'exécution (article 14 du Code des marchés publics)', CHANGE `clause_sociale_insertion` `clause_sociale_insertion` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Présence parmi les critères d\'attribution d\'un critère relatif à l\'insertion (article 53 du code des marchés publics)', CHANGE `clause_sociale_ateliers_proteges` `clause_sociale_ateliers_proteges` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Le marché est réservé à des ateliers protégés (article 15 du code des marchés publics)', CHANGE `clause_sociale_siae` `clause_sociale_siae` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Le marché est réservé à  SIAE', CHANGE `clause_sociale_ess` `clause_sociale_ess` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Le marché est réservé à  ESS', CHANGE `clause_env_specs_techniques` `clause_env_specs_techniques` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'pécifications techniques (article 6 du code des marchés publics)', CHANGE `clause_env_cond_execution` `clause_env_cond_execution` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Conditions d\'exécution (article 14 du code des marchés publics)', CHANGE `clause_env_criteres_select` `clause_env_criteres_select` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Critère de sélection (article 53.1 du code des marchés publics)';

ALTER TABLE `CategorieLot` CHANGE `clause_sociale_condition_execution` `clause_sociale_condition_execution` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Le marché comprend des clauses sociales d\'insertion comme condition d\'exécution (article 14 du Code des marchés publics)', CHANGE `clause_sociale_insertion` `clause_sociale_insertion` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Présence parmi les critères d\'attribution d\'un critère relatif à l\'insertion (article 53 du code des marchés publics)', CHANGE `clause_sociale_ateliers_proteges` `clause_sociale_ateliers_proteges` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Le marché est réservé à des ateliers protégés (article 15 du code des marchés publics)', CHANGE `clause_sociale_siae` `clause_sociale_siae` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Le marché est réservé à SIAE', CHANGE `clause_sociale_ess` `clause_sociale_ess` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Le marché est réservé à ESS', CHANGE `clause_env_specs_techniques` `clause_env_specs_techniques` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'pécifications techniques (article 6 du code des marchés publics)', CHANGE `clause_env_cond_execution` `clause_env_cond_execution` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Conditions d\'exécution (article 14 du code des marchés publics)', CHANGE `clause_env_criteres_select` `clause_env_criteres_select` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' COMMENT 'Critère de sélection (article 53.1 du code des marchés publics)';
ALTER TABLE `CategorieLot` ADD COLUMN `marche_insertion` BOOLEAN NULL DEFAULT 0 AFTER `id_donnee_complementaire`;
ALTER TABLE `CategorieLot` ADD COLUMN `clause_specification_technique` VARCHAR(255) NULL DEFAULT '0' AFTER `marche_insertion`;

ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale` `clause_sociale` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `clause_sociale_condition_execution` `clause_sociale_condition_execution` VARCHAR(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `clause_sociale_insertion` `clause_sociale_insertion` VARCHAR(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `clause_sociale_ateliers_proteges` `clause_sociale_ateliers_proteges` VARCHAR(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `clause_sociale_siae` `clause_sociale_siae` VARCHAR(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT 'Le marché est réservé à SIAE', CHANGE `clause_sociale_ess` `clause_sociale_ess` VARCHAR(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT 'Le marché est réservé à ESS', CHANGE `clause_environnementale` `clause_environnementale` VARCHAR(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `clause_env_specs_techniques` `clause_env_specs_techniques` VARCHAR(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `clause_env_cond_execution` `clause_env_cond_execution` VARCHAR(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `clause_env_criteres_select` `clause_env_criteres_select` VARCHAR(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `marche_insertion` BOOLEAN NULL DEFAULT 0 AFTER `code_cpv_2`;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `clause_specification_technique` VARCHAR(255) NULL DEFAULT '0' AFTER `marche_insertion`;
