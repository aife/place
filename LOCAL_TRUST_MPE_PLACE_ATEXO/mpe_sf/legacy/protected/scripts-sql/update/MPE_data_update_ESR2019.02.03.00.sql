-- MWA MPE-9642
UPDATE consultation_archive_bloc AS t1 JOIN consultation_archive AS t2 ON t2.id = t1.consultation_archive_id  SET t1.comp_id = t2.consultation_ref;
UPDATE consultation_archive_bloc AS t1 JOIN consultation_archive_atlas AS t2 ON t2.doc_id= t1.doc_id  SET t1.comp_id = t2.comp_id;
