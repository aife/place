-- MWA: optimisation DB MPE-4463
ALTER TABLE `consultation` ADD INDEX `Idx_Consultation_type_acces` (`type_acces`);
ALTER TABLE `consultation` ADD INDEX `Idx_Consultation_consultation_annulee` (`consultation_annulee`);

ALTER TABLE `Organisme` ADD INDEX `Idx_Organisme_active` (`active`);

ALTER TABLE `Entreprise` ADD INDEX `Idx_Entreprise_siren` (`siren`(9));

ALTER TABLE `t_bourse_cotraitance` ADD INDEX `Idx_bourse_cotraitance_reference_consultation` (`reference_consultation`);
ALTER TABLE `t_bourse_cotraitance` ADD INDEX `Idx_bourse_cotraitance_id_entreprise` (`id_entreprise`);
ALTER TABLE `t_bourse_cotraitance` ADD INDEX `Idx_bourse_cotraitance_id_etablissement_inscrite` (`id_etablissement_inscrite`);