-- MPE-11566


ALTER TABLE fichierEnveloppe DROP FOREIGN KEY IF EXISTS FK_fichierEnveloppe_blobOrganismeFile;
ALTER TABLE Offres DROP FOREIGN KEY IF EXISTS FK_Offres_blobOrganismeFile;
ALTER TABLE Offres DROP FOREIGN KEY IF EXISTS offres_id_blob_xml_reponse;



ALTER TABLE fichierEnveloppe ADD CONSTRAINT FK_fichierEnveloppe_blobOrganismeFile FOREIGN KEY (id_blob) REFERENCES blobOrganisme_file (id) ON DELETE NO ACTION;
ALTER TABLE Offres ADD CONSTRAINT FK_Offres_blobOrganismeFile FOREIGN KEY (id_blob_horodatage_hash) REFERENCES blobOrganisme_file (id) ON DELETE NO ACTION;
ALTER TABLE `Offres` ADD CONSTRAINT `offres_id_blob_xml_reponse` FOREIGN KEY ( `id_blob_xml_reponse` ) REFERENCES `blobOrganisme_file` (`id`) ON DELETE NO ACTION;