ALTER TABLE `Entreprise`	ADD COLUMN `id_externe` VARCHAR(50) NOT NULL DEFAULT '0';
ALTER TABLE `Inscrit`	ADD COLUMN `id_externe` VARCHAR(50) NOT NULL DEFAULT '0';
ALTER TABLE `t_etablissement`	ADD COLUMN `id_externe` VARCHAR(50) NOT NULL DEFAULT '0';
ALTER TABLE `Organisme` ADD COLUMN `id_externe` VARCHAR(50) NOT NULL DEFAULT '0';


ALTER TABLE `CertificatChiffrement` CHANGE `consultation_ref` `consultation_ref` INT(11) NULL DEFAULT '0';

-- FMA MPE-10676
ALTER TABLE `configuration_plateforme` ADD COLUMN `messagerie_v2` BOOLEAN NOT NULL DEFAULT '0' COMMENT 'active la messagerie sécurisé v2' AFTER `masquer_atexo_sign`;

-- FMA MPE-10650
ALTER TABLE `consultation` ADD COLUMN `version_messagerie` INT(11) NOT NULL DEFAULT '1' COMMENT 'version de la messagerie' AFTER `attestation_consultation`;

-- FMA MPE-10648
CREATE TABLE IF NOT EXISTS `mail_type_group` (
    id INT AUTO_INCREMENT NOT NULL,
    label VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `mail_type` (
    id INT AUTO_INCREMENT NOT NULL,
    mail_type_group_id INT NOT NULL,
    label VARCHAR(255) NOT NULL,
    code VARCHAR(255) NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT mail_type_mail_type_group_id FOREIGN KEY (mail_type_group_id) REFERENCES mail_type_group (id)
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `mail_template` (
    id INT AUTO_INCREMENT NOT NULL,
    mail_type_id INT NOT NULL,
    code VARCHAR(255) NOT NULL,
    titre VARCHAR(255) NOT NULL,
    corps LONGTEXT NOT NULL,
    ordre_affichage INT NULL,
    envoi_modalite VARCHAR(255) NOT NULL DEFAULT 'AVEC_AR' COMMENT 'AVEC_AR | SANS_AR',
    envoi_modalite_figee BOOLEAN NOT NULL DEFAULT '0',
    reponse_attendue BOOLEAN NOT NULL DEFAULT '0',
    reponse_attendue_figee BOOLEAN NOT NULL DEFAULT '0',
    PRIMARY KEY(id),
    CONSTRAINT mail_template_mail_type_id FOREIGN KEY (mail_type_id) REFERENCES mail_type (id)
)
ENGINE = InnoDB;

ALTER TABLE `mail_template` CHANGE COLUMN `titre` `objet` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `mail_type` ADD UNIQUE (code);
ALTER TABLE `mail_template` ADD UNIQUE (code);