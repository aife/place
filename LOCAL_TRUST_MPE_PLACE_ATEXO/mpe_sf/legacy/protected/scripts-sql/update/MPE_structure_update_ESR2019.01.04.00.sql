-- on garde les deux lignes car le champ a déjà été créé sur certaines PF (intégration)
-- NBS-MPE-10040
ALTER TABLE Enveloppe ADD COLUMN date_debut_dechiffrement VARCHAR(20) NOT NULL DEFAULT '0000-00-00 00:00:00';

-- NBS MPE-9829
ALTER TABLE Enveloppe MODIFY COLUMN date_debut_dechiffrement DATETIME NULL;

-- FMA MPE-10364
ALTER TABLE ProcedureEquivalence ADD COLUMN donnees_complementaire_non VARCHAR(2) NOT NULL DEFAULT '+0';
ALTER TABLE ProcedureEquivalence ADD COLUMN donnees_complementaire_oui VARCHAR(2) NOT NULL DEFAULT '+1';
