-- LEZ MPE-5113
ALTER TABLE `t_support_publication`	ADD COLUMN `detail_info` TEXT NULL DEFAULT NULL ;
ALTER TABLE `t_support_publication` ADD COLUMN `type_info` CHAR(1) NULL DEFAULT NULL COMMENT 'Designe le type d''info ,1=>texte ,2=>image' ;
ALTER TABLE `t_support_publication`	ADD COLUMN `affichage_infos` ENUM('0','1') NULL DEFAULT '0' COMMENT 'permet d''afficher ou nn le detail d''info du support' ;

-- APE MPE-4795
CREATE TABLE IF NOT EXISTS `t_dume_contexte` (
   `id` INT(11) NOT NULL AUTO_INCREMENT ,
   `ref_consultation` INT(11) NOT NULL ,
   `organisme` VARCHAR(45) NOT NULL ,
   `contexte_lt_dume_id` VARCHAR(255) NULL ,
   `type_dume` VARCHAR(45) NULL ,
   `status` INT(2) NOT NULL DEFAULT 99 ,
   PRIMARY KEY  (`id`),
   CONSTRAINT `FK_t_dume_contexte_consultation` FOREIGN KEY (`ref_consultation`) REFERENCES `consultation` (`reference`) ON UPDATE CASCADE ON DELETE CASCADE,
   CONSTRAINT `FK_t_dume_contexte_organisme` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `t_dume_numero` (
   `id` INT(11) NOT NULL AUTO_INCREMENT ,
   `numero_dume_national` VARCHAR(100) NULL ,
   `blob_id` INT(11) NULL ,
   `list_lot` VARCHAR(255) NULL ,
   `id_dume_contexte` INT(11) NULL ,
   PRIMARY KEY  (`id`),
   CONSTRAINT `FK_t_dume_contexte_numero_dume_contexte` FOREIGN KEY (`id_dume_contexte`) REFERENCES `t_dume_contexte` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `t_candidature` (
   `id` INT(11) NOT NULL AUTO_INCREMENT ,
   `ref_consultation` INT(11) NOT NULL ,
   `organisme` VARCHAR(45) NOT NULL ,
   `id_inscrit` INT(11) NOT NULL ,
   `id_entreprise` INT(11) NOT NULL ,
   `id_etablissement` INT(11) NOT NULL ,
   `status` INT(2) NOT NULL DEFAULT '99' ,
   `type_candidature` VARCHAR(45) NULL ,
   `type_candidature_dume` VARCHAR(45) NULL ,
   `id_dume_contexte` INT(11) NULL ,
   `id_offre` INT(11) NULL ,
   PRIMARY KEY  (`id`),
   CONSTRAINT `FK_t_candidature_consultation` FOREIGN KEY (`ref_consultation`) REFERENCES `consultation` (`reference`) ON UPDATE CASCADE ON DELETE CASCADE,
   CONSTRAINT `FK_t_candidature_organisme` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON UPDATE CASCADE ON DELETE CASCADE,
   CONSTRAINT `FK_t_candidature_dume_contexte` FOREIGN KEY (`id_dume_contexte`) REFERENCES `t_dume_contexte` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
   CONSTRAINT `FK_t_candidature_offres` FOREIGN KEY (`id_offre`) REFERENCES `Offres` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `t_liste_lots_candidature` (
   `id` INT(11) NOT NULL AUTO_INCREMENT ,
   `ref_consultation` INT(11) NOT NULL ,
   `organisme` VARCHAR(45) NOT NULL ,
   `id_inscrit` INT(11) NOT NULL ,
   `id_entreprise` INT(11) NOT NULL ,
   `id_etablissement` INT(11) NOT NULL ,
   `status` INT(2) NOT NULL DEFAULT '99' ,
   `id_candidature` INT(11) NULL ,
   `num_lot` INT(11) NOT NULL ,
   PRIMARY KEY  (`id`),
   CONSTRAINT `FK_t_liste_lots_candidature_consultation` FOREIGN KEY (`ref_consultation`) REFERENCES `consultation` (`reference`) ON UPDATE CASCADE ON DELETE CASCADE,
   CONSTRAINT `FK_t_liste_lots_candidature_organisme` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON UPDATE CASCADE ON DELETE CASCADE,
   CONSTRAINT `FK_t_liste_lots_candidature_candidature` FOREIGN KEY (`id_candidature`) REFERENCES `t_candidature` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB;

ALTER TABLE `consultation` ADD COLUMN `dume_demande` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de savoir si une Consultation permet la candidature DUME';
ALTER TABLE `consultation` ADD COLUMN `type_procedure_dume` INT(11) NOT NULL DEFAULT '0' COMMENT 'type procedure dume lié';
ALTER TABLE `t_access_token` CHANGE `user_id` `user_id` INT(11) NULL;
ALTER TABLE `t_dume_contexte` ADD COLUMN `is_standard` BOOLEAN NOT NULL DEFAULT TRUE AFTER `status`;
ALTER TABLE `t_dume_contexte` ADD COLUMN `date_creation` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `is_standard`;
ALTER TABLE `t_dume_contexte` ADD COLUMN `date_modification` TIMESTAMP NULL AFTER `date_creation`;



-- AME MPE-4729
CREATE TABLE `t_type_procedure_dume` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`libelle` VARCHAR(255) NOT NULL DEFAULT '',
	`active` ENUM('0','1') NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB;

CREATE TABLE `t_procedure_equivalence_dume` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_type_procedure` INT(11) NOT NULL DEFAULT '0',
	`organisme` VARCHAR(50) NOT NULL DEFAULT '',
	`id_type_procedure_dume` INT(11) NOT NULL DEFAULT '0',
	`afficher` ENUM('0','1') NOT NULL DEFAULT '0',
	`figer` ENUM('0','1') NOT NULL DEFAULT '0',
	`selectionner` ENUM('0','1') NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB;

ALTER TABLE `configuration_plateforme` ADD COLUMN `interface_dume` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le DUME' ;
ALTER TABLE `ProcedureEquivalence` ADD COLUMN `dume_demande` VARCHAR(2) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD COLUMN `type_procedure_dume` VARCHAR(2) NOT NULL DEFAULT '-0';


-- AME MPE-4801
ALTER TABLE `consultation` ADD COLUMN `dume_demande` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de savoir si une Consultation permet la candidature DUME' ;
ALTER TABLE `consultation` ADD COLUMN `type_procedure_dume` INT(11) NOT NULL DEFAULT '0' COMMENT 'type procedure dume lié';



-- AME MPE-5146
ALTER TABLE `t_type_procedure_dume`	ADD COLUMN `code_dume` VARCHAR(50) NULL;

-- AME MPE-5243
ALTER TABLE `CategorieConsultation`	ADD COLUMN `code_dume` VARCHAR(50) NULL;
ALTER TABLE `t_type_contrat`	ADD COLUMN `code_dume` VARCHAR(50) NULL;

ALTER TABLE `t_dume_numero`	ADD COLUMN `date_recuperation_pdf` DATETIME NULL AFTER `id_dume_contexte`;

-- LEZ MPE-5440
ALTER TABLE `Offres`ADD COLUMN `resultat_verification_hash_all_files` CHAR(1) NULL DEFAULT '1' COMMENT 'Contient le resultat de la vérification des hash des fichiers, 0=>KO(au moins un fichier non valide),1=>OK';
ALTER TABLE `Enveloppe` ADD COLUMN `resultat_verification_hash_files` CHAR(1) NULL DEFAULT '1' COMMENT 'Contient le resultat de la vérification des hash des fichiers, 0=>KO(au moins un fichier non valide),1=>OK' ;
ALTER TABLE `fichierEnveloppe` ADD COLUMN `resultat_verification_hash` CHAR(1) NULL DEFAULT '1' COMMENT 'Contient le resultat de la vérification du hash du fichier, 0=>KO,1=>OK' ;
