-- MBL
UPDATE ValeurReferentiel SET libelle_2 = '1;2;24;25;9;18;26' WHERE libelle_valeur_referentiel = 'Consultations' and id_referentiel=161;

-- Mise a jour de la table `t_type_contrat`
UPDATE `t_type_contrat` SET `type_contrat_statistique`='3' WHERE  `id_type_contrat`=2;
UPDATE `t_type_contrat` SET `type_contrat_statistique`='4' WHERE  `id_type_contrat`=3;
UPDATE `t_type_contrat` SET `type_contrat_statistique`='5' WHERE  `id_type_contrat`=4;
UPDATE `t_type_contrat` SET `type_contrat_statistique`='5' WHERE  `id_type_contrat`=8;
UPDATE `t_type_contrat` SET `type_contrat_statistique`='1' WHERE  `id_type_contrat`=7;
UPDATE `t_type_contrat` SET `type_contrat_statistique`='3' WHERE  `id_type_contrat`=5;
UPDATE `t_type_contrat` SET `type_contrat_statistique`='5' WHERE  `id_type_contrat`=9;
UPDATE `t_type_contrat` SET `type_contrat_statistique`='5' WHERE  `id_type_contrat`=11;
UPDATE `t_type_contrat` SET `type_contrat_statistique`='5' WHERE  `id_type_contrat`=12;
UPDATE `t_type_contrat` SET `type_contrat_statistique`='5' WHERE  `id_type_contrat`=10;

-- LEZ MPE-4161
INSERT INTO `ValeurReferentiel` (`id` ,`id_referentiel` ,`libelle_valeur_referentiel` ,`libelle_valeur_referentiel_fr` ,`libelle_2`) VALUES (5 , '161', 'Contrats', 'Contrats',  '27;28;29;30;31');

UPDATE ValeurReferentiel SET libelle_2 = '0;1;2;24;25;9;18;26' WHERE libelle_valeur_referentiel = 'Consultations' and id_referentiel=161;
UPDATE ValeurReferentiel SET libelle_2 = '0;5;17;6;7;8;23' WHERE libelle_valeur_referentiel = 'DCE' and id_referentiel=161;
UPDATE ValeurReferentiel SET libelle_2 = '0;3;4' WHERE libelle_valeur_referentiel = 'Publicités' and id_referentiel=161;
UPDATE ValeurReferentiel SET libelle_2 = '0;10;11;12;19;20;21' WHERE libelle_valeur_referentiel = 'Réponses électroniques' and id_referentiel=161;

-- AME MPE-4101
INSERT INTO `t_document_type` (`id_type_document`, `nom_type_document`, `code`, `type_doc_entreprise_etablissement`, `uri`, `params_uri`, `class_name`, `nature_document`, `type_retour_ws`) VALUES (11, 'Attestation AGEFIPH', 'attestations_agefiph', '2', '/attestations_agefiph/', 'context=MPS, recipient={__recipient__}, object={__object__}', '\\Atexo\\WebServiceApiGouvEntreprise\\AttestationAgefiphWebService', '2', '2');

-- LEZ MPE-4196
INSERT INTO `t_document_type` (`id_type_document`, `nom_type_document`) VALUES (10, 'Bordereau d''archive');
UPDATE `t_document` SET id_type_document = 10 where id_type_document = 4;
UPDATE `t_document_type` SET `synchro_actif`='0' WHERE  `id_type_document`=10;
UPDATE `t_document_type` SET `afficher_type_doc`='0' WHERE  `id_type_document`=10;

-- LEZ MPE-4446
UPDATE `t_document_type` SET `nature_document`='2' WHERE `id_type_document` = 10;

-- AME MPE-4532
UPDATE t_document_entreprise SET nom_document = replace(nom_document, 'Attestation_de_vigilence', 'Attestation_de_vigilance') WHERE id_type_document = 3;

