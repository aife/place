-- AME MPE-11197
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = 'Consortium representative' WHERE `ValeurReferentiel`.`id` = 1 AND `ValeurReferentiel`.`id_referentiel` = 13;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = 'Co-contractor with joint and several liability' WHERE `ValeurReferentiel`.`id` = 2 AND `ValeurReferentiel`.`id_referentiel` = 13;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = 'Co-contractor with joint liability only ' WHERE `ValeurReferentiel`.`id` = 3 AND `ValeurReferentiel`.`id_referentiel` = 13;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = 'Subcontractor' WHERE `ValeurReferentiel`.`id` = 4 AND `ValeurReferentiel`.`id_referentiel` = 13;
