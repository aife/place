CREATE TABLE IF NOT EXISTS `consultation_archive_atlas` (
    id INT AUTO_INCREMENT NOT NULL,
    doc_id VARCHAR(255) NOT NULL,
    numero_bloc INT NOT NULL,
    comp_id VARCHAR(255) NOT NULL,
    consultation_ref INT NOT NULL,
    organisme VARCHAR(255) NOT NULL,
    taille INT NOT NULL,
    date_envoi DATETIME NOT NULL,
    PRIMARY KEY(id)
)
ENGINE = InnoDB;

ALTER TABLE `consultation_archive_atlas` ADD INDEX `reference_organisme` (`consultation_ref`, `organisme`);

CREATE TABLE IF NOT EXISTS `consultation_archive` (
    id INT AUTO_INCREMENT NOT NULL,
    organisme VARCHAR(30) NOT NULL,
    consultation_ref INT NOT NULL,
    chemin_fichier VARCHAR(255) NULL,
    date_archivage DATETIME DEFAULT NULL,
    poids_archivage INT DEFAULT NULL,
    annee_creation_consultation INT DEFAULT NULL,
    status_global_transmission VARCHAR(50) NOT NULL,
    status_fragmentation  TINYINT(1) DEFAULT '0',
    nombre_bloc INT NULL,
    PRIMARY KEY(id)
)
ENGINE=InnoDB;
ALTER TABLE `consultation_archive` ADD INDEX `status_global_transmission` (`status_global_transmission`(12));
ALTER TABLE `consultation_archive` ADD KEY `consultation_archive_consultation` (`organisme`,`consultation_ref`);
ALTER TABLE `consultation_archive` ADD KEY `consultation_archive_status_fragmentation` (`status_fragmentation`);
ALTER TABLE `consultation_archive` ADD UNIQUE `unicite_consultation` (`consultation_ref`, `organisme`);

CREATE TABLE  IF NOT EXISTS `consultation_archive_bloc` (
    id INT AUTO_INCREMENT NOT NULL,
    consultation_archive_id INT NOT NULL,
    doc_id  VARCHAR(255) NOT NULL,
    chemin_fichier  VARCHAR(255) NULL,
    numero_bloc INT NULL,
    poids_bloc INT NULL,
    date_envoi_debut DATETIME NULL,
    date_envoi_fin DATETIME NULL,
    status_transmission TINYINT(1) DEFAULT '0' NOT NULL,
    erreur VARCHAR(255) NULL,
    INDEX IDX_EFDAB250179E9C92 (consultation_archive_id),
    PRIMARY KEY(id)
)
ENGINE=InnoDB;
ALTER TABLE `consultation_archive_bloc` ADD CONSTRAINT FK_EFDAB250179E9C92 FOREIGN KEY (consultation_archive_id) REFERENCES consultation_archive (id);
ALTER TABLE `consultation_archive_bloc` ADD UNIQUE(`doc_id`);

CREATE TABLE IF NOT EXISTS `consultation_archive_fichier` (
    id INT AUTO_INCREMENT NOT NULL,
    organisme VARCHAR(30) NOT NULL,
    consultation_ref INT NOT NULL,
    chemin_fichier VARCHAR(255) NULL,
    poids INT,
    PRIMARY KEY(id)
)
ENGINE=InnoDB;
ALTER TABLE `consultation_archive_fichier` ADD INDEX `reference_organisme` (`consultation_ref`, `organisme`);

ALTER TABLE `Offres` ADD COLUMN  `id_blob_xml_reponse` INT(11) NULL DEFAULT NULL;
ALTER TABLE `Offres` ADD CONSTRAINT `offres_id_blob_xml_reponse` FOREIGN KEY ( `id_blob_xml_reponse` ) REFERENCES `blobOrganisme_file` (`id`);

ALTER TABLE `blobOrganisme_file` ADD COLUMN `dossier` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.';
ALTER TABLE `blob_file` ADD COLUMN `dossier` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.';
