-- MWA MPE-3088
ALTER TABLE `t_contrat_titulaire` ADD INDEX `Idx_contrat_num_EJ` (`num_EJ`);
ALTER TABLE `t_contrat_titulaire` ADD INDEX `Idx_contrat_statut_EJ` (`statutEJ`(30000));
-- LEZ MPE-4130
ALTER TABLE `Chorus_groupement_achat` ADD COLUMN `id_service` INT(11) NULL DEFAULT '0' ;
ALTER TABLE `Chorus_groupement_achat`DROP PRIMARY KEY,	ADD PRIMARY KEY (`id`,`organisme`,`id_oa`,`id_service`);

-- LEZ MPE-4127
ALTER TABLE `Chorus_Fiche_Navette` ADD COLUMN `nom_acheteur` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Le nom de l''acheteur à ''origine de l''envoi';
ALTER TABLE `Chorus_Fiche_Navette` ADD COLUMN `intitule_marche` VARCHAR(255) NULL DEFAULT NULL COMMENT 'L''intitulé du contrat' ;
ALTER TABLE `Chorus_Fiche_Navette` ADD COLUMN `raison_sociale` TEXT NULL DEFAULT NULL COMMENT 'La raison sociale du titulaire du contrat ' ;

-- LEZ MPE-4131
ALTER TABLE `HabilitationAgent`	ADD COLUMN `gerer_OA_GA` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer la relation OA/GA pour chorus' ;
ALTER TABLE `HabilitationProfil` ADD COLUMN `gerer_OA_GA` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer la relation OA/GA pour chorus' ;

-- LEZ MPE-4605
ALTER TABLE `T_MesRecherches`	ADD COLUMN `type_avis` INT(2) NULL DEFAULT '3' ;

-- LEZ MPE-4154
ALTER TABLE `t_bourse_cotraitance`	ADD COLUMN `sous_traitant` ENUM('0','1') NULL DEFAULT '0';

-- LEZ MPE-4332
ALTER TABLE `t_message_accueil`	ADD COLUMN `authentifier` INT(1) NULL DEFAULT NULL COMMENT '0=>non authentifié(e) ,1=>authentifié(e)' ;
ALTER TABLE `t_message_accueil`	ADD COLUMN `config` VARCHAR(255) NULL DEFAULT '' COMMENT 'on l''utilise dans le cas que plusieurs pf utilise la mème base de donnée (cas : PLACE et ORME)' ;

-- LEZ MPE-4324
ALTER TABLE `HabilitationAgent`	ADD COLUMN `deplacer_service` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet de gérer le déplacement d''un service vers un autre service' ;
ALTER TABLE `HabilitationProfil` ADD COLUMN `deplacer_service` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet de gérer le déplacement d''un service vers un autre service' ;

CREATE TABLE IF NOT EXISTS `t_fusionner_services` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_service_source` INT(11) NULL DEFAULT NULL,
	`id_service_cible` INT(11) NULL DEFAULT NULL,
	`organisme` VARCHAR(50) NULL DEFAULT NULL,
	`id_agent` INT(11) NULL DEFAULT NULL,
	`date_creation` DATETIME NULL DEFAULT NULL,
	`date_fusion` DATETIME NULL DEFAULT NULL,
	`donnees_fusionnees` ENUM('0','1') NULL DEFAULT '0' COMMENT '0=> données pas encore fusionnées, 1=> données sont fusionnées',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


-- LEZ MPE-4679
ALTER TABLE `Agent`	ADD COLUMN `date_connexion` DATETIME NULL DEFAULT NULL COMMENT 'contient la date de la dernière connexion';