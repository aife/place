-- NKN MPE-8192
UPDATE Inscrit i left JOIN t_etablissement e ON e.id_etablissement = i.id_etablissement SET i.id_etablissement = NULL WHERE e.id_etablissement IS NULL;
ALTER TABLE `Inscrit` ADD CONSTRAINT `inscrit_etablissement_fk` FOREIGN KEY (`id_etablissement`) REFERENCES `t_etablissement`(`id_etablissement`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- NKN MPE-8224
ALTER TABLE `Echange` ADD CONSTRAINT `echange_dossier_volumineux_fk` FOREIGN KEY (`id_dossier_volumineux`) REFERENCES `dossier_volumineux` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE `consultation_favoris` ( `id` INT NOT NULL AUTO_INCREMENT , `id_consultation` INT NOT NULL , `id_agent` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `consultation_favoris` ADD CONSTRAINT `consultation_favoris_id_consultation_FK` FOREIGN KEY (`id_consultation`) REFERENCES `consultation` (`reference`)  ON DELETE CASCADE;
ALTER TABLE `consultation_favoris` ADD CONSTRAINT `consultation_favoris_id_agent_FK` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`) ON DELETE CASCADE;
