-- MWA MPE-11066
--
-- Structure de la table `archive_arcade`
--

CREATE TABLE `archive_arcade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annee` int(11) NOT NULL,
  `num_semaine` int(11) NOT NULL,
  `poids_archive` int(11) DEFAULT NULL,
  `chemin_fichier` varchar(255) DEFAULT NULL,
  `date_envoi_debut` DATETIME NULL,
  `date_envoi_fin` DATETIME NULL,
  `status_transmission` tinyint(1) DEFAULT '0' NOT NULL,
  `erreur` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `archive_arcade` ADD UNIQUE `IDX_ANNNEE_SEMAINE` (`annee`, `num_semaine`);
--
-- Structure de la table `consultation_archive_arcade`
--

CREATE TABLE `consultation_archive_arcade` (
  `consultation_archive_id` int(11) NOT NULL,
  `archive_arcade_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour la table `consultation_archive_arcade`
--
ALTER TABLE `consultation_archive_arcade`
  ADD PRIMARY KEY (`consultation_archive_id`),
  ADD KEY `archive_arcade_id` (`archive_arcade_id`);

--
-- Contraintes pour la table `consultation_archive_arcade`
--
ALTER TABLE `consultation_archive_arcade`
  ADD CONSTRAINT `FK_archive_arcade_id` FOREIGN KEY (`archive_arcade_id`) REFERENCES `archive_arcade` (`id`),
  ADD CONSTRAINT `FK_consultation_archive_id` FOREIGN KEY (`consultation_archive_id`) REFERENCES `consultation_archive` (`id`);