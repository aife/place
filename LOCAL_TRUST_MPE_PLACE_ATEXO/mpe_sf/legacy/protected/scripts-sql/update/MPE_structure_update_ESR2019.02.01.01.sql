-- FMA MPE-9466
ALTER TABLE `blob_file` ADD COLUMN `statut_synchro` INT(11) NOT NULL DEFAULT '0' COMMENT '0 : non vérifié / 1 : Vérification ok / 2 : fichier absent';
ALTER TABLE `blob_file` ADD COLUMN `hash` varchar (255) NULL DEFAULT 'ND' COMMENT 'Empreinte du blob, SHA1 du fichier / ND : Valeur par défaut pour non définit';

ALTER TABLE `blobOrganisme_file` ADD COLUMN `statut_synchro` INT(11) NOT NULL DEFAULT '0' COMMENT '0 : non vérifié / 1 : Vérification ok / 2 : fichier absent';
ALTER TABLE `blobOrganisme_file` ADD COLUMN `hash` varchar(255)  NULL DEFAULT 'ND' COMMENT 'Empreinte du blob, SHA1 du fichier / ND : Valeur par défaut pour non définit';

ALTER TABLE `blobOrganisme_file` ADD INDEX `blob_organisme_file_statut_synchro` (`statut_synchro`);
ALTER TABLE `blob_file` ADD INDEX `blob_file_statut_synchro` (`statut_synchro`);

ALTER TABLE `echanges_interfaces` DROP FOREIGN KEY `fi_id_echanges_interfaces`;

ALTER TABLE `echanges_interfaces`
    ADD CONSTRAINT `fk_id_echanges_interfaces` FOREIGN KEY (`id_echanges_interfaces`)
        REFERENCES `echanges_interfaces` (`id`)
        ON DELETE CASCADE;

UPDATE `t_type_contrat` SET concession = 1 WHERE id_type_contrat = 4;

ALTER TABLE `fichierEnveloppe` CHANGE `signature_fichier` `signature_fichier` MEDIUMTEXT NULL DEFAULT NULL;

ALTER TABLE `Offres` ADD COLUMN  `id_blob_xml_reponse` INT(11) NULL DEFAULT NULL;
ALTER TABLE `Offres` ADD CONSTRAINT `offres_id_blob_xml_reponse` FOREIGN KEY ( `id_blob_xml_reponse` ) REFERENCES `blobOrganisme_file` (`id`);

ALTER TABLE `blobOrganisme_file` ADD COLUMN `dossier` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.';
ALTER TABLE `blob_file` ADD COLUMN `dossier` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.';

ALTER TABLE `Offres` CHANGE `xml_string` `xml_string` LONGTEXT NULL DEFAULT NULL;


-- Suppression des tables blobOrganisme et blob
-- la bdd doit être arretée et la plateforme également

-- ATTENTION AVANT DE FAIRE LE RESTE VERIFIER QUE LES DEUX ID ONT LA MEME VALEUR
-- SELECT max(id) as max_file FROM `blobOrganisme` WHERE 1 UNION ALL SELECT max(id) as max_file FROM `blobOrganisme_file` WHERE 1;


ALTER TABLE `DocumentExterne` DROP FOREIGN KEY IF EXISTS `DOC_EX_Blob`;
ALTER TABLE `dossier_volumineux` DROP FOREIGN KEY IF EXISTS `dossier_volumineux_id_blob_descripteur_FK`;
ALTER TABLE `dossier_volumineux` DROP FOREIGN KEY IF EXISTS `dossier_volumineux_id_blob_logfile_FK`;
ALTER TABLE `Offres` DROP FOREIGN KEY IF EXISTS `offres_id_blob_xml_reponse`;

-- table `blobOrganisme_file`
-- rendre l''id auto incremental

ALTER TABLE `blobOrganisme_file` DROP PRIMARY KEY;
ALTER TABLE `blobOrganisme_file` CHANGE COLUMN `id` `old_id` int(11) NULL DEFAULT NULL;;
ALTER TABLE `blobOrganisme_file` ADD COLUMN `id` INT(11) PRIMARY KEY AUTO_INCREMENT;
ALTER TABLE `blobOrganisme_file` ADD INDEX `blob_organisme_file_old_id` (`old_id`);

ALTER TABLE `blob_file` DROP PRIMARY KEY;
ALTER TABLE `blob_file` CHANGE COLUMN `id` `old_id` int(11) NULL DEFAULT NULL;;
ALTER TABLE `blob_file` ADD COLUMN `id` INT(11) PRIMARY KEY AUTO_INCREMENT;
ALTER TABLE `blob_file` ADD INDEX `blob_file_old_id` (`old_id`);

ALTER TABLE `dossier_volumineux` ADD CONSTRAINT `dossier_volumineux_id_blob_descripteur_FK` FOREIGN KEY IF NOT EXISTS (`id_blob_descripteur`) REFERENCES `blob_file` (`id`);
ALTER TABLE `dossier_volumineux` ADD CONSTRAINT `dossier_volumineux_id_blob_logfile_FK` FOREIGN KEY IF NOT EXISTS (`id_blob_logfile`) REFERENCES `blob_file` (`id`);

-- ajout clé externe vers blobOrganisme_file
ALTER TABLE `DocumentExterne` ADD CONSTRAINT `DOC_EX_Blob` FOREIGN KEY (`idBlob`) REFERENCES `blobOrganisme_file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- table fichierEnveloppe
-- ajout clé externe vers la table blobOrganisme_file
ALTER TABLE fichierEnveloppe ADD CONSTRAINT FK_fichierEnveloppe_blobOrganismeFile FOREIGN KEY (id_blob) REFERENCES blobOrganisme_file (id);
ALTER TABLE Offres ADD CONSTRAINT FK_Offres_blobOrganismeFile FOREIGN KEY (id_blob_horodatage_hash) REFERENCES blobOrganisme_file (id);
ALTER TABLE `Offres` ADD CONSTRAINT `offres_id_blob_xml_reponse` FOREIGN KEY ( `id_blob_xml_reponse` ) REFERENCES `blobOrganisme_file` (`id`);

ALTER TABLE blobOrganisme_file DROP revision;
ALTER TABLE blob_file DROP revision;

DROP TABLE `blobOrganisme`;
DROP TABLE `blob`;

-- FMA-MPE-8162
ALTER TABLE `Organisme` ADD COLUMN `tag_purge` BOOLEAN NULL DEFAULT FALSE COMMENT 'Champs qui permet de définir si cette organisme doit être purger (0: on ne faite rien ,1: on purge)';
