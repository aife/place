-- AME MPE-5146 MPE-5243
TRUNCATE `t_type_procedure_dume`;

INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `code_dume`) VALUES (1, 'Procédure adaptée', '01');
INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `code_dume`) VALUES (2, 'Appel d''offre ouvert', '02');
INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `code_dume`) VALUES (3, 'Appel d''offre restreint', '03');
INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `code_dume`) VALUES (4, 'Procédure concurrentielle avec négociation', '04');
INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `code_dume`) VALUES (5, 'Procédure négociée  avec mise en concurrence préalable', '05');
INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `code_dume`) VALUES (6, 'Marché négocié sans publicité ni mise en concurrence préalable', '06');
INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `code_dume`) VALUES (7, 'Dialogue compétitif', '07');
INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `code_dume`) VALUES (8, 'Concours', '08');
INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `code_dume`) VALUES (9, 'Autres', '09');


UPDATE `CategorieConsultation` SET `code_dume`='01' WHERE  `id`='1';
UPDATE `CategorieConsultation` SET `code_dume`='02' WHERE  `id`='2';
UPDATE `CategorieConsultation` SET `code_dume`='03' WHERE  `id`='3';

-- AME DUME-267
UPDATE `t_type_contrat` SET `code_dume`='01' WHERE  `id_type_contrat`=1;
UPDATE `t_type_contrat` SET `code_dume`='03' WHERE  `id_type_contrat`=2;
UPDATE `t_type_contrat` SET `code_dume`='06' WHERE  `id_type_contrat`=3;
UPDATE `t_type_contrat` SET `code_dume`='06' WHERE  `id_type_contrat`=4;
UPDATE `t_type_contrat` SET `code_dume`='03' WHERE  `id_type_contrat`=5;
UPDATE `t_type_contrat` SET `code_dume`='03' WHERE  `id_type_contrat`=6;
UPDATE `t_type_contrat` SET `code_dume`='04' WHERE  `id_type_contrat`=7;
UPDATE `t_type_contrat` SET `code_dume`='02' WHERE  `id_type_contrat`=8;
UPDATE `t_type_contrat` SET `code_dume`='06' WHERE  `id_type_contrat`=9;
UPDATE `t_type_contrat` SET `code_dume`='06' WHERE  `id_type_contrat`=10;
UPDATE `t_type_contrat` SET `code_dume`='06' WHERE  `id_type_contrat`=11;
UPDATE `t_type_contrat` SET `code_dume`='06' WHERE  `id_type_contrat`=12;
UPDATE `t_type_contrat` SET `code_dume`='03' WHERE  `id_type_contrat`=13;


-- MPE-5036 et MPE-5098
INSERT INTO `ValeurReferentiel` (`id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_2`, `libelle_valeur_referentiel_it`)SELECT * FROM (SELECT 210, 'Prépartion de la page de [_nom_page_]', 'Prépartion de la page de [_nom_page_] ', 'DESCRIPTION39', '')AS tmp WHERE NOT EXISTS (SELECT libelle_2 FROM ValeurReferentiel WHERE libelle_2='DESCRIPTION39')LIMIT 1;
INSERT INTO `ValeurReferentiel` (`id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_2`, `libelle_valeur_referentiel_it`)SELECT * FROM (SELECT 210, 'Affichage de la page échec dépôt', 'Affichage de la page échec dépôt ', 'DESCRIPTION40', '')AS tmp WHERE NOT EXISTS (SELECT libelle_2 FROM ValeurReferentiel WHERE libelle_2='DESCRIPTION40')LIMIT 1;
INSERT INTO `ValeurReferentiel` (`id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_2`, `libelle_valeur_referentiel_it`)SELECT * FROM (SELECT 210, 'Validation anticipée', 'Validation anticipée ', 'DESCRIPTION41', '')AS tmp WHERE NOT EXISTS (SELECT libelle_2 FROM ValeurReferentiel WHERE libelle_2='DESCRIPTION41')LIMIT 1;
INSERT INTO `ValeurReferentiel` (`id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_2`, `libelle_valeur_referentiel_it`)SELECT * FROM (SELECT 210, 'Suppression d''un fichier  suite exception [_liste_fichiers_tailles_]', 'Suppression d''un fichier  suite exception [_liste_fichiers_tailles_] ', 'DESCRIPTION42', '')AS tmp WHERE NOT EXISTS (SELECT libelle_2 FROM ValeurReferentiel WHERE libelle_2='DESCRIPTION42')LIMIT 1;
INSERT INTO `ValeurReferentiel` (`id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_2`, `libelle_valeur_referentiel_it`)SELECT * FROM (SELECT 210, 'Suppression d''un fichier par la fonction supprimerFichiersNonValides [_liste_fichiers_tailles_]', 'Suppression d''un fichier par la fonction supprimerFichiersNonValides [_liste_fichiers_tailles_] ', 'DESCRIPTION43', '')AS tmp WHERE NOT EXISTS (SELECT libelle_2 FROM ValeurReferentiel WHERE libelle_2='DESCRIPTION43')LIMIT 1;
