
-- AME MPE-6313
ALTER TABLE `configuration_plateforme` ADD COLUMN `case_attestation_consultation` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet activer une case à cocher dans le formulaire de la consultation afin d''attester que le besoin n''est pas déjà couvert par un marché national ou local';
ALTER TABLE `consultation` ADD COLUMN `attestation_consultation` CHAR(2) NOT NULL DEFAULT '0' COMMENT 'Permet d''attester que le besoin n''est pas déjà couvert par un marché national ou local';

-- MKH MPE-7937
ALTER TABLE `Service` CHANGE `id_initial` `id_externe` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0';
