-- AME MPE-11047
ALTER TABLE `blobOrganisme_file` ADD COLUMN `extension` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'extension du fichier après -0 (.dce, .brouillon, .rc ...)';
ALTER TABLE `blob_file` ADD COLUMN `extension` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'extension du fichier après -0 (.dce, .brouillon, .rc ...)';


-- FLO MPE-11653
ALTER TABLE `t_etablissement` ADD COLUMN `etat_administratif` CHAR(1) NULL DEFAULT NULL COMMENT 'etat administratif : A => active, F => fermé';
ALTER TABLE `t_etablissement` ADD COLUMN `date_fermeture` DATETIME NULL COMMENT 'timestamp de la date de fermeture';
ALTER TABLE `Entreprise` ADD COLUMN `etat_administratif` CHAR(1) NULL DEFAULT NULL COMMENT 'etat administratif : A => active, C => Cessée';
ALTER TABLE `Entreprise` ADD COLUMN `date_cessation` DATETIME NULL DEFAULT NULL COMMENT 'date de cessation';

-- AME MPE-11575

CREATE TABLE `agent_technique_association` (
  `id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `id_agent` int(11) NOT NULL,
  `organisme` varchar(50) DEFAULT NULL,
  `id_service` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB;

ALTER TABLE `agent_technique_association` ADD UNIQUE KEY `association_unique` (`id_agent`,`organisme`,`id_service`);
