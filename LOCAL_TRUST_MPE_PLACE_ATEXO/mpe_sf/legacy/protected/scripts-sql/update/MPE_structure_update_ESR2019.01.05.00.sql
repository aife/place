-- FMA MPE-10083
ALTER TABLE `configuration_organisme` ADD COLUMN `espace_documentaire` BOOLEAN NOT NULL DEFAULT FALSE;

-- FMA MPE-10085
ALTER TABLE `HabilitationAgent` ADD COLUMN `espace_documentaire_consultation` BOOLEAN NOT NULL DEFAULT FALSE;

-- FMA MPE-10120
CREATE TABLE IF NOT EXISTS `autre_piece_consultation` (
    id INT AUTO_INCREMENT NOT NULL,
    blob_id INT NOT NULL,
    consultation_id INT NOT NULL,
    created_at  DATETIME NOT NULL,
    updated_at DATETIME NULL,
    PRIMARY KEY(id)
) ENGINE = InnoDB;


-- AME MPE-11242
ALTER TABLE config_client RENAME configuration_client;
ALTER TABLE config_messages_traduction RENAME configuration_messages_traduction;
