-- NCH MPE-5101
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `procedure_passation_pivot` varchar(255) NOT NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `nom_lieu_principal_execution` varchar(255) NOT NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `code_lieu_principal_execution` varchar(20) NOT NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `type_code_lieu_principal_execution` varchar(20) NOT NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `duree_initiale_contrat` int NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `forme_prix` varchar(255) NOT NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `date_publication_initiale_de` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `num_id_unique_marche_public` varchar(255) NOT NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `libelle_type_contrat_pivot` varchar(255) NOT NULL;
ALTER TABLE `GeolocalisationN2` ADD COLUMN `type_lieu` char(1) NOT NULL;

CREATE TABLE IF NOT EXISTS `api_user` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `username` VARCHAR(50) NOT NULL ,
    `password` VARCHAR(50) NOT NULL ,
    `roles` VARCHAR(255) NOT NULL ,
    `token` VARCHAR(255) NULL DEFAULT NULL ,
    `expires_at` DATETIME NULL DEFAULT NULL ,
    PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `type_procedure_pivot` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `libelle` VARCHAR(255) NOT NULL ,
    PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `type_procedure_mpe_pivot` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `id_type_procedure_mpe` INT NOT NULL ,
    `id_type_procedure_pivot` INT NOT NULL ,
    PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `type_contrat_pivot` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `libelle` VARCHAR(255) NOT NULL ,
    PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `type_contrat_mpe_pivot` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `id_type_contrat_mpe` INT NOT NULL ,
    `id_type_contrat_pivot` INT NOT NULL ,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

-- NCH MPE-5403
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `siret_pa_accord_cadre` varchar(255) NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `ac_marche_subsequent` BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `libelle_type_procedure_mpe` varchar(255) NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `nb_total_propositions_lot` int NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `nb_total_propositions_demat_lot` int NULL;
-- NCH MPE-5410
ALTER TABLE `Service` ADD COLUMN `forme_juridique` varchar(100) NOT NULL;
ALTER TABLE `Service` ADD COLUMN `forme_juridique_code` varchar(4) NOT NULL;

-- NCH MPE-5603
ALTER TABLE `t_support_publication` MODIFY COLUMN `type_info` CHAR(1) NULL DEFAULT NULL COMMENT 'Designe le type d''info, 1=>texte, 2=>image, 3=>url';

-- NCH MPE-5607
ALTER TABLE `t_support_publication`	ADD COLUMN `affichage_message_support` ENUM('0','1') NULL DEFAULT '0' COMMENT 'permet d''afficher ou non le champ message pour le support';
ALTER TABLE `t_support_annonce_consultation` ADD COLUMN `message_acheteur` VARCHAR(255) NULL DEFAULT NULL;

-- NCH MPE-5602
ALTER TABLE `t_support_publication`	ADD COLUMN `selection_departements_parution` ENUM('0','1') NULL DEFAULT '0';
ALTER TABLE `t_support_publication`	ADD COLUMN `departements_parution` VARCHAR(255) NULL;
ALTER TABLE `t_support_annonce_consultation` ADD COLUMN `departements_parution_annonce` VARCHAR(255) NULL DEFAULT NULL;

-- APE MPE-5786
CREATE TABLE IF NOT EXISTS `supervision_interface`(
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `date_min_format` VARCHAR(50) NOT NULL,
  `date_min` DATETIME NOT NULL,
  `date_max` DATETIME NOT NULL,
  `nom_batch` VARCHAR(100) NOT NULL,
  `service` VARCHAR(50) NOT NULL,
  `nom_interface` VARCHAR(50) NULL DEFAULT NULL,
  `total` INT(11) NOT NULL,
  `total_ok` INT(11) NULL DEFAULT NULL,
  `pourcentage_ok` FLOAT NOT NULL,
  `poids` INT(11) NULL DEFAULT NULL,
  `poids_ok` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY(`id`)
) ENGINE = INNODB;

ALTER TABLE `t_dume_numero` ADD COLUMN `blob_id_xml` INT NULL DEFAULT NULL AFTER `date_recuperation_pdf`;
ALTER TABLE `t_dume_numero` ADD COLUMN `date_recuperation_xml` DATETIME NULL DEFAULT NULL AFTER `blob_id_xml`;

-- APE MPE-5834
ALTER TABLE `t_membre_groupement_entreprise` ADD COLUMN `numeroSN` VARCHAR(255) NULL DEFAULT NULL AFTER `id_membre_parent`;
ALTER TABLE `t_membre_groupement_entreprise` ADD COLUMN `email` VARCHAR(255) NULL DEFAULT NULL AFTER `numeroSN`;

ALTER TABLE `t_groupement_entreprise` ADD COLUMN `id_candidature` INT(11) NULL DEFAULT NULL AFTER `date_creation`;
ALTER TABLE `t_groupement_entreprise`	ADD CONSTRAINT `fk_groupement_entreprise_candidature` FOREIGN KEY (`id_candidature`) REFERENCES `t_candidature` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;


-- FMA MPE-5754
CREATE TABLE IF NOT EXISTS `modification_contrat` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `id_contrat_titulaire` INT NOT NULL,
    `num_ordre` INT NOT NULL,
    `date_creation` DATETIME NOT NULL,
    `date_modification` DATETIME NOT NULL,
    `id_agent` INT NOT NULL,
    `objet` mediumtext NOT NULL,
    `date_signature` DATETIME NOT NULL,
    `montant` VARCHAR(255) NOT NULL,
    `id_etablissement` INT NOT NULL,
    `duree_marche` INT NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

ALTER TABLE `modification_contrat`
ADD CONSTRAINT `id_contrat_titulaire_FK` FOREIGN KEY (`id_contrat_titulaire`)
REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`);

ALTER TABLE `modification_contrat`
ADD CONSTRAINT `id_agent_FK` FOREIGN KEY (`id_agent`)
REFERENCES `Agent` (`id`);

ALTER TABLE `modification_contrat`
ADD CONSTRAINT `id_etablissement_FK`
FOREIGN KEY (`id_etablissement`) REFERENCES `t_etablissement` (`id_etablissement`);

ALTER TABLE `modification_contrat` CHANGE `objet` `objet_consultation` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

-- LEZ MPE-5737
ALTER TABLE `configuration_organisme`	ADD COLUMN `marche_defense` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher ou non le bloc ''marché de défense ou sécurité'' pour les contrats' ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `marche_defense` CHAR(1) NULL DEFAULT '0' COMMENT 'Permet de savoir si marché de défense est selectionné : 0=> Non renseigné,1=>Oui,2=>Non' ;

-- NCH MPE-5457
CREATE TABLE IF NOT EXISTS `document_serveur_docs` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `nom` VARCHAR(100) NOT NULL ,
    `poids` INT NOT NULL ,
    `type` VARCHAR(20) NULL ,
    `extension` VARCHAR(10) NOT NULL ,
    PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

ALTER TABLE `modification_contrat` CHANGE `montant` `montant` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `id_etablissement` `id_etablissement` INT(11) NULL DEFAULT NULL, CHANGE `duree_marche` `duree_marche` INT(11) NULL DEFAULT NULL;

ALTER TABLE `modification_contrat` CHANGE `objet_consultation` `objet_modification` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

-- NCH MPE-5819
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `siret` varchar(255) NULL;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `nom_entite_acheteur` varchar(255) NULL;

-- NCH MPE-3378
ALTER TABLE `Inscrit` ADD COLUMN `type_hash` VARCHAR(10) DEFAULT 'SHA1';
ALTER TABLE `Inscrit` ADD COLUMN `date_expiration_mdp` DATETIME NOT NULL;
ALTER TABLE `Inscrit` MODIFY COLUMN `mdp` VARCHAR(64) NOT NULL;
ALTER TABLE `Agent` ADD COLUMN `type_hash` VARCHAR(10) DEFAULT 'SHA1';
ALTER TABLE `Agent` ADD COLUMN `date_expiration_mdp` DATETIME NOT NULL;
ALTER TABLE `Agent` MODIFY COLUMN `mdp` VARCHAR(64) NOT NULL;
ALTER TABLE `Agent` MODIFY COLUMN `password` VARCHAR(64) NOT NULL;

-- LEZ MPE-5632
ALTER TABLE `categories_considerations_sociales`	ADD COLUMN `code` TEXT NULL ;

-- APE-MPE-5739
ALTER TABLE `configuration_plateforme` ADD COLUMN `donnees_essentielles_suivi_sn` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `plateforme_editeur`;

-- APE-MPE-6098
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `statut_publication_sn` INT(2) NOT NULL DEFAULT 0;
ALTER TABLE `modification_contrat` ADD COLUMN `statut_publication_sn` INT(2) NOT NULL DEFAULT 0;

-- LEZ MPE-6075
ALTER TABLE `Inscrit`	DROP COLUMN `date_expiration_mdp`;
ALTER TABLE `Agent`	DROP COLUMN `date_expiration_mdp`;

-- FMA MPE-6274
ALTER TABLE `t_type_contrat`
  ADD `concession` BOOLEAN NOT NULL DEFAULT FALSE AFTER `code_dume`;

CREATE TABLE IF NOT EXISTS `type_contrat_concession_pivot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `type_procedure_concession_pivot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `t_contrat_titulaire`
  ADD `id_type_procedure_pivot` INT NULL DEFAULT NULL AFTER `statut_publication_sn`,
  ADD `id_type_procedure_concession_pivot` INT NULL DEFAULT NULL AFTER `id_type_procedure_pivot`,
  ADD `id_type_contrat_pivot` INT NULL DEFAULT NULL AFTER `id_type_procedure_concession_pivot`,
  ADD `id_type_contrat_concession_pivot` INT NULL DEFAULT NULL AFTER `id_type_contrat_pivot`;


ALTER TABLE t_contrat_titulaire
  ADD CONSTRAINT id_type_procedure_pivot
  FOREIGN KEY (id_type_procedure_pivot)
  REFERENCES TypeProcedure(id_type_procedure);

ALTER TABLE t_contrat_titulaire
  ADD CONSTRAINT FK_id_type_procedure_concession_pivot
  FOREIGN KEY (id_type_procedure_concession_pivot)
  REFERENCES type_procedure_concession_pivot(id);

ALTER TABLE t_contrat_titulaire
  ADD CONSTRAINT FK_id_type_contrat_pivot
  FOREIGN KEY (id_type_contrat_pivot)
  REFERENCES type_contrat_pivot(id);

ALTER TABLE t_contrat_titulaire
  ADD CONSTRAINT FK_id_type_contrat_concession_pivot
  FOREIGN KEY (id_type_contrat_concession_pivot)
  REFERENCES type_contrat_concession_pivot(id);

-- LEZ MPE-6389

CREATE TABLE IF NOT EXISTS `donnees_annuelles_concession` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `id_contrat` INT(11) NOT NULL ,
    `valeur_depense` DOUBLE NULL DEFAULT NULL ,
    `date_saisie` DATETIME NULL DEFAULT NULL ,
    PRIMARY KEY  (`id`),
    CONSTRAINT FK_id_contrat_donnees_annuelles_concession FOREIGN KEY (id_contrat)
    REFERENCES t_contrat_titulaire(id_contrat_titulaire)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `donnees_annuelles_concession_tarif` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `id_donnees_annuelle` INT(11) NOT NULL ,
    `intitule_tarif` VARCHAR(255) NULL DEFAULT NULL ,
    `montant` DOUBLE NULL DEFAULT NULL ,
    PRIMARY KEY  (`id`),
    CONSTRAINT FK_id_donnees_annuelle_tarif FOREIGN KEY (id_donnees_annuelle)
    REFERENCES donnees_annuelles_concession(id)
) ENGINE = InnoDB;

ALTER TABLE `donnees_annuelles_concession` ADD COLUMN `num_ordre` INT(11) NULL DEFAULT NULL ;
ALTER TABLE `donnees_annuelles_concession` ADD COLUMN `suivi_publication_sn` INT(2) NULL DEFAULT NULL ;
-- APE MPE-6010
ALTER TABLE `ValeurReferentiel` ADD COLUMN `ordre_affichage` INT NOT NULL DEFAULT 0;

-- LEZ MPE-6275
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `montant_subvention_publique` DOUBLE NULL DEFAULT NULL ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `date_debut_execution` DATE NULL DEFAULT NULL ;
ALTER TABLE `t_contrat_titulaire` CHANGE COLUMN `procedure_passation_pivot` `procedure_passation_pivot` VARCHAR(255) NULL DEFAULT NULL ;

-- LEZ MPE-6577
ALTER TABLE `donnees_annuelles_concession` CHANGE COLUMN `suivi_publication_sn` `suivi_publication_sn` INT(2) NOT NULL DEFAULT '0';

-- LEZ MPE-6780
ALTER TABLE `ProcedureEquivalence` ADD COLUMN `type_formulaire_dume_standard` VARCHAR(2) NOT NULL DEFAULT '+1';
ALTER TABLE `ProcedureEquivalence` ADD COLUMN `type_formulaire_dume_simplifie` VARCHAR(2) NOT NULL DEFAULT '+0';

-- LEZ MPE-6781
ALTER TABLE `consultation`	ADD COLUMN `type_formulaire_dume` CHAR(2) NULL DEFAULT '0' COMMENT 'Permet de savoir le type de formulaire dume "0"=>standard,"1"=>simplifié';

-- AME MPE-6782
ALTER TABLE `t_dume_contexte`	CHANGE COLUMN `status` `status` INT(2) NOT NULL DEFAULT '99' COMMENT '99 : brouillon, 1 : valide, 2 : en attente publication, 3 : publie, 4 : supprimer, 5 : remplacer';

-- AME MPE-6488
ALTER TABLE `t_donnee_complementaire`	ADD COLUMN `projet_finance_fonds_union_europeenne` ENUM('0','1') NOT NULL;
ALTER TABLE `t_donnee_complementaire` ADD COLUMN `identification_projet` TEXT NOT NULL;

-- MPE-6988
ALTER TABLE `fichierEnveloppe` CHANGE `signature_fichier` `signature_fichier` MEDIUMTEXT NOT NULL;

-- FLO MPE-6805
ALTER TABLE `supervision_interface` CHANGE `poids` `poids` BIGINT(20) NULL DEFAULT NULL;
ALTER TABLE `supervision_interface` CHANGE `poids_ok` `poids_ok` BIGINT(20) NULL DEFAULT NULL;

-- AME MPE-6955
ALTER TABLE `Type_Procedure_Organisme` ADD COLUMN `id_externe`  VARCHAR(255) NULL DEFAULT '' COMMENT 'Id unique sauf pour les MAPA';
ALTER TABLE `t_type_contrat` ADD COLUMN `id_externe`  VARCHAR(255) NULL DEFAULT '' COMMENT 'Id unique sauf pour les MAPA';

-- FLO MPE-6672
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `uuid` VARCHAR(36) NULL DEFAULT NULL;

-- AME MPE-6806
ALTER TABLE `t_candidature`	ADD COLUMN `role_inscrit` INT NOT NULL DEFAULT '1' COMMENT '1 = mandataire / 2 = co-traitant';

-- LEZ MPE-6526
ALTER TABLE `t_tranche`	CHANGE COLUMN `nature_tranche` `nature_tranche` VARCHAR(100) NULL DEFAULT NULL;

-- LEZ MPE-5820
ALTER TABLE `ProcedureEquivalence`	ADD COLUMN `afficher_code_cpv` VARCHAR(2) NULL DEFAULT '1';
ALTER TABLE `ProcedureEquivalence`	ADD COLUMN `code_cpv_obligatoire` VARCHAR(2) NULL DEFAULT '1' ;

-- AME MPE-7433
ALTER TABLE `configuration_plateforme`	ADD COLUMN `dossier_volumineux` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''upload et download des fichiers volumineux';
