-- FMA MPE-4959
INSERT INTO `code_retour` (`code`, `description`) VALUES ('0', 'Ce code est retourné quand la tâche ou l’interface (dans le cas où l’interface ne retourne rien) sais exécuté sans erreur.');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('1', 'Ce code est retourné dans le cas d’erreur non définie.');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('2', 'Ce code est retourné lors d’un mauvais usage de la commande (mauvais paramètre, etc. )');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('126', 'Ce code est retourné lorsque la commande ou un service distant n’a pas les droits d’exécution ou que l’utilisateur n’ai pas autorisé à l’exécuter.');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('127', 'Ce code est retourné lorsque la commande ou un service distant  n’existe pas (exemple erreur apache : 404)');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('128', 'Ce code est retourné lorsque la commande ou un service distant  ne retourne pas le bon résultat attendu.');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('20', 'Ce code est retourné lorsqu’un service distant  ne réponde pas (timeout, ex : apache : 504)');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('21', 'Ce code est retourné lorsqu’un service distant nous retourne une erreur ( ex : code apache 500 ou 503)');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('10', 'Ce code est retourné lorsqu’un service n’arrive pas à écrire sur le systèmes.');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('11', 'Certains fichiers/dossiers sont  introuvables');
INSERT INTO `code_retour` (`code`, `description`) VALUES ('30', 'L’application a rencontré un problème.');

-- LEZ MPE-5413
UPDATE `HabilitationAgent`,`configuration_plateforme` SET `activer_version_clausier`='1' WHERE publier_version_clausier_editeur = '1' AND `plateforme_editeur`='1';

-- FMA MPE-5313
UPDATE `t_document_type` SET `type_retour_ws` = '2' WHERE `t_document_type`.`id_type_document` in (8, 9, 11);
