-- MWA MPE-3088
INSERT INTO `ValeurReferentiel` (`id` ,`id_referentiel` ,`libelle_valeur_referentiel` ,`libelle_valeur_referentiel_fr` ,`libelle_2`) VALUES (6 , '161', 'Interface PLACE-CHORUS', 'Interface PLACE-CHORUS',  '31;32;33;34;35');

-- LEZ MPE-4139
UPDATE `Chorus_type_marche`  SET `libelle` = 'ZCTR - Autres contrats',`code` = 'ZCTR' WHERE `id` = 10;

-- LEZ MPE-5151
UPDATE `t_document_type` SET `type_retour_ws`='1' WHERE  `type_retour_ws`='2';


UPDATE `t_document_type` SET `uri` = '/v2/attestations_fiscales_dgfip/' WHERE `t_document_type`.`id_type_document` = 1;
UPDATE `t_document_type` SET `uri` = '/v2/attestations_sociales_acoss/' WHERE `t_document_type`.`id_type_document` = 2;
UPDATE `t_document_type` SET `uri` = '/v2/attestations_sociales_acoss/' WHERE `t_document_type`.`id_type_document` = 3;
UPDATE `t_document_type` SET `uri` = '/v2/cartes_professionnelles_fntp/' WHERE `t_document_type`.`id_type_document` = 4;
UPDATE `t_document_type` SET `uri` = '/v2/certificats_cnetp/' WHERE `t_document_type`.`id_type_document` = 5;
UPDATE `t_document_type` SET `uri` = '/v2/certificats_qualibat/' WHERE `t_document_type`.`id_type_document` = 6;
UPDATE `t_document_type` SET `uri` = '/v2/attestations_cotisation_retraite_probtp/' WHERE `t_document_type`.`id_type_document` = 7;
UPDATE `t_document_type` SET `uri` = '/v2/certificats_opqibi/' WHERE `t_document_type`.`id_type_document` = 8;
UPDATE `t_document_type` SET `uri` = '/v2/cotisations_msa/' WHERE `t_document_type`.`id_type_document` = 9;
UPDATE `t_document_type` SET `uri` = '/v2/attestations_agefiph/' WHERE `t_document_type`.`id_type_document` = 11;
