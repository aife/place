-- FMA MPE-11702
INSERT INTO blobOrganisme_file(`old_id`, `organisme`, `name`, `deletion_datetime`, `chemin`, `dossier`, `statut_synchro`, `hash`, `id`) SELECT NULL, 'DELETE', 'DELETE', '2020-02-19 00:00:00', 'DELETE', 'DELETE', '0', 'ND', MAX(`old_id`) + 10 FROM blobOrganisme_file;
INSERT INTO blob_file(`old_id`, `name`, `deletion_datetime`, `chemin`, `dossier`, `statut_synchro`, `hash`, `id`) SELECT NULL, 'DELETE', '2020-02-19 00:00:00', 'DELETE', 'DELETE', '0', 'ND', MAX(`old_id`) + 10 FROM blob_file;

INSERT INTO `Tiers` (`login`, `password`, `denomination`, `fonctionnalite`, `organisme`) SELECT DISTINCT  'mpe_oversight', '759ec8276c2e77b', 'mpe_oversight', '4', NULL FROM Tiers where NOT EXISTS (SELECT 1 from  Tiers where Tiers.login like 'mpe_oversight');