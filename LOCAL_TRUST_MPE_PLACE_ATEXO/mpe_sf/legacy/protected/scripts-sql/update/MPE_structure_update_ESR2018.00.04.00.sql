ALTER TABLE `Chorus_echange` ADD COLUMN `dume_acheteur_items` VARCHAR(255) NULL DEFAULT NULL AFTER `dce_items`;
ALTER TABLE `Chorus_echange` ADD COLUMN `dume_oe_items` VARCHAR (255) NULL DEFAULT NULL AFTER `dume_acheteur_items`;

-- LEZ MPE-6924
ALTER TABLE `consultation` ADD COLUMN `source_externe` VARCHAR(255) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD COLUMN `id_source_externe` INT(11) NULL DEFAULT NULL ;

-- LEZ MPE-6915
ALTER TABLE `t_candidature`	ADD COLUMN `date_derniere_validation_dume` DATETIME NULL ;

-- FMA MPE-7148
ALTER TABLE `configuration_plateforme` ADD COLUMN `token_api_sgmap_api` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `configuration_plateforme` CHANGE `token_api_sgmap_api` `token_api_sgmap_api` TEXT NULL DEFAULT NULL;

-- LEZ MPE-6123
ALTER TABLE `Agent` CHANGE COLUMN `id_initial` `id_externe` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `Agent`	CHANGE COLUMN `id_externe` `id_externe` VARCHAR(50) NOT NULL DEFAULT '0';

-- FLO MPE-5827
ALTER TABLE `configuration_plateforme` ADD `afficher_valeur_estimee` ENUM('0','1') NOT NULL DEFAULT '0';
