-- mwa MPE-7615
ALTER TABLE `HabilitationAgent` ADD COLUMN `invite_permanent_entite_dependante` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `suivre_consultation_pole`, ADD `invite_permanent_mon_entite` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `invite_permanent_entite_dependante`, ADD `invite_permanent_transverse` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `invite_permanent_mon_entite`;
ALTER TABLE `HabilitationProfil` ADD COLUMN `invite_permanent_entite_dependante` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `suivre_consultation_pole`, ADD `invite_permanent_mon_entite` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `invite_permanent_entite_dependante`, ADD `invite_permanent_transverse` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `invite_permanent_mon_entite`;

-- mwa MPE-7617
CREATE TABLE  IF NOT EXISTS invite_permanent_transverse (
`id` INT NOT NULL AUTO_INCREMENT ,
`service_id` INT NOT NULL ,
`agent_id` INT NOT NULL ,
PRIMARY KEY (`id`),
INDEX `invitePermanentTransverse_service_idx` (`service_id`),
INDEX `invitePermanentTransverse_agent_idx` (`agent_id`)
) ENGINE = InnoDB;

ALTER TABLE invite_permanent_transverse
ADD CONSTRAINT `t_invite_permanent_transverse_service_id_FK` FOREIGN KEY (`service_id`)
REFERENCES `Service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE invite_permanent_transverse
ADD CONSTRAINT `t_invite_permanent_transverse_agent_id_FK` FOREIGN KEY (`agent_id`)
REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- FMA-7617
ALTER TABLE invite_permanent_transverse ADD COLUMN `acronyme` VARCHAR(30) NOT NULL;
ALTER TABLE invite_permanent_transverse CHANGE `service_id` `service_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE invite_permanent_transverse ADD CONSTRAINT `invite_permanent_transverse_organisme_acronyme_FK` FOREIGN KEY (`acronyme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Agent` ADD COLUMN `alerte_mes_consultations` ENUM('0','1') NOT NULL DEFAULT '1';
ALTER TABLE `Agent` ADD COLUMN `alerte_consultations_mon_entite` ENUM('0','1') NOT NULL DEFAULT '1';
ALTER TABLE `Agent` ADD COLUMN `alerte_consultations_des_entites_dependantes` ENUM('0','1') NOT NULL DEFAULT '1';
ALTER TABLE `Agent` ADD COLUMN `alerte_consultations_mes_entites_transverses` ENUM('0','1') NOT NULL DEFAULT '1';