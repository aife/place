-- MKH MPE-9865
ALTER TABLE `consultation` change `numero_projet_achat` `code_externe` VARCHAR(225) DEFAULT NULL AFTER `reference` ;

-- MWA MPE-9642
ALTER TABLE `consultation_archive_bloc` ADD COLUMN `comp_id` VARCHAR(255) NULL DEFAULT NULL AFTER `erreur`;

-- FMA-10327
ALTER TABLE `consultation_favoris` ADD UNIQUE `consultation_favoris_id_consultation_id_agent_unique` (`id_consultation`, `id_agent`);