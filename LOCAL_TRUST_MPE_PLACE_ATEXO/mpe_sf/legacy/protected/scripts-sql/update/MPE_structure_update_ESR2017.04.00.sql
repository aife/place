CREATE OR REPLACE
 VIEW `_Vue_Consultation_sup_a_2015`
 AS 
SELECT
	`datefin`,
	`date_mise_en_ligne_calcule`,
	`reference`,
	consultation.`organisme`,
	concat(`reference`,'_',consultation.`organisme`) as Ref_Orga,
	(`type_acces`-1) as 'consultation restreinte',
	(SELECT denomination_org FROM  `Organisme` WHERE acronyme = consultation.organisme) AS Nom_organisme,
	(SELECT active FROM  `Organisme` WHERE acronyme = consultation.organisme) AS active,
	consultation.`service_id`,
	(SELECT libelle FROM  `Service` ser WHERE ser.id = consultation.service_id and ser.organisme = consultation.organisme) AS Nom_service,
	`reference_utilisateur`,
	`categorie`,
	(SELECT libelle FROM  `CategorieConsultation` WHERE id = consultation.categorie) AS Nom_categorie,
	`id_type_procedure_org`,
	(SELECT libelle_type_procedure FROM  `Type_Procedure_Organisme` tpo WHERE tpo.id_type_procedure = consultation.id_type_procedure_org and tpo.organisme = consultation.organisme) AS Nom_Procedure_Agent,
	SUBSTRING(consultation.objet,1,100),
	`marche_public_simplifie`,
	`id_type_avis`,
	`alloti`,
	(SELECT COUNT( * ) FROM  `CategorieLot` WHERE consultation_ref = reference AND consultation.organisme = CategorieLot.organisme) AS nbre_lots,
	(1-(`alloti`-1)+(`alloti`-1)*(SELECT COUNT( * ) FROM  `CategorieLot` WHERE consultation_ref = reference AND consultation.organisme = CategorieLot.organisme)) as Equiv_Lots,
	(SELECT taille_dce/1000000 FROM DCE WHERE consultation.organisme = DCE.organisme AND consultation.reference = DCE.consultation_ref ORDER BY `DCE`.`untrusteddate` DESC LIMIT 0,1) AS "Taille_DCE_Mo",
	(SELECT COUNT(*) FROM Telechargement  WHERE consultation_ref = reference  AND consultation.organisme = Telechargement.organisme) AS nbre_retrait_NON_ANO,
	(SELECT COUNT(*) FROM TelechargementAnonyme  WHERE consultation_ref = reference  AND consultation.organisme = TelechargementAnonyme.organisme) AS nbre_retrait_ANONYME,
	(SELECT COUNT(*) FROM Offres  WHERE Offres.envoi_complet=1 AND Offres.statut_offres!='99' AND consultation_ref = reference  AND consultation.organisme = Offres.organisme) AS nbre_reponse_Elec,
	(SELECT COUNT(DISTINCT(id_entreprise)) FROM `t_candidature_mps` WHERE ref_consultation = reference AND id_offre != 'NULL' AND id_entreprise in (select entreprise_id from Offres where envoi_complet=1 and statut_offres!='99' AND consultation_ref = ref_consultation)) AS dont_reponse_MPS ,
	(SELECT SUM(fichierEnveloppe.taille_fichier)/1000000  FROM `Offres`, Enveloppe , fichierEnveloppe WHERE Offres.id = Enveloppe.offre_id AND Offres.organisme = Enveloppe.organisme AND Enveloppe.id_enveloppe_electro = fichierEnveloppe.id_enveloppe AND Enveloppe.organisme = fichierEnveloppe.organisme AND `envoi_complet`='1' and statut_offres!='99' AND consultation_ref = reference AND consultation.organisme = Offres.organisme ) AS 'Poids des réponses en Mo',
	age.id as 'idAgent',
    concat(age.nom, ' ', age.prenom) as 'nomPrenomAgent',
    age.email as 'mailAgent',
    age.num_tel as 'telephoneAgent'
FROM
	`consultation`
    Left Join Agent age on age.id = id_createur
WHERE
	id_type_avis='3'
	and consultation.organisme in (select acronyme from Organisme where active=1)
	AND `date_mise_en_ligne_calcule` < datefin
	AND `date_mise_en_ligne_calcule` != '0000-00-00 00:00:00'
	AND `date_mise_en_ligne_calcule` is not null
	AND datefin >= '2015-01-01 00:00:00' ;


CREATE OR REPLACE
 VIEW `_Vue_Offre_sup_a_2015`
AS
SELECT 
`untrusteddate` as borneInf_DateDepot,
`untrusteddate` as borneSup_DateDepot,
`id`,
`organisme`,
 (SELECT denomination_org FROM  `Organisme`  WHERE acronyme = Offres.organisme) AS Nom_organisme,
 (SELECT active FROM  `Organisme`  WHERE acronyme = Offres.organisme) AS active,
`consultation_ref`,
 (SELECT reference_utilisateur FROM  `consultation` WHERE reference = Offres.consultation_ref and organisme = Offres.organisme) AS Ref_Agent,
 (SELECT  SUBSTRING(consultation.objet,1,100) FROM  `consultation`  WHERE reference = Offres.consultation_ref and organisme = Offres.organisme) AS Objet,
`entreprise_id`,
`nom_entreprise_inscrit`,
(SELECT COUNT(DISTINCT(id_entreprise)) FROM `t_candidature_mps` WHERE ref_consultation = consultation_ref AND id_offre != 'NULL' AND id_entreprise = entreprise_id) AS reponse_MPS,
(SELECT SUM(fichierEnveloppe.taille_fichier)/1000000  FROM Enveloppe , fichierEnveloppe WHERE Offres.id = Enveloppe.offre_id AND Offres.organisme = Enveloppe.organisme  AND Enveloppe.id_enveloppe_electro = fichierEnveloppe.id_enveloppe AND Enveloppe.organisme = fichierEnveloppe.organisme) AS 'Poids_réponse_Mo', 
(SELECT COUNT(offre_id) FROM Enveloppe WHERE Offres.id = Enveloppe.offre_id AND Offres.organisme = Enveloppe.organisme ) AS Nbr_Env,
(SELECT COUNT(DISTINCT fichierEnveloppe.id_fichier) FROM Enveloppe , fichierEnveloppe WHERE Offres.id = Enveloppe.offre_id AND Enveloppe.id_enveloppe_electro = fichierEnveloppe.id_enveloppe AND Enveloppe.organisme = fichierEnveloppe.organisme AND Offres.organisme = Enveloppe.organisme) AS Nbr_Fichiers,
`uid_offre`,
`statut_offres`,
`envoi_complet`,
`created_at`,
`date_depot`,
DATEDIFF(untrusteddate,`date_depot`) as Horo_Moins_Depot,
DATEDIFF(untrusteddate,`created_at`) as horo_Moins_Creation
FROM `Offres` 
WHERE 
`envoi_complet`='1' and statut_offres!='99' 
AND organisme in (select acronyme from Organisme where active=1)
AND `untrusteddate` >= '2015-01-01 00:00:00';

CREATE OR REPLACE
 VIEW `_Vue_Telechargement_sup_a_2015`
AS
SELECT `id`,
`datetelechargement` as borneInf_DateTelechargement,
`datetelechargement` as borneSup_DateTelechargement,
`organisme`,
 (SELECT denomination_org FROM  `Organisme`  WHERE acronyme = Telechargement.organisme) AS Nom_organisme,
 (SELECT active FROM  `Organisme`  WHERE acronyme = Telechargement.organisme) AS active,
`consultation_ref`,
 (SELECT reference_utilisateur FROM  `consultation` WHERE reference = Telechargement.consultation_ref and organisme = Telechargement.organisme) AS Ref_Agent,
 (SELECT  SUBSTRING(consultation.objet,1,100) FROM  `consultation`  WHERE reference = Telechargement.consultation_ref and organisme = Telechargement.organisme) AS Objet,
`id_entreprise`,
`entreprise`,
`nom`,
`email`,
`codepostal`,
`ville`,
`pays` 
FROM `Telechargement`  
WHERE 
 organisme in (select acronyme from Organisme where active=1)
AND `datetelechargement` >= '2015-01-01 00:00:00';

CREATE OR REPLACE
 VIEW `_Vue_Telechargement_Anonyme_sup_a_2015`
AS
SELECT `id`,
`datetelechargement` as borneInf_DateTelechargement,
`datetelechargement` as borneSup_DateTelechargement,
`organisme`,
 (SELECT denomination_org FROM  `Organisme`  WHERE acronyme = TelechargementAnonyme.organisme) AS Nom_organisme,
 (SELECT active FROM  `Organisme`  WHERE acronyme = TelechargementAnonyme.organisme) AS active,
`consultation_ref`,
 (SELECT reference_utilisateur FROM  `consultation` WHERE reference = TelechargementAnonyme.consultation_ref and organisme = TelechargementAnonyme.organisme) AS Ref_Agent,
 (SELECT  SUBSTRING(consultation.objet,1,100) FROM  `consultation`  WHERE reference = TelechargementAnonyme.consultation_ref and organisme = TelechargementAnonyme.organisme) AS Objet
FROM `TelechargementAnonyme` 
WHERE 
 organisme in (select acronyme from Organisme where active=1)
AND `datetelechargement` >= '2015-01-01 00:00:00';