-- MPE-8633 [Masquage MPS] Paramétrage des types de procédure - Script de désactivation MPS
UPDATE ProcedureEquivalence SET marche_public_simplifie = '-0';

-- AME MPE-9064
UPDATE Type_Procedure_Organisme SET `libelle_type_procedure` = REPLACE(`libelle_type_procedure`, '?', 'é');