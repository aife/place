-- mwa MPE-7611
-- mise à jour de habilitations Agent
UPDATE HabilitationAgent SET invite_permanent_mon_entite='1' WHERE suivre_consultation_pole='1';
UPDATE HabilitationAgent SET invite_permanent_entite_dependante='1' WHERE suivre_consultation_pole='1' AND HabilitationAgent.id_agent IN (SELECT Agent.id FROM Agent, Organisme, configuration_organisme WHERE Agent.organisme=Organisme.acronyme AND Organisme.acronyme=configuration_organisme.organisme AND configuration_organisme.organisation_centralisee='1');

-- mise à jour des Profils hors PF PLACE
UPDATE HabilitationProfil SET invite_permanent_mon_entite='1' WHERE suivre_consultation_pole='1';
UPDATE HabilitationProfil SET invite_permanent_entite_dependante='1' WHERE suivre_consultation_pole='1';

-- mise à jour des Profils pour PF PLACE
UPDATE HabilitationProfil SET invite_permanent_mon_entite='1' WHERE suivre_consultation_pole='1';