-- LEZ MPE-3254
ALTER TABLE `configuration_plateforme` CHANGE COLUMN `groupement` `groupement` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet de gerer le bloc groupement des entreprises';
ALTER TABLE `configuration_plateforme` CHANGE COLUMN `notifications_agent` `notifications_agent` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet de gerer les notifications pour l''agent';

-- LEZ MPE-3355
ALTER TABLE `configuration_organisme`	CHANGE COLUMN `mode_applet` `mode_applet` ENUM('0','1') NOT NULL DEFAULT '0';

-- OKO: MPE-3444
ALTER TABLE `T_trace_operations_inscrit_details` ADD COLUMN `infos_browser` TEXT NULL DEFAULT NULL COMMENT 'Contient les infos sur le navigateur de l''utilisateur' AFTER `lien_download`;

-- OKO: MPE-3553
ALTER TABLE `T_trace_operations_inscrit_details` ADD COLUMN `date_debut_action_client` DATETIME NULL COMMENT 'Contient la date de debut du poste client' AFTER `infos_browser`;
ALTER TABLE `T_trace_operations_inscrit_details` ADD COLUMN `id_offre` INT(11) NULL COMMENT 'Contient l''id de l''offre si elle existe' AFTER `date_debut_action_client`;

-- LEZ MPE-3530
ALTER TABLE `EchangeDestinataire` ADD INDEX echangeDestinataire_organisme_idx( `organisme`);
ALTER TABLE `EchangeDestinataire` ADD INDEX echangeDestinataire_mail_destinataire_idx( `mail_destinataire`);
ALTER TABLE `EchangeDestinataire` ADD INDEX echangeDestinataire_uid_idx( `uid`);

ALTER TABLE `Echange` ADD INDEX echange_organisme_idx( `organisme`);
ALTER TABLE `Echange` ADD INDEX echange_ref_consultation_idx( `ref_consultation`);
ALTER TABLE `Echange` ADD INDEX echange_email_expediteur_idx( `email_expediteur`);

ALTER TABLE `fichierEnveloppe` ADD INDEX fichierEnveloppe_organisme_idx( `organisme`);
ALTER TABLE `fichierEnveloppe` ADD INDEX fichierEnveloppe_id_fichier_signature_idx( `id_fichier_signature`);

ALTER TABLE `Enveloppe` ADD INDEX enveloppe_id_enveloppe_electro_idx( `id_enveloppe_electro`);

ALTER TABLE `consultation` ADD INDEX consultation_id_regle_validation_idx( `id_regle_validation`);
ALTER TABLE `consultation` ADD INDEX consultation_id_etat_consultation_idx( `id_etat_consultation`);
ALTER TABLE `consultation` ADD INDEX consultation_doublon_idx( `doublon`);

ALTER TABLE `Inscrit` ADD INDEX inscrit_id_idx( `id`);
ALTER TABLE `Inscrit` ADD INDEX inscrit_id_idx( `bloque`);

ALTER TABLE `Offres` CHANGE COLUMN `created_at` `created_at` DATETIME NULL DEFAULT NULL;
-- OKO MPE-3604
ALTER TABLE `Offres` ADD COLUMN `uid_response` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Unique ID du depot de l''offre' AFTER `created_at`;
ALTER TABLE `Enveloppe` ADD COLUMN `uid_response` VARCHAR(255) NULL DEFAULT '' COMMENT 'Unique ID du depot de l''offre' AFTER `verification_signature`;
ALTER TABLE `fichierEnveloppe` ADD COLUMN `uid_response` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Unique ID du depot de l''offre' AFTER `id_fichier_signature`;
ALTER TABLE `Enveloppe` CHANGE COLUMN `uid_response` `uid_response` TEXT NULL COMMENT 'Unique ID du depot de l''offre' AFTER `verification_signature`;
ALTER TABLE `Offres` CHANGE COLUMN `uid_response` `uid_response` TEXT NULL DEFAULT NULL COMMENT 'Unique ID du depot de l''offre' AFTER `created_at`;
ALTER TABLE `fichierEnveloppe` CHANGE COLUMN `uid_response` `uid_response` TEXT NULL COMMENT 'Unique ID du depot de l''offre' AFTER `id_fichier_signature`;
-- MBL - MPE-2784_2
ALTER TABLE `Offres` MODIFY `created_at` datetime NULL;



-- LEZ MPE-3110
ALTER TABLE `t_document_type`	ADD COLUMN `type_retour_ws` CHAR(1) NULL DEFAULT '1' COMMENT 'Permet de precisier la nature de retour de ws, 1 : fichier , 2: metadonnee' AFTER `nature_document`;
ALTER TABLE `t_entreprise_document_version`	ADD COLUMN `jeton_document` TEXT NULL COMMENT 'contient le json retourner par le ws document dans le cas ou le ws ne retourne pas un fichier' AFTER `extension_document`;
ALTER TABLE `t_entreprise_document_version`	CHANGE COLUMN `hash` `hash` VARCHAR(500) NULL AFTER `date_recuperation`;
ALTER TABLE `t_entreprise_document_version`	CHANGE COLUMN `id_blob` `id_blob` INT(11) NULL AFTER `hash`;
ALTER TABLE `t_entreprise_document_version`	CHANGE COLUMN `taille_document` `taille_document` VARCHAR(50) NULL AFTER `id_blob`;
ALTER TABLE `t_entreprise_document_version`	CHANGE COLUMN `extension_document` `extension_document` VARCHAR(50) NULL AFTER `taille_document`;

-- AME MPE-3111
ALTER TABLE `t_candidature_mps`	ADD COLUMN `liste_lots` TEXT NULL AFTER `taille_fichier`;

-- LEZ MPE-3638
ALTER TABLE `t_entreprise_document_version` 	CHANGE COLUMN `jeton_document` `json_document` TEXT NULL COMMENT 'contient le json retourner par le ws document dans le cas ou le ws ne retourne pas un fichier' AFTER `extension_document`;


-- LEZ MPE-3667
ALTER TABLE `Offres`	ADD COLUMN `date_depot` DATETIME default NULL COMMENT ' c''est la date de la validation du dépôt' AFTER `uid_response`;



-- AME MPE-4077
ALTER TABLE `fichierEnveloppe`	ADD COLUMN `type_signature_fichier` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'Type signature : XADES, CADES ou PADES' AFTER `uid_response`;
ALTER TABLE `fichierEnveloppe`	ADD COLUMN `hash256` TEXT NOT NULL DEFAULT '' COMMENT 'hash 256 du fichier' AFTER `type_signature_fichier`;


-- AME MPE-4102
ALTER TABLE `t_document_type`	ADD COLUMN `synchro_actif` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet d''activer la synchronisation l''attestation ou la desactiver' AFTER `type_retour_ws`;
ALTER TABLE `t_document_type`	ADD COLUMN `message_desactivation_synchro` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Message lié à la désactivation de la synchro' AFTER `synchro_actif`;
ALTER TABLE `t_document_type`	ADD COLUMN `afficher_type_doc` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le type doc au niveau de la liste des documents' AFTER `message_desactivation_synchro`;



-- AME MPE-4103
CREATE TABLE `t_attestations_offres` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_offre` INT(11) NOT NULL,
	`id_document_entreprise` INT(11) NULL DEFAULT NULL,
	`id_document_version` INT(11) NULL DEFAULT NULL,
	`id_type_document` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;


-- AME MPE-1151
ALTER TABLE `HabilitationAgent`	ADD COLUMN `gerer_messages_accueil` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet de gerer la page de parametrage des messages d''accueil ' ;
ALTER TABLE `HabilitationProfil`	ADD COLUMN `gerer_messages_accueil` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet de gerer la page de parametrage des messages d''accueil ' ;

-- AME_4255
ALTER TABLE `Offres` DROP FOREIGN KEY `Offres_etablissement_id_fk`;

-- AME MPE-4608
ALTER TABLE `t_annonce_consultation` CHANGE COLUMN `id_type_annonce` `id_type_annonce` INT(11) NULL COMMENT 'Identifiant du type d''annonce' ;

-- AME MPE-4583
ALTER TABLE `t_support_publication`	ADD COLUMN `lieux_execution` TEXT NULL DEFAULT NULL COMMENT 'Permet de parametrer les lieux d''execution il faut utiliser le separateur # et tjr commencer # et terminer par #';

