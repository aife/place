-- mkh mpe-7581
ALTER TABLE `configuration_plateforme` ADD COLUMN `entreprise_mots_de_passe_historises` INT(2) NOT NULL DEFAULT 0 AFTER `interface_dume`;
ALTER TABLE `configuration_plateforme` ADD COLUMN `entreprise_duree_vie_mot_de_passe` INT(2) NOT NULL DEFAULT 0 AFTER `interface_dume`;

-- mkh mpe-7582
CREATE TABLE IF NOT EXISTS `historisation_mot_de_passe` ( `id` INT NOT NULL AUTO_INCREMENT , `ancien_mot_de_passe` VARCHAR(64) NOT NULL  ,  `date_modification` DATETIME NOT NULL , `id_inscrit` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `historisation_mot_de_passe` ADD CONSTRAINT `id_inscrit_histo_mdp_FK` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`);