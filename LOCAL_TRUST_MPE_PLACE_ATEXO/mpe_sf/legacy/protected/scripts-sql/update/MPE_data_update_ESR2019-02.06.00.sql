-- FMA MPE-10439
-- Partie prado
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/authentification/connexion/', 'GET', 'generation token (authentification)');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/annonceservice/create', 'POST', 'création annonce service');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/annonceservice/update', 'POST', 'update annonce service');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/annonceservice/get/', 'GET', 'get annonce service');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/consultation/create', 'POST', 'création consultation');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/consultation/update', 'POST', 'update consultation');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/consultation/get/', 'GET', 'get consultation');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/tableaudebord', 'POST', 'tableaubord');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/questions/', 'GET', 'get questions');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/question/', 'GET', 'get question');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/retraitdces/', 'GET', 'get retrait DCES');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/retraitdce/', 'GET', 'get retrait DCE');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/depots/', 'GET', 'get depots');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/depot/', 'GET', 'get depot');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/depot/update', 'POST', 'update depots');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/pli/', 'GET', 'get pli');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/pli/', 'POST', 'post pli');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/contactsBySiret/get/', 'GET', 'get contacts par Siret');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/contactsByEntrepriseId/get/', 'GET', 'get contacts par id Entreprise');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/entrepriseByRaisonSociale/get/', 'GET', 'get entreprise par Raison Sociale');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/entrepriseBySiren/get/', 'GET', 'get entreprise par Siret');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/archive/listeid/', 'GET', 'get archive liste');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/archive/fichier/', 'GET', 'get archive fichier');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/organismes', 'GET', 'get organismes');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/organisme/', 'GET', 'get organisme');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/document/', 'GET', 'get document');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/document/', 'POST', 'post document');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/depot/admissibilite/', 'GET', 'get depot admissibilite');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/depot/decision/', 'GET', 'get depot decision');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/offre/admissibilite/', 'GET', 'get offre admissibilite');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/offre/decision/', 'GET', 'get offre decision');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/echanges/', 'GET', 'get registre echanges');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/registre/echange/', 'GET', 'get registre echange');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/fichier/', 'GET', 'get fichier');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/document/synchro/', 'GET', 'get document synchro');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/', 'GET', 'get WS');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/', 'POST', 'post WS');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api.php/ws/', 'PUT', 'put WS');
-- Partie SF
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/referentiels/procedureContrats', 'GET', 'get procedure Contrats');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/referentiels', 'GET', 'get referentiels');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/agents.{format}', 'GET', 'get agents');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/agents.{format}', 'POST', 'post agents');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/agents.{format}', 'PUT', 'put agents');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/agents.{format}/habilitations', 'GET', 'get agents habilitations');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/contacts.{format}', 'GET', 'get contacts');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/contrats.{format}', 'GET', 'get contrats');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/donnees-essentielles/contrat/format-etendu', 'POST', 'post donnees-essentielles format-etendu');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/donnees-essentielles/contrat/format-pivot', 'POST', 'post donnees-essentielles format-pivot');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/entreprises.{format}', 'GET', 'get entreprises');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/etablissements.{format}', 'GET', 'get etablissements');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/monitoring/jmeter/hash/{scenario}.{format}', 'GET', 'get monitoring jmeter hash');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/monitoring/jmeter/{scenario}', 'GET', 'get monitoring jmeter');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/organismes.{format}', 'GET', 'get organismes (sous sf)');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/services.{format}', 'GET', 'get services');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/services.{format}', 'PATCH', 'PATCH services');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/services.{format}', 'POST', 'POST services');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/services.{format}', 'PUT', 'PUT services');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/tableau-de-bord/consultations-en-cours.{format}', 'GET', 'get tableau-de-bord consultations-en-cours');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/tableau-de-bord/contrats.{format}', 'GET', 'get tableau-de-bord contrats');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/tableau-de-bord/questions.{format}', 'GET', 'get tableau-de-bord questions');

-- FMA MPE-12111
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/statistiques/interfaces.{format}', 'GET', 'GET Statistique interface');
INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/statistiques/interfaces.{format}', 'POST', 'POST Statistique interface');

-- FMA MPE-11502
INSERT INTO jms_jobs (state, queue, priority, createdAt, startedAt, executeAfter, closedAt, command, args, output, errorOutput, exitCode, maxRuntime, maxRetries) SELECT DISTINCT  'pending', 'default', 0, CURRENT_DATE , null, CURRENT_DATE, null, 'mpe:migration:tiers', '', null,null, null, 0, 0 FROM jms_jobs as J where NOT EXISTS (SELECT 1 from  jms_jobs as jm where jm.command like 'mpe:migration:tiers');

INSERT INTO `Tiers` (`login`, `password`, `denomination`, `fonctionnalite`, `organisme`) SELECT DISTINCT  'mpe_oversight', '59is6onej32w', 'mpe_oversight', '4', NULL FROM Tiers where NOT EXISTS (SELECT 1 from  Tiers where Tiers.login like 'mpe_oversight');
INSERT INTO jms_jobs (queue, state, priority, createdAt, startedAt, executeAfter, closedAt, command, args, output, errorOutput, exitCode, maxRuntime, maxRetries) SELECT DISTINCT  'default', 'pending', 0, CURRENT_DATE , null, CURRENT_DATE, null, 'mpe:migration:tiers', '', null,null, null, 0, 0 FROM jms_jobs as J where NOT EXISTS (SELECT 1 from  jms_jobs as jm where jm.command like 'mpe:migration:tiers' AND state = 'pending');
