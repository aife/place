/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

--MPE-13924
UPDATE `t_contrat_titulaire` ct INNER JOIN Offres o ON ct.id_offre = o.id
INNER JOIN consultation c ON o.consultation_ref = c.reference AND o.organisme = c.organisme
SET ct.clause_sociale_condition_execution = c.clause_sociale_condition_execution
WHERE ct.clause_sociale_condition_execution = 'a:1(i:0;s';


UPDATE `t_contrat_titulaire` ct INNER JOIN Offres o ON ct.id_offre = o.id
INNER JOIN consultation c ON o.consultation_ref = c.reference AND o.organisme = c.organisme
SET ct.clause_specification_technique = c.clause_specification_technique
WHERE ct.clause_specification_technique = 'a:1(i:0;s';



UPDATE `t_contrat_titulaire` ct INNER JOIN Offres o ON ct.id_offre = o.id
INNER JOIN consultation c ON o.consultation_ref = c.reference AND o.organisme = c.organisme
SET ct.clause_sociale_insertion = c.clause_sociale_insertion
WHERE ct.clause_sociale_insertion = 'a:1(i:0;s';




