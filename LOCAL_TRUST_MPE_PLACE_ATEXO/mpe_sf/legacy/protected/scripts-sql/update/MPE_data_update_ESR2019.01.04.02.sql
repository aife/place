-- SLE MPE-11245
ALTER TABLE ProcedureEquivalence ALTER donnees_complementaire_non SET DEFAULT '+1';
ALTER TABLE ProcedureEquivalence ALTER donnees_complementaire_oui SET DEFAULT '+0';
UPDATE ProcedureEquivalence SET donnees_complementaire_non = '+1', donnees_complementaire_oui = '+0';
