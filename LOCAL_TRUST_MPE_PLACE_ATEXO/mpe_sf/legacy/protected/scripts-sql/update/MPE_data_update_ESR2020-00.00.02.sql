-- MPE-12492
INSERT INTO consultation_favoris (id_consultation, id_agent) (SELECT cons.id as id_consultation, ag.id as id_agent FROM `HabilitationAgent` as ha, consultation as cons, Organisme as org, Agent as ag WHERE org.acronyme = cons.organisme and ag.id = ha.`id_agent` and org.acronyme = ag.organisme and suivre_consultation_pole = '1') ON DUPLICATE KEY UPDATE consultation_favoris.id_consultation = cons.id,id_agent= ag.id;

-- MPE-12511
update `Entreprise` set id_externe=id_initial WHERE  `id_initial`!=0 and id_externe=0;
update `t_etablissement` set id_externe=id_initial WHERE  `id_initial`!=0 and id_externe=0;
UPDATE `Organisme` set id_externe=id_initial WHERE `id_initial`!=0 and id_externe=0;