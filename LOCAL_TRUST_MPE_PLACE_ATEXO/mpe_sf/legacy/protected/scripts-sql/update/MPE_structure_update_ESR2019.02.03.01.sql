ALTER TABLE supervision_interface CHANGE `nom_batch` `webservice_batch` VARCHAR(50) NOT NULL COMMENT 'Nom du batch ou du webservice';
ALTER TABLE supervision_interface CHANGE `date_min` `created_date`  DATE NOT NULL COMMENT 'Date de création';
ALTER TABLE supervision_interface CHANGE `nom_interface` `nom_interface`  VARCHAR(50) COMMENT 'Nom de l''interface utilisé par le rapports';
ALTER TABLE supervision_interface DROP date_min_format, DROP date_max, DROP pourcentage_ok;

-- MKH MPE-9867
ALTER TABLE `Enveloppe` ADD INDEX `enveloppe_statut_enveloppe` (`statut_enveloppe`);
