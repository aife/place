<?php
$listSqlFiles = glob(__DIR__.'/MPE_structure_update_*.sql');

foreach ($listSqlFiles as $file) {
    //Si le fichier courant est seulement du passage à l'ESR2020
    if (
        strpos($file, 'MPE_structure_update_ESR2019.99.00.00.sql') !== false
        && file_exists($queriesStructureExecuteFile)
    ) {
        $contentBkp = unserialize(file_get_contents($queriesStructureExecuteFile));
        $fileContent = file_get_contents($file);
        $queriesBkp = is_array($contentBkp) ? $contentBkp : array();
        preg_match_all("/^((ALTER|CREATE|DROP|UPDATE) .*;)\s*(?=$)$/Usm", $fileContent, $sqlToExecute);
        $alreadyExecuted=false;
        foreach ($sqlToExecute[1] as $query) {
            $base64Query = base64_encode($query);
            if (!empty($queriesBkp[$base64Query])) {
                $alreadyExecuted = true;
                break;
            }
        }
        //Si aucune requête du fichier a été joué alors nous incluons le fichier (nouvelle migration)
        // Sinon on fait rien sur ce fichier (client déjà migré).
        if (!$alreadyExecuted) {
            echo file_get_contents($file) . PHP_EOL;
        }
    } else {
        echo file_get_contents($file) . PHP_EOL;
    }
}