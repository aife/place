-- AME MPE-3862
ALTER TABLE `Telechargement` ADD COLUMN `poids_telechargement` INT(11) NOT NULL DEFAULT '0' COMMENT 'Taille des fichiers téléchargés' AFTER `acronyme_pays`;
ALTER TABLE `TelechargementAnonyme` ADD COLUMN `poids_telechargement` INT(11) NOT NULL DEFAULT '0' COMMENT 'Taille des fichiers téléchargés' AFTER `noms_fichiers_dce`;
