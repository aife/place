ALTER TABLE `consultation` ADD COLUMN `numero_projet_achat` VARCHAR(255) DEFAULT NULL;
ALTER TABLE `configuration_organisme` ADD COLUMN `numero_projet_achat` ENUM('0','1') NOT NULL DEFAULT '0';
-- NBS-MPE-8632
ALTER TABLE `configuration_plateforme` ADD COLUMN `masquer_elements_mps` ENUM('0','1') NOT NULL DEFAULT '1';
-- AME MPE-9213
ALTER TABLE `configuration_organisme`	ADD COLUMN `module_exec` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la communication avec le module EXEC';
-- AME MPE-9214
ALTER TABLE `Service`	ADD COLUMN `synchronisation_exec` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet d''activer la synchronisation avec le module EXEC';
-- AME MPE-9221
ALTER TABLE `HabilitationProfil`	ADD COLUMN `exec_voir_contrats` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''accéder au module EXEC et visualiser ses contrats';
ALTER TABLE `HabilitationProfil`	ADD COLUMN `exec_voir_contrats_ea` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de Visualiser les contrats de l''Entité d''Achat au module EXEC';
ALTER TABLE `HabilitationProfil`	ADD COLUMN `exec_voir_contrats_ea_dependantes` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de visualiser les contrats de l''Entité d''Achat et des Entités d''Achat dépendantes au module EXEC';
ALTER TABLE `HabilitationProfil`	ADD COLUMN `exec_voir_contrats_organisme` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de visualiser les contrats de l''Organisme au module EXEC';
ALTER TABLE `HabilitationAgent`	ADD COLUMN `exec_voir_contrats` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''accéder au module EXEC et visualiser ses contrats';
ALTER TABLE `HabilitationAgent`	ADD COLUMN `exec_voir_contrats_ea` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de Visualiser les contrats de l''Entité d''Achat au module EXEC';
ALTER TABLE `HabilitationAgent`	ADD COLUMN `exec_voir_contrats_ea_dependantes` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de visualiser les contrats de l''Entité d''Achat et des Entités d''Achat dépendantes au module EXEC';
ALTER TABLE `HabilitationAgent`	ADD COLUMN `exec_voir_contrats_organisme` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de visualiser les contrats de l''Organisme au module EXEC';

ALTER TABLE `Offres` ADD INDEX `offres_statut_offres` (`statut_offres`);

ALTER TABLE `t_attestations_offres` ADD INDEX `t_attestations_offres_id_offre` (`id_offre`);
ALTER TABLE `t_attestations_offres` ADD INDEX `t_attestations_offres_id_type_document` (`id_type_document`);

ALTER TABLE `SuiviAcces` ADD INDEX `suivi_acces_date_acces` (`date_acces`);

ALTER TABLE `Telechargement` ADD INDEX `telechargement_datetelechargement` (`datetelechargement`);
ALTER TABLE `TelechargementAnonyme` ADD INDEX `telechargementAnonyme_datetelechargement` (`datetelechargement`);

ALTER TABLE `T_trace_operations_inscrit_details` ADD INDEX `t_trace_operations_inscrit_details_date_debut_action` (`date_debut_action`);
