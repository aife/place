-- MKH-MPE-8237
-- on initialise created_at et updated_at du contact avec celle du contrat.
UPDATE `t_contrat_titulaire` contrat INNER JOIN `t_contact_contrat` contact ON contrat.id_contact_contrat = contact.id_contact_contrat SET contact.`updated_at` = contrat.date_modification;
UPDATE `t_contrat_titulaire` contrat INNER JOIN `t_contact_contrat` contact ON contrat.id_contact_contrat = contact.id_contact_contrat SET contact.`created_at` = contrat.date_creation;

-- MKH-MPE-8378
UPDATE `Agent` set  `date_modification` = '1970-01-01 01:01:01' where `date_modification` IS NULL;
UPDATE `Agent` set  `date_creation` = `date_modification` where `date_creation` IS NULL;

UPDATE `Service` set  `date_modification` = '1970-01-01 01:01:01' where `date_modification` IS NULL;
UPDATE `Service` set  `date_creation` = `date_modification` where `date_creation` IS NULL;

UPDATE `Entreprise` set  `date_modification` = '1970-01-01 01:01:01'  where `date_modification` = '';
UPDATE `Entreprise` set  `date_creation` = `date_modification` where `date_creation` = '';

UPDATE `t_etablissement` set  `date_modification` = '1970-01-01 01:01:01' where `date_modification` = '0000-00-00 00:00:00';
UPDATE `t_etablissement` set  `date_creation` = '1970-01-01 01:01:01' where `date_creation` = '0000-00-00 00:00:00';
