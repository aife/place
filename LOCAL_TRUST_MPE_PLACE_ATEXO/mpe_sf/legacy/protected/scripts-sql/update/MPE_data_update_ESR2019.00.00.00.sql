-- MPE-7675

UPDATE `type_procedure_mpe_pivot` SET `id_type_procedure_pivot` = '6' WHERE `type_procedure_mpe_pivot`.`id_type_procedure_mpe` = 8;

-- FMA MPE-7447
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`, `valeur_sub`, `ordre_affichage`) VALUES (NULL, '210', 'Téléchargement des annexes techniques du DCE', 'Téléchargement des annexes techniques du DCE', '', '', '', '', '', '', 'ANNEXES1', '', '', '0');
INSERT INTO `ValeurReferentiel` (`id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_2`, `libelle_valeur_referentiel_it`) SELECT * FROM (SELECT 210, 'Téléchargement des annexes techniques du DCE', 'Téléchargement des annexes techniques du DCE', 'ANNEXES1', '') AS tmp WHERE NOT EXISTS (SELECT libelle_2 FROM ValeurReferentiel WHERE libelle_2='ANNEXES1')LIMIT 1;
