-- LEZ MPE-4605
INSERT INTO `TypeAvis` (`id`, `intitule_avis`, `intitule_avis_fr`, `intitule_avis_en`, `intitule_avis_es`, `abbreviation`, `id_type_avis_ANM`) VALUES(9, 'Projet d''achat', 'Projet d''achat', 'en Projet d''achat ', 'es Projet d''achat ',  'APA', 9);

-- NCH MPE-5814
INSERT INTO `t_support_publication` (`id`, `image_logo`, `nom`, `visible`, `ordre`, `default_value`, `code`, `actif`, `url`, `groupe`, `tag_debut_fin_groupe`, `description`, `nbre_total_groupe`, `lieux_execution`) VALUES ('20', 'support-LAL.png', 'Le journal des Annonces Landaises', '0', '19', '1', 'LAL', '0', 'https://annonces-landaises.com/', '19', '2', NULL, 1, NULL);
INSERT INTO `t_Offre_Support_Publicite` (`id`, `libelle_offre`, `id_support`, `actif`, `logo`, `mapa`, `montant_inf`, `montant_max`, `code`, `ordre`) VALUES (NULL, '< 90 K&euro; = 2 UP', '20', '1', 'NULL', 2, 0, 90000, 'LAL_Inf90_avisResume', 1);
INSERT INTO `t_Offre_Support_Publicite` (`id`, `libelle_offre`, `id_support`, `actif`, `logo`, `mapa`, `montant_inf`, `montant_max`, `code`, `ordre`) VALUES (NULL, '> 90 K&euro; = 6 UP', '20', '1', 'NULL', 2, 90000, 0, 'LAL_Sup90_avisIntegral', 2);


-- NCH MPE-5101
UPDATE `GeolocalisationN2` SET type_lieu = 1 WHERE id_geolocalisationN1 != 234 AND id_geolocalisationN1 != 233;

INSERT INTO `type_procedure_pivot`
    (id, libelle)
VALUES
    (1, 'PROCEDURE_ADAPTEE'),
    (2, 'TEXT_APPEL_OFFRE_OUVERT'),
    (3, 'APPEL_OFFRES_RESTREINT'),
    (4, 'TEXT_PROCEDURE_CONCURENTIELLE_AVEC_NEGOCIATION'),
    (5, 'TEXT_PROCEDURE_CONCURENTIELLE_AVEC_CONCURRENCE'),
    (6, 'TEXT_PROCEDURE_CONCURENTIELLE_SANS_NEGOCIATION_NI_CONCURRENCE'),
    (7, 'DIALOGUE_COMPETITIF');

INSERT INTO `type_procedure_mpe_pivot`
    (id_type_procedure_mpe, id_type_procedure_pivot)
VALUES
    (1, 2),
    (2, 3),
    (3, 1),
    (4, 2),
    (5, 3),
    (7, 7),
    (8, 5),
    (9, 5),
    (10, 2),
    (11, 2),
    (15, 2),
    (30, 2),
    (31, 2),
    (32, 2),
    (33, 2),
    (35, 2),
    (1000, 4);

INSERT INTO `type_contrat_pivot`
    (id, libelle)
VALUES
    (1, 'TYPE_CONTRAT_MARCHE'),
    (2, 'TYPE_CONTRAT_PARTENARIAT_PUBLIC_PRIVE'),
    (3, 'DEFINE_ACCORD_CADRES'),
    (4, 'TEXT_MARCHE_SUBSEQUENT');

INSERT INTO `type_contrat_mpe_pivot`
    (`id`, `id_type_contrat_mpe`, `id_type_contrat_pivot`)
VALUES
    (1, 1, 1),
    (2, 2, 3),
    (3, 3, 3),
    (4, 4, 1),
    (5, 5, 3),
    (6, 6, 3),
    (7, 7, 4),
    (8, 8, 2),
    (9, 9, 1),
    (10, 10, 1),
    (11, 11, 1),
    (12, 12, 1),
    (13, 13, 3)
ON DUPLICATE KEY UPDATE
`id` = VALUES(`id`),
`id_type_contrat_mpe` = VALUES(`id_type_contrat_mpe`),
`id_type_contrat_pivot` = VALUES(`id_type_contrat_pivot`);

-- NCH MPE-5601
INSERT INTO `t_Offre_Support_Publicite`
  (`id`, `libelle_offre`, `id_support`, `actif`, `logo`, `mapa`, `montant_inf`, `montant_max`, `code`, `ordre`)
VALUES
  (24, 'Avis initial - Marché > 90K&euro;', 4, '1', NULL, 2, 90000, 0, NULL, NULL),
  (30, 'Avis initial - Marché < 90K&euro;', 4, '1', NULL, 2, 0, 90000, NULL, NULL),
  (40, 'Avis de pré-information (1UP, cf tarif pack)', 8, '1', NULL, 0, 0, 0, '28_EMARCHEPUBLIC', 3),
  (41, 'Avis de marché < 90 K&euro;   (1UP, cf tarif pack)', 8, '1', NULL, 1, 0, 90000, '29_EMARCHEPUBLIC', 0),
  (42, 'Avis de marché >= 90 K€  (8 UP, cf tarif pack)', 8, '1', NULL, 2, 90000, 0, '30_EMARCHEPUBLIC', 0),
  (45, 'Autre avis - E-marchespublics.com (8 UP, cf tarif pack)', 8, '1', NULL, 2, 0, 0, '37_EMARCHEPUBLIC', 0),
  (48, 'Avis de marché >= 90 K€ - Le Parisien + E-marchespublics.com (18 UP, cf tarif pack)', 19, '1', NULL, 2, 90000, 0, '33_LEPARISIENBIMEDIA', 0),
  (49, 'Avis de marché < 90 K€ - Le Parisien (avis résumé) + E-marchespublics.com  (6 UP, cf tarif pack)', 19, '1', NULL, 2, 0, 90000, '34_LEPARISIENBIMEDIA', 0),
  (46, 'Avis de marché >=  90 K€ - Les Echos + site E-marchespublics (20 UP, cf tarif pack)', 18, '1', NULL, 2, 90000, 0, '31_LESECHOSBIMEDIA', 0),
  (47, 'Avis de marché < 90 K€ - Les Echos (avis résumé) + E-marchespublics.com  (6 UP, cf tarif pack)', 18, '1', NULL, 2, 0, 90000, '32_LESECHOSBIMEDIA', 0),
  (36, 'Avis de marché < 90 K€ - 1 dpt - avis résumé (300 € ou 5 UP)', 5, '1', NULL, 1, 0, 90000, '22_LEPARISIEN', 0),
  (37, 'Avis de marché >= 90 K€ - 1 dpt (550 € ou 10 UP)', 5, '1', NULL, 2, 90000, 0, '23_LEPARISIEN', 0),
  (38, 'Avis de DSP / Concessions - 1 dpt  (990 € ou 20 UP)', 5, '1', NULL, 0, 0, 0, '24_LEPARISIEN', 4),
  (33, 'Avis de DSP / Concessions - 1 dpt  (990 € ou 20 UP)', 2, '1', NULL, 1, 0, 90000, '17_LESECHOS', 0),
  (34, 'Avis de DSP / Concessions - 1 dpt  (990 € ou 20 UP)', 2, '1', NULL, 2, 90000, 0, '18_LESECHOS', 0),
  (35, 'Avis de DSP / Concessions - 1 dpt  (990 € ou 20 UP)', 2, '1', NULL, 0, 0, 0, '19_LESECHOS', 3)
ON DUPLICATE KEY UPDATE
  `id`     = VALUES(`id`),
  `libelle_offre` = VALUES(`libelle_offre`),
  `id_support` = VALUES(`id_support`),
  `actif` = VALUES(`actif`),
  `logo` = VALUES(`logo`),
  `mapa` = VALUES(`mapa`),
  `montant_inf` = VALUES(`montant_inf`),
  `montant_max` = VALUES(`montant_max`),
  `code` = VALUES(`code`),
  `ordre` = VALUES(`ordre`);

DELETE FROM `t_Offre_Support_Publicite` WHERE `id` = 39;

-- NCH MPE-5603
UPDATE `t_support_publication` SET `type_info` = 3, `affichage_infos` = '1', `detail_info` = 'https://www.e-marchespublics.com/documents/tarifs_les_echos_leparisien-emp.html' WHERE `id` IN (2, 5, 8, 18, 19);

-- NCH MPE-5605
UPDATE `t_support_publication` SET `visible` = '1', `actif` = '1' WHERE `id` = 8;


-- NCH MPE-5607
UPDATE `t_support_publication` SET `affichage_message_support` = '1' WHERE `id` IN (2, 5, 8, 18, 19);

-- LEZ MPE-5602

UPDATE `t_support_publication` SET `departements_parution` = '254,256,257,258,259,260,261,307' WHERE `id` = 2;
UPDATE `t_support_publication` SET `departements_parution` = '254,255,256,257,258,259,260,261,290' WHERE `id` = 5;



-- NCH MPE-5814
INSERT INTO `t_support_publication` (`id`, `image_logo`, `nom`, `visible`, `ordre`, `default_value`, `code`, `actif`, `url`, `groupe`, `tag_debut_fin_groupe`, `description`, `nbre_total_groupe`, `lieux_execution`) VALUES ('20', 'support-LAL.png', 'Le journal des Annonces Landaises', '0', '19', '1', 'LAL', '0', 'https://annonces-landaises.com/', '19', '2', NULL, 1, NULL);
INSERT INTO `t_Offre_Support_Publicite` (`id`, `libelle_offre`, `id_support`, `actif`, `logo`, `mapa`, `montant_inf`, `montant_max`, `code`, `ordre`) VALUES (NULL, '< 90 K&euro; = 2 UP', '20', '1', 'NULL', 2, 0, 90000, 'LAL_Inf90_avisResume', 1);
INSERT INTO `t_Offre_Support_Publicite` (`id`, `libelle_offre`, `id_support`, `actif`, `logo`, `mapa`, `montant_inf`, `montant_max`, `code`, `ordre`) VALUES (NULL, '> 90 K&euro; = 6 UP', '20', '1', 'NULL', 2, 90000, 0, 'LAL_Sup90_avisIntegral', 2);

-- LEZ MPE-5782
UPDATE `t_type_contrat` SET `article_133`='1' WHERE  1;

-- NCH MPE-3378
UPDATE `Inscrit` SET `date_expiration_mdp` = DATE_ADD(NOW(), INTERVAL 100 DAY);
UPDATE `Agent` SET `date_expiration_mdp` = DATE_ADD(NOW(), INTERVAL 100 DAY);




-- FMA MPE-5618
TRUNCATE `categories_considerations_sociales`;

INSERT INTO `categories_considerations_sociales` (`id`, `label`, `code`) VALUES (1, 'Insertion par l''activité économique', 'insertionActiviteEconomique');
INSERT INTO `categories_considerations_sociales` (`id`, `label`, `code`) VALUES (2, 'Clause sociale de formation sous statut scolaire au bénéfice de jeunes en situation de décrochage scolaire', 'clauseSocialeFormationScolaire');
INSERT INTO `categories_considerations_sociales` (`id`, `label`, `code`) VALUES (3, 'Lutte contre les discriminations : Egalité femmes/hommes, etc.', 'lutteContreDiscriminations');
INSERT INTO `categories_considerations_sociales` (`id`, `label`, `code`) VALUES (4, 'Commerce équitable', 'commerceEquitable');
INSERT INTO `categories_considerations_sociales` (`id`, `label`, `code`) VALUES (5, 'Achats éthiques, traçabilité sociale des services/fournitures, etc.', 'achatsEthiquesTracabiliteSociale');
INSERT INTO `categories_considerations_sociales` (`id`, `label`, `code`) VALUES (6, 'Autre(s) clause(s) sociale(s)', 'autreClauseSociale');

-- LEZ MPE-5632
-- TRUNCATE `categories_considerations_sociales`;

-- INSERT INTO `categories_considerations_sociales` (`id`, `label`) VALUES (1, 'Insertion par l''activité économique');
-- INSERT INTO `categories_considerations_sociales` (`id`, `label`) VALUES (2, 'Clause sociale de formation sous statut scolaire au bénéfice de jeunes en situation de décrochage scolaire');
-- INSERT INTO `categories_considerations_sociales` (`id`, `label`) VALUES (3, 'Lutte contre les discriminations : Egalité femmes/hommes, etc.');
-- INSERT INTO `categories_considerations_sociales` (`id`, `label`) VALUES (4, 'Commerce équitable');
-- INSERT INTO `categories_considerations_sociales` (`id`, `label`) VALUES (5, 'Achats éthiques, traçabilité sociale des services/fournitures, etc.');
-- INSERT INTO `categories_considerations_sociales` (`id`, `label`) VALUES (6, 'Autre(s) clause(s) sociale(s)');
--
-- UPDATE `categories_considerations_sociales` SET code='insertionActiviteEconomique' where id=1;
-- UPDATE `categories_considerations_sociales` SET code='clauseSocialeFormationScolaire' where id=2;
-- UPDATE `categories_considerations_sociales` SET code='lutteContreDiscriminations' where id=3;
-- UPDATE `categories_considerations_sociales` SET code='commerceEquitable' where id=4;
-- UPDATE `categories_considerations_sociales` SET code='achatsEthiquesTracabiliteSociale' where id=5;
-- UPDATE `categories_considerations_sociales` SET code='autreClauseSociale' where id=6;

-- FMA MPE-6274
UPDATE `t_type_contrat` SET `concession` = '1' WHERE `t_type_contrat`.`id_type_contrat` = 4;

INSERT INTO `type_contrat_concession_pivot` (`id`, `libelle`) VALUES (1, 'Concession de travaux');
INSERT INTO `type_contrat_concession_pivot` (`id`, `libelle`) VALUES (2, 'Concession de service');
INSERT INTO `type_contrat_concession_pivot` (`id`, `libelle`) VALUES (3, 'Concession de service public');
INSERT INTO `type_contrat_concession_pivot` (`id`, `libelle`) VALUES (4, 'Délégation de service public');

-- APE MPE-6051
INSERT INTO `code_retour` (`code`, `description`) VALUES ('3', 'Ce code est retourné lorsqu’une ressource n’est pas trouvé');


-- APE MPE-6010
UPDATE `t_donnee_complementaire` SET `id_duree_delai_description` = 0 WHERE `id_duree_delai_description` = 2;

UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Durée ferme', `libelle_valeur_referentiel_fr` = 'Durée ferme' WHERE `id` = 1 AND `id_referentiel` = 18;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Description libre de la durée du marché', `libelle_valeur_referentiel_fr` = 'Description libre de la durée du marché' WHERE `id` = 3 AND `id_referentiel` = 18;
DELETE FROM `ValeurReferentiel` WHERE `id` = 2 AND `id_referentiel` = 18;

UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Critères énoncés ci après (par ordre de priorité décroissante)', `libelle_valeur_referentiel_fr` = 'Critères énoncés ci après (par ordre de priorité décroissante)', `ordre_affichage` = 4 WHERE `id` = 1 AND `id_referentiel` = 19;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Critères énoncés ci-après avec leur pondération', `libelle_valeur_referentiel_fr` = 'Critères énoncés ci-après avec leur pondération', `ordre_affichage` = 3 WHERE `id` = 2 AND `id_referentiel` = 19;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Critère unique de prix', `libelle_valeur_referentiel_fr` = 'Critère unique de prix', `ordre_affichage` = 2 WHERE `id` = 3 AND `id_referentiel` = 19;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Critères énoncés dans les documents de la consultation', `libelle_valeur_referentiel_fr` = 'Critères énoncés dans les documents de la consultation', `ordre_affichage` = 5 WHERE `id` = 4 AND `id_referentiel` = 19;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Critère unique de coût', `libelle_valeur_referentiel_fr` = 'Critère unique de coût', `ordre_affichage` = 1 WHERE `id` = 5 AND `id_referentiel` = 19;

UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel`= 'CCAG-Tx', `libelle_valeur_referentiel_fr`= 'CCAG-Tx' WHERE `id` = 3 AND `id_referentiel` = 20;

-- FMA MPE-6274-bis
INSERT INTO `type_procedure_concession_pivot` (`id`, `libelle`) VALUES (1, 'Procédure négociée ouverte');
INSERT INTO `type_procedure_concession_pivot` (`id`, `libelle`) VALUES (2, 'Procédure non négociée ouverte');
INSERT INTO `type_procedure_concession_pivot` (`id`, `libelle`) VALUES (3, 'Procédure négociée restreinte');
INSERT INTO `type_procedure_concession_pivot` (`id`, `libelle`) VALUES (4, 'Procédure non négociée restreinte');

-- LEZ MPE-6581
INSERT INTO jms_jobs (state, queue, priority, createdAt, startedAt, executeAfter, closedAt, command, args, output, errorOutput, exitCode, maxRuntime, maxRetries) SELECT DISTINCT  'pending', 'default', 0, CURRENT_DATE , null, "2019-01-01 00:00:00", null, 'mpe:update:marchePublie', '[2019]', null,null, null, 0, 0 FROM jms_jobs as J where NOT EXISTS (SELECT 1 from  jms_jobs as jm where jm.command like 'mpe:update:marchePublie' AND jm.args like '[2019]');

-- LEZ MPE-6528
UPDATE `configuration_plateforme` SET `article_133_generation_pf` = '1' ;

-- AME MPE-6958

INSERT INTO `t_type_contrat` (`id_type_contrat`, `libelle_type_contrat`, `abreviation_type_contrat`, `type_contrat_statistique`, `multi`, `accord_cadre_sad`, `avec_chapeau`, `avec_montant`, `mode_echange_chorus`, `marche_subsequent`, `avec_montant_max`, `ordre_affichage`, `article_133`, `code_dume`, `concession`) VALUES (14, 'TYPE_CONTRAT_MARCHE_GLOBAL_PERFORMANCE', 'AU', 0, '0', '0', '0', '1', '1', '0', '0', 14, '1', '06', 0);
INSERT INTO `t_type_contrat` (`id_type_contrat`, `libelle_type_contrat`, `abreviation_type_contrat`, `type_contrat_statistique`, `multi`, `accord_cadre_sad`, `avec_chapeau`, `avec_montant`, `mode_echange_chorus`, `marche_subsequent`, `avec_montant_max`, `ordre_affichage`, `article_133`, `code_dume`, `concession`) VALUES (15, 'TYPE_CONTRAT_MARCHE_SECTORIEL', 'AU', 0, '0', '0', '0', '1', '1', '0', '0', 13, '1', '06', 0);


-- AME MPE-6955

UPDATE Type_Procedure_Organisme SET id_externe = abreviation_interface WHERE mapa ='0';
UPDATE Type_Procedure_Organisme SET id_externe = 'PA-INF' WHERE mapa = '1' AND id_montant_mapa = '1';
UPDATE Type_Procedure_Organisme SET id_externe = 'PA-SUP' WHERE mapa = '1' AND id_montant_mapa = '2';


UPDATE `t_type_contrat` SET `id_externe`='mar' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_MARCHE';
UPDATE `t_type_contrat` SET `id_externe`='amo' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_AC_MONO_ATTRIBUTAIRE';
UPDATE `t_type_contrat` SET `id_externe`='sad' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_SYSTEME_ACQUISITION_DYNAMIQUE';
UPDATE `t_type_contrat` SET `id_externe`='dsp' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_DELEGATION_DE_SERVICE_PUBLIC';
UPDATE `t_type_contrat` SET `id_externe`='amu' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_AC_MULTI_ATTRIBUTAIRE';
UPDATE `t_type_contrat` SET `id_externe`='marmu' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_MARCHE_PUBLIC_MULTI_ATTRIBUTAIRES';
UPDATE `t_type_contrat` SET `id_externe`='msa' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_MARCHE_SUBSEQUENT';
UPDATE `t_type_contrat` SET `id_externe`='ppp' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_PARTENARIAT_PUBLIC_PRIVE';
UPDATE `t_type_contrat` SET `id_externe`='pdi' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_PARTENARIAT_D_INNOVATION';
UPDATE `t_type_contrat` SET `id_externe`='marsp' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_MARCHE_SPECIFIQUE';
UPDATE `t_type_contrat` SET `id_externe`='ccm' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_MARCHE_DE_CONCEPTION_REALISATION';
UPDATE `t_type_contrat` SET `id_externe`='aut' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_AUTRE';
UPDATE `t_type_contrat` SET `id_externe`='abcmo' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_ACCORD_CADRE_BON_COMMANDE_MONO_ATTRIBUTAIRE';
UPDATE `t_type_contrat` SET `id_externe`='margp' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_MARCHE_GLOBAL_PERFORMANCE';
UPDATE `t_type_contrat` SET `id_externe`='margs' WHERE  `libelle_type_contrat`='TYPE_CONTRAT_MARCHE_SECTORIEL';

-- LEZ MPE-6785
INSERT INTO `ValeurReferentiel` (`id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_2`, `libelle_valeur_referentiel_it`) SELECT * FROM (SELECT 210, 'Téléchargement (download) du DUME Acheteur', 'Téléchargement (download) du DUME Acheteur ', 'DESCRIPTION44', '') AS tmp WHERE NOT EXISTS (SELECT libelle_2 FROM ValeurReferentiel WHERE libelle_2='DESCRIPTION44')LIMIT 1;

-- LEZ MPE-6526
UPDATE `t_tranche` SET  `nature_tranche`='TO' where `nature_tranche` like '';
