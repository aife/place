INSERT INTO jms_jobs (state, queue, priority, createdAt, startedAt, executeAfter, closedAt, command, args, output, errorOutput, exitCode, maxRuntime, maxRetries) SELECT DISTINCT  'pending', 'default', 0, CURRENT_DATE , null, DATE_ADD(CURRENT_DATE,INTERVAL 1 day), null, 'mpe:sendmail:purge', '', null,null, null, 0, 0 FROM jms_jobs as J where NOT EXISTS (SELECT 1 from  jms_jobs as jm where jm.command like 'mpe:sendmail:purge' );

-- MKH MPE-8522
-- TABLE TYPEPROCEDURE
-- Renommage des types de procédure côté entreprise
UPDATE TypeProcedure SET libelle_type_procedure = REPLACE(libelle_type_procedure, 'Procédure concurrentielle avec négociation', 'Procédure avec négociation') WHERE abbreviation LIKE '%PCN%';

-- Renommage des abréviations côté entreprise
UPDATE TypeProcedure SET abbreviation = REPLACE(abbreviation, 'PCN', 'PAN') WHERE abbreviation LIKE '%PCN%';

-- TABLE TYPE_PROCEDURE_ORGANISME
-- Renommage des types de procédure côté agent
UPDATE Type_Procedure_Organisme SET libelle_type_procedure = REPLACE(libelle_type_procedure, 'Procédure concurrentielle avec négociation', 'Procédure avec négociation') WHERE abbreviation LIKE '%PCN%';
UPDATE Type_Procedure_Organisme SET libelle_type_procedure = REPLACE(libelle_type_procedure, 'Proc?dure concurrentielle avec n?gociation', 'Procédure avec négociation') WHERE abbreviation LIKE '%PCN%';

UPDATE Type_Procedure_Organisme SET libelle_type_procedure = REPLACE(libelle_type_procedure, 'Procédure négociée avec mise en concurrence préalable', 'Procédure avec négociation') WHERE abbreviation LIKE '%PCN%';
UPDATE Type_Procedure_Organisme SET libelle_type_procedure = REPLACE(libelle_type_procedure, 'Proc?dure n?goci?e avec mise en concurrence pr?alable', 'Procédure avec négociation') WHERE abbreviation LIKE '%PCN%';

-- Renommage des abréviations côté agent
UPDATE Type_Procedure_Organisme SET abbreviation = REPLACE(abbreviation, 'PCN', 'PAN') WHERE abbreviation LIKE '%PCN%';


--Modification de la table d'association types de procédure
DELETE FROM type_procedure_mpe_pivot;
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (1, 1, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (2, 2, 3);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (3, 3, 1);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (4, 4, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (5, 5, 3);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (6, 7, 7);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (7, 8, 6);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (8, 9, 4);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (9, 10, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (10, 11, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (11, 15, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (12, 30, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (13, 31, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (14, 32, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (15, 33, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (16, 35, 2);
INSERT INTO type_procedure_mpe_pivot (id, id_type_procedure_mpe, id_type_procedure_pivot) VALUES (17, 1000, 4);

-- AME MPE-8948
-- Table t_contrat_titulaire
-- Reprise des contrats existants : le type de procédure pivot "Procédure négociée avec mise en concurrence préalable" (ID 5 dans la table type_procedure_pivot) est supprimé, au profit du type de procédure pivot "Procédure avec négociation" (ID 4 dans la table type_procedure_pivot)
UPDATE t_contrat_titulaire SET procedure_passation_pivot = '4' WHERE procedure_passation_pivot LIKE '5';

-- Table type_procedure_pivot
-- Suppression du type de procédure pivot "Procédure négociée avec mise en concurrence préalable"
DELETE FROM type_procedure_pivot WHERE id = 5;
