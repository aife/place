-- OKO MPE-3553
ALTER TABLE `T_trace_operations_inscrit_details` ADD COLUMN `debut_action_millisecond` BIGINT NULL DEFAULT NULL COMMENT 'Contient le nombre de millisecondes correspondant a la date de debut' AFTER `id_offre`;

-- LEZ MPE-3873
-- ALTER TABLE `Offres`	ADD COLUMN `date_demande_ouverture` DATETIME NULL DEFAULT NULL COMMENT 'Date de reception du statut en cours de dechiffrement/ouverture' AFTER `date_depot`;

-- MBL MPE-3087
ALTER TABLE `Type_Procedure_Organisme` ADD COLUMN `pn` ENUM('0','1') NOT NULL DEFAULT '0' comment 'tag de la procédure "Procédure avec négociation"' AFTER `accord_cadre`;
ALTER TABLE `t_type_contrat` ADD COLUMN `type_contrat_statistique` SMALLINT NOT NULL DEFAULT '0' comment '0:Marché public, 1:Marché subsequent, 2:Marché spécifique, 3:Accord cadre, 4:SAD' AFTER `abreviation_type_contrat`;


-- AME MPE-4236
ALTER TABLE `t_contrat_titulaire`	CHANGE COLUMN `num_EJ` `num_EJ` VARCHAR(100) NULL DEFAULT NULL;


--
-- Structure de la table `t_numero_reponse`
--

CREATE TABLE IF NOT EXISTS t_numero_reponse( id int(11) NOT NULL, consultation_ref int(11) NOT NULL, numero_reponse int(11) NOT NULL, organisme varchar(50) DEFAULT NULL, PRIMARY KEY(id)) ENGINE=InnoDB DEFAULT CHARSET=latin1;