-- FLO MPE-11055
INSERT INTO `mail_type_group` (`id`, `label`) VALUES
(1, 'MessecV2'),
(2, 'MPE');

INSERT INTO `mail_type` (`id`, `mail_type_group_id`, `label`, `code`) VALUES
(1, 1, 'Courrier libre', 'COURRIER_LIBRE'),
(2, 1, 'Invitation à concourir', 'INVITATION_CONCOURIR'),
(3, 1, 'Information de modification de la consultation', 'MODIFICATION_CONSULTATION'),
(4, 1, 'Réponse à une question', 'REPONSE_QUESTION'),
(5, 1, 'Demande de complément', 'DEMANDE_COMPLEMENT'),
(6, 1, 'Courrier d\'attribution', 'COURRIER_ATTRIBUTION'),
(7, 1, 'Courrier de notification', 'COURRIER_NOTIFICATION'),
(8, 1, 'Courrier de rejet', 'COURRIER_REJET'),
(9, 1, 'Information d\'annulation de la consultation', 'COURRIER_ANNULATION');

INSERT INTO `mail_template` (`id`, `mail_type_id`, `code`, `objet`, `corps`, `ordre_affichage`, `envoi_modalite`, `envoi_modalite_figee`, `reponse_attendue`, `reponse_attendue_figee`) VALUES
(1, 1, 'COURRIER_LIBRE', 'Courrier libre', '<p>Bonjour,</p>\n<p><strong>TEXTE COURRIER LIBRE</strong></p>\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.</p>\n<p>La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 1, 'AVEC_AR', 0, 0, 0),
(2, 2, 'INVITATION_CONCOURIR', 'Invitation à concourir', '<p>Bonjour,</p>\n<p>Vous &ecirc;tes invit&eacute; &agrave; concourir pour la consultation cit&eacute;e en r&eacute;f&eacute;rence.</p>\n<p>&nbsp;</p>', 2, 'AVEC_AR', 0, 0, 0),
(3, 3, 'MODIFICATION_CONSULTATION', 'Information de modification de la consultation', '<p>Bonjour,</p>\r\n<p>La consultation cit&eacute;e en r&eacute;f&eacute;rence a &eacute;t&eacute; modifi&eacute;e.</p>\r\n<p>Les &eacute;l&eacute;ments modifi&eacute;s sont : <strong>A PRECISER AU CAS PAR CAS</strong> Merci de votre int&eacute;r&ecirc;t pour cette consultation. <br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 3, 'AVEC_AR', 0, 0, 0),
(4, 4, 'REPONSE_QUESTION', 'Réponse à une question', '<p>Bonjour,</p>\r\n<p><strong>R&Eacute;PONSE &Agrave; LA QUESTION</strong></p>\r\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 4, 'AVEC_AR', 0, 0, 0),
(5, 5, 'DEMANDE_COMPLEMENT', 'Demande de complément', '<p>Bonjour,</p>\n<p>Nous vous remercions d\'avoir r&eacute;pondu &agrave; la consultation cit&eacute;e en r&eacute;f&eacute;rence.<br />Apr&egrave;s analyse, il vous est demand&eacute; d\'apporter les pr&eacute;cisions suivantes : <strong>A PRECISER AU CAS PAR CAS</strong></p>\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 5, 'AVEC_AR', 1, 1, 1),
(6, 6, 'COURRIER_ATTRIBUTION', 'Courrier d\'attribution', '<p>Bonjour,</p>\n<p>J&rsquo;ai l&rsquo;honneur de vous informer que vous avez &eacute;t&eacute; d&eacute;sign&eacute; attributaire de la consultation cit&eacute;e en r&eacute;f&eacute;rence.</p>\n<p>Je vous informe que le contrat ne sera valablement form&eacute; qu&rsquo;apr&egrave;s sa signature par un repr&eacute;sentant habilit&eacute;, et ne produira ses effets qu&rsquo;au terme de la notification dudit march&eacute;.</p>\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 6, 'AVEC_AR', 1, 0, 0),
(7, 7, 'COURRIER_NOTIFICATION', 'Courrier de notification', '<p>Bonjour,</p>\r\n<p>Votre entreprise a &eacute;t&eacute; d&eacute;clar&eacute;e attributaire de la consultation cit&eacute;e en r&eacute;f&eacute;rence.<br />Veuillez-trouver &agrave; titre de notification le march&eacute; sign&eacute; par un repr&eacute;sentant habilit&eacute;.<br />L\'Accus&eacute; de r&eacute;ception de ce message vaut notification officielle du march&eacute;.</p>\r\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 7, 'AVEC_AR', 1, 0, 0),
(8, 8, 'COURRIER_REJET', 'Courrier de rejet', '<p>Bonjour,</p>\r\n<p>Nous vous remercions d\'avoir r&eacute;pondu &agrave; la consultation cit&eacute;e en r&eacute;f&eacute;rence.<br />Nous sommes toutefois au regret de vous annoncer que votre r&eacute;ponse n\'a pas &eacute;t&eacute; retenue.</p>\r\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 8, 'AVEC_AR', 0, 0, 0),
(9, 9, 'COURRIER_ANNULATION', 'Information d\'annulation de la consultation', '<p>Bonjour,</p>\r\n<p>La consultation cit&eacute;e en r&eacute;f&eacute;rence a &eacute;t&eacute; annul&eacute;e.</p>\r\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 9, 'AVEC_AR', 0, 0, 0);

-- MPE-12400
UPDATE `configuration_plateforme` SET `messagerie_v2` = '1' WHERE `configuration_plateforme`.`id_auto` = 1;