-- SDR-MPE-7366
ALTER TABLE `InscritHistorique`	ENGINE=InnoDB;
ALTER TABLE `EtatConsultation`	ENGINE=InnoDB;
ALTER TABLE `Departement`	ENGINE=InnoDB;

-- AME MPE-8514
ALTER TABLE `Offres` ADD COLUMN `id_blob_horodatage_hash` INT NULL DEFAULT NULL COMMENT 'id blob fichier hash256 des fichiers de la reponse';