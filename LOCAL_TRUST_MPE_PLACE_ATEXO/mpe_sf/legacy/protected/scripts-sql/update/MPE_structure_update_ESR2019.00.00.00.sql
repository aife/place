ALTER TABLE `blob_file` ADD COLUMN `chemin` TEXT NULL DEFAULT NULL;
ALTER TABLE `blobOrganisme_file` ADD COLUMN `chemin` TEXT NULL DEFAULT NULL;

-- FMA-MPE-7465
CREATE TABLE IF NOT EXISTS `dossier_volumineux` ( `id` INT NOT NULL AUTO_INCREMENT , `uuid` VARCHAR(30) NOT NULL , `nom` VARCHAR(30) NOT NULL , `taille` BIGINT UNSIGNED NOT NULL , `date_creation` DATETIME NOT NULL , `statut` BOOLEAN NOT NULL , `id_agent` INT NULL , `id_inscrit` INT NULL , `id_blob` INT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `dossier_volumineux` ADD CONSTRAINT `id_agent_dv_FK` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`);
ALTER TABLE `dossier_volumineux` ADD CONSTRAINT `id_inscrit_FK` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`);
ALTER TABLE `dossier_volumineux` ADD CONSTRAINT `id_blob_FK` FOREIGN KEY (`id_blob`) REFERENCES `blob_file` (`id`);
ALTER TABLE `dossier_volumineux` ADD UNIQUE(`uuid`);
ALTER TABLE `dossier_volumineux` CHANGE `taille` `taille` BIGINT(20) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `dossier_volumineux` CHANGE `statut` `statut` TINYINT(1) NOT NULL DEFAULT '0';


ALTER TABLE `consultation` ADD COLUMN `id_dossier_volumineux` INT NULL;
ALTER TABLE `consultation` ADD CONSTRAINT `id_dossier_volumineux_FK` FOREIGN KEY (`id_dossier_volumineux`) REFERENCES `dossier_volumineux` (`id`);

ALTER TABLE `Enveloppe` ADD COLUMN `id_dossier_volumineux` INT NULL;
ALTER TABLE `Enveloppe` ADD CONSTRAINT `id_dv_FK` FOREIGN KEY (`id_dossier_volumineux`) REFERENCES `dossier_volumineux` (`id`);

ALTER TABLE `dossier_volumineux` ADD COLUMN `uuid_tus` VARCHAR(255) NOT NULL;
ALTER TABLE `dossier_volumineux` CHANGE `uuid_tus` `uuid_technique` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `dossier_volumineux` ADD UNIQUE(`uuid_technique`);
ALTER TABLE `dossier_volumineux` CHANGE `uuid` `uuid_reference` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `dossier_volumineux` DROP INDEX `uuid`, ADD UNIQUE `uuid_reference` (`uuid_reference`) USING BTREE;

ALTER TABLE `dossier_volumineux` DROP INDEX `id_agent_dv_FK`, ADD INDEX `dossier_volumineux_agent_FK` (`id_agent`) USING BTREE;
ALTER TABLE `dossier_volumineux` DROP INDEX `id_inscrit_FK`, ADD INDEX `dossier_volumineux_inscrit_FK` (`id_inscrit`) USING BTREE;
ALTER TABLE `dossier_volumineux` DROP INDEX `id_blob_FK`, ADD INDEX `dossier_volumineux_blob_FK` (`id_blob`) USING BTREE;

-- FMA-MPE-7575
ALTER TABLE `dossier_volumineux` CHANGE `statut` `statut` VARCHAR(30) NOT NULL DEFAULT 'init' COMMENT 'valauer possible : init, complet, incomplet';
ALTER TABLE `dossier_volumineux` ADD COLUMN `actif` BOOLEAN NULL DEFAULT FALSE;
ALTER TABLE `dossier_volumineux` ADD COLUMN `id_blob_logfile` INT NULL DEFAULT NULL;
ALTER TABLE `dossier_volumineux` ADD CONSTRAINT `dossier_volumineux_id_blob_logfile_FK` FOREIGN KEY (`id_blob_logfile`) REFERENCES `blob_file` (`id`);
ALTER TABLE `dossier_volumineux` DROP FOREIGN KEY `id_blob_FK`;
ALTER TABLE `dossier_volumineux` DROP INDEX `dossier_volumineux_blob_FK`;
ALTER TABLE `dossier_volumineux` CHANGE `id_blob` `id_blob_descripteur` INT(11) NULL DEFAULT NULL;
ALTER TABLE `dossier_volumineux` ADD CONSTRAINT `dossier_volumineux_id_blob_descripteur_FK` FOREIGN KEY (`id_blob_descripteur`) REFERENCES `blob_file` (`id`);

-- FLO-MPE-7436
ALTER TABLE `dossier_volumineux` CHANGE `nom` `nom` VARCHAR(30) NULL DEFAULT NULL;

-- MKH-MPE-7448
ALTER TABLE `Echange` ADD COLUMN `id_dossier_volumineux` INT NULL;

-- FMA-MPE-7575
ALTER TABLE `dossier_volumineux` ADD COLUMN `date_modification` DATETIME NULL DEFAULT NULL;

ALTER TABLE `AVIS` ADD COLUMN `chemin` TEXT NULL DEFAULT NULL;
