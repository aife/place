-- FMA MPE-10434
ALTER TABLE `Agent` ADD COLUMN `technique` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Définit si l utilisateur est un utilisateur technique (accès WS)';

-- FMA MPE-10435
ALTER TABLE `HabilitationAgent` ADD COLUMN `acces_ws` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'habilitation pour les accès au WS';

-- FMA MPE-10436
CREATE TABLE IF NOT EXISTS `web_service` (
    id INT AUTO_INCREMENT NOT NULL,
    route_ws VARCHAR(255) NOT NULL,
    method_ws VARCHAR(255) NOT NULL,
    nom_ws varchar(255) NOT NULL,
    PRIMARY KEY(id)
)
ENGINE = InnoDB;

-- FMA MPE-10437
CREATE TABLE IF NOT EXISTS `habilitation_agent_ws` (
    id INT AUTO_INCREMENT NOT NULL,
    agent_id int(11) NOT NULL,
    web_service_id int(11) NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT `habilitation_agent_ws_id_agent_fk` FOREIGN KEY (`agent_id`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `habilitation_agent_ws_id_web_service_fk` FOREIGN KEY (`web_service_id`) REFERENCES `web_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = InnoDB;

-- FMA MPE-10442
ALTER TABLE `web_service` ADD COLUMN `uniquement_technique` BOOLEAN NOT NULL DEFAULT TRUE COMMENT 'Définit si l accès du Ws n est qu accessible que par un agent technique';
CREATE TABLE IF NOT EXISTS `agent_technique_token` (
    id INT AUTO_INCREMENT NOT NULL,
    agent_id int(11) NOT NULL,
    token varchar(255) NOT NULL,
    create_at datetime NOT NULL,
    update_at datetime NOT NULL,
    date_expiration datetime NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT `agent_technique_token_agent_id_fk` FOREIGN KEY (`agent_id`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = InnoDB;

ALTER TABLE `web_service` ADD CONSTRAINT  web_service_route_ws_method_ws UNIQUE (route_ws, method_ws);

-- FMA MPE-10479
ALTER TABLE `Agent` CHANGE COLUMN `organisme` `organisme` VARCHAR(30) NULL DEFAULT NULL;