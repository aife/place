<?php
$sqlCustomRootDir = realpath(Atexo_Config::getParameter('DOCUMENT_ROOT_DIR').'/../config/scripts-sql');
$listSqlFiles = glob($sqlCustomRootDir.'/*.sql');
foreach($listSqlFiles as $file){
    echo '-- start file '.$file.PHP_EOL;
    echo file_get_contents($file).PHP_EOL;
    echo '-- end file '.$file.PHP_EOL;
}
$env = Atexo_Config::getParameter('PF_ENV');
if(!empty($env) && in_array($env,array('prod','preprod','rec','dev','demo','ecole','int'))){
    $listSqlEnvFiles = glob($sqlCustomRootDir.'/'.$env.'/*.sql');
    foreach($listSqlEnvFiles as $file){
        echo '-- start file '.$file.PHP_EOL;
        echo file_get_contents($file).PHP_EOL;
        echo '-- end file '.$file.PHP_EOL;
    }
}
