-- AME MPE-9014
ALTER TABLE `configuration_plateforme` DROP COLUMN `num_donnees_essentielles_manuel`;
ALTER TABLE `configuration_organisme` ADD COLUMN `num_donnees_essentielles_manuel` ENUM('0','1') NULL DEFAULT '0' COMMENT 'donne la  possibilite pour un acheteur de saisir manuellement le numéro d''identification, via le champ "Numéro de contrat" (colonne reference_libre de la table t_contrat_titulaire) de la popup PopupNumerotationContrat';
