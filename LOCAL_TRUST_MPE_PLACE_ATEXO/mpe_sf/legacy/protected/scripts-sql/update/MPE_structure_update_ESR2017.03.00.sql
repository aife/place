CREATE TABLE `t_message_accueil` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`type_message` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'le style du message, prend les valeursinfo, avertissement, erreur',
	`contenu` TEXT NOT NULL,
	`destinataire` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'entreprise , agent ',
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;

-- LEZ-4196
ALTER TABLE `t_document_type`	DROP COLUMN `document_entreprise`;

-- LEZ MPE-4085
ALTER TABLE `HabilitationAgent` ADD COLUMN `gerer_gabarit_editeur` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer les gabarits de clausier éditeur';
ALTER TABLE `HabilitationAgent` ADD COLUMN `gerer_gabarit` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer les gabarits de clausier client';
ALTER TABLE `HabilitationAgent` ADD COLUMN `gerer_gabarit_entite_achats` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer les gabarits de clausier entité d''achat';
ALTER TABLE `HabilitationAgent` ADD COLUMN `gerer_gabarit_agent` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer les gabarits de clausier agent';

ALTER TABLE `HabilitationProfil` ADD COLUMN `gerer_gabarit_editeur` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer les gabarits de clausier éditeur';
ALTER TABLE `HabilitationProfil` ADD COLUMN `gerer_gabarit` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer les gabarits de clausier client';
ALTER TABLE `HabilitationProfil` ADD COLUMN `gerer_gabarit_entite_achats` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer les gabarits de clausier entité d''achat';
ALTER TABLE `HabilitationProfil` ADD COLUMN `gerer_gabarit_agent` ENUM('0','1') NULL DEFAULT '0' COMMENT 'Permet d''administrer les gabarits de clausier agent';