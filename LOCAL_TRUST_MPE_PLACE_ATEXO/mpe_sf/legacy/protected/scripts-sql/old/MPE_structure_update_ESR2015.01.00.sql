-- LEZ
CREATE TABLE IF NOT EXISTS `t_contrat_multi` (
  `id_contrat_multi` INT(11) NOT NULL AUTO_INCREMENT,
  `numero_contrat_multi` VARCHAR(255) NULL,
  `id_type_contrat_multi` INT(11) NULL,
  `organisme` VARCHAR(45) NULL,
  `service_id` INT(11) NULL,
  `objet_contrat_multi` VARCHAR(255) NULL,
  `montant_max_estime` DOUBLE NULL,
  `id_tranche` INT(11) NULL,
  `numero_long` VARCHAR(255) NULL,
  `reference_libre` VARCHAR(255) NULL,
  `categorie` INT(11) NULL,
  `ccag_applicable` INT(11) NULL,
  `clause_sociale` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_condition_execution` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_insertion` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_ateliers_proteges` VARCHAR(10) NULL DEFAULT 0,
  `clause_environnementale` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_specs_techniques` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_cond_execution` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_criteres_select` VARCHAR(10) NULL DEFAULT 0,
  PRIMARY KEY (`id_contrat_multi`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `t_contact_contrat` (
  `id_contact_contrat` INT(11) NOT NULL AUTO_INCREMENT,
  `id_entreprise` INT(11) NULL,
  `id_etablissement` INT(11) NULL,
  `id_inscrit` INT(11) NULL,
  `nom` VARCHAR(80) NULL,
  `prenom` VARCHAR(80) NULL,
  `email` VARCHAR(100) NULL,
  `telephone` VARCHAR(20) NULL,
  `fax` VARCHAR(30) NULL,
  PRIMARY KEY (`id_contact_contrat`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `t_contrat_titulaire` (
  `id_contrat_titulaire` INT(11) NOT NULL AUTO_INCREMENT,
  `id_type_contrat` INT(11) NULL DEFAULT NULL,
  `id_contact_contrat` INT(11) NULL,
  `numero_contrat` VARCHAR(255) NULL,
  `lien_AC_SAD` INT(11) NULL,
  `id_contrat_multi` INT(11) NULL,
  `organisme` VARCHAR(45) NULL,
  `service_id` INT(11) NULL,
  `id_titulaire` INT(11) NULL,
  `id_titulaire_etab` INT(11) NULL,
  `objet_contrat` VARCHAR(255) NULL,
  `montant_contrat` DOUBLE NULL,
  `id_tranche_budgetaire` INT(11) NULL,
  `montant_max_estime` DOUBLE NULL,
  `publication_montant` TINYINT NULL,
  `id_motif_non_publication_montant` INT(11) NULL,
  `desc_motif_non_publication_montant` VARCHAR(255) NULL,
  `publication_contrat` TINYINT NULL,
  `num_EJ` VARCHAR(45) NULL,
  `num_long_OEAP` VARCHAR(45) NULL,
  `reference_libre` VARCHAR(45) NULL,
  `statut_contrat` INT(11) NULL,
  `categorie` INT(11) NULL,
  `ccag_applicable` INT(11) NULL,
  `clause_sociale` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_condition_execution` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_insertion` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_ateliers_proteges` VARCHAR(10) NULL DEFAULT 0,
  `clause_environnementale` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_specs_techniques` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_cond_execution` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_criteres_select` VARCHAR(10) NULL DEFAULT 0,
  `date_prevue_notification` DATE NULL,
  `date_prevue_fin_contrat` DATE NULL,
  `date_prevue_max_fin_contrat` DATE NULL,
  `date_notification` DATE NULL,
  `date_fin_contrat` DATE NULL,
  `date_max_fin_contrat` DATE NULL,
  PRIMARY KEY (`id_contrat_titulaire`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

ALTER TABLE `t_contrat_titulaire` ADD INDEX `fk_Contrat_Titulaire_Contrat_Multi1_idx` (`id_contrat_multi`) ;
ALTER TABLE `t_contrat_titulaire` ADD CONSTRAINT `fk_Contrat_Titulaire_Contrat_Multi1` FOREIGN KEY ( `id_contrat_multi` ) REFERENCES `t_contrat_multi` (`id_contrat_multi`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `t_contrat_titulaire` ADD CONSTRAINT `fk_Contrat_Titulaire_Contact_Contrat`  FOREIGN KEY (`id_contact_contrat`) REFERENCES `t_contact_contrat` (`id_contact_contrat`) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `t_contrat_titulaire` CHANGE `statut_contrat` `statut_contrat` INT( 11 ) NULL DEFAULT 0 COMMENT 'permet de definir le statut du contrat : 0=>etape Type de decision,1=>etape Donnees du contrat - A saisir,2=>Numerotion autonome,3=>etape Notification,4=>etape Notification effectuee';

CREATE TABLE IF NOT EXISTS `t_cons_lot_contrat` (
  `id_cons_lot_contrat` INT(11) NOT NULL AUTO_INCREMENT,
  `organisme` VARCHAR(45) NULL,
  `consultation_ref` INT(11) NULL,
  `lot` INT(11) NULL,
  `id_contrat_titulaire` INT(11) NOT NULL,
  PRIMARY KEY (`id_cons_lot_contrat`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

ALTER TABLE `t_cons_lot_contrat` ADD INDEX `fk_Lot_Contrat_Contrat_Titulaire_idx` (`id_contrat_titulaire`) ;
ALTER TABLE `t_cons_lot_contrat` ADD INDEX `fk_t_cons_lot_contrat_lot1_idx` (`lot`) ;
ALTER TABLE `t_cons_lot_contrat` ADD CONSTRAINT `fk_Lot_Contrat_Contrat_Titulaire` FOREIGN KEY ( `id_contrat_titulaire` ) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Entreprise` ADD COLUMN `created_from_decision` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de definir si l''entreprise est creee depuis la decision ou non' AFTER `saisie_manuelle`;

ALTER TABLE `t_contrat_titulaire` ADD COLUMN `id_offre` INT( 11 ) default NULL AFTER `id_titulaire_etab` ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `type_depot_reponse` CHAR( 1 ) default NULL AFTER `id_offre` ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `date_attribution` DATE default NULL AFTER `date_max_fin_contrat`;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `date_creation` DATETIME default NULL AFTER `date_attribution`;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `date_modification` DATETIME default NULL AFTER `date_creation`;

ALTER TABLE `t_contrat_multi` ADD COLUMN `date_creation` DATETIME default NULL AFTER `clause_env_criteres_select`;
ALTER TABLE `t_contrat_multi` ADD COLUMN `date_modification` DATETIME default NULL AFTER `date_creation`;

CREATE TABLE IF NOT EXISTS `t_type_contrat` (
  `id_type_contrat` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_type_contrat` varchar(255) NOT NULL,
  `multi` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'ce champ est egale a un si ce type accepte contrat multi',
  PRIMARY KEY (`id_type_contrat`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `t_numerotation_automatique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organisme` varchar(45) NOT NULL,
  `service_id` INT(11) NULL,
  `annee` varchar(10) NULL,
  `contrat_AC_multi` varchar(20) NULL,
  `contrat_SA_dynamique` varchar(20) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_numerotation_automatique` ADD COLUMN `contrat_titulaire` VARCHAR( 20 ) NULL AFTER `contrat_SA_dynamique`;

ALTER TABLE `t_contrat_titulaire` ADD COLUMN `statutEJ` TEXT NULL AFTER `num_EJ`;

CREATE TABLE IF NOT EXISTS `t_decision_selection_entreprise` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `consultation_ref` INT(11) NULL,
  `organisme` VARCHAR(45) NULL,
  `service_id` INT(11) NULL,
  `lot` INT(11) NULL,
  `id_entreprise` INT(11) NULL,
  `id_etablissement` INT(11) NULL,
  `id_offre` INT(11) NULL,
  `type_depot_reponse` CHAR( 1 ) NULL,
  `date_creation` DATETIME default NULL,
  `date_modification` DATETIME default NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

ALTER TABLE `t_decision_selection_entreprise` ADD COLUMN `id_contact_contrat` INT(11) NULL AFTER `date_modification`;
ALTER TABLE `t_decision_selection_entreprise` ADD CONSTRAINT `fk_Decision_Selection_Entreprise_Contact_Contrat`  FOREIGN KEY (`id_contact_contrat`) REFERENCES `t_contact_contrat` (`id_contact_contrat`) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `Marche` ADD COLUMN `id_contrat_titulaire` INT( 11 ) NOT NULL AFTER `id_decision_enveloppe` ;
ALTER TABLE `Marche` ADD CONSTRAINT `fk_Marche_contrat_titulaire`  FOREIGN KEY (`id_contrat_titulaire`) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `Offre_papier` ADD COLUMN `entreprise_id` INT( 11 ) NULL  AFTER `consultation_ref` ;
ALTER TABLE `Offre_papier` ADD COLUMN `id_etablissement` INT( 11 )  NULL AFTER `entreprise_id` ;

ALTER TABLE `t_contrat_titulaire` ADD COLUMN `envoi_interface` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de specifier si la decision a ete exportee vers une interface';

ALTER TABLE `t_contrat_titulaire` ADD COLUMN `contrat_class_key` INT( 1 ) NOT NULL DEFAULT '1' COMMENT 'utiliser pour utiliser l''heritage de propel la valeur 1 => l''objet contrat titulaire,2=> l''objet contrat multi' ;

ALTER TABLE `t_contrat_titulaire` DROP FOREIGN KEY `fk_Contrat_Titulaire_Contrat_Multi1` ;
ALTER TABLE `t_contrat_titulaire` ADD CONSTRAINT `fk_Contrat_Titulaire_Contrat_Multi1` FOREIGN KEY ( `id_contrat_multi` ) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON DELETE CASCADE ON UPDATE CASCADE ;

DROP TABLE IF EXISTS `t_contrat_multi`;

ALTER TABLE `t_contrat_titulaire` ADD COLUMN `envoi_interface` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de specifier si la decision a ete exportee vers une interface';

-- LEZ
ALTER TABLE  `Chorus_echange` DROP FOREIGN KEY  `ChorusEchange_decisionEnveloppe` ;

-- OKO
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale` `clause_sociale` VARCHAR( 10 ) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale_condition_execution` `clause_sociale_condition_execution` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale_insertion` `clause_sociale_insertion` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale_ateliers_proteges` `clause_sociale_ateliers_proteges` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_environnementale` `clause_environnementale` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_env_specs_techniques` `clause_env_specs_techniques` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_env_cond_execution` `clause_env_cond_execution` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_env_criteres_select` `clause_env_criteres_select` VARCHAR(10) NULL DEFAULT NULL;

-- ASO
CREATE TABLE IF NOT EXISTS `t_bourse_cotraitance` (
	`id_auto_BC` INT NOT NULL AUTO_INCREMENT,
	`reference_consultation` INT(11) NOT NULL,
	`id_entreprise` INT(11) NOT NULL,
	`nom_inscrit` VARCHAR(100) NOT NULL,
	`prenom_inscrit` VARCHAR(100) NOT NULL,
	`adresse_inscrit` VARCHAR(100) NOT NULL,
	`adresse2_incsrit` VARCHAR(100) NULL DEFAULT NULL,
	`cp_inscrit` VARCHAR(20) NOT NULL,
	`ville_inscrit` VARCHAR(100) NOT NULL,
	`pays_inscrit` VARCHAR(250) NULL DEFAULT NULL,
	`fonction_inscrit` VARCHAR(100) NOT NULL,
	`email_inscrit` VARCHAR(100) NOT NULL,
	`tel_fixe_inscrit` VARCHAR(100) NULL DEFAULT NULL,
	`tel_mobile_inscrit` VARCHAR(100) NULL DEFAULT NULL,
	`mandataire_groupement` ENUM('0','1') NOT NULL DEFAULT '0',
	`cotraitant_solidaire` ENUM('0','1') NOT NULL DEFAULT '0',
	`cotraitant_conjoint` ENUM('0','1') NOT NULL DEFAULT '0',
	`desc_mon_apport_marche` VARCHAR(360) NULL DEFAULT NULL,
	`desc_type_cotraitance_recherche` VARCHAR(360) NULL DEFAULT NULL,
	PRIMARY KEY (`id_auto_BC`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

ALTER TABLE `t_bourse_cotraitance` ADD COLUMN `clause_social` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `desc_type_cotraitance_recherche`;
ALTER TABLE `t_bourse_cotraitance` ADD COLUMN `entreprise_adapte` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `clause_social`;

ALTER TABLE `configuration_plateforme` ADD COLUMN `bourse_cotraitance` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer la bourse a la cotraitance' AFTER `autoriser_creation_entreprise_etrangere`;

-- oko : mise a jour de la table etablissement
ALTER TABLE `t_etablissement` ADD COLUMN `long` DOUBLE NULL DEFAULT NULL COMMENT 'Longitude' AFTER `inscrit_annuaire_defense` ;
ALTER TABLE `t_etablissement` ADD COLUMN `lat` DOUBLE NULL DEFAULT NULL COMMENT 'Latitude' AFTER `long` ;
ALTER TABLE `t_etablissement` ADD COLUMN `maj_long_lat` DATETIME NULL DEFAULT NULL COMMENT 'Date de mise a jour de la latitude et longitude' AFTER `lat` ;

-- ASO
ALTER TABLE `t_bourse_cotraitance` ADD COLUMN `id_etablissement_inscrite` INT(11) NOT NULL AFTER `id_entreprise`;

ALTER TABLE `t_bourse_cotraitance` CHANGE COLUMN `desc_mon_apport_marche` `desc_mon_apport_marche` TEXT NULL DEFAULT NULL AFTER `cotraitant_conjoint`;
ALTER TABLE `t_bourse_cotraitance` CHANGE COLUMN `desc_type_cotraitance_recherche` `desc_type_cotraitance_recherche` TEXT NULL DEFAULT NULL AFTER `desc_mon_apport_marche`;

-- ASO
ALTER TABLE `t_bourse_cotraitance` ADD COLUMN `long` DOUBLE NULL DEFAULT NULL COMMENT 'Longitude' AFTER `entreprise_adapte` ;
ALTER TABLE `t_bourse_cotraitance` ADD COLUMN `lat` DOUBLE NULL DEFAULT NULL COMMENT 'Latitude' AFTER `long` ;
ALTER TABLE `t_bourse_cotraitance` ADD COLUMN `maj_long_lat` DATETIME NULL DEFAULT NULL COMMENT 'Date de mise a jour de la latitude et longitude' AFTER `lat` ;

-- OKO
ALTER TABLE `HabilitationAgent` ADD COLUMN `creer_contrat` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Autorise la creation d''un contrat';
ALTER TABLE `HabilitationProfil` ADD COLUMN `creer_contrat` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Autorise la creation d''un contrat';

-- OKO
ALTER TABLE `t_type_contrat` ADD COLUMN `abreviation_type_contrat` CHAR( 2 ) NULL DEFAULT NULL COMMENT 'Abreviation des types de contrats' AFTER `libelle_type_contrat` ;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale` `clause_sociale` VARCHAR( 10 ) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale_condition_execution` `clause_sociale_condition_execution` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale_insertion` `clause_sociale_insertion` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale_ateliers_proteges` `clause_sociale_ateliers_proteges` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_environnementale` `clause_environnementale` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_env_specs_techniques` `clause_env_specs_techniques` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_env_cond_execution` `clause_env_cond_execution` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_env_criteres_select` `clause_env_criteres_select` VARCHAR(10) NULL DEFAULT NULL;

-- OKO
ALTER TABLE `t_numerotation_automatique` CHANGE `contrat_AC_multi` `contrat_multi` VARCHAR( 20 ) NULL DEFAULT NULL ;

-- LEZ
ALTER TABLE `t_type_contrat` ADD COLUMN `accord_cadre_sad` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `multi`;

ALTER TABLE `t_type_contrat` ADD COLUMN `avec_chapeau` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `accord_cadre_sad`;
ALTER TABLE `t_type_contrat` ADD COLUMN `avec_montant` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `avec_chapeau`;
ALTER TABLE `t_type_contrat` ADD COLUMN `mode_echange_chorus` CHAR(1) NOT NULL DEFAULT '0' AFTER `avec_montant`;

ALTER TABLE `t_contrat_titulaire` ADD COLUMN `pme_pmi` int(11) NOT NULL DEFAULT '0' ;

ALTER TABLE `t_type_contrat` ADD COLUMN `marche_subsequent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `mode_echange_chorus`;

ALTER TABLE `t_contrat_titulaire` ADD CONSTRAINT `fk_Contrat_Titulaire_Type_Contrat`  FOREIGN KEY (`id_type_contrat`) REFERENCES `t_type_contrat` (`id_type_contrat`) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `consultation` ADD COLUMN `numero_AC` VARCHAR(255) default NULL AFTER `DATE_FIN_UNIX`;

-- OKO
ALTER TABLE `Service` ADD COLUMN `acces_chorus` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module Chorus pour le service';

-- LEZ
ALTER TABLE `t_type_contrat` ADD COLUMN `avec_montant_max` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'permet d''afficher ou non le montant max dans le chapeau' AFTER `marche_subsequent`;

ALTER TABLE `t_type_contrat` ADD COLUMN `ordre_affichage` INT( 11 ) NULL AFTER `avec_montant_max`;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `pme_pmi` int(11) NOT NULL DEFAULT '0' ;

-- OKO
ALTER TABLE `Chorus_echange` ADD COLUMN `type_contrat` INT( 10 ) NULL DEFAULT NULL COMMENT 'Type de contrat';
ALTER TABLE `Chorus_echange` ADD COLUMN `intitule_contrat` VARCHAR( 255 ) NULL DEFAULT NULL COMMENT 'Intitulé du contrat';
ALTER TABLE `Chorus_echange` ADD COLUMN `objet_contrat` VARCHAR( 255 ) NULL DEFAULT NULL COMMENT 'Objet du contrat';
ALTER TABLE `Chorus_echange` ADD COLUMN `identifiant_accord_cadre_chapeau` INT( 10 ) NULL DEFAULT NULL COMMENT 'Identifiant de l''accord cadre chapeau';

-- OKO
ALTER TABLE `t_type_contrat` CHANGE `accord_cadre_sad` `accord_cadre_sad` ENUM( '0', '1', '2' ) NOT NULL DEFAULT '0'COMMENT '0: ni accord-cadre ni sad, 1: accord-cadre, 2: sad';
ALTER TABLE `Chorus_echange` CHANGE `identifiant_accord_cadre_chapeau` `identifiant_accord_cadre_chapeau` VARCHAR( 255 ) NULL DEFAULT NULL COMMENT 'Identifiant de l''accord cadre chapeau';

-- OKO
ALTER TABLE `chorus_noms_fichiers` ADD COLUMN `numero_ordre` INT( 10 ) NOT NULL COMMENT 'Numéro d''ordre pour le cas des FSO' AFTER `id_echange`;

-- AME

CREATE TABLE IF NOT EXISTS `t_referentiel_nace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_nace_5` varchar(255) NOT NULL,
  `libelle_activite_detaillee` text NOT NULL,
  `code_nace_2` varchar(255) NOT NULL,
  `libelle_activite_general` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_nace_5` (`code_nace_5`,`code_nace_2`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `t_contrat_titulaire_favori` (
  `id_contrat_titulaire_favori` int(11) NOT NULL AUTO_INCREMENT,
  `id_contrat_titulaire` int(11) NOT NULL,
  `organisme` varchar(45) NOT NULL,
  `id_agent` int(11) NOT NULL,
  PRIMARY KEY (`id_contrat_titulaire_favori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- LEZ
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `reference_consultation` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `pme_pmi` ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `hors_passation` INT( 1 ) NOT NULL DEFAULT '0' COMMENT 'Permet de savoir si le contrat est crée hors passation ( décision) ou non,0 => crée de puis passation,1 => hors passation ' AFTER `reference_consultation` ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `id_agent` INT( 11 ) NULL DEFAULT NULL AFTER `hors_passation` ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `nom_agent` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `id_agent` ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `prenom_agent` VARCHAR( 255 ) NULL AFTER `nom_agent` ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `lieu_execution` TEXT NULL AFTER `prenom_agent` ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `code_cpv_1` VARCHAR( 8 ) NULL AFTER `lieu_execution` ;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `code_cpv_2` VARCHAR( 255 ) NULL AFTER `code_cpv_1`;

-- OKO
ALTER TABLE `Chorus_echange` ADD COLUMN `type_flux` INT( 2 ) NULL DEFAULT NULL COMMENT 'Types de flux Chorus (1: FEN111, 2:FEN211)';
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `code_cpv_2` VARCHAR( 255 ) NULL AFTER `code_cpv_1`;

-- LEZ
ALTER TABLE `consultation` ADD COLUMN `id_contrat` INT( 11 ) NULL DEFAULT NULL AFTER `numero_AC`;
ALTER TABLE `t_type_contrat` CHANGE `marche_subsequent` `marche_subsequent` CHAR( 1 )  NOT NULL DEFAULT '0' COMMENT 'permet de définir le type de marché : 0=>pas de spécification sur le type, 1=>marché subséquent,2=>marché spécifique';
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `pme_pmi` int(11) NOT NULL DEFAULT '0' ;

-- OKO : modification de la table 'service'
ALTER TABLE `Service` ADD COLUMN `acces_chorus` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module Chorus pour le service';

-- LEZ
CREATE TABLE IF NOT EXISTS `t_type_contrat_et_procedure` (
  `id_type_contrat_et_procedure` int(11) NOT NULL AUTO_INCREMENT,
  `id_type_contrat` int(11) NOT NULL,
  `id_type_procedure` int(11) NOT NULL,
  `organisme` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_type_contrat_et_procedure`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

-- LEZ
ALTER TABLE `Type_Procedure_Organisme` ADD COLUMN `ordre_affichage` INT( 11 ) NULL DEFAULT '0' COMMENT 'Permet de définir l''ordre d''affichage du type de procédure par ordre décroissant ';

-- LEZ
ALTER TABLE `configuration_plateforme` ADD COLUMN `ac_sad_transversaux` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la gestion des entités éligibles' ;

-- AME
ALTER TABLE `configuration_plateforme` ADD COLUMN `publicite` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'permet d''activer la publicité via les formulaires SUB et le concentrateur' ;


--
-- Structure de la table `t_annonce_consultation`
--

CREATE TABLE IF NOT EXISTS `t_annonce_consultation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_consultation` int(11) NOT NULL,
  `organisme` varchar(30) NOT NULL,
  `id_dossier_sub` int(11) NOT NULL,
  `id_dispositif` int(11) NOT NULL,
  `statut` varchar(255) NOT NULL,
  `id_support` int(11) NOT NULL COMMENT 'Identifiant du support de publicité',
  `lien_publication` varchar(255) DEFAULT NULL COMMENT 'Lien de publication dans le support',
  `numero_avis` varchar(255) DEFAULT NULL COMMENT 'Numero de la publication dans le support',
  `id_agent` int(11) NOT NULL COMMENT 'Identifiant de l''agent createur de la publicite',
  `prenom_nom_agent` varchar(255) NOT NULL COMMENT 'Prénom nom de l''agent createur de la publicite',
  `date_creation` datetime NOT NULL COMMENT 'Date de creation de la publicite',
  `date_modification` datetime NOT NULL COMMENT 'Date de modification de la publicite',
  `date_statut` datetime NOT NULL COMMENT 'Date de mise a jour du statut',
  `motif_statut` varchar(255) DEFAULT NULL COMMENT 'Motif justifiant le statut',
  `date_envoi_support` datetime NOT NULL COMMENT 'Date d''envoi de la publicité au support',
  PRIMARY KEY (`id`),
  KEY `ref_consultation` (`ref_consultation`),
  KEY `organisme` (`organisme`),
  KEY `id_dossier_sub` (`id_dossier_sub`),
  KEY `id_dispositif` (`id_dispositif`),
  KEY `statut` (`statut`),
  KEY `id_support` (`id_support`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

--
-- Structure de la table `t_support_publication`
--

CREATE TABLE IF NOT EXISTS `t_support_publication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_logo` varchar(255) DEFAULT NULL COMMENT 'nom de l''image du logo dans le dossier thèmes',
  `nom` varchar(255) NOT NULL,
  `visible` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'affichage coté acheteur',
  `ordre` int(11) DEFAULT NULL COMMENT 'permet de personnalisé l''ordre d''affichage des support',
  `default_value` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'parametrer la valeau par defaut la première fois l''agent accède au formulaire de choix des préferences',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

--
-- Structure de la table `t_preference_support_publication`
--

CREATE TABLE IF NOT EXISTS `t_preference_support_publication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_support` int(11) NOT NULL,
  `id_agent` int(11) NOT NULL,
  `organisme` varchar(30) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

--
-- Structure de la table `t_dispositif_annonce`
--

CREATE TABLE IF NOT EXISTS `t_dispositif_annonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  `id_externe` int(11) NOT NULL,
  `visible` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'Gère la visibilité du dispositif (0=> non visible, 1=> visible)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

--
-- Structure de la table `t_historique_annonce`
--

CREATE TABLE IF NOT EXISTS `t_historique_annonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_annonce` int(11) NOT NULL COMMENT 'Identifiant de l''annonce de publicité',
  `statut` varchar(255) NOT NULL COMMENT 'Statut de l''annonce de publicité',
  `motif` varchar(255) DEFAULT NULL COMMENT 'Motif du statut de l''annonce de publicité',
  `date_modification` datetime NOT NULL COMMENT 'Date de mise a jour',
  PRIMARY KEY (`id`),
  KEY `id_annonce` (`id_annonce`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_historique_annonce` ADD CONSTRAINT `t_historique_annonce_id` FOREIGN KEY (`id_annonce`) REFERENCES `t_annonce_consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Structure de la table `t_support_annonce_consultation`
--

CREATE TABLE IF NOT EXISTS `t_support_annonce_consultation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_support` int(11) NOT NULL,
  `id_annonce_cons` int(11) NOT NULL,
  `prenom_nom_agent_createur` varchar(255) NOT NULL,
  `id_agent` int(11) NOT NULL,
  `date_creation` datetime NOT NULL,
  `statut` varchar(255) NOT NULL,
  `date_statut` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_support_annonce_consultation` ADD INDEX ( `id_support` ) ;
ALTER TABLE `t_support_annonce_consultation` ADD INDEX ( `id_annonce_cons` ) ;
ALTER TABLE `t_support_annonce_consultation` ADD CONSTRAINT `fk_tSupportAnnonceCons_reference_tSupportPublication` FOREIGN KEY ( `id_support` ) REFERENCES `t_support_publication` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `t_support_annonce_consultation` ADD CONSTRAINT `fk_tSupportAnnonceCons_reference_tAnnonceConsultation` FOREIGN KEY ( `id_annonce_cons` ) REFERENCES `t_annonce_consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `t_historique_annonce` ADD COLUMN `id_support` INT NOT NULL DEFAULT '0';

-- OKO : modification de la structure de la table "t_annonce_consultation"
ALTER TABLE `t_annonce_consultation` ADD COLUMN `id_type_annonce` INT( 11 ) NOT NULL COMMENT 'Identifiant du type d''annonce' AFTER `id_support` ;
ALTER TABLE `t_annonce_consultation` ADD CONSTRAINT `fk_tAnnonceConsultation_reference_tDispositifAnnonce` FOREIGN KEY ( `id_dispositif` ) REFERENCES `t_dispositif_annonce` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `t_annonce_consultation`ADD CONSTRAINT `fk_tAnnonceConsultation_reference_tTypeAnnonceConsultation` FOREIGN KEY ( `id_type_annonce` ) REFERENCES `t_type_annonce_consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;

--
-- Structure de la table `t_type_annonce_consultation`
--

CREATE TABLE IF NOT EXISTS `t_type_annonce_consultation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_support_publication` ADD COLUMN `code` VARCHAR( 255 ) NULL COMMENT 'Code donné au support pour faciliter sa recuperation. Ce code est ajouté pour des fins purement techniques';
ALTER TABLE `t_annonce_consultation` ADD COLUMN `id_compte_boamp` INT( 11 ) NOT NULL COMMENT 'Identifiant du compte BOAMP associé' AFTER `id_dispositif` ;

ALTER TABLE `t_support_annonce_consultation` ADD COLUMN `numero_avis` INT( 11 ) NULL COMMENT 'Numero de l''avis envoyé par le concentrateur';
ALTER TABLE `t_support_annonce_consultation` ADD COLUMN `message_statut` VARCHAR( 255 ) NULL COMMENT 'Message correspondant au statut';
ALTER TABLE `t_annonce_consultation` ADD COLUMN `id_compte_boamp` INT( 11 ) NOT NULL COMMENT 'Identifiant du compte BOAMP associé' AFTER `id_dispositif` ;

ALTER TABLE `TypeProcedure` ADD COLUMN `value_binding_sub` VARCHAR( 255 ) NOT NULL DEFAULT '' COMMENT 'ce champs est utilisé afin de faire la correspondance entre valeur MPE et SUB';


UPDATE `TypeProcedure` SET `value_binding_sub`='01_appelOuvert' WHERE  `id_type_procedure`=1;
UPDATE `TypeProcedure` SET `value_binding_sub`='02_appelRestreint' WHERE  `id_type_procedure`=2;
UPDATE `TypeProcedure` SET `value_binding_sub`='06_dialogueCompetitif' WHERE  `id_type_procedure`=7;
UPDATE `TypeProcedure` SET `value_binding_sub`='04_marcheNegocie' WHERE  `id_type_procedure`=8;
UPDATE `TypeProcedure` SET `value_binding_sub`='07_procedureAdaptee' WHERE  `id_type_procedure`=15;
UPDATE `TypeProcedure` SET `value_binding_sub`='08_concoursOuvert' WHERE  `id_type_procedure`=4;
UPDATE `TypeProcedure` SET `value_binding_sub`='07_procedureAdaptee' WHERE  `id_type_procedure`=3;
UPDATE `TypeProcedure` SET `value_binding_sub`='09_concoursRestreint' WHERE  `id_type_procedure`=5;
UPDATE `TypeProcedure` SET `value_binding_sub`='10_autre' WHERE  `id_type_procedure`=15;
UPDATE `TypeProcedure` SET `value_binding_sub`='10_autre' WHERE  `id_type_procedure`=30;
UPDATE `TypeProcedure` SET `value_binding_sub`='10_autre' WHERE  `id_type_procedure`=31;
UPDATE `TypeProcedure` SET `value_binding_sub`='10_autre' WHERE  `id_type_procedure`=32;
UPDATE `TypeProcedure` SET `value_binding_sub`='10_autre' WHERE  `id_type_procedure`=33;

ALTER TABLE `t_annonce_consultation` DROP `id_support`;
ALTER TABLE `t_annonce_consultation` DROP `lien_publication`;
ALTER TABLE `t_annonce_consultation` DROP `numero_avis`;
ALTER TABLE `t_annonce_consultation` DROP `date_envoi_support`;

ALTER TABLE `t_support_annonce_consultation` ADD COLUMN `lien_publication` TEXT NULL COMMENT 'lien de publication de l''annonce';


UPDATE `TypeProcedure` SET `value_binding_sub`='10_autre' WHERE  `id_type_procedure`=33;

-- OKO
ALTER TABLE `t_dispositif_annonce` ADD COLUMN `type` INT( 11 ) NULL COMMENT 'Le type de dispositif ' AFTER `id_externe` ;

--
-- Structure de la table `t_parametrage_formulaire_publicite`
--

CREATE TABLE IF NOT EXISTS `t_parametrage_formulaire_publicite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_support` int(11) NOT NULL,
  `id_type_annonce` int(11) NOT NULL,
  `ids_formulaire` varchar(255) DEFAULT NULL COMMENT 'Les ids des formulaires doivent etre separes par des virgules',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- OKO : modification de la table  `CategorieINSEE`
ALTER TABLE `CategorieINSEE` ADD COLUMN `code` VARCHAR( 10 ) NULL COMMENT 'Code de la catégorie INSEE correspondant';

-- AME
ALTER TABLE `t_support_annonce_consultation` CHANGE `numero_avis` `numero_avis` VARCHAR( 255 ) NULL DEFAULT NULL COMMENT 'Numero de l''avis envoyé par le concentrateur';

ALTER TABLE `AcheteurPublic` ADD COLUMN `departement_mise_en_ligne` TEXT NULL ;
ALTER TABLE `AcheteurPublic` ADD COLUMN `reference_commande` VARCHAR( 255 ) NOT NULL ;

-- OKO : Modification de la table 't_annonce_consultation'
ALTER TABLE `t_annonce_consultation` ADD CONSTRAINT `fk_tAnnonceConsultation_Consultation` FOREIGN KEY ( `ref_consultation` ) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE ;

-- OKO : modification de la table 't_support_annonce_consultation'
ALTER TABLE `t_support_annonce_consultation` ADD COLUMN `date_envoi_support` DATETIME NULL COMMENT 'Date d''envoi de la publicité au support';
ALTER TABLE `t_support_annonce_consultation` ADD COLUMN `date_publication_support` DATETIME NULL COMMENT 'Date de publication de la publicité au support';

-- OKO : modification de la table 't_dispositif_annonce'
ALTER TABLE `t_dispositif_annonce` ADD COLUMN `id_dispositif_rectificatif` INT( 11 ) NULL COMMENT 'Identifiant du dispositif SUB rectificatif pour ce dispositif';
ALTER TABLE `t_dispositif_annonce` ADD COLUMN `id_dispositif_annulation` INT( 11 ) NULL COMMENT 'Identifiant du dispositif SUB d''annulation pour ce dispositif';
ALTER TABLE `t_dispositif_annonce` ADD COLUMN `sigle` VARCHAR( 255 ) NULL COMMENT 'Sigle du dispositif';
ALTER TABLE `t_dispositif_annonce` ADD COLUMN `id_type_avis_rectificatif` INT( 11 ) NULL COMMENT 'Identifiant du type d''avis rectificatif associé';
ALTER TABLE `t_dispositif_annonce` ADD COLUMN `id_type_avis_annulation` INT( 11 ) NULL COMMENT 'Identifiant du type d''avis d''annulation associé';


-- OKO : modification de la table 't_type_annonce_consultation'
ALTER TABLE `t_type_annonce_consultation` ADD COLUMN `visible` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

-- OKO : modification de la table 't_annonce_consultation'
ALTER TABLE `t_annonce_consultation` ADD COLUMN `id_dossier_parent` INT( 11 ) NULL COMMENT 'Identifiant du dossier parent';
ALTER TABLE `t_annonce_consultation` ADD COLUMN `id_dispositif_parent` INT( 11 ) NULL COMMENT 'Identifiant du dispositif parent';

-- OKO : modification de la table 't_support_annonce_consultation'
ALTER TABLE `t_support_annonce_consultation` ADD COLUMN `numero_avis_parent` INT( 11 ) NULL COMMENT 'Numero de l''avis parent';
ALTER TABLE `t_support_annonce_consultation` CHANGE `numero_avis_parent` `numero_avis_parent` VARCHAR( 255 ) NULL DEFAULT NULL COMMENT 'Numero de l''avis parent';

-- OKO : creation de la table `t_nature_marche`
CREATE TABLE IF NOT EXISTS `t_nature_marche` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `id_categorie_consultation` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- OKO : creation de la table `t_donnee_complementaire`
ALTER TABLE `t_donnee_complementaire` ADD COLUMN `nature_marche` INT( 11 ) NULL ;
ALTER TABLE `t_donnee_complementaire` ADD COLUMN `montant_marche` double NULL ;

-- OKO : modification de la table 'configuration_plateforme'
ALTER TABLE `configuration_plateforme` ADD COLUMN `redac_echos` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''affichage des champs necessaires pour la publicité BOAMP et les Echos';

-- OKO : modification de la table 't_donnee_complementaire'
ALTER TABLE `t_donnee_complementaire` ADD COLUMN `article_29` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `t_donnee_complementaire` ADD COLUMN `article_30` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `t_donnee_complementaire` ADD COLUMN `article_147` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `t_donnee_complementaire` ADD COLUMN `article_148` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- AME
ALTER TABLE `ValeurReferentielOrg` ADD COLUMN `valeur_sub` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `ValeurReferentiel` ADD COLUMN `valeur_sub` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `GeolocalisationN2` ADD COLUMN `valeur_sub` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `t_nature_marche` ADD COLUMN `valeur_sub` VARCHAR( 255 ) NOT NULL ;


CREATE TABLE IF NOT EXISTS `t_referentiel_produit_associe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  `valeur_sub` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `t_referentiel_mots_cles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  `valeur_sub` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `t_donnee_complementaire` ADD COLUMN `produits_associes` VARCHAR( 255 ) NULL ;
ALTER TABLE `t_donnee_complementaire` ADD COLUMN `mots_cles` VARCHAR( 255 ) NULL;


ALTER TABLE `Organisme` ADD COLUMN `sous_type_organisme` INT( 11 ) NOT NULL DEFAULT '2' COMMENT '1: Etat et ses établissements publics - Autres que ceux ayant un caractère industriel et commercial 2 :Collectivités territoriales / EPL / EPS';


CREATE TABLE IF NOT EXISTS `t_parametrage_choix_formulaire_pub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_organisme` int(11) NOT NULL,
  `sous_type_organisme` int(11) NOT NULL,
  `categorie` int(11) NOT NULL,
  `article` int(11) NOT NULL,
  `seuil_min` int(11) NOT NULL,
  `seuil_max` int(11) NOT NULL,
  `libelle_formulaire` varchar(255) NOT NULL,
  `id_dispositif_sub` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `t_relation_consultation_anoonce_pub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_consultation` int(11) NOT NULL,
  `organisme` varchar(30) NOT NULL,
  `id_compte_boamp` int(11) NOT NULL,
  `id_dispositif` int(11) NOT NULL,
  `id_annonce_consultation` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_relation_consultation_anoonce_pub` ADD INDEX `IDX_REF_CONS_RELATION` ( `ref_consultation` ) ;
ALTER TABLE `t_relation_consultation_anoonce_pub` ADD INDEX `IDX_ORG_RELATION` ( `organisme` ) ;
ALTER TABLE `t_relation_consultation_anoonce_pub` ADD INDEX `IDX_COMPTE_BOAMP_RELATION` ( `id_compte_boamp` ) ;
ALTER TABLE `t_relation_consultation_anoonce_pub` ADD INDEX `IDX_DISPOSITIF_RELATION` ( `id_dispositif` ) ;
ALTER TABLE `t_relation_consultation_anoonce_pub` ADD INDEX `IDX_ID_ANN_CONS_RELATION` ( `id_annonce_consultation` ) ;

ALTER TABLE `t_parametrage_choix_formulaire_pub` ADD INDEX `IDX_TYPE_ORG` ( `type_organisme` ) ;
ALTER TABLE `t_parametrage_choix_formulaire_pub` ADD INDEX `IDX_SOUS_TYPE_ORG` ( `sous_type_organisme` ) ;
ALTER TABLE `t_parametrage_choix_formulaire_pub` ADD INDEX `IDX_CATEGORIE` ( `categorie` ) ;
ALTER TABLE `t_parametrage_choix_formulaire_pub` ADD INDEX `IDX_ARTICLE` ( `article` ) ;
ALTER TABLE `t_parametrage_choix_formulaire_pub` ADD INDEX `IDX_SEUIL_MIN` ( `seuil_min` ) ;
ALTER TABLE `t_parametrage_choix_formulaire_pub` ADD INDEX `IDX_SEUIL_MAX` ( `seuil_max` ) ;
ALTER TABLE `t_parametrage_choix_formulaire_pub` ADD INDEX `IDX_DISPO_SUB` ( `id_dispositif_sub` ) ;

ALTER TABLE `t_dispositif_annonce` ADD COLUMN `libelle_formulaire_propose` VARCHAR( 255 ) NOT NULL ;
DROP TABLE t_parametrage_choix_formulaire_pub;

