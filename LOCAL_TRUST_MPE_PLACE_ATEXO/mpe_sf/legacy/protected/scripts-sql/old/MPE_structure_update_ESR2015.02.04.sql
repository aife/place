-- OKO MPE-1941
ALTER TABLE `Chorus_echange` ADD COLUMN `type_envoi` INT( 2 ) NOT NULL DEFAULT '0' COMMENT '0=>valeur par defaut, 1=> type d''envoi pieces jointes, 2=>types d''envoi acte modificatif';
-- YEL MPE-1881

ALTER TABLE `Tiers` ADD COLUMN `organisme` varchar(30) NULL DEFAULT NULL;
ALTER TABLE `Tiers` ADD CONSTRAINT `Tiers_organisme_FK` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `configuration_plateforme` ADD COLUMN `web_service_par_silo` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'lorsqu,il est à 1 le filtre par silo organisme au niveau du WS est activé';