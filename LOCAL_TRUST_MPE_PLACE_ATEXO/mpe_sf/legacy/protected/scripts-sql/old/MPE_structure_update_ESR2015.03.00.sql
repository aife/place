-- LEZ MEP-1662
CREATE TABLE IF NOT EXISTS `t_document` (
  `id_document` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NULL DEFAULT NULL,
  `taille` varchar(50) NULL DEFAULT NULL,
  `organisme` varchar(45) NULL DEFAULT NULL,
  `consultation_ref` int(11) NULL DEFAULT NULL,
  `lot` int(11) NULL DEFAULT 0,
  `empreinte` TEXT NULL DEFAULT NULL,
  `type_empreinte` varchar(255) NULL DEFAULT NULL,
  `id_type_document` int(11) NULL DEFAULT NULL,
  `date_creation` DATETIME default NULL,
  `id_fichier` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_document`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_document_type` ADD COLUMN `document_entreprise` CHAR(1) NOT NULL DEFAULT '0' ;