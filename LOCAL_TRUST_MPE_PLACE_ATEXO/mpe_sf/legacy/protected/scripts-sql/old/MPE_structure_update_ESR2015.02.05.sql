-- AME
ALTER TABLE `HabilitationAgent`	ADD COLUMN `gerer_contrat` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise d''Éditer le contrat et la Supprimer ' AFTER `creer_contrat`;
ALTER TABLE `HabilitationAgent` ADD COLUMN `consulter_contrat` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise de Consulter le contrat' AFTER `gerer_contrat`;
ALTER TABLE `HabilitationProfil`	ADD COLUMN `gerer_contrat` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise d''Éditer le contrat et la Supprimer ' AFTER `creer_contrat`;
ALTER TABLE `HabilitationProfil` ADD COLUMN `consulter_contrat` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise de Consulter le contrat' AFTER `gerer_contrat`;

ALTER TABLE `t_contrat_titulaire`	ADD COLUMN `intitule` VARCHAR(255) NULL DEFAULT '' AFTER `objet_contrat`;


ALTER TABLE `InvitationConsultationTransverse` ADD COLUMN `id_contrat_titulaire` INT(11) NULL DEFAULT NULL AFTER `date_decision`;

-- LEZ
CREATE TABLE IF NOT EXISTS `t_donnees_consultation` (
	`id_donnees_consultation` INT(11) NOT NULL AUTO_INCREMENT,
	`reference_consultation` INT(11) NULL,
	`id_contrat_titulaire` INT(11) NULL,
	`organisme` VARCHAR(45) NULL,
	`service_id` INT(11) NULL,
	`id_type_procedure` INT(11) NULL,
	`libelle_type_procedure` VARCHAR(100) NULL,
	`nbre_offres_recues` INT(11) NULL,
	`nbre_offres_dematerialisees` INT(11) NULL,
	PRIMARY KEY (`id_donnees_consultation`)
) ENGINE=InnoDB;

ALTER TABLE `t_donnees_consultation`	DROP FOREIGN KEY `fk_Donnees_Consultation_Contrat_Titulaire`;
ALTER TABLE `t_donnees_consultation`	ADD CONSTRAINT `fk_Donnees_Consultation_Contrat_Titulaire` FOREIGN KEY (`id_contrat_titulaire`) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `t_contrat_titulaire`	DROP COLUMN `code_cpv_3`;
ALTER TABLE `t_contrat_titulaire`	DROP COLUMN `code_cpv_4`;

-- LEZ
ALTER TABLE `t_donnees_consultation` ADD COLUMN `signature_offre` CHAR(1) NULL ;

-- OKO MPE-1970
ALTER TABLE `Chorus_echange` ADD COLUMN `tmp_file_name` VARCHAR( 255 ) NULL COMMENT 'Chemin du fichier temporaire de stockage du flux apres la generation';
ALTER TABLE `Chorus_echange` ADD COLUMN `type_flux_a_envoyer` INT( 10 ) NULL COMMENT '1=>FEN111, 2=>FEN211, 3=>FIR';
