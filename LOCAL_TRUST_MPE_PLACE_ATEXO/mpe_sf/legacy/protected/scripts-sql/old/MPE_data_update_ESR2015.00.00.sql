INSERT INTO `t_document_type` (`id_type_document`, `nom_type_document`) VALUES (1, 'Attestation fiscale');
INSERT INTO `t_document_type` (`id_type_document`, `nom_type_document`) VALUES (2, 'Attestation de marché public');
INSERT INTO `t_document_type` (`id_type_document`, `nom_type_document`) VALUES (3, 'Attestation de vigilence');

UPDATE `t_document_type` SET  `nom_type_document` =  'Attestation de vigilance' WHERE  `t_document_type`.`id_type_document` =3;

-- LEZ
UPDATE `DocumentsAttaches` SET `nom_document`='NOTI2' WHERE `id_document`=4;
UPDATE `DocumentsAttaches` SET `nom_document`='Autre' WHERE `id_document`=6;



UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr`='Validation du formulaire de réponse de la consultation avec : ' WHERE  `id`=202 AND `id_referentiel`=210;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel`='Validation du formulaire de réponse de la consultation avec : ' WHERE  `id`=202 AND `id_referentiel`=210;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel`='Affichage de la page de confirmation du dépôt de la réponse et envoi d''un courriel de confirmation à l''utilisateur' WHERE  `id`=204 AND `id_referentiel`=210;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr`='Affichage de la page de confirmation du dépôt de la réponse et envoi d''un courriel de confirmation à l''utilisateur' WHERE  `id`=204 AND `id_referentiel`=210;


INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES (406, 210, 'Présentation à l''utilisateur d''une alerte lui indiquant un problème avec les signatures de ses documents', 'Présentation à l''utilisateur d''une alerte lui indiquant un problème avec les signatures de ses documents', '', '', '', '', '', '', 'DESCRIPTION25', '');UPDATE `DocumentsAttaches` SET `nom_document`='Autre' WHERE `id_document`=6;

-- LEZ MPE-1154
UPDATE consultation SET consultation.lieu_execution=CONCAT(`lieu_execution`,',') WHERE lieu_execution NOT LIKE '' AND lieu_execution NOT LIKE '%,';
UPDATE consultation SET consultation.lieu_execution=CONCAT(',',`lieu_execution`) WHERE lieu_execution NOT LIKE '' AND lieu_execution NOT LIKE ',%';

-- AME
UPDATE `Type_Procedure_Organisme` SET `libelle_type_procedure`='Accord-Cadre - 1 Sélection des Attributaires (obsolète)' WHERE  `id_type_procedure`=30;
UPDATE `Type_Procedure_Organisme` SET `libelle_type_procedure`='Procédure de passation des marchés subséquents' WHERE  `id_type_procedure`=31;
UPDATE `Type_Procedure_Organisme` SET `libelle_type_procedure`='Système d''Acquisition Dynamique' WHERE  `id_type_procedure`=32;
UPDATE `Type_Procedure_Organisme` SET `libelle_type_procedure`='Procédure de passation des marchés spécifiques' WHERE  `id_type_procedure`=33;
UPDATE `Type_Procedure_Organisme` SET `libelle_type_procedure`='Délégation de service public (obsolète)' WHERE  `id_type_procedure`=35;

UPDATE `TypeProcedure` SET `libelle_type_procedure`='Accord-Cadre - 1 Sélection des Attributaires (obsolète)' WHERE  `id_type_procedure`=30;
UPDATE `TypeProcedure` SET `libelle_type_procedure`='Procédure de passation des marchés subséquents' WHERE  `id_type_procedure`=31;
UPDATE `TypeProcedure` SET `libelle_type_procedure`='Système d''Acquisition Dynamique' WHERE  `id_type_procedure`=32;
UPDATE `TypeProcedure` SET `libelle_type_procedure`='Procédure de passation des marchés spécifiques' WHERE  `id_type_procedure`=3;
UPDATE `TypeProcedure` SET `libelle_type_procedure`='Délégation de service public (obsolète)' WHERE  `id_type_procedure`=35;


UPDATE `Type_Procedure_Organisme` SET `activer_mapa`='0' WHERE  `id_type_procedure`=6 ;
UPDATE `Type_Procedure_Organisme` SET `activer_mapa`='0' WHERE  `id_type_procedure`=7 ;

UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel`='Critère unique de prix ou de coût' WHERE `id`=3 AND `id_referentiel`=19;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr`='Critère unique de prix ou de coût' WHERE `id`=3 AND `id_referentiel`=19;

INSERT INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`,`abbreviation`, `type_boamp`, `categorie_procedure`, `id_type_procedure_ANM`, `delai_alerte`, `mapa`, `consultation_transverse`, `code_recensement`,`abbreviation_portail_ANM`, `id_modele`, `libelle_type_procedure_it`) VALUES
(1000, 'Procédure concurrentielle avec négociations', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PCN', '0', '1', '1', '0', '0', '0', NULL, 'PCN', '0', NULL);

UPDATE `TypeProcedure` SET `libelle_type_procedure`='Procédure de passation des marchés spécifiques' WHERE  `id_type_procedure`=33;
UPDATE `TypeProcedure` SET `libelle_type_procedure`='Procédure adaptée' WHERE  `id_type_procedure`=3;

INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_2`) VALUES (5, 19, 'Critère unique du coût déterminé selon une approche globale', 'Critère unique du coût déterminé selon une approche globale', 'layerDefinitionVide_3');