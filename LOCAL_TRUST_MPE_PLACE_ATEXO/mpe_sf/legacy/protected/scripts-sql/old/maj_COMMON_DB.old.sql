-- Script de mise à jour des BDD communes
--
-- convention: 
-- Chaque nouvelle requete doit commencer au debut d'une ligne et se finir par un point virgule suivi d'un retour à la ligne
-- si besoin est de splitter en plusieurs lignes, mettre une tabulation

ALTER TABLE `OrganePublication` ADD `org_id` INT NOT NULL;
ALTER TABLE `ModuleOrganisme` ADD `retrait_par_lots_DCE` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleOrganisme` ADD `alerte_reponse_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `Inscrit` CHANGE `categorie` `categorie` TEXT DEFAULT NULL;

-- date : 02/05/2007 ; auteur : RIAD Nawal ; raison : FS génération automatique de codes restreints
ALTER TABLE `consultation` CHANGE `code_procedure` `code_procedure` VARCHAR( 9 ) NULL DEFAULT NULL;
--date : 02/05/2007 ; auteur : RIAD Nawal ; raison : FS Icone Authentification 
ALTER TABLE `consultation` ADD `signature_offre` char(1) default NULL;
--date : 02/05/2007 ; auteur : RIAD Nawal ; raison : FS Gestion des Alertes
ALTER TABLE `Agent` ADD `RECEVOIR_MAIL` ENUM( '0', '1' ) DEFAULT '0' NOT NULL ;
ALTER TABLE `ModuleOrganisme` ADD `gestion_alerte` ENUM ('0' , '1')  NOT NULL default '0';
--date : 02/05/2007 ; auteur : RIAD Nawal ; raison : FS Gestion des types de MAPA
ALTER TABLE `HabilitationAgent` ADD `gestion_mapa` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `gestion_mapa` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `TypeProcedure` ADD `mapa` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `ModuleOrganisme` ADD `gestion_mapa` ENUM ('0' , '1')  NOT NULL default '0';
--date : 02/05/2007 ; auteur : RIAD Nawal ; raison : FS Validation des types de procédures
ALTER TABLE `consultation` ADD `id_type_validation` int(11) NOT NULL default '2';
ALTER TABLE `consultation` ADD `etat_approbation` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `consultation` ADD `etat_validation` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `HabilitationAgent` ADD `gestion_type_validation` ENUM ('0', '1')  NOT NULL default '0';
ALTER TABLE `HabilitationAgent` ADD `approuver_consultation` ENUM ('0', '1')  NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `gestion_type_validation` ENUM ('0', '1')  NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `approuver_consultation` ENUM ('0', '1')  NOT NULL default '0';
ALTER TABLE `ModuleOrganisme` ADD `type_validation` ENUM ('0' , '1')  NOT NULL default '0';
--date : 02/05/2007 ; auteur : RIAD Nawal ; raison : FS Procedures administrables
ALTER TABLE `HabilitationAgent` ADD `administrer_procedure` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `administrer_procedure` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `ModuleOrganisme` ADD `gestion_procedure_administrable` ENUM ('0' , '1')  NOT NULL default '0';

--date : 03/05/2007 ; auteur : DIAGNE OUSSEYNOU ; raison : Ajout Module de pré-remplissage des formulaires BOAMP  
ALTER TABLE `ModuleOrganisme` ADD `boamp_champs_preremplis` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
--date : 03/05/2007 ; auteur : RIAD Nawal ; raison : FS Duplication des agents
ALTER TABLE `ModuleOrganisme` ADD `duplication_agent` ENUM ('0' , '1')  NOT NULL default '0';

--date : 04/05/2007 ; auteur : RIAD Nawal ; raison : FS Acces MAPA
ALTER TABLE `ModuleOrganisme` ADD `acces_mapa` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `HabilitationAgent` ADD `restreindre_creation` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `restreindre_creation` ENUM ('0' , '1')  NOT NULL default '0';

--date : 07/05/2007 ; auteur : GOUBA Nadine ; raison : FS Admin acces mdp agents
ALTER TABLE `HabilitationAgent` ADD `administrer_compte` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `administrer_compte` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `ModuleOrganisme` ADD `gestion_acces_compte` ENUM ('0' , '1')  NOT NULL default '0';

--date : 07/05/2007 ; auteur : GOUBA Nadine ; raison : FS Champ supp invisible
ALTER TABLE `consultation` ADD `champ_supp_invisible` varchar(100) NOT NULL default '';

--date : 07/05/2007 ; auteur : GOUBA Nadine ; raison : FS Champ supp par lot
ALTER TABLE `CategorieLot` ADD `description_detail` VARCHAR( 255 ) NULL ;

--date : 18/05/2007 ; auteur : RIAD Nawal ; raison : Intégration de JOSSO et du socle
ALTER TABLE `ModuleOrganisme` ADD `pae_socle` ENUM ('0' , '1')  NOT NULL default '0';

--date : 23/05/2007 ; auteur : GOUBA Nadine ; raison : FS Liste des marchés
ALTER TABLE `ModuleOrganisme` ADD `liste_marches` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `HabilitationAgent` ADD `creer_liste_marches` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `creer_liste_marches` ENUM ('0' , '1')  NOT NULL default '0';
--date : 23/05/2007 ; auteur : KHALID BENAMAR ; raison : FS Gestion des types de MAPA
UPDATE `TypeProcedure` SET `mapa`='1' where `id_type_procedure`='4';

--date : 29/05/2007 ; auteur : LCH : ajout des tables manquantes suite à la synchro des bases (prod et dev)
CREATE TABLE IF NOT EXISTS `Commission` (
  `id` int(11) NOT NULL auto_increment,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `lieu` varchar(200) NOT NULL default '',
  `salle` varchar(100) NOT NULL default '',
  `type` varchar(200) NOT NULL default '',
  `libelle` varchar(250) default NULL,
  `ordre_du_jour` longblob,
  `invitations_zip` longblob,
  `status_cao` char(2) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;
CREATE TABLE IF NOT EXISTS `Intervenant_Externe` (
  `id` int(11) NOT NULL auto_increment,
  `nom` varchar(30) NOT NULL default '',
  `prenom` varchar(30) NOT NULL default '',
  `organisation` varchar(30) NOT NULL default '',
  `fonction` varchar(200) NOT NULL default '',
  `adresse` varchar(255) NOT NULL default '',
  `mail` varchar(100) NOT NULL default '',
  `type_invitation` int(1) NOT NULL default '1',
  `civilite` varchar(255) default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;
CREATE TABLE IF NOT EXISTS `Intervenant_Externe_Commission` (
  `id_commission` int(11) NOT NULL default '0',
  `id_intervenant_externe` int(11) NOT NULL default '0',
  `convocation` longblob,
  `convoc_send` char(1) NOT NULL default '0',
  `nom_convoc` varchar(255) default NULL,
  `contenu_envoi` text,
  `nom_fichier_envoye` varchar(255) default NULL,
  `fichier_envoye` longblob,
  `date_envoi` datetime default NULL,
  PRIMARY KEY  (`id_commission`,`id_intervenant_externe`),
  KEY `id_intervenant_externe` (`id_intervenant_externe`)
) TYPE=InnoDB;
CREATE TABLE IF NOT EXISTS `Intervenant_Ordre_Du_Jour` (
  `id` int(11) NOT NULL auto_increment,
  `id_ordre_du_jour` int(11) NOT NULL default '0',
  `id_intervenant` int(11) NOT NULL default '0',
  `id_agent` int(11) NOT NULL default '0',
  `convocation` longblob,
  `convoc_send` char(1) NOT NULL default '0',
  `nom_convoc` varchar(255) default NULL,
  `contenu_envoi` text,
  `nom_fichier_envoye` varchar(255) default NULL,
  `fichier_envoye` longblob,
  `date_envoi` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `id_ordre_du_jour` (`id_ordre_du_jour`)
) TYPE=InnoDB;
CREATE TABLE IF NOT EXISTS `Ordre_Du_Jour` (
  `id` int(11) NOT NULL auto_increment,
  `id_commission` int(11) NOT NULL default '0',
  `ref_consultation` varchar(255) default NULL,
  `ref_libre` varchar(50) default NULL,
  `id_etape` int(11) NOT NULL default '1',
  `intitule_ordre_du_jour` varchar(255) default NULL,
  `lots_odj_libre` varchar(100) default NULL,
  `id_type_procedure` int(11) default NULL,
  `date_cloture` datetime default NULL,
  `type_env` int(1) default NULL,
  `sous_pli` int(2) default NULL,
  `heure` varchar(5) NOT NULL default '00',
  `etape_consultation` varchar(200) NOT NULL default '',
  `type_consultation` varchar(200) NOT NULL default '',
  `minutes` char(2) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id_commission` (`id_commission`)
) TYPE=InnoDB;

--date : 29/05/2007 ; auteur : LCH : ajout des champs manquants suite à la synchro des bases (prod et dev)
ALTER TABLE `MessagePublication` ADD  `id_boamp` int(11) default NULL;
ALTER TABLE `MessagePublication` ADD   `attached_file` int(11) default NULL;
ALTER TABLE `MessagePublication`  ADD   `attached_file_name` varchar(80) default NULL;
ALTER TABLE `ModuleOrganisme`  ADD   `envoi_mail_avec_ar_obligatoire` enum('0','1') NOT NULL default '0';
ALTER TABLE `ModuleOrganisme`  ADD `gestion_commissions` enum('0','1') NOT NULL default '0';
ALTER TABLE `ModuleOrganisme` CHANGE `gestion_procedure_administrable` `gestion_procedue_administrable` enum('0','1') NOT NULL default '1';
ALTER TABLE `consultation` CHANGE `id_type_validation` `id_type_validation` INT( 11 ) NOT NULL DEFAULT '2';
ALTER TABLE `HabilitationProfil` ADD `suivi_seul_consultation` enum('0','1') NOT NULL default '0';
ALTER TABLE `HabilitationAgent` ADD `suivi_seul_consultation` enum('0','1') NOT NULL default '0';

-- date : 25/06/2007; auteur : LCH  : Champs manquant
ALTER TABLE `Annonce` ADD `nom_fichier_xml` varchar(255) NOT NULL default '' AFTER `consultaion_ref`;

-- date : 09/07/2007; auteur : NRI  : Ajout du champ 'status'
ALTER TABLE `Organisme`  ADD `status` enum('0','1') NOT NULL default '0';

-- date : 25/07/2007; auteur : NRI  : nouvelle fs centrale de publication MEDIALEX
CREATE TABLE IF NOT EXISTS `JournalPublication` (
  `id_journal` int(11) NOT NULL auto_increment,
  `identifiant_journal` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `centrale_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id_journal`),
  KEY `centrale_id` (`centrale_id`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;
ALTER TABLE `OrganePublication` ADD `centrale` ENUM ('0' , '1')  NOT NULL default '0'AFTER `org_id`;
ALTER TABLE `ModuleOrganisme` ADD `centrale_publication` ENUM ('0' , '1')  NOT NULL default '0' AFTER `gestion_commissions`;

-- date : 27/07/2007; auteur : NRI  : nouvelle fs centrale de publication MEDIALEX
ALTER TABLE `JournalPublication`
ADD CONSTRAINT  FOREIGN KEY (`centrale_id`) REFERENCES `OrganePublication` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- date : 27/07/2007; auteur : CGO  : Correction Type procédure par défaut
UPDATE `ProcedureEquivalence` SET `mise_en_ligne1` = '+0', `mise_en_ligne2` = '+1', `mise_en_ligne3` = '+0', `mise_en_ligne4` = '+0', `env_offre_type_unique` = '+1', `env_offre_type_multiple` = '+0', `no_fichier_annonce` = '+0', `fichier_importe` = '+0', `fichier_boamp` = '+0', `reglement_cons` = '+1', `dossier_dce` = '+1', `partial_dce_download` = '+1', `service` = '+1', `constitution_dossier_reponse` = '+0', `env_offre_type_unique2` = '+1', `env_offre_type_multiple2` = '+0', `gestion_envois_postaux` = '+0', `tireur_plan_non` = '+1', `tireur_plan_oui` = '+0', `tireur_plan_papier` = '+0', `tireur_plan_cdrom` = '+0', `tireur_plan_nom` = '+0', `tirage_descriptif` = '+1' WHERE `no_fichier_annonce` = '';
-- date : 05/10/2007; auteur : NRI  : nouvelle fs Coffre fort éléctronique
ALTER TABLE `ModuleCommun` ADD `gestion_coffre_fort_entreprise` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `responsableengagement` ADD ` champ_supp_invisible`;
ALTER TABLE `HabilitationProfil` ADD `attribution_marche` ENUM( '0', '1' ) NOT NULL DEFAULT '0'; 
ALTER TABLE `HabilitationAgent` ADD `attribution_marche` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- date : 10/10/2007; auteur : KBE  : nouvelle fs Code Cpv
ALTER TABLE `ModuleCommun` ADD `code_cpv` ENUM ('0' , '1')  NOT NULL default '0';

ALTER TABLE `consultation` ADD `code_cpv_1`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `code_cpv_2`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `code_cpv_3`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `code_cpv_4`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `code_cpv_1`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `code_cpv_2`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `code_cpv_3`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `code_cpv_4`  VARCHAR( 8 ) NULL DEFAULT NULL ;

ALTER TABLE `consultation` ADD `libelle_code_cpv_1`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `libelle_code_cpv_2`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `libelle_code_cpv_3`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `libelle_code_cpv_4`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `libelle_code_cpv_1`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `libelle_code_cpv_2`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `libelle_code_cpv_3`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `libelle_code_cpv_4`  VARCHAR( 100 ) NULL DEFAULT NULL ;


ALTER TABLE `Inscrit` ADD `code_cpv`  TEXT NULL DEFAULT NULL ;

-- date : 19/11/2007; auteur : KBE  : Coffre fort électronique Migration des tables justificatifs et documentsattaches à la base commune

CREATE TABLE `DocumentsAttaches` (
  `id_document` int(11) NOT NULL auto_increment,
  `nom_document` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id_document`),
  KEY `id_document` (`id_document`)
) TYPE=InnoDB;

INSERT INTO `DocumentsAttaches` (`id_document`, `nom_document`) VALUES 
(1, 'Certificats fiscaux 3666 volets 1 à 4'),
(2, 'Certificats sociaux'),
(3, 'K-BIS'),
(4, 'DC7'),
(5, 'Attestation d''assurance'),
(6, 'Certificat de congés payés');

CREATE TABLE `Justificatifs` (
  `id` int(11) NOT NULL auto_increment,
  `intitule_justificatif` varchar(255) NOT NULL default '',
  `id_entreprise` int(11) NOT NULL default '0',
  `taille` varchar(80) NOT NULL default '',
  `justificatif` int(11) NOT NULL default '0',
  `statut` enum('0','1') NOT NULL default '0',
  `id_document` int(11) default NULL,
  `nom` varchar(100) default NULL,
  PRIMARY KEY  (`id`),
  KEY `id` (`id`),
  KEY `id_entreprise` (`id_entreprise`),
  KEY `id_document` (`id_document`)
) TYPE=InnoDB ;

CREATE TABLE `blob_file` (
  `id` int(11) NOT NULL default '0',
  `revision` int(11) NOT NULL default '0',
  `name` text NOT NULL,
  `deletion_datetime` datetime default NULL,
  PRIMARY KEY  (`id`,`revision`)
) TYPE=InnoDB;

CREATE TABLE `blob` (
  `id` int(11) NOT NULL auto_increment,
  `revision` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB ;

ALTER TABLE `ModuleCommun` ADD `gestion_coffre_fort_entreprise` ENUM ('0' , '1')  NOT NULL default '0' ;

-- date : 27/11/2007; auteur : CGO  : Formulaire création consultations - Passage en mode module des Onglets
ALTER TABLE `ModuleOrganisme` ADD `form_creation_onglets` ENUM ('0' , '1')  NOT NULL default '0';

-- date : 07/02/2008; auteur : CGO  : Infos/Liens situé dans le menu agent de gauche - Passage en mode module
ALTER TABLE `ModuleOrganisme` ADD `info_menu_gauche_agent` ENUM ('0' , '1')  NOT NULL default '0';
CREATE TABLE `Code_cpv_inscrit` (
  `id_inscrit` int(11) NOT NULL default '0',
  `code_cpv` varchar(8) NOT NULL default '',
  `libelle_code_cpv` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id_inscrit`,`code_cpv`)
) TYPE=InnoDB;


--date : 05/11/2007; auteur : AZA : ouverture simultanée (4 eyes)
ALTER TABLE `ModuleOrganisme` ADD `ouverture_simultanee` ENUM ('0' , '1')  NOT NULL default '0';

--date : 05/11/2007; auteur : AZA : Taille entreprise 
ALTER TABLE `ModuleCommun` ADD `option_taille_entreprise` ENUM ('0' , '1')  NOT NULL default '0';

CREATE TABLE `tailleentreprise` (
  `id_taille_entreprise` int(11) NOT NULL auto_increment,
  `tailleentreprise` varchar(25) NOT NULL default '',
  PRIMARY KEY  (`id_taille_entreprise`)
) TYPE=MyISAM AUTO_INCREMENT=6 ;

-- 
-- Contenu de la table `tailleentreprise`
-- 

INSERT INTO `tailleentreprise` (`id_taille_entreprise`, `tailleentreprise`) VALUES 
(5, 'de 1 à 10 personnes'),
(1, 'de 11 à 50 personnes'),
(2, 'de 51 à 100 personnes'),
(3, 'plus de 100 personnes');


ALTER TABLE `Entreprise` ADD `taille` int(2) default NULL AFTER `email` ;

--date : 28/11/2007 auteur : AZA : Multilinguisme dynamique
ALTER TABLE `Inscrit` ADD `id_langue`  int(11) NULL DEFAULT NULL ;
ALTER TABLE `ModuleCommun` ADD `multi_linguisme` ENUM ('0' , '1')  NOT NULL default '0';

ALTER TABLE `TypeAvis` ADD `intitule_avis_fr` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeAvis` ADD `intitule_avis_en` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeAvis` ADD `intitule_avis_es` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeAvis` ADD `intitule_avis_su` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeAvis` ADD `intitule_avis_du` VARCHAR( 100 ) NULL ; 
ALTER TABLE `TypeAvis` ADD `intitule_avis_cz` VARCHAR( 100 ) NULL ;

ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_fr` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_en` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_es` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_su` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_du` VARCHAR( 100 ) NULL ; 
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_cz` VARCHAR( 100 ) NULL ;

ALTER TABLE `CategorieConsultation` ADD `libelle_fr` VARCHAR( 100 ) NULL ;
ALTER TABLE `CategorieConsultation` ADD `libelle_en` VARCHAR( 100 ) NULL ;
ALTER TABLE `CategorieConsultation` ADD `libelle_es` VARCHAR( 100 ) NULL ;
ALTER TABLE `CategorieConsultation` ADD `libelle_su` VARCHAR( 100 ) NULL ;
ALTER TABLE `CategorieConsultation` ADD `libelle_du` VARCHAR( 100 ) NULL ; 
ALTER TABLE `CategorieConsultation` ADD `libelle_cz` VARCHAR( 100 ) NULL ;


ALTER TABLE `consultation` ADD `titre_fr` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `titre_en` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `titre_es` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `titre_su` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `titre_du` LONGTEXT  NULL ; 
ALTER TABLE `consultation` ADD `titre_cz` LONGTEXT  NULL ;

ALTER TABLE `consultation` ADD `resume_fr` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `resume_en` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `resume_es` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `resume_su` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `resume_du` LONGTEXT  NULL ; 
ALTER TABLE `consultation` ADD `resume_cz` LONGTEXT  NULL ;

ALTER TABLE `CategorieLot` ADD `description_fr` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_en` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_es` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_su` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_du` VARCHAR( 255 ) NULL ; 
ALTER TABLE `CategorieLot` ADD `description_cz` VARCHAR( 255 ) NULL ;

ALTER TABLE `CategorieLot` ADD `description_detail_fr` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_detail_en` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_detail_es` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_detail_su` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_detail_du` VARCHAR( 255 ) NULL ; 
ALTER TABLE `CategorieLot` ADD `description_detail_cz` VARCHAR( 255 ) NULL ;

CREATE TABLE `Langue` (
  `id_langue` int(11) NOT NULL auto_increment,
  `langue` varchar(10) NOT NULL default '',
  `active` enum('0','1') NOT NULL default '0',
  `defaut` enum('0','1') default '0',
  PRIMARY KEY  (`id_langue`)
) TYPE=InnoDB AUTO_INCREMENT=7 ;

CREATE TABLE `ConsultationLangue` (
  `id_langue` int(11) NOT NULL default '0',
  `reference` varchar(255) NOT NULL default ''
) TYPE=InnoDB;

INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`) VALUES (1, 'cz', '0', '0', NULL);
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`) VALUES (2, 'du', '0', '0', NULL);
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`) VALUES (3, 'en', '1', '0', 'English');
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`) VALUES (4, 'es', '1', '0', 'Spanish');
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`) VALUES (5, 'fr', '1', '1', 'French');
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`) VALUES (6, 'su', '0', '0', NULL);

CREATE TABLE `changement_heure` (
  `organisme` varchar(30) NOT NULL default '',
  `date_debut_zone` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_fin_zone` datetime NOT NULL default '0000-00-00 00:00:00',
  `decalage` char(3) NOT NULL default '0',
  PRIMARY KEY  (`date_debut_zone`,`date_fin_zone`,`organisme`),
  KEY `organisme` (`organisme`)
) TYPE=InnoDB;


-- 07-04-2008 : ajouter les trois nouvelles habilitations dansla tables habilitationAgent

ALTER TABLE `HabilitationAgent` ADD `fiche_recensement` ENUM( '0', '1' ) NOT NULL ,
ADD `declarer_infructueux` ENUM( '0', '1' ) NOT NULL ,
ADD `declarer_sans_suite` ENUM( '0', '1' ) NOT NULL ;

ALTER TABLE `HabilitationProfil` ADD `fiche_recensement` ENUM( '0', '1' ) NOT NULL ,
ADD `declarer_infructueux` ENUM( '0', '1' ) NOT NULL ,
ADD `declarer_sans_suite` ENUM( '0', '1' ) NOT NULL ;

ALTER TABLE `HabilitationAgent` ADD `creer_consultation_transverse` ENUM( '0', '1' ) NOT NULL;
ALTER TABLE `HabilitationProfil` ADD `creer_consultation_transverse` ENUM( '0', '1' ) NOT NULL;

CREATE TABLE `Departement` (
`id_departement` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`nom_departement` VARCHAR( 100 ) NOT NULL
) TYPE = MYISAM ;

ALTER TABLE `consultation` ADD `poursuivre_affichage` INT( 3 ) NOT NULL DEFAULT '0';
--22/04/2008 :MMI creation des tables de geolocalisation

CREATE TABLE `GeolocalisationN0` (
  `id` int(11) NOT NULL auto_increment,
  `denomination` varchar(40) NOT NULL default '',
  `type` enum('0','1') NOT NULL default '0',
  `libelle_selectionner` varchar(40) NOT NULL default '',
  `libelle_tous` varchar(40) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB AUTO_INCREMENT=4 ;



CREATE TABLE `GeolocalisationN1` (
  `id` int(11) NOT NULL auto_increment,
  `id_geolocalisationN0` int(11) NOT NULL default '0',
  `denomination1` varchar(30) NOT NULL default '',
  `denomination2` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id_geolocalisationN0` (`id_geolocalisationN0`)
) TYPE=InnoDB AUTO_INCREMENT=8 ;

-- 
-- Contenu de la table `GeolocalisationN1`
-- 



ALTER TABLE `GeolocalisationN1`
  ADD CONSTRAINT `GeolocalisationN1_ibfk_1` FOREIGN KEY (`id_geolocalisationN0`) REFERENCES `GeolocalisationN0` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE `GeolocalisationN2` (
  `id` int(11) NOT NULL auto_increment,
  `id_geolocalisationN1` int(11) NOT NULL default '0',
  `denomination1` varchar(30) NOT NULL default '',
  `denomination2` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id_geolocalisationN1` (`id_geolocalisationN1`)
) TYPE=InnoDB AUTO_INCREMENT=25 ;

-- 
-- Contenu de la table `GeolocalisationN2`
-- 



-- 
-- Contraintes pour la table `GeolocalisationN2`
-- 
ALTER TABLE `GeolocalisationN2`
  ADD CONSTRAINT `GeolocalisationN2_ibfk_1` FOREIGN KEY (`id_geolocalisationN1`) REFERENCES `GeolocalisationN1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ModuleCommun` ADD `gestion_fournisseurs_docs_pole` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `ModuleCommun` ADD `menu_pmi` ENUM( '0', '1' ) NOT NULL DEFAULT '0';


-- 30-04-2008 :mmi
ALTER TABLE `consultation` CHANGE `lieu_execution` `lieu_execution` TEXT NOT NULL ;



-- 07/05/2008 :mmi modification taille de champ denomination1 
ALTER TABLE `GeolocalisationN2` CHANGE `denomination1` `denomination1` VARCHAR( 50 ) NOT NULL;

-- 09-05-2008 :mmi 

ALTER TABLE `Entreprise` ADD `telephone` VARCHAR( 50 ) NULL;
ALTER TABLE `Entreprise` ADD `fax` VARCHAR( 50 ) NULL ;
-- 09-05-2008 : mmi

ALTER TABLE `ModuleCommun` ADD `authenticate_inscrit_by_cert` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `ModuleCommun` ADD `authenticate_inscrit_by_login` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `authenticate_inscrit_by_cert` ;

-- 12/09/2008 : MMI ajout champs profil dans la table inscrit
ALTER TABLE `Inscrit` ADD `profil` INT( 1 ) NOT NULL DEFAULT '1';

ALTER TABLE `ModuleCommun` ADD `base_qualifiee_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0';


ALTER TABLE `Inscrit` ADD `adresse2` VARCHAR( 80 ) NULL ;

-- 09-05-2008 
ALTER TABLE `Entreprise` ADD `site_internet` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `description_activite` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `activite_domaine_defense` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `annee_cloture_exercice1` VARCHAR( 15 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `annee_cloture_exercice2` VARCHAR( 15 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `annee_cloture_exercice3` VARCHAR( 15 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `effectif_moyen1` INT( 10 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `effectif_moyen2` INT( 10 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `effectif_moyen3` INT( 10 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `effectif_encadrement1` INT( 10 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `effectif_encadrement2` INT( 10 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `effectif_encadrement3` INT( 10 ) NOT NULL ;
ALTER TABLE `Entreprise` ADD `pme1` ENUM( '1', '0' ) NULL ;
ALTER TABLE `Entreprise` ADD `pme2` ENUM( '1', '0' ) NULL ;
ALTER TABLE `Entreprise` ADD `pme3` ENUM( '1', '0' ) NULL ;
ALTER TABLE `Entreprise` ADD `adresse2` VARCHAR( 80 ) NULL ;
ALTER TABLE `Entreprise` CHANGE `codeape` `codeape` VARCHAR( 5 ) NOT NULL; 


ALTER TABLE `CategorieLot` CHANGE `consultation` `consultation_ref` varchar(255) NOT NULL;
ALTER TABLE `Entreprise` ADD `origine_compte` varchar(255) default NULL AFTER `codeape`;
ALTER TABLE `Entreprise` ADD `telephone` varchar(50) default NULL AFTER `origine_compte`;
ALTER TABLE `GeolocalisationN0`ADD `libelle_Aucun` varchar(50) NOT NULL default '' AFTER `libelle_tous`;


-- 15/05/2008 MMI:

ALTER TABLE `Entreprise` ADD `nicSiege` VARCHAR( 5 ) NULL ;

--23/05/2008 MMI
ALTER TABLE `GeolocalisationN1` CHANGE `denomination1` `denomination1` VARCHAR( 100 ) NOT NULL ,
CHANGE `denomination2` `denomination2` VARCHAR( 100 ) NOT NULL ;


--26/05/2008 MMI:


-- 
-- Contenu de la table `GeolocalisationN0`
-- 

INSERT INTO `GeolocalisationN0` VALUES (1, 'France métropolitaine', '0', 'Sélectionner par département(s)', 'Tous les départements', 'Aucun département');
INSERT INTO `GeolocalisationN0` VALUES (2, 'France - Collectivité(s) d''Outre-Mer', '0', 'Sélectionner par collectivité(s)', 'Toutes les collectivités', 'Aucune collectivité');
INSERT INTO `GeolocalisationN0` VALUES (3, 'Autre(s) pays', '1', 'Sélectionner par pays', 'Tous les pays', 'Aucun pays');


-- 
-- Contenu de la table `GeolocalisationN1`
-- 

INSERT INTO `GeolocalisationN1` VALUES (40, 1, 'Alsace', '');
INSERT INTO `GeolocalisationN1` VALUES (41, 1, 'Aquitaine', '');
INSERT INTO `GeolocalisationN1` VALUES (42, 1, 'Auvergne', '');
INSERT INTO `GeolocalisationN1` VALUES (43, 1, 'Basse-Normandie', '');
INSERT INTO `GeolocalisationN1` VALUES (44, 1, 'Bourgogne', '');
INSERT INTO `GeolocalisationN1` VALUES (45, 1, 'Bretagne', '');
INSERT INTO `GeolocalisationN1` VALUES (46, 1, 'Centre', '');
INSERT INTO `GeolocalisationN1` VALUES (48, 1, 'Corse', '');
INSERT INTO `GeolocalisationN1` VALUES (49, 1, 'Franche-Comté', '');
INSERT INTO `GeolocalisationN1` VALUES (50, 1, 'Haute-Normandie', '');
INSERT INTO `GeolocalisationN1` VALUES (51, 1, 'Champagne-Ardennes', '');
INSERT INTO `GeolocalisationN1` VALUES (52, 1, 'Ile-de-France', '');
INSERT INTO `GeolocalisationN1` VALUES (53, 1, 'Languedoc-Roussillon', '');
INSERT INTO `GeolocalisationN1` VALUES (54, 1, 'Limousin', '');
INSERT INTO `GeolocalisationN1` VALUES (55, 1, 'Lorraine', '');
INSERT INTO `GeolocalisationN1` VALUES (56, 1, 'Midi-Pyrénées', '');
INSERT INTO `GeolocalisationN1` VALUES (57, 1, 'Nord-Pas-de-Calais', '');
INSERT INTO `GeolocalisationN1` VALUES (58, 1, 'Pays-de-la-Loire', '');
INSERT INTO `GeolocalisationN1` VALUES (59, 1, 'Picardie', '');
INSERT INTO `GeolocalisationN1` VALUES (60, 1, 'Poitou-Charentes', '');
INSERT INTO `GeolocalisationN1` VALUES (61, 1, 'Provence-Alpes Côte d''Azur', '');
INSERT INTO `GeolocalisationN1` VALUES (62, 1, 'Rhône-Alpes', '');
INSERT INTO `GeolocalisationN1` VALUES (233, 2, 'Départements d''Outre-Mer', '');
INSERT INTO `GeolocalisationN1` VALUES (234, 3, 'Pays', '');




-- 
-- Contenu de la table `GeolocalisationN2`
-- 

-- 
-- Contenu de la table `GeolocalisationN2`
-- 

INSERT INTO `GeolocalisationN2` VALUES (214, 40, '(67) Bas-Rhin', '');
INSERT INTO `GeolocalisationN2` VALUES (215, 40, '(68) Haut-Rhin', '');
INSERT INTO `GeolocalisationN2` VALUES (216, 41, '(24) Dordogne', '');
INSERT INTO `GeolocalisationN2` VALUES (217, 41, '(33) Gironde', '');
INSERT INTO `GeolocalisationN2` VALUES (218, 41, '(40) Landes', '');
INSERT INTO `GeolocalisationN2` VALUES (219, 41, '(47) Lot-et-Garonne', '');
INSERT INTO `GeolocalisationN2` VALUES (220, 41, '(64) Pyrénées-Atlantiques', '');
INSERT INTO `GeolocalisationN2` VALUES (221, 42, '(03) Allier', '');
INSERT INTO `GeolocalisationN2` VALUES (222, 42, '(15) Cantal', '');
INSERT INTO `GeolocalisationN2` VALUES (223, 42, '(43) Haute-Loire', '');
INSERT INTO `GeolocalisationN2` VALUES (224, 42, '(63) Puy-de-Dôme', '');
INSERT INTO `GeolocalisationN2` VALUES (225, 43, '(14) Calvados', '');
INSERT INTO `GeolocalisationN2` VALUES (226, 43, '(50) Manche', '');
INSERT INTO `GeolocalisationN2` VALUES (227, 43, '(61) Orne', '');
INSERT INTO `GeolocalisationN2` VALUES (228, 44, '(21) Côte-d''Or', '');
INSERT INTO `GeolocalisationN2` VALUES (229, 44, '(58) Nièvre', '');
INSERT INTO `GeolocalisationN2` VALUES (230, 44, '(71) Saône-et-Loire', '');
INSERT INTO `GeolocalisationN2` VALUES (231, 44, '(89) Yonne', '');
INSERT INTO `GeolocalisationN2` VALUES (232, 45, '(22) Côtes-d''Armor', '');
INSERT INTO `GeolocalisationN2` VALUES (233, 45, '(29) Finistère', '');
INSERT INTO `GeolocalisationN2` VALUES (234, 45, '(35) Ille-et-Vilaine', '');
INSERT INTO `GeolocalisationN2` VALUES (235, 45, '(56) Morbihan', '');
INSERT INTO `GeolocalisationN2` VALUES (236, 46, '(18) Cher', '');
INSERT INTO `GeolocalisationN2` VALUES (237, 46, '(28) Eure-et-Loir', '');
INSERT INTO `GeolocalisationN2` VALUES (238, 46, '(36) Indre', '');
INSERT INTO `GeolocalisationN2` VALUES (239, 46, '(37) Indre-et-Loire', '');
INSERT INTO `GeolocalisationN2` VALUES (240, 46, '(41) Loir-et-Cher', '');
INSERT INTO `GeolocalisationN2` VALUES (241, 46, '(45) Loiret', '');
INSERT INTO `GeolocalisationN2` VALUES (242, 48, '(2A) Corse-du-Sud', '');
INSERT INTO `GeolocalisationN2` VALUES (243, 48, '(2B) Haute-Corse', '');
INSERT INTO `GeolocalisationN2` VALUES (244, 49, '(25) Doubs', '');
INSERT INTO `GeolocalisationN2` VALUES (245, 49, '(39) Jura', '');
INSERT INTO `GeolocalisationN2` VALUES (246, 49, '(70) Haute-Saône', '');
INSERT INTO `GeolocalisationN2` VALUES (247, 49, '(90) Territoire-de-Belfort', '');
INSERT INTO `GeolocalisationN2` VALUES (248, 50, '(27) Eure', '');
INSERT INTO `GeolocalisationN2` VALUES (249, 50, '(76) Seine-Maritime', '');
INSERT INTO `GeolocalisationN2` VALUES (250, 51, '(08) Ardennes', '');
INSERT INTO `GeolocalisationN2` VALUES (251, 51, '(10) Aube', '');
INSERT INTO `GeolocalisationN2` VALUES (252, 51, '(51) Marne', '');
INSERT INTO `GeolocalisationN2` VALUES (253, 51, '(52) Haute-Marne', '');
INSERT INTO `GeolocalisationN2` VALUES (254, 52, '(75) Paris', '');
INSERT INTO `GeolocalisationN2` VALUES (255, 52, '(77) Seine-et-Marne', '');
INSERT INTO `GeolocalisationN2` VALUES (256, 52, '(78) Yvelines', '');
INSERT INTO `GeolocalisationN2` VALUES (257, 52, '(91) Essonne', '');
INSERT INTO `GeolocalisationN2` VALUES (258, 52, '(92) Hauts-de-Seine', '');
INSERT INTO `GeolocalisationN2` VALUES (259, 52, '(93) Seine-Saint-Denis', '');
INSERT INTO `GeolocalisationN2` VALUES (260, 52, '(94) Val-de-Marne', '');
INSERT INTO `GeolocalisationN2` VALUES (261, 52, '(95) Val-d''Oise', '');
INSERT INTO `GeolocalisationN2` VALUES (262, 53, '(11) Aude', '');
INSERT INTO `GeolocalisationN2` VALUES (263, 53, '(30) Gard', '');
INSERT INTO `GeolocalisationN2` VALUES (264, 53, '(34) Hérault', '');
INSERT INTO `GeolocalisationN2` VALUES (265, 53, '(48) Lozère', '');
INSERT INTO `GeolocalisationN2` VALUES (266, 53, '(66) Pyrénées-Orientales', '');
INSERT INTO `GeolocalisationN2` VALUES (267, 54, '(19) Corrèze', '');
INSERT INTO `GeolocalisationN2` VALUES (268, 54, '(23) Creuse', '');
INSERT INTO `GeolocalisationN2` VALUES (269, 54, '(87) Haute-Vienne', '');
INSERT INTO `GeolocalisationN2` VALUES (270, 55, '(54) Meurthe-et-Moselle', '');
INSERT INTO `GeolocalisationN2` VALUES (271, 55, '(55) Meuse', '');
INSERT INTO `GeolocalisationN2` VALUES (272, 55, '(57) Moselle', '');
INSERT INTO `GeolocalisationN2` VALUES (273, 55, '(88) Vosges', '');
INSERT INTO `GeolocalisationN2` VALUES (274, 56, '(09) Ariège', '');
INSERT INTO `GeolocalisationN2` VALUES (275, 56, '(12) Aveyron', '');
INSERT INTO `GeolocalisationN2` VALUES (276, 56, '(31) Haute-Garonne', '');
INSERT INTO `GeolocalisationN2` VALUES (277, 56, '(32) Gers', '');
INSERT INTO `GeolocalisationN2` VALUES (278, 56, '(46) Lot', '');
INSERT INTO `GeolocalisationN2` VALUES (279, 56, '(65) Hautes-Pyrénées', '');
INSERT INTO `GeolocalisationN2` VALUES (280, 56, '(81) Tarn', '');
INSERT INTO `GeolocalisationN2` VALUES (281, 56, '(82) Tarn-et-Garonne', '');
INSERT INTO `GeolocalisationN2` VALUES (282, 57, '(59) Nord', '');
INSERT INTO `GeolocalisationN2` VALUES (283, 57, '(62) Pas-de-Calais', '');
INSERT INTO `GeolocalisationN2` VALUES (284, 58, '(44) Loire-Atlantique', '');
INSERT INTO `GeolocalisationN2` VALUES (285, 58, '(49) Maine-et-Loire', '');
INSERT INTO `GeolocalisationN2` VALUES (286, 58, '(53) Mayenne', '');
INSERT INTO `GeolocalisationN2` VALUES (287, 58, '(72) Sarthe', '');
INSERT INTO `GeolocalisationN2` VALUES (288, 58, '(85) Vendée', '');
INSERT INTO `GeolocalisationN2` VALUES (289, 59, '(02) Aisne', '');
INSERT INTO `GeolocalisationN2` VALUES (290, 59, '(60) Oise', '');
INSERT INTO `GeolocalisationN2` VALUES (291, 59, '(80) Somme', '');
INSERT INTO `GeolocalisationN2` VALUES (292, 60, '(16) Charente', '');
INSERT INTO `GeolocalisationN2` VALUES (293, 60, '(17) Charente-Maritime', '');
INSERT INTO `GeolocalisationN2` VALUES (294, 60, '(79) Deux-Sèvres', '');
INSERT INTO `GeolocalisationN2` VALUES (295, 60, '(86) Vienne', '');
INSERT INTO `GeolocalisationN2` VALUES (296, 61, '(04) Alpes-de-Haute-Provence', '');
INSERT INTO `GeolocalisationN2` VALUES (297, 61, '(05) Hautes-Alpes', '');
INSERT INTO `GeolocalisationN2` VALUES (298, 61, '(06) Alpes-Maritimes', '');
INSERT INTO `GeolocalisationN2` VALUES (299, 61, '(13) Bouches-du-Rhône', '');
INSERT INTO `GeolocalisationN2` VALUES (300, 61, '(83) Var', '');
INSERT INTO `GeolocalisationN2` VALUES (301, 61, '(84) Vaucluse', '');
INSERT INTO `GeolocalisationN2` VALUES (302, 62, '(01) Ain', '');
INSERT INTO `GeolocalisationN2` VALUES (303, 62, '(07) Ardèche', '');
INSERT INTO `GeolocalisationN2` VALUES (304, 62, '(26) Drôme', '');
INSERT INTO `GeolocalisationN2` VALUES (305, 62, '(38) Isère', '');
INSERT INTO `GeolocalisationN2` VALUES (306, 62, '(42) Loire', '');
INSERT INTO `GeolocalisationN2` VALUES (307, 62, '(69) Rhône', '');
INSERT INTO `GeolocalisationN2` VALUES (308, 62, '(73) Savoie', '');
INSERT INTO `GeolocalisationN2` VALUES (309, 62, '(74) Haute-Savoie', '');
INSERT INTO `GeolocalisationN2` VALUES (713, 233, 'GUADELOUPE', 'GP');
INSERT INTO `GeolocalisationN2` VALUES (714, 233, 'GUYANE FRANÇAISE', 'GF');
INSERT INTO `GeolocalisationN2` VALUES (715, 233, 'MARTINIQUE', 'MQ');
INSERT INTO `GeolocalisationN2` VALUES (716, 233, 'MAYOTTE', 'YT');
INSERT INTO `GeolocalisationN2` VALUES (717, 233, 'NOUVELLE-CALÉDONIE', 'NC');
INSERT INTO `GeolocalisationN2` VALUES (718, 233, 'POLYNÉSIE FRANÇAISE', 'PF');
INSERT INTO `GeolocalisationN2` VALUES (719, 233, 'RÉUNION', 'RE');
INSERT INTO `GeolocalisationN2` VALUES (720, 233, 'SAINT-BARTHÉLEMY', 'BL');
INSERT INTO `GeolocalisationN2` VALUES (721, 233, 'SAINT-MARTIN', 'MF');
INSERT INTO `GeolocalisationN2` VALUES (722, 233, 'SAINT-PIERRE-ET-MIQUELON', 'PM');
INSERT INTO `GeolocalisationN2` VALUES (723, 233, 'TERRES AUSTRALES FRANÇAISES', 'TF');
INSERT INTO `GeolocalisationN2` VALUES (724, 233, 'WALLIS ET FUTUNA', 'WF');
INSERT INTO `GeolocalisationN2` VALUES (6235, 234, 'AFGHANISTAN', 'AF');
INSERT INTO `GeolocalisationN2` VALUES (6236, 234, 'AFRIQUE DU SUD', 'ZA');
INSERT INTO `GeolocalisationN2` VALUES (6237, 234, 'ÂLAND, ÎLES', 'AX');
INSERT INTO `GeolocalisationN2` VALUES (6238, 234, 'ALBANIE', 'AL');
INSERT INTO `GeolocalisationN2` VALUES (6239, 234, 'ALGÉRIE', 'DZ');
INSERT INTO `GeolocalisationN2` VALUES (6240, 234, 'ALLEMAGNE', 'DE');
INSERT INTO `GeolocalisationN2` VALUES (6241, 234, 'ANDORRE', 'AD');
INSERT INTO `GeolocalisationN2` VALUES (6242, 234, 'ANGOLA', 'AO');
INSERT INTO `GeolocalisationN2` VALUES (6243, 234, 'ANGUILLA', 'AI');
INSERT INTO `GeolocalisationN2` VALUES (6244, 234, 'ANTARCTIQUE', 'AQ');
INSERT INTO `GeolocalisationN2` VALUES (6245, 234, 'ANTIGUA-ET-BARBUDA', 'AG');
INSERT INTO `GeolocalisationN2` VALUES (6246, 234, 'ANTILLES NÉERLANDAISES', 'AN');
INSERT INTO `GeolocalisationN2` VALUES (6247, 234, 'ARABIE SAOUDITE', 'SA');
INSERT INTO `GeolocalisationN2` VALUES (6248, 234, 'ARGENTINE', 'AR');
INSERT INTO `GeolocalisationN2` VALUES (6249, 234, 'ARMÉNIE', 'AM');
INSERT INTO `GeolocalisationN2` VALUES (6250, 234, 'ARUBA', 'AW');
INSERT INTO `GeolocalisationN2` VALUES (6251, 234, 'AUSTRALIE', 'AU');
INSERT INTO `GeolocalisationN2` VALUES (6252, 234, 'AUTRICHE', 'AT');
INSERT INTO `GeolocalisationN2` VALUES (6253, 234, 'AZERBAÏDJAN', 'AZ');
INSERT INTO `GeolocalisationN2` VALUES (6254, 234, 'BAHAMAS', 'BS');
INSERT INTO `GeolocalisationN2` VALUES (6255, 234, 'BAHREÏN', 'BH');
INSERT INTO `GeolocalisationN2` VALUES (6256, 234, 'BANGLADESH', 'BD');
INSERT INTO `GeolocalisationN2` VALUES (6257, 234, 'BARBADE', 'BB');
INSERT INTO `GeolocalisationN2` VALUES (6258, 234, 'BÉLARUS', 'BY');
INSERT INTO `GeolocalisationN2` VALUES (6259, 234, 'BELGIQUE', 'BE');
INSERT INTO `GeolocalisationN2` VALUES (6260, 234, 'BELIZE', 'BZ');
INSERT INTO `GeolocalisationN2` VALUES (6261, 234, 'BÉNIN', 'BJ');
INSERT INTO `GeolocalisationN2` VALUES (6262, 234, 'BERMUDES', 'BM');
INSERT INTO `GeolocalisationN2` VALUES (6263, 234, 'BHOUTAN', 'BT');
INSERT INTO `GeolocalisationN2` VALUES (6264, 234, 'BOLIVIE', 'BO');
INSERT INTO `GeolocalisationN2` VALUES (6265, 234, 'BOSNIE-HERZÉGOVINE', 'BA');
INSERT INTO `GeolocalisationN2` VALUES (6266, 234, 'BOTSWANA', 'BW');
INSERT INTO `GeolocalisationN2` VALUES (6267, 234, 'BOUVET, ÎLE', 'BV');
INSERT INTO `GeolocalisationN2` VALUES (6268, 234, 'BRÉSIL', 'BR');
INSERT INTO `GeolocalisationN2` VALUES (6269, 234, 'BRUNÉI DARUSSALAM', 'BN');
INSERT INTO `GeolocalisationN2` VALUES (6270, 234, 'BULGARIE', 'BG');
INSERT INTO `GeolocalisationN2` VALUES (6271, 234, 'BURKINA FASO', 'BF');
INSERT INTO `GeolocalisationN2` VALUES (6272, 234, 'BURUNDI', 'BI');
INSERT INTO `GeolocalisationN2` VALUES (6273, 234, 'CAÏMANES, ÎLES', 'KY');
INSERT INTO `GeolocalisationN2` VALUES (6274, 234, 'CAMBODGE', 'KH');
INSERT INTO `GeolocalisationN2` VALUES (6275, 234, 'CAMEROUN', 'CM');
INSERT INTO `GeolocalisationN2` VALUES (6276, 234, 'CANADA', 'CA');
INSERT INTO `GeolocalisationN2` VALUES (6277, 234, 'CAP-VERT', 'CV');
INSERT INTO `GeolocalisationN2` VALUES (6278, 234, 'CENTRAFRICAINE, RÉPUBLIQUE', 'CF');
INSERT INTO `GeolocalisationN2` VALUES (6279, 234, 'CHILI', 'CL');
INSERT INTO `GeolocalisationN2` VALUES (6280, 234, 'CHINE', 'CN');
INSERT INTO `GeolocalisationN2` VALUES (6281, 234, 'CHRISTMAS, ÎLE', 'CX');
INSERT INTO `GeolocalisationN2` VALUES (6282, 234, 'CHYPRE', 'CY');
INSERT INTO `GeolocalisationN2` VALUES (6283, 234, 'COCOS (KEELING), ÎLES', 'CC');
INSERT INTO `GeolocalisationN2` VALUES (6284, 234, 'COLOMBIE', 'CO');
INSERT INTO `GeolocalisationN2` VALUES (6285, 234, 'COMORES', 'KM');
INSERT INTO `GeolocalisationN2` VALUES (6286, 234, 'CONGO', 'CG');
INSERT INTO `GeolocalisationN2` VALUES (6287, 234, 'CONGO, LA RÉPUBLIQUE DÉMOCRATIQUE DU', 'CD');
INSERT INTO `GeolocalisationN2` VALUES (6288, 234, 'COOK, ÎLES', 'CK');
INSERT INTO `GeolocalisationN2` VALUES (6289, 234, 'CORÉE, RÉPUBLIQUE DE', 'KR');
INSERT INTO `GeolocalisationN2` VALUES (6290, 234, 'CORÉE, RÉPUBLIQUE POPULAIRE DÉMOCRATIQUE DE', 'KP');
INSERT INTO `GeolocalisationN2` VALUES (6291, 234, 'COSTA RICA', 'CR');
INSERT INTO `GeolocalisationN2` VALUES (6292, 234, 'CÔTE D''IVOIRE', 'CI');
INSERT INTO `GeolocalisationN2` VALUES (6293, 234, 'CROATIE', 'HR');
INSERT INTO `GeolocalisationN2` VALUES (6294, 234, 'CUBA', 'CU');
INSERT INTO `GeolocalisationN2` VALUES (6295, 234, 'DANEMARK', 'DK');
INSERT INTO `GeolocalisationN2` VALUES (6296, 234, 'DJIBOUTI', 'DJ');
INSERT INTO `GeolocalisationN2` VALUES (6297, 234, 'DOMINICAINE, RÉPUBLIQUE', 'DO');
INSERT INTO `GeolocalisationN2` VALUES (6298, 234, 'DOMINIQUE', 'DM');
INSERT INTO `GeolocalisationN2` VALUES (6299, 234, 'ÉGYPTE', 'EG');
INSERT INTO `GeolocalisationN2` VALUES (6300, 234, 'EL SALVADOR', 'SV');
INSERT INTO `GeolocalisationN2` VALUES (6301, 234, 'ÉMIRATS ARABES UNIS', 'AE');
INSERT INTO `GeolocalisationN2` VALUES (6302, 234, 'ÉQUATEUR', 'EC');
INSERT INTO `GeolocalisationN2` VALUES (6303, 234, 'ÉRYTHRÉE', 'ER');
INSERT INTO `GeolocalisationN2` VALUES (6304, 234, 'ESPAGNE', 'ES');
INSERT INTO `GeolocalisationN2` VALUES (6305, 234, 'ESTONIE', 'EE');
INSERT INTO `GeolocalisationN2` VALUES (6306, 234, 'ÉTATS-UNIS', 'US');
INSERT INTO `GeolocalisationN2` VALUES (6307, 234, 'ÉTHIOPIE', 'ET');
INSERT INTO `GeolocalisationN2` VALUES (6308, 234, 'FALKLAND, ÎLES (MALVINAS)', 'FK');
INSERT INTO `GeolocalisationN2` VALUES (6309, 234, 'FÉROÉ, ÎLES', 'FO');
INSERT INTO `GeolocalisationN2` VALUES (6310, 234, 'FIDJI', 'FJ');
INSERT INTO `GeolocalisationN2` VALUES (6311, 234, 'FINLANDE', 'FI');
INSERT INTO `GeolocalisationN2` VALUES (6313, 234, 'GABON', 'GA');
INSERT INTO `GeolocalisationN2` VALUES (6314, 234, 'GAMBIE', 'GM');
INSERT INTO `GeolocalisationN2` VALUES (6315, 234, 'GÉORGIE', 'GE');
INSERT INTO `GeolocalisationN2` VALUES (6316, 234, 'GÉORGIE DU SUD ET LES ÎLES SANDWICH DU SUD', 'GS');
INSERT INTO `GeolocalisationN2` VALUES (6317, 234, 'GHANA', 'GH');
INSERT INTO `GeolocalisationN2` VALUES (6318, 234, 'GIBRALTAR', 'GI');
INSERT INTO `GeolocalisationN2` VALUES (6319, 234, 'GRÈCE', 'GR');
INSERT INTO `GeolocalisationN2` VALUES (6320, 234, 'GRENADE', 'GD');
INSERT INTO `GeolocalisationN2` VALUES (6321, 234, 'GROENLAND', 'GL');
INSERT INTO `GeolocalisationN2` VALUES (6322, 234, 'GUAM', 'GU');
INSERT INTO `GeolocalisationN2` VALUES (6323, 234, 'GUATEMALA', 'GT');
INSERT INTO `GeolocalisationN2` VALUES (6324, 234, 'GUERNESEY', 'GG');
INSERT INTO `GeolocalisationN2` VALUES (6325, 234, 'GUINÉE', 'GN');
INSERT INTO `GeolocalisationN2` VALUES (6326, 234, 'GUINÉE-BISSAU', 'GW');
INSERT INTO `GeolocalisationN2` VALUES (6327, 234, 'GUINÉE ÉQUATORIALE', 'GQ');
INSERT INTO `GeolocalisationN2` VALUES (6328, 234, 'GUYANA', 'GY');
INSERT INTO `GeolocalisationN2` VALUES (6329, 234, 'HAÏTI', 'HT');
INSERT INTO `GeolocalisationN2` VALUES (6330, 234, 'HEARD, ÎLE ET MCDONALD, ÎLES', 'HM');
INSERT INTO `GeolocalisationN2` VALUES (6331, 234, 'HONDURAS', 'HN');
INSERT INTO `GeolocalisationN2` VALUES (6332, 234, 'HONG-KONG', 'HK');
INSERT INTO `GeolocalisationN2` VALUES (6333, 234, 'HONGRIE', 'HU');
INSERT INTO `GeolocalisationN2` VALUES (6334, 234, 'ÎLE DE MAN', 'IM');
INSERT INTO `GeolocalisationN2` VALUES (6335, 234, 'ÎLES MINEURES ÉLOIGNÉES DES ÉTATS-UNIS', 'UM');
INSERT INTO `GeolocalisationN2` VALUES (6336, 234, 'ÎLES VIERGES BRITANNIQUES', 'VG');
INSERT INTO `GeolocalisationN2` VALUES (6337, 234, 'ÎLES VIERGES DES ÉTATS-UNIS', 'VI');
INSERT INTO `GeolocalisationN2` VALUES (6338, 234, 'INDE', 'IN');
INSERT INTO `GeolocalisationN2` VALUES (6339, 234, 'INDONÉSIE', 'ID');
INSERT INTO `GeolocalisationN2` VALUES (6340, 234, 'IRAN, RÉPUBLIQUE ISLAMIQUE D''', 'IR');
INSERT INTO `GeolocalisationN2` VALUES (6341, 234, 'IRAQ', 'IQ');
INSERT INTO `GeolocalisationN2` VALUES (6342, 234, 'IRLANDE', 'IE');
INSERT INTO `GeolocalisationN2` VALUES (6343, 234, 'ISLANDE', 'IS');
INSERT INTO `GeolocalisationN2` VALUES (6344, 234, 'ISRAËL', 'IL');
INSERT INTO `GeolocalisationN2` VALUES (6345, 234, 'ITALIE', 'IT');
INSERT INTO `GeolocalisationN2` VALUES (6346, 234, 'JAMAÏQUE', 'JM');
INSERT INTO `GeolocalisationN2` VALUES (6347, 234, 'JAPON', 'JP');
INSERT INTO `GeolocalisationN2` VALUES (6348, 234, 'JERSEY', 'JE');
INSERT INTO `GeolocalisationN2` VALUES (6349, 234, 'JORDANIE', 'JO');
INSERT INTO `GeolocalisationN2` VALUES (6350, 234, 'KAZAKHSTAN', 'KZ');
INSERT INTO `GeolocalisationN2` VALUES (6351, 234, 'KENYA', 'KE');
INSERT INTO `GeolocalisationN2` VALUES (6352, 234, 'KIRGHIZISTAN', 'KG');
INSERT INTO `GeolocalisationN2` VALUES (6353, 234, 'KIRIBATI', 'KI');
INSERT INTO `GeolocalisationN2` VALUES (6354, 234, 'KOWEÏT', 'KW');
INSERT INTO `GeolocalisationN2` VALUES (6355, 234, 'LAO, RÉPUBLIQUE DÉMOCRATIQUE POPULAIRE', 'LA');
INSERT INTO `GeolocalisationN2` VALUES (6356, 234, 'LESOTHO', 'LS');
INSERT INTO `GeolocalisationN2` VALUES (6357, 234, 'LETTONIE', 'LV');
INSERT INTO `GeolocalisationN2` VALUES (6358, 234, 'LIBAN', 'LB');
INSERT INTO `GeolocalisationN2` VALUES (6359, 234, 'LIBÉRIA', 'LR');
INSERT INTO `GeolocalisationN2` VALUES (6360, 234, 'LIBYENNE, JAMAHIRIYA ARABE', 'LY');
INSERT INTO `GeolocalisationN2` VALUES (6361, 234, 'LIECHTENSTEIN', 'LI');
INSERT INTO `GeolocalisationN2` VALUES (6362, 234, 'LITUANIE', 'LT');
INSERT INTO `GeolocalisationN2` VALUES (6363, 234, 'LUXEMBOURG', 'LU');
INSERT INTO `GeolocalisationN2` VALUES (6364, 234, 'MACAO', 'MO');
INSERT INTO `GeolocalisationN2` VALUES (6365, 234, 'MACÉDOINE, L''EX-RÉPUBLIQUE YOUGOSLAVE DE', 'MK');
INSERT INTO `GeolocalisationN2` VALUES (6366, 234, 'MADAGASCAR', 'MG');
INSERT INTO `GeolocalisationN2` VALUES (6367, 234, 'MALAISIE', 'MY');
INSERT INTO `GeolocalisationN2` VALUES (6368, 234, 'MALAWI', 'MW');
INSERT INTO `GeolocalisationN2` VALUES (6369, 234, 'MALDIVES', 'MV');
INSERT INTO `GeolocalisationN2` VALUES (6370, 234, 'MALI', 'ML');
INSERT INTO `GeolocalisationN2` VALUES (6371, 234, 'MALTE', 'MT');
INSERT INTO `GeolocalisationN2` VALUES (6372, 234, 'MARIANNES DU NORD, ÎLES', 'MP');
INSERT INTO `GeolocalisationN2` VALUES (6373, 234, 'MAROC', 'MA');
INSERT INTO `GeolocalisationN2` VALUES (6374, 234, 'MARSHALL, ÎLES', 'MH');
INSERT INTO `GeolocalisationN2` VALUES (6375, 234, 'MAURICE', 'MU');
INSERT INTO `GeolocalisationN2` VALUES (6376, 234, 'MAURITANIE', 'MR');
INSERT INTO `GeolocalisationN2` VALUES (6377, 234, 'MEXIQUE', 'MX');
INSERT INTO `GeolocalisationN2` VALUES (6378, 234, 'MICRONÉSIE, ÉTATS FÉDÉRÉS DE', 'FM');
INSERT INTO `GeolocalisationN2` VALUES (6379, 234, 'MOLDOVA, RÉPUBLIQUE DE', 'MD');
INSERT INTO `GeolocalisationN2` VALUES (6380, 234, 'MONACO', 'MC');
INSERT INTO `GeolocalisationN2` VALUES (6381, 234, 'MONGOLIE', 'MN');
INSERT INTO `GeolocalisationN2` VALUES (6382, 234, 'MONTÉNÉGRO', 'ME');
INSERT INTO `GeolocalisationN2` VALUES (6383, 234, 'MONTSERRAT', 'MS');
INSERT INTO `GeolocalisationN2` VALUES (6384, 234, 'MOZAMBIQUE', 'MZ');
INSERT INTO `GeolocalisationN2` VALUES (6385, 234, 'MYANMAR', 'MM');
INSERT INTO `GeolocalisationN2` VALUES (6386, 234, 'NAMIBIE', 'NA');
INSERT INTO `GeolocalisationN2` VALUES (6387, 234, 'NAURU', 'NR');
INSERT INTO `GeolocalisationN2` VALUES (6388, 234, 'NÉPAL', 'NP');
INSERT INTO `GeolocalisationN2` VALUES (6389, 234, 'NICARAGUA', 'NI');
INSERT INTO `GeolocalisationN2` VALUES (6390, 234, 'NIGER', 'NE');
INSERT INTO `GeolocalisationN2` VALUES (6391, 234, 'NIGÉRIA', 'NG');
INSERT INTO `GeolocalisationN2` VALUES (6392, 234, 'NIUÉ', 'NU');
INSERT INTO `GeolocalisationN2` VALUES (6393, 234, 'NORFOLK, ÎLE', 'NF');
INSERT INTO `GeolocalisationN2` VALUES (6394, 234, 'NORVÈGE', 'NO');
INSERT INTO `GeolocalisationN2` VALUES (6395, 234, 'NOUVELLE-ZÉLANDE', 'NZ');
INSERT INTO `GeolocalisationN2` VALUES (6396, 234, 'OCÉAN INDIEN, TERRITOIRE BRITANNIQUE DE L''', 'IO');
INSERT INTO `GeolocalisationN2` VALUES (6397, 234, 'OMAN', 'OM');
INSERT INTO `GeolocalisationN2` VALUES (6398, 234, 'OUGANDA', 'UG');
INSERT INTO `GeolocalisationN2` VALUES (6399, 234, 'OUZBÉKISTAN', 'UZ');
INSERT INTO `GeolocalisationN2` VALUES (6400, 234, 'PAKISTAN', 'PK');
INSERT INTO `GeolocalisationN2` VALUES (6401, 234, 'PALAOS', 'PW');
INSERT INTO `GeolocalisationN2` VALUES (6402, 234, 'PALESTINIEN OCCUPÉ, TERRITOIRE', 'PS');
INSERT INTO `GeolocalisationN2` VALUES (6403, 234, 'PANAMA', 'PA');
INSERT INTO `GeolocalisationN2` VALUES (6404, 234, 'PAPOUASIE-NOUVELLE-GUINÉE', 'PG');
INSERT INTO `GeolocalisationN2` VALUES (6405, 234, 'PARAGUAY', 'PY');
INSERT INTO `GeolocalisationN2` VALUES (6406, 234, 'PAYS-BAS', 'NL');
INSERT INTO `GeolocalisationN2` VALUES (6407, 234, 'PÉROU', 'PE');
INSERT INTO `GeolocalisationN2` VALUES (6408, 234, 'PHILIPPINES', 'PH');
INSERT INTO `GeolocalisationN2` VALUES (6409, 234, 'PITCAIRN', 'PN');
INSERT INTO `GeolocalisationN2` VALUES (6410, 234, 'POLOGNE', 'PL');
INSERT INTO `GeolocalisationN2` VALUES (6411, 234, 'PORTO RICO', 'PR');
INSERT INTO `GeolocalisationN2` VALUES (6412, 234, 'PORTUGAL', 'PT');
INSERT INTO `GeolocalisationN2` VALUES (6413, 234, 'QATAR', 'QA');
INSERT INTO `GeolocalisationN2` VALUES (6414, 234, 'ROUMANIE', 'RO');
INSERT INTO `GeolocalisationN2` VALUES (6415, 234, 'ROYAUME-UNI', 'GB');
INSERT INTO `GeolocalisationN2` VALUES (6416, 234, 'RUSSIE, FÉDÉRATION DE', 'RU');
INSERT INTO `GeolocalisationN2` VALUES (6417, 234, 'RWANDA', 'RW');
INSERT INTO `GeolocalisationN2` VALUES (6418, 234, 'SAHARA OCCIDENTAL', 'EH');
INSERT INTO `GeolocalisationN2` VALUES (6419, 234, 'SAINTE-HÉLÈNE', 'SH');
INSERT INTO `GeolocalisationN2` VALUES (6420, 234, 'SAINTE-LUCIE', 'LC');
INSERT INTO `GeolocalisationN2` VALUES (6421, 234, 'SAINT-KITTS-ET-NEVIS', 'KN');
INSERT INTO `GeolocalisationN2` VALUES (6422, 234, 'SAINT-MARIN', 'SM');
INSERT INTO `GeolocalisationN2` VALUES (6423, 234, 'SAINT-SIÈGE (ÉTAT DE LA CITÉ DU VATICAN)', 'VA');
INSERT INTO `GeolocalisationN2` VALUES (6424, 234, 'SAINT-VINCENT-ET-LES GRENADINES', 'VC');
INSERT INTO `GeolocalisationN2` VALUES (6425, 234, 'SALOMON, ÎLES', 'SB');
INSERT INTO `GeolocalisationN2` VALUES (6426, 234, 'SAMOA', 'WS');
INSERT INTO `GeolocalisationN2` VALUES (6427, 234, 'SAMOA AMÉRICAINES', 'AS');
INSERT INTO `GeolocalisationN2` VALUES (6428, 234, 'SAO TOMÉ-ET-PRINCIPE', 'ST');
INSERT INTO `GeolocalisationN2` VALUES (6429, 234, 'SÉNÉGAL', 'SN');
INSERT INTO `GeolocalisationN2` VALUES (6430, 234, 'SERBIE', 'RS');
INSERT INTO `GeolocalisationN2` VALUES (6431, 234, 'SEYCHELLES', 'SC');
INSERT INTO `GeolocalisationN2` VALUES (6432, 234, 'SIERRA LEONE', 'SL');
INSERT INTO `GeolocalisationN2` VALUES (6433, 234, 'SINGAPOUR', 'SG');
INSERT INTO `GeolocalisationN2` VALUES (6434, 234, 'SLOVAQUIE', 'SK');
INSERT INTO `GeolocalisationN2` VALUES (6435, 234, 'SLOVÉNIE', 'SI');
INSERT INTO `GeolocalisationN2` VALUES (6436, 234, 'SOMALIE', 'SO');
INSERT INTO `GeolocalisationN2` VALUES (6437, 234, 'SOUDAN', 'SD');
INSERT INTO `GeolocalisationN2` VALUES (6438, 234, 'SRI LANKA', 'LK');
INSERT INTO `GeolocalisationN2` VALUES (6439, 234, 'SUÈDE', 'SE');
INSERT INTO `GeolocalisationN2` VALUES (6440, 234, 'SUISSE', 'CH');
INSERT INTO `GeolocalisationN2` VALUES (6441, 234, 'SURINAME', 'SR');
INSERT INTO `GeolocalisationN2` VALUES (6442, 234, 'SVALBARD ET ÎLE JAN MAYEN', 'SJ');
INSERT INTO `GeolocalisationN2` VALUES (6443, 234, 'SWAZILAND', 'SZ');
INSERT INTO `GeolocalisationN2` VALUES (6444, 234, 'SYRIENNE, RÉPUBLIQUE ARABE', 'SY');
INSERT INTO `GeolocalisationN2` VALUES (6445, 234, 'TADJIKISTAN', 'TJ');
INSERT INTO `GeolocalisationN2` VALUES (6446, 234, 'TAÏWAN, PROVINCE DE CHINE', 'TW');
INSERT INTO `GeolocalisationN2` VALUES (6447, 234, 'TANZANIE, RÉPUBLIQUE-UNIE DE', 'TZ');
INSERT INTO `GeolocalisationN2` VALUES (6448, 234, 'TCHAD', 'TD');
INSERT INTO `GeolocalisationN2` VALUES (6449, 234, 'TCHÈQUE, RÉPUBLIQUE', 'CZ');
INSERT INTO `GeolocalisationN2` VALUES (6450, 234, 'THAÏLANDE', 'TH');
INSERT INTO `GeolocalisationN2` VALUES (6451, 234, 'TIMOR-LESTE', 'TL');
INSERT INTO `GeolocalisationN2` VALUES (6452, 234, 'TOGO', 'TG');
INSERT INTO `GeolocalisationN2` VALUES (6453, 234, 'TOKELAU', 'TK');
INSERT INTO `GeolocalisationN2` VALUES (6454, 234, 'TONGA', 'TO');
INSERT INTO `GeolocalisationN2` VALUES (6455, 234, 'TRINITÉ-ET-TOBAGO', 'TT');
INSERT INTO `GeolocalisationN2` VALUES (6456, 234, 'TUNISIE', 'TN');
INSERT INTO `GeolocalisationN2` VALUES (6457, 234, 'TURKMÉNISTAN', 'TM');
INSERT INTO `GeolocalisationN2` VALUES (6458, 234, 'TURKS ET CAÏQUES, ÎLES', 'TC');
INSERT INTO `GeolocalisationN2` VALUES (6459, 234, 'TURQUIE', 'TR');
INSERT INTO `GeolocalisationN2` VALUES (6460, 234, 'TUVALU', 'TV');
INSERT INTO `GeolocalisationN2` VALUES (6461, 234, 'UKRAINE', 'UA');
INSERT INTO `GeolocalisationN2` VALUES (6462, 234, 'URUGUAY', 'UY');
INSERT INTO `GeolocalisationN2` VALUES (6463, 234, 'VANUATU', 'VU');
INSERT INTO `GeolocalisationN2` VALUES (6464, 234, 'VENEZUELA', 'VE');
INSERT INTO `GeolocalisationN2` VALUES (6465, 234, 'VIET NAM', 'VN');
INSERT INTO `GeolocalisationN2` VALUES (6466, 234, 'YÉMEN', 'YE');
INSERT INTO `GeolocalisationN2` VALUES (6467, 234, 'ZAMBIE', 'ZM');
INSERT INTO `GeolocalisationN2` VALUES (6468, 234, 'ZIMBABWE', 'ZW');



--26/05/2008 MMI 

ALTER TABLE `GeolocalisationN2` ADD INDEX ( `id_geolocalisationN1` ) ;
ALTER TABLE `GeolocalisationN2` ADD INDEX ( `id_geolocalisationN1` ) ;

-- 27/05/2008

ALTER TABLE `Inscrit` CHANGE `siret` `siret` VARCHAR( 5 ) NOT NULL ;

--  27/05/2008 MMI:

INSERT INTO `GeolocalisationN1` ( `id` , `id_geolocalisationN0` , `denomination1` , `denomination2` ) 
VALUES (
NULL , '3', 'Pays', ''
);


ALTER TABLE `HabilitationAgent` ADD `ouvrir_candidature_en_ligne` ENUM( '0', '1' ) NOT NULL DEFAULT '0' ;
ALTER TABLE `HabilitationAgent` ADD `ouvrir_candidature_a_distance` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationAgent` ADD `refuser_enveloppe` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationAgent` ADD `gerer_admissibilite` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `refuser_enveloppe`;
ALTER TABLE `HabilitationAgent` ADD `restaurer_enveloppe` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `gerer_admissibilite`;
ALTER TABLE `HabilitationAgent` ADD `ouvrir_anonymat_en_ligne` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `HabilitationAgent` ADD `ouvrir_offre_en_ligne` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- 02-06-2008 AZA : ajout du nombre de telechargement du DCE dans la table consultation
ALTER TABLE `consultation` ADD `nbr_telechargement_dce` INT( 5 ) NULL ;

ALTER TABLE `ModuleCommun` ADD `gestion_boamp_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `gestion_bi_cle_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationAgent` ADD `gestion_compte_boamp` ENUM( '0', '1' ) NOT NULL ;

ALTER TABLE `HabilitationProfil` ADD  `ouvrir_candidature_en_ligne` enum( '0', '1' ) NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `ouvrir_candidature_a_distance` enum( '0', '1' ) NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `refuser_enveloppe` enum( '0', '1' ) NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `gerer_admissibilite` enum( '0', '1' ) NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `restaurer_enveloppe` enum( '0', '1' ) NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `ouvrir_offre_en_ligne` enum( '0', '1' ) NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `ouvrir_anonymat_en_ligne` enum( '0', '1' ) NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `gestion_compte_boamp` enum( '0', '1' ) NOT NULL default '0';

ALTER TABLE `consultation` CHANGE `datevalidation` `datevalidation` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';

CREATE TABLE `ServicesMetiers` (
  `id` int(5) NOT NULL default '0',
  `code` varchar(25) NOT NULL default '',
  `description` varchar(75) NOT NULL default '',
  `active` enum('1','0') NOT NULL default '1',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

INSERT INTO `ServicesMetiers` (`id`, `code`, `description`, `active`) VALUES 
(0, 'PMI', 'Place de Marché Interministérielle', '1');


-- 08/06/2008 MMI :
ALTER TABLE `consultation` ADD `service_id` INT( 11 ) NULL ,
ADD `service_associe_id` INT( 11 ) NULL ;

-- 09/06/2008 YSB :
ALTER TABLE `Organisme` ADD `sigle` VARCHAR( 100 ) NOT NULL ;
ALTER TABLE `Organisme` ADD `adresse2` VARCHAR( 100 ) NOT NULL;
ALTER TABLE `Organisme` ADD `tel` VARCHAR( 50 ) NOT NULL;
ALTER TABLE `Organisme` ADD `telecopie` VARCHAR( 50 ) NOT NULL;

ALTER TABLE `ModuleCommun` ADD `nom_entreprise_toujours_visible` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationAgent` ADD `gestion_agents` enum('0','1') NOT NULL default '0';
ALTER TABLE `HabilitationAgent` ADD `gestion_habilitations` enum('0','1') NOT NULL default '0';

ALTER TABLE `HabilitationAgent` ADD `gerer_mapa_inferieur_montant` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `gerer_mapa_superieur_montant` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `modifier_consultation_avant_validation` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `modifier_consultation_apres_validation` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `acces_reponses` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `telechargement_groupe_anticipe_plis_chiffres` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `telechargement_unitaire_plis_chiffres` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `ouvrir_offre_a_distance` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `creer_annonce_information` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `saisie_marches` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `validation_marches` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `publication_marches` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `gerer_statistiques_metier` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `gerer_archives` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `gerer_courrier_entreprise` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `administrer_procedures_formalisees` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationAgent` ADD `creer_annonce_attribution` ENUM( '0', '1' ) NOT NULL ;

ALTER TABLE `HabilitationProfil` ADD `gestion_agents` enum('0','1') NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `gestion_habilitations` enum('0','1') NOT NULL default '0';

ALTER TABLE `HabilitationProfil` ADD `gerer_mapa_inferieur_montant` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `gerer_mapa_superieur_montant` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `modifier_consultation_avant_validation` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `modifier_consultation_apres_validation` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `acces_reponses` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `telechargement_groupe_anticipe_plis_chiffres` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `telechargement_unitaire_plis_chiffres` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `ouvrir_offre_a_distance` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `creer_annonce_information` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `saisie_marches` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `validation_marches` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `publication_marches` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `gerer_statistiques_metier` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `gerer_archives` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `gerer_courrier_entreprise` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `administrer_procedures_formalisees` ENUM( '0', '1' ) NOT NULL ;
ALTER TABLE `HabilitationProfil` ADD `creer_annonce_attribution` ENUM( '0', '1' ) NOT NULL ;


ALTER TABLE `HabilitationAgent` ADD `acces_registre_retraits_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationAgent` ADD `acces_registre_questions_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationAgent` ADD `acces_registre_depots_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `HabilitationProfil` ADD `acces_registre_retraits_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationProfil` ADD `acces_registre_questions_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationProfil` ADD `acces_registre_depots_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `Entreprise` ADD `acronyme_pays` VARCHAR( 10 ) NULL ;
INSERT INTO `GeolocalisationN2` ( `id` , `id_geolocalisationN1` , `denomination1` , `denomination2` ) VALUES (6469 , '234', 'FRANCE', 'FR');

ALTER TABLE `HabilitationAgent` ADD `validation_simple` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationAgent` ADD `validation_intermediaire` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationAgent` ADD `validation_finale` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `HabilitationProfil` ADD `validation_simple` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationProfil` ADD `validation_intermediaire` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationProfil` ADD `validation_finale` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `ModuleOrganisme` ADD `organisation_centralisee` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `Organisme` ADD `pays` VARCHAR( 150 ) NULL ;
ALTER TABLE `consultation` ADD `detail_consultation` VARCHAR( 100 ) NOT NULL ;
ALTER TABLE `consultation` ADD `date_fin_affichage` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `HabilitationAgent` ADD `creer_suite_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationProfil` ADD `creer_suite_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `consultation` ADD `depouillable_phase_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

--08/07/2008 :MMI

ALTER TABLE `Inscrit` DROP `xmlAlert` ;

CREATE TABLE `Alerte` (
  `id` int(11) NOT NULL auto_increment,
  `id_inscrit` int(11) NOT NULL default '0',
  `denomination` varchar(200) NOT NULL default '',
  `periodicite` char(1) NOT NULL default '',
  `xmlCriteria` text,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB AUTO_INCREMENT=8 ;


ALTER TABLE `Alerte` ADD `xmlCriteria` TEXT NULL ;
ALTER TABLE `ModuleCommun` CHANGE `authentication_without_certificate` `authentication_without_certificate` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `Alerte` ADD `periodicite` CHAR( 1 ) NOT NULL ;

INSERT INTO `EtatConsultation` VALUES (5, 'A archiver', 'AA', '1');  
INSERT INTO `EtatConsultation` VALUES (6, 'Archive r&eacute;alis&eacute;e', 'AR', '1');


--10/07/2008 :mmi

ALTER TABLE `consultation` ADD `consultation_transverse` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `TypeProcedure` ADD `consultation_transverse` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

CREATE TABLE `InvitationConsultationTransverse` (
`organise_emetteur` VARCHAR( 30 ) NOT NULL ,
`reference` VARCHAR( 255 ) NOT NULL ,
`organisme_invite` VARCHAR( 30 ) NOT NULL ,
`lot` INT( 11 ) NOT NULL 
) TYPE = innodb;

ALTER TABLE `InvitationConsultationTransverse` ADD `date_decision` DATE NULL ;

--10/07/2008 : AZA Gestion des agents administrateur
ALTER TABLE `HabilitationAgent` ADD `hyper_admin` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `hyper_admin` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `HabilitationAgent` ADD `droit_gestion_services` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `droit_gestion_services` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `HabilitationAgent` ADD `suivi_acces` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `suivi_acces` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- 
-- Contenu de la table `HabilitationProfil`
-- 


--11/07/2008: AZA Gestion des alertes
ALTER TABLE `Agent` ADD `alerte_reponse_electronique` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `Agent` ADD `alerte_cloture_consultation` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `Agent` ADD `alerte_reception_message` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `Agent` ADD `alerte_publication_boamp` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `Agent` ADD `alerte_echec_publication_boamp` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `Agent` ADD `alerte_creation_modification_agent` ENUM ('0' , '1')  NOT NULL default '0';

ALTER TABLE `InvitationConsultationTransverse` ADD `date_decision` DATE NULL ;
ALTER TABLE `InvitationConsultationTransverse` CHANGE `organise_emetteur` `organisme_emetteur` VARCHAR( 30 ) NOT NULL;

ALTER TABLE `InvitationConsultationTransverse` ADD `id` INT( 11 ) NOT NULL FIRST ;
ALTER TABLE `InvitationConsultationTransverse` ADD PRIMARY KEY ( `id` ) ;
ALTER TABLE `InvitationConsultationTransverse` CHANGE `id` `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;

UPDATE `HabilitationProfil` SET `gestion_agent_pole` = '0' WHERE `id` IN (1, 2, 3, 4, 5);

--AZA : 16/07/2008 Suivi d'accès
-- 
-- Structure de la table `SuiviAcces`
-- 

CREATE TABLE `SuiviAcces` (
  `id_agent` int(8) NOT NULL default '0',
  `date_acces` varchar(20) NOT NULL default '',
  `id_service` int(8) default NULL,
  `nom` varchar(100) default NULL,
  `prenom` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `organisme` varchar(100) default NULL
) TYPE=InnoDB;

ALTER TABLE `ModuleCommun` ADD `gestion_jal_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `Inscrit` ADD `bloque` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
-- 18/07/2008 :MMI

-- 
-- Structure de la table `InscritHistorique`
-- 

CREATE TABLE `InscritHistorique` (
  `id` int(11) NOT NULL auto_increment,
  `entreprise_id` int(11) NOT NULL default '0',
  `inscrit1` varchar(64) NOT NULL default '',
 `mail1` varchar(64) NOT NULL default '',
  `inscrit2` varchar(64) NOT NULL default '',
`mail2` varchar(64) NOT NULL default '',
  `date` varchar(32) NOT NULL default '',
  `action` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `entreprise_id` (`entreprise_id`)
) TYPE=MyISAM COMMENT='Historiques des actions ' AUTO_INCREMENT=100 ;

ALTER TABLE `consultation` ADD `consultation_achat_publique` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `url_consultation_achat_publique` VARCHAR( 100 ) NULL ;

ALTER TABLE `Offre` CHANGE `untrusteddate` `untrusteddate` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `HabilitationAgent` ADD `statistiques_site` ENUM ('0', '1')  NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `statistiques_site` ENUM ('0', '1')  NOT NULL default '0';

ALTER TABLE `consultation` ADD `partial_dce_download` CHAR( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `tirage_plan` INT( 11 ) NOT NULL DEFAULT '0';

ALTER TABLE `Organisme` ADD `affichage_entite` CHAR( 1 ) NOT NULL ;

ALTER TABLE `HabilitationAgent` ADD `statistiques_QoS` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `statistiques_QoS` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `consultation` ADD `tireur_plan` INT( 11 ) NOT NULL ;

ALTER TABLE `Telechargement` ADD `id_inscrit` INT( 11 ) DEFAULT '0',ADD `id_entreprise` INT( 11 ) DEFAULT '0';
ALTER TABLE `QuestionDCE` ADD `id_inscrit` INT( 11 ) NOT NULL ,ADD `id_entreprise` INT( 11 ) NOT NULL ;
ALTER TABLE `Alerte` ADD `categorie` VARCHAR( 30 ) NULL ;
ALTER TABLE `consultation` ADD `date_mise_en_ligne_calcule` DATETIME NULL;

REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (1, 'Administrateur Ministériel', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (2, 'Administrateur entité d''achat', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (3, 'Rédacteur', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '1', '0', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', '0', '0', '0');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (4, 'Acheteur', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (5, 'Administrateur / Acheteur', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (6, 'Acheteur MAPA < 90 kEUR', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1', '1', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (7, 'Acheteur MAPA &#8805; 90 kEUR', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (8, 'Observateur', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (9, 'Officier des Retraîts et des Dépôts', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (10, 'RPA', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
REPLACE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `gerer_courrier_entreprise`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`) VALUES (11, 'Président de CAO', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');


ALTER TABLE `consultation` ADD `accessibilite_en` CHAR( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_es` CHAR( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `nbr_reponse` INT( 5 ) NULL ;
ALTER TABLE `consultation` ADD `id_type_procedure_org` INT( 1 ) NOT NULL DEFAULT '0';

UPDATE `TypeAvis` SET `intitule_avis_en` = 'en Annonce d''information ',
`intitule_avis_es` = 'es Annonce d''information ' WHERE `TypeAvis`.`id` =2;

UPDATE `TypeAvis` SET `intitule_avis_en` = 'en Annonce de consultation ',
`intitule_avis_es` = 'es Annonce de consultation' WHERE `TypeAvis`.`id` =3;

UPDATE `TypeAvis` SET `intitule_avis_en` = 'en Annonce d''attribution ',
`intitule_avis_es` = 'es Annonce d''attribution ' WHERE `TypeAvis`.`id` =4;


ALTER TABLE `Telechargement` ADD `support` INT( 11 ) DEFAULT '0';
ALTER TABLE `TypeProcedure` ADD `code_recensement` VARCHAR( 3 ) NULL ;

ALTER TABLE `SuiviAcces` ADD `email` varchar(100) default NULL;
ALTER TABLE `SuiviAcces` ADD  `organisme` varchar(100) default NULL;

ALTER TABLE `HabilitationAgent` ADD `ouvrir_anonymat_a_distance` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `ouvrir_anonymat_a_distance` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `organisme_consultation_init` VARCHAR( 255 ) NOT NULL ;

ALTER TABLE `consultation` ADD `date_validation_intermediaire` VARCHAR( 20 ) NULL ;

CREATE TABLE `Historique_suppression_agent` (
  `id` int(11) NOT NULL auto_increment,
  `id_agent_suppresseur` int(8) NOT NULL default '0',
  `id_service` int(8) default NULL,
  `nom` varchar(100) default NULL,
  `prenom` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `organisme` varchar(100) default NULL,
  `date_suppression` varchar(20) NOT NULL default '',
  `id_agent_supprime` int(8) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

ALTER TABLE `HabilitationAgent` ADD `gestion_compte_jal` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `gestion_compte_jal` ENUM ('0' , '1')  NOT NULL default '0';

CREATE TABLE `Historique_suppression_agent` (
  `id` int(11) NOT NULL auto_increment,
  `id_agent_suppresseur` int(8) NOT NULL default '0',
  `id_service` int(8) default NULL,
  `nom` varchar(100) default NULL,
  `prenom` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `organisme` varchar(100) default NULL,
  `date_suppression` varchar(20) NOT NULL default '',
  `id_agent_supprime` int(8) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

ALTER TABLE `ModuleOrganisme` ADD `presence_elu` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

CREATE TABLE `EntrepriseInsee` (
  `id` int(30) NOT NULL auto_increment,
  `siren` varchar(9) default NULL,
  `raisonSociale` varchar(200) default NULL,
  `nbEtA` int(1) default NULL,
  `etat` char(1) default NULL,
  `etatLib` varchar(40) default NULL,
  `etatDebDate` varchar(20) default NULL,
  `eff3112Tr` int(11) default NULL,
  `eff3112TrLib` varchar(200) default NULL,
  `effAn` varchar(10) default NULL,
  `apen` varchar(5) default NULL,
  `apenLib` varchar(200) default NULL,
  `cj` varchar(4) default NULL,
  `cjLib` varchar(200) default NULL,
  `indNDC` char(1) default NULL,
  `indDoublon` char(1) default NULL,
  `indPurge` char(1) default NULL,
  `nicSiege` varchar(5) default '00000',
  `etatSiege` char(1) default NULL,
  `etatSiegeLib` varchar(10) default NULL,
  `etatDebDateSiege` varchar(20) default NULL,
  `eff3112TrSiege` int(11) default NULL,
  `eff3112TrSiegeLib` varchar(200) default NULL,
  `effAnSiege` varchar(10) default NULL,
  `apetSiege` varchar(5) default NULL,
  `apetSiegeLib` varchar(200) default NULL,
  `trtDerDateSiege` varchar(20) default NULL,
  `adrEtVoieNum` varchar(100) default NULL,
  `adrEtVoieType` varchar(100) default NULL,
  `adrEtVoieLib` varchar(100) default NULL,
  `adrEtDepCom` varchar(100) default NULL,
  `adrEtComLib` varchar(100) default NULL,
  `adrEtCodePost` varchar(100) default NULL,
  `adrEtPost1` varchar(200) default NULL,
  `adrEtPost2` varchar(200) default NULL,
  `adrEtPost3` varchar(200) default NULL,
  `nic` varchar(5) default '00000',
  `etatEt` char(2) default NULL,
  `etatDebDateEt` varchar(15) default NULL,
  `catEt` char(2) default NULL,
  `catEtLib` varchar(200) default NULL,
  `eff3112TrEt` char(2) default NULL,
  `eff3112TrEtLib` varchar(100) default NULL,
  `effAnEt` varchar(15) default NULL,
  `apet` varchar(5) default NULL,
  `apetLib` varchar(200) default NULL,
  `trtDerDateEt` varchar(15) default NULL,
  `etatEtLib` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE = innodb;

ALTER TABLE `ModuleCommun` ADD `choix_langue_affichage_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleOrganisme` ADD `traduire_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `consultation` ADD `accessibilite_fr` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_cz` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_du` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_su` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `TypeProcedure` ADD `abbreviation_portail_europe` VARCHAR( 50 ) NOT NULL ;

UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'AOO' WHERE `TypeProcedure`.`id_type_procedure` =1 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'AOR' WHERE `TypeProcedure`.`id_type_procedure` =2 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'PAD' WHERE `TypeProcedure`.`id_type_procedure` =3 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'COU' WHERE `TypeProcedure`.`id_type_procedure` =4 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'CRE' WHERE `TypeProcedure`.`id_type_procedure` =5 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'DCO' WHERE `TypeProcedure`.`id_type_procedure` =7 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'MNE' WHERE `TypeProcedure`.`id_type_procedure` =8 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =9 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =10 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =11 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =15 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =30 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =31 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =32 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =33 LIMIT 1 ;

ALTER TABLE `Langue` ADD `libelle` VARCHAR( 100 ) NULL ;

DELETE FROM `Langue`;
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`, `libelle`) VALUES (1, 'fr', '1', '1', 'French', 'Français');
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`, `libelle`) VALUES (2, 'en', '1', '0', 'English', 'Anglais');
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`, `libelle`) VALUES (3, 'es', '1', '0', 'Spanish', 'Espagnol');
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`, `libelle`) VALUES (4, 'du', '0', '0', NULL, 'Allemand');
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`, `libelle`) VALUES (5, 'su', '0', '0', NULL, 'Suisse');
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `setlocal`, `libelle`) VALUES (6, 'cz', '0', '0', NULL, 'Tchèque');

UPDATE `Langue` set `libelle`='Français' where `setlocal`='fr';
UPDATE `Langue` set `libelle`='Anglais' where `setlocal`='en';
UPDATE `Langue` set `libelle`='Espagnol' where `setlocal`='es';
UPDATE `Langue` set `libelle`='Allemand' where `setlocal`='du';
UPDATE `Langue` set `libelle`='Suisse' where `setlocal`='su';
UPDATE `Langue` set `libelle`='Tchèque' where `setlocal`='cz';

ALTER TABLE `ModuleCommun` ADD `vision_ea_entreprise` ENUM('0' ,'1') NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `ac_sad_transv` ENUM('0' ,'1') NOT NULL DEFAULT '0'; 
ALTER TABLE `ModuleCommun` ADD `Enchere_Entreprise` ENUM('0' ,'1') NOT NULL DEFAULT '0';














-- 20 Aril 2009
ALTER TABLE `ModuleCommun` ADD `choix_langue_affichage_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleOrganisme` ADD `traduire_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `consultation` ADD `accessibilite_fr` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_cz` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_du` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_su` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `TypeProcedure` ADD `abbreviation_portail_europe` VARCHAR( 50 ) NOT NULL ;

UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'AOO' WHERE `TypeProcedure`.`id_type_procedure` =1 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'AOR' WHERE `TypeProcedure`.`id_type_procedure` =2 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'PAD' WHERE `TypeProcedure`.`id_type_procedure` =3 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'COU' WHERE `TypeProcedure`.`id_type_procedure` =4 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'CRE' WHERE `TypeProcedure`.`id_type_procedure` =5 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'DCO' WHERE `TypeProcedure`.`id_type_procedure` =7 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'MNE' WHERE `TypeProcedure`.`id_type_procedure` =8 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =9 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =10 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =11 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =15 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =30 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =31 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =32 LIMIT 1 ;
UPDATE `TypeProcedure` SET `abbreviation_portail_europe` = 'OTH' WHERE `TypeProcedure`.`id_type_procedure` =33 LIMIT 1 ;

ALTER TABLE `Langue` ADD `libelle` VARCHAR( 100 ) NULL ;

ALTER TABLE `ModuleCommun` ADD `vision_ea_entreprise` ENUM('0' ,'1') NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `ac_sad_transv` ENUM('0' ,'1') NOT NULL DEFAULT '0'; 
ALTER TABLE `ModuleCommun` ADD `Enchere_Entreprise` ENUM('0' ,'1') NOT NULL DEFAULT '0';

ALTER TABLE `consultation` CHANGE `detail_consultation` `detail_consultation` TEXT NOT NULL DEFAULT '';
ALTER TABLE `Annonce` CHANGE `date_envoi` `date_envoi` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `Inscrit` ADD UNIQUE `login` ( `login` );
ALTER TABLE `Inscrit` CHANGE `login` `login` VARCHAR( 50 ) NULL DEFAULT NULL;

ALTER TABLE `Agent` ADD UNIQUE `login` ( `login` );
ALTER TABLE `Annonce` CHANGE `datepub` `datepub` VARCHAR( 10 ) NOT NULL DEFAULT '0000-00-00';

-- OAB : ajout des indexs sur les champs utilisés dans la recherche

CREATE INDEX Idx_Consultation_Categorie on consultation (categorie);

CREATE INDEX Idx_Consultation_Titre on consultation (titre);

CREATE INDEX Idx_Consultation_Resume on consultation (resume);

CREATE INDEX Idx_Consultation_CAP on consultation (consultation_achat_publique);

CREATE INDEX Idx_Consultation_Datevalid on consultation (datevalidation);

CREATE INDEX Idx_Consultation_EA on consultation (etat_approbation);

CREATE INDEX Idx_Consultation_DVI on consultation (date_validation_intermediaire);

CREATE INDEX Idx_Consultation_RU on consultation (reference_utilisateur);

CREATE INDEX Idx_Consultation_Datefin on consultation (datefin);

CREATE INDEX Idx_Consultation_ITP on consultation (id_type_procedure);

CREATE INDEX Idx_Consultation_ITA on consultation (id_type_avis);

CREATE INDEX Idx_Consultation_TML on consultation (type_mise_en_ligne);

CREATE INDEX Idx_Consultation_DMLC on consultation (date_mise_en_ligne_calcule);

CREATE INDEX Idx_Consultation_Datemiseenligne on consultation (datemiseenligne);

CREATE INDEX Idx_Consultation_Code_CPV on consultation (code_cpv_1,code_cpv_2,code_cpv_3,code_cpv_4);

CREATE INDEX Idx_Annonce on Annonce (consultation_ref);

CREATE INDEX Idx_CategorieLot on CategorieLot (consultation_ref);

UPDATE `GeolocalisationN1` SET `denomination1` = 'Champagne-Ardenne' WHERE `id` = 51;

ALTER TABLE `consultation` ADD `alloti` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `numero_phase` INT( 3 ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleOrganisme` ADD `suivi_passation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- 
-- Structure de la table `Agent_Service_Metier`
-- 

CREATE TABLE `Agent_Service_Metier` (
  `id_agent` int(11) NOT NULL default '0',
  `id_service_metier` int(11) NOT NULL default '0',
  `id_profil_service` int(11) NOT NULL default '0',
  KEY `id_agent` (`id_agent`),
  KEY `id_service_metier` (`id_service_metier`)
) TYPE=InnoDB;

-- 
-- Contenu de la table `Agent_Service_Metier`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `Organisme_Service_Metier`
-- 

CREATE TABLE `Organisme_Service_Metier` (
  `organisme` varchar(30) NOT NULL default '0',
  `id_service_metier` int(11) NOT NULL default '0',
  KEY `organisme` (`organisme`),
  KEY `id_service_metier` (`id_service_metier`)
) TYPE=InnoDB;

-- 
-- Contenu de la table `Organisme_Service_Metier`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `Service_Mertier`
-- 

CREATE TABLE `Service_Mertier` (
  `id` int(11) NOT NULL default '0',
  `sigle` varchar(50) default NULL,
  `denomination` varchar(255) default NULL,
  `url_acces` text,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;


-- 
-- Structure de la table `Service_Mertier_Profils`
-- 

CREATE TABLE `Service_Mertier_Profils` (
  `id_interne` int(11) NOT NULL default '0',
  `id_service_metier` int(11) NOT NULL default '0',
  `id_externe` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL,
  KEY `id_service_metier` (`id_service_metier`)
) TYPE=InnoDB;

-- 
-- Contenu de la table `Service_Mertier_Profils`
-- 

-- 
-- Contraintes pour les tables exportées
-- 

-- 
-- Contraintes pour la table `Agent_Service_Metier`
-- 

ALTER TABLE `Agent_Service_Metier`  ADD CONSTRAINT `Agent_Service_Metier_ibfk_2` FOREIGN KEY (`id_service_metier`) REFERENCES `Service_Mertier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Agent_Service_Metier`  ADD CONSTRAINT `Agent_Service_Metier_ibfk_1` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Contraintes pour la table `Organisme_Service_Metier`
-- 

ALTER TABLE `Organisme_Service_Metier`  ADD CONSTRAINT `Organisme_Service_Metier_ibfk_2` FOREIGN KEY (`id_service_metier`) REFERENCES `Service_Mertier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Organisme_Service_Metier`  ADD CONSTRAINT `Organisme_Service_Metier_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Contraintes pour la table `Service_Mertier_Profils`
-- 
ALTER TABLE `Service_Mertier_Profils` ADD CONSTRAINT `Service_Mertier_Profils_ibfk_1` FOREIGN KEY (`id_service_metier`) REFERENCES `Service_Mertier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Agent` ADD `date_creation` VARCHAR( 20 ) NULL ;
ALTER TABLE `Agent` ADD `date_modification` VARCHAR( 20 ) NULL ;

ALTER TABLE `Agent_Service_Metier` ADD `date_creation` VARCHAR( 20 ) NULL ;
ALTER TABLE `Agent_Service_Metier` ADD `date_modification` VARCHAR( 20 ) NULL ;

ALTER TABLE `Agent_Service_Metier` ADD PRIMARY KEY ( `id_agent` , `id_service_metier` ) ;

ALTER TABLE `ModuleOrganisme` ADD `suivi_passation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `ModuleCommun` ADD `socle_interne` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

--04/12/2008 MMI
CREATE TABLE `sso_agent` (
`id_sso` VARCHAR( 40 ) NOT NULL ,
`id_agent` INT( 20 ) NOT NULL ,
`organisme` VARCHAR( 20 ) NOT NULL ,
`id_service` INT( 20 ) NOT NULL ,
`date_connexion` VARCHAR( 20 ) NOT NULL ,
`date_last_request` VARCHAR( 20 ) NOT NULL
) ENGINE = InnoDB;
ALTER TABLE `sso_agent` ADD PRIMARY KEY ( `id_sso` ) ;

CREATE TABLE `Socle_Habilitation_Agent` (
`id_agent` INT( 11 ) NOT NULL ,
`gestion_agent_pole_socle` ENUM( '0', '1' ) NOT NULL DEFAULT '0',
`gestion_agents_socle` ENUM( '0', '1' ) NOT NULL DEFAULT '0',
`droit_gestion_services_socle` ENUM( '0', '1' ) NOT NULL DEFAULT '0'
) ENGINE = InnoDB;

ALTER TABLE `Socle_Habilitation_Agent` ADD PRIMARY KEY ( `id_agent` );

ALTER TABLE `ModuleCommun` ADD `donnee_complementaires_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

CREATE TABLE IF NOT EXISTS `CG76_Donnee_Complementaire_entreprise` (
  `Ref` smallint(6) NOT NULL auto_increment,
  `idInscrit` smallint(5) NOT NULL default '0',
  `IdEntreprise` int(11) NOT NULL default '0',
  `type_formation` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `cout_moyen_journee` varchar(10) default NULL,
  `collaboration_fpt` int(1) NOT NULL default '0',
  `collaboration_fpe` int(1) NOT NULL default '0',
  `centre_documentation` int(1) NOT NULL default '0',
  `service_reprographie` int(1) NOT NULL default '0',
  `salle_info` int(1) NOT NULL default '0',
  `salle_cours` int(1) NOT NULL default '0',
  `aire_geo_inter` varchar(255) default NULL,
  PRIMARY KEY  (`Ref`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `CG76_Donnee_Complementaire_Domaine` (
  `Id` int(11) NOT NULL auto_increment,
  `IdDonneeComlementaire` int(11) NOT NULL,
  `IdDomaine` int(11) NOT NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `CG76_PieceJointe` (
  `Ref` int(11) NOT NULL auto_increment,
  `IdPJ` int(11) NOT NULL default '0',
  `idEntreprise` int(11) NOT NULL default '0',
  PRIMARY KEY  (`Ref`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

CREATE TABLE IF NOT EXISTS `CG76_Domaine` (
  `Id` int(11) NOT NULL auto_increment,
  `libelle` varchar(150) NOT NULL,
  `Parent` smallint(5) NOT NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (1, 'Repères et outils fondamentaux prof.', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (2, 'Management', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (3, 'Affaires juridiques', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (4, 'Gestion des ressources humaines', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (5, 'Formation professionnelle et recherche', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (6, 'Finances et gestion financière', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (7, 'Communication institutionnelle', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (8, 'Informatique et systèmes d''information', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (9, 'Europe et international', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (10, 'Citoyenneté et population', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (11, 'Social et santé', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (12, 'Enfance, éducation, jeunesse', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (13, 'Restauration', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (14, 'Culture, archives et documentation', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (15, 'Environnement', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (16, 'Hygiène et sécurité', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (17, 'Génie technique', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (18, 'Développement local', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (19, 'Sport', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (20, 'Positionnement', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (21, 'Développement durable', 0);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (22, 'Observation du milieu professionnel', 1);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (23, 'Tech. expression communication et relat.', 1);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (24, 'Tech. administ.d''organis°et secrétariat', 1);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (25, 'Connaissance de l''environnement territorial', 1);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (26, 'Savoirs de base intégration vie profess', 1);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (27, 'Management stratégique', 2);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (28, 'Management organisationnel', 2);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (29, 'Management des équipes et des personnes', 2);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (30, 'Approche général du management', 2);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (31, 'Modes de gestion des services publics', 3);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (32, 'Achat public, Marchés pub et contrats', 3);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (33, 'Assurances', 3);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (34, 'Prévention juridique et contentieux', 3);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (35, 'Approche générale du droit', 3);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (36, 'Gestion administrative et statutaire', 4);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (37, 'Politiques de GRH', 4);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (38, 'Conditions de travail', 4);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (39, 'Relations sociales', 4);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (40, 'Fonction formation', 4);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (41, 'Approche générale de la GRH', 4);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (42, 'Connais. Acteurs disp polit emploi form', 5);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (43, 'Contrôle évaluation org form recherche', 5);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (44, 'Instruc organis gest form prof et rech', 5);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (45, 'Procédure budgétaire et comptable', 6);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (46, 'Gestion et stratégie financière', 6);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (47, 'Fiscalité et dotation de l''Etat', 6);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (48, 'Approche générale des finances', 6);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (49, 'Stratégie de communication', 7);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (50, 'Techniques et outils de communication', 7);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (51, 'Appr géné de la communication institut', 7);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (52, 'Architecture et admin des syst d''infos', 8);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (53, 'Réseaux et télécommunication', 8);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (54, 'Techniques informatiques', 8);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (55, 'Bureautique et utilisation outils inform', 8);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (56, 'Connaissances des institutions', 9);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (57, 'Politiques régionales', 9);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (58, 'Coopération décentralisée', 9);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (59, 'Politiques sectorielles', 9);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (60, 'Démographie', 10);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (61, 'état civil', 10);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (62, 'élections', 10);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (63, 'gestion funéraire', 10);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (64, 'affaires générales', 10);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (65, 'Connais acteurs des dispos et polit', 11);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (66, 'Protection de l''enfance et de l''adolesc.', 11);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (67, 'Insertion et développement social', 11);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (68, 'Accueil et accompagnement des pers âgées', 11);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (69, 'Prévention et promotion de la santé', 11);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (70, 'Méthodologie du travail social', 11);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (71, 'Appr. gén. de l''act. soc. et de la santé', 11);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (72, 'Accueil et accompagnement des pers handicapées', 11);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (73, 'Connais. acteurs dispositifs, politiques', 12);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (74, 'Accueil de l''enfance et de l''adolescence', 12);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (75, 'Animation enfance, jeunesse', 12);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (76, 'Organisation et production', 13);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (77, 'Hygiène et sécurité alimentaire', 13);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (78, 'Distribution et service', 13);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (79, 'Equilibre nutritionnel', 13);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (80, 'Approche génér. de la restauration', 13);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (81, 'Connais. act dispositifs, polit act cult', 14);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (82, 'Gestion et enrichissement des fonds', 14);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (83, 'Diffusion, promot, protection juridique', 14);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (84, 'Protect dév du patri architec et archéo', 14);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (85, 'Enseignement artistique', 14);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (86, 'Programmation et techniques du spectacle', 14);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (87, 'Tech de documentat et d''information', 14);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (88, 'Approche générale de l''action culturelle', 14);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (89, 'Polit géné d''environ et gest des ress.', 15);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (90, 'Résorption des nuisances et pollutions', 15);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (91, 'Production d''eau potable', 15);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (92, 'Traitement des eaux usées', 15);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (93, 'Biologie, hygiène alim et santé animale', 15);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (94, 'Collecte, trait et gestion des déchets', 15);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (95, 'Connaissance des act.,des dispo.des pol.', 15);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (96, 'Maintenance des moyens techniques', 15);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (97, 'Prév et opér d''incendie et de secours', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (98, 'Sûreté et sécurité dans la ville', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (99, 'Prév et gest des risques tech et nat.', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (100, 'Prévention et protection du public', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (101, 'Sécurité des agents au travail', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (102, 'Responsabilité du personnel d?encadrement ', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (103, 'Plan de prévention entreprises extérieures intervenantes', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (104, 'Travail sur écran', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (105, 'Fonction achat des équipements dans une logique d?ergonomie', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (106, 'Formation des ACMO', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (107, 'Formation des membres du CHS', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (108, 'Nuisances sonores', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (109, 'Equipement de protection individuelle (EPI)', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (110, 'Document unique', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (111, 'Prévention et Secours Civiques de niveau ?PSC1-', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (112, 'SST', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (113, 'SSIAP 1', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (114, 'Utilisation du CMSI et du SSI', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (115, 'Initiation à la lutte contre l?incendie', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (116, 'Habilitations électriques', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (117, 'CACES', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (118, 'Signalisation temporaire des zones d?intervention', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (119, 'Manutention de ponts roulants', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (120, 'Risque routier', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (121, 'FCOS', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (122, 'Permis Poids lourd', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (123, 'Soudure', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (124, 'Utilisation de la débroussailleuse', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (125, 'Utilisation de la tronçonneuse', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (126, 'Manutentions manuelles et mécaniques de charges', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (127, 'Montage et démontage des échafaudages, scènes, podiums', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (128, 'Utilisation des produits phyto sanitaires', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (129, 'Gestion des déchets industriels et ménagers', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (130, 'Maîtrise du risque infectieux en milieu médico social', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (131, 'Nettoyage et désinfection des locaux', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (132, 'Fonction achat des produits d?entretien', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (133, 'Gestion du stress', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (134, 'Gestion des phénomènes agressifs', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (135, 'HACCP', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (136, 'Hygiène des aliments', 16);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (137, 'Architecture et construction', 17);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (138, 'Infrastructures et réseaux', 17);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (139, 'Espaces verts et paysage', 17);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (140, 'Maint des bâtiments tt corps d''état', 17);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (141, 'Maîtrise de l''énergie', 17);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (142, 'Maintenance des moyens techniques', 17);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (143, 'Connais. acteurs disp politiq dév territ', 18);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (144, 'Aménag, urbanisme et act foncière', 18);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (145, 'Politique de l''habitat logement social', 18);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (146, 'Action économique', 18);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (147, 'Développement touristique', 18);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (148, 'Déplacements et transports', 18);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (149, 'Connais. acteurs disp politiq sportives', 19);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (150, 'Enseignement des prat sportives', 19);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (151, 'Entretien des matériels sportifs', 19);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (152, 'Orientation formative, posit évaluation', 20);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (153, 'Remise à niveau', 20);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (154, 'Acquisition de connaissances', 20);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (155, 'Méthodo et entraînement aux épreuves', 20);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (156, 'Sensibilisation générale', 21);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (157, 'Marchés publics et développement durable', 21);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (158, 'Energies et approche environnementale', 21);
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (159, 'Eco labels et certifications', 21);

ALTER TABLE `CG76_Donnee_Complementaire_entreprise` ADD `commentaire` TEXT NULL DEFAULT NULL ;

ALTER TABLE `ModuleOrganisme` ADD `numerotation_ref_cons` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `consultation` ADD `consultation_migration` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `url_consultation_migration` VARCHAR( 100 ) NULL ;

CREATE TABLE `Faq_Entreprise` (
  `id` int(11) NOT NULL auto_increment,
  `question` text NOT NULL,
  `question_fr` text NOT NULL,
  `question_en` text NOT NULL,
  `question_es` text NOT NULL,
  `reponse` text NOT NULL,
  `reponse_fr` text NOT NULL,
  `reponse_en` text NOT NULL,
  `reponse_es` text NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB ;

INSERT INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`) VALUES (1, 'Que faire si l''applet Java ne se charge pas ?', 'Que faire si l''applet Java ne se charge pas ?', '', '', '<p>Vérifier que votre poste est conforme aux "Conditions d''utilisation" du site qui présente les pré-requis d''installation et de configuration des postes de travail (ex : version de la machine java, fichiers à installer, etc.).</p>', '<p>Vérifier que votre poste est conforme aux "Conditions d''utilisation" du site qui présente les pré-requis d''installation et de configuration des postes de travail (ex : version de la machine java, fichiers à installer, etc.).</p>', '', '');
INSERT INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`) VALUES (2, 'Que dois-je faire lorsque lors de la remise d''une réponse, le message suivant s''affiche "Les fichiers d''autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d''utilisation?" ?', 'Que dois-je faire lorsque lors de la remise d''une réponse, le message suivant s''affiche "Les fichiers d''autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d''utilisation?" ?', '', '', '<p>Les "Conditions d''utilisation" indiquent la marche à suivre pour installer ces fichiers manquants.</p>', '<p>Les "Conditions d''utilisation" indiquent la marche à suivre pour installer ces fichiers manquants.</p>', '', '');
INSERT INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`) VALUES (3, 'Que faire si j''ai oublié mon identifiant ou mon mot de passe de connexion ?', 'Que faire si j''ai oublié mon identifiant ou mon mot de passe de connexion ?', '', '', '<p>La fonction "Mot de passe oublié" vous permet de recevoir votre identifiant et votre nouveau mot de passe.</p>', '<p>La fonction "Mot de passe oublié" vous permet de recevoir votre identifiant et votre nouveau mot de passe.</p>', '', '');
INSERT INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`) VALUES (4, 'Que faire si je ne retrouve pas une consultation restreinte sur la PMI ?', 'Que faire si je ne retrouve pas une consultation restreinte sur la PMI ?', '', '', '<p>Il est nécessaire d''utiliser le moteur de recherche avancée avec la référence et le code d''accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l''assistance téléphonique.</p>', '<p>Il est nécessaire d''utiliser le moteur de recherche avancée avec la référence et le code d''accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l''assistance téléphonique.</p>', '', '');
INSERT INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`) VALUES (5, 'Est ce que l''entreprise reçoit un AR quand elle pose une question via la plate-forme ?', 'Est ce que l''entreprise reçoit un AR quand elle pose une question via la plate-forme ?', '', '', '<p>Non. Seuls les dépôts de plis donnent lieu à un accusé de réception.</p>', '<p>Non. Seuls les dépôts de plis donnent lieu à un accusé de réception.</p>', '', '');
INSERT INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`) VALUES (6, 'Pourquoi je ne retrouve pas une consultation publique sur le portail ?', 'Pourquoi je ne retrouve pas une consultation publique sur le portail ?', '', '', '<p>Veuillez vérifier que la date limite de remise des plis n''est pas déjà passée. Si ce n''est pas le cas, vérifiez votre requête ou assurez-vous que la consultation n''a pas été retirée par le Pouvoir adjudicateur.</p>', '<p>Veuillez vérifier que la date limite de remise des plis n''est pas déjà passée. Si ce n''est pas le cas, vérifiez votre requête ou assurez-vous que la consultation n''a pas été retirée par le Pouvoir adjudicateur.</p>', '', '');
INSERT INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`) VALUES (7, 'Pourquoi n''ai-je pas reçu d''accusé de réception par courriel après mon dépôt de pli ?', 'Pourquoi n''ai-je pas reçu d''accusé de réception par courriel après mon dépôt de pli ?', '', '', '<p>L''application vous envoie un AR par courriel lorsque l''écran de confirmation de bonne réception de votre envoi s''affiche.\r\nVérifier que votre adresse électronique a été correctement saisie dans votre compte sur la plate-forme, ou que ce courriel ne se trouve pas dans vos courriels indésirables.</p>', '<p>L''application vous envoie un AR par courriel lorsque l''écran de confirmation de bonne réception de votre envoi s''affiche.\r\nVérifier que votre adresse électronique a été correctement saisie dans votre compte sur la plate-forme, ou que ce courriel ne se trouve pas dans vos courriels indésirables.</p>', '', '');
INSERT INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`) VALUES (8, 'Les enveloppes de candidature ou d''offre peuvent-elles être envoyées au format PDF?', 'Les enveloppes de candidature ou d''offre peuvent-elles être envoyées au format PDF?', '', '', '<p>Non. Seul le format ZIP est accepté.</p>', '<p>Non. Seul le format ZIP est accepté.</p>', '', '');

ALTER TABLE `ModuleCommun` ADD `module_certificat` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleOrganisme` ADD `afficher_image_organisme` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleOrganisme` ADD `generation_reference_utilisateur` ENUM( '0', '1' ) NOT NULL DEFAULT '0'  AFTER `traduire_consultation`;

ALTER TABLE `consultation` CHANGE `url_consultation_migration` `url_consultation_migration` TEXT NULL DEFAULT NULL ;

ALTER TABLE `consultation` CHANGE `url_consultation_migration` `url_consultation_migration` TEXT NULL DEFAULT NULL ;
ALTER TABLE `ModuleCommun` ADD `socle_externe_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `module_certificat`;

ALTER TABLE `ModuleOrganisme` DROP `afficher_image_organisme`;
ALTER TABLE `ModuleCommun` ADD `afficher_image_organisme`ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- demande lié à la migration epm 

ALTER TABLE `consultation` CHANGE `code_procedure` `code_procedure` VARCHAR( 15 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` CHANGE `libelle_code_cpv_1` `libelle_code_cpv_1` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `consultation` CHANGE `libelle_code_cpv_2` `libelle_code_cpv_2` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `consultation` CHANGE `libelle_code_cpv_3` `libelle_code_cpv_3` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `consultation` CHANGE `libelle_code_cpv_4` `libelle_code_cpv_4` VARCHAR( 200 ) NULL DEFAULT NULL;

ALTER TABLE `CategorieLot` CHANGE `description` `description` VARCHAR( 1000 ) NOT NULL DEFAULT '';
ALTER TABLE `CategorieLot` CHANGE `libelle_code_cpv_1` `libelle_code_cpv_1` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `CategorieLot` CHANGE `libelle_code_cpv_2` `libelle_code_cpv_2` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `CategorieLot` CHANGE `libelle_code_cpv_3` `libelle_code_cpv_3` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `CategorieLot` CHANGE `libelle_code_cpv_4` `libelle_code_cpv_4` VARCHAR( 200 ) NULL DEFAULT NULL;

-- Pour la migration ebourgogne

ALTER TABLE `Entreprise` ADD `date_creation` VARCHAR( 20 ) NOT NULL;
ALTER TABLE `Entreprise` ADD `date_modification` VARCHAR( 20 ) NOT NULL;
ALTER TABLE `Organisme` ADD `id_initial` INT( 11 ) NOT NULL AFTER `affichage_entite` ;
ALTER TABLE `Entreprise` ADD `id_initial` INT( 11 ) NOT NULL AFTER `date_modification` ;
ALTER TABLE `Agent` ADD `id_initial` INT( 11 ) NOT NULL AFTER `date_modification`;
ALTER TABLE `Inscrit` ADD `id_initial` INT( 11 ) NOT NULL AFTER `bloque` ;

CREATE TABLE IF NOT EXISTS `DateLancementCron` (
  `id` int(11) NOT NULL auto_increment,
  `mise_a_jour_socle` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='cette table enregistre la derniere date de lancement de cron';

-- KBE : 16/02/2009 : Correction ordre des champs pour qu'il corresponds au mappping propel
ALTER TABLE `Agent` CHANGE `id_initial` `id_initial` INT( 11 ) NOT NULL DEFAULT '0' AFTER `date_modification`;
ALTER TABLE `Organisme` CHANGE `id_initial` `id_initial` INT( 11 ) NOT NULL DEFAULT '0' AFTER `affichage_entite`;
ALTER TABLE `Entreprise` CHANGE `id_initial` `id_initial` INT( 11 ) NOT NULL DEFAULT '0' AFTER `date_modification`;
ALTER TABLE `Inscrit` CHANGE `id_initial` `id_initial` INT( 11 ) NOT NULL DEFAULT '0' AFTER `bloque`;

ALTER TABLE `Agent` ADD `id_profil_socle_externe` INT( 11 ) NOT NULL;

-- LCH - 06/03/2009
ALTER TABLE `GeolocalisationN0` ADD `denomination_fr` VARCHAR( 40 ) NOT NULL AFTER `denomination` ,ADD `denomination_en` VARCHAR( 40 ) NOT NULL AFTER `denomination_fr`, ADD `denomination_es` VARCHAR( 40 ) NOT NULL AFTER `denomination_en` ;
ALTER TABLE `GeolocalisationN0` ADD `libelle_selectionner_fr` VARCHAR( 40 ) NOT NULL AFTER `libelle_selectionner`, ADD `libelle_selectionner_en` VARCHAR( 40 ) NOT NULL AFTER `libelle_selectionner_fr` ,ADD `libelle_selectionner_es` VARCHAR( 40 ) NOT NULL AFTER `libelle_selectionner_en` ;
ALTER TABLE `GeolocalisationN0` ADD `libelle_tous_fr` VARCHAR( 40 ) NOT NULL AFTER `libelle_tous` ,ADD `libelle_tous_en` VARCHAR( 40 ) NOT NULL AFTER `libelle_tous_fr` ,ADD `libelle_tous_es` VARCHAR( 40 ) NOT NULL AFTER `libelle_tous_en` ;
ALTER TABLE `GeolocalisationN0` ADD `libelle_Aucun_fr` VARCHAR( 50 ) NOT NULL AFTER `libelle_Aucun` ,ADD `libelle_Aucun_en` VARCHAR( 50 ) NOT NULL AFTER `libelle_Aucun_fr` , ADD `libelle_Aucun_es` VARCHAR( 50 ) NOT NULL AFTER `libelle_Aucun_en` ;
UPDATE `GeolocalisationN0` SET denomination_fr = denomination;
UPDATE `GeolocalisationN0` SET libelle_selectionner_fr = libelle_selectionner;
UPDATE `GeolocalisationN0` SET libelle_tous_fr = libelle_tous;
UPDATE `GeolocalisationN0` SET libelle_Aucun_fr = libelle_Aucun;
UPDATE `GeolocalisationN0` SET `denomination_en` = 'Others countries',`libelle_selectionner_en` = 'Select by country',`libelle_tous_en` = 'All countries',`libelle_Aucun_en` = 'No countries' WHERE `GeolocalisationN0`.`id` =3 LIMIT 1 ;
UPDATE `GeolocalisationN0` SET `denomination_en` = 'Metropolitan France',`libelle_selectionner_en` = 'Select by department(s)',`libelle_tous_en` = 'All departments',`libelle_Aucun_en` = 'No Department' WHERE `GeolocalisationN0`.`id` =1 LIMIT 1 ;
UPDATE `GeolocalisationN0` SET `denomination_en` = 'France - overseas community',`libelle_selectionner_en` = 'Select by community',`libelle_tous_en` = 'All community',`libelle_Aucun_en` = 'No community' WHERE `GeolocalisationN0`.`id` =2 LIMIT 1 ;

-- MWA - 09/03/2009
UPDATE `GeolocalisationN0` SET `denomination_es` = 'Otro(s) pais(es)',`libelle_selectionner_es` = 'Seleccionar por pais(es)',`libelle_tous_es` = 'Todos los pais(es)',`libelle_Aucun_es` = 'Ningún pais' WHERE `GeolocalisationN0`.`id` =3 LIMIT 1 ;
UPDATE `GeolocalisationN0` SET `denomination_es` = 'Francia Metropolitana',`libelle_selectionner_es` = 'Seleccionar por Departamento(s)',`libelle_tous_es` = 'Todos los Departamentos',`libelle_Aucun_es` = 'Ningún Departamento' WHERE `GeolocalisationN0`.`id` =1 LIMIT 1 ;
UPDATE `GeolocalisationN0` SET `denomination_es` = 'Francia - Colectividade(s) de Ultra-Mar',`libelle_selectionner_es` = 'Seleccionar por Colectividade(s)',`libelle_tous_es` = 'Todas las Colectividades',`libelle_Aucun_es` = 'Ninguna Colectividad' WHERE `GeolocalisationN0`.`id` =2 LIMIT 1 ;

-- MWA - 09/03/2009
UPDATE `Faq_Entreprise`  set `question_en` = 'How to proceed if the Java applet does not load?',  `reponse_en`='<p>Make sure your equipment and system meet all the requirements defined in the "Terms of use" section (e.g.: Java version, file download, etc.).</p>' where id=1;
UPDATE `Faq_Entreprise`  set `question_en` = 'How to proceed if I get the following message while submitting a bid "Les fichiers d''autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d''utilisation"?',  `reponse_en`='<p>You will find the procedure to follow in the "Terms of use" section in order to install the missing files.</p>' where id=2;
UPDATE `Faq_Entreprise`  set `question_en` = 'How to proceed if I have lost my ID and password?',  `reponse_en`='<p>Click the "Forgotten password" link and fill in your e-mail address to retrieve your ID and password.</p>' where id=3;
UPDATE `Faq_Entreprise`  set `question_en` = 'I cannot find a restricted tender on the PMI website. What can I do?',  `reponse_en`='<p>Use the advanced search page to search the reference and the access code provided by the public entity.<br />You will then have access to the restricted tender.<br />If you encounter any problem, please contact our hotline.</p>' where id=4;
UPDATE `Faq_Entreprise`  set `question_en` = 'Does the company receive an AR (acknowledgement of receipt) when submitting a question on the platform?',  `reponse_en`='<p>No. An acknowledgement of receipt is sent to those submitting a bid only.</p>' where id=5;
UPDATE `Faq_Entreprise`  set `question_en` = 'Why can not I find a public tender on the portal?',  `reponse_en`='<p>Please make sure the closing date has not passed. Otherwise, make sure the tender has not been removed by the public entity.</p>' where id=6;
UPDATE `Faq_Entreprise`  set `question_en` = 'Why haven''t I received any AR by mail after I submitted my envelop?',  `reponse_en`='<p>You will receive an AR by mail once a confirmation message is displayed on screen. <br />Make sure the email address has been correctly entered in your account or check your spam folder.</p>' where id=7;
UPDATE `Faq_Entreprise`  set `question_en` = 'Can the application /bid envelops be sent in a PDF format?',  `reponse_en`='<p>No. ZIP is the only format allowed.</p>' where id=8;

UPDATE `Faq_Entreprise`  set `question_es` = '¿Qué pasa si el applet de Java no se carga?',  `reponse_es`='<p>Compruebe que su puesto está en conformidad con "las condiciones de uso" del sitio que presenta los requisitos previos de instalación y configuración de los puestos de trabajo (por ejemplo, la versión de la máquina para instalar los archivos java, etc.).</p>' where id=1;
UPDATE `Faq_Entreprise`  set `question_es` = '¿Qué debo hacer cuando al momento de presentar una respuesta, el siguiente mensaje "Les fichiers d''autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d''utilisation" ?',  `reponse_es`='<p>Las "Condiciones de utilización" muestran cómo instalar estos archivos que faltan.</p>' where id=2;
UPDATE `Faq_Entreprise`  set `question_es` = '¿Qué sucede si olvido mi nombre de usuario o mi contraseña de inicio de sesión?',  `reponse_es`='<p>La función "¿Olvidaste tu contraseña?" le permite recibir su login y contraseña.</p>' where id=3;
UPDATE `Faq_Entreprise`  set `question_es` = '¿Qué sucede si no se encuentra un procedimiento restringido?',  `reponse_es`='<p>Es necesario utilizar la búsqueda avanzada con la referencia y el código de acceso transmitido por la autoridad contratante.<br />Usted tendrá acceso a los datos del procedimiento restringido.<br />En caso de dificultad, póngase en contacto con la línea directa.</p>' where id=4;
UPDATE `Faq_Entreprise`  set `question_es` = '¿La empresa recibe un AR cuando ella hace una pregunta a través de la plataforma?',  `reponse_es`='<p>No. Sólo los depósitos de pliegues dan lugar a un recibo.</p>' where id=5;
UPDATE `Faq_Entreprise`  set `question_es` = '¿Por qué no puedo encontrar una consulta pública en el portal?',  `reponse_es`='<p>Por favor, verifique que la fecha límite para la presentación de los pliegues no ha pasado. Si este no es el caso, compruebe su solicitud o asegúrese de que la consulta no ha sido retirada por la Autoridad contratante.</p>' where id=6;
UPDATE `Faq_Entreprise`  set `question_es` = '¿Por qué no recibo un acuse por correo después de la presentación de mi pliego?',  `reponse_es`='<p>La aplicación le envía un AR por correo electrónico cuando la pantalla de confirmación de recibo de su envío aparece. <br />Compruebe que su dirección de correo electrónico está escrito correctamente en su cuenta en la plataforma, o que el correo electrónico no está en su correo basura.</p>' where id=7;
UPDATE `Faq_Entreprise`  set `question_es` = '¿Los sobres de candidatura u ofertas pueden ser enviados en formato PDF?',  `reponse_es`='<p>No. Sólo el formato ZIP es aceptado.</p>' where id=8;

UPDATE `Faq_Entreprise`  set `reponse_fr`='<p>Il est nécessaire d''utiliser le moteur de recherche avancée avec la référence et le code d''accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l''assistance téléphonique.</p>' where id=4;
UPDATE `Faq_Entreprise`  set `reponse`='<p>Il est nécessaire d''utiliser le moteur de recherche avancée avec la référence et le code d''accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l''assistance téléphonique.</p>' where id=4;

-- MWA - 10/03/2009 demande lié à la migration epm
ALTER TABLE `CategorieLot` ADD COLUMN `id_lot_externe` int(11) NULL DEFAULT NULL; 
ALTER TABLE `CategorieLot` CHANGE `description_detail` `description_detail` VARCHAR( 1000 ) NOT NULL DEFAULT '';
ALTER TABLE `CategorieLot` CHANGE `description_detail_fr` `description_detail_fr` VARCHAR( 1000 ) NOT NULL DEFAULT '';
ALTER TABLE `CategorieLot` CHANGE `description_fr` `description_fr` VARCHAR( 1000 ) NOT NULL DEFAULT '';

-- OAB - 27/03/2009 le module socle_externe_agent
ALTER TABLE `ModuleCommun` ADD `socle_externe_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `afficher_image_organisme`;

CREATE TABLE `InterfaceTypeProcedure` (
  `id` int(11) NOT NULL auto_increment,
  `type_procedure_sis` varchar(10) default NULL,
  `type_procedure_marco` varchar(10) default NULL,
  `type_procedure` varchar(10) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB AUTO_INCREMENT=20 ;


INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(1, 'ACI', NULL, 'ATR');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(2, 'ACR', NULL, 'ATR');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(3, 'AOCR', NULL, 'ATR');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(4, 'AOO', NULL, 'ATR');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(5, 'AOO2', NULL, 'AOO');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(6, 'AOR', NULL, 'ARC');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(7, 'COO', NULL, 'COO');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(8, 'COR', NULL, 'CRC');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(9, 'MNAC', NULL, 'MNE');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(10, 'MNAP', NULL, 'MNE');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(11, 'MNSC', NULL, 'MNE');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(12, 'MOCN', NULL, 'CRC');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(13, 'MOSC', NULL, 'MNE');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(14, 'MSF', NULL, 'ATR');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(15, 'MSFA', NULL, 'ACA');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(16, 'MSFB', NULL, 'ACS');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(17, 'MSFC', NULL, 'MAI');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(18, 'MSFR', NULL, 'ATR');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES(19, 'PDC', NULL, 'DCC');

ALTER TABLE `TypeProcedure` DROP `abreviation_interface`;

ALTER TABLE `InterfaceTypeProcedure` ADD `abreviation_type_procedure_organisme` VARCHAR( 30 ) NULL;

UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AOO' WHERE type_procedure='AOO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AOR-C' WHERE type_procedure='ARC';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AOR-O' WHERE type_procedure='ARO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'PA-INF' WHERE type_procedure='MAI';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'PA-SUP' WHERE type_procedure='MAS';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'PA-A30-INF' WHERE type_procedure='PSI';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'PA-A30-SUP' WHERE type_procedure='PSS';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'CO' WHERE type_procedure='COO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'CR-C' WHERE type_procedure='CRC';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'CR-O' WHERE type_procedure='CRO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'DC-C' WHERE type_procedure='DCC';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'DC-O' WHERE type_procedure='DCO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AUT' WHERE type_procedure='ATR';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'MN' WHERE type_procedure='MNE';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AC-SA' WHERE type_procedure='ACA';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AC-MS' WHERE type_procedure='ACS';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'SAD-A' WHERE type_procedure='SAA';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'SAD-MS' WHERE type_procedure='SAM'; 

ALTER TABLE `Entreprise` ADD `date_creation` VARCHAR( 20 ) NOT NULL default '';
ALTER TABLE `Entreprise` ADD `date_modification` VARCHAR( 20 ) NOT NULL default '';

ALTER TABLE `TypeProcedure` DROP `abreviation_interface`;

ALTER TABLE `InterfaceTypeProcedure` ADD `abreviation_type_procedure_organisme` VARCHAR( 30 ) NULL;

UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AOO' WHERE type_procedure='AOO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AOR-C' WHERE type_procedure='ARC';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AOR-O' WHERE type_procedure='ARO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'PA-INF' WHERE type_procedure='MAI';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'PA-SUP' WHERE type_procedure='MAS';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'PA-A30-INF' WHERE type_procedure='PSI';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'PA-A30-SUP' WHERE type_procedure='PSS';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'CO' WHERE type_procedure='COO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'CR-C' WHERE type_procedure='CRC';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'CR-O' WHERE type_procedure='CRO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'DC-C' WHERE type_procedure='DCC';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'DC-O' WHERE type_procedure='DCO';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AUT' WHERE type_procedure='ATR';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'MN' WHERE type_procedure='MNE';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AC-SA' WHERE type_procedure='ACA';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'AC-MS' WHERE type_procedure='ACS';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'SAD-A' WHERE type_procedure='SAA';
UPDATE `InterfaceTypeProcedure` set abreviation_type_procedure_organisme= 'SAD-MS' WHERE type_procedure='SAM'; 

-------------------------------- 
-- 17 Avril 2009
--------------------------------

--05/05/2009



ALTER TABLE `Offre` DROP FOREIGN KEY `Offre_ibfk_1` ;

ALTER TABLE `Inscrit` ADD `inscrit_annuaire_defense` ENUM( '0', '1' ) NOT NULL default '0';


ALTER TABLE `ModuleCommun` DROP `centrale_publication` ;
ALTER TABLE `Inscrit` ADD `date_creation` VARCHAR( 20 ) NOT NULL default '';
ALTER TABLE `Inscrit` ADD `date_modification` VARCHAR( 20 ) NOT NULL default '';

-- kbe 14/05/2009 ajout de la table sso_entreprise (interface defense)
CREATE TABLE `sso_entreprise` (
`id_sso` VARCHAR( 40 ) NOT NULL default '',
`id_inscrit` INT( 11 ) NOT NULL default '0',
`date_connexion` VARCHAR( 20 ) NOT NULL default '',
`date_last_request` VARCHAR( 20 ) NOT NULL default '',
PRIMARY KEY ( `id_sso` ) 
) TYPE = innodb;

ALTER TABLE `ModuleOrganisme` ADD `portail_defense_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
-- ------------
-- MLA 20/05/2009
ALTER TABLE `Agent` ADD `lieu_execution` TEXT NULL ;
-- -------------
-- MLA 21/05/2009
ALTER TABLE `Agent` ADD `alerte_question_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
-- ---
ALTER TABLE `consultation` CHANGE `url_consultation_achat_publique` `url_consultation_achat_publique` TEXT NULL DEFAULT NULL ;
-- MLA 02/06/2009
ALTER TABLE `ModuleOrganisme` ADD `responsable_archive` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `Entreprise` CHANGE `nom` `nom` TEXT NOT NULL default '';

ALTER TABLE `GeolocalisationN2` ADD `valeur_avec_sous_categorie` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
-- MLA 04/06/2009
ALTER TABLE `ModuleOrganisme` CHANGE `responsable_archive` `interface_archive_arcade_pmi` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- NBE 15/06/2009

CREATE TABLE `Entreprise_info_exercice` (
  `id_Entreprise` int(11) NOT NULL default '0',
  `debutExerciceGlob` varchar(15) default NULL,
  `finExerciceGlob` varchar(15) default NULL,
  `ventesGlob` varchar(10) default NULL,
  `biensGlob` varchar(10) default NULL,
  `servicesGlob` varchar(10) default NULL,
  `totalGlob` varchar(10) default NULL,
  `annee_cloture_exercice` varchar(15) NOT NULL default '',
  `effectif_moyen` int(10) NOT NULL default '0',
  `effectif_encadrement` int(10) NOT NULL default '0',
  `pme` enum('1','0') default NULL,
  PRIMARY KEY  (`id_Entreprise`,`annee_cloture_exercice`)
) TYPE=InnoDB;

ALTER TABLE `Entreprise_info_exercice` CHANGE `ventesGlob` `ventesGlob` VARCHAR( 10 ) NULL DEFAULT '0',
CHANGE `biensGlob` `biensGlob` VARCHAR( 10 ) NULL DEFAULT '0',
CHANGE `servicesGlob` `servicesGlob` VARCHAR( 10 ) NULL DEFAULT '0',
CHANGE `totalGlob` `totalGlob` VARCHAR( 10 ) NULL DEFAULT '0',
CHANGE `effectif_moyen` `effectif_moyen` INT( 10 )  NULL DEFAULT '0',
CHANGE `effectif_encadrement` `effectif_encadrement` INT( 10 )  NULL DEFAULT '0';

-- MLA 16/07/2009


CREATE TABLE `chorus_numeros_marches` (
  `numero_marche` varchar(100) NOT NULL default '',
  `acronyme_organisme` varchar(100) NOT NULL default '',
  `id_decision` int(11) NOT NULL default '0',
  PRIMARY KEY  (`numero_marche`)
) TYPE=InnoDB;

-- LCH (25/08/2009) : Suppression des index inutiles sur GeolocalisationN2
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_2`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_3`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_4`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_5`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_6`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_7`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_8`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_9`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_10`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_11`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_12`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_13`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_14`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_15`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_16`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_17`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_18`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_19`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_20`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_21`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_22`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_23`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_24`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_25`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_26`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_27`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_28`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_29`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_30`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_31`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_32`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_33`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_34`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_35`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_36`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_37`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_38`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_39`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_40`;
ALTER TABLE `GeolocalisationN2` DROP INDEX `id_geolocalisationN1_41`;


ALTER TABLE `ModuleOrganisme` ADD `acces_agents_cfe` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `Justificatifs` ADD `date_fin_validite` DATE NOT NULL DEFAULT '0000-00-00' ;
ALTER TABLE `ModuleOrganisme` DROP `responsable_archive`;

CREATE TABLE `Chorus_numero_sequence` (
`id` INT( 50 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`numero` VARCHAR( 20 ) NOT NULL default ''
) TYPE = innodb;

-- MLA (09/09/209) :Ajouter la liste des abréviations de correspondance avec Marco

INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (20, NULL, '3', 'AOO', 'AOO');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (21, NULL, '4', 'ARC', 'AOR-C');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (22, NULL, '5', 'CRC', 'CR-C');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (23, NULL, '6', 'DCC', 'DC-C');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (24, NULL, '7', 'MNE', 'MN');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (25, NULL, '8', 'MNE', 'MN');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (26, NULL, '10', 'ATR', 'AUT');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (27, NULL, '12', 'COO', 'CO');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (28, NULL, '13', 'MNE', 'MN');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (29, NULL, '14', 'MNE', 'MN');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (30, NULL, '16', 'ATR', 'AUT');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (31, NULL, '17', 'COO', 'CO');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (32, NULL, '18', 'AOO', 'AOO');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (33, NULL, '19', 'ARC', 'AOR-C');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (34, NULL, '20', 'MNE', 'MN');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (35, NULL, '21', 'MAI', 'PA-INF');
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`, `abreviation_type_procedure_organisme`) VALUES (36, NULL, '22', 'MAI', 'PA-INF');
 
-- MLA (09/09/2009) : suppression de la colonne `abreviation_type_procedure_organisme`
ALTER TABLE InterfaceTypeProcedure DROP `abreviation_type_procedure_organisme`;

ALTER TABLE `consultation` ADD `org_denomination` VARCHAR( 250 ) NULL ;

UPDATE `GeolocalisationN2` set denomination2= 'VA' WHERE id='6423';
UPDATE `GeolocalisationN2` set denomination2= 'CC' WHERE id='6283';
UPDATE `GeolocalisationN2` set denomination2= 'FK' WHERE id='6308';

CREATE TABLE `chorus_noms_fichiers` (
`id_echange` INT( 11 ) NOT NULL ,
`nom_fichier` VARCHAR( 200 ) NOT NULL ,
PRIMARY KEY ( `id_echange` )
) TYPE = innodb;
ALTER TABLE `chorus_noms_fichiers` ADD `acronyme_organisme` VARCHAR(200) NOT NULL default ''; 
--  0003407: Gestion des Profils - Retirer l'habiltation Suivi des accès sur tous les profils et sur toutes les plates-formes 
UPDATE `HabilitationProfil` SET `suivi_acces` = '0';
-- 0003408: Gestion des Profils - Retirer les habilitations "Entité" du profil Administrateur Service
UPDATE `HabilitationProfil` SET `gestion_mapa` = '0', `gestion_type_validation` = '0', `administrer_procedure` = '0' WHERE `HabilitationProfil`.`id` ='2';


-- OKO le 08/10/2009 : Ajout d'un champ à la table `Justificatifs`
--
ALTER TABLE `Justificatifs` ADD `visible_par_agents` ENUM( '0', '1' ) NULL DEFAULT '0' AFTER `date_fin_validite`;

-- OKO le 09/10/2009 : Pour associer des documents du coffre-fort à une consultation

CREATE TABLE  `consultation_document_cfe` (
  `id` int(11) NOT NULL auto_increment,
  `id_justificatif` int(11) NOT NULL default '0',
  `ref_consultation` varchar(255) NOT NULL default '',
  `organisme_consultation` varchar(30) NOT NULL default '',
  `nom_fichier` varchar(255) NOT NULL default '',
  `id_entreprise` int(11) default NULL,
  `taille_document` varchar(80) default NULL,
  `id_blob` int(11) default NULL,
  `type_document` varchar(255) default NULL,
  `date_fin_validite` varchar(20) default '',
  PRIMARY KEY  (`id`),
  KEY `id_justificatif` (`id_justificatif`,`ref_consultation`,`id_entreprise`),
  KEY `id_entreprise` (`id_entreprise`),
  KEY `ref_consultation` (`ref_consultation`),
  KEY `organisme_consultation` (`organisme_consultation`)
) ENGINE=InnoDB  COMMENT='Pour associer des documents du coffre-fort à une consultation';


ALTER TABLE `consultation_document_cfe` ADD CONSTRAINT `consultation_document_cfe_ibfk_1` FOREIGN KEY (`ref_consultation`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `consultation_document_cfe` ADD CONSTRAINT `consultation_document_cfe_ibfk_2` FOREIGN KEY (`id_entreprise`) REFERENCES `Entreprise` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE `ModuleCommun` ADD `portail_defense_entreprise` ENUM( '0', '1' ) NOT NULL default '0';
ALTER TABLE `ModuleCommun` ADD `annonces_marches` ENUM( '0', '1' ) NOT NULL default '0';
ALTER TABLE `ModuleCommun` ADD `mpe_tgr` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `cfe_date_fin_validite_obligatoire` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Champ date de fin de validité obligatoire dans l''ajout d''un document du coffre-fort';
ALTER TABLE `ModuleCommun` ADD `associer_documents_cfe_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''associer les documents du coffre fort électronique à une consultation' AFTER `cfe_date_fin_validite_obligatoire`;
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_region` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ région dans le formulaire de création des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_province` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ province dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_telephone2` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ telephone 2 dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_telephone3` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ telephone 3 dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_cnss` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ numero du cnss dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_tax_prof` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ numero de la taxe professionnelle dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_rcnum` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ du numero du registe du commerce dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_rcville` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ du numero du registe du commerce dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_domaine_activite` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ domaines d\'activités dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_inscrit_code_nic` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `compte_entreprise_domaine_activite`;
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_code_ape` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `compte_inscrit_code_nic`;
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_declaration_honneur` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ declaration sur l\'honneur dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `search_entreprise_siren` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'pour afficher le champ siren dans le formulaire de recherche des entreprises';
ALTER TABLE `ModuleCommun` ADD `search_entreprise_nic` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'pour afficher le champ siret dans le formulaire de recherche des entreprises';
ALTER TABLE `ModuleCommun` ADD `search_entreprise_ape` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'pour afficher le champ code ape naf et nace dans le formulaire de recherche des entreprises';
ALTER TABLE `ModuleCommun` ADD `search_entreprise_domaines_activites` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ domaines d\'activités dans le formulaire de recherche des entreprises';
ALTER TABLE `ModuleCommun` ADD `search_entreprise_qualification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ qualification dans le formulaire de recherche des entreprises';
ALTER TABLE `ModuleCommun` ADD `search_entreprise_agrements` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher le champ agrements dans le formulaire de recherche des entreprises';
ALTER TABLE `ModuleCommun` ADD `search_entreprise_region` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher la region  dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `search_entreprise_province` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher la province inscrit defense dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_documents_commerciaux` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `search_entreprise_province`;
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_qualification` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_agrement` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_moyens_techniques` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_moyens_humains` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_prestations_realisees` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_activite_domaine_defense` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_chiffre_affaire_production_biens_services` enum('0','1') NOT NULL default '1' COMMENT 'vente de marchandises, production, biens, services';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_donnees_financieres` enum('0','1') NOT NULL default '0' COMMENT 'CA, besoin de financemet, cash flow, capacite d''endettement';
ALTER TABLE `ModuleCommun` ADD `enveloppe_offre_technique` ENUM( '0', '1' ) DEFAULT '0' NOT NULL COMMENT 'gestion de l''enveloppe offre technique ';
ALTER TABLE `ModuleCommun` ADD `enveloppe_anonymat` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'gestion de l''enveloppe offre technique ';
ALTER TABLE `ModuleCommun` ADD `publicite_format_xml` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gerer la visibilite de la publicite au format xml' AFTER `enveloppe_anonymat` ;
ALTER TABLE `ModuleCommun` ADD `article_133` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilite du bloc ''liste des marchés (articles 133)''dans le menu gauche coté agent' AFTER `publicite_format_xml` ;
ALTER TABLE `ModuleCommun` ADD `compte_inscrit_msg_ates` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'pour afficher mon compte utilisateur ou administrateur  dans le formulaire de creation de comptes inscrits';
ALTER TABLE `ModuleCommun` ADD `procedure_adaptee` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilité du module procedure adaptée dans le menu gauche et dans les habilitations' AFTER `compte_inscrit_msg_ates` ;
ALTER TABLE `ModuleCommun` ADD `menu_bdf` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le menu "Base de données des fournisseurs"' AFTER `procedure_adaptee` ;
ALTER TABLE `ModuleCommun` ADD `administration_metier_visualiser_entreprises` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le menu "Administration métier / visualiser les entreprises" ' AFTER `menu_bdf` ;
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_siren` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'pour afficher le champ sirent et siret dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` ADD `actions_entreprise_consultation_prolongee` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'si 0 Retirer les actions entreprise sur une consultation pdt l affichage prolongé ';
ALTER TABLE `ModuleCommun` ADD `telechargement_outil_verif_horodatage` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `actions_entreprise_consultation_prolongee`;  
ALTER TABLE `ModuleCommun` ADD `affichage_code_cpv` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `ModuleCommun` ADD `consultation_domaines_activites` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- OKO le 03/11/2009 : module gère la visiblité du lien Mesure de la dématérialisation dans les administration métier / statistiques
ALTER TABLE `ModuleCommun` ADD `statistiques_mesure_demat` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'gère la visiblité du lien Mesure de la dématérialisation dans les administration métier / statistiques' AFTER `consultation_domaines_activites` ;
ALTER TABLE `ModuleCommun` ADD `publication_procure` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Gère la visibilité du module ''Rendre accessible cette consultation dans toutes les langues au portail européen PROCURE''' AFTER `statistiques_mesure_demat` ;
ALTER TABLE `ModuleCommun` ADD `activation_compte_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module pour activer le compte entreprise pour qu''il s''affiche dans la recherche coté agent';

-- OTA le 11/11/2009
ALTER TABLE `ModuleCommun` ADD `masquer_fct_ates_pour_ues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'masquer les fonctions sur la page d''accueil de l''utilisateur entreprise admin pour l''utilisateur non admin' AFTER `activation_compte_entreprise`;
ALTER TABLE `ModuleCommun` ADD `menu_entreprise_toutes_les_consultations` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'affiche le lien ''toutes les consultations'' dans le menu gauche entreprise';
ALTER TABLE `ModuleCommun` ADD `menu_entreprise_consultations_en_cours` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'affiche le lien ''consultations en cours'' dans le menu gauche entreprise';

-- MMI le 11/11/2009
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_capital_social` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module pour activer le compte entreprise pour qu''il affiche le champs capital social ';
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_cp_obligatoire` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Module pour activer le compte entreprise pour qu''il affiche le champs code postal n''est pas obligatoire ';

ALTER TABLE `ModuleCommun` ADD `annuler_depot` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `compte_entreprise_cp_obligatoire`;

-- OKO le 24 novembre 2009
ALTER TABLE `ModuleCommun` ADD `traduire_entite_achat_arabe` ENUM( '0', '1' ) NULL DEFAULT '0' AFTER `annuler_depot`;
ALTER TABLE `ModuleCommun` ADD `traduire_organisme_arabe` ENUM( '0', '1' ) NULL DEFAULT '0' AFTER `traduire_entite_achat_arabe`;
ALTER TABLE `ModuleCommun` ADD `gestion_agent_mes_sous_services` ENUM( '0', '1' ) NULL DEFAULT '1' AFTER `traduire_organisme_arabe`;

-- MMI : 16/11/2009
ALTER TABLE `ModuleCommun` ADD `message_confirmation_inscrit_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module envoyer mail aux inscrits avc des informations sur le compte entreprise et la procedure pour actvier le compte';

ALTER TABLE `ModuleCommun` ADD `decision_date_notification` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Module pour afficher la date de notification dans la decision attributaire';
ALTER TABLE `ModuleCommun` ADD `decision_cp` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Module pour afficher le code postal dans la decision attributaire';
ALTER TABLE `ModuleCommun` ADD `decision_pmi_pme` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Module pour afficher le champ pmi /pme dans la decision attributaire';
ALTER TABLE `ModuleCommun` ADD `decision_tranche_budgetaire` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Module pour afficher la tranche budgetaire dans la decision attributaire';
ALTER TABLE `ModuleCommun` ADD `decision_nature_prestations` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Module pour afficher la nature de Prestations dans la decision attributaire';

ALTER TABLE `ModuleCommun` ADD `decision_objet_marche` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Module pour afficher objet marche dans la decision attributaire';

ALTER TABLE `ModuleCommun` ADD `decision_note` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module pour afficherla note dans la decision attributaire';
ALTER TABLE `ModuleCommun` ADD `decision_classement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module pour afficher le classement dans la decision attributaire';

ALTER TABLE `ModuleCommun` ADD `decision_fiche_recensement` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Module pour afficher le lien pour telecharger la fiche de recensement dans la decision attributaire';
ALTER TABLE `ModuleCommun` ADD `decision_afficher_detail_candidat_par_defaut` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module pour afficher la liste de tout les candidats dans la decision attributaire';

ALTER TABLE `ModuleCommun` CHANGE `decision_note` `decision_note` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module pour afficherla note dans la decision attributaire';
ALTER TABLE `ModuleCommun` CHANGE `decision_classement` `decision_classement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module pour afficher le classement dans la decision attributaire';

ALTER TABLE `ModuleCommun` ADD `liste_marches` ENUM( '0', '1' ) NOT NULL DEFAULT '0'  AFTER `decision_afficher_detail_candidat_par_defaut`;

ALTER TABLE `ModuleCommun` ADD `multi_linguisme_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0'  AFTER `liste_marches`;
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_ifu` ENUM( '0', '1' ) NOT NULL DEFAULT '0'  AFTER `multi_linguisme_agent`;

-- AEL : 09/12/2009
ALTER TABLE `ModuleCommun` ADD `gestion_organisme_par_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l Hyper admin de gérer les services par organismes. A besoin de mettre habilitation HyperAdmin à 1' AFTER `compte_entreprise_ifu`;
ALTER TABLE `ModuleCommun` ADD `utiliser_lucene` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'à 1 pour utiliser lucène, à 0 pour ne pas utilliser lucène' AFTER `gestion_organisme_par_agent`;
ALTER TABLE `ModuleCommun` ADD `utiliser_page_html_lieux_execution` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'à 1 pour utiliser la page .html des lieux executions, à 0 pour utiliser la page commun.LieuxExecution' AFTER `utiliser_lucene` ;
ALTER TABLE `ModuleCommun` ADD `prado_validateur_format_date` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'à 1 pour garder actifs les validateurs prado, à 0 sinon' AFTER `utiliser_page_html_lieux_execution`;
ALTER TABLE `ModuleCommun` ADD `prado_validateur_format_email` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'à 1 pour garder actifs les validateur email, à 0 sinon' AFTER `prado_validateur_format_date`;

-- KBE :23/12/2009
ALTER TABLE `ModuleCommun` ADD `socle_externe_ppp` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Socle externe PPP de e-bourgogne' AFTER `prado_validateur_format_email`;
-- AEL 18/01/2010
ALTER TABLE `ModuleCommun` ADD `validation_format_champs_stricte` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER socle_externe_ppp;
ALTER TABLE `ModuleCommun` ADD `poser_question_necessite_authentification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER validation_format_champs_stricte;
ALTER TABLE `ModuleCommun` ADD `autoriser_suppression_dernier_administrateur_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER poser_question_necessite_authentification;

ALTER TABLE `ModuleCommun` ADD `autoriser_modif_profil_inscrit_ates` ENUM( '0', '1' ) NULL DEFAULT '1' AFTER autoriser_suppression_dernier_administrateur_entreprise;
ALTER TABLE `ModuleCommun` ADD `unicite_reference_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER autoriser_modif_profil_inscrit_ates;
ALTER TABLE `ModuleCommun` ADD `champ_rc_obligatoires_ajout_registre_papier` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER unicite_reference_consultation;
ALTER TABLE `ModuleCommun` DROP `champs_obligatoires_ajout_registre_papier`;
ALTER TABLE `ModuleCommun` ADD `champ_adresse_obligatoires_ajout_registre_papier` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER champ_rc_obligatoires_ajout_registre_papier;
ALTER TABLE `ModuleCommun` ADD `telecharger_dce_sans_identification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'possiblité de télécharger le DCE anonymement' AFTER `champ_adresse_obligatoires_ajout_registre_papier`;
ALTER TABLE `ModuleCommun` ADD `champ_mail_obligatoire_ajout_registre_papier` ENUM( '0', '1' ) NOT NULL DEFAULT '0' ;
ALTER TABLE `ModuleCommun` ADD `gestion_entreprise_par_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' ;

ALTER TABLE `ModuleCommun` ADD `autoriser_caracteres_speciaux_dans_reference` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `gestion_entreprise_par_agent` ;
-- OKO: le 22-01-2010
ALTER TABLE `ModuleCommun` ADD `indicateurs_cles` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet l''activation du module ''indicateurs clés''';
ALTER TABLE `ModuleCommun` ADD `ajout_rpa_champ_email` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `indicateurs_cles` ;
ALTER TABLE `ModuleCommun` ADD `ajout_rpa_champ_telephone` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `ajout_rpa_champ_email` ;
ALTER TABLE `ModuleCommun` ADD `ajout_rpa_champ_fax` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `ajout_rpa_champ_telephone` ;
ALTER TABLE `ModuleCommun` ADD `question_sans_pj` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `ajout_rpa_champ_fax`;
ALTER TABLE `ModuleCommun` ADD `url_demarche_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `question_sans_pj`;
ALTER TABLE `ModuleCommun` ADD `url_demarche_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `url_demarche_agent`;
-- MMI le 09/032010
ALTER TABLE `ModuleCommun` ADD `siret_detail_entite_achat` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `url_demarche_entreprise`;
ALTER TABLE `ModuleCommun` ADD `presence_elu` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `siret_detail_entite_achat`;

-- MMI le 10/03/2010
ALTER TABLE `ModuleCommun` ADD `gerer_les_services` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `presence_elu`;
ALTER TABLE `ModuleCommun` ADD `gerer_mon_service` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `gerer_les_services`;

-- AEL 24/01/2010
ALTER TABLE `ModuleCommun` ADD `depouillement_enveloppe_depend_RAT_enveloppe_precedente` ENUM( '0', '1' ) NOT NULL DEFAULT '0'  ;
ALTER TABLE `ModuleCommun` ADD `inscription_libre_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `ModuleCommun` ADD `consultation_adresse_retrais_dossiers` ENUM( '0', '1' ) NOT NULL DEFAULT '0'  ;
ALTER TABLE `ModuleCommun` ADD `consultation_adresse_depot_offres` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_adresse_retrais_dossiers`;
ALTER TABLE `ModuleCommun` ADD `consultation_caution_provisoire` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_adresse_depot_offres`;
ALTER TABLE `ModuleCommun` ADD `consultation_lieu_ouverture_plis` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_caution_provisoire`;

ALTER TABLE `ModuleCommun` ADD `consultation_qualification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_lieu_ouverture_plis`;
ALTER TABLE `ModuleCommun` ADD `consultation_agrement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_qualification`;
ALTER TABLE `ModuleCommun` ADD `consultation_echantillons_demandes` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_agrement`;
ALTER TABLE `ModuleCommun` ADD `consultation_reunion` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_echantillons_demandes`;
ALTER TABLE `ModuleCommun` ADD `consultation_visite_des_lieux` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_reunion`;

ALTER TABLE `ModuleCommun` ADD `consultation_prix_acquisition` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_visite_des_lieux`;
ALTER TABLE `ModuleCommun` ADD `resultat_analyse_avant_decision` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_prix_acquisition` ;
ALTER TABLE `ModuleCommun` ADD `creation_inscrit_par_ates` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `resultat_analyse_avant_decision`;
ALTER TABLE `ModuleCommun` ADD `consultation_variantes_autorisees` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `creation_inscrit_par_ates`;
ALTER TABLE `ModuleCommun` ADD `recherche_avancee_par_type_org` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_variantes_autorisees`;
ALTER TABLE `ModuleCommun` ADD `societes_exclues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `recherche_avancee_par_type_org` ;
ALTER TABLE `ModuleCommun` ADD `recherche_avancee_par_domaines_activite` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `societes_exclues`;
ALTER TABLE `ModuleCommun` ADD `recherche_avancee_par_qualification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `recherche_avancee_par_domaines_activite`;
ALTER TABLE `ModuleCommun` ADD `recherche_avancee_par_agrement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `recherche_avancee_par_qualification`;
ALTER TABLE `ModuleCommun` ADD `contact_administratif_dans_detail_consultation_cote_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `recherche_avancee_par_agrement` ;
ALTER TABLE `ModuleCommun` ADD `consultation_pieces_dossiers` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `contact_administratif_dans_detail_consultation_cote_entreprise`;
ALTER TABLE `ModuleCommun` ADD `gerer_adresses_service` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `consultation_pieces_dossiers`;

ALTER TABLE `ModuleCommun` ADD `traduire_annonces` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `gerer_adresses_service` ;
ALTER TABLE `ModuleCommun` ADD `affichier_bloc_actions_dans_detail_annonces` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `traduire_annonces` ;
ALTER TABLE `ModuleCommun` ADD `autoriser_une_seule_reponse_principale_par_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `affichier_bloc_actions_dans_detail_annonces` ;

ALTER TABLE `ModuleCommun` ADD `generation_avis` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `autoriser_une_seule_reponse_principale_par_entreprise` ;

-- OKO: le 19/03/2010
ALTER TABLE `ModuleCommun` ADD `appliquer_donnees_ensemble_lots` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'permet de reprendre automatiquement pour tous les lots de la consultation les données spécifiques saisies pour un lot' AFTER `generation_avis`;
-- OKO: le 26 mars 2010
ALTER TABLE `ModuleCommun` ADD `autre_annonce_extrait_pv` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Pour la gestion du type d''annonce "Extrait de PV"' AFTER `appliquer_donnees_ensemble_lots` ;
ALTER TABLE `ModuleCommun` ADD `autre_annonce_rapport_achevement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Pour la gestion du type d''annonce "Rapport d''achevement"' AFTER `autre_annonce_extrait_pv` ;
ALTER TABLE `ModuleCommun` ADD `ajout_fichier_joint_autre_annonce` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Pour l''ajout des pièces jointes dans les autres annonces' AFTER `autre_annonce_rapport_achevement` ;

ALTER TABLE `ModuleCommun` ADD `consultation_mode_passation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'afficher les choix de mode passation au rabais et sur offre de prix' AFTER `ajout_fichier_joint_autre_annonce` ;
-- MLA (09/10/2009) ajout d'autres champs sur la popup des dirigeants

ALTER TABLE `responsableengagement` ADD `email` VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `responsableengagement` ADD `telephone` VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `responsableengagement` ADD `fax` VARCHAR( 100 ) NULL DEFAULT NULL ;
 
-- MLA (09/09/2009)
-- 
-- Table structure for table `Prestation`
-- 
CREATE TABLE `Prestation` (
  `id` int(11) NOT NULL auto_increment,
  `num_marche` varchar(255) default NULL,
  `type_procedure` int(11) default NULL,
  `objet` longtext,
  `montant` varchar(255) default NULL,
  `maitre_ouvrage` varchar(255) default NULL,
  `date_debut_execution` varchar(20) default NULL,
  `date_fin_execution` varchar(20) default NULL,
  `id_entreprise` int(11) default NULL,
  PRIMARY KEY  (`ID`),
  KEY `id_entreprise` (`ID_ENTREPRISE`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `Prestation` ADD FOREIGN KEY ( `id_entreprise` ) REFERENCES `Entreprise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;

-- -------------------
-- MLA (12/10/2009) l'ajout des champs

ALTER TABLE `Entreprise_info_exercice` ADD `chiffre_affaires` VARCHAR( 255 ) NULL DEFAULT NULL ;
ALTER TABLE `Entreprise_info_exercice` ADD   `besoin_excedent_financement` VARCHAR( 255 ) NULL DEFAULT NULL ;
ALTER TABLE `Entreprise_info_exercice` ADD   `cash_flow` VARCHAR( 255 ) NULL DEFAULT NULL ;
ALTER TABLE `Entreprise_info_exercice` ADD   `capacite_endettement` VARCHAR( 255 ) NULL DEFAULT NULL ;
 
-- 
-- Structure de la table `Agrement`
-- 

CREATE TABLE `Agrement` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  PRIMARY KEY  (`ID`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

-- 
-- Contenu de la table `Agrement`
-- 


-- --------------------
-- MLA(12/10/2009) ajout de champs sur la table entreprise



ALTER TABLE `Entreprise` ADD `region` VARCHAR( 250 ) NULL ;
ALTER TABLE `Entreprise` ADD `province` VARCHAR( 250 ) NULL ;
ALTER TABLE `Entreprise` ADD `telephone2` VARCHAR( 250 ) NULL ;
ALTER TABLE `Entreprise` ADD `telephone3` VARCHAR( 250 ) NULL ;
ALTER TABLE `Entreprise` ADD `cnss` VARCHAR( 250 ) NULL ;
ALTER TABLE `Entreprise` ADD `rc_num` VARCHAR( 250 ) NULL ;
ALTER TABLE `Entreprise` ADD `rc_ville` VARCHAR( 250 ) NULL ;
ALTER TABLE `Entreprise` ADD `domaines_activites` VARCHAR(255) NULL ;
ALTER TABLE `Entreprise` ADD `num_tax` VARCHAR( 250 ) NULL ;
ALTER TABLE `Entreprise` ADD `documents_commerciaux` INT( 11 ) NULL ;
ALTER TABLE `Entreprise` ADD `intitule_documents_commerciaux` VARCHAR( 255 ) NULL ;
ALTER TABLE `Entreprise` ADD `taille_documents_commerciaux` VARCHAR( 50 ) NULL  AFTER `intitule_documents_commerciaux`;
ALTER TABLE `Entreprise` ADD `qualification` VARCHAR( 255 ) NULL ;
ALTER TABLE `Entreprise` ADD `agrement` VARCHAR( 255 ) NULL ;
ALTER TABLE `Entreprise` ADD `moyens_technique` LONGTEXT NULL ;
ALTER TABLE `Entreprise` ADD `moyens_humains` LONGTEXT NULL ;


ALTER TABLE `Entreprise` ADD FOREIGN KEY ( `region` ) REFERENCES `GeolocalisationN1` (`id`);

ALTER TABLE `Entreprise` ADD FOREIGN KEY ( `province` ) REFERENCES `GeolocalisationN2` (`id`);

ALTER TABLE `Entreprise` CHANGE `siren` `siren` VARCHAR( 20 ) NULL;
ALTER TABLE `Entreprise` CHANGE `description_activite` `description_activite` TEXT;

ALTER TABLE `Entreprise` ADD INDEX `region` (`region`) ; 
ALTER TABLE `Entreprise` ADD INDEX `province` (`province`) ;
ALTER TABLE `Entreprise` ADD INDEX `domaines_activites` ( `domaines_activites`);
ALTER TABLE `Entreprise` ADD INDEX `qualification`  (`qualification` ) ;

UPDATE `Langue` SET `libelle` = 'français' WHERE `Langue`.`id_langue` =1  ;

UPDATE `Langue` SET `libelle` = 'anglais' WHERE `Langue`.`id_langue` =2  ;

UPDATE `Langue` SET `libelle` = 'espagnol' WHERE `Langue`.`id_langue` =3  ;

-- (OKO )
ALTER TABLE `consultation` ADD `titre_ar` LONGTEXT NULL AFTER `titre_cz` ;
ALTER TABLE `consultation` ADD `resume_ar` LONGTEXT NULL AFTER `resume_cz` ;
ALTER TABLE `consultation` ADD `accessibilite_ar` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `accessibilite_su` ;

CREATE TABLE IF NOT EXISTS `Qualification` (
  `id` int(11) NOT NULL auto_increment COMMENT 'clé primaire',
  `id_interne` varchar(20) NOT NULL default '' COMMENT 'identifiant interne pour gérer le classement des qualifications',
  `libelle` varchar(255) default NULL,
  `id_interne_parent` varchar(20) default NULL COMMENT 'id interne du parent',
  PRIMARY KEY  (`id`),
  KEY `id_interne` (`id_interne`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `SousCategorie` (
  `id` varchar(30) NOT NULL default '',
  `id_categorie` varchar(50) NOT NULL default '',
  `libelle` varchar(250) NOT NULL default '',
  `libelle_ar` varchar(250) default NULL,
  `libelle_fr` varchar(250) default NULL,
  `libelle_en` varchar(250) default NULL,
  `libelle_es` varchar(250) default NULL,
  `libelle_su` varchar(250) default NULL,
  `libelle_du` varchar(250) default NULL,
  `libelle_cz` varchar(250) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

-- AEL : 29/10/2009 
ALTER TABLE `chorus_noms_fichiers` ADD `type_fichier` CHAR( 3 ) NOT NULL DEFAULT 'FEN';
ALTER TABLE chorus_noms_fichiers DROP PRIMARY KEY;
ALTER TABLE `chorus_noms_fichiers` ADD PRIMARY KEY ( `id_echange` , `nom_fichier` ) ;



ALTER TABLE `Langue` ADD `theme_specifique` ENUM( '0', '1' ) NOT NULL DEFAULT '0';


ALTER TABLE `Offre` ADD `depot_annule` ENUM( '0', '1' ) NULL DEFAULT '0' ;
ALTER TABLE `Offre` ADD `date_annulation` VARCHAR( 20 ) NULL ;

-- 09/11/2009 : MMI
ALTER TABLE `Entreprise` ADD `compte_actif` INT( 1 ) NOT NULL DEFAULT '1';
ALTER TABLE `Entreprise` ADD `capital_social` VARCHAR( 50 ) NOT NULL default '';




-- OKO le 25-11-2009
ALTER TABLE `consultation_document_cfe` CHANGE `id_blob` `id_blob` INT( 11 ) NOT NULL default '';
ALTER TABLE `Justificatifs` CHANGE `justificatif` `justificatif` INT( 11 ) NOT NULL default '';

-- OKO le 05/11/2009 : Pour la gestion des langues dans les agrements
ALTER TABLE `Agrement` ADD `libelle_ar` VARCHAR( 255 ) NULL AFTER `libelle` ;
ALTER TABLE `Agrement` ADD `libelle_fr` VARCHAR( 255 ) NULL AFTER `libelle_ar` ;
ALTER TABLE `Agrement` ADD `libelle_en` VARCHAR( 255 ) NULL AFTER `libelle_fr` ;
ALTER TABLE `Agrement` ADD `libelle_es` VARCHAR( 255 ) NULL AFTER `libelle_en` ;
ALTER TABLE `Agrement` ADD `libelle_su` VARCHAR( 255 ) NULL AFTER `libelle_es` ;
ALTER TABLE `Agrement` ADD `libelle_du` VARCHAR( 255 ) NULL AFTER `libelle_su` ;
ALTER TABLE `Agrement` ADD `libelle_cz` VARCHAR( 255 ) NULL AFTER `libelle_du` ;
-- OKO le 05/11/2009 : Pour la gestion des langues dans les qualificatifs
ALTER TABLE `Qualification` ADD `libelle_ar` VARCHAR( 255 ) NULL AFTER `id_interne_parent` ;
ALTER TABLE `Qualification` ADD `libelle_fr` VARCHAR( 255 ) NULL AFTER `libelle_ar` ;
ALTER TABLE `Qualification` ADD `libelle_en` VARCHAR( 255 ) NULL AFTER `libelle_fr` ;
ALTER TABLE `Qualification` ADD `libelle_es` VARCHAR( 255 ) NULL AFTER `libelle_en` ;
ALTER TABLE `Qualification` ADD `libelle_su` VARCHAR( 255 ) NULL AFTER `libelle_es` ;
ALTER TABLE `Qualification` ADD `libelle_du` VARCHAR( 255 ) NULL AFTER `libelle_su` ;
ALTER TABLE `Qualification` ADD `libelle_cz` VARCHAR( 255 ) NULL AFTER `libelle_du` ;
-- OKO : le 09-11-2009 (Traduction)
ALTER TABLE `Organisme` ADD `denomination_org_ar` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Organisme` ADD `description_org_ar` MEDIUMTEXT NOT NULL  default '';
ALTER TABLE `Organisme` ADD `adresse_ar` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `ville_ar` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `adresse2_ar` VARCHAR( 100 ) NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `pays_ar` VARCHAR( 150 ) NOT NULL default '' ;

ALTER TABLE `Organisme` ADD `denomination_org_fr` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `description_org_fr` MEDIUMTEXT NOT NULL default '';
ALTER TABLE `Organisme` ADD `adresse_fr` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `ville_fr` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Organisme` ADD `adresse2_fr` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Organisme` ADD `pays_fr` VARCHAR( 150 ) NOT NULL default '' ;

ALTER TABLE `Organisme` ADD `denomination_org_es` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `description_org_es` MEDIUMTEXT NOT NULL  default '';
ALTER TABLE `Organisme` ADD `adresse_es` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `ville_es` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `adresse2_es` VARCHAR( 100 ) NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `pays_es` VARCHAR( 150 ) NOT NULL default '';

ALTER TABLE `Organisme` ADD `denomination_org_en` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Organisme` ADD `description_org_en` MEDIUMTEXT NOT NULL  default '';
ALTER TABLE `Organisme` ADD `adresse_en` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `ville_en` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `adresse2_en` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Organisme` ADD `pays_en` VARCHAR( 150 ) NOT NULL  default '';

ALTER TABLE `Organisme` ADD `denomination_org_su` VARCHAR( 100 ) NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `description_org_su` MEDIUMTEXT NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `adresse_su` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `ville_su` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `adresse2_su` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `pays_su` VARCHAR( 150 ) NOT NULL  default '' ;

ALTER TABLE `Organisme` ADD `denomination_org_du` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Organisme` ADD `description_org_du` MEDIUMTEXT NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `adresse_du` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Organisme` ADD `ville_du` VARCHAR( 100 ) NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `adresse2_du` VARCHAR( 100 ) NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `pays_du` VARCHAR( 150 ) NOT NULL default '' ;

ALTER TABLE `Organisme` ADD `denomination_org_cz` VARCHAR( 100 ) NOT NULL  default '';
ALTER TABLE `Organisme` ADD `description_org_cz` MEDIUMTEXT NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `adresse_cz` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Organisme` ADD `ville_cz` VARCHAR( 100 ) NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `adresse2_cz` VARCHAR( 100 ) NOT NULL default '' ;
ALTER TABLE `Organisme` ADD `pays_cz` VARCHAR( 150 ) NOT NULL default '' ;

-OKO: le 10-11-2009 (Mise à jour pour traduction)
ALTER TABLE `GeolocalisationN0` ADD `denomination_ar` VARCHAR( 40 ) NOT NULL default '' ;
ALTER TABLE `GeolocalisationN0` ADD `libelle_selectionner_ar` VARCHAR( 40 ) NOT NULL default '' ;
ALTER TABLE `GeolocalisationN0` ADD `libelle_tous_ar` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_Aucun_ar` VARCHAR( 40 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN0` ADD `denomination_su` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_selectionner_su` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_tous_su` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_Aucun_su` VARCHAR( 40 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN0` ADD `denomination_du` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_selectionner_du` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_tous_du` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_Aucun_du` VARCHAR( 40 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN0` ADD `denomination_cz` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_selectionner_cz` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_tous_cz` VARCHAR( 40 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN0` ADD `libelle_Aucun_cz` VARCHAR( 40 ) NOT NULL default '';

-OKO: le 10-11-2009 (Mise à jour pour traduction)
ALTER TABLE `GeolocalisationN1` ADD `denomination1_ar` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN1` ADD `denomination2_ar` VARCHAR( 100 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN1` ADD `denomination1_fr` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN1` ADD `denomination2_fr` VARCHAR( 100 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN1` ADD `denomination1_en` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN1` ADD `denomination2_en` VARCHAR( 100 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN1` ADD `denomination1_es` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN1` ADD `denomination2_es` VARCHAR( 100 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN1` ADD `denomination1_su` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN1` ADD `denomination2_su` VARCHAR( 100 ) NOT NULL ;

ALTER TABLE `GeolocalisationN1` ADD `denomination1_du` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN1` ADD `denomination2_du` VARCHAR( 100 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN1` ADD `denomination1_cz` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN1` ADD `denomination2_cz` VARCHAR( 100 ) NOT NULL default '';

-OKO: le 10-11-2009 (Mise à jour pour traduction)
ALTER TABLE `GeolocalisationN2` ADD `denomination1_ar` VARCHAR( 50 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN2` ADD `denomination2_ar` VARCHAR( 30 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN2` ADD `denomination1_fr` VARCHAR( 50 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN2` ADD `denomination2_fr` VARCHAR( 30 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN2` ADD `denomination1_en` VARCHAR( 50 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN2` ADD `denomination2_en` VARCHAR( 30 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN2` ADD `denomination1_es` VARCHAR( 50 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN2` ADD `denomination2_es` VARCHAR( 30 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN2` ADD `denomination1_su` VARCHAR( 50 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN2` ADD `denomination2_su` VARCHAR( 30 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN2` ADD `denomination1_du` VARCHAR( 50 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN2` ADD `denomination2_du` VARCHAR( 30 ) NOT NULL default '';

ALTER TABLE `GeolocalisationN2` ADD `denomination1_cz` VARCHAR( 50 ) NOT NULL default '';
ALTER TABLE `GeolocalisationN2` ADD `denomination2_cz` VARCHAR( 30 ) NOT NULL default '';


ALTER TABLE `CategorieLot` ADD `description_detail_ar` LONGTEXT NULL AFTER `description_detail_cz` ;
ALTER TABLE `CategorieLot` ADD `description_ar` LONGTEXT NULL AFTER `description_cz` ;

-- OKO: le 27-11-2009
ALTER TABLE `consultation_document_cfe` ADD `horodatage` LONGBLOB NOT NULL default '';
ALTER TABLE `consultation_document_cfe` ADD `untrusteddate` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_ar` VARCHAR( 100 ) NULL AFTER `libelle_type_procedure_cz`;
ALTER TABLE `TypeAvis` ADD `intitule_avis_ar` VARCHAR( 100 ) NULL AFTER `intitule_avis_cz`;
ALTER TABLE `CategorieConsultation` ADD `libelle_ar` VARCHAR( 100 ) NULL AFTER `libelle_cz`;

-- MMI : 04/12/2009 

CREATE TABLE IF NOT EXISTS `Referentiel` (
  `id_referentiel` int(10) NOT NULL auto_increment,
  `libelle_referentiel` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`id_referentiel`)
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `ValeurReferentiel` (
  `id` int(10) NOT NULL auto_increment,
  `id_referentiel` int(10) NOT NULL default '0',
  `libelle_valeur_referentiel` varchar(200) NOT NULL default '',
  `libelle_2` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

ALTER TABLE `Referentiel` ADD INDEX `id_referentiel` ( `id_referentiel` ) ;
ALTER TABLE `ValeurReferentiel` ADD INDEX `id_referentiel` ( `id_referentiel` ) ;
ALTER TABLE `ValeurReferentiel` ADD FOREIGN KEY ( `id_referentiel` ) REFERENCES `Referentiel` (`id_referentiel`);
ALTER TABLE `Entreprise` ADD `ifu` VARCHAR( 200 ) NOT NULL default '';



ALTER TABLE `formejuridique` CHANGE `formejuridique` `formejuridique` VARCHAR( 255 ) NOT NULL default '';
ALTER TABLE `formejuridique` ADD `ordre` INT( 5 ) NULL ;
ALTER TABLE `formejuridique` ADD INDEX `ordre` ( `ordre` ) ; 

ALTER TABLE `HabilitationAgent` ADD `gestion_centrale_pub` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `gestion_centrale_pub` ENUM( '0', '1' ) NULL DEFAULT '0';

ALTER TABLE `HabilitationAgent` ADD `Gestion_Compte_Groupe_Moniteur` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `Gestion_Compte_Groupe_Moniteur` enum('0','1') NOT NULL default '0';

ALTER TABLE `HabilitationAgent` ADD `ouvrir_offre_technique_en_ligne` enum('0','1') NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD `ouvrir_offre_technique_en_ligne` enum('0','1') NOT NULL default '0';

ALTER TABLE `HabilitationAgent` ADD  `ouvrir_offre_technique_a_distance` enum('0','1') NOT NULL default '0';
ALTER TABLE `HabilitationProfil` ADD  `ouvrir_offre_technique_a_distance` enum('0','1') NOT NULL default '0';

-- MMI le 25/11/2009
ALTER TABLE `HabilitationAgent` ADD `activation_compte_entreprise` ENUM( '0', '1' ) NULL DEFAULT '0' AFTER `ouvrir_offre_technique_a_distance`;
ALTER TABLE `HabilitationProfil` ADD `activation_compte_entreprise` ENUM( '0', '1' ) NULL DEFAULT '0'AFTER `ouvrir_offre_technique_a_distance`;
-- AEL
ALTER TABLE `HabilitationAgent` ADD `importer_enveloppe` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `activation_compte_entreprise`; 
ALTER TABLE `HabilitationProfil` ADD `importer_enveloppe` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `activation_compte_entreprise`; 

-- OKO: le 29-12-2009

ALTER TABLE `HabilitationAgent` ADD `suivi_seul_registre_depots_papier` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `suivi_seul_registre_depots_papier` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `HabilitationAgent` ADD `suivi_seul_registre_retraits_papier` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `suivi_seul_registre_retraits_papier` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `HabilitationAgent` ADD `suivi_seul_registre_questions_papier` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `suivi_seul_registre_questions_papier` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `HabilitationAgent` ADD `suivi_seul_registre_depots_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationProfil` ADD `suivi_seul_registre_depots_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `HabilitationAgent` ADD `suivi_seul_registre_retraits_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationProfil` ADD `suivi_seul_registre_retraits_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `HabilitationAgent` ADD `suivi_seul_registre_questions_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `HabilitationProfil` ADD `suivi_seul_registre_questions_electronique` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

-- OKO le 14-01-2010
ALTER TABLE `HabilitationAgent` ADD `modifier_consultation_mapa_inferieur_montant_apres_validation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `modifier_consultation_mapa_inferieur_montant_apres_validation` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `HabilitationAgent` ADD `modifier_consultation_mapa_superieur_montant_apres_validation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `modifier_consultation_mapa_superieur_montant_apres_validation` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `HabilitationAgent` ADD `modifier_consultation_procedures_formalisees_apres_validation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `modifier_consultation_procedures_formalisees_apres_validation` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `HabilitationAgent` ADD `gerer_les_entreprises` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `gerer_les_entreprises` ENUM( '0', '1' ) NULL DEFAULT '0';
--OKO le 05/02/2010
ALTER TABLE `HabilitationAgent` ADD `portee_societes_exclues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent de creer une société exclue pour un organisme';
ALTER TABLE `HabilitationProfil` ADD `portee_societes_exclues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent de creer une société exclue pour un organisme';

ALTER TABLE `HabilitationAgent` ADD `portee_societes_exclues_tous_organismes` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent de creer une société exclue pour un organisme pour l''ensemble des organismes';
ALTER TABLE `HabilitationProfil` ADD `portee_societes_exclues_tous_organismes` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent de creer une société exclue pour un organisme pour l''ensemble des organismes';

ALTER TABLE `HabilitationAgent` ADD `modifier_societes_exclues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent de modifier une societe exclue';
ALTER TABLE `HabilitationProfil` ADD `modifier_societes_exclues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent de modifier une societe exclue';

ALTER TABLE `HabilitationAgent` ADD `supprimer_societes_exclues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent de supprimer une societe exclue';
ALTER TABLE `HabilitationProfil` ADD `supprimer_societes_exclues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent de supprimer une societe exclue';

ALTER TABLE `HabilitationAgent` ADD `resultat_analyse` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `supprimer_societes_exclues` ;
ALTER TABLE `HabilitationProfil` ADD `resultat_analyse` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `supprimer_societes_exclues` ;

ALTER TABLE `HabilitationAgent` ADD `gerer_adresses_service` ENUM( '0', '1' ) NULL DEFAULT '0' AFTER `resultat_analyse`;
ALTER TABLE `HabilitationProfil` ADD `gerer_adresses_service` ENUM( '0', '1' ) NULL DEFAULT '0' AFTER `resultat_analyse`;

-- KBE 15/01/2010
ALTER TABLE `responsableengagement` ADD `id_initial` INT( 11 ) default '0' ;


ALTER TABLE `Langue` ADD `obligatoire_pour_publication_consultation` ENUM( '0', '1' ) NULL DEFAULT '0';

-- OKO: le 05-01-2010
ALTER TABLE `ModuleOrganisme` ADD `desarchivage_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la fonction de desarchivage d''une consultation';

-- KBE 09/02/2010 
ALTER TABLE `ModuleOrganisme` ADD `alimentation_automatique_liste_invites` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Alimentation automatique de la liste des invités d''une consultation par la désignation du service associé';



ALTER TABLE `consultation` ADD `domaines_activites` VARCHAR( 250 )  NULL DEFAULT '';
ALTER TABLE `consultation` ADD `id_affaire` INT( 11 ) NULL ;

-- MMI 26/01/2010
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers` VARCHAR( 255 ) NOT NULL default '' AFTER `id_affaire`;
ALTER TABLE `consultation` ADD `caution_provisoire` VARCHAR( 255 ) NOT NULL default '' AFTER `adresse_retrais_dossiers`;
ALTER TABLE `consultation` ADD `adresse_depot_offres` VARCHAR( 255 ) NOT NULL default '' AFTER `caution_provisoire`;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis` VARCHAR( 255 ) NOT NULL default '' AFTER `adresse_depot_offres`;
ALTER TABLE `consultation` ADD `prix_aquisition_plans` VARCHAR( 255 ) NOT NULL default '' AFTER `lieu_ouverture_plis`;
-- MMI 04/02/10
ALTER TABLE `consultation` ADD `qualification` VARCHAR( 250 ) NOT NULL default '' AFTER `prix_aquisition_plans`;
ALTER TABLE `consultation` ADD `agrements` VARCHAR( 250 ) NOT NULL default '' AFTER `qualification`;
ALTER TABLE `consultation` ADD `add_echantillion` VARCHAR( 250 ) NOT NULL default '' AFTER `agrements`;
ALTER TABLE `consultation` ADD `date_limite_echantillion` VARCHAR( 50 ) NOT NULL default '' AFTER `add_echantillion`;
ALTER TABLE `consultation` ADD `add_reunion` VARCHAR( 250 ) NOT NULL default '' AFTER `date_limite_echantillion`;
ALTER TABLE `consultation` ADD `date_reunion` VARCHAR( 50 ) NOT NULL default '' AFTER `add_reunion`;
ALTER TABLE `consultation` ADD `variantes` CHAR( 1 ) NOT NULL default '' AFTER `date_reunion`;

ALTER TABLE `consultation` ADD `adresse_depot_offres_ar` VARCHAR( 255 ) NOT NULL default '' AFTER `variantes`;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_ar` VARCHAR( 255 ) NOT NULL default '' AFTER `adresse_depot_offres_ar`;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_ar` VARCHAR( 255 ) NOT NULL default '' AFTER `lieu_ouverture_plis_ar`;
ALTER TABLE `consultation` ADD `pieces_dossier_admin` VARCHAR( 255 ) NOT NULL default '' AFTER `adresse_retrais_dossiers_ar`;
ALTER TABLE `consultation` ADD `pieces_dossier_tech` VARCHAR( 255 ) NOT NULL default '' AFTER `pieces_dossier_admin`;
ALTER TABLE `consultation` ADD `pieces_dossier_additif` VARCHAR( 255 ) NOT NULL default '' AFTER `pieces_dossier_tech`;
ALTER TABLE `consultation` ADD `id_rpa` INT(11) NULL DEFAULT NULL AFTER `pieces_dossier_additif`;

ALTER TABLE `consultation` ADD `detail_consultation_fr` text AFTER `id_rpa`;
ALTER TABLE `consultation` ADD `detail_consultation_en` text AFTER `detail_consultation_fr`;
ALTER TABLE `consultation` ADD `detail_consultation_es` text AFTER `detail_consultation_en`;
ALTER TABLE `consultation` ADD `detail_consultation_su` text AFTER `detail_consultation_es`;
ALTER TABLE `consultation` ADD `detail_consultation_du` text AFTER `detail_consultation_su`;
ALTER TABLE `consultation` ADD `detail_consultation_cz` text AFTER `detail_consultation_du`;
ALTER TABLE `consultation` ADD `detail_consultation_ar` text AFTER `detail_consultation_cz`;

ALTER TABLE `consultation` ADD `echantillon` ENUM( '0', '1' ) NOT NULL default '0' AFTER `detail_consultation_ar`;
ALTER TABLE `consultation` ADD `reunion` ENUM( '0', '1' ) NOT NULL default '0' AFTER `echantillon`;
ALTER TABLE `consultation` ADD `visites_lieux` ENUM( '0', '1' ) NOT NULL default '0' AFTER `reunion`;

ALTER TABLE `consultation` ADD `variante_calcule` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `visites_lieux`;

ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_fr` VARCHAR( 255 ) NULL AFTER `variante_calcule` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_en` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_fr` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_es` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_en` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_su` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_es` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_du` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_su` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_cz` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_du` ;

ALTER TABLE `consultation` ADD `adresse_depot_offres_fr` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_cz` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_en` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_fr` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_es` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_en` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_su` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_es` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_du` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_su` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_cz` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_du` ;


ALTER TABLE `consultation` ADD `lieu_ouverture_plis_fr` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_cz` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_en` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_fr` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_es` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_en` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_su` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_es` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_du` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_su` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_cz` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_du` ;

ALTER TABLE `consultation` ADD `add_echantillion_fr` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_cz` ;
ALTER TABLE `consultation` ADD `add_echantillion_en` VARCHAR( 255 ) NULL AFTER `add_echantillion_fr` ;
ALTER TABLE `consultation` ADD `add_echantillion_es` VARCHAR( 255 ) NULL AFTER `add_echantillion_en` ;
ALTER TABLE `consultation` ADD `add_echantillion_su` VARCHAR( 255 ) NULL AFTER `add_echantillion_es` ;
ALTER TABLE `consultation` ADD `add_echantillion_du` VARCHAR( 255 ) NULL AFTER `add_echantillion_su` ;
ALTER TABLE `consultation` ADD `add_echantillion_cz` VARCHAR( 255 ) NULL AFTER `add_echantillion_du` ;
ALTER TABLE `consultation` ADD `add_echantillion_ar` VARCHAR( 255 ) NULL AFTER `add_echantillion_cz` ;


ALTER TABLE `consultation` ADD `add_reunion_fr` VARCHAR( 255 ) NULL AFTER `add_echantillion_ar` ;
ALTER TABLE `consultation` ADD `add_reunion_en` VARCHAR( 255 ) NULL AFTER `add_reunion_fr` ;
ALTER TABLE `consultation` ADD `add_reunion_es` VARCHAR( 255 ) NULL AFTER `add_reunion_en` ;
ALTER TABLE `consultation` ADD `add_reunion_su` VARCHAR( 255 ) NULL AFTER `add_reunion_es` ;
ALTER TABLE `consultation` ADD `add_reunion_du` VARCHAR( 255 ) NULL AFTER `add_reunion_su` ;
ALTER TABLE `consultation` ADD `add_reunion_cz` VARCHAR( 255 ) NULL AFTER `add_reunion_du` ;
ALTER TABLE `consultation` ADD `add_reunion_ar` VARCHAR( 255 ) NULL AFTER `add_reunion_cz` ;

-- MMI : 31/03/2010
ALTER TABLE `consultation` ADD `mode_passation` VARCHAR( 1 ) NULL AFTER `add_reunion_ar` ;

ALTER TABLE `CategorieLot` ADD `caution_provisoire` VARCHAR( 255 ) NOT NULL default '' AFTER `id_lot_externe`;
ALTER TABLE `CategorieLot` ADD `qualification` VARCHAR( 250 ) NOT NULL default '' AFTER `caution_provisoire`;
ALTER TABLE `CategorieLot` ADD `agrements` VARCHAR( 250 ) NOT NULL default '' AFTER `qualification`;
ALTER TABLE `CategorieLot` ADD `add_echantillion` VARCHAR( 250 ) NOT NULL default '' AFTER `agrements`;
ALTER TABLE `CategorieLot` ADD `date_limite_echantillion` VARCHAR( 50 ) NOT NULL default '' AFTER `add_echantillion`;
ALTER TABLE `CategorieLot` ADD `add_reunion` VARCHAR( 250 ) NOT NULL default '' AFTER `date_limite_echantillion`;
ALTER TABLE `CategorieLot` ADD `date_reunion` VARCHAR( 50 ) NOT NULL default '' AFTER `add_reunion`;
ALTER TABLE `CategorieLot` ADD `variantes` CHAR( 1 ) NOT NULL default '' AFTER `date_reunion`;
ALTER TABLE `CategorieLot` ADD `echantillon` ENUM( '0', '1' ) NOT NULL default '0' AFTER `variantes`;
ALTER TABLE `CategorieLot` ADD `reunion` ENUM( '0', '1' ) NOT NULL default '0' AFTER `echantillon`;
ALTER TABLE `CategorieLot` ADD `visites_lieux` ENUM( '0', '1' ) NOT NULL default '0' AFTER `reunion`;

ALTER TABLE `CategorieLot` ADD `add_echantillion_fr` VARCHAR( 255 ) NULL AFTER `visites_lieux` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_en` VARCHAR( 255 ) NULL AFTER `add_echantillion_fr` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_es` VARCHAR( 255 ) NULL AFTER `add_echantillion_en` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_su` VARCHAR( 255 ) NULL AFTER `add_echantillion_es` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_du` VARCHAR( 255 ) NULL AFTER `add_echantillion_su` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_cz` VARCHAR( 255 ) NULL AFTER `add_echantillion_du` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_ar` VARCHAR( 255 ) NULL AFTER `add_echantillion_cz` ;

ALTER TABLE `CategorieLot` ADD `add_reunion_fr` VARCHAR( 255 ) NULL AFTER `add_echantillion_ar` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_en` VARCHAR( 255 ) NULL AFTER `add_reunion_fr` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_es` VARCHAR( 255 ) NULL AFTER `add_reunion_en` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_su` VARCHAR( 255 ) NULL AFTER `add_reunion_es` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_du` VARCHAR( 255 ) NULL AFTER `add_reunion_su` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_cz` VARCHAR( 255 ) NULL AFTER `add_reunion_du` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_ar` VARCHAR( 255 ) NULL AFTER `add_reunion_cz` ;


-- MMI  16/02/2010

ALTER TABLE `Entreprise` ADD `id_agent_createur` INT( 20 ) NOT NULL default '0' AFTER `ifu`;
ALTER TABLE `Entreprise`  ADD `nom_agent` VARCHAR( 200 ) NOT NULL default '' AFTER `id_agent_createur`;
ALTER TABLE `Entreprise`  ADD `prenom_agent` VARCHAR( 200 ) NOT NULL default '' AFTER `nom_agent`;

-- OKO le 09/02/2010
CREATE TABLE `Societes_Exclues` (
`id_societes_exclues` INT NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Clé primaire',
`organisme_acronyme` VARCHAR( 30 ) NULL,
`id_service` INT( 11 ) NULL ,
`id_agent` INT( 11 ) NULL ,
`nom_document` VARCHAR( 255 ) NULL ,
`id_blob` INT( 11 ) NULL ,
`taille_document` VARCHAR( 80 ) NULL ,
`identifiant_entreprise` VARCHAR( 20 ) NULL ,
`raison_sociale` VARCHAR( 255 ) NULL ,
`motif` VARCHAR( 255 ) NULL ,
`raison_sociale_fr` VARCHAR( 255 ) NULL ,
`motif_fr` VARCHAR( 255 ) NULL ,
`raison_sociale_ar` VARCHAR( 255 ) NULL ,
`motif_ar` VARCHAR( 255 ) NULL ,
`type_exclusion` ENUM( '0', '1' ) NULL default '0',
`date_debut_exclusion` VARCHAR( 20 ) default NULL ,
`date_fin_exclusion` VARCHAR( 20 ) default NULL ,
`type_portee` ENUM( '0', '1' ) NULL default '0'
) ENGINE=InnoDB;


-- OKO: le 22/02/2010
ALTER TABLE `ModuleOrganisme` ADD `interface_chorus_pmi` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `alimentation_automatique_liste_invites` ;

-- MMI :  01/04/2010
ALTER TABLE `ModuleOrganisme` ADD `archivage_consultation_sur_pf` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `interface_chorus_pmi`;

-- MWA : le 23/02/2010, mise à 0 par défaut des modules actives pour TGR
UPDATE `ModuleCommun` set `decision_note`= '0' WHERE `decision_note`= '1';
UPDATE `ModuleCommun` set `decision_classement`= '0' WHERE `decision_classement`= '1';
UPDATE `ModuleCommun` set `question_sans_pj`= '0' WHERE `question_sans_pj`= '1';


-- MMI 24/02/2010

CREATE TABLE IF NOT EXISTS `Certificats_Entreprises` (
  `id` int(50) NOT NULL auto_increment,
  `certificat` text NULL ,
  `date_debut` varchar(20) NULL ,
  `date_fin` varchar(20) NULL ,
  `id_inscrit` int(20) NULL ,
  `nom_inscrit` varchar(200) NULL ,
  `prenom_inscrit` varchar(200) NULL ,
  `id_entreprise` int(20) NULL ,
  `statut_revoque` varchar(20) NULL ,
  `date_revoquation` varchar(20) NULL ,
  `mail_inscrit` varchar(20) NULL ,
  PRIMARY KEY  (`id`),
  KEY `id_entreprise` (`id_entreprise`),
  KEY `id_inscrit` (`id_inscrit`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

ALTER TABLE `Certificats_Entreprises` ADD CONSTRAINT `Certificats_Entreprises_ibfk_2` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`) ON DELETE CASCADE;

-- MLA le 23/02/2010
ALTER TABLE `Offre` ADD `offre_variante` ENUM( '0', '1' ) NULL DEFAULT NULL ;


ALTER TABLE `ValeurReferentiel` CHANGE `libelle_valeur_referentiel` `libelle_valeur_referentiel` TEXT NOT NULL default '';

ALTER TABLE `TypeProcedure` ADD `id_modele` INT( 1 ) NOT NULL default '0' AFTER `abbreviation_portail_europe` ;

INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES ('3', 'modèle avis appel d'offre ouvert');
INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES ('4', 'modèle avis appel d''offre avec sélection');
INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES ('5', 'modèle avis concours');

INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_2`) VALUES (27, '3', 'à l''appel d''offres', '01');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_2`) VALUES (28, 3, 'dossier d'appel d'offres', '02');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_2`) VALUES (29, 3, 'des articles 26 et 28', '03');

INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_2`) VALUES (30, '4', 'à l''appel d''offres avec présélection', '01');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_2`) VALUES (31, 4, 'dossier d'appel d'offres', '02');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_2`) VALUES (32, 4, 'de l'article 53', '03');

INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_2`) VALUES (33, '5', 'au concours', '01');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_2`) VALUES (34, 5, 'dossier du concours', '02');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_2`) VALUES (35, 5, 'des articles 51 et 53', '03');



ALTER TABLE `HabilitationAgent` ADD `gerer_mon_service` ENUM( '0', '1' ) NULL DEFAULT '0' ;
ALTER TABLE `HabilitationProfil` ADD `gerer_mon_service` ENUM( '0', '1' ) NULL DEFAULT '0';

ALTER TABLE `HabilitationAgent` ADD `download_archives` ENUM( '0', '1' ) NULL DEFAULT '0' AFTER `gerer_mon_service`;
ALTER TABLE `HabilitationProfil` ADD `download_archives` ENUM( '0', '1' ) NULL DEFAULT '0' AFTER `gerer_mon_service`; 



-- FBO : 16/03/2010 : 0004223: MPE : Agent et Inscrit 

ALTER TABLE `Agent` ADD `actif` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `alerte_question_entreprise`;
ALTER TABLE `Inscrit` ADD `tentatives_mdp` INT( 1 ) NOT NULL DEFAULT '0' AFTER `date_modification`;

ALTER TABLE `HabilitationProfil` ADD `creer_annonce_extrait_pv`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Gère l''habilitation pour la gestion des type d''annonce ''Annonce_extrait_pv''';
ALTER TABLE `HabilitationProfil` ADD `creer_annonce_rapport_achevement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Gère l''habilitation pour la gestion des type d''annonce ''Annonce_rapport_achevement''';

ALTER TABLE `HabilitationAgent` ADD `creer_annonce_extrait_pv`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Gère l''habilitation pour la gestion des type d''annonce ''Annonce_extrait_pv''';
ALTER TABLE `HabilitationAgent` ADD `creer_annonce_rapport_achevement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Gère l''habilitation pour la gestion des type d''annonce ''Annonce_rapport_achevement''';

-- OKO: le 01/04/2010
ALTER TABLE `ModuleOrganisme` ADD `autoriser_modification_apres_phase_consultation`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Empêche la modification d''une consultation après phase ''consultation''' AFTER `archivage_consultation_sur_pf`  ;

-- OKO le 30/04/2010
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_identifiant_unique` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Autorise les entreprises à s''inscrire avec un identifiant unique national rattaché à une ville d''enregistrement au registre de commerce. L''activation de ce module cache l''inscription avec SIREN/SIRET. Il impacte également les fonctions de recherche des entreprises du côté agent ainsi que les registres papier de téchargement et de dépôt.' AFTER `consultation_mode_passation`  ;

ALTER TABLE `consultation` CHANGE `pieces_dossier_admin` `pieces_dossier_admin`  TEXT  NULL DEFAULT '';
ALTER TABLE `consultation` CHANGE `pieces_dossier_tech` `pieces_dossier_tech`  TEXT NULL DEFAULT '';
ALTER TABLE `consultation` CHANGE `pieces_dossier_additif` `pieces_dossier_additif`  TEXT NULL DEFAULT '';

-- OKO: le 16/04/2010 : requêtes de suppression des modules communs et organismes
ALTER TABLE `ModuleCommun` DROP `login_by_mail` ;
ALTER TABLE `ModuleCommun` DROP `authentication_without_certificate` ;
ALTER TABLE `ModuleCommun` DROP `annuaire_organismes` ;
ALTER TABLE `ModuleCommun` DROP `authenticate_agent_by_cert` ;
ALTER TABLE `ModuleOrganisme` DROP `authenticate_agent_by_cert`  ;
ALTER TABLE `ModuleCommun` DROP `duplication_agent` ;
ALTER TABLE `ModuleOrganisme` DROP `duplication_agent` ;
ALTER TABLE `ModuleCommun` DROP `cross_border` ;
ALTER TABLE `ModuleCommun` DROP `option_taille_entreprise` ;
ALTER TABLE `ModuleCommun` DROP `menu_pmi` ;
ALTER TABLE `ModuleCommun` DROP `pae_socle` ;
ALTER TABLE `ModuleOrganisme` DROP `pae_socle` ;
ALTER TABLE `ModuleOrganisme` DROP `lieu_execution` ;
ALTER TABLE `ModuleOrganisme` DROP `module_commande` ;
ALTER TABLE `ModuleOrganisme` DROP `module_facture` ;
ALTER TABLE `ModuleOrganisme` DROP `connecteur_marco` ;
ALTER TABLE `ModuleOrganisme` DROP `connecteur_sis` ;
ALTER TABLE `ModuleOrganisme` DROP `messagerie_securisee`  ;
ALTER TABLE `ModuleOrganisme` DROP `appel_projet_formation`  ;
ALTER TABLE `ModuleOrganisme` DROP `type_validation` ;
ALTER TABLE `ModuleOrganisme` DROP `gestion_envois_postaux`  ;
ALTER TABLE `ModuleOrganisme` DROP `classement_lot` ;
ALTER TABLE `ModuleOrganisme` DROP `bi_cle_permanent` ;
ALTER TABLE `ModuleOrganisme` DROP `envoi_mail_notification_dce_parametrable`  ;
ALTER TABLE `ModuleOrganisme` DROP `mise_en_ligne_apres_envoi_boamp`  ;
ALTER TABLE `ModuleOrganisme` DROP `mise_en_ligne_apres_publication_boamp`  ;
ALTER TABLE `ModuleOrganisme` DROP `mise_en_ligne_apres_date`  ;
ALTER TABLE `ModuleOrganisme` DROP `mise_en_ligne_manuelle`  ;
ALTER TABLE `ModuleOrganisme` DROP `accept_cond_telecharger_DCE`  ;
ALTER TABLE `ModuleOrganisme` DROP `accept_cond_poser_question`  ; 
ALTER TABLE `ModuleOrganisme` DROP `accept_cond_repondre`  ;
ALTER TABLE `ModuleOrganisme` DROP `mail_history` ;
ALTER TABLE `ModuleOrganisme` DROP `publicite_boamp` ;
ALTER TABLE `ModuleOrganisme` DROP `publicite_moniteur` ;
ALTER TABLE `ModuleOrganisme` DROP `publicite_presseloc` ;
ALTER TABLE `ModuleOrganisme` DROP `demande_confirmation_DCE`  ;
ALTER TABLE `ModuleOrganisme` DROP `envoi_mail_avec_ar` ;
ALTER TABLE `ModuleOrganisme` DROP `verification_tous_champs_obligatoire_DCE`  ;
ALTER TABLE `ModuleOrganisme` DROP `verification_tous_champs_obligatoire_question`  ;
ALTER TABLE `ModuleOrganisme` DROP `gestion_champs_optionnels`  ;
ALTER TABLE `ModuleOrganisme` DROP `poser_question` ;
ALTER TABLE `ModuleOrganisme` DROP `pub_marches_attribues`  ;
ALTER TABLE `ModuleOrganisme` DROP `alerte_delai_procedure`  ;
ALTER TABLE `ModuleOrganisme` DROP `analyse_anti_virus` ;
ALTER TABLE `ModuleOrganisme` DROP `master_key_permanente`  ;
ALTER TABLE `ModuleOrganisme` DROP `no_export_from_client_cao`  ;
ALTER TABLE `ModuleOrganisme` DROP `mail_information_entreprise`  ;
ALTER TABLE `ModuleOrganisme` DROP `retrait_par_lots_DCE`  ;
ALTER TABLE `ModuleOrganisme` DROP `gestion_acces_compte`  ;
ALTER TABLE `ModuleOrganisme` DROP `gestion_alerte` ;
ALTER TABLE `ModuleOrganisme` DROP `gestion_procedue_administrable`  ;
ALTER TABLE `ModuleOrganisme` DROP `boamp_champs_preremplis`  ;
ALTER TABLE `ModuleOrganisme` DROP `acces_mapa` ;
ALTER TABLE `ModuleOrganisme` DROP `ouverture_simultanee`  ;
ALTER TABLE `ModuleOrganisme` DROP `form_creation_onglets`  ;
ALTER TABLE `ModuleOrganisme` DROP `publicite_atexo` ;
ALTER TABLE `ModuleOrganisme` DROP `info_menu_gauche_agent`  ;
ALTER TABLE `ModuleOrganisme` DROP `sequestre` ;
ALTER TABLE `ModuleOrganisme` DROP `envoi_mail_avec_ar_obligatoire`  ;
ALTER TABLE `ModuleOrganisme` DROP `generation_reference_utilisateur`  ; 
ALTER TABLE `ModuleCommun` DROP `menu_bdf`  ;
ALTER TABLE `ModuleCommun` DROP `administration_metier_visualiser_entreprises`  ;
ALTER TABLE `ModuleCommun` DROP `gestion_coffre_fort_entreprise`  ;
ALTER TABLE `ModuleCommun` DROP `multi_zone`  ;
ALTER TABLE `ModuleCommun` DROP `search_entreprise_siren` ;
ALTER TABLE `ModuleCommun` DROP `search_entreprise_ape` ;
ALTER TABLE `ModuleCommun` DROP `search_entreprise_province` ;
ALTER TABLE `ModuleCommun` DROP `search_entreprise_domaines_activites` ;
ALTER TABLE `ModuleCommun` DROP `search_entreprise_qualification` ;
ALTER TABLE `ModuleCommun` DROP `search_entreprise_region` ;
ALTER TABLE `ModuleCommun` DROP `search_entreprise_agrements` ;
ALTER TABLE `ModuleCommun` DROP `search_entreprise_nic` ;
ALTER TABLE `ModuleCommun` DROP `mpe_tgr` ;
 
-- MMI LE : 29/04/2010

ALTER TABLE `chorus_numeros_marches` CHANGE `numero_marche` `numero_marche` VARCHAR( 250 )  NOT NULL DEFAULT '' ;

-- OKO: module à renommer 
ALTER TABLE `ModuleCommun` CHANGE `multi_linguisme` `multi_linguisme_entreprise`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `gestion_fournisseurs_docs_pole` `gestion_fournisseurs_docs_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `base_qualifiee_entreprise` `base_qualifiee_entreprise_insee`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `donnee_complementaires_entreprise` `compte_entreprise_donnees_complementaires`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `vision_ea_entreprise` `annuaire_entites_achat_visible_par_entreprise`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `ac_sad_transv` `affichage_recherche_avancee_agent_ac_sad_transversaux`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `Enchere_Entreprise` `encheres_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `compte_inscrit_code_etablissement` `compte_inscrit_code_nic`  ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_codeape` `compte_entreprise_code_ape`  ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_documents_comerciaux` `compte_entreprise_documents_commerciaux`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `actions_entreprise_consultation_prolongee` `entreprise_repondre_consultation_apres_cloture` ENUM( '0', '1' )  NOT NULL DEFAULT '1' COMMENT 'Permet à une entreprise de répondre à la consultation pendant l''affichage prolongé, si l''affichage prolongé a été paramétré pour la consultation';
ALTER TABLE `ModuleCommun` CHANGE `article_133` `article_133_generation_pf` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction de génération de la liste des marchés à partir de la plate-forme'; 

-- OKO: suppression des modules renommés
ALTER TABLE `ModuleCommun` DROP `multi_linguisme` ;
ALTER TABLE `ModuleCommun` DROP `gestion_fournisseurs_docs_pole` ;
ALTER TABLE `ModuleCommun` DROP `base_qualifiee_entreprise` ;
ALTER TABLE `ModuleCommun` DROP `donnee_complementaires_entreprise` ;
ALTER TABLE `ModuleCommun` DROP `vision_ea_entreprise` ;
ALTER TABLE `ModuleCommun` DROP `ac_sad_transv` ;
ALTER TABLE `ModuleCommun` DROP `Enchere_Entreprise` ;
ALTER TABLE `ModuleCommun` DROP `compte_inscrit_code_etablissement` ;
ALTER TABLE `ModuleCommun` DROP `compte_entreprise_codeape` ;
ALTER TABLE `ModuleCommun` DROP `compte_entreprise_documents_comerciaux` ;
ALTER TABLE `ModuleCommun` DROP `actions_entreprise_consultation_prolongee` ;
ALTER TABLE `ModuleCommun` DROP `article_133` ;
ALTER TABLE `ModuleCommun` DROP `gestion_agent_mes_sous_services`;


-- OKO le 27/04/2010: commentaires sur les modules
ALTER TABLE `ModuleCommun` CHANGE `code_cpv` `code_cpv` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Rend obligatoire la saisie du code cpv à la création de la consultation pour le bloc principal et les lots. 1=saisie cpv obligatoire';
ALTER TABLE `ModuleCommun` CHANGE `multi_linguisme_entreprise` `multi_linguisme_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Rend l''application MPE multilingue du côté entreprise. L''activation de ce module se fait en parallèle du paramétrage de la table Langue dans la base commune';
ALTER TABLE `ModuleCommun` CHANGE `gestion_fournisseurs_docs_mes_sous_services` `gestion_fournisseurs_docs_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les fournisseurs de documents, de gérer les fournisseurs de documents de ses sous services';
ALTER TABLE `ModuleCommun` CHANGE `authenticate_inscrit_by_cert` `authenticate_inscrit_by_cert`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Rend visible le bloc authentification par certificat du côté entreprise et active de cette manière la fonction d''authentification par certificat';
ALTER TABLE `ModuleCommun` CHANGE `authenticate_inscrit_by_login` `authenticate_inscrit_by_login`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Rend visible le bloc authentification par identifiant / mot de passe du côté entreprise et active de cette manière la fonction d''authentification par identifiant';
ALTER TABLE `ModuleCommun` CHANGE `base_qualifiee_entreprise_insee` `base_qualifiee_entreprise_insee`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de pré-remplir les informations d''une entreprise lors de l''inscription à partir de la base INSEE. L''interconnexion avec la base INSEE se faisant par Web Services. Ce module est utilisable uniquement en France.';
ALTER TABLE `ModuleCommun` CHANGE `gestion_boamp_mes_sous_services` `gestion_boamp_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les comptes BOAMP, de gérer les comptes BOAMP de ses sous services';
ALTER TABLE `ModuleCommun` CHANGE `gestion_bi_cle_mes_sous_services` `gestion_bi_cle_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les clés de chiffrement, de gérer les clés de chiffrements de ses sous services';
ALTER TABLE `ModuleCommun` CHANGE `nom_entreprise_toujours_visible` `nom_entreprise_toujours_visible`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le nom de l''entreprise dans la phase d''ouverture et d''analyse au niveau du tableau de réponses et de la page de téléchargement des plis chiffrés'; 
ALTER TABLE `ModuleCommun` CHANGE `gestion_jal_mes_sous_services` `gestion_jal_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les JAL, de gérer les JAL de ses sous services';
ALTER TABLE `ModuleCommun` CHANGE `choix_langue_affichage_consultation` `choix_langue_affichage_consultation`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher dans la page de résultat de recherche des consultations du côté entreprise, les informations relatives à la consultation dans les autres langues. Ce module s''utilise avec le module organisme traduire_consultation.';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_donnees_complementaires` `compte_entreprise_donnees_complementaires`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne accès à un lien ''Données complémentaires - Organisme de formation'' dans le compte d''une entreprise. Ce lien redirige vers une page qui permet à l''entreprise de préciser son activité dans le domaine de la formation.';
ALTER TABLE `ModuleCommun` CHANGE `annuaire_entites_achat_visible_par_entreprise` `annuaire_entites_achat_visible_par_entreprise`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le lien ''Visualiser les services'' côté entreprise. C''est l''annuaire des entités d''achat de la plate-forme. Les organismes visibles dans la liste déroulante sont celles qui ont affichage_entite=1 dans la table organisme.';
ALTER TABLE `ModuleCommun` CHANGE `affichage_recherche_avancee_agent_ac_sad_transversaux` `affichage_recherche_avancee_agent_ac_sad_transversaux` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''accéder aux Accords-cadres et Systèmes d''acquisition dynamique transversaux, en vue de passer un marché subséquent ou spécifique dans le moteur de recherche avancé côté agent.';
ALTER TABLE `ModuleCommun` CHANGE `encheres_entreprise` `encheres_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module enchère côté entreprise. Ce module s''utilise avec le module encheres de la table ModuleOrganisme.';
ALTER TABLE `ModuleCommun` CHANGE `socle_interne` `socle_interne` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de transformer l''application MPE comme un socle de portail d''administration électronique du côté des agents. Le socle interne est à activer quand une application différente de MPE est intégrée en sus au portail (ex: parapheur).';
ALTER TABLE `ModuleCommun` CHANGE `module_certificat` `module_certificat` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer un lien dans le menu gauche agent dans l''aide>outils informatique qui affiche une page permettant de télécharger le certificat racine de la plate-forme afin de pouvoir utiliser les outils de vérifications de jeton d''horodatage.';
ALTER TABLE `ModuleCommun` CHANGE `afficher_image_organisme` `afficher_image_organisme`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de gérer un logo par organisme qui s''affichera dans le bandeau à droite de la plate-forme quand on y accède avec l''acronyme et dans les résultats du moteur de recherche côté entreprise. L''administration du logo se fait dans /administration.';
ALTER TABLE `ModuleCommun` CHANGE `portail_defense_entreprise` `portail_defense_entreprise`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Pemet l''Intégration de la gestion de l''inscription des entreprises sur les portails du MINDEF ainsi que le sso en provenance des portails MINDEF.';
ALTER TABLE `ModuleCommun` CHANGE `annonces_marches` `annonces_marches` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de transformer la plate-forme MPE comme un portail d''annonces regroupant les consultations en provenance de plusieurs sources. Si ce module est activé, certaines fonctions du côté entreprise sont adapatées par rapport à une PF MPE classique.';
ALTER TABLE `ModuleCommun` CHANGE `associer_documents_cfe_consultation` `associer_documents_cfe_consultation`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''''associer les documents du coffre fort électronique à une consultation, dans ce cas le document est visible par l''agent pendant la phase d''analyse des offres et dans l''archive.';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_region` `compte_entreprise_region`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ région dans le formulaire de création des entreprises, la liste est alimentée à partir de la table GeolocalisationN1';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_province` `compte_entreprise_province`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ province dans le formulaire de creation des entreprises, la liste est alimentée à partir de la table GeolocalisationN2';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_telephone2` `compte_entreprise_telephone2`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ telephone 2 dans le formulaire de creation des entreprises''';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_telephone3` `compte_entreprise_telephone3`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ telephone 3 dans le formulaire de creation des entreprises''';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_cnss` `compte_entreprise_cnss`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ numero du cnss dans le formulaire de creation des entreprises''';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_tax_prof` `compte_entreprise_tax_prof`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ numero de la taxe professionnelle dans le formulaire de creation des entreprises''';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_rcnum` `compte_entreprise_rcnum`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ du numero du registe du commerce dans le formulaire de creation des entreprises''';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_rcville` `compte_entreprise_rcville`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher la ville associée au champ du numero du registe du commerce dans le formulaire de creation des entreprises''';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_domaine_activite` `compte_entreprise_domaine_activite`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise la table Category, SousCategory de la base commune.';
ALTER TABLE `ModuleCommun` CHANGE `compte_inscrit_code_nic` `compte_inscrit_code_nic`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ code nic dans le formulaire de creation du compte d''un inscrit';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_code_ape` `compte_entreprise_code_ape`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ code ape dans le formulaire de creation des entreprises''';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_declaration_honneur` `compte_entreprise_declaration_honneur`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ declaration sur l''''honneur dans le formulaire de creation des entreprises'' que l''inscrit coche lors de l''enregistrement.';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_documents_commerciaux` `compte_entreprise_documents_commerciaux`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ ajout de documents commerciaux dans la page du compte entreprise et leur téléchargement dans la page détail entreprise du côté Agent';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_qualification` `compte_entreprise_qualification`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur qualification qui sont organisées hiérarchiquement. Ce module utilise la table Qualification de la base commune.';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_agrement` `compte_entreprise_agrement`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur agréement qui sont organisées hiérarchiquement. Ce module utilise la table Agrement de la base commune.';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_moyens_techniques` `compte_entreprise_moyens_techniques`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ moyen techniques dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent';
ALTER TABLE `ModuleCommun` CHANGE `cfe_date_fin_validite_obligatoire` `cfe_date_fin_validite_obligatoire`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Champ date de fin de validité est obligatoire quand on ajoute un document dans le coffre-fort';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_moyens_humains` `compte_entreprise_moyens_humains`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ moyen humains dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_prestations_realisees` `compte_entreprise_prestations_realisees`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ prestations réalisés dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_activite_domaine_defense`  `compte_entreprise_activite_domaine_defense`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ description activités domaine défense dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_chiffre_affaire_production_biens_services`  `compte_entreprise_chiffre_affaire_production_biens_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le tableau relatif aux donnees financieres de l''entreprise relativement à la vente de marchandises, à la production et aux services sur les 3 dernieres années';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_donnees_financieres` `compte_entreprise_donnees_financieres`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le tableau relatif aux donnees financieres de l''entreprise relativement au CA, au besoin de financemet, au cash flow et à la capacite d\\''endettement'' sur les 3 dernieres années';
ALTER TABLE `ModuleCommun` CHANGE `enveloppe_offre_technique` `enveloppe_offre_technique`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''associer une enveloppe d''offre technique';
ALTER TABLE `ModuleCommun` CHANGE `enveloppe_anonymat` `enveloppe_anonymat` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''associer une enveloppe d''anonymat';
ALTER TABLE `ModuleCommun` CHANGE `publicite_format_xml` `publicite_format_xml`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gerer la visibilite de la publicite au format xml dans le menu gauche agent ainsi que les fonctions associées (gestion compte boamp, moniteur, statistiques avis publiés, typologie de mise en ligne de la consultation, habilitations, alertes agent';
ALTER TABLE `ModuleCommun` CHANGE `procedure_adaptee` `procedure_adaptee` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilité des MAPA dans la plate-forme: accès aux fonctions relatives à la gestion des MAPA, aux statistiques MAPA et les habilitations'; 
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_siren` `compte_entreprise_siren` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ siren et siret dans le formulaire de creation des entreprises';
ALTER TABLE `ModuleCommun` CHANGE `telechargement_outil_verif_horodatage` `telechargement_outil_verif_horodatage` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Affiche un lien permettant de télécharger l''outil de vérification des jetons d''horodatages à partir de la page Autres Outils Informatique';
ALTER TABLE `ModuleCommun` CHANGE `affichage_code_cpv` `affichage_code_cpv` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet l''utilisation du module Code CPV dans MPE';
ALTER TABLE `ModuleCommun` CHANGE `consultation_domaines_activites` `consultation_domaines_activites` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification d''une consultation en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise la table Category, SousCategory de la base commune.';
ALTER TABLE `ModuleCommun` CHANGE `statistiques_mesure_demat` `statistiques_mesure_demat` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilité de la fonction Mesure Avancement Dematerialisation dans la plate-forme';
ALTER TABLE `ModuleCommun` CHANGE `publication_procure` `publication_procure` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Gère la visibilité du module ''''Rendre accessible cette consultation dans toutes les langues au portail européen PROCURE"';
ALTER TABLE `ModuleCommun` CHANGE `traduire_entite_achat_arabe` `traduire_entite_achat_arabe` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module pour activer la visibilité des champs à traduire en arabe (côté agent et administration, gestion de l''organisme)';
ALTER TABLE `ModuleCommun` CHANGE `decision_afficher_detail_candidat_par_defaut` `decision_afficher_detail_candidat_par_defaut` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Ce module permet d''afficher le détail du candidat sans avoir à cliquer sur la case à cocher ''attributaire''';
ALTER TABLE `ModuleCommun` CHANGE `liste_marches` `liste_marches` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction d''import de la liste des marchés à partir de fichier';
ALTER TABLE `ModuleCommun` CHANGE `autoriser_modif_profil_inscrit_ates` `autoriser_modif_profil_inscrit_ates` ENUM( '0', '1' ) NULL DEFAULT '0' COMMENT 'Ce module permet à l''entreprise qui s''enregistre de choisir son profil (Utilisateur Simple ou Administrateur). Si 0 alors pas de choix possible, le premier inscrit est Administrateur et les autres US.';



-- FBO Traduction table Referentiel le 09/06/2010

ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_fr` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel`;
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_en` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_fr`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_es` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_en`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_su` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_es`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_du` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_su`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_cz` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_du`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_ar` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_cz`  ;
-- FBO Traduction table Consultation:champs pieces_dossier_admin le 09/06/2010

ALTER TABLE `consultation` ADD `pieces_dossier_admin_fr` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin`; 
ALTER TABLE `consultation` ADD `pieces_dossier_admin_en` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_fr`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_es` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_en`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_su` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_es`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_du` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_su`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_cz` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_du`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_ar` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_cz`;
-- FBO Traduction table Consultation:champs pieces_dossier_tech le 09/06/2010

ALTER TABLE `consultation` ADD `pieces_dossier_tech_fr` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_en` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_fr`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_es` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_en`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_su` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_es`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_du` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_su`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_cz` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_du`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_ar` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_cz`;
-- FBO Traduction table Consultation:champs pieces_dossier_pieces_dossier_additif le 09/06/2010

ALTER TABLE `consultation` ADD `pieces_dossier_additif_fr` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_en` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_fr`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_es` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_en`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_su` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_es`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_du` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_su`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_cz` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_du`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_ar` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_cz`;  

-- MMI
ALTER TABLE `ModuleCommun` ADD `gerer_certificats_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'gestion des certificats pour les agents' AFTER `compte_entreprise_identifiant_unique`  ;
ALTER TABLE `HabilitationAgent` ADD `gestion_certificats_agent` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `gestion_certificats_agent` ENUM( '0', '1' ) NULL DEFAULT '0';

CREATE TABLE IF NOT EXISTS `Certificat_Agent` (
  `id` int(50) NOT NULL auto_increment,
  `certificat` text NOT NULL default '',
  `date_debut` varchar(20) NOT NULL default '',
  `date_fin` varchar(20) NOT NULL default '',
  `id_agent` int(20) NULL DEFAULT '0',
  `nom_agent` varchar(200) NOT NULL default '',
  `prenom_agent` varchar(200) NOT NULL default '',
  `id_service` int(20) NULL,
  `statut_revoque` varchar(20) NOT NULL default '',
  `date_revoquation` varchar(20) NOT NULL default '',
  `mail_agent` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id_service` (`id_service`),
  KEY `id_agent` (`id_agent`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


-- MMI

ALTER TABLE `HabilitationAgent` ADD `creer_avis_programme_previsionnel` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `creer_avis_programme_previsionnel` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `autre_annonce_programme_previsionnel` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Creer autre type annonce programme previsionnel' AFTER `gerer_certificats_agent`  ;

-- KBE

ALTER TABLE `HabilitationAgent` ADD `annuler_consultation` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `HabilitationProfil` ADD `annuler_consultation` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` ADD `annuler_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'annuler une consultation' AFTER `autre_annonce_programme_previsionnel`  ;

ALTER TABLE `consultation` ADD `consultation_annulee` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- OKO le 23-06-2010
ALTER TABLE `ModuleCommun` ADD `cfe_entreprise_accessible_par_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Ce module permet de rendre les documents du Coffre Fort des entreprises visibles par les agents. Il faut que le module agent acces_agents_cfe soit actif également.' AFTER `annuler_consultation` ; 

-- OKO: le 5 juillet 2010 : insertion de la langue italienne dans la table des langues
ALTER TABLE `Langue` ADD UNIQUE `langue` ( `langue` );
INSERT INTO `Langue` (`id_langue` ,`langue` ,`active` ,`defaut` ,`setlocal` ,`libelle` ,`theme_specifique` ,`obligatoire_pour_publication_consultation`)VALUES (NULL , 'it', '0', '0', 'italian', 'italien', '0', '0');

-- OKO: le 5 juillet 2010 : traduction des champs en italien
ALTER TABLE `Agrement` ADD `libelle_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `Qualification` ADD `libelle_it` VARCHAR( 255 ) NULL DEFAULT ''; 
ALTER TABLE `Organisme` ADD `denomination_org_it` VARCHAR( 100 ) NOT NULL default ''; 
ALTER TABLE `Organisme` ADD `description_org_it` MEDIUMTEXT NOT NULL  default ''; 
ALTER TABLE `Organisme` ADD `adresse_it` VARCHAR( 100 ) NOT NULL  default ''; 
ALTER TABLE `Organisme` ADD `ville_it` VARCHAR( 100 ) NOT NULL  default ''; 
ALTER TABLE `Organisme` ADD `adresse2_it` VARCHAR( 100 ) NOT NULL default '' ; 
ALTER TABLE `Organisme` ADD `pays_it` VARCHAR( 150 ) NOT NULL default '' ; 
ALTER TABLE `GeolocalisationN0` ADD `denomination_it` VARCHAR( 40 ) NOT NULL default '' ; 
ALTER TABLE `GeolocalisationN0` ADD `libelle_selectionner_it` VARCHAR( 40 ) NOT NULL default '' ; 
ALTER TABLE `GeolocalisationN0` ADD `libelle_tous_it` VARCHAR( 40 ) NOT NULL default ''; 
ALTER TABLE `GeolocalisationN0` ADD `libelle_Aucun_it` VARCHAR( 40 ) NOT NULL default ''; 
ALTER TABLE `GeolocalisationN1` ADD `denomination1_it` VARCHAR( 100 ) NOT NULL default ''; 
ALTER TABLE `GeolocalisationN1` ADD `denomination2_it` VARCHAR( 100 ) NOT NULL default ''; 
ALTER TABLE `GeolocalisationN2` ADD `denomination1_it` VARCHAR( 50 ) NOT NULL default ''; 
ALTER TABLE `GeolocalisationN2` ADD `denomination2_it` VARCHAR( 30 ) NOT NULL default ''; 
ALTER TABLE `CategorieLot` ADD `description_detail_it` LONGTEXT NULL DEFAULT '' ; 
ALTER TABLE `CategorieLot` ADD `description_it` LONGTEXT NULL DEFAULT '' ; 
ALTER TABLE `CategorieLot` ADD `add_echantillion_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `CategorieLot` ADD `add_reunion_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `CategorieConsultation` ADD `libelle_it` VARCHAR( 100 ) NULL DEFAULT '' ; 
ALTER TABLE `consultation` ADD `titre_it` LONGTEXT NULL DEFAULT '' ; 
ALTER TABLE `consultation` ADD `resume_it` LONGTEXT NULL DEFAULT '' ; 
ALTER TABLE `consultation` ADD `accessibilite_it` ENUM( '0', '1' ) NOT NULL DEFAULT '0' ; 
ALTER TABLE `consultation` ADD `adresse_depot_offres_it` VARCHAR( 255 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_it` VARCHAR( 255 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_it` VARCHAR( 255 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `pieces_dossier_admin_it` VARCHAR( 255 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `pieces_dossier_tech_it` VARCHAR( 255 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `pieces_dossier_additif_it` VARCHAR( 255 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `detail_consultation_it` text DEFAULT '' ; 
ALTER TABLE `consultation` ADD `add_echantillion_it` VARCHAR( 250 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `add_reunion_it` VARCHAR( 250 ) NOT NULL default '' ; 
ALTER TABLE `SousCategorie` ADD `libelle_it` varchar(250) default NULL ; 
ALTER TABLE `Societes_Exclues` ADD `raison_sociale_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `Societes_Exclues` ADD `motif_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `TypeAvis` ADD `intitule_avis_it` VARCHAR( 100 ) NULL DEFAULT '' ; 
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_it` VARCHAR( 100 ) NULL DEFAULT '' ; 
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_it` TEXT  NOT NULL DEFAULT ''; 
ALTER TABLE `Faq_Entreprise` ADD `question_it` text NOT NULL DEFAULT '' ; 
ALTER TABLE `Faq_Entreprise` ADD `reponse_it` text NOT NULL DEFAULT '' ;

-- KBE : gestion referentiel
ALTER TABLE `consultation` CHANGE `code_cpv_2` `code_cpv_2` VARCHAR( 255 ) NULL DEFAULT NULL;   
ALTER TABLE `CategorieLot` CHANGE `code_cpv_2` `code_cpv_2` VARCHAR( 255 ) NULL DEFAULT NULL;  

-- OKO le 09 juillet 2010 : ajout du champ 'compte_entreprise_code_nace_referentiel'
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_code_nace_referentiel` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ code NACE dans le formulaire de création des entreprises et dans la base de données fournisseurs, en passant par un référentiel externe' AFTER `cfe_entreprise_accessible_par_agent` ;

-- OKO le 12 juillet 2010
ALTER TABLE `Entreprise` CHANGE `codeape` `codeape` VARCHAR( 20 ) NOT NULL DEFAULT ''  ;

-- OKO le 04 Aout 2010: gestion des codes nuts dans LT-Réf
ALTER TABLE `ModuleCommun` ADD `code_nut_lt_referentiel` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ ''code nut'' dans le formulaire de création et de recherche avancée des consultations. Ce module fait appel à un referentiel externe' AFTER `compte_entreprise_code_nace_referentiel` ;
ALTER TABLE `consultation` ADD `codes_nuts` TEXT NOT NULL COMMENT 'Contient les codes nuts, pour l''utilisation d''un référentiel externe';
ALTER TABLE `ModuleCommun` ADD `lieux_execution` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher les lieux d''exécution d''une consultation en utilisant la nomenclature classique.' AFTER `code_nut_lt_referentiel` ;
ALTER TABLE `Agent` ADD `codes_nuts` TEXT NULL DEFAULT NULL COMMENT 'Contient les codes nuts, pour l''utilisation d''un référentiel externe' AFTER `actif` ;

-- OKO le 11/08/2010: gestion des domaines d'activités LT-Référentiel
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_domaine_activite_lt_referentiel` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise un référentiel externe.' AFTER `lieux_execution` ;
ALTER TABLE `ModuleCommun` ADD `consultation_domaines_activites_lt_referentiel` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher les domaines d''activités lors de la création de la consultation en utilisant LT-Référentiel' AFTER `compte_entreprise_domaine_activite_lt_referentiel` ;
-- OKO le 13/08/2010: gestion des certificats des comptes entreprises
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_agrement_lt_referentiel` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur certification qui sont organisées hiérarchiquement. Ce module utilise un référentiel externe.' AFTER `consultation_domaines_activites_lt_referentiel` ;

ALTER TABLE `ModuleOrganisme` ADD `importer_enveloppe` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Ce module agent est activable par organisme. Il permet d''accéder à la fonction "importer une enveloppe" en phase décision s''il est actif et si l''agent dispose de l''habilitation';
-- OKO le 01/09/2010: Gestion des qualifications des comptes entreprises
ALTER TABLE `ModuleCommun` ADD `compte_entreprise_qualification_lt_referentiel` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur qualification qui sont organisées hiérarchiquement. Ce module utilise un référentiel externe.' AFTER `compte_entreprise_agrement_lt_referentiel` ;
-- OKO le 13/09/2010: suppression des champs 'setlocal' et 'libelle' de la table langue
ALTER TABLE `Langue` DROP `setlocal` ;
ALTER TABLE `Langue` DROP `libelle` ;

--AME mantis 5466
ALTER TABLE `ModuleCommun` DROP `masquer_fct_ates_pour_ues`;

-- MWA le 08-07-2010 script pour ref. evol BOAMP
INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES
(6, 'Type de pouvoir adjudicateur'),
(7, ' Principale(s) activité(s) du pouvoir adjudicateur'),
(8, ' Principale(s) activité(s) de l''entité adjudicatrice');

--  MWA : 17/09/2010 Ajout libellé INTL
UPDATE `CategorieConsultation` SET `libelle_fr` = 'Travaux', `libelle_en` = 'Works', `libelle_es` = 'Obras', `libelle_it` = 'Lavori' WHERE `CategorieConsultation`.`id` = '1';
UPDATE `CategorieConsultation` SET `libelle_fr` = 'Fournitures', `libelle_en` = 'Supplies', `libelle_es` = 'Suministros', `libelle_it` = 'Forniture' WHERE `CategorieConsultation`.`id` = '2';
UPDATE `CategorieConsultation` SET `libelle_fr` = 'Services', `libelle_en` = 'Services', `libelle_es` = 'Servicios', `libelle_it` = 'Servizi' WHERE `CategorieConsultation`.`id` = '3';
ALTER TABLE `ModuleCommun` DROP `masquer_fct_ates_pour_ues`;

-- OKO le 16/09/2010: mise à jour du contenu de la table 'EtatConsultation' pour stocker des codes messages qui seront en correspondance avec messages.xml
ALTER TABLE `EtatConsultation` ADD `code_etat` VARCHAR( 200 ) NOT NULL DEFAULT '' AFTER `id_etat`;
INSERT INTO `EtatConsultation` (`id_etat`, `code_etat`, `abreviation_etat`, `visible`) VALUES ('1', 'DEFINE_PREPARATION', 'P', '0')  ON DUPLICATE KEY UPDATE `code_etat` = 'DEFINE_PREPARATION';
INSERT INTO `EtatConsultation` (`id_etat`, `code_etat`, `abreviation_etat`, `visible`) VALUES ('2', 'DEFINE_CONSULTATION', 'C', '0')  ON DUPLICATE KEY UPDATE `code_etat` = 'DEFINE_CONSULTATION';
INSERT INTO `EtatConsultation` (`id_etat`, `code_etat`, `abreviation_etat`, `visible`) VALUES ('3', 'DEFINE_OUVERTURE_ANALYSE', 'OA', '1')  ON DUPLICATE KEY UPDATE `code_etat` = 'DEFINE_OUVERTURE_ANALYSE';
INSERT INTO `EtatConsultation` (`id_etat`, `code_etat`, `abreviation_etat`, `visible`) VALUES ('4', 'DEFINE_DECISION', 'D', '1')  ON DUPLICATE KEY UPDATE `code_etat` = 'DEFINE_DECISION';
INSERT INTO `EtatConsultation` (`id_etat`, `code_etat`, `abreviation_etat`, `visible`) VALUES ('5', 'DEFINE_A_ARCHIVER', 'AA', '1')  ON DUPLICATE KEY UPDATE `code_etat` = 'DEFINE_A_ARCHIVER';
INSERT INTO `EtatConsultation` (`id_etat`, `code_etat`, `abreviation_etat`, `visible`) VALUES ('6', 'DEFINE_TEXT_ARCHIVE_REALISEE', 'AR', '1')  ON DUPLICATE KEY UPDATE `code_etat` = 'DEFINE_TEXT_ARCHIVE_REALISEE';
ALTER TABLE `EtatConsultation` DROP `libelle_etat`;

-- OKO le 17/09/2010: ajout de nouveau champ dans la table 'Service_Mertier_Profils' et mise à jour des données
ALTER TABLE `Service_Mertier_Profils` ADD `libelle_fr` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `Service_Mertier_Profils` ADD `libelle_en` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `Service_Mertier_Profils` ADD `libelle_es` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `Service_Mertier_Profils` ADD `libelle_su` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `Service_Mertier_Profils` ADD `libelle_du` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `Service_Mertier_Profils` ADD `libelle_cz` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `Service_Mertier_Profils` ADD `libelle_ar` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `Service_Mertier_Profils` ADD `libelle_it` VARCHAR( 255 ) NULL DEFAULT '' ;

-- OKO le 17/09/2010: ajout de nouveaux champs dans la table 'formejuridique'
ALTER TABLE `formejuridique` ADD `libelle_formejuridique` VARCHAR( 255 ) NOT NULL DEFAULT '' ;
ALTER TABLE `formejuridique` ADD `libelle_formejuridique_fr` VARCHAR( 255 ) NOT NULL DEFAULT '' ;
ALTER TABLE `formejuridique` ADD `libelle_formejuridique_en` VARCHAR( 255 ) NOT NULL DEFAULT '' ;
ALTER TABLE `formejuridique` ADD `libelle_formejuridique_es` VARCHAR( 255 ) NOT NULL DEFAULT '' ;
ALTER TABLE `formejuridique` ADD `libelle_formejuridique_su` VARCHAR( 255 ) NOT NULL DEFAULT '' ;
ALTER TABLE `formejuridique` ADD `libelle_formejuridique_du` VARCHAR( 255 ) NOT NULL DEFAULT '' ;
ALTER TABLE `formejuridique` ADD `libelle_formejuridique_cz` VARCHAR( 255 ) NOT NULL DEFAULT '' ;
ALTER TABLE `formejuridique` ADD `libelle_formejuridique_ar` VARCHAR( 255 ) NOT NULL DEFAULT '' ;
ALTER TABLE `formejuridique` ADD `libelle_formejuridique_it` VARCHAR( 255 ) NOT NULL DEFAULT '' ;

-- OKO le 20/09/2010: ajout de nouveaux champs dans la table 'DocumentsAttaches'
ALTER TABLE `DocumentsAttaches` ADD `nom_document_fr` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `DocumentsAttaches` ADD `nom_document_es` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `DocumentsAttaches` ADD `nom_document_en` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `DocumentsAttaches` ADD `nom_document_su` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `DocumentsAttaches` ADD `nom_document_du` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `DocumentsAttaches` ADD `nom_document_cz` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `DocumentsAttaches` ADD `nom_document_ar` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `DocumentsAttaches` ADD `nom_document_it` VARCHAR( 100 ) NOT NULL DEFAULT '' ;

-- OKO le 20/09/2010: ajout de nouveaux champs dans la table 'Justificatifs'
ALTER TABLE `Justificatifs` ADD `nom_fr` VARCHAR( 100 ) NULL DEFAULT '' AFTER `nom` ;
ALTER TABLE `Justificatifs` ADD `nom_en` VARCHAR( 100 ) NULL DEFAULT '' AFTER `nom_fr` ;
ALTER TABLE `Justificatifs` ADD `nom_es` VARCHAR( 100 ) NULL DEFAULT '' AFTER `nom_en` ;
ALTER TABLE `Justificatifs` ADD `nom_su` VARCHAR( 100 ) NULL DEFAULT '' AFTER `nom_es` ;
ALTER TABLE `Justificatifs` ADD `nom_du` VARCHAR( 100 ) NULL DEFAULT '' AFTER `nom_su` ;
ALTER TABLE `Justificatifs` ADD `nom_cz` VARCHAR( 100 ) NULL DEFAULT '' AFTER `nom_du` ;
ALTER TABLE `Justificatifs` ADD `nom_ar` VARCHAR( 100 ) NULL DEFAULT '' AFTER `nom_cz` ;
ALTER TABLE `Justificatifs` ADD `nom_it` VARCHAR( 100 ) NULL DEFAULT '' AFTER `nom_ar` ;

-- OKO le 27/09/2010: traduction de la table 'CategorieINSEE'
ALTER TABLE `CategorieINSEE` ADD `libelle_fr` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `CategorieINSEE` ADD `libelle_en` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `CategorieINSEE` ADD `libelle_es` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `CategorieINSEE` ADD `libelle_su` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `CategorieINSEE` ADD `libelle_du` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `CategorieINSEE` ADD `libelle_cz` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `CategorieINSEE` ADD `libelle_ar` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `CategorieINSEE` ADD `libelle_it` VARCHAR( 100 ) NOT NULL DEFAULT '' ;

--
--0004961: PMI : évolution cinématique de signature et de réponse 
--
ALTER TABLE `ModuleCommun` ADD `reponse_pas_a_pas` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT '0004961: PMI : évolution cinématique de signature et de réponse' AFTER `compte_entreprise_qualification_lt_referentiel` ;

-- OKO le 27/09/2010: suppression des tables non utilisées
DROP TABLE IF EXISTS `Etape_cao`  ;
DROP TABLE IF EXISTS `tailleentreprise`  ;
DROP TABLE IF EXISTS `AR_Action`;
DROP TABLE IF EXISTS `Code_cpv_inscrit`;
DROP TABLE IF EXISTS `ConsultationLangue`;
DROP TABLE IF EXISTS `consultation_nettoyee`;
DROP TABLE IF EXISTS `HomePage`;
DROP TABLE IF EXISTS `liste_plateforme`;
DROP TABLE IF EXISTS `Mail_information_entreprise`;
DROP TABLE IF EXISTS `MessagePublication`;
DROP TABLE IF EXISTS `JournalPublication`;
DROP TABLE IF EXISTS `OrganePublication`;
DROP TABLE IF EXISTS `Tiers`;
DROP TABLE IF EXISTS `OrganismeTiers`;
DROP TABLE IF EXISTS `ServeurOrganisme`;
DROP TABLE IF EXISTS `admission_systeme_acquisition`;
DROP TABLE IF EXISTS `ServicesMetiers`;

-- OKO le 01/10/2010: 
ALTER TABLE `HabilitationAgent` ADD `envoyer_publicite` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Conditionne l''affichage du bouton "Envoyer" dans les colonnes "Actions" des tableaux "Liste des destinataires"';
ALTER TABLE `HabilitationProfil` ADD `envoyer_publicite` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Conditionne l''affichage du bouton "Envoyer" dans les colonnes "Actions" des tableaux "Liste des destinataires"'; 

-- AME le 04/10/2010 mantis 5732(FS1) 
ALTER TABLE `ModuleCommun`  ADD `agent_controle_format_mot_de_passe` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'ce module permet de controler le format du mot de passe saisit par l''agent, 8 caractères minimum pris dans au moins 3 types de caractères parmi les 4 suivants : Minuscules, Majuscules, Chiffres, Caractères spéciaux.';

-- OKO le 05/10/2010: 
ALTER TABLE `ModuleCommun` ADD `entreprise_validation_email_inscription` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'ce module permet de valider l''adresse email saisie par l''entreprise lors de la création du compte.A la création du compte, un mail est automatiquement envoyé par la plateforme à l''entreprise. Ce mail permet de valider l''adresse mail de l''entreprise.';
ALTER TABLE `Inscrit` ADD `uid` VARCHAR( 50 ) NULL ;

-- OKO le 08/10/2010:
ALTER TABLE `ModuleCommun` ADD `telecharger_dce_avec_authentification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Ce module implique que le téléchargement des DCE doit être fait en étant être inscrit et authentifié.' AFTER `entreprise_validation_email_inscription` ; 

-- AME 11/10/2010 : suppression des index inutils et l'ajout des index utils
ALTER TABLE `Entreprise` DROP INDEX `region_2`;
ALTER TABLE `Entreprise` DROP INDEX `province_2`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_2`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_2`;
ALTER TABLE `Entreprise` DROP INDEX `region_3`;
ALTER TABLE `Entreprise` DROP INDEX `province_3`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_3`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_3`;
ALTER TABLE `Entreprise` DROP INDEX `region_4`;
ALTER TABLE `Entreprise` DROP INDEX `province_4`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_4`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_4`;
ALTER TABLE `Entreprise` DROP INDEX `region_5`;
ALTER TABLE `Entreprise` DROP INDEX `province_5`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_5`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_5`;
ALTER TABLE `Entreprise` DROP INDEX `region_6`;
ALTER TABLE `Entreprise` DROP INDEX `province_6`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_6`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_6`;
ALTER TABLE `Entreprise` DROP INDEX `region_7`;
ALTER TABLE `Entreprise` DROP INDEX `province_7`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_7`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_7`;
ALTER TABLE `Entreprise` DROP INDEX `region_8`;
ALTER TABLE `Entreprise` DROP INDEX `province_8`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_8`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_8`;
ALTER TABLE `Entreprise` DROP INDEX `region_9`;
ALTER TABLE `Entreprise` DROP INDEX `province_9`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_9`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_9`;
ALTER TABLE `Entreprise` DROP INDEX `region_10`;
ALTER TABLE `Entreprise` DROP INDEX `province_10`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_10`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_10`;
ALTER TABLE `Entreprise` DROP INDEX `region_11`;
ALTER TABLE `Entreprise` DROP INDEX `province_11`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_11`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_11`;
ALTER TABLE `Entreprise` DROP INDEX `region_12`;
ALTER TABLE `Entreprise` DROP INDEX `province_12`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_12`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_12`;
ALTER TABLE `Entreprise` DROP INDEX `region_13`;
ALTER TABLE `Entreprise` DROP INDEX `province_13`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_13`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_13`;
ALTER TABLE `Entreprise` DROP INDEX `region_14`;
ALTER TABLE `Entreprise` DROP INDEX `province_14`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_14`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_14`;
ALTER TABLE `Entreprise` DROP INDEX `region_15`;
ALTER TABLE `Entreprise` DROP INDEX `province_15`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_15`;
ALTER TABLE `Entreprise` DROP INDEX `qualification_15`;
ALTER TABLE `Entreprise` DROP INDEX `region_16`;
ALTER TABLE `Entreprise` DROP INDEX `province_16`;
ALTER TABLE `Entreprise` DROP INDEX `domaines_activites_16`;

ALTER TABLE `formejuridique` DROP INDEX `ordre_2`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_3`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_4`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_5`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_6`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_7`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_8`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_9`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_10`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_11`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_12`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_13`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_14`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_15`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_16`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_17`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_18`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_19`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_20`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_21`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_22`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_23`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_24`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_25`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_26`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_27`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_28`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_29`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_30`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_31`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_32`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_33`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_34`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_35`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_36`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_37`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_38`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_39`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_40`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_41`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_42`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_43`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_44`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_45`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_46`;
ALTER TABLE `formejuridique` DROP INDEX `ordre_47`;

ALTER TABLE `Langue` DROP INDEX `langue_2`;
ALTER TABLE `Langue` DROP INDEX `langue_3`;
ALTER TABLE `Langue` DROP INDEX `langue_4`;
ALTER TABLE `Langue` DROP INDEX `langue_5`;
ALTER TABLE `Langue` DROP INDEX `langue_6`;
ALTER TABLE `Langue` DROP INDEX `langue_7`;
ALTER TABLE `Langue` DROP INDEX `langue_8`;
ALTER TABLE `Langue` DROP INDEX `langue_9`;
ALTER TABLE `Langue` DROP INDEX `langue_10`;
ALTER TABLE `Langue` DROP INDEX `langue_11`;
ALTER TABLE `Langue` DROP INDEX `langue_12`;
ALTER TABLE `Langue` DROP INDEX `langue_13`;
ALTER TABLE `Langue` DROP INDEX `langue_14`;
ALTER TABLE `Langue` DROP INDEX `langue_15`;
ALTER TABLE `Langue` DROP INDEX `langue_16`;
ALTER TABLE `Langue` DROP INDEX `langue_17`;
ALTER TABLE `Langue` DROP INDEX `langue_18`;
ALTER TABLE `Langue` DROP INDEX `langue_19`;
ALTER TABLE `Langue` DROP INDEX `langue_20`;
ALTER TABLE `Langue` DROP INDEX `langue_21`;
ALTER TABLE `Langue` DROP INDEX `langue_22`;
ALTER TABLE `Langue` DROP INDEX `langue_23`;
ALTER TABLE `Langue` DROP INDEX `langue_24`;
ALTER TABLE `Langue` DROP INDEX `langue_25`;
ALTER TABLE `Langue` DROP INDEX `langue_26`;
ALTER TABLE `Langue` DROP INDEX `langue_27`;
ALTER TABLE `Langue` DROP INDEX `langue_28`;
ALTER TABLE `Langue` DROP INDEX `langue_29`;
ALTER TABLE `Langue` DROP INDEX `langue_30`;
ALTER TABLE `Langue` DROP INDEX `langue_31`;
ALTER TABLE `Langue` DROP INDEX `langue_32`;
ALTER TABLE `Langue` DROP INDEX `langue_33`;
ALTER TABLE `Langue` DROP INDEX `langue_34`;
ALTER TABLE `Langue` DROP INDEX `langue_35`;

ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_2`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_3`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_4`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_5`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_6`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_7`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_8`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_9`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_10`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_11`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_12`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_13`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_14`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_15`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_16`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_17`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_18`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_19`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_20`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_21`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_22`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_23`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_24`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_25`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_26`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_27`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_28`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_29`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_30`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_31`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_32`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_33`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_34`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_35`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_36`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_37`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_38`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_39`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_40`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_41`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_42`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_43`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_44`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_45`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_46`;
ALTER TABLE `Referentiel` DROP INDEX `id_referentiel_47`;

ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_2`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_3`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_4`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_5`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_6`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_7`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_8`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_9`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_10`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_11`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_12`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_13`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_14`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_15`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_16`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_17`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_18`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_19`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_20`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_21`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_22`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_23`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_24`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_25`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_26`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_27`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_28`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_29`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_30`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_31`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_32`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_33`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_34`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_35`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_36`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_37`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_38`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_39`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_40`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_41`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_42`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_43`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_44`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_45`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_46`;
ALTER TABLE `ValeurReferentiel` DROP INDEX `id_referentiel_47`;


ALTER TABLE `CG76_Donnee_Complementaire_Domaine` ADD INDEX `id_domaine` ( `IdDomaine` );  
ALTER TABLE `chorus_numeros_marches` ADD INDEX `organisme` ( `acronyme_organisme` );
ALTER TABLE `CG76_Donnee_Complementaire_entreprise` ADD INDEX `idInscrit` ( `idInscrit` );  
ALTER TABLE `CG76_Donnee_Complementaire_entreprise` ADD INDEX `Entreprise` ( `IdEntreprise`);
ALTER TABLE `CG76_PieceJointe` ADD INDEX `Entreprise` ( `idEntreprise` );
ALTER TABLE `chorus_noms_fichiers` ADD INDEX `organisme` ( `acronyme_organisme` );
ALTER TABLE `consultation_document_cfe` ADD INDEX `blob` ( `id_blob` );
ALTER TABLE `consultation_nettoyee` ADD INDEX `organisme` ( `organisme` );
ALTER TABLE `consultation_nettoyee` ADD INDEX `ref_Consultation` ( `reference` );
ALTER TABLE `EtatConsultation` ADD INDEX `code_etat` ( `code_etat` );
ALTER TABLE `Historique_suppression_agent` ADD INDEX `organisme` ( `organisme` ) ;
ALTER TABLE `Historique_suppression_agent` ADD INDEX `service` ( `id_service` );
ALTER TABLE `InvitationConsultationTransverse` ADD INDEX `organisme_emetteur` ( `organisme_emetteur` );
ALTER TABLE `InvitationConsultationTransverse` ADD INDEX `organisme_invite` ( `organisme_invite` );
ALTER TABLE `Ordre_Du_Jour` ADD INDEX `ref_Consultation` ( `ref_consultation` );
ALTER TABLE `QuestionDCE` ADD INDEX `id_inscrit` ( `id_inscrit`);
ALTER TABLE `QuestionDCE` ADD INDEX `Entreprise` ( `id_entreprise` );

-----
-- FBO :12/10/2010: Champ reponse_pas_a_pas pour les statiques des reponses de type pas a pas.
----
ALTER TABLE `Offre` ADD `reponse_pas_a_pas` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `offre_variante`; 

--AME mantis 5781    
ALTER TABLE `ModuleCommun` ADD `authentification_basic` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'ce module permet d''utiliser l''authentification basic de la norme RFC2617';

-- AME ajout du code acheteur pour les services
ALTER TABLE `ModuleCommun` DROP `code`;
ALTER TABLE `ModuleCommun` ADD `afficher_code_service` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'ce module permet d''afficher le code service/entité achat. la valeur est enregistrer dans le champs siren';

-- OKO le 20/10/2010:
ALTER TABLE `HabilitationAgent` ADD `liste_marches_notifies` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''accéder à la page de recherche avancée des marchés notifiés.';
ALTER TABLE `HabilitationProfil` ADD `liste_marches_notifies` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''accéder à la page de recherche avancée des marchés notifiés.';
 
-- OKO le 20/10/2010: 
ALTER TABLE `ModuleOrganisme` ADD `export_marches_notifies` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à un agent d''un organisme ou d''un service d''accéder à la page de recherche avancée et d''export des marchés notifiés.';

--AME le 10/11/2010 mantis 6262
UPDATE `HabilitationProfil` SET `Gestion_Compte_Groupe_Moniteur` = '1' WHERE `HabilitationProfil`.`id` =1 ;
UPDATE `HabilitationProfil` SET `Gestion_Compte_Groupe_Moniteur` = '1' WHERE `HabilitationProfil`.`id` =2 ;
UPDATE `HabilitationProfil` SET `Gestion_Compte_Groupe_Moniteur` = '1' WHERE `HabilitationProfil`.`id` =5 ;

-- FBO le 01/12/2010 : Module regelement_consultation
ALTER TABLE `ModuleCommun` ADD `reglement_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Ce module permet d''ajouter le Réglement de la consultation à la création consultation.';

-- OKO le 03/12/2010: ajout de modules organisme pour la gestion des coffres forts coté agent
ALTER TABLE `ModuleOrganisme` ADD `acces_agents_cfe_bd_fournisseur` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Ce module lorsqu''il est activé permet d''avoir accès aux documents rendus visibles du coffre fort entreprise à partir de la fonction visualiser les entreprises.' AFTER `export_marches_notifies` ;
ALTER TABLE `ModuleOrganisme` ADD `acces_agents_cfe_ouverture_analyse` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'ce module permet à l''agent d''accéder aux documents rendus visibles du coffre fort entreprise à partir de la page ouverture et analyse' AFTER `acces_agents_cfe_bd_fournisseur` ;

-- OKO le 08/12/2010: suppression de l'habilitation 'gerer_courrier_entreprise'
ALTER TABLE `HabilitationAgent` DROP `gerer_courrier_entreprise`;
ALTER TABLE `HabilitationProfil` DROP `gerer_courrier_entreprise`;

-- AME mantis 6316
ALTER TABLE `ModuleCommun` ADD `authenticate_agent_by_cert` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'ce module permet l''authentification par certificat côté agent';
ALTER TABLE `ModuleCommun` ADD `authenticate_agent_by_login` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Rend visible le bloc authentification par identifiant / mot de passe du côté agent et active de cette manière la fonction d''authentification par identifiant';
ALTER TABLE `Agent` ADD `num_certificat` VARCHAR( 64 ) NULL DEFAULT '';
ALTER TABLE `Agent` ADD `alerte_validation_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- FBO le 13/12/2010 : 6757: MPE 3 : tracking des opérations inscrits à partir de la réponse.

CREATE TABLE `trace_operations_inscrit` (
`id_trace` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`id_inscrit` INT( 11 ) NOT NULL DEFAULT '0' ,
`id_entreprise` INT( 11 ) NOT NULL DEFAULT '0' ,
`addr_ip` VARCHAR( 20 ) NOT NULL DEFAULT '' ,
`date` DATE NOT NULL ,
`operations` TEXT NOT NULL DEFAULT '' ,
PRIMARY KEY ( `id_trace` )
) ENGINE=InnoDB;

-- OKO le 07/01/2011: commentaires sur les modules:
ALTER TABLE `ModuleCommun` CHANGE `menu_entreprise_toutes_les_consultations` `menu_entreprise_toutes_les_consultations` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher toutes les consultations y compris celles clôturées pendant la période d''affichage prolongée, dans le menu de gauche et sur la page d''authentification de l''entreprise.';
ALTER TABLE `ModuleCommun` CHANGE `menu_entreprise_consultations_en_cours` `menu_entreprise_consultations_en_cours` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher les consultations en cours non clôturées sur la plateforme dans le menu de gauche et sur la page d''authentification de l''entreprise.';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_capital_social` `compte_entreprise_capital_social` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ ''capital social'' dans le formulaire de création de l''entreprise et dans les détails de l''entreprise coté acheteur public.';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_cp_obligatoire` `compte_entreprise_cp_obligatoire` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de rendre obligatoire le ''code postal'' et activer le validateur correspondant dans le formulaire de création et de modification de l''entreprise.';
ALTER TABLE `ModuleCommun` CHANGE `annuler_depot` `annuler_depot` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la fonctionnalité d''annulation de dépôt électronique par l''inscrit de l''entreprise dans "mon compteà Mes réponses" et de dépôt papier par l''acheteur public dans les registres de dépôt de la consultation.';
ALTER TABLE `ModuleCommun` CHANGE `traduire_organisme_arabe` `traduire_organisme_arabe` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la traduction des champs en arabe coté acheteur public dans la gestion des organismes et des services.';
ALTER TABLE `ModuleCommun` CHANGE `decision_date_notification` `decision_date_notification` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le bloc de saisie de la date de notification lors de l''attribution du marché au moment de la décision.';
ALTER TABLE `ModuleCommun` CHANGE `decision_cp` `decision_cp` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le bloc de saisie du code postal de l''entreprise attributaire lors de l''attribution du marché au moment de la décision.';
ALTER TABLE `ModuleCommun` CHANGE `decision_pmi_pme` `decision_pmi_pme` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le bloc de sélection ''PME / PMI'' lors de l''attribution du marché au moment de la décision.';
ALTER TABLE `ModuleCommun` CHANGE `decision_tranche_budgetaire` `decision_tranche_budgetaire` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le bloc de sélection de la tranche budgétaire du marché lors de l''attribution au moment de la décision.';
ALTER TABLE `ModuleCommun` CHANGE `decision_nature_prestations` `decision_nature_prestations` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le bloc de sélection de la nature des prestations lors de l''attribution du marché au moment de la décision.';
ALTER TABLE `ModuleCommun` CHANGE `decision_objet_marche` `decision_objet_marche` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le bloc de saisie de l''objet du marché lors de l''attribution au moment de la décision.';
ALTER TABLE `ModuleCommun` CHANGE `decision_note` `decision_note` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher la note de l''entreprise lors de l''attribution du marché au moment de la décision.';
ALTER TABLE `ModuleCommun` CHANGE `decision_classement` `decision_classement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le classement de l''entreprise lors de l''attribution du marché au moment de la décision.';
ALTER TABLE `ModuleCommun` CHANGE `decision_fiche_recensement` `decision_fiche_recensement` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le bloc de téléchargement de la fiche de recensement économique des achats publics lors de l''attribution du marché au moment de la décision.';
ALTER TABLE `ModuleCommun` CHANGE `multi_linguisme_agent` `multi_linguisme_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Rend l''application MPE multilingue du coté acheteur public. L''activation de ce module se fait en parallèle du paramétrage de la table Langue dans la base commune.';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_ifu` `compte_entreprise_ifu` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ identifiant fiscal unique lors de la création et la modification de l''entreprise.';
ALTER TABLE `ModuleCommun` CHANGE `gestion_organisme_par_agent` `gestion_organisme_par_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent détenteur de l''habilitation hyper_admin de gérer les services par organismes. L''utilisation de ce module se fait en parallèle avec l''activation de l''habilitation hyper_admin.';
ALTER TABLE `ModuleCommun` CHANGE `utiliser_lucene` `utiliser_lucene` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''utiliser le module lucene pour le moteur de recherche. Module actif à 1.';
ALTER TABLE `ModuleCommun` CHANGE `utiliser_page_html_lieux_execution` `utiliser_page_html_lieux_execution` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''optimiser les performances de la plateforme en utilisant une page html générée lors de l''utilisation des lieux d''exécution. Module actif à 1 et utilisation classique à 0.';
ALTER TABLE `ModuleCommun` CHANGE `prado_validateur_format_date` `prado_validateur_format_date` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''activer les validateurs au format date dans la page de recherche avancée d''une consultation, les données complémentaires de passation, la page d''ajout de document dans le coffre fort et les prestations de l''entreprise.';
ALTER TABLE `ModuleCommun` CHANGE `prado_validateur_format_email` `prado_validateur_format_email` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''activer les validateurs au format email sur la plateforme.';
ALTER TABLE `ModuleCommun` CHANGE `socle_externe_ppp` `socle_externe_ppp` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le socle externe pour la plateforme E-Bourgogne. Le socle externe regroupe un ensemble de services avec une authentification unique.';
ALTER TABLE `ModuleCommun` CHANGE `validation_format_champs_stricte` `validation_format_champs_stricte` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer les validateurs au format précisé par des expressions régulières dans le compte de l''entreprise. (Ex : numéro de téléphone, code postal, ...).';
ALTER TABLE `ModuleCommun` CHANGE `poser_question_necessite_authentification` `poser_question_necessite_authentification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Oblige l''inscrit de l''entreprise à s''authentifier pour poser une question concernant une consultation. Module actif à 1.';
ALTER TABLE `ModuleCommun` CHANGE `unicite_reference_consultation` `unicite_reference_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''assurer l''unicité de la référence utilisateur de la consultation lors de la création. Module actif à 1. Si la référence utilisateur existe déjà, un message d''erreur apparaît.';
ALTER TABLE `ModuleCommun` CHANGE `telecharger_dce_sans_identification` `telecharger_dce_sans_identification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité à l''inscrit de l''entreprise de télécharger le DCE sans être obligé de s''authentifier sur la plateforme.';
ALTER TABLE `ModuleCommun` CHANGE `gestion_entreprise_par_agent` `gestion_entreprise_par_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet la gestion des inscriptions et les certificats des fournisseurs par l''acheteur public dans le menu de gauche. Cette fonctionnalité s''active en parallèle avec l''habilitation ''gerer_les_fournisseurs''.';
ALTER TABLE `ModuleCommun` CHANGE `autoriser_caracteres_speciaux_dans_reference` `autoriser_caracteres_speciaux_dans_reference` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Autorise la saisie des caractères spéciaux dans la référence lors de la création de la consultation.';
ALTER TABLE `ModuleCommun` CHANGE `ajout_rpa_champ_email` `ajout_rpa_champ_email` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher l''adresse électronique lors de l''ajout et de l''affichage des représentants du pouvoir adjudicateur dans la gestion des services.';
ALTER TABLE `ModuleCommun` CHANGE `ajout_rpa_champ_telephone` `ajout_rpa_champ_telephone` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le numéro de téléphone lors de l''ajout et de l''affichage des représentants du pouvoir adjudicateur dans la gestion des services.';
ALTER TABLE `ModuleCommun` CHANGE `ajout_rpa_champ_fax` `ajout_rpa_champ_fax` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le numéro de fax lors de l''ajout et de l''affichage des représentants du pouvoir adjudicateur dans la gestion des services.';
ALTER TABLE `ModuleCommun` CHANGE `url_demarche_agent` `url_demarche_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher un lien qui explique les détails d''inscription de l''acheteur public sur la page d''authentification.';
ALTER TABLE `ModuleCommun` CHANGE `url_demarche_entreprise` `url_demarche_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher un lien qui explique les détails d''inscription de l''inscrit de l''entreprise sur la page d''authentification de la plateforme.';
ALTER TABLE `ModuleCommun` CHANGE `siret_detail_entite_achat` `siret_detail_entite_achat` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le siret lors de la création et dans les détails de l''entité d''achat de l''organisme.';
ALTER TABLE `ModuleCommun` CHANGE `presence_elu` `presence_elu` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le bloc "type de compte" lors de la création et de la modification d''un agent. L''utilisateur peut ainsi choisir entre un compte "Agent" ou "Elu".';
ALTER TABLE `ModuleCommun` CHANGE `gerer_mon_service` `gerer_mon_service` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne accès au lien dans le menu de gauche, et la possibilité à l''agent habilité de gérer le service auquel il est rattaché. Ce module est utilisé en parallèle avec l''habilitation ''gérer mon service''.';
ALTER TABLE `ModuleCommun` CHANGE `depouillement_enveloppe_depend_RAT_enveloppe_precedente` `depouillement_enveloppe_depend_RAT_enveloppe_precedente` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Oblige l''acheteur public à ouvrir nécessairement toutes les enveloppes de candidatures avant de pouvoir accéder aux offres des entreprises. Fonctionnalité accessible dans la phase ''Ouverture et analyse''.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_adresse_depot_offres` `consultation_adresse_depot_offres` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de renseigner l''adresse de dépôt des offres lors de la création, la modification et la traduction de la consultation. Affiche également l''historique de dépôt des offres.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_caution_provisoire` `consultation_caution_provisoire` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de renseigner la caution provisoire lors de la création et la modification de la consultation, et l''affiche également dans les détails de la consultation et des lots.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_lieu_ouverture_plis` `consultation_lieu_ouverture_plis` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de renseigner le lieu d''ouverture des plis lors de la création, la modification et la traduction de la consultation. Affiche également l''historique des lieux d''ouverture des plis.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_qualification` `consultation_qualification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de renseigner la qualification lors de la création et la modification de la consultation, et l''affiche également dans les détails de la consultation, des lots, ainsi que dans l''historique de la consultation.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_agrement` `consultation_agrement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de renseigner l''agrément lors de la création et la modification de la consultation, et l''affiche également dans les détails de la consultation, des lots, ainsi que dans l''historique de la consultation.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_echantillons_demandes` `consultation_echantillons_demandes` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de préciser s''il y a possibilité de demander des échantillons lors de la création et la modification de la consultation, et l''affiche également dans les détails de la consultation, des lots, ainsi que dans l''historique de la consultation.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_reunion` `consultation_reunion` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de préciser s''il y a réunion lors de la création et la modification de la consultation, et l''affiche également dans les détails de la consultation, des lots, ainsi que dans l''historique de la consultation.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_visite_des_lieux` `consultation_visite_des_lieux` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de préciser s''il y a des visites des lieux lors de la création et la modification de la consultation, et l''affiche également dans les détails de la consultation, des lots, ainsi que dans l''historique de la consultation.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_prix_acquisition` `consultation_prix_acquisition` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de renseigner le prix d''acquisition des plans lors de la création et la modification de la consultation, et l''affiche également dans les détails de la consultation et des lots.';
ALTER TABLE `ModuleCommun` CHANGE `resultat_analyse_avant_decision` `resultat_analyse_avant_decision` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''agent habilité de renseigner le résultat de l''analyse pendant la phase ''ouverture et analyse'' et avant la phase ''décision''. L''accès est disponible sur le tableau de bord dans les ''actions''.';
ALTER TABLE `ModuleCommun` CHANGE `inscription_libre_entreprise` `inscription_libre_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité aux entreprises de s''inscrire et de disposer d''un compte sur la plateforme. Les modules socle_externe_entreprise et socle_externe_ppp doivent être désactivés.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_adresse_retrais_dossiers` `consultation_adresse_retrais_dossiers` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de renseigner l''adresse de retrait des dossiers lors de la création, la modification et la traduction de la consultation. Affiche également l''historique des retraits des dossiers.';
ALTER TABLE `ModuleCommun` CHANGE `recherche_avancee_par_domaines_activite` `recherche_avancee_par_domaines_activite` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet la possibilité de rechercher des consultations par domaines d''activités dans la recherche avancée des consultations et dans les alertes de l''entreprise.';
ALTER TABLE `ModuleCommun` CHANGE `recherche_avancee_par_qualification` `recherche_avancee_par_qualification` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet la possibilité de rechercher des consultations par qualifications dans la recherche avancée des consultations et dans les alertes de l''entreprise.';
ALTER TABLE `ModuleCommun` CHANGE `recherche_avancee_par_agrement` `recherche_avancee_par_agrement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet la possibilité de rechercher des consultations par agréments dans la recherche avancée des consultations et dans les alertes de l''entreprise.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_variantes_autorisees` `consultation_variantes_autorisees` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de préciser s''il y a des variantes autorisées lors de la création et la modification de la consultation, et l''affiche également dans les détails de la consultation, des lots et dans les registres.';
ALTER TABLE `ModuleCommun` CHANGE `recherche_avancee_par_type_org` `recherche_avancee_par_type_org` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet la possibilité de rechercher des consultations par types d''organismes dans la recherche avancée des consultations et dans les alertes des l''entreprises.';
ALTER TABLE `ModuleCommun` CHANGE `contact_administratif_dans_detail_consultation_cote_entreprise` `contact_administratif_dans_detail_consultation_cote_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le contact administratif du représentant du pouvoir adjudicataire dans les détails de la consultation sur le portail des entreprises.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_pieces_dossiers` `consultation_pieces_dossiers` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''acheteur public de renseigner les pièces du dossier administratif, technique et additif lors de la création, modification et traduction de la consultation.';
ALTER TABLE `ModuleCommun` CHANGE `gerer_adresses_service` `gerer_adresses_service` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de paramétrer les adresses de retrait des dossiers, de dépôt des offres et le lieu d''ouverture des plis dans le menu de gauche. Il s''active en parallèle avec l''habilitation gerer_adresses_service.';
ALTER TABLE `ModuleCommun` CHANGE `traduire_annonces` `traduire_annonces` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''acheteur public de traduire les annonces dans les langues activées sur la plateforme. Module actif à 1.';
ALTER TABLE `ModuleCommun` CHANGE `affichier_bloc_actions_dans_detail_annonces` `affichier_bloc_actions_dans_detail_annonces` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le bloc ''Mes actions'' dans les détails des annonces de types autres que consultations sur la plateforme des acheteurs publics.';
ALTER TABLE `ModuleCommun` CHANGE `autoriser_une_seule_reponse_principale_par_entreprise` `autoriser_une_seule_reponse_principale_par_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Autorise l''entreprise à répondre une seule fois à la consultation. Pour répondre une nouvelle fois, l''inscrit devra annuler la première offre puis ajouter la nouvelle.';
ALTER TABLE `ModuleCommun` CHANGE `generation_avis` `generation_avis` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet la génération d''un avis à partir des données de la consultation, lors de la validation et la modification.';
ALTER TABLE `ModuleCommun` CHANGE `autre_annonce_extrait_pv` `autre_annonce_extrait_pv` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de créer et gérer les annonces de type ''Extrait de PV'' sur la plateforme. Fonctionnalité accessible dans le menu de gauche, les détails des annonces et la recherche avancée.';
ALTER TABLE `ModuleCommun` CHANGE `autre_annonce_rapport_achevement` `autre_annonce_rapport_achevement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de créer et gérer les annonces de type ''Rapport d''Achèvement'' sur la plateforme. Fonctionnalité accessible dans le menu de gauche, les détails des annonces et la recherche avancée.';
ALTER TABLE `ModuleCommun` CHANGE `ajout_fichier_joint_autre_annonce` `ajout_fichier_joint_autre_annonce` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet la possibilité de joindre un fichier lors de la modification des annonces de types autres que consultations.';
ALTER TABLE `ModuleCommun` CHANGE `consultation_mode_passation` `consultation_mode_passation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité de choisir le mode de passation de la consultation, soit ''Sur offre de prix'' ou ''Au rabais'' lors de la création et la modification de la consultation. Il affiche également dans les détails de la consultation.';
ALTER TABLE `ModuleCommun` CHANGE `gerer_certificats_agent` `gerer_certificats_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne accès à la fonctionnalité de gestion des certificats électroniques (acheteurs publics et fournisseurs) dans le menu de gauche coté acheteur public.';
ALTER TABLE `ModuleCommun` CHANGE `autre_annonce_programme_previsionnel` `autre_annonce_programme_previsionnel` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de créer et gérer les annonces de type ''Programme prévisionnel'' sur la plateforme. Fonctionnalité accessible dans le menu de gauche, les détails des annonces et la recherche avancée.';
ALTER TABLE `ModuleCommun` CHANGE `annuler_consultation` `annuler_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Ajoute la fonctionnalité ''Annuler la consultation''. Ce module s''active en parallèle avec l''habilitation annuler_consultation. Fonctionnalité accessible dans les détails de la consultation, ''Mes actions'' du coté acheteur public.';
ALTER TABLE `ModuleCommun` CHANGE `cfe_entreprise_accessible_par_agent` `cfe_entreprise_accessible_par_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Ce module permet de rendre les documents du Coffre Fort des entreprises visibles par les agents. Il faut que le module agent acces_agents_cfe soit actif également.';
ALTER TABLE `ModuleCommun` CHANGE `creation_inscrit_par_ates` `creation_inscrit_par_ates` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à l''administrateur de l''entreprise d''ajouter de nouveaux inscrits sur la plateforme. Fonctionnalité accessible dans ''Mon compte'', puis ''Utilisateurs de mon Entreprise''.';

-- OKO le 07/01/2011: ajout de commentaires sur les modules organismes.
ALTER TABLE `ModuleOrganisme` CHANGE `encheres` `encheres` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la fonctionnalité d''enchères sur la plateforme des acheteurs publics pour l''organisme. Ce module s''utilise en parallèle avec les habilitations gerer_encheres et suivre_encheres.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `no_activex` `no_activex` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''organisme d''utiliser ou non l''activeX lors de la génération des clés de chiffrement. ActiveX actif à 0.';
ALTER TABLE `ModuleOrganisme` CHANGE `gestion_mapa` `gestion_mapa` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''activer pour chaque organisme la fonctionnalité de gestion des marchés à procédures adaptés (MAPA). Fonctionnalité accessible dans le menu de gauche des acheteurs publics.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `centrale_publication` `centrale_publication` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer pour l''organisme la fonctionnalité de Gestion des Comptes de Publication dans le menu de gauche des acheteurs publics. Ce module s''utilise en parallèle avec l''habilitation ''GestionCentralePub''.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `organisation_centralisee` `organisation_centralisee` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''activer pour l''agent d''un organisme donné, la fonctionnalité de gérer uniquement les consultations et les services de son organisme. Fonctionnalité disponible si le module est desactivé.';
ALTER TABLE `ModuleOrganisme` CHANGE `presence_elu` `presence_elu` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet pour un organisme d''afficher le bloc ''type de compte'' lors de la création et de la modification d''un agent. L''utilisateur peut ainsi choisir entre un compte ''Agent'' ou ''Elu''.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `traduire_consultation` `traduire_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer pour l''organisme la fonctionnalité de traduction des consultations dans les langues activées sur la plateforme, accessible dans les détails de la consultation bloc ''Mes actions''.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `suivi_passation` `suivi_passation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer pour l''organisme la fonctionnalité de gestion des données complémentaires de passation dans les résultats de la recherche des consultations, et la génération des excels statistiques de passations, marchés conclus et avenants.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `numerotation_ref_cons` `numerotation_ref_cons` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer pour l''organisme la fonctionnalité de générer automatiquement la référence de la consultation lors de la création par l''acheteur public.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `interface_archive_arcade_pmi` `interface_archive_arcade_pmi` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de designer un responsable archivage et de télécharger l''archive dans un dossier spécifique à Arcade PMI.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `desarchivage_consultation` `desarchivage_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer pour l''organisme la fonctionnalité de désarchivage d''une consultation à l''état archivé.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `alimentation_automatique_liste_invites` `alimentation_automatique_liste_invites` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à un organisme d''alimenter automatiquement la liste des invités d''une consultation par la désignation du service associé. Fonctionnalité accessible dans les droits d''accès.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `interface_chorus_pmi` `interface_chorus_pmi` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet pour un organisme d''accéder à l''interface des échanges réalisés avec CHORUS. Fonctionnalité accessible dans la phase décision, lors de l''attribution du marché.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `archivage_consultation_sur_pf` `archivage_consultation_sur_pf` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet l''accès au téléchargement des archives sur la plate forme.' ;
ALTER TABLE `ModuleOrganisme` CHANGE `autoriser_modification_apres_phase_consultation` `autoriser_modification_apres_phase_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Autorise pour un organisme la modification d''une consultation clôturée.' ;

-- OKO le 07/01/2011: renommage des module organismes:
ALTER TABLE `ModuleOrganisme` CHANGE `dossier_complementaire` `consultation_pj_autres_pieces_telechargeables` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet à l''acheteur public d''afficher le bloc ''Autres pièces téléchargeables par les entreprises'' et de pouvoir joindre une pièce lors de la création de la consultation. Ce module s''active par organisme.';
ALTER TABLE `ModuleOrganisme` CHANGE `liste_marches` `article_133_upload_fichier` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet pour un organisme de gérer la visibilité du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction d''import de la liste des marchés à partir de fichier.';
ALTER TABLE `ModuleOrganisme` CHANGE `portail_defense_agent` `pmi_lien_portail_defense_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer pour l''organisme le lien d''accès aux portails de la défense. Fonctionnalité accessible dans la page d''accueil des acheteurs publics.';

-- OKO le 10/01/2011: ajout du champ alerte_chorus dans la table 'agent'
ALTER TABLE `Agent` ADD `alerte_chorus` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet à l''acheteur de s''abonner à l''ensemble des alertes chorus à chaque reception d''un flux Chorus par la PMI.' AFTER `alerte_validation_consultation` ;

-- KAT 06/01/2011: Ajout d'un champ dans consultation qui permet de savoir la date de changement d'état de la consultation (decision) 
ALTER TABLE `consultation` ADD `date_decision` VARCHAR( 10 ) NOT NULL DEFAULT '' ;

-- KAT 18/01/2011: Ajout d'un module pour permettre le contrôle sur le format du mot de passe
ALTER TABLE `ModuleCommun` ADD `entreprise_controle_format_mot_de_passe` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'ce module permet de controler le format du mot de passe saisit par l''entreprise, 8 caractères minimum pris dans au moins 3 types de caractères parmi les 4 suivants : Minuscules, Majuscules, Chiffres, Caractères spéciaux.';

-- OKO le 28 janvier 2011: renommage des modules communs
ALTER TABLE `ModuleCommun` CHANGE `liste_marches` `article_133_upload_fichier` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction d''import de la liste des marchés à partir de fichier';
ALTER TABLE `ModuleCommun` CHANGE `champ_rc_obligatoires_ajout_registre_papier` `registre_papier_rcnum_rcville_obligatoires` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de rendre obligatoire la saisie de la ville et du numéro du registre de commerce. Ce module nécessite l''activation des modules ''compte_entreprise_rcnum'' et ''compte_entreprise_rcville''.';
ALTER TABLE `ModuleCommun` CHANGE `champ_adresse_obligatoires_ajout_registre_papier` `registre_papier_adresse_cp_ville_obligatoires` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de rendre obligatoire l''adresse, le code postal et la ville lors d''un dépôt papier au niveau des registres. Module actif à 1.';
ALTER TABLE `ModuleCommun` CHANGE `champ_mail_obligatoire_ajout_registre_papier` `registre_papier_mail_obligatoire` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de rendre obligatoire l''adresse électronique lors de l''ajout d''un dépôt papier par l''acheteur public au niveau des registres.';
ALTER TABLE `ModuleCommun` CHANGE `indicateurs_cles` `menu_entreprise_indicateurs_cles` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Affiche les ''indicateurs clés'' dans le menu de gauche sur la plateforme des entreprises.';
ALTER TABLE `ModuleCommun` CHANGE `question_sans_pj` `entreprise_poser_question_sans_pj` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de masquer le bloc d''ajout de fichier joint lorsque l''inscrit de l''entreprise pose une question. Module actif à 1.';
ALTER TABLE `ModuleCommun` CHANGE `societes_exclues` `menu_agent_societes_exclues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne accès à la fonctionnalité qui permet d''exclure une ou des sociétés à des consultations pour divers motifs sur la plateforme. Accès à partir du menu de gauche.';
ALTER TABLE `ModuleCommun` CHANGE `appliquer_donnees_ensemble_lots` `passation_appliquer_donnees_ensemble_lots` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de reprendre automatiquement pour tous les lots de la consultation, les données spécifiques saisies pour un lot, dans les données complémentaires de passation.';
ALTER TABLE `ModuleCommun` CHANGE `affichier_bloc_actions_dans_detail_annonces` `afficher_bloc_actions_dans_details_annonces` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le bloc ''Mes actions'' dans les détails des annonces de types autres que consultations sur la plateforme des acheteurs publics.';
ALTER TABLE `ModuleCommun` CHANGE `activation_compte_entreprise` `compte_entreprise_activation_inscription_par_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''enregistrer les demandes d''inscription des entreprises sans activer le compte au moment de l''enregistrement. L''activation se faisant par un agent habilité.' ;
ALTER TABLE `ModuleCommun` CHANGE `message_confirmation_inscrit_entreprise` `mail_activation_compte_inscrit_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de gérer l''envoi d''un mail spécifique lors de l''inscription d''un compte inscrit avec des informations sur le compte entreprise et la procedure pour activer le compte.' ;
ALTER TABLE `ModuleCommun` CHANGE `compte_inscrit_msg_ates` `compte_inscrit_choix_profil` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher lors de la création d''un compte inscrit le choix du type de compte utilisateur ou administrateur dans le formulaire' ;

-- OKO le 28 janvier 2011: suppression des modules communs non utilisés
ALTER TABLE `ModuleCommun` DROP `liste_marches` ;
ALTER TABLE `ModuleCommun` DROP `champ_rc_obligatoires_ajout_registre_papier` ;
ALTER TABLE `ModuleCommun` DROP `champ_adresse_obligatoires_ajout_registre_papier` ;
ALTER TABLE `ModuleCommun` DROP `champ_mail_obligatoire_ajout_registre_papier` ;
ALTER TABLE `ModuleCommun` DROP `indicateurs_cles` ;
ALTER TABLE `ModuleCommun` DROP `question_sans_pj` ;
ALTER TABLE `ModuleCommun` DROP `societes_exclues` ;
ALTER TABLE `ModuleCommun` DROP `appliquer_donnees_ensemble_lots` ;
ALTER TABLE `ModuleCommun` DROP `affichier_bloc_actions_dans_detail_annonces` ;
ALTER TABLE `ModuleCommun` DROP `activation_compte_entreprise` ;
ALTER TABLE `ModuleCommun` DROP `message_confirmation_inscrit_entreprise` ;

-- OKO le 28 janvier 2011: suppression des modules communs et organismes non utilisés
ALTER TABLE `ModuleCommun` DROP `gerer_les_services`;
ALTER TABLE `ModuleCommun` DROP `autoriser_suppression_dernier_administrateur_entreprise`;
ALTER TABLE `ModuleCommun` DROP `decision_show_candidats`;
ALTER TABLE `ModuleOrganisme` DROP `registre_retraits_papier`;
ALTER TABLE `ModuleOrganisme` DROP `filtre_cle_permanente_par_service`;
ALTER TABLE `ModuleOrganisme` DROP `alerte_reponse_electronique`;
ALTER TABLE `ModuleOrganisme` DROP `gestion_commissions`;
ALTER TABLE `ModuleOrganisme` DROP `gestion_procedure_administrable`;
ALTER TABLE `ModuleOrganisme` DROP `acces_agents_cfe`;
ALTER TABLE `ModuleOrganisme` DROP `systeme_acquisition_dynamique` ;
ALTER TABLE `ModuleCommun` DROP `compte_inscrit_msg_ates` ;
ALTER TABLE `ModuleOrganisme` DROP `portail_defense_agent`;

-- OKO le 31 janvier 2011: modification de la valeur par défaut de modules communs
ALTER TABLE `ModuleCommun` CHANGE `gestion_fournisseurs_docs_mes_sous_services` `gestion_fournisseurs_docs_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les fournisseurs de documents, de gérer les fournisseurs de documents de ses sous services.' ;
ALTER TABLE `ModuleCommun` CHANGE `gestion_boamp_mes_sous_services` `gestion_boamp_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les comptes BOAMP, de gérer les comptes BOAMP de ses sous services.' ;
ALTER TABLE `ModuleCommun` CHANGE `gestion_bi_cle_mes_sous_services` `gestion_bi_cle_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les clés de chiffrement, de gérer les clés de chiffrements de ses sous services.' ;
ALTER TABLE `ModuleCommun` CHANGE `gestion_jal_mes_sous_services` `gestion_jal_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les JAL, de gérer les JAL de ses sous services' ;
ALTER TABLE `ModuleCommun` CHANGE `annuaire_entites_achat_visible_par_entreprise` `annuaire_entites_achat_visible_par_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''activer le lien ''Visualiser les services'' côté entreprise. C''est l''annuaire des entités d''achat de la plate-forme. Les organismes qui apparaissent dans la liste déroulante sont celles qui ont active et affichage_entite=1 dans la table organisme.' ;

-- OKO le 31 janvier 2011: modification des commentaires sur des modules
ALTER TABLE `ModuleCommun` CHANGE `annuler_depot` `annuler_depot` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la fonctionnalité d''annulation de dépôt électronique par l''inscrit de l''entreprise dans "mon compteà Mes réponses" et de dépôt papier par l''acheteur public dans les registres de dépôt de la consultation.' ;

-- OKO le 01/02/2011: modification du type de champ date en varchar pour ajouter une valeur par defaut
ALTER TABLE `trace_operations_inscrit` CHANGE `date` `date` VARCHAR( 200 ) NOT NULL DEFAULT '';

-- LCH  02/02/2011 - default value (pour le connecteur RSEM) 
ALTER TABLE `consultation` CHANGE `adresse_retrais_dossiers` `adresse_retrais_dossiers` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `caution_provisoire` `caution_provisoire` VARCHAR( 255 ) NULL ;
ALTER TABLE `consultation` CHANGE `adresse_depot_offres` `adresse_depot_offres` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `lieu_ouverture_plis` `lieu_ouverture_plis` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `prix_aquisition_plans` `prix_aquisition_plans` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `qualification` `qualification` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `agrements` `agrements` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `add_echantillion` `add_echantillion` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `add_reunion` `add_reunion` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `adresse_depot_offres_ar` `adresse_depot_offres_ar` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `lieu_ouverture_plis_ar` `lieu_ouverture_plis_ar` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `adresse_retrais_dossiers_ar` `adresse_retrais_dossiers_ar` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `date_limite_echantillion` `date_limite_echantillion` VARCHAR( 50 ) NULL;
ALTER TABLE `consultation` CHANGE `date_reunion` `date_reunion` VARCHAR( 50 ) NULL;
ALTER TABLE `consultation` CHANGE `variantes` `variantes` CHAR( 1 ) NULL;
ALTER TABLE `consultation` CHANGE `codes_nuts` `codes_nuts` TEXT NULL DEFAULT NULL COMMENT 'Contient les codes nuts, pour l''utilisation d''un referentiel externe' ;

ALTER TABLE `CategorieLot` CHANGE `caution_provisoire` `caution_provisoire` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` CHANGE `qualification` `qualification` VARCHAR( 255 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `agrements` `agrements` VARCHAR( 255 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `add_echantillion` `add_echantillion` VARCHAR( 255 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `add_reunion` `add_reunion` VARCHAR( 255 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `date_limite_echantillion` `date_limite_echantillion` VARCHAR( 50 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `date_reunion` `date_reunion` VARCHAR( 50 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `variantes` `variantes` CHAR( 1 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `echantillon` `echantillon` ENUM( '0', '1') NOT NULL DEFAULT  '0';
ALTER TABLE `CategorieLot` CHANGE `reunion` `reunion` ENUM( '0',  '1' ) NOT NULL DEFAULT  '0';
ALTER TABLE `CategorieLot` CHANGE `visites_lieux` `visites_lieux` ENUM( '0', '1') NOT NULL DEFAULT '0';

-- OKO le 09/02/2010: suppréssion des champs cpv non utilisés
ALTER TABLE `consultation` DROP `code_cpv_3`;  
ALTER TABLE `consultation`  DROP `code_cpv_4`;  
ALTER TABLE `consultation`  DROP `libelle_code_cpv_1`;  
ALTER TABLE `consultation`  DROP `libelle_code_cpv_2`;  
ALTER TABLE `consultation`  DROP `libelle_code_cpv_3`;  
ALTER TABLE `consultation`  DROP `libelle_code_cpv_4`;  
ALTER TABLE `CategorieLot` DROP `code_cpv_3`;  
ALTER TABLE `CategorieLot`  DROP `code_cpv_4`;  
ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_1`;  
ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_2`;  
ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_3`;  
ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_4`;

-- OKO le 10/02/2011: ajout de deux habilitations agent et profil
ALTER TABLE `HabilitationAgent` ADD `suivre_message` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité de suivre les messages via l''interface des détails de la consultation.' AFTER `liste_marches_notifies` ;
ALTER TABLE `HabilitationAgent` ADD `envoyer_message` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité d''envoyer des messages via l''interface des details de la consultation.' AFTER `suivre_message` ;
ALTER TABLE `HabilitationProfil` ADD `suivre_message` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité de suivre les messages via l''interface des détails de la consultation.' AFTER `liste_marches_notifies` ;
ALTER TABLE `HabilitationProfil` ADD `envoyer_message` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité d''envoyer des messages via l''interface des details de la consultation.' AFTER `suivre_message` ;
UPDATE `HabilitationProfil` SET `suivre_message` = '0' WHERE `HabilitationProfil`.`id` =8 ;
UPDATE `HabilitationProfil` SET `envoyer_message` = '0' WHERE `HabilitationProfil`.`id` =8 ;

-- AME 22/02/2011 --
CREATE TABLE IF NOT EXISTS `Lt_Referentiel` (`id` int(11) NOT NULL auto_increment, `code_libelle` varchar(200) NOT NULL, `agent` enum('0','1') NOT NULL default '0', `entreprise` enum('0','1') NOT NULL default '0', `consultation` enum('0','1') NOT NULL default '0', `lot` enum('0','1') NOT NULL default '0', `obligatoire` enum('0','1') NOT NULL default '0', `type_search` enum('0','1') NOT NULL default '0', `pages` varchar(400) default NULL, `path_config` varchar(200) NOT NULL, `cas` varchar(20) default NULL, PRIMARY KEY  (`id`)) ENGINE=InnoDB ;
CREATE TABLE IF NOT EXISTS `Referentiel_Entreprise` (`id` int(11) NOT NULL auto_increment, `id_entreprise` int(100) NOT NULL, `id_Lt_Referentiel` int(100) NOT NULL, `lot` int(11) NOT NULL default '0', `valeur_Principale_Lt_Referentiel` varchar(200) NOT NULL, `valeur_Secondaire_Lt_Referentiel` varchar(200) NOT NULL, PRIMARY KEY  (`id`)) ENGINE=InnoDB;
CREATE TABLE IF NOT EXISTS `Referentiel_Consultation` (`id` int(11) NOT NULL auto_increment,`organisme` varchar(20) NOT NULL,`reference` int(100) NOT NULL,`id_Lt_Referentiel` int(100) NOT NULL,`lot` int(11) NOT NULL default '0',`valeur_Principale_Lt_Referentiel` varchar(200) NOT NULL,`valeur_Secondaire_Lt_Referentiel` varchar(200) NOT NULL,PRIMARY KEY  (`id`)) ENGINE=InnoDB;

-- OKO le 01/03/2011: ajout d'un nouveau champ dans la table `chorus_noms_fichiers`
ALTER TABLE `chorus_noms_fichiers` ADD `date_ajout` VARCHAR( 20 ) NULL DEFAULT NULL COMMENT 'Renseigne la date d''ajout d''un nouveau fichier' AFTER `type_fichier` ;

-- OKO le 02/03/2011: ajout d'une nouvelle habilitation
ALTER TABLE `HabilitationAgent` ADD `suivi_flux_chorus_transversal` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''accès à la fonctionnalité de recherche avancée des échanges' AFTER `envoyer_message` ;
ALTER TABLE `HabilitationProfil` ADD `suivi_flux_chorus_transversal` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''accès à la fonctionnalité de recherche avancée des échanges' AFTER `envoyer_message` ;

--AME mantis 7829

ALTER TABLE `Lt_Referentiel` ADD `mode_affichage` varchar(20) default NULL;
ALTER TABLE `Lt_Referentiel` ADD `mode_modification` varchar(20) default NULL;
ALTER TABLE `Lt_Referentiel` ADD `mode_recherche` varchar(20) default NULL;
ALTER TABLE `Lt_Referentiel` DROP `cas`;
ALTER TABLE `Lt_Referentiel` DROP `agent`;
ALTER TABLE `Lt_Referentiel` CHANGE `type_search` `type_search` enum('0','1','2','3') NOT NULL default '0' COMMENT '''0'' : recherche seulement du code fourni.// ''1'' : recherche du code fourni et ses ascendants.// ''2'' : recherche du code fourni et ses descendants.// ''3'' : recherche du code fourni, ses ascendants et ses descendants.';

-- OKO - ajout d'un nouveau module commun
ALTER TABLE `ModuleCommun` ADD `autre_annonce_information` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de créer et gérer les annonces de type ''Information'' sur la plateforme. Fonctionnalité accessible dans le menu de gauche, les détails des annonces et la recherche avancée.' AFTER `entreprise_controle_format_mot_de_passe` ;
ALTER TABLE `ModuleCommun` ADD `creer_autre_annonce` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de créer une nouvelle annonce autre que consultation dans le menu de gauche des agents.' AFTER `autre_annonce_information` ;

-- MWA le 01/03/2011 - donner l'habilitation à tous les profils ayant l'habilitation de saisie de marchés
UPDATE `HabilitationProfil` SET `liste_marches_notifies` = '1' WHERE `HabilitationProfil`.`id` =1 ;
UPDATE `HabilitationProfil` SET `liste_marches_notifies` = '1' WHERE `HabilitationProfil`.`id` =4 ;
UPDATE `HabilitationProfil` SET `liste_marches_notifies` = '1' WHERE `HabilitationProfil`.`id` =5 ;
UPDATE `HabilitationProfil` SET `liste_marches_notifies` = '1' WHERE `HabilitationProfil`.`id` =6 ;
UPDATE `HabilitationProfil` SET `liste_marches_notifies` = '1' WHERE `HabilitationProfil`.`id` =7 ;

-- MWA le 01/03/2011 - donner l'habilitation à tous les agents ayant l'habilitation de saisie de marchés
UPDATE `HabilitationAgent` SET `liste_marches_notifies` = '1' WHERE `HabilitationAgent`.`saisie_marches` =1;

-- MWA le 01/03/2011, module par défaut pour tout le monde 
UPDATE `ModuleOrganisme` SET `export_marches_notifies` = '1';

-- MWA le 01/03/2011,module par défaut pour tout le monde pour controler le format mot de passe agent
UPDATE `ModuleCommun` SET `agent_controle_format_mot_de_passe` = '1';

-- MWA le 01/03/2011 donner l'habilitation à tous les agents ayant l'habilitation de gestion boamp
UPDATE `HabilitationAgent` SET `Gestion_Compte_Groupe_Moniteur` = '1' WHERE `HabilitationAgent`.`gestion_compte_boamp` =1 ;

--FBO Evolution formulaire acte dengagement
ALTER TABLE `ModuleCommun` ADD `generer_acte_dengagement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Ce module permet d''activer la génération de l''acte d''engagement lors de l''envoi de la réponse';

-- HHA le 25/03/2011 Redimensionner le champ "champ_supp_invisible" de la table "consultation"
ALTER TABLE `consultation` CHANGE `champ_supp_invisible` `champ_supp_invisible` TEXT NOT NULL;

-- HHA le 5/04/2011
ALTER TABLE `Organisme` ADD `siren` VARCHAR( 9 ) NOT NULL ;
ALTER TABLE `Organisme` ADD `complement` VARCHAR( 5 ) NOT NULL ;


-- AME renomation du champs Mdp --
ALTER TABLE `Agent` ADD `password` VARCHAR( 40 ) NOT NULL ;

ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_4`;

-- OKO le 03/03/2011 - 1566 modification de la table Organisme
ALTER TABLE `Organisme` DROP `canton`;
ALTER TABLE `Organisme` DROP `latitude_degre`;
ALTER TABLE `Organisme` DROP `latitude_minute`;
ALTER TABLE `Organisme` DROP `latitude_seconde`;
ALTER TABLE `Organisme` DROP `longitude_degre`;
ALTER TABLE `Organisme` DROP `longitude_minute`;
ALTER TABLE `Organisme` DROP `longitude_seconde`;

--
-- OKO le 03/03/2011 : Structure de la table `ModuleCommun`
--

CREATE TABLE IF NOT EXISTS `configuration_plateforme` (
  `code_cpv` enum('0','1') NOT NULL default '0' COMMENT 'Rend obligatoire la saisie du code cpv à la création de la consultation pour le bloc principal et les lots. 1=saisie cpv obligatoire',
  `multi_linguisme_entreprise` enum('0','1') NOT NULL default '0' COMMENT 'Rend l''application MPE multilingue du côté entreprise. L''activation de ce module se fait en parallèle du paramétrage de la table Langue dans la base commune',
  `gestion_fournisseurs_docs_mes_sous_services` enum('0','1') NOT NULL default '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les fournisseurs de documents, de gérer les fournisseurs de documents de ses sous services',
  `authenticate_inscrit_by_cert` enum('0','1') NOT NULL default '0' COMMENT 'Rend visible le bloc authentification par certificat du côté entreprise et active de cette manière la fonction d''authentification par certificat',
  `authenticate_inscrit_by_login` enum('0','1') NOT NULL default '1' COMMENT 'Rend visible le bloc authentification par identifiant / mot de passe du côté entreprise et active de cette manière la fonction d''authentification par identifiant',
  `base_qualifiee_entreprise_insee` enum('0','1') NOT NULL default '0' COMMENT 'Permet de pré-remplir les informations d''une entreprise lors de l''inscription à partir de la base INSEE. L''interconnexion avec la base INSEE se faisant par Web Services. Ce module est utilisable uniquement en France.',
  `gestion_boamp_mes_sous_services` enum('0','1') NOT NULL default '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les comptes BOAMP, de gérer les comptes BOAMP de ses sous services',
  `gestion_bi_cle_mes_sous_services` enum('0','1') NOT NULL default '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les clés de chiffrement, de gérer les clés de chiffrements de ses sous services',
  `nom_entreprise_toujours_visible` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le nom de l''entreprise dans la phase d''ouverture et d''analyse au niveau du tableau de réponses et de la page de téléchargement des plis chiffrés',
  `gestion_jal_mes_sous_services` enum('0','1') NOT NULL default '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les JAL, de gérer les JAL de ses sous services',
  `choix_langue_affichage_consultation` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher dans la page de résultat de recherche des consultations du côté entreprise, les informations relatives à la consultation dans les autres langues. Ce module s''utilise avec le module organisme traduire_consultation.',
  `compte_entreprise_donnees_complementaires` enum('0','1') NOT NULL default '0' COMMENT 'Donne accès à un lien ''Données complémentaires - Organisme de formation'' dans le compte d''une entreprise. Ce lien redirige vers une page qui permet à l''entreprise de préciser son activité dans le domaine de la formation.',
  `annuaire_entites_achat_visible_par_entreprise` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer le lien ''Visualiser les services'' côté entreprise. C''est l''annuaire des entités d''achat de la plate-forme. Les organismes visibles dans la liste déroulante sont celles qui ont affichage_entite=1 dans la table organisme.',
  `affichage_recherche_avancee_agent_ac_sad_transversaux` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''accéder aux Accords-cadres et Systèmes d''acquisition dynamique transversaux, en vue de passer un marché subséquent ou spécifique dans le moteur de recherche avancé côté agent.',
  `encheres_entreprise` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer le module enchère côté entreprise. Ce module s''utilise avec le module encheres de la table ModuleOrganisme.',
  `socle_interne` enum('0','1') NOT NULL default '0' COMMENT 'Permet de transformer l''application MPE comme un socle de portail d''administration électronique du côté des agents. Le socle interne est à activer quand une application différente de MPE est intégrée en sus au portail (ex: parapheur).',
  `module_certificat` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer un lien dans le menu gauche agent dans l''aide>outils informatique qui affiche une page permettant de télécharger le certificat racine de la plate-forme afin de pouvoir utiliser les outils de vérifications de jeton d''horodatage.',
  `socle_externe_agent` enum('0','1') NOT NULL default '0',
  `afficher_image_organisme` enum('0','1') NOT NULL default '0' COMMENT 'Permet de gérer un logo par organisme qui s''affichera dans le bandeau à droite de la plate-forme quand on y accède avec l''acronyme et dans les résultats du moteur de recherche côté entreprise. L''administration du logo se fait dans /administration.',
  `socle_externe_entreprise` enum('0','1') NOT NULL default '0',
  `portail_defense_entreprise` enum('0','1') NOT NULL COMMENT 'Pemet l''Intégration de la gestion de l''inscription des entreprises sur les portails du MINDEF ainsi que le sso en provenance des portails MINDEF.',
  `compte_entreprise_province` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ province dans le formulaire de creation des entreprises, la liste est alimentée à partir de la table GeolocalisationN2',
  `compte_entreprise_telephone3` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ telephone 3 dans le formulaire de creation des entreprises''',
  `compte_entreprise_tax_prof` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ numero de la taxe professionnelle dans le formulaire de creation des entreprises''',
  `compte_entreprise_rcville` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher la ville associée au champ du numero du registe du commerce dans le formulaire de creation des entreprises''',
  `compte_entreprise_declaration_honneur` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ declaration sur l''''honneur dans le formulaire de creation des entreprises'' que l''inscrit coche lors de l''enregistrement.',
  `compte_entreprise_qualification` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur qualification qui sont organisées hiérarchiquement. Ce module utilise la table Qualification de la base commune.',
  `compte_entreprise_moyens_techniques` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ moyen techniques dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent',
  `compte_entreprise_prestations_realisees` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ prestations réalisés dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent',
  `compte_entreprise_chiffre_affaire_production_biens_services` enum('0','1') NOT NULL default '1' COMMENT 'Permet d''afficher le tableau relatif aux donnees financieres de l''entreprise relativement à la vente de marchandises, à la production et aux services sur les 3 dernieres années',
  `enveloppe_offre_technique` enum('0','1') NOT NULL COMMENT 'Permet d''associer une enveloppe d''offre technique',
  `compte_inscrit_msg_ates` enum('0','1') NOT NULL default '0' COMMENT 'pour afficher mon compte utilisateur ou administrateur  dans le formulaire de creation de comptes inscrits',
  `procedure_adaptee` enum('0','1') NOT NULL default '1' COMMENT 'Permet de gérer la visibilité des MAPA dans la plate-forme: accès aux fonctions relatives à la gestion des MAPA, aux statistiques MAPA et les habilitations',
  `compte_entreprise_siren` enum('0','1') NOT NULL default '1' COMMENT 'Permet d''afficher le champ siren et siret dans le formulaire de creation des entreprises',
  `activation_compte_entreprise` enum('0','1') NOT NULL default '0' COMMENT 'Module pour activer le compte entreprise pour qu''il s''affiche dans la recherche coté agent',
  `menu_entreprise_consultations_en_cours` enum('0','1') NOT NULL default '0' COMMENT 'affiche le lien ''consultations en cours'' dans le menu gauche entreprise',
  `compte_entreprise_capital_social` enum('0','1') NOT NULL default '0' COMMENT 'Module pour activer le compte entreprise pour qu''il affiche le champs capital social ',
  `message_confirmation_inscrit_entreprise` enum('0','1') NOT NULL default '0' COMMENT 'Module envoyer mail aux inscrits avc des informations sur le compte entreprise et la procedure pour actvier le compte',
  `decision_date_notification` enum('0','1') NOT NULL default '1' COMMENT 'Module pour afficher la date de notification dans la decision attributaire',
  `decision_pmi_pme` enum('0','1') NOT NULL default '1' COMMENT 'Module pour afficher le champ pmi /pme dans la decision attributaire',
  `decision_nature_prestations` enum('0','1') NOT NULL default '1' COMMENT 'Module pour afficher la nature de Prestations dans la decision attributaire',
  `decision_objet_marche` enum('0','1') NOT NULL default '1' COMMENT 'Module pour afficher objet marche dans la decision attributaire',
  `decision_note` enum('0','1') NOT NULL default '0' COMMENT 'Module pour afficherla note dans la decision attributaire',
  `decision_fiche_recensement` enum('0','1') NOT NULL default '1' COMMENT 'Module pour afficher le lien pour telecharger la fiche de recensement dans la decision attributaire',
  `champ_mail_obligatoire_ajout_registre_papier` enum('0','1') NOT NULL default '0',
  `indicateurs_cles` enum('0','1') NOT NULL default '0' COMMENT 'Permet l''activation du module ''indicateurs clés''',
  `ajout_rpa_champ_email` enum('0','1') NOT NULL default '0',
  `ajout_rpa_champ_telephone` enum('0','1') NOT NULL default '0',
  `ajout_rpa_champ_fax` enum('0','1') NOT NULL default '0',
  `question_sans_pj` enum('0','1') NOT NULL default '0',
  `url_demarche_agent` enum('0','1') NOT NULL default '0',
  `url_demarche_entreprise` enum('0','1') NOT NULL default '0',
  `siret_detail_entite_achat` enum('0','1') NOT NULL default '1',
  `presence_elu` enum('0','1') NOT NULL default '1', 
  `gerer_les_services` enum('0','1') NOT NULL default '1',
  `gerer_mon_service` enum('0','1') NOT NULL default '0',
  `depouillement_enveloppe_depend_RAT_enveloppe_precedente` enum('0','1') NOT NULL default '0',
  `consultation_adresse_retrais_dossiers` enum('0','1') NOT NULL default '0',
  `consultation_adresse_depot_offres` enum('0','1') NOT NULL default '0',
  `consultation_caution_provisoire` enum('0','1') NOT NULL default '0',
  `consultation_lieu_ouverture_plis` enum('0','1') NOT NULL default '0',
  `consultation_qualification` enum('0','1') NOT NULL default '0',
  `consultation_agrement` enum('0','1') NOT NULL default '0',
  `consultation_echantillons_demandes` enum('0','1') NOT NULL default '0',
  `consultation_reunion` enum('0','1') NOT NULL default '0',
  `consultation_visite_des_lieux` enum('0','1') NOT NULL default '0',
  `consultation_prix_acquisition` enum('0','1') NOT NULL default '0',
  `resultat_analyse_avant_decision` enum('0','1') NOT NULL default '0',
  `creation_inscrit_par_ates` enum('0','1') NOT NULL default '0',
  `consultation_variantes_autorisees` enum('0','1') NOT NULL default '0',
  `recherche_avancee_par_type_org` enum('0','1') NOT NULL default '0',
  `societes_exclues` enum('0','1') NOT NULL default '0',
  `recherche_avancee_par_domaines_activite` enum('0','1') NOT NULL default '0',
  `recherche_avancee_par_qualification` enum('0','1') NOT NULL default '0',
  `recherche_avancee_par_agrement` enum('0','1') NOT NULL default '0',
  `contact_administratif_dans_detail_consultation_cote_entreprise` enum('0','1') NOT NULL default '0',
  `consultation_pieces_dossiers` enum('0','1') NOT NULL default '0',
  `gerer_adresses_service` enum('0','1') NOT NULL default '0',
  `traduire_annonces` enum('0','1') NOT NULL default '0',
  `affichier_bloc_actions_dans_detail_annonces` enum('0','1') NOT NULL default '0',
  `autoriser_une_seule_reponse_principale_par_entreprise` enum('0','1') NOT NULL default '0',
  `generation_avis` enum('0','1') NOT NULL default '0',
  `appliquer_donnees_ensemble_lots` enum('0','1') NOT NULL default '0' COMMENT 'permet de reprendre automatiquement pour tous les lots de la consultation les données spécifiques saisies pour un lot',
  `autre_annonce_extrait_pv` enum('0','1') NOT NULL default '0' COMMENT 'Pour la gestion du type d''annonce "Extrait de PV"',
  `autre_annonce_rapport_achevement` enum('0','1') NOT NULL default '0' COMMENT 'Pour la gestion du type d''annonce "Rapport d''achevement"',
  `ajout_fichier_joint_autre_annonce` enum('0','1') NOT NULL default '0' COMMENT 'Pour l''ajout des pièces jointes dans les autres annonces',
  `consultation_mode_passation` enum('0','1') NOT NULL default '0' COMMENT 'afficher les choix de mode passation au rabais et sur offre de prix',
  `compte_entreprise_identifiant_unique` enum('0','1') NOT NULL default '0' COMMENT 'Autorise les entreprises à s''inscrire avec un identifiant unique national rattaché à une ville d''enregistrement au registre de commerce. L''activation de ce module cache l''inscription avec SIREN/SIRET. Il impacte également les fonctions de recherche des en',
  `gerer_certificats_agent` enum('0','1') NOT NULL default '0' COMMENT 'gestion des certificats pour les agents',
  `autre_annonce_programme_previsionnel` enum('0','1') NOT NULL default '0' COMMENT 'Creer autre type annonce programme previsionnel',
  `annuler_consultation` enum('0','1') NOT NULL default '0' COMMENT 'annuler une consultation',
  `cfe_entreprise_accessible_par_agent` enum('0','1') NOT NULL default '1' COMMENT 'Ce module permet de rendre les documents du Coffre Fort des entreprises visibles par les agents. Il faut que le module agent acces_agents_cfe soit actif également.',
  `compte_entreprise_code_nace_referentiel` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ code NACE dans le formulaire de création des entreprises et dans la base de données fournisseurs, en passant par un référentiel externe',
  `code_nut_lt_referentiel` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ ''code nut'' dans le formulaire de création et de recherche avancée des consultations. Ce module fait appel à un referentiel externe',
  `lieux_execution` enum('0','1') NOT NULL default '1' COMMENT 'Permet d''afficher les lieux d''exécution d''une consultation en utilisant la nomenclature classique.',
  `compte_entreprise_domaine_activite_lt_referentiel` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise un référentiel externe.',
  `consultation_domaines_activites_lt_referentiel` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher les domaines d''activités lors de la création de la consultation en utilisant LT-Référentiel',
  `compte_entreprise_agrement_lt_referentiel` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur certification qui sont organisées hiérarchiquement. Ce module utilise un référentiel externe.',
  `compte_entreprise_qualification_lt_referentiel` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur qualification qui sont organisées hiérarchiquement. Ce module utilise un référentiel externe.',
  `reponse_pas_a_pas` enum('0','1') NOT NULL default '0' COMMENT '0004961: PMI : évolution cinématique de signature et de réponse',
  `agent_controle_format_mot_de_passe` enum('0','1') NOT NULL default '0' COMMENT 'ce module permet de controler le format du mot de passe saisit par l''agent, 8 caractères minimum pris dans au moins 3 types de caractères parmi les 4 suivants : Minuscules, Majuscules, Chiffres, Caractères spéciaux.',
  `entreprise_validation_email_inscription` enum('0','1') NOT NULL default '0' COMMENT 'ce module permet de valider l''adresse email saisie par l''entreprise lors de la création du compte.A la création du compte, un mail est automatiquement envoyé par la plateforme à l''entreprise. Ce mail permet de valider l''adresse mail de l''entreprise.',
  `telecharger_dce_avec_authentification` enum('0','1') NOT NULL default '0' COMMENT 'Ce module implique que le téléchargement des DCE doit être fait en étant être inscrit et authentifié.',
  `authentification_basic` enum('0','1') NOT NULL default '0' COMMENT 'ce module permet d''utiliser l''authentification basic de la norme RFC2617',
  `reglement_consultation` enum('0','1') NOT NULL default '1' COMMENT 'Ce module permet d''ajouter le Réglement de la consultation à la création consultation.',
  `annonces_marches` enum('0','1') NOT NULL COMMENT 'Permet de transformer la plate-forme MPE comme un portail d''annonces regroupant les consultations en provenance de plusieurs sources. Si ce module est activé, certaines fonctions du côté entreprise sont adapatées par rapport à une PF MPE classique.',
  `cfe_date_fin_validite_obligatoire` enum('0','1') NOT NULL default '1' COMMENT 'Champ date de fin de validité est obligatoire quand on ajoute un document dans le coffre-fort',
  `associer_documents_cfe_consultation` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''''associer les documents du coffre fort électronique à une consultation, dans ce cas le document est visible par l''agent pendant la phase d''analyse des offres et dans l''archive.',
  `compte_entreprise_region` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ région dans le formulaire de création des entreprises, la liste est alimentée à partir de la table GeolocalisationN1',
  `compte_entreprise_telephone2` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ telephone 2 dans le formulaire de creation des entreprises''',
  `compte_entreprise_cnss` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ numero du cnss dans le formulaire de creation des entreprises''',
  `compte_entreprise_rcnum` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ du numero du registe du commerce dans le formulaire de creation des entreprises''',
  `compte_entreprise_domaine_activite` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise la table Category, SousCategory de la base commune.',
  `compte_inscrit_code_nic` enum('0','1') NOT NULL default '1' COMMENT 'Permet d''afficher le champ code nic dans le formulaire de creation du compte d''un inscrit',
  `compte_entreprise_code_ape` enum('0','1') NOT NULL default '1' COMMENT 'Permet d''afficher le champ code ape dans le formulaire de creation des entreprises''',
  `compte_entreprise_documents_commerciaux` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ ajout de documents commerciaux dans la page du compte entreprise et leur téléchargement dans la page détail entreprise du côté Agent',
  `compte_entreprise_agrement` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur agréement qui sont organisées hiérarchiquement. Ce module utilise la table Agrement de la base commune.',
  `compte_entreprise_moyens_humains` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le champ moyen humains dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent',
  `compte_entreprise_activite_domaine_defense` enum('0','1') NOT NULL default '1' COMMENT 'Permet d''afficher le champ description activités domaine défense dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent',
  `compte_entreprise_donnees_financieres` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''afficher le tableau relatif aux donnees financieres de l''entreprise relativement au CA, au besoin de financemet, au cash flow et à la capacite d\\''endettement'' sur les 3 dernieres années',
  `enveloppe_anonymat` enum('0','1') NOT NULL default '1' COMMENT 'Permet d''associer une enveloppe d''anonymat',
  `publicite_format_xml` enum('0','1') NOT NULL default '1' COMMENT 'Permet de gerer la visibilite de la publicite au format xml dans le menu gauche agent ainsi que les fonctions associées (gestion compte boamp, moniteur, statistiques avis publiés, typologie de mise en ligne de la consultation, habilitations, alertes agent',
  `article_133_generation_pf` enum('0','1') NOT NULL default '1' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction de génération de la liste des marchés à partir de la plate-forme',
  `entreprise_repondre_consultation_apres_cloture` enum('0','1') NOT NULL default '1' COMMENT 'Permet à une entreprise de répondre à la consultation pendant l''affichage prolongé, si l''affichage prolongé a été paramétré pour la consultation',
  `telechargement_outil_verif_horodatage` enum('0','1') NOT NULL default '0' COMMENT 'Affiche un lien permettant de télécharger l''outil de vérification des jetons d''horodatages à partir de la page Autres Outils Informatique',
  `affichage_code_cpv` enum('0','1') NOT NULL default '1' COMMENT 'Permet l''utilisation du module Code CPV dans MPE',
  `consultation_domaines_activites` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer le module qui gère la classification d''une consultation en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise la table Category, SousCategory de la base commune.',
  `statistiques_mesure_demat` enum('0','1') NOT NULL default '1' COMMENT 'Permet de gérer la visibilité de la fonction Mesure Avancement Dematerialisation dans la plate-forme',
  `publication_procure` enum('0','1') NOT NULL default '0' COMMENT 'Gère la visibilité du module ''''Rendre accessible cette consultation dans toutes les langues au portail européen PROCURE"',
  `menu_entreprise_toutes_les_consultations` enum('0','1') NOT NULL default '1' COMMENT 'affiche le lien ''toutes les consultations'' dans le menu gauche entreprise',
  `compte_entreprise_cp_obligatoire` enum('0','1') NOT NULL default '1' COMMENT 'Module pour activer le compte entreprise pour qu''il affiche le champs code postal n''est pas obligatoire ',
  `annuler_depot` enum('0','1') NOT NULL default '0',
  `traduire_entite_achat_arabe` enum('0','1') NOT NULL default '0' COMMENT 'Module pour activer la visibilité des champs à traduire en arabe (côté agent et administration, gestion de l''organisme)',
  `traduire_organisme_arabe` enum('0','1') default '0',
  `decision_cp` enum('0','1') NOT NULL default '1' COMMENT 'Module pour afficher le code postal dans la decision attributaire',
  `decision_tranche_budgetaire` enum('0','1') NOT NULL default '1' COMMENT 'Module pour afficher la tranche budgetaire dans la decision attributaire',
  `decision_classement` enum('0','1') NOT NULL default '0' COMMENT 'Module pour afficher le classement dans la decision attributaire',
  `decision_afficher_detail_candidat_par_defaut` enum('0','1') NOT NULL default '0' COMMENT 'Ce module permet d''afficher le détail du candidat sans avoir à cliquer sur la case à cocher ''attributaire''',
  `liste_marches` enum('0','1') NOT NULL default '0' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction d''import de la liste des marchés à partir de fichier',
  `multi_linguisme_agent` enum('0','1') NOT NULL default '0',
  `compte_entreprise_ifu` enum('0','1') NOT NULL default '0',
  `gestion_organisme_par_agent` enum('0','1') NOT NULL default '0' COMMENT 'Permet à l Hyper admin de gérer les services par organismes. A besoin de mettre habilitation HyperAdmin à 1',
  `utiliser_lucene` enum('0','1') NOT NULL default '1' COMMENT 'à 1 pour utiliser lucène, à 0 pour ne pas utilliser lucène',
  `utiliser_page_html_lieux_execution` enum('0','1') NOT NULL default '1' COMMENT 'à 1 pour utiliser la page .html des lieux executions, à 0 pour utiliser la page commun.LieuxExecution',
  `prado_validateur_format_date` enum('0','1') NOT NULL default '1' COMMENT 'à 1 pour garder actifs les validateurs prado, à 0 sinon',
  `prado_validateur_format_email` enum('0','1') NOT NULL default '1' COMMENT 'à 1 pour garder actifs les validateur email, à 0 sinon',
  `socle_externe_ppp` enum('0','1') NOT NULL default '0' COMMENT 'Socle externe PPP de e-bourgogne',
  `validation_format_champs_stricte` enum('0','1') NOT NULL default '0',
  `poser_question_necessite_authentification` enum('0','1') NOT NULL default '0',
  `autoriser_suppression_dernier_administrateur_entreprise` enum('0','1') NOT NULL default '0',
  `autoriser_modif_profil_inscrit_ates` enum('0','1') default '0' COMMENT 'Ce module permet à l''entreprise qui s''enregistre de choisir son profil (Utilisateur Simple ou Administrateur). Si 0 alors pas de choix possible, le premier inscrit est Administrateur et les autres US.',
  `unicite_reference_consultation` enum('0','1') NOT NULL default '0',
  `champ_rc_obligatoires_ajout_registre_papier` enum('0','1') NOT NULL default '0',
  `champ_adresse_obligatoires_ajout_registre_papier` enum('0','1') NOT NULL default '0',
  `telecharger_dce_sans_identification` enum('0','1') NOT NULL default '0' COMMENT 'possiblité de télécharger le DCE anonymement',
  `gestion_entreprise_par_agent` enum('0','1') NOT NULL default '0',
  `autoriser_caracteres_speciaux_dans_reference` enum('0','1') NOT NULL default '0',
  `inscription_libre_entreprise` enum('0','1') NOT NULL default '1',
  `afficher_code_service` enum('0','1') NOT NULL default '0' COMMENT 'ce module permet d''afficher le code service/entité achat. la valeur est enregistrer dans le champs siren',
  `authenticate_agent_by_login` enum('0','1') NOT NULL default '1' COMMENT 'Rend visible le bloc authentification par identifiant / mot de passe du côté agent et active de cette manière la fonction d''authentification par identifiant',
  `authenticate_agent_by_cert` enum('0','1') NOT NULL default '0' COMMENT 'ce module permet l''authentification par certificat côté agent'
) ENGINE=InnoDB ;

--
-- OKO le 03/03/2011 : Structure de la table `ModuleOrganisme`
--


CREATE TABLE IF NOT EXISTS `configuration_organisme` (
  `organisme` varchar(30) NOT NULL default '',
  `encheres` enum('0','1') NOT NULL default '0',
  `dossier_complementaire` enum('0','1') NOT NULL default '1',
  `registre_retraits_papier` enum('0','1') NOT NULL default '1',
  `systeme_acquisition_dynamique` enum('0','1') NOT NULL default '1',
  `no_activex` enum('0','1') NOT NULL default '0',
  `filtre_cle_permanente_par_service` enum('0','1') NOT NULL default '1',
  `gestion_mapa` enum('0','1') NOT NULL default '1',
  `liste_marches` enum('0','1') NOT NULL default '1',
  `centrale_publication` enum('0','1') NOT NULL default '0',
  `alerte_reponse_electronique` enum('0','1') NOT NULL default '1',
  `gestion_commissions` enum('0','1') NOT NULL default '1',
  `gestion_procedure_administrable` enum('0','1') NOT NULL default '1',
  `organisation_centralisee` enum('0','1') NOT NULL default '1',
  `presence_elu` enum('0','1') NOT NULL default '0',
  `traduire_consultation` enum('0','1') NOT NULL default '0',
  `suivi_passation` enum('0','1') NOT NULL default '0',
  `numerotation_ref_cons` enum('0','1') NOT NULL default '0',
  `portail_defense_agent` enum('0','1') NOT NULL default '0',
  `interface_archive_arcade_pmi` enum('0','1') NOT NULL default '0',
  `acces_agents_cfe` enum('0','1') NOT NULL default '0',
  `desarchivage_consultation` enum('0','1') NOT NULL default '0' COMMENT 'Permet d''activer la fonction de desarchivage d''une consultation',
  `alimentation_automatique_liste_invites` enum('0','1') NOT NULL default '0' COMMENT 'Alimentation automatique de la liste des invités d''une consultation par la désignation du service associé',
  `interface_chorus_pmi` enum('0','1') NOT NULL default '0',
  `archivage_consultation_sur_pf` enum('0','1') NOT NULL default '0',
  `autoriser_modification_apres_phase_consultation` enum('0','1') NOT NULL default '1' COMMENT 'Empêche la modification d''une consultation après phase ''consultation''',
  `importer_enveloppe` enum('0','1') NOT NULL default '1' COMMENT 'Ce module agent est activable par organisme. Il permet d''accéder à la fonction "importer une enveloppe" en phase décision s''il est actif et si l''agent dispose de l''habilitation',
  `export_marches_notifies` enum('0','1') NOT NULL default '0' COMMENT 'Permet à un agent d''un organisme ou d''un service d''accéder à la page de recherche avancée et d''export des marchés notifiés.',
  `acces_agents_cfe_bd_fournisseur` enum('0','1') NOT NULL default '0' COMMENT 'Ce module lorsqu''il est activé permet d''avoir accès aux documents rendus visibles du coffre fort entreprise à partir de la fonction visualiser les entreprises.',
  `acces_agents_cfe_ouverture_analyse` enum('0','1') NOT NULL default '1' COMMENT 'ce module permet à l''agent d''accéder aux documents rendus visibles du coffre fort entreprise à partir de la page ouverture et analyse',
  PRIMARY KEY  (`organisme`)
) ENGINE=InnoDB ;

--
-- Contraintes pour la table `ModuleOrganisme`
--
ALTER TABLE `configuration_organisme` ADD CONSTRAINT `configuration_organisme_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;


--- renomation des champs type_procedure,reponse_electronique,id_type_validation,type_mise_en_ligne,titre,resumee

ALTER TABLE `consultation` ADD `intitule` LONGTEXT NOT NULL ;
ALTER TABLE `consultation` ADD `objet` LONGTEXT NOT NULL ;
ALTER TABLE `consultation` ADD `type_acces` CHAR( 1 ) NOT NULL ;
ALTER TABLE `consultation` ADD `autoriser_reponse_electronique` CHAR( 1 ) NOT NULL DEFAULT '1';
ALTER TABLE `consultation` ADD `regle_mise_en_ligne` INT( 11 ) NOT NULL DEFAULT '1';
ALTER TABLE `consultation` ADD `id_regle_validation` INT( 11 ) NOT NULL DEFAULT '2';

-- AME 
ALTER TABLE `consultation` ADD `intitule_fr` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_en` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_es` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_su` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_du` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_cz` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_ar` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_it` LONGTEXT NULL ;

ALTER TABLE `consultation` ADD `objet_fr` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_en` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_es` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_su` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_du` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_cz` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_ar` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_it` LONGTEXT NULL ;

-- AME
ALTER TABLE `Agent` DROP `admin`;
ALTER TABLE `Agent` DROP `admin_type`;

-- MWA le 01/03/2011, module par défaut pour tout le monde 
UPDATE `configuration_organisme` SET `export_marches_notifies` = '1';

-- MWA le 01/03/2011,module par défaut pour tout le monde pour controler le format mot de passe agent
UPDATE `configuration_plateforme` SET `agent_controle_format_mot_de_passe` = '1';

--FBO Evolution formulaire acte dengagement
ALTER TABLE `configuration_plateforme` ADD `generer_acte_dengagement` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Ce module permet d''activer la génération de l''acte d''engagement lors de l''envoi de la réponse';



-- OKO: module à renommer 
ALTER TABLE `ModuleCommun` CHANGE `multi_linguisme` `multi_linguisme_entreprise`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `gestion_fournisseurs_docs_pole` `gestion_fournisseurs_docs_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `base_qualifiee_entreprise` `base_qualifiee_entreprise_insee`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `donnee_complementaires_entreprise` `compte_entreprise_donnees_complementaires`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `vision_ea_entreprise` `annuaire_entites_achat_visible_par_entreprise`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `ac_sad_transv` `affichage_recherche_avancee_agent_ac_sad_transversaux`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `Enchere_Entreprise` `encheres_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `compte_inscrit_code_etablissement` `compte_inscrit_code_nic`  ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_codeape` `compte_entreprise_code_ape`  ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `ModuleCommun` CHANGE `compte_entreprise_documents_comerciaux` `compte_entreprise_documents_commerciaux`  ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `ModuleCommun` CHANGE `actions_entreprise_consultation_prolongee` `entreprise_repondre_consultation_apres_cloture` ENUM( '0', '1' )  NOT NULL DEFAULT '1' COMMENT 'Permet à une entreprise de répondre à la consultation pendant l''affichage prolongé, si l''affichage prolongé a été paramétré pour la consultation';
ALTER TABLE `ModuleCommun` CHANGE `article_133` `article_133_generation_pf` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction de génération de la liste des marchés à partir de la plate-forme'; 

-- OKO le 27/04/2010: commentaires sur les modules
ALTER TABLE `configuration_plateforme` CHANGE `code_cpv` `code_cpv` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Rend obligatoire la saisie du code cpv à la création de la consultation pour le bloc principal et les lots. 1=saisie cpv obligatoire';
ALTER TABLE `configuration_plateforme` CHANGE `multi_linguisme_entreprise` `multi_linguisme_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Rend l''application MPE multilingue du côté entreprise. L''activation de ce module se fait en parallèle du paramétrage de la table Langue dans la base commune';
ALTER TABLE `configuration_plateforme` CHANGE `gestion_fournisseurs_docs_mes_sous_services` `gestion_fournisseurs_docs_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les fournisseurs de documents, de gérer les fournisseurs de documents de ses sous services';
ALTER TABLE `configuration_plateforme` CHANGE `authenticate_inscrit_by_cert` `authenticate_inscrit_by_cert`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Rend visible le bloc authentification par certificat du côté entreprise et active de cette manière la fonction d''authentification par certificat';
ALTER TABLE `configuration_plateforme` CHANGE `authenticate_inscrit_by_login` `authenticate_inscrit_by_login`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Rend visible le bloc authentification par identifiant / mot de passe du côté entreprise et active de cette manière la fonction d''authentification par identifiant';
ALTER TABLE `configuration_plateforme` CHANGE `base_qualifiee_entreprise_insee` `base_qualifiee_entreprise_insee`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de pré-remplir les informations d''une entreprise lors de l''inscription à partir de la base INSEE. L''interconnexion avec la base INSEE se faisant par Web Services. Ce module est utilisable uniquement en France.';
ALTER TABLE `configuration_plateforme` CHANGE `gestion_boamp_mes_sous_services` `gestion_boamp_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les comptes BOAMP, de gérer les comptes BOAMP de ses sous services';
ALTER TABLE `configuration_plateforme` CHANGE `gestion_bi_cle_mes_sous_services` `gestion_bi_cle_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les clés de chiffrement, de gérer les clés de chiffrements de ses sous services';
ALTER TABLE `configuration_plateforme` CHANGE `nom_entreprise_toujours_visible` `nom_entreprise_toujours_visible`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le nom de l''entreprise dans la phase d''ouverture et d''analyse au niveau du tableau de réponses et de la page de téléchargement des plis chiffrés'; 
ALTER TABLE `configuration_plateforme` CHANGE `gestion_jal_mes_sous_services` `gestion_jal_mes_sous_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les JAL, de gérer les JAL de ses sous services';
ALTER TABLE `configuration_plateforme` CHANGE `choix_langue_affichage_consultation` `choix_langue_affichage_consultation`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher dans la page de résultat de recherche des consultations du côté entreprise, les informations relatives à la consultation dans les autres langues. Ce module s''utilise avec le module organisme traduire_consultation.';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_donnees_complementaires` `compte_entreprise_donnees_complementaires`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne accès à un lien ''Données complémentaires - Organisme de formation'' dans le compte d''une entreprise. Ce lien redirige vers une page qui permet à l''entreprise de préciser son activité dans le domaine de la formation.';
ALTER TABLE `configuration_plateforme` CHANGE `annuaire_entites_achat_visible_par_entreprise` `annuaire_entites_achat_visible_par_entreprise`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le lien ''Visualiser les services'' côté entreprise. C''est l''annuaire des entités d''achat de la plate-forme. Les organismes visibles dans la liste déroulante sont celles qui ont affichage_entite=1 dans la table organisme.';
ALTER TABLE `configuration_plateforme` CHANGE `affichage_recherche_avancee_agent_ac_sad_transversaux` `affichage_recherche_avancee_agent_ac_sad_transversaux` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''accéder aux Accords-cadres et Systèmes d''acquisition dynamique transversaux, en vue de passer un marché subséquent ou spécifique dans le moteur de recherche avancé côté agent.';
ALTER TABLE `configuration_plateforme` CHANGE `encheres_entreprise` `encheres_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module enchère côté entreprise. Ce module s''utilise avec le module encheres de la table ModuleOrganisme.';
ALTER TABLE `configuration_plateforme` CHANGE `socle_interne` `socle_interne` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de transformer l''application MPE comme un socle de portail d''administration électronique du côté des agents. Le socle interne est à activer quand une application différente de MPE est intégrée en sus au portail (ex: parapheur).';
ALTER TABLE `configuration_plateforme` CHANGE `module_certificat` `module_certificat` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer un lien dans le menu gauche agent dans l''aide>outils informatique qui affiche une page permettant de télécharger le certificat racine de la plate-forme afin de pouvoir utiliser les outils de vérifications de jeton d''horodatage.';
ALTER TABLE `configuration_plateforme` CHANGE `afficher_image_organisme` `afficher_image_organisme`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de gérer un logo par organisme qui s''affichera dans le bandeau à droite de la plate-forme quand on y accède avec l''acronyme et dans les résultats du moteur de recherche côté entreprise. L''administration du logo se fait dans /administration.';
ALTER TABLE `configuration_plateforme` CHANGE `portail_defense_entreprise` `portail_defense_entreprise`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Pemet l''Intégration de la gestion de l''inscription des entreprises sur les portails du MINDEF ainsi que le sso en provenance des portails MINDEF.';
ALTER TABLE `configuration_plateforme` CHANGE `annonces_marches` `annonces_marches` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de transformer la plate-forme MPE comme un portail d''annonces regroupant les consultations en provenance de plusieurs sources. Si ce module est activé, certaines fonctions du côté entreprise sont adapatées par rapport à une PF MPE classique.';
ALTER TABLE `configuration_plateforme` CHANGE `associer_documents_cfe_consultation` `associer_documents_cfe_consultation`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''''associer les documents du coffre fort électronique à une consultation, dans ce cas le document est visible par l''agent pendant la phase d''analyse des offres et dans l''archive.';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_region` `compte_entreprise_region`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ région dans le formulaire de création des entreprises, la liste est alimentée à partir de la table GeolocalisationN1';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_province` `compte_entreprise_province`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ province dans le formulaire de creation des entreprises, la liste est alimentée à partir de la table GeolocalisationN2';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_telephone2` `compte_entreprise_telephone2`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ telephone 2 dans le formulaire de creation des entreprises''';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_telephone3` `compte_entreprise_telephone3`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ telephone 3 dans le formulaire de creation des entreprises''';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_cnss` `compte_entreprise_cnss`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ numero du cnss dans le formulaire de creation des entreprises''';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_tax_prof` `compte_entreprise_tax_prof`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ numero de la taxe professionnelle dans le formulaire de creation des entreprises''';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_rcnum` `compte_entreprise_rcnum`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ du numero du registe du commerce dans le formulaire de creation des entreprises''';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_rcville` `compte_entreprise_rcville`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher la ville associée au champ du numero du registe du commerce dans le formulaire de creation des entreprises''';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_domaine_activite` `compte_entreprise_domaine_activite`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise la table Category, SousCategory de la base commune.';
ALTER TABLE `configuration_plateforme` CHANGE `compte_inscrit_code_nic` `compte_inscrit_code_nic`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ code nic dans le formulaire de creation du compte d''un inscrit';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_code_ape` `compte_entreprise_code_ape`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ code ape dans le formulaire de creation des entreprises''';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_declaration_honneur` `compte_entreprise_declaration_honneur`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ declaration sur l''''honneur dans le formulaire de creation des entreprises'' que l''inscrit coche lors de l''enregistrement.';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_documents_commerciaux` `compte_entreprise_documents_commerciaux`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ ajout de documents commerciaux dans la page du compte entreprise et leur téléchargement dans la page détail entreprise du côté Agent';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_qualification` `compte_entreprise_qualification`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur qualification qui sont organisées hiérarchiquement. Ce module utilise la table Qualification de la base commune.';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_agrement` `compte_entreprise_agrement`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur agréement qui sont organisées hiérarchiquement. Ce module utilise la table Agrement de la base commune.';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_moyens_techniques` `compte_entreprise_moyens_techniques`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ moyen techniques dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent';
ALTER TABLE `configuration_plateforme` CHANGE `cfe_date_fin_validite_obligatoire` `cfe_date_fin_validite_obligatoire`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Champ date de fin de validité est obligatoire quand on ajoute un document dans le coffre-fort';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_moyens_humains` `compte_entreprise_moyens_humains`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ moyen humains dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_prestations_realisees` `compte_entreprise_prestations_realisees`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ prestations réalisés dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_activite_domaine_defense`  `compte_entreprise_activite_domaine_defense`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ description activités domaine défense dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_chiffre_affaire_production_biens_services`  `compte_entreprise_chiffre_affaire_production_biens_services`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le tableau relatif aux donnees financieres de l''entreprise relativement à la vente de marchandises, à la production et aux services sur les 3 dernieres années';
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_donnees_financieres` `compte_entreprise_donnees_financieres`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le tableau relatif aux donnees financieres de l''entreprise relativement au CA, au besoin de financemet, au cash flow et à la capacite d\\''endettement'' sur les 3 dernieres années';
ALTER TABLE `configuration_plateforme` CHANGE `enveloppe_offre_technique` `enveloppe_offre_technique`  ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''associer une enveloppe d''offre technique';
ALTER TABLE `configuration_plateforme` CHANGE `enveloppe_anonymat` `enveloppe_anonymat` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''associer une enveloppe d''anonymat';
ALTER TABLE `configuration_plateforme` CHANGE `publicite_format_xml` `publicite_format_xml`  ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gerer la visibilite de la publicite au format xml dans le menu gauche agent ainsi que les fonctions associées (gestion compte boamp, moniteur, statistiques avis publiés, typologie de mise en ligne de la consultation, habilitations, alertes agent';
ALTER TABLE `configuration_plateforme` CHANGE `procedure_adaptee` `procedure_adaptee` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilité des MAPA dans la plate-forme: accès aux fonctions relatives à la gestion des MAPA, aux statistiques MAPA et les habilitations'; 
ALTER TABLE `configuration_plateforme` CHANGE `compte_entreprise_siren` `compte_entreprise_siren` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ siren et siret dans le formulaire de creation des entreprises';
ALTER TABLE `configuration_plateforme` CHANGE `telechargement_outil_verif_horodatage` `telechargement_outil_verif_horodatage` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Affiche un lien permettant de télécharger l''outil de vérification des jetons d''horodatages à partir de la page Autres Outils Informatique';
ALTER TABLE `configuration_plateforme` CHANGE `affichage_code_cpv` `affichage_code_cpv` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet l''utilisation du module Code CPV dans MPE';
ALTER TABLE `configuration_plateforme` CHANGE `consultation_domaines_activites` `consultation_domaines_activites` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification d''une consultation en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise la table Category, SousCategory de la base commune.';
ALTER TABLE `configuration_plateforme` CHANGE `statistiques_mesure_demat` `statistiques_mesure_demat` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilité de la fonction Mesure Avancement Dematerialisation dans la plate-forme';
ALTER TABLE `configuration_plateforme` CHANGE `publication_procure` `publication_procure` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Gère la visibilité du module ''''Rendre accessible cette consultation dans toutes les langues au portail européen PROCURE"';
ALTER TABLE `configuration_plateforme` CHANGE `traduire_entite_achat_arabe` `traduire_entite_achat_arabe` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Module pour activer la visibilité des champs à traduire en arabe (côté agent et administration, gestion de l''organisme)';
ALTER TABLE `configuration_plateforme` CHANGE `decision_afficher_detail_candidat_par_defaut` `decision_afficher_detail_candidat_par_defaut` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Ce module permet d''afficher le détail du candidat sans avoir à cliquer sur la case à cocher ''attributaire''';
ALTER TABLE `configuration_plateforme` CHANGE `liste_marches` `liste_marches` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction d''import de la liste des marchés à partir de fichier';
ALTER TABLE `configuration_plateforme` CHANGE `autoriser_modif_profil_inscrit_ates` `autoriser_modif_profil_inscrit_ates` ENUM( '0', '1' ) NULL DEFAULT '0' COMMENT 'Ce module permet à l''entreprise qui s''enregistre de choisir son profil (Utilisateur Simple ou Administrateur). Si 0 alors pas de choix possible, le premier inscrit est Administrateur et les autres US.';


-- OKO le 28 janvier 2011: renommage des modules communs
ALTER TABLE `configuration_plateforme` CHANGE `liste_marches` `article_133_upload_fichier` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction d''import de la liste des marchés à partir de fichier';
ALTER TABLE `configuration_plateforme` CHANGE `champ_rc_obligatoires_ajout_registre_papier` `registre_papier_rcnum_rcville_obligatoires` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de rendre obligatoire la saisie de la ville et du numéro du registre de commerce. Ce module nécessite l''activation des modules ''compte_entreprise_rcnum'' et ''compte_entreprise_rcville''.';
ALTER TABLE `configuration_plateforme` CHANGE `champ_adresse_obligatoires_ajout_registre_papier` `registre_papier_adresse_cp_ville_obligatoires` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de rendre obligatoire l''adresse, le code postal et la ville lors d''un dépôt papier au niveau des registres. Module actif à 1.';
ALTER TABLE `configuration_plateforme` CHANGE `champ_mail_obligatoire_ajout_registre_papier` `registre_papier_mail_obligatoire` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de rendre obligatoire l''adresse électronique lors de l''ajout d''un dépôt papier par l''acheteur public au niveau des registres.';
ALTER TABLE `configuration_plateforme` CHANGE `indicateurs_cles` `menu_entreprise_indicateurs_cles` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Affiche les ''indicateurs clés'' dans le menu de gauche sur la plateforme des entreprises.';
ALTER TABLE `configuration_plateforme` CHANGE `question_sans_pj` `entreprise_poser_question_sans_pj` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de masquer le bloc d''ajout de fichier joint lorsque l''inscrit de l''entreprise pose une question. Module actif à 1.';
ALTER TABLE `configuration_plateforme` CHANGE `societes_exclues` `menu_agent_societes_exclues` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Donne accès à la fonctionnalité qui permet d''exclure une ou des sociétés à des consultations pour divers motifs sur la plateforme. Accès à partir du menu de gauche.';
ALTER TABLE `configuration_plateforme` CHANGE `appliquer_donnees_ensemble_lots` `passation_appliquer_donnees_ensemble_lots` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de reprendre automatiquement pour tous les lots de la consultation, les données spécifiques saisies pour un lot, dans les données complémentaires de passation.';
ALTER TABLE `configuration_plateforme` CHANGE `affichier_bloc_actions_dans_detail_annonces` `afficher_bloc_actions_dans_details_annonces` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le bloc ''Mes actions'' dans les détails des annonces de types autres que consultations sur la plateforme des acheteurs publics.';
ALTER TABLE `configuration_plateforme` CHANGE `activation_compte_entreprise` `compte_entreprise_activation_inscription_par_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''enregistrer les demandes d''inscription des entreprises sans activer le compte au moment de l''enregistrement. L''activation se faisant par un agent habilité.' ;
ALTER TABLE `configuration_plateforme` CHANGE `message_confirmation_inscrit_entreprise` `mail_activation_compte_inscrit_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de gérer l''envoi d''un mail spécifique lors de l''inscription d''un compte inscrit avec des informations sur le compte entreprise et la procedure pour activer le compte.' ;
ALTER TABLE `configuration_plateforme` CHANGE `compte_inscrit_msg_ates` `compte_inscrit_choix_profil` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher lors de la création d''un compte inscrit le choix du type de compte utilisateur ou administrateur dans le formulaire' ;


-- OKO le 07/01/2011: renommage des module organismes:
ALTER TABLE `configuration_organisme` CHANGE `dossier_complementaire` `consultation_pj_autres_pieces_telechargeables` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet à l''acheteur public d''afficher le bloc ''Autres pièces téléchargeables par les entreprises'' et de pouvoir joindre une pièce lors de la création de la consultation. Ce module s''active par organisme.';
ALTER TABLE `configuration_organisme` CHANGE `liste_marches` `article_133_upload_fichier` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet pour un organisme de gérer la visibilité du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction d''import de la liste des marchés à partir de fichier.';
ALTER TABLE `configuration_organisme` CHANGE `portail_defense_agent` `pmi_lien_portail_defense_agent` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer pour l''organisme le lien d''accès aux portails de la défense. Fonctionnalité accessible dans la page d''accueil des acheteurs publics.';


-- OKO le 28 janvier 2011: suppression des modules communs non utilisés
ALTER TABLE `configuration_plateforme` DROP `liste_marches` ;
ALTER TABLE `configuration_plateforme` DROP `champ_rc_obligatoires_ajout_registre_papier` ;
ALTER TABLE `configuration_plateforme` DROP `champ_adresse_obligatoires_ajout_registre_papier` ;
ALTER TABLE `configuration_plateforme` DROP `champ_mail_obligatoire_ajout_registre_papier` ;
ALTER TABLE `configuration_plateforme` DROP `indicateurs_cles` ;
ALTER TABLE `configuration_plateforme` DROP `question_sans_pj` ;
ALTER TABLE `configuration_plateforme` DROP `societes_exclues` ;
ALTER TABLE `configuration_plateforme` DROP `appliquer_donnees_ensemble_lots` ;
ALTER TABLE `configuration_plateforme` DROP `affichier_bloc_actions_dans_detail_annonces` ;
ALTER TABLE `configuration_plateforme` DROP `activation_compte_entreprise` ;
ALTER TABLE `configuration_plateforme` DROP `message_confirmation_inscrit_entreprise` ;

-- OKO le 28 janvier 2011: suppression des modules communs et organismes non utilisés
ALTER TABLE `configuration_plateforme` DROP `gerer_les_services`;
ALTER TABLE `configuration_plateforme` DROP `autoriser_suppression_dernier_administrateur_entreprise`;
ALTER TABLE `configuration_plateforme` DROP `decision_show_candidats`;
ALTER TABLE `configuration_organisme` DROP `registre_retraits_papier`;
ALTER TABLE `configuration_organisme` DROP `filtre_cle_permanente_par_service`;
ALTER TABLE `configuration_organisme` DROP `alerte_reponse_electronique`;
ALTER TABLE `configuration_organisme` DROP `gestion_commissions`;
ALTER TABLE `configuration_organisme` DROP `gestion_procedure_administrable`;
ALTER TABLE `configuration_organisme` DROP `acces_agents_cfe`;
ALTER TABLE `configuration_organisme` DROP `systeme_acquisition_dynamique` ;
ALTER TABLE `configuration_plateforme` DROP `compte_inscrit_msg_ates` ;
ALTER TABLE `configuration_organisme` DROP `portail_defense_agent`;


-- OKO: suppression des modules renommés
ALTER TABLE `configuration_plateforme` DROP `multi_linguisme` ;
ALTER TABLE `configuration_plateforme` DROP `gestion_fournisseurs_docs_pole` ;
ALTER TABLE `configuration_plateforme` DROP `base_qualifiee_entreprise` ;
ALTER TABLE `configuration_plateforme` DROP `donnee_complementaires_entreprise` ;
ALTER TABLE `configuration_plateforme` DROP `vision_ea_entreprise` ;
ALTER TABLE `configuration_plateforme` DROP `ac_sad_transv` ;
ALTER TABLE `configuration_plateforme` DROP `Enchere_Entreprise` ;
ALTER TABLE `configuration_plateforme` DROP `compte_inscrit_code_etablissement` ;
ALTER TABLE `configuration_plateforme` DROP `compte_entreprise_codeape` ;
ALTER TABLE `configuration_plateforme` DROP `compte_entreprise_documents_comerciaux` ;
ALTER TABLE `configuration_plateforme` DROP `actions_entreprise_consultation_prolongee` ;
ALTER TABLE `configuration_plateforme` DROP `article_133` ;
ALTER TABLE `configuration_plateforme` DROP `gestion_agent_mes_sous_services`;

-- OKO le 31 janvier 2011: modification de la valeur par défaut de modules communs
ALTER TABLE `configuration_plateforme` CHANGE `gestion_fournisseurs_docs_mes_sous_services` `gestion_fournisseurs_docs_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les fournisseurs de documents, de gérer les fournisseurs de documents de ses sous services.' ;
ALTER TABLE `configuration_plateforme` CHANGE `gestion_boamp_mes_sous_services` `gestion_boamp_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les comptes BOAMP, de gérer les comptes BOAMP de ses sous services.' ;
ALTER TABLE `configuration_plateforme` CHANGE `gestion_bi_cle_mes_sous_services` `gestion_bi_cle_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les clés de chiffrement, de gérer les clés de chiffrements de ses sous services.' ;
ALTER TABLE `configuration_plateforme` CHANGE `gestion_jal_mes_sous_services` `gestion_jal_mes_sous_services` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les JAL, de gérer les JAL de ses sous services' ;
ALTER TABLE `configuration_plateforme` CHANGE `annuaire_entites_achat_visible_par_entreprise` `annuaire_entites_achat_visible_par_entreprise` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''activer le lien ''Visualiser les services'' côté entreprise. C''est l''annuaire des entités d''achat de la plate-forme. Les organismes qui apparaissent dans la liste déroulante sont celles qui ont active et affichage_entite=1 dans la table organisme.' ;

-- OKO le 31 janvier 2011: modification des commentaires sur des modules
ALTER TABLE `configuration_plateforme` CHANGE `annuler_depot` `annuler_depot` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la fonctionnalité d''annulation de dépôt électronique par l''inscrit de l''entreprise dans "mon compteà Mes réponses" et de dépôt papier par l''acheteur public dans les registres de dépôt de la consultation.' ;


-- KAT 18/01/2011: Ajout d'un module pour permettre le contrôle sur le format du mot de passe
ALTER TABLE `configuration_plateforme` ADD `entreprise_controle_format_mot_de_passe` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'ce module permet de controler le format du mot de passe saisit par l''entreprise, 8 caractères minimum pris dans au moins 3 types de caractères parmi les 4 suivants : Minuscules, Majuscules, Chiffres, Caractères spéciaux.';

-- OKO - ajout d'un nouveau module commun
ALTER TABLE `configuration_plateforme` ADD `autre_annonce_information` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de créer et gérer les annonces de type ''Information'' sur la plateforme. Fonctionnalité accessible dans le menu de gauche, les détails des annonces et la recherche avancée.' AFTER `entreprise_controle_format_mot_de_passe` ;
ALTER TABLE `configuration_plateforme` ADD `creer_autre_annonce` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet de créer une nouvelle annonce autre que consultation dans le menu de gauche des agents.' AFTER `autre_annonce_information` ;

ALTER TABLE `ModuleCommun` DROP `scan_antivirus`;
ALTER TABLE `configuration_plateforme` DROP `scan_antivirus`;

-- OKO le 06 avril 2011: ajout d'un nouveau module organisme
ALTER TABLE `ModuleOrganisme` ADD `utiliser_parametrage_encheres` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''utiliser le module enchères parametrées par défaut sur une enchère inversée.' AFTER `acces_agents_cfe_ouverture_analyse` ;
ALTER TABLE `configuration_organisme` ADD `utiliser_parametrage_encheres` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''utiliser le module enchères parametrées par défaut sur une enchère inversée.' AFTER `acces_agents_cfe_ouverture_analyse` ;

-- AME 8609: CR-IDF : Evolution : Ajout des Clauses sociales et Environnementales

ALTER TABLE `consultation` ADD `clause_sociale` ENUM( '0', '1', '2' ) NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non';
ALTER TABLE `consultation` ADD `clause_environnementale` ENUM( '0', '1', '2' ) NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non';
ALTER TABLE `configuration_plateforme` ADD `consultation_clause` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'ce module permet de renseigner les clauses de la consultation';

ALTER TABLE `CategorieLot` ADD `clause_sociale` ENUM( '0', '1', '2' ) NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non';
ALTER TABLE `CategorieLot` ADD `clause_environnementale` ENUM( '0', '1', '2' ) NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non';

-- HHA le 22 avril 2011: ajout d'un nouveau champ dans la table consultation
ALTER TABLE `consultation` ADD `reponse_obligatoire` CHAR( 1 ) NOT NULL DEFAULT '0';

