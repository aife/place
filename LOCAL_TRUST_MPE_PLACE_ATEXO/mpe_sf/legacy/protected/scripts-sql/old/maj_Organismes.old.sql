-- Script de mise à jour des BDD propres aux organismes
--
-- CONSIGNES: 
-- Chaque nouvelle requete doit commencer au debut d'une ligne et se finir par un point virgule suivi d'un retour à la ligne
-- si besoin est de splitter en plusieurs lignes, mettre une tabulation
-- Rajouter des  IF NOT EXISTS  lors de la creation de tables
-- Mettre des REPLACE au lieu des INSERT car ce script sera lancé plusieurs fois !!!
-- Lorsqu'un champs a deja ete ajoute avec ALTER TABLE xxx ADD, s'il doit etre modifie par la suite faire un ALTER TABLE xxx CHANGE ! (car on ne peut pas reajouter un champ)

-- known bug : pour le moment, il ne faut pas qu'il y ait de ";" dans une requete car je m'en sers comme delimiteur de fin de requete

ALTER TABLE Annonce ADD envoi_joue enum( '0', '1' ) NOT NULL , ADD mapa enum( '0', '1' ) NOT NULL , ADD implique_SAD enum( '0', '1' ) NOT NULL;
CREATE TABLE IF NOT EXISTS `InterneConsultationSuiviSeul` ( `id` int(11) NOT NULL auto_increment, `interne_id` int(11) NOT NULL default '0', `consultation_ref` varchar(255) NOT NULL default '0', PRIMARY KEY  (`id`), KEY `interne_id` (`interne_id`), KEY `consultation_ref` (`consultation_ref`)) TYPE=InnoDB;
ALTER TABLE `InterneConsultationSuiviSeul` ADD CONSTRAINT `C55982` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE TABLE IF NOT EXISTS `FichierLot` ( `id` int(11) NOT NULL auto_increment, `consultation_ref` varchar(255) NOT NULL default '', `lot` int(11) NOT NULL default '0',`fichier_pli` int(11) NOT NULL default '0',`nom_fichier_pli` varchar(80) NOT NULL default '',`statut` char(1) NOT NULL default '1',`nom_fichier` varchar(80) NOT NULL default '',`ancien_fichier` varchar(80) NOT NULL default '',`horodatage` longblob NOT NULL,`untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',`agent_id` int(11) NOT NULL default '0',PRIMARY KEY  (`id`), KEY `consultation_ref` (`consultation_ref`)) TYPE=InnoDB;
ALTER TABLE `Telechargement` ADD `lots` VARCHAR( 255 ) NULL DEFAULT '';
CREATE TABLE IF NOT EXISTS `AnnonceMoniteur` ( `id` int(22) NOT NULL auto_increment, `date_envoi` date NOT NULL default '0000-00-00', `type_moniteur` text NOT NULL, `statut_envoi` enum('0','1','2','3','4','5') NOT NULL default '0', `date_creation` date NOT NULL default '0000-00-00', `statut_web` enum('0','1','2','3','4','5') NOT NULL default '0',`statut_arg` enum('0','1','2','3','4','5') NOT NULL default '0',`statut_usn` enum('0','1','2','3','4','5') NOT NULL default '0', `statut_01i` enum('0','1','2','3','4','5') NOT NULL default '0', `date_pub` date NOT NULL default '0000-00-00', `num_annonce` int(11) NOT NULL default '0', `message_error` text NOT NULL, `timestamp` blob NOT NULL, `nom_timestamp` varchar(100) NOT NULL default '', `xml_moniteur` text NOT NULL, `id_annonce_boamp` int(22) NOT NULL default '0', PRIMARY KEY  (`id`), KEY `id_annonce_boamp` (`id_annonce_boamp`) ) TYPE=InnoDB AUTO_INCREMENT=1 ;
CREATE TABLE IF NOT EXISTS  `CompteMoniteur` ( `id` int(22) NOT NULL auto_increment, `moniteur_login` varchar(100) NOT NULL default '', `moniteur_password` varchar(100) NOT NULL default '', `moniteur_mail` varchar(100) NOT NULL default '', `moniteur_target` char(1) NOT NULL default '0', PRIMARY KEY  (`id`) ) TYPE=InnoDB AUTO_INCREMENT=1 ; 
ALTER TABLE `AnnonceMoniteur` ADD CONSTRAINT `AnnonceMoniteur_ibfk_1` FOREIGN KEY (`id_annonce_boamp`) REFERENCES `Annonce` (`id_boamp`) ON DELETE CASCADE ON UPDATE CASCADE; 
CREATE TABLE IF NOT EXISTS `Agent_Commission` (  `id_commission` int(11) NOT NULL default '0', `id_agent` int(11) NOT NULL default '0', `convocation` longblob, `convoc_send` char(1) NOT NULL default '0', `nom_convoc` varchar(255) default NULL, `contenu_envoi` text, `fichier_envoye` longblob, `nom_fichier_envoye` varchar(255) default NULL, `date_envoi` datetime default NULL, PRIMARY KEY  (`id_commission`,`id_agent`) ) TYPE=InnoDB;   
CREATE TABLE IF NOT EXISTS `Commission` ( `id` int(11) NOT NULL auto_increment, `date` datetime NOT NULL default '0000-00-00 00:00:00', `lieu` varchar(200) NOT NULL default '', `salle` varchar(100) NOT NULL default '', `type` varchar(200) NOT NULL default '', `libelle` varchar(250) default NULL, `ordre_du_jour` longblob, `invitations_zip` longblob, `status_cao` char(2) NOT NULL default '', PRIMARY KEY  (`id`) ) TYPE=InnoDB;  
CREATE TABLE IF NOT EXISTS `Intervenant_Externe` ( `id` int(11) NOT NULL auto_increment, `nom` varchar(30) NOT NULL default '', `prenom` varchar(30) NOT NULL default '', `organisation` varchar(30) NOT NULL default '', `fonction` varchar(200) NOT NULL default '', `adresse` varchar(255) NOT NULL default '', `mail` varchar(100) NOT NULL default '', `type_invitation` int(1) NOT NULL default '1', `civilite` varchar(255) default '', PRIMARY KEY  (`id`) ) TYPE=InnoDB;   
CREATE TABLE IF NOT EXISTS `Intervenant_Ordre_Du_Jour` ( `id` int(11) NOT NULL auto_increment, `id_ordre_du_jour` int(11) NOT NULL default '0', `id_intervenant` int(11) NOT NULL default '0', `id_agent` int(11) NOT NULL default '0', `convocation` longblob, `convoc_send` char(1) NOT NULL default '0', `nom_convoc` varchar(255) default NULL, `contenu_envoi` text, `nom_fichier_envoye` varchar(255) default NULL, `fichier_envoye` longblob, `date_envoi` datetime default NULL, PRIMARY KEY  (`id`), KEY `id_ordre_du_jour` (`id_ordre_du_jour`) ) TYPE=InnoDB;   
CREATE TABLE IF NOT EXISTS `Ordre_Du_Jour` ( `id` int(11) NOT NULL auto_increment, `id_commission` int(11) NOT NULL default '0', `ref_consultation` varchar(255) default NULL, `ref_libre` varchar(50) default NULL, `id_etape` int(11) NOT NULL default '1', `intitule_ordre_du_jour` varchar(255) default NULL, `lots_odj_libre` varchar(100) default NULL, `id_type_procedure` int(11) default NULL, `date_cloture` datetime default NULL, `type_env` int(1) default NULL, `sous_pli` int(2) default NULL, `heure` varchar(5) NOT NULL default '00', `etape_consultation` varchar(200) NOT NULL default '', `type_consultation` varchar(200) NOT NULL default '', `minutes` char(2) NOT NULL default '', PRIMARY KEY  (`id`), KEY `id_commission` (`id_commission`) ) TYPE=InnoDB ;   
CREATE TABLE IF NOT EXISTS `Intervenant_Externe_Commission` (`id_commission` int(11) NOT NULL default '0', `id_intervenant_externe` int(11) NOT NULL default '0',`convocation` longblob, `convoc_send` char(1) NOT NULL default '0', `nom_convoc` varchar(255) default NULL, `contenu_envoi` text, `nom_fichier_envoye` varchar(255) default NULL, `fichier_envoye` longblob, `date_envoi` datetime default NULL, PRIMARY KEY  (`id_commission`,`id_intervenant_externe`), KEY `id_intervenant_externe` (`id_intervenant_externe`)) TYPE=InnoDB;
ALTER TABLE `Agent_Commission` ADD CONSTRAINT `fk_cao_ag` FOREIGN KEY (`id_commission`) REFERENCES `Commission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;   
ALTER TABLE `Intervenant_Externe_Commission` ADD CONSTRAINT `fk_cao_ext` FOREIGN KEY (`id_intervenant_externe`) REFERENCES `Intervenant_Externe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, ADD CONSTRAINT `fk_odj_exte_com` FOREIGN KEY (`id_commission`) REFERENCES `Commission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;   
ALTER TABLE `Intervenant_Ordre_Du_Jour` ADD CONSTRAINT `fk_odj_com` FOREIGN KEY (`id_ordre_du_jour`) REFERENCES `Ordre_Du_Jour` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;   
ALTER TABLE `Ordre_Du_Jour` ADD CONSTRAINT `fk_cao_odj` FOREIGN KEY (`id_commission`) REFERENCES `Commission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, ADD CONSTRAINT `fk_frm_hab` FOREIGN KEY (`id_commission`) REFERENCES `Commission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE; 

--date : 02/05/2007 ; auteur : RIAD Nawal ; raison : FS Gestion des types de MAPA
ALTER TABLE `TypeProcedure` ADD `mapa` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `TypeProcedure` ADD `activer_mapa` ENUM ('0' , '1')  NOT NULL default '1';

--date : 02/05/2007 ; auteur : RIAD Nawal ; raison : FS Validation des types de procédures
ALTER TABLE `consultation` ADD `id_type_validation` int(11) NOT NULL default '1';
ALTER TABLE `consultation` CHANGE `id_type_validation` `id_type_validation` INT( 11 ) NOT NULL DEFAULT '2';
ALTER TABLE `consultation` ADD `etat_approbation` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `consultation` ADD `etat_validation` ENUM ('0' , '1')  NOT NULL default '0';
ALTER TABLE `consultation` ADD `id_approbateur` int(11) NOT NULL default '0';
ALTER TABLE `consultation` ADD `id_valideur` int(11) NOT NULL default '0';
ALTER TABLE `TypeProcedure` ADD `id_type_validation` int(11) NOT NULL default '1';
ALTER TABLE `TypeProcedure` CHANGE `id_type_validation` `id_type_validation` INT( 11 ) NOT NULL DEFAULT '2';
ALTER TABLE `consultation` ADD `service_validation` int(11) NOT NULL default '0';
ALTER TABLE `TypeProcedure` ADD `service_validation` int(11) NOT NULL default '0';

--date : 02/05/2007 ; auteur : RIAD Nawal ; raison : FS Procedures administrables
ALTER TABLE `ProcedureEquivalence` ADD `mise_en_ligne1` CHAR( 2 ) NOT NULL , ADD `mise_en_ligne2` CHAR( 2 ) NOT NULL , ADD `mise_en_ligne3` CHAR( 2 ) NOT NULL , ADD `mise_en_ligne4` CHAR( 2 ) NOT NULL , ADD `env_offre_type_unique` CHAR( 2 ) NOT NULL , ADD `env_offre_type_multiple` CHAR( 2 ) NOT NULL , ADD `no_fichier_annonce` CHAR( 2 ) NOT NULL , ADD `fichier_importe` CHAR( 2 ) NOT NULL , ADD `fichier_boamp` CHAR( 2 ) NOT NULL , ADD `reglement_cons` CHAR( 2 ) NOT NULL ,ADD `dossier_dce` CHAR( 2 ) NOT NULL , ADD `partial_dce_download` CHAR( 2 ) NOT NULL , ADD `service` CHAR( 2 ) NOT NULL , ADD `constitution_dossier_reponse` CHAR( 2 ) NOT NULL , ADD `env_offre_type_unique2` CHAR( 2 ) NOT NULL , ADD `env_offre_type_multiple2` CHAR( 2 ) NOT NULL , ADD `gestion_envois_postaux` CHAR( 2 ) NOT NULL , ADD `tireur_plan_non` CHAR( 2 ) NOT NULL , ADD `tireur_plan_oui` CHAR( 2 ) NOT NULL , ADD `tireur_plan_papier` CHAR( 2 ) NOT NULL , ADD `tireur_plan_cdrom` CHAR( 2 ) NOT NULL , ADD `tireur_plan_nom` CHAR( 2 ) NOT NULL , ADD `tirage_descriptif` CHAR( 2 ) NOT NULL ; 

--date : 03/05/2007 ; auteur : DIAGNE OUSSEYNOU ; raison : Ajout de champ au niveau de la table AcheteurPublic où les infos pour chaque type de formulaire BOAMP seront stockés  
ALTER TABLE `AcheteurPublic` ADD `default_form_values` longtext NOT NULL;
ALTER TABLE `AcheteurPublic` ADD `defaut_form_am_boamp` longtext NOT NULL, ADD `defaut_form_am_boamp_joue` longtext NOT NULL, ADD `defaut_form_mapa_boamp` longtext NOT NULL, ADD `defaut_form_aconcours` longtext NOT NULL, ADD `defaut_form_as_boamp_joue` longtext NOT NULL, ADD `defaut_form_aa_boamp` longtext NOT NULL, ADD `defaut_form_ar_mapa_boamp` longtext NOT NULL;

-- date : 03/05/2007 ; auteur : RIAD Nawal ; raison : FS génération automatique de codes restreints
ALTER TABLE `consultation` CHANGE `code_procedure` `code_procedure` VARCHAR( 9 ) NULL DEFAULT NULL;

--date : 07/05/2007 ; auteur : GOUBA Nadine ; raison : FS Champ supp invisible
ALTER TABLE `consultation` ADD `champ_supp_invisible` varchar(100) NOT NULL default '';

--date : 07/05/2007 ; auteur : GOUBA Nadine ; raison : FS Champ supp par lot
ALTER TABLE `CategorieLot` ADD `description_detail` VARCHAR( 255 ) NULL ;

--date : 10/05/2007 ; auteur : GOUBA Nadine ; raison : FS Champ supp invisible
CREATE TABLE IF NOT EXISTS  `ChampParametrable` (`id_champ` int(11) NOT NULL auto_increment, `libelle_champ_supp_invisible` varchar(30) default '', PRIMARY KEY  (`id_champ`)) TYPE=InnoDB AUTO_INCREMENT=1;
REPLACE INTO `ChampParametrable` (`id_champ` , `libelle_champ_supp_invisible` ) VALUES (1, 'Commentaire interne');

--date : 22/05/2007 ; auteur : Laurent C ; raison : déplacement depuis create_db.php vers son emplacement approprié (FS Gestion des types de MAPA)
--UPDATE `TypeProcedure` SET `activer_mapa`='1';
--UPDATE `TypeProcedure` SET `mapa`='1' WHERE type_boamp = '23'; --where `id_type_procedure` in ('4', '5', '6', '7');

-- date : 23/05/2007 ; auteur : NAWAL RIAD ; raison : FS gestion type validation
--UPDATE `TypeProcedure` SET `id_type_validation`='2' WHERE id_type_validation = 1 ;
--UPDATE `consultation` SET `id_type_validation`='2';

-- synchro des vielles tables
ALTER TABLE `TypeProcedure` ADD `delai_alerte` int(11) NOT NULL default '0' AFTER `categorie_procedure`;
ALTER TABLE `ProcedureEquivalence` ADD  `envoi_mail_par_mpe` char(2) NOT NULL default '0' AFTER `procedure_restreinte_offre`;
ALTER TABLE `ProcedureEquivalence` ADD  `no_envoi_mail_par_mpe` char(2) NOT NULL default '0' AFTER `envoi_mail_par_mpe`;
ALTER TABLE `CertificatPermanent` ADD  `service_id` int(11) NOT NULL default '-1' AFTER `Certificat`;
ALTER TABLE `consultation` ADD  `datefin_sad` datetime NOT NULL default '0000-00-00 00:00:00' AFTER `reference_connecteur`;
ALTER TABLE `consultation` ADD   `is_sys_acq_dyn` int(11) NOT NULL default '0' AFTER `datefin_sad`;
ALTER TABLE `consultation` ADD   `cons_statut` char(1) NOT NULL default '0' AFTER `is_sys_acq_dyn`;
ALTER TABLE `consultation`  ADD  `champ_supp_invisible` varchar(100) NOT NULL default '' AFTER `cons_statut`;
ALTER TABLE `Mail` ADD   `num_ar` varchar(30) NOT NULL default '0' AFTER `agent_id`;

CREATE TABLE IF NOT EXISTS `admission_systeme_acquisition` (`id` int(11) NOT NULL auto_increment,`num_reponse` int(11) NOT NULL default '0',`is_reponse_electronique` int(11) NOT NULL default '0',`id_entreprise` int(11) NOT NULL default '0',`nom_entreprise` varchar(200) NOT NULL default '',`id_agent` int(11) NOT NULL default '0',`nom_agent` varchar(200) NOT NULL default '',`cons_ref` varchar(255) NOT NULL default '',`date_admission` datetime NOT NULL default '0000-00-00 00:00:00',`id_offre` int(22) NOT NULL default '0',PRIMARY KEY  (`id`),KEY `cons_ref` (`cons_ref`),KEY `id_offre` (`id_offre`)) TYPE=InnoDB;
CREATE TABLE IF NOT EXISTS `ARMail` (`id` int(11) NOT NULL auto_increment,`consultation_ref` varchar(255) NOT NULL default '',`num_ar` varchar(30) NOT NULL default '0',`nom` varchar(100) NOT NULL default '',`email` varchar(100) NOT NULL default '',`entreprise` varchar(100) NOT NULL default '',`cmd` varchar(30) NOT NULL default '',`date_envoi_mail` datetime NOT NULL default '0000-00-00 00:00:00',`confirmed` enum('0','1') NOT NULL default '0',`date_confirmation` datetime NOT NULL default '0000-00-00 00:00:00',`lot` int(11) NOT NULL default '0',PRIMARY KEY  (`id`),KEY `consultation_ref` (`consultation_ref`),KEY `mail_id` (`num_ar`),KEY `date_reception` (`date_confirmation`)) TYPE=InnoDB;
CREATE TABLE IF NOT EXISTS `DATEFIN_sad` (`id` int(11) NOT NULL auto_increment,`consultation_ref` varchar(255) NOT NULL default '',`datefin` datetime NOT NULL default '0000-00-00 00:00:00',`statut` char(1) NOT NULL default '1',`horodatage` longblob NOT NULL,`untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',`agent_id` int(11) NOT NULL default '0',PRIMARY KEY  (`id`),KEY `consultation_ref` (`consultation_ref`)) TYPE=InnoDB;
CREATE TABLE IF NOT EXISTS `Etape_cao` (`id_etape_cao` int(11) NOT NULL auto_increment,`intitule_etape_cao` varchar(250) NOT NULL default '',PRIMARY KEY  (`id_etape_cao`)) TYPE=InnoDB;
CREATE TABLE IF NOT EXISTS `historique_actions_systeme_acquisition` (`id_entreprise` int(11) NOT NULL default '0',`num_reponse` int(11) NOT NULL default '0',`is_reponse_electronique` int(11) NOT NULL default '0',`nom_entreprise` varchar(200) NOT NULL default '',`id_agent` int(11) NOT NULL default '0',`nom_agent` varchar(200) NOT NULL default '',`cons_ref` varchar(255) NOT NULL default '',`date_action` datetime NOT NULL default '0000-00-00 00:00:00',`id_offre` int(22) NOT NULL default '0',`type_action` int(11) NOT NULL default '0',KEY `cons_ref` (`cons_ref`),KEY `id_offre` (`id_offre`)) TYPE=InnoDB;

ALTER TABLE `admission_systeme_acquisition` ADD CONSTRAINT `admission_systeme_acquisition_ibfk_1` FOREIGN KEY (`cons_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE, ADD CONSTRAINT `admission_systeme_acquisition_ibfk_2` FOREIGN KEY (`id_offre`) REFERENCES `Offre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `ARMail` ADD CONSTRAINT `ARMail_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `DATEFIN_sad` ADD CONSTRAINT `C51028` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `historique_actions_systeme_acquisition` ADD CONSTRAINT `admission_systeme_acquisition_ibfk_4` FOREIGN KEY (`id_offre`) REFERENCES `Offre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, ADD CONSTRAINT `admission_systeme_acquisition_ibfk_5` FOREIGN KEY (`cons_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;
-- date : 25/07/2007 ; auteur : NAWAL RIAD ; raison : FS centrale de publication MEDIALEX
CREATE TABLE IF NOT EXISTS `AcheteurPublicCentrale` (`id_compte` int(11) NOT NULL auto_increment,`identifiant_compte` varchar(255) NOT NULL default '',`email` varchar(255) NOT NULL default '',`telecopieur` varchar(255) NOT NULL default '',`centrale_publication` varchar(255) NOT NULL default '',PRIMARY KEY  (`id_compte`)) TYPE=InnoDB AUTO_INCREMENT=1 ;
--date : 07/06/2007 ; auteur : DIAGNE OUSSEYNOU ; raison : Ajout de champ au niveau de la table AcheteurPublic (pour le formulaire_05) où les infos pour chaque type de formulaire BOAMP seront stockés  
ALTER TABLE `AcheteurPublic` ADD `defaut_form_05_boamp` longtext NOT NULL;

--date : 03/08/2007 ; auteur : DIAGNE OUSSEYNOU ; raison : Ajout de champ au niveau de la table AcheteurPublic (pour le formulaire avis rectificatif) où les infos pour chaque type de formulaire BOAMP seront stockés  
ALTER TABLE `AcheteurPublic` ADD `defaut_form_rect` longtext NOT NULL;
--date : 17/08/2007 ; auteur : DIAGNE OUSSEYNOU ; raison : Ajout de champ au niveau de la table AcheteurPublic (pour le formulaire 03 (attribution boamp et joue) où les infos pour chaque type de formulaire BOAMP seront stockés  
ALTER TABLE `AcheteurPublic` ADD `defaut_form_aa_boamp_joue` longtext NOT NULL;

-- date : 10/10/2007; auteur : KBE  : nouvelle fs Code Cpv

ALTER TABLE `consultation` ADD `code_cpv_1`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `code_cpv_2`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `code_cpv_3`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `code_cpv_4`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `code_cpv_1`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `code_cpv_2`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `code_cpv_3`  VARCHAR( 8 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `code_cpv_4`  VARCHAR( 8 ) NULL DEFAULT NULL ;

ALTER TABLE `consultation` ADD `libelle_code_cpv_1`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `libelle_code_cpv_2`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `libelle_code_cpv_3`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` ADD `libelle_code_cpv_4`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `libelle_code_cpv_1`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `libelle_code_cpv_2`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `libelle_code_cpv_3`  VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `CategorieLot` ADD `libelle_code_cpv_4`  VARCHAR( 100 ) NULL DEFAULT NULL ;

--date : 05/10/2007 auteur : AZA : Ouverture simultanée ( 4 Eyes )
ALTER TABLE `Enveloppe` ADD `agent_id_ouverture2` INT NULL DEFAULT NULL AFTER `agent_id_ouverture` ;

--date : 28/11/2007 auteur : AZA : Multilinguisme dynamique

ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_fr` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_en` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_es` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_su` VARCHAR( 100 ) NULL ;
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_du` VARCHAR( 100 ) NULL ; 
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_cz` VARCHAR( 100 ) NULL ;

ALTER TABLE `CategorieConsultation` ADD `libelle_fr` VARCHAR( 100 ) NULL ;
ALTER TABLE `CategorieConsultation` ADD `libelle_en` VARCHAR( 100 ) NULL ;
ALTER TABLE `CategorieConsultation` ADD `libelle_es` VARCHAR( 100 ) NULL ;
ALTER TABLE `CategorieConsultation` ADD `libelle_su` VARCHAR( 100 ) NULL ;
ALTER TABLE `CategorieConsultation` ADD `libelle_du` VARCHAR( 100 ) NULL ; 
ALTER TABLE `CategorieConsultation` ADD `libelle_cz` VARCHAR( 100 ) NULL ;


ALTER TABLE `consultation` ADD `titre_fr` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `titre_en` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `titre_es` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `titre_su` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `titre_du` LONGTEXT  NULL ; 
ALTER TABLE `consultation` ADD `titre_cz` LONGTEXT  NULL ;

ALTER TABLE `consultation` ADD `resume_fr` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `resume_en` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `resume_es` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `resume_su` LONGTEXT  NULL ;
ALTER TABLE `consultation` ADD `resume_du` LONGTEXT  NULL ; 
ALTER TABLE `consultation` ADD `resume_cz` LONGTEXT  NULL ;


ALTER TABLE `CategorieLot` ADD `description_fr` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_en` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_es` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_su` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_du` VARCHAR( 255 ) NULL ; 
ALTER TABLE `CategorieLot` ADD `description_cz` VARCHAR( 255 ) NULL ;

ALTER TABLE `CategorieLot` ADD `description_detail_fr` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_detail_en` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_detail_es` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_detail_su` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` ADD `description_detail_du` VARCHAR( 255 ) NULL ; 
ALTER TABLE `CategorieLot` ADD `description_detail_cz` VARCHAR( 255 ) NULL ;

-- date : à mettre à jour ; auteur : AZA : fonctionnalité CROSS-BORDER
ALTER TABLE `consultation` ADD `publication_europe` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `etat_publication` INT( 11 ) NOT NULL DEFAULT '0';

-- 07-04-2008 : mmi :modifier tous les cahmps DateTime dans la table consultation en Varchar(20)
ALTER TABLE `consultation` CHANGE `datedebut` `datedebut` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00',
CHANGE `datefin` `datefin` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00',
CHANGE `datevalidation` `datevalidation` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00',
CHANGE `datemiseenligne` `datemiseenligne` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';

-- 10-04-2008 : kbe : ajouter le service associé à une consultation		
ALTER TABLE `consultation` ADD `service_associe_id` INT( 11 ) NOT NULL AFTER `etat_publication`;

-- 15-04-2008 : ael : ajouter l'auteur de la consultation (id, non , prénom) et le représentant du pouvoir adjudicateur
ALTER TABLE `consultation` ADD `id_createur` INT( 11 ) NOT NULL ;
ALTER TABLE `consultation` ADD `id_rpa` INT( 11 ) NOT NULL ;
ALTER TABLE `consultation` ADD `nom_createur` VARCHAR( 100 ) NULL , ADD `prenom_createur` VARCHAR( 100 ) NULL ;
ALTER TABLE `AcheteurPublic` ADD `id_service` INT( 11 ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `signature_acte_engagement` ENUM( '0', '1' ) NOT NULL DEFAULT '0', ADD `poursuivre_affichage` INT( 3 ) NOT NULL DEFAULT '0';
ALTER TABLE `AcheteurPublic` ADD `facture_pays` VARCHAR( 100 ) NOT NULL AFTER `facture_ville` ;

-- 23-04-2008 : AZA : ajouter le service_id dans la table TyteurPlan
ALTER TABLE `TireurPlan` ADD `id_service` INT( 11 ) DEFAULT NULL ;


-- 24/04/2008 :MMI modifier le champ lieu_exeution de la table consultation 

ALTER TABLE `consultation` CHANGE `lieu_execution` `lieu_execution` TEXT NOT NULL;

-- 29/04/2008 :YSB ajouter des champs dans table service (Entité achat)

ALTER TABLE `Service` 
ADD `libelle` VARCHAR( 100 ) NOT NULL ,
ADD `sigle` VARCHAR( 100 ) NOT NULL ,
ADD `adresse` VARCHAR( 100 ) NOT NULL ,
ADD `adresse_suite` VARCHAR( 100 ) NOT NULL ,
ADD `cp` VARCHAR( 5 ) NOT NULL ,
ADD `ville` VARCHAR( 100 ) NOT NULL ,
ADD `telephone` VARCHAR( 100 ) NOT NULL ,
ADD `fax` VARCHAR( 100 ) NOT NULL ,
ADD `mail` VARCHAR( 100 ) NOT NULL ;

-- 05-05-2008 : ael : ajouter des champs dans la table AcheteurPublic

ALTER TABLE `AcheteurPublic` ADD `url_acheteur` VARCHAR( 100 ) NULL AFTER `url` ;
ALTER TABLE `AcheteurPublic` ADD `facturation_service`  VARCHAR( 100 ) NULL AFTER `facture_denomination` ;
ALTER TABLE `AcheteurPublic` ADD `livraison_service` VARCHAR( 100 ) NULL ;
ALTER TABLE `AcheteurPublic` ADD `livraison_adresse` VARCHAR( 100 ) NULL ;
ALTER TABLE `AcheteurPublic` ADD `livraison_code_postal` VARCHAR( 5 ) NULL ;
ALTER TABLE `AcheteurPublic` ADD `livraison_ville` VARCHAR( 100 ) NULL ;
ALTER TABLE `AcheteurPublic` ADD `livraison_pays` VARCHAR( 100 ) NULL ;

-- 30/04/2008 : KBE : ajout des champs pour le connecteur EPM
ALTER TABLE `Annonce` ADD `date_maj` DATETIME;
ALTER TABLE `AnnonceMoniteur` ADD `date_maj` DATETIME;

-- 05/04/2008 : AZA ajout de champs dans la table ProcedureEquivalence
ALTER TABLE `ProcedureEquivalence` ADD `delai_date_limite_remise_pli` VARCHAR( 2 ) NOT NULL ,ADD `signature_propre` VARCHAR( 2 ) NOT NULL ;
ALTER TABLE `ProcedureEquivalence` ADD `procedure_restreinte` VARCHAR( 2 ) NOT NULL ;
ALTER TABLE `ProcedureEquivalence` ADD `ouverture_simultanee` VARCHAR( 2 ) NOT NULL ;

-- 09/05/2008 : YSB ajout de champs dans la table RPA

CREATE TABLE `RPA` (
	  `id` int(11) NOT NULL auto_increment,
	  `id_service` int(11) NOT NULL default '0',
	  `nom` varchar(30) NOT NULL default '',
	  `prenom` varchar(30) NOT NULL default '',
	  `adresse1` varchar(80) NOT NULL default '',
	  `adresse2` varchar(80) NOT NULL default '',
	  `codepostal` varchar(20) NOT NULL default '',
	  `ville` varchar(50) NOT NULL default '',
	  PRIMARY KEY  (`id`)
	) TYPE=InnoDB; 
	
-- 09/05/2008 : YSB ajout de champs dans la table RPA
ALTER TABLE `RPA` ADD `Fonction` VARCHAR( 200 ) NOT NULL ;
ALTER TABLE `RPA` ADD `Organisme` VARCHAR( 200 ) NOT NULL ;

-- 21-05-2008 ael : ajout du champs status de l'enveloppe et de la table StatusEnveloppe
ALTER TABLE `Enveloppe` ADD `statut_enveloppe` INT( 2 ) NOT NULL DEFAULT '1' ;
ALTER TABLE `Enveloppe_papier` ADD `statut_enveloppe` INT( 2 ) NOT NULL DEFAULT '1';

CREATE TABLE IF NOT EXISTS  `Admissibilite_Enveloppe_Lot` (
`id_enveloppe` INT( 22 ) NOT NULL ,
`sous_pli` INT( 3 ) NOT NULL ,
`admissibilite` INT( 1 ) NOT NULL ,
`commentaire` TEXT NULL ,
PRIMARY KEY ( `id_enveloppe` , `sous_pli` )
) TYPE = MYISAM ;

CREATE TABLE IF NOT EXISTS  `Admissibilite_Enveloppe_papier_Lot` (
`id_enveloppe_papier` INT( 22 ) NOT NULL ,
`sous_pli` INT( 3 ) NOT NULL ,
`admissibilite` INT( 1 ) NOT NULL ,
`commentaire` TEXT NULL ,
PRIMARY KEY ( `id_enveloppe_papier` , `sous_pli` )
) TYPE = MYISAM ;

CREATE TABLE IF NOT EXISTS `StatutEnveloppe` (
`id_statut` INT( 2 ) NOT NULL ,
`description` VARCHAR( 100 ) NOT NULL ,
PRIMARY KEY ( `id_statut` )
) TYPE = InnoDB ;

INSERT INTO `StatutEnveloppe` (`id_statut`, `description`) VALUES(1, 'Fermée');
INSERT INTO `StatutEnveloppe` (`id_statut`, `description`) VALUES(2, 'Ouverte en ligne');
INSERT INTO `StatutEnveloppe` (`id_statut`, `description`) VALUES(3, 'Ouverte hors ligne');
INSERT INTO `StatutEnveloppe` (`id_statut`, `description`) VALUES(4, 'Refusée');
INSERT INTO `StatutEnveloppe` (`id_statut`, `description`) VALUES(5, 'Ouverte');

ALTER TABLE `QuestionDCE` ADD `pays` VARCHAR( 150 ) NULL ;

--date 29-05-2008 : AZA ajout de champs dans la table `Telechargement`

ALTER TABLE `Telechargement` ADD `id_inscrit` INT( 11 ) NULL ;
ALTER TABLE `Telechargement` ADD `id_entreprise` INT( 11 ) NULL ;
ALTER TABLE `Telechargement` ADD `sirenEtranger` VARCHAR( 20 ) NULL ;
ALTER TABLE `Telechargement` ADD `adresse2` VARCHAR( 80 ) NULL ;
ALTER TABLE `Telechargement` ADD `prenom` VARCHAR( 11 ) NULL ;
ALTER TABLE `Telechargement` CHANGE `pays` `pays` VARCHAR( 50 ) NULL;

--date 04-06-2008 : YSB ajout des tables echange (module messagerie)

CREATE TABLE IF NOT EXISTS `Echange` (
  `id` int(11) NOT NULL auto_increment,
  `objet` varchar(255) default NULL,
  `corps` text NOT NULL,
  `expediteur` varchar(100) NOT NULL default '',
  `id_createur` int(11) NULL default '0',
  `ref_consultation` int(11) NOT NULL default '0',
  `option_envoi` int(11) NOT NULL default '0',
  `date_message` varchar(25) NOT NULL default '',
  `format` int(11) NOT NULL default '0',
  `id_action_declencheur` int(11) NOT NULL default '0',
  `status` int(2) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EchangeActionDeclencheur` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EchangeDestinataire` (
  `id` int(11) NOT NULL auto_increment,
  `id_echange` int(11) NOT NULL default '0',
  `mail_destinataire` varchar(255) NOT NULL default '',
  `ar` enum('0','1') NOT NULL default '0',
  `date_ar` varchar(25) NOT NULL default '',
  `uid` varchar(32) NOT NULL default '',
  `type_ar` int(11) NOT NULL default '0',
  `id_inscrit` INT( 11 ) NOT NULL,
  `id_entreprise` INT( 11 ) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_echange` (`id_echange`)
) TYPE=InnoDB;
ALTER TABLE `EchangeDestinataire`
  ADD CONSTRAINT `EchangeDestinataire_ibfk_1` FOREIGN KEY (`id_echange`) REFERENCES `Echange` (`id`);
  
CREATE TABLE IF NOT EXISTS `EchangeFormat` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EchangePieceJointe` (
  `id` int(11) NOT NULL auto_increment,
  `id_message` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `piece` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(25) NOT NULL default '',
  `taille` varchar(25) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id_message` (`id_message`)
) TYPE=InnoDB;
ALTER TABLE `EchangePieceJointe`
  ADD CONSTRAINT `EchangePieceJointe_ibfk_1` FOREIGN KEY (`id_message`) REFERENCES `Echange` (`id`) ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS `EchangeTypeAR` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EchangeTypeMessage` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  `corps` text NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

INSERT INTO `EchangeFormat` (`id`, `libelle`) VALUES (1, 'Echange plate-forme');
INSERT INTO `EchangeFormat` (`id`, `libelle`) VALUES (2, 'Courriel hors plate-forme');
INSERT INTO `EchangeFormat` (`id`, `libelle`) VALUES (3, 'Courrier recommandé');
INSERT INTO `EchangeFormat` (`id`, `libelle`) VALUES (4, 'Courrier simple');
INSERT INTO `EchangeFormat` (`id`, `libelle`) VALUES (5, 'Télécopie');
INSERT INTO `EchangeFormat` (`id`, `libelle`) VALUES (6, 'Autre');

INSERT INTO `EchangeTypeAR` (`id`, `libelle`) VALUES (1, 'Sans objet');
INSERT INTO `EchangeTypeAR` (`id`, `libelle`) VALUES (2, 'Non retiré');
INSERT INTO `EchangeTypeAR` (`id`, `libelle`) VALUES (3, 'Date et heure');

INSERT INTO `EchangeTypeMessage` (`id`, `libelle`, `corps`) VALUES (1, 'Courrier de notification', '');
INSERT INTO `EchangeTypeMessage` (`id`, `libelle`, `corps`) VALUES (2, 'Courrier de rejet', '');
INSERT INTO `EchangeTypeMessage` (`id`, `libelle`, `corps`) VALUES (3, 'Avertissement - Modification de consultation', '');
INSERT INTO `EchangeTypeMessage` (`id`, `libelle`, `corps`) VALUES (4, 'Courrier libre', '
TEXTE COURRIER LIBRE
');


--date 05-06-2008 : TDE Encheres inversées
CREATE TABLE IF NOT EXISTS `EnchereEntreprisePmi` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `nom` varchar(255) NOT NULL default '',
  `numeroAnonyme` int(11) unsigned default NULL,
  `email` varchar(255) NOT NULL default '',
  `mdp` varchar(255) NOT NULL default '',
  `noteTechnique` double default NULL,
  `idEntreprise` int(10) unsigned default NULL,
  `datePing` datetime NOT NULL default '0000-00-00 00:00:00',
  `nomAgentConnecte` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `Entreprise_FKIndex1` (`idEnchere`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EnchereOffre` (
  `id` int(11) NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `idEnchereEntreprise` int(10) unsigned NOT NULL default '0',
  `date` datetime default '0000-00-00 00:00:00',
  `valeurTIC` double default NULL,
  `valeurTC` double default NULL,
  `valeurNETC` double default NULL,
  `valeurNGC` double default NULL,
  `rang` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `EnchereOffre_FKIndex1` (`idEnchereEntreprise`),
  KEY `EnchereOffre_FKIndex2` (`idEnchere`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EnchereOffreReference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchereOffre` int(11) NOT NULL default '0',
  `idEnchereReference` int(10) unsigned NOT NULL default '0',
  `valeur` double NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `EnchereOffreReference_FKIndex1` (`idEnchereOffre`),
  KEY `EnchereOffreReference_FKIndex2` (`idEnchereReference`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EncherePmi` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `refConsultation` int(10) unsigned default NULL,
  `referenceUtilisateur` varchar(45) default NULL,
  `auteur` varchar(255) default NULL,
  `idLot` int(10) unsigned default NULL,
  `objet` longtext,
  `dateDebut` datetime default '0000-00-00 00:00:00',
  `dateFin` datetime default '0000-00-00 00:00:00',
  `dateSuspension` datetime default '0000-00-00 00:00:00',
  `delaiProlongation` int(11) default NULL,
  `commentaire` longtext,
  `meilleureEnchereObligatoire` enum('0','1') NOT NULL default '0',
  `typeBaremeNETC` enum('1','2','3','4') NOT NULL default '1',
  `typeBaremeEnchereGlobale` enum('1','2') NOT NULL default '1',
  `meilleurNoteHaute` enum('0','1') NOT NULL default '0',
  `nbrCandidatsVisible` enum('0','1') NOT NULL default '1',
  `listeCandidatsVisible` enum('0','1') NOT NULL default '1',
  `rangVisible` enum('0','1') NOT NULL default '1',
  `meilleureOffreVisible` enum('0','1') NOT NULL default '1',
  `montantReserve` double default NULL,
  `noteMaxBaremeRelatif` double default NULL,
  `coeffA` double default NULL,
  `coeffB` double default NULL,
  `coeffC` double default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `referenceUtilisateur` (`referenceUtilisateur`),
  KEY `Enchere_V2_FKIndex1` (`refConsultation`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EnchereReference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `libelle` varchar(255) NOT NULL default '',
  `numero` int(11) NOT NULL default '0',
  `quantite` double default NULL,
  `isMontant` enum('0','1') NOT NULL default '1',
  `unite` varchar(20) default NULL,
  `pasMin` double default '0',
  `pasMax` double default NULL,
  `valeurReference` double default NULL,
  `valeurDepartCommune` enum('0','1') NOT NULL default '1',
  `valeurDepart` double default NULL,
  `typeBaremeReference` enum('1','2','3') default NULL,
  `ponderationNoteReference` double default '1',
  `noteMaxBaremeRelatif` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `EnchereReference_FKIndex1` (`idEnchere`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EnchereTrancheBaremeReference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idReference` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `TrancheBaremeReference_FKIndex1` (`idReference`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EnchereTranchesBaremeNETC` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `EnchereTranchesBaremeNETC_FKIndex1` (`idEnchere`)
) TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS `EnchereValeursInitiales` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchereEntreprise` int(10) unsigned NOT NULL default '0',
  `idEnchereReference` int(10) unsigned NOT NULL default '0',
  `valeur` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `EnchereValeursInitiales_FKIndex1` (`idEnchereEntreprise`),
  KEY `EnchereValeursInitiales_FKIndex2` (`idEnchereReference`)
) TYPE=InnoDB;

ALTER TABLE `EnchereEntreprisePmi` ADD CONSTRAINT `EnchereEntreprisePmi_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `EnchereOffre` ADD CONSTRAINT `EnchereOffre_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `EnchereOffre` ADD CONSTRAINT `EnchereOffre_ibfk_2` FOREIGN KEY (`idEnchereEntreprise`) REFERENCES `EnchereEntreprisePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `EnchereOffreReference` ADD CONSTRAINT `EnchereOffreReference_ibfk_2` FOREIGN KEY (`idEnchereReference`) REFERENCES `EnchereReference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `EnchereOffreReference` ADD CONSTRAINT `EnchereOffreReference_ibfk_1` FOREIGN KEY (`idEnchereOffre`) REFERENCES `EnchereOffre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `EnchereReference` ADD CONSTRAINT `EnchereReference_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `EnchereTrancheBaremeReference` ADD CONSTRAINT `EnchereTrancheBaremeReference_ibfk_1` FOREIGN KEY (`idReference`) REFERENCES `EnchereReference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `EnchereTranchesBaremeNETC` ADD CONSTRAINT `EnchereTranchesBaremeNETC_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `EnchereValeursInitiales` ADD CONSTRAINT `EnchereValeursInitiales_ibfk_2` FOREIGN KEY (`idEnchereReference`) REFERENCES `EnchereReference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `EnchereValeursInitiales` ADD CONSTRAINT `EnchereValeursInitiales_ibfk_1` FOREIGN KEY (`idEnchereEntreprise`) REFERENCES `EnchereEntreprisePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `EncherePmi` ADD `idEntiteeAssociee` int(11) default NULL;
ALTER TABLE `EncherePmi` ADD INDEX (`idEntiteeAssociee`);

ALTER TABLE `EncherePmi` 
	ADD CONSTRAINT `EncherePmi_ibfk_1` FOREIGN KEY (`idEntiteeAssociee`) REFERENCES `Service` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--date 10-06-2008 : YSB ajout du champ service_id dans la table echange (module messagerie)
ALTER TABLE `Echange` ADD `service_id` INT( 11 ) NOT NULL DEFAULT '0'; 
 -- 11/06/2008 :MMI

-- 
-- Structure de la table `ReferentielDestinationFormXml`
-- 

CREATE TABLE `ReferentielDestinationFormXml` (
  `id` int(11) NOT NULL auto_increment,
  `destinataire` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB ;

-- 
-- Contenu de la table `ReferentielDestinationFormXml`
-- 

INSERT INTO `ReferentielDestinationFormXml` (`id`, `destinataire`) VALUES (1, 'BOAMP seul');
INSERT INTO `ReferentielDestinationFormXml` (`id`, `destinataire`) VALUES (2, 'BOAMP et JOUE');
INSERT INTO `ReferentielDestinationFormXml` (`id`, `destinataire`) VALUES (3, 'Groupe Moniteur');
INSERT INTO `ReferentielDestinationFormXml` (`id`, `destinataire`) VALUES (4, 'Portail Entreprise');


-- 
-- Structure de la table `ReferentielFormXml`
-- 

CREATE TABLE `ReferentielFormXml` (
  `id` int(11) NOT NULL auto_increment,
  `id_type_xml` int(11) NOT NULL default '0',
  `consultation_ref` varchar(100) NOT NULL default '',
  `xml` text NOT NULL,
  `date_creation` date default NULL,
  `statut` char(1) default NULL,
  PRIMARY KEY  (`id`),
  KEY `id_type_xml` (`id_type_xml`),
  KEY `consultation_ref` (`consultation_ref`)
) TYPE=InnoDB;



CREATE TABLE `ReferentielTypeXml` (
  `id` int(11) NOT NULL auto_increment,
  `libelle_type` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB  ;

-- 
-- Contenu de la table `ReferentielTypeXml`
-- 

INSERT INTO `ReferentielTypeXml` (`id`, `libelle_type`) VALUES  (1, 'Avis de marché national');
INSERT INTO `ReferentielTypeXml` (`id`, `libelle_type`) VALUES  (2, 'Avis de MAPA');
INSERT INTO `ReferentielTypeXml` (`id`, `libelle_type`) VALUES  (3, 'Avis de marché européen');
INSERT INTO `ReferentielTypeXml` (`id`, `libelle_type`) VALUES  (4, 'Avis d''attribution européen');
INSERT INTO `ReferentielTypeXml` (`id`, `libelle_type`) VALUES  (5, 'Avis d''attribution MAPA');
INSERT INTO `ReferentielTypeXml` (`id`, `libelle_type`) VALUES  (6, 'Avis rectificatif national');


-- 
-- Contraintes pour la table `ReferentielFormXml`
-- 
ALTER TABLE `ReferentielFormXml` 
  ADD CONSTRAINT `ReferentielFormXml_ibfk_1` FOREIGN KEY (`id_type_xml`) REFERENCES `ReferentielTypeXml` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Table Annonce
-- 

ALTER TABLE `Annonce` ADD `id_destination_form_xml` int(11) NOT NULL default '0';
ALTER TABLE `Annonce` ADD `id_form_xml` int(11) NOT NULL default '0';
ALTER TABLE `Annonce` ADD `id_type_xml` int(11) NOT NULL default '0';
ALTER TABLE `Annonce` ADD `statut_destinataire` char(1) NOT NULL default '';
ALTER TABLE `Annonce` ADD `accuse_reception` char(1) NOT NULL default '';
ALTER TABLE `Annonce` ADD `lien` varchar(50) NOT NULL default '';

ALTER TABLE `Annonce` ADD INDEX ( `id_destination_form_xml`) ;
ALTER TABLE `Annonce` ADD INDEX ( `id_form_xml` ) ;
ALTER TABLE `Annonce` ADD INDEX ( `id_type_xml` ) ;

-- 
-- Contraintes pour la table `Annonce`
-- 
ALTER TABLE `Annonce` ADD CONSTRAINT `Annonce_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Annonce` ADD CONSTRAINT `Annonce_ibfk_2` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`);
ALTER TABLE `Annonce` ADD CONSTRAINT `Annonce_ibfk_3` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`);
ALTER TABLE `Annonce` ADD CONSTRAINT `Annonce_ibfk_4` FOREIGN KEY (`id_type_xml`) REFERENCES `ReferentielTypeXml` (`id`);

--date 11/06/2008 : TDE champs devenu inutile dans les enchères
ALTER TABLE `EnchereReference` DROP `numero`;

--date 11/06/2008 : AZA mise à jour de téléchargement
ALTER TABLE `Telechargement` ADD `id_inscrit` INT( 11 ) NULL ;
ALTER TABLE `Telechargement` ADD `id_entreprise` INT( 11 ) NULL ;
ALTER TABLE `Telechargement` ADD `sirenEtranger` VARCHAR( 20 ) NULL ;
ALTER TABLE `Telechargement` ADD `adresse2` VARCHAR( 80 ) NULL ;
ALTER TABLE `Telechargement` ADD `prenom` VARCHAR( 11 ) NULL ;
ALTER TABLE `Telechargement` CHANGE `pays` `pays` VARCHAR( 50 ) NULL;
ALTER TABLE `Telechargement` ADD `noms_fichiers_dce` text NULL ;

ALTER TABLE `Service` ADD `pays` VARCHAR( 150 ) NULL ;
ALTER TABLE `RPA` ADD `pays` VARCHAR( 150 ) NULL ;

ALTER TABLE `TypeProcedure` ADD `id_montant_mapa` INT( 2 ) NOT NULL DEFAULT '0';

CREATE TABLE `Referentiel` (
`id_referentiel` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`libelle_referentiel` VARCHAR( 200 ) NOT NULL  
) TYPE = INNODB;

CREATE TABLE `ValeurReferentiel` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_referentiel` INT( 10 ) NOT NULL ,
`libelle_valeur_referentiel` VARCHAR( 200 ) NOT NULL
) TYPE = innodb;

INSERT INTO `Referentiel` ( `id_referentiel` , `libelle_referentiel` ) VALUES (1 , 'Type organismes');




--12/06/2008 : mmi

--ALTER TABLE `Annonce` ADD `envoi_boamp` INT NULL DEFAULT '0' AFTER `consultation_ref` ;
--ALTER TABLE `Annonce` ADD `ann_xml` LONGBLOB NOT NULL AFTER `date_envoi` ;
--ALTER TABLE `Annonce` ADD `ann_pdf` LONGBLOB NOT NULL AFTER `ann_xml` ;
--ALTER TABLE `Annonce` ADD `ann_form_values` LONGBLOB NOT NULL AFTER `ann_pdf` ;


ALTER TABLE `EncherePmi` ADD `mail` longtext default NULL;

--date 13/06/2008 : ajout du champ email de l'expediteur
ALTER TABLE `Echange` ADD `email_expediteur` VARCHAR( 100 ) NOT NULL ;


--17/06/208 : MMI

ALTER TABLE `ReferentielTypeXml` ADD `id_destinataire` VARCHAR( 50 ) NOT NULL AFTER `id` ;

ALTER TABLE `Telechargement` ADD `Observation` TEXT NULL ;
ALTER TABLE `Retrait_Papier` ADD `Observation` TEXT NULL ;
ALTER TABLE `Retrait_Papier` ADD `prenom` varchar(100) NULL;
ALTER TABLE `Retrait_Papier` ADD `adresse2` varchar(100) NULL;
ALTER TABLE `QuestionDCE` ADD `Observation` TEXT NULL ;

ALTER TABLE `Telechargement` ADD `acronyme_pays` VARCHAR( 10 ) NULL ;
ALTER TABLE `Retrait_Papier` ADD `identifiant_national` VARCHAR( 20 ) NULL ;
ALTER TABLE `QuestionDCE` ADD `siret` VARCHAR( 14 ) NULL ;
ALTER TABLE `QuestionDCE` ADD `identifiant_national` VARCHAR( 20 ) NULL ;
ALTER TABLE `QuestionDCE` ADD `acronyme_pays` VARCHAR( 10 ) NULL ;

ALTER TABLE `Retrait_Papier` ADD `acronyme_pays` varchar(10) NULL;
ALTER TABLE `QuestionDCE` ADD `adresse2` VARCHAR( 80 ) NULL ;
ALTER TABLE `Offre` ADD `Observation` TEXT NULL ;
ALTER TABLE `Retrait_Papier` CHANGE `siret` `siret` VARCHAR( 14 ) NULL ;
ALTER TABLE `QuestionDCE` ADD `id_fichier` INT( 11 ) NULL ;
ALTER TABLE `QuestionDCE` ADD `nom_fichier` VARCHAR( 100 ) NULL ;
ALTER TABLE `QuestionDCE` ADD `prenom` VARCHAR( 80 ) NULL ;

ALTER TABLE `Offre_papier` ADD `nom` VARCHAR( 80 ) NULL ;
ALTER TABLE `Offre_papier` ADD `prenom` VARCHAR( 80 ) NULL ;
ALTER TABLE `Offre_papier` ADD `adresse` VARCHAR( 100 ) NULL ;
ALTER TABLE `Offre_papier` ADD `adresse2` VARCHAR( 100 ) NULL ;
ALTER TABLE `Offre_papier` ADD `telephone` VARCHAR( 20 ) NULL ;
ALTER TABLE `Offre_papier` ADD `fax` VARCHAR( 30 ) NULL ;
ALTER TABLE `Offre_papier` ADD `code_postal` VARCHAR( 5 ) NULL ;
ALTER TABLE `Offre_papier` ADD `ville` VARCHAR( 50 ) NULL ;
ALTER TABLE `Offre_papier` ADD `pays` VARCHAR( 50 ) NULL ;
ALTER TABLE `Offre_papier` ADD `acronyme_pays` VARCHAR( 10 ) NULL ;
ALTER TABLE `Offre_papier` ADD `siret` VARCHAR( 14 ) NULL ;
ALTER TABLE `Offre_papier` ADD `identifiant_national` VARCHAR( 20 ) NULL ;
ALTER TABLE `Offre_papier` ADD `email` VARCHAR( 100 ) NULL ;
ALTER TABLE `Offre_papier` ADD `observation` TEXT NULL ;

ALTER TABLE `TypeProcedure` ADD `code_recensement` VARCHAR( 3 ) NULL ;

REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (1, 'Appel d''offres ouvert', 'AOO', 1, 1, 1, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '01');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (2, 'Appel d''offres restreint - Candidature', 'AOR-C', 2, 2, 2, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '02');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (3, 'Appel d''offres restreint - Offre', 'AOR-O', 2, 2, 2, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '02');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (4, 'Procédure adaptée < 90 k EUR HT', 'PA-INF', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', 1, '09');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (5, 'Procédure adaptée &#8805; 90 k EUR HT', 'PA-SUP', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', 2, '09');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (6, 'Procédure adaptée spécifique article 30 < 90 k EUR HT', 'PA-A30-INF', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', 1, '09');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (7, 'Procédure adaptée spécifique article 30 &#8805; 90 k EUR HT', 'PA-A30-SUP', 23, 3, 0, 0, 2, 0, '1', '1', '', '', '', '', '', '', 2, '09');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (8, 'Concours ouvert', 'CO', 7, 4, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0 , '07');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (9, 'Concours restreint - Candidature', 'CR-C', 8, 5, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '07');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (10, 'Concours restreint - Offre', 'CR-O', 8, 5, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0,'07');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (11, 'Dialogue compétitif - Candidature', 'DC-C', 22, 7, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0,'06');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (12, 'Dialogue compétitif - Offre', 'DC-O', 22, 7, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0,'06');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (13, 'Contrat de Partenariat de l''Etat - 1 Candidature', 'CPE-C', 24, 10, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (14, 'Contrat de Partenariat de l''Etat - 2 Dialogue', 'CPE-D', 24, 10, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (16, 'Contrat de Partenariat de l''Etat - 3 Appel d''offres', 'CPE-AO', 24, 10, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (17, 'Procédure Autre', 'AUT', 24, 15, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (18, 'Marché négocié', 'MN', 18, 8, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (22, 'Procédure article 3', 'AR3', 22, 11, 3, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (23, 'Marché négocié - Décret Spécifique Défense - Candidature', 'DSD-C', 24, 9, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '05');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (24, 'Marché négocié - Décret Spécifique Défense - Offre', 'DSD-O', 24, 9, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '05');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (30, 'Accord-Cadre - 1 Sélection des Attributaires', 'AC-SA', 1, 30, 1, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (31, 'Accord-Cadre - 2 Marché Subséquent', 'AC-MS', 2, 31, 2, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (32, 'Système d''Acquisition Dynamique - Admission', 'SAD-A', 8, 32, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0,'08');
REPLACE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `id_montant_mapa`, `code_recensement`) VALUES (33, 'Système d''Acquisition Dynamique - Marché Spécifique', 'SAD-MS', 8, 33, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0,'08');

UPDATE `TypeProcedure` SET `id_montant_mapa` = '2' WHERE `id_type_procedure` =5 LIMIT 1 ;
UPDATE `TypeProcedure` SET `id_montant_mapa` = '2' WHERE `id_type_procedure` =7 LIMIT 1 ;
DELETE FROM `TypeProcedure` WHERE `id_type_procedure` = 15 LIMIT 1;

REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (1, '1', '0', '1', '0', '1', '0', '1', '1', '-0', '-1', '-1', '1', '0', '0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (2, '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '-0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (3, '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (4, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (5, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (6, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (7, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (8, '+1', '+0', '+1', '+0', '+1', '+0', '1', '1', '1', '-1', '-1', '1', '+0', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (9, '+1', '+0', '+1', '+0', '+1', '+0', '1', '0', '0', '-1', '-1', '1', '+1', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '-0', '0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (10, '+1', '+0', '+1', '+0', '+1', '+0', '-0', '1', '1', '-1', '-1', '0', '+0', '+1', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (11, '1', '0', '+1', '+0', '+1', '+0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '+0', '+1', '+0', '+0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (12, '+1', '+0', '+1', '+0', '+1', '+0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (13, '1', '0', '+1', '+0', '+1', '+0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '+0', '+1', '+0', '+0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (14, '+1', '+0', '+1', '0', '+1', '+0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (16, '1', '0', '+1', '+0', '+1', '+0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (17, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '0', '0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (18, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+0', '+0', '+1', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '-1', '-1', '-1', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (22, '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '0', '+0', '+1', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '-1', '-1', '-1', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-1', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (23, '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+0', '-0', '-1', '-1', '0', '1', '0', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (24, '+1', '+0', '+1', '+0', '+1', '+0', '0', '1', '-0', '-1', '-1', '0', '+0', '+1', '+0', '+0', '+0', '+1', '-0', '-0', '1', '-0', '-1', '-1', '-1', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (30, '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '-0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (31, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (32, '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '+0', '+1', '+0', '+0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0');
REPLACE INTO `ProcedureEquivalence` (`id_type_procedure`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`) VALUES (33, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '+0', '+1', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1');

--date 20/06/2008 : TDE ajout des tables relatives à la liste des marchés article 133
CREATE TABLE IF NOT EXISTS `Marche` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `idMarcheTrancheBudgetaire` int(11) unsigned NOT NULL default '0',
  `numeroMarcheAnnee` int(11) unsigned NOT NULL default '0',
  `idService` int(11) NOT NULL default '0',
  `nomAttributaire` varchar(255) NOT NULL default '',
  `dateNotification` date NOT NULL default '0000-00-00',
  `codePostal` varchar(20) NOT NULL default '',
  `natureMarche` varchar(30) NOT NULL default '1',
  `objetMarche` longtext NOT NULL,
  `montantMarche` double default NULL,
  `valide` enum('0','1') NOT NULL default '0',
  `isManuel` enum('0','1') NOT NULL default '0',
  `pmePmi` enum('0','1','2') NOT NULL default '2',
  PRIMARY KEY  (`id`),
  KEY `ListeMarches_FKIndex2` (`idMarcheTrancheBudgetaire`),
  KEY `ListeMarches_FKIndex3` (`idService`),
  KEY `numeroMarcheAnnee` (`numeroMarcheAnnee`),
  KEY `natureMarche` (`natureMarche`)
) TYPE=InnoDB;


CREATE TABLE IF NOT EXISTS `MarcheAnnee` (
  `numero` int(11) unsigned NOT NULL default '0',
  `afficher` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`numero`)
) TYPE=InnoDB;


CREATE TABLE IF NOT EXISTS `MarchePublie` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `numeroMarcheAnnee` int(11) unsigned NOT NULL default '0',
  `idService` int(11) NOT NULL default '0',
  `isPubliee` enum('0','1') NOT NULL default '0',
  `isImportee` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `ListeMarchePubliee_FKIndex1` (`idService`),
  KEY `ListeMarchePubliee_FKIndex2` (`numeroMarcheAnnee`)
) TYPE=InnoDB;


CREATE TABLE IF NOT EXISTS `MarcheTrancheBudgetaire` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;


ALTER TABLE `Marche` ADD CONSTRAINT `Marche_ibfk_4` FOREIGN KEY (`natureMarche`) REFERENCES `CategorieConsultation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,  ADD CONSTRAINT `Marche_ibfk_1` FOREIGN KEY (`idMarcheTrancheBudgetaire`) REFERENCES `MarcheTrancheBudgetaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,  ADD CONSTRAINT `Marche_ibfk_2` FOREIGN KEY (`numeroMarcheAnnee`) REFERENCES `MarcheAnnee` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE,  ADD CONSTRAINT `Marche_ibfk_3` FOREIGN KEY (`idService`) REFERENCES `Service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `MarchePublie` ADD CONSTRAINT `MarchePublie_ibfk_1` FOREIGN KEY (`numeroMarcheAnnee`) REFERENCES `MarcheAnnee` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE,  ADD CONSTRAINT `MarchePublie_ibfk_2` FOREIGN KEY (`idService`) REFERENCES `Service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `QuestionDCE` ADD `siret_etranger` VARCHAR( 20 ) NULL ;

INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (2006, '1');
INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (2007, '1');
INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (2008, '1');
INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (2009, '0');
INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (20010, '0');
INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (20011, '0');
INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (20012, '0');
INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (20013, '0');
INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (20014, '0');
INSERT INTO `MarcheAnnee` (`numero`, `afficher`) VALUES (20015, '0');

INSERT INTO `MarcheTrancheBudgetaire` (`id`, `libelle`) VALUES (1, '4 000 HT à 19 999,99 HT');
INSERT INTO `MarcheTrancheBudgetaire` (`id`, `libelle`) VALUES (2, '20 000 HT à 49 999,99 HT');
INSERT INTO `MarcheTrancheBudgetaire` (`id`, `libelle`) VALUES (3, '50 000 HT à 89 999,99 HT');
INSERT INTO `MarcheTrancheBudgetaire` (`id`, `libelle`) VALUES (4, '90 000 HT à 134 999,99 HT');
INSERT INTO `MarcheTrancheBudgetaire` (`id`, `libelle`) VALUES (5, '135 000 HT à 209 999,99 HT');
INSERT INTO `MarcheTrancheBudgetaire` (`id`, `libelle`) VALUES (6, '210 000 HT à 999 999,99 HT');
INSERT INTO `MarcheTrancheBudgetaire` (`id`, `libelle`) VALUES (7, '1 000 000 HT à 2 999 999,99 HT');
INSERT INTO `MarcheTrancheBudgetaire` (`id`, `libelle`) VALUES (8, '3 000 000 HT à 5 269 999,99 HT');
INSERT INTO `MarcheTrancheBudgetaire` (`id`, `libelle`) VALUES (9, '5 270 000 HT et plus.');

CREATE TABLE IF NOT EXISTS `DocumentExterne` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `refConsultation` int(11) default NULL,
  `idBlob` int(11) default NULL,
  `idEntreprise` int(11) default NULL,
  `type` enum('1','2','3','4','5','6','7') default NULL,
  `nom` varchar(255) NOT NULL default '',
  `description` longtext NOT NULL,
  `date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`),
  KEY `idBlob` (`idBlob`),
  KEY `refConsultation` (`refConsultation`)
) TYPE=InnoDB AUTO_INCREMENT=9 ;


ALTER TABLE `DocumentExterne`  ADD CONSTRAINT `DocumentExterne_ibfk_3` FOREIGN KEY (`refConsultation`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `DocumentExterne`  ADD CONSTRAINT `DocumentExterne_ibfk_4` FOREIGN KEY (`idBlob`) REFERENCES `blob` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `consultation` ADD `archiveMetaDescription` longtext;
ALTER TABLE `consultation` ADD `archiveMetaMotsClef` longtext;
 
 
ALTER TABLE `consultation` ADD `archiveIdBlobZip` int(11) default NULL;
  
ALTER TABLE `Enveloppe_papier` CHANGE `champs_optionnels` `champs_optionnels` LONGTEXT NULL ;

ALTER TABLE `Enveloppe_papier` ADD `enveloppe_postule` ENUM( '0', '1' ) NOT NULL DEFAULT '1'; 

--date 01/07/2008 : ajout du champ detail consultation
ALTER TABLE `consultation` ADD `detail_consultation` VARCHAR( 100 ) NOT NULL ; 

ALTER TABLE `QuestionDCE` ADD `id_inscrit` INT( 11 ) NOT NULL ;
ALTER TABLE `QuestionDCE` ADD `id_entreprise` INT( 11 ) NOT NULL ;

-- 03/07/2008 MMI: 

ALTER TABLE `TypeProcedure` ADD `depouillable_phase_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `consultation` ADD `depouillable_phase_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `consultation` DROP `depouiable_phase_consultation` ;

ALTER TABLE `Admissibilite_Enveloppe_Lot` CHANGE `id_enveloppe` `id_Offre` INT( 22 ) NOT NULL DEFAULT '0';
ALTER TABLE `Admissibilite_Enveloppe_papier_Lot` CHANGE `id_enveloppe_papier` `id_offre_papier` INT( 22 ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `decision_partielle` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `consultation` ADD `date_fin_affichage` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `CategorieLot` ADD `decision` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

CREATE TABLE `TypeDecision` (
`id_type_decision` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`libelle_type_decision` VARCHAR( 100 ) NOT NULL
) TYPE =  innoDB ;

	   		  
UPDATE `TypeProcedure` SET `depouillable_phase_consultation` = '1' WHERE `TypeProcedure`.`id_type_procedure` =32 LIMIT 1 ;
UPDATE `TypeProcedure` SET `depouillable_phase_consultation` = '1' WHERE `TypeProcedure`.`id_type_procedure` =23 LIMIT 1 ;

-- 08/07/2008 YSB :
UPDATE `EchangeTypeMessage` SET `corps` = '
TEXTE COURRIER DE NOTIFICATION
' WHERE `EchangeTypeMessage`.`id` = 1 ;
UPDATE `EchangeTypeMessage` SET `corps` = '
TEXTE COURRIER DE REJET
' WHERE `EchangeTypeMessage`.`id` = 2 ;
UPDATE `EchangeTypeMessage` SET `corps` = '
Bonjour,

la consultation ci-dessus a été modifiée.
' WHERE `EchangeTypeMessage`.`id` = 3 ;
UPDATE `EchangeTypeMessage` SET `corps` = '
TEXTE COURRIER LIBRE
' WHERE `EchangeTypeMessage`.`id` = 4 ;

INSERT INTO `EchangeTypeMessage` (`id`, `libelle`, `corps`) VALUES (5, 'Courrier de demande de complément', '
TEXTE COURRIER DE DEMANDE DE COMPLEMENT
');

ALTER TABLE `EchangeDestinataire` ADD `id_inscrit` INT( 11 ) NOT NULL ,
ADD `id_entreprise` INT( 11 ) NOT NULL ;

ALTER TABLE `Echange` CHANGE `id_createur` `id_createur` INT( 11 ) NULL DEFAULT '0',
CHANGE `service_id` `service_id` INT( 11 ) NULL DEFAULT '0';

CREATE TABLE `DecisionLot` (
 `id_decision_lot` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`consultation_ref` INT( 11 ) NOT NULL ,
`lot` INT( 11 ) NOT NULL DEFAULT '0',
`id_type_decision` INT( 11 ) NOT NULL DEFAULT '0',
`autre_a_preciser` VARCHAR( 100 ) NULL,
`date_decision` DATE NULL ,
`commentaire` TEXT NULL 
) TYPE = innodb;

CREATE TABLE `decisionEnveloppe` (
`id_decision_enveloppe` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`consultation_ref` INT( 11 ) NOT NULL ,
`id_offre` INT( 11 ) NOT NULL,
`lot` INT( 11 ) NOT NULL DEFAULT '0',
`decision` ENUM( '0', '1' ) NOT NULL DEFAULT '0',
`date_notification` DATE NULL ,
`code_postal` VARCHAR( 5 ) NULL ,
`pme_pmi` INT( 11 ) NOT NULL DEFAULT '0',
`tranche_budgetaire` INT( 11 ) NULL , 
`montant_marche` VARCHAR( 50 ) NULL ,
`categorie` INT( 11 ) NULL ,
`objet_marche` TEXT NULL,
`commentaire` TEXT NULL,
`fichier_joint` INT( 11 ) NULL  ,
`nom_fichier_joint` VARCHAR( 100 ) NULL ,
`type_enveloppe` ENUM( '1', '2' ) NULL 
) TYPE = innodb; 

INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES ('2', 'Tranche budgétaire');

INSERT INTO `Referentiel` ( `id_referentiel` , `libelle_referentiel` )VALUES ('3', 'pme pmi');

TRUNCATE TABLE `TypeDecision` ;

INSERT INTO `TypeDecision` (`id_type_decision`, `libelle_type_decision`) VALUES (2, 'Attribution du marché');
INSERT INTO `TypeDecision` (`id_type_decision`, `libelle_type_decision`) VALUES (3, 'Déclaration Sans suite');
INSERT INTO `TypeDecision` (`id_type_decision`, `libelle_type_decision`) VALUES (4, 'Déclaration Infructueux');
INSERT INTO `TypeDecision` (`id_type_decision`, `libelle_type_decision`) VALUES (5, 'Sélection des entreprises (en vue de la phase suivante)');
INSERT INTO `TypeDecision` (`id_type_decision`, `libelle_type_decision`) VALUES (6, 'Attribution de l''Accord cadre');
INSERT INTO `TypeDecision` (`id_type_decision`, `libelle_type_decision`) VALUES (7, 'Admission au SAD');
INSERT INTO `TypeDecision` (`id_type_decision`, `libelle_type_decision`) VALUES (8, 'Autre');
INSERT INTO `TypeDecision` (`id_type_decision`, `libelle_type_decision`) VALUES (9, 'A renseigner');

ALTER TABLE `Enveloppe` DROP PRIMARY KEY ,ADD PRIMARY KEY ( `id_enveloppe_electro` ) ;
ALTER TABLE `Enveloppe_papier` DROP PRIMARY KEY ,ADD PRIMARY KEY ( `id_enveloppe_papier` ) ;
INSERT INTO `StatutEnveloppe` (`id_statut`, `description`) VALUES ('6', 'Supprimée');

ALTER TABLE `Enveloppe_papier` ADD `nom_agent_ouverture` VARCHAR( 100 ) NULL ;


INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (1, 1, 'Etat');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (2, 1, 'Région');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (3, 1, 'Département');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (4, 1, 'Commune');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (5, 1, 'Etablissement public national');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (6, 1, 'Etablissement public territorial');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (7, 1, 'Autre');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (9, 3, 'Non renseigné');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (10, 3, 'Oui');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (11, 3, 'Non');


INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (12, 2, '4 000 HT à 19 999,99 HT');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (13, 2, '20 000 HT à 49 999,99 HT');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (14, 2, '50 000 HT à 89 999,99 HT');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (15, 2, '90 000 HT à 134 999,99 HT');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (16, 2, '135 000 HT à 209 999,99 HT');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (17, 2, '210 000 HT à 999 999,99 HT');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (18, 2, '1 000 000 HT à 2 999 999,99 HT');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`) VALUES (19, 2, '3 000 000 HT à 5 269 999,99 HT');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel` )VALUES (20, 2, '&#8805; 5 270 k EUR HT');
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '&#8805; 4 k EUR HT et < 20 k EUR HT' WHERE `id` =12;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '&#8805; 20 k EUR HT et < 50 k EUR HT' WHERE `id` =13;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '&#8805; 50 k EUR HT et < 90 k EUR HT' WHERE `id` =14;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '&#8805; 90 k EUR HT et < 135 k EUR HT' WHERE `id` =15;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '&#8805; 135 k EUR HT et < 210 k EUR HT' WHERE `id` =16;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '&#8805; 210 k EUR HT et < 1 000 k EUR HT' WHERE `id` =17;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '&#8805; 1 000 k EUR HT et < 3 000 k EUR HT' WHERE `id` =18;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '&#8805; 3 000 k EUR HT et < 5 270 k EUR HT' WHERE `id` =19;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '&#8805; 5 270 k EUR HT' WHERE `id` =20;


ALTER TABLE `consultation` ADD `consultation_transverse` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `TypeProcedure` ADD `consultation_transverse` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

UPDATE `TypeProcedure` SET `consultation_transverse` = '1' WHERE `id_type_procedure` =30 LIMIT 1 ;

UPDATE `TypeProcedure` SET `consultation_transverse` = '1' WHERE `id_type_procedure` =32 LIMIT 1 ;

UPDATE `EchangeTypeMessage` SET `corps` = 'Votre entreprise a été déclarée attributaire de la consultation citée en référence.
Veuillez-trouver à titre de notification le marché signé par le Représentant du Pouvoir Adjudicateur.
L''Accusé de réception de ce message vaut notification officielle du marché.
' WHERE `id` =1 LIMIT 1 ;

UPDATE `EchangeTypeMessage` SET `corps` = 'Nous vous remercions d''avoir répondu à la consultation citée en référence.
Nous sommes toutefois au regret de vous annoncer que votre réponse n''a pas été retenue par le Représentant du Pouvoir Adjudicateur.
Cordialement,
' WHERE `id` =2 LIMIT 1 ;

UPDATE `EchangeTypeMessage` SET `corps` = 'La consultation citée en référence a été modifiée.
Les éléments modifiés sont : [à préciser au cas par cas]
Cordialement,
' WHERE `id` =3 LIMIT 1 ;

UPDATE `EchangeTypeMessage` SET `corps` = 'Nous vous remercions d''avoir répondu à la consultation citée en référence.
Après analyse, il vous est demandé d''apporter les précisions suivantes : [à préciser au cas par cas].
La réponse à ces questions peut se faire via l''application, à partir de la page d''accès à cette demande de complément.
Il est nécessaire de disposer d''un Compte entreprise sur l''application pour accéder à cette réponse.
Cordialement,' WHERE `id` =5 LIMIT 1 ;

ALTER TABLE `ProcedureEquivalence` ADD `type_decision_a_renseigner` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD `type_decision_attribution_marche` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD `type_decision_declaration_sans_suite` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD `type_decision_declaration_infructueux` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD `type_decision_selection_entreprise` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD `type_decision_attribution_accord_cadre` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD `type_decision_admission_sad` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD `type_decision_autre` CHAR( 2 ) NOT NULL DEFAULT '-0';

ALTER TABLE `consultation` ADD `type_decision_a_renseigner` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `consultation` ADD `type_decision_attribution_marche`  ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `consultation` ADD `type_decision_declaration_sans_suite`  ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `consultation` ADD `type_decision_declaration_infructueux` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `consultation` ADD `type_decision_selection_entreprise`  ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `consultation` ADD `type_decision_attribution_accord_cadre` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `consultation` ADD `type_decision_admission_sad`  ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `consultation` ADD `type_decision_autre`  ENUM( '0', '1' ) NOT NULL DEFAULT '1';

UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0', `type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =1 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =2 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =3 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =4 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =5 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =6 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =7 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =8 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =9 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =10 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =11 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =12 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =13 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =14 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =16 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =17 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =18 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =22 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =23 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_selection_entreprise` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =24 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_attribution_accord_cadre` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =30 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =31 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_admission_sad` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =32 LIMIT 1 ;
UPDATE `ProcedureEquivalence` SET `type_decision_a_renseigner` = '+0',`type_decision_attribution_marche` = '+0',`type_decision_declaration_sans_suite` = '+0',`type_decision_declaration_infructueux` = '+0',`type_decision_autre` = '+0' WHERE `ProcedureEquivalence`.`id_type_procedure` =33 LIMIT 1 ;
DELETE FROM `ProcedureEquivalence` WHERE `id_type_procedure` = '15';
DELETE FROM `TypeProcedure` WHERE `id_type_procedure` = '15';

UPDATE `ProcedureEquivalence` SET `type_decision_attribution_marche`='-0' where `ProcedureEquivalence`.`id_type_procedure` =23 LIMIT 1 ;


CREATE TABLE IF NOT EXISTS `JAL` (
  `id` int(11) NOT NULL auto_increment,
  `id_service` int(11) NOT NULL default '0',
  `nom` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `email_ar` varchar(100) NOT NULL default '',
  `telecopie` varchar(20) NOT NULL default '',
  `information_facturation` text NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `Admissibilite_Enveloppe_Lot`  ENGINE = innodb;
ALTER TABLE `Admissibilite_Enveloppe_papier_Lot`  ENGINE = innodb ;

UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.8 BOAMP3 - Avis de marché National' WHERE `id` =1;

UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.8 BOAMP6 - Avis de MAPA' WHERE `id` =2;

UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.8 JOUE_02 - Avis de marché' WHERE `id` =3;

UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.8 JOUE_03 - Avis d''attribution' WHERE `id` =4;

UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.8 BOAMP4 - Avis d''attribution' WHERE `id` =5;

UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.8 BOAMP2 - Avis rectificatif' WHERE `id` =6;

REPLACE INTO `ReferentielTypeXml` ( `id` , `id_destinataire` , `libelle_type` ) VALUES (7 , '1#3', 'XML 1.8 BOAMP7 - Résultat de MAPA');

REPLACE INTO `ReferentielTypeXml` ( `id` , `id_destinataire` , `libelle_type` ) VALUES (8 , '1#3', 'XML 1.8 JOUE_14T - Formulaire rectificatif / annulation');

UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.8 BOAMP7 - Résultat de MAPA' WHERE `id` =7;

CREATE TABLE `fichierEnveloppe` (
`id_fichier` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_enveloppe` INT( 11 ) NOT NULL ,
`type_fichier` CHAR( 3 ) NOT NULL ,
`num_ordre_fichier` INT( 5 ) NOT NULL ,
`nom_fichier` VARCHAR( 100 ) NOT NULL,
`taille_fichier` VARCHAR( 50 ) NOT NULL ,
`signature_fichier` TEXT NOT NULL ,
`hash` TEXT NOT NULL ,
`verification_certificat` VARCHAR( 5 ) NOT NULL 
) TYPE = innodb;

CREATE TABLE `blocFichierEnveloppe` (
`id_bloc_fichier` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_fichier` INT( 11 ) NOT NULL ,
`numero_ordre_bloc` INT( 5 ) NOT NULL ,
`id_blob` INT( 11 ) NOT NULL 
) TYPE = innodb;

CREATE TABLE `blocFichierEnveloppeTemporaire` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`uid` VARCHAR( 50 ) NOT NULL ,
`type_env` INT( 1 ) NOT NULL ,
`sous_pli` INT( 3 ) NOT NULL ,
`type_fichier` CHAR( 3 ) NOT NULL ,
`numero_ordre_fichier` INT( 5 ) NOT NULL ,
`numero_ordre_bloc` INT( 5 ) NOT NULL,
`id_blob` INT( 11 ) NOT NULL
) TYPE = innodb;


CREATE TABLE `AnnonceJAL` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`date_creation` VARCHAR( 14 ) NOT NULL  ,
`objet` VARCHAR( 255 ) NULL ,
`texte` TEXT NULL ,
`consultation_ref` INT( 11 ) NOT NULL
) TYPE = innodb;

--
ALTER TABLE `AVIS` ADD `url` TEXT NULL ,
ADD `type` CHAR( 1 ) NULL DEFAULT '0';

CREATE TABLE `DestinataireAnnonceJAL` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`idJAL` INT( 11 ) NOT NULL ,
`date_envoi` VARCHAR( 14 ) NOT NULL ,
`date_pub` VARCHAR( 14 ) NOT NULL ,
`statut` CHAR( 1 ) NOT NULL ,
`accuse` CHAR( 1 ) NOT NULL ,
INDEX ( `idJAL` ) 
) TYPE = innodb;




ALTER TABLE `DestinataireAnnonceJAL` ADD `idAnnonceJAL` INT( 11 ) NOT NULL AFTER `idJAL` ;

ALTER TABLE `DestinataireAnnonceJAL` ADD INDEX ( `idAnnonceJAL` ) ;

ALTER TABLE `AVIS` ADD `date_creation` VARCHAR( 14 ) NOT NULL ;

ALTER TABLE `AnnonceJAL` ADD `libelle_type` VARCHAR( 250 ) NOT NULL AFTER `id` ;


CREATE TABLE `PjAnnonceJAL` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_annonceJal` INT( 11 ) NOT NULL ,
`intitule_pj` VARCHAR( 250 ) NOT NULL ,
`id_pj` INT( 11 ) NOT NULL ,
`statut` CHAR( 1 ) NULL ,
`taille` VARCHAR( 11 ) NULL ,
INDEX ( `id_avis` ) 
) TYPE = innodb;

ALTER TABLE `AVIS` ADD `date_pub` VARCHAR( 14 ) NULL ;

ALTER TABLE `AVIS` CHANGE `statut` `statut` CHAR( 1 )NOT NULL DEFAULT '0' ;
ALTER TABLE `AVIS` CHANGE `date_creation` `date_creation` VARCHAR( 20 )  NOT NULL ;

ALTER TABLE `AVIS` CHANGE `date_pub` `date_pub` VARCHAR( 20 ) NULL DEFAULT NULL ;
ALTER TABLE `AnnonceJAL` CHANGE `date_creation` `date_creation` VARCHAR( 20 ) NOT NULL;

ALTER TABLE `Offre` ADD `xml_string` TEXT NOT NULL  ;
ALTER TABLE `Enveloppe` CHANGE `dateheure_ouverture` `dateheure_ouverture` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `fichierEnveloppe` ADD `verification_certificat` VARCHAR( 5 ) NOT NULL ;
CREATE TABLE `AnnonceJALPieceJointe` (
  `id` int(11) NOT NULL auto_increment,
  `id_annonce_jal` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) character set latin1 NOT NULL default '',
  `piece` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(25) character set latin1 NOT NULL default '',
  `taille` varchar(25) character set latin1 NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id_annonce_jal` (`id_annonce_jal`)
) ENGINE=InnoDB;

ALTER TABLE `AnnonceJALPieceJointe` ADD CONSTRAINT `AnnonceJALPieceJointe_ibfk_1` FOREIGN KEY (`id_annonce_jal`) REFERENCES `AnnonceJAL` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `fichierEnveloppe` ADD INDEX ( `id_enveloppe` );
ALTER TABLE `fichierEnveloppe` ADD FOREIGN KEY ( `id_enveloppe` ) REFERENCES `Enveloppe` (`id_enveloppe_electro`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `blocFichierEnveloppe` ADD `id_blob_dechiffre` INT( 11 ) NULL DEFAULT NULL;
ALTER TABLE `blocFichierEnveloppe` CHANGE `id_blob` `id_blob_chiffre` INT( 11 ) NOT NULL ;
ALTER TABLE `blocFichierEnveloppe` ADD INDEX ( `id_fichier` ) ;
ALTER TABLE `blocFichierEnveloppe` ADD FOREIGN KEY ( `id_fichier` ) REFERENCES `fichierEnveloppe` (`id_fichier`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `fichierEnveloppe` ADD `id_blob` INT( 11 ) NULL DEFAULT NULL ;

ALTER TABLE `Enveloppe` ADD `agent_telechargement` INT( 11 ) NULL DEFAULT NULL ;
ALTER TABLE `Enveloppe` ADD `date_telechargement` VARCHAR( 20 ) NULL DEFAULT NULL ;
ALTER TABLE `Enveloppe` ADD `repertoire_telechargement` VARCHAR( 100 ) NULL DEFAULT NULL ;
ALTER TABLE `Telechargement` ADD `id_inscrit` INT( 11 ) DEFAULT '0',ADD `id_entreprise` INT( 11 ) DEFAULT '0';
ALTER TABLE `consultation` ADD `id_archiveur` int(11) NULL;
ALTER TABLE `ReferentielFormXml` CHANGE `date_creation` `date_creation` VARCHAR( 20 ) NULL DEFAULT NULL  ;
ALTER TABLE `Annonce` CHANGE `statut_destinataire` `statut_destinataire` CHAR( 10 ) NOT NULL ;
ALTER TABLE `ReferentielFormXml` ADD `id_compte_boamp` int(11) NOT NULL;
ALTER TABLE `Annonce` ADD `lien_boamp` TEXT NULL ;
ALTER TABLE `Annonce` ADD `lien_pdf` TEXT NULL ;

ALTER TABLE `consultation` ADD `prenom_nom_agent_telechargement_plis` VARCHAR( 255 ) NULL ;
ALTER TABLE `consultation` ADD `id_agent_telechargement_plis` int(11) NOT NULL;
ALTER TABLE `consultation` ADD `path_telechargement_plis` VARCHAR( 255 ) NULL ;
ALTER TABLE `consultation` ADD `date_telechargement_plis` VARCHAR( 20 ) NULL ;

ALTER TABLE `TypeProcedure` ADD `tag_Boamp` VARCHAR( 50 ) NOT NULL ;

UPDATE `TypeProcedure` SET `tag_Boamp` = '<AppelOuvert/>' WHERE `TypeProcedure`.`id_type_procedure` =1 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<AppelRestreint/>' WHERE `TypeProcedure`.`id_type_procedure` =2 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<AppelRestreint/>' WHERE `TypeProcedure`.`id_type_procedure` =3 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<ProcedureAdaptee/>' WHERE `TypeProcedure`.`id_type_procedure` =4 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<ProcedureAdaptee/>' WHERE `TypeProcedure`.`id_type_procedure` =5 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<ProcedureAdaptee/>' WHERE `TypeProcedure`.`id_type_procedure` =6 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<ProcedureAdaptee/>' WHERE `TypeProcedure`.`id_type_procedure` =7 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<ConcoursOuvert/>' WHERE `TypeProcedure`.`id_type_procedure` =8 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<ConcoursRestreint/>' WHERE `TypeProcedure`.`id_type_procedure` =9 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<ConcoursRestreint/>' WHERE `TypeProcedure`.`id_type_procedure` =10 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<DialogueCompetitif/>' WHERE `TypeProcedure`.`id_type_procedure` =11 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<DialogueCompetitif/>' WHERE `TypeProcedure`.`id_type_procedure` =12 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<Autres/>' WHERE `TypeProcedure`.`id_type_procedure` =13 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<Autres/>' WHERE `TypeProcedure`.`id_type_procedure` =14 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<Autres/>' WHERE `TypeProcedure`.`id_type_procedure` =16 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<Autres/>' WHERE `TypeProcedure`.`id_type_procedure` =17 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<MarcheNegocie/>' WHERE `TypeProcedure`.`id_type_procedure` =18 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<Autres/>' WHERE `TypeProcedure`.`id_type_procedure` =22 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<MarcheNegocie/>' WHERE `TypeProcedure`.`id_type_procedure` =23 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<MarcheNegocie/>' WHERE `TypeProcedure`.`id_type_procedure` =24 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<Autres/>' WHERE `TypeProcedure`.`id_type_procedure` =30 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<Autres/>' WHERE `TypeProcedure`.`id_type_procedure` =31 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<Autres/>' WHERE `TypeProcedure`.`id_type_procedure` =32 ;
UPDATE `TypeProcedure` SET `tag_Boamp` = '<Autres/>' WHERE `TypeProcedure`.`id_type_procedure` =33 ;

INSERT INTO `StatutEnveloppe` ( `id_statut` , `description` ) VALUES ('7', 'Ouverte à distance');
ALTER TABLE `consultation` ADD `accessibilite_en` CHAR( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_es` CHAR( 1 ) NOT NULL DEFAULT '0';

ALTER TABLE `TypeProcedure`  ADD `ao` ENUM('0','1') NOT NULL DEFAULT '0',
ADD `mn` ENUM('0','1') NOT NULL DEFAULT '0',
ADD `dc` ENUM('0','1') NOT NULL DEFAULT '0',
ADD `autre` ENUM('0','1') NOT NULL DEFAULT '0' ;

ALTER TABLE `TypeProcedure` ADD `sad` ENUM( '0', '1' ) NOT NULL DEFAULT '0',
ADD `accord_cadre` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `Enveloppe` ADD `nom_agent_ouverture` VARCHAR( 100 ) NOT NULL ;

DROP TABLE `MarcheTrancheBudgetaire`;

ALTER TABLE `Marche` DROP FOREIGN KEY `Marche_ibfk_1`;
ALTER TABLE `Marche` MODIFY `pmePmi` int(11) NOT NULL default '0';

UPDATE `TypeProcedure` SET `ao` = '1' WHERE `id_type_procedure` =1 LIMIT 1 ;

ALTER TABLE `consultation` ADD `accessibilite_en` CHAR( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_es` CHAR( 1 ) NOT NULL DEFAULT '0';

UPDATE `TypeProcedure` SET `ao` = '1' WHERE `TypeProcedure`.`id_type_procedure` =1 LIMIT 1 ;
UPDATE `TypeProcedure` SET `ao` = '1' WHERE `TypeProcedure`.`id_type_procedure` =2 LIMIT 1 ;
UPDATE `TypeProcedure` SET `ao` = '1' WHERE `TypeProcedure`.`id_type_procedure` =3 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =8 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =9 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =10 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =13 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =14 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =16 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =17 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =22 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =32 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =33 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =30 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =31 LIMIT 1 ;
UPDATE `TypeProcedure` SET `dc` = '1' WHERE `TypeProcedure`.`id_type_procedure` =11 LIMIT 1 ;
UPDATE `TypeProcedure` SET `dc` = '1' WHERE `TypeProcedure`.`id_type_procedure` =12 LIMIT 1 ;
UPDATE `TypeProcedure` SET `mn` = '1' WHERE `TypeProcedure`.`id_type_procedure` =18 LIMIT 1 ;
UPDATE `TypeProcedure` SET `mn` = '1' WHERE `TypeProcedure`.`id_type_procedure` =23 LIMIT 1 ;
UPDATE `TypeProcedure` SET `mn` = '1' WHERE `TypeProcedure`.`id_type_procedure` =24 LIMIT 1 ;
UPDATE `TypeProcedure` SET `accord_cadre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =30 LIMIT 1 ;
UPDATE `TypeProcedure` SET `accord_cadre` = '1' WHERE `TypeProcedure`.`id_type_procedure` =31 LIMIT 1 ;
UPDATE `TypeProcedure` SET `sad` = '1' WHERE `TypeProcedure`.`id_type_procedure` =32 LIMIT 1 ;
UPDATE `TypeProcedure` SET `sad` = '1' WHERE `TypeProcedure`.`id_type_procedure` =33 LIMIT 1 ;

ALTER TABLE `consultation` ADD `organisme_consultation_init` VARCHAR( 255 ) NOT NULL ;

ALTER TABLE `consultation` ADD `service_validation_intermediaire` INT( 11 ) NULL ;
ALTER TABLE `consultation` ADD `date_validation_intermediaire` VARCHAR( 20 ) NULL ;
ALTER TABLE `consultation` ADD `date_mise_en_ligne_calcule` VARCHAR( 20 ) NULL ;

UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =1 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =2 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '2#3' WHERE `id` =3 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '2#3' WHERE `id` =4 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =5 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =6 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =7 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '2#3' WHERE `id` =8 LIMIT 1 ;

ALTER TABLE `consultation` ADD `consultation_achat_publique` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `consultation` ADD `accessibilite_fr` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_cz` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_du` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_su` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `Complement` CHANGE `untrusteddate` `untrusteddate` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `AnnonceMoniteur` CHANGE `statut_boamp` `statut_xml` VARCHAR( 4 ) NOT NULL DEFAULT '0' ;















------ ajoute le 20 avril 2009
ALTER TABLE `consultation` ADD `organisme_consultation_init` VARCHAR( 255 ) NOT NULL ;

ALTER TABLE `consultation` ADD `service_validation_intermediaire` INT( 11 ) NULL ;
ALTER TABLE `consultation` ADD `date_validation_intermediaire` VARCHAR( 20 ) NULL ;
ALTER TABLE `consultation` ADD `date_mise_en_ligne_calcule` VARCHAR( 20 ) NULL ;

UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =1 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =2 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '2#3' WHERE `id` =3 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '2#3' WHERE `id` =4 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =5 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =6 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '1#3' WHERE `id` =7 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '2#3' WHERE `id` =8 LIMIT 1 ;

ALTER TABLE `consultation` ADD `consultation_achat_publique` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `consultation` ADD `accessibilite_fr` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_cz` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_du` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `accessibilite_su` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `Complement` CHANGE `untrusteddate` `untrusteddate` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `consultation` CHANGE `detail_consultation` `detail_consultation` TEXT NOT NULL DEFAULT '';
ALTER TABLE `Annonce` CHANGE `date_envoi` `date_envoi` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `Annonce` CHANGE `datepub` `datepub` VARCHAR( 10 ) NOT NULL DEFAULT '0000-00-00';

-- OAB : ajout des indexs sur les champs utilisés dans la recherche


CREATE INDEX Idx_Consultation_Categorie on consultation (categorie);

CREATE INDEX Idx_Consultation_Titre on consultation (titre);

CREATE INDEX Idx_Consultation_Resume on consultation (resume);

CREATE INDEX Idx_Consultation_CAP on consultation (consultation_achat_publique);

CREATE INDEX Idx_Consultation_SV on consultation (service_validation);

CREATE INDEX Idx_Consultation_Datevalid on consultation (datevalidation);

CREATE INDEX Idx_Consultation_EA on consultation (etat_approbation);

CREATE INDEX Idx_Consultation_DVI on consultation (date_validation_intermediaire);

CREATE INDEX Idx_Consultation_SVI on consultation (service_validation_intermediaire);

CREATE INDEX Idx_Consultation_RU on consultation (reference_utilisateur);

CREATE INDEX Idx_Consultation_Datefin on consultation (datefin);

CREATE INDEX Idx_Consultation_ITP on consultation (id_type_procedure);

CREATE INDEX Idx_Consultation_ITA on consultation (id_type_avis);

CREATE INDEX Idx_Consultation_TML on consultation (type_mise_en_ligne);

CREATE INDEX Idx_Consultation_DMLC on consultation (date_mise_en_ligne_calcule);

CREATE INDEX Idx_Consultation_Datemiseenligne on consultation (datemiseenligne);

CREATE INDEX Idx_Consultation_LE on consultation (lieu_execution);

CREATE INDEX Idx_Consultation_Code_CPV on consultation (code_cpv_1,code_cpv_2,code_cpv_3,code_cpv_4);

CREATE INDEX Idx_Consultation_IEC on consultation (id_etat_consultation);

CREATE INDEX Idx_Annonce on Annonce (consultation_ref);

CREATE INDEX Idx_CategorieLot on CategorieLot (consultation_ref);

ALTER TABLE `consultation` CHANGE `detail_consultation` `detail_consultation` TEXT NOT NULL DEFAULT '';

CREATE TABLE `Mesure_avancement` (
`id_mesure` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_service` INT( 11 ) NOT NULL ,
`annee` VARCHAR( 4 ) NOT NULL ,
`trimestre` CHAR( 1 ) NOT NULL ,
`siren` VARCHAR( 9 ) NOT NULL ,
`nic` VARCHAR( 5 ) NOT NULL ,
`identifiant_service` VARCHAR( 100 ) NOT NULL ,
`mail` VARCHAR( 100 ) NOT NULL ,
`type_pouvoir_adjudicateur` INT( 11 ) NOT NULL ,
`departement` INT( 11 ) NOT NULL ,
`date_envoi` VARCHAR( 20 ) NULL ,
`date_accuse` VARCHAR( 20 ) NULL ,
`etat` VARCHAR( 5 ) NULL DEFAULT 'BR',
`date_creation` VARCHAR( 20 ) NOT NULL
) TYPE = innodb;


CREATE TABLE `Donnee_mesure_avancement` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_mesure` INT( 11 ) NOT NULL ,
`aapc_aoo_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`aapc_aoo_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`aapc_aoo_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`aapc_aoo_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`aapc_aor_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`aapc_aor_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`aapc_aor_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`aapc_aor_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`aapc_mapa_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`aapc_mapa_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`aapc_mapa_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`aapc_mapa_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`aapc_aoo_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`aapc_aoo_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`aapc_aoo_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`aapc_aoo_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`aapc_aor_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`aapc_aor_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`aapc_aor_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`aapc_aor_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`aapc_mapa_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`aapc_mapa_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`aapc_mapa_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`aapc_mapa_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`aapc_aoo_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`aapc_aoo_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`aapc_aoo_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`aapc_aoo_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`aapc_aor_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`aapc_aor_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`aapc_aor_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`aapc_aor_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`aapc_mapa_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`aapc_mapa_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`aapc_mapa_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`aapc_mapa_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`dce_aoo_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`dce_aoo_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`dce_aoo_travaux_nombre_retraits_papier` VARCHAR ( 20 ) NULL ,
`dce_aoo_travaux_nombre_telechargement` VARCHAR ( 20 ) NULL ,
`dce_aoo_travaux_nombre_retraits_mixte` VARCHAR ( 20 ) NULL ,
`dce_aor_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`dce_aor_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`dce_aor_travaux_nombre_retraits_papier` VARCHAR ( 20 ) NULL ,
`dce_aor_travaux_nombre_telechargement` VARCHAR ( 20 ) NULL ,
`dce_aor_travaux_nombre_retraits_mixte` VARCHAR ( 20 ) NULL ,
`dce_mapa_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`dce_mapa_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`dce_mapa_travaux_nombre_retraits_papier` VARCHAR ( 20 ) NULL ,
`dce_mapa_travaux_nombre_telechargement` VARCHAR ( 20 ) NULL ,
`dce_mapa_travaux_nombre_retraits_mixte` VARCHAR ( 20 ) NULL ,
`dce_aoo_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`dce_aoo_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`dce_aoo_fournitures_nombre_retraits_papier` VARCHAR ( 20 ) NULL ,
`dce_aoo_fournitures_nombre_telechargement` VARCHAR ( 20 ) NULL ,
`dce_aoo_fournitures_nombre_retraits_mixte` VARCHAR ( 20 ) NULL ,
`dce_aor_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`dce_aor_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`dce_aor_fournitures_nombre_retraits_papier` VARCHAR ( 20 ) NULL ,
`dce_aor_fournitures_nombre_telechargement` VARCHAR ( 20 ) NULL ,
`dce_aor_fournitures_nombre_retraits_mixte` VARCHAR ( 20 ) NULL ,
`dce_mapa_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`dce_mapa_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`dce_mapa_fournitures_nombre_retraits_papier` VARCHAR ( 20 ) NULL ,
`dce_mapa_fournitures_nombre_telechargement` VARCHAR ( 20 ) NULL ,
`dce_mapa_fournitures_nombre_retraits_mixte` VARCHAR ( 20 ) NULL ,
`dce_aoo_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`dce_aoo_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`dce_aoo_services_nombre_retraits_papier` VARCHAR ( 20 ) NULL ,
`dce_aoo_services_nombre_telechargement` VARCHAR ( 20 ) NULL ,
`dce_aoo_services_nombre_retraits_mixte` VARCHAR ( 20 ) NULL ,
`dce_aor_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`dce_aor_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`dce_aor_services_nombre_retraits_papier` VARCHAR ( 20 ) NULL ,
`dce_aor_services_nombre_telechargement` VARCHAR ( 20 ) NULL ,
`dce_aor_services_nombre_retraits_mixte` VARCHAR ( 20 ) NULL ,
`dce_mapa_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`dce_mapa_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`dce_mapa_services_nombre_retraits_papier` VARCHAR ( 20 ) NULL ,
`dce_mapa_services_nombre_telechargement` VARCHAR ( 20 ) NULL ,
`dce_mapa_services_nombre_retraits_mixte` VARCHAR ( 20 ) NULL ,
`candidature_aoo_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`candidature_aoo_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`candidature_aoo_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`candidature_aoo_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`candidature_aor_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`candidature_aor_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`candidature_aor_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`candidature_aor_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`candidature_mapa_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`candidature_mapa_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`candidature_mapa_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`candidature_mapa_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`candidature_aoo_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`candidature_aoo_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`candidature_aoo_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`candidature_aoo_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`candidature_aor_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`candidature_aor_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`candidature_aor_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`candidature_aor_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`candidature_mapa_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`candidature_mapa_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`candidature_mapa_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`candidature_mapa_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`candidature_aoo_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`candidature_aoo_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`candidature_aoo_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`candidature_aoo_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`candidature_aor_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`candidature_aor_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`candidature_aor_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`candidature_aor_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`candidature_mapa_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`candidature_mapa_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`candidature_mapa_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`candidature_mapa_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`offres_aoo_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`offres_aoo_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`offres_aoo_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`offres_aoo_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`offres_aor_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`offres_aor_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`offres_aor_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`offres_aor_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`offres_mapa_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`offres_mapa_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`offres_mapa_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`offres_mapa_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`offres_aoo_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`offres_aoo_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`offres_aoo_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`offres_aoo_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`offres_aor_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`offres_aor_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`offres_aor_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`offres_aor_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`offres_mapa_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`offres_mapa_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`offres_mapa_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`offres_mapa_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`offres_aoo_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`offres_aoo_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`offres_aoo_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`offres_aoo_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`offres_aor_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`offres_aor_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`offres_aor_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`offres_aor_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`offres_mapa_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`offres_mapa_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`offres_mapa_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`offres_mapa_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`notifications_aoo_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`notifications_aoo_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`notifications_aoo_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`notifications_aoo_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`notifications_aor_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`notifications_aor_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`notifications_aor_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`notifications_aor_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`notifications_mapa_travaux_nombre_procedures` VARCHAR ( 20 ) NULL ,
`notifications_mapa_travaux_nombre_lots` VARCHAR ( 20 ) NULL ,
`notifications_mapa_travaux_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`notifications_mapa_travaux_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`notifications_aoo_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`notifications_aoo_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`notifications_aoo_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`notifications_aoo_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`notifications_aor_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`notifications_aor_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`notifications_aor_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`notifications_aor_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`notifications_mapa_fournitures_nombre_procedures` VARCHAR ( 20 ) NULL ,
`notifications_mapa_fournitures_nombre_lots` VARCHAR ( 20 ) NULL ,
`notifications_mapa_fournitures_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`notifications_mapa_fournitures_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`notifications_aoo_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`notifications_aoo_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`notifications_aoo_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`notifications_aoo_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`notifications_aor_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`notifications_aor_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`notifications_aor_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`notifications_aor_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ,
`notifications_mapa_services_nombre_procedures` VARCHAR ( 20 ) NULL ,
`notifications_mapa_services_nombre_lots` VARCHAR ( 20 ) NULL ,
`notifications_mapa_services_nombre_avis_papier` VARCHAR ( 20 ) NULL ,
`notifications_mapa_services_nombre_avis_dematerialise` VARCHAR ( 20 ) NULL ) TYPE = innodb;

ALTER TABLE `consultation` ADD `alloti` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `numero_phase` INT( 3 ) NOT NULL DEFAULT '0';

CREATE TABLE IF NOT EXISTS `Passation_consultation` (
  `id` int(11) NOT NULL auto_increment,
  `reference` int(11) NOT NULL default '0',
  `unite` int(5) default '0',
  `mandataire` int(5) default '0',
  `lieu_detaille_predefini` int(5) default '0',
  `lieux_detailles_non_definis` varchar(100) default NULL,
  `code_postal_lieu_principal_execution` varchar(20) default NULL,
  `Ville` varchar(50) default NULL,
  `montant_estime_consultation` varchar(20) default NULL,
  `commentaires` text,
  `numero_deliberation_financiere` varchar(20) default NULL,
  `date_deliberation_financiere` varchar(10) default NULL,
  `imputation_budgetaire` text,
  `numero_deliberation_autorisant_signature_marche` varchar(20) default NULL,
  `date_deliberation_autorisant_signature_marche` varchar(10) default NULL,
  `date_notification_previsionnelle` varchar(10) default NULL,
  `date_reception_projet_DCE_Service_Validateur` varchar(10) default NULL,
  `date_formulations_premieres_observations` varchar(10) default NULL,
  `date_retour_projet_DCE_finalise` varchar(10) default NULL,
  `date_validation_projet_DCE_par_service_validateur` varchar(10) default NULL,
  `date_validation_projet_DCE_vue_par` int(5) default '0',
  `date_reception_projet_AAPC_par_Service_Validateur` varchar(10) default NULL,
  `date_formulations_premieres_observations_AAPC` varchar(10) default NULL,
  `date_retour_projet_AAPC_finalise` varchar(10) default NULL,
  `date_validation_projet_AAPC_par_Service_Validateur` varchar(10) default NULL,
  `date_validation_projet_AAPC_par_Service_Validateur_vu_par` int(5) default '0',
  `date_envoi_publicite` varchar(10) default NULL,
  `date_envoi_invitations_remettre_offre` varchar(10) default NULL,
  `date_limite_remise_offres` varchar(10) default NULL,
  `delai_validite_offres` varchar(5) default NULL,
  `commentaires_phase_consultation` text,
  `date_reunion_ouverture_candidatures` varchar(10) default NULL,
  `date_reunion_ouverture_offres` varchar(10) default NULL,
  `decision` int(5) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1;

--22/12/2008 : MMI
ALTER TABLE `AnnonceMoniteur` ADD `id_form_xml` INT( 11 ) NOT NULL ;
ALTER TABLE `AnnonceMoniteur` ADD INDEX ( `id_form_xml` ) ;
ALTER TABLE `AnnonceMoniteur` ADD `accuse_reception` CHAR( 1 ) NOT NULL ;
ALTER TABLE `AnnonceMoniteur` ADD `id_destination_form_xml` INT( 11 ) NOT NULL ;
ALTER TABLE `AnnonceMoniteur` ADD INDEX ( `id_destination_form_xml` ) ;

--23/12/2008 :MMI
ALTER TABLE `AnnonceMoniteur` CHANGE `date_envoi` `date_envoi` VARCHAR( 20 ) NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `AnnonceMoniteur` CHANGE `date_pub` `datepub` VARCHAR( 10 ) NOT NULL DEFAULT '0000-00-00';
ALTER TABLE `AnnonceMoniteur` DROP FOREIGN KEY `AnnonceMoniteur_ibfk_1` ;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_annonce_boamp` ;
ALTER TABLE `AnnonceMoniteur` ADD `statut_destinataire` VARCHAR( 10 ) NULL ;

ALTER TABLE `AnnonceMoniteur` CHANGE `statut_envoi` `statut_boamp` VARCHAR( 10 ) NOT NULL DEFAULT '0';
-- Compte de production
INSERT INTO `CompteMoniteur` VALUES (1, 'Atexo', 'mdp08eT@', 'suivi.mpe@atexo.com', '1');
-- Compte de test
INSERT INTO `CompteMoniteur` VALUES (2, 'Atexo', '@texo', 'dev.pmi@atexo.biz', '0');

CREATE TABLE IF NOT EXISTS `passation_marche_a_venir` (
  `id` int(11) NOT NULL auto_increment,
  `id_passation_consultation` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `montant_estime` int(11) NOT NULL default '0',
  `id_nature_acte_juridique` int(11) NOT NULL default '0',
  `forme_groupement` int(11) NOT NULL default '0',
  `variante_autorisee` enum('0','1') NOT NULL default '0',
  `variante_technique_obligatoire` enum('0','1') NOT NULL default '0',
  `nombre_variante_technique` int(11) NOT NULL default '0',
  `description_variante_technique` text NOT NULL,
  `mode_execution_contrat` int(11) NOT NULL default '0',
  `type_bon_commande` int(11) NOT NULL default '0',
  `min_bon_commande` int(11) NOT NULL default '0',
  `max_bon_commande` int(11) NOT NULL default '0',
  `duree_execution_marche_hors_reconduction` int(11) NOT NULL default '0',
  `nombre_reconduction` int(11) NOT NULL default '0',
  `duree_total_marche` int(11) NOT NULL default '0',
  `ccag_applicable` int(11) NOT NULL default '0',
  `marche_transversal` enum('0','1') NOT NULL default '0',
  `date_reception_analyse_offre` varchar(10) default NULL,
  `date_formulation_observation_projet_rapport` varchar(10) default NULL,
  `date_retour_projet_rapport_finalise` varchar(10) default NULL,
  `date_validation_projet_rapport` varchar(10) default NULL,
  `projet_rapport_vu_par` int(11) default NULL,
  `date_reunion_attribution` varchar(10) default NULL,
  `decision` int(11) default NULL,
  `date_envoi_courrier_condidat_non_retenu` varchar(10) default NULL,
  `date_signature_marche_pa` varchar(10) default NULL,
  `date_reception_controle_legalite` varchar(10) default NULL,
  `date_formulation_observation_dossier` varchar(10) default NULL,
  `date_retour_dossier_finalise` varchar(10) default NULL,
  `date_transmission_prefecture` varchar(10) default NULL,
  `dossier_vu_par` int(11) default NULL,
  `date_validation_rapport_information` varchar(10) default NULL,
  `commentaire` text,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `ccag_applicable` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

INSERT INTO `ccag_applicable` (`id`, `libelle`) VALUES (1, 'FCS');
INSERT INTO `ccag_applicable` (`id`, `libelle`) VALUES (2, 'PI');
INSERT INTO `ccag_applicable` (`id`, `libelle`) VALUES (3, 'T');

CREATE TABLE IF NOT EXISTS `mode_execution_contrat` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

INSERT INTO `mode_execution_contrat` (`id`, `libelle`) VALUES (1, '1 - Ordinaire (Sans tranche, exclusivement à prix forfaitaire)');
INSERT INTO `mode_execution_contrat` (`id`, `libelle`) VALUES (2, '2 - A bons de commande (Sans tranche, exclusivement à bons de commande)');
INSERT INTO `mode_execution_contrat` (`id`, `libelle`) VALUES (3, '3 - A Tranches (Avec TF et TC, exclusivement à prix forfaitaire)');
INSERT INTO `mode_execution_contrat` (`id`, `libelle`) VALUES (4, '4 - A tranches et bons de commande (Avec TF et TC, en partie à bons de commande)');
INSERT INTO `mode_execution_contrat` (`id`, `libelle`) VALUES (5, '5 - A phases');
INSERT INTO `mode_execution_contrat` (`id`, `libelle`) VALUES (6, '6 - Autres');

CREATE TABLE IF NOT EXISTS `nature_acte_juridique` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;
 
INSERT INTO `nature_acte_juridique` (`id`, `libelle`) VALUES (1, '1-Contrat initial');
INSERT INTO `nature_acte_juridique` (`id`, `libelle`) VALUES (2, '2-Contrat complémentaire');
INSERT INTO `nature_acte_juridique` (`id`, `libelle`) VALUES (3, '3-Contrat sur la base d''un accord cadre');
INSERT INTO `nature_acte_juridique` (`id`, `libelle`) VALUES (4, '4-Marché de définition');
INSERT INTO `nature_acte_juridique` (`id`, `libelle`) VALUES (5, '5-Autre');

CREATE TABLE IF NOT EXISTS `Fcsp_unite` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL
) ENGINE=InnoDB;

--
-- Contenu de la table `Fcsp_unite`
--

ALTER TABLE `Fcsp_unite` ADD PRIMARY KEY(`id`);

CREATE TABLE IF NOT EXISTS `Fcsp_Mandataire` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `Fcsp_Lieu` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL
) ENGINE=InnoDB;

ALTER TABLE `Fcsp_Lieu` ADD PRIMARY KEY(`id`);


CREATE TABLE `Contrat` (
  `id_contrat` int(11) NOT NULL auto_increment,
  `id_marche` int(11) NOT NULL default '0',
  `id_decision` int(11) NOT NULL default '0',
  `informaions_complementaires` text,
  PRIMARY KEY  (`id_contrat`),
  KEY `id_marche` (`id_marche`),
  KEY `id_decision` (`id_decision`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `Avenant` (
  `id_avenant` int(11) NOT NULL auto_increment,
  `id_contrat` int(11) NOT NULL,
  `numero_avenant` varchar(20) default NULL,
  `type_avenant` int(5) default NULL,
  `objet_avenant` varchar(100) default NULL,
  `montant_avenant_ht` varchar(30) default NULL,
  `montant_avenant_ttc` varchar(30) default NULL,
  `pourcentage_augmentation_marche_initial` varchar(3) default NULL,
  `pourcentage_augmentation_cumule` varchar(3) default NULL,
  `montant_total_marche_tout_avenant_cumule` varchar(30) default NULL,
  `date_reception_projet_par_secretaire_cao` varchar(10) default NULL,
  `date_reception_projet_par_charge_etude` varchar(10) default NULL,
  `date_observation_par_sv` varchar(10) default NULL,
  `date_retour_projet` varchar(10) default NULL,
  `date_validation_projet` varchar(10) default NULL,
  `date_validation_projet_vu_par` int(5) default NULL,
  `date_cao` varchar(10) default NULL,
  `avis_cao` int(5) default NULL,
  `date_cp` varchar(10) default NULL,
  `date_signature_avenant` varchar(10) default NULL,
  `date_reception_dossier` varchar(10) default NULL,
  `date_formulation_observation_par_sv_sur_sdossier` varchar(10) default NULL,
  `date_retour_dossier_finalise` varchar(10) default NULL,
  `date_validation_dossier_finalise` varchar(10) NOT NULL,
  `date_transmission_prefecture` varchar(10) default NULL,
  `date_notification` varchar(10) default NULL,
  `operation_validation_vues_par` int(5) default NULL,
  `commentaires` Text default NULL,
  PRIMARY KEY  (`id_avenant`),
  KEY `id_contrat` (`id_contrat`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


--
-- Contraintes pour la table `Avenant`
--
ALTER TABLE `Avenant`   ADD CONSTRAINT `Avenant_ibfk_1` FOREIGN KEY (`id_contrat`) REFERENCES `Contrat` (`id_contrat`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `decisionEnveloppe` ADD `numero_marche` VARCHAR( 30 ) NULL DEFAULT NULL;

CREATE TABLE `DecisionPassationConsultation` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`libelle` VARCHAR( 100 ) NULL
) TYPE = innodb;

INSERT INTO `DecisionPassationConsultation` (`id`, `libelle`) VALUES (1, 'Poursuite');
INSERT INTO `DecisionPassationConsultation` (`id`, `libelle`) VALUES (2, 'Infructueux');
INSERT INTO `DecisionPassationConsultation` (`id`, `libelle`) VALUES (3, 'Sans suite');

CREATE TABLE `DecisionPassationMarcheAVenir` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`libelle` VARCHAR( 100 ) NULL
) TYPE = innodb;

INSERT INTO `DecisionPassationMarcheAVenir` (`id`, `libelle`) VALUES (1, 'Attribué');
INSERT INTO `DecisionPassationMarcheAVenir` (`id`, `libelle`) VALUES (2, 'Infructueux');
INSERT INTO `DecisionPassationMarcheAVenir` (`id`, `libelle`) VALUES (3, 'Sans suite');

CREATE TABLE `TypeAvenant` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`libelle` VARCHAR( 100 ) NULL
) TYPE = innodb;

INSERT INTO `TypeAvenant` (`id`, `libelle`) VALUES (1, '1-Sans incidence financière');
INSERT INTO `TypeAvenant` (`id`, `libelle`) VALUES (2, '2-Avenant augmentant le montant initial');
INSERT INTO `TypeAvenant` (`id`, `libelle`) VALUES (3, '3-avenant diminuant le montant initial');

CREATE TABLE `AvisCao` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`libelle` VARCHAR( 100 ) NULL
) TYPE = innodb;

INSERT INTO `AvisCao` (`id`, `libelle`) VALUES (1, 'Avis favorable');
INSERT INTO `AvisCao` (`id`, `libelle`) VALUES (2, 'Avis défavorable');
INSERT INTO `AvisCao` (`id`, `libelle`) VALUES (3, 'Sans suite');

--
-- Création de la table Numerotation_ref_cons_auto
--

CREATE TABLE IF NOT EXISTS `Numerotation_ref_cons_auto` (
  `id_cons_auto` int(11) NOT NULL default '0',
  `annee` year(4) NOT NULL default '0000',
  PRIMARY KEY  (`annee`)
)TYPE = innodb;

ALTER TABLE `ModuleOrganisme` ADD `suivi_passation` ENUM( '0', '1' ) NOT NULL DEFAULT '1';

ALTER TABLE `consultation` ADD `consultation_migration` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `url_consultation_migration` VARCHAR( 100 ) NULL ;

ALTER TABLE `AnnonceMoniteur` ADD `commentaire` TEXT NULL ;
ALTER TABLE `AnnonceMoniteur` CHANGE `datepub` `datepub` VARCHAR( 10 ) NULL DEFAULT NULL ;

ALTER TABLE `Echange` ADD `id_type_message` INT(11) NOT NULL;

ALTER TABLE `Offre` CHANGE `xml_string` `xml_string` LONGTEXT NOT NULL;

ALTER TABLE `Mesure_avancement` ADD `xml` LONGTEXT NULL DEFAULT NULL ;
ALTER TABLE `Mesure_avancement` CHANGE `trimestre` `trimestre` CHAR( 2 ) NOT NULL ;
ALTER TABLE `AnnonceMoniteur` CHANGE `statut_boamp` `statut_xml` VARCHAR( 4 ) NOT NULL DEFAULT '0' ;

ALTER TABLE `Mesure_avancement` CHANGE `trimestre` `trimestre` CHAR( 2 ) NOT NULL ;
ALTER TABLE `TypeProcedure` ADD `tag_name_mesure_avancement` VARCHAR( 100 ) NOT NULL ;


CREATE TABLE `Mesure_Type_Procedure` (
  `id_tag_name` int(11) NOT NULL auto_increment,
  `libelle_tag_name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id_tag_name`)
) TYPE=InnoDB AUTO_INCREMENT=12 ;

-- 
-- Contenu de la table `Mesure_Type_Procedure`
-- 
INSERT INTO `Mesure_Type_Procedure` (`id_tag_name`, `libelle_tag_name`) VALUES 
(1, 'Appel d''offres ouvert'),
(2, 'Appel d''offres restreint'),
(3, 'Dialogue compétitif'),
(4, 'Marché à procédure adaptée'),
(5, 'Marché négocié avec publicité préalable et mise en concurrence'),
(6, 'Concours'),
(7, 'Procédure spécifique à certains marchés de défense nationale'),
(8, 'Accord cadre'),
(9, 'Système d''acquisition dynamique'),
(10, 'Marché subséquent à accord cadre'),
(11, 'Marché spécifique sur système acquisition dynamique');

UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '10' WHERE `TypeProcedure`.`id_type_procedure` =31 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '8' WHERE `TypeProcedure`.`id_type_procedure` =30 ;

UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '1' WHERE `TypeProcedure`.`id_type_procedure` =1 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '2' WHERE `TypeProcedure`.`id_type_procedure` =2 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '2' WHERE `TypeProcedure`.`id_type_procedure` =3 ;

UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '6' WHERE `TypeProcedure`.`id_type_procedure` =8 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '6' WHERE `TypeProcedure`.`id_type_procedure` =9 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '6' WHERE `TypeProcedure`.`id_type_procedure` =10 ;

UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '3' WHERE `TypeProcedure`.`id_type_procedure` =11 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '3' WHERE `TypeProcedure`.`id_type_procedure` =12 ;

UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '5' WHERE `TypeProcedure`.`id_type_procedure` =18 ;

UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '7' WHERE `TypeProcedure`.`id_type_procedure` =23 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '7' WHERE `TypeProcedure`.`id_type_procedure` =24 ;

UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '4' WHERE `TypeProcedure`.`id_type_procedure` =4 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '4' WHERE `TypeProcedure`.`id_type_procedure` =5 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '4' WHERE `TypeProcedure`.`id_type_procedure` =6 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '4' WHERE `TypeProcedure`.`id_type_procedure` =7 ;

UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '9' WHERE `TypeProcedure`.`id_type_procedure` =32 ;
UPDATE `TypeProcedure` SET `tag_name_mesure_avancement` = '11' WHERE `TypeProcedure`.`id_type_procedure` =33 ;

ALTER TABLE `MarchePublie` DROP FOREIGN KEY `MarchePublie_ibfk_2` ;

-- demande lié à la migration epm 

ALTER TABLE `consultation` CHANGE `code_procedure` `code_procedure` VARCHAR( 15 ) NULL DEFAULT NULL ;
ALTER TABLE `consultation` CHANGE `libelle_code_cpv_1` `libelle_code_cpv_1` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `consultation` CHANGE `libelle_code_cpv_2` `libelle_code_cpv_2` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `consultation` CHANGE `libelle_code_cpv_3` `libelle_code_cpv_3` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `consultation` CHANGE `libelle_code_cpv_4` `libelle_code_cpv_4` VARCHAR( 200 ) NULL DEFAULT NULL;

ALTER TABLE `CategorieLot` CHANGE `description` `description` VARCHAR( 1000 ) NOT NULL DEFAULT '';
ALTER TABLE `CategorieLot` CHANGE `libelle_code_cpv_1` `libelle_code_cpv_1` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `CategorieLot` CHANGE `libelle_code_cpv_2` `libelle_code_cpv_2` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `CategorieLot` CHANGE `libelle_code_cpv_3` `libelle_code_cpv_3` VARCHAR( 200 ) NULL DEFAULT NULL;
ALTER TABLE `CategorieLot` CHANGE `libelle_code_cpv_4` `libelle_code_cpv_4` VARCHAR( 200 ) NULL DEFAULT NULL;

ALTER TABLE `Service` ADD `id_initial` INT( 11 ) NOT NULL;

ALTER TABLE `Mesure_avancement` ADD `name_xml_genere` VARCHAR( 225 ) NULL ;

ALTER TABLE `AnnonceMoniteur` CHANGE `date_maj` `date_maj` VARCHAR( 20 ) NULL DEFAULT '0000-00-00 00:00:00';

-- KBE : 16/02/2009 : Correction ordre des champs pour qu'il corresponds au mappping propel
ALTER TABLE `Service` CHANGE `id_initial` `id_initial` INT( 11 ) NOT NULL DEFAULT '0' AFTER `pays`;
ALTER TABLE `Marche` DROP FOREIGN KEY `Marche_ibfk_3` ;


ALTER TABLE `AnnonceMoniteur` ADD FOREIGN KEY ( `id_form_xml` ) REFERENCES `ReferentielFormXml` (`id`);

ALTER TABLE `AnnonceMoniteur` ADD FOREIGN KEY ( `id_destination_form_xml` ) REFERENCES `ReferentielDestinationFormXml` ( `id` ) ;

ALTER TABLE `Avenant` CHANGE `pourcentage_augmentation_marche_initial` `pourcentage_augmentation_marche_initial` CHAR( 5 ) NULL DEFAULT NULL , CHANGE `pourcentage_augmentation_cumule` `pourcentage_augmentation_cumule` CHAR( 5 ) NULL DEFAULT NULL ;

-- LCH - 09/03/2009 - Tranches budgetaire
UPDATE `MarcheTrancheBudgetaire` SET `libelle` = '90 000 HT à 132 999,99 HT' WHERE `MarcheTrancheBudgetaire`.`id` =4 LIMIT 1 ;
UPDATE `MarcheTrancheBudgetaire` SET `libelle` = '133 000 HT à 205 999,99 HT' WHERE `MarcheTrancheBudgetaire`.`id` =5 LIMIT 1 ;
UPDATE `MarcheTrancheBudgetaire` SET `libelle` = '206 000 HT à 999 999,99 HT' WHERE `MarcheTrancheBudgetaire`.`id` =6 LIMIT 1 ;
UPDATE `MarcheTrancheBudgetaire` SET `libelle` = '3 000 000 HT à 5 149 999,99 HT' WHERE `MarcheTrancheBudgetaire`.`id` =8 LIMIT 1 ;
UPDATE `MarcheTrancheBudgetaire` SET `libelle` = '5 150 000 HT et plus.' WHERE `MarcheTrancheBudgetaire`.`id` =9 LIMIT 1 ;

UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '90 000 HT à 132 999,99 HT' WHERE `ValeurReferentiel`.`id` =15 LIMIT 1 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '133 000 HT à 205 999,99 HT' WHERE `ValeurReferentiel`.`id` =16 LIMIT 1 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '206 000 HT à 999 999,99 HT' WHERE `ValeurReferentiel`.`id` =17 LIMIT 1 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '3 000 000 HT à 5 149 999,99 HT' WHERE `ValeurReferentiel`.`id` =19 LIMIT 1 ;
INSERT INTO `ValeurReferentiel` VALUES (20,2,'5 150 000 HT et plus.');


-- LCH - 09/03/2009 - Correction bug procedure restreinte
UPDATE `ProcedureEquivalence` SET `procedure_restreinte` = '0' WHERE `ProcedureEquivalence`.`id_type_procedure` =2 LIMIT 1 ;

-- MWA - 10/03/2009 demande lié à la migration epm
ALTER TABLE `CategorieLot` ADD COLUMN `id_lot_externe` int(11) NULL DEFAULT NULL; 
ALTER TABLE `CategorieLot` CHANGE `description_detail` `description_detail` VARCHAR( 1000 ) NULL DEFAULT '';
ALTER TABLE `CategorieLot` CHANGE `description_detail_fr` `description_detail_fr` VARCHAR( 1000 ) NULL DEFAULT '';
ALTER TABLE `CategorieLot` CHANGE `description_fr` `description_fr` VARCHAR( 1000 ) NULL DEFAULT '';

ALTER TABLE `TypeProcedure` ADD `abreviation_interface` VARCHAR( 50 ) NULL ;

UPDATE `TypeProcedure` SET `abreviation_interface` = 'AOO' WHERE  `TypeProcedure`.`id_type_procedure` = 1;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'ARC' WHERE  `TypeProcedure`.`id_type_procedure` = 2;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'ARO' WHERE  `TypeProcedure`.`id_type_procedure` = 3;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'MAI' WHERE  `TypeProcedure`.`id_type_procedure` = 4;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'MAS' WHERE  `TypeProcedure`.`id_type_procedure` = 5;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'PSI' WHERE  `TypeProcedure`.`id_type_procedure` = 6;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'PSS' WHERE  `TypeProcedure`.`id_type_procedure` = 7;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'COO' WHERE  `TypeProcedure`.`id_type_procedure` = 8;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'CRC' WHERE  `TypeProcedure`.`id_type_procedure` = 9;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'CRO' WHERE  `TypeProcedure`.`id_type_procedure` = 10;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'DCC' WHERE  `TypeProcedure`.`id_type_procedure` = 11;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'DCO' WHERE  `TypeProcedure`.`id_type_procedure` = 12;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'CPC' WHERE  `TypeProcedure`.`id_type_procedure` = 13;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'CPD' WHERE  `TypeProcedure`.`id_type_procedure` = 14;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'CPO' WHERE  `TypeProcedure`.`id_type_procedure` = 16;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'ATR' WHERE  `TypeProcedure`.`id_type_procedure` = 17;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'MNE' WHERE  `TypeProcedure`.`id_type_procedure` = 18;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'PA3' WHERE  `TypeProcedure`.`id_type_procedure` = 22;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'MNC' WHERE  `TypeProcedure`.`id_type_procedure` = 23;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'MNO' WHERE  `TypeProcedure`.`id_type_procedure` = 24;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'ACA' WHERE  `TypeProcedure`.`id_type_procedure` = 30;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'ACS' WHERE  `TypeProcedure`.`id_type_procedure` = 31;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'SAA' WHERE  `TypeProcedure`.`id_type_procedure` = 32;
UPDATE `TypeProcedure` SET `abreviation_interface` = 'SAM' WHERE  `TypeProcedure`.`id_type_procedure` = 33;



-- 31/03/2009 MMI: Script lié au module Helios


CREATE TABLE `Helios_piece_publicite` (
  `id` int(11) NOT NULL auto_increment,
  `nom_fichier` varchar(100) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '1',
  `consultation_ref` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) TYPE=InnoDB;

-- --------------------------------------------------------

-- 
-- Structure de la table `Helios_pv_consultation`
-- 

CREATE TABLE `Helios_pv_consultation` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '0',
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) TYPE=InnoDB;

-- --------------------------------------------------------

-- 
-- Structure de la table `Helios_rapport_prefet`
-- 

CREATE TABLE `Helios_rapport_prefet` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '0',
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) TYPE=InnoDB;

-- --------------------------------------------------------

-- 
-- Structure de la table `Helios_tableau_ar`
-- 

CREATE TABLE `Helios_tableau_ar` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `description` mediumtext,
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) TYPE=InnoDB ;

-- --------------------------------------------------------

-- 
-- Structure de la table `Helios_teletransmission`
-- 

CREATE TABLE `Helios_teletransmission` (
  `id` int(11) NOT NULL auto_increment,
  `shown_id` varchar(20) NOT NULL default '',
  `id_agent_creation` int(11) NOT NULL default '0',
  `consultation_ref` int(11) default NULL,
  `publicite_cons` char(1) default NULL,
  `dce_items` varchar(255) default NULL,
  `ids_pvs_cao` varchar(255) default NULL,
  `ids_rapports_prefet` varchar(255) default NULL,
  `deliberation_mapa` char(1) default NULL,
  `tableau_ar` char(1) default NULL,
  `date_creation` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `date_envoi` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `id_agent_envoi` int(11) NOT NULL default '0',
  `nom_piecej1` varchar(250) NOT NULL default '',
  `piecej1` int(11) NOT NULL default '0',
  `horodatage_piecej1` longblob NOT NULL,
  `untrusteddate_piecej1` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille_piecej1` varchar(20) NOT NULL default '',
  `nom_piecej2` varchar(250) NOT NULL default '',
  `piecej2` int(11) NOT NULL default '0',
  `horodatage_piecej2` longblob NOT NULL,
  `untrusteddate_piecej2` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille_piecej2` varchar(20) NOT NULL default '',
  `piecej1_desc` mediumtext NOT NULL,
  `piecej2_desc` mediumtext NOT NULL,
  `fichier_xml` longblob NOT NULL,
  `nom_dossierzip` varchar(250) NOT NULL default '',
  `taille_dossierzip` varchar(250) NOT NULL default '',
  `objet_libre` varchar(250) NOT NULL default '',
  `commentaire_libre` varchar(250) NOT NULL default '',
  `sig_piece_principale` longblob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) TYPE=InnoDB  ;

-- --------------------------------------------------------

-- 
-- Structure de la table `Helios_teletransmission_lot`
-- 

CREATE TABLE `Helios_teletransmission_lot` (
  `id_teletransmission` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `numero_marche` varchar(250) NOT NULL default '',
  `ids_env_actes_engagements` varchar(255) NOT NULL default '',
  `ids_enveloppes_items` varchar(255) NOT NULL default '',
  `ids_docs_acceptes` varchar(255) NOT NULL default '',
  `ids_msg_notif` varchar(255) NOT NULL default '',
  `lot_in_transmission` char(1) NOT NULL default '',
  `statut_envoi` int(11) NOT NULL default '1',
  `id_zip_send` varchar(40) default NULL,
  `nom_lettre_commande` varchar(100) default NULL,
  `lettre_commande` int(20) default NULL,
  `horodatage` varchar(20) NOT NULL default '',
  `untrusteddate` varchar(20) NOT NULL default '',
  `taille` varchar(20) NOT NULL default '',
  `valider_comptable` enum('0','1') NOT NULL default '0',
  `signature_acte` varchar(100) default NULL,
  `horodatage_acte` longblob,
  `untrusteddate_acte` varchar(20) default NULL,
  `taille_acte` varchar(20) default '0',
  `id_Blob` int(20) default '0',
  PRIMARY KEY  (`id_teletransmission`,`lot`)
) TYPE=InnoDB;

-- 
-- Contraintes pour les tables exportées
-- 

-- 
-- Contraintes pour la table `Helios_piece_publicite`
-- 
ALTER TABLE `Helios_piece_publicite` ADD CONSTRAINT `Helios_piece_publicite_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Contraintes pour la table `Helios_pv_consultation`
-- 
ALTER TABLE `Helios_pv_consultation` ADD CONSTRAINT `Helios_pv_consultation_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Contraintes pour la table `Helios_rapport_prefet`
-- 
ALTER TABLE `Helios_rapport_prefet` ADD CONSTRAINT `Helios_rapport_prefet_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Contraintes pour la table `Helios_tableau_ar`
-- 
ALTER TABLE `Helios_tableau_ar` ADD CONSTRAINT `Helios_tableau_ar_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Contraintes pour la table `Helios_teletransmission`
-- 
ALTER TABLE `Helios_teletransmission` ADD CONSTRAINT `Helios_teletransmission_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Contraintes pour la table `Helios_teletransmission_lot`
-- 
ALTER TABLE `Helios_teletransmission_lot` ADD CONSTRAINT `Helios_teletransmission_lot_ibfk_1` FOREIGN KEY (`id_teletransmission`) REFERENCES `Helios_teletransmission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;



ALTER TABLE `Helios_teletransmission` CHANGE `publicite_cons` `publicite_cons` CHAR( 20 ) NULL DEFAULT NULL; 

-- MMI 07/04/2009 
ALTER TABLE `Helios_teletransmission_lot` DROP `signature_acte` ,
DROP `horodatage_acte` ,
DROP `untrusteddate_acte` ,
DROP `taille_acte` ,
DROP `id_Blob` ;


CREATE TABLE `Helios_cosignature` (
`id_fichier` INT( 20 ) NOT NULL ,
`signature_acte` VARCHAR( 20 ) NOT NULL ,
`horodatage_acte` LONGBLOB NOT NULL ,
`untrusteddate_acte` VARCHAR( 20 ) NOT NULL ,
`taille_acte` VARCHAR( 20 ) NOT NULL ,
`id_Blob` VARCHAR( 20 ) NOT NULL 
) TYPE = innodb;
ALTER TABLE `Helios_cosignature` ADD PRIMARY KEY ( `id_fichier` ) ;
ALTER TABLE `Helios_cosignature` ADD INDEX ( `id_fichier` ) ;


-- MLA 22/04/2009

CREATE TABLE `Centrale_publication` (
`id` INT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`nom` VARCHAR( 200 ) NULL  ,
`mail` VARCHAR( 200 ) NULL 
) TYPE = innodb;

CREATE TABLE `Journaux` (
`ID_JOURNAL` INT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ID_CENTRALE` INT( 20 ) NOT NULL default '0',
`NOM_JOURNAL` VARCHAR( 200 ) NULL 
) TYPE = innodb;
CREATE TABLE `compte_centrale` (
`ID_COMPTE` INT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ID_CENTRALE` INT( 20 ) NULL ,
`MAIL` VARCHAR( 200 ) NULL ,
`FAX` VARCHAR( 20 ) NULL ,
`INFO_CIMPLEMENTAIRE` TEXT NULL 
) TYPE = innodb;
ALTER TABLE `Journaux` ADD INDEX `ID_CENTRALE` ( `ID_CENTRALE` );
ALTER TABLE `compte_centrale` ADD INDEX `ID_CENTRALE` ( `ID_CENTRALE` ) ;

CREATE TABLE `destinataire_centrale_pub` (
`id` INT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_annonce_jal` INT( 20 ) NULL ,
`id_compte` INT( 20 ) NULL ,
`ids_journaux` VARCHAR( 200 ) NULL ,
`statut` CHAR( 1 ) NULL ,
`date_envoi` VARCHAR( 20 ) NULL DEFAULT '0000-00-00 00:00:00',
`date_pub` VARCHAR( 20 ) NULL DEFAULT '0000-00-00 00:00:00',
`accuse` VARCHAR( 20 ) NULL
) TYPE = innodb;

ALTER TABLE `compte_centrale` ADD INDEX  `ID_CENTRALE` ( `ID_CENTRALE` ) ;
ALTER TABLE `Centrale_publication` ADD INDEX `id` ( `id` ) ;
ALTER TABLE `destinataire_centrale_pub` ADD INDEX `id_annonce_jal` ( `id_annonce_jal` ) ;
ALTER TABLE `destinataire_centrale_pub` ADD INDEX `id_compte` ( `id_compte` ) ;

ALTER TABLE `Centrale_publication` ADD `fax` VARCHAR( 50 ) NULL ,
ADD `information` TEXT NULL ;
ALTER TABLE `compte_centrale` ADD `ID_SERVICE` INT( 11 ) NULL DEFAULT '0';

INSERT INTO `Centrale_publication` ( `id` , `nom` , `mail` ) 
VALUES (
'1' , 'MEDIALEX', 'technique@atexo.com'
);
INSERT INTO `Journaux` ( `ID_JOURNAL` , `ID_CENTRALE` , `NOM_JOURNAL` ) 
VALUES ('1' , '1', 'Ouest France'),('2' , '1', 'Le maine'),('3' , '1', 'Presse Océan'),('4' , '1', 'La Presse de la Manche');

ALTER TABLE `Service` ADD `date_creation` VARCHAR( 20 ) NULL ;
ALTER TABLE `Service` ADD `date_modification` VARCHAR( 20 ) NULL ;

ALTER TABLE `MarchePublie` ADD `newVersion` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `Offre` ADD `nom_entreprise` varchar(30) NULL ;
ALTER TABLE `Offre` ADD `nom_inscrit` varchar(80) default NULL;
ALTER TABLE `Offre` ADD `prenom_inscrit` varchar(80) default NULL;
ALTER TABLE `Offre` ADD `adresse_inscrit` varchar(100) default NULL;
ALTER TABLE `Offre` ADD `adresse2_inscrit` varchar(100) default NULL;
ALTER TABLE `Offre` ADD `telephone_inscrit` varchar(20) default NULL;
ALTER TABLE `Offre` ADD `fax_inscrit` varchar(30) default NULL;
ALTER TABLE `Offre` ADD `code_postal_inscrit` varchar(5) default NULL;
ALTER TABLE `Offre` ADD `ville_inscrit` varchar(50) default NULL;
ALTER TABLE `Offre` ADD `pays_inscrit` varchar(50) default NULL;
ALTER TABLE `Offre` ADD `acronyme_pays` varchar(10) default NULL;
ALTER TABLE `Offre` ADD `siret_entreprise` varchar(14) default NULL;
ALTER TABLE `Offre` ADD `identifiant_national` varchar(20) default NULL;
ALTER TABLE `Offre` ADD `email_inscrit` varchar(100) default NULL;
ALTER TABLE `Offre` ADD `siret_inscrit` varchar(14) default NULL;

ALTER TABLE `Offre` CHANGE `nom_entreprise` `nom_entreprise_inscrit` VARCHAR( 30 ) NULL DEFAULT NULL;
ALTER TABLE `Offre` ADD `nom_entreprise` varchar(30) NULL AFTER `siret_inscrit` ;;
-- NBE 26/05/2009
ALTER TABLE `RPA` ADD `date_creation` VARCHAR( 20 ) NULL ;
ALTER TABLE `RPA` ADD `date_modification` VARCHAR( 20 ) NULL ;

-- MWA 27/05/2009
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.8 JOUE_14T - Formulaire rectificatif / annulation (envoyé uniquement au BOAMP et non au JOUE)' WHERE `ReferentielTypeXml`.`id` =8;
-- MLA 02/06/2009
ALTER TABLE `RPA` ADD `responsable_archive` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
-- MWA 04/06/2009
-- mis en comment par LCH le 21/09/09 car on en a plus besoin et ça bloque Ville de Paris qui en a besoin pour EPM
--UPDATE ProcedureEquivalence SET `env_candidature` = '0' WHERE `ProcedureEquivalence`.`id_type_procedure` =1 AND `ProcedureEquivalence`.`env_candidature` like '%1%';

ALTER TABLE `Service` CHANGE `service` `service` TEXT NOT NULL default '';
ALTER TABLE `Service` CHANGE `libelle` `libelle` TEXT NOT NULL default '';
ALTER TABLE `Service` CHANGE `sigle` `sigle` TEXT NOT NULL default '';

ALTER TABLE `decisionEnveloppe` ADD `statutEJ` VARCHAR( 100 ) NULL ;


CREATE TABLE `Chorus_echange` (
  `id` int(20) NOT NULL auto_increment,
  `id_decision` int(20) default NULL,
  `num_ordre` varchar(20) default NULL,
  `nom_createur` varchar(200) default NULL,
  `prenom_createur` varchar(200) default NULL,
  `id_createur` int(20) default NULL,
  `statutEchange` varchar(20) default NULL,
  `date_creation` varchar(20) default NULL,
  `date_envoi` varchar(20) default NULL,
  `retour_chorus` varchar(20) default NULL,
  `id_oa` int(20) default NULL,
  `id_ga` int(20) default NULL,
  `id_type_marche` int(20) default NULL,
  `dce_items` varchar(200) default NULL,
  `ids_env_ae` varchar(200) default NULL,
  `ids_env_items` varchar(200) default NULL,
  `ids_pieces_externes` varchar(200) default NULL,
  `id_agent_envoi` int(200) default NULL,
  `nom_agent` varchar(200) default NULL,
  `prenom_agent` varchar(200) default NULL,
  `signACE` enum('0','1') NOT NULL default '0',
  `siren` varchar(9) default NULL,
  `siret` varchar(5) default NULL,
  `date_notification` varchar(20) default NULL,
  `date_fin_marche` varchar(20) default NULL,
  `id_acte_juridique` char(1) default NULL,
  `cpv_1` varchar(20) default NULL,
  `cpv_2` varchar(20) default NULL,
  `cpv_3` varchar(20) default NULL,
  `cpv_4` varchar(20) default NULL,
  `id_type_procedure` int(20) default NULL,
  `id_forme_prix` char(1) default NULL,
  `Nbr_entreprises_cotraitantes` varchar(20) default NULL,
  `sous_traitance_declaree` char(1) default NULL,
  `carte_achat` char(1) default NULL,
  `clause_sociale` char(1) default NULL,
  `clause_environnementale` char(1) default NULL,
  `Nbr_proposition_recues` varchar(20) default NULL,
  `Nbr_proposition_dematerialisees` varchar(20) default NULL,
  `nom_fichier` VARCHAR( 200 )default NULL,
  `erreur_rejet` TEXT default NULL,
  PRIMARY KEY  (`id`),
  KEY `id_decision` (`id_decision`),
  KEY `id_decision_2` (`id_decision`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;


-- 
-- Contraintes pour les tables exportées
-- 

-- 
-- Contraintes pour la table `Chorus_echange`
-- 
ALTER TABLE `Chorus_echange`
  ADD CONSTRAINT `Chorus_echange_ibfk_1` FOREIGN KEY (`id_decision`) REFERENCES `decisionEnveloppe` (`id_decision_enveloppe`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Structure de la table `Chorus_pj`
-- 

CREATE TABLE `Chorus_pj` (
  `id` int(20) NOT NULL auto_increment,
  `id_echange` int(20) NOT NULL default '0',
  `nom_fichier` varchar(50) NOT NULL default '',
  `fichier` varchar(20) NOT NULL default '',
  `horodatage` longblob NOT NULL default '',
  `untrusteddate` varchar(20) NOT NULL default '',
  `taille` int(20) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id_echange` (`id_echange`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

-- 
-- Contraintes pour les tables exportées
-- 

-- 
-- Contraintes pour la table `Chorus_pj`
-- 
ALTER TABLE `Chorus_pj`
  ADD CONSTRAINT `Chorus_pj_ibfk_1` FOREIGN KEY (`id_echange`) REFERENCES `Chorus_echange` (`id`);


-- MLA 25/08/2009
-- 
-- Table structure for table `Groupe_Moniteur`
-- 
CREATE TABLE `Groupe_Moniteur` (
  `id` int(11) NOT NULL auto_increment,
  `id_service` int(11) NOT NULL default '0',
  `Identifiant` varchar(50) NOT NULL default '',
  `Mdp` varchar(50) NOT NULL default '',
  `Num_Abonnement` varchar(50) default NULL,
  `Num_Abonne` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB ;


-- MLA 24/07/2009
ALTER TABLE `Service` ADD `siren` VARCHAR( 9 ) NULL DEFAULT NULL ,
ADD `complement` VARCHAR( 5 ) NULL DEFAULT NULL ;

CREATE TABLE `Chorus_type_procedure` (
`id` INT( 50 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`libelle` VARCHAR( 200 ) NOT NULL default '',
`code` VARCHAR( 50 ) NOT NULL default ''
) TYPE = innodb;

INSERT INTO `Chorus_type_procedure` (`id`, `libelle`, `code`) VALUES 
(1, 'Appel d\'offres ouvert', '001'),
(2, 'Appel d\'offres restreint', '002'),
(3, 'Procédure négociée après publicité préalable et mise en concurrence', '003'),
(4, 'Procédure négociée sans publicité préalable et sans mise en concurrence', '004'),
(5, 'Procédure spécifique à certains marchés de la défense', '005'),
(6, 'Dialogue compétitif', '006'),
(7, 'Concours', '007'),
(8, 'Système d''acquisition dynamique', '008'),
(9, 'Procédure adaptée ("MAPA")', '009'),
(10, 'Autre', '010');

-- 
-- Structure de la table `Chorus_acte_juridique`
-- 

CREATE TABLE `Chorus_acte_juridique` (
  `id` int(10) NOT NULL default '0',
  `libelle` varchar(200) NOT NULL default '',
  `code` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

-- 
-- Contenu de la table `Chorus_acte_juridique`
-- 

INSERT INTO `Chorus_acte_juridique` (`id`, `libelle`, `code`) VALUES 
(1, '1 - Contrat Initial', '001'),
(2, '2 - Contrat Complémentaire', '002'),
(3, '3 - Contrat sur la Base d''un Accord Cadre', '003'),
(4, '4 - Marché de Définition', '004'),
(5, '5 - Autre', '005');

-- Structure de la table `Chorus_forme_prix`
-- 

CREATE TABLE `Chorus_forme_prix` (
  `id` int(20) NOT NULL default '0',
  `libelle` varchar(200) NOT NULL default '',
  `code` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

-- 
-- Contenu de la table `Chorus_forme_prix`
-- 

INSERT INTO `Chorus_forme_prix` (`id`, `libelle`, `code`) VALUES 
(1, '1 - Prix Fermes', '001'),
(2, '2 - Prix Fermes Actualisables', '002'),
(3, '3 - Prix Révisables', '003');

CREATE TABLE `Chorus_organisation_achat` (
  `id` int(20) NOT NULL auto_increment,
  `libelle` varchar(250) NOT NULL default '',
  `code` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB ;

-- 
-- Contenu de la table `Chorus_organisation_achat`
-- 

INSERT INTO `Chorus_organisation_achat` (`id`, `libelle`, `code`) VALUES 
(1, 'OA Justice', 'C001'),
(2, 'OA 2', 'C002'),
(3, 'OA 3', 'C003'),
(4, 'OA 4', 'C004');
-- 
-- Structure de la table `Chorus_groupement_achat`
-- 

CREATE TABLE `Chorus_groupement_achat` (
  `id` int(20) NOT NULL auto_increment,
  `id_oa` int(20) NOT NULL default '0',
  `libelle` varchar(250) NOT NULL default '',
  `code` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id_oa` (`id_oa`)
) TYPE=InnoDB ;

-- 
-- Contenu de la table `Chorus_groupement_achat`
-- 

INSERT INTO `Chorus_groupement_achat` (`id`, `id_oa`, `libelle`, `code`) VALUES 
(1, 1, 'GA 01D', '01D'),
(2, 1, 'GA 2', 'GA2'),
(3, 2, 'GA 3', 'GA3'),
(4, 2, 'GA 4', 'GA4'),
(5, 3, 'GA 5', 'GA5'),
(6, 3, 'GA 6', 'GA6');
-- 
-- Structure de la table `Chorus_type_marche`
-- 

CREATE TABLE `Chorus_type_marche` (
  `id` int(20) NOT NULL auto_increment,
  `libelle` varchar(250) NOT NULL default '',
  `code` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB ;

-- 
-- Contenu de la table `Chorus_type_marche`
-- 

INSERT INTO `Chorus_type_marche` (`id`, `libelle`, `code`) VALUES 
(1, 'Subventions', 'ZSUB'),
(2, 'Marché à BDC', 'ZMBC'),
(3, 'MAPA à BDC', 'ZMPC'),
(4, 'MAPA à tranches', 'ZMPT'),
(5, 'MAPA unique', 'ZMPU'),
(6, 'MAPA mixte', 'ZMPX'),
(7, 'Marché à tranches', 'ZMT'),
(8, 'Marché unique', 'ZMU'),
(9, 'Marché mixte', 'ZMX');


-- 24/09/2009

ALTER TABLE `Chorus_echange` ADD `code_cpv_libelle1` VARCHAR( 250 ) NULL ;
ALTER TABLE `Chorus_echange` ADD `code_cpv_libelle2` VARCHAR( 250 ) NULL ;
ALTER TABLE `Chorus_echange` ADD `code_cpv_libelle3` VARCHAR( 250 ) NULL ;
ALTER TABLE `Chorus_echange` ADD `code_cpv_libelle4` VARCHAR( 250 ) NULL ;

-- MWA : 29/09/2009 passage en production BOAMP 1.9

INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (9, '2#3', 'XML 1.9 JOUE_12 - Avis Concours');
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (10, '2#3', 'XML 1.9 JOUE_14 - Formulaire rectificatif / annulation');
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (11, '2#3', 'XML 1.9 JOUE_05 - Secteurs Spéciaux');

UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.9 BOAMP3 - Avis de marché National'  WHERE `ReferentielTypeXml`.`id` =1 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.9 BOAMP6 - Avis de MAPA (uniquement si inférieur à 90 000 EUR) '  WHERE `ReferentielTypeXml`.`id` =2 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.9 JOUE_02 - Avis de marché'  WHERE `ReferentielTypeXml`.`id` =3 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.9 JOUE_03 - Avis d\'attribution'  WHERE `ReferentielTypeXml`.`id` =4 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.9 BOAMP4 - Avis d\'attribution'  WHERE `ReferentielTypeXml`.`id` =5 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.9 BOAMP2 - Avis rectificatif'  WHERE `ReferentielTypeXml`.`id` =6 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.9 BOAMP7 - Résultat de MAPA'  WHERE `ReferentielTypeXml`.`id` =7 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '' WHERE `ReferentielTypeXml`.`id` =8 LIMIT 1 ;

ALTER TABLE `ProcedureEquivalence` ADD `env_offre_technique` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD `env_offre_technique_type_unique` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `ProcedureEquivalence` ADD `env_offre_technique_type_multiple` CHAR( 2 ) NOT NULL DEFAULT '-0';
ALTER TABLE `consultation` ADD `env_offre_technique` INT( 3 ) NOT NULL default '0';

ALTER TABLE `consultation` ADD `titre_ar` LONGTEXT NULL AFTER `titre_cz` ;
ALTER TABLE `consultation` ADD `resume_ar` LONGTEXT NULL AFTER `resume_cz` ;
ALTER TABLE `consultation` ADD `accessibilite_ar` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `accessibilite_su` ;

ALTER TABLE `consultation` ADD `domaines_activites` VARCHAR( 250 )  NULL DEFAULT '';

-- MWA : 03/11/2009 forcer la MAJ des formulaires BOAMP 1.9
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.9 JOUE_12 - Avis Concours'  WHERE `ReferentielTypeXml`.`id` =9 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.9 JOUE_05 - Secteurs Spéciaux'  WHERE `ReferentielTypeXml`.`id` =11 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '2#3' WHERE `ReferentielTypeXml`.`id` =9 LIMIT 1 ;
UPDATE `ReferentielTypeXml` SET `id_destinataire` = '2#3' WHERE `ReferentielTypeXml`.`id` =11 LIMIT 1 ;

-- AEL     : 04/11/2009 2ème et 3ème enveloppe TGR
ALTER TABLE `Admissibilite_Enveloppe_Lot` ADD `type_enveloppe` INT( 1 ) NULL DEFAULT '2';
ALTER TABLE `Admissibilite_Enveloppe_papier_Lot` ADD `type_enveloppe` INT( 1 ) NULL DEFAULT '2';

ALTER TABLE Admissibilite_Enveloppe_Lot DROP PRIMARY KEY;
ALTER TABLE Admissibilite_Enveloppe_papier_Lot DROP PRIMARY KEY;

ALTER TABLE `Admissibilite_Enveloppe_Lot` ADD PRIMARY KEY ( `id_offre` , `sous_pli` , `type_enveloppe` ) ;
ALTER TABLE `Admissibilite_Enveloppe_papier_Lot` ADD PRIMARY KEY ( `id_offre_papier` , `sous_pli` , `type_enveloppe` ) ;

ALTER TABLE `Offre` ADD `horodatage_annulation` LONGBLOB NULL ;
ALTER TABLE `Offre` ADD `date_annulation` VARCHAR( 20 ) NULL ;
ALTER TABLE `Offre` ADD `signature_annulation` TEXT NULL ;
ALTER TABLE `Offre` ADD `depot_annule` ENUM( '0', '1' ) NULL DEFAULT '0';
ALTER TABLE `Offre` ADD `string_annulation` TEXT NULL ;
ALTER TABLE `Offre` DROP `verification_signature_annulation`;
ALTER TABLE `Offre` ADD `verification_certificat_annulation` VARCHAR( 5 ) NULL ;

ALTER TABLE `Offre_papier` ADD `date_annulation` VARCHAR( 20 ) NULL ;
ALTER TABLE `Offre_papier` ADD `depot_annule` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

-- 19/11/2009 MMI

ALTER TABLE `decisionEnveloppe` ADD `note` VARCHAR( 200 ) NOT NULL default '';
ALTER TABLE `decisionEnveloppe` ADD `classement` VARCHAR( 200 ) NOT NULL default '';

-- OKO : le 09-11-2009 (Traduction)
ALTER TABLE `Service` ADD `libelle_ar` TEXT NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_ar` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_suite_ar` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `ville_ar` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `pays_ar` VARCHAR( 150 ) NOT NULL DEFAULT 'NULL';

ALTER TABLE `Service` ADD `libelle_fr` TEXT NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_fr` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_suite_fr` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `ville_fr` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `pays_fr` VARCHAR( 150 ) NOT NULL DEFAULT 'NULL';

ALTER TABLE `Service` ADD `libelle_es` TEXT NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_es` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_suite_es` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `ville_es` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `pays_es` VARCHAR( 150 ) NOT NULL DEFAULT 'NULL';

ALTER TABLE `Service` ADD `libelle_en` TEXT NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_en` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_suite_en` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `ville_en` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `pays_en` VARCHAR( 150 ) NOT NULL DEFAULT 'NULL';

ALTER TABLE `Service` ADD `libelle_su` TEXT NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_su` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_suite_su` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `ville_su` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `pays_su` VARCHAR( 150 ) NOT NULL DEFAULT 'NULL';

ALTER TABLE `Service` ADD `libelle_du` TEXT NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_du` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_suite_du` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `ville_du` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `pays_du` VARCHAR( 150 ) NOT NULL DEFAULT 'NULL';

ALTER TABLE `Service` ADD `libelle_cz` TEXT NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_cz` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `adresse_suite_cz` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `ville_cz` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Service` ADD `pays_cz` VARCHAR( 150 ) NOT NULL DEFAULT 'NULL' default '';
-- MLA : 12/11/2009
ALTER TABLE `Service` ADD `path_service` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `pays_cz` ;

-- OKO : Gestion de la traduction de la consultation en langue arabe (le 09-11-2009)
ALTER TABLE `consultation` ADD `titre_ar` LONGTEXT NULL AFTER `titre_cz` default '';
ALTER TABLE `consultation` ADD `resume_ar` LONGTEXT NULL AFTER `resume_cz` default '';
ALTER TABLE `consultation` ADD `accessibilite_ar` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `accessibilite_su` ;
ALTER TABLE `CategorieConsultation` ADD `libelle_ar` VARCHAR( 100 ) NULL AFTER `libelle_cz` ;
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_ar` VARCHAR( 100 ) NULL AFTER `libelle_type_procedure_cz` ;

ALTER TABLE `CategorieLot` ADD `description_detail_ar` LONGTEXT NULL AFTER `description_detail_cz` ;
ALTER TABLE `CategorieLot` ADD `description_ar` LONGTEXT NULL AFTER `description_cz` ;

-- MMI : 03/12/2009
CREATE TABLE IF NOT EXISTS `fichiers_liste_marches` (
  `id` int(20) NOT NULL auto_increment,
  `nom_fichier` varchar(50) NOT NULL default '',
  `fichier` varchar(20) NOT NULL default '',
  `horodatage` longblob NOT NULL default '',
  `untrusteddate` varchar(20) NOT NULL default '',
  `taille` int(20) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;
-- MMI : 08/12/2009
ALTER TABLE `Helios_teletransmission` ADD `ref_utilisateur` VARCHAR( 200 ) NOT NULL default '';
-- OKO: le 05-01-2010
ALTER TABLE `ModuleOrganisme` ADD `desarchivage_consultation` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la fonction de desarchivage d''une consultation';
-- -- MLA :08/12/2009  ajout des champs path_service pour les autres langues
ALTER TABLE `Service` ADD `path_service_ar` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `path_service`;
ALTER TABLE `Service` ADD `path_service_fr` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `path_service_ar`;
ALTER TABLE `Service` ADD  `path_service_es` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `path_service_fr`;
ALTER TABLE `Service` ADD  `path_service_en` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `path_service_es`;
ALTER TABLE `Service` ADD  `path_service_su` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `path_service_en`;
ALTER TABLE `Service` ADD  `path_service_du` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `path_service_su`;
ALTER TABLE `Service` ADD  `path_service_cz` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `path_service_du`; 

-- MLA 15/12/2009
ALTER TABLE `AnnonceJAL` ADD `option_envoi` INT( 11 ) NULL  ;
-- MLA 22/12/2009
ALTER TABLE `DestinataireAnnonceJAL` ADD `id_echange` INT( 11 ) NULL DEFAULT NULL AFTER `accuse`;;
ALTER TABLE `destinataire_centrale_pub` ADD `id_echange` INT( 11 ) NULL DEFAULT NULL AFTER `accuse`;
ALTER TABLE `DestinataireAnnonceJAL` ADD `date_ar` VARCHAR( 25 ) NULL DEFAULT NULL AFTER `id_echange`; 
ALTER TABLE `destinataire_centrale_pub` ADD `date_ar` VARCHAR( 25 ) NULL DEFAULT NULL AFTER `id_echange`; 
--AEL 21/01/2010
ALTER TABLE `consultation` ADD `id_affaire` INT( 11 ) NULL ;
ALTER TABLE `RPA` ADD `email` VARCHAR( 50 ) NULL ;
ALTER TABLE `RPA` ADD `telephone` VARCHAR( 50 ) NULL ;
ALTER TABLE `RPA` ADD `fax` VARCHAR( 50 ) NULL ;
-- MWA 03/02/2010 (activation article 133 2009 et modification des mauvaises années)
UPDATE `MarcheAnnee` SET `afficher`='1' WHERE `numero`='2009';
UPDATE `MarcheAnnee` SET `numero`='2010' WHERE `numero`='20010';
UPDATE `MarcheAnnee` SET `numero`='2011' WHERE `numero`='20011';
UPDATE `MarcheAnnee` SET `numero`='2012' WHERE `numero`='20012';
UPDATE `MarcheAnnee` SET `numero`='2013' WHERE `numero`='20013';
UPDATE `MarcheAnnee` SET `numero`='2014' WHERE `numero`='20014';
UPDATE `MarcheAnnee` SET `numero`='2015' WHERE `numero`='20015';
-- MWA 03/02/2010 (modification structure table pour accepter NULL)
ALTER TABLE `MarchePublie` CHANGE `idService` `idService` INT( 11 ) NULL DEFAULT '0';
ALTER TABLE `RPA` ADD `fax` VARCHAR( 50 ) NULL ;
-- OKO: le 08/02/2010
ALTER TABLE `passation_marche_a_venir` CHANGE `montant_estime` `montant_estime` VARCHAR( 11 ) NOT NULL DEFAULT '0';
ALTER TABLE `passation_marche_a_venir` CHANGE `min_bon_commande` `min_bon_commande` VARCHAR( 11 ) NOT NULL DEFAULT '0';
ALTER TABLE `passation_marche_a_venir` CHANGE `max_bon_commande` `max_bon_commande` VARCHAR( 11 ) NOT NULL DEFAULT '0';
-- MWA : le 11/02/2010
 ALTER TABLE `Marche` CHANGE `montantMarche` `montantMarche` VARCHAR( 50 ) NULL DEFAULT '0';
-- MWA : le 15/01/2010 Rajout formulaire FR13
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (12, '2', 'XML 1.9 JOUE_13 - Résultat Concours');
-- MWA : le 15/01/2010 Modification libellé 14T
UPDATE `ReferentielTypeXml` SET `libelle_type` = 'XML 1.8 JOUE_14T - Formulaire déprécié (à ne plus utiliser)'  WHERE `ReferentielTypeXml`.`id` =8;

--OKO: le 18/02/2010
ALTER TABLE `decisionEnveloppe` CHANGE `date_notification` `date_notification` VARCHAR( 10 ) NULL DEFAULT NULL ;

-- MMI : 22/002/2010

-- MMI 26/01/2010
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers` VARCHAR( 255 ) NOT NULL default '' AFTER `id_affaire`;
ALTER TABLE `consultation` ADD `caution_provisoire` VARCHAR( 255 ) NOT NULL default '' AFTER `adresse_retrais_dossiers`;
ALTER TABLE `consultation` ADD `adresse_depot_offres` VARCHAR( 255 ) NOT NULL default '' AFTER `caution_provisoire`;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis` VARCHAR( 255 ) NOT NULL default '' AFTER `adresse_depot_offres`;
ALTER TABLE `consultation` ADD `prix_aquisition_plans` VARCHAR( 255 ) NOT NULL default '' AFTER `lieu_ouverture_plis`;
-- MMI 04/02/10
ALTER TABLE `consultation` ADD `qualification` VARCHAR( 250 ) NOT NULL default '' AFTER `prix_aquisition_plans`;
ALTER TABLE `consultation` ADD `agrements` VARCHAR( 250 ) NOT NULL default '' AFTER `qualification`;
ALTER TABLE `consultation` ADD `add_echantillion` VARCHAR( 250 ) NOT NULL default '' AFTER `agrements`;
ALTER TABLE `consultation` ADD `date_limite_echantillion` VARCHAR( 50 ) NOT NULL default '' AFTER `add_echantillion`;
ALTER TABLE `consultation` ADD `add_reunion` VARCHAR( 250 ) NOT NULL default '' AFTER `date_limite_echantillion`;
ALTER TABLE `consultation` ADD `date_reunion` VARCHAR( 50 ) NOT NULL default '' AFTER `add_reunion`;
ALTER TABLE `consultation` ADD `variantes` CHAR( 1 ) NOT NULL default '' AFTER `date_reunion`;

ALTER TABLE `consultation` ADD `adresse_depot_offres_ar` VARCHAR( 255 ) NOT NULL default '' AFTER `variantes`;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_ar` VARCHAR( 255 ) NOT NULL default '' AFTER `adresse_depot_offres_ar`;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_ar` VARCHAR( 255 ) NOT NULL default '' AFTER `lieu_ouverture_plis_ar`;
ALTER TABLE `consultation` ADD `pieces_dossier_admin` VARCHAR( 255 ) NOT NULL default '' AFTER `adresse_retrais_dossiers_ar`;
ALTER TABLE `consultation` ADD `pieces_dossier_tech` VARCHAR( 255 ) NOT NULL default '' AFTER `pieces_dossier_admin`;
ALTER TABLE `consultation` ADD `pieces_dossier_additif` VARCHAR( 255 ) NOT NULL default '' AFTER `pieces_dossier_tech`;

ALTER TABLE `consultation` ADD `detail_consultation_fr` text AFTER `pieces_dossier_additif`;
ALTER TABLE `consultation` ADD `detail_consultation_en` text AFTER `detail_consultation_fr`;
ALTER TABLE `consultation` ADD `detail_consultation_es` text AFTER `detail_consultation_en`;
ALTER TABLE `consultation` ADD `detail_consultation_su` text AFTER `detail_consultation_es`;
ALTER TABLE `consultation` ADD `detail_consultation_du` text AFTER `detail_consultation_su`;
ALTER TABLE `consultation` ADD `detail_consultation_cz` text AFTER `detail_consultation_du`;
ALTER TABLE `consultation` ADD `detail_consultation_ar` text AFTER `detail_consultation_cz`;

ALTER TABLE `consultation` ADD `echantillon` ENUM( '0', '1' ) NOT NULL default '0' AFTER `detail_consultation_ar`;
ALTER TABLE `consultation` ADD `reunion` ENUM( '0', '1' ) NOT NULL default '0' AFTER `echantillon`;
ALTER TABLE `consultation` ADD `visites_lieux` ENUM( '0', '1' ) NOT NULL default '0' AFTER `reunion`;

ALTER TABLE `consultation` ADD `variante_calcule` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `visites_lieux`;

ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_fr` VARCHAR( 255 ) NULL AFTER `variante_calcule` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_en` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_fr` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_es` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_en` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_su` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_es` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_du` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_su` ;
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_cz` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_du` ;

ALTER TABLE `consultation` ADD `adresse_depot_offres_fr` VARCHAR( 255 ) NULL AFTER `adresse_retrais_dossiers_cz` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_en` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_fr` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_es` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_en` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_su` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_es` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_du` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_su` ;
ALTER TABLE `consultation` ADD `adresse_depot_offres_cz` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_du` ;


ALTER TABLE `consultation` ADD `lieu_ouverture_plis_fr` VARCHAR( 255 ) NULL AFTER `adresse_depot_offres_cz` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_en` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_fr` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_es` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_en` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_su` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_es` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_du` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_su` ;
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_cz` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_du` ;

ALTER TABLE `consultation` ADD `add_echantillion_fr` VARCHAR( 255 ) NULL AFTER `lieu_ouverture_plis_cz` ;
ALTER TABLE `consultation` ADD `add_echantillion_en` VARCHAR( 255 ) NULL AFTER `add_echantillion_fr` ;
ALTER TABLE `consultation` ADD `add_echantillion_es` VARCHAR( 255 ) NULL AFTER `add_echantillion_en` ;
ALTER TABLE `consultation` ADD `add_echantillion_su` VARCHAR( 255 ) NULL AFTER `add_echantillion_es` ;
ALTER TABLE `consultation` ADD `add_echantillion_du` VARCHAR( 255 ) NULL AFTER `add_echantillion_su` ;
ALTER TABLE `consultation` ADD `add_echantillion_cz` VARCHAR( 255 ) NULL AFTER `add_echantillion_du` ;
ALTER TABLE `consultation` ADD `add_echantillion_ar` VARCHAR( 255 ) NULL AFTER `add_echantillion_cz` ;


ALTER TABLE `consultation` ADD `add_reunion_fr` VARCHAR( 255 ) NULL AFTER `add_echantillion_ar` ;
ALTER TABLE `consultation` ADD `add_reunion_en` VARCHAR( 255 ) NULL AFTER `add_reunion_fr` ;
ALTER TABLE `consultation` ADD `add_reunion_es` VARCHAR( 255 ) NULL AFTER `add_reunion_en` ;
ALTER TABLE `consultation` ADD `add_reunion_su` VARCHAR( 255 ) NULL AFTER `add_reunion_es` ;
ALTER TABLE `consultation` ADD `add_reunion_du` VARCHAR( 255 ) NULL AFTER `add_reunion_su` ;
ALTER TABLE `consultation` ADD `add_reunion_cz` VARCHAR( 255 ) NULL AFTER `add_reunion_du` ;
ALTER TABLE `consultation` ADD `add_reunion_ar` VARCHAR( 255 ) NULL AFTER `add_reunion_cz` ;


ALTER TABLE `CategorieLot` ADD `caution_provisoire` VARCHAR( 255 ) NOT NULL default '' AFTER `id_lot_externe`;
ALTER TABLE `CategorieLot` ADD `qualification` VARCHAR( 250 ) NOT NULL default '' AFTER `caution_provisoire`;
ALTER TABLE `CategorieLot` ADD `agrements` VARCHAR( 250 ) NOT NULL default '' AFTER `qualification`;
ALTER TABLE `CategorieLot` ADD `add_echantillion` VARCHAR( 250 ) NOT NULL default '' AFTER `agrements`;
ALTER TABLE `CategorieLot` ADD `date_limite_echantillion` VARCHAR( 50 ) NOT NULL default '' AFTER `add_echantillion`;
ALTER TABLE `CategorieLot` ADD `add_reunion` VARCHAR( 250 ) NOT NULL default '' AFTER `date_limite_echantillion`;
ALTER TABLE `CategorieLot` ADD `date_reunion` VARCHAR( 50 ) NOT NULL default '' AFTER `add_reunion`;
ALTER TABLE `CategorieLot` ADD `variantes` CHAR( 1 ) NOT NULL default '' AFTER `date_reunion`;
ALTER TABLE `CategorieLot` ADD `echantillon` ENUM( '0', '1' ) NOT NULL default '0' AFTER `variantes`;
ALTER TABLE `CategorieLot` ADD `reunion` ENUM( '0', '1' ) NOT NULL default '0' AFTER `echantillon`;
ALTER TABLE `CategorieLot` ADD `visites_lieux` ENUM( '0', '1' ) NOT NULL default '0' AFTER `reunion`;

ALTER TABLE `CategorieLot` ADD `add_echantillion_fr` VARCHAR( 255 ) NULL AFTER `visites_lieux` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_en` VARCHAR( 255 ) NULL AFTER `add_echantillion_fr` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_es` VARCHAR( 255 ) NULL AFTER `add_echantillion_en` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_su` VARCHAR( 255 ) NULL AFTER `add_echantillion_es` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_du` VARCHAR( 255 ) NULL AFTER `add_echantillion_su` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_cz` VARCHAR( 255 ) NULL AFTER `add_echantillion_du` ;
ALTER TABLE `CategorieLot` ADD `add_echantillion_ar` VARCHAR( 255 ) NULL AFTER `add_echantillion_cz` ;

ALTER TABLE `CategorieLot` ADD `add_reunion_fr` VARCHAR( 255 ) NULL AFTER `add_echantillion_ar` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_en` VARCHAR( 255 ) NULL AFTER `add_reunion_fr` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_es` VARCHAR( 255 ) NULL AFTER `add_reunion_en` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_su` VARCHAR( 255 ) NULL AFTER `add_reunion_es` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_du` VARCHAR( 255 ) NULL AFTER `add_reunion_su` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_cz` VARCHAR( 255 ) NULL AFTER `add_reunion_du` ;
ALTER TABLE `CategorieLot` ADD `add_reunion_ar` VARCHAR( 255 ) NULL AFTER `add_reunion_cz` ;


CREATE TABLE `gestion_adresses` (
  `id` int(50) NOT NULL auto_increment,
  `id_service` int(11) NOT NULL default '0',
  `id_agent` int(11) NOT NULL default '0',
  `nom_agent` varchar(250) NOT NULL default '',
  `prenom_agent` varchar(250) NOT NULL default '',
  `adresse_depot_offres` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis` varchar(250) NOT NULL default '',
  `adresse_depot_offres_fr` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_fr` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_fr` varchar(250) NOT NULL default '',
  `adresse_depot_offres_ar` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_ar` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_ar` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `historiques_consultation` (
  `id` int(50) NOT NULL auto_increment,
  `ref_consultation` int(50) NOT NULL default '0',
  `statut` char(1) NOT NULL default '',
  `id_agent` int(50) NOT NULL default '0',
  `nom_agent` varchar(250) NOT NULL default '',
  `prenom_agent` varchar(250) NOT NULL default '',
  `nom_element` varchar(250) NOT NULL default '',
  `valeur` varchar(250) NOT NULL default '',
  `valeur_detail_1` varchar(250) NOT NULL default '',
  `valeur_detail_2` varchar(250) NOT NULL default '',
  `numero_lot` varchar(250) NOT NULL default '',
  `horodatage` longblob NOT NULL default '',
  `untrusteddate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

CREATE TABLE `resultat_analyse` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT,
`consultation_ref` INT( 11 ) NOT NULL default '0',
`id_offre` INT( 11 ) NOT NULL default '0',
`lot` INT( 11 ) NOT NULL default '0',
`montant_offre` VARCHAR( 200 ) NULL ,
`note` VARCHAR( 200 ) NULL ,
`classement` TINYINT( 200 ) NULL ,
`observation` VARCHAR( 200 ) NULL ,
`type_enveloppe` INT( 1 ) NOT NULL default '0',
PRIMARY KEY ( `id` )
) TYPE = innodb;

CREATE TABLE `resultat_analyse_decision` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT,
`consultation_ref` INT( 11 ) NOT NULL default '0',
`lot` INT( 11 ) NOT NULL default '0',
`date_decision` VARCHAR( 20 ) NULL ,
`type_decision` VARCHAR( 200 ) NULL ,
`commentaire` TEXT NULL,
PRIMARY KEY ( `id` )
) TYPE = innodb;

CREATE TABLE `visite_lieux` (
  `id` int(50) NOT NULL auto_increment,
  `reference` int(50) NOT NULL default '0',
  `adresse` varchar(250) NOT NULL default '',
  `date` varchar(50) NOT NULL default '',
  `lot` char(1) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `reference` (`reference`)
) TYPE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `visite_lieux` ADD CONSTRAINT `visite_lieux_ibfk_1` FOREIGN KEY (`reference`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `visite_lieux` ADD `adresse_fr` VARCHAR( 255 ) NULL AFTER `lot` ;
ALTER TABLE `visite_lieux` ADD `adresse_en` VARCHAR( 255 ) NULL AFTER `adresse_fr` ;
ALTER TABLE `visite_lieux` ADD `adresse_es` VARCHAR( 255 ) NULL AFTER `adresse_en` ;
ALTER TABLE `visite_lieux` ADD `adresse_su` VARCHAR( 255 ) NULL AFTER `adresse_es` ;
ALTER TABLE `visite_lieux` ADD `adresse_du` VARCHAR( 255 ) NULL AFTER `adresse_su` ;
ALTER TABLE `visite_lieux` ADD `adresse_cz` VARCHAR( 255 ) NULL AFTER `adresse_du` ;
ALTER TABLE `visite_lieux` ADD `adresse_ar` VARCHAR( 255 ) NULL AFTER `adresse_cz` ;

-- MLA le 23/02/2010
ALTER TABLE `Offre` ADD `offre_variante` ENUM( '0', '1' ) NULL DEFAULT NULL ;
-- MLA le 24/02/2010
ALTER TABLE `Offre_papier` ADD `offre_variante` ENUM( '0', '1' ) NULL DEFAULT NULL;

-- MMI

ALTER TABLE `gestion_adresses` ADD `adresse_depot_offres_en` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `adresse_depot_offres_es` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `adresse_depot_offres_su` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `adresse_depot_offres_du` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `adresse_depot_offres_cz` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `adresse_retrais_dossiers_en` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `adresse_retrais_dossiers_es` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `adresse_retrais_dossiers_su` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `adresse_retrais_dossiers_du` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `adresse_retrais_dossiers_cz` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `lieu_ouverture_plis_en` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `lieu_ouverture_plis_es` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `lieu_ouverture_plis_su` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `lieu_ouverture_plis_du` VARCHAR( 250 ) NOT NULL default '';
ALTER TABLE `gestion_adresses` ADD `lieu_ouverture_plis_cz` VARCHAR( 250 ) NOT NULL default '';


ALTER TABLE `ValeurReferentiel` CHANGE `libelle_valeur_referentiel` `libelle_valeur_referentiel` TEXT NOT NULL default '';
ALTER TABLE `AVIS` ADD `type_doc_genere` INT( 11 ) NOT NULL DEFAULT '0';

-- OKO le 01/03/2010
UPDATE `TypeProcedure` SET `autre` = '0' WHERE `TypeProcedure`.`id_type_procedure` =19 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '0' WHERE `TypeProcedure`.`id_type_procedure` =20 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '0' WHERE `TypeProcedure`.`id_type_procedure` =30 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '0' WHERE `TypeProcedure`.`id_type_procedure` =31 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '0' WHERE `TypeProcedure`.`id_type_procedure` =32 LIMIT 1 ;
UPDATE `TypeProcedure` SET `autre` = '0' WHERE `TypeProcedure`.`id_type_procedure` =33 LIMIT 1 ;


ALTER TABLE `Helios_tableau_ar` CHANGE `description` `description` LONGTEXT  DEFAULT NULL ;
ALTER TABLE `Helios_teletransmission` CHANGE `piecej1_desc` `piecej1_desc` LONGTEXT  NULL ;
ALTER TABLE `Helios_teletransmission` CHANGE `piecej2_desc` `piecej2_desc` LONGTEXT  NULL ;

ALTER TABLE `Helios_teletransmission` CHANGE `tableau_ar` `tableau_ar` INT NULL DEFAULT NULL ;
ALTER TABLE `resultat_analyse_decision` ADD `autre` TEXT NULL ;
ALTER TABLE `resultat_analyse_decision` CHANGE `autre` `autre_type_decision` TEXT NULL DEFAULT NULL ;
-- OKO: le 31/03/2010
ALTER TABLE `Retrait_Papier` ADD `support` ENUM( '1', '2', '3' ) NOT NULL DEFAULT '1' AFTER `acronyme_pays` ;

-- MMI : 31/03/2010
ALTER TABLE `consultation` ADD `mode_passation` VARCHAR( 1 ) NULL DEFAULT '' AFTER `add_reunion_ar` ;

CREATE TABLE `renseignements_boamp` (
`id` INT( 50 ) NOT NULL AUTO_INCREMENT,
`id_type` INT( 50 ) NOT NULL DEFAULT '0' ,
`id_compte` INT( 50 ) NOT NULL DEFAULT '0' ,
`correspondant` VARCHAR( 200 ) NULL DEFAULT NULL ,
`organisme` VARCHAR( 200 ) NULL DEFAULT NULL ,
`adresse` VARCHAR( 200 ) NULL DEFAULT NULL ,
`cp` VARCHAR( 200 ) NULL DEFAULT NULL ,
`ville` VARCHAR( 200 ) NULL DEFAULT NULL ,
`pays` VARCHAR( 200 ) NULL DEFAULT NULL ,
`telephone` VARCHAR( 200 ) NULL DEFAULT NULL ,
`poste` VARCHAR( 200 ) NULL DEFAULT NULL ,
`fax` VARCHAR( 200 ) NULL DEFAULT NULL ,
`mail` VARCHAR( 200 ) NULL DEFAULT NULL ,
`url` TEXT NULL DEFAULT NULL ,
PRIMARY KEY ( `id` ) ,
INDEX ( `id_type` , `id_compte` )
) ENGINE = InnoDB ;

ALTER TABLE `AcheteurPublic` ADD `type_pouvoir_activite`  TEXT NULL DEFAULT '' ;
ALTER TABLE `AcheteurPublic` ADD `code_nuts`  VARCHAR( 20 ) NULL DEFAULT '' ;
ALTER TABLE `AcheteurPublic` ADD `modalites_financement`  TEXT NULL DEFAULT '' ;

ALTER TABLE `consultation` CHANGE `pieces_dossier_admin` `pieces_dossier_admin`  TEXT  NULL DEFAULT '';
ALTER TABLE `consultation` CHANGE `pieces_dossier_tech` `pieces_dossier_tech`  TEXT NULL DEFAULT '';
ALTER TABLE `consultation` CHANGE `pieces_dossier_additif` `pieces_dossier_additif`  TEXT NULL DEFAULT '';

-- MWA : 14/04/2010 ajout nouveaux formulaires
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (14, '2', 'XML 1.9 JOUE_10 - Concession de travaux publics');
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (15, '2', 'XML 1.9 JOUE_01T - Avis de préinformation - Travaux');
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (16, '2', 'XML 1.9 JOUE_01FS - Avis de préinformation - Fournitures / Services');


-- MMI : 29/04/2010

ALTER TABLE `decisionEnveloppe` CHANGE `numero_marche` `numero_marche` TEXT NULL DEFAULT NULL ;
ALTER TABLE `decisionEnveloppe` CHANGE `statutEJ` `statutEJ` TEXT NULL DEFAULT NULL ;


--  MWA : le 05/05/2010 (correction anomalie sur taille fichier externe)
ALTER TABLE `Chorus_pj` CHANGE `nom_fichier` `nom_fichier` VARCHAR( 300 ) NOT NULL DEFAULT '';

-- MMI 

ALTER TABLE `consultation` ADD `ref_org_partenaire` VARCHAR( 250 ) NOT NULL DEFAULT '';
ALTER TABLE `renseignements_boamp` ADD `organe_charge_procedure`  TEXT NULL DEFAULT '' ;

-- FBO Traduction table Consultation:champs pieces_dossier_admin le 09/06/2010
ALTER TABLE `consultation` ADD `pieces_dossier_admin_fr` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin`;
ALTER TABLE `consultation` ADD `pieces_dossier_admin_en` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_fr`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_es` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_en`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_su` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_es`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_du` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_su`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_cz` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_du`;  
ALTER TABLE `consultation` ADD `pieces_dossier_admin_ar` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_admin_cz`;
-- FBO Traduction table Consultation:champs pieces_dossier_tech le 09/06/2010
ALTER TABLE `consultation` ADD `pieces_dossier_tech_fr` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_en` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_fr`;
ALTER TABLE `consultation` ADD `pieces_dossier_tech_es` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_en`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_su` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_es`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_du` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_su`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_cz` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_du`;  
ALTER TABLE `consultation` ADD `pieces_dossier_tech_ar` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_tech_cz`;
-- FBO Traduction table Consultation:champs pieces_dossier_pieces_dossier_additif le 09/06/2010
ALTER TABLE `consultation` ADD `pieces_dossier_additif_fr` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_en` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_fr`;
ALTER TABLE `consultation` ADD `pieces_dossier_additif_es` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_en`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_su` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_es`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_du` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_su`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_cz` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_du`;  
ALTER TABLE `consultation` ADD `pieces_dossier_additif_ar` TEXT  NULL DEFAULT NULL AFTER `pieces_dossier_additif_cz`;

-- FBO table Avis : Langue Avis
ALTER TABLE `AVIS` ADD `langue` VARCHAR( 10 )  NOT NULL DEFAULT '' AFTER `type_doc_genere`; 

-- MMI :

ALTER TABLE `ReferentielTypeXml` ADD `id_avis_marche` INT NULL DEFAULT '0' ;

UPDATE `ReferentielTypeXml` SET `id_avis_marche` = '3' WHERE `id` =4 ;
UPDATE `ReferentielTypeXml` SET `id_avis_marche` = '1' WHERE `id` =5 ;
UPDATE `ReferentielTypeXml` SET `id_avis_marche` = '2' WHERE `id` =7 ;
-- UPDATE `ReferentielTypeXml` SET `id_avis_marche` = '9' WHERE `id` =12 ;

-- OKO 16/06/2010: Ajout de la date d'archivage à la consultation
ALTER TABLE `consultation` ADD `date_archivage` VARCHAR( 20 ) NULL DEFAULT '' COMMENT 'Précise la date de réalisation de l''archivage'  AFTER `ref_org_partenaire` ; 

ALTER TABLE `consultation` ADD `consultation_annulee` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `consultation` ADD `date_decision_annulation` VARCHAR( 20 ) NULL DEFAULT '';
ALTER TABLE `consultation` ADD `commentaire_annulation` TEXT NULL DEFAULT '' ;

-- MWA 22/06/2010
ALTER TABLE `Chorus_echange` ADD `id_ej_appli_ext` VARCHAR( 20 ) NOT NULL DEFAULT '' AFTER `retour_chorus` ;

-- OKO: le 17/06/2010: Ajout de la contrainte de clé sécondaire à la table passation_marche_a_venir
ALTER TABLE `passation_marche_a_venir` ADD INDEX `id_passation_consultation` ( `id_passation_consultation`  ) ;
ALTER TABLE `passation_marche_a_venir` ADD FOREIGN KEY ( `id_passation_consultation` ) REFERENCES `Passation_consultation` ( `id` ) ;

-- OKO: le 5 juillet 2010
ALTER TABLE `CategorieConsultation` ADD `libelle_it` VARCHAR( 100 ) NULL DEFAULT '' ; 
ALTER TABLE `CategorieLot` ADD `description_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `CategorieLot` ADD `description_detail_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `CategorieLot` ADD `add_echantillion_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `CategorieLot` ADD `add_reunion_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `consultation` ADD `titre_it` LONGTEXT NULL DEFAULT '' ; 
ALTER TABLE `consultation` ADD `resume_it` LONGTEXT NULL DEFAULT '' ; 
ALTER TABLE `consultation` ADD `accessibilite_it` ENUM( '0', '1' ) NOT NULL DEFAULT '0' ; 
ALTER TABLE `consultation` ADD `adresse_depot_offres_it` VARCHAR( 255 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `lieu_ouverture_plis_it` VARCHAR( 255 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_it` VARCHAR( 255 ) NOT NULL default '' ; 
ALTER TABLE `consultation` ADD `pieces_dossier_admin_it` TEXT  NULL DEFAULT NULL ; 
ALTER TABLE `consultation` ADD `pieces_dossier_tech_it` TEXT  NULL DEFAULT NULL ; 
ALTER TABLE `consultation` ADD `pieces_dossier_additif_it` TEXT  NULL DEFAULT NULL ; 
ALTER TABLE `consultation` ADD `detail_consultation_it` text DEFAULT ''; 
ALTER TABLE `consultation` ADD `add_echantillion_it` VARCHAR( 255 ) NULL DEFAULT '' ; 
ALTER TABLE `consultation` ADD `add_reunion_it` VARCHAR( 255 ) NULL DEFAULT ''  ; 
ALTER TABLE `gestion_adresses` ADD `adresse_depot_offres_it` varchar(250) NOT NULL default '' ; 
ALTER TABLE `gestion_adresses` ADD `adresse_retrais_dossiers_it` varchar(250) NOT NULL default '' ; 
ALTER TABLE `gestion_adresses` ADD `lieu_ouverture_plis_it` varchar(250) NOT NULL default '' ; 
ALTER TABLE `Service` ADD `libelle_it` TEXT NOT NULL default ''; 
ALTER TABLE `Service` ADD `adresse_it` VARCHAR( 100 ) NOT NULL default ''; 
ALTER TABLE `Service` ADD `adresse_suite_it` VARCHAR( 100 ) NOT NULL default ''; 
ALTER TABLE `Service` ADD `ville_it` VARCHAR( 100 ) NOT NULL default ''; 
ALTER TABLE `Service` ADD `pays_it` VARCHAR( 150 ) NOT NULL DEFAULT 'NULL'; 
ALTER TABLE `Service` ADD `path_service_it` VARCHAR( 255 ) NULL DEFAULT NULL ; 
ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_it` VARCHAR( 100 ) NULL DEFAULT '' ; 
ALTER TABLE `visite_lieux` ADD `adresse_it` VARCHAR( 255 ) NULL DEFAULT '' ;

-- KBE : gestion referentiel
ALTER TABLE `consultation` CHANGE `code_cpv_2` `code_cpv_2` VARCHAR( 255 ) NULL DEFAULT NULL;   
ALTER TABLE `CategorieLot` CHANGE `code_cpv_2` `code_cpv_2` VARCHAR( 255 ) NULL DEFAULT NULL;  

-- OKO le 04 Aout 2010: gestion des codes nuts dans LT-Réf
ALTER TABLE `consultation` ADD `codes_nuts` TEXT NOT NULL DEFAULT '' COMMENT 'Contient les codes nuts, pour l''utilisation d''un referentiel externe';

-- AME change le champ code postal qui est un int en varchar
ALTER TABLE `Retrait_Papier` CHANGE `codepostal` `codepostal` VARCHAR( 5 ) NOT NULL DEFAULT '0';

-- OKO le 14/09/2010: Mise à jour des valeurs du champ 'description', les nouvelles valeurs sont en correspondance avec le messages.xml
UPDATE `StatutEnveloppe` SET `description` = 'FERMEE' WHERE `StatutEnveloppe`.`id_statut` =1 ;
UPDATE `StatutEnveloppe` SET `description` = 'DEFINE_OUVERTE_EN_LIGNE' WHERE `StatutEnveloppe`.`id_statut` =2 ;
UPDATE `StatutEnveloppe` SET `description` = 'DEFINE_OUVERTE_HORS_LIGNE' WHERE `StatutEnveloppe`.`id_statut` =3 ;
UPDATE `StatutEnveloppe` SET `description` = 'DEFINE_REFUSEE' WHERE `StatutEnveloppe`.`id_statut` =4 ;
UPDATE `StatutEnveloppe` SET `description` = 'OUVERTE' WHERE `StatutEnveloppe`.`id_statut` =5 ;
UPDATE `StatutEnveloppe` SET `description` = 'DEFINE_SUPPRIMEE' WHERE `StatutEnveloppe`.`id_statut` =6 ;
UPDATE `StatutEnveloppe` SET `description` = 'DEFINE_OUVERTE_A_DISTANCE' WHERE `StatutEnveloppe`.`id_statut` =7 ;

-- OKO le 14/09/2010: ajout d'un nouveau champ dans la table EchangeTypeMessage, remplissage du contenu de ce contenu et suppression du champ 'libelle'
ALTER TABLE `EchangeTypeMessage` ADD `code` VARCHAR( 100 ) NOT NULL DEFAULT '' COMMENT 'Les contenus de ce champ sont en correspondance avec messages.xml' AFTER `id` ;
UPDATE `EchangeTypeMessage` SET `code` = 'DEFINE_COURRIER_NOTIFICATION' WHERE `EchangeTypeMessage`.`id` =1 ;
UPDATE `EchangeTypeMessage` SET `code` = 'DEFINE_COURRIER_REJET' WHERE `EchangeTypeMessage`.`id` =2 ;
UPDATE `EchangeTypeMessage` SET `code` = 'DEFINE_AVERTISSEMENT_MODIFICATION_CONSULTATION' WHERE `EchangeTypeMessage`.`id` =3 ;
UPDATE `EchangeTypeMessage` SET `code` = 'DEFINE_COURRIER_LIBRE' WHERE `EchangeTypeMessage`.`id` =4 ;
UPDATE `EchangeTypeMessage` SET `code` = 'DEFINE_COURRIER_DEMANDE_COMPLEMENT' WHERE `EchangeTypeMessage`.`id` =5 ;
ALTER TABLE `EchangeTypeMessage` DROP `libelle` ;


-- MWA : 14/06/2010 ajout nouveaux formulaires
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (17, '1', 'XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification');

-- OKO le 16/09/2010: Ajout d'un nouveau champ dans la table TypeDecison, remplissage de ce nouveau champ et suppression du champ 'libelle_type_decision'
ALTER TABLE  `TypeDecision` ADD  `code_type_decision` VARCHAR( 100 ) NOT NULL default '' COMMENT  'Pour obtenir le libellé, ce champ est mis en correspondance avec messages.xml' AFTER `id_type_decision`;
UPDATE  `TypeDecision` SET  `code_type_decision` =  'DECISION_ATTRIBUTION_MARCHE' WHERE  `TypeDecision`.`id_type_decision` =2 ;
UPDATE  `TypeDecision` SET  `code_type_decision` =  'DECISION_DECLARATION_SANS_SUITE' WHERE  `TypeDecision`.`id_type_decision` =3 ;
UPDATE  `TypeDecision` SET  `code_type_decision` =  'DECISION_DECLARATION_INFRUCTUEUX' WHERE  `TypeDecision`.`id_type_decision` =4 ;
UPDATE  `TypeDecision` SET  `code_type_decision` =  'DECISION_SELECTION_ENTREPRISES' WHERE  `TypeDecision`.`id_type_decision` =5 ;
UPDATE  `TypeDecision` SET  `code_type_decision` =  'DECISION_ATTRIBUTION_DE_ACCORD_CADRE' WHERE  `TypeDecision`.`id_type_decision` =6 ;
UPDATE  `TypeDecision` SET  `code_type_decision` =  'DECISION_ADMISSION_SAD' WHERE  `TypeDecision`.`id_type_decision` =7 ;
UPDATE  `TypeDecision` SET  `code_type_decision` =  'DECISION_AUTRE' WHERE  `TypeDecision`.`id_type_decision` =8 ;
UPDATE  `TypeDecision` SET  `code_type_decision` =  'DECISION_A_RENSEIGNER' WHERE  `TypeDecision`.`id_type_decision` =9 ;
ALTER TABLE  `TypeDecision` DROP  `libelle_type_decision` ;

-- OKO le 17/09/2010: traduction de la table 'ReferentielTypeXml'
ALTER TABLE `ReferentielTypeXml` ADD `libelle_type_fr` VARCHAR( 100 ) NOT NULL DEFAULT '' AFTER `libelle_type` ;
ALTER TABLE `ReferentielTypeXml` ADD `libelle_type_en` VARCHAR( 100 ) NOT NULL DEFAULT '' AFTER `libelle_type_fr` ;
ALTER TABLE `ReferentielTypeXml` ADD `libelle_type_es` VARCHAR( 100 ) NOT NULL DEFAULT '' AFTER `libelle_type_en` ;
ALTER TABLE `ReferentielTypeXml` ADD `libelle_type_su` VARCHAR( 100 ) NOT NULL DEFAULT '' AFTER `libelle_type_es` ;
ALTER TABLE `ReferentielTypeXml` ADD `libelle_type_du` VARCHAR( 100 ) NOT NULL DEFAULT '' AFTER `libelle_type_su` ;
ALTER TABLE `ReferentielTypeXml` ADD `libelle_type_cz` VARCHAR( 100 ) NOT NULL DEFAULT '' AFTER `libelle_type_du` ;
ALTER TABLE `ReferentielTypeXml` ADD `libelle_type_ar` VARCHAR( 100 ) NOT NULL DEFAULT '' AFTER `libelle_type_cz` ;
ALTER TABLE `ReferentielTypeXml` ADD `libelle_type_it` VARCHAR( 100 ) NOT NULL DEFAULT '' AFTER `libelle_type_ar` ;

-- OKO le 22/09/2010: traduction de la table 'EchangeTypeAR'
ALTER TABLE `EchangeTypeAR` ADD `libelle_fr` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeTypeAR` ADD `libelle_en` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeTypeAR` ADD `libelle_es` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeTypeAR` ADD `libelle_su` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeTypeAR` ADD `libelle_du` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeTypeAR` ADD `libelle_cz` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeTypeAR` ADD `libelle_ar` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeTypeAR` ADD `libelle_it` VARCHAR( 100 ) NOT NULL DEFAULT '' ;

-- OKO le 22/09/2010: traduction de la table 'EchangeFormat'
ALTER TABLE `EchangeFormat` ADD `libelle_fr` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeFormat` ADD `libelle_en` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeFormat` ADD `libelle_es` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeFormat` ADD `libelle_su` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeFormat` ADD `libelle_du` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeFormat` ADD `libelle_cz` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeFormat` ADD `libelle_ar` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `EchangeFormat` ADD `libelle_it` VARCHAR( 100 ) NOT NULL DEFAULT '' ;

-- OKO le 23/09/2010 : traduction de la table 'DecisionPassationMarcheAVenir'
ALTER TABLE `DecisionPassationMarcheAVenir` ADD `libelle_fr` VARCHAR( 100 ) NULL DEFAULT '';
ALTER TABLE `DecisionPassationMarcheAVenir` ADD `libelle_en` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationMarcheAVenir` ADD `libelle_es` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationMarcheAVenir` ADD `libelle_su` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationMarcheAVenir` ADD `libelle_du` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationMarcheAVenir` ADD `libelle_cz` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationMarcheAVenir` ADD `libelle_ar` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationMarcheAVenir` ADD `libelle_it` VARCHAR( 100 ) NULL DEFAULT '' ;

-- OKO le 23/09/2010 : traduction de la table 'DecisionPassationConsultation'
ALTER TABLE `DecisionPassationConsultation` ADD `libelle_fr` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationConsultation` ADD `libelle_en` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationConsultation` ADD `libelle_es` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationConsultation` ADD `libelle_su` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationConsultation` ADD `libelle_du` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationConsultation` ADD `libelle_cz` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationConsultation` ADD `libelle_ar` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `DecisionPassationConsultation` ADD `libelle_it` VARCHAR( 100 ) NULL DEFAULT '' ;

-- OKO le 23/09/2010 : traduction de la table 'AvisCao'
ALTER TABLE `AvisCao` ADD `libelle_fr` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `AvisCao` ADD `libelle_en` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `AvisCao` ADD `libelle_es` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `AvisCao` ADD `libelle_su` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `AvisCao` ADD `libelle_du` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `AvisCao` ADD `libelle_cz` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `AvisCao` ADD `libelle_ar` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `AvisCao` ADD `libelle_it` VARCHAR( 100 ) NULL DEFAULT '' ;

-- OKO le 23/09/2010 : traduction de la table 'TypeAvenant'
ALTER TABLE `TypeAvenant` ADD `libelle_fr` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `TypeAvenant` ADD `libelle_en` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `TypeAvenant` ADD `libelle_es` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `TypeAvenant` ADD `libelle_su` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `TypeAvenant` ADD `libelle_du` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `TypeAvenant` ADD `libelle_cz` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `TypeAvenant` ADD `libelle_ar` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `TypeAvenant` ADD `libelle_it` VARCHAR( 100 ) NULL DEFAULT '' ;

-- OKO le 24/09/2010 : traduction de la table 'nature_acte_juridique'
ALTER TABLE `nature_acte_juridique` ADD `libelle_fr` VARCHAR( 50 ) NULL DEFAULT '' ;
ALTER TABLE `nature_acte_juridique` ADD `libelle_en` VARCHAR( 50 ) NULL DEFAULT '' ;
ALTER TABLE `nature_acte_juridique` ADD `libelle_es` VARCHAR( 50 ) NULL DEFAULT '' ;
ALTER TABLE `nature_acte_juridique` ADD `libelle_su` VARCHAR( 50 ) NULL DEFAULT '' ;
ALTER TABLE `nature_acte_juridique` ADD `libelle_du` VARCHAR( 50 ) NULL DEFAULT '' ;
ALTER TABLE `nature_acte_juridique` ADD `libelle_cz` VARCHAR( 50 ) NULL DEFAULT '' ;
ALTER TABLE `nature_acte_juridique` ADD `libelle_ar` VARCHAR( 50 ) NULL DEFAULT '' ;
ALTER TABLE `nature_acte_juridique` ADD `libelle_it` VARCHAR( 50 ) NULL DEFAULT '' ;

-- OKO le 24/09/2010 : traduction de la table 'mode_execution_contrat'
ALTER TABLE `mode_execution_contrat` ADD `libelle_fr` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `mode_execution_contrat` ADD `libelle_en` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `mode_execution_contrat` ADD `libelle_es` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `mode_execution_contrat` ADD `libelle_su` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `mode_execution_contrat` ADD `libelle_du` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `mode_execution_contrat` ADD `libelle_cz` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `mode_execution_contrat` ADD `libelle_ar` VARCHAR( 255 ) NULL DEFAULT '' ;
ALTER TABLE `mode_execution_contrat` ADD `libelle_it` VARCHAR( 255 ) NULL DEFAULT '' ;

-- OKO le 24/09/2010 : traduction de la table valeur_referentiel
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_fr` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_en` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_fr`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_es` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_en`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_su` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_es`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_du` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_su`;  
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_cz` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_du`; 
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_ar` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_cz`;
ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_it` TEXT  NOT NULL DEFAULT '' AFTER `libelle_valeur_referentiel_ar`;

-- OKO le 27/09/2010: suppréssion des tables non utilisées  
DROP TABLE IF EXISTS `ChampParametrable`;
DROP TABLE IF EXISTS `CommandeStatut`;
DROP TABLE IF EXISTS `MarcheTrancheBudgetaire`;
DROP TABLE IF EXISTS `Etape_cao`;

-- FBO le 29/09/2010 :5062: TGR : Création d'une annonce programme prévisionnel 
CREATE TABLE IF NOT EXISTS `programme_previsionnel` (
  `id` int(20) NOT NULL auto_increment,
  `nom_fichier` varchar(50) NOT NULL default '',
  `fichier` varchar(20) NOT NULL default '',
  `date` varchar(20) NOT NULL default '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '',
  `taille` int(20) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `programme_previsionnel` ADD `annee` VARCHAR( 20 ) NOT NULL DEFAULT '' AFTER `taille` ;
ALTER TABLE `programme_previsionnel` ADD `service_id` INT( 20 ) NOT NULL DEFAULT '0' AFTER `annee` ;
ALTER TABLE `programme_previsionnel` ADD INDEX `service_id` ( `service_id` ) ;

-- OKO le 01/10/2010: suppréssion des tables non utilisées
DROP TABLE IF EXISTS `dfpcarep`;
DROP TABLE IF EXISTS `dossierDfpca`;
DROP TABLE IF EXISTS `telechargement_aap`;
DROP TABLE IF EXISTS `aapDfpca`;
DROP TABLE IF EXISTS `dfpcaform`;
DROP TABLE IF EXISTS `aapdomaine`;
DROP TABLE IF EXISTS `aapFormDyn`;
DROP TABLE IF EXISTS `admission_systeme_acquisition`;
DROP TABLE IF EXISTS `ARMail`;
DROP TABLE IF EXISTS `ARModificationDCE`;
DROP TABLE IF EXISTS `ChampOptionnelLibelle`;
DROP TABLE IF EXISTS `ClassementOffre`;
DROP TABLE IF EXISTS `CommandeLigne`;
DROP TABLE IF EXISTS `CommandeHistoriqueStatut`;
DROP TABLE IF EXISTS `CommandeFile`;
DROP TABLE IF EXISTS `Commande`;
DROP TABLE IF EXISTS `ConsultationLotMarco`;
DROP TABLE IF EXISTS `DATEFIN_sad`;
DROP TABLE IF EXISTS `Demande_DCE`;
DROP TABLE IF EXISTS `FactureHistoriqueStatut`;
DROP TABLE IF EXISTS `FactureStatut`;
DROP TABLE IF EXISTS `Facture`;
DROP TABLE IF EXISTS `Mail`;
DROP TABLE IF EXISTS `historique_actions_systeme_acquisition`;
DROP TABLE IF EXISTS `question_aap`;

-- AME 11/10/2010 : suppression des index inutils et l'ajout des index utils

ALTER TABLE `Annonce` DROP INDEX `id_destination_form_xml_2`;
ALTER TABLE `Annonce` DROP INDEX `id_form_xml_2`;
ALTER TABLE `Annonce` DROP INDEX `id_type_xml_2`;
ALTER TABLE `Annonce` DROP INDEX `id_destination_form_xml_3`;
ALTER TABLE `Annonce` DROP INDEX `id_form_xml_3`;
ALTER TABLE `Annonce` DROP INDEX `id_type_xml_3`;

ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_form_xml_2`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_destination_form_xml_2`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_form_xml_3`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_destination_form_xml_3`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_form_xml_4`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_destination_form_xml_4`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_form_xml_5`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_destination_form_xml_5`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_form_xml_6`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_destination_form_xml_6`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_form_xml_7`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_destination_form_xml_7`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_form_xml_8`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_destination_form_xml_8`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_form_xml_9`;
ALTER TABLE `AnnonceMoniteur` DROP INDEX `id_destination_form_xml_9`;

ALTER TABLE `blocFichierEnveloppe` DROP INDEX `id_fichier_2`;
ALTER TABLE `blocFichierEnveloppe` DROP INDEX `id_fichier_3`;
ALTER TABLE `blocFichierEnveloppe` DROP INDEX `id_fichier_4`;


ALTER TABLE `Centrale_publication` DROP INDEX `id_2`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_3`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_4`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_5`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_6`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_7`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_8`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_9`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_10`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_11`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_12`;
ALTER TABLE `Centrale_publication` DROP INDEX `id_13`;

ALTER TABLE `Chorus_echange` DROP INDEX `id_decision_2`;

ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_2`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_3`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_4`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_5`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_6`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_7`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_8`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_9`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_10`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_11`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_12`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_13`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_14`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_15`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_16`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_17`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_18`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_19`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_20`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_21`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_22`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_23`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_24`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_25`;
ALTER TABLE `compte_centrale` DROP INDEX `ID_CENTRALE_26`;

ALTER TABLE `DestinataireAnnonceJAL` DROP INDEX `idAnnonceJAL_2`;
ALTER TABLE `DestinataireAnnonceJAL` DROP INDEX `idAnnonceJAL_3`;

ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_2`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_2`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_3`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_3`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_4`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_4`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_5`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_5`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_6`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_6`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_7`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_7`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_8`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_8`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_9`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_9`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_10`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_10`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_11`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_11`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_12`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_12`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_annonce_jal_13`;
ALTER TABLE `destinataire_centrale_pub` DROP INDEX `id_compte_13`;

ALTER TABLE `EncherePmi` DROP INDEX `idEntiteeAssociee_2`;
ALTER TABLE `EncherePmi` DROP INDEX `idEntiteeAssociee_3`;
ALTER TABLE `EncherePmi` DROP FOREIGN KEY `EncherePmi_ibfk_1` ;

ALTER TABLE `fichierEnveloppe` DROP INDEX `id_enveloppe_2`;
ALTER TABLE `fichierEnveloppe` DROP INDEX `id_enveloppe_3`;
ALTER TABLE `fichierEnveloppe` DROP INDEX `id_enveloppe_4`;

ALTER TABLE `Helios_cosignature` DROP INDEX `id_fichier_2`;

ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_2`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_3`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_4`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_5`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_6`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_7`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_8`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_9`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_10`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_11`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_12`;
ALTER TABLE `Journaux` DROP INDEX `ID_CENTRALE_13`;

ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_2`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_3`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_4`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_5`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_6`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_7`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_8`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_9`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_10`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_11`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_12`;
ALTER TABLE `passation_marche_a_venir` DROP INDEX `id_passation_consultation_13`;

ALTER TABLE `decisionEnveloppe` ADD INDEX `ref_Consultation` ( `consultation_ref` );
ALTER TABLE `DecisionLot` ADD INDEX `ref_Consultation` ( `consultation_ref` ) ;
ALTER TABLE `historiques_consultation` ADD INDEX `ref_Consultation` ( `ref_consultation` )  ;
ALTER TABLE `resultat_analyse` ADD INDEX `ref_Consultation` ( `consultation_ref` );
ALTER TABLE `resultat_analyse_decision` ADD INDEX `ref_Consultation` ( `consultation_ref` );

-----
-- FBO :12/10/2010:Champ reponse_pas_a_pas pour les statiques des reponses de type pas a pas.
----
ALTER TABLE `Offre` ADD `reponse_pas_a_pas` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `offre_variante` ;

--  MWA : 27/10 rajout formulaires XML
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (13, '2#3', 'XML 1.9 JOUE_06 -  Avis d\'attribution - Secteurs Spéciaux');
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`) VALUES (17, '1', 'XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification');

-- OKO le 12/11/2010: suppression du destinataire "Portail Entreprise" dans la publicité
DELETE FROM `ReferentielDestinationFormXml` WHERE `ReferentielDestinationFormXml`.`id` = 4;

-- OKO: le 23/10/2010: changement du type de champ en texte pour l'utilisation des codes nuts LT-Ref.
ALTER TABLE `AcheteurPublic` CHANGE `code_nuts` `code_nuts` TEXT NULL DEFAULT NULL ;

-- OKO le 01 Decembre 2010: Ajout d'un nouveau champ pour le parametrage du type de formulaire xml dans la publicité
ALTER TABLE `TypeProcedure` ADD `publicite_types_form_xml` TEXT NOT NULL DEFAULT '' COMMENT 'Permet de parametrer les types de formulaires au format xml dans la publicité en fonction du type de procedure. Les valeurs de ce champ doivent etre separés par des ''#''';

-- KAT 05/01/2011: Ajout d'une table pour relier l'echange avec la decision
CREATE TABLE `RelationEchange` (
`id_echange` INT( 11 ) NOT NULL DEFAULT '0',
`id_externe` INT( 11 ) NOT NULL DEFAULT '0',
`type_relation` INT( 11 ) NOT NULL DEFAULT '0'
) ENGINE = InnoDB;

INSERT INTO `EchangeTypeMessage` (`id`, `code`, `corps`) 
VALUES (6, 'DEFINE_COURRIER_REPONSE_A_QUESTION', 'TEXTE REPONSE A QUESTION\r\n'); 

-- OKO le 12/01/2010:
ALTER TABLE `TypeProcedure` ADD `tag_name_chorus` VARCHAR( 100 ) NOT NULL DEFAULT '' ;
ALTER TABLE `Chorus_echange` CHANGE `cpv_2` `cpv_2` TEXT NULL DEFAULT NULL ;

-- KAT 06/01/2011: Ajout d'un champ dans consultation qui permet de savoir la date de changement d'état de la consultation (decision) 
ALTER TABLE `consultation` ADD `date_decision` VARCHAR( 10 ) NULL DEFAULT '' ;

-- OKO: 24/01/2011 - modification de la table Chorus_echange
ALTER TABLE `Chorus_echange` ADD `pieces_notif_items` VARCHAR( 200 ) NULL DEFAULT '' COMMENT 'Permet de stocker les items des pieces de notification' AFTER `code_cpv_libelle4` ;
-- OKO: 25/01/2011 - modification de la table Chorus_echange
ALTER TABLE `decisionEnveloppe` ADD `id_blob_pieces_notification` VARCHAR( 20 ) NOT NULL DEFAULT '' COMMENT 'Permet de stocker les idBLob des pièces de notification' AFTER `classement`;

--AME mantis 7208
ALTER TABLE `Service` ADD `nom_service_archiveur` VARCHAR( 100 ) NULL DEFAULT '' ;
ALTER TABLE `Service` ADD `identifiant_service_archiveur` VARCHAR( 100 ) NULL DEFAULT '' ; 

-- LCH  02/02/2011 - default value (pour le connecteur RSEM) 
ALTER TABLE `consultation` CHANGE `adresse_retrais_dossiers` `adresse_retrais_dossiers` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `caution_provisoire` `caution_provisoire` VARCHAR( 255 ) NULL ;
ALTER TABLE `consultation` CHANGE `adresse_depot_offres` `adresse_depot_offres` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `lieu_ouverture_plis` `lieu_ouverture_plis` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `prix_aquisition_plans` `prix_aquisition_plans` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `qualification` `qualification` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `agrements` `agrements` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `add_echantillion` `add_echantillion` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `add_reunion` `add_reunion` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `adresse_depot_offres_ar` `adresse_depot_offres_ar` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `lieu_ouverture_plis_ar` `lieu_ouverture_plis_ar` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `adresse_retrais_dossiers_ar` `adresse_retrais_dossiers_ar` VARCHAR( 255 ) NULL;
ALTER TABLE `consultation` CHANGE `date_limite_echantillion` `date_limite_echantillion` VARCHAR( 50 ) NULL;
ALTER TABLE `consultation` CHANGE `date_reunion` `date_reunion` VARCHAR( 50 ) NULL;
ALTER TABLE `consultation` CHANGE `variantes` `variantes` CHAR( 1 ) NULL;
ALTER TABLE `consultation` CHANGE `codes_nuts` `codes_nuts` TEXT NULL DEFAULT NULL COMMENT 'Contient les codes nuts, pour l''utilisation d''un referentiel externe' ;

ALTER TABLE `CategorieLot` CHANGE `caution_provisoire` `caution_provisoire` VARCHAR( 255 ) NULL ;
ALTER TABLE `CategorieLot` CHANGE `qualification` `qualification` VARCHAR( 255 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `agrements` `agrements` VARCHAR( 255 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `add_echantillion` `add_echantillion` VARCHAR( 255 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `add_reunion` `add_reunion` VARCHAR( 255 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `date_limite_echantillion` `date_limite_echantillion` VARCHAR( 50 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `date_reunion` `date_reunion` VARCHAR( 50 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `variantes` `variantes` CHAR( 1 ) NULL;
ALTER TABLE `CategorieLot` CHANGE `echantillon` `echantillon` ENUM( '0', '1') NOT NULL DEFAULT  '0';
ALTER TABLE `CategorieLot` CHANGE `reunion` `reunion` ENUM( '0',  '1' ) NOT NULL DEFAULT  '0';
ALTER TABLE `CategorieLot` CHANGE `visites_lieux` `visites_lieux` ENUM( '0', '1') NOT NULL DEFAULT '0';

-- OKO: 02/02/2011 - modification de la table `CertificatPermanent`
ALTER TABLE `CertificatPermanent` ADD `id_agent` INT NULL DEFAULT '0' AFTER `master_key` ;
ALTER TABLE `CertificatPermanent` ADD `CSP` VARCHAR( 200 ) NULL DEFAULT '' AFTER `ID_AGENT` ;
ALTER TABLE `CertificatPermanent` ADD `date_modification` VARCHAR( 200 ) NULL DEFAULT '' AFTER `CSP` ;

-- KAT 07/02/2011: Sppression de la table MarcheAnnee
ALTER TABLE Marche DROP FOREIGN KEY Marche_ibfk_2;
ALTER TABLE MarchePublie DROP FOREIGN KEY MarchePublie_ibfk_1;
DROP TABLE `MarcheAnnee`;

-- OKO le 09/02/2010: suppréssion des champs cpv non utilisés
ALTER TABLE `consultation` DROP `code_cpv_3`;  
ALTER TABLE `consultation`  DROP `code_cpv_4`;  
ALTER TABLE `consultation`  DROP `libelle_code_cpv_1`;  
ALTER TABLE `consultation`  DROP `libelle_code_cpv_2`;  
ALTER TABLE `consultation`  DROP `libelle_code_cpv_3`;  
ALTER TABLE `consultation`  DROP `libelle_code_cpv_4`;  
ALTER TABLE `CategorieLot` DROP `code_cpv_3`;  
ALTER TABLE `CategorieLot`  DROP `code_cpv_4`;  
ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_1`;  
ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_2`;  
ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_3`;  
ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_4`;

-- LCH 15/02/2011 : augmentation de la longueur des champs pour être compatible avec le connecteur RSEM
ALTER TABLE `AVIS` CHANGE `nom_avis` `nom_avis` VARCHAR( 200 )  NOT NULL ;
ALTER TABLE `AVIS` CHANGE `nom_fichier` `nom_fichier` VARCHAR( 200 )  NOT NULL;
ALTER TABLE `DCE` CHANGE `nom_dce` `nom_dce` VARCHAR( 150 )  NOT NULL;
ALTER TABLE `RG` CHANGE `nom_fichier` `nom_fichier` VARCHAR( 150 )  NOT NULL;

-- OKO le 18/02/2011: modification des valeurs de la tranche budgetaire.
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '90 000 - 124 999.99' WHERE `ValeurReferentiel`.`id` =15 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '90 000 HT à 124 999,99 HT' WHERE `ValeurReferentiel`.`id` =15 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = '90 000 - 124 999.99' WHERE `ValeurReferentiel`.`id` =15 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_it` = '90 000 - 124 999.99' WHERE `ValeurReferentiel`.`id` =15 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '125 000 - 192 999.99' WHERE `ValeurReferentiel`.`id` =16 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '125 000 HT à 192 999,99 HT' WHERE `ValeurReferentiel`.`id` =16 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = '125 000 - 192 999.99' WHERE `ValeurReferentiel`.`id` =16 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_it` = '125 000 - 192 999.99' WHERE `ValeurReferentiel`.`id` =16 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '193 000 - 999 999.99' WHERE `ValeurReferentiel`.`id` =17 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '193 000 HT à 999 999,99 HT' WHERE `ValeurReferentiel`.`id` =17 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = '193 000 - 999 999.99' WHERE `ValeurReferentiel`.`id` =17 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_it` = '193 000 - 999 999.99' WHERE `ValeurReferentiel`.`id` =17 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '3 000 000 - 4 844 999.99' WHERE `ValeurReferentiel`.`id` =19 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '3 000 000 HT à 4 844 999,99 HT' WHERE `ValeurReferentiel`.`id` =19 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = '3 000 000 - 4 844 999.99' WHERE `ValeurReferentiel`.`id` =19 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_it` = '3 000 000 - 4 844 999.99' WHERE `ValeurReferentiel`.`id` =19 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '4 845 000 - ...' WHERE `ValeurReferentiel`.`id` =20 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '4 845 000 HT et plus.' WHERE `ValeurReferentiel`.`id` =20 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = '4 845 000 - ...' WHERE `ValeurReferentiel`.`id` =20 AND `ValeurReferentiel`.`id_referentiel` = 2 ;
UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_it` = '4 845 000 - ...' WHERE `ValeurReferentiel`.`id` =20 AND `ValeurReferentiel`.`id_referentiel` = 2 ;

-- FBO le 24/02/2011: 7769: CNFPT : FS3 - Numérotation automatique des consultations
ALTER TABLE `Numerotation_ref_cons_auto` DROP PRIMARY KEY;
ALTER TABLE `Numerotation_ref_cons_auto` ADD `service_id` INT( 11 ) NULL DEFAULT NULL;

-- AME 22/02/2011 --
CREATE TABLE IF NOT EXISTS `Referentiel_Consultation` (`id` int(11) NOT NULL auto_increment,`reference` int(100) NOT NULL,`id_Lt_Referentiel` int(100) NOT NULL,`lot` int(11) NOT NULL default '0',`valeur_Principale_Lt_Referentiel` varchar(200) NOT NULL,`valeur_Secondaire_Lt_Referentiel` varchar(200) NOT NULL,PRIMARY KEY  (`id`)) ENGINE=InnoDB;

-- OKO le 01/03/2011: suppréssion du champ `dateMAJ` dans la table decisionEnveloppe
ALTER TABLE `decisionEnveloppe` DROP `dateMAJ`;

-- HHA le 10/03/2011 : Ajout du champs date_maj 
ALTER TABLE `DecisionLot` ADD `date_maj` VARCHAR( 20 ) NULL ;

--  MWA : 11/03 rajout formulaires XML
INSERT INTO `ReferentielTypeXml` (`id`, `id_destinataire`, `libelle_type`, `id_avis_marche`) VALUES (18, '2', 'XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire', '3');
-- OKO le 15/03/2011:
INSERT INTO `EchangeTypeMessage` (`id` ,`code` ,`corps`)VALUES ('7' , 'DEFINE_AVERTISSEMENT_ANNULATION_CONSULTATION', 'La consultation citée en référence a été annulée. Cordialement,');
-- HHA le 25/03/2011 Redimensionner le champ "champ_supp_invisible" de la table "consultation"
ALTER TABLE `consultation` CHANGE `champ_supp_invisible` `champ_supp_invisible` TEXT NOT NULL ;

-- OKO: le 03/03/2011 - 1566 modification de la table Service
ALTER TABLE `Service` ADD `chemin_complet` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `path_service_it` ;
ALTER TABLE `Service` ADD `chemin_complet_fr` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `chemin_complet` ;
ALTER TABLE `Service` ADD `chemin_complet_en` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `chemin_complet_fr` ;
ALTER TABLE `Service` ADD `chemin_complet_es` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `chemin_complet_en` ;
ALTER TABLE `Service` ADD `chemin_complet_su` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `chemin_complet_es` ;
ALTER TABLE `Service` ADD `chemin_complet_du` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `chemin_complet_su` ;
ALTER TABLE `Service` ADD `chemin_complet_cz` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `chemin_complet_du` ;
ALTER TABLE `Service` ADD `chemin_complet_ar` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `chemin_complet_cz` ;
ALTER TABLE `Service` ADD `chemin_complet_it` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `chemin_complet_ar` ;
ALTER TABLE `Service` DROP `service`;
ALTER TABLE `Service` DROP FOREIGN KEY `C16485` ;
ALTER TABLE `Service` DROP INDEX `id_acheteur_public`;
ALTER TABLE `Service` DROP `id_acheteur_public`;

ALTER TABLE `consultation` ADD `intitule` LONGTEXT NOT NULL AFTER `codes_nuts`;
ALTER TABLE `consultation` ADD `objet` LONGTEXT NOT NULL AFTER `intitule`;
ALTER TABLE `consultation` ADD `type_acces` CHAR( 1 ) NOT NULL AFTER `objet`;
ALTER TABLE `consultation` ADD `autoriser_reponse_electronique` CHAR( 1 ) NOT NULL DEFAULT '1' AFTER `type_acces`;
ALTER TABLE `consultation` ADD `regle_mise_en_ligne` INT( 11 ) NOT NULL DEFAULT '1' AFTER `autoriser_reponse_electronique`;
ALTER TABLE `consultation` ADD `id_regle_validation` INT( 11 ) NOT NULL DEFAULT '2' AFTER `regle_mise_en_ligne`;

-- AME 
ALTER TABLE `consultation` ADD `intitule_fr` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_en` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_es` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_su` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_du` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_cz` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_ar` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `intitule_it` LONGTEXT NULL ;

ALTER TABLE `consultation` ADD `objet_fr` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_en` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_es` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_su` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_du` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_cz` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_ar` LONGTEXT NULL ;
ALTER TABLE `consultation` ADD `objet_it` LONGTEXT NULL ;

-- AME 

ALTER TABLE `consultation` DROP `envoi_moniteur`;
ALTER TABLE `consultation` DROP `moniteur_pdf_API`;
ALTER TABLE `consultation` DROP `moniteur_pdf_APU`;
ALTER TABLE `consultation` DROP `moniteur_pdf_AAPC`;
ALTER TABLE `consultation` DROP `moniteur_pdf_AM`;
ALTER TABLE `consultation` DROP `moniteur_pdf_RECT`;
ALTER TABLE `consultation` DROP `moniteur_pdf_ANNU`;
ALTER TABLE `consultation` DROP `type_avis`;

ALTER TABLE `consultation` CHANGE `champ_supp_invisible` `champ_supp_invisible` TEXT NOT NULL ;

-- OKO le 05/04/2011: ajout de nouvelles tables
--
-- Structure de la table `Parametrage_Enchere`
--

CREATE TABLE IF NOT EXISTS `Parametrage_Enchere` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `refConsultation` int(10) unsigned default NULL,
  `idEntiteeAssociee` int(11) default NULL,
  `referenceUtilisateur` varchar(45) default NULL,
  `auteur` varchar(255) default NULL,
  `idLot` int(10) unsigned default NULL,
  `objet` longtext,
  `dateDebut` datetime default '0000-00-00 00:00:00',
  `dateFin` datetime default '0000-00-00 00:00:00',
  `dateSuspension` datetime default '0000-00-00 00:00:00',
  `delaiProlongation` int(11) default NULL,
  `commentaire` longtext,
  `meilleureEnchereObligatoire` enum('0','1') NOT NULL default '0',
  `typeBaremeNETC` enum('1','2','3','4') NOT NULL default '1',
  `typeBaremeEnchereGlobale` enum('1','2') NOT NULL default '1',
  `meilleurNoteHaute` enum('0','1') NOT NULL default '0',
  `nbrCandidatsVisible` enum('0','1') NOT NULL default '1',
  `listeCandidatsVisible` enum('0','1') NOT NULL default '1',
  `rangVisible` enum('0','1') NOT NULL default '1',
  `meilleureOffreVisible` enum('0','1') NOT NULL default '1',
  `montantReserve` double default NULL,
  `noteMaxBaremeRelatif` double default NULL,
  `coeffA` double default NULL,
  `coeffB` double default NULL,
  `coeffC` double default NULL,
  `mail` longtext,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `referenceUtilisateur` (`referenceUtilisateur`),
  KEY `Enchere_V2_FKIndex1` (`refConsultation`),
  KEY `Enchere_FKIndex2` (`idEntiteeAssociee`),
  KEY `idEntiteeAssociee` (`idEntiteeAssociee`)
) ENGINE=InnoDB ;

--
-- Structure de la table `Parametrage_Enchere_Reference`
--

CREATE TABLE IF NOT EXISTS `Parametrage_Enchere_Reference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `libelle` varchar(255) NOT NULL default '',
  `quantite` double default NULL,
  `isMontant` enum('0','1') NOT NULL default '1',
  `unite` varchar(20) default NULL,
  `pasMin` double default '0',
  `pasMax` double default NULL,
  `valeurReference` double default NULL,
  `valeurDepartCommune` enum('0','1') NOT NULL default '1',
  `valeurDepart` double default NULL,
  `typeBaremeReference` enum('1','2','3') default NULL,
  `ponderationNoteReference` double default '1',
  `noteMaxBaremeRelatif` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `Parametrage_Enchere_Reference_FKIndex1` (`idEnchere`)
) ENGINE=InnoDB ;

--
-- Contraintes pour la table `Parametrage_Enchere_Reference`
--
ALTER TABLE `Parametrage_Enchere_Reference`
  ADD CONSTRAINT `Parametrage_Enchere_Reference_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `Parametrage_Enchere` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
--
-- Structure de la table `EnchereTrancheBaremeReference`
--

CREATE TABLE IF NOT EXISTS `Parametrage_Enchere_Tranche_Bareme_Reference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idReference` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `Parametrage_Enchere_Tranche_Bareme_Reference_FKIndex1` (`idReference`)
) ENGINE=InnoDB ;

--
-- Contraintes pour la table `EnchereTrancheBaremeReference`
--
ALTER TABLE `Parametrage_Enchere_Tranche_Bareme_Reference`
  ADD CONSTRAINT `Parametrage_Enchere_Tranche_Bareme_Reference_ibfk_1` FOREIGN KEY (`idReference`) REFERENCES `Parametrage_Enchere_Reference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
-- OKO le 06/04/2011: ajout d'un champ dans la table 'Parametrage_Enchere'
ALTER TABLE `Parametrage_Enchere` ADD `note_entreprises` VARCHAR( 50 ) NULL COMMENT 'Permet de stocker la note commune aux entreprises lors de la phase "Parametrage".' AFTER `mail` ;

--
-- Structure de la table `Parametrage_Enchere_Tranches_Bareme_NETC`
--

CREATE TABLE IF NOT EXISTS `Parametrage_Enchere_Tranches_Bareme_NETC` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `Parametrage_Enchere_Tranches_Bareme_NETC_FKIndex1` (`idEnchere`)
) ENGINE=InnoDB;

--
-- Contraintes pour la table `Parametrage_Enchere_Tranches_Bareme_NETC`
--
ALTER TABLE `Parametrage_Enchere_Tranches_Bareme_NETC`
  ADD CONSTRAINT `Parametrage_Enchere_Tranches_Bareme_NETC_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `Parametrage_Enchere` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


-- FBO Gestion variantes pour Formulaire AE

ALTER TABLE `resultat_analyse` ADD `offre_variante` ENUM( '0', '1' ) NULL DEFAULT NULL COMMENT 'Ce champ permet de definir le montant de bese de ceux de variantes: null pour le montant d''une offre normale, 0 pour le montant de base et 1 pour les montants de variantes` ;
