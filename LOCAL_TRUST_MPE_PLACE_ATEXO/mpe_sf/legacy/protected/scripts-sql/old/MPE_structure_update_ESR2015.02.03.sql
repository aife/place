-- ASO MPE-1237
CREATE TABLE IF NOT EXISTS `t_chorus_ccag` (
  `id_ccag` int(11) NOT NULL AUTO_INCREMENT,
  `reference_ccag_chorus` varchar(3) NOT NULL,
  `libelle_ccag` varchar(250) NOT NULL,
  `reference_ccag_orme` int(11) NOT NULL,
  PRIMARY KEY (`id_ccag`),
  UNIQUE KEY `reference_ccag_chorus` (`reference_ccag_chorus`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- AME

ALTER TABLE `configuration_organisme`	ADD COLUMN `filtre_contrat_ac_sad` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d\'afficher le filtre AS/SAD et Mon entité dans la recherche des contrats' AFTER `profil_rma`;

-- LEZ MPE-1850
ALTER TABLE `GeolocalisationN2`	ADD COLUMN `code_interface` VARCHAR(255) NULL ;
-- LEZ
ALTER TABLE `t_donnee_complementaire`	CHANGE COLUMN `justification_non_alloti` `justification_non_alloti` VARCHAR(255) NULL DEFAULT NULL;

-- LEZ MPE-1850
ALTER TABLE `GeolocalisationN2`	CHANGE COLUMN `code_interface` `code_interface` VARCHAR(255) NULL DEFAULT NULL COMMENT 'contient le code ISO-3166 de chaque lieu' ;