CREATE TABLE IF NOT EXISTS `t_document_entreprise` (
`id_document` int(11) NOT NULL AUTO_INCREMENT,
`id_entreprise` int(11) NOT NULL,
`nom_document` varchar(100) NOT NULL,
`id_type_document` int(11) NOT NULL,
`id_derniere_version` int(11) NULL ,
PRIMARY KEY (`id_document`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_document_entreprise` ADD CONSTRAINT `entreprise_document` FOREIGN KEY ( `id_entreprise` ) REFERENCES `Entreprise` (`id`);

CREATE TABLE IF NOT EXISTS `t_entreprise_document_version` (
`id_version_document` int(11) NOT NULL AUTO_INCREMENT,
`id_document` int(11) NOT NULL ,
`date_recuperation` datetime NOT NULL,
`hash` varchar(500) NOT NULL,
`id_blob` int(11) NOT NULL,
PRIMARY KEY (`id_version_document`)
) ENGINE=InnoDB ;

ALTER TABLE `t_entreprise_document_version` ADD CONSTRAINT `entreprise_document_version` FOREIGN KEY ( `id_document` ) REFERENCES `t_document_entreprise` (`id_document`);

CREATE TABLE IF NOT EXISTS `t_document_type` (
`id_type_document` int(11) NOT NULL,
`nom_type_document` varchar(100) NOT NULL,
PRIMARY KEY (`id_type_document`)
) ENGINE=InnoDB ;

ALTER TABLE `t_document_entreprise` ADD CONSTRAINT `entreprise_document_type` FOREIGN KEY ( `id_type_document` ) REFERENCES `t_document_type` (`id_type_document`);

ALTER TABLE `t_entreprise_document_version` ADD COLUMN `taille_document` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `t_entreprise_document_version` ADD COLUMN `extension_document` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `configuration_plateforme` ADD COLUMN `donnees_candidat` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'gerer l''affichage des attestation de l''entreprise'  ;

ALTER TABLE `EchangePieceJointe` CHANGE `nom_fichier` `nom_fichier` VARCHAR( 255 ) NOT NULL DEFAULT '';
ALTER TABLE `EchangePieceJointe` ADD COLUMN `extension` VARCHAR( 100 ) NOT NULL DEFAULT '' COMMENT 'Extension du fichier' AFTER `nom_fichier` ;

ALTER TABLE `t_sous_critere_attribution` CHANGE COLUMN `enonce` `enonce` TEXT NULL DEFAULT NULL ;

ALTER TABLE `ProcedureEquivalence` CHANGE `marche_public_simplifie` `marche_public_simplifie` VARCHAR( 2 ) NOT NULL DEFAULT '-0';

ALTER TABLE `configuration_plateforme` ADD COLUMN `autoriser_creation_entreprise_etrangere` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''autoriser la création d''une entreprise etrangère' AFTER `donnees_candidat` ;

CREATE TABLE IF NOT EXISTS `HistoriqueNbrConsultationsPubliees` (
  `id_historique_nbr_cons` int(11) NOT NULL AUTO_INCREMENT,
  `date_publication` date NOT NULL DEFAULT '0000-00-00',
  `nbr_consultations_publiees` int(11) NOT NULL,
  PRIMARY KEY (`id_historique_nbr_cons`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- ASO MPE-639
ALTER TABLE `GeolocalisationN0`	ADD COLUMN `actif` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT 'mettre a 0 si on veut masquer ce type '  AFTER `type` ;

-- LEZ MPE-723
ALTER TABLE `DCE` ADD COLUMN `taille_dce` VARCHAR( 255 ) NULL DEFAULT NULL;

-- OKO : Structure de la table `t_appels_valides_ws_sgmap_documents`
CREATE TABLE IF NOT EXISTS `t_derniers_appels_valides_ws_sgmap_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_type_document` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `id_agent` int(11) NOT NULL,
  `date_dernier_appel_valide` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;



-- ASO MPE-1107
ALTER TABLE `T_trace_operations_inscrit_details` ALTER `descripton` DROP DEFAULT;
ALTER TABLE `T_trace_operations_inscrit_details` CHANGE COLUMN `descripton` `descripton` TEXT NOT NULL AFTER `afficher`;
ALTER TABLE `T_trace_operations_inscrit_details`	ADD COLUMN `lien_download` VARCHAR(250) NULL COMMENT 'contient l url de telechargement du pdf recap du mail du detais de la reponse envoyé à l''inscrit' AFTER `log_applet`;

-- AME
ALTER TABLE `t_donnee_complementaire`	ADD COLUMN `justification_non_alloti` VARCHAR(255) NOT NULL DEFAULT '' ;


ALTER TABLE `consultation` ADD COLUMN `clause_sociale_siae` VARCHAR(10) NULL DEFAULT '0' COMMENT 'Le marché est réservé à  SIAE' AFTER `clause_sociale_ateliers_proteges`;
ALTER TABLE `consultation` ADD COLUMN `clause_sociale_ess` VARCHAR(10) NULL DEFAULT '0' COMMENT 'Le marché est réservé à  ESS' AFTER `clause_sociale_siae`;

ALTER TABLE `CategorieLot` ADD COLUMN `clause_sociale_siae` VARCHAR(10) NULL DEFAULT '0' COMMENT 'Le marché est réservé à  SIAE' AFTER `clause_sociale_ateliers_proteges`;
ALTER TABLE `CategorieLot` ADD COLUMN `clause_sociale_ess` VARCHAR(10) NULL DEFAULT '0' COMMENT 'Le marché est réservé à  ESS' AFTER `clause_sociale_siae`;

ALTER TABLE `t_donnee_complementaire`	ADD COLUMN `variante_exigee` TINYINT(1) NULL DEFAULT NULL AFTER `variantes_autorisees`;

ALTER TABLE `t_donnee_complementaire`	CHANGE COLUMN `justification_non_alloti` `justification_non_alloti` TEXT NOT NULL DEFAULT '' AFTER `criteres_identiques`;

ALTER TABLE `configuration_organisme` CHANGE `type_contrat` `type_contrat` ENUM( '0', '1' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilité du type de contrat sur le formulaire consultation';

-- OKO le 29/07/2016 sur ESR-2015
ALTER TABLE `consultation` ADD COLUMN `pin_api_sgmap_mps` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Code PIN de la consultation via l''API MPS du SGMAP';

