-- phpMyAdmin SQL Dump
-- version 2.11.3deb1ubuntu1.1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 20 Avril 2009 à 13:00
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.4-2ubuntu5.5

SET FOREIGN_KEY_CHECKS=0;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

SET AUTOCOMMIT=0;
START TRANSACTION;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `reference_COMMON_DB`
--

-- --------------------------------------------------------

--
-- Structure de la table `Administrateur`
--

CREATE TABLE IF NOT EXISTS `Administrateur` (
  `id` int(11) NOT NULL auto_increment,
  `original_login` varchar(100) NOT NULL default '',
  `login` varchar(100) NOT NULL default '',
  `certificat` text,
  `mdp` varchar(40) NOT NULL default '',
  `nom` varchar(100) NOT NULL default '',
  `prenom` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `organisme` varchar(30) default NULL,
  `admin_general` char(1) NOT NULL default '0',
  `tentatives_mdp` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `organisme` (`organisme`)
) ENGINE=InnoDB   AUTO_INCREMENT=23 ;

--
-- Contenu de la table `Administrateur`
--

INSERT INTO `Administrateur` VALUES
(1, 'a1a', 'a1a', '', 'ATX32008', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(2, 'a2z', 'a2z', '', 'F836A9BA', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(3, 'e3r', 'e3r', '', '5ECFF512', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(4, 'a1t', 'a1t', '', 'F78DEDFG', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(5, 't5y', 't5y', '', 'F59D993B', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(6, 'u6i', 'u6i', '', 'B37F18E1', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(7, 'o8p', 'o8p', '', '128BB460', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(8, 'q1s', 'q1s', '', '23029148', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(9, 'd2f', 'd2f', '', '7E6089B4', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(10, 'g3h', 'g3h', '', 'C33EB20C', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(11, 'j4k', 'j4k', '', '4880CF1F', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(12, 'm5w', 'm5w', '', '211B364A', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(13, 'x6c', 'x6c', '', '5ABD3710', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(14, 'v5b', 'v5b', '', 'A4F67F3C', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(15, 'n2a', 'n2a', '', '7CC4BE4D', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(16, 'z5e', 'z5e', '', 'F583CCE1', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(17, 'r8t', 'r8t', '', '69FA10BC', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(18, 'y6u', 'y6u', '', '5A2C9690', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(19, 'i9o', 'i9o', '', '255E39B3', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(20, 'p2q', 'p2q', '', 'F47B15D8', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(21, 's3d', 's3d', '', 'FC20DED2', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(22, 'f4g', 'f4g', '', '80F16829', 'Nom', 'Prenom', 'email', NULL, '0', 0),
(23, 'h5j', 'h5j', '', 'E8721360', 'Nom', 'Prenom', 'email', NULL, '0', 0);

-- --------------------------------------------------------

--
-- Structure de la table `Agent`
--

CREATE TABLE IF NOT EXISTS `Agent` (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(255) default NULL,
  `email` varchar(100) NOT NULL default '',
  `mdp` varchar(40) NOT NULL default '',
  `certificat` text,
  `nom` varchar(100) NOT NULL default '',
  `prenom` varchar(100) NOT NULL default '',
  `tentatives_mdp` int(1) NOT NULL default '0',
  `organisme` varchar(30) NOT NULL default '',
  `service_id` int(11) NOT NULL default '0',
  `admin` char(1) NOT NULL default '0',
  `admin_type` char(1) NOT NULL default '0',
  `RECEVOIR_MAIL` enum('0','1') NOT NULL default '0',
  `elu` char(1) NOT NULL default '0',
  `nom_fonction` varchar(100) NOT NULL default '',
  `num_tel` varchar(20) default NULL,
  `num_fax` varchar(20) default NULL,
  `type_comm` char(1) NOT NULL default '2',
  `adr_postale` varchar(255) NOT NULL default '',
  `civilite` varchar(255) NOT NULL default '',
  `alerte_reponse_electronique` enum('0','1') NOT NULL default '0',
  `alerte_cloture_consultation` enum('0','1') NOT NULL default '0',
  `alerte_reception_message` enum('0','1') NOT NULL default '0',
  `alerte_publication_boamp` enum('0','1') NOT NULL default '0',
  `alerte_echec_publication_boamp` enum('0','1') NOT NULL default '0',
  `alerte_creation_modification_agent` enum('0','1') NOT NULL default '0',
  `date_creation` varchar(20) default NULL,
  `date_modification` varchar(20) default NULL,
  `id_initial` int(11) NOT NULL default '0',
  `id_profil_socle_externe` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `organisme` (`organisme`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Agent`
--


-- --------------------------------------------------------

--
-- Structure de la table `Agent_Service_Metier`
--

CREATE TABLE IF NOT EXISTS `Agent_Service_Metier` (
  `id_agent` int(11) NOT NULL default '0',
  `id_service_metier` int(11) NOT NULL default '0',
  `id_profil_service` int(11) NOT NULL default '0',
  `date_creation` varchar(20) default NULL,
  `date_modification` varchar(20) default NULL,
  PRIMARY KEY  (`id_agent`,`id_service_metier`),
  KEY `id_agent` (`id_agent`),
  KEY `id_service_metier` (`id_service_metier`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Agent_Service_Metier`
--


-- --------------------------------------------------------

--
-- Structure de la table `Alerte`
--

CREATE TABLE IF NOT EXISTS `Alerte` (
  `id` int(11) NOT NULL auto_increment,
  `id_inscrit` int(11) NOT NULL default '0',
  `denomination` varchar(200) NOT NULL default '',
  `periodicite` char(1) NOT NULL default '',
  `xmlCriteria` text,
  `categorie` varchar(30) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Alerte`
--


-- --------------------------------------------------------

--
-- Structure de la table `Annonce`
--

CREATE TABLE IF NOT EXISTS `Annonce` (
  `id_boamp` int(11) NOT NULL default '0',
  `organisme` varchar(100) NOT NULL default '',
  `envoi_boamp` int(11) NOT NULL default '0',
  `date_envoi` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `datepub` varchar(10) NOT NULL default '0000-00-00',
  `consultation_ref` varchar(100) NOT NULL default '',
  `nom_fichier_xml` varchar(255) NOT NULL default '',
  `envoi_joue` enum('0','1') NOT NULL default '0',
  `mapa` enum('0','1') NOT NULL default '0',
  `implique_SAD` enum('0','1') NOT NULL default '0',
  `date_maj` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id_boamp`,`organisme`),
  KEY `organisme` (`organisme`,`consultation_ref`),
  KEY `Idx_Annonce` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Annonce`
--


-- --------------------------------------------------------

--
-- Structure de la table `blob`
--

CREATE TABLE IF NOT EXISTS `blob` (
  `id` int(11) NOT NULL auto_increment,
  `revision` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `blob`
--


-- --------------------------------------------------------

--
-- Structure de la table `blob_file`
--

CREATE TABLE IF NOT EXISTS `blob_file` (
  `id` int(11) NOT NULL default '0',
  `revision` int(11) NOT NULL default '0',
  `name` text NOT NULL,
  `deletion_datetime` datetime default NULL,
  PRIMARY KEY  (`id`,`revision`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `blob_file`
--


-- --------------------------------------------------------

--
-- Structure de la table `CategorieConsultation`
--

CREATE TABLE IF NOT EXISTS `CategorieConsultation` (
  `id` varchar(30) NOT NULL default '',
  `libelle` varchar(100) NOT NULL default '',
  `libelle_fr` varchar(100) default '',
  `libelle_en` varchar(100) default '',
  `libelle_es` varchar(100) default '',
  `libelle_su` varchar(100) default '',
  `libelle_du` varchar(100) default '',
  `libelle_cz` varchar(100) default '',
  `id_categorie_ANM` varchar(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `CategorieConsultation`
--

INSERT INTO `CategorieConsultation` VALUES
('1', 'Travaux', '1', NULL, NULL, NULL, NULL, NULL, '1'),
('2', 'Fournitures', '2', NULL, NULL, NULL, NULL, NULL, '2'),
('3', 'Services', '3', NULL, NULL, NULL, NULL, NULL, '3');

-- --------------------------------------------------------

--
-- Structure de la table `CategorieINSEE`
--

CREATE TABLE IF NOT EXISTS `CategorieINSEE` (
  `id` varchar(20) NOT NULL default '',
  `libelle` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `CategorieINSEE`
--

INSERT INTO `CategorieINSEE` VALUES
('4', 'Personne morale de droit public soumise au droit commercial'),
('4.1', 'Établissement public ou régie à caractère industriel ou commercial'),
('4.1.10', 'Établissement public national à caractère industriel ou commercial doté d''un comptable public'),
('4.1.20', 'Établissement public national à caractère industriel ou commercial non doté d''un comptable public'),
('4.1.30', 'Exploitant public'),
('4.1.40', 'Établissement public local à caractère industriel ou commercial'),
('4.1.50', 'Régie d''une collectivité locale à caractère industriel ou commercial'),
('4.1.60', 'Institution Banque de France'),
('7', 'Personne morale et organisme soumis au droit administratif'),
('7.1', 'Administration de l''état'),
('7.1.11', 'Autorité constitutionnelle'),
('7.1.12', 'Autorité administrative indépendante'),
('7.1.13', 'Ministère'),
('7.1.20', 'Service central d’un ministère'),
('7.1.50', 'Service du ministère de la Défense'),
('7.1.60', 'Service déconcentré à compétence nation. D’un ministère (hors Défense)'),
('7.1.71', 'Service déconcentré de l’État à compétence (inter) régionale'),
('7.1.72', 'Service déconcentré de l’État à compétence (inter) départementale'),
('7.1.79', '(Autre) Service déconcentré de l’État à compétence territoriale'),
('7.1.90', 'Ecole nationale non dotée de la personnalité morale'),
('7.2', 'Collectivité territoriale'),
('7.2.10', 'Commune'),
('7.2.20', 'Département'),
('7.2.25', 'Territoire d’Outre-mer'),
('7.2.29', '(Autre) Collectivité territoriale'),
('7.2.30', 'Région'),
('7.3', 'Établissement public administratif'),
('7.3.12', 'Commune associée'),
('7.3.13', 'Section de commune'),
('7.3.14', 'Ensemble urbain'),
('7.3.21', 'Association syndicale autorisée'),
('7.3.22', 'Association foncière urbaine'),
('7.3.23', 'Association foncière de remembrement'),
('7.3.31', 'Établissement public local d’enseignement'),
('7.3.41', 'Secteur de commune'),
('7.3.42', 'District urbain'),
('7.3.43', 'Communauté urbaine'),
('7.3.45', 'Syndicat intercommunal à vocation multiple (SIVOM)'),
('7.3.46', 'Communauté de communes'),
('7.3.47', 'Communauté de villes'),
('7.3.48', 'Communauté d''agglomération'),
('7.3.49', 'Autre établissement public local de coopération non spécialisé ou entente'),
('7.3.51', 'Institution interdépartemental ou entente'),
('7.3.52', 'Institution interrégionale ou entente'),
('7.3.53', 'Syndicat intercommunal à vocation unique (SIVU)'),
('7.3.54', 'Syndicat mixte communal'),
('7.3.55', 'Autre syndicat mixte'),
('7.3.56', 'Commission syndicale pour la gestion des biens indivis des communes'),
('7.3.61', 'Centre communal d''action sociale'),
('7.3.62', 'Caisse des écoles'),
('7.3.63', 'Caisse de crédit municipal'),
('7.3.64', 'Établissement d''hospitalisation'),
('7.3.65', 'Syndicat inter hospitalier'),
('7.3.66', 'Établissement public local social et médico-social'),
('7.3.71', 'Office public d''habitation à loyer modéré (OPHLM)'),
('7.3.72', 'Service départemental d''incendie'),
('7.3.73', 'Établissement public local culturel'),
('7.3.79', '(Autre) Établissement public administratif local'),
('7.3.81', 'Organisme consulaire'),
('7.3.82', 'Établissement public national ayant fonction d''administration centrale'),
('7.3.83', 'Établissement public national à caractère scientifique culturel et professionnel'),
('7.3.84', 'Autre établissement public national d''enseignement'),
('7.3.85', 'Autre établissement public national administratif à compétence territoriale limitée'),
('7.3.89', 'Établissement public national à caractère administratif'),
('7.4', 'Autre personne morale de droit public administratif'),
('7.4.10', 'Groupement d''intérêt public (GIP)'),
('7.4.30', 'Établissement public des cultes d''Alsace-Lorraine'),
('7.4.50', 'Cercle et foyer dans les armées'),
('7.4.90', 'Autre personne morale de droit administratif');

-- --------------------------------------------------------

--
-- Structure de la table `CategorieLot`
--

CREATE TABLE IF NOT EXISTS `CategorieLot` (
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` varchar(255) NOT NULL default '',
  `lot` int(11) NOT NULL default '0',
  `description` varchar(1000) NOT NULL default '',
  `categorie` varchar(30) NOT NULL default '',
  `description_detail` varchar(1000) NOT NULL default '',
  `code_cpv_1` varchar(8) default NULL,
  `code_cpv_2` varchar(8) default NULL,
  `code_cpv_3` varchar(8) default NULL,
  `code_cpv_4` varchar(8) default NULL,
  `libelle_code_cpv_1` varchar(200) default NULL,
  `libelle_code_cpv_2` varchar(200) default NULL,
  `libelle_code_cpv_3` varchar(200) default NULL,
  `libelle_code_cpv_4` varchar(200) default NULL,
  `description_fr` varchar(1000) NOT NULL default '',
  `description_en` varchar(255) NOT NULL default '',
  `description_es` varchar(255) NOT NULL default '',
  `description_su` varchar(255) NOT NULL default '',
  `description_du` varchar(255) NOT NULL default '',
  `description_cz` varchar(255) NOT NULL default '',
  `description_detail_fr` varchar(1000) NOT NULL default '',
  `description_detail_en` varchar(255) default NULL,
  `description_detail_es` varchar(255) default NULL,
  `description_detail_su` varchar(255) default NULL,
  `description_detail_du` varchar(255) default NULL,
  `description_detail_cz` varchar(255) default NULL,
  `id_lot_externe` int(11) default NULL,
  PRIMARY KEY  (`organisme`,`consultation_ref`,`lot`),
  KEY `organisme` (`organisme`,`consultation_ref`),
  KEY `Idx_CategorieLot` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `CategorieLot`
--


-- --------------------------------------------------------

--
-- Structure de la table `CG76_Domaine`
--

CREATE TABLE IF NOT EXISTS `CG76_Domaine` (
  `Id` int(11) NOT NULL auto_increment,
  `libelle` varchar(150) NOT NULL,
  `Parent` smallint(5) NOT NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=MyISAM   AUTO_INCREMENT=160 ;

--
-- Contenu de la table `CG76_Domaine`
--

INSERT INTO `CG76_Domaine` VALUES
(1, 'Repères et outils fondamentaux prof.', 0),
(2, 'Management', 0),
(3, 'Affaires juridiques', 0),
(4, 'Gestion des ressources humaines', 0),
(5, 'Formation professionnelle et recherche', 0),
(6, 'Finances et gestion financière', 0),
(7, 'Informatique et systèmes d''information', 0),
(8, 'Europe et international', 0),
(9, 'Citoyenneté et population', 0),
(10, 'Social et santé', 0),
(11, 'Enfance, éducation, jeunesse', 0),
(12, 'Restauration', 0),
(13, 'Culture, archives et documentation', 0),
(14, 'Environnement', 0),
(15, 'Hygiène et sécurité', 0),
(16, 'Génie technique', 0),
(17, 'Développement local', 0),
(18, 'Sport', 0),
(19, 'Positionnement', 0),
(20, 'Tech. expression communication et relat.', 1),
(21, 'Observation du milieu professionnel', 1),
(22, 'Tech. administ.d''organis° et secrétariat', 1),
(23, 'Connaissance de l''environnement territorial', 1),
(24, 'Savoirs de base intégration vie profess', 1),
(25, 'Management stratégique', 2),
(26, 'Management organisationnel', 2),
(27, 'Management des équipes et des personnes', 2),
(28, 'Approche général du management', 2),
(29, 'Modes de gestion des services publics', 3),
(30, 'Achat public, Marchés pub et contrats', 3),
(31, 'Prévention juridique et contentieux', 3),
(32, 'Approche générale du droit', 3),
(33, 'Assurances', 3),
(34, 'Prévention juridique et contentieux', 3),
(35, 'Approche générale du droit', 3),
(36, 'Gestion administrative et statutaire', 4),
(37, 'Politiques de GRH', 4),
(38, 'Conditions de travail', 4),
(39, 'Relations sociales', 4),
(40, 'Fonction formation', 4),
(41, 'Approche générale de la GRH', 4),
(42, 'Connais. Acteurs disp polit emploi form', 5),
(43, 'Contrôle évaluation org form recherche', 5),
(44, 'Instruc organis gest form prof et rech', 5),
(45, 'Procédure budgétaire et comptable', 6),
(46, 'Gestion et stratégie financière', 6),
(47, 'Fiscalité et dotation de l''Etat', 6),
(48, 'Approche générale des finances', 6),
(49, 'Stratégie de communication', 7),
(50, 'Techniques et outils de communication', 7),
(51, 'Appr géné de la communication institut', 7),
(52, 'Architecture et admin des syst d''infos', 8),
(53, 'Réseaux et télécommunication', 8),
(54, 'Techniques informatiques', 8),
(55, 'Bureautique et utilisation outils inform', 8),
(56, 'Connaissances des institutions', 9),
(57, 'Politiques régionales', 9),
(58, 'Coopération décentralisée', 9),
(59, 'Politiques sectorielles', 9),
(60, 'Démographie', 10),
(61, 'état civil', 10),
(62, 'élections', 10),
(63, 'gestion funéraire', 10),
(64, 'affaires générales', 10),
(65, 'Connais acteurs des dispos et polit', 11),
(66, 'Protection de l''enfance et de l''adolesc.', 11),
(67, 'Insertion et développement social', 11),
(68, 'Accueil et accompagnement des pers âgées', 11),
(69, 'Prévention et promotion de la santé', 11),
(70, 'Méthodologie du travail social', 11),
(71, 'Appr. gén. de l''act. soc. et de la santé', 11),
(72, 'Accueil et accompagnement des pers handicapées', 11),
(73, 'Connais. acteurs dispositifs, politiques', 12),
(74, 'Accueil de l''enfance et de l''adolescence', 12),
(75, 'Animation enfance, jeunesse', 12),
(76, 'Organisation et production', 13),
(77, 'Hygiène et sécurité alimentaire', 13),
(78, 'Distribution et service', 13),
(79, 'Equilibre nutritionnel', 13),
(80, 'Approche génér. de la restauration', 13),
(81, 'Connais. act dispositifs, polit act cult', 14),
(82, 'Gestion et enrichissement des fonds', 14),
(83, 'Diffusion, promot, protection juridique', 14),
(84, 'Protect dév du patri architec et archéo', 14),
(85, 'Enseignement artistique', 14),
(86, 'Programmation et techniques du spectacle', 14),
(87, 'Tech de documentat et d''information', 14),
(88, 'Approche générale de l''action culturelle', 14),
(89, 'Polit géné d''environ et gest des ress.', 15),
(90, 'Résorption des nuisances et pollutions', 15),
(91, 'Production d''eau potable', 15),
(92, 'Traitement des eaux usées', 15),
(93, 'Biologie, hygiène alim et santé animale', 15),
(94, 'Collecte, trait et gestion des déchets', 15),
(95, 'Connaissance des act.,des dispo.des pol.', 15),
(96, 'Maintenance des moyens techniques', 15),
(97, 'Prév et opér d''incendie et de secours', 16),
(98, 'Sûreté et sécurité dans la ville', 16),
(99, 'Prév et gest des risques tech et nat.', 16),
(100, 'Prévention et protection du public', 16),
(101, 'Sécurité des agents au travail', 16),
(102, 'Responsabilité du personnel d?encadrement ', 16),
(103, 'Plan de prévention entreprises extérieures intervenantes', 16),
(104, 'Travail sur écran', 16),
(105, 'Fonction achat des équipements dans une logique d?ergonomie', 16),
(106, 'Formation des ACMO', 16),
(107, 'Formation des membres du CHS', 16),
(108, 'Nuisances sonores', 16),
(109, 'Equipement de protection individuelle (EPI)', 16),
(110, 'Document unique', 16),
(111, 'Prévention et Secours Civiques de niveau ?PSC1-', 16),
(112, 'SST', 16),
(113, 'SSIAP 1', 16),
(114, 'Utilisation du CMSI et du SSI', 16),
(115, 'Initiation à la lutte contre l?incendie', 16),
(116, 'Habilitations électriques', 16),
(117, 'CACES', 16),
(118, 'Signalisation temporaire des zones d?intervention', 16),
(119, 'Manutention de ponts roulants', 16),
(120, 'Risque routier', 16),
(121, 'FCOS', 16),
(122, 'Permis Poids lourd', 16),
(123, 'Soudure', 16),
(124, 'Utilisation de la débroussailleuse', 16),
(125, 'Utilisation de la tronçonneuse', 16),
(126, 'Manutentions manuelles et mécaniques de charges', 16),
(127, 'Montage et démontage des échafaudages, scènes, podiums', 16),
(128, 'Utilisation des produits phyto sanitaires', 16),
(129, 'Gestion des déchets industriels et ménagers', 16),
(130, 'Maîtrise du risque infectieux en milieu médico social', 16),
(131, 'Nettoyage et désinfection des locaux', 16),
(132, 'Fonction achat des produits d?entretien', 16),
(133, 'Gestion du stress', 16),
(134, 'Gestion des phénomènes agressifs', 16),
(135, 'HACCP', 16),
(136, 'Hygiène des aliments', 16),
(137, 'Architecture et construction', 17),
(138, 'Infrastructures et réseaux', 17),
(139, 'Espaces verts et paysage', 17),
(140, 'Maint des bâtiments tt corps d''état', 17),
(141, 'Maîtrise de l''énergie', 17),
(142, 'Maintenance des moyens techniques', 17),
(143, 'Connais. acteurs disp politiq dév territ', 18),
(144, 'Aménag, urbanisme et act foncière', 18),
(145, 'Politique de l''habitat logement social', 18),
(146, 'Action économique', 18),
(147, 'Développement touristique', 18),
(148, 'Déplacements et transports', 18),
(149, 'Connais. acteurs disp politiq sportives', 19),
(150, 'Enseignement des prat sportives', 19),
(151, 'Entretien des matériels sportifs', 19),
(152, 'Orientation formative, posit évaluation', 20),
(153, 'Remise à niveau', 20),
(154, 'Acquisition de connaissances', 20),
(155, 'Méthodo et entraînement aux épreuves', 20),
(156, 'Sensibilisation générale', 21),
(157, 'Marchés publics et développement durable', 21),
(158, 'Energies et approche environnementale', 21),
(159, 'Eco labels et certifications', 21);

-- --------------------------------------------------------

--
-- Structure de la table `CG76_Donnee_Complementaire_Domaine`
--

CREATE TABLE IF NOT EXISTS `CG76_Donnee_Complementaire_Domaine` (
  `Id` int(11) NOT NULL auto_increment,
  `IdDonneeComlementaire` int(11) NOT NULL,
  `IdDomaine` int(11) NOT NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `CG76_Donnee_Complementaire_Domaine`
--


-- --------------------------------------------------------

--
-- Structure de la table `CG76_Donnee_Complementaire_entreprise`
--

CREATE TABLE IF NOT EXISTS `CG76_Donnee_Complementaire_entreprise` (
  `Ref` smallint(6) NOT NULL auto_increment,
  `idInscrit` smallint(5) NOT NULL default '0',
  `IdEntreprise` int(11) NOT NULL default '0',
  `type_formation` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `cout_moyen_journee` varchar(10) default NULL,
  `collaboration_fpt` int(1) NOT NULL default '0',
  `collaboration_fpe` int(1) NOT NULL default '0',
  `centre_documentation` int(1) NOT NULL default '0',
  `service_reprographie` int(1) NOT NULL default '0',
  `salle_info` int(1) NOT NULL default '0',
  `salle_cours` int(1) NOT NULL default '0',
  `aire_geo_inter` varchar(255) default NULL,
  `commentaire` text,
  PRIMARY KEY  (`Ref`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `CG76_Donnee_Complementaire_entreprise`
--


-- --------------------------------------------------------

--
-- Structure de la table `CG76_PieceJointe`
--

CREATE TABLE IF NOT EXISTS `CG76_PieceJointe` (
  `Ref` int(11) NOT NULL auto_increment,
  `IdPJ` int(11) NOT NULL default '0',
  `idEntreprise` int(11) NOT NULL default '0',
  PRIMARY KEY  (`Ref`)
) ENGINE=MyISAM   AUTO_INCREMENT=48 ;

--
-- Contenu de la table `CG76_PieceJointe`
--


-- --------------------------------------------------------

--
-- Structure de la table `changement_heure`
--

CREATE TABLE IF NOT EXISTS `changement_heure` (
  `organisme` varchar(30) NOT NULL default '',
  `date_debut_zone` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_fin_zone` datetime NOT NULL default '0000-00-00 00:00:00',
  `decalage` char(3) NOT NULL default '0',
  PRIMARY KEY  (`date_debut_zone`,`date_fin_zone`,`organisme`),
  KEY `organisme` (`organisme`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `changement_heure`
--


-- --------------------------------------------------------

--
-- Structure de la table `Commission`
--

CREATE TABLE IF NOT EXISTS `Commission` (
  `id` int(11) NOT NULL auto_increment,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `lieu` varchar(200) NOT NULL default '',
  `salle` varchar(100) NOT NULL default '',
  `type` varchar(200) NOT NULL default '',
  `libelle` varchar(250) default NULL,
  `ordre_du_jour` longblob,
  `invitations_zip` longblob,
  `status_cao` char(2) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Commission`
--


-- --------------------------------------------------------

--
-- Structure de la table `consultation`
--

CREATE TABLE IF NOT EXISTS `consultation` (
  `organisme` varchar(30) NOT NULL default '',
  `reference` varchar(255) NOT NULL default '',
  `reference_utilisateur` varchar(255) NOT NULL default '',
  `categorie` varchar(30) NOT NULL default '0',
  `titre` longtext NOT NULL,
  `resume` longtext NOT NULL,
  `datedebut` date NOT NULL default '0000-00-00',
  `datefin` datetime NOT NULL default '0000-00-00 00:00:00',
  `datevalidation` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `type_procedure` char(1) NOT NULL default '',
  `code_procedure` varchar(15) default NULL,
  `reponse_electronique` char(1) NOT NULL default '1',
  `num_procedure` int(1) NOT NULL default '0',
  `id_type_procedure` int(1) NOT NULL default '0',
  `id_type_avis` int(2) NOT NULL default '0',
  `lieu_execution` text NOT NULL,
  `type_mise_en_ligne` int(11) NOT NULL default '1',
  `datemiseenligne` datetime NOT NULL default '0000-00-00 00:00:00',
  `is_tiers_avis` char(1) NOT NULL default '0',
  `url` varchar(255) NOT NULL default '',
  `datefin_sad` datetime NOT NULL default '0000-00-00 00:00:00',
  `is_sys_acq_dyn` int(11) NOT NULL default '0',
  `reference_consultation_init` varchar(250) NOT NULL default '',
  `signature_offre` char(1) default NULL,
  `id_type_validation` int(11) NOT NULL default '2',
  `etat_approbation` enum('0','1') NOT NULL default '0',
  `etat_validation` enum('0','1') NOT NULL default '0',
  `champ_supp_invisible` varchar(100) NOT NULL default '',
  `code_cpv_1` varchar(8) default NULL,
  `code_cpv_2` varchar(8) default NULL,
  `code_cpv_3` varchar(8) default NULL,
  `code_cpv_4` varchar(8) default NULL,
  `libelle_code_cpv_1` varchar(200) default NULL,
  `libelle_code_cpv_2` varchar(200) default NULL,
  `libelle_code_cpv_3` varchar(200) default NULL,
  `libelle_code_cpv_4` varchar(200) default NULL,
  `titre_fr` longtext,
  `titre_en` longtext,
  `titre_es` longtext,
  `titre_su` longtext,
  `titre_du` longtext,
  `titre_cz` longtext,
  `resume_fr` longtext,
  `resume_en` longtext,
  `resume_es` longtext,
  `resume_su` longtext,
  `resume_du` longtext,
  `resume_cz` longtext,
  `publication_europe` enum('0','1') default '0',
  `etat_publication` int(11) NOT NULL default '0',
  `poursuivre_affichage` int(3) NOT NULL default '0',
  `nbr_telechargement_dce` int(5) default NULL,
  `service_id` int(11) default NULL,
  `service_associe_id` int(11) default NULL,
  `detail_consultation` text NOT NULL,
  `date_fin_affichage` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `depouillable_phase_consultation` enum('0','1') NOT NULL default '0',
  `consultation_transverse` enum('0','1') NOT NULL default '0',
  `consultation_achat_publique` enum('0','1') NOT NULL default '0',
  `url_consultation_achat_publique` varchar(100) default NULL,
  `partial_dce_download` char(1) NOT NULL default '0',
  `tirage_plan` int(11) NOT NULL default '0',
  `tireur_plan` int(11) NOT NULL default '0',
  `date_mise_en_ligne_calcule` datetime default NULL,
  `accessibilite_en` char(1) NOT NULL default '0',
  `accessibilite_es` char(1) NOT NULL default '0',
  `nbr_reponse` int(5) default NULL,
  `id_type_procedure_org` int(1) NOT NULL default '0',
  `organisme_consultation_init` varchar(255) NOT NULL default '',
  `tirage_descriptif` mediumtext NOT NULL,
  `date_validation_intermediaire` varchar(20) default NULL,
  `accessibilite_fr` enum('0','1') NOT NULL default '0',
  `accessibilite_cz` enum('0','1') NOT NULL default '0',
  `accessibilite_du` enum('0','1') NOT NULL default '0',
  `accessibilite_su` enum('0','1') NOT NULL default '0',
  `alloti` enum('0','1') NOT NULL default '0',
  `numero_phase` int(3) NOT NULL default '0',
  `consultation_migration` enum('0','1') NOT NULL default '0',
  `url_consultation_migration` text,
  PRIMARY KEY  (`organisme`,`reference`),
  KEY `organisme` (`organisme`),
  KEY `reference` (`reference`),
  KEY `Idx_Consultation_Categorie` (`categorie`),
  KEY `Idx_Consultation_CAP` (`consultation_achat_publique`),
  KEY `Idx_Consultation_Datevalid` (`datevalidation`),
  KEY `Idx_Consultation_EA` (`etat_approbation`),
  KEY `Idx_Consultation_DVI` (`date_validation_intermediaire`),
  KEY `Idx_Consultation_RU` (`reference_utilisateur`),
  KEY `Idx_Consultation_Datefin` (`datefin`),
  KEY `Idx_Consultation_ITP` (`id_type_procedure`),
  KEY `Idx_Consultation_ITA` (`id_type_avis`),
  KEY `Idx_Consultation_TML` (`type_mise_en_ligne`),
  KEY `Idx_Consultation_DMLC` (`date_mise_en_ligne_calcule`),
  KEY `Idx_Consultation_Datemiseenligne` (`datemiseenligne`),
  KEY `Idx_Consultation_Code_CPV` (`code_cpv_1`,`code_cpv_2`,`code_cpv_3`,`code_cpv_4`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `consultation`
--


-- --------------------------------------------------------

--
-- Structure de la table `DateLancementCron`
--

CREATE TABLE IF NOT EXISTS `DateLancementCron` (
  `id` int(11) NOT NULL auto_increment,
  `mise_a_jour_socle` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  COMMENT='cette table enregistre la derniere date de lancement de cron' AUTO_INCREMENT=1 ;

--
-- Contenu de la table `DateLancementCron`
--


-- --------------------------------------------------------

--
-- Structure de la table `Departement`
--

CREATE TABLE IF NOT EXISTS `Departement` (
  `id_departement` int(11) NOT NULL auto_increment,
  `nom_departement` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id_departement`)
) ENGINE=MyISAM  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Departement`
--


-- --------------------------------------------------------

--
-- Structure de la table `DocumentsAttaches`
--

CREATE TABLE IF NOT EXISTS `DocumentsAttaches` (
  `id_document` int(11) NOT NULL default '0',
  `nom_document` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id_document`),
  KEY `id_document` (`id_document`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `DocumentsAttaches`
--

INSERT INTO `DocumentsAttaches` VALUES
(1, 'Certificats fiscaux 3666 volets 1 à 4'),
(2, 'Certificats sociaux'),
(3, 'K-BIS'),
(4, 'DC7'),
(5, 'Attestation d''assurance'),
(6, 'Certificat de congés payés');

-- --------------------------------------------------------

--
-- Structure de la table `Entreprise`
--

CREATE TABLE IF NOT EXISTS `Entreprise` (
  `id` int(11) NOT NULL auto_increment,
  `admin_id` int(11) NOT NULL default '0',
  `siren` varchar(9) NOT NULL default '',
  `repmetiers` char(1) NOT NULL default '',
  `nom` varchar(30) NOT NULL default '',
  `adresse` varchar(80) NOT NULL default '',
  `codepostal` varchar(5) NOT NULL default '',
  `villeadresse` varchar(50) NOT NULL default '',
  `paysadresse` varchar(50) NOT NULL default '',
  `email` varchar(100) default NULL,
  `taille` int(2) default NULL,
  `formejuridique` varchar(6) NOT NULL default '',
  `villeenregistrement` varchar(50) default NULL,
  `motifNonIndNum` int(11) default NULL,
  `ordreProfOuAgrement` varchar(30) default NULL,
  `dateConstSociete` date default NULL,
  `nomOrgInscription` varchar(30) default NULL,
  `adrOrgInscription` varchar(80) default NULL,
  `dateConstAssoc` date default NULL,
  `dateConstAssocEtrangere` date default NULL,
  `nomPersonnePublique` varchar(30) default NULL,
  `nationalite` char(2) default NULL,
  `redressement` int(11) default NULL,
  `paysenregistrement` varchar(50) default NULL,
  `sirenEtranger` varchar(20) default NULL,
  `numAssoEtrangere` varchar(20) default NULL,
  `debutExerciceGlob1` varchar(15) default '',
  `finExerciceGlob1` varchar(15) default '',
  `debutExerciceGlob2` varchar(15) default '',
  `finExerciceGlob2` varchar(15) default '',
  `debutExerciceGlob3` varchar(15) default '',
  `finExerciceGlob3` varchar(15) default '',
  `ventesGlob1` varchar(10) default '',
  `ventesGlob2` varchar(10) default '',
  `ventesGlob3` varchar(10) default '',
  `biensGlob1` varchar(10) default '',
  `biensGlob2` varchar(10) default '',
  `biensGlob3` varchar(10) default '',
  `servicesGlob1` varchar(10) default '',
  `servicesGlob2` varchar(10) default '',
  `servicesGlob3` varchar(10) default '',
  `totalGlob1` varchar(10) default '',
  `totalGlob2` varchar(10) default '',
  `totalGlob3` varchar(10) default '',
  `codeape` varchar(5) NOT NULL,
  `origine_compte` varchar(255) default NULL,
  `telephone` varchar(50) default NULL,
  `fax` varchar(50) default NULL,
  `site_internet` varchar(50) NOT NULL default '',
  `description_activite` varchar(255) NOT NULL default '',
  `activite_domaine_defense` varchar(255) NOT NULL default '',
  `annee_cloture_exercice1` varchar(15) NOT NULL default '',
  `annee_cloture_exercice2` varchar(15) NOT NULL default '',
  `annee_cloture_exercice3` varchar(15) NOT NULL default '',
  `effectif_moyen1` int(10) NOT NULL default '0',
  `effectif_moyen2` int(10) NOT NULL default '0',
  `effectif_moyen3` int(10) NOT NULL default '0',
  `effectif_encadrement1` int(10) NOT NULL default '0',
  `effectif_encadrement2` int(10) NOT NULL default '0',
  `effectif_encadrement3` int(10) NOT NULL default '0',
  `pme1` enum('1','0') default NULL,
  `pme2` enum('1','0') default NULL,
  `pme3` enum('1','0') default NULL,
  `adresse2` varchar(80) default NULL,
  `nicSiege` varchar(5) default NULL,
  `acronyme_pays` varchar(10) default NULL,
  `date_creation` varchar(20) NOT NULL,
  `date_modification` varchar(20) NOT NULL,
  `id_initial` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB   AUTO_INCREMENT=4 ;

--
-- Contenu de la table `Entreprise`
--

INSERT INTO `Entreprise` VALUES
(1, 0, '444914568', '', 'SOCIETEST', '231, rue Saint-Honoré', '75001', 'Paris', 'France', NULL, NULL, 'SA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FR', '', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '6202A', NULL, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '', '00019', 'FR', '', '', 0),
(2, 0, '313955866', '', 'ENTREDEMO', '6 Pl d''Alleray', '75015', 'PARIS 15', 'France', NULL, NULL, 'SARL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FR', '', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '5630Z', NULL, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '', '00014', 'FR', '', '', 0),
(3, 0, '451491609', '', 'COMPANESSAI', '1 Place Carpeaux', '92800', 'PUTEAUX', 'France', NULL, NULL, 'SARL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FR', '', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '748H', NULL, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '', '00017', 'FR', '', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `EntrepriseInsee`
--

CREATE TABLE IF NOT EXISTS `EntrepriseInsee` (
  `id` int(30) NOT NULL auto_increment,
  `siren` varchar(9) default NULL,
  `raisonSociale` varchar(200) default NULL,
  `nbEtA` int(1) default NULL,
  `etat` char(1) default NULL,
  `etatLib` varchar(40) default NULL,
  `etatDebDate` varchar(20) default NULL,
  `eff3112Tr` int(11) default NULL,
  `eff3112TrLib` varchar(200) default NULL,
  `effAn` varchar(10) default NULL,
  `apen` varchar(5) default NULL,
  `apenLib` varchar(200) default NULL,
  `cj` varchar(4) default NULL,
  `cjLib` varchar(200) default NULL,
  `indNDC` char(1) default NULL,
  `indDoublon` char(1) default NULL,
  `indPurge` char(1) default NULL,
  `nicSiege` varchar(5) default '00000',
  `etatSiege` char(1) default NULL,
  `etatSiegeLib` varchar(10) default NULL,
  `etatDebDateSiege` varchar(20) default NULL,
  `eff3112TrSiege` int(11) default NULL,
  `eff3112TrSiegeLib` varchar(200) default NULL,
  `effAnSiege` varchar(10) default NULL,
  `apetSiege` varchar(5) default NULL,
  `apetSiegeLib` varchar(200) default NULL,
  `trtDerDateSiege` varchar(20) default NULL,
  `adrEtVoieNum` varchar(100) default NULL,
  `adrEtVoieType` varchar(100) default NULL,
  `adrEtVoieLib` varchar(100) default NULL,
  `adrEtDepCom` varchar(100) default NULL,
  `adrEtComLib` varchar(100) default NULL,
  `adrEtCodePost` varchar(100) default NULL,
  `adrEtPost1` varchar(200) default NULL,
  `adrEtPost2` varchar(200) default NULL,
  `adrEtPost3` varchar(200) default NULL,
  `nic` varchar(5) default '00000',
  `etatEt` char(2) default NULL,
  `etatDebDateEt` varchar(15) default NULL,
  `catEt` char(2) default NULL,
  `catEtLib` varchar(200) default NULL,
  `eff3112TrEt` char(2) default NULL,
  `eff3112TrEtLib` varchar(100) default NULL,
  `effAnEt` varchar(15) default NULL,
  `apet` varchar(5) default NULL,
  `apetLib` varchar(200) default NULL,
  `trtDerDateEt` varchar(15) default NULL,
  `etatEtLib` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `EntrepriseInsee`
--


-- --------------------------------------------------------

--
-- Structure de la table `EtatConsultation`
--

CREATE TABLE IF NOT EXISTS `EtatConsultation` (
  `id_etat` int(11) NOT NULL auto_increment,
  `libelle_etat` varchar(200) NOT NULL default '',
  `abreviation_etat` varchar(20) NOT NULL default '',
  `visible` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id_etat`)
) ENGINE=MyISAM   AUTO_INCREMENT=7 ;

--
-- Contenu de la table `EtatConsultation`
--

INSERT INTO `EtatConsultation` VALUES
(1, 'Préparation', 'P', 0),
(2, 'Consultation', 'C', 0),
(3, 'Ouverture et Analyse', 'OA', 1),
(4, 'Décision', 'D', 1),
(5, 'A archiver', 'AA', 1),
(6, 'Archive r&eacute;alis&eacute;e', 'AR', 1);

-- --------------------------------------------------------

--
-- Structure de la table `Faq_Entreprise`
--

CREATE TABLE IF NOT EXISTS `Faq_Entreprise` (
  `id` int(11) NOT NULL auto_increment,
  `question` text NOT NULL,
  `question_fr` text NOT NULL,
  `question_en` text NOT NULL,
  `question_es` text NOT NULL,
  `reponse` text NOT NULL,
  `reponse_fr` text NOT NULL,
  `reponse_en` text NOT NULL,
  `reponse_es` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB   AUTO_INCREMENT=9 ;

--
-- Contenu de la table `Faq_Entreprise`
--

INSERT INTO `Faq_Entreprise` VALUES
(1, 'Que faire si l''applet Java ne se charge pas ?', 'Que faire si l''applet Java ne se charge pas ?', 'How to proceed if the Java applet does not load?', '¿Qué pasa si el applet de Java no se carga?', '<p>Vérifier que votre poste est conforme aux "Conditions d''utilisation" du site qui présente les pré-requis d''installation et de configuration des postes de travail (ex : version de la machine java, fichiers à installer, etc.).</p>', '<p>Vérifier que votre poste est conforme aux "Conditions d''utilisation" du site qui présente les pré-requis d''installation et de configuration des postes de travail (ex : version de la machine java, fichiers à installer, etc.).</p>', '<p>Make sure your equipment and system meet all the requirements defined in the "Terms of use" section (e.g.: Java version, file download, etc.).</p>', '<p>Compruebe que su puesto está en conformidad con "las condiciones de uso" del sitio que presenta los requisitos previos de instalación y configuración de los puestos de trabajo (por ejemplo, la versión de la máquina para instalar los archivos java, etc.).</p>'),
(2, 'Que dois-je faire lorsque lors de la remise d''une réponse, le message suivant s''affiche "Les fichiers d''autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d''utilisation?" ?', 'Que dois-je faire lorsque lors de la remise d''une réponse, le message suivant s''affiche "Les fichiers d''autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d''utilisation?" ?', 'How to proceed if I get the following message while submitting a bid "Les fichiers d''autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d''utilisation"?', '¿Qué debo hacer cuando al momento de presentar una respuesta, el siguiente mensaje "Les fichiers d''autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d''utilisation" ?', '<p>Les "Conditions d''utilisation" indiquent la marche à suivre pour installer ces fichiers manquants.</p>', '<p>Les "Conditions d''utilisation" indiquent la marche à suivre pour installer ces fichiers manquants.</p>', '<p>You will find the procedure to follow in the "Terms of use" section in order to install the missing files.</p>', '<p>Las "Condiciones de utilización" muestran cómo instalar estos archivos que faltan.</p>'),
(3, 'Que faire si j''ai oublié mon identifiant ou mon mot de passe de connexion ?', 'Que faire si j''ai oublié mon identifiant ou mon mot de passe de connexion ?', 'How to proceed if I have lost my ID and password?', '¿Qué sucede si olvido mi nombre de usuario o mi contraseña de inicio de sesión?', '<p>La fonction "Mot de passe oublié" vous permet de recevoir votre identifiant et votre nouveau mot de passe.</p>', '<p>La fonction "Mot de passe oublié" vous permet de recevoir votre identifiant et votre nouveau mot de passe.</p>', '<p>Click the "Forgotten password" link and fill in your e-mail address to retrieve your ID and password.</p>', '<p>La función "¿Olvidaste tu contraseña?" le permite recibir su login y contraseña.</p>'),
(4, 'Que faire si je ne retrouve pas une consultation restreinte sur la PMI ?', 'Que faire si je ne retrouve pas une consultation restreinte sur la PMI ?', 'I cannot find a restricted tender on the PMI website. What can I do?', '¿Qué sucede si no se encuentra un procedimiento restringido?', '<p>Il est nécessaire d''utiliser le moteur de recherche avancée avec la référence et le code d''accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l''assistance téléphonique.</p>', '<p>Il est nécessaire d''utiliser le moteur de recherche avancée avec la référence et le code d''accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l''assistance téléphonique.</p>', '<p>Use the advanced search page to search the reference and the access code provided by the public entity.<br />You will then have access to the restricted tender.<br />If you encounter any problem, please contact our hotline.</p>', '<p>Es necesario utilizar la búsqueda avanzada con la referencia y el código de acceso transmitido por la autoridad contratante.<br />Usted tendrá acceso a los datos del procedimiento restringido.<br />En caso de dificultad, póngase en contacto con la línea directa.</p>'),
(5, 'Est ce que l''entreprise reçoit un AR quand elle pose une question via la plate-forme ?', 'Est ce que l''entreprise reçoit un AR quand elle pose une question via la plate-forme ?', 'Does the company receive an AR (acknowledgement of receipt) when submitting a question on the platform?', '¿La empresa recibe un AR cuando ella hace una pregunta a través de la plataforma?', '<p>Non. Seuls les dépôts de plis donnent lieu à un accusé de réception.</p>', '<p>Non. Seuls les dépôts de plis donnent lieu à un accusé de réception.</p>', '<p>No. An acknowledgement of receipt is sent to those submitting a bid only.</p>', '<p>No. Sólo los depósitos de pliegues dan lugar a un recibo.</p>'),
(6, 'Pourquoi je ne retrouve pas une consultation publique sur le portail ?', 'Pourquoi je ne retrouve pas une consultation publique sur le portail ?', 'Why can not I find a public tender on the portal?', '¿Por qué no puedo encontrar una consulta pública en el portal?', '<p>Veuillez vérifier que la date limite de remise des plis n''est pas déjà passée. Si ce n''est pas le cas, vérifiez votre requête ou assurez-vous que la consultation n''a pas été retirée par le Pouvoir adjudicateur.</p>', '<p>Veuillez vérifier que la date limite de remise des plis n''est pas déjà passée. Si ce n''est pas le cas, vérifiez votre requête ou assurez-vous que la consultation n''a pas été retirée par le Pouvoir adjudicateur.</p>', '<p>Please make sure the closing date has not passed. Otherwise, make sure the tender has not been removed by the public entity.</p>', '<p>Por favor, verifique que la fecha límite para la presentación de los pliegues no ha pasado. Si este no es el caso, compruebe su solicitud o asegúrese de que la consulta no ha sido retirada por la Autoridad contratante.</p>'),
(7, 'Pourquoi n''ai-je pas reçu d''accusé de réception par courriel après mon dépôt de pli ?', 'Pourquoi n''ai-je pas reçu d''accusé de réception par courriel après mon dépôt de pli ?', 'Why haven''t I received any AR by mail after I submitted my envelop?', '¿Por qué no recibo un acuse por correo después de la presentación de mi pliego?', '<p>L''application vous envoie un AR par courriel lorsque l''écran de confirmation de bonne réception de votre envoi s''affiche.\r\nVérifier que votre adresse électronique a été correctement saisie dans votre compte sur la plate-forme, ou que ce courriel ne se trouve pas dans vos courriels indésirables.</p>', '<p>L''application vous envoie un AR par courriel lorsque l''écran de confirmation de bonne réception de votre envoi s''affiche.\r\nVérifier que votre adresse électronique a été correctement saisie dans votre compte sur la plate-forme, ou que ce courriel ne se trouve pas dans vos courriels indésirables.</p>', '<p>You will receive an AR by mail once a confirmation message is displayed on screen. <br />Make sure the email address has been correctly entered in your account or check your spam folder.</p>', '<p>La aplicación le envía un AR por correo electrónico cuando la pantalla de confirmación de recibo de su envío aparece. <br />Compruebe que su dirección de correo electrónico está escrito correctamente en su cuenta en la plataforma, o que el correo electrónico no está en su correo basura.</p>'),
(8, 'Les enveloppes de candidature ou d''offre peuvent-elles être envoyées au format PDF?', 'Les enveloppes de candidature ou d''offre peuvent-elles être envoyées au format PDF?', 'Can the application /bid envelops be sent in a PDF format?', '¿Los sobres de candidatura u ofertas pueden ser enviados en formato PDF?', '<p>Non. Seul le format ZIP est accepté.</p>', '<p>Non. Seul le format ZIP est accepté.</p>', '<p>No. ZIP is the only format allowed.</p>', '<p>No. Sólo el formato ZIP es aceptado.</p>');

-- --------------------------------------------------------

--
-- Structure de la table `formejuridique`
--

CREATE TABLE IF NOT EXISTS `formejuridique` (
  `formejuridique` varchar(8) NOT NULL default '',
  PRIMARY KEY  (`formejuridique`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `formejuridique`
--

INSERT INTO `formejuridique` VALUES
('Autre'),
('EURL'),
('GIE'),
('SA'),
('SARL'),
('SAS'),
('SASU'),
('SNC');

-- --------------------------------------------------------

--
-- Structure de la table `GeolocalisationN0`
--

CREATE TABLE IF NOT EXISTS `GeolocalisationN0` (
  `id` int(11) NOT NULL auto_increment,
  `denomination` varchar(40) NOT NULL default '',
  `denomination_fr` varchar(40) NOT NULL,
  `denomination_en` varchar(40) NOT NULL,
  `denomination_es` varchar(40) NOT NULL,
  `type` enum('0','1') NOT NULL default '0',
  `libelle_selectionner` varchar(40) NOT NULL default '',
  `libelle_selectionner_fr` varchar(40) NOT NULL,
  `libelle_selectionner_en` varchar(40) NOT NULL,
  `libelle_selectionner_es` varchar(40) NOT NULL,
  `libelle_tous` varchar(40) NOT NULL default '',
  `libelle_tous_fr` varchar(40) NOT NULL,
  `libelle_tous_en` varchar(40) NOT NULL,
  `libelle_tous_es` varchar(40) NOT NULL,
  `libelle_Aucun` varchar(50) NOT NULL default '',
  `libelle_Aucun_fr` varchar(50) NOT NULL,
  `libelle_Aucun_en` varchar(50) NOT NULL,
  `libelle_Aucun_es` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB   AUTO_INCREMENT=4 ;

--
-- Contenu de la table `GeolocalisationN0`
--

INSERT INTO `GeolocalisationN0` VALUES
(1, 'France métropolitaine', 'France métropolitaine', 'Metropolitan France', 'Francia Metropolitana', '0', 'Sélectionner par département(s)', 'Sélectionner par département(s)', 'Select by department(s)', 'Seleccionar por Departamento(s)', 'Tous les départements', 'Tous les départements', 'All departments', 'Todos los Departamentos', 'Aucun département', 'Aucun département', 'No Department', 'Ningún Departamento'),
(2, 'France - Collectivité(s) d''Outre-Mer', 'France - Collectivité(s) d''Outre-Mer', 'France - overseas community', 'Francia - Colectividade(s) de Ultra-Mar', '0', 'Sélectionner par collectivité(s)', 'Sélectionner par collectivité(s)', 'Select by community', 'Seleccionar por Colectividade(s)', 'Toutes les collectivités', 'Toutes les collectivités', 'All community', 'Todas las Colectividades', 'Aucune collectivité', 'Aucune collectivité', 'No community', 'Ninguna Colectividad'),
(3, 'Autre(s) pays', 'Autre(s) pays', 'Others countries', 'Otro(s) pais(es)', '1', 'Sélectionner par pays', 'Sélectionner par pays', 'Select by country', 'Seleccionar por pais(es)', 'Tous les pays', 'Tous les pays', 'All countries', 'Todos los pais(es)', 'Aucun pays', 'Aucun pays', 'No countries', 'Ningún pais');

-- --------------------------------------------------------

--
-- Structure de la table `GeolocalisationN1`
--

CREATE TABLE IF NOT EXISTS `GeolocalisationN1` (
  `id` int(11) NOT NULL auto_increment,
  `id_geolocalisationN0` int(11) NOT NULL default '0',
  `denomination1` varchar(100) NOT NULL,
  `denomination2` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_geolocalisationN0` (`id_geolocalisationN0`)
) ENGINE=InnoDB   AUTO_INCREMENT=293 ;

--
-- Contenu de la table `GeolocalisationN1`
--

INSERT INTO `GeolocalisationN1` VALUES
(40, 1, 'Alsace', ''),
(41, 1, 'Aquitaine', ''),
(42, 1, 'Auvergne', ''),
(43, 1, 'Basse-Normandie', ''),
(44, 1, 'Bourgogne', ''),
(45, 1, 'Bretagne', ''),
(46, 1, 'Centre', ''),
(48, 1, 'Corse', ''),
(49, 1, 'Franche-Comté', ''),
(50, 1, 'Haute-Normandie', ''),
(51, 1, 'Champagne-Ardenne', ''),
(52, 1, 'Ile-de-France', ''),
(53, 1, 'Languedoc-Roussillon', ''),
(54, 1, 'Limousin', ''),
(55, 1, 'Lorraine', ''),
(56, 1, 'Midi-Pyrénées', ''),
(57, 1, 'Nord-Pas-de-Calais', ''),
(58, 1, 'Pays-de-la-Loire', ''),
(59, 1, 'Picardie', ''),
(60, 1, 'Poitou-Charentes', ''),
(61, 1, 'Provence-Alpes Côte d''Azur', ''),
(62, 1, 'Rhône-Alpes', ''),
(233, 2, 'Départements d''Outre-Mer', ''),
(234, 3, 'Pays', ''),
(235, 3, 'Pays', ''),
(236, 3, 'Pays', ''),
(237, 3, 'Pays', ''),
(238, 3, 'Pays', ''),
(239, 3, 'Pays', ''),
(240, 3, 'Pays', ''),
(241, 3, 'Pays', ''),
(242, 3, 'Pays', ''),
(243, 3, 'Pays', ''),
(244, 3, 'Pays', ''),
(245, 3, 'Pays', ''),
(246, 3, 'Pays', ''),
(247, 3, 'Pays', ''),
(248, 3, 'Pays', ''),
(249, 3, 'Pays', ''),
(250, 3, 'Pays', ''),
(251, 3, 'Pays', ''),
(252, 3, 'Pays', ''),
(253, 3, 'Pays', ''),
(254, 3, 'Pays', ''),
(255, 3, 'Pays', ''),
(256, 3, 'Pays', ''),
(257, 3, 'Pays', ''),
(258, 3, 'Pays', ''),
(259, 3, 'Pays', ''),
(260, 3, 'Pays', ''),
(261, 3, 'Pays', ''),
(262, 3, 'Pays', ''),
(263, 3, 'Pays', ''),
(264, 3, 'Pays', ''),
(265, 3, 'Pays', ''),
(266, 3, 'Pays', ''),
(267, 3, 'Pays', ''),
(268, 3, 'Pays', ''),
(269, 3, 'Pays', ''),
(270, 3, 'Pays', ''),
(271, 3, 'Pays', ''),
(272, 3, 'Pays', ''),
(273, 3, 'Pays', ''),
(274, 3, 'Pays', ''),
(275, 3, 'Pays', ''),
(276, 3, 'Pays', ''),
(277, 3, 'Pays', ''),
(278, 3, 'Pays', ''),
(279, 3, 'Pays', ''),
(280, 3, 'Pays', ''),
(281, 3, 'Pays', ''),
(282, 3, 'Pays', ''),
(283, 3, 'Pays', ''),
(284, 3, 'Pays', ''),
(285, 3, 'Pays', ''),
(286, 3, 'Pays', ''),
(287, 3, 'Pays', ''),
(288, 3, 'Pays', ''),
(289, 3, 'Pays', ''),
(290, 3, 'Pays', ''),
(291, 3, 'Pays', ''),
(292, 3, 'Pays', '');

-- --------------------------------------------------------

--
-- Structure de la table `GeolocalisationN2`
--

CREATE TABLE IF NOT EXISTS `GeolocalisationN2` (
  `id` int(11) NOT NULL auto_increment,
  `id_geolocalisationN1` int(11) NOT NULL default '0',
  `denomination1` varchar(50) NOT NULL,
  `denomination2` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id_geolocalisationN1` (`id_geolocalisationN1`)
) ENGINE=InnoDB   AUTO_INCREMENT=6470 ;

--
-- Contenu de la table `GeolocalisationN2`
--

INSERT INTO `GeolocalisationN2` (`id`, `id_geolocalisationN1`, `denomination1`, `denomination2`) VALUES
(214, 40, '(67) Bas-Rhin', '67'),
(215, 40, '(68) Haut-Rhin', '68'),
(216, 41, '(24) Dordogne', '24'),
(217, 41, '(33) Gironde', '33'),
(218, 41, '(40) Landes', '40'),
(219, 41, '(47) Lot-et-Garonne', '47'),
(220, 41, '(64) Pyrénées-Atlantiques', '64'),
(221, 42, '(3) Allier', '3'),
(222, 42, '(15) Cantal', '15'),
(223, 42, '(43) Haute-Loire', '43'),
(224, 42, '(63) Puy-de-Dôme', '63'),
(225, 43, '(14) Calvados', '14'),
(226, 43, '(50) Manche', '50'),
(227, 43, '(61) Orne', '61'),
(228, 44, '(21) Côte-d''Or', '21'),
(229, 44, '(58) Nièvre', '58'),
(230, 44, '(71) Saône-et-Loire', '71'),
(231, 44, '(89) Yonne', '89'),
(232, 45, '(22) Côtes-d''Armor', '22'),
(233, 45, '(29) Finistère', '29'),
(234, 45, '(35) Ille-et-Vilaine', '35'),
(235, 45, '(56) Morbihan', '56'),
(236, 46, '(18) Cher', '18'),
(237, 46, '(28) Eure-et-Loir', '28'),
(238, 46, '(36) Indre', '36'),
(239, 46, '(37) Indre-et-Loire', '37'),
(240, 46, '(41) Loir-et-Cher', '41'),
(241, 46, '(45) Loiret', '45'),
(242, 48, '(2A) Corse-du-Sud', '2A'),
(243, 48, '(2B) Haute-Corse', '2B'),
(244, 49, '(25) Doubs', '25'),
(245, 49, '(39) Jura', '39'),
(246, 49, '(70) Haute-Saône', '70'),
(247, 49, '(90) Territoire-de-Belfort', '90'),
(248, 50, '(27) Eure', '27'),
(249, 50, '(76) Seine-Maritime', '76'),
(250, 51, '(8) Ardennes', '8'),
(251, 51, '(10) Aube', '10'),
(252, 51, '(51) Marne', '51'),
(253, 51, '(52) Haute-Marne', '52'),
(254, 52, '(75) Paris', '75'),
(255, 52, '(77) Seine-et-Marne', '77'),
(256, 52, '(78) Yvelines', '78'),
(257, 52, '(91) Essonne', '91'),
(258, 52, '(92) Hauts-de-Seine', '92'),
(259, 52, '(93) Seine-Saint-Denis', '93'),
(260, 52, '(94) Val-de-Marne', '94'),
(261, 52, '(95) Val-d''Oise', '95'),
(262, 53, '(11) Aude', '11'),
(263, 53, '(30) Gard', '30'),
(264, 53, '(34) Hérault', '34'),
(265, 53, '(48) Lozère', '48'),
(266, 53, '(66) Pyrénées-Orientales', '66'),
(267, 54, '(19) Corrèze', '19'),
(268, 54, '(23) Creuse', '23'),
(269, 54, '(87) Haute-Vienne', '87'),
(270, 55, '(54) Meurthe-et-Moselle', '54'),
(271, 55, '(55) Meuse', '55'),
(272, 55, '(57) Moselle', '57'),
(273, 55, '(88) Vosges', '88'),
(274, 56, '(9) Ariège', '9'),
(275, 56, '(12) Aveyron', '12'),
(276, 56, '(31) Haute-Garonne', '31'),
(277, 56, '(32) Gers', '32'),
(278, 56, '(46) Lot', '46'),
(279, 56, '(65) Hautes-Pyrénées', '65'),
(280, 56, '(81) Tarn', '81'),
(281, 56, '(82) Tarn-et-Garonne', '82'),
(282, 57, '(59) Nord', '59'),
(283, 57, '(62) Pas-de-Calais', '62'),
(284, 58, '(44) Loire-Atlantique', '44'),
(285, 58, '(49) Maine-et-Loire', '49'),
(286, 58, '(53) Mayenne', '53'),
(287, 58, '(72) Sarthe', '72'),
(288, 58, '(85) Vendée', '85'),
(289, 59, '(2) Aisne', '2'),
(290, 59, '(60) Oise', '60'),
(291, 59, '(80) Somme', '80'),
(292, 60, '(16) Charente', '16'),
(293, 60, '(17) Charente-Maritime', '17'),
(294, 60, '(79) Deux-Sèvres', '79'),
(295, 60, '(86) Vienne', '86'),
(296, 61, '(4) Alpes-de-Haute-Provence', '4'),
(297, 61, '(5) Hautes-Alpes', '5'),
(298, 61, '(6) Alpes-Maritimes', '6'),
(299, 61, '(13) Bouches-du-Rhône', '13'),
(300, 61, '(83) Var', '83'),
(301, 61, '(84) Vaucluse', '84'),
(302, 62, '(1) Ain', '1'),
(303, 62, '(7) Ardèche', '7'),
(304, 62, '(26) Drôme', '26'),
(305, 62, '(38) Isère', '38'),
(306, 62, '(42) Loire', '42'),
(307, 62, '(69) Rhône', '69'),
(308, 62, '(73) Savoie', '73'),
(309, 62, '(74) Haute-Savoie', '74'),
(713, 233, 'GUADELOUPE', 'GP'),
(714, 233, 'GUYANE FRANÇAISE', 'GF'),
(715, 233, 'MARTINIQUE', 'MQ'),
(716, 233, 'MAYOTTE', 'YT'),
(717, 233, 'NOUVELLE-CALÉDONIE', 'NC'),
(718, 233, 'POLYNÉSIE FRANÇAISE', 'PF'),
(719, 233, 'RÉUNION', 'RE'),
(720, 233, 'SAINT-BARTHÉLEMY', 'BL'),
(721, 233, 'SAINT-MARTIN', 'MF'),
(722, 233, 'SAINT-PIERRE-ET-MIQUELON', 'PM'),
(723, 233, 'TERRES AUSTRALES FRANÇAISES', 'TF'),
(724, 233, 'WALLIS ET FUTUNA', 'WF'),
(6235, 234, 'AFGHANISTAN', 'AF'),
(6236, 234, 'AFRIQUE DU SUD', 'ZA'),
(6237, 234, 'ÂLAND, ÎLES', 'AX'),
(6238, 234, 'ALBANIE', 'AL'),
(6239, 234, 'ALGÉRIE', 'DZ'),
(6240, 234, 'ALLEMAGNE', 'DE'),
(6241, 234, 'ANDORRE', 'AD'),
(6242, 234, 'ANGOLA', 'AO'),
(6243, 234, 'ANGUILLA', 'AI'),
(6244, 234, 'ANTARCTIQUE', 'AQ'),
(6245, 234, 'ANTIGUA-ET-BARBUDA', 'AG'),
(6246, 234, 'ANTILLES NÉERLANDAISES', 'AN'),
(6247, 234, 'ARABIE SAOUDITE', 'SA'),
(6248, 234, 'ARGENTINE', 'AR'),
(6249, 234, 'ARMÉNIE', 'AM'),
(6250, 234, 'ARUBA', 'AW'),
(6251, 234, 'AUSTRALIE', 'AU'),
(6252, 234, 'AUTRICHE', 'AT'),
(6253, 234, 'AZERBAÏDJAN', 'AZ'),
(6254, 234, 'BAHAMAS', 'BS'),
(6255, 234, 'BAHREÏN', 'BH'),
(6256, 234, 'BANGLADESH', 'BD'),
(6257, 234, 'BARBADE', 'BB'),
(6258, 234, 'BÉLARUS', 'BY'),
(6259, 234, 'BELGIQUE', 'BE'),
(6260, 234, 'BELIZE', 'BZ'),
(6261, 234, 'BÉNIN', 'BJ'),
(6262, 234, 'BERMUDES', 'BM'),
(6263, 234, 'BHOUTAN', 'BT'),
(6264, 234, 'BOLIVIE', 'BO'),
(6265, 234, 'BOSNIE-HERZÉGOVINE', 'BA'),
(6266, 234, 'BOTSWANA', 'BW'),
(6267, 234, 'BOUVET, ÎLE', 'BV'),
(6268, 234, 'BRÉSIL', 'BR'),
(6269, 234, 'BRUNÉI DARUSSALAM', 'BN'),
(6270, 234, 'BULGARIE', 'BG'),
(6271, 234, 'BURKINA FASO', 'BF'),
(6272, 234, 'BURUNDI', 'BI'),
(6273, 234, 'CAÏMANES, ÎLES', 'KY'),
(6274, 234, 'CAMBODGE', 'KH'),
(6275, 234, 'CAMEROUN', 'CM'),
(6276, 234, 'CANADA', 'CA'),
(6277, 234, 'CAP-VERT', 'CV'),
(6278, 234, 'CENTRAFRICAINE, RÉPUBLIQUE', 'CF'),
(6279, 234, 'CHILI', 'CL'),
(6280, 234, 'CHINE', 'CN'),
(6281, 234, 'CHRISTMAS, ÎLE', 'CX'),
(6282, 234, 'CHYPRE', 'CY'),
(6283, 234, 'COCOS (KEELING), ÎLES', 'COCOS KEELING'),
(6284, 234, 'COLOMBIE', 'CO'),
(6285, 234, 'COMORES', 'KM'),
(6286, 234, 'CONGO', 'CG'),
(6287, 234, 'CONGO, LA RÉPUBLIQUE DÉMOCRATIQUE DU', 'CD'),
(6288, 234, 'COOK, ÎLES', 'CK'),
(6289, 234, 'CORÉE, RÉPUBLIQUE DE', 'KR'),
(6290, 234, 'CORÉE, RÉPUBLIQUE POPULAIRE DÉMOCRATIQUE DE', 'KP'),
(6291, 234, 'COSTA RICA', 'CR'),
(6292, 234, 'CÔTE D''IVOIRE', 'CI'),
(6293, 234, 'CROATIE', 'HR'),
(6294, 234, 'CUBA', 'CU'),
(6295, 234, 'DANEMARK', 'DK'),
(6296, 234, 'DJIBOUTI', 'DJ'),
(6297, 234, 'DOMINICAINE, RÉPUBLIQUE', 'DO'),
(6298, 234, 'DOMINIQUE', 'DM'),
(6299, 234, 'ÉGYPTE', 'EG'),
(6300, 234, 'EL SALVADOR', 'SV'),
(6301, 234, 'ÉMIRATS ARABES UNIS', 'AE'),
(6302, 234, 'ÉQUATEUR', 'EC'),
(6303, 234, 'ÉRYTHRÉE', 'ER'),
(6304, 234, 'ESPAGNE', 'ES'),
(6305, 234, 'ESTONIE', 'EE'),
(6306, 234, 'ÉTATS-UNIS', 'US'),
(6307, 234, 'ÉTHIOPIE', 'ET'),
(6308, 234, 'FALKLAND, ÎLES (MALVINAS)', 'FALKLAND, ÎLES MALVINAS'),
(6309, 234, 'FÉROÉ, ÎLES', 'FO'),
(6310, 234, 'FIDJI', 'FJ'),
(6311, 234, 'FINLANDE', 'FI'),
(6313, 234, 'GABON', 'GA'),
(6314, 234, 'GAMBIE', 'GM'),
(6315, 234, 'GÉORGIE', 'GE'),
(6316, 234, 'GÉORGIE DU SUD ET LES ÎLES SANDWICH DU SUD', 'GS'),
(6317, 234, 'GHANA', 'GH'),
(6318, 234, 'GIBRALTAR', 'GI'),
(6319, 234, 'GRÈCE', 'GR'),
(6320, 234, 'GRENADE', 'GD'),
(6321, 234, 'GROENLAND', 'GL'),
(6322, 234, 'GUAM', 'GU'),
(6323, 234, 'GUATEMALA', 'GT'),
(6324, 234, 'GUERNESEY', 'GG'),
(6325, 234, 'GUINÉE', 'GN'),
(6326, 234, 'GUINÉE-BISSAU', 'GW'),
(6327, 234, 'GUINÉE ÉQUATORIALE', 'GQ'),
(6328, 234, 'GUYANA', 'GY'),
(6329, 234, 'HAÏTI', 'HT'),
(6330, 234, 'HEARD, ÎLE ET MCDONALD, ÎLES', 'HM'),
(6331, 234, 'HONDURAS', 'HN'),
(6332, 234, 'HONG-KONG', 'HK'),
(6333, 234, 'HONGRIE', 'HU'),
(6334, 234, 'ÎLE DE MAN', 'IM'),
(6335, 234, 'ÎLES MINEURES ÉLOIGNÉES DES ÉTATS-UNIS', 'UM'),
(6336, 234, 'ÎLES VIERGES BRITANNIQUES', 'VG'),
(6337, 234, 'ÎLES VIERGES DES ÉTATS-UNIS', 'VI'),
(6338, 234, 'INDE', 'IN'),
(6339, 234, 'INDONÉSIE', 'ID'),
(6340, 234, 'IRAN, RÉPUBLIQUE ISLAMIQUE D''', 'IR'),
(6341, 234, 'IRAQ', 'IQ'),
(6342, 234, 'IRLANDE', 'IE'),
(6343, 234, 'ISLANDE', 'IS'),
(6344, 234, 'ISRAËL', 'IL'),
(6345, 234, 'ITALIE', 'IT'),
(6346, 234, 'JAMAÏQUE', 'JM'),
(6347, 234, 'JAPON', 'JP'),
(6348, 234, 'JERSEY', 'JE'),
(6349, 234, 'JORDANIE', 'JO'),
(6350, 234, 'KAZAKHSTAN', 'KZ'),
(6351, 234, 'KENYA', 'KE'),
(6352, 234, 'KIRGHIZISTAN', 'KG'),
(6353, 234, 'KIRIBATI', 'KI'),
(6354, 234, 'KOWEÏT', 'KW'),
(6355, 234, 'LAO, RÉPUBLIQUE DÉMOCRATIQUE POPULAIRE', 'LA'),
(6356, 234, 'LESOTHO', 'LS'),
(6357, 234, 'LETTONIE', 'LV'),
(6358, 234, 'LIBAN', 'LB'),
(6359, 234, 'LIBÉRIA', 'LR'),
(6360, 234, 'LIBYENNE, JAMAHIRIYA ARABE', 'LY'),
(6361, 234, 'LIECHTENSTEIN', 'LI'),
(6362, 234, 'LITUANIE', 'LT'),
(6363, 234, 'LUXEMBOURG', 'LU'),
(6364, 234, 'MACAO', 'MO'),
(6365, 234, 'MACÉDOINE, L''EX-RÉPUBLIQUE YOUGOSLAVE DE', 'MK'),
(6366, 234, 'MADAGASCAR', 'MG'),
(6367, 234, 'MALAISIE', 'MY'),
(6368, 234, 'MALAWI', 'MW'),
(6369, 234, 'MALDIVES', 'MV'),
(6370, 234, 'MALI', 'ML'),
(6371, 234, 'MALTE', 'MT'),
(6372, 234, 'MARIANNES DU NORD, ÎLES', 'MP'),
(6373, 234, 'MAROC', 'MA'),
(6374, 234, 'MARSHALL, ÎLES', 'MH'),
(6375, 234, 'MAURICE', 'MU'),
(6376, 234, 'MAURITANIE', 'MR'),
(6377, 234, 'MEXIQUE', 'MX'),
(6378, 234, 'MICRONÉSIE, ÉTATS FÉDÉRÉS DE', 'FM'),
(6379, 234, 'MOLDOVA, RÉPUBLIQUE DE', 'MD'),
(6380, 234, 'MONACO', 'MC'),
(6381, 234, 'MONGOLIE', 'MN'),
(6382, 234, 'MONTÉNÉGRO', 'ME'),
(6383, 234, 'MONTSERRAT', 'MS'),
(6384, 234, 'MOZAMBIQUE', 'MZ'),
(6385, 234, 'MYANMAR', 'MM'),
(6386, 234, 'NAMIBIE', 'NA'),
(6387, 234, 'NAURU', 'NR'),
(6388, 234, 'NÉPAL', 'NP'),
(6389, 234, 'NICARAGUA', 'NI'),
(6390, 234, 'NIGER', 'NE'),
(6391, 234, 'NIGÉRIA', 'NG'),
(6392, 234, 'NIUÉ', 'NU'),
(6393, 234, 'NORFOLK, ÎLE', 'NF'),
(6394, 234, 'NORVÈGE', 'NO'),
(6395, 234, 'NOUVELLE-ZÉLANDE', 'NZ'),
(6396, 234, 'OCÉAN INDIEN, TERRITOIRE BRITANNIQUE DE L''', 'IO'),
(6397, 234, 'OMAN', 'OM'),
(6398, 234, 'OUGANDA', 'UG'),
(6399, 234, 'OUZBÉKISTAN', 'UZ'),
(6400, 234, 'PAKISTAN', 'PK'),
(6401, 234, 'PALAOS', 'PW'),
(6402, 234, 'PALESTINIEN OCCUPÉ, TERRITOIRE', 'PS'),
(6403, 234, 'PANAMA', 'PA'),
(6404, 234, 'PAPOUASIE-NOUVELLE-GUINÉE', 'PG'),
(6405, 234, 'PARAGUAY', 'PY'),
(6406, 234, 'PAYS-BAS', 'NL'),
(6407, 234, 'PÉROU', 'PE'),
(6408, 234, 'PHILIPPINES', 'PH'),
(6409, 234, 'PITCAIRN', 'PN'),
(6410, 234, 'POLOGNE', 'PL'),
(6411, 234, 'PORTO RICO', 'PR'),
(6412, 234, 'PORTUGAL', 'PT'),
(6413, 234, 'QATAR', 'QA'),
(6414, 234, 'ROUMANIE', 'RO'),
(6415, 234, 'ROYAUME-UNI', 'GB'),
(6416, 234, 'RUSSIE, FÉDÉRATION DE', 'RU'),
(6417, 234, 'RWANDA', 'RW'),
(6418, 234, 'SAHARA OCCIDENTAL', 'EH'),
(6419, 234, 'SAINTE-HÉLÈNE', 'SH'),
(6420, 234, 'SAINTE-LUCIE', 'LC'),
(6421, 234, 'SAINT-KITTS-ET-NEVIS', 'KN'),
(6422, 234, 'SAINT-MARIN', 'SM'),
(6423, 234, 'SAINT-SIÈGE (ÉTAT DE LA CITÉ DU VATICAN)', 'SAINT-SIÈGE ÉTAT DE LA CITÉ DU'),
(6424, 234, 'SAINT-VINCENT-ET-LES GRENADINES', 'VC'),
(6425, 234, 'SALOMON, ÎLES', 'SB'),
(6426, 234, 'SAMOA', 'WS'),
(6427, 234, 'SAMOA AMÉRICAINES', 'AS'),
(6428, 234, 'SAO TOMÉ-ET-PRINCIPE', 'ST'),
(6429, 234, 'SÉNÉGAL', 'SN'),
(6430, 234, 'SERBIE', 'RS'),
(6431, 234, 'SEYCHELLES', 'SC'),
(6432, 234, 'SIERRA LEONE', 'SL'),
(6433, 234, 'SINGAPOUR', 'SG'),
(6434, 234, 'SLOVAQUIE', 'SK'),
(6435, 234, 'SLOVÉNIE', 'SI'),
(6436, 234, 'SOMALIE', 'SO'),
(6437, 234, 'SOUDAN', 'SD'),
(6438, 234, 'SRI LANKA', 'LK'),
(6439, 234, 'SUÈDE', 'SE'),
(6440, 234, 'SUISSE', 'CH'),
(6441, 234, 'SURINAME', 'SR'),
(6442, 234, 'SVALBARD ET ÎLE JAN MAYEN', 'SJ'),
(6443, 234, 'SWAZILAND', 'SZ'),
(6444, 234, 'SYRIENNE, RÉPUBLIQUE ARABE', 'SY'),
(6445, 234, 'TADJIKISTAN', 'TJ'),
(6446, 234, 'TAÏWAN, PROVINCE DE CHINE', 'TW'),
(6447, 234, 'TANZANIE, RÉPUBLIQUE-UNIE DE', 'TZ'),
(6448, 234, 'TCHAD', 'TD'),
(6449, 234, 'TCHÈQUE, RÉPUBLIQUE', 'CZ'),
(6450, 234, 'THAÏLANDE', 'TH'),
(6451, 234, 'TIMOR-LESTE', 'TL'),
(6452, 234, 'TOGO', 'TG'),
(6453, 234, 'TOKELAU', 'TK'),
(6454, 234, 'TONGA', 'TO'),
(6455, 234, 'TRINITÉ-ET-TOBAGO', 'TT'),
(6456, 234, 'TUNISIE', 'TN'),
(6457, 234, 'TURKMÉNISTAN', 'TM'),
(6458, 234, 'TURKS ET CAÏQUES, ÎLES', 'TC'),
(6459, 234, 'TURQUIE', 'TR'),
(6460, 234, 'TUVALU', 'TV'),
(6461, 234, 'UKRAINE', 'UA'),
(6462, 234, 'URUGUAY', 'UY'),
(6463, 234, 'VANUATU', 'VU'),
(6464, 234, 'VENEZUELA', 'VE'),
(6465, 234, 'VIET NAM', 'VN'),
(6466, 234, 'YÉMEN', 'YE'),
(6467, 234, 'ZAMBIE', 'ZM'),
(6468, 234, 'ZIMBABWE', 'ZW'),
(6469, 234, 'FRANCE', 'FR');

-- --------------------------------------------------------

--
-- Structure de la table `HabilitationAgent`
--

CREATE TABLE IF NOT EXISTS `HabilitationAgent` (
  `id_agent` int(11) NOT NULL default '0',
  `gestion_agent_pole` enum('0','1') NOT NULL default '0',
  `gestion_fournisseurs_envois_postaux` enum('0','1') NOT NULL default '0',
  `gestion_bi_cles` enum('0','1') NOT NULL default '0',
  `creer_consultation` enum('0','1') NOT NULL default '1',
  `modifier_consultation` enum('0','1') NOT NULL default '0',
  `valider_consultation` enum('0','1') NOT NULL default '0',
  `publier_consultation` enum('0','1') NOT NULL default '0',
  `suivre_consultation` enum('0','1') NOT NULL default '1',
  `suivre_consultation_pole` enum('0','1') NOT NULL default '0',
  `supprimer_enveloppe` enum('0','1') NOT NULL default '0',
  `supprimer_consultation` enum('0','1') NOT NULL default '0',
  `depouiller_candidature` enum('0','1') NOT NULL default '1',
  `depouiller_offre` enum('0','1') NOT NULL default '1',
  `messagerie_securisee` enum('0','1') NOT NULL default '0',
  `acces_registre_depots_papier` enum('0','1') NOT NULL default '0',
  `acces_registre_retraits_papier` enum('0','1') NOT NULL default '0',
  `acces_registre_questions_papier` enum('0','1') NOT NULL default '0',
  `gerer_encheres` enum('0','1') NOT NULL default '0',
  `suivre_encheres` enum('0','1') NOT NULL default '0',
  `suivi_entreprise` enum('0','1') NOT NULL default '0',
  `envoi_boamp` enum('0','1') NOT NULL default '0',
  `acces_classement_lot` enum('0','1') NOT NULL default '0',
  `connecteur_sis` enum('0','1') NOT NULL default '0',
  `connecteur_marco` enum('0','1') NOT NULL default '0',
  `repondre_aux_questions` enum('0','1') NOT NULL default '0',
  `appel_projet_formation` enum('0','1') NOT NULL default '0',
  `utiliser_client_CAO` enum('0','1') NOT NULL default '0',
  `notification_boamp` enum('0','1') NOT NULL default '0',
  `administrer_compte` enum('0','1') NOT NULL default '0',
  `gestion_mapa` enum('0','1') NOT NULL default '0',
  `gestion_type_validation` enum('0','1') NOT NULL default '0',
  `approuver_consultation` enum('0','1') NOT NULL default '0',
  `administrer_procedure` enum('0','1') NOT NULL default '0',
  `restreindre_creation` enum('0','1') NOT NULL default '0',
  `creer_liste_marches` enum('0','1') NOT NULL default '0',
  `gestion_commissions` enum('0','1') NOT NULL default '0',
  `suivi_seul_consultation` enum('0','1') NOT NULL default '0',
  `attribution_marche` enum('0','1') NOT NULL default '0',
  `fiche_recensement` enum('0','1') NOT NULL default '0',
  `declarer_infructueux` enum('0','1') NOT NULL default '0',
  `declarer_sans_suite` enum('0','1') NOT NULL default '0',
  `creer_consultation_transverse` enum('0','1') NOT NULL default '0',
  `ouvrir_candidature_en_ligne` enum('0','1') NOT NULL default '0',
  `ouvrir_candidature_a_distance` enum('0','1') NOT NULL default '0',
  `refuser_enveloppe` enum('0','1') NOT NULL default '0',
  `gerer_admissibilite` enum('0','1') NOT NULL default '0',
  `restaurer_enveloppe` enum('0','1') NOT NULL default '0',
  `ouvrir_anonymat_en_ligne` enum('0','1') NOT NULL default '0',
  `ouvrir_offre_en_ligne` enum('0','1') NOT NULL default '0',
  `gestion_compte_boamp` enum('0','1') NOT NULL default '0',
  `gestion_agents` enum('0','1') NOT NULL default '0',
  `gestion_habilitations` enum('0','1') NOT NULL default '0',
  `gerer_mapa_inferieur_montant` enum('0','1') NOT NULL default '0',
  `gerer_mapa_superieur_montant` enum('0','1') NOT NULL default '0',
  `modifier_consultation_avant_validation` enum('0','1') NOT NULL default '0',
  `modifier_consultation_apres_validation` enum('0','1') NOT NULL default '0',
  `acces_reponses` enum('0','1') NOT NULL default '0',
  `telechargement_groupe_anticipe_plis_chiffres` enum('0','1') NOT NULL default '0',
  `telechargement_unitaire_plis_chiffres` enum('0','1') NOT NULL default '0',
  `ouvrir_offre_a_distance` enum('0','1') NOT NULL default '0',
  `creer_annonce_information` enum('0','1') NOT NULL default '0',
  `saisie_marches` enum('0','1') NOT NULL default '0',
  `validation_marches` enum('0','1') NOT NULL default '0',
  `publication_marches` enum('0','1') NOT NULL default '0',
  `gerer_statistiques_metier` enum('0','1') NOT NULL default '0',
  `gerer_archives` enum('0','1') NOT NULL default '0',
  `gerer_courrier_entreprise` enum('0','1') NOT NULL default '0',
  `administrer_procedures_formalisees` enum('0','1') NOT NULL default '0',
  `creer_annonce_attribution` enum('0','1') NOT NULL default '0',
  `acces_registre_retraits_electronique` enum('0','1') NOT NULL default '1',
  `acces_registre_questions_electronique` enum('0','1') NOT NULL default '1',
  `acces_registre_depots_electronique` enum('0','1') NOT NULL default '1',
  `validation_simple` enum('0','1') NOT NULL default '1',
  `validation_intermediaire` enum('0','1') NOT NULL default '1',
  `validation_finale` enum('0','1') NOT NULL default '1',
  `creer_suite_consultation` enum('0','1') NOT NULL default '1',
  `hyper_admin` enum('0','1') NOT NULL default '0',
  `droit_gestion_services` enum('0','1') NOT NULL default '0',
  `suivi_acces` enum('0','1') NOT NULL default '0',
  `statistiques_site` enum('0','1') NOT NULL default '0',
  `statistiques_QoS` enum('0','1') NOT NULL default '0',
  `ouvrir_anonymat_a_distance` enum('0','1') NOT NULL default '0',
  `gestion_compte_jal` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id_agent`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `HabilitationAgent`
--


-- --------------------------------------------------------

--
-- Structure de la table `HabilitationProfil`
--

CREATE TABLE IF NOT EXISTS `HabilitationProfil` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  `gestion_agent_pole` enum('0','1') NOT NULL default '0',
  `gestion_fournisseurs_envois_postaux` enum('0','1') NOT NULL default '0',
  `gestion_bi_cles` enum('0','1') NOT NULL default '0',
  `creer_consultation` enum('0','1') NOT NULL default '1',
  `modifier_consultation` enum('0','1') NOT NULL default '0',
  `valider_consultation` enum('0','1') NOT NULL default '0',
  `publier_consultation` enum('0','1') NOT NULL default '0',
  `suivre_consultation` enum('0','1') NOT NULL default '1',
  `suivre_consultation_pole` enum('0','1') NOT NULL default '0',
  `supprimer_enveloppe` enum('0','1') NOT NULL default '0',
  `supprimer_consultation` enum('0','1') NOT NULL default '0',
  `depouiller_candidature` enum('0','1') NOT NULL default '1',
  `depouiller_offre` enum('0','1') NOT NULL default '1',
  `messagerie_securisee` enum('0','1') NOT NULL default '0',
  `acces_registre_depots_papier` enum('0','1') NOT NULL default '0',
  `acces_registre_retraits_papier` enum('0','1') NOT NULL default '0',
  `acces_registre_questions_papier` enum('0','1') NOT NULL default '0',
  `gerer_encheres` enum('0','1') NOT NULL default '0',
  `suivre_encheres` enum('0','1') NOT NULL default '0',
  `suivi_entreprise` enum('0','1') NOT NULL default '0',
  `envoi_boamp` enum('0','1') NOT NULL default '0',
  `acces_classement_lot` enum('0','1') NOT NULL default '0',
  `connecteur_sis` enum('0','1') NOT NULL default '0',
  `connecteur_marco` enum('0','1') NOT NULL default '0',
  `repondre_aux_questions` enum('0','1') NOT NULL default '0',
  `appel_projet_formation` enum('0','1') NOT NULL default '0',
  `utiliser_client_CAO` enum('0','1') NOT NULL default '1',
  `notification_boamp` enum('0','1') NOT NULL default '1',
  `administrer_compte` enum('0','1') NOT NULL default '0',
  `gestion_mapa` enum('0','1') NOT NULL default '0',
  `gestion_type_validation` enum('0','1') NOT NULL default '0',
  `approuver_consultation` enum('0','1') NOT NULL default '0',
  `administrer_procedure` enum('0','1') NOT NULL default '0',
  `restreindre_creation` enum('0','1') NOT NULL default '0',
  `creer_liste_marches` enum('0','1') NOT NULL default '0',
  `gestion_commissions` enum('0','1') NOT NULL default '0',
  `suivi_seul_consultation` enum('0','1') NOT NULL default '0',
  `attribution_marche` enum('0','1') NOT NULL default '0',
  `fiche_recensement` enum('0','1') NOT NULL default '0',
  `declarer_infructueux` enum('0','1') NOT NULL default '0',
  `declarer_sans_suite` enum('0','1') NOT NULL default '0',
  `creer_consultation_transverse` enum('0','1') NOT NULL default '0',
  `ouvrir_candidature_en_ligne` enum('0','1') NOT NULL default '0',
  `ouvrir_candidature_a_distance` enum('0','1') NOT NULL default '0',
  `refuser_enveloppe` enum('0','1') NOT NULL default '0',
  `gerer_admissibilite` enum('0','1') NOT NULL default '0',
  `restaurer_enveloppe` enum('0','1') NOT NULL default '0',
  `ouvrir_offre_en_ligne` enum('0','1') NOT NULL default '0',
  `ouvrir_anonymat_en_ligne` enum('0','1') NOT NULL default '0',
  `gestion_compte_boamp` enum('0','1') NOT NULL default '0',
  `gestion_agents` enum('0','1') NOT NULL default '0',
  `gestion_habilitations` enum('0','1') NOT NULL default '0',
  `gerer_mapa_inferieur_montant` enum('0','1') NOT NULL default '0',
  `gerer_mapa_superieur_montant` enum('0','1') NOT NULL default '0',
  `modifier_consultation_avant_validation` enum('0','1') NOT NULL default '0',
  `modifier_consultation_apres_validation` enum('0','1') NOT NULL default '0',
  `acces_reponses` enum('0','1') NOT NULL default '0',
  `telechargement_groupe_anticipe_plis_chiffres` enum('0','1') NOT NULL default '0',
  `telechargement_unitaire_plis_chiffres` enum('0','1') NOT NULL default '0',
  `ouvrir_offre_a_distance` enum('0','1') NOT NULL default '0',
  `creer_annonce_information` enum('0','1') NOT NULL default '0',
  `saisie_marches` enum('0','1') NOT NULL default '0',
  `validation_marches` enum('0','1') NOT NULL default '0',
  `publication_marches` enum('0','1') NOT NULL default '0',
  `gerer_statistiques_metier` enum('0','1') NOT NULL default '0',
  `gerer_archives` enum('0','1') NOT NULL default '0',
  `gerer_courrier_entreprise` enum('0','1') NOT NULL default '0',
  `administrer_procedures_formalisees` enum('0','1') NOT NULL default '0',
  `creer_annonce_attribution` enum('0','1') NOT NULL default '0',
  `acces_registre_retraits_electronique` enum('0','1') NOT NULL default '1',
  `acces_registre_questions_electronique` enum('0','1') NOT NULL default '1',
  `acces_registre_depots_electronique` enum('0','1') NOT NULL default '1',
  `validation_simple` enum('0','1') NOT NULL default '1',
  `validation_intermediaire` enum('0','1') NOT NULL default '1',
  `validation_finale` enum('0','1') NOT NULL default '1',
  `creer_suite_consultation` enum('0','1') NOT NULL default '1',
  `hyper_admin` enum('0','1') NOT NULL default '0',
  `droit_gestion_services` enum('0','1') NOT NULL default '0',
  `suivi_acces` enum('0','1') NOT NULL default '0',
  `statistiques_site` enum('0','1') NOT NULL default '0',
  `statistiques_QoS` enum('0','1') NOT NULL default '0',
  `ouvrir_anonymat_a_distance` enum('0','1') NOT NULL default '0',
  `gestion_compte_jal` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB   AUTO_INCREMENT=12 ;

--
-- Contenu de la table `HabilitationProfil`
--

INSERT INTO `HabilitationProfil` VALUES
(1, 'Administrateur Entité Publique', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '0', '0', '1'),
(2, 'Administrateur Service', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '0', '0', '0', '0', '1'),
(4, 'Acheteur', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '1'),
(5, 'Administrateur / Acheteur', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0'),
(6, 'Acheteur MAPA < 90 kEUR', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1', '1', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0'),
(8, 'Observateur', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Structure de la table `Historique_suppression_agent`
--

CREATE TABLE IF NOT EXISTS `Historique_suppression_agent` (
  `id` int(11) NOT NULL auto_increment,
  `id_agent_suppresseur` int(8) NOT NULL default '0',
  `id_service` int(8) default NULL,
  `nom` varchar(100) default NULL,
  `prenom` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `organisme` varchar(100) default NULL,
  `date_suppression` varchar(20) NOT NULL default '',
  `id_agent_supprime` int(8) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Historique_suppression_agent`
--


-- --------------------------------------------------------

--
-- Structure de la table `Inscrit`
--

CREATE TABLE IF NOT EXISTS `Inscrit` (
  `id` int(11) NOT NULL auto_increment,
  `entreprise_id` int(11) NOT NULL default '0',
  `login` varchar(50) default NULL,
  `mdp` varchar(40) default NULL,
  `num_cert` varchar(64) default NULL,
  `cert` mediumtext,
  `civilite` tinyint(1) NOT NULL default '0',
  `nom` varchar(30) NOT NULL default '',
  `prenom` varchar(30) NOT NULL default '',
  `adresse` varchar(80) NOT NULL default '',
  `codepostal` varchar(20) NOT NULL default '',
  `ville` varchar(50) NOT NULL default '',
  `pays` varchar(50) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `telephone` varchar(20) NOT NULL default '',
  `categorie` text,
  `motstitreresume` mediumtext,
  `periode` tinyint(3) unsigned NOT NULL default '0',
  `siret` varchar(5) NOT NULL,
  `fax` varchar(20) NOT NULL default '',
  `code_cpv` text,
  `id_langue` int(11) default NULL,
  `profil` int(1) NOT NULL default '1',
  `adresse2` varchar(80) default NULL,
  `bloque` enum('0','1') NOT NULL default '0',
  `id_initial` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`entreprise_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `login` (`login`),
  KEY `entreprise_id` (`entreprise_id`)
) ENGINE=InnoDB   AUTO_INCREMENT=8 ;

--
-- Contenu de la table `Inscrit`
--

INSERT INTO `Inscrit` VALUES
(1, 1, 'mp', '91f6e575920d2d503d8597338fd1637d580f4243', NULL, NULL, 0, 'PASSET', 'Marc', '231, rue Saint-Honoré', '75001', 'Paris', 'France', 'mp@atexo.com', '0153430500', NULL, NULL, 0, '00019', '', NULL, NULL, 2, '', '0', 0),
(2, 1, 'cc', 'bdb480de655aa6ec75ca058c849c4faf3c0f75b1', NULL, NULL, 0, 'CIVETE', 'Charles', '231, rue Saint-Honoré', '75001', 'Paris', 'France', 'cc@atexo.com', '0153430500', NULL, NULL, 0, '00019', '', NULL, NULL, 1, '', '0', 0),
(4, 2, 'mp2', '44609257f13936e49292cbd87c6d7fd6079ff095', NULL, NULL, 0, 'POLLET', 'Michel', '6 Pl d''Alleray', '75015', 'PARIS 15', 'France', 'mp2@atexo.com', '0153430544', NULL, NULL, 0, '00014', '', NULL, NULL, 2, '', '0', 0),
(5, 2, 'cc2', '1e090fd026721c03e6b552e951234fa45f17c2c8', NULL, NULL, 0, 'CHARON', 'Clément', '6 Pl d''Alleray', '75015', 'PARIS 15', 'France', 'cc2@atexo.com', '0153430550', NULL, NULL, 0, '00014', '', NULL, NULL, 1, '', '0', 0),
(6, 3, 'mp3', '1bc0264d5bf8869899640171b693092d8c4899f8', NULL, NULL, 0, 'POSTIER', 'Maxime', '1 Place Carpeaux', '92800', 'PUTEAUX', 'France', 'mp3@atexo.com', '0153430540', NULL, NULL, 0, '00017', '', NULL, NULL, 2, '', '0', 0),
(7, 3, 'cc3', '26aca93edcc6da566cbdcc5b9de15a277289618e', NULL, NULL, 0, 'CARLET', 'Christian', '1 Place Carpeaux', '92800', 'PUTEAUX', 'France', 'cc3@atexo.com', '014354069', NULL, NULL, 0, '00017', '', NULL, NULL, 1, '', '0', 0),
(8, 1, 'alerte.v3', 'b7124ab3f60eec664940f58cd11d876ae911c9e0', NULL, NULL, 0, 'ALERTE V3', 'alertev3', '231, rue Saint-Honoré', '58768', 'Paris', 'France', 'alerte.v3@atexo.com', '0153430540', NULL, NULL, 0, '00019', '', NULL, NULL, 1, 'Escalier A - 3ème étage', '0', 0);

-- --------------------------------------------------------

--
-- Structure de la table `InscritHistorique`
--

CREATE TABLE IF NOT EXISTS `InscritHistorique` (
  `id` int(11) NOT NULL auto_increment,
  `entreprise_id` int(11) NOT NULL default '0',
  `inscrit1` varchar(64) NOT NULL default '',
  `mail1` varchar(64) NOT NULL default '',
  `inscrit2` varchar(64) NOT NULL default '',
  `mail2` varchar(64) NOT NULL default '',
  `date` varchar(32) NOT NULL default '',
  `action` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `entreprise_id` (`entreprise_id`)
) ENGINE=MyISAM   COMMENT='Historiques des actions ' AUTO_INCREMENT=100 ;

--
-- Contenu de la table `InscritHistorique`
--


-- --------------------------------------------------------

--
-- Structure de la table `InterfaceTypeProcedure`
--

CREATE TABLE IF NOT EXISTS `InterfaceTypeProcedure` (
  `id` int(11) NOT NULL auto_increment,
  `type_procedure_sis` varchar(10) default NULL,
  `type_procedure_marco` varchar(10) default NULL,
  `type_procedure` varchar(10) default NULL,
  `abreviation_type_procedure_organisme` varchar(30) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB   AUTO_INCREMENT=20 ;

--
-- Contenu de la table `InterfaceTypeProcedure`
--

INSERT INTO `InterfaceTypeProcedure` VALUES
(1, 'ACI', NULL, 'ATR', 'AUT'),
(2, 'ACR', NULL, 'ATR', 'AUT'),
(3, 'AOCR', NULL, 'ATR', 'AUT'),
(4, 'AOO', NULL, 'ATR', 'AUT'),
(5, 'AOO2', NULL, 'AOO', 'AOO'),
(6, 'AOR', NULL, 'ARC', 'AOR-C'),
(7, 'COO', NULL, 'COO', 'CO'),
(8, 'COR', NULL, 'CRC', 'CR-C'),
(9, 'MNAC', NULL, 'MNE', 'MN'),
(10, 'MNAP', NULL, 'MNE', 'MN'),
(11, 'MNSC', NULL, 'MNE', 'MN'),
(12, 'MOCN', NULL, 'CRC', 'CR-C'),
(13, 'MOSC', NULL, 'MNE', 'MN'),
(14, 'MSF', NULL, 'ATR', 'AUT'),
(15, 'MSFA', NULL, 'ACA', 'AC-SA'),
(16, 'MSFB', NULL, 'ACS', 'AC-MS'),
(17, 'MSFC', NULL, 'MAI', 'PA-INF'),
(18, 'MSFR', NULL, 'ATR', 'AUT'),
(19, 'PDC', NULL, 'DCC', 'DC-C');

-- --------------------------------------------------------

--
-- Structure de la table `Intervenant_Externe`
--

CREATE TABLE IF NOT EXISTS `Intervenant_Externe` (
  `id` int(11) NOT NULL auto_increment,
  `nom` varchar(30) NOT NULL default '',
  `prenom` varchar(30) NOT NULL default '',
  `organisation` varchar(30) NOT NULL default '',
  `fonction` varchar(200) NOT NULL default '',
  `adresse` varchar(255) NOT NULL default '',
  `mail` varchar(100) NOT NULL default '',
  `type_invitation` int(1) NOT NULL default '1',
  `civilite` varchar(255) default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Intervenant_Externe`
--


-- --------------------------------------------------------

--
-- Structure de la table `Intervenant_Externe_Commission`
--

CREATE TABLE IF NOT EXISTS `Intervenant_Externe_Commission` (
  `id_commission` int(11) NOT NULL default '0',
  `id_intervenant_externe` int(11) NOT NULL default '0',
  `convocation` longblob,
  `convoc_send` char(1) NOT NULL default '0',
  `nom_convoc` varchar(255) default NULL,
  `contenu_envoi` text,
  `nom_fichier_envoye` varchar(255) default NULL,
  `fichier_envoye` longblob,
  `date_envoi` datetime default NULL,
  PRIMARY KEY  (`id_commission`,`id_intervenant_externe`),
  KEY `id_intervenant_externe` (`id_intervenant_externe`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Intervenant_Externe_Commission`
--


-- --------------------------------------------------------

--
-- Structure de la table `Intervenant_Ordre_Du_Jour`
--

CREATE TABLE IF NOT EXISTS `Intervenant_Ordre_Du_Jour` (
  `id` int(11) NOT NULL auto_increment,
  `id_ordre_du_jour` int(11) NOT NULL default '0',
  `id_intervenant` int(11) NOT NULL default '0',
  `id_agent` int(11) NOT NULL default '0',
  `convocation` longblob,
  `convoc_send` char(1) NOT NULL default '0',
  `nom_convoc` varchar(255) default NULL,
  `contenu_envoi` text,
  `nom_fichier_envoye` varchar(255) default NULL,
  `fichier_envoye` longblob,
  `date_envoi` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `id_ordre_du_jour` (`id_ordre_du_jour`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Intervenant_Ordre_Du_Jour`
--


-- --------------------------------------------------------

--
-- Structure de la table `InvitationConsultationTransverse`
--

CREATE TABLE IF NOT EXISTS `InvitationConsultationTransverse` (
  `id` int(11) NOT NULL auto_increment,
  `organisme_emetteur` varchar(30) NOT NULL default '',
  `reference` varchar(255) NOT NULL default '',
  `organisme_invite` varchar(30) NOT NULL default '',
  `lot` int(11) NOT NULL default '0',
  `date_decision` date default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `InvitationConsultationTransverse`
--


-- --------------------------------------------------------

--
-- Structure de la table `Justificatifs`
--

CREATE TABLE IF NOT EXISTS `Justificatifs` (
  `id` int(11) NOT NULL auto_increment,
  `intitule_justificatif` varchar(255) NOT NULL default '',
  `id_entreprise` int(11) NOT NULL default '0',
  `taille` varchar(80) NOT NULL default '',
  `justificatif` int(11) NOT NULL default '0',
  `statut` enum('0','1') NOT NULL default '0',
  `id_document` int(11) default NULL,
  `nom` varchar(100) default NULL,
  PRIMARY KEY  (`id`),
  KEY `id` (`id`),
  KEY `id_entreprise` (`id_entreprise`),
  KEY `id_document` (`id_document`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Justificatifs`
--


-- --------------------------------------------------------

--
-- Structure de la table `Langue`
--

CREATE TABLE IF NOT EXISTS `Langue` (
  `id_langue` int(11) NOT NULL auto_increment,
  `langue` varchar(10) NOT NULL default '',
  `active` enum('0','1') NOT NULL default '0',
  `defaut` enum('0','1') default '0',
  `setlocal` varchar(30) default NULL,
  `libelle` varchar(100) default NULL,
  PRIMARY KEY  (`id_langue`)
) ENGINE=InnoDB   AUTO_INCREMENT=7 ;

--
-- Contenu de la table `Langue`
--

INSERT INTO `Langue` VALUES
(1, 'fr', '1', '1', 'French', NULL),
(2, 'en', '1', '0', 'English', NULL),
(3, 'es', '1', '0', 'Spanish', NULL),
(4, 'cz', '0', '0', NULL, NULL),
(5, 'du', '0', '0', NULL, NULL),
(6, 'su', '0', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `LieuExecution`
--

CREATE TABLE IF NOT EXISTS `LieuExecution` (
  `id` int(22) NOT NULL default '0',
  `libelle` varchar(255) NOT NULL default '',
  `CodePostal_ANM` varchar(5) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `LieuExecution`
--


-- --------------------------------------------------------

--
-- Structure de la table `ModuleCommun`
--

CREATE TABLE IF NOT EXISTS `ModuleCommun` (
  `login_by_mail` enum('0','1') NOT NULL default '0',
  `authentication_without_certificate` enum('0','1') NOT NULL default '1',
  `annuaire_organismes` enum('0','1') NOT NULL default '0',
  `authenticate_agent_by_cert` enum('0','1') NOT NULL default '0',
  `pae_socle` enum('0','1') NOT NULL default '0',
  `duplication_agent` enum('0','1') NOT NULL default '0',
  `centrale_publication` enum('0','1') NOT NULL default '0',
  `code_cpv` enum('0','1') NOT NULL default '0',
  `gestion_coffre_fort_entreprise` enum('0','1') NOT NULL default '0',
  `multi_zone` enum('0','1') NOT NULL default '0',
  `multi_linguisme` enum('0','1') NOT NULL default '0',
  `cross_border` enum('0','1') NOT NULL default '0',
  `option_taille_entreprise` enum('0','1') NOT NULL default '0',
  `gestion_fournisseurs_docs_pole` enum('0','1') NOT NULL default '0',
  `menu_pmi` enum('0','1') NOT NULL default '0',
  `authenticate_inscrit_by_cert` enum('0','1') NOT NULL default '0',
  `authenticate_inscrit_by_login` enum('0','1') NOT NULL default '1',
  `base_qualifiee_entreprise` enum('0','1') NOT NULL default '0',
  `gestion_boamp_mes_sous_services` enum('0','1') NOT NULL default '0',
  `gestion_bi_cle_mes_sous_services` enum('0','1') NOT NULL default '0',
  `nom_entreprise_toujours_visible` enum('0','1') NOT NULL default '0',
  `gestion_jal_mes_sous_services` enum('0','1') NOT NULL default '0',
  `choix_langue_affichage_consultation` enum('0','1') NOT NULL default '0',
  `donnee_complementaires_entreprise` enum('0','1') NOT NULL default '0',
  `vision_ea_entreprise` enum('0','1') NOT NULL default '0',
  `ac_sad_transv` enum('0','1') NOT NULL default '0',
  `Enchere_Entreprise` enum('0','1') NOT NULL default '0',
  `socle_interne` enum('0','1') NOT NULL default '0',
  `module_certificat` enum('0','1') NOT NULL default '0',
  `socle_externe_agent` enum('0','1') NOT NULL default '0',
  `afficher_image_organisme` enum('0','1') NOT NULL default '0',
  `socle_externe_entreprise` enum('0','1') NOT NULL default '0'
) ENGINE=InnoDB ;

--
-- Contenu de la table `ModuleCommun`
--

INSERT INTO `ModuleCommun` VALUES
('0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '1', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Structure de la table `ModuleOrganisme`
--

CREATE TABLE IF NOT EXISTS `ModuleOrganisme` (
  `organisme` varchar(30) NOT NULL default '',
  `lieu_execution` enum('0','1') NOT NULL default '1',
  `connecteur_marco` enum('0','1') NOT NULL default '0',
  `connecteur_sis` enum('0','1') NOT NULL default '0',
  `gestion_envois_postaux` enum('0','1') NOT NULL default '1',
  `encheres` enum('0','1') NOT NULL default '0',
  `dossier_complementaire` enum('0','1') NOT NULL default '1',
  `bi_cle_permanent` enum('0','1') NOT NULL default '1',
  `registre_retraits_papier` enum('0','1') NOT NULL default '1',
  `envoi_mail_notification_dce_parametrable` enum('0','1') NOT NULL default '1',
  `mise_en_ligne_apres_envoi_boamp` enum('0','1') NOT NULL default '1',
  `mise_en_ligne_apres_publication_boamp` enum('0','1') NOT NULL default '1',
  `mise_en_ligne_apres_date` enum('0','1') NOT NULL default '1',
  `mise_en_ligne_manuelle` enum('0','1') NOT NULL default '1',
  `accept_cond_telecharger_DCE` enum('0','1') NOT NULL default '1',
  `classement_lot` enum('0','1') NOT NULL default '1',
  `messagerie_securisee` enum('0','1') NOT NULL default '1',
  `accept_cond_poser_question` enum('0','1') NOT NULL default '1',
  `accept_cond_repondre` enum('0','1') NOT NULL default '1',
  `mail_history` enum('0','1') NOT NULL default '1',
  `appel_projet_formation` enum('0','1') NOT NULL default '0',
  `publicite_boamp` enum('0','1') NOT NULL default '1',
  `publicite_moniteur` enum('0','1') NOT NULL default '1',
  `publicite_presseloc` enum('0','1') NOT NULL default '1',
  `publicite_atexo` enum('0','1') NOT NULL default '1',
  `module_commande` enum('0','1') NOT NULL default '0',
  `module_facture` enum('0','1') NOT NULL default '0',
  `demande_confirmation_DCE` enum('0','1') NOT NULL default '1',
  `envoi_mail_avec_ar` enum('0','1') NOT NULL default '1',
  `sequestre` enum('0','1') NOT NULL default '0',
  `verification_tous_champs_obligatoire_DCE` enum('0','1') NOT NULL default '1',
  `verification_tous_champs_obligatoire_question` enum('0','1') NOT NULL default '1',
  `gestion_champs_optionnels` enum('0','1') NOT NULL default '0',
  `poser_question` enum('0','1') NOT NULL default '1',
  `pub_marches_attribues` enum('0','1') NOT NULL default '1',
  `alerte_delai_procedure` enum('0','1') NOT NULL default '1',
  `analyse_anti_virus` enum('0','1') NOT NULL default '1',
  `systeme_acquisition_dynamique` enum('0','1') NOT NULL default '1',
  `no_activex` enum('0','1') NOT NULL default '0',
  `master_key_permanente` enum('0','1') NOT NULL default '1',
  `no_export_from_client_cao` enum('0','1') NOT NULL default '0',
  `authenticate_agent_by_cert` enum('0','1') NOT NULL default '0',
  `mail_information_entreprise` enum('0','1') NOT NULL default '1',
  `filtre_cle_permanente_par_service` enum('0','1') NOT NULL default '1',
  `retrait_par_lots_DCE` enum('0','1') NOT NULL default '1',
  `gestion_acces_compte` enum('0','1') NOT NULL default '1',
  `gestion_alerte` enum('0','1') NOT NULL default '1',
  `gestion_mapa` enum('0','1') NOT NULL default '1',
  `type_validation` enum('0','1') NOT NULL default '1',
  `gestion_procedue_administrable` enum('0','1') NOT NULL default '1',
  `boamp_champs_preremplis` enum('0','1') NOT NULL default '1',
  `duplication_agent` enum('0','1') NOT NULL default '1',
  `acces_mapa` enum('0','1') NOT NULL default '1',
  `liste_marches` enum('0','1') NOT NULL default '1',
  `pae_socle` enum('0','1') NOT NULL default '0',
  `centrale_publication` enum('0','1') NOT NULL default '0',
  `ouverture_simultanee` enum('0','1') NOT NULL default '1',
  `alerte_reponse_electronique` enum('0','1') NOT NULL default '1',
  `envoi_mail_avec_ar_obligatoire` enum('0','1') NOT NULL default '1',
  `gestion_commissions` enum('0','1') NOT NULL default '1',
  `form_creation_onglets` enum('0','1') NOT NULL default '1',
  `info_menu_gauche_agent` enum('0','1') NOT NULL default '1',
  `gestion_procedure_administrable` enum('0','1') NOT NULL default '1',
  `organisation_centralisee` enum('0','1') NOT NULL default '1',
  `presence_elu` enum('0','1') NOT NULL default '0',
  `traduire_consultation` enum('0','1') NOT NULL default '0',
  `generation_reference_utilisateur` enum('0','1') NOT NULL default '0',
  `suivi_passation` enum('0','1') NOT NULL default '0',
  `numerotation_ref_cons` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`organisme`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `ModuleOrganisme`
--


-- --------------------------------------------------------

--
-- Structure de la table `Offre`
--

CREATE TABLE IF NOT EXISTS `Offre` (
  `organisme` varchar(30) NOT NULL default '',
  `id` int(22) NOT NULL default '0',
  `consultation_ref` varchar(255) NOT NULL default '',
  `entreprise_id` int(22) NOT NULL default '0',
  `inscrit_id` int(22) NOT NULL default '0',
  `mailsignataire` varchar(80) NOT NULL default '',
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `untrustedserial` varchar(40) NOT NULL default '',
  PRIMARY KEY  (`organisme`,`id`),
  KEY `organisme` (`organisme`,`consultation_ref`),
  KEY `entreprise_id` (`entreprise_id`),
  KEY `inscrit_id` (`inscrit_id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Offre`
--


-- --------------------------------------------------------

--
-- Structure de la table `Ordre_Du_Jour`
--

CREATE TABLE IF NOT EXISTS `Ordre_Du_Jour` (
  `id` int(11) NOT NULL auto_increment,
  `id_commission` int(11) NOT NULL default '0',
  `ref_consultation` varchar(255) default NULL,
  `ref_libre` varchar(50) default NULL,
  `id_etape` int(11) NOT NULL default '1',
  `intitule_ordre_du_jour` varchar(255) default NULL,
  `lots_odj_libre` varchar(100) default NULL,
  `id_type_procedure` int(11) default NULL,
  `date_cloture` datetime default NULL,
  `type_env` int(1) default NULL,
  `sous_pli` int(2) default NULL,
  `heure` varchar(5) NOT NULL default '00',
  `etape_consultation` varchar(200) NOT NULL default '',
  `type_consultation` varchar(200) NOT NULL default '',
  `minutes` char(2) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id_commission` (`id_commission`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Ordre_Du_Jour`
--


-- --------------------------------------------------------

--
-- Structure de la table `Organisme`
--

CREATE TABLE IF NOT EXISTS `Organisme` (
  `id` int(11) NOT NULL auto_increment,
  `acronyme` varchar(30) NOT NULL default '',
  `type_article_org` int(1) NOT NULL default '0',
  `denomination_org` varchar(100) NOT NULL default '',
  `categorie_insee` varchar(20) default NULL,
  `description_org` mediumtext,
  `adresse` varchar(100) NOT NULL default '',
  `cp` varchar(5) NOT NULL default '',
  `ville` varchar(100) NOT NULL default '',
  `canton` varchar(100) NOT NULL default '',
  `latitude_degre` char(3) NOT NULL default '0',
  `latitude_minute` char(3) NOT NULL default '0',
  `latitude_seconde` char(3) NOT NULL default '0',
  `longitude_degre` char(3) NOT NULL default '0',
  `longitude_minute` char(3) NOT NULL default '0',
  `longitude_seconde` char(3) NOT NULL default '0',
  `email` varchar(100) NOT NULL default '',
  `url` varchar(100) NOT NULL default '',
  `id_attrib_file` varchar(11) default NULL,
  `attrib_file` varchar(150) NOT NULL default '',
  `date_creation` datetime NOT NULL default '0000-00-00 00:00:00',
  `active` char(1) NOT NULL default '1',
  `id_client_ANM` varchar(32) NOT NULL default '0',
  `status` enum('0','1') NOT NULL default '0',
  `signataire_cao` text,
  `offset` char(3) NOT NULL default '0',
  `sigle` varchar(100) NOT NULL default '',
  `adresse2` varchar(100) NOT NULL default '',
  `tel` varchar(50) NOT NULL default '',
  `telecopie` varchar(50) NOT NULL default '',
  `pays` varchar(150) default NULL,
  `affichage_entite` char(1) NOT NULL default '',
  `id_initial` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `acronyme` (`acronyme`),
  KEY `categorie_insee` (`categorie_insee`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Organisme`
--


-- --------------------------------------------------------

--
-- Structure de la table `Organisme_Service_Metier`
--

CREATE TABLE IF NOT EXISTS `Organisme_Service_Metier` (
  `organisme` varchar(30) NOT NULL default '0',
  `id_service_metier` int(11) NOT NULL default '0',
  KEY `organisme` (`organisme`),
  KEY `id_service_metier` (`id_service_metier`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Organisme_Service_Metier`
--


-- --------------------------------------------------------

--
-- Structure de la table `QuestionDCE`
--

CREATE TABLE IF NOT EXISTS `QuestionDCE` (
  `id` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` varchar(255) NOT NULL default '',
  `date_depot` datetime NOT NULL default '0000-00-00 00:00:00',
  `id_inscrit` int(11) NOT NULL default '0',
  `id_entreprise` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `organisme` (`organisme`,`consultation_ref`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `QuestionDCE`
--


-- --------------------------------------------------------

--
-- Structure de la table `responsableengagement`
--

CREATE TABLE IF NOT EXISTS `responsableengagement` (
  `id` int(11) NOT NULL auto_increment,
  `entreprise_id` int(11) NOT NULL default '0',
  `nom` varchar(30) default NULL,
  `prenom` varchar(30) default NULL,
  `qualite` varchar(32) default NULL,
  PRIMARY KEY  (`id`,`entreprise_id`),
  KEY `entreprise_id` (`entreprise_id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `responsableengagement`
--


-- --------------------------------------------------------

--
-- Structure de la table `RPA`
--

CREATE TABLE IF NOT EXISTS `RPA` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `nom` varchar(30) NOT NULL default '',
  `prenom` varchar(30) NOT NULL default '',
  `adresse1` varchar(80) NOT NULL default '',
  `adresse2` varchar(80) NOT NULL default '',
  `codepostal` varchar(20) NOT NULL default '',
  `ville` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `organisme` (`organisme`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `RPA`
--


-- --------------------------------------------------------

--
-- Structure de la table `Service_Mertier`
--

CREATE TABLE IF NOT EXISTS `Service_Mertier` (
  `id` int(11) NOT NULL default '0',
  `sigle` varchar(50) default NULL,
  `denomination` varchar(255) default NULL,
  `url_acces` text,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Service_Mertier`
--

INSERT INTO `Service_Mertier` VALUES
(1, 'MPE', 'Marchés Publics Electroniques', '?page=agent.AccueilAgentAuthentifie');

-- --------------------------------------------------------

--
-- Structure de la table `Service_Mertier_Profils`
--

CREATE TABLE IF NOT EXISTS `Service_Mertier_Profils` (
  `id_interne` int(11) NOT NULL default '0',
  `id_service_metier` int(11) NOT NULL default '0',
  `id_externe` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL,
  KEY `id_service_metier` (`id_service_metier`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Service_Mertier_Profils`
--

INSERT INTO `Service_Mertier_Profils` VALUES
(1, 1, 1, 'Administrateur Entité Publique'),
(2, 1, 2, 'Administrateur Service'),
(4, 1, 4, 'Acheteur'),
(5, 1, 5, 'Administrateur / Acheteur'),
(6, 1, 6, 'Acheteur MAPA < 90 kEUR'),
(8, 1, 8, 'Observateur');

-- --------------------------------------------------------

--
-- Structure de la table `Socle_Habilitation_Agent`
--

CREATE TABLE IF NOT EXISTS `Socle_Habilitation_Agent` (
  `id_agent` int(11) NOT NULL,
  `gestion_agent_pole_socle` enum('0','1') NOT NULL default '0',
  `gestion_agents_socle` enum('0','1') NOT NULL default '0',
  `droit_gestion_services_socle` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id_agent`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Socle_Habilitation_Agent`
--


-- --------------------------------------------------------

--
-- Structure de la table `sso_agent`
--

CREATE TABLE IF NOT EXISTS `sso_agent` (
  `id_sso` varchar(40) NOT NULL,
  `id_agent` int(20) NOT NULL,
  `organisme` varchar(20) NOT NULL,
  `id_service` int(20) NOT NULL,
  `date_connexion` varchar(20) NOT NULL,
  `date_last_request` varchar(20) NOT NULL,
  PRIMARY KEY  (`id_sso`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `sso_agent`
--


-- --------------------------------------------------------

--
-- Structure de la table `SuiviAcces`
--

CREATE TABLE IF NOT EXISTS `SuiviAcces` (
  `id_agent` int(8) NOT NULL default '0',
  `date_acces` varchar(20) NOT NULL default '',
  `id_service` int(8) default NULL,
  `nom` varchar(100) default NULL,
  `prenom` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `organisme` varchar(100) default NULL
) ENGINE=InnoDB ;

--
-- Contenu de la table `SuiviAcces`
--


-- --------------------------------------------------------

--
-- Structure de la table `Telechargement`
--

CREATE TABLE IF NOT EXISTS `Telechargement` (
  `id` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` varchar(255) NOT NULL default '',
  `datetelechargement` datetime NOT NULL default '0000-00-00 00:00:00',
  `tirage_plan` int(11) NOT NULL default '0',
  `id_inscrit` int(11) default '0',
  `id_entreprise` int(11) default '0',
  `support` int(11) default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `organisme` (`organisme`,`consultation_ref`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contenu de la table `Telechargement`
--


-- --------------------------------------------------------

--
-- Structure de la table `TypeAvis`
--

CREATE TABLE IF NOT EXISTS `TypeAvis` (
  `id` int(11) NOT NULL auto_increment,
  `intitule_avis` varchar(100) NOT NULL default '',
  `intitule_avis_fr` varchar(100) default NULL,
  `intitule_avis_en` varchar(100) default NULL,
  `intitule_avis_es` varchar(100) default NULL,
  `intitule_avis_su` varchar(100) default NULL,
  `intitule_avis_du` varchar(100) default NULL,
  `intitule_avis_cz` varchar(100) default NULL,
  `abbreviation` varchar(50) NOT NULL default '',
  `id_type_avis_ANM` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB   AUTO_INCREMENT=5 ;

--
-- Contenu de la table `TypeAvis`
--

INSERT INTO `TypeAvis` VALUES
(2, 'Annonce d''information', 'Annonce d''information', 'en Annonce d''information ', 'es Annonce d''information ', NULL, NULL, NULL, 'API', 1),
(3, 'Annonce de consultation', 'Annonce de consultation', 'en Annonce de consultation ', 'es Annonce de consultation', NULL, NULL, NULL, 'AAPC', 2),
(4, 'Annonce d''attribution', 'Annonce d''attribution', 'en Annonce d''attribution ', 'es Annonce d''attribution ', NULL, NULL, NULL, 'AA', 3);

-- --------------------------------------------------------

--
-- Structure de la table `TypeProcedure`
--

CREATE TABLE IF NOT EXISTS `TypeProcedure` (
  `id_type_procedure` int(11) NOT NULL auto_increment,
  `libelle_type_procedure` varchar(100) NOT NULL default '',
  `libelle_type_procedure_fr` varchar(100) default '',
  `libelle_type_procedure_en` varchar(100) default '',
  `libelle_type_procedure_es` varchar(100) default '',
  `libelle_type_procedure_su` varchar(100) default '',
  `libelle_type_procedure_du` varchar(100) default '',
  `libelle_type_procedure_cz` varchar(100) default '',
  `abbreviation` varchar(50) NOT NULL default '',
  `type_boamp` int(11) NOT NULL default '0',
  `categorie_procedure` int(11) NOT NULL default '0',
  `id_type_procedure_ANM` int(11) NOT NULL default '0',
  `delai_alerte` int(11) NOT NULL default '0',
  `mapa` enum('0','1') NOT NULL default '0',
  `consultation_transverse` enum('0','1') NOT NULL default '0',
  `code_recensement` char(3) default NULL,
  `abbreviation_portail_europe` varchar(50) NOT NULL,
  PRIMARY KEY  (`id_type_procedure`)
) ENGINE=InnoDB   AUTO_INCREMENT=34 ;

--
-- Contenu de la table `TypeProcedure`
--

INSERT INTO `TypeProcedure` VALUES
(1, 'Appel d''offres ouvert', NULL, NULL, NULL, NULL, NULL, NULL, 'AOO', 0, 1, 1, 0, '0', '0', NULL, 'AOO'),
(2, 'Appel d''offres restreint', NULL, NULL, NULL, NULL, NULL, NULL, 'AOR', 0, 2, 2, 0, '0', '0', NULL, 'AOR'),
(3, 'Procédure adaptée', NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA', 0, 3, 7, 0, '1', '0', NULL, 'PAD'),
(4, 'Concours ouvert', NULL, NULL, NULL, NULL, NULL, NULL, 'COUV', 0, 4, 3, 0, '1', '0', NULL, 'COU'),
(5, 'Concours restreint', NULL, NULL, NULL, NULL, NULL, NULL, 'CRES', 0, 5, 4, 0, '0', '0', NULL, 'CRE'),
(7, 'Dialogue compétitif', NULL, NULL, NULL, NULL, NULL, NULL, 'DCO', 0, 7, 6, 0, '0', '0', NULL, 'DCO'),
(8, 'Marché négocié', NULL, NULL, NULL, NULL, NULL, NULL, 'MN', 0, 8, 5, 0, '0', '0', NULL, 'MNE'),
(15, 'Procédure autre', NULL, NULL, NULL, NULL, NULL, NULL, 'AUT', 0, 15, 8, 0, '0', '0', NULL, 'OTH'),
(30, 'Accord-Cadre - Sélection des Attributaires', NULL, NULL, NULL, NULL, NULL, NULL, 'AC-PA', 1, 1, 1, 0, '0', '0', NULL, 'OTH'),
(31, 'Accord-Cadre - Marché Subséquent', NULL, NULL, NULL, NULL, NULL, NULL, 'AC-MS', 2, 2, 3, 0, '0', '0', NULL, 'OTH'),
(32, 'Système d''Acquisition Dynamique - Admission', NULL, NULL, NULL, NULL, NULL, NULL, 'SAD-A', 8, 4, 8, 0, '0', '0', NULL, 'OTH'),
(33, 'Système d''Acquisition Dynamique - Marché Spécifique', NULL, NULL, NULL, NULL, NULL, NULL, 'SAD-MS', 8, 4, 8, 0, '0', '0', NULL, 'OTH');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Administrateur`
--
ALTER TABLE `Administrateur`
  ADD CONSTRAINT `Administrateur_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Agent`
--
ALTER TABLE `Agent`
  ADD CONSTRAINT `Agent_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Agent_Service_Metier`
--
ALTER TABLE `Agent_Service_Metier`
  ADD CONSTRAINT `Agent_Service_Metier_ibfk_1` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Agent_Service_Metier_ibfk_2` FOREIGN KEY (`id_service_metier`) REFERENCES `Service_Mertier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Annonce`
--
ALTER TABLE `Annonce`
  ADD CONSTRAINT `Annonce_ibfk_1` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CategorieLot`
--
ALTER TABLE `CategorieLot`
  ADD CONSTRAINT `CategorieLot_ibfk_1` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `consultation`
--
ALTER TABLE `consultation`
  ADD CONSTRAINT `consultation_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `GeolocalisationN1`
--
ALTER TABLE `GeolocalisationN1`
  ADD CONSTRAINT `GeolocalisationN1_ibfk_1` FOREIGN KEY (`id_geolocalisationN0`) REFERENCES `GeolocalisationN0` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `GeolocalisationN2`
--
ALTER TABLE `GeolocalisationN2`
  ADD CONSTRAINT `GeolocalisationN2_ibfk_1` FOREIGN KEY (`id_geolocalisationN1`) REFERENCES `GeolocalisationN1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `HabilitationAgent`
--
ALTER TABLE `HabilitationAgent`
  ADD CONSTRAINT `HabilitationAgent_ibfk_1` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Inscrit`
--
ALTER TABLE `Inscrit`
  ADD CONSTRAINT `Inscrit_ibfk_1` FOREIGN KEY (`entreprise_id`) REFERENCES `Entreprise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Justificatifs`
--
ALTER TABLE `Justificatifs`
  ADD CONSTRAINT `Justificatifs_ibfk_1` FOREIGN KEY (`id_document`) REFERENCES `DocumentsAttaches` (`id_document`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ModuleOrganisme`
--
ALTER TABLE `ModuleOrganisme`
  ADD CONSTRAINT `ModuleOrganisme_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Offre`
--
ALTER TABLE `Offre`
  ADD CONSTRAINT `Offre_ibfk_1` FOREIGN KEY (`entreprise_id`) REFERENCES `Entreprise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Offre_ibfk_2` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Organisme`
--
ALTER TABLE `Organisme`
  ADD CONSTRAINT `Organisme_ibfk_1` FOREIGN KEY (`categorie_insee`) REFERENCES `CategorieINSEE` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Organisme_Service_Metier`
--
ALTER TABLE `Organisme_Service_Metier`
  ADD CONSTRAINT `Organisme_Service_Metier_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Organisme_Service_Metier_ibfk_2` FOREIGN KEY (`id_service_metier`) REFERENCES `Service_Mertier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `QuestionDCE`
--
ALTER TABLE `QuestionDCE`
  ADD CONSTRAINT `QuestionDCE_ibfk_1` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `responsableengagement`
--
ALTER TABLE `responsableengagement`
  ADD CONSTRAINT `responsableengagement_ibfk_1` FOREIGN KEY (`entreprise_id`) REFERENCES `Entreprise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `RPA`
--
ALTER TABLE `RPA`
  ADD CONSTRAINT `RPA_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Service_Mertier_Profils`
--
ALTER TABLE `Service_Mertier_Profils`
  ADD CONSTRAINT `Service_Mertier_Profils_ibfk_1` FOREIGN KEY (`id_service_metier`) REFERENCES `Service_Mertier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Telechargement`
--
ALTER TABLE `Telechargement`
  ADD CONSTRAINT `Telechargement_ibfk_1` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;
  
--
-- Données par défaut de la table `Alerte`
--
INSERT INTO `Alerte` (`id`, `id_inscrit`, `denomination`, `periodicite`, `xmlCriteria`, `categorie`) VALUES
(1, 8, 'Alerte complète', '1', '<?xml version="1.0" encoding="UTF-8"?>\n<alerte>\n	<organisme>NULL</organisme>\n	 <entiteAchat>NULL</entiteAchat>\n	 <typeAnnonce>3</typeAnnonce>\n	<typeProcedure>NULL</typeProcedure>\n	<categorie>NULL</categorie>\n	<lieuExecution>NULL</lieuExecution>\n	<cpv1>NULL</cpv1>\n	<cpv2>NULL</cpv2>\n	 <cpv3>NULL</cpv3>\n	 <cpv4>NULL</cpv4>\n	 <libelle_cpv1>NULL</libelle_cpv1>\n	<libelle_cpv2>NULL</libelle_cpv2>\n	 <libelle_cpv3>NULL</libelle_cpv3>\n	 <libelle_cpv4>NULL</libelle_cpv4>\n	 <motCle></motCle>\n<domainesActivite>NULL</domainesActivite><qualifications>NULL</qualifications><agrements>NULL</agrements></alerte>\n', NULL),
(2, 8, 'Alerte CPV Informatique', '1', '<?xml version="1.0" encoding="UTF-8"?>\n<alerte>\n	<organisme>NULL</organisme>\n	 <entiteAchat>NULL</entiteAchat>\n	 <typeAnnonce>3</typeAnnonce>\n	<typeProcedure>NULL</typeProcedure>\n	<categorie>NULL</categorie>\n	<lieuExecution>NULL</lieuExecution>\n	<cpv1>NULL</cpv1>\n	<cpv2>#30211300#48900000#72000000</cpv2>\n	 <cpv3>NULL</cpv3>\n	 <cpv4>NULL</cpv4>\n	 <libelle_cpv1>NULL</libelle_cpv1>\n	<libelle_cpv2>NULL</libelle_cpv2>\n	 <libelle_cpv3>NULL</libelle_cpv3>\n	 <libelle_cpv4>NULL</libelle_cpv4>\n	 <motCle></motCle>\n<domainesActivite>NULL</domainesActivite><qualifications>NULL</qualifications><agrements>NULL</agrements></alerte>\n', NULL);

-- MMI (pour les comptes BOAMP)
INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES
(6, 'Type de pouvoir adjudicateur'),
(7, ' Principale(s) activité(s) du pouvoir adjudicateu'),
(8, ' Principale(s) activité(s) de l''entité adjudicatrice');

INSERT INTO `ValeurReferentiel` (`id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(6, 'Ministère ou toute autre autorité nationale ou fédérale, y compris leurs subdivisions régionales ou locales', '', '', '', '', '', '', '', '', ''),
(6, 'Agence/office national(e) ou fédéral(e)', '', '', '', '', '', '', '', '', ''),
(6, 'Collectivité territoriale', '', '', '', '', '', '', '', '', ''),
(6, 'Agence/office régional(e) ou local(e)', '', '', '', '', '', '', '', '', ''),
(6, 'Organisme de droit public', '', '', '', '', '', '', '', '', ''),
(6, 'Institution/agence européenne ou organisation européenne', '', '', '', '', '', '', '', '', ''),
(6, 'Autre', '', '', '', '', '', '', '', '', ''),
(7, 'Services généraux des administrations publiques', '', '', '', '', '', '', '', '<servicesGenerauxAdmPub/>', ''),
(7, 'Défense', '', '', '', '', '', '', '', '<defense/>', ''),
(7, 'Ordre et sécurité publique\r\n', '', '', '', '', '', '', '', '<ordreSecuritePub/>', ''),
(7, 'Environnement\r\n', '', '', '', '', '', '', '', '<environnement/>', ''),
(7, 'Affaires économiques et financières', '', '', '', '', '', '', '', '<affairesEcoFi/>', ''),
(7, 'Santé', '', '', '', '', '', '', '', '<sante/>', ''),
(8, 'Production, transport et distribution de gaz ou de chaleur', '', '', '', '', '', '', '', '<gazChaleur/>', ''),
(8, 'Electricités', '', '', '', '', '', '', '', '<electricites/>', ''),
(8, 'Prospection et extraction de pétrole et de gaz\r\n', '', '', '', '', '', '', '', '<gazPetrole/>', ''),
(8, 'Prospection et extraction de charbon ou autres combustibles\r\n', '', '', '', '', '', '', '', '<charbon/>', ''),
(8, 'Eau', '', '', '', '', '', '', '', '<eau/>', ''),
(8, 'Services postaux', '', '', '', '', '', '', '', '<servicesPostaux/>', ''),
(8, 'Services de chemin de fer', '', '', '', '', '', '', '', '<servicesCheminFer/>', ''),
(8, 'Services de chemin de fer urbains, de tramway ou d''autobus\r\n', '', '', '', '', '', '', '', '<servicesCheminFerUrbains/>', ''),
(8, 'Activités portuaires\r\n', '', '', '', '', '', '', '', '<activitesPortuaires/>', ''),
(8, 'Activités aéroportuaires', '', '', '', '', '', '', '', '<activitesAeroportuaires/>', '');

SET FOREIGN_KEY_CHECKS=1;

COMMIT;
