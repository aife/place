DROP TABLE IF EXISTS `Etape_cao`  ;
DROP TABLE IF EXISTS `tailleentreprise`  ;
DROP TABLE IF EXISTS `AR_Action`;
DROP TABLE IF EXISTS `Code_cpv_inscrit`;
DROP TABLE IF EXISTS `ConsultationLangue`;
DROP TABLE IF EXISTS `consultation_nettoyee`;
DROP TABLE IF EXISTS `HomePage`;
DROP TABLE IF EXISTS `liste_plateforme`;
DROP TABLE IF EXISTS `Mail_information_entreprise`;
DROP TABLE IF EXISTS `MessagePublication`;
DROP TABLE IF EXISTS `JournalPublication`;
DROP TABLE IF EXISTS `OrganePublication`;
DROP TABLE IF EXISTS `Tiers`;
DROP TABLE IF EXISTS `OrganismeTiers`;
DROP TABLE IF EXISTS `ServeurOrganisme`;
DROP TABLE IF EXISTS `admission_systeme_acquisition`;
DROP TABLE IF EXISTS `ServicesMetiers`;

-- ASO 
ALTER TABLE `AnnonceBoamp` DROP FOREIGN KEY `AnnonceBoamp_ReferentielTypeXml` ;
ALTER TABLE `AnnonceBoamp` DROP FOREIGN KEY `AnnonceBoamp_ReferentielDestinationFormXml` ;
ALTER TABLE `AnnonceBoamp` DROP FOREIGN KEY `AnnonceBoamp_ReferentielFormXml` ;


-- AME MPE 4
CREATE TABLE IF NOT EXISTS `questions_dce` (  
  `id` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `date_depot` datetime NOT NULL default '0000-00-00 00:00:00',
  `email` varchar(100) NOT NULL default '',
  `nom` varchar(80) default NULL,
  `entreprise` varchar(50) default NULL,
  `adresse` varchar(200) default NULL,
  `cp` varchar(100) default NULL,
  `ville` varchar(150) default NULL,
  `tel` varchar(150) default NULL,
  `fax` varchar(150) default NULL,
  `question` longtext NOT NULL,
  `statut` int(11) NOT NULL default '0',
  `date_reponse` date NOT NULL default '0000-00-00',
  `personne_repondu` text,
  `type_depot` enum('1','2') NOT NULL default '1',
  `pays` varchar(150) default NULL,
  `Observation` text,
  `siret` varchar(14) default NULL,
  `identifiant_national` varchar(20) default NULL,
  `acronyme_pays` varchar(10) default NULL,
  `adresse2` varchar(80) default NULL,
  `id_fichier` int(11) default NULL,
  `nom_fichier` varchar(100) default NULL,
  `prenom` varchar(80) default NULL,
  `siret_etranger` varchar(20) default NULL,
  `id_inscrit` int(11) NOT NULL default '0',
  `id_entreprise` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`),
  KEY `id_inscrit` (`id_inscrit`),
  KEY `Entreprise` (`id_entreprise`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `Type_Procedure_Organisme` (
  `id_type_procedure` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `libelle_type_procedure` varchar(100) NOT NULL default '',
  `abbreviation` varchar(50) NOT NULL default '',
  `type_boamp` int(11) NOT NULL default '0',
  `id_type_procedure_portail` int(11) NOT NULL default '0',
  `categorie_procedure` int(11) NOT NULL default '0',
  `delai_alerte` int(11) NOT NULL default '0',
  `id_type_validation` int(11) NOT NULL default '2',
  `service_validation` int(11) NOT NULL default '0',
  `mapa` enum('0','1') NOT NULL default '0',
  `activer_mapa` enum('0','1') NOT NULL default '1',
  `libelle_type_procedure_fr` varchar(100) default '',
  `libelle_type_procedure_en` varchar(100) default '',
  `libelle_type_procedure_es` varchar(100) default '',
  `libelle_type_procedure_su` varchar(100) default '',
  `libelle_type_procedure_du` varchar(100) default '',
  `libelle_type_procedure_cz` varchar(100) default '',
  `libelle_type_procedure_ar` varchar(100) default NULL,
  `id_montant_mapa` int(2) NOT NULL default '0',
  `code_recensement` char(3) default NULL,
  `depouillable_phase_consultation` enum('0','1') NOT NULL default '0',
  `consultation_transverse` enum('0','1') NOT NULL default '0',
  `tag_Boamp` varchar(50) NOT NULL default '',
  `ao` enum('0','1') NOT NULL default '0',
  `mn` enum('0','1') NOT NULL default '0',
  `dc` enum('0','1') NOT NULL default '0',
  `autre` enum('0','1') NOT NULL default '0',
  `sad` enum('0','1') NOT NULL default '0',
  `accord_cadre` enum('0','1') NOT NULL default '0',
  `tag_name_mesure_avancement` varchar(100) NOT NULL,
  `abreviation_interface` varchar(50) default NULL,
  `libelle_type_procedure_it` varchar(100) default NULL,
  `publicite_types_form_xml` text NOT NULL COMMENT 'Permet de parametrer les types de formulaires au format xml dans la publicité en fonction du type de procedure. Les valeurs de ce champ doivent etre separés par des ''#''',
  `tag_name_chorus` varchar(100) NOT NULL,
  PRIMARY KEY  (`id_type_procedure`,`organisme`)
) ENGINE=InnoDB  ;



ALTER TABLE `Telechargement` ADD  `nom` varchar(100) NOT NULL default '';
ALTER TABLE `Telechargement` ADD  `adresse` varchar(100) NOT NULL default '';
ALTER TABLE `Telechargement` ADD  `email` varchar(100) NOT NULL default '';
ALTER TABLE `Telechargement` ADD  `entreprise` varchar(100) NOT NULL default '';
ALTER TABLE `Telechargement` ADD  `codepostal` varchar(5) NOT NULL default '0';
ALTER TABLE `Telechargement` ADD  `ville` varchar(50) NOT NULL default '';
ALTER TABLE `Telechargement` ADD  `pays` varchar(50) default NULL;
ALTER TABLE `Telechargement` ADD  `telephone` varchar(20) NOT NULL default '';
ALTER TABLE `Telechargement` ADD  `siret` varchar(14) NOT NULL default '';
ALTER TABLE `Telechargement` ADD  `fax` varchar(30) NOT NULL default '';
ALTER TABLE `Telechargement` ADD  `lots` varchar(255) default '';
ALTER TABLE `Telechargement` ADD  `sirenEtranger` varchar(20) default '0';
ALTER TABLE `Telechargement` ADD  `adresse2` varchar(80) NOT NULL default '0';
ALTER TABLE `Telechargement` ADD  `prenom` varchar(11) default '';
ALTER TABLE `Telechargement` ADD  `noms_fichiers_dce` text;
ALTER TABLE `Telechargement` ADD  `Observation` text;
ALTER TABLE `Telechargement` ADD  `acronyme_pays` varchar(10) default NULL;

CREATE TABLE IF NOT EXISTS `Retrait_Papier` (
  `id` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `nom` varchar(100) NOT NULL default '',
  `adresse` varchar(100) NOT NULL default '',
  `entreprise` varchar(100) NOT NULL default '',
  `datetelechargement` datetime NOT NULL default '0000-00-00 00:00:00',
  `codepostal` varchar(5) NOT NULL default '0',
  `ville` varchar(50) NOT NULL default '',
  `pays` varchar(50) NOT NULL default '',
  `telephone` varchar(20) NOT NULL default '',
  `tirage_plan` int(11) NOT NULL default '0',
  `siret` varchar(14) default NULL,
  `fax` varchar(30) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `Observation` text,
  `prenom` varchar(100) default NULL,
  `adresse2` varchar(100) default NULL,
  `identifiant_national` varchar(20) default NULL,
  `acronyme_pays` varchar(10) default NULL,
  `support` enum('1','2','3') NOT NULL default '1',
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `Service` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `type_service` enum('1','2') NOT NULL default '2',
  `libelle` text NOT NULL,
  `sigle` text NOT NULL,
  `adresse` varchar(100) NOT NULL default '',
  `adresse_suite` varchar(100) NOT NULL default '',
  `cp` varchar(5) NOT NULL default '',
  `ville` varchar(100) NOT NULL default '',
  `telephone` varchar(100) NOT NULL default '',
  `fax` varchar(100) NOT NULL default '',
  `mail` varchar(100) NOT NULL default '',
  `pays` varchar(150) default NULL,
  `id_initial` int(11) NOT NULL default '0',
  `date_creation` varchar(20) default NULL,
  `date_modification` varchar(20) default NULL,
  `siren` varchar(9) default NULL,
  `complement` varchar(5) default NULL,
  `libelle_ar` text NOT NULL,
  `adresse_ar` varchar(100) NOT NULL default '',
  `adresse_suite_ar` varchar(100) NOT NULL default '',
  `ville_ar` varchar(100) NOT NULL default '',
  `pays_ar` varchar(150) NOT NULL default 'NULL',
  `libelle_fr` text NOT NULL,
  `adresse_fr` varchar(100) NOT NULL default '',
  `adresse_suite_fr` varchar(100) NOT NULL default '',
  `ville_fr` varchar(100) NOT NULL default '',
  `pays_fr` varchar(150) NOT NULL default 'NULL',
  `libelle_es` text NOT NULL,
  `adresse_es` varchar(100) NOT NULL default '',
  `adresse_suite_es` varchar(100) NOT NULL default '',
  `ville_es` varchar(100) NOT NULL default '',
  `pays_es` varchar(150) NOT NULL default 'NULL',
  `libelle_en` text NOT NULL,
  `adresse_en` varchar(100) NOT NULL default '',
  `adresse_suite_en` varchar(100) NOT NULL default '',
  `ville_en` varchar(100) NOT NULL default '',
  `pays_en` varchar(150) NOT NULL default 'NULL',
  `libelle_su` text NOT NULL,
  `adresse_su` varchar(100) NOT NULL default '',
  `adresse_suite_su` varchar(100) NOT NULL default '',
  `ville_su` varchar(100) NOT NULL default '',
  `pays_su` varchar(150) NOT NULL default 'NULL',
  `libelle_du` text NOT NULL,
  `adresse_du` varchar(100) NOT NULL default '',
  `adresse_suite_du` varchar(100) NOT NULL default '',
  `ville_du` varchar(100) NOT NULL default '',
  `pays_du` varchar(150) NOT NULL default 'NULL',
  `libelle_cz` text NOT NULL,
  `adresse_cz` varchar(100) NOT NULL default '',
  `adresse_suite_cz` varchar(100) NOT NULL default '',
  `ville_cz` varchar(100) NOT NULL default '',
  `pays_cz` varchar(150) NOT NULL default '',
  `libelle_it` text NOT NULL,
  `adresse_it` varchar(100) NOT NULL default '',
  `adresse_suite_it` varchar(100) NOT NULL default '',
  `ville_it` varchar(100) NOT NULL default '',
  `pays_it` varchar(150) NOT NULL default 'NULL',
  `chemin_complet` varchar(255) NOT NULL default '',
  `chemin_complet_fr` varchar(255) NOT NULL default '',
  `chemin_complet_en` varchar(255) NOT NULL default '',
  `chemin_complet_es` varchar(255) NOT NULL default '',
  `chemin_complet_su` varchar(255) NOT NULL default '',
  `chemin_complet_du` varchar(255) NOT NULL default '',
  `chemin_complet_cz` varchar(255) NOT NULL default '',
  `chemin_complet_ar` varchar(255) NOT NULL default '',
  `chemin_complet_it` varchar(255) NOT NULL default '',
  `nom_service_archiveur` varchar(100) default NULL,
  `identifiant_service_archiveur` varchar(100) default NULL,
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `AffiliationService` (
  `organisme` varchar(30) NOT NULL default '',
  `id_pole` int(11) NOT NULL default '0',
  `id_service` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id_service`,`id_pole`,`organisme`),
  KEY `id_service` (`id_service`),
  KEY `id_pole` (`id_pole`),
  KEY `organisme` (`organisme`)
) ENGINE=InnoDB ;


ALTER TABLE `AffiliationService` ADD CONSTRAINT `AffiliationService_Pole` FOREIGN KEY (`id_pole`, `organisme`) REFERENCES `Service` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `AffiliationService` ADD CONSTRAINT `AffiliationService_Service` FOREIGN KEY (`id_service`, `organisme`) REFERENCES `Service` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE ;

--- AME migration consultation
ALTER TABLE `consultation` ADD  `type_envoi` char(1) NOT NULL default '';
ALTER TABLE `consultation` ADD  `chiffrement_offre` char(1) NOT NULL default '';
ALTER TABLE `consultation` ADD  `env_candidature` int(1) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `env_offre` int(3) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `env_anonymat` int(1) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `id_etat_consultation` int(11) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `reference_connecteur` varchar(255) NOT NULL default '';
ALTER TABLE `consultation` ADD  `cons_statut` char(1) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `id_approbateur` int(11) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `id_valideur` int(11) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `service_validation` int(11) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `id_createur` int(11) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `nom_createur` varchar(100) default NULL;
ALTER TABLE `consultation` ADD  `prenom_createur` varchar(100) default NULL;
ALTER TABLE `consultation` ADD  `signature_acte_engagement` enum('0','1') NOT NULL default '0';
ALTER TABLE `consultation` ADD  `archiveMetaDescription` longtext;
ALTER TABLE `consultation` ADD  `archiveMetaMotsClef` longtext;
ALTER TABLE `consultation` ADD  `archiveIdBlobZip` int(11) default NULL;
ALTER TABLE `consultation` ADD  `decision_partielle` enum('0','1') NOT NULL default '0';
ALTER TABLE `consultation` ADD  `type_decision_a_renseigner` enum('0','1') NOT NULL default '1';
ALTER TABLE `consultation` ADD  `type_decision_attribution_marche` enum('0','1') NOT NULL default '1';
ALTER TABLE `consultation` ADD  `type_decision_declaration_sans_suite` enum('0','1') NOT NULL default '1';
ALTER TABLE `consultation` ADD  `type_decision_declaration_infructueux` enum('0','1') NOT NULL default '1';
ALTER TABLE `consultation` ADD  `type_decision_selection_entreprise` enum('0','1') NOT NULL default '1';
ALTER TABLE `consultation` ADD  `type_decision_attribution_accord_cadre` enum('0','1') NOT NULL default '1';
ALTER TABLE `consultation` ADD  `type_decision_admission_sad` enum('0','1') NOT NULL default '1';
ALTER TABLE `consultation` ADD  `type_decision_autre` enum('0','1') NOT NULL default '1';
ALTER TABLE `consultation` ADD  `id_archiveur` int(11) default NULL;
ALTER TABLE `consultation` ADD  `prenom_nom_agent_telechargement_plis` varchar(255) default NULL;
ALTER TABLE `consultation` ADD  `id_agent_telechargement_plis` int(11) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `path_telechargement_plis` varchar(255) default NULL;
ALTER TABLE `consultation` ADD  `date_telechargement_plis` varchar(20) default NULL;
ALTER TABLE `consultation` ADD  `service_validation_intermediaire` int(11) default NULL;
ALTER TABLE `consultation` ADD  `env_offre_technique` int(3) NOT NULL default '0';
ALTER TABLE `consultation` ADD  `ref_org_partenaire` varchar(250) NOT NULL default '';
ALTER TABLE `consultation` ADD  `date_archivage` varchar(20) default NULL COMMENT 'Précise la date de réalisation de l''archivage';
ALTER TABLE `consultation` ADD  `date_decision_annulation` varchar(20) default NULL;
ALTER TABLE `consultation` ADD  `commentaire_annulation` text;

ALTER TABLE `Annonce` DROP FOREIGN KEY `Annonce_ibfk_1`;
ALTER TABLE `CategorieLot` DROP FOREIGN KEY `CategorieLot_ibfk_1`;
ALTER TABLE `Offre` DROP FOREIGN KEY `Offre_ibfk_2`;
ALTER TABLE `QuestionDCE` DROP FOREIGN KEY `QuestionDCE_ibfk_1`;
ALTER TABLE `Telechargement` DROP FOREIGN KEY `Telechargement_ibfk_1`;
ALTER TABLE `consultation_document_cfe` DROP FOREIGN KEY `consultation_document_cfe_ibfk_2`;
ALTER TABLE `consultation_document_cfe` DROP FOREIGN KEY `consultation_document_cfe_ibfk_1` ;
ALTER TABLE `consultation_document_cfe` CHANGE `ref_consultation` `ref_consultation` INT( 11 ) NOT NULL ;

ALTER TABLE `consultation` CHANGE `reference` `reference` INT( 11 ) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `Annonce` CHANGE `consultation_ref` `consultation_ref` INT( 11 ) NOT NULL ;
ALTER TABLE `CategorieLot` CHANGE `consultation_ref` `consultation_ref` INT( 11 ) NOT NULL ;
ALTER TABLE `Offre` CHANGE `consultation_ref` `consultation_ref` INT( 11 ) NOT NULL ;
ALTER TABLE `QuestionDCE` CHANGE `consultation_ref` `consultation_ref` INT( 11 ) NOT NULL ;
ALTER TABLE `Telechargement` CHANGE `consultation_ref` `consultation_ref` INT( 11 ) NOT NULL ;

ALTER TABLE `consultation_document_cfe`  ADD CONSTRAINT `consultation_document_cfe_ibfk_1` FOREIGN KEY (`ref_consultation`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Annonce` ADD CONSTRAINT `Annonce_consultation` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `CategorieLot` ADD CONSTRAINT `CategorieLot_consultation` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Offre` ADD CONSTRAINT `Offre_consultation` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `QuestionDCE`  ADD CONSTRAINT `QuestionDCE_consultation` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `Telechargement`  ADD CONSTRAINT `Telechargement_consultation` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;



-- AME migration DCE

CREATE TABLE IF NOT EXISTS `DCE` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` INT( 11 ) NOT NULL default '0',
  `dce` int(11) NOT NULL default '0',
  `nom_dce` varchar(150) NOT NULL,
  `statut` char(1) NOT NULL default '1',
  `nom_fichier` varchar(80) NOT NULL default '',
  `ancien_fichier` varchar(80) NOT NULL default '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(11) NOT NULL default '0',
   PRIMARY KEY  (`id`,`organisme`),
   KEY `consultation_ref` (`consultation_ref`),
   KEY `organisme` (`organisme`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;
 
 
ALTER TABLE `DCE` ADD CONSTRAINT `DCE_consultation` FOREIGN KEY ( `organisme`, `consultation_ref` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;

-- AME migration RG 

CREATE TABLE IF NOT EXISTS `RG` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `rg` int(11) NOT NULL default '0',
  `nom_fichier` varchar(150) NOT NULL,
  `statut` char(1) NOT NULL default '1',
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

ALTER TABLE `RG` ADD CONSTRAINT `RG_consultation` FOREIGN KEY ( `organisme`, `consultation_ref` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;

--- AME migration DateFin

CREATE TABLE IF NOT EXISTS `DATEFIN` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `datefin` datetime NOT NULL default '0000-00-00 00:00:00',
  `statut` char(1) NOT NULL default '1',
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `DATEFIN` ADD CONSTRAINT `DATEFIN_consultation` FOREIGN KEY ( `organisme`, `consultation_ref` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;

--- AME migration Complement

CREATE TABLE IF NOT EXISTS `Complement` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `complement` int(11) NOT NULL default '0',
  `nom_fichier` varchar(80) NOT NULL default '',
  `statut` char(1) NOT NULL default '1',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `Complement` ADD CONSTRAINT `Complement_consultation` FOREIGN KEY ( `organisme`, `consultation_ref` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;

-- AME migration HistoriqueConsultation

CREATE TABLE IF NOT EXISTS `historiques_consultation` (
  `id` int(50) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `ref_consultation` int(50) NOT NULL default '0',
  `statut` char(1) NOT NULL default '',
  `id_agent` int(50) NOT NULL default '0',
  `nom_agent` varchar(250) NOT NULL default '',
  `prenom_agent` varchar(250) NOT NULL default '',
  `nom_element` varchar(250) NOT NULL default '',
  `valeur` varchar(250) NOT NULL default '',
  `valeur_detail_1` varchar(250) NOT NULL default '',
  `valeur_detail_2` varchar(250) NOT NULL default '',
  `numero_lot` varchar(250) NOT NULL default '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `ref_Consultation` (`ref_consultation`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;


-- AME migration NumerotationRefConsAuto

CREATE TABLE IF NOT EXISTS `Numerotation_ref_cons_auto` (
  `id_cons_auto` int(11) NOT NULL default '0',
  `organisme` varchar(30) NOT NULL default '',
  `annee` year(4) NOT NULL default '0000',
  `service_id` int(11) default NULL
) ENGINE=InnoDB ;

-- AME migration CategorieLot

ALTER TABLE `CategorieLot` ADD `decision` enum('0','1') NOT NULL default '0';

-- AME migration visitesLieux

CREATE TABLE IF NOT EXISTS `visite_lieux` (
  `id` int(50) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `reference` int(50) NOT NULL default '0',
  `adresse` varchar(250) NOT NULL default '',
  `date` varchar(50) NOT NULL default '',
  `lot` char(1) NOT NULL default '',
  `adresse_fr` varchar(255) default NULL,
  `adresse_en` varchar(255) default NULL,
  `adresse_es` varchar(255) default NULL,
  `adresse_su` varchar(255) default NULL,
  `adresse_du` varchar(255) default NULL,
  `adresse_cz` varchar(255) default NULL,
  `adresse_ar` varchar(255) default NULL,
  `adresse_it` varchar(255) default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `reference` (`reference`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `visite_lieux` ADD CONSTRAINT `visite_lieux_consultation` FOREIGN KEY ( `organisme`, `reference` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;

-- AME Migration  `InterneConsultation`

CREATE TABLE IF NOT EXISTS `InterneConsultation` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `interne_id` int(11) NOT NULL default '0',
  `consultation_ref` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `interne_id` (`interne_id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `InterneConsultation` ADD CONSTRAINT `InterneConsultation_consultation` FOREIGN KEY ( `organisme`, `consultation_ref` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;

-- AME migration `InterneConsultationSuiviSeul`

CREATE TABLE IF NOT EXISTS `InterneConsultationSuiviSeul` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `interne_id` int(11) NOT NULL default '0',
  `consultation_ref` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `interne_id` (`interne_id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `InterneConsultationSuiviSeul` ADD CONSTRAINT `InterneConsultationSuiviSeul_consultation` FOREIGN KEY ( `organisme`, `consultation_ref` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;

--HHA le 4 mai 2011: modification du type de support de la table Telechargement

ALTER TABLE `Telechargement` CHANGE `support` `support` ENUM( '1', '2', '3' ) NOT NULL DEFAULT '1' ;

--HHA le 11 mai 2011:Creation de la table TireurPlan

CREATE TABLE IF NOT EXISTS `TireurPlan` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`organisme` varchar( 30 ) NOT NULL default '',
`nom` varchar( 100 ) NOT NULL default '',
`email` varchar( 100 ) NOT NULL default '',
`id_service` int( 11 ) default NULL ,
PRIMARY KEY ( `id` , `organisme` )
) ENGINE = InnoDB AUTO_INCREMENT =1;	

--HHA le 13 mai 2011:Creation de la table CertificatPermanent

CREATE TABLE IF NOT EXISTS `CertificatPermanent` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `Titre` varchar(100) NOT NULL default '',
  `Prenom` varchar(100) NOT NULL default '',
  `Nom` varchar(100) NOT NULL default '',
  `EMail` varchar(100) NOT NULL default '',
  `Certificat` mediumtext NOT NULL,
  `service_id` int(11) NOT NULL default '-1',
  `master_key` enum('0','1') NOT NULL default '0',
  `id_agent` int(11) default '0',
  `CSP` varchar(200) default '',
  `date_modification` varchar(200) default '',
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE = InnoDB AUTO_INCREMENT =1 ;


--HHA le 16 mai 2011:Creation de la table AcheteurPublic

CREATE TABLE IF NOT EXISTS `AcheteurPublic` (
  `id` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `denomination` varchar(100) NOT NULL default '',
  `prm` varchar(100) NOT NULL default '',
  `adresse` varchar(100) NOT NULL default '',
  `cp` varchar(5) NOT NULL default '',
  `ville` varchar(100) NOT NULL default '',
  `dept` char(3) NOT NULL default '',
  `type_org` char(1) NOT NULL default '',
  `telephone` varchar(100) NOT NULL default '',
  `fax` varchar(100) NOT NULL default '',
  `mail` varchar(100) NOT NULL default '',
  `url` varchar(100) NOT NULL default '',
  `url_acheteur` varchar(100) default NULL,
  `facture_numero` varchar(100) NOT NULL default '',
  `facture_code` varchar(100) NOT NULL default '',
  `facture_denomination` varchar(100) NOT NULL default '',
  `facturation_service` varchar(100) default NULL,
  `facture_adresse` varchar(255) NOT NULL default '',
  `facture_cp` varchar(10) NOT NULL default '',
  `facture_ville` varchar(100) NOT NULL default '',
  `facture_pays` varchar(100) NOT NULL default '',
  `id_aapc` int(22) NOT NULL default '0',
  `boamp_login` varchar(100) NOT NULL default '',
  `boamp_password` varchar(100) NOT NULL default '',
  `boamp_mail` varchar(100) NOT NULL default '',
  `boamp_target` char(1) NOT NULL default '0',
  `default_form_values` longtext NOT NULL,
  `defaut_form_am_boamp` longtext NOT NULL,
  `defaut_form_am_boamp_joue` longtext NOT NULL,
  `defaut_form_mapa_boamp` longtext NOT NULL,
  `defaut_form_aconcours` longtext NOT NULL,
  `defaut_form_as_boamp_joue` longtext NOT NULL,
  `defaut_form_aa_boamp` longtext NOT NULL,
  `defaut_form_ar_mapa_boamp` longtext NOT NULL,
  `defaut_form_05_boamp` longtext NOT NULL,
  `defaut_form_rect` longtext NOT NULL,
  `defaut_form_aa_boamp_joue` longtext NOT NULL,
  `id_service` int(11) NOT NULL default '0',
  `livraison_service` varchar(100) default NULL,
  `livraison_adresse` varchar(100) default NULL,
  `livraison_code_postal` varchar(5) default NULL,
  `livraison_ville` varchar(100) default NULL,
  `livraison_pays` varchar(100) default NULL,
  `type_pouvoir_activite` text,
  `code_nuts` text,
  `modalites_financement` text,
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


--HHA le 16 mai 2011:Creation de la table Groupe_Moniteur

CREATE TABLE IF NOT EXISTS `Groupe_Moniteur` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `id_service` int(11) NOT NULL default '0',
  `Identifiant` varchar(50) NOT NULL default '',
  `Mdp` varchar(50) NOT NULL default '',
  `Num_Abonnement` varchar(50) default NULL,
  `Num_Abonne` varchar(50) default NULL,
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

--HHA le 16 mai 2011:Creation de la table ProcedureEquivalence

CREATE TABLE IF NOT EXISTS `ProcedureEquivalence` (
  `id_type_procedure` int(11) NOT NULL default '0',
  `organisme` varchar(30) NOT NULL default '',
  `elec_resp` char(2) NOT NULL default '',
  `no_elec_resp` char(2) NOT NULL default '',
  `cipher_enabled` char(2) NOT NULL default '',
  `cipher_disabled` char(2) NOT NULL default '',
  `signature_enabled` char(2) NOT NULL default '',
  `signature_disabled` char(2) NOT NULL default '',
  `env_candidature` char(2) NOT NULL default '',
  `env_offre` char(2) NOT NULL default '',
  `env_anonymat` char(2) NOT NULL default '',
  `envoi_complet` char(2) NOT NULL default '',
  `envoi_differe` char(2) NOT NULL default '',
  `procedure_publicite` char(2) NOT NULL default '',
  `procedure_restreinte_candidature` char(2) NOT NULL default '',
  `procedure_restreinte_offre` char(2) NOT NULL default '',
  `envoi_mail_par_mpe` char(2) NOT NULL default '0',
  `no_envoi_mail_par_mpe` char(2) NOT NULL default '0',
  `mise_en_ligne1` char(2) NOT NULL default '',
  `mise_en_ligne2` char(2) NOT NULL default '',
  `mise_en_ligne3` char(2) NOT NULL default '',
  `mise_en_ligne4` char(2) NOT NULL default '',
  `env_offre_type_unique` char(2) NOT NULL default '',
  `env_offre_type_multiple` char(2) NOT NULL default '',
  `no_fichier_annonce` char(2) NOT NULL default '',
  `fichier_importe` char(2) NOT NULL default '',
  `fichier_boamp` char(2) NOT NULL default '',
  `reglement_cons` char(2) NOT NULL default '',
  `dossier_dce` char(2) NOT NULL default '',
  `partial_dce_download` char(2) NOT NULL default '',
  `service` char(2) NOT NULL default '',
  `constitution_dossier_reponse` char(2) NOT NULL default '',
  `env_offre_type_unique2` char(2) NOT NULL default '',
  `env_offre_type_multiple2` char(2) NOT NULL default '',
  `gestion_envois_postaux` char(2) NOT NULL default '',
  `tireur_plan_non` char(2) NOT NULL default '',
  `tireur_plan_oui` char(2) NOT NULL default '',
  `tireur_plan_papier` char(2) NOT NULL default '',
  `tireur_plan_cdrom` char(2) NOT NULL default '',
  `tireur_plan_nom` char(2) NOT NULL default '',
  `tirage_descriptif` char(2) NOT NULL default '',
  `delai_date_limite_remise_pli` char(2) NOT NULL default '',
  `signature_propre` char(2) NOT NULL default '',
  `procedure_restreinte` char(2) NOT NULL default '',
  `ouverture_simultanee` char(2) NOT NULL default '',
  `type_decision_a_renseigner` char(2) NOT NULL default '-0',
  `type_decision_attribution_marche` char(2) NOT NULL default '-0',
  `type_decision_declaration_sans_suite` char(2) NOT NULL default '-0',
  `type_decision_declaration_infructueux` char(2) NOT NULL default '-0',
  `type_decision_selection_entreprise` char(2) NOT NULL default '-0',
  `type_decision_attribution_accord_cadre` char(2) NOT NULL default '-0',
  `type_decision_admission_sad` char(2) NOT NULL default '-0',
  `type_decision_autre` char(2) NOT NULL default '-0',
  `env_offre_technique` char(2) NOT NULL default '-0',
  `env_offre_technique_type_unique` char(2) NOT NULL default '-0',
  `env_offre_technique_type_multiple` char(2) NOT NULL default '-0',
  PRIMARY KEY  (`id_type_procedure`,`organisme`)
) ENGINE=InnoDB;

--HHA le 17 mai 2011:Update de la table RPA

ALTER TABLE `RPA` ADD `id_service` INT NOT NULL DEFAULT '0' ;
ALTER TABLE `RPA` ADD `Fonction` VARCHAR( 200 ) NULL ;
ALTER TABLE `RPA` ADD `acronymeOrg` varchar(200) NOT NULL AFTER `id`;
ALTER TABLE `RPA` ADD `pays` VARCHAR( 150 ) NULL ;
ALTER TABLE `RPA` ADD `date_creation` VARCHAR( 20 ) NULL ;
ALTER TABLE `RPA` ADD `date_modification` VARCHAR( 20 ) NULL ;
ALTER TABLE `RPA` ADD `responsable_archive` ENUM( '0', '1' ) NOT NULL DEFAULT '0';
ALTER TABLE `RPA` ADD `email` VARCHAR( 50 ) NULL ;
ALTER TABLE `RPA` ADD `telephone` VARCHAR( 50 ) NULL ;
ALTER TABLE `RPA` ADD `fax` VARCHAR( 50 ) NULL ;
ALTER TABLE `RPA` DROP PRIMARY KEY, ADD PRIMARY KEY(`id`,`acronymeOrg`);
ALTER TABLE `RPA` DROP FOREIGN KEY `RPA_ibfk_1`;
ALTER TABLE `RPA` DROP INDEX `organisme` ,ADD INDEX `organisme` ( `acronymeOrg` );
ALTER TABLE `RPA` ADD CONSTRAINT `RPA_ibfk_1` FOREIGN KEY ( `acronymeOrg` ) REFERENCES `Organisme` ( `acronyme` ) ;

--HHA le 17 mai 2011:Creation de la table  programme_previsionnel

CREATE TABLE IF NOT EXISTS `programme_previsionnel` (
  `id` int(20) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `nom_fichier` varchar(50) NOT NULL default '',
  `fichier` varchar(20) NOT NULL default '',
  `date` varchar(20) NOT NULL default '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '',
  `taille` int(20) NOT NULL default '0',
  `annee` varchar(20) NOT NULL,
  `service_id` int(20) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

--HHA le 18 mai 2011:Creation de la table  gestion_adresses

CREATE TABLE IF NOT EXISTS `gestion_adresses` (
  `id` int(50) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `id_service` int(11) NOT NULL default '0',
  `id_agent` int(11) NOT NULL default '0',
  `nom_agent` varchar(250) NOT NULL default '',
  `prenom_agent` varchar(250) NOT NULL default '',
  `adresse_depot_offres` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis` varchar(250) NOT NULL default '',
  `adresse_depot_offres_fr` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_fr` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_fr` varchar(250) NOT NULL default '',
  `adresse_depot_offres_ar` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_ar` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_ar` varchar(250) NOT NULL default '',
  `adresse_depot_offres_en` varchar(250) NOT NULL default '',
  `adresse_depot_offres_es` varchar(250) NOT NULL default '',
  `adresse_depot_offres_su` varchar(250) NOT NULL default '',
  `adresse_depot_offres_du` varchar(250) NOT NULL default '',
  `adresse_depot_offres_cz` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_en` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_es` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_su` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_du` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_cz` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_en` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_es` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_su` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_du` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_cz` varchar(250) NOT NULL default '',
  `adresse_depot_offres_it` varchar(250) NOT NULL default '',
  `adresse_retrais_dossiers_it` varchar(250) NOT NULL default '',
  `lieu_ouverture_plis_it` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

--HHA le 19 mai 2011:Creation de la table  fichiers_liste_marches

CREATE TABLE IF NOT EXISTS `fichiers_liste_marches` (
  `id` int(20) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `nom_fichier` varchar(50) NOT NULL default '',
  `fichier` varchar(20) NOT NULL default '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '',
  `taille` int(20) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--HHA le 19 mai 2011:Creation de la table  Marche

CREATE TABLE IF NOT EXISTS `Marche` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `idMarcheTrancheBudgetaire` int(11) unsigned default '0',
  `numeroMarcheAnnee` int(11) unsigned default '0',
  `idService` int(11) default '0',
  `nomAttributaire` varchar(255) default NULL,
  `dateNotification` date NOT NULL default '0000-00-00',
  `codePostal` varchar(20) default NULL,
  `natureMarche` varchar(30) default '1',
  `objetMarche` longtext,
  `montantMarche` varchar(50) default '0',
  `valide` enum('0','1') NOT NULL default '0',
  `isManuel` enum('0','1') NOT NULL default '0',
  `pmePmi` int(11) default '0',
  `id_decision_enveloppe` int(11) NOT NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `ListeMarches_FKIndex2` (`idMarcheTrancheBudgetaire`),
  KEY `ListeMarches_FKIndex3` (`idService`),
  KEY `numeroMarcheAnnee` (`numeroMarcheAnnee`),
  KEY `natureMarche` (`natureMarche`),
  KEY `id_decision_enveloppe` (`id_decision_enveloppe`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;
ALTER TABLE `Marche`ADD CONSTRAINT `Marche_ibfk_4` FOREIGN KEY (`natureMarche`) REFERENCES `CategorieConsultation` (`id`) ;


--HHA le 19 mai 2011:Creation de la table  MarchePublie

CREATE TABLE IF NOT EXISTS `MarchePublie` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `numeroMarcheAnnee` int(11) unsigned default '0',
  `idService` int(11) default '0',
  `isPubliee` enum('0','1') NOT NULL default '0',
  `isImportee` enum('0','1') NOT NULL default '0',
  `newVersion` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `ListeMarchePubliee_FKIndex1` (`idService`),
  KEY `ListeMarchePubliee_FKIndex2` (`numeroMarcheAnnee`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--HHA le 23 mai 2011:Creation de la table  ConsultationHistoriqueEtat

CREATE TABLE IF NOT EXISTS `ConsultationHistoriqueEtat` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `id_agent` int(11) NOT NULL default '0',
  `consultation_ref` int(11) NOT NULL default '0',
  `id_etat` int(11) NOT NULL default '0',
  `date_modification` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;
ALTER TABLE `ConsultationHistoriqueEtat` ADD CONSTRAINT `ConsultationHistoriqueEtat` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ;

--FBO le 4/5/2011 Migration vers base unique : depot de reponse ****
CREATE TABLE IF NOT EXISTS `Offres` (
  `id` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `entreprise_id` int(22) NOT NULL default '0',
  `inscrit_id` int(22) NOT NULL default '0',
  `signatureenvxml` longblob NOT NULL,
  `horodatage` longblob NOT NULL,
  `mailsignataire` varchar(80) NOT NULL default '',
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `untrustedserial` varchar(40) NOT NULL default '',
  `envoi_complet` char(1) NOT NULL default '',
  `date_depot_differe` datetime NOT NULL default '0000-00-00 00:00:00',
  `horodatage_envoi_differe` longblob NOT NULL,
  `signatureenvxml_envoi_differe` longblob NOT NULL,
  `external_serial` varchar(8) default NULL,
  `internal_serial` varchar(8) default NULL,
  `uid_offre` varchar(40) NOT NULL default '',
  `offre_selectionnee` int(11) NOT NULL default '0',
  `Observation` text,
  `xml_string` longtext NOT NULL,
  `nom_entreprise_inscrit` varchar(30) default NULL,
  `nom_inscrit` varchar(80) default NULL,
  `prenom_inscrit` varchar(80) default NULL,
  `adresse_inscrit` varchar(100) default NULL,
  `adresse2_inscrit` varchar(100) default NULL,
  `telephone_inscrit` varchar(20) default NULL,
  `fax_inscrit` varchar(30) default NULL,
  `code_postal_inscrit` varchar(5) default NULL,
  `ville_inscrit` varchar(50) default NULL,
  `pays_inscrit` varchar(50) default NULL,
  `acronyme_pays` varchar(10) default NULL,
  `siret_entreprise` varchar(14) default NULL,
  `identifiant_national` varchar(20) default NULL,
  `email_inscrit` varchar(100) default NULL,
  `siret_inscrit` varchar(14) default NULL,
  `nom_entreprise` varchar(30) default NULL,
  `horodatage_annulation` longblob,
  `date_annulation` varchar(20) default NULL,
  `signature_annulation` text,
  `depot_annule` enum('0','1') default '0',
  `string_annulation` text,
  `verification_certificat_annulation` varchar(5) default NULL,
  `offre_variante` enum('0','1') default NULL,
  `reponse_pas_a_pas` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  UNIQUE KEY `untrustedserial` (`untrustedserial`),
  KEY `inscrit_id` (`inscrit_id`),
  KEY `consultation_ref` (`consultation_ref`),
  KEY `entreprise_id` (`entreprise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `Enveloppe` (
  `id_enveloppe_electro` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `offre_id` int(22) NOT NULL default '0',
  `champs_optionnels` longblob NOT NULL,
  `fichier` int(11) NOT NULL default '0',
  `supprime` char(1) NOT NULL default '0',
  `cryptage` char(1) NOT NULL default '1',
  `nom_fichier` varchar(255) NOT NULL default '',
  `hash` varchar(40) NOT NULL default '',
  `type_env` int(1) NOT NULL default '0',
  `sous_pli` int(3) NOT NULL default '0',
  `attribue` char(1) NOT NULL default '0',
  `dateheure_ouverture` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `agent_id_ouverture` int(11) default NULL,
  `agent_id_ouverture2` int(11) default NULL,
  `donnees_ouverture` longblob NOT NULL,
  `horodatage_donnees_ouverture` longblob NOT NULL,
  `statut_enveloppe` int(2) NOT NULL default '1',
  `agent_telechargement` int(11) default NULL,
  `date_telechargement` varchar(20) default NULL,
  `repertoire_telechargement` varchar(100) default NULL,
  `nom_agent_ouverture` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id_enveloppe_electro`,`organisme`),
  KEY `offre_id` (`offre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `fichierEnveloppe` (
	`id_fichier` int( 11 ) NOT NULL AUTO_INCREMENT ,
	`organisme` varchar( 30 ) NOT NULL default '',
	`id_enveloppe` int( 11 ) NOT NULL default '0',
	`type_fichier` char( 3 ) NOT NULL default '',
	`num_ordre_fichier` int( 5 ) NOT NULL default '0',
	`nom_fichier` text NOT NULL ,
	`taille_fichier` varchar( 50 ) NOT NULL default '',
	`signature_fichier` text NOT NULL ,
	`hash` text NOT NULL ,
	`verification_certificat` varchar( 5 ) NOT NULL default '',
	`id_blob` int( 11 ) default NULL ,
	PRIMARY KEY ( `id_fichier` , `organisme` ) ,
	KEY `id_enveloppe` ( `id_enveloppe` )
) ENGINE = InnoDB  AUTO_INCREMENT =1;

CREATE TABLE IF NOT EXISTS `blocFichierEnveloppe` (
	`id_bloc_fichier` int( 11 ) NOT NULL AUTO_INCREMENT ,
	`organisme` varchar( 30 ) NOT NULL default '',
	`id_fichier` int( 11 ) NOT NULL default '0',
	`numero_ordre_bloc` int( 5 ) NOT NULL default '0',
	`id_blob_chiffre` int( 11 ) NOT NULL default '0',
	`id_blob_dechiffre` int( 11 ) default NULL ,
	PRIMARY KEY ( `id_bloc_fichier` , `organisme` ) ,
	KEY `id_fichier` ( `id_fichier` )
) ENGINE = InnoDB  AUTO_INCREMENT =1;

CREATE TABLE IF NOT EXISTS `blocFichierEnveloppeTemporaire` (
	`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
	`organisme` varchar( 30 ) NOT NULL default '',
	`uid` varchar( 50 ) NOT NULL default '',
	`type_env` int( 1 ) NOT NULL default '0',
	`sous_pli` int( 3 ) NOT NULL default '0',
	`type_fichier` char( 3 ) NOT NULL default '',
	`numero_ordre_fichier` int( 5 ) NOT NULL default '0',
	`numero_ordre_bloc` int( 5 ) NOT NULL default '0',
	`id_blob` int( 11 ) NOT NULL default '0',
	PRIMARY KEY ( `id` , `organisme` )
) ENGINE = InnoDB  AUTO_INCREMENT =1;

CREATE TABLE IF NOT EXISTS `CertificatChiffrement` (
	`consultation_ref` int( 11 ) NOT NULL default '0',
	`organisme` varchar( 30 ) NOT NULL default '',
	`type_env` int( 1 ) NOT NULL default '0',
	`sous_pli` int( 3 ) NOT NULL default '0',
	`index_certificat` int( 3 ) NOT NULL default '1',
	`certificat` mediumtext NOT NULL ,
	PRIMARY KEY ( `consultation_ref` , `type_env` , `sous_pli` , `index_certificat` , `organisme` ) ,
	KEY `consultation_ref` ( `consultation_ref` )
) ENGINE = InnoDB ;

CREATE TABLE IF NOT EXISTS `Offre_papier` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_entreprise` varchar(30) NOT NULL default '',
  `date_depot` datetime NOT NULL default '0000-00-00 00:00:00',
  `external_serial` varchar(8) default NULL,
  `internal_serial` varchar(8) default NULL,
  `offre_selectionnee` int(11) NOT NULL default '0',
  `nom` varchar(80) default NULL,
  `prenom` varchar(80) default NULL,
  `adresse` varchar(100) default NULL,
  `adresse2` varchar(100) default NULL,
  `telephone` varchar(20) default NULL,
  `fax` varchar(30) default NULL,
  `code_postal` varchar(5) default NULL,
  `ville` varchar(50) default NULL,
  `pays` varchar(50) default NULL,
  `acronyme_pays` varchar(10) default NULL,
  `siret` varchar(14) default NULL,
  `identifiant_national` varchar(20) default NULL,
  `email` varchar(100) default NULL,
  `observation` text,
  `date_annulation` varchar(20) default NULL,
  `depot_annule` enum('0','1') NOT NULL default '0',
  `offre_variante` enum('0','1') default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB  AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `Enveloppe_papier` (
	`id_enveloppe_papier` int( 11 ) NOT NULL AUTO_INCREMENT ,
	`organisme` varchar( 30 ) NOT NULL default '',
	`offre_papier_id` int( 22 ) NOT NULL default '0',
	`statut` int( 11 ) NOT NULL default '0',
	`supprime` char( 1 ) NOT NULL default '0',
	`cryptage` char( 1 ) NOT NULL default '1',
	`is_send` int( 1 ) NOT NULL default '1',
	`type_env` int( 1 ) NOT NULL default '0',
	`sous_pli` int( 3 ) NOT NULL default '0',
	`champs_optionnels` longtext,
	`agent_id_ouverture` int( 11 ) NOT NULL default '0',
	`dateheure_ouverture` datetime NOT NULL default '0000-00-00 00:00:00',
	`statut_enveloppe` int( 2 ) NOT NULL default '1',
	`enveloppe_postule` enum( '0', '1' ) NOT NULL default '1',
	`nom_agent_ouverture` varchar( 100 ) default NULL ,
	PRIMARY KEY ( `id_enveloppe_papier` , `organisme` ) ,
	KEY `offre_papier_id` ( `offre_papier_id` )
) ENGINE = InnoDB AUTO_INCREMENT =1;

CREATE TABLE IF NOT EXISTS `decisionEnveloppe` (
  `id_decision_enveloppe` int(11) NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `id_offre` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `decision` enum('0','1') NOT NULL default '0',
  `date_notification` varchar(10) default NULL,
  `code_postal` varchar(5) default NULL,
  `pme_pmi` int(11) NOT NULL default '0',
  `tranche_budgetaire` int(11) default NULL,
  `montant_marche` varchar(50) default NULL,
  `categorie` int(11) default NULL,
  `objet_marche` text,
  `commentaire` text,
  `fichier_joint` int(11) default NULL,
  `nom_fichier_joint` varchar(100) default NULL,
  `type_enveloppe` enum('1','2') default NULL,
  `numero_marche` text,
  `statutEJ` text,
  `note` varchar(200) NOT NULL default '',
  `classement` varchar(200) NOT NULL default '',
  `id_blob_pieces_notification` varchar(20) NOT NULL COMMENT 'Permet de stocker les idBLob des pièces de notification',
  PRIMARY KEY  (`id_decision_enveloppe`,`organisme`),
  KEY `ref_Consultation` (`consultation_ref`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `StatutEnveloppe` (
  `id_statut` int(2) NOT NULL default '0',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id_statut`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `StatutEnveloppe`
--

INSERT INTO `StatutEnveloppe` (`id_statut`, `description`) VALUES
(1, 'FERMEE'),
(2, 'DEFINE_OUVERTE_EN_LIGNE'),
(3, 'DEFINE_OUVERTE_HORS_LIGNE'),
(4, 'DEFINE_REFUSEE'),
(5, 'OUVERTE'),
(6, 'DEFINE_SUPPRIMEE'),
(7, 'DEFINE_OUVERTE_A_DISTANCE');

CREATE TABLE IF NOT EXISTS `Admissibilite_Enveloppe_Lot` (
  `id_Offre` int(22) NOT NULL default '0',
  `organisme` varchar( 30 ) NOT NULL default '',
  `sous_pli` int(3) NOT NULL default '0',
  `admissibilite` int(1) NOT NULL default '0',
  `commentaire` text,
  `type_enveloppe` int(1) NOT NULL default '2',
  PRIMARY KEY  (`id_Offre`,`sous_pli`,`type_enveloppe`,`organisme`)
) ENGINE=InnoDB ;

CREATE TABLE IF NOT EXISTS `Admissibilite_Enveloppe_papier_Lot` (
  `id_offre_papier` int(22) NOT NULL default '0',
  `organisme` varchar( 30 ) NOT NULL default '',
  `sous_pli` int(3) NOT NULL default '0',
  `admissibilite` int(1) NOT NULL default '0',
  `commentaire` text,
  `type_enveloppe` int(1) NOT NULL default '2',
  PRIMARY KEY  (`id_offre_papier`,`sous_pli`,`type_enveloppe`,`organisme`)
) ENGINE=InnoDB ;

ALTER TABLE  `blocFichierEnveloppe` DROP PRIMARY KEY ,ADD PRIMARY KEY (  `id_bloc_fichier` ,  `organisme` );
ALTER TABLE  `blocFichierEnveloppe` ADD CONSTRAINT `blocFichierEnveloppe_fichierEnveloppe` FOREIGN KEY (  `id_fichier`, `organisme` ) REFERENCES  `fichierEnveloppe` (`id_fichier`, `organisme`);

ALTER TABLE  `Enveloppe` DROP INDEX  `offre_id` , ADD INDEX  `offre_id` (  `offre_id` ,  `organisme` );

CREATE TABLE IF NOT EXISTS `resultat_analyse` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `id_offre` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `montant_offre` varchar(200) default NULL,
  `note` varchar(200) default NULL,
  `classement` tinyint(200) default NULL,
  `observation` varchar(200) default NULL,
  `type_enveloppe` int(1) NOT NULL default '0',
  `offre_variante` enum('0','1') default NULL COMMENT 'Ce champ permet de definir le montant de bese de ceux de variantes: null pour le montant d''une offre normale, 0 pour le montant de base et 1 pour les montants de variantes',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `ref_Consultation` (`consultation_ref`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `resultat_analyse_decision` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `date_decision` varchar(20) default NULL,
  `type_decision` varchar(200) default NULL,
  `commentaire` text,
  `autre_type_decision` text,
  `autre` text,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `ref_Consultation` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `DecisionLot` (
  `id_decision_lot` int(11) NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `id_type_decision` int(11) NOT NULL default '0',
  `autre_a_preciser` varchar(100) default NULL,
  `date_decision` date default NULL,
  `commentaire` text,
  `date_maj` varchar(20) default NULL,
  PRIMARY KEY  (`id_decision_lot`,`organisme`),
  KEY `ref_Consultation` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `TypeDecision` (
  `id_type_decision` int(11) NOT NULL auto_increment,
  `code_type_decision` varchar(100) NOT NULL COMMENT 'Pour obtenir le libellé, ce champ est mis en correspondance avec messages.xml',
  PRIMARY KEY  (`id_type_decision`)
) ENGINE=InnoDB AUTO_INCREMENT=10 ;

--
-- Contenu de la table `TypeDecision`
--

INSERT INTO `TypeDecision` (`id_type_decision`, `code_type_decision`) VALUES
(2, 'DECISION_ATTRIBUTION_MARCHE'),
(3, 'DECISION_DECLARATION_SANS_SUITE'),
(4, 'DECISION_DECLARATION_INFRUCTUEUX'),
(5, 'DECISION_SELECTION_ENTREPRISES'),
(6, 'DECISION_ATTRIBUTION_DE_ACCORD_CADRE'),
(7, 'DECISION_ADMISSION_SAD'),
(8, 'DECISION_AUTRE'),
(9, 'DECISION_A_RENSEIGNER');


-- FBO Migration BLOBS TO UNIQUE DB

CREATE TABLE IF NOT EXISTS `blobOrganisme` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `revision` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`, `organisme`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blobOrganisme_file` (
  `id` int(11) NOT NULL default '0',
  `organisme` varchar( 30 ) NOT NULL default '',
  `revision` int(11) NOT NULL default '0',
  `name` text NOT NULL,
  `deletion_datetime` datetime default NULL,
  PRIMARY KEY  (`id`,`revision`, `organisme`)
) ENGINE=InnoDB ;

--Migration BD unique : Encheres

CREATE TABLE IF NOT EXISTS `EnchereEntreprisePmi` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `nom` varchar(255) NOT NULL default '',
  `numeroAnonyme` int(11) unsigned default NULL,
  `email` varchar(255) NOT NULL default '',
  `mdp` varchar(255) NOT NULL default '',
  `noteTechnique` double default NULL,
  `idEntreprise` int(10) unsigned default NULL,
  `datePing` datetime NOT NULL default '0000-00-00 00:00:00',
  `nomAgentConnecte` varchar(255) default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idEnchere` (`idEnchere`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `EncherePmi` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `refConsultation` int(10) unsigned default NULL,
  `idEntiteeAssociee` int(11) default NULL,
  `referenceUtilisateur` varchar(45) default NULL,
  `auteur` varchar(255) default NULL,
  `idLot` int(10) unsigned default NULL,
  `objet` longtext,
  `dateDebut` datetime default '0000-00-00 00:00:00',
  `dateFin` datetime default '0000-00-00 00:00:00',
  `dateSuspension` datetime default '0000-00-00 00:00:00',
  `delaiProlongation` int(11) default NULL,
  `commentaire` longtext,
  `meilleureEnchereObligatoire` enum('0','1') NOT NULL default '0',
  `typeBaremeNETC` enum('1','2','3','4') NOT NULL default '1',
  `typeBaremeEnchereGlobale` enum('1','2') NOT NULL default '1',
  `meilleurNoteHaute` enum('0','1') NOT NULL default '0',
  `nbrCandidatsVisible` enum('0','1') NOT NULL default '1',
  `listeCandidatsVisible` enum('0','1') NOT NULL default '1',
  `rangVisible` enum('0','1') NOT NULL default '1',
  `meilleureOffreVisible` enum('0','1') NOT NULL default '1',
  `montantReserve` double default NULL,
  `noteMaxBaremeRelatif` double default NULL,
  `coeffA` double default NULL,
  `coeffB` double default NULL,
  `coeffC` double default NULL,
  `mail` longtext,
  PRIMARY KEY  (`id`,`organisme`),
  UNIQUE KEY `referenceUtilisateur` (`referenceUtilisateur`),
  KEY `refConsultation` (`refConsultation`),
  KEY `idEntiteeAssociee` (`idEntiteeAssociee`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `EnchereOffre` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `idEnchereEntreprise` int(10) unsigned NOT NULL default '0',
  `date` datetime default '0000-00-00 00:00:00',
  `valeurTIC` double default NULL,
  `valeurTC` double default NULL,
  `valeurNETC` double default NULL,
  `valeurNGC` double default NULL,
  `rang` int(11) default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idEnchereEntreprise` (`idEnchereEntreprise`),
  KEY `idEnchere` (`idEnchere`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `EnchereOffreReference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idEnchereOffre` int(11) NOT NULL default '0',
  `idEnchereReference` int(10) unsigned NOT NULL default '0',
  `valeur` double NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idEnchereOffre` (`idEnchereOffre`),
  KEY `idEnchereReference` (`idEnchereReference`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `EnchereReference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `libelle` varchar(255) NOT NULL default '',
  `quantite` double default NULL,
  `isMontant` enum('0','1') NOT NULL default '1',
  `unite` varchar(20) default NULL,
  `pasMin` double default '0',
  `pasMax` double default NULL,
  `valeurReference` double default NULL,
  `valeurDepartCommune` enum('0','1') NOT NULL default '1',
  `valeurDepart` double default NULL,
  `typeBaremeReference` enum('1','2','3') default NULL,
  `ponderationNoteReference` double default '1',
  `noteMaxBaremeRelatif` double default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idEnchere` (`idEnchere`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `EnchereTrancheBaremeReference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idReference` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idReference` (`idReference`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `EnchereTranchesBaremeNETC` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idEnchere` (`idEnchere`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `EnchereValeursInitiales` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idEnchereEntreprise` int(10) unsigned NOT NULL default '0',
  `idEnchereReference` int(10) unsigned NOT NULL default '0',
  `valeur` double default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idEnchereEntreprise` (`idEnchereEntreprise`),
  KEY `idEnchereReference` (`idEnchereReference`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `Parametrage_Enchere` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `refConsultation` int(10) unsigned default NULL,
  `idEntiteeAssociee` int(11) default NULL,
  `referenceUtilisateur` varchar(45) default NULL,
  `auteur` varchar(255) default NULL,
  `idLot` int(10) unsigned default NULL,
  `objet` longtext,
  `dateDebut` datetime default '0000-00-00 00:00:00',
  `dateFin` datetime default '0000-00-00 00:00:00',
  `dateSuspension` datetime default '0000-00-00 00:00:00',
  `delaiProlongation` int(11) default NULL,
  `commentaire` longtext,
  `meilleureEnchereObligatoire` enum('0','1') NOT NULL default '0',
  `typeBaremeNETC` enum('1','2','3','4') NOT NULL default '1',
  `typeBaremeEnchereGlobale` enum('1','2') NOT NULL default '1',
  `meilleurNoteHaute` enum('0','1') NOT NULL default '0',
  `nbrCandidatsVisible` enum('0','1') NOT NULL default '1',
  `listeCandidatsVisible` enum('0','1') NOT NULL default '1',
  `rangVisible` enum('0','1') NOT NULL default '1',
  `meilleureOffreVisible` enum('0','1') NOT NULL default '1',
  `montantReserve` double default NULL,
  `noteMaxBaremeRelatif` double default NULL,
  `coeffA` double default NULL,
  `coeffB` double default NULL,
  `coeffC` double default NULL,
  `mail` longtext,
  `note_entreprises` varchar(50) default NULL COMMENT 'Permet de stocker la note commune aux entreprises lors de la phase "Parametrage".',
  PRIMARY KEY  (`id`,`organisme`),
  UNIQUE KEY `referenceUtilisateur` (`referenceUtilisateur`),
  KEY `refConsultation` (`refConsultation`),
  KEY `idEntiteeAssociee` (`idEntiteeAssociee`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `Parametrage_Enchere_Reference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `libelle` varchar(255) NOT NULL default '',
  `quantite` double default NULL,
  `isMontant` enum('0','1') NOT NULL default '1',
  `unite` varchar(20) default NULL,
  `pasMin` double default '0',
  `pasMax` double default NULL,
  `valeurReference` double default NULL,
  `valeurDepartCommune` enum('0','1') NOT NULL default '1',
  `valeurDepart` double default NULL,
  `typeBaremeReference` enum('1','2','3') default NULL,
  `ponderationNoteReference` double default '1',
  `noteMaxBaremeRelatif` double default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idEnchere` (`idEnchere`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `Parametrage_Enchere_Tranches_Bareme_NETC` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idEnchere` (`idEnchere`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `Parametrage_Enchere_Tranche_Bareme_Reference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `organisme` varchar( 30 ) NOT NULL default '',
  `idReference` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idReference` (`idReference`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


ALTER TABLE  `EnchereEntreprisePmi` ADD CONSTRAINT `EnchereEntreprisePmi_EncherePmi` FOREIGN KEY (  `idEnchere` , `organisme`) REFERENCES  `EncherePmi` (`id`, `organisme`);
ALTER TABLE  `EnchereReference` ADD CONSTRAINT `EnchereReference_EncherePmi` FOREIGN KEY (  `idEnchere` , `organisme` ) REFERENCES  `EncherePmi` (`id`, `organisme`);
ALTER TABLE  `EnchereOffre` ADD CONSTRAINT `EnchereOffre_EncherePmi` FOREIGN KEY ( `idEnchere` , `organisme` ) REFERENCES  `EncherePmi` (`id`, `organisme`);
ALTER TABLE  `EnchereOffre` ADD CONSTRAINT `EnchereOffre_EnchereEntreprisePmi` FOREIGN KEY ( `idEnchereEntreprise`, `organisme` ) REFERENCES  `EnchereEntreprisePmi` (`id`, `organisme`);
ALTER TABLE  `EnchereOffreReference` ADD CONSTRAINT `EnchereOffreReference_EnchereOffre` FOREIGN KEY (  `idEnchereOffre` , `organisme`) REFERENCES  `EnchereOffre` (`id`, `organisme`);
ALTER TABLE  `EnchereOffreReference` ADD CONSTRAINT `EnchereOffreReference_EnchereReference` FOREIGN KEY (   `idEnchereReference`, `organisme` ) REFERENCES  `EnchereReference` (`id`, `organisme`);
ALTER TABLE  `EnchereTrancheBaremeReference` ADD CONSTRAINT `EnchereTrancheBaremeReference_EnchereReference` FOREIGN KEY ( `idReference`, `organisme`) REFERENCES  `EnchereReference` (`id`, `organisme`);
ALTER TABLE  `EnchereTranchesBaremeNETC` ADD CONSTRAINT `EnchereTranchesBaremeNETC_EncherePmi` FOREIGN KEY (   `idEnchere` , `organisme` ) REFERENCES  `EncherePmi` (`id`, `organisme`);
ALTER TABLE  `EnchereValeursInitiales` ADD CONSTRAINT `EnchereValeursInitiales_EnchereEntreprisePmi` FOREIGN KEY ( `idEnchereEntreprise`,  `organisme` ) REFERENCES  `EnchereEntreprisePmi` (`id`, `organisme`);
ALTER TABLE  `EnchereValeursInitiales` ADD CONSTRAINT `EnchereValeursInitiales_EnchereReference` FOREIGN KEY ( `idEnchereReference`, `organisme` ) REFERENCES  `EnchereReference` (`id`, `organisme`);
ALTER TABLE  `Parametrage_Enchere_Reference` ADD CONSTRAINT `Parametrage_Enchere_Reference_Parametrage_Enchere` FOREIGN KEY (  `idEnchere` , `organisme` ) REFERENCES  `Parametrage_Enchere` (`id`, `organisme`);
ALTER TABLE  `Parametrage_Enchere_Tranches_Bareme_NETC` ADD CONSTRAINT `Parametrage_Enchere_Tranches_Bareme_NETC_Parametrage_Enchere` FOREIGN KEY (  `idEnchere` , `organisme` ) REFERENCES  `Parametrage_Enchere` (`id`, `organisme`);
ALTER TABLE  `Parametrage_Enchere_Tranche_Bareme_Reference` ADD CONSTRAINT `Parametrage_Enchere_Tranche_Bareme_Reference_PER` FOREIGN KEY ( `idReference`, `organisme`) REFERENCES  `Parametrage_Enchere_Reference` (`id`, `organisme`);

-- OKO le 05/05/2011 : Ajout de nouvelles tables
--
-- Structure de la table `Echange`
--

CREATE TABLE IF NOT EXISTS `Echange` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `objet` varchar(255) NOT NULL default '',
  `corps` text NOT NULL,
  `expediteur` varchar(100) NOT NULL default '',
  `id_createur` int(11) default '0',
  `ref_consultation` int(11) NOT NULL default '0',
  `option_envoi` int(11) NOT NULL default '0',
  `date_message` varchar(25) NOT NULL default '',
  `format` int(11) NOT NULL default '0',
  `id_action_declencheur` int(11) NOT NULL default '0',
  `status` int(2) NOT NULL default '0',
  `service_id` int(11) default '0',
  `email_expediteur` varchar(100) NOT NULL default '',
  `id_type_message` int(11) NOT NULL,
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB;

--
-- Structure de la table `EchangeDestinataire`
--

CREATE TABLE IF NOT EXISTS `EchangeDestinataire` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_echange` int(11) NOT NULL default '0',
  `mail_destinataire` varchar(255) NOT NULL default '',
  `ar` enum('0','1') NOT NULL default '0',
  `date_ar` varchar(25) NOT NULL default '',
  `uid` varchar(32) NOT NULL default '',
  `type_ar` int(11) NOT NULL default '0',
  `id_inscrit` int(11) NOT NULL default '0',
  `id_entreprise` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `id_echange` (`id_echange`)
) ENGINE=InnoDB;

--
-- Contraintes pour la table `EchangeDestinataire`
--
ALTER TABLE `EchangeDestinataire` ADD CONSTRAINT `EchangeDestinataire_ibfk_1` FOREIGN KEY (`id_echange`) REFERENCES `Echange` (`id`) ON DELETE CASCADE;

--
-- Structure de la table `EchangePieceJointe`
--

CREATE TABLE IF NOT EXISTS `EchangePieceJointe` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_message` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `piece` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(25) NOT NULL default '',
  `taille` varchar(25) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `id_message` (`id_message`)
) ENGINE=InnoDB;

--
-- Contraintes pour la table `EchangePieceJointe`
--
ALTER TABLE `EchangePieceJointe` ADD CONSTRAINT `EchangePieceJointe_ibfk_1` FOREIGN KEY (`id_message`) REFERENCES `Echange` (`id`) ON DELETE CASCADE;

--
-- Structure de la table `EchangeFormat`
--

CREATE TABLE IF NOT EXISTS `EchangeFormat` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  `libelle_fr` varchar(100) NOT NULL,
  `libelle_en` varchar(100) NOT NULL,
  `libelle_es` varchar(100) NOT NULL,
  `libelle_su` varchar(100) NOT NULL,
  `libelle_du` varchar(100) NOT NULL,
  `libelle_cz` varchar(100) NOT NULL,
  `libelle_ar` varchar(100) NOT NULL,
  `libelle_it` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `EchangeTypeAR`
--

CREATE TABLE IF NOT EXISTS `EchangeTypeAR` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  `libelle_fr` varchar(100) NOT NULL,
  `libelle_en` varchar(100) NOT NULL,
  `libelle_es` varchar(100) NOT NULL,
  `libelle_su` varchar(100) NOT NULL,
  `libelle_du` varchar(100) NOT NULL,
  `libelle_cz` varchar(100) NOT NULL,
  `libelle_ar` varchar(100) NOT NULL,
  `libelle_it` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `EchangeTypeMessage`
--

CREATE TABLE IF NOT EXISTS `EchangeTypeMessage` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(100) NOT NULL COMMENT 'Les contenus de ce champ sont en correspondance avec messages.xml',
  `corps` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `Relation_Echange`
--

CREATE TABLE IF NOT EXISTS `Relation_Echange` (
  `id_echange` int(11) NOT NULL default '0',
  `organisme` varchar(30) NOT NULL default '',
  `id_externe` int(11) NOT NULL default '0',
  `type_relation` int(11) NOT NULL default '0'
) ENGINE=InnoDB;


ALTER TABLE `Relation_Echange` DROP PRIMARY KEY ;
--
-- Structure de la table `AnnonceJAL`
--

CREATE TABLE IF NOT EXISTS `AnnonceJAL` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `libelle_type` varchar(250) NOT NULL default '',
  `date_creation` varchar(20) NOT NULL,
  `objet` varchar(255) default NULL,
  `texte` text,
  `consultation_ref` int(11) NOT NULL default '0',
  `option_envoi` int(11) default NULL,
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB ;

--
-- Structure de la table `AnnonceJALPieceJointe`
--

CREATE TABLE IF NOT EXISTS `AnnonceJALPieceJointe` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_annonce_jal` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `piece` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(25) NOT NULL default '',
  `taille` varchar(25) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `id_annonce_jal` (`id_annonce_jal`)
) ENGINE=InnoDB ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `AnnonceJALPieceJointe`
--
ALTER TABLE `AnnonceJALPieceJointe` ADD CONSTRAINT `AnnonceJALPieceJointe_ibfk_1` FOREIGN KEY (`id_annonce_jal`) REFERENCES `AnnonceJAL` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
-- OKO le 09/05/2011
--
-- Structure de la table `Centrale_publication`
--

CREATE TABLE IF NOT EXISTS `Centrale_publication` (
  `id` int(20) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `nom` varchar(200) default NULL,
  `mail` varchar(200) default NULL,
  `fax` varchar(50) default NULL,
  `information` text,
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB;

--
-- Structure de la table `compte_centrale`
--

CREATE TABLE IF NOT EXISTS `compte_centrale` (
  `ID_COMPTE` int(20) NOT NULL auto_increment,
  `ORGANISME` varchar(30) NOT NULL,
  `ID_CENTRALE` int(20) default NULL,
  `MAIL` varchar(200) default NULL,
  `FAX` varchar(20) default NULL,
  `INFO_CIMPLEMENTAIRE` text,
  `ID_SERVICE` int(11) default '0',
  PRIMARY KEY  (`ID_COMPTE`,`ORGANISME`),
  KEY `ID_CENTRALE` (`ID_CENTRALE`)
) ENGINE=InnoDB;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `compte_centrale`
--
ALTER TABLE `compte_centrale` ADD CONSTRAINT `compte_centrale_ibfk_1` FOREIGN KEY (`ID_CENTRALE`) REFERENCES `Centrale_publication` (`id`);

--
-- Structure de la table `JAL`
--

CREATE TABLE IF NOT EXISTS `JAL` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_service` int(11) NOT NULL default '0',
  `nom` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `email_ar` varchar(100) NOT NULL default '',
  `telecopie` varchar(20) NOT NULL default '',
  `information_facturation` text NOT NULL,
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB;    

--
-- Structure de la table `DestinataireAnnonceJAL`
--

CREATE TABLE IF NOT EXISTS `DestinataireAnnonceJAL` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `idJAL` int(11) NOT NULL default '0',
  `idAnnonceJAL` int(11) NOT NULL default '0',
  `date_envoi` varchar(14) NOT NULL default '',
  `date_pub` varchar(14) NOT NULL default '',
  `statut` char(1) NOT NULL default '',
  `accuse` char(1) NOT NULL default '',
  `id_echange` int(11) default NULL,
  `date_ar` varchar(25) default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idJAL` (`idJAL`),
  KEY `idAnnonceJAL` (`idAnnonceJAL`)
) ENGINE=InnoDB;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `DestinataireAnnonceJAL`
--
ALTER TABLE `DestinataireAnnonceJAL`  ADD CONSTRAINT `DestinataireAnnonceJAL_ibfk_2` FOREIGN KEY (`idAnnonceJAL`) REFERENCES `AnnonceJAL` (`id`);
ALTER TABLE `DestinataireAnnonceJAL`  ADD CONSTRAINT `DestinataireAnnonceJAL_ibfk_1` FOREIGN KEY (`idJAL`) REFERENCES `JAL` (`id`);

--
-- Structure de la table `destinataire_centrale_pub`
--

CREATE TABLE IF NOT EXISTS `destinataire_centrale_pub` (
  `id` int(20) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_annonce_jal` int(20) default NULL,
  `id_compte` int(20) default NULL,
  `ids_journaux` varchar(200) default NULL,
  `statut` char(1) default NULL,
  `date_envoi` varchar(20) default '0000-00-00 00:00:00',
  `date_pub` varchar(20) default '0000-00-00 00:00:00',
  `accuse` varchar(20) default NULL,
  `id_echange` int(11) default NULL,
  `date_ar` varchar(25) default NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `id_annonce_jal` (`id_annonce_jal`),
  KEY `id_compte` (`id_compte`)
) ENGINE=InnoDB;

--
-- Structure de la table `Journaux`
--

CREATE TABLE IF NOT EXISTS `Journaux` (
  `ID_JOURNAL` int(20) NOT NULL auto_increment,
  `ORGANISME` varchar(30) NOT NULL,
  `ID_CENTRALE` int(20) NOT NULL default '0',
  `NOM_JOURNAL` varchar(200) default NULL,
  PRIMARY KEY  (`ID_JOURNAL`,`ORGANISME`),
  KEY `ID_CENTRALE` (`ID_CENTRALE`)
) ENGINE=InnoDB;

--
-- Structure de la table `AVIS`
--

CREATE TABLE IF NOT EXISTS `AVIS` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `consultation_ref` varchar(255) NOT NULL,
  `avis` int(11) NOT NULL default '0',
  `intitule_avis` int(2) NOT NULL default '0',
  `nom_avis` varchar(200) NOT NULL,
  `statut` char(1) NOT NULL default '0',
  `nom_fichier` varchar(200) NOT NULL,
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(11) NOT NULL default '0',
  `avis_telechargeable` int(11) NOT NULL default '0',
  `url` text,
  `type` char(1) default '0',
  `date_creation` varchar(20) NOT NULL,
  `date_pub` varchar(20) default NULL,
  `type_doc_genere` int(11) NOT NULL default '0',
  `langue` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contraintes pour la table `AVIS`
--
ALTER TABLE `AVIS` CHANGE `consultation_ref` `consultation_ref` INT( 11 ) NOT NULL ;
ALTER TABLE `AVIS` ADD CONSTRAINT `AVIS_consultation` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Structure de la table `CompteMoniteur`
--

CREATE TABLE IF NOT EXISTS `CompteMoniteur` (
  `id` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `moniteur_login` varchar(100) NOT NULL default '',
  `moniteur_password` varchar(100) NOT NULL default '',
  `moniteur_mail` varchar(100) NOT NULL default '',
  `moniteur_target` char(1) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB ;


--
-- Structure de la table `ReferentielTypeXml`
--

CREATE TABLE IF NOT EXISTS `ReferentielTypeXml` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_destinataire` varchar(50) NOT NULL default '',
  `libelle_type` varchar(100) NOT NULL default '',
  `libelle_type_fr` varchar(100) NOT NULL,
  `libelle_type_en` varchar(100) NOT NULL,
  `libelle_type_es` varchar(100) NOT NULL,
  `libelle_type_su` varchar(100) NOT NULL,
  `libelle_type_du` varchar(100) NOT NULL,
  `libelle_type_cz` varchar(100) NOT NULL,
  `libelle_type_ar` varchar(100) NOT NULL,
  `libelle_type_it` varchar(100) NOT NULL,
  `id_avis_marche` int(11) default NULL,
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB;


--
-- Structure de la table `ReferentielFormXml`
--

CREATE TABLE IF NOT EXISTS `ReferentielFormXml` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_type_xml` int(11) NOT NULL default '0',
  `consultation_ref` varchar(100) NOT NULL default '',
  `xml` text NOT NULL,
  `date_creation` varchar(20) default NULL,
  `statut` char(1) default NULL,
  `id_compte_boamp` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `id_type_xml` (`id_type_xml`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ReferentielFormXml`
--
ALTER TABLE `ReferentielFormXml` ADD CONSTRAINT `ReferentielFormXml_ibfk_1` FOREIGN KEY (`id_type_xml`) REFERENCES `ReferentielTypeXml` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Structure de la table `ReferentielDestinationFormXml`
--

CREATE TABLE IF NOT EXISTS `ReferentielDestinationFormXml` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `destinataire` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB;

--
-- Structure de la table `AnnonceBoamp`
--

CREATE TABLE IF NOT EXISTS `AnnonceBoamp` (
  `id_boamp` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `consultation_ref` INT( 11 ) NOT NULL default '0',
  `envoi_boamp` int(22) NOT NULL default '0',
  `date_envoi` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `ann_xml` longblob NOT NULL,
  `ann_pdf` longblob NOT NULL,
  `ann_form_values` longblob NOT NULL,
  `ann_error` longtext NOT NULL,
  `type_boamp` text NOT NULL,
  `type_ann` int(11) NOT NULL default '0',
  `datepub` varchar(10) NOT NULL default '0000-00-00',
  `num_ann` int(11) NOT NULL default '0',
  `parution` text NOT NULL,
  `id_jo` varchar(255) NOT NULL default '',
  `erreurs` text NOT NULL,
  `nom_fichier_xml` varchar(255) NOT NULL default '',
  `envoi_joue` enum('0','1') NOT NULL default '0',
  `mapa` enum('0','1') NOT NULL default '0',
  `implique_SAD` enum('0','1') NOT NULL default '0',
  `date_maj` datetime default NULL,
  `id_destination_form_xml` int(11) NOT NULL default '0',
  `id_form_xml` int(11) NOT NULL default '0',
  `id_type_xml` int(11) NOT NULL default '0',
  `statut_destinataire` char(10) NOT NULL,
  `accuse_reception` char(1) NOT NULL default '',
  `lien` varchar(50) NOT NULL default '',
  `lien_boamp` text,
  `lien_pdf` text,
  PRIMARY KEY  (`id_boamp`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`),
  KEY `id_destination_form_xml` (`id_destination_form_xml`),
  KEY `id_form_xml` (`id_form_xml`),
  KEY `id_type_xml` (`id_type_xml`),
  KEY `Idx_Annonce` (`consultation_ref`)
) ENGINE=InnoDB;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `AnnonceBoamp`
--

ALTER TABLE `AnnonceBoamp` ADD CONSTRAINT `AnnonceBoamp_consultation` FOREIGN KEY (`organisme`, `consultation_ref`) REFERENCES `consultation` (`organisme`, `reference`);

--
-- Structure de la table `AnnonceMoniteur`
--

CREATE TABLE IF NOT EXISTS `AnnonceMoniteur` (
  `id` int(22) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `date_envoi` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `type_moniteur` text NOT NULL,
  `statut_xml` varchar(4) NOT NULL default '0',
  `date_creation` date NOT NULL default '0000-00-00',
  `statut_web` enum('0','1','2','3','4','5') NOT NULL default '0',
  `statut_arg` enum('0','1','2','3','4','5') NOT NULL default '0',
  `statut_usn` enum('0','1','2','3','4','5') NOT NULL default '0',
  `statut_01i` enum('0','1','2','3','4','5') NOT NULL default '0',
  `datepub` varchar(10) default NULL,
  `num_annonce` int(11) NOT NULL default '0',
  `message_error` text NOT NULL,
  `timestamp` blob NOT NULL,
  `nom_timestamp` varchar(100) NOT NULL default '',
  `xml_moniteur` text NOT NULL,
  `id_annonce_boamp` int(22) NOT NULL default '0',
  `date_maj` varchar(20) default '0000-00-00 00:00:00',
  `id_form_xml` int(11) NOT NULL,
  `accuse_reception` char(1) NOT NULL,
  `id_destination_form_xml` int(11) NOT NULL,
  `statut_destinataire` varchar(10) default NULL,
  `commentaire` text,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `id_form_xml` (`id_form_xml`),
  KEY `id_destination_form_xml` (`id_destination_form_xml`)
) ENGINE=InnoDB;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `AnnonceMoniteur`
--
ALTER TABLE `AnnonceMoniteur` ADD CONSTRAINT `AnnonceMoniteur_ibfk_1` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`);
ALTER TABLE `AnnonceMoniteur` ADD CONSTRAINT `AnnonceMoniteur_ibfk_2` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`); 

--
-- Structure de la table `renseignements_boamp`
--
 
CREATE TABLE IF NOT EXISTS `renseignements_boamp` (
  `id` int(50) NOT NULL auto_increment,
  `acronymeOrg` varchar(30) NOT NULL,
  `id_type` int(50) NOT NULL,
  `id_compte` int(50) NOT NULL,
  `correspondant` varchar(200) default NULL,
  `organisme` varchar(200) default NULL,
  `adresse` varchar(200) default NULL,
  `cp` varchar(200) default NULL,
  `ville` varchar(200) default NULL,
  `pays` varchar(200) default NULL,
  `telephone` varchar(200) default NULL,
  `poste` varchar(200) default NULL,
  `fax` varchar(200) default NULL,
  `mail` varchar(200) default NULL,
  `url` text,
  `organe_charge_procedure` text,
  PRIMARY KEY  (`id`,`acronymeOrg`),
  KEY `id_type` (`id_type`,`id_compte`)
) ENGINE=InnoDB;

--
-- Structure de la table `nature_acte_juridique`
--

CREATE TABLE IF NOT EXISTS `nature_acte_juridique` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(50) default NULL,
  `libelle_fr` varchar(50) default NULL,
  `libelle_en` varchar(50) default NULL,
  `libelle_es` varchar(50) default NULL,
  `libelle_su` varchar(50) default NULL,
  `libelle_du` varchar(50) default NULL,
  `libelle_cz` varchar(50) default NULL,
  `libelle_ar` varchar(50) default NULL,
  `libelle_it` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `mode_execution_contrat`
--

CREATE TABLE IF NOT EXISTS `mode_execution_contrat` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL,
  `libelle_fr` varchar(255) default NULL,
  `libelle_en` varchar(255) default NULL,
  `libelle_es` varchar(255) default NULL,
  `libelle_su` varchar(255) default NULL,
  `libelle_du` varchar(255) default NULL,
  `libelle_cz` varchar(255) default NULL,
  `libelle_ar` varchar(255) default NULL,
  `libelle_it` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `ccag_applicable`
--

CREATE TABLE IF NOT EXISTS `ccag_applicable` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `Fcsp_Lieu`
--

CREATE TABLE IF NOT EXISTS `Fcsp_Lieu` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `Fcsp_unite`
--

CREATE TABLE IF NOT EXISTS `Fcsp_unite` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `Fcsp_Mandataire`
--

CREATE TABLE IF NOT EXISTS `Fcsp_Mandataire` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL
) ENGINE=InnoDB;

--
-- Structure de la table `DecisionPassationConsultation`
--

CREATE TABLE IF NOT EXISTS `DecisionPassationConsultation` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) default NULL,
  `libelle_fr` varchar(100) default NULL,
  `libelle_en` varchar(100) default NULL,
  `libelle_es` varchar(100) default NULL,
  `libelle_su` varchar(100) default NULL,
  `libelle_du` varchar(100) default NULL,
  `libelle_cz` varchar(100) default NULL,
  `libelle_ar` varchar(100) default NULL,
  `libelle_it` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `DecisionPassationMarcheAVenir`
--

CREATE TABLE IF NOT EXISTS `DecisionPassationMarcheAVenir` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) default NULL,
  `libelle_fr` varchar(100) default NULL,
  `libelle_en` varchar(100) default NULL,
  `libelle_es` varchar(100) default NULL,
  `libelle_su` varchar(100) default NULL,
  `libelle_du` varchar(100) default NULL,
  `libelle_cz` varchar(100) default NULL,
  `libelle_ar` varchar(100) default NULL,
  `libelle_it` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `AvisCao`
--

CREATE TABLE IF NOT EXISTS `AvisCao` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) default NULL,
  `libelle_fr` varchar(100) default NULL,
  `libelle_en` varchar(100) default NULL,
  `libelle_es` varchar(100) default NULL,
  `libelle_su` varchar(100) default NULL,
  `libelle_du` varchar(100) default NULL,
  `libelle_cz` varchar(100) default NULL,
  `libelle_ar` varchar(100) default NULL,
  `libelle_it` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--
-- Structure de la table `Contrat`
--

CREATE TABLE IF NOT EXISTS `Contrat` (
  `id_contrat` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_marche` int(11) NOT NULL default '0',
  `id_decision` int(11) NOT NULL default '0',
  `informaions_complementaires` text,
  PRIMARY KEY  (`id_contrat`,`organisme`),
  KEY `id_marche` (`id_marche`),
  KEY `id_decision` (`id_decision`)
) ENGINE=InnoDB;

--
-- Structure de la table `Passation_consultation`
--

CREATE TABLE IF NOT EXISTS `Passation_consultation` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `reference` int(11) NOT NULL default '0',
  `unite` int(5) default '0',
  `mandataire` int(5) default '0',
  `lieu_detaille_predefini` int(5) default '0',
  `lieux_detailles_non_definis` varchar(100) default NULL,
  `code_postal_lieu_principal_execution` varchar(20) default NULL,
  `Ville` varchar(50) default NULL,
  `montant_estime_consultation` varchar(20) default NULL,
  `commentaires` text,
  `numero_deliberation_financiere` varchar(20) default NULL,
  `date_deliberation_financiere` varchar(10) default NULL,
  `imputation_budgetaire` text,
  `numero_deliberation_autorisant_signature_marche` varchar(20) default NULL,
  `date_deliberation_autorisant_signature_marche` varchar(10) default NULL,
  `date_notification_previsionnelle` varchar(10) default NULL,
  `date_reception_projet_DCE_Service_Validateur` varchar(10) default NULL,
  `date_formulations_premieres_observations` varchar(10) default NULL,
  `date_retour_projet_DCE_finalise` varchar(10) default NULL,
  `date_validation_projet_DCE_par_service_validateur` varchar(10) default NULL,
  `date_validation_projet_DCE_vue_par` int(5) default '0',
  `date_reception_projet_AAPC_par_Service_Validateur` varchar(10) default NULL,
  `date_formulations_premieres_observations_AAPC` varchar(10) default NULL,
  `date_retour_projet_AAPC_finalise` varchar(10) default NULL,
  `date_validation_projet_AAPC_par_Service_Validateur` varchar(10) default NULL,
  `date_validation_projet_AAPC_par_Service_Validateur_vu_par` int(5) default '0',
  `date_envoi_publicite` varchar(10) default NULL,
  `date_envoi_invitations_remettre_offre` varchar(10) default NULL,
  `date_limite_remise_offres` varchar(10) default NULL,
  `delai_validite_offres` varchar(5) default NULL,
  `commentaires_phase_consultation` text,
  `date_reunion_ouverture_candidatures` varchar(10) default NULL,
  `date_reunion_ouverture_offres` varchar(10) default NULL,
  `decision` int(5) default '0',
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB;

--
-- Structure de la table `passation_marche_a_venir`
--

CREATE TABLE IF NOT EXISTS `passation_marche_a_venir` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_passation_consultation` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `montant_estime` varchar(11) NOT NULL default '0',
  `id_nature_acte_juridique` int(11) NOT NULL default '0',
  `forme_groupement` int(11) NOT NULL default '0',
  `variante_autorisee` enum('0','1') NOT NULL default '0',
  `variante_technique_obligatoire` enum('0','1') NOT NULL default '0',
  `nombre_variante_technique` int(11) NOT NULL default '0',
  `description_variante_technique` text NOT NULL,
  `mode_execution_contrat` int(11) NOT NULL default '0',
  `type_bon_commande` int(11) NOT NULL default '0',
  `min_bon_commande` varchar(11) NOT NULL default '0',
  `max_bon_commande` varchar(11) NOT NULL default '0',
  `duree_execution_marche_hors_reconduction` int(11) NOT NULL default '0',
  `nombre_reconduction` int(11) NOT NULL default '0',
  `duree_total_marche` int(11) NOT NULL default '0',
  `ccag_applicable` int(11) NOT NULL default '0',
  `marche_transversal` enum('0','1') NOT NULL default '0',
  `date_reception_analyse_offre` varchar(10) default NULL,
  `date_formulation_observation_projet_rapport` varchar(10) default NULL,
  `date_retour_projet_rapport_finalise` varchar(10) default NULL,
  `date_validation_projet_rapport` varchar(10) default NULL,
  `projet_rapport_vu_par` int(11) default NULL,
  `date_reunion_attribution` varchar(10) default NULL,
  `decision` int(11) default NULL,
  `date_envoi_courrier_condidat_non_retenu` varchar(10) default NULL,
  `date_signature_marche_pa` varchar(10) default NULL,
  `date_reception_controle_legalite` varchar(10) default NULL,
  `date_formulation_observation_dossier` varchar(10) default NULL,
  `date_retour_dossier_finalise` varchar(10) default NULL,
  `date_transmission_prefecture` varchar(10) default NULL,
  `dossier_vu_par` int(11) default NULL,
  `date_validation_rapport_information` varchar(10) default NULL,
  `commentaire` text,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `id_passation_consultation` (`id_passation_consultation`)
) ENGINE=InnoDB;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `passation_marche_a_venir`
--
ALTER TABLE `passation_marche_a_venir` ADD CONSTRAINT `passation_marche_a_venir_fk` FOREIGN KEY (`id_passation_consultation`) REFERENCES `Passation_consultation` (`id`);

--
-- Structure de la table `Avenant`
--

CREATE TABLE IF NOT EXISTS `Avenant` (
  `id_avenant` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_contrat` int(11) NOT NULL,
  `numero_avenant` varchar(20) default NULL,
  `type_avenant` int(5) default NULL,
  `objet_avenant` varchar(100) default NULL,
  `montant_avenant_ht` varchar(30) default NULL,
  `montant_avenant_ttc` varchar(30) default NULL,
  `pourcentage_augmentation_marche_initial` char(5) default NULL,
  `pourcentage_augmentation_cumule` char(5) default NULL,
  `montant_total_marche_tout_avenant_cumule` varchar(30) default NULL,
  `date_reception_projet_par_secretaire_cao` varchar(10) default NULL,
  `date_reception_projet_par_charge_etude` varchar(10) default NULL,
  `date_observation_par_sv` varchar(10) default NULL,
  `date_retour_projet` varchar(10) default NULL,
  `date_validation_projet` varchar(10) default NULL,
  `date_validation_projet_vu_par` int(5) default NULL,
  `date_cao` varchar(10) default NULL,
  `avis_cao` int(5) default NULL,
  `date_cp` varchar(10) default NULL,
  `date_signature_avenant` varchar(10) default NULL,
  `date_reception_dossier` varchar(10) default NULL,
  `date_formulation_observation_par_sv_sur_sdossier` varchar(10) default NULL,
  `date_retour_dossier_finalise` varchar(10) default NULL,
  `date_validation_dossier_finalise` varchar(10) NOT NULL,
  `date_transmission_prefecture` varchar(10) default NULL,
  `date_notification` varchar(10) default NULL,
  `operation_validation_vues_par` int(5) default NULL,
  `commentaires` text,
  PRIMARY KEY  (`id_avenant`,`organisme`),
  KEY `id_contrat` (`id_contrat`)
) ENGINE=InnoDB;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Avenant`
--
ALTER TABLE `Avenant` ADD CONSTRAINT `Avenant_ibfk_1` FOREIGN KEY (`id_contrat`) REFERENCES `Contrat` (`id_contrat`) ON DELETE CASCADE ON UPDATE CASCADE;

-- OKO le 08/06/2011 : Ajout de la table TypeAvenant
--
-- Structure de la table `TypeAvenant`
--

CREATE TABLE IF NOT EXISTS `TypeAvenant` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) default NULL,
  `libelle_fr` varchar(100) default NULL,
  `libelle_en` varchar(100) default NULL,
  `libelle_es` varchar(100) default NULL,
  `libelle_su` varchar(100) default NULL,
  `libelle_du` varchar(100) default NULL,
  `libelle_cz` varchar(100) default NULL,
  `libelle_ar` varchar(100) default NULL,
  `libelle_it` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

--FBO Migration Mesure Dematerialisation MPE4

CREATE TABLE IF NOT EXISTS `Mesure_Type_Procedure` (
  `id_tag_name` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `libelle_tag_name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id_tag_name`,`organisme`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `Mesure_avancement` (
  `id_mesure` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL,
  `id_service` int(11) NOT NULL,
  `annee` varchar(4) NOT NULL,
  `trimestre` char(2) NOT NULL,
  `siren` varchar(9) NOT NULL,
  `nic` varchar(5) NOT NULL,
  `identifiant_service` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `type_pouvoir_adjudicateur` int(11) NOT NULL,
  `departement` int(11) NOT NULL,
  `date_envoi` varchar(20) default NULL,
  `date_accuse` varchar(20) default NULL,
  `etat` varchar(5) default 'BR',
  `date_creation` varchar(20) NOT NULL,
  `xml` longtext,
  `name_xml_genere` varchar(225) default NULL,
  PRIMARY KEY  (`id_mesure`,`organisme`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

---HHA Migration MPE4

CREATE TABLE IF NOT EXISTS `ValeurReferentielOrg` (
  `id` int(10) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `id_referentiel` int(10) NOT NULL default '0',
  `libelle_valeur_referentiel` text NOT NULL,
  `libelle_valeur_referentiel_fr` text NOT NULL,
  `libelle_valeur_referentiel_en` text NOT NULL,
  `libelle_valeur_referentiel_es` text NOT NULL,
  `libelle_valeur_referentiel_su` text NOT NULL,
  `libelle_valeur_referentiel_du` text NOT NULL,
  `libelle_valeur_referentiel_cz` text NOT NULL,
  `libelle_valeur_referentiel_ar` text NOT NULL,
  `libelle_valeur_referentiel_it` text NOT NULL,
  PRIMARY KEY  (`id`,`organisme`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `ReferentielOrg` (
  `id_referentiel` int(10) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `libelle_referentiel` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`id_referentiel`,`organisme`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;


---AME migration DocumentExterne


CREATE TABLE IF NOT EXISTS `DocumentExterne` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `refConsultation` int(11) default NULL,
  `organisme` varchar(30) NOT NULL default '',
  `idBlob` int(11) default NULL,
  `idEntreprise` int(11) default NULL,
  `type` enum('1','2','3','4','5','6','7') default NULL,
  `nom` varchar(255) NOT NULL default '',
  `description` longtext NOT NULL,
  `date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `idBlob` (`idBlob`),
  KEY `refConsultation` (`refConsultation`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

--
-- Contraintes pour la table `DocumentExterne`
--
ALTER TABLE `DocumentExterne`  ADD CONSTRAINT `DOC_EX_consultation` FOREIGN KEY (`organisme`, `refConsultation`) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `DocumentExterne`  ADD CONSTRAINT `DOC_EX_Blob` FOREIGN KEY (`idBlob`) REFERENCES `blobOrganisme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS `Helios_cosignature` (
  `id_fichier` int(20) NOT NULL,
  `organisme` varchar(30) NOT NULL default '',
  `signature_acte` varchar(20) NOT NULL,
  `horodatage_acte` longblob NOT NULL,
  `untrusteddate_acte` varchar(20) NOT NULL,
  `taille_acte` varchar(20) NOT NULL,
  `id_Blob` varchar(20) NOT NULL,
  `idEnveloppe` int(20) NOT NULL,
  PRIMARY KEY  (`id_fichier`,`organisme`),
  KEY `id_fichier` (`id_fichier`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `Helios_piece_publicite` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `nom_fichier` varchar(100) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '1',
  `consultation_ref` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


ALTER TABLE `Helios_piece_publicite`  ADD CONSTRAINT `Helios_piece_publicite_Consultation` FOREIGN KEY (`organisme`,`consultation_ref`) REFERENCES `consultation` (`organisme`,`reference`) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS `Helios_rapport_prefet` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '0',
  `description` text, 
  PRIMARY KEY  (`id`, `organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `Helios_rapport_prefet`  ADD CONSTRAINT `Helios_rapport_prefet_Consultation` FOREIGN KEY (`organisme`,`consultation_ref`) REFERENCES `consultation` (`organisme`,`reference`) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS `Helios_tableau_ar` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `description` longtext,
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '0',
  PRIMARY KEY  (`id`, `organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `Helios_tableau_ar`  ADD CONSTRAINT `Helios_tableau_ar_Consultation` FOREIGN KEY (`organisme`,`consultation_ref`) REFERENCES `consultation` (`organisme`,`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `Helios_teletransmission` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `shown_id` varchar(20) NOT NULL default '',
  `id_agent_creation` int(11) NOT NULL default '0',
  `consultation_ref` int(11) default NULL,
  `publicite_cons` char(20) default NULL,
  `dce_items` varchar(255) default NULL,
  `ids_pvs_cao` varchar(255) default NULL,
  `ids_rapports_prefet` varchar(255) default NULL,
  `deliberation_mapa` char(1) default NULL,
  `tableau_ar` int(11) default NULL,
  `date_creation` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `date_envoi` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `id_agent_envoi` int(11) NOT NULL default '0',
  `nom_piecej1` varchar(250) NOT NULL default '',
  `piecej1` int(11) NOT NULL default '0',
  `horodatage_piecej1` longblob NOT NULL,
  `untrusteddate_piecej1` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille_piecej1` varchar(20) NOT NULL default '',
  `nom_piecej2` varchar(250) NOT NULL default '',
  `piecej2` int(11) NOT NULL default '0',
  `horodatage_piecej2` longblob NOT NULL,
  `untrusteddate_piecej2` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille_piecej2` varchar(20) NOT NULL default '',
  `piecej1_desc` longtext,
  `piecej2_desc` longtext,
  `fichier_xml` longblob NOT NULL,
  `nom_dossierzip` varchar(250) NOT NULL default '',
  `taille_dossierzip` varchar(250) NOT NULL default '',
  `objet_libre` varchar(250) NOT NULL default '',
  `commentaire_libre` varchar(250) NOT NULL default '',
  `sig_piece_principale` longblob NOT NULL,
  `ref_utilisateur` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`id`, `organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=2 ;

ALTER TABLE `Helios_teletransmission`  ADD CONSTRAINT `Helios_teletransmission_Consultation` FOREIGN KEY (`organisme`,`consultation_ref`) REFERENCES `consultation` (`organisme`,`reference`) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS `Helios_teletransmission_lot` (
  `id_teletransmission` int(11) NOT NULL default '0',
  `organisme` varchar(30) NOT NULL default '',
  `lot` int(11) NOT NULL default '0',
  `numero_marche` varchar(250) NOT NULL default '',
  `ids_env_actes_engagements` varchar(255) NOT NULL default '',
  `ids_enveloppes_items` varchar(255) NOT NULL default '',
  `ids_docs_acceptes` varchar(255) NOT NULL default '',
  `ids_msg_notif` varchar(255) NOT NULL default '',
  `lot_in_transmission` char(1) NOT NULL default '',
  `statut_envoi` int(11) NOT NULL default '1',
  `id_zip_send` varchar(40) default NULL,
  `nom_lettre_commande` varchar(100) default NULL,
  `lettre_commande` int(20) default NULL,
  `horodatage` varchar(20) NOT NULL default '',
  `untrusteddate` varchar(20) NOT NULL default '',
  `taille` varchar(20) NOT NULL default '',
  `valider_comptable` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id_teletransmission`,`organisme`,`lot`)
) ENGINE=InnoDB ;

ALTER TABLE `Helios_teletransmission_lot`  ADD CONSTRAINT `Helios_teletransmission_lot_Helios_teletransmission` FOREIGN KEY (`id_teletransmission`,`organisme`) REFERENCES `Helios_teletransmission` (`id`,`organisme`) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS `Helios_pv_consultation` (
  `id` int(11) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '0',
  `description` text,
  PRIMARY KEY  (`id`,`organisme`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;

ALTER TABLE `Helios_pv_consultation`  ADD CONSTRAINT `Helios_pv_consultation_Consultation` FOREIGN KEY (`organisme`,`consultation_ref`) REFERENCES `consultation` (`organisme`,`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Migration mpe4 Chorus
CREATE TABLE IF NOT EXISTS `Chorus_acte_juridique` (
  `id` int(10) NOT NULL default '0',
  `libelle` varchar(200) NOT NULL default '',
  `code` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;


CREATE TABLE IF NOT EXISTS `Chorus_echange` (
  `id` int(20) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `id_decision` int(20) default NULL,
  `num_ordre` varchar(20) default NULL,
  `nom_createur` varchar(200) default NULL,
  `prenom_createur` varchar(200) default NULL,
  `id_createur` int(20) default NULL,
  `statutEchange` varchar(20) default NULL,
  `date_creation` varchar(20) default NULL,
  `date_envoi` varchar(20) default NULL,
  `retour_chorus` varchar(20) default NULL,
  `id_ej_appli_ext` varchar(20) NOT NULL default '',
  `id_oa` int(20) default NULL,
  `id_ga` int(20) default NULL,
  `id_type_marche` int(20) default NULL,
  `dce_items` varchar(200) default NULL,
  `ids_env_ae` varchar(200) default NULL,
  `ids_env_items` varchar(200) default NULL,
  `ids_pieces_externes` varchar(200) default NULL,
  `id_agent_envoi` int(200) default NULL,
  `nom_agent` varchar(200) default NULL,
  `prenom_agent` varchar(200) default NULL,
  `signACE` enum('0','1') NOT NULL default '0',
  `siren` varchar(9) default NULL,
  `siret` varchar(5) default NULL,
  `date_notification` varchar(20) default NULL,
  `date_fin_marche` varchar(20) default NULL,
  `id_acte_juridique` char(1) default NULL,
  `cpv_1` varchar(20) default NULL,
  `cpv_2` text,
  `cpv_3` varchar(20) default NULL,
  `cpv_4` varchar(20) default NULL,
  `id_type_procedure` int(20) default NULL,
  `id_forme_prix` char(1) default NULL,
  `Nbr_entreprises_cotraitantes` varchar(20) default NULL,
  `sous_traitance_declaree` char(1) default NULL,
  `carte_achat` char(1) default NULL,
  `clause_sociale` char(1) default NULL,
  `clause_environnementale` char(1) default NULL,
  `Nbr_proposition_recues` varchar(20) default NULL,
  `Nbr_proposition_dematerialisees` varchar(20) default NULL,
  `nom_fichier` varchar(200) default NULL,
  `erreur_rejet` text,
  `code_cpv_libelle1` varchar(250) default NULL,
  `code_cpv_libelle2` varchar(250) default NULL,
  `code_cpv_libelle3` varchar(250) default NULL,
  `code_cpv_libelle4` varchar(250) default NULL,
  `pieces_notif_items` varchar(200) default NULL COMMENT 'Permet de stocker les items des pieces de notification',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `id_decision` (`id_decision`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `Chorus_echange`  ADD CONSTRAINT `Chorus_echange_decisionEnveloppe` FOREIGN KEY (`id_decision`,`organisme`) REFERENCES `decisionEnveloppe` (`id_decision_enveloppe`,`organisme`) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS `Chorus_forme_prix` (
  `id` int(20) NOT NULL default '0',
  `libelle` varchar(200) NOT NULL default '',
  `code` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;


CREATE TABLE IF NOT EXISTS `Chorus_groupement_achat` (
  `id` int(20) NOT NULL auto_increment,
  `id_oa` int(20) NOT NULL default '0',
  `libelle` varchar(250) NOT NULL default '',
  `code` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id_oa` (`id_oa`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `Chorus_organisation_achat` (
  `id` int(20) NOT NULL auto_increment,
  `libelle` varchar(250) NOT NULL default '',
  `code` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `Chorus_pj` (
  `id` int(20) NOT NULL auto_increment,
  `organisme` varchar(30) NOT NULL default '',
  `id_echange` int(20) NOT NULL default '0',
  `nom_fichier` varchar(300) NOT NULL default '',
  `fichier` varchar(20) NOT NULL default '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '',
  `taille` int(20) NOT NULL default '0',
  PRIMARY KEY  (`id`,`organisme`),
  KEY `id_echange` (`id_echange`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


ALTER TABLE `Chorus_pj`  ADD CONSTRAINT `Chorus_pj_Chorus_echange` FOREIGN KEY (`id_echange`,`organisme`) REFERENCES `Chorus_echange` (`id`,`organisme`);


CREATE TABLE IF NOT EXISTS `Chorus_type_marche` (
  `id` int(20) NOT NULL auto_increment,
  `libelle` varchar(250) NOT NULL default '',
  `code` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `Chorus_type_procedure` (
  `id` int(50) NOT NULL auto_increment,
  `libelle` varchar(200) NOT NULL default '',
  `code` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


ALTER TABLE `Offres` ADD CONSTRAINT `Offres_consultation` FOREIGN KEY ( `organisme`, `consultation_ref` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `Retrait_Papier` ADD CONSTRAINT `Retrait_Papier_consultation` FOREIGN KEY ( `organisme`, `consultation_ref` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `Offre_papier` ADD CONSTRAINT `Offre_papier_consultation` FOREIGN KEY ( `organisme`, `consultation_ref` ) REFERENCES `consultation` (`organisme`, `reference`) ON DELETE CASCADE ON UPDATE CASCADE ;

--- Migration MPE4

ALTER TABLE `Commission` DROP PRIMARY KEY , ADD PRIMARY KEY ( `id` , `organisme` ) ;

ALTER TABLE `Intervenant_Externe` DROP PRIMARY KEY , ADD PRIMARY KEY ( `id` , `organisme` );

ALTER TABLE `Intervenant_Externe_Commission` DROP PRIMARY KEY ,ADD PRIMARY KEY ( `id_commission` , `id_intervenant_externe` , `organisme` );

ALTER TABLE `Intervenant_Ordre_Du_Jour` DROP PRIMARY KEY , ADD PRIMARY KEY ( `id` , `organisme` ) ;

ALTER TABLE `Ordre_Du_Jour` DROP PRIMARY KEY , ADD PRIMARY KEY ( `id` , `organisme` );   

-- Migration OKO
ALTER TABLE `Fcsp_unite` DROP INDEX `id_3`;
ALTER TABLE `Fcsp_unite` DROP INDEX `id_2`;
ALTER TABLE `Fcsp_unite` ADD `organisme` VARCHAR( 5 ) NOT NULL AFTER `id` ;
ALTER TABLE `Fcsp_unite` DROP PRIMARY KEY ;
ALTER TABLE `Fcsp_unite` ADD PRIMARY KEY ( `id` , `organisme` ) ;
ALTER TABLE `Fcsp_Mandataire` DROP INDEX `id_3`;
ALTER TABLE `Fcsp_Mandataire` DROP INDEX `id_2`;
ALTER TABLE `Fcsp_Mandataire` ADD `organisme` VARCHAR( 5 ) NOT NULL AFTER `id` ;
ALTER TABLE `Fcsp_Mandataire` DROP PRIMARY KEY ;
ALTER TABLE `Fcsp_Mandataire` ADD PRIMARY KEY ( `id` , `organisme` ) ;
ALTER TABLE `Fcsp_Mandataire` DROP INDEX `id`; 
ALTER TABLE `ccag_applicable` ADD `organisme` VARCHAR( 5 ) NOT NULL AFTER `id` ;
ALTER TABLE `ccag_applicable` DROP PRIMARY KEY;
ALTER TABLE `ccag_applicable` ADD PRIMARY KEY(`id`,`organisme`);
ALTER TABLE `Fcsp_Lieu` ADD `organisme` VARCHAR( 5 ) NOT NULL AFTER `id` ;
ALTER TABLE `Fcsp_Lieu` DROP PRIMARY KEY;
ALTER TABLE `Fcsp_Lieu` ADD PRIMARY KEY(`id`,`organisme`);
ALTER TABLE `Fcsp_Lieu` DROP INDEX `id`;
ALTER TABLE `Fcsp_Lieu` DROP INDEX `id_3`;
ALTER TABLE `Fcsp_Lieu` DROP INDEX `id_2`;



ALTER TABLE `Fcsp_unite` DROP PRIMARY KEY;
ALTER TABLE `Fcsp_Mandataire` DROP PRIMARY KEY;
ALTER TABLE `Fcsp_Lieu` DROP PRIMARY KEY;
ALTER TABLE `ccag_applicable` DROP PRIMARY KEY;

