ALTER TABLE `t_donnee_complementaire` ADD COLUMN `id_ccag_dpi` INT( 11 ) NULL DEFAULT NULL;

ALTER TABLE `HabilitationAgent` ADD COLUMN `gerer_newsletter_redac` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'permet d''acceder à la page "Message aux redacteurs"';
ALTER TABLE `HabilitationProfil` ADD COLUMN `gerer_newsletter_redac` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'permet d''acceder à la page "Message aux redacteurs"';
ALTER TABLE `Newsletter` ADD COLUMN `is_redac` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `type_destinataire`;

-- LEZ MPE-991
CREATE TABLE IF NOT EXISTS `t_chorus_fiche_modificative` (
  `id_fiche_modificative` int(11) NOT NULL AUTO_INCREMENT,
  `id_echange` int(11) NOT NULL,
  `organisme` varchar(30) NOT NULL DEFAULT '',
  `type_modification` int(11) DEFAULT NULL,
  `date_prevue_notification` date DEFAULT NULL,
  `date_fin_marche` date DEFAULT NULL,
  `date_fin_marche_modifie` date DEFAULT NULL,
  `montant_marche` double DEFAULT NULL,
  `montant_acte` double DEFAULT NULL,
  `taux_tva` varchar(10) DEFAULT NULL,
  `nombre_fournisseur_cotraitant` varchar(250) DEFAULT NULL,
  `localites_fournisseurs` varchar(250) DEFAULT NULL,
  `siren_fournisseur` text,
  `siret_fournisseur` text,
  `nom_fournisseur` text,
  `type_fournisseur` text,
  `visa_accf` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Non renseigné,1=>oui,2=>non',
  `visa_prefet` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Non renseigné,1=>oui,2=>non',
  `remarque` text,
  `id_blob_piece_justificatives` text,
  `id_blob_fiche_modificative` text,
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_fiche_modificative`),
  KEY `chorus_echange_chorus_fiche_modificative_fk` (`id_echange`,`organisme`),
  CONSTRAINT `chorus_echange_chorus_fiche_modificative_fk` FOREIGN KEY (`id_echange`, `organisme`) REFERENCES `Chorus_echange` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `t_chorus_fiche_modificative_pj` (
  `id_fiche_modificative_pj` int(20) NOT NULL AUTO_INCREMENT,
  `id_fiche_modificative` int(11) NOT NULL DEFAULT '0',
  `nom_fichier` varchar(300) NOT NULL DEFAULT '',
  `fichier` varchar(20) NOT NULL DEFAULT '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL DEFAULT '',
  `taille` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_fiche_modificative_pj`),
  KEY `chorus_fiche_modificative_pj_chorus_fiche_modificative_fk` (`id_fiche_modificative`),
  CONSTRAINT `chorus_fiche_modificative_pj_chorus_fiche_modificative_fk` FOREIGN KEY (`id_fiche_modificative`) REFERENCES `t_chorus_fiche_modificative` (`id_fiche_modificative`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;
-- MPE-1026 : Gestion de la nouvelle habilitation RMA

ALTER TABLE `HabilitationAgent` ADD COLUMN `profil_rma` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'rma: Responsable ministeriel d''achat' ;
ALTER TABLE `HabilitationProfil` ADD COLUMN `profil_rma` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'rma: Responsable ministeriel d''achat' ;

ALTER TABLE `HabilitationAgent` ADD COLUMN `affectation_vision_rma` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'rma: Responsable ministeriel d''achat' ;
ALTER TABLE `HabilitationProfil` ADD COLUMN `affectation_vision_rma` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'rma: Responsable ministeriel d''achat' ;

ALTER TABLE `configuration_organisme` ADD COLUMN `profil_rma` ENUM( '0', '1' ) NOT NULL DEFAULT '0' ;


--
-- Structure de la table `t_vison_rma_agent_organisme`
--

CREATE TABLE IF NOT EXISTS `t_vision_rma_agent_organisme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_agent` int(11) NOT NULL,
  `acronyme` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_vision_rma_agent_organisme` ADD CONSTRAINT `t_vision_rma_agent_organisme_ID_AGENT_FK` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`);
ALTER TABLE `t_vision_rma_agent_organisme` ADD CONSTRAINT `t_vision_rma_agent_organisme_ACRONYME_FK` FOREIGN KEY (`acronyme`) REFERENCES `Organisme` (`acronyme`);
