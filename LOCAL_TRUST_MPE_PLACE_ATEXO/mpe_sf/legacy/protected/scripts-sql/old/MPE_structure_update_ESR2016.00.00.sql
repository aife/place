-- Structure de la table `t_consultation_compte_pub`
CREATE TABLE IF NOT EXISTS `t_consultation_compte_pub` (
`id` int(22) NOT NULL AUTO_INCREMENT,
`id_donnees_complementaires` int(22) NOT NULL,
`organisme` varchar(30) NOT NULL,
`id_compte_pub` int(22) NOT NULL,
`boamp_login` varchar(100) NOT NULL DEFAULT '',
`boamp_password` varchar(100) NOT NULL DEFAULT '',
`boamp_mail` varchar(100) NOT NULL DEFAULT '',
`boamp_target` char(1) NOT NULL DEFAULT '0',
`denomination` varchar(100) NOT NULL DEFAULT '',
`prm` varchar(100) NOT NULL DEFAULT '',
`adresse` varchar(100) NOT NULL DEFAULT '',
`cp` varchar(5) NOT NULL DEFAULT '',
`ville` varchar(100) NOT NULL DEFAULT '',
`url` varchar(100) NOT NULL DEFAULT '',
`facture_denomination` varchar(100) DEFAULT NULL,
`facture_adresse` varchar(255) NOT NULL DEFAULT '',
`facture_cp` varchar(10) NOT NULL DEFAULT '',
`facture_ville` varchar(100) NOT NULL DEFAULT '',
`instance_recours_organisme` varchar(200) DEFAULT NULL,
`instance_recours_adresse` varchar(200) DEFAULT NULL,
`instance_recours_cp` varchar(200) DEFAULT NULL,
`instance_recours_ville` varchar(200) DEFAULT NULL,
`instance_recours_url` text,
PRIMARY KEY (`id`),
KEY `id_donnees_complementaires` (`id_donnees_complementaires`),
KEY `id_compte_pub` (`id_compte_pub`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- Contraintes pour la table `t_consultation_compte_pub`
ALTER TABLE `t_consultation_compte_pub` ADD CONSTRAINT `t_consultation_compte_pub_ref_idDonneesComplementaires` FOREIGN KEY (`id_donnees_complementaires`) REFERENCES `t_donnee_complementaire` (`id_donnee_complementaire`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AME les ECHOS
ALTER TABLE `t_support_publication`	ADD COLUMN `actif` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0 : inactif, 1 : actif';

CREATE TABLE `t_Offre_Support_Publicite` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`libelle_offre` VARCHAR(255) NULL,
	`id_support` INT NOT NULL,
	`actif` ENUM('0','1') NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_t_support_publication` FOREIGN KEY (`id_support`) REFERENCES `t_support_publication` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0;


ALTER TABLE `t_Offre_Support_Publicite`	ADD COLUMN `logo` VARCHAR(255) NULL ;
ALTER TABLE `t_support_publication`	ADD COLUMN `url` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `t_support_annonce_consultation` ADD COLUMN `id_offre` INT NULL DEFAULT NULL;

ALTER TABLE `t_annonce_consultation`	ADD COLUMN `publicite_simplifie` ENUM('0','1') NOT NULL DEFAULT '0';
DROP TABLE `t_relation_consultation_anoonce_pub`;

-- Modification dela table "consultation"
ALTER TABLE `consultation` ADD COLUMN `donnee_publicite_obligatoire` ENUM('0','1') NULL DEFAULT NULL COMMENT 'permet de sauvegarder la valeur de la case à cocher "Je souhaite publier un avis depuis cette plate-forme pour cette consultation"' AFTER `id_contrat`;

-- Modification de la table "t_donnee_complementaire"
ALTER TABLE `t_donnee_complementaire`	ADD COLUMN `procedure_accord_marches_publics_omc` TINYINT(1) NULL DEFAULT NULL;
ALTER TABLE `t_donnee_complementaire`	ADD COLUMN `cautionnement_regime_financier` LONGTEXT NULL DEFAULT NULL;
ALTER TABLE `t_donnee_complementaire`	ADD COLUMN `modalites_financement_regime_financier` LONGTEXT NULL DEFAULT NULL;


ALTER TABLE `t_annonce_consultation`	DROP FOREIGN KEY `fk_tAnnonceConsultation_reference_tDispositifAnnonce`;

ALTER TABLE `configuration_plateforme` DROP COLUMN `redac_echos`;
ALTER TABLE `t_donnee_complementaire`	DROP COLUMN `nature_marche`;
ALTER TABLE `t_donnee_complementaire`	DROP COLUMN `article_29`;
ALTER TABLE `t_donnee_complementaire`	DROP COLUMN `article_30`;
ALTER TABLE `t_donnee_complementaire`	DROP COLUMN `article_147`;
ALTER TABLE `t_donnee_complementaire`	DROP COLUMN `article_148`;
ALTER TABLE `t_donnee_complementaire`	DROP COLUMN `produits_associes`;

DROP TABLE `t_nature_marche`;
DROP TABLE `t_referentiel_produit_associe`;


ALTER TABLE `t_Offre_Support_Publicite`	ADD COLUMN `mapa` INT NOT NULL DEFAULT '0' COMMENT '0: pas mapa, 1: mapa' AFTER `logo`;
ALTER TABLE `t_Offre_Support_Publicite`	ADD COLUMN `montant_inf` INT NOT NULL DEFAULT '0' AFTER `mapa`;
ALTER TABLE `t_Offre_Support_Publicite`	ADD COLUMN `montant_max` INT NOT NULL DEFAULT '0' AFTER `montant_inf`;
ALTER TABLE `t_Offre_Support_Publicite`	ADD COLUMN `code` VARCHAR(255) NOT NULL DEFAULT '' AFTER `montant_max`;

ALTER TABLE `t_Offre_Support_Publicite`	CHANGE COLUMN `mapa` `mapa` INT(11) NOT NULL DEFAULT '0' COMMENT '0: procedure formalise, 1: mapa, 2: mapa et procedure formalise' AFTER `logo`;
-- LEZ MPE-2152
ALTER TABLE `Entreprise`	ADD COLUMN `id_code_effectif` INT(11) NULL DEFAULT NULL COMMENT 'L''id de la valeur referenciel des tranches effectifs' AFTER `created_from_decision`;

-- LEZ MPE-260
ALTER TABLE `Enveloppe`	ADD COLUMN `enveloppe_fictive` ENUM('0','1') NULL DEFAULT '0' COMMENT 'la valeur 1 si l''enveloppe est fictive si non 0' AFTER `dateheure_ouverture_agent2`;

-- AME
ALTER TABLE `Enveloppe`	ADD COLUMN `integrite_fichier` INT(1) NOT NULL DEFAULT '0' AFTER `enveloppe_fictive`;

-- LEZ MPE-2679
ALTER TABLE `t_document_type` ADD COLUMN `code` VARCHAR(50) NULL COMMENT 'Permet de faire le lien entre le type de document et le retour du ws agrégateur' AFTER `nom_type_document`;
ALTER TABLE `t_document_type` ADD COLUMN `type_doc_entreprise_etablissement` ENUM('1','2') NULL COMMENT 'Précise si le document est de type entreprise ou etablissement (1: entreprise, 2: etablissement)' AFTER `code`;
ALTER TABLE `t_document_type` ADD COLUMN `uri` VARCHAR(100) NULL COMMENT 'URI de la requete ws' AFTER `type_doc_entreprise_etablissement`;
ALTER TABLE `t_document_type` ADD COLUMN `params_uri` VARCHAR(100) NOT NULL COMMENT 'Paramètres de la requete ws' COMMENT 'Les parametres doivent etre separes par des virgules' AFTER `uri`;
ALTER TABLE `t_document_type` ADD COLUMN `class_name` VARCHAR(100) NOT NULL COMMENT 'Le nom de la classe a utiliser lors de l''appel du web service' AFTER `params_uri`;

ALTER TABLE `t_document_entreprise`	ADD COLUMN `id_etablissement` INT(11) NOT NULL AFTER `id_entreprise`;

ALTER TABLE `t_derniers_appels_valides_ws_sgmap_documents` ADD COLUMN `identifiant` BIGINT NOT NULL COMMENT 'Siren (9 caractères) pour l''entreprise ou siret (14 caractères) pour l''etablissement' AFTER `id`;

ALTER TABLE `t_document_type` ADD COLUMN `nature_document` ENUM('1','2') NOT NULL DEFAULT '1' COMMENT 'Precise si le type de document est principal ou supplementaire (1=>principal, 2=>supplementaire)' AFTER `class_name`;



-- LEZ MPE-2561
CREATE TABLE IF NOT EXISTS `t_groupement_entreprise` (
	`id_groupement_entreprise` INT(11) NOT NULL AUTO_INCREMENT,
	`id_type_groupement` INT(11) NULL DEFAULT NULL,
	`id_offre` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id_groupement_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `t_type_groupement_entreprise` (
	`id_type_groupement` INT(11) NOT NULL AUTO_INCREMENT,
	`libelle_type_groupement` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id_type_groupement`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_groupement_entreprise`	ADD CONSTRAINT `fk_Groupement_Entreprise_Type_Groupement` FOREIGN KEY (`id_type_groupement`) REFERENCES `t_type_groupement_entreprise` (`id_type_groupement`);

CREATE TABLE IF NOT EXISTS `t_role_juridique` (
	`id_role_juridique` INT(11) NOT NULL AUTO_INCREMENT,
	`libelle_role_juridique` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id_role_juridique`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `t_membre_groupement_entreprise` (
	`id_membre_groupement_entreprise` INT(11) NOT NULL AUTO_INCREMENT,
	`id_role_juridique` INT(11) NULL DEFAULT NULL,
	`id_groupement_entreprise` INT(11) NULL DEFAULT NULL,
	`id_entreprise` INT(11) NULL DEFAULT NULL,
	`id_etablissement` INT(11) NULL DEFAULT NULL,
	`id_membre_parent` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id_membre_groupement_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_membre_groupement_entreprise`	ADD CONSTRAINT `fk_Membre_Groupement_Entreprise_Role_Juridique` FOREIGN KEY (`id_role_juridique`) REFERENCES `t_role_juridique` (`id_role_juridique`);
ALTER TABLE `configuration_plateforme`	ADD COLUMN `groupement` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de gerer le bloc groupement des entreprises' ;
ALTER TABLE `t_membre_groupement_entreprise`	ADD CONSTRAINT `fk_Membre_Groupement_Entreprise_Groupement` FOREIGN KEY (`id_groupement_entreprise`) REFERENCES `t_groupement_entreprise` (`id_groupement_entreprise`);
ALTER TABLE `t_membre_groupement_entreprise`	ADD CONSTRAINT `fk_Membre_Groupement_Entreprise_Entreprise` FOREIGN KEY (`id_entreprise`) REFERENCES `Entreprise` (`id`);
ALTER TABLE `t_membre_groupement_entreprise`	ADD CONSTRAINT `fk_Membre_Groupement_Entreprise_Etablissement` FOREIGN KEY (`id_etablissement`) REFERENCES `t_etablissement` (`id_etablissement`);
ALTER TABLE `t_membre_groupement_entreprise`	ADD CONSTRAINT `fk_Membre_Groupement_Entreprise_Perent` FOREIGN KEY (`id_membre_parent`) REFERENCES `t_membre_groupement_entreprise` (`id_membre_groupement_entreprise`);
ALTER TABLE `t_groupement_entreprise`	ADD CONSTRAINT `fk_Groupement_Entreprise_Offres` FOREIGN KEY (`id_offre`) REFERENCES `Offres` (`id`);
ALTER TABLE `t_groupement_entreprise`	ADD COLUMN `date_creation` DATETIME default NULL;

-- T_Access_Token
CREATE TABLE t_access_token (
  id CHAR(36) NOT NULL COMMENT '(DC2Type:uuid)',
  data LONGTEXT NOT NULL COMMENT '(DC2Type:json_array)',
  ip VARCHAR(15) NOT NULL,
  user_id INT NOT NULL,
  user_type INT NOT NULL,
  created_at DATETIME NOT NULL,
  INDEX idenfifiant_acces_token (id),
  PRIMARY KEY(id)
) ENGINE=InnoDB;

ALTER TABLE `t_document_type`	CHANGE COLUMN `id_type_document` `id_type_document` INT(11) NOT NULL AUTO_INCREMENT ;

-- LEZ MPE-2687
ALTER TABLE `Entreprise` ADD COLUMN `categorie_entreprise` VARCHAR(255) NULL DEFAULT NULL COMMENT 'La catégorie de l''entreprise exemple : PME' AFTER `id_code_effectif`;

-- LEZ MPE-2149
ALTER TABLE `Entreprise_info_exercice` ADD COLUMN `saisie_manuelle` ENUM('0','1') NULL DEFAULT '1' AFTER `capacite_endettement`;

-- Ame MPE-2787
ALTER TABLE `configuration_organisme`	ADD COLUMN `mode_applet` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet d''activer le passage au serveur crypto au lieu de l''applet' AFTER `affichage_nom_service_pere`;

-- OKO MPE-2781 : Dépot: Cas d'erreur en cas de non réponse du serveur crypto
ALTER TABLE `Offres` ADD COLUMN `etat_chiffrement` INT(1) NOT NULL DEFAULT '0' COMMENT 'Permet de stocker l''etat du chiffrement de l''offre. Valeurs possibles : 0 => pour les offres deja existantes avant cette evolution, 1 => etat_chiffrement_ok, 2 => etat_chiffrement_ko_error_rest, 3 => etat_chiffrement_ko_error_horodatage, 4 => etat_chiffrement_ko_error_json ' AFTER `candidature_id_externe`;
ALTER TABLE `Offres` ADD COLUMN `erreur_chiffrement` LONGTEXT NULL DEFAULT NULL COMMENT 'Permet de stocker le message d''erreur en cas de chiffrement KO' AFTER `etat_chiffrement`;

ALTER TABLE `Offres` ADD COLUMN `date_fin_chiffrement` DATETIME NULL AFTER `erreur_chiffrement`;

-- AME MPE-2777
ALTER TABLE `Offres` ADD COLUMN `date_horodatage` DATETIME NULL DEFAULT NULL COMMENT 'date extraite depuis l''horodatage (ne pas utiliser)' AFTER `date_fin_chiffrement`;
ALTER TABLE `Offres` ADD COLUMN `verification_hotodatage` ENUM('0','1','2') NOT NULL DEFAULT '0' COMMENT '(0 : pas encore vérifier, 1 : vérification OK, 2 : vérification KO)' AFTER `date_horodatage`;
ALTER TABLE `Enveloppe`	ADD COLUMN `verification_signature` VARCHAR(50) NOT NULL DEFAULT '' AFTER `integrite_fichier`;
ALTER TABLE `Offres`	ADD COLUMN `verification_signature` VARCHAR(50) NOT NULL DEFAULT '' AFTER `verification_hotodatage`;
ALTER TABLE `Offres` CHANGE COLUMN `verification_signature` `verification_signature_offre` VARCHAR(50) NOT NULL DEFAULT '' AFTER `verification_hotodatage`;

-- OKO
ALTER TABLE `Offres` ADD COLUMN `horodatage_hash_fichiers` VARCHAR(255) NULL DEFAULT '' COMMENT 'Permet de stocker le base64 xml de l ensemble des hash de l offre' AFTER `verification_signature_offre`;
ALTER TABLE `Offres` CHANGE COLUMN `horodatage_hash_fichiers` `horodatage_hash_fichiers` TEXT NULL DEFAULT '' COMMENT 'Permet de stocker le base64 xml de l ensemble des hash de l offre' AFTER `verification_signature_offre`;
ALTER TABLE `fichierEnveloppe` ADD COLUMN `signature_infos` TEXT NULL DEFAULT NULL COMMENT 'Permet de stocker les infos signatures renvoyées par le serveur crypto' AFTER `date_signature`;
ALTER TABLE `fichierEnveloppe` ADD COLUMN `signature_infos_date` DATETIME NULL DEFAULT NULL COMMENT 'Permet de stocker la date de sauvegarde des infos de la signature (champ ''signature_infos'')' AFTER `signature_infos`;
ALTER TABLE `Offres` DROP COLUMN `signature_infos`;
ALTER TABLE `Offres` DROP COLUMN `signature_infos_date`;
ALTER TABLE `fichierEnveloppe` ADD COLUMN `id_fichier_signature` INT(11) NULL DEFAULT NULL COMMENT 'Contient l''id du fichier signature du fichier courant' AFTER `signature_infos_date`;

-- LEZ MPE-2835
ALTER TABLE `configuration_plateforme`	ADD COLUMN `notifications_agent` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet  de gerer les notification agent' ;

CREATE TABLE IF NOT EXISTS `t_notification_agent` (
	`id_notification` INT(11) NOT NULL AUTO_INCREMENT,
	`id_agent` INT(11) DEFAULT  NULL,
	`id_type` INT(11) NOT NULL,
	`service_id` INT(11) default NULL,
	`organisme` VARCHAR(45) default NULL,
	`reference_objet` VARCHAR(255) default  NULL,
	`libelle_notification` VARCHAR(255) NULL,
	`description` TEXT NULL,
	`notification_lue` enum('0','1') NOT NULL DEFAULT '0',
	`notification_actif` enum('0','1') NOT NULL DEFAULT '1',
	`url_notification` VARCHAR(500) default NULL,
	`date_creation` datetime default NULL,
	`date_lecture` datetime default NULL,
	PRIMARY KEY (`id_notification`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `t_type_notification_agent` (
	`id_type` INT(11) NOT NULL AUTO_INCREMENT,
	`libelle` VARCHAR(255) NULL,
	PRIMARY KEY (`id_type`)
) ENGINE=InnoDB;

ALTER TABLE `t_notification_agent`	ADD CONSTRAINT `fk_Type_Notification_Agent` FOREIGN KEY (`id_type`) REFERENCES `t_type_notification_agent` (`id_type`) ON UPDATE CASCADE ON DELETE CASCADE;

-- LEZ-2871
CREATE TABLE jms_cron_jobs (id INT UNSIGNED AUTO_INCREMENT NOT NULL, command VARCHAR(200) NOT NULL, lastRunAt DATETIME NOT NULL, UNIQUE INDEX UNIQ_55F5ED428ECAEAD4 (command), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

CREATE TABLE jms_jobs (
  id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
  state VARCHAR(15) NOT NULL,
  queue VARCHAR(50) NOT NULL,
  priority SMALLINT NOT NULL,
  createdAt DATETIME NOT NULL,
  startedAt DATETIME DEFAULT NULL,
  checkedAt DATETIME DEFAULT NULL,
  workerName VARCHAR(50) DEFAULT NULL,
  executeAfter DATETIME DEFAULT NULL,
  closedAt DATETIME DEFAULT NULL,
  command VARCHAR(255) NOT NULL,
  args LONGTEXT NOT NULL COMMENT '(DC2Type:json_array)',
  output LONGTEXT DEFAULT NULL,
  errorOutput LONGTEXT DEFAULT NULL,
  exitCode SMALLINT UNSIGNED DEFAULT NULL,
  maxRuntime SMALLINT UNSIGNED NOT NULL,
  maxRetries SMALLINT UNSIGNED NOT NULL,
  stackTrace LONGBLOB DEFAULT NULL COMMENT '(DC2Type:jms_job_safe_object)',
  runtime SMALLINT UNSIGNED DEFAULT NULL,
  memoryUsage INT UNSIGNED DEFAULT NULL,
  memoryUsageReal INT UNSIGNED DEFAULT NULL,
  originalJob_id BIGINT UNSIGNED DEFAULT NULL,
  INDEX IDX_704ADB9349C447F1 (originalJob_id),
  INDEX cmd_search_index (command),
  INDEX sorting_index (state, priority, id),
  PRIMARY KEY(id))
DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

CREATE TABLE jms_job_dependencies (
  source_job_id BIGINT UNSIGNED NOT NULL,
  dest_job_id BIGINT UNSIGNED NOT NULL,
  INDEX IDX_8DCFE92CBD1F6B4F (source_job_id),
  INDEX IDX_8DCFE92C32CF8D4C (dest_job_id),
  PRIMARY KEY(source_job_id, dest_job_id))
DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

CREATE TABLE jms_job_related_entities (
  job_id BIGINT UNSIGNED NOT NULL,
  related_class VARCHAR(150) NOT NULL,
  related_id VARCHAR(100) NOT NULL,
  INDEX IDX_E956F4E2BE04EA9 (job_id),
  PRIMARY KEY(job_id, related_class, related_id))
DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

CREATE TABLE jms_job_statistics (
  job_id BIGINT UNSIGNED NOT NULL,
  characteristic VARCHAR(30) NOT NULL,
  createdAt DATETIME NOT NULL,
  charValue DOUBLE PRECISION NOT NULL,
  PRIMARY KEY(job_id, characteristic, createdAt))
DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

ALTER TABLE jms_jobs ADD CONSTRAINT FK_704ADB9349C447F1 FOREIGN KEY (originalJob_id) REFERENCES jms_jobs (id);
ALTER TABLE jms_job_dependencies ADD CONSTRAINT FK_8DCFE92CBD1F6B4F FOREIGN KEY (source_job_id) REFERENCES jms_jobs (id);
ALTER TABLE jms_job_dependencies ADD CONSTRAINT FK_8DCFE92C32CF8D4C FOREIGN KEY (dest_job_id) REFERENCES jms_jobs (id);
ALTER TABLE jms_job_related_entities ADD CONSTRAINT FK_E956F4E2BE04EA9 FOREIGN KEY (job_id) REFERENCES jms_jobs (id);

-- AME
ALTER TABLE `Offres` ADD COLUMN `id_pdf_echange_accuse` INT NULL AFTER `horodatage_hash_fichiers`;
-- MBL - MPE-2784
ALTER TABLE `Offres` ADD COLUMN `created_at` int NULL;
ALTER TABLE blocFichierEnveloppe DROP FOREIGN KEY blocFichierEnveloppe_fichierEnveloppe;
ALTER TABLE blocFichierEnveloppe ADD CONSTRAINT blocFichierEnveloppe_fichierEnveloppe FOREIGN KEY (`id_fichier`, `organisme`) REFERENCES `fichierEnveloppe` (`id_fichier`, `organisme`) ON DELETE CASCADE;