-- AME SIMAP

ALTER TABLE `Type_Avis_Pub`	ADD COLUMN `id_dispositif` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `Type_Avis_Pub`	ADD COLUMN `type_pub` INT(11) NOT NULL DEFAULT '0' COMMENT '0 : Esender, 1: SUB, 2: TED ';
ALTER TABLE `Destinataire_Pub`	ADD COLUMN `id_dispositif` INT(11) NOT NULL DEFAULT '0' ;
ALTER TABLE `Destinataire_Pub`	ADD COLUMN `id_dossier` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `Destinataire_Pub`	ADD COLUMN `type_pub` INT(11) NOT NULL DEFAULT '0' COMMENT '0 : Esender, 1: SUB, 2: TED ';


-- AME les ECHOS
ALTER TABLE `t_support_publication`	ADD COLUMN `actif` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0 : inactif, 1 : actif';

CREATE TABLE `t_Offre_Support_Publicite` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`libelle_offre` VARCHAR(255) NULL,
	`id_support` INT NOT NULL,
	`actif` ENUM('0','1') NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_t_support_publication` FOREIGN KEY (`id_support`) REFERENCES `t_support_publication` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0;


ALTER TABLE `t_Offre_Support_Publicite`	ADD COLUMN `logo` VARCHAR(255) NULL ;

CREATE TABLE `t_publicite_favoris` (
	`id` INT(22) NOT NULL AUTO_INCREMENT,
	`organisme` VARCHAR(30) NOT NULL,
	`service_id` INT(22) NOT NULL,
	`libelle` VARCHAR(100) NOT NULL DEFAULT '',
	`regime_financier_cautionnement` LONGTEXT NULL,
	`regime_financier_modalites_financement` LONGTEXT NULL,
	`acheteur_correspondant` VARCHAR(100) NOT NULL DEFAULT '',
	`acheteur_nom_organisme` VARCHAR(200) NULL DEFAULT NULL,
	`acheteur_adresse` VARCHAR(100) NOT NULL DEFAULT '',
	`acheteur_cp` VARCHAR(5) NOT NULL DEFAULT '',
	`acheteur_ville` VARCHAR(100) NOT NULL DEFAULT '',
	`acheteur_url` VARCHAR(100) NOT NULL DEFAULT '',
	`facture_denomination` VARCHAR(100) NULL DEFAULT NULL,
	`facture_adresse` VARCHAR(255) NOT NULL DEFAULT '',
	`facture_cp` VARCHAR(10) NOT NULL DEFAULT '',
	`facture_ville` VARCHAR(100) NOT NULL DEFAULT '',
	`instance_recours_organisme` VARCHAR(200) NULL DEFAULT NULL,
	`instance_recours_adresse` VARCHAR(200) NULL DEFAULT NULL,
	`instance_recours_cp` VARCHAR(200) NULL DEFAULT NULL,
	`instance_recours_ville` VARCHAR(200) NULL DEFAULT NULL,
	`instance_recours_url` TEXT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0;

ALTER TABLE `t_donnee_complementaire`	ADD COLUMN `publication_montant_estimation` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'publication du montant estime 0 => non,  1 => oui' AFTER `modalites_financement_regime_financier`;
ALTER TABLE `t_donnee_complementaire`	ADD COLUMN `valeur_montant_estimation_publiee` DOUBLE NULL AFTER `publication_montant_estimation`;

ALTER TABLE `t_support_publication` ADD COLUMN `groupe` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Permet de regrouper des supports' AFTER `url`;
ALTER TABLE `t_support_publication` ADD COLUMN `tag_debut_fin_groupe` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Permet de determiner le debut et la fin d un groupe valeur possible 1 => debut, 2 => mellieu, 3 => fin' AFTER `groupe`;


CREATE TABLE IF NOT EXISTS `t_preference_offre_support_publication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_preference` int(11) NOT NULL,
  `id_support` int(11) NOT NULL,
  `id_offre` int(11) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_preference_offre_support_publication`	ADD COLUMN `id_agent` INT NULL AFTER `active`;
ALTER TABLE `t_preference_offre_support_publication`	ADD COLUMN `organisme` INT NULL AFTER `id_agent`;

ALTER TABLE `t_support_publication`	ADD COLUMN `description` TEXT NULL COMMENT 'description du support' AFTER `tag_debut_fin_groupe`;
ALTER TABLE `t_Offre_Support_Publicite`	ADD COLUMN `ordre` INT NULL COMMENT 'ordre affichage des offres' AFTER `code`;

ALTER TABLE `t_support_publication`	ADD COLUMN `nbre_total_groupe` INT NOT NULL DEFAULT '1' COMMENT 'nbre total des suppots du group du support' AFTER `description`;
