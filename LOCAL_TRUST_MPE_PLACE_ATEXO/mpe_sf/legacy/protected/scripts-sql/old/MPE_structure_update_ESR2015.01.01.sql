-- GCH
ALTER TABLE `t_contrat_titulaire` CHANGE COLUMN `objet_contrat` `objet_contrat` TEXT NULL DEFAULT NULL AFTER `type_depot_reponse`;

-- LEZ
ALTER TABLE `Chorus_organisation_achat` ADD COLUMN `flux_fen111` ENUM( '0', '1' ) NOT NULL DEFAULT '1' ;
ALTER TABLE `Chorus_organisation_achat` ADD COLUMN `flux_fen211` ENUM( '0', '1' ) NOT NULL DEFAULT '1' ;

-- AME
ALTER TABLE `t_type_contrat` ADD COLUMN `article_133` ENUM( '0', '1' ) NOT NULL DEFAULT '0';

ALTER TABLE `t_type_contrat_et_procedure` ADD UNIQUE INDEX `Type_Contrat_Procedure_Organisme_Index` (`id_type_contrat`, `id_type_procedure`, `organisme`);

-- AME
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `clause_sociale_siae` VARCHAR(10) NULL DEFAULT '0' COMMENT 'Le marché est réservé à  SIAE' AFTER `clause_sociale_ateliers_proteges`;
ALTER TABLE `t_contrat_titulaire` ADD COLUMN `clause_sociale_ess` VARCHAR(10) NULL DEFAULT '0' COMMENT 'Le marché est réservé à  ESS' AFTER `clause_sociale_siae`;
