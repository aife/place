
-- Script de mise à jour des BDD communes
--
-- convention: 
-- Chaque nouvelle requete doit commencer au debut d'une ligne et se finir par un point virgule suivi d'un retour à la ligne
-- si besoin est de splitter en plusieurs lignes, mettre une tabulation

CREATE TABLE IF NOT EXISTS `t_document_entreprise` (
`id_document` int(11) NOT NULL AUTO_INCREMENT,
`id_entreprise` int(11) NOT NULL,
`nom_document` varchar(100) NOT NULL,
`id_type_document` int(11) NOT NULL,
`id_derniere_version` int(11) NULL ,
PRIMARY KEY (`id_document`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_document_entreprise` ADD CONSTRAINT `entreprise_document` FOREIGN KEY ( `id_entreprise` ) REFERENCES `Entreprise` (`id`);

CREATE TABLE IF NOT EXISTS `t_entreprise_document_version` (

`id_version_document` int(11) NOT NULL AUTO_INCREMENT,
`id_document` int(11) NOT NULL ,
`date_recuperation` datetime NOT NULL,
`hash` varchar(500) NOT NULL,
`id_blob` int(11) NOT NULL,
PRIMARY KEY (`id_version_document`)
) ENGINE=InnoDB ;

ALTER TABLE `t_entreprise_document_version` ADD CONSTRAINT `entreprise_document_version` FOREIGN KEY ( `id_document` ) REFERENCES `t_document_entreprise` (`id_document`);

CREATE TABLE IF NOT EXISTS `t_document_type` (
`id_type_document` int(11) NOT NULL,
`nom_type_document` varchar(100) NOT NULL,
PRIMARY KEY (`id_type_document`)
) ENGINE=InnoDB ;

ALTER TABLE `t_document_entreprise` ADD CONSTRAINT `entreprise_document_type` FOREIGN KEY ( `id_type_document` ) REFERENCES `t_document_type` (`id_type_document`);

INSERT INTO `t_document_type` (`id_type_document`, `nom_type_document`) VALUES (1, 'Attestation fiscale');
INSERT INTO `t_document_type` (`id_type_document`, `nom_type_document`) VALUES (2, 'Attestation de marché public');
INSERT INTO `t_document_type` (`id_type_document`, `nom_type_document`) VALUES (3, 'Attestation de vigilence');

ALTER TABLE `t_entreprise_document_version` ADD `taille_document` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `t_entreprise_document_version` ADD `extension_document` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `configuration_plateforme` ADD `donnees_candidat` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'gerer l''affichage des attestation de l''entreprise'  ;

ALTER TABLE `EchangePieceJointe` CHANGE `nom_fichier` `nom_fichier` VARCHAR( 255 ) NOT NULL DEFAULT '';
ALTER TABLE `EchangePieceJointe` ADD `extension` VARCHAR( 100 ) NOT NULL DEFAULT '' COMMENT 'Extension du fichier' AFTER `nom_fichier` ;

ALTER TABLE `t_sous_critere_attribution` CHANGE COLUMN `enonce` `enonce` TEXT NULL DEFAULT NULL ;

ALTER TABLE `ProcedureEquivalence` CHANGE `marche_public_simplifie` `marche_public_simplifie` VARCHAR( 2 ) NOT NULL DEFAULT '-0';

ALTER TABLE `configuration_plateforme` ADD `autoriser_creation_entreprise_etrangere` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT 'Permet d''autoriser la création d''une entreprise etrangère' AFTER `donnees_candidat` ;

CREATE TABLE IF NOT EXISTS `HistoriqueNbrConsultationsPubliees` (
  `id_historique_nbr_cons` int(11) NOT NULL AUTO_INCREMENT,
  `date_publication` date NOT NULL DEFAULT '0000-00-00',
  `nbr_consultations_publiees` int(11) NOT NULL,
  PRIMARY KEY (`id_historique_nbr_cons`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

UPDATE `t_document_type` SET  `nom_type_document` =  'Attestation de vigilance' WHERE  `t_document_type`.`id_type_document` =3;

-- LEZ
CREATE TABLE IF NOT EXISTS `t_contrat_multi` (
  `id_contrat_multi` INT(11) NOT NULL AUTO_INCREMENT,
  `numero_contrat_multi` VARCHAR(255) NULL,
  `id_type_contrat_multi` INT(11) NULL,
  `organisme` VARCHAR(45) NULL,
  `service_id` INT(11) NULL,
  `objet_contrat_multi` VARCHAR(255) NULL,
  `montant_max_estime` DOUBLE NULL,
  `id_tranche` INT(11) NULL,
  `numero_long` VARCHAR(255) NULL,
  `reference_libre` VARCHAR(255) NULL,
  `categorie` INT(11) NULL,
  `ccag_applicable` INT(11) NULL,
  `clause_sociale` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_condition_execution` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_insertion` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_ateliers_proteges` VARCHAR(10) NULL DEFAULT 0,
  `clause_environnementale` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_specs_techniques` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_cond_execution` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_criteres_select` VARCHAR(10) NULL DEFAULT 0,
  PRIMARY KEY (`id_contrat_multi`))
ENGINE = InnoDB AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `t_contact_contrat` (
  `id_contact_contrat` INT(11) NOT NULL AUTO_INCREMENT,
  `id_entreprise` INT(11) NULL,
  `id_etablissement` INT(11) NULL,
  `id_inscrit` INT(11) NULL,
  `nom` VARCHAR(80) NULL,
  `prenom` VARCHAR(80) NULL,
  `email` VARCHAR(100) NULL,
  `telephone` VARCHAR(20) NULL,
  `fax` VARCHAR(30) NULL,
  PRIMARY KEY (`id_contact_contrat`))
ENGINE = InnoDB AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `t_contrat_titulaire` (
  `id_contrat_titulaire` INT(11) NOT NULL AUTO_INCREMENT,
  `id_type_contrat` INT(11) NULL DEFAULT NULL,
  `id_contact_contrat` INT(11) NULL,
  `numero_contrat` VARCHAR(255) NULL,
  `lien_AC_SAD` INT(11) NULL,
  `id_contrat_multi` INT(11) NULL,
  `organisme` VARCHAR(45) NULL,
  `service_id` INT(11) NULL,
  `id_titulaire` INT(11) NULL,
  `id_titulaire_etab` INT(11) NULL,
  `objet_contrat` VARCHAR(255) NULL,
  `montant_contrat` DOUBLE NULL,
  `id_tranche_budgetaire` INT(11) NULL,
  `montant_max_estime` DOUBLE NULL,
  `publication_montant` TINYINT NULL,
  `id_motif_non_publication_montant` INT(11) NULL,
  `desc_motif_non_publication_montant` VARCHAR(255) NULL,
  `publication_contrat` TINYINT NULL,
  `num_EJ` VARCHAR(45) NULL,
  `num_long_OEAP` VARCHAR(45) NULL,
  `reference_libre` VARCHAR(45) NULL,
  `statut_contrat` INT(11) NULL,
  `categorie` INT(11) NULL,
  `ccag_applicable` INT(11) NULL,
  `clause_sociale` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_condition_execution` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_insertion` VARCHAR(10) NULL DEFAULT 0,
  `clause_sociale_ateliers_proteges` VARCHAR(10) NULL DEFAULT 0,
  `clause_environnementale` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_specs_techniques` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_cond_execution` VARCHAR(10) NULL DEFAULT 0,
  `clause_env_criteres_select` VARCHAR(10) NULL DEFAULT 0,
  `date_prevue_notification` DATE NULL,
  `date_prevue_fin_contrat` DATE NULL,
  `date_prevue_max_fin_contrat` DATE NULL,
  `date_notification` DATE NULL,
  `date_fin_contrat` DATE NULL,
  `date_max_fin_contrat` DATE NULL,
  PRIMARY KEY (`id_contrat_titulaire`)
  )ENGINE = InnoDB AUTO_INCREMENT=1;

ALTER TABLE `t_contrat_titulaire` ADD INDEX `fk_Contrat_Titulaire_Contrat_Multi1_idx` (`id_contrat_multi`) ;
ALTER TABLE `t_contrat_titulaire` ADD CONSTRAINT `fk_Contrat_Titulaire_Contrat_Multi1` FOREIGN KEY ( `id_contrat_multi` ) REFERENCES `t_contrat_multi` (`id_contrat_multi`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `t_contrat_titulaire` ADD CONSTRAINT `fk_Contrat_Titulaire_Contact_Contrat`  FOREIGN KEY (`id_contact_contrat`) REFERENCES `t_contact_contrat` (`id_contact_contrat`) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `t_contrat_titulaire` CHANGE `statut_contrat` `statut_contrat` INT( 11 ) NULL DEFAULT 0 COMMENT 'permet de définir le statut du contrat : 0=>Étape Type de décision,1=>Étape Données du contrat - A saisir,2=>Numèrotion autonome,3=>Étape Notification,4=>Étape Notification effectuée';

CREATE TABLE IF NOT EXISTS `t_cons_lot_contrat` (
  `id_cons_lot_contrat` INT(11) NOT NULL AUTO_INCREMENT,
  `organisme` VARCHAR(45) NULL,
  `consultation_ref` INT(11) NULL,
  `lot` INT(11) NULL,
  `id_contrat_titulaire` INT(11) NOT NULL,
  PRIMARY KEY (`id_cons_lot_contrat`))
ENGINE = InnoDB AUTO_INCREMENT=1;

ALTER TABLE `t_cons_lot_contrat` ADD INDEX `fk_Lot_Contrat_Contrat_Titulaire_idx` (`id_contrat_titulaire`) ;
ALTER TABLE `t_cons_lot_contrat` ADD INDEX `fk_t_cons_lot_contrat_lot1_idx` (`lot`) ;
ALTER TABLE `t_cons_lot_contrat` ADD CONSTRAINT `fk_Lot_Contrat_Contrat_Titulaire` FOREIGN KEY ( `id_contrat_titulaire` ) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Entreprise` ADD `created_from_decision` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de définir si l''entreprise est créée depuis la décision ou non' AFTER `saisie_manuelle`;

ALTER TABLE `t_contrat_titulaire` ADD `id_offre` INT( 11 ) default NULL AFTER `id_titulaire_etab` ;
ALTER TABLE `t_contrat_titulaire` ADD `type_depot_reponse` CHAR( 1 ) default NULL AFTER `id_offre` ;
ALTER TABLE `t_contrat_titulaire` ADD `date_attribution` DATE default NULL AFTER `date_max_fin_contrat`;
ALTER TABLE `t_contrat_titulaire` ADD `date_creation` DATETIME default NULL AFTER `date_attribution`;
ALTER TABLE `t_contrat_titulaire` ADD `date_modification` DATETIME default NULL AFTER `date_creation`;

ALTER TABLE `t_contrat_multi` ADD `date_creation` DATETIME default NULL AFTER `clause_env_criteres_select`;
ALTER TABLE `t_contrat_multi` ADD `date_modification` DATETIME default NULL AFTER `date_creation`;

CREATE TABLE IF NOT EXISTS `t_type_contrat` (
  `id_type_contrat` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_type_contrat` varchar(255) NOT NULL,
  `multi` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'ce champ est égale à un si ce type accepte contrat multi',
  PRIMARY KEY (`id_type_contrat`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

INSERT INTO `t_type_contrat` (`id_type_contrat`, `libelle_type_contrat`, `multi`) VALUES (1, 'TYPE_CONTRAT_MARCHE', '0');
INSERT INTO `t_type_contrat` (`id_type_contrat`, `libelle_type_contrat`, `multi`) VALUES (2, 'TYPE_CONTRAT_AC_MONO_ATTRIBUTAIRE', '0');
INSERT INTO `t_type_contrat` (`id_type_contrat`, `libelle_type_contrat`, `multi`) VALUES (3, 'TYPE_CONTRAT_SYSTEME_ACQUISITION_DYNAMIQUE', '1');
INSERT INTO `t_type_contrat` (`id_type_contrat`, `libelle_type_contrat`, `multi`) VALUES (4, 'TYPE_CONTRAT_DELEGATION_DE_SERVICE_PUBLIC', '0');
INSERT INTO `t_type_contrat` (`id_type_contrat`, `libelle_type_contrat`, `multi`) VALUES (5, 'TYPE_CONTRAT_AC_MULTI_ATTRIBUTAIRE', '1');

CREATE TABLE IF NOT EXISTS `t_numerotation_automatique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organisme` varchar(45) NOT NULL,
  `service_id` INT(11) NULL,
  `annee` varchar(10) NULL,
  `contrat_AC_multi` varchar(20) NULL,
  `contrat_SA_dynamique` varchar(20) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_numerotation_automatique` ADD `contrat_titulaire` VARCHAR( 20 ) NULL AFTER `contrat_SA_dynamique`;

ALTER TABLE `t_contrat_titulaire` ADD `statutEJ` TEXT NULL AFTER `num_EJ`;

CREATE TABLE IF NOT EXISTS `t_decision_selection_entreprise` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `consultation_ref` INT(11) NULL,
  `organisme` VARCHAR(45) NULL,
  `service_id` INT(11) NULL,
  `lot` INT(11) NULL,
  `id_entreprise` INT(11) NULL,
  `id_etablissement` INT(11) NULL,
  `id_offre` INT(11) NULL,
  `type_depot_reponse` CHAR( 1 ) NULL,
  `date_creation` DATETIME default NULL,
  `date_modification` DATETIME default NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB AUTO_INCREMENT=1;

ALTER TABLE `t_decision_selection_entreprise` ADD `id_contact_contrat` INT(11) NULL AFTER `date_modification`;
ALTER TABLE `t_decision_selection_entreprise` ADD CONSTRAINT `fk_Decision_Selection_Entreprise_Contact_Contrat`  FOREIGN KEY (`id_contact_contrat`) REFERENCES `t_contact_contrat` (`id_contact_contrat`) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `Marche` ADD `id_contrat_titulaire` INT( 11 ) NOT NULL AFTER `id_decision_enveloppe` ;
ALTER TABLE `Marche` ADD CONSTRAINT `fk_Marche_contrat_titulaire`  FOREIGN KEY (`id_contrat_titulaire`) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `Offre_papier` ADD `entreprise_id` INT( 11 ) NULL  AFTER `consultation_ref` ;
ALTER TABLE `Offre_papier` ADD `id_etablissement` INT( 11 )  NULL AFTER `entreprise_id` ;

ALTER TABLE `t_contrat_titulaire` ADD `envoi_interface` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de spécifier si la decision a été exportée vers une interface';

ALTER TABLE `t_contrat_titulaire` ADD `contrat_class_key` INT( 1 ) NOT NULL DEFAULT '1' COMMENT 'utiliser pour utiliser l''heritage de propel la valeur 1 => l''objet contrat titulaire,2=> l''objet contrat multi' ;

ALTER TABLE `t_contrat_titulaire` DROP FOREIGN KEY `fk_Contrat_Titulaire_Contrat_Multi1` ;
ALTER TABLE `t_contrat_titulaire` ADD CONSTRAINT `fk_Contrat_Titulaire_Contrat_Multi1` FOREIGN KEY ( `id_contrat_multi` ) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON DELETE CASCADE ON UPDATE CASCADE ;

DROP TABLE IF EXISTS `t_contrat_multi`;

ALTER TABLE `t_contrat_titulaire` ADD `envoi_interface` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'Permet de spécifier si la decision a été exportée vers une interface';

-- LEZ
ALTER TABLE  `Chorus_echange` DROP FOREIGN KEY  `ChorusEchange_decisionEnveloppe` ;

-- OKO
ALTER TABLE `t_type_contrat` ADD `abreviation_type_contrat` CHAR( 2 ) NULL DEFAULT NULL COMMENT 'Abreviation des types de contrats' AFTER `libelle_type_contrat` ;
UPDATE `t_type_contrat` SET `abreviation_type_contrat` = 'AC' WHERE `t_type_contrat`.`libelle_type_contrat` ='TYPE_CONTRAT_AC_MONO_ATTRIBUTAIRE';
UPDATE `t_type_contrat` SET `abreviation_type_contrat` = 'AM' WHERE `t_type_contrat`.`libelle_type_contrat` ='TYPE_CONTRAT_AC_MULTI_ATTRIBUTAIRE';
UPDATE `t_type_contrat` SET `abreviation_type_contrat` = 'AU' WHERE `t_type_contrat`.`libelle_type_contrat` ='TYPE_CONTRAT_DELEGATION_DE_SERVICE_PUBLIC';
UPDATE `t_type_contrat` SET `abreviation_type_contrat` = 'MA' WHERE `t_type_contrat`.`libelle_type_contrat` ='TYPE_CONTRAT_MARCHE';
UPDATE `t_type_contrat` SET `abreviation_type_contrat` = 'SA' WHERE `t_type_contrat`.`libelle_type_contrat` ='TYPE_CONTRAT_SYSTEME_ACQUISITION_DYNAMIQUE';


-- OKO
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale` `clause_sociale` VARCHAR( 10 ) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale_condition_execution` `clause_sociale_condition_execution` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale_insertion` `clause_sociale_insertion` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_sociale_ateliers_proteges` `clause_sociale_ateliers_proteges` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_environnementale` `clause_environnementale` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_env_specs_techniques` `clause_env_specs_techniques` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_env_cond_execution` `clause_env_cond_execution` VARCHAR(10) NULL DEFAULT NULL;
ALTER TABLE `t_contrat_titulaire` CHANGE `clause_env_criteres_select` `clause_env_criteres_select` VARCHAR(10) NULL DEFAULT NULL;

-- ASO
CREATE TABLE IF NOT EXISTS `t_bourse_cotraitance` (
	`id_auto_BC` INT NOT NULL AUTO_INCREMENT,
	`reference_consultation` INT(11) NOT NULL,
	`id_entreprise` INT(11) NOT NULL,
	`nom_inscrit` VARCHAR(100) NOT NULL,
	`prenom_inscrit` VARCHAR(100) NOT NULL,
	`adresse_inscrit` VARCHAR(100) NOT NULL,
	`adresse2_incsrit` VARCHAR(100) NULL DEFAULT NULL,
	`cp_inscrit` VARCHAR(20) NOT NULL,
	`ville_inscrit` VARCHAR(100) NOT NULL,
	`pays_inscrit` VARCHAR(250) NULL DEFAULT NULL,
	`fonction_inscrit` VARCHAR(100) NOT NULL,
	`email_inscrit` VARCHAR(100) NOT NULL,
	`tel_fixe_inscrit` VARCHAR(100) NULL DEFAULT NULL,
	`tel_mobile_inscrit` VARCHAR(100) NULL DEFAULT NULL,
	`mandataire_groupement` ENUM('0','1') NOT NULL DEFAULT '0',
	`cotraitant_solidaire` ENUM('0','1') NOT NULL DEFAULT '0',
	`cotraitant_conjoint` ENUM('0','1') NOT NULL DEFAULT '0',
	`desc_mon_apport_marche` VARCHAR(360) NULL DEFAULT NULL,
	`desc_type_cotraitance_recherche` VARCHAR(360) NULL DEFAULT NULL,
	PRIMARY KEY (`id_auto_BC`)
)
ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE `t_bourse_cotraitance` ADD COLUMN `clause_social` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `desc_type_cotraitance_recherche`;
ALTER TABLE `t_bourse_cotraitance` ADD COLUMN `entreprise_adapte` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `clause_social`;

ALTER TABLE `configuration_plateforme` ADD COLUMN `bourse_cotraitance` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer la bourse à la cotraitance' AFTER `autoriser_creation_entreprise_etrangere`;

ALTER TABLE `t_bourse_cotraitance` ADD COLUMN `id_etablissement_inscrite` INT(11) NOT NULL AFTER `id_entreprise`;

ALTER TABLE `t_bourse_cotraitance` CHANGE COLUMN `desc_mon_apport_marche` `desc_mon_apport_marche` TEXT NULL DEFAULT NULL AFTER `cotraitant_conjoint`;
ALTER TABLE `t_bourse_cotraitance` CHANGE COLUMN `desc_type_cotraitance_recherche` `desc_type_cotraitance_recherche` TEXT NULL DEFAULT NULL AFTER `desc_mon_apport_marche`;