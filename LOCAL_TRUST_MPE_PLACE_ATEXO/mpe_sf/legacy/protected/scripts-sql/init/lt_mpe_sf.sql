--
-- Table structure for table `temp_contextes`
--

CREATE TABLE T_Access_Token (
  id CHAR(36) NOT NULL COMMENT '(DC2Type:uuid)',
  data LONGTEXT NOT NULL COMMENT '(DC2Type:json_array)',
  ip VARCHAR(15) NOT NULL,
  user_id INT NOT NULL,
  user_type INT NOT NULL,
  created_at DATETIME NOT NULL,
  INDEX idenfifiant_acces_token (id),
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

