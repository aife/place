-- phpMyAdmin SQL Dump
-- version 2.11.3deb1ubuntu1.1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 20 Avril 2009 à 17:18
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.4-2ubuntu5.5

SET FOREIGN_KEY_CHECKS=0;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

SET AUTOCOMMIT=0;
START TRANSACTION;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `mpe3_a1z`
--

-- --------------------------------------------------------

--
-- Structure de la table `AcheteurPublic`
--

CREATE TABLE IF NOT EXISTS `AcheteurPublic` (
  `id` int(22) NOT NULL auto_increment,
  `denomination` varchar(100) NOT NULL default '',
  `prm` varchar(100) NOT NULL default '',
  `adresse` varchar(100) NOT NULL default '',
  `cp` varchar(5) NOT NULL default '',
  `ville` varchar(100) NOT NULL default '',
  `dept` char(3) NOT NULL default '',
  `type_org` char(1) NOT NULL default '',
  `telephone` varchar(100) NOT NULL default '',
  `fax` varchar(100) NOT NULL default '',
  `mail` varchar(100) NOT NULL default '',
  `url` varchar(100) NOT NULL default '',
  `url_acheteur` varchar(100) default NULL,
  `facture_numero` varchar(100) NOT NULL default '',
  `facture_code` varchar(100) NOT NULL default '',
  `facture_denomination` varchar(100) NOT NULL default '',
  `facturation_service` varchar(100) default NULL,
  `facture_adresse` varchar(255) NOT NULL default '',
  `facture_cp` varchar(10) NOT NULL default '',
  `facture_ville` varchar(100) NOT NULL default '',
  `facture_pays` varchar(100) NOT NULL default '',
  `id_aapc` int(22) NOT NULL default '0',
  `boamp_login` varchar(100) NOT NULL default '',
  `boamp_password` varchar(100) NOT NULL default '',
  `boamp_mail` varchar(100) NOT NULL default '',
  `boamp_target` char(1) NOT NULL default '0',
  `default_form_values` longtext NOT NULL,
  `defaut_form_am_boamp` longtext NOT NULL,
  `defaut_form_am_boamp_joue` longtext NOT NULL,
  `defaut_form_mapa_boamp` longtext NOT NULL,
  `defaut_form_aconcours` longtext NOT NULL,
  `defaut_form_as_boamp_joue` longtext NOT NULL,
  `defaut_form_aa_boamp` longtext NOT NULL,
  `defaut_form_ar_mapa_boamp` longtext NOT NULL,
  `defaut_form_05_boamp` longtext NOT NULL,
  `defaut_form_rect` longtext NOT NULL,
  `defaut_form_aa_boamp_joue` longtext NOT NULL,
  `id_service` int(11) NOT NULL default '0',
  `livraison_service` varchar(100) default NULL,
  `livraison_adresse` varchar(100) default NULL,
  `livraison_code_postal` varchar(5) default NULL,
  `livraison_ville` varchar(100) default NULL,
  `livraison_pays` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `AcheteurPublic`
--


-- --------------------------------------------------------

--
-- Structure de la table `AcheteurPublicCentrale`
--

CREATE TABLE IF NOT EXISTS `AcheteurPublicCentrale` (
  `id_compte` int(11) NOT NULL auto_increment,
  `identifiant_compte` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `telecopieur` varchar(255) NOT NULL default '',
  `centrale_publication` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id_compte`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `AcheteurPublicCentrale`
--


-- --------------------------------------------------------

--
-- Structure de la table `Admissibilite_Enveloppe_Lot`
--

CREATE TABLE IF NOT EXISTS `Admissibilite_Enveloppe_Lot` (
  `id_Offre` int(22) NOT NULL default '0',
  `sous_pli` int(3) NOT NULL default '0',
  `admissibilite` int(1) NOT NULL default '0',
  `commentaire` text,
  PRIMARY KEY  (`id_Offre`,`sous_pli`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Admissibilite_Enveloppe_Lot`
--


-- --------------------------------------------------------

--
-- Structure de la table `Admissibilite_Enveloppe_papier_Lot`
--

CREATE TABLE IF NOT EXISTS `Admissibilite_Enveloppe_papier_Lot` (
  `id_offre_papier` int(22) NOT NULL default '0',
  `sous_pli` int(3) NOT NULL default '0',
  `admissibilite` int(1) NOT NULL default '0',
  `commentaire` text,
  PRIMARY KEY  (`id_offre_papier`,`sous_pli`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Admissibilite_Enveloppe_papier_Lot`
--


-- --------------------------------------------------------


--
-- Structure de la table `AffiliationService`
--

CREATE TABLE IF NOT EXISTS `AffiliationService` (
  `id_pole` int(11) NOT NULL default '0',
  `id_service` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id_service`,`id_pole`),
  KEY `id_service` (`id_service`),
  KEY `id_pole` (`id_pole`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `AffiliationService`
--


-- --------------------------------------------------------

--
-- Structure de la table `Agent_Commission`
--

CREATE TABLE IF NOT EXISTS `Agent_Commission` (
  `id_commission` int(11) NOT NULL default '0',
  `id_agent` int(11) NOT NULL default '0',
  `convocation` longblob,
  `convoc_send` char(1) NOT NULL default '0',
  `nom_convoc` varchar(255) default NULL,
  `contenu_envoi` text,
  `fichier_envoye` longblob,
  `nom_fichier_envoye` varchar(255) default NULL,
  `date_envoi` datetime default NULL,
  PRIMARY KEY  (`id_commission`,`id_agent`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Agent_Commission`
--


-- --------------------------------------------------------

--
-- Structure de la table `Annonce`
--

CREATE TABLE IF NOT EXISTS `Annonce` (
  `id_boamp` int(22) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `envoi_boamp` int(22) NOT NULL default '0',
  `date_envoi` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `ann_xml` longblob NOT NULL,
  `ann_pdf` longblob NOT NULL,
  `ann_form_values` longblob NOT NULL,
  `ann_error` longtext NOT NULL,
  `type_boamp` text NOT NULL,
  `type_ann` int(11) NOT NULL default '0',
  `datepub` varchar(10) NOT NULL default '0000-00-00',
  `num_ann` int(11) NOT NULL default '0',
  `parution` text NOT NULL,
  `id_jo` varchar(255) NOT NULL default '',
  `erreurs` text NOT NULL,
  `nom_fichier_xml` varchar(255) NOT NULL default '',
  `envoi_joue` enum('0','1') NOT NULL default '0',
  `mapa` enum('0','1') NOT NULL default '0',
  `implique_SAD` enum('0','1') NOT NULL default '0',
  `date_maj` datetime default NULL,
  `id_destination_form_xml` int(11) NOT NULL default '0',
  `id_form_xml` int(11) NOT NULL default '0',
  `id_type_xml` int(11) NOT NULL default '0',
  `statut_destinataire` char(10) NOT NULL,
  `accuse_reception` char(1) NOT NULL default '',
  `lien` varchar(50) NOT NULL default '',
  `lien_boamp` text,
  `lien_pdf` text,
  PRIMARY KEY  (`id_boamp`),
  KEY `consultation_ref` (`consultation_ref`),
  KEY `id_destination_form_xml` (`id_destination_form_xml`),
  KEY `id_form_xml` (`id_form_xml`),
  KEY `id_type_xml` (`id_type_xml`),
  KEY `id_destination_form_xml_2` (`id_destination_form_xml`),
  KEY `id_form_xml_2` (`id_form_xml`),
  KEY `id_type_xml_2` (`id_type_xml`),
  KEY `id_destination_form_xml_3` (`id_destination_form_xml`),
  KEY `id_form_xml_3` (`id_form_xml`),
  KEY `id_type_xml_3` (`id_type_xml`),
  KEY `Idx_Annonce` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Annonce`
--


-- --------------------------------------------------------

--
-- Structure de la table `AnnonceJAL`
--

CREATE TABLE IF NOT EXISTS `AnnonceJAL` (
  `id` int(11) NOT NULL auto_increment,
  `libelle_type` varchar(250) NOT NULL default '',
  `date_creation` varchar(20) NOT NULL,
  `objet` varchar(255) default NULL,
  `texte` text,
  `consultation_ref` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `AnnonceJAL`
--


-- --------------------------------------------------------

--
-- Structure de la table `AnnonceJALPieceJointe`
--

CREATE TABLE IF NOT EXISTS `AnnonceJALPieceJointe` (
  `id` int(11) NOT NULL auto_increment,
  `id_annonce_jal` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `piece` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(25) NOT NULL default '',
  `taille` varchar(25) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id_annonce_jal` (`id_annonce_jal`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `AnnonceJALPieceJointe`
--


-- --------------------------------------------------------

--
-- Structure de la table `AnnonceMoniteur`
--

CREATE TABLE IF NOT EXISTS `AnnonceMoniteur` (
  `id` int(22) NOT NULL auto_increment,
  `date_envoi` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `type_moniteur` text NOT NULL,
  `statut_xml` varchar(4) NOT NULL default '0',
  `date_creation` date NOT NULL default '0000-00-00',
  `statut_web` enum('0','1','2','3','4','5') NOT NULL default '0',
  `statut_arg` enum('0','1','2','3','4','5') NOT NULL default '0',
  `statut_usn` enum('0','1','2','3','4','5') NOT NULL default '0',
  `statut_01i` enum('0','1','2','3','4','5') NOT NULL default '0',
  `datepub` varchar(10) default NULL,
  `num_annonce` int(11) NOT NULL default '0',
  `message_error` text NOT NULL,
  `timestamp` blob NOT NULL,
  `nom_timestamp` varchar(100) NOT NULL default '',
  `xml_moniteur` text NOT NULL,
  `id_annonce_boamp` int(22) NOT NULL default '0',
  `date_maj` varchar(20) default '0000-00-00 00:00:00',
  `id_form_xml` int(11) NOT NULL,
  `accuse_reception` char(1) NOT NULL,
  `id_destination_form_xml` int(11) NOT NULL,
  `statut_destinataire` varchar(10) default NULL,
  `commentaire` text,
  PRIMARY KEY  (`id`),
  KEY `id_form_xml` (`id_form_xml`),
  KEY `id_destination_form_xml` (`id_destination_form_xml`),
  KEY `id_form_xml_2` (`id_form_xml`),
  KEY `id_destination_form_xml_2` (`id_destination_form_xml`),
  KEY `id_form_xml_3` (`id_form_xml`),
  KEY `id_destination_form_xml_3` (`id_destination_form_xml`),
  KEY `id_form_xml_4` (`id_form_xml`),
  KEY `id_destination_form_xml_4` (`id_destination_form_xml`),
  KEY `id_form_xml_5` (`id_form_xml`),
  KEY `id_destination_form_xml_5` (`id_destination_form_xml`),
  KEY `id_form_xml_6` (`id_form_xml`),
  KEY `id_destination_form_xml_6` (`id_destination_form_xml`),
  KEY `id_form_xml_7` (`id_form_xml`),
  KEY `id_destination_form_xml_7` (`id_destination_form_xml`),
  KEY `id_form_xml_8` (`id_form_xml`),
  KEY `id_destination_form_xml_8` (`id_destination_form_xml`),
  KEY `id_form_xml_9` (`id_form_xml`),
  KEY `id_destination_form_xml_9` (`id_destination_form_xml`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `AnnonceMoniteur`
--


-- --------------------------------------------------------


--
-- Structure de la table `Avenant`
--

CREATE TABLE IF NOT EXISTS `Avenant` (
  `id_avenant` int(11) NOT NULL auto_increment,
  `id_contrat` int(11) NOT NULL,
  `numero_avenant` varchar(20) default NULL,
  `type_avenant` int(5) default NULL,
  `objet_avenant` varchar(100) default NULL,
  `montant_avenant_ht` varchar(30) default NULL,
  `montant_avenant_ttc` varchar(30) default NULL,
  `pourcentage_augmentation_marche_initial` char(5) default NULL,
  `pourcentage_augmentation_cumule` char(5) default NULL,
  `montant_total_marche_tout_avenant_cumule` varchar(30) default NULL,
  `date_reception_projet_par_secretaire_cao` varchar(10) default NULL,
  `date_reception_projet_par_charge_etude` varchar(10) default NULL,
  `date_observation_par_sv` varchar(10) default NULL,
  `date_retour_projet` varchar(10) default NULL,
  `date_validation_projet` varchar(10) default NULL,
  `date_validation_projet_vu_par` int(5) default NULL,
  `date_cao` varchar(10) default NULL,
  `avis_cao` int(5) default NULL,
  `date_cp` varchar(10) default NULL,
  `date_signature_avenant` varchar(10) default NULL,
  `date_reception_dossier` varchar(10) default NULL,
  `date_formulation_observation_par_sv_sur_sdossier` varchar(10) default NULL,
  `date_retour_dossier_finalise` varchar(10) default NULL,
  `date_validation_dossier_finalise` varchar(10) NOT NULL,
  `date_transmission_prefecture` varchar(10) default NULL,
  `date_notification` varchar(10) default NULL,
  `operation_validation_vues_par` int(5) default NULL,
  `commentaires` text,
  PRIMARY KEY  (`id_avenant`),
  KEY `id_contrat` (`id_contrat`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Avenant`
--


-- --------------------------------------------------------

--
-- Structure de la table `AVIS`
--

CREATE TABLE IF NOT EXISTS `AVIS` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `avis` int(11) NOT NULL default '0',
  `intitule_avis` int(2) NOT NULL default '0',
  `nom_avis` varchar(80) NOT NULL default '',
  `statut` char(1) NOT NULL default '0',
  `nom_fichier` varchar(80) NOT NULL default '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(11) NOT NULL default '0',
  `avis_telechargeable` int(11) NOT NULL default '0',
  `url` text,
  `type` char(1) default '0',
  `date_creation` varchar(20) NOT NULL,
  `date_pub` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `AVIS`
--


-- --------------------------------------------------------

--
-- Structure de la table `AvisCao`
--

CREATE TABLE IF NOT EXISTS `AvisCao` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `AvisCao`
--

INSERT INTO `AvisCao` VALUES
(1, 'Avis favorable'),
(2, 'Avis défavorable'),
(3, 'Sans suite');

-- --------------------------------------------------------

--
-- Structure de la table `blob`
--

CREATE TABLE IF NOT EXISTS `blob` (
  `id` int(11) NOT NULL auto_increment,
  `revision` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `blob`
--


-- --------------------------------------------------------

--
-- Structure de la table `blob_file`
--

CREATE TABLE IF NOT EXISTS `blob_file` (
  `id` int(11) NOT NULL default '0',
  `revision` int(11) NOT NULL default '0',
  `name` text NOT NULL,
  `deletion_datetime` datetime default NULL,
  PRIMARY KEY  (`id`,`revision`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `blob_file`
--


-- --------------------------------------------------------

--
-- Structure de la table `blocFichierEnveloppe`
--

CREATE TABLE IF NOT EXISTS `blocFichierEnveloppe` (
  `id_bloc_fichier` int(11) NOT NULL auto_increment,
  `id_fichier` int(11) NOT NULL default '0',
  `numero_ordre_bloc` int(5) NOT NULL default '0',
  `id_blob_chiffre` int(11) NOT NULL default '0',
  `id_blob_dechiffre` int(11) default NULL,
  PRIMARY KEY  (`id_bloc_fichier`),
  KEY `id_fichier` (`id_fichier`),
  KEY `id_fichier_2` (`id_fichier`),
  KEY `id_fichier_3` (`id_fichier`),
  KEY `id_fichier_4` (`id_fichier`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `blocFichierEnveloppe`
--


-- --------------------------------------------------------

--
-- Structure de la table `blocFichierEnveloppeTemporaire`
--

CREATE TABLE IF NOT EXISTS `blocFichierEnveloppeTemporaire` (
  `id` int(11) NOT NULL auto_increment,
  `uid` varchar(50) NOT NULL default '',
  `type_env` int(1) NOT NULL default '0',
  `sous_pli` int(3) NOT NULL default '0',
  `type_fichier` char(3) NOT NULL default '',
  `numero_ordre_fichier` int(5) NOT NULL default '0',
  `numero_ordre_bloc` int(5) NOT NULL default '0',
  `id_blob` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `blocFichierEnveloppeTemporaire`
--


-- --------------------------------------------------------

--
-- Structure de la table `CategorieConsultation`
--

CREATE TABLE IF NOT EXISTS `CategorieConsultation` (
  `id` varchar(30) NOT NULL default '',
  `libelle` varchar(100) NOT NULL default '',
  `connecteur` varchar(50) NOT NULL default '',
  `id_categorie_portail` varchar(11) NOT NULL default '',
  `libelle_fr` varchar(100) default '',
  `libelle_en` varchar(100) default '',
  `libelle_es` varchar(100) default '',
  `libelle_su` varchar(100) default '',
  `libelle_du` varchar(100) default '',
  `libelle_cz` varchar(100) default '',  
  `libelle_ar` varchar(100) default '',
  `libelle_it` varchar(100) default '',
  
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `CategorieConsultation`
--


INSERT INTO `CategorieConsultation` (`id`, `libelle`, `connecteur`, `id_categorie_portail`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES
('1', 'Travaux', '', '1', 'Travaux', 'Works', 'Obras', '(su)Travaux', '(du)Travaux', '(cz)Travaux', NULL, 'Lavori'),
('2', 'Fournitures', '', '2', 'Fournitures', 'Supplies', 'Suministros', '(su)Fournitures', '(du)Fournitures', '(cz)Fournitures', NULL, 'Forniture'),
('3', 'Services', '', '3', 'Services', 'Services', 'Servicios', '(su)Services', '(du)Services', '(cz)Services', NULL, 'Servizi');
-- --------------------------------------------------------

--
-- Structure de la table `CategorieLot`
--

CREATE TABLE IF NOT EXISTS `CategorieLot` (
  `consultation_ref` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `description` varchar(1000) NOT NULL default '',
  `categorie` varchar(30) NOT NULL default '',
  `description_detail` varchar(1000) default '',
  `code_cpv_1` varchar(8) default NULL,
  `code_cpv_2` varchar(8) default NULL,
  `code_cpv_3` varchar(8) default NULL,
  `code_cpv_4` varchar(8) default NULL,
  `libelle_code_cpv_1` varchar(200) default NULL,
  `libelle_code_cpv_2` varchar(200) default NULL,
  `libelle_code_cpv_3` varchar(200) default NULL,
  `libelle_code_cpv_4` varchar(200) default NULL,
  `description_fr` varchar(1000) default '',
  `description_en` varchar(255) NOT NULL default '',
  `description_es` varchar(255) NOT NULL default '',
  `description_su` varchar(255) NOT NULL default '',
  `description_du` varchar(255) NOT NULL default '',
  `description_cz` varchar(255) NOT NULL default '',
  `description_detail_fr` varchar(1000) default '',
  `description_detail_en` varchar(255) default NULL,
  `description_detail_es` varchar(255) default NULL,
  `description_detail_su` varchar(255) default NULL,
  `description_detail_du` varchar(255) default NULL,
  `description_detail_cz` varchar(255) default NULL,
  `decision` enum('0','1') NOT NULL default '0',
  `id_lot_externe` int(11) default NULL,
  PRIMARY KEY  (`consultation_ref`,`lot`),
  KEY `consultation_ref` (`consultation_ref`),
  KEY `Idx_CategorieLot` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `CategorieLot`
--


-- --------------------------------------------------------

--
-- Structure de la table `ccag_applicable`
--

CREATE TABLE IF NOT EXISTS `ccag_applicable` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `ccag_applicable`
--

INSERT INTO `ccag_applicable` VALUES
(1, 'FCS'),
(2, 'PI'),
(3, 'T');

-- --------------------------------------------------------

--
-- Structure de la table `CertificatChiffrement`
--

CREATE TABLE IF NOT EXISTS `CertificatChiffrement` (
  `consultation_ref` int(11) NOT NULL default '0',
  `type_env` int(1) NOT NULL default '0',
  `sous_pli` int(3) NOT NULL default '0',
  `index_certificat` int(3) NOT NULL default '1',
  `certificat` mediumtext NOT NULL,
  PRIMARY KEY  (`consultation_ref`,`type_env`,`sous_pli`,`index_certificat`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `CertificatChiffrement`
--


-- --------------------------------------------------------

--
-- Structure de la table `CertificatPermanent`
--

CREATE TABLE IF NOT EXISTS `CertificatPermanent` (
  `id` int(11) NOT NULL auto_increment,
  `Titre` varchar(100) NOT NULL default '',
  `Prenom` varchar(100) NOT NULL default '',
  `Nom` varchar(100) NOT NULL default '',
  `EMail` varchar(100) NOT NULL default '',
  `Certificat` mediumtext NOT NULL,
  `service_id` int(11) NOT NULL default '-1',
  `master_key` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `CertificatPermanent`
--


-- --------------------------------------------------------

--
-- Structure de la table `ClientCAO`
--

CREATE TABLE IF NOT EXISTS `ClientCAO` (
  `id` int(22) NOT NULL auto_increment,
  `date_operation` datetime NOT NULL default '0000-00-00 00:00:00',
  `operation` varchar(255) NOT NULL default '',
  `client_ip` varchar(255) NOT NULL default '0.0.0.0',
  `agent_id` int(22) NOT NULL default '0',
  `offre_id` int(22) NOT NULL default '0',
  `type_env` int(1) NOT NULL default '0',
  `sous_pli` int(3) NOT NULL default '0',
  `donnees_operation` longblob NOT NULL,
  `horodatage_operation` longblob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `offre_id` (`offre_id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `ClientCAO`
--


-- --------------------------------------------------------


--
-- Structure de la table `Commission`
--

CREATE TABLE IF NOT EXISTS `Commission` (
  `id` int(11) NOT NULL auto_increment,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `lieu` varchar(200) NOT NULL default '',
  `salle` varchar(100) NOT NULL default '',
  `type` varchar(200) NOT NULL default '',
  `libelle` varchar(250) default NULL,
  `ordre_du_jour` longblob,
  `invitations_zip` longblob,
  `status_cao` char(2) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Commission`
--


-- --------------------------------------------------------

--
-- Structure de la table `Complement`
--

CREATE TABLE IF NOT EXISTS `Complement` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `complement` int(11) NOT NULL default '0',
  `nom_fichier` varchar(80) NOT NULL default '',
  `statut` char(1) NOT NULL default '1',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Complement`
--


-- --------------------------------------------------------

--
-- Structure de la table `CompteMoniteur`
--

CREATE TABLE IF NOT EXISTS `CompteMoniteur` (
  `id` int(22) NOT NULL auto_increment,
  `moniteur_login` varchar(100) NOT NULL default '',
  `moniteur_password` varchar(100) NOT NULL default '',
  `moniteur_mail` varchar(100) NOT NULL default '',
  `moniteur_target` char(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `CompteMoniteur`
--

INSERT INTO `CompteMoniteur` VALUES
(1, 'Atexo', 'mdp08eT@', 'suivi.mpe@atexo.com', '1'),
(2, 'Atexo', '@texo', 'dev.pmi@atexo.biz', '0');

-- --------------------------------------------------------

--
-- Structure de la table `consultation`
--

CREATE TABLE IF NOT EXISTS `consultation` (
  `reference` int(11) NOT NULL auto_increment,
  `reference_utilisateur` varchar(255) NOT NULL default '',
  `categorie` varchar(30) NOT NULL default '0',
  `titre` longtext NOT NULL,
  `resume` longtext NOT NULL,
  `datedebut` varchar(20) NOT NULL default '0000-00-00',
  `datefin` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `datevalidation` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `datemiseenligne` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `type_mise_en_ligne` int(11) NOT NULL default '2',
  `type_procedure` char(1) NOT NULL default '',
  `code_procedure` varchar(15) default NULL,
  `type_envoi` char(1) NOT NULL default '',
  `chiffrement_offre` char(1) NOT NULL default '',
  `signature_offre` char(1) NOT NULL default '',
  `reponse_electronique` char(1) NOT NULL default '0',
  `num_procedure` int(1) NOT NULL default '0',
  `service_id` int(11) NOT NULL default '0',
  `env_candidature` int(1) NOT NULL default '0',
  `env_offre` int(3) NOT NULL default '0',
  `env_anonymat` int(1) NOT NULL default '0',
  `id_type_procedure` int(1) NOT NULL default '0',
  `id_type_avis` int(2) NOT NULL default '0',
  `partial_dce_download` char(1) NOT NULL default '0',
  `tirage_plan` int(11) NOT NULL default '0',
  `tireur_plan` int(11) NOT NULL default '0',
  `tirage_descriptif` mediumtext NOT NULL,
  `lieu_execution` text NOT NULL,
  `envoi_moniteur` varchar(60) NOT NULL default '0',
  `moniteur_pdf_API` longblob NOT NULL,
  `moniteur_pdf_APU` longblob NOT NULL,
  `moniteur_pdf_AAPC` longblob NOT NULL,
  `moniteur_pdf_AM` longblob NOT NULL,
  `moniteur_pdf_RECT` longblob NOT NULL,
  `moniteur_pdf_ANNU` longblob NOT NULL,
  `type_avis` enum('0','1','2') NOT NULL default '0',
  `reference_consultation_init` varchar(250) NOT NULL default '',
  `id_etat_consultation` int(11) NOT NULL default '0',
  `reference_connecteur` varchar(255) NOT NULL default '',
  `datefin_sad` datetime NOT NULL default '0000-00-00 00:00:00',
  `is_sys_acq_dyn` int(11) NOT NULL default '0',
  `cons_statut` char(1) NOT NULL default '0',
  `id_type_validation` int(11) NOT NULL default '2',
  `etat_approbation` enum('0','1') NOT NULL default '0',
  `etat_validation` enum('0','1') NOT NULL default '0',
  `id_approbateur` int(11) NOT NULL default '0',
  `id_valideur` int(11) NOT NULL default '0',
  `service_validation` int(11) NOT NULL default '0',
  `champ_supp_invisible` varchar(100) NOT NULL default '',
  `code_cpv_1` varchar(8) default NULL,
  `code_cpv_2` varchar(8) default NULL,
  `code_cpv_3` varchar(8) default NULL,
  `code_cpv_4` varchar(8) default NULL,
  `libelle_code_cpv_1` varchar(200) default NULL,
  `libelle_code_cpv_2` varchar(200) default NULL,
  `libelle_code_cpv_3` varchar(200) default NULL,
  `libelle_code_cpv_4` varchar(200) default NULL,
  `titre_fr` longtext,
  `titre_en` longtext,
  `titre_es` longtext,
  `titre_su` longtext,
  `titre_du` longtext,
  `titre_cz` longtext,
  `resume_fr` longtext,
  `resume_en` longtext,
  `resume_es` longtext,
  `resume_su` longtext,
  `resume_du` longtext,
  `resume_cz` longtext,
  `publication_europe` enum('0','1') default '0',
  `etat_publication` int(11) NOT NULL default '0',
  `service_associe_id` int(11) NOT NULL default '0',
  `id_createur` int(11) NOT NULL default '0',
  `id_rpa` int(11) NOT NULL default '0',
  `nom_createur` varchar(100) default NULL,
  `prenom_createur` varchar(100) default NULL,
  `signature_acte_engagement` enum('0','1') NOT NULL default '0',
  `poursuivre_affichage` int(3) NOT NULL default '0',
  `detail_consultation` text NOT NULL,
  `archiveMetaDescription` longtext,
  `archiveMetaMotsClef` longtext,
  `archiveIdBlobZip` int(11) default NULL,
  `depouillable_phase_consultation` enum('0','1') NOT NULL default '0',
  `decision_partielle` enum('0','1') NOT NULL default '0',
  `date_fin_affichage` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `consultation_transverse` enum('0','1') NOT NULL default '0',
  `type_decision_a_renseigner` enum('0','1') NOT NULL default '1',
  `type_decision_attribution_marche` enum('0','1') NOT NULL default '1',
  `type_decision_declaration_sans_suite` enum('0','1') NOT NULL default '1',
  `type_decision_declaration_infructueux` enum('0','1') NOT NULL default '1',
  `type_decision_selection_entreprise` enum('0','1') NOT NULL default '1',
  `type_decision_attribution_accord_cadre` enum('0','1') NOT NULL default '1',
  `type_decision_admission_sad` enum('0','1') NOT NULL default '1',
  `type_decision_autre` enum('0','1') NOT NULL default '1',
  `id_archiveur` int(11) default NULL,
  `prenom_nom_agent_telechargement_plis` varchar(255) default NULL,
  `id_agent_telechargement_plis` int(11) NOT NULL default '0',
  `path_telechargement_plis` varchar(255) default NULL,
  `date_telechargement_plis` varchar(20) default NULL,
  `accessibilite_en` char(1) NOT NULL default '0',
  `accessibilite_es` char(1) NOT NULL default '0',
  `organisme_consultation_init` varchar(255) NOT NULL default '',
  `service_validation_intermediaire` int(11) default NULL,
  `date_validation_intermediaire` varchar(20) default NULL,
  `date_mise_en_ligne_calcule` varchar(20) default NULL,
  `consultation_achat_publique` enum('0','1') NOT NULL default '0',
  `accessibilite_cz` enum('0','1') NOT NULL default '0',
  `accessibilite_du` enum('0','1') NOT NULL default '0',
  `accessibilite_fr` enum('0','1') NOT NULL default '0',
  `accessibilite_su` enum('0','1') NOT NULL default '0',
  `alloti` enum('0','1') NOT NULL default '0',
  `numero_phase` int(3) NOT NULL default '0',
  `consultation_migration` enum('0','1') NOT NULL default '0',
  `url_consultation_migration` varchar(100) default NULL,
  PRIMARY KEY  (`reference`),
  KEY `Idx_Consultation_Categorie` (`categorie`),
  KEY `Idx_Consultation_CAP` (`consultation_achat_publique`),
  KEY `Idx_Consultation_SV` (`service_validation`),
  KEY `Idx_Consultation_Datevalid` (`datevalidation`),
  KEY `Idx_Consultation_EA` (`etat_approbation`),
  KEY `Idx_Consultation_DVI` (`date_validation_intermediaire`),
  KEY `Idx_Consultation_SVI` (`service_validation_intermediaire`),
  KEY `Idx_Consultation_RU` (`reference_utilisateur`),
  KEY `Idx_Consultation_Datefin` (`datefin`),
  KEY `Idx_Consultation_ITP` (`id_type_procedure`),
  KEY `Idx_Consultation_ITA` (`id_type_avis`),
  KEY `Idx_Consultation_TML` (`type_mise_en_ligne`),
  KEY `Idx_Consultation_DMLC` (`date_mise_en_ligne_calcule`),
  KEY `Idx_Consultation_Datemiseenligne` (`datemiseenligne`),
  KEY `Idx_Consultation_Code_CPV` (`code_cpv_1`,`code_cpv_2`,`code_cpv_3`,`code_cpv_4`),
  KEY `Idx_Consultation_IEC` (`id_etat_consultation`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `consultation`
--


-- --------------------------------------------------------

--
-- Structure de la table `ConsultationHistoriqueEtat`
--

CREATE TABLE IF NOT EXISTS `ConsultationHistoriqueEtat` (
  `id` int(11) NOT NULL auto_increment,
  `id_agent` int(11) NOT NULL default '0',
  `consultation_ref` int(11) NOT NULL default '0',
  `id_etat` int(11) NOT NULL default '0',
  `date_modification` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `ConsultationHistoriqueEtat`
--


-- --------------------------------------------------------

--
-- Structure de la table `Contrat`
--

CREATE TABLE IF NOT EXISTS `Contrat` (
  `id_contrat` int(11) NOT NULL auto_increment,
  `id_marche` int(11) NOT NULL default '0',
  `id_decision` int(11) NOT NULL default '0',
  `informaions_complementaires` text,
  PRIMARY KEY  (`id_contrat`),
  KEY `id_marche` (`id_marche`),
  KEY `id_decision` (`id_decision`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Contrat`
--


-- --------------------------------------------------------

--
-- Structure de la table `DATEFIN`
--

CREATE TABLE IF NOT EXISTS `DATEFIN` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `datefin` datetime NOT NULL default '0000-00-00 00:00:00',
  `statut` char(1) NOT NULL default '1',
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `DATEFIN`
--


-- --------------------------------------------------------

--
-- Structure de la table `DCE`
--

CREATE TABLE IF NOT EXISTS `DCE` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `dce` int(11) NOT NULL default '0',
  `nom_dce` varchar(80) NOT NULL default '',
  `statut` char(1) NOT NULL default '1',
  `nom_fichier` varchar(80) NOT NULL default '',
  `ancien_fichier` varchar(80) NOT NULL default '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `DCE`
--


-- --------------------------------------------------------

--
-- Structure de la table `decisionEnveloppe`
--

CREATE TABLE IF NOT EXISTS `decisionEnveloppe` (
  `id_decision_enveloppe` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `id_offre` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `decision` enum('0','1') NOT NULL default '0',
  `date_notification` date default NULL,
  `code_postal` varchar(5) default NULL,
  `pme_pmi` int(11) NOT NULL default '0',
  `tranche_budgetaire` int(11) default NULL,
  `montant_marche` varchar(50) default NULL,
  `categorie` int(11) default NULL,
  `objet_marche` text,
  `commentaire` text,
  `fichier_joint` int(11) default NULL,
  `nom_fichier_joint` varchar(100) default NULL,
  `type_enveloppe` enum('1','2') default NULL,
  `numero_marche` varchar(30) default NULL,
  PRIMARY KEY  (`id_decision_enveloppe`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `decisionEnveloppe`
--


-- --------------------------------------------------------

--
-- Structure de la table `DecisionLot`
--

CREATE TABLE IF NOT EXISTS `DecisionLot` (
  `id_decision_lot` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `id_type_decision` int(11) NOT NULL default '0',
  `autre_a_preciser` varchar(100) default NULL,
  `date_decision` date default NULL,
  `commentaire` text,
  PRIMARY KEY  (`id_decision_lot`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `DecisionLot`
--


-- --------------------------------------------------------

--
-- Structure de la table `DecisionPassationConsultation`
--

CREATE TABLE IF NOT EXISTS `DecisionPassationConsultation` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `DecisionPassationConsultation`
--

INSERT INTO `DecisionPassationConsultation` VALUES
(1, 'Poursuite'),
(2, 'Infructueux'),
(3, 'Sans suite');

-- --------------------------------------------------------

--
-- Structure de la table `DecisionPassationMarcheAVenir`
--

CREATE TABLE IF NOT EXISTS `DecisionPassationMarcheAVenir` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `DecisionPassationMarcheAVenir`
--

INSERT INTO `DecisionPassationMarcheAVenir` VALUES
(1, 'Attribué'),
(2, 'Infructueux'),
(3, 'Sans suite');

-- --------------------------------------------------------

--
-- Structure de la table `DestinataireAnnonceJAL`
--

CREATE TABLE IF NOT EXISTS `DestinataireAnnonceJAL` (
  `id` int(11) NOT NULL auto_increment,
  `idJAL` int(11) NOT NULL default '0',
  `idAnnonceJAL` int(11) NOT NULL default '0',
  `date_envoi` varchar(14) NOT NULL default '',
  `date_pub` varchar(14) NOT NULL default '',
  `statut` char(1) NOT NULL default '',
  `accuse` char(1) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `idJAL` (`idJAL`),
  KEY `idAnnonceJAL` (`idAnnonceJAL`),
  KEY `idAnnonceJAL_2` (`idAnnonceJAL`),
  KEY `idAnnonceJAL_3` (`idAnnonceJAL`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `DestinataireAnnonceJAL`
--


-- --------------------------------------------------------

--
-- Structure de la table `DocumentExterne`
--

CREATE TABLE IF NOT EXISTS `DocumentExterne` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `refConsultation` int(11) default NULL,
  `idBlob` int(11) default NULL,
  `idEntreprise` int(11) default NULL,
  `type` enum('1','2','3','4','5','6','7') default NULL,
  `nom` varchar(255) NOT NULL default '',
  `description` longtext NOT NULL,
  `date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`),
  KEY `idBlob` (`idBlob`),
  KEY `refConsultation` (`refConsultation`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `DocumentExterne`
--


-- --------------------------------------------------------

--
-- Structure de la table `Donnee_mesure_avancement`
--

CREATE TABLE IF NOT EXISTS `Donnee_mesure_avancement` (
  `id` int(11) NOT NULL auto_increment,
  `id_mesure` int(11) NOT NULL,
  `aapc_aoo_travaux_nombre_procedures` varchar(20) default NULL,
  `aapc_aoo_travaux_nombre_lots` varchar(20) default NULL,
  `aapc_aoo_travaux_nombre_avis_papier` varchar(20) default NULL,
  `aapc_aoo_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `aapc_aor_travaux_nombre_procedures` varchar(20) default NULL,
  `aapc_aor_travaux_nombre_lots` varchar(20) default NULL,
  `aapc_aor_travaux_nombre_avis_papier` varchar(20) default NULL,
  `aapc_aor_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `aapc_mapa_travaux_nombre_procedures` varchar(20) default NULL,
  `aapc_mapa_travaux_nombre_lots` varchar(20) default NULL,
  `aapc_mapa_travaux_nombre_avis_papier` varchar(20) default NULL,
  `aapc_mapa_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `aapc_aoo_fournitures_nombre_procedures` varchar(20) default NULL,
  `aapc_aoo_fournitures_nombre_lots` varchar(20) default NULL,
  `aapc_aoo_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `aapc_aoo_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `aapc_aor_fournitures_nombre_procedures` varchar(20) default NULL,
  `aapc_aor_fournitures_nombre_lots` varchar(20) default NULL,
  `aapc_aor_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `aapc_aor_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `aapc_mapa_fournitures_nombre_procedures` varchar(20) default NULL,
  `aapc_mapa_fournitures_nombre_lots` varchar(20) default NULL,
  `aapc_mapa_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `aapc_mapa_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `aapc_aoo_services_nombre_procedures` varchar(20) default NULL,
  `aapc_aoo_services_nombre_lots` varchar(20) default NULL,
  `aapc_aoo_services_nombre_avis_papier` varchar(20) default NULL,
  `aapc_aoo_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `aapc_aor_services_nombre_procedures` varchar(20) default NULL,
  `aapc_aor_services_nombre_lots` varchar(20) default NULL,
  `aapc_aor_services_nombre_avis_papier` varchar(20) default NULL,
  `aapc_aor_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `aapc_mapa_services_nombre_procedures` varchar(20) default NULL,
  `aapc_mapa_services_nombre_lots` varchar(20) default NULL,
  `aapc_mapa_services_nombre_avis_papier` varchar(20) default NULL,
  `aapc_mapa_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `dce_aoo_travaux_nombre_procedures` varchar(20) default NULL,
  `dce_aoo_travaux_nombre_lots` varchar(20) default NULL,
  `dce_aoo_travaux_nombre_retraits_papier` varchar(20) default NULL,
  `dce_aoo_travaux_nombre_telechargement` varchar(20) default NULL,
  `dce_aoo_travaux_nombre_retraits_mixte` varchar(20) default NULL,
  `dce_aor_travaux_nombre_procedures` varchar(20) default NULL,
  `dce_aor_travaux_nombre_lots` varchar(20) default NULL,
  `dce_aor_travaux_nombre_retraits_papier` varchar(20) default NULL,
  `dce_aor_travaux_nombre_telechargement` varchar(20) default NULL,
  `dce_aor_travaux_nombre_retraits_mixte` varchar(20) default NULL,
  `dce_mapa_travaux_nombre_procedures` varchar(20) default NULL,
  `dce_mapa_travaux_nombre_lots` varchar(20) default NULL,
  `dce_mapa_travaux_nombre_retraits_papier` varchar(20) default NULL,
  `dce_mapa_travaux_nombre_telechargement` varchar(20) default NULL,
  `dce_mapa_travaux_nombre_retraits_mixte` varchar(20) default NULL,
  `dce_aoo_fournitures_nombre_procedures` varchar(20) default NULL,
  `dce_aoo_fournitures_nombre_lots` varchar(20) default NULL,
  `dce_aoo_fournitures_nombre_retraits_papier` varchar(20) default NULL,
  `dce_aoo_fournitures_nombre_telechargement` varchar(20) default NULL,
  `dce_aoo_fournitures_nombre_retraits_mixte` varchar(20) default NULL,
  `dce_aor_fournitures_nombre_procedures` varchar(20) default NULL,
  `dce_aor_fournitures_nombre_lots` varchar(20) default NULL,
  `dce_aor_fournitures_nombre_retraits_papier` varchar(20) default NULL,
  `dce_aor_fournitures_nombre_telechargement` varchar(20) default NULL,
  `dce_aor_fournitures_nombre_retraits_mixte` varchar(20) default NULL,
  `dce_mapa_fournitures_nombre_procedures` varchar(20) default NULL,
  `dce_mapa_fournitures_nombre_lots` varchar(20) default NULL,
  `dce_mapa_fournitures_nombre_retraits_papier` varchar(20) default NULL,
  `dce_mapa_fournitures_nombre_telechargement` varchar(20) default NULL,
  `dce_mapa_fournitures_nombre_retraits_mixte` varchar(20) default NULL,
  `dce_aoo_services_nombre_procedures` varchar(20) default NULL,
  `dce_aoo_services_nombre_lots` varchar(20) default NULL,
  `dce_aoo_services_nombre_retraits_papier` varchar(20) default NULL,
  `dce_aoo_services_nombre_telechargement` varchar(20) default NULL,
  `dce_aoo_services_nombre_retraits_mixte` varchar(20) default NULL,
  `dce_aor_services_nombre_procedures` varchar(20) default NULL,
  `dce_aor_services_nombre_lots` varchar(20) default NULL,
  `dce_aor_services_nombre_retraits_papier` varchar(20) default NULL,
  `dce_aor_services_nombre_telechargement` varchar(20) default NULL,
  `dce_aor_services_nombre_retraits_mixte` varchar(20) default NULL,
  `dce_mapa_services_nombre_procedures` varchar(20) default NULL,
  `dce_mapa_services_nombre_lots` varchar(20) default NULL,
  `dce_mapa_services_nombre_retraits_papier` varchar(20) default NULL,
  `dce_mapa_services_nombre_telechargement` varchar(20) default NULL,
  `dce_mapa_services_nombre_retraits_mixte` varchar(20) default NULL,
  `candidature_aoo_travaux_nombre_procedures` varchar(20) default NULL,
  `candidature_aoo_travaux_nombre_lots` varchar(20) default NULL,
  `candidature_aoo_travaux_nombre_avis_papier` varchar(20) default NULL,
  `candidature_aoo_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `candidature_aor_travaux_nombre_procedures` varchar(20) default NULL,
  `candidature_aor_travaux_nombre_lots` varchar(20) default NULL,
  `candidature_aor_travaux_nombre_avis_papier` varchar(20) default NULL,
  `candidature_aor_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `candidature_mapa_travaux_nombre_procedures` varchar(20) default NULL,
  `candidature_mapa_travaux_nombre_lots` varchar(20) default NULL,
  `candidature_mapa_travaux_nombre_avis_papier` varchar(20) default NULL,
  `candidature_mapa_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `candidature_aoo_fournitures_nombre_procedures` varchar(20) default NULL,
  `candidature_aoo_fournitures_nombre_lots` varchar(20) default NULL,
  `candidature_aoo_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `candidature_aoo_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `candidature_aor_fournitures_nombre_procedures` varchar(20) default NULL,
  `candidature_aor_fournitures_nombre_lots` varchar(20) default NULL,
  `candidature_aor_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `candidature_aor_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `candidature_mapa_fournitures_nombre_procedures` varchar(20) default NULL,
  `candidature_mapa_fournitures_nombre_lots` varchar(20) default NULL,
  `candidature_mapa_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `candidature_mapa_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `candidature_aoo_services_nombre_procedures` varchar(20) default NULL,
  `candidature_aoo_services_nombre_lots` varchar(20) default NULL,
  `candidature_aoo_services_nombre_avis_papier` varchar(20) default NULL,
  `candidature_aoo_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `candidature_aor_services_nombre_procedures` varchar(20) default NULL,
  `candidature_aor_services_nombre_lots` varchar(20) default NULL,
  `candidature_aor_services_nombre_avis_papier` varchar(20) default NULL,
  `candidature_aor_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `candidature_mapa_services_nombre_procedures` varchar(20) default NULL,
  `candidature_mapa_services_nombre_lots` varchar(20) default NULL,
  `candidature_mapa_services_nombre_avis_papier` varchar(20) default NULL,
  `candidature_mapa_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `offres_aoo_travaux_nombre_procedures` varchar(20) default NULL,
  `offres_aoo_travaux_nombre_lots` varchar(20) default NULL,
  `offres_aoo_travaux_nombre_avis_papier` varchar(20) default NULL,
  `offres_aoo_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `offres_aor_travaux_nombre_procedures` varchar(20) default NULL,
  `offres_aor_travaux_nombre_lots` varchar(20) default NULL,
  `offres_aor_travaux_nombre_avis_papier` varchar(20) default NULL,
  `offres_aor_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `offres_mapa_travaux_nombre_procedures` varchar(20) default NULL,
  `offres_mapa_travaux_nombre_lots` varchar(20) default NULL,
  `offres_mapa_travaux_nombre_avis_papier` varchar(20) default NULL,
  `offres_mapa_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `offres_aoo_fournitures_nombre_procedures` varchar(20) default NULL,
  `offres_aoo_fournitures_nombre_lots` varchar(20) default NULL,
  `offres_aoo_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `offres_aoo_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `offres_aor_fournitures_nombre_procedures` varchar(20) default NULL,
  `offres_aor_fournitures_nombre_lots` varchar(20) default NULL,
  `offres_aor_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `offres_aor_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `offres_mapa_fournitures_nombre_procedures` varchar(20) default NULL,
  `offres_mapa_fournitures_nombre_lots` varchar(20) default NULL,
  `offres_mapa_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `offres_mapa_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `offres_aoo_services_nombre_procedures` varchar(20) default NULL,
  `offres_aoo_services_nombre_lots` varchar(20) default NULL,
  `offres_aoo_services_nombre_avis_papier` varchar(20) default NULL,
  `offres_aoo_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `offres_aor_services_nombre_procedures` varchar(20) default NULL,
  `offres_aor_services_nombre_lots` varchar(20) default NULL,
  `offres_aor_services_nombre_avis_papier` varchar(20) default NULL,
  `offres_aor_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `offres_mapa_services_nombre_procedures` varchar(20) default NULL,
  `offres_mapa_services_nombre_lots` varchar(20) default NULL,
  `offres_mapa_services_nombre_avis_papier` varchar(20) default NULL,
  `offres_mapa_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `notifications_aoo_travaux_nombre_procedures` varchar(20) default NULL,
  `notifications_aoo_travaux_nombre_lots` varchar(20) default NULL,
  `notifications_aoo_travaux_nombre_avis_papier` varchar(20) default NULL,
  `notifications_aoo_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `notifications_aor_travaux_nombre_procedures` varchar(20) default NULL,
  `notifications_aor_travaux_nombre_lots` varchar(20) default NULL,
  `notifications_aor_travaux_nombre_avis_papier` varchar(20) default NULL,
  `notifications_aor_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `notifications_mapa_travaux_nombre_procedures` varchar(20) default NULL,
  `notifications_mapa_travaux_nombre_lots` varchar(20) default NULL,
  `notifications_mapa_travaux_nombre_avis_papier` varchar(20) default NULL,
  `notifications_mapa_travaux_nombre_avis_dematerialise` varchar(20) default NULL,
  `notifications_aoo_fournitures_nombre_procedures` varchar(20) default NULL,
  `notifications_aoo_fournitures_nombre_lots` varchar(20) default NULL,
  `notifications_aoo_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `notifications_aoo_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `notifications_aor_fournitures_nombre_procedures` varchar(20) default NULL,
  `notifications_aor_fournitures_nombre_lots` varchar(20) default NULL,
  `notifications_aor_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `notifications_aor_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `notifications_mapa_fournitures_nombre_procedures` varchar(20) default NULL,
  `notifications_mapa_fournitures_nombre_lots` varchar(20) default NULL,
  `notifications_mapa_fournitures_nombre_avis_papier` varchar(20) default NULL,
  `notifications_mapa_fournitures_nombre_avis_dematerialise` varchar(20) default NULL,
  `notifications_aoo_services_nombre_procedures` varchar(20) default NULL,
  `notifications_aoo_services_nombre_lots` varchar(20) default NULL,
  `notifications_aoo_services_nombre_avis_papier` varchar(20) default NULL,
  `notifications_aoo_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `notifications_aor_services_nombre_procedures` varchar(20) default NULL,
  `notifications_aor_services_nombre_lots` varchar(20) default NULL,
  `notifications_aor_services_nombre_avis_papier` varchar(20) default NULL,
  `notifications_aor_services_nombre_avis_dematerialise` varchar(20) default NULL,
  `notifications_mapa_services_nombre_procedures` varchar(20) default NULL,
  `notifications_mapa_services_nombre_lots` varchar(20) default NULL,
  `notifications_mapa_services_nombre_avis_papier` varchar(20) default NULL,
  `notifications_mapa_services_nombre_avis_dematerialise` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Donnee_mesure_avancement`
--


-- --------------------------------------------------------


--
-- Structure de la table `Echange`
--

CREATE TABLE IF NOT EXISTS `Echange` (
  `id` int(11) NOT NULL auto_increment,
  `objet` varchar(255) NOT NULL default '',
  `corps` text NOT NULL,
  `expediteur` varchar(100) NOT NULL default '',
  `id_createur` int(11) default '0',
  `ref_consultation` int(11) NOT NULL default '0',
  `option_envoi` int(11) NOT NULL default '0',
  `date_message` varchar(25) NOT NULL default '',
  `format` int(11) NOT NULL default '0',
  `id_action_declencheur` int(11) NOT NULL default '0',
  `status` int(2) NOT NULL default '0',
  `service_id` int(11) default '0',
  `email_expediteur` varchar(100) NOT NULL default '',
  `id_type_message` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Echange`
--


-- --------------------------------------------------------

--
-- Structure de la table `EchangeActionDeclencheur`
--

CREATE TABLE IF NOT EXISTS `EchangeActionDeclencheur` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EchangeActionDeclencheur`
--


-- --------------------------------------------------------

--
-- Structure de la table `EchangeDestinataire`
--

CREATE TABLE IF NOT EXISTS `EchangeDestinataire` (
  `id` int(11) NOT NULL auto_increment,
  `id_echange` int(11) NOT NULL default '0',
  `mail_destinataire` varchar(255) NOT NULL default '',
  `ar` enum('0','1') NOT NULL default '0',
  `date_ar` varchar(25) NOT NULL default '',
  `uid` varchar(32) NOT NULL default '',
  `type_ar` int(11) NOT NULL default '0',
  `id_inscrit` int(11) NOT NULL default '0',
  `id_entreprise` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id_echange` (`id_echange`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EchangeDestinataire`
--


-- --------------------------------------------------------

--
-- Structure de la table `EchangeFormat`
--

CREATE TABLE IF NOT EXISTS `EchangeFormat` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `EchangeFormat`
--

INSERT INTO `EchangeFormat` VALUES
(1, 'Echange plate-forme'),
(2, 'Courriel hors plate-forme'),
(3, 'Courrier recommandé'),
(4, 'Courrier simple'),
(5, 'Télécopie'),
(6, 'Autre');

-- --------------------------------------------------------

--
-- Structure de la table `EchangePieceJointe`
--

CREATE TABLE IF NOT EXISTS `EchangePieceJointe` (
  `id` int(11) NOT NULL auto_increment,
  `id_message` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `piece` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(25) NOT NULL default '',
  `taille` varchar(25) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id_message` (`id_message`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EchangePieceJointe`
--


-- --------------------------------------------------------

--
-- Structure de la table `EchangeTypeAR`
--

CREATE TABLE IF NOT EXISTS `EchangeTypeAR` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `EchangeTypeAR`
--

INSERT INTO `EchangeTypeAR` VALUES
(1, 'Sans objet'),
(2, 'Non retiré'),
(3, 'Date et heure');

-- --------------------------------------------------------

--
-- Structure de la table `EchangeTypeMessage`
--

CREATE TABLE IF NOT EXISTS `EchangeTypeMessage` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) NOT NULL default '',
  `corps` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `EchangeTypeMessage`
--

INSERT INTO `EchangeTypeMessage` VALUES
(1, 'Courrier de notification', 'Votre entreprise a été déclarée attributaire de la consultation citée en référence.\nVeuillez-trouver à titre de notification le marché signé par le Représentant du Pouvoir Adjudicateur.\nL''Accusé de réception de ce message vaut notification officielle du marché.\n'),
(2, 'Courrier de rejet', 'Nous vous remercions d''avoir répondu à la consultation citée en référence.\nNous sommes toutefois au regret de vous annoncer que votre réponse n''a pas été retenue par le Représentant du Pouvoir Adjudicateur.\nCordialement,\n'),
(3, 'Avertissement - Modification de consultation', 'La consultation citée en référence a été modifiée.\nLes éléments modifiés sont : [à préciser au cas par cas]\nCordialement,\n'),
(4, 'Courrier libre', '\nTEXTE COURRIER LIBRE\n'),
(5, 'Courrier de demande de complément', 'Nous vous remercions d''avoir répondu à la consultation citée en référence.\nAprès analyse, il vous est demandé d''apporter les précisions suivantes : [à préciser au cas par cas].\nLa réponse à ces questions peut se faire via l''application, à partir de la page d''accès à cette demande de complément.\nIl est nécessaire de disposer d''un Compte entreprise sur l''application pour accéder à cette réponse.\nCordialement,');

-- --------------------------------------------------------

--
-- Structure de la table `enchere`
--

CREATE TABLE IF NOT EXISTS `enchere` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `num_env` int(11) NOT NULL default '0',
  `commentaire` text,
  `date_debut` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_fin` datetime NOT NULL default '0000-00-00 00:00:00',
  `nom_critere_1` varchar(70) default NULL,
  `nom_critere_2` varchar(70) default NULL,
  `nom_critere_3` varchar(70) default NULL,
  `val_critere_1` float default NULL,
  `val_critere_2` float default NULL,
  `val_critere_3` float default NULL,
  `unite_critere_1` varchar(10) default NULL,
  `unite_critere_2` varchar(10) default NULL,
  `unite_critere_3` varchar(10) default NULL,
  `orientation_critere_1` int(11) NOT NULL default '1',
  `orientation_critere_2` int(11) NOT NULL default '1',
  `orientation_critere_3` int(11) NOT NULL default '1',
  `fichier_accept` longblob,
  `date_creation` datetime NOT NULL default '0000-00-00 00:00:00',
  `admin_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`consultation_ref`,`num_env`,`date_creation`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `enchere`
--


-- --------------------------------------------------------

--
-- Structure de la table `EnchereEntreprisePmi`
--

CREATE TABLE IF NOT EXISTS `EnchereEntreprisePmi` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `nom` varchar(255) NOT NULL default '',
  `numeroAnonyme` int(11) unsigned default NULL,
  `email` varchar(255) NOT NULL default '',
  `mdp` varchar(255) NOT NULL default '',
  `noteTechnique` double default NULL,
  `idEntreprise` int(10) unsigned default NULL,
  `datePing` datetime NOT NULL default '0000-00-00 00:00:00',
  `nomAgentConnecte` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `Entreprise_FKIndex1` (`idEnchere`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EnchereEntreprisePmi`
--


-- --------------------------------------------------------

--
-- Structure de la table `EnchereFichierDetail`
--

CREATE TABLE IF NOT EXISTS `EnchereFichierDetail` (
  `id` int(11) NOT NULL auto_increment,
  `fichier_detail` longblob NOT NULL,
  `horodatage` longblob NOT NULL,
  `nom_fichier` varchar(100) NOT NULL default '',
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `admin_id` int(11) NOT NULL default '0',
  `id_enchere` int(11) NOT NULL default '0',
  `statut` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `id_enchere` (`id_enchere`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EnchereFichierDetail`
--


-- --------------------------------------------------------

--
-- Structure de la table `EnchereOffre`
--

CREATE TABLE IF NOT EXISTS `EnchereOffre` (
  `id` int(11) NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `idEnchereEntreprise` int(10) unsigned NOT NULL default '0',
  `date` datetime default '0000-00-00 00:00:00',
  `valeurTIC` double default NULL,
  `valeurTC` double default NULL,
  `valeurNETC` double default NULL,
  `valeurNGC` double default NULL,
  `rang` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `EnchereOffre_FKIndex1` (`idEnchereEntreprise`),
  KEY `EnchereOffre_FKIndex2` (`idEnchere`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EnchereOffre`
--


-- --------------------------------------------------------

--
-- Structure de la table `EnchereOffreReference`
--

CREATE TABLE IF NOT EXISTS `EnchereOffreReference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchereOffre` int(11) NOT NULL default '0',
  `idEnchereReference` int(10) unsigned NOT NULL default '0',
  `valeur` double NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `EnchereOffreReference_FKIndex1` (`idEnchereOffre`),
  KEY `EnchereOffreReference_FKIndex2` (`idEnchereReference`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EnchereOffreReference`
--


-- --------------------------------------------------------

--
-- Structure de la table `EncherePmi`
--

CREATE TABLE IF NOT EXISTS `EncherePmi` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `refConsultation` int(10) unsigned default NULL,
  `idEntiteeAssociee` int(11) default NULL,
  `referenceUtilisateur` varchar(45) default NULL,
  `auteur` varchar(255) default NULL,
  `idLot` int(10) unsigned default NULL,
  `objet` longtext,
  `dateDebut` datetime default '0000-00-00 00:00:00',
  `dateFin` datetime default '0000-00-00 00:00:00',
  `dateSuspension` datetime default '0000-00-00 00:00:00',
  `delaiProlongation` int(11) default NULL,
  `commentaire` longtext,
  `meilleureEnchereObligatoire` enum('0','1') NOT NULL default '0',
  `typeBaremeNETC` enum('1','2','3','4') NOT NULL default '1',
  `typeBaremeEnchereGlobale` enum('1','2') NOT NULL default '1',
  `meilleurNoteHaute` enum('0','1') NOT NULL default '0',
  `nbrCandidatsVisible` enum('0','1') NOT NULL default '1',
  `listeCandidatsVisible` enum('0','1') NOT NULL default '1',
  `rangVisible` enum('0','1') NOT NULL default '1',
  `meilleureOffreVisible` enum('0','1') NOT NULL default '1',
  `montantReserve` double default NULL,
  `noteMaxBaremeRelatif` double default NULL,
  `coeffA` double default NULL,
  `coeffB` double default NULL,
  `coeffC` double default NULL,
  `mail` longtext,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `referenceUtilisateur` (`referenceUtilisateur`),
  KEY `Enchere_V2_FKIndex1` (`refConsultation`),
  KEY `Enchere_FKIndex2` (`idEntiteeAssociee`),
  KEY `idEntiteeAssociee` (`idEntiteeAssociee`),
  KEY `idEntiteeAssociee_2` (`idEntiteeAssociee`),
  KEY `idEntiteeAssociee_3` (`idEntiteeAssociee`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EncherePmi`
--


-- --------------------------------------------------------

--
-- Structure de la table `EnchereReference`
--

CREATE TABLE IF NOT EXISTS `EnchereReference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `libelle` varchar(255) NOT NULL default '',
  `quantite` double default NULL,
  `isMontant` enum('0','1') NOT NULL default '1',
  `unite` varchar(20) default NULL,
  `pasMin` double default '0',
  `pasMax` double default NULL,
  `valeurReference` double default NULL,
  `valeurDepartCommune` enum('0','1') NOT NULL default '1',
  `valeurDepart` double default NULL,
  `typeBaremeReference` enum('1','2','3') default NULL,
  `ponderationNoteReference` double default '1',
  `noteMaxBaremeRelatif` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `EnchereReference_FKIndex1` (`idEnchere`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EnchereReference`
--

-- --------------------------------------------------------

--
-- Structure de la table `EnchereTrancheBaremeReference`
--

CREATE TABLE IF NOT EXISTS `EnchereTrancheBaremeReference` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idReference` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `TrancheBaremeReference_FKIndex1` (`idReference`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EnchereTrancheBaremeReference`
--



-- --------------------------------------------------------

--
-- Structure de la table `EnchereTranchesBaremeNETC`
--

CREATE TABLE IF NOT EXISTS `EnchereTranchesBaremeNETC` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchere` int(10) unsigned NOT NULL default '0',
  `borneInf` double default NULL,
  `borneSup` double default NULL,
  `note` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `EnchereTranchesBaremeNETC_FKIndex1` (`idEnchere`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EnchereTranchesBaremeNETC`
--


-- --------------------------------------------------------

--
-- Structure de la table `EnchereValeursInitiales`
--

CREATE TABLE IF NOT EXISTS `EnchereValeursInitiales` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idEnchereEntreprise` int(10) unsigned NOT NULL default '0',
  `idEnchereReference` int(10) unsigned NOT NULL default '0',
  `valeur` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `EnchereValeursInitiales_FKIndex1` (`idEnchereEntreprise`),
  KEY `EnchereValeursInitiales_FKIndex2` (`idEnchereReference`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `EnchereValeursInitiales`
--


-- --------------------------------------------------------

--
-- Structure de la table `enchere_entreprise`
--

CREATE TABLE IF NOT EXISTS `enchere_entreprise` (
  `id_enchere_entreprise` int(11) NOT NULL auto_increment,
  `mdp` varchar(80) NOT NULL default '',
  `entreprise_id` int(11) default NULL,
  `conditions` text,
  `sig_conditions` longblob,
  `email_entreprise` varchar(100) default NULL,
  `nom_entreprise` varchar(200) default NULL,
  `id_entreprise_selectionne` int(11) NOT NULL default '0',
  `id_enchere` int(11) NOT NULL default '0',
  `id_inscrit` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id_enchere_entreprise`),
  UNIQUE KEY `mdp` (`mdp`),
  KEY `id_enchere` (`id_enchere`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `enchere_entreprise`
--


-- --------------------------------------------------------

--
-- Structure de la table `Enveloppe`
--

CREATE TABLE IF NOT EXISTS `Enveloppe` (
  `id_enveloppe_electro` int(22) NOT NULL auto_increment,
  `offre_id` int(22) NOT NULL default '0',
  `champs_optionnels` longblob NOT NULL,
  `fichier` int(11) NOT NULL default '0',
  `supprime` char(1) NOT NULL default '0',
  `cryptage` char(1) NOT NULL default '1',
  `nom_fichier` varchar(255) NOT NULL default '',
  `hash` varchar(40) NOT NULL default '',
  `type_env` int(1) NOT NULL default '0',
  `sous_pli` int(3) NOT NULL default '0',
  `attribue` char(1) NOT NULL default '0',
  `dateheure_ouverture` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `agent_id_ouverture` int(11) default NULL,
  `agent_id_ouverture2` int(11) default NULL,
  `donnees_ouverture` longblob NOT NULL,
  `horodatage_donnees_ouverture` longblob NOT NULL,
  `statut_enveloppe` int(2) NOT NULL default '1',
  `agent_telechargement` int(11) default NULL,
  `date_telechargement` varchar(20) default NULL,
  `repertoire_telechargement` varchar(100) default NULL,
  `nom_agent_ouverture` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id_enveloppe_electro`),
  KEY `offre_id` (`offre_id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Enveloppe`
--


-- --------------------------------------------------------

--
-- Structure de la table `Enveloppe_papier`
--

CREATE TABLE IF NOT EXISTS `Enveloppe_papier` (
  `id_enveloppe_papier` int(11) NOT NULL auto_increment,
  `offre_papier_id` int(22) NOT NULL default '0',
  `statut` int(11) NOT NULL default '0',
  `supprime` char(1) NOT NULL default '0',
  `cryptage` char(1) NOT NULL default '1',
  `is_send` int(1) NOT NULL default '1',
  `type_env` int(1) NOT NULL default '0',
  `sous_pli` int(3) NOT NULL default '0',
  `champs_optionnels` longtext,
  `agent_id_ouverture` int(11) NOT NULL default '0',
  `dateheure_ouverture` datetime NOT NULL default '0000-00-00 00:00:00',
  `statut_enveloppe` int(2) NOT NULL default '1',
  `enveloppe_postule` enum('0','1') NOT NULL default '1',
  `nom_agent_ouverture` varchar(100) default NULL,
  PRIMARY KEY  (`id_enveloppe_papier`),
  KEY `offre_papier_id` (`offre_papier_id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Enveloppe_papier`
--


-- --------------------------------------------------------

--
-- Structure de la table `Fcsp_Lieu`
--

CREATE TABLE IF NOT EXISTS `Fcsp_Lieu` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Fcsp_Lieu`
--


-- --------------------------------------------------------

--
-- Structure de la table `Fcsp_Mandataire`
--

CREATE TABLE IF NOT EXISTS `Fcsp_Mandataire` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL
) ENGINE=InnoDB ;

--
-- Contenu de la table `Fcsp_Mandataire`
--

INSERT INTO `Fcsp_Mandataire` VALUES
(1, 'Mandataire UL - SOGEFI'),
(2, 'Mandataire UO - SIPARC'),
(3, 'Mandataire UO - RIALI'),
(1, 'Mandataire UL - SOGEFI'),
(2, 'Mandataire UO - SIPARC'),
(3, 'Mandataire UO - RIALI'),
(1, 'Mandataire UL - SOGEFI'),
(2, 'Mandataire UO - SIPARC'),
(3, 'Mandataire UO - RIALI'),
(1, 'Mandataire UL - SOGEFI'),
(2, 'Mandataire UO - SIPARC'),
(3, 'Mandataire UO - RIALI'),
(1, 'Mandataire UL - SOGEFI'),
(2, 'Mandataire UO - SIPARC'),
(3, 'Mandataire UO - RIALI'),
(1, 'Mandataire UL - SOGEFI'),
(2, 'Mandataire UO - SIPARC'),
(3, 'Mandataire UO - RIALI'),
(1, 'Mandataire UL - SOGEFI'),
(2, 'Mandataire UO - SIPARC'),
(3, 'Mandataire UO - RIALI'),
(1, 'Mandataire UL - SOGEFI'),
(2, 'Mandataire UO - SIPARC'),
(3, 'Mandataire UO - RIALI'),
(1, 'Mandataire UL - SOGEFI'),
(2, 'Mandataire UO - SIPARC'),
(3, 'Mandataire UO - RIALI');

-- --------------------------------------------------------

--
-- Structure de la table `Fcsp_unite`
--

CREATE TABLE IF NOT EXISTS `Fcsp_unite` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Fcsp_unite`
--


-- --------------------------------------------------------

--
-- Structure de la table `fichierEnveloppe`
--

CREATE TABLE IF NOT EXISTS `fichierEnveloppe` (
  `id_fichier` int(11) NOT NULL auto_increment,
  `id_enveloppe` int(11) NOT NULL default '0',
  `type_fichier` char(3) NOT NULL default '',
  `num_ordre_fichier` int(5) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `taille_fichier` varchar(50) NOT NULL default '',
  `signature_fichier` text NOT NULL,
  `hash` text NOT NULL,
  `verification_certificat` varchar(5) NOT NULL default '',
  `id_blob` int(11) default NULL,
  PRIMARY KEY  (`id_fichier`),
  KEY `id_enveloppe` (`id_enveloppe`),
  KEY `id_enveloppe_2` (`id_enveloppe`),
  KEY `id_enveloppe_3` (`id_enveloppe`),
  KEY `id_enveloppe_4` (`id_enveloppe`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `fichierEnveloppe`
--


-- --------------------------------------------------------

--
-- Structure de la table `FichierLot`
--

CREATE TABLE IF NOT EXISTS `FichierLot` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` varchar(255) NOT NULL default '',
  `lot` int(11) NOT NULL default '0',
  `fichier_pli` int(11) NOT NULL default '0',
  `nom_fichier_pli` varchar(80) NOT NULL default '',
  `statut` char(1) NOT NULL default '1',
  `nom_fichier` varchar(80) NOT NULL default '',
  `ancien_fichier` varchar(80) NOT NULL default '',
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `FichierLot`
--


-- --------------------------------------------------------

--
-- Structure de la table `Helios_cosignature`
--

CREATE TABLE IF NOT EXISTS `Helios_cosignature` (
  `id_fichier` int(20) NOT NULL,
  `signature_acte` varchar(20) NOT NULL,
  `horodatage_acte` longblob NOT NULL,
  `untrusteddate_acte` varchar(20) NOT NULL,
  `taille_acte` varchar(20) NOT NULL,
  `id_Blob` varchar(20) NOT NULL,
  `idEnveloppe` INT( 20 ) NOT NULL,
  PRIMARY KEY  (`id_fichier`),
  KEY `id_fichier` (`id_fichier`),
  KEY `id_fichier_2` (`id_fichier`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Helios_cosignature`
--


-- --------------------------------------------------------

--
-- Structure de la table `Helios_piece_publicite`
--

CREATE TABLE IF NOT EXISTS `Helios_piece_publicite` (
  `id` int(11) NOT NULL auto_increment,
  `nom_fichier` varchar(100) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '1',
  `consultation_ref` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Helios_piece_publicite`
--


-- --------------------------------------------------------

--
-- Structure de la table `Helios_pv_consultation`
--

CREATE TABLE IF NOT EXISTS `Helios_pv_consultation` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '0',
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Helios_pv_consultation`
--


-- --------------------------------------------------------

--
-- Structure de la table `Helios_rapport_prefet`
--

CREATE TABLE IF NOT EXISTS `Helios_rapport_prefet` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '0',
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Helios_rapport_prefet`
--


-- --------------------------------------------------------

--
-- Structure de la table `Helios_tableau_ar`
--

CREATE TABLE IF NOT EXISTS `Helios_tableau_ar` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_fichier` varchar(100) NOT NULL default '',
  `description` mediumtext,
  `fichier` int(11) NOT NULL default '0',
  `horodatage` longblob NOT NULL,
  `untrusteddate` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille` varchar(20) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Helios_tableau_ar`
--


-- --------------------------------------------------------

--
-- Structure de la table `Helios_teletransmission`
--

CREATE TABLE IF NOT EXISTS `Helios_teletransmission` (
  `id` int(11) NOT NULL auto_increment,
  `shown_id` varchar(20) NOT NULL default '',
  `id_agent_creation` int(11) NOT NULL default '0',
  `consultation_ref` int(11) default NULL,
  `publicite_cons` char(20) default NULL,
  `dce_items` varchar(255) default NULL,
  `ids_pvs_cao` varchar(255) default NULL,
  `ids_rapports_prefet` varchar(255) default NULL,
  `deliberation_mapa` char(1) default NULL,
  `tableau_ar` char(1) default NULL,
  `date_creation` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `date_envoi` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `id_agent_envoi` int(11) NOT NULL default '0',
  `nom_piecej1` varchar(250) NOT NULL default '',
  `piecej1` int(11) NOT NULL default '0',
  `horodatage_piecej1` longblob NOT NULL,
  `untrusteddate_piecej1` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille_piecej1` varchar(20) NOT NULL default '',
  `nom_piecej2` varchar(250) NOT NULL default '',
  `piecej2` int(11) NOT NULL default '0',
  `horodatage_piecej2` longblob NOT NULL,
  `untrusteddate_piecej2` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `taille_piecej2` varchar(20) NOT NULL default '',
  `piecej1_desc` mediumtext NOT NULL,
  `piecej2_desc` mediumtext NOT NULL,
  `fichier_xml` longblob NOT NULL,
  `nom_dossierzip` varchar(250) NOT NULL default '',
  `taille_dossierzip` varchar(250) NOT NULL default '',
  `objet_libre` varchar(250) NOT NULL default '',
  `commentaire_libre` varchar(250) NOT NULL default '',
  `sig_piece_principale` longblob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Helios_teletransmission`
--


-- --------------------------------------------------------

--
-- Structure de la table `Helios_teletransmission_lot`
--

CREATE TABLE IF NOT EXISTS `Helios_teletransmission_lot` (
  `id_teletransmission` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `numero_marche` varchar(250) NOT NULL default '',
  `ids_env_actes_engagements` varchar(255) NOT NULL default '',
  `ids_enveloppes_items` varchar(255) NOT NULL default '',
  `ids_docs_acceptes` varchar(255) NOT NULL default '',
  `ids_msg_notif` varchar(255) NOT NULL default '',
  `lot_in_transmission` char(1) NOT NULL default '',
  `statut_envoi` int(11) NOT NULL default '1',
  `id_zip_send` varchar(40) default NULL,
  `nom_lettre_commande` varchar(100) default NULL,
  `lettre_commande` int(20) default NULL,
  `horodatage` varchar(20) NOT NULL default '',
  `untrusteddate` varchar(20) NOT NULL default '',
  `taille` varchar(20) NOT NULL default '',
  `valider_comptable` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id_teletransmission`,`lot`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Helios_teletransmission_lot`
--


-- --------------------------------------------------------

--
-- Structure de la table `InterneConsultation`
--

CREATE TABLE IF NOT EXISTS `InterneConsultation` (
  `id` int(11) NOT NULL auto_increment,
  `interne_id` int(11) NOT NULL default '0',
  `consultation_ref` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `interne_id` (`interne_id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `InterneConsultation`
--


-- --------------------------------------------------------

--
-- Structure de la table `InterneConsultationSuiviSeul`
--

CREATE TABLE IF NOT EXISTS `InterneConsultationSuiviSeul` (
  `id` int(11) NOT NULL auto_increment,
  `interne_id` int(11) NOT NULL default '0',
  `consultation_ref` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `interne_id` (`interne_id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `InterneConsultationSuiviSeul`
--


-- --------------------------------------------------------

--
-- Structure de la table `Intervenant_Externe`
--

CREATE TABLE IF NOT EXISTS `Intervenant_Externe` (
  `id` int(11) NOT NULL auto_increment,
  `nom` varchar(30) NOT NULL default '',
  `prenom` varchar(30) NOT NULL default '',
  `organisation` varchar(30) NOT NULL default '',
  `fonction` varchar(200) NOT NULL default '',
  `adresse` varchar(255) NOT NULL default '',
  `mail` varchar(100) NOT NULL default '',
  `type_invitation` int(1) NOT NULL default '1',
  `civilite` varchar(255) default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Intervenant_Externe`
--


-- --------------------------------------------------------

--
-- Structure de la table `Intervenant_Externe_Commission`
--

CREATE TABLE IF NOT EXISTS `Intervenant_Externe_Commission` (
  `id_commission` int(11) NOT NULL default '0',
  `id_intervenant_externe` int(11) NOT NULL default '0',
  `convocation` longblob,
  `convoc_send` char(1) NOT NULL default '0',
  `nom_convoc` varchar(255) default NULL,
  `contenu_envoi` text,
  `nom_fichier_envoye` varchar(255) default NULL,
  `fichier_envoye` longblob,
  `date_envoi` datetime default NULL,
  PRIMARY KEY  (`id_commission`,`id_intervenant_externe`),
  KEY `id_intervenant_externe` (`id_intervenant_externe`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Intervenant_Externe_Commission`
--


-- --------------------------------------------------------

--
-- Structure de la table `Intervenant_Ordre_Du_Jour`
--

CREATE TABLE IF NOT EXISTS `Intervenant_Ordre_Du_Jour` (
  `id` int(11) NOT NULL auto_increment,
  `id_ordre_du_jour` int(11) NOT NULL default '0',
  `id_intervenant` int(11) NOT NULL default '0',
  `id_agent` int(11) NOT NULL default '0',
  `convocation` longblob,
  `convoc_send` char(1) NOT NULL default '0',
  `nom_convoc` varchar(255) default NULL,
  `contenu_envoi` text,
  `nom_fichier_envoye` varchar(255) default NULL,
  `fichier_envoye` longblob,
  `date_envoi` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `id_ordre_du_jour` (`id_ordre_du_jour`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Intervenant_Ordre_Du_Jour`
--


-- --------------------------------------------------------

--
-- Structure de la table `JAL`
--

CREATE TABLE IF NOT EXISTS `JAL` (
  `id` int(11) NOT NULL auto_increment,
  `id_service` int(11) NOT NULL default '0',
  `nom` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `email_ar` varchar(100) NOT NULL default '',
  `telecopie` varchar(20) NOT NULL default '',
  `information_facturation` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `JAL`
--


-- --------------------------------------------------------

--
-- Structure de la table `Marche`
--

CREATE TABLE IF NOT EXISTS `Marche` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `idMarcheTrancheBudgetaire` int(11) unsigned default '0',
  `numeroMarcheAnnee` int(11) unsigned default '0',
  `idService` int(11) default '0',
  `nomAttributaire` varchar(255) default NULL,
  `dateNotification` date NOT NULL default '0000-00-00',
  `codePostal` varchar(20) default NULL,
  `natureMarche` varchar(30) default '1',
  `objetMarche` longtext,
  `montantMarche` double default NULL,
  `valide` enum('0','1') NOT NULL default '0',
  `isManuel` enum('0','1') NOT NULL default '0',
  `pmePmi` int(11) default '0',
  `id_decision_enveloppe` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `ListeMarches_FKIndex2` (`idMarcheTrancheBudgetaire`),
  KEY `ListeMarches_FKIndex3` (`idService`),
  KEY `numeroMarcheAnnee` (`numeroMarcheAnnee`),
  KEY `natureMarche` (`natureMarche`),
  KEY `id_decision_enveloppe` (`id_decision_enveloppe`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `Marche`
--


-- --------------------------------------------------------

--
-- Structure de la table `MarcheAnnee`
--

CREATE TABLE IF NOT EXISTS `MarcheAnnee` (
  `numero` int(11) unsigned NOT NULL default '0',
  `afficher` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`numero`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `MarcheAnnee`
--

INSERT INTO `MarcheAnnee` VALUES
(2006, '1'),
(2007, '1'),
(2008, '1'),
(2009, '1'),
(2010, '0'),
(2011, '0'),
(2012, '0'),
(2013, '0'),
(2014, '0'),
(2015, '0');

-- --------------------------------------------------------

--
-- Structure de la table `MarchePublie`
--

CREATE TABLE IF NOT EXISTS `MarchePublie` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `numeroMarcheAnnee` int(11) unsigned NOT NULL default '0',
  `idService` int(11) default '0',
  `isPubliee` enum('0','1') NOT NULL default '0',
  `isImportee` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `ListeMarchePubliee_FKIndex1` (`idService`),
  KEY `ListeMarchePubliee_FKIndex2` (`numeroMarcheAnnee`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `MarchePublie`
--


-- --------------------------------------------------------

--
-- Structure de la table `Message`
--

CREATE TABLE IF NOT EXISTS `Message` (
  `id` int(11) NOT NULL default '0',
  `data` mediumtext NOT NULL,
  `s_email` varchar(100) NOT NULL default '',
  `s_id` int(11) NOT NULL default '0',
  `s_type` varchar(90) NOT NULL default '',
  `r_email` varchar(100) NOT NULL default '',
  `r_id` int(11) NOT NULL default '0',
  `r_type` varchar(90) NOT NULL default '',
  `delivery_date` varchar(24) default NULL,
  `receipt_date` varchar(24) default NULL,
  `delivery_sig` text,
  `receipt_sig` text,
  `delivery_ts` mediumtext,
  `receipt_ts` mediumtext,
  `offre_id` int(22) NOT NULL default '0',
  `msg_type` int(11) NOT NULL default '0',
  `lot` int(5) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `offre_id` (`offre_id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Message`
--


-- --------------------------------------------------------

--
-- Structure de la table `Mesure_avancement`
--

CREATE TABLE IF NOT EXISTS `Mesure_avancement` (
  `id_mesure` int(11) NOT NULL auto_increment,
  `id_service` int(11) NOT NULL,
  `annee` varchar(4) NOT NULL,
  `trimestre` char(2) NOT NULL,
  `siren` varchar(9) NOT NULL,
  `nic` varchar(5) NOT NULL,
  `identifiant_service` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `type_pouvoir_adjudicateur` int(11) NOT NULL,
  `departement` int(11) NOT NULL,
  `date_envoi` varchar(20) default NULL,
  `date_accuse` varchar(20) default NULL,
  `etat` varchar(5) default 'BR',
  `date_creation` varchar(20) NOT NULL,
  `xml` longtext,
  `name_xml_genere` varchar(225) default NULL,
  PRIMARY KEY  (`id_mesure`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Mesure_avancement`
--


-- --------------------------------------------------------

--
-- Structure de la table `Mesure_Type_Procedure`
--

CREATE TABLE IF NOT EXISTS `Mesure_Type_Procedure` (
  `id_tag_name` int(11) NOT NULL auto_increment,
  `libelle_tag_name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id_tag_name`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `Mesure_Type_Procedure`
--

INSERT INTO `Mesure_Type_Procedure` VALUES
(1, 'Appel d''offres ouvert'),
(2, 'Appel d''offres restreint'),
(3, 'Dialogue compétitif'),
(4, 'Marché à procédure adaptée'),
(5, 'Marché négocié avec publicité préalable et mise en concurrence'),
(6, 'Concours'),
(7, 'Procédure spécifique à certains marchés de défense nationale'),
(8, 'Accord cadre'),
(9, 'Système d''acquisition dynamique'),
(10, 'Marché subséquent à accord cadre'),
(11, 'Marché spécifique sur système acquisition dynamique');

-- --------------------------------------------------------

--
-- Structure de la table `mode_execution_contrat`
--

CREATE TABLE IF NOT EXISTS `mode_execution_contrat` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `mode_execution_contrat`
--

INSERT INTO `mode_execution_contrat` VALUES
(1, '1 - Ordinaire (Sans tranche, exclusivement à prix forfaitaire)'),
(2, '2 - A bons de commande (Sans tranche, exclusivement à bons de commande)'),
(3, '3 - A Tranches (Avec TF et TC, exclusivement à prix forfaitaire)'),
(4, '4 - A tranches et bons de commande (Avec TF et TC, en partie à bons de commande)'),
(5, '5 - A phases'),
(6, '6 - Autres');

-- --------------------------------------------------------

--
-- Structure de la table `nature_acte_juridique`
--

CREATE TABLE IF NOT EXISTS `nature_acte_juridique` (
  `id` int(11) NOT NULL default '0',
  `libelle` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `nature_acte_juridique`
--

INSERT INTO `nature_acte_juridique` VALUES
(1, '1-Contrat initial'),
(2, '2-Contrat complémentaire'),
(3, '3-Contrat sur la base d''un accord cadre'),
(4, '4-Marché de définition'),
(5, '5-Autre');

-- --------------------------------------------------------

--
-- Structure de la table `Numerotation_ref_cons_auto`
--

CREATE TABLE IF NOT EXISTS `Numerotation_ref_cons_auto` (
  `id_cons_auto` int(11) NOT NULL default '0',
  `annee` year(4) NOT NULL default '0000',
  PRIMARY KEY  (`annee`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Numerotation_ref_cons_auto`
--


-- --------------------------------------------------------

--
-- Structure de la table `Offre`
--

CREATE TABLE IF NOT EXISTS `Offre` (
  `id` int(22) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `entreprise_id` int(22) NOT NULL default '0',
  `inscrit_id` int(22) NOT NULL default '0',
  `signatureenvxml` longblob NOT NULL,
  `horodatage` longblob NOT NULL,
  `mailsignataire` varchar(80) NOT NULL default '',
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `untrustedserial` varchar(40) NOT NULL default '',
  `envoi_complet` char(1) NOT NULL default '',
  `date_depot_differe` datetime NOT NULL default '0000-00-00 00:00:00',
  `horodatage_envoi_differe` longblob NOT NULL,
  `signatureenvxml_envoi_differe` longblob NOT NULL,
  `external_serial` varchar(8) default NULL,
  `internal_serial` varchar(8) default NULL,
  `uid_offre` varchar(40) NOT NULL default '',
  `offre_selectionnee` int(11) NOT NULL default '0',
  `Observation` text,
  `xml_string` longtext NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `untrustedserial` (`untrustedserial`),
  KEY `inscrit_id` (`inscrit_id`),
  KEY `consultation_ref` (`consultation_ref`),
  KEY `entreprise_id` (`entreprise_id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Offre`
--


-- --------------------------------------------------------

--
-- Structure de la table `Offre_papier`
--

CREATE TABLE IF NOT EXISTS `Offre_papier` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `nom_entreprise` varchar(30) NOT NULL default '',
  `date_depot` datetime NOT NULL default '0000-00-00 00:00:00',
  `external_serial` varchar(8) default NULL,
  `internal_serial` varchar(8) default NULL,
  `offre_selectionnee` int(11) NOT NULL default '0',
  `nom` varchar(80) default NULL,
  `prenom` varchar(80) default NULL,
  `adresse` varchar(100) default NULL,
  `adresse2` varchar(100) default NULL,
  `telephone` varchar(20) default NULL,
  `fax` varchar(30) default NULL,
  `code_postal` varchar(5) default NULL,
  `ville` varchar(50) default NULL,
  `pays` varchar(50) default NULL,
  `acronyme_pays` varchar(10) default NULL,
  `siret` varchar(14) default NULL,
  `identifiant_national` varchar(20) default NULL,
  `email` varchar(100) default NULL,
  `observation` text,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Offre_papier`
--


-- --------------------------------------------------------

--
-- Structure de la table `Ordre_Du_Jour`
--

CREATE TABLE IF NOT EXISTS `Ordre_Du_Jour` (
  `id` int(11) NOT NULL auto_increment,
  `id_commission` int(11) NOT NULL default '0',
  `ref_consultation` varchar(255) default NULL,
  `ref_libre` varchar(50) default NULL,
  `id_etape` int(11) NOT NULL default '1',
  `intitule_ordre_du_jour` varchar(255) default NULL,
  `lots_odj_libre` varchar(100) default NULL,
  `id_type_procedure` int(11) default NULL,
  `date_cloture` datetime default NULL,
  `type_env` int(1) default NULL,
  `sous_pli` int(2) default NULL,
  `heure` varchar(5) NOT NULL default '00',
  `etape_consultation` varchar(200) NOT NULL default '',
  `type_consultation` varchar(200) NOT NULL default '',
  `minutes` char(2) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id_commission` (`id_commission`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Ordre_Du_Jour`
--


-- --------------------------------------------------------

--
-- Structure de la table `Passation_consultation`
--

CREATE TABLE IF NOT EXISTS `Passation_consultation` (
  `id` int(11) NOT NULL auto_increment,
  `reference` int(11) NOT NULL default '0',
  `unite` int(5) default '0',
  `mandataire` int(5) default '0',
  `lieu_detaille_predefini` int(5) default '0',
  `lieux_detailles_non_definis` varchar(100) default NULL,
  `code_postal_lieu_principal_execution` varchar(20) default NULL,
  `Ville` varchar(50) default NULL,
  `montant_estime_consultation` varchar(20) default NULL,
  `commentaires` text,
  `numero_deliberation_financiere` varchar(20) default NULL,
  `date_deliberation_financiere` varchar(10) default NULL,
  `imputation_budgetaire` text,
  `numero_deliberation_autorisant_signature_marche` varchar(20) default NULL,
  `date_deliberation_autorisant_signature_marche` varchar(10) default NULL,
  `date_notification_previsionnelle` varchar(10) default NULL,
  `date_reception_projet_DCE_Service_Validateur` varchar(10) default NULL,
  `date_formulations_premieres_observations` varchar(10) default NULL,
  `date_retour_projet_DCE_finalise` varchar(10) default NULL,
  `date_validation_projet_DCE_par_service_validateur` varchar(10) default NULL,
  `date_validation_projet_DCE_vue_par` int(5) default '0',
  `date_reception_projet_AAPC_par_Service_Validateur` varchar(10) default NULL,
  `date_formulations_premieres_observations_AAPC` varchar(10) default NULL,
  `date_retour_projet_AAPC_finalise` varchar(10) default NULL,
  `date_validation_projet_AAPC_par_Service_Validateur` varchar(10) default NULL,
  `date_validation_projet_AAPC_par_Service_Validateur_vu_par` int(5) default '0',
  `date_envoi_publicite` varchar(10) default NULL,
  `date_envoi_invitations_remettre_offre` varchar(10) default NULL,
  `date_limite_remise_offres` varchar(10) default NULL,
  `delai_validite_offres` varchar(5) default NULL,
  `commentaires_phase_consultation` text,
  `date_reunion_ouverture_candidatures` varchar(10) default NULL,
  `date_reunion_ouverture_offres` varchar(10) default NULL,
  `decision` int(5) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Passation_consultation`
--


-- --------------------------------------------------------

--
-- Structure de la table `passation_marche_a_venir`
--

CREATE TABLE IF NOT EXISTS `passation_marche_a_venir` (
  `id` int(11) NOT NULL auto_increment,
  `id_passation_consultation` int(11) NOT NULL default '0',
  `lot` int(11) NOT NULL default '0',
  `montant_estime` int(11) NOT NULL default '0',
  `id_nature_acte_juridique` int(11) NOT NULL default '0',
  `forme_groupement` int(11) NOT NULL default '0',
  `variante_autorisee` enum('0','1') NOT NULL default '0',
  `variante_technique_obligatoire` enum('0','1') NOT NULL default '0',
  `nombre_variante_technique` int(11) NOT NULL default '0',
  `description_variante_technique` text NOT NULL,
  `mode_execution_contrat` int(11) NOT NULL default '0',
  `type_bon_commande` int(11) NOT NULL default '0',
  `min_bon_commande` int(11) NOT NULL default '0',
  `max_bon_commande` int(11) NOT NULL default '0',
  `duree_execution_marche_hors_reconduction` int(11) NOT NULL default '0',
  `nombre_reconduction` int(11) NOT NULL default '0',
  `duree_total_marche` int(11) NOT NULL default '0',
  `ccag_applicable` int(11) NOT NULL default '0',
  `marche_transversal` enum('0','1') NOT NULL default '0',
  `date_reception_analyse_offre` varchar(10) default NULL,
  `date_formulation_observation_projet_rapport` varchar(10) default NULL,
  `date_retour_projet_rapport_finalise` varchar(10) default NULL,
  `date_validation_projet_rapport` varchar(10) default NULL,
  `projet_rapport_vu_par` int(11) default NULL,
  `date_reunion_attribution` varchar(10) default NULL,
  `decision` int(11) default NULL,
  `date_envoi_courrier_condidat_non_retenu` varchar(10) default NULL,
  `date_signature_marche_pa` varchar(10) default NULL,
  `date_reception_controle_legalite` varchar(10) default NULL,
  `date_formulation_observation_dossier` varchar(10) default NULL,
  `date_retour_dossier_finalise` varchar(10) default NULL,
  `date_transmission_prefecture` varchar(10) default NULL,
  `dossier_vu_par` int(11) default NULL,
  `date_validation_rapport_information` varchar(10) default NULL,
  `commentaire` text,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `passation_marche_a_venir`
--


-- --------------------------------------------------------

--
-- Structure de la table `ProcedureEquivalence`
--

CREATE TABLE IF NOT EXISTS `ProcedureEquivalence` (
  `id_type_procedure` int(11) NOT NULL default '0',
  `elec_resp` char(2) NOT NULL default '',
  `no_elec_resp` char(2) NOT NULL default '',
  `cipher_enabled` char(2) NOT NULL default '',
  `cipher_disabled` char(2) NOT NULL default '',
  `signature_enabled` char(2) NOT NULL default '',
  `signature_disabled` char(2) NOT NULL default '',
  `env_candidature` char(2) NOT NULL default '',
  `env_offre` char(2) NOT NULL default '',
  `env_anonymat` char(2) NOT NULL default '',
  `envoi_complet` char(2) NOT NULL default '',
  `envoi_differe` char(2) NOT NULL default '',
  `procedure_publicite` char(2) NOT NULL default '',
  `procedure_restreinte_candidature` char(2) NOT NULL default '',
  `procedure_restreinte_offre` char(2) NOT NULL default '',
  `envoi_mail_par_mpe` char(2) NOT NULL default '0',
  `no_envoi_mail_par_mpe` char(2) NOT NULL default '0',
  `mise_en_ligne1` char(2) NOT NULL default '',
  `mise_en_ligne2` char(2) NOT NULL default '',
  `mise_en_ligne3` char(2) NOT NULL default '',
  `mise_en_ligne4` char(2) NOT NULL default '',
  `env_offre_type_unique` char(2) NOT NULL default '',
  `env_offre_type_multiple` char(2) NOT NULL default '',
  `no_fichier_annonce` char(2) NOT NULL default '',
  `fichier_importe` char(2) NOT NULL default '',
  `fichier_boamp` char(2) NOT NULL default '',
  `reglement_cons` char(2) NOT NULL default '',
  `dossier_dce` char(2) NOT NULL default '',
  `partial_dce_download` char(2) NOT NULL default '',
  `service` char(2) NOT NULL default '',
  `constitution_dossier_reponse` char(2) NOT NULL default '',
  `env_offre_type_unique2` char(2) NOT NULL default '',
  `env_offre_type_multiple2` char(2) NOT NULL default '',
  `gestion_envois_postaux` char(2) NOT NULL default '',
  `tireur_plan_non` char(2) NOT NULL default '',
  `tireur_plan_oui` char(2) NOT NULL default '',
  `tireur_plan_papier` char(2) NOT NULL default '',
  `tireur_plan_cdrom` char(2) NOT NULL default '',
  `tireur_plan_nom` char(2) NOT NULL default '',
  `tirage_descriptif` char(2) NOT NULL default '',
  `delai_date_limite_remise_pli` char(2) NOT NULL default '',
  `signature_propre` char(2) NOT NULL default '',
  `procedure_restreinte` char(2) NOT NULL default '',
  `ouverture_simultanee` char(2) NOT NULL default '',
  `type_decision_a_renseigner` char(2) NOT NULL default '-0',
  `type_decision_attribution_marche` char(2) NOT NULL default '-0',
  `type_decision_declaration_sans_suite` char(2) NOT NULL default '-0',
  `type_decision_declaration_infructueux` char(2) NOT NULL default '-0',
  `type_decision_selection_entreprise` char(2) NOT NULL default '-0',
  `type_decision_attribution_accord_cadre` char(2) NOT NULL default '-0',
  `type_decision_admission_sad` char(2) NOT NULL default '-0',
  `type_decision_autre` char(2) NOT NULL default '-0',
  PRIMARY KEY  (`id_type_procedure`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `ProcedureEquivalence`
--

INSERT INTO `ProcedureEquivalence` VALUES
(1, '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '1', '0', '0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0'),
(2, '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0'),
(3, '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0'),
(4, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0'),
(5, '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0'),
(6, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0'),
(7, '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0'),
(8, '+1', '+0', '+1', '+0', '+1', '+0', '1', '1', '1', '-1', '-1', '1', '+0', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0'),
(9, '+1', '+0', '+1', '+0', '+1', '+0', '1', '0', '0', '-1', '-1', '1', '+1', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0'),
(10, '+1', '+0', '+1', '+0', '+1', '+0', '-0', '1', '1', '-1', '-1', '0', '+0', '+1', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0'),
(11, '1', '0', '+1', '+0', '+1', '+0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0'),
(12, '+1', '+0', '+1', '+0', '+1', '+0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '-0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0'),
(17, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+0', '-1', '-1', '+1', '+0', '+0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0'),
(18, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+0', '+0', '+1', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '-1', '-1', '-1', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0'),
(30, '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '+0', '-0', '+0'),
(31, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '+0', '+1', '-0', '-0', '1', '0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0'),
(32, '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '-0', '+0', '+0'),
(33, '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0');

-- --------------------------------------------------------

--
-- Structure de la table `Publicite`
--

CREATE TABLE IF NOT EXISTS `Publicite` (
  `id` int(11) NOT NULL auto_increment,
  `consultation` int(11) NOT NULL default '0',
  `organe` int(11) NOT NULL default '0',
  `date_envoi` date NOT NULL default '0000-00-00',
  `destinataires` mediumtext,
  PRIMARY KEY  (`id`),
  KEY `consultation` (`consultation`),
  KEY `organe` (`organe`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Publicite`
--


-- --------------------------------------------------------

--
-- Structure de la table `QuestionDCE`
--

CREATE TABLE IF NOT EXISTS `QuestionDCE` (
  `id` int(22) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `date_depot` datetime NOT NULL default '0000-00-00 00:00:00',
  `email` varchar(100) NOT NULL default '',
  `nom` varchar(80) default NULL,
  `entreprise` varchar(50) default NULL,
  `adresse` varchar(200) default NULL,
  `cp` varchar(100) default NULL,
  `ville` varchar(150) default NULL,
  `tel` varchar(150) default NULL,
  `fax` varchar(150) default NULL,
  `question` longtext NOT NULL,
  `statut` int(11) NOT NULL default '0',
  `date_reponse` date NOT NULL default '0000-00-00',
  `personne_repondu` text,
  `type_depot` enum('1','2') NOT NULL default '1',
  `pays` varchar(150) default NULL,
  `Observation` text,
  `siret` varchar(14) default NULL,
  `identifiant_national` varchar(20) default NULL,
  `acronyme_pays` varchar(10) default NULL,
  `adresse2` varchar(80) default NULL,
  `id_fichier` int(11) default NULL,
  `nom_fichier` varchar(100) default NULL,
  `prenom` varchar(80) default NULL,
  `siret_etranger` varchar(20) default NULL,
  `id_inscrit` int(11) NOT NULL default '0',
  `id_entreprise` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `QuestionDCE`
--


-- --------------------------------------------------------

--
-- Structure de la table `ReceptionEnveloppe`
--

CREATE TABLE IF NOT EXISTS `ReceptionEnveloppe` (
  `id` int(11) NOT NULL auto_increment,
  `uid_offre` varchar(40) NOT NULL default '',
  `type_enveloppe` int(11) NOT NULL default '0',
  `sous_pli` int(11) NOT NULL default '0',
  `champs_optionnels` longblob NOT NULL,
  `nom_fichier` varchar(255) NOT NULL default '',
  `fichier` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `ReceptionEnveloppe`
--


-- --------------------------------------------------------

--
-- Structure de la table `Referentiel`
--

CREATE TABLE IF NOT EXISTS `Referentiel` (
  `id_referentiel` int(10) NOT NULL auto_increment,
  `libelle_referentiel` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`id_referentiel`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `Referentiel`
--

INSERT INTO `Referentiel` VALUES
(1, 'Type organismes'),
(2, 'Tranche budgétaire'),
(3, 'pme pmi');

-- --------------------------------------------------------

--
-- Structure de la table `ReferentielDestinationFormXml`
--

CREATE TABLE IF NOT EXISTS `ReferentielDestinationFormXml` (
  `id` int(11) NOT NULL auto_increment,
  `destinataire` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `ReferentielDestinationFormXml`
--

INSERT INTO `ReferentielDestinationFormXml` VALUES
(1, 'BOAMP seul'),
(2, 'BOAMP et JOUE'),
(3, 'Groupe Moniteur');

-- --------------------------------------------------------

--
-- Structure de la table `ReferentielFormXml`
--

CREATE TABLE IF NOT EXISTS `ReferentielFormXml` (
  `id` int(11) NOT NULL auto_increment,
  `id_type_xml` int(11) NOT NULL default '0',
  `consultation_ref` varchar(100) NOT NULL default '',
  `xml` text NOT NULL,
  `date_creation` varchar(20) default NULL,
  `statut` char(1) default NULL,
  `id_compte_boamp` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id_type_xml` (`id_type_xml`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `ReferentielFormXml`
--


-- --------------------------------------------------------

--
-- Structure de la table `ReferentielTypeXml`
--

CREATE TABLE IF NOT EXISTS `ReferentielTypeXml` (
  `id` int(11) NOT NULL auto_increment,
  `id_destinataire` varchar(50) NOT NULL default '',
  `libelle_type` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `ReferentielTypeXml`
--

INSERT INTO `ReferentielTypeXml` VALUES
(1, '1#3', 'XML 1.9 BOAMP3 - Avis de marché National'),
(2, '1#3', 'XML 1.9 BOAMP6 - Avis de MAPA'),
(3, '2#3', 'XML 1.9 JOUE_02 - Avis de marché'),
(4, '2#3', 'XML 1.9 JOUE_03 - Avis d''attribution'),
(5, '1#3', 'XML 1.9 BOAMP4 - Avis d''attribution'),
(6, '1#3', 'XML 1.9 BOAMP2 - Avis rectificatif'),
(7, '1#3', 'XML 1.9 BOAMP7 - Résultat de MAPA'),
(9, '2#3', 'XML 1.9 JOUE_12 - Avis Concours'),
(10, '2#3', 'XML 1.9 JOUE_14 - Formulaire rectificatif / annulation'),
(11, '2#3', 'XML 1.9 JOUE_05 - Secteurs Spéciaux');

-- --------------------------------------------------------

--
-- Structure de la table `Retrait_Papier`
--

CREATE TABLE IF NOT EXISTS `Retrait_Papier` (
  `id` int(22) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `nom` varchar(100) NOT NULL default '',
  `adresse` varchar(100) NOT NULL default '',
  `entreprise` varchar(100) NOT NULL default '',
  `datetelechargement` datetime NOT NULL default '0000-00-00 00:00:00',
  `codepostal` int(5) NOT NULL default '0',
  `ville` varchar(50) NOT NULL default '',
  `pays` varchar(50) NOT NULL default '',
  `telephone` varchar(20) NOT NULL default '',
  `tirage_plan` int(11) NOT NULL default '0',
  `siret` varchar(14) default NULL,
  `fax` varchar(30) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `Observation` text,
  `prenom` varchar(100) default NULL,
  `adresse2` varchar(100) default NULL,
  `identifiant_national` varchar(20) default NULL,
  `acronyme_pays` varchar(10) default NULL,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Retrait_Papier`
--


-- --------------------------------------------------------

--
-- Structure de la table `RG`
--

CREATE TABLE IF NOT EXISTS `RG` (
  `id` int(11) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `rg` int(11) NOT NULL default '0',
  `nom_fichier` varchar(80) NOT NULL default '',
  `statut` char(1) NOT NULL default '1',
  `horodatage` longblob NOT NULL,
  `untrusteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `agent_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `RG`
--


-- --------------------------------------------------------

--
-- Structure de la table `RPA`
--

CREATE TABLE IF NOT EXISTS `RPA` (
  `id` int(11) NOT NULL auto_increment,
  `id_service` int(11) NOT NULL default '0',
  `nom` varchar(30) NOT NULL default '',
  `prenom` varchar(30) NOT NULL default '',
  `adresse1` varchar(80) NOT NULL default '',
  `adresse2` varchar(80) NOT NULL default '',
  `codepostal` varchar(20) NOT NULL default '',
  `ville` varchar(50) NOT NULL default '',
  `Fonction` varchar(200) default NULL,
  `Organisme` varchar(200) NOT NULL default '',
  `pays` varchar(150) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `RPA`
--


-- --------------------------------------------------------

--
-- Structure de la table `Service`
--

CREATE TABLE IF NOT EXISTS `Service` (
  `id` int(11) NOT NULL auto_increment,
  `service` varchar(100) NOT NULL default '',
  `type_service` enum('1','2') NOT NULL default '2',
  `id_acheteur_public` int(22) default NULL,
  `libelle` varchar(100) NOT NULL default '',
  `sigle` varchar(100) NOT NULL default '',
  `adresse` varchar(100) NOT NULL default '',
  `adresse_suite` varchar(100) NOT NULL default '',
  `cp` varchar(5) NOT NULL default '',
  `ville` varchar(100) NOT NULL default '',
  `telephone` varchar(100) NOT NULL default '',
  `fax` varchar(100) NOT NULL default '',
  `mail` varchar(100) NOT NULL default '',
  `pays` varchar(150) default NULL,
  `id_initial` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id_acheteur_public` (`id_acheteur_public`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Service`
--


-- --------------------------------------------------------

--
-- Structure de la table `StatutEnveloppe`
--

CREATE TABLE IF NOT EXISTS `StatutEnveloppe` (
  `id_statut` int(2) NOT NULL default '0',
  `description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id_statut`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `StatutEnveloppe`
--

INSERT INTO `StatutEnveloppe` VALUES
(1, 'Fermée'),
(2, 'Ouverte en ligne'),
(3, 'Ouverte hors ligne'),
(4, 'Refusée'),
(5, 'Ouverte'),
(6, 'Supprimée'),
(7, 'Ouverte à distance');

-- --------------------------------------------------------

--
-- Structure de la table `Telechargement`
--

CREATE TABLE IF NOT EXISTS `Telechargement` (
  `id` int(22) NOT NULL auto_increment,
  `consultation_ref` int(11) NOT NULL default '0',
  `nom` varchar(100) NOT NULL default '',
  `adresse` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `entreprise` varchar(100) NOT NULL default '',
  `datetelechargement` datetime NOT NULL default '0000-00-00 00:00:00',
  `codepostal` varchar(5) NOT NULL default '0',
  `ville` varchar(50) NOT NULL default '',
  `pays` varchar(50) default NULL,
  `telephone` varchar(20) NOT NULL default '',
  `tirage_plan` int(11) NOT NULL default '0',
  `siret` varchar(14) NOT NULL default '',
  `fax` varchar(30) NOT NULL default '',
  `support` enum('1','2','3') NOT NULL default '1',
  `lots` varchar(255) default '',
  `id_inscrit` int(11) default '0',
  `id_entreprise` int(11) default '0',
  `sirenEtranger` varchar(20) default '0',
  `adresse2` varchar(80) NOT NULL default '0',
  `prenom` varchar(11) default '',
  `noms_fichiers_dce` text,
  `Observation` text,
  `acronyme_pays` varchar(10) default NULL,
  PRIMARY KEY  (`id`),
  KEY `consultation_ref` (`consultation_ref`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `Telechargement`
--


-- --------------------------------------------------------


--
-- Structure de la table `TireurPlan`
--

CREATE TABLE IF NOT EXISTS `TireurPlan` (
  `id` int(11) NOT NULL auto_increment,
  `nom` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `id_service` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `TireurPlan`
--


-- --------------------------------------------------------

--
-- Structure de la table `TypeAvenant`
--

CREATE TABLE IF NOT EXISTS `TypeAvenant` (
  `id` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `TypeAvenant`
--

INSERT INTO `TypeAvenant` VALUES
(1, '1-Sans incidence financière'),
(2, '2-Avenant augmentant le montant initial'),
(3, '3-avenant diminuant le montant initial');

-- --------------------------------------------------------

--
-- Structure de la table `TypeDecision`
--

CREATE TABLE IF NOT EXISTS `TypeDecision` (
  `id_type_decision` int(11) NOT NULL auto_increment,
  `libelle_type_decision` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id_type_decision`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `TypeDecision`
--

INSERT INTO `TypeDecision` VALUES
(2, 'Attribution du marché'),
(3, 'Déclaration Sans suite'),
(4, 'Déclaration Infructueux'),
(5, 'Sélection des entreprises (en vue de la phase suivante)'),
(6, 'Attribution de l''Accord cadre'),
(7, 'Admission au SAD'),
(8, 'Autre'),
(9, 'A renseigner');

-- --------------------------------------------------------

--
-- Structure de la table `TypeProcedure`
--

CREATE TABLE IF NOT EXISTS `TypeProcedure` (
  `id_type_procedure` int(11) NOT NULL auto_increment,
  `libelle_type_procedure` varchar(100) NOT NULL default '',
  `abbreviation` varchar(50) NOT NULL default '',
  `type_boamp` int(11) NOT NULL default '0',
  `id_type_procedure_portail` int(11) NOT NULL default '0',
  `categorie_procedure` int(11) NOT NULL default '0',
  `delai_alerte` int(11) NOT NULL default '0',
  `id_type_validation` int(11) NOT NULL default '2',
  `service_validation` int(11) NOT NULL default '0',
  `mapa` enum('0','1') NOT NULL default '0',
  `activer_mapa` enum('0','1') NOT NULL default '1',
  `libelle_type_procedure_fr` varchar(100) default '',
  `libelle_type_procedure_en` varchar(100) default '',
  `libelle_type_procedure_es` varchar(100) default '',
  `libelle_type_procedure_su` varchar(100) default '',
  `libelle_type_procedure_du` varchar(100) default '',
  `libelle_type_procedure_cz` varchar(100) default '',
  `id_montant_mapa` int(2) NOT NULL default '0',
  `code_recensement` char(3) default NULL,
  `depouillable_phase_consultation` enum('0','1') NOT NULL default '0',
  `consultation_transverse` enum('0','1') NOT NULL default '0',
  `tag_Boamp` varchar(50) NOT NULL default '',
  `ao` enum('0','1') NOT NULL default '0',
  `mn` enum('0','1') NOT NULL default '0',
  `dc` enum('0','1') NOT NULL default '0',
  `autre` enum('0','1') NOT NULL default '0',
  `sad` enum('0','1') NOT NULL default '0',
  `accord_cadre` enum('0','1') NOT NULL default '0',
  `tag_name_mesure_avancement` varchar(100) NOT NULL,
  `abreviation_interface` varchar(50) default NULL,
  PRIMARY KEY  (`id_type_procedure`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `TypeProcedure`
--

INSERT INTO `TypeProcedure` VALUES
(1, 'Appel d''offres ouvert', 'AOO', 1, 1, 1, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '01', '0', '0', '<AppelOuvert/>', '1', '0', '0', '0', '0', '0', '1', 'AOO'),
(2, 'Appel d''offres restreint - Candidature', 'AOR-C', 2, 2, 2, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARC'),
(3, 'Appel d''offres restreint - Offre', 'AOR-O', 2, 2, 2, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARO'),
(4, 'Procédure adaptée < 90 k EUR HT', 'PA-INF', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAI'),
(5, 'Procédure adaptée &#8805; 90 k EUR HT', 'PA-SUP', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', 2, NULL, '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAS'),
(6, 'Procédure adaptée spécifique article 30 < 90 k EUR HT', 'PA-A30-INF', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSI'),
(7, 'Procédure adaptée spécifique article 30 &#8805; 90 k EUR HT', 'PA-A30-SUP', 23, 3, 0, 0, 2, 0, '1', '1', '', '', '', '', '', '', 2, NULL, '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSS'),
(8, 'Concours ouvert', 'CO', 7, 4, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursOuvert/>', '0', '0', '0', '1', '0', '0', '6', 'COO'),
(9, 'Concours restreint - Candidature', 'CR-C', 8, 5, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRC'),
(10, 'Concours restreint - Offre', 'CR-O', 8, 5, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRO'),
(11, 'Dialogue compétitif - Candidature', 'DC-C', 22, 7, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCC'),
(12, 'Dialogue compétitif - Offre', 'DC-O', 22, 7, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCO'),
(17, 'Procédure Autre', 'AUT', 24, 15, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '1', '0', '0', '', 'ATR'),
(18, 'Marché négocié', 'MN', 18, 8, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10', '0', '0', '<MarcheNegocie/>', '0', '1', '0', '0', '0', '0', '5', 'MNE'),
(30, 'Accord-Cadre - 1 Sélection des Attributaires', 'AC-SA', 1, 30, 1, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10', '0', '1', '<Autres/>', '0', '0', '0', '1', '0', '1', '8', 'ACA'),
(31, 'Accord-Cadre - 2 Marché Subséquent', 'AC-MS', 2, 31, 2, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '1', '0', '1', '10', 'ACS'),
(32, 'Système d''Acquisition Dynamique - Admission', 'SAD-A', 8, 32, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '08', '1', '1', '<Autres/>', '0', '0', '0', '1', '1', '0', '9', 'SAA'),
(33, 'Système d''Acquisition Dynamique - Marché Spécifique', 'SAD-MS', 8, 33, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', 0, '08', '0', '0', '<Autres/>', '0', '0', '0', '1', '1', '0', '11', 'SAM');

-- --------------------------------------------------------

--
-- Structure de la table `ValeurReferentiel`
--

CREATE TABLE IF NOT EXISTS `ValeurReferentiel` (
  `id` int(10) NOT NULL auto_increment,
  `id_referentiel` int(10) NOT NULL default '0',
  `libelle_valeur_referentiel` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  ;

--
-- Contenu de la table `ValeurReferentiel`
--

INSERT INTO `ValeurReferentiel` VALUES
(1, 1, 'Etat'),
(2, 1, 'Région'),
(3, 1, 'Département'),
(4, 1, 'Commune'),
(5, 1, 'Etablissement public national'),
(6, 1, 'Etablissement public territorial'),
(7, 1, 'Autre'),
(9, 3, 'Non renseigné'),
(10, 3, 'Oui'),
(11, 3, 'Non'),
(12, 2, '4 000 HT à 19 999,99 HT'),
(13, 2, '20 000 HT à 49 999,99 HT'),
(14, 2, '50 000 HT à 89 999,99 HT'),
(15, 2, '90 000 HT à 132 999,99 HT'),
(16, 2, '133 000 HT à 205 999,99 HT'),
(17, 2, '206 000 HT à 999 999,99 HT'),
(18, 2, '1 000 000 HT à 2 999 999,99 HT'),
(19, 2, '3 000 000 HT à 5 149 999,99 HT'),
(20, 2, '5 150 000 HT et plus.');

-- --------------------------------------------------------

--
-- Structure de la table `valeur_enchere`
--

CREATE TABLE IF NOT EXISTS `valeur_enchere` (
  `id` int(11) NOT NULL auto_increment,
  `id_enchere_entreprise` int(11) NOT NULL default '0',
  `sig_xml` longtext NOT NULL,
  `horo_xml` longblob NOT NULL,
  `enchere_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `data_xml` longblob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_enchere_entreprise` (`id_enchere_entreprise`)
) ENGINE=InnoDB ;

--
-- Contenu de la table `valeur_enchere`
--


--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `AffiliationService`
--
ALTER TABLE `AffiliationService`
  ADD CONSTRAINT `C27256` FOREIGN KEY (`id_pole`) REFERENCES `Service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `C34295` FOREIGN KEY (`id_service`) REFERENCES `Service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Agent_Commission`
--
ALTER TABLE `Agent_Commission`
  ADD CONSTRAINT `fk_cao_ag` FOREIGN KEY (`id_commission`) REFERENCES `Commission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Annonce`
--
ALTER TABLE `Annonce`
  ADD CONSTRAINT `Annonce_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Annonce_ibfk_2` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `Annonce_ibfk_3` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`),
  ADD CONSTRAINT `Annonce_ibfk_4` FOREIGN KEY (`id_type_xml`) REFERENCES `ReferentielTypeXml` (`id`);

--
-- Contraintes pour la table `AnnonceJALPieceJointe`
--
ALTER TABLE `AnnonceJALPieceJointe`
  ADD CONSTRAINT `AnnonceJALPieceJointe_ibfk_1` FOREIGN KEY (`id_annonce_jal`) REFERENCES `AnnonceJAL` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `AnnonceMoniteur`
--
ALTER TABLE `AnnonceMoniteur`
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_18` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_10` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_11` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_12` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_13` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_14` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_15` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_16` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_17` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_2` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_3` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_4` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_5` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_6` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_7` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_8` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
  ADD CONSTRAINT `AnnonceMoniteur_ibfk_9` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`);

--
-- Contraintes pour la table `Avenant`
--
ALTER TABLE `Avenant`
  ADD CONSTRAINT `Avenant_ibfk_1` FOREIGN KEY (`id_contrat`) REFERENCES `Contrat` (`id_contrat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `AVIS`
--
ALTER TABLE `AVIS`
  ADD CONSTRAINT `C67125` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `blocFichierEnveloppe`
--
ALTER TABLE `blocFichierEnveloppe`
  ADD CONSTRAINT `blocFichierEnveloppe_ibfk_1` FOREIGN KEY (`id_fichier`) REFERENCES `fichierEnveloppe` (`id_fichier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CategorieLot`
--
ALTER TABLE `CategorieLot`
  ADD CONSTRAINT `C236987` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CertificatChiffrement`
--
ALTER TABLE `CertificatChiffrement`
  ADD CONSTRAINT `C903654` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ClientCAO`
--
ALTER TABLE `ClientCAO`
  ADD CONSTRAINT `C15144552` FOREIGN KEY (`offre_id`) REFERENCES `Offre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Complement`
--
ALTER TABLE `Complement`
  ADD CONSTRAINT `Complement_ibfk_10545` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ConsultationHistoriqueEtat`
--
ALTER TABLE `ConsultationHistoriqueEtat`
  ADD CONSTRAINT `ConsultationHistoriqueEtat` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE;

--
-- Contraintes pour la table `DATEFIN`
--
ALTER TABLE `DATEFIN`
  ADD CONSTRAINT `C51025` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `DCE`
--
ALTER TABLE `DCE`
  ADD CONSTRAINT `C70314` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `DocumentExterne`
--
ALTER TABLE `DocumentExterne`
  ADD CONSTRAINT `DocumentExterne_ibfk_3` FOREIGN KEY (`refConsultation`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `DocumentExterne_ibfk_4` FOREIGN KEY (`idBlob`) REFERENCES `blob` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EchangeDestinataire`
--
ALTER TABLE `EchangeDestinataire`
  ADD CONSTRAINT `EchangeDestinataire_ibfk_1` FOREIGN KEY (`id_echange`) REFERENCES `Echange` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `EchangePieceJointe`
--
ALTER TABLE `EchangePieceJointe`
  ADD CONSTRAINT `EchangePieceJointe_ibfk_1` FOREIGN KEY (`id_message`) REFERENCES `Echange` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `enchere`
--
ALTER TABLE `enchere`
  ADD CONSTRAINT `C191236` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EnchereEntreprisePmi`
--
ALTER TABLE `EnchereEntreprisePmi`
  ADD CONSTRAINT `EnchereEntreprisePmi_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EnchereFichierDetail`
--
ALTER TABLE `EnchereFichierDetail`
  ADD CONSTRAINT `0_3863305` FOREIGN KEY (`id_enchere`) REFERENCES `enchere` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EnchereOffre`
--
ALTER TABLE `EnchereOffre`
  ADD CONSTRAINT `EnchereOffre_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `EnchereOffre_ibfk_2` FOREIGN KEY (`idEnchereEntreprise`) REFERENCES `EnchereEntreprisePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EnchereOffreReference`
--
ALTER TABLE `EnchereOffreReference`
  ADD CONSTRAINT `EnchereOffreReference_ibfk_1` FOREIGN KEY (`idEnchereOffre`) REFERENCES `EnchereOffre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `EnchereOffreReference_ibfk_2` FOREIGN KEY (`idEnchereReference`) REFERENCES `EnchereReference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EncherePmi`
--
ALTER TABLE `EncherePmi`
  ADD CONSTRAINT `EncherePmi_ibfk_1` FOREIGN KEY (`idEntiteeAssociee`) REFERENCES `Service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EnchereReference`
--
ALTER TABLE `EnchereReference`
  ADD CONSTRAINT `EnchereReference_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EnchereTrancheBaremeReference`
--
ALTER TABLE `EnchereTrancheBaremeReference`
  ADD CONSTRAINT `EnchereTrancheBaremeReference_ibfk_1` FOREIGN KEY (`idReference`) REFERENCES `EnchereReference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EnchereTranchesBaremeNETC`
--
ALTER TABLE `EnchereTranchesBaremeNETC`
  ADD CONSTRAINT `EnchereTranchesBaremeNETC_ibfk_1` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `EnchereValeursInitiales`
--
ALTER TABLE `EnchereValeursInitiales`
  ADD CONSTRAINT `EnchereValeursInitiales_ibfk_1` FOREIGN KEY (`idEnchereEntreprise`) REFERENCES `EnchereEntreprisePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `EnchereValeursInitiales_ibfk_2` FOREIGN KEY (`idEnchereReference`) REFERENCES `EnchereReference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `enchere_entreprise`
--
ALTER TABLE `enchere_entreprise`
  ADD CONSTRAINT `C204014` FOREIGN KEY (`id_enchere`) REFERENCES `enchere` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Enveloppe`
--
ALTER TABLE `Enveloppe`
  ADD CONSTRAINT `C151452` FOREIGN KEY (`offre_id`) REFERENCES `Offre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `fichierEnveloppe`
--
ALTER TABLE `fichierEnveloppe`
  ADD CONSTRAINT `fichierEnveloppe_ibfk_1` FOREIGN KEY (`id_enveloppe`) REFERENCES `Enveloppe` (`id_enveloppe_electro`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Helios_piece_publicite`
--
ALTER TABLE `Helios_piece_publicite`
  ADD CONSTRAINT `Helios_piece_publicite_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Helios_pv_consultation`
--
ALTER TABLE `Helios_pv_consultation`
  ADD CONSTRAINT `Helios_pv_consultation_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Helios_rapport_prefet`
--
ALTER TABLE `Helios_rapport_prefet`
  ADD CONSTRAINT `Helios_rapport_prefet_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Helios_tableau_ar`
--
ALTER TABLE `Helios_tableau_ar`
  ADD CONSTRAINT `Helios_tableau_ar_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Helios_teletransmission`
--
ALTER TABLE `Helios_teletransmission`
  ADD CONSTRAINT `Helios_teletransmission_ibfk_1` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Helios_teletransmission_lot`
--
ALTER TABLE `Helios_teletransmission_lot`
  ADD CONSTRAINT `Helios_teletransmission_lot_ibfk_1` FOREIGN KEY (`id_teletransmission`) REFERENCES `Helios_teletransmission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `InterneConsultation`
--
ALTER TABLE `InterneConsultation`
  ADD CONSTRAINT `C44871` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `InterneConsultationSuiviSeul`
--
ALTER TABLE `InterneConsultationSuiviSeul`
  ADD CONSTRAINT `C55982` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Intervenant_Externe_Commission`
--
ALTER TABLE `Intervenant_Externe_Commission`
  ADD CONSTRAINT `fk_cao_ext` FOREIGN KEY (`id_intervenant_externe`) REFERENCES `Intervenant_Externe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_odj_exte_com` FOREIGN KEY (`id_commission`) REFERENCES `Commission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Intervenant_Ordre_Du_Jour`
--
ALTER TABLE `Intervenant_Ordre_Du_Jour`
  ADD CONSTRAINT `fk_odj_com` FOREIGN KEY (`id_ordre_du_jour`) REFERENCES `Ordre_Du_Jour` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Marche`
--
ALTER TABLE `Marche`
  ADD CONSTRAINT `Marche_ibfk_2` FOREIGN KEY (`numeroMarcheAnnee`) REFERENCES `MarcheAnnee` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Marche_ibfk_4` FOREIGN KEY (`natureMarche`) REFERENCES `CategorieConsultation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MarchePublie`
--
ALTER TABLE `MarchePublie`
  ADD CONSTRAINT `MarchePublie_ibfk_1` FOREIGN KEY (`numeroMarcheAnnee`) REFERENCES `MarcheAnnee` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Message`
--
ALTER TABLE `Message`
  ADD CONSTRAINT `C18541` FOREIGN KEY (`offre_id`) REFERENCES `Offre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Offre`
--
ALTER TABLE `Offre`
  ADD CONSTRAINT `C13748` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Offre_papier`
--
ALTER TABLE `Offre_papier`
  ADD CONSTRAINT `C14021` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Ordre_Du_Jour`
--
ALTER TABLE `Ordre_Du_Jour`
  ADD CONSTRAINT `fk_cao_odj` FOREIGN KEY (`id_commission`) REFERENCES `Commission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ProcedureEquivalence`
--
ALTER TABLE `ProcedureEquivalence`
  ADD CONSTRAINT `C9004554` FOREIGN KEY (`id_type_procedure`) REFERENCES `TypeProcedure` (`id_type_procedure`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Publicite`
--
ALTER TABLE `Publicite`
  ADD CONSTRAINT `C2204165` FOREIGN KEY (`consultation`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `QuestionDCE`
--
ALTER TABLE `QuestionDCE`
  ADD CONSTRAINT `C12658` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ReferentielFormXml`
--
ALTER TABLE `ReferentielFormXml`
  ADD CONSTRAINT `ReferentielFormXml_ibfk_1` FOREIGN KEY (`id_type_xml`) REFERENCES `ReferentielTypeXml` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Retrait_Papier`
--
ALTER TABLE `Retrait_Papier`
  ADD CONSTRAINT `C11021` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `RG`
--
ALTER TABLE `RG`
  ADD CONSTRAINT `C810254` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Service`
--
ALTER TABLE `Service`
  ADD CONSTRAINT `C16485` FOREIGN KEY (`id_acheteur_public`) REFERENCES `AcheteurPublic` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Telechargement`
--
ALTER TABLE `Telechargement`
  ADD CONSTRAINT `C10145` FOREIGN KEY (`consultation_ref`) REFERENCES `consultation` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `valeur_enchere`
--
ALTER TABLE `valeur_enchere`
  ADD CONSTRAINT `C2144621` FOREIGN KEY (`id_enchere_entreprise`) REFERENCES `enchere_entreprise` (`id_enchere_entreprise`) ON DELETE CASCADE ON UPDATE CASCADE;

SET FOREIGN_KEY_CHECKS=1;

COMMIT;
