-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 25 Juin 2021 à 09:47
-- Version du serveur :  10.3.28-MariaDB-1:10.3.28+maria~xenial-log
-- Version de PHP :  7.3.28-1+ubuntu16.04.1+deb.sury.org+1

START TRANSACTION;

--
-- Contenu de la table `AvisCao`
-- @todo : fait

INSERT IGNORE INTO `AvisCao` (id, libelle) VALUES
(1, 'Avis favorable'),
(2, 'Avis défavorable'),
(3, 'Sans suite');

--
-- Contenu de la table `CategorieConsultation`
-- @todo : fait

INSERT IGNORE INTO `CategorieConsultation` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `id_categorie_ANM`, `libelle_it`, `code`, `code_dume`) VALUES
('1', 'Travaux', 'Travaux', 'Works', 'Obras', NULL, NULL, NULL, NULL, '1', 'Lavori', NULL, '01'),
('2', 'Fournitures', 'Fournitures', 'Supplies', 'Suministros', NULL, NULL, NULL, NULL, '2', 'Forniture', NULL, '02'),
('3', 'Services', 'Services', 'Services', 'Servicios', NULL, NULL, NULL, NULL, '3', 'Servizi', NULL, '03');

--
-- Contenu de la table `ccag_applicable`
-- @todo : fait

INSERT IGNORE INTO `ccag_applicable` (`id_auto`, `id`, `organisme`, `libelle`) VALUES
(1, 1, '', 'FCS'),
(2, 2, '', 'PI'),
(3, 3, '', 'T');


--
-- Contenu de la table `DecisionPassationConsultation`
--

INSERT IGNORE INTO `DecisionPassationConsultation` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES
(1, 'Poursuite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Infructueux', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Sans suite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Contenu de la table `DecisionPassationMarcheAVenir`
-- @todo : fait


INSERT IGNORE INTO `DecisionPassationMarcheAVenir` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES
(1, 'Attribué', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Infructueux', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Sans suite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- Contenu de la table `EchangeFormat`
-- @todo : fait

INSERT IGNORE INTO `EchangeFormat` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES
(1, 'Echange plate-forme', '', '', '', '', '', '', '', ''),
(2, 'Courriel hors plate-forme', '', '', '', '', '', '', '', ''),
(3, 'Courrier recommandé', '', '', '', '', '', '', '', ''),
(4, 'Courrier simple', '', '', '', '', '', '', '', ''),
(5, 'Télécopie', '', '', '', '', '', '', '', ''),
(6, 'Autre', '', '', '', '', '', '', '', '');


--
-- Contenu de la table `EchangeTypeAR`
-- @todo : fait

INSERT IGNORE INTO `EchangeTypeAR` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES
(1, 'A préciser', '', '', '', '', '', '', '', ''),
(2, 'sans objet', '', '', '', '', '', '', '', ''),
(3, 'Non retiré', '', '', '', '', '', '', '', ''),
(4, 'Date et heure', '', '', '', '', '', '', '', ''),
(5, 'Télécopie', '', '', '', '', '', '', '', ''),
(6, 'Autre', '', '', '', '', '', '', '', '');

--
-- Contenu de la table `EchangeTypeMessage`
-- @todo : fait

INSERT IGNORE INTO `EchangeTypeMessage` (`id`, `code`, `corps`) VALUES
(1, 'DEFINE_COURRIER_NOTIFICATION', "Votre entreprise a été déclarée attributaire de la consultation citée en référence.\nVeuillez-trouver à titre de notification le marché signé par le Représentant du Pouvoir Adjudicateur.\nL\'Accusé de réception de ce message vaut notification officielle du marché.\n"),
(2, 'DEFINE_COURRIER_REJET', "Nous vous remercions d\'avoir répondu à la consultation citée en référence.\nNous sommes toutefois au regret de vous annoncer que votre réponse n\'a pas été retenue par le Représentant du Pouvoir Adjudicateur.\nCordialement,\n"),
(3, 'DEFINE_AVERTISSEMENT_MODIFICATION_CONSULTATION', "La consultation citée en référence a été modifiée.\nLes éléments modifiés sont : [à préciser au cas par cas]\nCordialement,\n"),
(4, 'DEFINE_COURRIER_LIBRE', "\nTEXTE COURRIER LIBRE\n"),
(5, 'DEFINE_COURRIER_DEMANDE_COMPLEMENT', "Nous vous remercions d\'avoir répondu à la consultation citée en référence.\nAprès analyse, il vous est demandé d\'apporter les précisions suivantes : [à préciser au cas par cas].\nLa réponse à ces questions peut se faire via l\'application, à partir de la page d\'accès à cette demande de complément.\nIl est nécessaire de disposer d\'un Compte entreprise sur l\'application pour accéder à cette réponse.\nCordialement,"),
(6, 'DEFINE_COURRIER_REPONSE_A_QUESTION', "TEXTE REPONSE A QUESTION\r\n"),
(7, 'DEFINE_AVERTISSEMENT_ANNULATION_CONSULTATION', "La consultation citée en référence a été annulée. Cordialement,"),
(9, 'DEFINE_COURRIER_ATTRIBUTION_DEMANDE_DEVIS', "Madame, Monsieur \r\n\r\nNous avons le plaisir de vous informer que votre entreprise a été déclarée attributaire de la consultation citée en référence.\r\n\r\nNéanmoins, cette attribution ne pourra devenir définitive que si vous nous faites parvenir, dans un délai de 5 JOURS OUVRABLES à compter de la réception du présent mail, les éléments énumérés dans le document joint.  \r\n\r\nCordialement,");


--
-- Contenu de la table `Fcsp_Mandataire`
-- @todo : fait

INSERT IGNORE INTO `Fcsp_Mandataire` ( `id`, `organisme`, `libelle`) VALUES
(1, "", "Mandataire UL - SOGEFI"),
(2, "", "Mandataire UO - SIPARC"),
(3, "", "Mandataire UO - RIALI"),
(1, "", "Mandataire UL - SOGEFI"),
(2, "", "Mandataire UO - SIPARC"),
(3, "", "Mandataire UO - RIALI"),
(1, "", "Mandataire UL - SOGEFI"),
(2, "", "Mandataire UO - SIPARC"),
(3, "", "Mandataire UO - RIALI"),
(1, "", "Mandataire UL - SOGEFI"),
(2, "", "Mandataire UO - SIPARC"),
(3, "", "Mandataire UO - RIALI"),
(1, "", "Mandataire UL - SOGEFI"),
(2, "", "Mandataire UO - SIPARC"),
(3, "", "Mandataire UO - RIALI"),
(1, "", "Mandataire UL - SOGEFI"),
(2, "", "Mandataire UO - SIPARC"),
(3, "", "Mandataire UO - RIALI"),
(1, "", "Mandataire UL - SOGEFI"),
(2, "", "Mandataire UO - SIPARC"),
(3, "", "Mandataire UO - RIALI"),
(1, "", "Mandataire UL - SOGEFI"),
(2, "", "Mandataire UO - SIPARC"),
(3, "", "Mandataire UO - RIALI"),
(1, "", "Mandataire UL - SOGEFI"),
(2, "", "Mandataire UO - SIPARC"),
(3, "", "Mandataire UO - RIALI");

--
-- Contenu de la table `nature_acte_juridique`
-- @todo : fait

INSERT IGNORE INTO `nature_acte_juridique` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES
(1, "1-Contrat initial", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, "2-Contrat complémentaire", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, "3-Contrat sur la base d'un accord cadre", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, "4-Marché de définition", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, "5-Autre", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- Contenu de la table `Referentiel`
-- @todo : fait

INSERT IGNORE INTO `Referentiel` VALUES
(1, 'Type organismes'),
(2, 'Tranche budgétaire'),
(3, 'pme pmi');


--
-- Contenu de la table `StatutEnveloppe`
-- @todo : fait
INSERT IGNORE INTO `StatutEnveloppe` (`id_statut`, `description`) VALUES
(1, 'FERMEE'),
(2, 'DEFINE_OUVERTE_EN_LIGNE'),
(3, 'DEFINE_OUVERTE_HORS_LIGNE'),
(4, 'DEFINE_REFUSEE'),
(5, 'OUVERTE'),
(6, 'DEFINE_SUPPRIMEE'),
(7, 'DEFINE_OUVERTE_A_DISTANCE'),
(8, 'DEFINE_EN_COURS_CHIFFREMENT'),
(9, 'DEFINE_EN_COURS_DECHIFFREMENT'),
(10, 'DEFINE_EN_ATTENTE_CHIFFREMENT'),
(11, 'DEFINE_EN_COURS_FERMETURE'),
(12, 'DEFINE_EN_ATTENTE_FERMETURE');


--
-- Contenu de la table `TypeAvenant`
-- @todo : fait

INSERT IGNORE INTO `TypeAvenant` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES
(1, "1-Sans incidence financière", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, "2-Avenant augmentant le montant initial", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, "3-avenant diminuant le montant initial", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- Contenu de la table `TypeProcedure`
-- @todo : fait

INSERT IGNORE INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `abbreviation`, `type_boamp`, `categorie_procedure`, `id_type_procedure_ANM`, `delai_alerte`, `mapa`, `consultation_transverse`, `code_recensement`, `abbreviation_portail_ANM`, `id_modele`, `libelle_type_procedure_it`, `value_binding_sub`) VALUES
(1, "Appel d'offres ouvert", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "AOO", 0, 1, 1, 0, "0", "0", NULL, "AOO", 0, NULL, "01_AOO"),
(2, "Appel d'offres restreint", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "AOR", 0, 2, 2, 0, "0", "0", NULL, "AOR", 0, NULL, "02_AOR"),
(3, "Procédure adaptée", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "MAPA", 0, 3, 7, 0, "1", "0", NULL, "MAPA", 0, NULL, "03_MAPA"),
(4, "Concours ouvert", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "COUV", 0, 4, 3, 0, "1", "0", NULL, "COUV", 0, NULL, "04_Concours"),
(5, "Concours restreint", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "CRES", 0, 5, 4, 0, "0", "0", NULL, "CRES", 0, NULL, "05_ConcoursRestreint"),
(7, "Dialogue compétitif", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "DCO", 0, 7, 6, 0, "0", "0", NULL, "DCO", 0, NULL, "06_DialogueCompetitif"),
(8, "Marché négocié", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "MN", 0, 8, 5, 0, "0", "0", NULL, "MN", 0, NULL, "08_MarcheNegocie"),
(9, "Marché négocié - Décret Spécifique Défense", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "DSD", 0, 9, 5, 0, "0", "0", NULL, "DSD", 0, NULL, "09_MarcheNegocieDefense"),
(10, "Contrat de Partenariat de l'Etat", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "CPE", 0, 10, 8, 0, "0", "0", NULL, "CPE", 0, NULL, "10_ContratPartenariat"),
(11, "Procédure article 3", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "ART3", 0, 11, 8, 0, "0", "0", NULL, "ART3", 0, NULL, "11_ProcedureArt3"),
(15, "Procédure autre", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "AUT", 0, 15, 8, 0, "0", "0", NULL, "AUT", 0, NULL, "12_Autre"),
(30, "Accord-Cadre - 1 Sélection des Attributaires (obsolète)", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "AC-PA", 1, 1, 1, 0, "0", "0", NULL, "AC-PA", 0, NULL, "13_AccordCadreSelection"),
(31, "Procédure de passation des marchés subséquents", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "AC-MS", 2, 2, 3, 0, "0", "0", NULL, "AC-MS", 0, NULL, "14_MarcheSubsequent"),
(32, "Système d'Acquisition Dynamique", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "SAD-A", 8, 4, 8, 0, "0", "0", NULL, "SAD-A", 0, NULL, "15_SAD"),
(33, "Procédure de passation des marchés spécifiques", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "SAD-MS", 8, 4, 8, 0, "0", "0", NULL, "SAD-MS", 0, NULL, "16_MarcheSpecifique"),
(34, "Procédure avec négociations", NULL, NULL, NULL, NULL, NULL, NULL, NULL, "PAN", 0, 1, 1, 0, "0", "0", NULL, "PCN", 0, NULL, "18_PCN");

--
-- Contenu de la table `CategorieINSEE`
-- @todo : fait

INSERT IGNORE INTO `CategorieINSEE` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES
('4', 'Personne morale de droit public soumise au droit commercial', '', '', '', '', '', '', '', ''),
('4.1', 'Établissement public ou régie à caractère industriel ou commercial', '', '', '', '', '', '', '', ''),
('4.1.10', 'Établissement public national à caractère industriel ou commercial doté d\'un comptable public', '', '', '', '', '', '', '', ''),
	('4.1.20', 'Établissement public national à caractère industriel ou commercial non doté d\'un comptable public', '', '', '', '', '', '', '', ''),
('4.1.30', 'Exploitant public', '', '', '', '', '', '', '', ''),
('4.1.40', 'Établissement public local à caractère industriel ou commercial', '', '', '', '', '', '', '', ''),
('4.1.50', 'Régie d\'une collectivité locale à caractère industriel ou commercial', '', '', '', '', '', '', '', ''),
	('4.1.60', 'Institution Banque de France', '', '', '', '', '', '', '', ''),
	('7', 'Personne morale et organisme soumis au droit administratif', '', '', '', '', '', '', '', ''),
	('7.1', 'Administration de l\'état', '', '', '', '', '', '', '', ''),
('7.1.11', 'Autorité constitutionnelle', '', '', '', '', '', '', '', ''),
('7.1.12', 'Autorité administrative indépendante', '', '', '', '', '', '', '', ''),
('7.1.13', 'Ministère', '', '', '', '', '', '', '', ''),
('7.1.20', 'Service central d\'un ministère', '', '', '', '', '', '', '', ''),
	('7.1.50', 'Service du ministère de la Défense', '', '', '', '', '', '', '', ''),
	('7.1.60', 'Service déconcentré à compétence nation. D\'un ministère (hors Défense)', '', '', '', '', '', '', '', ''),
('7.1.71', 'Service déconcentré de l\'État à compétence (inter) régionale', '', '', '', '', '', '', '', ''),
	('7.1.72', 'Service déconcentré de l\'État à compétence (inter) départementale', '', '', '', '', '', '', '', ''),
('7.1.79', '(Autre) Service déconcentré de l\'État à compétence territoriale', '', '', '', '', '', '', '', ''),
	('7.1.90', 'Ecole nationale non dotée de la personnalité morale', '', '', '', '', '', '', '', ''),
	('7.2', 'Collectivité territoriale', '', '', '', '', '', '', '', ''),
	('7.2.10', 'Commune', '', '', '', '', '', '', '', ''),
	('7.2.20', 'Département', '', '', '', '', '', '', '', ''),
	('7.2.25', 'Territoire d\'Outre-mer', '', '', '', '', '', '', '', ''),
('7.2.29', '(Autre) Collectivité territoriale', '', '', '', '', '', '', '', ''),
('7.2.30', 'Région', '', '', '', '', '', '', '', ''),
('7.3', 'Établissement public administratif', '', '', '', '', '', '', '', ''),
('7.3.12', 'Commune associée', '', '', '', '', '', '', '', ''),
('7.3.13', 'Section de commune', '', '', '', '', '', '', '', ''),
('7.3.14', 'Ensemble urbain', '', '', '', '', '', '', '', ''),
('7.3.21', 'Association syndicale autorisée', '', '', '', '', '', '', '', ''),
('7.3.22', 'Association foncière urbaine', '', '', '', '', '', '', '', ''),
('7.3.23', 'Association foncière de remembrement', '', '', '', '', '', '', '', ''),
('7.3.31', 'Établissement public local d\'enseignement', '', '', '', '', '', '', '', ''),
('7.3.41', 'Secteur de commune', '', '', '', '', '', '', '', ''),
	('7.3.42', 'District urbain', '', '', '', '', '', '', '', ''),
	('7.3.43', 'Communauté urbaine', '', '', '', '', '', '', '', ''),
	('7.3.45', 'Syndicat intercommunal à vocation multiple (SIVOM)', '', '', '', '', '', '', '', ''),
	('7.3.46', 'Communauté de communes', '', '', '', '', '', '', '', ''),
	('7.3.47', 'Communauté de villes', '', '', '', '', '', '', '', ''),
	('7.3.48', 'Communauté d\'agglomération', '', '', '', '', '', '', '', ''),
('7.3.49', 'Autre établissement public local de coopération non spécialisé ou entente', '', '', '', '', '', '', '', ''),
('7.3.51', 'Institution interdépartemental ou entente', '', '', '', '', '', '', '', ''),
('7.3.52', 'Institution interrégionale ou entente', '', '', '', '', '', '', '', ''),
('7.3.53', 'Syndicat intercommunal à vocation unique (SIVU)', '', '', '', '', '', '', '', ''),
('7.3.54', 'Syndicat mixte communal', '', '', '', '', '', '', '', ''),
('7.3.55', 'Autre syndicat mixte', '', '', '', '', '', '', '', ''),
('7.3.56', 'Commission syndicale pour la gestion des biens indivis des communes', '', '', '', '', '', '', '', ''),
('7.3.61', 'Centre communal d\'action sociale', '', '', '', '', '', '', '', ''),
	('7.3.62', 'Caisse des écoles', '', '', '', '', '', '', '', ''),
	('7.3.63', 'Caisse de crédit municipal', '', '', '', '', '', '', '', ''),
	('7.3.64', 'Établissement d\'hospitalisation', '', '', '', '', '', '', '', ''),
('7.3.65', 'Syndicat inter hospitalier', '', '', '', '', '', '', '', ''),
('7.3.66', 'Établissement public local social et médico-social', '', '', '', '', '', '', '', ''),
('7.3.71', 'Office public d\'habitation à loyer modéré (OPHLM)', '', '', '', '', '', '', '', ''),
	('7.3.72', 'Service départemental d\'incendie', '', '', '', '', '', '', '', ''),
('7.3.73', 'Établissement public local culturel', '', '', '', '', '', '', '', ''),
('7.3.79', '(Autre) Établissement public administratif local', '', '', '', '', '', '', '', ''),
('7.3.81', 'Organisme consulaire', '', '', '', '', '', '', '', ''),
('7.3.82', 'Établissement public national ayant fonction d\'administration centrale', '', '', '', '', '', '', '', ''),
	('7.3.83', 'Établissement public national à caractère scientifique culturel et professionnel', '', '', '', '', '', '', '', ''),
	('7.3.84', 'Autre établissement public national d\'enseignement', '', '', '', '', '', '', '', ''),
('7.3.85', 'Autre établissement public national administratif à compétence territoriale limitée', '', '', '', '', '', '', '', ''),
('7.3.89', 'Établissement public national à caractère administratif', '', '', '', '', '', '', '', ''),
('7.4', 'Autre personne morale de droit public administratif', '', '', '', '', '', '', '', ''),
('7.4.10', 'Groupement d\'intérêt public (GIP)', '', '', '', '', '', '', '', ''),
	('7.4.30', 'Établissement public des cultes d\'Alsace-Lorraine', '', '', '', '', '', '', '', ''),
('7.4.50', 'Cercle et foyer dans les armées', '', '', '', '', '', '', '', ''),
('7.4.90', 'Autre personne morale de droit administratif', '', '', '', '', '', '', '', '');

--
-- Contenu de la table `CG76_Domaine`
-- @todo : fait

INSERT IGNORE INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES
(1, 'Repères et outils fondamentaux prof.', 0),
(2, 'Management', 0),
(3, 'Affaires juridiques', 0),
(4, 'Gestion des ressources humaines', 0),
(5, 'Formation professionnelle et recherche', 0),
(6, 'Finances et gestion financière', 0),
(7, 'Informatique et systèmes d\'information', 0),
	(8, 'Europe et international', 0),
	(9, 'Citoyenneté et population', 0),
	(10, 'Social et santé', 0),
	(11, 'Enfance, éducation, jeunesse', 0),
	(12, 'Restauration', 0),
	(13, 'Culture, archives et documentation', 0),
	(14, 'Environnement', 0),
	(15, 'Hygiène et sécurité', 0),
	(16, 'Génie technique', 0),
	(17, 'Développement local', 0),
	(18, 'Sport', 0),
	(19, 'Positionnement', 0),
	(20, 'Tech. expression communication et relat.', 1),
	(21, 'Observation du milieu professionnel', 1),
	(22, 'Tech. administ.d\'organis° et secrétariat', 1),
(23, 'Connaissance de l\'environnement territorial', 1),
	(24, 'Savoirs de base intégration vie profess', 1),
	(25, 'Management stratégique', 2),
	(26, 'Management organisationnel', 2),
	(27, 'Management des équipes et des personnes', 2),
	(28, 'Approche général du management', 2),
	(29, 'Modes de gestion des services publics', 3),
	(30, 'Achat public, Marchés pub et contrats', 3),
	(31, 'Prévention juridique et contentieux', 3),
	(32, 'Approche générale du droit', 3),
	(33, 'Assurances', 3),
	(34, 'Prévention juridique et contentieux', 3),
	(35, 'Approche générale du droit', 3),
	(36, 'Gestion administrative et statutaire', 4),
	(37, 'Politiques de GRH', 4),
	(38, 'Conditions de travail', 4),
	(39, 'Relations sociales', 4),
	(40, 'Fonction formation', 4),
	(41, 'Approche générale de la GRH', 4),
	(42, 'Connais. Acteurs disp polit emploi form', 5),
	(43, 'Contrôle évaluation org form recherche', 5),
	(44, 'Instruc organis gest form prof et rech', 5),
	(45, 'Procédure budgétaire et comptable', 6),
	(46, 'Gestion et stratégie financière', 6),
	(47, 'Fiscalité et dotation de l\'Etat', 6),
(48, 'Approche générale des finances', 6),
(49, 'Stratégie de communication', 7),
(50, 'Techniques et outils de communication', 7),
(51, 'Appr géné de la communication institut', 7),
(52, 'Architecture et admin des syst d\'infos', 8),
	(53, 'Réseaux et télécommunication', 8),
	(54, 'Techniques informatiques', 8),
	(55, 'Bureautique et utilisation outils inform', 8),
	(56, 'Connaissances des institutions', 9),
	(57, 'Politiques régionales', 9),
	(58, 'Coopération décentralisée', 9),
	(59, 'Politiques sectorielles', 9),
	(60, 'Démographie', 10),
	(61, 'état civil', 10),
	(62, 'élections', 10),
	(63, 'gestion funéraire', 10),
	(64, 'affaires générales', 10),
	(65, 'Connais acteurs des dispos et polit', 11),
	(66, 'Protection de l\'enfance et de l\'adolesc.', 11),
	(67, 'Insertion et développement social', 11),
	(68, 'Accueil et accompagnement des pers âgées', 11),
	(69, 'Prévention et promotion de la santé', 11),
	(70, 'Méthodologie du travail social', 11),
	(71, 'Appr. gén. de l\'act. soc. et de la santé', 11),
(72, 'Accueil et accompagnement des pers handicapées', 11),
(73, 'Connais. acteurs dispositifs, politiques', 12),
(74, 'Accueil de l\'enfance et de l\'adolescence', 12),
(75, 'Animation enfance, jeunesse', 12),
(76, 'Organisation et production', 13),
(77, 'Hygiène et sécurité alimentaire', 13),
(78, 'Distribution et service', 13),
(79, 'Equilibre nutritionnel', 13),
(80, 'Approche génér. de la restauration', 13),
(81, 'Connais. act dispositifs, polit act cult', 14),
(82, 'Gestion et enrichissement des fonds', 14),
(83, 'Diffusion, promot, protection juridique', 14),
(84, 'Protect dév du patri architec et archéo', 14),
(85, 'Enseignement artistique', 14),
(86, 'Programmation et techniques du spectacle', 14),
(87, 'Tech de documentat et d\'information', 14),
	(88, 'Approche générale de l\'action culturelle', 14),
(89, 'Polit géné d\'environ et gest des ress.', 15),
	(90, 'Résorption des nuisances et pollutions', 15),
	(91, 'Production d\'eau potable', 15),
(92, 'Traitement des eaux usées', 15),
(93, 'Biologie, hygiène alim et santé animale', 15),
(94, 'Collecte, trait et gestion des déchets', 15),
(95, 'Connaissance des act.,des dispo.des pol.', 15),
(96, 'Maintenance des moyens techniques', 15),
(97, 'Prév et opér d\'incendie et de secours', 16),
	(98, 'Sûreté et sécurité dans la ville', 16),
	(99, 'Prév et gest des risques tech et nat.', 16),
	(100, 'Prévention et protection du public', 16),
	(101, 'Sécurité des agents au travail', 16),
	(102, 'Responsabilité du personnel d?encadrement ', 16),
	(103, 'Plan de prévention entreprises extérieures intervenantes', 16),
	(104, 'Travail sur écran', 16),
	(105, 'Fonction achat des équipements dans une logique d?ergonomie', 16),
	(106, 'Formation des ACMO', 16),
	(107, 'Formation des membres du CHS', 16),
	(108, 'Nuisances sonores', 16),
	(109, 'Equipement de protection individuelle (EPI)', 16),
	(110, 'Document unique', 16),
	(111, 'Prévention et Secours Civiques de niveau ?PSC1-', 16),
	(112, 'SST', 16),
	(113, 'SSIAP 1', 16),
	(114, 'Utilisation du CMSI et du SSI', 16),
	(115, 'Initiation à la lutte contre l?incendie', 16),
	(116, 'Habilitations électriques', 16),
	(117, 'CACES', 16),
	(118, 'Signalisation temporaire des zones d?intervention', 16),
	(119, 'Manutention de ponts roulants', 16),
	(120, 'Risque routier', 16),
	(121, 'FCOS', 16),
	(122, 'Permis Poids lourd', 16),
	(123, 'Soudure', 16),
	(124, 'Utilisation de la débroussailleuse', 16),
	(125, 'Utilisation de la tronçonneuse', 16),
	(126, 'Manutentions manuelles et mécaniques de charges', 16),
	(127, 'Montage et démontage des échafaudages, scènes, podiums', 16),
	(128, 'Utilisation des produits phyto sanitaires', 16),
	(129, 'Gestion des déchets industriels et ménagers', 16),
	(130, 'Maîtrise du risque infectieux en milieu médico social', 16),
	(131, 'Nettoyage et désinfection des locaux', 16),
	(132, 'Fonction achat des produits d?entretien', 16),
	(133, 'Gestion du stress', 16),
	(134, 'Gestion des phénomènes agressifs', 16),
	(135, 'HACCP', 16),
	(136, 'Hygiène des aliments', 16),
	(137, 'Architecture et construction', 17),
	(138, 'Infrastructures et réseaux', 17),
	(139, 'Espaces verts et paysage', 17),
	(140, 'Maint des bâtiments tt corps d\'état', 17),
(141, 'Maîtrise de l\'énergie', 17),
	(142, 'Maintenance des moyens techniques', 17),
	(143, 'Connais. acteurs disp politiq dév territ', 18),
	(144, 'Aménag, urbanisme et act foncière', 18),
	(145, 'Politique de l\'habitat logement social', 18),
(146, 'Action économique', 18),
(147, 'Développement touristique', 18),
(148, 'Déplacements et transports', 18),
(149, 'Connais. acteurs disp politiq sportives', 19),
(150, 'Enseignement des prat sportives', 19),
(151, 'Entretien des matériels sportifs', 19),
(152, 'Orientation formative, posit évaluation', 20),
(153, 'Remise à niveau', 20),
(154, 'Acquisition de connaissances', 20),
(155, 'Méthodo et entraînement aux épreuves', 20),
(156, 'Sensibilisation générale', 21),
(157, 'Marchés publics et développement durable', 21),
(158, 'Energies et approche environnementale', 21),
(159, 'Eco labels et certifications', 21);


--
-- Contenu de la table `configuration_plateforme`
-- @todo : fait

INSERT IGNORE INTO `configuration_plateforme` (`id_auto`, `code_cpv`, `multi_linguisme_entreprise`, `gestion_fournisseurs_docs_mes_sous_services`, `authenticate_inscrit_by_cert`, `authenticate_inscrit_by_login`, `base_qualifiee_entreprise_insee`, `gestion_boamp_mes_sous_services`, `gestion_bi_cle_mes_sous_services`, `nom_entreprise_toujours_visible`, `gestion_jal_mes_sous_services`, `choix_langue_affichage_consultation`, `compte_entreprise_donnees_complementaires`, `annuaire_entites_achat_visible_par_entreprise`, `affichage_recherche_avancee_agent_ac_sad_transversaux`, `encheres_entreprise`, `socle_interne`, `module_certificat`, `socle_externe_agent`, `afficher_image_organisme`, `socle_externe_entreprise`, `portail_defense_entreprise`, `compte_entreprise_province`, `compte_entreprise_telephone3`, `compte_entreprise_tax_prof`, `compte_entreprise_rcville`, `compte_entreprise_declaration_honneur`, `compte_entreprise_qualification`, `compte_entreprise_moyens_techniques`, `compte_entreprise_prestations_realisees`, `compte_entreprise_chiffre_affaire_production_biens_services`, `enveloppe_offre_technique`, `compte_inscrit_choix_profil`, `procedure_adaptee`, `compte_entreprise_siren`, `compte_entreprise_activation_inscription_par_agent`, `menu_entreprise_consultations_en_cours`, `compte_entreprise_capital_social`, `mail_activation_compte_inscrit_entreprise`, `decision_date_notification`, `decision_pmi_pme`, `decision_nature_prestations`, `decision_objet_marche`, `decision_note`, `decision_fiche_recensement`, `registre_papier_mail_obligatoire`, `menu_entreprise_indicateurs_cles`, `ajout_rpa_champ_email`, `ajout_rpa_champ_telephone`, `ajout_rpa_champ_fax`, `entreprise_poser_question_sans_pj`, `url_demarche_agent`, `url_demarche_entreprise`, `siret_detail_entite_achat`, `presence_elu`, `gerer_mon_service`, `depouillement_enveloppe_depend_RAT_enveloppe_precedente`, `consultation_adresse_retrais_dossiers`, `consultation_adresse_depot_offres`, `consultation_caution_provisoire`, `consultation_lieu_ouverture_plis`, `consultation_qualification`, `consultation_agrement`, `consultation_echantillons_demandes`, `consultation_reunion`, `consultation_visite_des_lieux`, `consultation_prix_acquisition`, `resultat_analyse_avant_decision`, `creation_inscrit_par_ates`, `consultation_variantes_autorisees`, `recherche_avancee_par_type_org`, `menu_agent_societes_exclues`, `recherche_avancee_par_domaines_activite`, `recherche_avancee_par_qualification`, `recherche_avancee_par_agrement`, `contact_administratif_dans_detail_consultation_cote_entreprise`, `consultation_pieces_dossiers`, `gerer_adresses_service`, `traduire_annonces`, `afficher_bloc_actions_dans_details_annonces`, `autoriser_une_seule_reponse_principale_par_entreprise`, `generation_avis`, `passation_appliquer_donnees_ensemble_lots`, `autre_annonce_extrait_pv`, `autre_annonce_rapport_achevement`, `ajout_fichier_joint_autre_annonce`, `consultation_mode_passation`, `compte_entreprise_identifiant_unique`, `gerer_certificats_agent`, `autre_annonce_programme_previsionnel`, `annuler_consultation`, `cfe_entreprise_accessible_par_agent`, `compte_entreprise_code_nace_referentiel`, `code_nut_lt_referentiel`, `lieux_execution`, `compte_entreprise_domaine_activite_lt_referentiel`, `consultation_domaines_activites_lt_referentiel`, `compte_entreprise_agrement_lt_referentiel`, `compte_entreprise_qualification_lt_referentiel`, `reponse_pas_a_pas`, `agent_controle_format_mot_de_passe`, `entreprise_validation_email_inscription`, `telecharger_dce_avec_authentification`, `authentification_basic`, `reglement_consultation`, `annonces_marches`, `cfe_date_fin_validite_obligatoire`, `associer_documents_cfe_consultation`, `compte_entreprise_region`, `compte_entreprise_telephone2`, `compte_entreprise_cnss`, `compte_entreprise_rcnum`, `compte_entreprise_domaine_activite`, `compte_inscrit_code_nic`, `compte_entreprise_code_ape`, `compte_entreprise_documents_commerciaux`, `compte_entreprise_agrement`, `compte_entreprise_moyens_humains`, `compte_entreprise_activite_domaine_defense`, `compte_entreprise_donnees_financieres`, `enveloppe_anonymat`, `publicite_format_xml`, `article_133_generation_pf`, `entreprise_repondre_consultation_apres_cloture`, `telechargement_outil_verif_horodatage`, `affichage_code_cpv`, `consultation_domaines_activites`, `statistiques_mesure_demat`, `publication_procure`, `menu_entreprise_toutes_les_consultations`, `compte_entreprise_cp_obligatoire`, `annuler_depot`, `traduire_entite_achat_arabe`, `traduire_organisme_arabe`, `decision_cp`, `decision_tranche_budgetaire`, `decision_classement`, `decision_afficher_detail_candidat_par_defaut`, `article_133_upload_fichier`, `multi_linguisme_agent`, `compte_entreprise_ifu`, `gestion_organisme_par_agent`, `utiliser_lucene`, `utiliser_page_html_lieux_execution`, `prado_validateur_format_date`, `prado_validateur_format_email`, `socle_externe_ppp`, `validation_format_champs_stricte`, `poser_question_necessite_authentification`, `autoriser_modif_profil_inscrit_ates`, `unicite_reference_consultation`, `registre_papier_rcnum_rcville_obligatoires`, `registre_papier_adresse_cp_ville_obligatoires`, `telecharger_dce_sans_identification`, `gestion_entreprise_par_agent`, `autoriser_caracteres_speciaux_dans_reference`, `inscription_libre_entreprise`, `afficher_code_service`, `authenticate_agent_by_login`, `authenticate_agent_by_cert`, `generer_acte_dengagement`, `entreprise_controle_format_mot_de_passe`, `autre_annonce_information`, `creer_autre_annonce`, `consultation_clause`, `panier_entreprise`, `parametrage_publicite_par_type_procedure`, `export_decision`, `regle_mise_en_ligne_par_entite_coordinatrice`, `gestion_newsletter`, `publicite_opoce`, `gestion_modeles_formulaire`, `gestion_adresses_facturation_JAL`, `publicite_marches_en_ligne`, `lieu_ouverture_plis_obligatoire`, `dossier_additif`, `type_marche`, `type_prestation`, `afficher_tjr_bloc_caracteristique_reponse`, `alerte_metier`, `bourse_a_la_sous_traitance`, `partager_consultation`, `annuaire_acheteurs_publics`, `entreprise_actions_groupees`, `publier_guides`, `recherche_auto_completion`, `statut_compte_entreprise`, `gestion_organismes`, `accueil_entreprise_personnalise`, `interface_module_sub`, `authentification_agent_multi_organismes`, `lieux_execution_carte`, `surcharge_referentiels`, `Mode_Restriction_RGS`, `autre_annonce_decision_resiliation`, `autre_annonce_synthese_rapport_audit`, `fiche_weka`, `generation_automatique_mdp_agent`, `generation_automatique_mdp_inscrit`, `liste_ac_rgs`, `liste_cons_org`, `marche_public_simplifie_entreprise`, `archive_par_lot`, `documents_reference`, `recherches_favorites`, `synchronisation_SGMAP`) VALUES
(1, '1', '0', '1', '0', '1', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '1', '0', '1', '1', '0', '1', '0', '1', '0', '1', '1', '1', '1', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '1', '0', '1', '1', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '1', '1', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

--
-- Contenu de la table `DocumentsAttaches`
-- @todo : fait

INSERT IGNORE INTO `DocumentsAttaches` (`id_document`, `nom_document`, `nom_document_fr`, `nom_document_es`, `nom_document_en`, `nom_document_su`, `nom_document_du`, `nom_document_cz`, `nom_document_ar`, `nom_document_it`) VALUES
(1, 'Certificats fiscaux 3666 volets 1 à 4', '', '', '', '', '', '', '', ''),
(2, 'Certificats sociaux', '', '', '', '', '', '', '', ''),
(3, 'K-BIS', '', '', '', '', '', '', '', ''),
(4, 'DC7', '', '', '', '', '', '', '', ''),
(5, "Attestation d'assurance", '', '', '', '', '', '', '', ''),
(6, 'Certificat de congés payés', '', '', '', '', '', '', '', '');


--
-- Contenu de la table `Entreprise`
-- @todo : fait
INSERT IGNORE INTO `Entreprise` (`id`, `admin_id`, `siren`, `repmetiers`, `nom`, `adresse`, `codepostal`, `villeadresse`, `paysadresse`, `email`, `taille`, `formejuridique`, `villeenregistrement`, `motifNonIndNum`, `ordreProfOuAgrement`, `dateConstSociete`, `nomOrgInscription`, `adrOrgInscription`, `dateConstAssoc`, `dateConstAssocEtrangere`, `nomPersonnePublique`, `nationalite`, `redressement`, `paysenregistrement`, `sirenEtranger`, `numAssoEtrangere`, `debutExerciceGlob1`, `finExerciceGlob1`, `debutExerciceGlob2`, `finExerciceGlob2`, `debutExerciceGlob3`, `finExerciceGlob3`, `ventesGlob1`, `ventesGlob2`, `ventesGlob3`, `biensGlob1`, `biensGlob2`, `biensGlob3`, `servicesGlob1`, `servicesGlob2`, `servicesGlob3`, `totalGlob1`, `totalGlob2`, `totalGlob3`, `codeape`, `libelle_ape`, `origine_compte`, `telephone`, `fax`, `site_internet`, `description_activite`, `activite_domaine_defense`, `annee_cloture_exercice1`, `annee_cloture_exercice2`, `annee_cloture_exercice3`, `effectif_moyen1`, `effectif_moyen2`, `effectif_moyen3`, `effectif_encadrement1`, `effectif_encadrement2`, `effectif_encadrement3`, `pme1`, `pme2`, `pme3`, `adresse2`, `nicSiege`, `acronyme_pays`, `date_creation`, `date_modification`, `id_initial`, `region`, `province`, `telephone2`, `telephone3`, `cnss`, `rc_num`, `rc_ville`, `domaines_activites`, `num_tax`, `documents_commerciaux`, `intitule_documents_commerciaux`, `taille_documents_commerciaux`, `qualification`, `agrement`, `moyens_technique`, `moyens_humains`, `compte_actif`, `capital_social`, `ifu`, `id_agent_createur`, `nom_agent`, `prenom_agent`, `adresses_electroniques`, `visible_bourse`, `type_collaboration`, `entreprise_EA`, `entreprise_SIAE`, `saisie_manuelle`) VALUES
(1, 0, '444914568', '', 'SOCIETEST', '231, rue Saint-Honoré', '75001', 'Paris', 'France', NULL, NULL, 'SA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FR', '', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '6202A', NULL, NULL, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '', '00019', 'FR', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '', '', 0, '', '', NULL, '0', NULL, '0', '0', '0'),
(2, 0, '313955866', '', 'ENTREDEMO', '83, rue Mederic', '50110', 'Cherbourg', 'France', NULL, NULL, 'SARL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FR', '', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '5630Z', NULL, NULL, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '', '00014', 'FR', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '', '', 0, '', '', NULL, '0', NULL, '0', '0', '0'),
(3, 0, '482281094', '', 'COMPANESSAI', '6, rue Emile Expert', '26000', 'Valence', 'France', NULL, NULL, 'SARL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FR', '', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7010Z', NULL, NULL, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '', '00011', 'FR', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '', '', 0, '', '', NULL, '0', NULL, '0', '0', '0');

--
-- Contenu de la table `EtatConsultation`
-- @todo : fait
INSERT IGNORE INTO `EtatConsultation` (`id_etat`, `code_etat`, `abreviation_etat`, `visible`) VALUES
(1, 'DEFINE_PREPARATION', 'P', 0),
(2, 'DEFINE_CONSULTATION', 'C', 0),
(3, 'DEFINE_OUVERTURE_ANALYSE', 'OA', 1),
(4, 'DEFINE_DECISION', 'D', 1),
(5, 'DEFINE_A_ARCHIVER', 'AA', 1),
(6, 'DEFINE_TEXT_ARCHIVE_REALISEE', 'AR', 1);

--
-- Contenu de la table `Faq_Entreprise`
-- @todo : fait
INSERT IGNORE INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`, `question_it`, `reponse_it`, `theme`, `ordre`) VALUES
(1, 'Que faire si l\'applet Java ne se charge pas ?', 'Que faire si l\'applet Java ne se charge pas ?', 'How to proceed if the Java applet does not load?', '¿Qué pasa si el applet de Java no se carga?', '<p>Vérifier que votre poste est conforme aux "Conditions d\'utilisation" du site qui présente les pré-requis d\'installation et de configuration des postes de travail (ex : version de la machine java, fichiers à installer, etc.).</p>', '<p>Vérifier que votre poste est conforme aux "Conditions d\'utilisation" du site qui présente les pré-requis d\'installation et de configuration des postes de travail (ex : version de la machine java, fichiers à installer, etc.).</p>', '<p>Make sure your equipment and system meet all the requirements defined in the "Terms of use" section (e.g.: Java version, file download, etc.).</p>', '<p>Compruebe que su puesto está en conformidad con "las condiciones de uso" del sitio que presenta los requisitos previos de instalación y configuración de los puestos de trabajo (por ejemplo, la versión de la máquina para instalar los archivos java, etc.).</p>', '', '', '', 0),
	(2, 'Que dois-je faire lorsque lors de la remise d\'une réponse, le message suivant s\'affiche "Les fichiers d\'autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d\'utilisation?" ?', 'Que dois-je faire lorsque lors de la remise d\'une réponse, le message suivant s\'affiche "Les fichiers d\'autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d\'utilisation?" ?', 'How to proceed if I get the following message while submitting a bid "Les fichiers d\'autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d\'utilisation"?', '¿Qué debo hacer cuando al momento de presentar una respuesta, el siguiente mensaje "Les fichiers d\'autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d\'utilisation" ?', '<p>Les "Conditions d\'utilisation" indiquent la marche à suivre pour installer ces fichiers manquants.</p>', '<p>Les "Conditions d\'utilisation" indiquent la marche à suivre pour installer ces fichiers manquants.</p>', '<p>You will find the procedure to follow in the "Terms of use" section in order to install the missing files.</p>', '<p>Las "Condiciones de utilización" muestran cómo instalar estos archivos que faltan.</p>', '', '', '', 0),
	(3, 'Que faire si j\'ai oublié mon identifiant ou mon mot de passe de connexion ?', 'Que faire si j\'ai oublié mon identifiant ou mon mot de passe de connexion ?', 'How to proceed if I have lost my ID and password?', '¿Qué sucede si olvido mi nombre de usuario o mi contraseña de inicio de sesión?', '<p>La fonction "Mot de passe oublié" vous permet de recevoir votre identifiant et votre nouveau mot de passe.</p>', '<p>La fonction "Mot de passe oublié" vous permet de recevoir votre identifiant et votre nouveau mot de passe.</p>', '<p>Click the "Forgotten password" link and fill in your e-mail address to retrieve your ID and password.</p>', '<p>La función "¿Olvidaste tu contraseña?" le permite recibir su login y contraseña.</p>', '', '', '', 0),
	(4, 'Que faire si je ne retrouve pas une consultation restreinte sur la PMI ?', 'Que faire si je ne retrouve pas une consultation restreinte sur la PMI ?', 'I cannot find a restricted tender on the PMI website. What can I do?', '¿Qué sucede si no se encuentra un procedimiento restringido?', '<p>Il est nécessaire d\'utiliser le moteur de recherche avancée avec la référence et le code d\'accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l\'assistance téléphonique.</p>', '<p>Il est nécessaire d\'utiliser le moteur de recherche avancée avec la référence et le code d\'accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l\'assistance téléphonique.</p>', '<p>Use the advanced search page to search the reference and the access code provided by the public entity.<br />You will then have access to the restricted tender.<br />If you encounter any problem, please contact our hotline.</p>', '<p>Es necesario utilizar la búsqueda avanzada con la referencia y el código de acceso transmitido por la autoridad contratante.<br />Usted tendrá acceso a los datos del procedimiento restringido.<br />En caso de dificultad, póngase en contacto con la línea directa.</p>', '', '', '', 0),
	(5, 'Est ce que l\'entreprise reçoit un AR quand elle pose une question via la plate-forme ?', 'Est ce que l\'entreprise reçoit un AR quand elle pose une question via la plate-forme ?', 'Does the company receive an AR (acknowledgement of receipt) when submitting a question on the platform?', '¿La empresa recibe un AR cuando ella hace una pregunta a través de la plataforma?', '<p>Non. Seuls les dépôts de plis donnent lieu à un accusé de réception.</p>', '<p>Non. Seuls les dépôts de plis donnent lieu à un accusé de réception.</p>', '<p>No. An acknowledgement of receipt is sent to those submitting a bid only.</p>', '<p>No. Sólo los depósitos de pliegues dan lugar a un recibo.</p>', '', '', '', 0),
	(6, 'Pourquoi je ne retrouve pas une consultation publique sur le portail ?', 'Pourquoi je ne retrouve pas une consultation publique sur le portail ?', 'Why can not I find a public tender on the portal?', '¿Por qué no puedo encontrar una consulta pública en el portal?', '<p>Veuillez vérifier que la date limite de remise des plis n\'est pas déjà passée. Si ce n\'est pas le cas, vérifiez votre requête ou assurez-vous que la consultation n\'a pas été retirée par le Pouvoir adjudicateur.</p>', '<p>Veuillez vérifier que la date limite de remise des plis n\'est pas déjà passée. Si ce n\'est pas le cas, vérifiez votre requête ou assurez-vous que la consultation n\'a pas été retirée par le Pouvoir adjudicateur.</p>', '<p>Please make sure the closing date has not passed. Otherwise, make sure the tender has not been removed by the public entity.</p>', '<p>Por favor, verifique que la fecha límite para la presentación de los pliegues no ha pasado. Si este no es el caso, compruebe su solicitud o asegúrese de que la consulta no ha sido retirada por la Autoridad contratante.</p>', '', '', '', 0),
	(7, 'Pourquoi n\'ai-je pas reçu d\'accusé de réception par courriel après mon dépôt de pli ?', 'Pourquoi n\'ai-je pas reçu d\'accusé de réception par courriel après mon dépôt de pli ?', 'Why haven\'t I received any AR by mail after I submitted my envelop?', '¿Por qué no recibo un acuse por correo después de la presentación de mi pliego?', '<p>L\'application vous envoie un AR par courriel lorsque l\'écran de confirmation de bonne réception de votre envoi s\'affiche.\r\nVérifier que votre adresse électronique a été correctement saisie dans votre compte sur la plate-forme, ou que ce courriel ne se trouve pas dans vos courriels indésirables.</p>', '<p>L\'application vous envoie un AR par courriel lorsque l\'écran de confirmation de bonne réception de votre envoi s\'affiche.\r\nVérifier que votre adresse électronique a été correctement saisie dans votre compte sur la plate-forme, ou que ce courriel ne se trouve pas dans vos courriels indésirables.</p>', '<p>You will receive an AR by mail once a confirmation message is displayed on screen. <br />Make sure the email address has been correctly entered in your account or check your spam folder.</p>', '<p>La aplicación le envía un AR por correo electrónico cuando la pantalla de confirmación de recibo de su envío aparece. <br />Compruebe que su dirección de correo electrónico está escrito correctamente en su cuenta en la plataforma, o que el correo electrónico no está en su correo basura.</p>', '', '', '', 0),
(8, 'Les enveloppes de candidature ou d\'offre peuvent-elles être envoyées au format PDF?', 'Les enveloppes de candidature ou d\'offre peuvent-elles être envoyées au format PDF?', 'Can the application /bid envelops be sent in a PDF format?', '¿Los sobres de candidatura u ofertas pueden ser enviados en formato PDF?', '<p>Non. Seul le format ZIP est accepté.</p>', '<p>Non. Seul le format ZIP est accepté.</p>', '<p>No. ZIP is the only format allowed.</p>', '<p>No. Sólo el formato ZIP es aceptado.</p>', '', '', '', 0);


--
-- Contenu de la table `formejuridique`
-- @todo : fait
INSERT IGNORE INTO `formejuridique` (`formejuridique`, `ordre`, `libelle_formejuridique`, `libelle_formejuridique_fr`, `libelle_formejuridique_en`, `libelle_formejuridique_es`, `libelle_formejuridique_su`, `libelle_formejuridique_du`, `libelle_formejuridique_cz`, `libelle_formejuridique_ar`, `libelle_formejuridique_it`) VALUES
('Autre', NULL, '', '', '', '', '', '', '', '', ''),
('EURL', NULL, '', '', '', '', '', '', '', '', ''),
('GIE', NULL, '', '', '', '', '', '', '', '', ''),
('SA', NULL, '', '', '', '', '', '', '', '', ''),
('SARL', NULL, '', '', '', '', '', '', '', '', ''),
('SAS', NULL, '', '', '', '', '', '', '', '', ''),
('SASU', NULL, '', '', '', '', '', '', '', '', ''),
('SNC', NULL, '', '', '', '', '', '', '', '', '');

--
-- Contenu de la table `GeolocalisationN0`
-- @todo : fait
INSERT IGNORE INTO `GeolocalisationN0` (`id`, `denomination`, `denomination_fr`, `denomination_en`, `denomination_es`, `type`, `libelle_selectionner`, `libelle_selectionner_fr`, `libelle_selectionner_en`, `libelle_selectionner_es`, `libelle_tous`, `libelle_tous_fr`, `libelle_tous_en`, `libelle_tous_es`, `libelle_Aucun`, `libelle_Aucun_fr`, `libelle_Aucun_en`, `libelle_Aucun_es`, `denomination_ar`, `libelle_selectionner_ar`, `libelle_tous_ar`, `libelle_Aucun_ar`, `denomination_su`, `libelle_selectionner_su`, `libelle_tous_su`, `libelle_Aucun_su`, `denomination_du`, `libelle_selectionner_du`, `libelle_tous_du`, `libelle_Aucun_du`, `denomination_cz`, `libelle_selectionner_cz`, `libelle_tous_cz`, `libelle_Aucun_cz`, `denomination_it`, `libelle_selectionner_it`, `libelle_tous_it`, `libelle_Aucun_it`) VALUES
(1, 'France métropolitaine', 'France métropolitaine', 'Metropolitan France', 'Francia Metropolitana', 2, 'Sélectionner par département(s)', 'Sélectionner par département(s)', 'Select by department(s)', 'Seleccionar por Departamento(s)', 'Tous les départements', 'Tous les départements', 'All departments', 'Todos los Departamentos', 'Aucun département', 'Aucun département', 'No Department', 'Ningún Departamento', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 'France - Collectivité(s) d\'Outre-Mer', 'France - Collectivité(s) d\'Outre-Mer', 'France - overseas community', 'Francia - Colectividade(s) de Ultra-Mar', 0, 'Sélectionner par collectivité(s)', 'Sélectionner par collectivité(s)', 'Select by community', 'Seleccionar por Colectividade(s)', 'Toutes les collectivités', 'Toutes les collectivités', 'All community', 'Todas las Colectividades', 'Aucune collectivité', 'Aucune collectivité', 'No community', 'Ninguna Colectividad', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 'Autre(s) pays', 'Autre(s) pays', 'Others countries', 'Otro(s) pais(es)', 1, 'Sélectionner par pays', 'Sélectionner par pays', 'Select by country', 'Seleccionar por pais(es)', 'Tous les pays', 'Tous les pays', 'All countries', 'Todos los pais(es)', 'Aucun pays', 'Aucun pays', 'No countries', 'Ningún pais', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

--
-- Contenu de la table `GeolocalisationN1`
-- @todo : fait
INSERT IGNORE INTO `GeolocalisationN1` (`id`, `id_geolocalisationN0`, `denomination1`, `denomination2`, `denomination1_ar`, `denomination2_ar`, `denomination1_fr`, `denomination2_fr`, `denomination1_en`, `denomination2_en`, `denomination1_es`, `denomination2_es`, `denomination1_su`, `denomination2_su`, `denomination1_du`, `denomination2_du`, `denomination1_cz`, `denomination2_cz`, `denomination1_it`, `denomination2_it`) VALUES
(40, 1, 'Alsace', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(41, 1, 'Aquitaine', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(42, 1, 'Auvergne', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(43, 1, 'Basse-Normandie', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(44, 1, 'Bourgogne', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(45, 1, 'Bretagne', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(46, 1, 'Centre', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(48, 1, 'Corse', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(49, 1, 'Franche-Comté', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(50, 1, 'Haute-Normandie', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(51, 1, 'Champagne-Ardenne', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(52, 1, 'Ile-de-France', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(53, 1, 'Languedoc-Roussillon', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(54, 1, 'Limousin', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(55, 1, 'Lorraine', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(56, 1, 'Midi-Pyrénées', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(57, 1, 'Nord-Pas-de-Calais', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(58, 1, 'Pays-de-la-Loire', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(59, 1, 'Picardie', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(60, 1, 'Poitou-Charentes', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(61, 1, 'Provence-Alpes Côte d\'Azur', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(62, 1, 'Rhône-Alpes', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(233, 2, 'Départements d\'Outre-Mer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(234, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(235, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(236, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(237, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(238, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(239, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(240, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(241, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(242, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(243, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(244, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(245, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(246, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(247, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(248, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(249, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(250, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(251, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(252, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(253, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(254, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(255, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(256, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(257, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(258, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(259, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(260, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(261, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(262, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(263, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(264, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(265, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(266, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(267, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(268, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(269, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(270, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(271, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(272, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(273, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(274, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(275, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(276, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(277, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(278, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(279, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(280, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(281, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(282, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(283, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(284, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(285, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(286, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(287, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(288, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(289, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(290, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(291, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(292, 3, 'Pays', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

--
-- Contenu de la table `GeolocalisationN2`
-- @todo : fait
--
-- Contenu de la table `GeolocalisationN2`
--

INSERT IGNORE INTO `GeolocalisationN2` (`id`, `id_geolocalisationN1`, `denomination1`, `denomination2`, `valeur_avec_sous_categorie`, `denomination1_ar`, `denomination2_ar`, `denomination1_fr`, `denomination2_fr`, `denomination1_en`, `denomination2_en`, `denomination1_es`, `denomination2_es`, `denomination1_su`, `denomination2_su`, `denomination1_du`, `denomination2_du`, `denomination1_cz`, `denomination2_cz`, `denomination1_it`, `denomination2_it`, `valeur_sub`, `code_interface`, `type_lieu`, `code_nuts`) VALUES
(214, 40, '(67) Bas-Rhin', '67', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '066', 'FR-67', '1', 'FRF11'),
(215, 40, '(68) Haut-Rhin', '68', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '067', 'FR-68', '1', 'FRF12'),
(216, 41, '(24) Dordogne', '24', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '023', 'FR-24', '1', 'FRI11'),
(217, 41, '(33) Gironde', '33', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '032', 'FR-33', '1', 'FRI12'),
(218, 41, '(40) Landes', '40', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '039', 'FR-40', '1', 'FRI13'),
(219, 41, '(47) Lot-et-Garonne', '47', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '046', 'FR-47', '1', 'FRI14'),
(220, 41, '(64) Pyrénées-Atlantiques', '64', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '063', 'FR-64', '1', 'FRI15'),
(221, 42, '(03) Allier', '03', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '003', 'FR-03', '1', 'FRK11'),
(222, 42, '(15) Cantal', '15', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '015', 'FR-15', '1', 'FRK12'),
(223, 42, '(43) Haute-Loire', '43', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '042', 'FR-43', '1', 'FRK13'),
(224, 42, '(63) Puy-de-Dôme', '63', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '062', 'FR-63', '1', 'FRK14'),
(225, 43, '(14) Calvados', '14', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '014', 'FR-14', '1', 'FRD11'),
(226, 43, '(50) Manche', '50', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '049', 'FR-50', '1', 'FRD12'),
(227, 43, '(61) Orne', '61', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '060', 'FR-61', '1', 'FRD13'),
(228, 44, '(21) Côte-d\'Or', '21', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '020', 'FR-21', '1', 'FRC11'),
(229, 44, '(58) Nièvre', '58', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '057', 'FR-58', '1', 'FRC12'),
(230, 44, '(71) Saône-et-Loire', '71', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '070', 'FR-71', '1', 'FRC13'),
(231, 44, '(89) Yonne', '89', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '088', 'FR-89', '1', 'FRC14'),
(232, 45, '(22) Côtes-d\'Armor', '22', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '021', 'FR-22', '1', 'FRH01'),
(233, 45, '(29) Finistère', '29', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '028', 'FR-29', '1', 'FRH02'),
(234, 45, '(35) Ille-et-Vilaine', '35', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '034', 'FR-35', '1', 'FRH03'),
(235, 45, '(56) Morbihan', '56', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '055', 'FR-56', '1', 'FRH04'),
(236, 46, '(18) Cher', '18', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '018', 'FR-18', '1', 'FRB01'),
(237, 46, '(28) Eure-et-Loir', '28', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '027', 'FR-28', '1', 'FRB02'),
(238, 46, '(36) Indre', '36', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '035', 'FR-36', '1', 'FRB03'),
(239, 46, '(37) Indre-et-Loire', '37', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '036', 'FR-37', '1', 'FRB04'),
(240, 46, '(41) Loir-et-Cher', '41', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '040', 'FR-41', '1', 'FRB05'),
(241, 46, '(45) Loiret', '45', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '044', 'FR-45', '1', 'FRB06'),
(242, 48, '(2A) Corse-du-Sud', '2A', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '095', 'FR-2A', '1', 'FRM01'),
(243, 48, '(2B) Haute-Corse', '2B', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '096', 'FR-2B', '1', 'FRM02'),
(244, 49, '(25) Doubs', '25', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '024', 'FR-25', '1', 'FRC21'),
(245, 49, '(39) Jura', '39', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '038', 'FR-39', '1', 'FRC22'),
(246, 49, '(70) Haute-Saône', '70', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '069', 'FR-70', '1', 'FRC23'),
(247, 49, '(90) Territoire-de-Belfort', '90', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '089', 'FR-90', '1', 'FRC24'),
(248, 50, '(27) Eure', '27', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '026', 'FR-27', '1', 'FRD21'),
(249, 50, '(76) Seine-Maritime', '76', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '075', 'FR-76', '1', 'FRD22'),
(250, 51, '(08) Ardennes', '08', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '008', 'FR-08', '1', 'FRF21'),
(251, 51, '(10) Aube', '10', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '010', 'FR-10', '1', 'FRF22'),
(252, 51, '(51) Marne', '51', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '050', 'FR-51', '1', 'FRF23'),
(253, 51, '(52) Haute-Marne', '52', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '051', 'FR-52', '1', 'FRF24'),
(254, 52, '(75) Paris', '75', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '074', 'FR-75', '1', 'FR101'),
(255, 52, '(77) Seine-et-Marne', '77', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '076', 'FR-77', '1', 'FR102'),
(256, 52, '(78) Yvelines', '78', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '077', 'FR-78', '1', 'FR103'),
(257, 52, '(91) Essonne', '91', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '090', 'FR-91', '1', 'FR104'),
(258, 52, '(92) Hauts-de-Seine', '92', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '091', 'FR-92', '1', 'FR105'),
(259, 52, '(93) Seine-Saint-Denis', '93', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '092', 'FR-93', '1', 'FR106'),
(260, 52, '(94) Val-de-Marne', '94', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '093', 'FR-94', '1', 'FR107'),
(261, 52, '(95) Val-d\'Oise', '95', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '094', 'FR-95', '1', 'FR108'),
(262, 53, '(11) Aude', '11', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '011', 'FR-11', '1', 'FRJ11'),
(263, 53, '(30) Gard', '30', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '029', 'FR-30', '1', 'FRJ12'),
(264, 53, '(34) Hérault', '34', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '033', 'FR-34', '1', 'FRJ13'),
(265, 53, '(48) Lozère', '48', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '047', 'FR-48', '1', 'FRJ14'),
(266, 53, '(66) Pyrénées-Orientales', '66', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '065', 'FR-66', '1', 'FRJ15'),
(267, 54, '(19) Corrèze', '19', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '019', 'FR-19', '1', 'FRI21'),
(268, 54, '(23) Creuse', '23', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '022', 'FR-23', '1', 'FRI22'),
(269, 54, '(87) Haute-Vienne', '87', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '086', 'FR-87', '1', 'FRI23'),
(270, 55, '(54) Meurthe-et-Moselle', '54', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '053', 'FR-54', '1', 'FRF31'),
(271, 55, '(55) Meuse', '55', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '054', 'FR-55', '1', 'FRF32'),
(272, 55, '(57) Moselle', '57', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '056', 'FR-57', '1', 'FRF33'),
(273, 55, '(88) Vosges', '88', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '087', 'FR-88', '1', 'FRF34'),
(274, 56, '(09) Ariège', '09', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '009', 'FR-09', '1', 'FRJ21'),
(275, 56, '(12) Aveyron', '12', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '012', 'FR-12', '1', 'FRJ22'),
(276, 56, '(31) Haute-Garonne', '31', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '030', 'FR-31', '1', 'FRJ23'),
(277, 56, '(32) Gers', '32', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '031', 'FR-32', '1', 'FRJ24'),
(278, 56, '(46) Lot', '46', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '045', 'FR-46', '1', 'FRJ25'),
(279, 56, '(65) Hautes-Pyrénées', '65', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '064', 'FR-65', '1', 'FRJ26'),
(280, 56, '(81) Tarn', '81', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '080', 'FR-81', '1', 'FRJ27'),
(281, 56, '(82) Tarn-et-Garonne', '82', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '081', 'FR-82', '1', 'FRJ28'),
(282, 57, '(59) Nord', '59', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '058', 'FR-59', '1', 'FRE11'),
(283, 57, '(62) Pas-de-Calais', '62', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '061', 'FR-62', '1', 'FRE12'),
(284, 58, '(44) Loire-Atlantique', '44', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '043', 'FR-44', '1', 'FRG01'),
(285, 58, '(49) Maine-et-Loire', '49', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '048', 'FR-49', '1', 'FRG02'),
(286, 58, '(53) Mayenne', '53', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '052', 'FR-53', '1', 'FRG03'),
(287, 58, '(72) Sarthe', '72', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '071', 'FR-72', '1', 'FRG04'),
(288, 58, '(85) Vendée', '85', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '084', 'FR-85', '1', 'FRG05'),
(289, 59, '(02) Aisne', '02', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '002', 'FR-02', '1', 'FRE21'),
(290, 59, '(60) Oise', '60', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '059', 'FR-60', '1', 'FRE22'),
(291, 59, '(80) Somme', '80', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '079', 'FR-80', '1', 'FRE23'),
(292, 60, '(16) Charente', '16', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '016', 'FR-16', '1', 'FRI31'),
(293, 60, '(17) Charente-Maritime', '17', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '017', 'FR-17', '1', 'FRI32'),
(294, 60, '(79) Deux-Sèvres', '79', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '078', 'FR-79', '1', 'FRI33'),
(295, 60, '(86) Vienne', '86', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '085', 'FR-86', '1', 'FRI34'),
(296, 61, '(04) Alpes-de-Haute-Provence', '04', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '004', 'FR-04', '1', 'FRL01'),
(297, 61, '(05) Hautes-Alpes', '05', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '005', 'FR-05', '1', 'FRL02'),
(298, 61, '(06) Alpes-Maritimes', '06', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '006', 'FR-06', '1', 'FRL03'),
(299, 61, '(13) Bouches-du-Rhône', '13', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '013', 'FR-13', '1', 'FRL04'),
(300, 61, '(83) Var', '83', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '082', 'FR-83', '1', 'FRL05'),
(301, 61, '(84) Vaucluse', '84', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '083', 'FR-84', '1', 'FRL06'),
(302, 62, '(01) Ain', '01', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '001', 'FR-01', '1', 'FRK21'),
(303, 62, '(07) Ardèche', '07', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '007', 'FR-07', '1', 'FRK22'),
(304, 62, '(26) Drôme', '26', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '025', 'FR-26', '1', 'FRK23'),
(305, 62, '(38) Isère', '38', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '037', 'FR-38', '1', 'FRK24'),
(306, 62, '(42) Loire', '42', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '041', 'FR-42', '1', 'FRK25'),
(307, 62, '(69) Rhône', '69', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '068', 'FR-69', '1', 'FRK26'),
(308, 62, '(73) Savoie', '73', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '072', 'FR-73', '1', 'FRK27'),
(309, 62, '(74) Haute-Savoie', '74', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '073', 'FR-74', '1', 'FRK28'),
(713, 233, 'GUADELOUPE', 'GP', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '097', 'FR-GP', '', 'FRY10'),
(714, 233, 'GUYANE FRANÇAISE', 'GF', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '099', 'FR-GF', '', NULL),
(715, 233, 'MARTINIQUE', 'MQ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '098', 'FR-MQ', '', 'FRY2'),
(716, 233, 'MAYOTTE', 'YT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '20B', 'FR-YT', '', NULL),
(717, 233, 'NOUVELLE-CALÉDONIE', 'NC', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NC', '', NULL),
(718, 233, 'POLYNÉSIE FRANÇAISE', 'PF', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PF', '', NULL),
(719, 233, 'RÉUNION', 'RE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '20A', 'FR-RE', '', 'FRY4'),
(720, 233, 'SAINT-BARTHÉLEMY', 'BL', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FR-BL', '', NULL),
(721, 233, 'SAINT-MARTIN', 'MF', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FR-MF', '', NULL),
(722, 233, 'SAINT-PIERRE-ET-MIQUELON', 'PM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FR-PM', '', NULL),
(723, 233, 'TERRES AUSTRALES FRANÇAISES', 'TF', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FR-TF', '', NULL),
(724, 233, 'WALLIS ET FUTUNA', 'WF', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FR-WF', '', NULL),
(6235, 234, 'AFGHANISTAN', 'AF', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AF', '', NULL),
(6236, 234, 'AFRIQUE DU SUD', 'ZA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ZA', '', NULL),
(6237, 234, 'ÂLAND, ÎLES', 'AX', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AX', '', NULL),
(6238, 234, 'ALBANIE', 'AL', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AL', '', NULL),
(6239, 234, 'ALGÉRIE', 'DZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'DZ', '', NULL),
(6240, 234, 'ALLEMAGNE', 'DE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'DE', '', NULL),
(6241, 234, 'ANDORRE', 'AD', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AD', '', NULL),
(6242, 234, 'ANGOLA', 'AO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AO', '', NULL),
(6243, 234, 'ANGUILLA', 'AI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AI', '', NULL),
(6244, 234, 'ANTARCTIQUE', 'AQ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AQ', '', NULL),
(6245, 234, 'ANTIGUA-ET-BARBUDA', 'AG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AG', '', NULL),
(6246, 234, 'ANTILLES NÉERLANDAISES', 'AN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AN', '', NULL),
(6247, 234, 'ARABIE SAOUDITE', 'SA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SA', '', NULL),
(6248, 234, 'ARGENTINE', 'AR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AR', '', NULL),
(6249, 234, 'ARMÉNIE', 'AM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AM', '', NULL),
(6250, 234, 'ARUBA', 'AW', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AW', '', NULL),
(6251, 234, 'AUSTRALIE', 'AU', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AU', '', NULL),
(6252, 234, 'AUTRICHE', 'AT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AT', '', NULL),
(6253, 234, 'AZERBAÏDJAN', 'AZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AZ', '', NULL),
(6254, 234, 'BAHAMAS', 'BS', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BS', '', NULL),
(6255, 234, 'BAHREÏN', 'BH', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BH', '', NULL),
(6256, 234, 'BANGLADESH', 'BD', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BD', '', NULL),
(6257, 234, 'BARBADE', 'BB', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BB', '', NULL),
(6258, 234, 'BÉLARUS', 'BY', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BY', '', NULL),
(6259, 234, 'BELGIQUE', 'BE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BE', '', NULL),
(6260, 234, 'BELIZE', 'BZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BZ', '', NULL),
(6261, 234, 'BÉNIN', 'BJ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BJ', '', NULL),
(6262, 234, 'BERMUDES', 'BM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BM', '', NULL),
(6263, 234, 'BHOUTAN', 'BT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BT', '', NULL),
(6264, 234, 'BOLIVIE', 'BO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BO', '', NULL),
(6265, 234, 'BOSNIE-HERZÉGOVINE', 'BA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BA', '', NULL),
(6266, 234, 'BOTSWANA', 'BW', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BW', '', NULL),
(6267, 234, 'BOUVET, ÎLE', 'BV', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BV', '', NULL),
(6268, 234, 'BRÉSIL', 'BR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BR', '', NULL),
(6269, 234, 'BRUNÉI DARUSSALAM', 'BN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BN', '', NULL),
(6270, 234, 'BULGARIE', 'BG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BG', '', NULL),
(6271, 234, 'BURKINA FASO', 'BF', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BF', '', NULL),
(6272, 234, 'BURUNDI', 'BI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'BI', '', NULL),
(6273, 234, 'CAÏMANES, ÎLES', 'KY', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KY', '', NULL),
(6274, 234, 'CAMBODGE', 'KH', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KH', '', NULL),
(6275, 234, 'CAMEROUN', 'CM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CM', '', NULL),
(6276, 234, 'CANADA', 'CA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CA', '', NULL),
(6277, 234, 'CAP-VERT', 'CV', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CV', '', NULL),
(6278, 234, 'CENTRAFRICAINE, RÉPUBLIQUE', 'CF', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CF', '', NULL),
(6279, 234, 'CHILI', 'CL', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CL', '', NULL),
(6280, 234, 'CHINE', 'CN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CN', '', NULL),
(6281, 234, 'CHRISTMAS, ÎLE', 'CX', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CX', '', NULL),
(6282, 234, 'CHYPRE', 'CY', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CY', '', NULL),
(6283, 234, 'COCOS (KEELING), ÎLES', 'CC', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CC', '', NULL),
(6284, 234, 'COLOMBIE', 'CO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CO', '', NULL),
(6285, 234, 'COMORES', 'KM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KM', '', NULL),
(6286, 234, 'CONGO', 'CG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CG', '', NULL),
(6287, 234, 'CONGO, LA RÉPUBLIQUE DÉMOCRATIQUE DU', 'CD', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CD', '', NULL),
(6288, 234, 'COOK, ÎLES', 'CK', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CK', '', NULL),
(6289, 234, 'CORÉE, RÉPUBLIQUE DE', 'KR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KR', '', NULL),
(6290, 234, 'CORÉE, RÉPUBLIQUE POPULAIRE DÉMOCRATIQUE DE', 'KP', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KP', '', NULL),
(6291, 234, 'COSTA RICA', 'CR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CR', '', NULL),
(6292, 234, 'CÔTE D\'IVOIRE', 'CI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CI', '', NULL),
(6293, 234, 'CROATIE', 'HR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'HR', '', NULL),
(6294, 234, 'CUBA', 'CU', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CU', '', NULL),
(6295, 234, 'DANEMARK', 'DK', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'DK', '', NULL),
(6296, 234, 'DJIBOUTI', 'DJ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'DJ', '', NULL),
(6297, 234, 'DOMINICAINE, RÉPUBLIQUE', 'DO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'DO', '', NULL),
(6298, 234, 'DOMINIQUE', 'DM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'DM', '', NULL),
(6299, 234, 'ÉGYPTE', 'EG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'EG', '', NULL),
(6300, 234, 'EL SALVADOR', 'SV', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SV', '', NULL),
(6301, 234, 'ÉMIRATS ARABES UNIS', 'AE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AE', '', NULL),
(6302, 234, 'ÉQUATEUR', 'EC', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'EC', '', NULL),
(6303, 234, 'ÉRYTHRÉE', 'ER', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ER', '', NULL),
(6304, 234, 'ESPAGNE', 'ES', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ES', '', NULL),
(6305, 234, 'ESTONIE', 'EE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'EE', '', NULL),
(6306, 234, 'ÉTATS-UNIS', 'US', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'US', '', NULL),
(6307, 234, 'ÉTHIOPIE', 'ET', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ET', '', NULL),
(6308, 234, 'FALKLAND, ÎLES (MALVINAS)', 'FK', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FK', '', NULL),
(6309, 234, 'FÉROÉ, ÎLES', 'FO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FO', '', NULL),
(6310, 234, 'FIDJI', 'FJ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FJ', '', NULL),
(6311, 234, 'FINLANDE', 'FI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FI', '', NULL),
(6313, 234, 'GABON', 'GA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GA', '', NULL),
(6314, 234, 'GAMBIE', 'GM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GM', '', NULL),
(6315, 234, 'GÉORGIE', 'GE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GE', '', NULL),
(6316, 234, 'GÉORGIE DU SUD ET LES ÎLES SANDWICH DU SUD', 'GS', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GS', '', NULL),
(6317, 234, 'GHANA', 'GH', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GH', '', NULL),
(6318, 234, 'GIBRALTAR', 'GI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GI', '', NULL),
(6319, 234, 'GRÈCE', 'GR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GR', '', NULL),
(6320, 234, 'GRENADE', 'GD', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GD', '', NULL),
(6321, 234, 'GROENLAND', 'GL', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GL', '', NULL),
(6322, 234, 'GUAM', 'GU', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GU', '', NULL),
(6323, 234, 'GUATEMALA', 'GT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GT', '', NULL),
(6324, 234, 'GUERNESEY', 'GG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GG', '', NULL),
(6325, 234, 'GUINÉE', 'GN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GN', '', NULL),
(6326, 234, 'GUINÉE-BISSAU', 'GW', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GW', '', NULL),
(6327, 234, 'GUINÉE ÉQUATORIALE', 'GQ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GQ', '', NULL),
(6328, 234, 'GUYANA', 'GY', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GY', '', NULL),
(6329, 234, 'HAÏTI', 'HT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'HT', '', NULL),
(6330, 234, 'HEARD, ÎLE ET MCDONALD, ÎLES', 'HM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'HM', '', NULL),
(6331, 234, 'HONDURAS', 'HN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'HN', '', NULL),
(6332, 234, 'HONG-KONG', 'HK', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'HK', '', NULL),
(6333, 234, 'HONGRIE', 'HU', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'HU', '', NULL),
(6334, 234, 'ÎLE DE MAN', 'IM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IM', '', NULL),
(6335, 234, 'ÎLES MINEURES ÉLOIGNÉES DES ÉTATS-UNIS', 'UM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'UM', '', NULL),
(6336, 234, 'ÎLES VIERGES BRITANNIQUES', 'VG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'VG', '', NULL),
(6337, 234, 'ÎLES VIERGES DES ÉTATS-UNIS', 'VI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'VI', '', NULL),
(6338, 234, 'INDE', 'IN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IN', '', NULL),
(6339, 234, 'INDONÉSIE', 'ID', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ID', '', NULL),
(6340, 234, 'IRAN, RÉPUBLIQUE ISLAMIQUE D\'', 'IR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IR', '', NULL),
(6341, 234, 'IRAQ', 'IQ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IQ', '', NULL),
(6342, 234, 'IRLANDE', 'IE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IE', '', NULL),
(6343, 234, 'ISLANDE', 'IS', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IS', '', NULL),
(6344, 234, 'ISRAËL', 'IL', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IL', '', NULL),
(6345, 234, 'ITALIE', 'IT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IT', '', NULL),
(6346, 234, 'JAMAÏQUE', 'JM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'JM', '', NULL),
(6347, 234, 'JAPON', 'JP', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'JP', '', NULL),
(6348, 234, 'JERSEY', 'JE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'JE', '', NULL),
(6349, 234, 'JORDANIE', 'JO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'JO', '', NULL),
(6350, 234, 'KAZAKHSTAN', 'KZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KZ', '', NULL),
(6351, 234, 'KENYA', 'KE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KE', '', NULL),
(6352, 234, 'KIRGHIZISTAN', 'KG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KG', '', NULL),
(6353, 234, 'KIRIBATI', 'KI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KI', '', NULL),
(6354, 234, 'KOWEÏT', 'KW', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KW', '', NULL),
(6355, 234, 'LAO, RÉPUBLIQUE DÉMOCRATIQUE POPULAIRE', 'LA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LA', '', NULL),
(6356, 234, 'LESOTHO', 'LS', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LS', '', NULL),
(6357, 234, 'LETTONIE', 'LV', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LV', '', NULL),
(6358, 234, 'LIBAN', 'LB', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LB', '', NULL),
(6359, 234, 'LIBÉRIA', 'LR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LR', '', NULL),
(6360, 234, 'LIBYENNE, JAMAHIRIYA ARABE', 'LY', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LY', '', NULL),
(6361, 234, 'LIECHTENSTEIN', 'LI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LI', '', NULL),
(6362, 234, 'LITUANIE', 'LT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LT', '', NULL),
(6363, 234, 'LUXEMBOURG', 'LU', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LU', '', NULL),
(6364, 234, 'MACAO', 'MO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MO', '', NULL),
(6365, 234, 'MACÉDOINE, L\'EX-RÉPUBLIQUE YOUGOSLAVE DE', 'MK', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MK', '', NULL),
(6366, 234, 'MADAGASCAR', 'MG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MG', '', NULL),
(6367, 234, 'MALAISIE', 'MY', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MY', '', NULL),
(6368, 234, 'MALAWI', 'MW', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MW', '', NULL),
(6369, 234, 'MALDIVES', 'MV', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MV', '', NULL),
(6370, 234, 'MALI', 'ML', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ML', '', NULL),
(6371, 234, 'MALTE', 'MT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MT', '', NULL),
(6372, 234, 'MARIANNES DU NORD, ÎLES', 'MP', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MP', '', NULL),
(6373, 234, 'MAROC', 'MA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MA', '', NULL),
(6374, 234, 'MARSHALL, ÎLES', 'MH', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MH', '', NULL),
(6375, 234, 'MAURICE', 'MU', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MU', '', NULL),
(6376, 234, 'MAURITANIE', 'MR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MR', '', NULL),
(6377, 234, 'MEXIQUE', 'MX', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MX', '', NULL),
(6378, 234, 'MICRONÉSIE, ÉTATS FÉDÉRÉS DE', 'FM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FM', '', NULL),
(6379, 234, 'MOLDOVA, RÉPUBLIQUE DE', 'MD', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MD', '', NULL),
(6380, 234, 'MONACO', 'MC', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MC', '', NULL),
(6381, 234, 'MONGOLIE', 'MN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MN', '', NULL),
(6382, 234, 'MONTÉNÉGRO', 'ME', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ME', '', NULL),
(6383, 234, 'MONTSERRAT', 'MS', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MS', '', NULL),
(6384, 234, 'MOZAMBIQUE', 'MZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MZ', '', NULL),
(6385, 234, 'MYANMAR', 'MM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'MM', '', NULL),
(6386, 234, 'NAMIBIE', 'NA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NA', '', NULL),
(6387, 234, 'NAURU', 'NR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NR', '', NULL),
(6388, 234, 'NÉPAL', 'NP', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NP', '', NULL),
(6389, 234, 'NICARAGUA', 'NI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NI', '', NULL),
(6390, 234, 'NIGER', 'NE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NE', '', NULL),
(6391, 234, 'NIGÉRIA', 'NG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NG', '', NULL),
(6392, 234, 'NIUÉ', 'NU', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NU', '', NULL),
(6393, 234, 'NORFOLK, ÎLE', 'NF', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NF', '', NULL),
(6394, 234, 'NORVÈGE', 'NO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NO', '', NULL),
(6395, 234, 'NOUVELLE-ZÉLANDE', 'NZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NZ', '', NULL),
(6396, 234, 'OCÉAN INDIEN, TERRITOIRE BRITANNIQUE DE L\'', 'IO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'IO', '', NULL),
(6397, 234, 'OMAN', 'OM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'OM', '', NULL),
(6398, 234, 'OUGANDA', 'UG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'UG', '', NULL),
(6399, 234, 'OUZBÉKISTAN', 'UZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'UZ', '', NULL),
(6400, 234, 'PAKISTAN', 'PK', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PK', '', NULL),
(6401, 234, 'PALAOS', 'PW', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PW', '', NULL),
(6402, 234, 'PALESTINIEN OCCUPÉ, TERRITOIRE', 'PS', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PS', '', NULL),
(6403, 234, 'PANAMA', 'PA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PA', '', NULL),
(6404, 234, 'PAPOUASIE-NOUVELLE-GUINÉE', 'PG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PG', '', NULL),
(6405, 234, 'PARAGUAY', 'PY', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PY', '', NULL),
(6406, 234, 'PAYS-BAS', 'NL', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'NL', '', NULL),
(6407, 234, 'PÉROU', 'PE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PE', '', NULL),
(6408, 234, 'PHILIPPINES', 'PH', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PH', '', NULL),
(6409, 234, 'PITCAIRN', 'PN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PN', '', NULL),
(6410, 234, 'POLOGNE', 'PL', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PL', '', NULL),
(6411, 234, 'PORTO RICO', 'PR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PR', '', NULL),
(6412, 234, 'PORTUGAL', 'PT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PT', '', NULL),
(6413, 234, 'QATAR', 'QA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'QA', '', NULL),
(6414, 234, 'ROUMANIE', 'RO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'RO', '', NULL),
(6415, 234, 'ROYAUME-UNI', 'GB', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'GB', '', NULL),
(6416, 234, 'RUSSIE, FÉDÉRATION DE', 'RU', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'RU', '', NULL),
(6417, 234, 'RWANDA', 'RW', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'RW', '', NULL),
(6418, 234, 'SAHARA OCCIDENTAL', 'EH', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'EH', '', NULL),
(6419, 234, 'SAINTE-HÉLÈNE', 'SH', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SH', '', NULL),
(6420, 234, 'SAINTE-LUCIE', 'LC', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LC', '', NULL),
(6421, 234, 'SAINT-KITTS-ET-NEVIS', 'KN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'KN', '', NULL),
(6422, 234, 'SAINT-MARIN', 'SM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SM', '', NULL),
(6423, 234, 'SAINT-SIÈGE (ÉTAT DE LA CITÉ DU VATICAN)', 'VA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'VA', '', NULL),
(6424, 234, 'SAINT-VINCENT-ET-LES GRENADINES', 'VC', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'VC', '', NULL),
(6425, 234, 'SALOMON, ÎLES', 'SB', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SB', '', NULL),
(6426, 234, 'SAMOA', 'WS', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'WS', '', NULL),
(6427, 234, 'SAMOA AMÉRICAINES', 'AS', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'AS', '', NULL),
(6428, 234, 'SAO TOMÉ-ET-PRINCIPE', 'ST', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ST', '', NULL),
(6429, 234, 'SÉNÉGAL', 'SN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SN', '', NULL),
(6430, 234, 'SERBIE', 'RS', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'RS', '', NULL),
(6431, 234, 'SEYCHELLES', 'SC', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SC', '', NULL),
(6432, 234, 'SIERRA LEONE', 'SL', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SL', '', NULL),
(6433, 234, 'SINGAPOUR', 'SG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SG', '', NULL),
(6434, 234, 'SLOVAQUIE', 'SK', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SK', '', NULL),
(6435, 234, 'SLOVÉNIE', 'SI', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SI', '', NULL),
(6436, 234, 'SOMALIE', 'SO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SO', '', NULL),
(6437, 234, 'SOUDAN', 'SD', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SD', '', NULL),
(6438, 234, 'SRI LANKA', 'LK', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'LK', '', NULL),
(6439, 234, 'SUÈDE', 'SE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SE', '', NULL),
(6440, 234, 'SUISSE', 'CH', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CH', '', NULL),
(6441, 234, 'SURINAME', 'SR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SR', '', NULL),
(6442, 234, 'SVALBARD ET ÎLE JAN MAYEN', 'SJ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SJ', '', NULL),
(6443, 234, 'SWAZILAND', 'SZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SZ', '', NULL),
(6444, 234, 'SYRIENNE, RÉPUBLIQUE ARABE', 'SY', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'SY', '', NULL),
(6445, 234, 'TADJIKISTAN', 'TJ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TJ', '', NULL),
(6446, 234, 'TAÏWAN, PROVINCE DE CHINE', 'TW', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TW', '', NULL),
(6447, 234, 'TANZANIE, RÉPUBLIQUE-UNIE DE', 'TZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TZ', '', NULL),
(6448, 234, 'TCHAD', 'TD', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TD', '', NULL),
(6449, 234, 'TCHÈQUE, RÉPUBLIQUE', 'CZ', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CZ', '', NULL),
(6450, 234, 'THAÏLANDE', 'TH', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TH', '', NULL),
(6451, 234, 'TIMOR-LESTE', 'TL', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TL', '', NULL),
(6452, 234, 'TOGO', 'TG', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TG', '', NULL),
(6453, 234, 'TOKELAU', 'TK', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TK', '', NULL),
(6454, 234, 'TONGA', 'TO', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TO', '', NULL),
(6455, 234, 'TRINITÉ-ET-TOBAGO', 'TT', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TT', '', NULL),
(6456, 234, 'TUNISIE', 'TN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TN', '', NULL),
(6457, 234, 'TURKMÉNISTAN', 'TM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TM', '', NULL),
(6458, 234, 'TURKS ET CAÏQUES, ÎLES', 'TC', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TC', '', NULL),
(6459, 234, 'TURQUIE', 'TR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TR', '', NULL),
(6460, 234, 'TUVALU', 'TV', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'TV', '', NULL),
(6461, 234, 'UKRAINE', 'UA', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'UA', '', NULL),
(6462, 234, 'URUGUAY', 'UY', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'UY', '', NULL),
(6463, 234, 'VANUATU', 'VU', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'VU', '', NULL),
(6464, 234, 'VENEZUELA', 'VE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'VE', '', NULL),
(6465, 234, 'VIET NAM', 'VN', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'VN', '', NULL),
(6466, 234, 'YÉMEN', 'YE', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'YE', '', NULL),
(6467, 234, 'ZAMBIE', 'ZM', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ZM', '', NULL),
(6468, 234, 'ZIMBABWE', 'ZW', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ZW', '', NULL),
(6469, 234, 'FRANCE', 'FR', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FR', '', NULL);

--
-- Contenu de la table `HabilitationProfil`
-- @todo : fait

INSERT IGNORE INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`, `statistiques_QoS`, `ouvrir_anonymat_a_distance`, `gestion_compte_jal`, `gestion_centrale_pub`, `Gestion_Compte_Groupe_Moniteur`, `ouvrir_offre_technique_en_ligne`, `ouvrir_offre_technique_a_distance`, `activation_compte_entreprise`, `importer_enveloppe`, `suivi_seul_registre_depots_papier`, `suivi_seul_registre_retraits_papier`, `suivi_seul_registre_questions_papier`, `suivi_seul_registre_depots_electronique`, `suivi_seul_registre_retraits_electronique`, `suivi_seul_registre_questions_electronique`, `modifier_consultation_mapa_inferieur_montant_apres_validation`, `modifier_consultation_mapa_superieur_montant_apres_validation`, `modifier_consultation_procedures_formalisees_apres_validation`, `gerer_les_entreprises`, `portee_societes_exclues`, `portee_societes_exclues_tous_organismes`, `modifier_societes_exclues`, `supprimer_societes_exclues`, `resultat_analyse`, `gerer_adresses_service`, `gerer_mon_service`, `download_archives`, `creer_annonce_extrait_pv`, `creer_annonce_rapport_achevement`, `gestion_certificats_agent`, `creer_avis_programme_previsionnel`, `annuler_consultation`, `envoyer_publicite`, `liste_marches_notifies`, `suivre_message`, `envoyer_message`, `suivi_flux_chorus_transversal`, `gestion_mandataire`, `gerer_newsletter`, `gestion_modeles_formulaire`, `gestion_adresses_facturation_jal`, `administrer_adresses_facturation_jal`, `redaction_documents_redac`, `validation_documents_redac`, `gestion_mise_disposition_pieces_marche`, `annuaire_acheteur`, `reprendre_integralement_article`, `administrer_clauses`, `valider_clauses`, `administrer_canevas`, `valider_canevas`, `administrer_clauses_entite_achats`, `generer_pieces_format_odt`, `publier_version_clausier_editeur`, `administrer_clauses_editeur`, `valider_clauses_editeur`, `administrer_canevas_editeur`, `valider_canevas_editeur`, `decision_suivi_seul`, `ouvrir_candidature_hors_ligne`, `ouvrir_offre_hors_ligne`, `ouvrir_offre_technique_hors_ligne`, `ouvrir_anonymat_hors_ligne`, `espace_collaboratif_gestionnaire`, `espace_collaboratif_contributeur`, `gerer_organismes`, `gerer_associations_agents`, `module_redaction_uniquement`, `historique_navigation_inscrits`, `telecharger_accords_cadres`, `creer_annonce_decision_resiliation`, `creer_annonce_synthese_rapport_audit`, `gerer_operations`, `telecharger_siret_acheteur`, `gerer_reouvertures_modification`, `acceder_tous_telechargements`) VALUES
(1, 'Administrateur/Acheteur', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '0', '1', '0', '1', '1', '1', '1', '0', '0', '0', '1', '0', '0', '0', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
(4, 'Acheteur', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
(6, 'Acheteur < 90 kEUR', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '1', '1', '1', '0', '0', '0', '1', '1', '1', '1', '0', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
(12, 'Administrateur/Acheteur', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '0', '1', '0', '1', '1', '1', '1', '0', '0', '0', '1', '0', '0', '0', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
(13, 'Administrateur Service', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '1', '0', '1', '1', '1', '0', '0', '0', '1', '1', '1', '1', '0', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0');

--
-- Contenu de la table `Inscrit`
-- @todo : fait

INSERT IGNORE INTO `t_etablissement` (`id_etablissement`, `id_entreprise`, `code_etablissement`, `est_siege`, `adresse`, `adresse2`, `code_postal`, `ville`, `pays`, `saisie_manuelle`, `id_initial`, `date_creation`, `date_modification`, `date_suppression`, `statut_actif`, `inscrit_annuaire_defense`, `long`, `lat`, `maj_long_lat`, `tva_intracommunautaire`, `etat_administratif`, `date_fermeture`, `id_externe`) VALUES
( 1, 1, '00019', '1', '231 RUE DU MARCHE ST HONORE', '', '75001', 'PARIS 1', 'France', '0', 0, '2015-03-20 09:55:25', '2021-06-21 09:45:04', NULL, '1', '0', 2.3370411863327, 48.8621574, '2015-09-07 10:26:44', NULL, 'F', '2014-03-24 00:00:00', '0'),
( 2, 2, '00019', '1', '231 RUE DU MARCHE ST HONORE', '', '75001', 'PARIS 1', 'France', '0', 0, '2015-03-20 09:55:25', '2021-06-21 09:45:04', NULL, '1', '0', 2.3370411863327, 48.8621574, '2015-09-07 10:26:44', NULL, 'F', '2014-03-24 00:00:00', '0'),
( 3, 3, '00019', '1', '231 RUE DU MARCHE ST HONORE', '', '75001', 'PARIS 1', 'France', '0', 0, '2015-03-20 09:55:25', '2021-06-21 09:45:04', NULL, '1', '0', 2.3370411863327, 48.8621574, '2015-09-07 10:26:44', NULL, 'F', '2014-03-24 00:00:00', '0');



INSERT IGNORE INTO `Inscrit` (`id`, `entreprise_id`, `id_etablissement`, `login`, `mdp`, `num_cert`, `cert`, `civilite`, `nom`, `prenom`, `adresse`, `codepostal`, `ville`, `pays`, `email`, `telephone`, `categorie`, `motstitreresume`, `periode`, `siret`, `fax`, `code_cpv`, `id_langue`, `profil`, `adresse2`, `bloque`, `id_initial`, `inscrit_annuaire_defense`, `date_creation`, `date_modification`, `tentatives_mdp`, `uid`) VALUES
(1, 1, 1, 'mp', '91f6e575920d2d503d8597338fd1637d580f4243', NULL, NULL, 0, 'PASSET', 'Marc', '', '', '', '', 'mp@atexo.com', '0153430500', NULL, NULL, 0, '', '', NULL, NULL, 2, '', '0', 0, '0', '', '', 0, NULL),
(2, 1, 1, 'cc', '9f107db7187c84fd1836accd91a272f496866a30', NULL, NULL, 0, 'CIVETE', 'Charles', '', '', '', '', 'cc@atexo.com', '0153430500', NULL, NULL, 0, '', '', NULL, NULL, 1, '', '0', 0, '0', '', '', 3, NULL),
(4, 2, 2, 'mp2', '44609257f13936e49292cbd87c6d7fd6079ff095', NULL, NULL, 0, 'POLLET', 'Michel', '', '', '', '', 'mp2@atexo.com', '0153430544', NULL, NULL, 0, '', '', NULL, NULL, 2, '', '0', 0, '0', '', '', 0, NULL),
(5, 2, 2, 'cc2', '1e090fd026721c03e6b552e951234fa45f17c2c8', NULL, NULL, 0, 'CHARON', 'Clément', '', '', '', '', 'cc2@atexo.com', '0153430550', NULL, NULL, 0, '', '', NULL, NULL, 1, '', '0', 0, '0', '', '', 0, NULL),
(6, 3, 3, 'mp3', '1bc0264d5bf8869899640171b693092d8c4899f8', NULL, NULL, 0, 'POSTIER', 'Maxime', '', '', '', '', 'mp3@atexo.com', '0153430540', NULL, NULL, 0, '', '', NULL, NULL, 2, '', '0', 0, '0', '', '', 0, NULL),
(7, 3, 3, 'cc3', '26aca93edcc6da566cbdcc5b9de15a277289618e', NULL, NULL, 0, 'CARLET', 'Christian', '', '', '', '', 'cc3@atexo.com', '014354069', NULL, NULL, 0, '', '', NULL, NULL, 1, '', '0', 0, '0', '', '', 0, NULL),
(8, 1, 1, 'alerte.v3', 'b7124ab3f60eec664940f58cd11d876ae911c9e0', NULL, NULL, 0, 'ALERTE V3', 'alertev3', '', '', '', '', 'alerte.v3@atexo.com', '0153430540', NULL, NULL, 0, '', '', NULL, NULL, 1, '', '0', 0, '0', '', '', 0, NULL);

--
-- Contenu de la table `InterfaceTypeProcedure`
-- @todo : fait
INSERT IGNORE INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES
(1, 'ACI', NULL, 'ATR'),
(2, 'ACR', NULL, 'ATR'),
(3, 'AOCR', NULL, 'ATR'),
(4, 'AOO', NULL, 'ATR'),
(5, 'AOO2', NULL, 'AOO'),
(6, 'AOR', NULL, 'ARC'),
(7, 'COO', NULL, 'COO'),
(8, 'COR', NULL, 'CRC'),
(9, 'MNAC', NULL, 'MNE'),
(10, 'MNAP', NULL, 'MNE'),
(11, 'MNSC', NULL, 'MNE'),
(12, 'MOCN', NULL, 'CRC'),
(13, 'MOSC', NULL, 'MNE'),
(14, 'MSF', NULL, 'ATR'),
(15, 'MSFA', NULL, 'ACA'),
(16, 'MSFB', NULL, 'ACS'),
(17, 'MSFC', NULL, 'MAI'),
(18, 'MSFR', NULL, 'ATR'),
(19, 'PDC', NULL, 'DCC'),
(20, NULL, '3', 'AOO'),
(21, NULL, '4', 'ARC'),
(22, NULL, '5', 'CRC'),
(23, NULL, '6', 'DCC'),
(24, NULL, '7', 'MNE'),
(25, NULL, '8', 'MNE'),
(26, NULL, '10', 'ATR'),
(27, NULL, '12', 'COO'),
(28, NULL, '13', 'MNE'),
(29, NULL, '14', 'MNE'),
(30, NULL, '16', 'ATR'),
(31, NULL, '17', 'COO'),
(32, NULL, '18', 'AOO'),
(33, NULL, '19', 'ARC'),
(34, NULL, '20', 'MNE'),
(35, NULL, '21', 'MAI'),
(36, NULL, '22', 'MAI');

--
-- Contenu de la table `Langue`
-- @todo : fait
INSERT IGNORE INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `theme_specifique`, `obligatoire_pour_publication_consultation`) VALUES
(1, 'fr', '1', '1', '0', '0'),
(2, 'en', '1', '0', '0', '0'),
(3, 'es', '1', '0', '0', '0'),
(4, 'cz', '0', '0', '0', '0'),
(5, 'du', '0', '0', '0', '0'),
(6, 'su', '0', '0', '0', '0'),
(7, 'it', '0', '0', '0', '0');

--
-- Contenu de la table `mode_execution_contrat`
-- @todo : fait
INSERT IGNORE INTO `mode_execution_contrat` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES
(1, '1 - Ordinaire (Sans tranche, exclusivement à prix forfaitaire)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '2 - A bons de commande (Sans tranche, exclusivement à bons de commande)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '3 - A Tranches (Avec TF et TC, exclusivement à prix forfaitaire)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '4 - A tranches et bons de commande (Avec TF et TC, en partie à bons de commande)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '5 - A phases', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '6 - Autres', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- Contenu de la table `Referentiel`
-- @todo : fait
INSERT IGNORE INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES
(4, "modèle avis appel d\'offre avec sélection"),
(5, "modèle avis concours"),
(6, "Type de pouvoir adjudicateur"),
(7, " Principale(s) activité(s) du pouvoir adjudicateu"),
(8, " Principale(s) activité(s) de l\'entité adjudicatrice"),
(13, "Type de collaboration"),
(14, "types de documents pour la Mise à disposition des pièces de marchés"),
(15, "Forme juridique du groupement attributaire"),
(16, "Durée  ou délai du marché"),
(18, "Durée  ou délai du marché"),
(19, "Critère(s) d\'attribution"),
(20, "CCAG de référence"),
(21, "Unité durée marché"),
(22, "Variation du prix"),
(23, "Type de Prix"),
(24, "Unité de prix"),
(25, "Rejeté, problème de tiers fournisseur "),
(26, "Les tranches effectis"),
(157, "Etapes de l\'ordre du jour de la séance"),
(161, "Les catégories des statistiques sur la plateforme"),
(184, "Civilité"),
(185, "Statut de la séance de commission"),
(210, "Liste des actions tracées côte entreprises");

--
-- Contenu de la table `Service_Mertier`
-- @todo : fait
INSERT IGNORE INTO `Service_Mertier` (`id`, `sigle`, `denomination`, `url_acces`, `logo`, `url_deconnexion`, `ordre`) VALUES
(1, 'MPE', 'MARCHES_PUBLICS_ELECTRONIQUES', '?page=agent.AccueilAgentAuthentifie', '', '', 0);

--
-- Contenu de la table `Service_Mertier_Profils`
-- @todo : fait
INSERT IGNORE INTO `Service_Mertier_Profils` (`id_auto`, `id_interne`, `id_service_metier`, `id_externe`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`, `show_profile_for_hyperadmin_only`) VALUES
(1, 1, 1, 1, 'Administrateur/Acheteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(2, 4, 1, 4, 'Acheteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(3, 6, 1, 6, 'Acheteur < 90 kEUR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(4, 12, 1, 12, 'Administrateur/Acheteur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(5, 13, 1, 13, 'Administrateur Service', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0');

--
-- Contenu de la table `TypeAvis`
-- @todo : fait
INSERT IGNORE INTO `TypeAvis` (`id`, `intitule_avis`, `intitule_avis_fr`, `intitule_avis_en`, `intitule_avis_es`, `intitule_avis_su`, `intitule_avis_du`, `intitule_avis_cz`, `intitule_avis_ar`, `abbreviation`, `id_type_avis_ANM`, `intitule_avis_it`) VALUES
(2, 'Annonce d\'information', 'Annonce d\'information', 'en Annonce d\'information ', 'es Annonce d\'information ', NULL, NULL, NULL, NULL, 'API', 1, NULL),
(3, 'Annonce de consultation', 'Annonce de consultation', 'en Annonce de consultation ', 'es Annonce de consultation', NULL, NULL, NULL, NULL, 'AAPC', 2, NULL),
(4, 'Annonce d\'attribution', 'Annonce d\'attribution', 'en Annonce d\'attribution ', 'es Annonce d\'attribution ', NULL, NULL, NULL, NULL, 'AA', 3, NULL),
(8, 'Annonce de décision de résiliation', 'Annonce de décision de résiliation', NULL, NULL, NULL, NULL, NULL, '??????? ???? ???????', 'ADR', 0, NULL);


--
-- Contenu de la table `TypeDecision`
-- @todo : fait
INSERT IGNORE INTO `TypeDecision` (`id_type_decision`, `code_type_decision`) VALUES
(2, 'DECISION_ATTRIBUTION_MARCHE'),
(3, 'DECISION_DECLARATION_SANS_SUITE'),
(4, 'DECISION_DECLARATION_INFRUCTUEUX'),
(5, 'DECISION_SELECTION_ENTREPRISES'),
(6, 'DECISION_ATTRIBUTION_DE_ACCORD_CADRE'),
(7, 'DECISION_ADMISSION_SAD'),
(8, 'DECISION_AUTRE'),
(9, 'DECISION_A_RENSEIGNER');


--
-- Contenu de la table `t_flux_rss`
-- @todo : fait
INSERT IGNORE INTO `t_flux_rss` (`id`, `tender_xml`, `nom_fichier`, `libelle`, `module`, `afficher_flux_rss`) VALUES
(1, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender category="Travaux" enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rssTr.xml', 'LISTE_AVIS_EN_LIGNE_TRAVAUX', NULL, '1'),
(2, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender category="Services" enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rssSr.xml', 'LISTE_AVIS_EN_LIGNE_SERVICES', NULL, '1'),
(3, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender atelierProtege="1" enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rssAP.xml', 'LISTE_AVIS_EN_LIGNE_ATELIER_PROTEGE', 'ConsultationClause', '1'),
(4, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rssOnline.xml', 'LISTE_AVIS_EN_LIGNE', NULL, '1'),
(5, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender category="Fournitures" enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rssFn.xml', 'LISTE_AVIS_EN_LIGNE_FOURNITURES', NULL, '1'),
(6, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender clauseSociale="1" enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rssCS.xml', 'LISTE_AVIS_EN_LIGNE_AVEC_CLAUSES_SOCIALES', 'ConsultationClause', '1'),
(7, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender clauseEnv="1" enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rssCE.xml', 'LISTE_AVIS_EN_LIGNE_AVEC_CLAUSES_ENVIRONNEMENTALES', 'ConsultationClause', '1'),
(8, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender type="AAPC" enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rss.xml', NULL, NULL, '0'),
(9, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender type="AA" enCours="1"/> \r\n </Request>\r\n</Transaction>', 'rss-AA.xml', NULL, NULL, '0'),
(10, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n   <Tender type="API" enCours="1"/> \r\n </Request>\r\n</Transaction>', 'rss-API.xml', NULL, NULL, '0'),
(11, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender mps="1" enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rss-MPS.xml', 'LISTE_AVIS_EN_LIGNE_AVEC_MARCHE_PUBLIC_SIMPLIFIE', NULL, '1');

--
-- Contenu de la table `t_referentiel_certificat`
-- @todo : fait
INSERT IGNORE INTO `t_referentiel_certificat` (`id`, `nom_referentiel_certificat`, `statut_referentiel_certificat`, `nom_referentiel_fonctionnel`, `message`) VALUES
(1, 'atx', 0, 'AC de test ATEXO', NULL),
(2, 'europe_fr', 0, '', NULL),
(3, 'lux', 0, '', NULL),
(4, 'place', 0, 'Certificats de l\'Etat', NULL),
	(5, 'prisv1', 2, 'PRIS V1', NULL),
	(6, 'rgs', 0, 'RGS', NULL),
	(7, 'ATEXO', 0, 'AC de test ATEXO', NULL),
	(8, 'PLACE', 0, 'Certificats de l\'Etat', NULL),
(9, 'PRISV1', 2, 'PRIS V1', NULL),
(10, 'RGS1E', 0, 'RGS 1 Etoile', NULL),
(11, 'RGS2E', 0, 'RGS 2 Etoiles', NULL),
(12, 'RGS3E', 0, 'RGS 3 Etoiles', NULL),
(13, 'TSL-AT', 0, 'TSL-AT', NULL),
(14, 'TSL-BE', 0, 'TSL-BE', NULL),
(15, 'TSL-BG', 0, 'TSL-BG', NULL),
(16, 'TSL-CY', 0, 'TSL-CY', NULL),
(17, 'TSL-CZ', 0, 'TSL-CZ', NULL),
(18, 'TSL-DE', 0, 'TSL-DE', NULL),
(19, 'TSL-DK', 0, 'TSL-DK', NULL),
(20, 'TSL-EE', 0, 'TSL-EE', NULL),
(21, 'TSL-EL', 0, 'TSL-EL', NULL),
(22, 'TSL-ES', 0, 'TSL-ES', NULL),
(23, 'TSL-FI', 0, 'TSL-FI', NULL),
(24, 'TSL-FR', 0, 'TSL-FR', NULL),
(25, 'TSL-HR', 0, 'TSL-HR', NULL),
(26, 'TSL-HU', 0, 'TSL-HU', NULL),
(27, 'TSL-IE', 0, 'TSL-IE', NULL),
(28, 'TSL-IT', 0, 'TSL-IT', NULL),
(29, 'TSL-LI', 0, 'TSL-LI', NULL),
(30, 'TSL-LT', 0, 'TSL-LT', NULL),
(31, 'TSL-LU', 0, 'TSL-LU', NULL),
(32, 'TSL-LV', 0, 'TSL-LV', NULL),
(33, 'TSL-MT', 0, 'TSL-MT', NULL),
(34, 'TSL-NL', 0, 'TSL-NL', NULL),
(35, 'TSL-NO', 0, 'TSL-NO', NULL),
(36, 'TSL-PL', 0, 'TSL-PL', NULL),
(37, 'TSL-PT', 0, 'TSL-PT', NULL),
(38, 'TSL-RO', 0, 'TSL-RO', NULL),
(39, 'TSL-SE', 0, 'TSL-SE', NULL),
(40, 'TSL-SI', 0, 'TSL-SI', NULL),
(41, 'TSL-SK', 0, 'TSL-SK', NULL),
(42, 'TSL-UK', 0, 'TSL-UK', NULL);

--
-- Contenu de la table `ValeurReferentiel`
-- @todo : fait
INSERT IGNORE INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(1, 13, "Mandataire de groupement", "Mandataire de groupement", "", "", "", "", "", "", "", ""),
(1, 16, "Solidaire", "Solidaire", "", "", "", "", "", "", "SOLIDAIRE", ""),
(1, 18, "Durée du marché", "Durée du marché", "", "", "", "", "", "", "DUREE_MARCHEE", ""),
(1, 19, "Critères énoncés ci après (par ordre de priorité décroissante)", "Critères énoncés ci après (par ordre de priorité décroissante)", "", "", "", "", "", "", "layerDefinitionCriteres_1", ""),
(1, 20, "CCAG-FCS", "CCAG-FCS", "", "", "", "", "", "", "", ""),
(1, 22, "Prix révisables", "Prix révisables", "", "", "", "", "", "", "PRIX_REVISABLES", ""),
(1, 23, "Prix sur catalogues", "Prix sur catalogues", "", "", "", "", "", "", "PRIX_CATALOGUE", ""),
(1, 24, "EUR HT", "EUR HT", "", "", "", "", "", "", "", ""),
(1, 25, "Le ou les tiers fournisseurs suivants n''existent pas dans CHORUS.Veuillez contacter votre CSP pour demander la création dans CHORUS du ou des tiers fournisseurs suivants", "Le ou les tiers fournisseurs suivants n''existent pas dans CHORUS.Veuillez contacter votre CSP pour demander la création dans CHORUS du ou des tiers fournisseurs suivants", "", "", "", "", "", "", "TIER_N_EXISTE_PAS", ""),
(1, 26, "Non renseigné", "Non renseigné", "", "", "", "", "", "", "", ""),
(1, 157, "Ouverture", "Ouverture", "", "", "", "", "", "", "", ""),
(1, 161, "Consultations", "Consultations", "", "", "", "", "", "", "1;2;9;22;13;14;16;15;18;24;25", ""),
(1, 184, "M.", "M.", "", "", "", "", "", "", "", ""),
(1, 185, "En attente d''édition", "En attente d''édition", "", "", "", "", "", "", "", ""),
(2, 13, "Co-traitant solidaire", "Co-traitant solidaire", "", "", "", "", "", "", "", ""),
(2, 16, "Conjoint", "Conjoint", "", "", "", "", "", "", "CONJOINT", ""),
(2, 18, "Délai d''exécution des prestations", "Délai d''exécution des prestations", "", "", "", "", "", "", "DELAI_EXECUTION", ""),
(2, 19, "Critères énoncés ci après avec leur pondération", "Critères énoncés ci après avec leur pondération", "", "", "", "", "", "", "layerDefinitionCriteres_2", ""),
(2, 20, "CCAG-MI", "CCAG-MI", "", "", "", "", "", "", "", ""),
(2, 22, "Prix actualisables", "Prix actualisables", "", "", "", "", "", "", "PRIX_ACTUALISABLES", ""),
(2, 23, "Bordereau de prix", "Bordereau de prix", "", "", "", "", "", "", "BORDEREAU_PRIX", ""),
(2, 24, "EUR TTC", "EUR TTC", "", "", "", "", "", "", "", ""),
(2, 25, "Le ou les tiers fournisseurs suivants existent dans CHORUS mais ne sont pas étendus sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander l''extension dans CHORUS du ou des tiers fournisseurs suivants", "Le ou les tiers fournisseurs suivants existent dans CHORUS mais ne sont pas étendus sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander l''extension dans CHORUS du ou des tiers fournisseurs suivants", "", "", "", "", "", "", "TIER_NON_ETENDU", ""),
(2, 26, "Unités non employeuses (pas de salarié au cours de l''année de référence et pas d''effectif au 31/12).", "Unités non employeuses (pas de salarié au cours de l''année de référence et pas d''effectif au 31/12).", "", "", "", "", "", "", "", ""),
(2, 157, "Sélection des candidatures", "Sélection des candidatures", "", "", "", "", "", "", "", ""),
(2, 161, "DCE", "DCE", "", "", "", "", "", "", "5;17;6;7;8;23", ""),
(2, 184, "Mme", "Mme", "", "", "", "", "", "", "", ""),
(2, 185, "Envoyé", "Envoyé", "", "", "", "", "", "", "", ""),
(3, 13, "Co-traitant conjoint", "Co-traitant conjoint", "", "", "", "", "", "", "", ""),
(3, 16, "Au choix", "Au choix", "", "", "", "", "", "", "AU_CHOIX", ""),
(3, 18, "Description libre du délai ou de la durée", "Description libre du délai ou de la durée", "", "", "", "", "", "", "DESCRIPTION_LIBRE", ""),
(3, 19, "Critère unique du prix le plus bas", "Critère unique du prix le plus bas", "", "", "", "", "", "", "layerDefinitionVide", ""),
(3, 20, "CCAG-TR", "CCAG-TR", "", "", "", "", "", "", "", ""),
(3, 22, "Prix fermes", "Prix fermes", "", "", "", "", "", "", "PRIX_FERMES", ""),
(3, 23, "Autre", "Autre", "", "", "", "", "", "", "AUTRE", ""),
(3, 24, "Dossier(s)", "Dossier(s)", "", "", "", "", "", "", "", ""),
(3, 25, "Le ou les tiers fournisseurs suivants existent dans CHORUS mais sont bloqués sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander le déblocage dans CHORUS du ou des tiers fournisseurs suivants", "Le ou les tiers fournisseurs suivants existent dans CHORUS mais sont bloqués sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander le déblocage dans CHORUS du ou des tiers fournisseurs suivants", "", "", "", "", "", "", "TIER_BLOQUE", ""),
(3, 26, "0 salarié (n''ayant pas d''effectif au 31/12 mais ayant employé des salariés au cours de l''année de référence)", "0 salarié (n''ayant pas d''effectif au 31/12 mais ayant employé des salariés au cours de l''année de référence)", "", "", "", "", "", "", "", ""),
(3, 157, "Présentation de DCE", "Présentation de DCE", "", "", "", "", "", "", "", ""),
(3, 161, "Publicités", "Publicités", "", "", "", "", "", "", "3;4", ""),
(3, 185, "A archiver", "A archiver", "", "", "", "", "", "", "", ""),
(4, 13, "Sous-traitant", "Sous-traitant", "", "", "", "", "", "", "", ""),
(4, 19, "Critères énoncés dans le cahier des charges", "Critères énoncés dans le cahier des charges", "", "", "", "", "", "", "layerDefinitionVide_2", ""),
(4, 20, "CCAG-PI", "CCAG-PI", "", "", "", "", "", "", "", ""),
(4, 24, "Jour(s)", "Jour(s)", "", "", "", "", "", "", "", ""),
(4, 25, "Le ou les tiers fournisseurs suivants n''ont pu être vérifiés dans CHORUS. Veuillez contacter l''assistance téléphonique de PLACE afin d''identifier la nature du problème", "Le ou les tiers fournisseurs suivants n''ont pu être vérifiés dans CHORUS. Veuillez contacter l''assistance téléphonique de PLACE afin d''identifier la nature du problème", "", "", "", "", "", "", "TIER_ERRONE", ""),
(4, 26, "1 ou 2 salariés", "1 ou 2 salariés", "", "", "", "", "", "", "", ""),
(4, 157, "Attribution", "Attribution", "", "", "", "", "", "", "", ""),
(4, 161, "Réponses électroniques", "Réponses électroniques", "", "", "", "", "", "", "10;11;12;19;20;21", ""),
(4, 185, "Archivé", "Archivé", "", "", "", "", "", "", "", ""),
(5, 20, "CCAG-TIC", "CCAG-TIC", "", "", "", "", "", "", "", ""),
(5, 26, "3 à 5 salariés", "3 à 5 salariés", "", "", "", "", "", "", "", ""),
(5, 157, "Avenant", "Avenant", "", "", "", "", "", "", "", ""),
(6, 26, "6 à 9 salariés", "6 à 9 salariés", "", "", "", "", "", "", "", ""),
(6, 157, "Avis", "Avis", "", "", "", "", "", "", "", ""),
(7, 26, "10 à 19 salariés", "10 à 19 salariés", "", "", "", "", "", "", "", ""),
(7, 157, "Négociation", "Négociation", "", "", "", "", "", "", "", ""),
(8, 26, "20 à 49 salariés", "20 à 49 salariés", "", "", "", "", "", "", "", ""),
(8, 157, "Autre", "Autre", "", "", "", "", "", "", "", ""),
(9, 26, "50 à 99 salariés", "50 à 99 salariés", "", "", "", "", "", "", "", ""),
(10, 26, "100 à 199 salariés", "100 à 199 salariés", "", "", "", "", "", "", "", ""),
(11, 26, "200 à 249 salariés", "200 à 249 salariés", "", "", "", "", "", "", "", ""),
(12, 26, "250 à 499 salariés", "250 à 499 salariés", "", "", "", "", "", "", "", ""),
(13, 26, "500 à 999 salariés", "500 à 999 salariés", "", "", "", "", "", "", "", ""),
(14, 26, "1 000 à 1 999 salariés", "1 000 à 1 999 salariés", "", "", "", "", "", "", "", ""),
(15, 26, "2 000 à 4 999 salariés", "2 000 à 4 999 salariés", "", "", "", "", "", "", "", ""),
(16, 26, "5 000 à 9 999 salariés", "5 000 à 9 999 salariés", "", "", "", "", "", "", "", ""),
(17, 26, "10 000 salariés et plus", "10 000 salariés et plus", "", "", "", "", "", "", "", ""),
(30, 4, "à l''appel d''offres avec présélection", "", "", "", "", "", "", "", "01", ""),
(33, 5, "au concours", "", "", "", "", "", "", "", "01", ""),
(34, 5, "dossier du concours", "", "", "", "", "", "", "", "02", ""),
(35, 5, "des articles 51 et 53", "", "", "", "", "", "", "", "03", ""),
(36, 6, "Ministère ou toute autre autorité nationale ou fédérale, y compris leurs subdivisions régionales ou locales", "", "", "", "", "", "", "", "", ""),
(37, 6, "Agence/office national(e) ou fédéral(e)", "", "", "", "", "", "", "", "", ""),
(38, 6, "Collectivité territoriale", "", "", "", "", "", "", "", "", ""),
(39, 6, "Agence/office régional(e) ou local(e)", "", "", "", "", "", "", "", "", ""),
(40, 6, "Organisme de droit public", "", "", "", "", "", "", "", "", ""),
(41, 6, "Institution/agence européenne ou organisation européenne", "", "", "", "", "", "", "", "", ""),
(42, 6, "Autre", "", "", "", "", "", "", "", "", ""),
(43, 7, "Services généraux des administrations publiques", "", "", "", "", "", "", "", "<servicesGenerauxAdmPub/>", ""),
(44, 7, "Défense", "", "", "", "", "", "", "", "<defense/>", ""),
(45, 7, "Ordre et sécurité publique\r\n", "", "", "", "", "", "", "", "<ordreSecuritePub/>", ""),
(46, 7, "Environnement\r\n", "", "", "", "", "", "", "", "<environnement/>", ""),
(47, 7, "Affaires économiques et financières", "", "", "", "", "", "", "", "<affairesEcoFi/>", ""),
(48, 7, "Santé", "", "", "", "", "", "", "", "<sante/>", ""),
(49, 8, "Production, transport et distribution de gaz ou de chaleur", "", "", "", "", "", "", "", "<gazChaleur/>", ""),
(50, 8, "Electricités", "", "", "", "", "", "", "", "<electricites/>", ""),
(51, 8, "Prospection et extraction de pétrole et de gaz\r\n", "", "", "", "", "", "", "", "<gazPetrole/>", ""),
(52, 8, "Prospection et extraction de charbon ou autres combustibles\r\n", "", "", "", "", "", "", "", "<charbon/>", ""),
(53, 8, "Eau", "", "", "", "", "", "", "", "<eau/>", ""),
(54, 8, "Services postaux", "", "", "", "", "", "", "", "<servicesPostaux/>", ""),
(55, 8, "Services de chemin de fer", "", "", "", "", "", "", "", "<servicesCheminFer/>", ""),
(56, 8, "Services de chemin de fer urbains, de tramway ou d''autobus\r\n", "", "", "", "", "", "", "", "<servicesCheminFerUrbains/>", ""),
(57, 8, "Activités portuaires\r\n", "", "", "", "", "", "", "", "<activitesPortuaires/>", ""),
(58, 8, "Activités aéroportuaires", "", "", "", "", "", "", "", "<activitesAeroportuaires/>", ""),
(59, 6, "Ministère ou toute autre autorité nationale ou fédérale, y compris leurs subdivisions régionales ou locales", "", "", "", "", "", "", "", "", ""),
(60, 6, "Agence/office national(e) ou fédéral(e)", "", "", "", "", "", "", "", "", ""),
(61, 6, "Collectivité territoriale", "", "", "", "", "", "", "", "", ""),
(62, 6, "Agence/office régional(e) ou local(e)", "", "", "", "", "", "", "", "", ""),
(63, 6, "Organisme de droit public", "", "", "", "", "", "", "", "", ""),
(64, 6, "Institution/agence européenne ou organisation européenne", "", "", "", "", "", "", "", "", ""),
(65, 6, "Autre", "", "", "", "", "", "", "", "", ""),
(66, 7, "Services généraux des administrations publiques", "", "", "", "", "", "", "", "<servicesGenerauxAdmPub/>", ""),
(67, 7, "Défense", "", "", "", "", "", "", "", "<defense/>", ""),
(68, 7, "Ordre et sécurité publique\r\n", "", "", "", "", "", "", "", "<ordreSecuritePub/>", ""),
(69, 7, "Environnement\r\n", "", "", "", "", "", "", "", "<environnement/>", ""),
(70, 7, "Affaires économiques et financières", "", "", "", "", "", "", "", "<affairesEcoFi/>", ""),
(71, 7, "Santé", "", "", "", "", "", "", "", "<sante/>", ""),
(72, 8, "Production, transport et distribution de gaz ou de chaleur", "", "", "", "", "", "", "", "<gazChaleur/>", ""),
(73, 8, "Electricités", "", "", "", "", "", "", "", "<electricites/>", ""),
(74, 8, "Prospection et extraction de pétrole et de gaz\r\n", "", "", "", "", "", "", "", "<gazPetrole/>", ""),
(75, 8, "Prospection et extraction de charbon ou autres combustibles\r\n", "", "", "", "", "", "", "", "<charbon/>", ""),
(76, 8, "Eau", "", "", "", "", "", "", "", "<eau/>", ""),
(77, 8, "Services postaux", "", "", "", "", "", "", "", "<servicesPostaux/>", ""),
(78, 8, "Services de chemin de fer", "", "", "", "", "", "", "", "<servicesCheminFer/>", ""),
(79, 8, "Services de chemin de fer urbains, de tramway ou d''autobus\r\n", "", "", "", "", "", "", "", "<servicesCheminFerUrbains/>", ""),
(80, 8, "Activités portuaires\r\n", "", "", "", "", "", "", "", "<activitesPortuaires/>", ""),
(81, 8, "Activités aéroportuaires", "", "", "", "", "", "", "", "<activitesAeroportuaires/>", ""),
(82, 6, "Ministère ou toute autre autorité nationale ou fédérale, y compris leurs subdivisions régionales ou locales", "", "", "", "", "", "", "", "", ""),
(83, 6, "Agence/office national(e) ou fédéral(e)", "", "", "", "", "", "", "", "", ""),
(84, 6, "Collectivité territoriale", "", "", "", "", "", "", "", "", ""),
(85, 6, "Agence/office régional(e) ou local(e)", "", "", "", "", "", "", "", "", ""),
(86, 6, "Organisme de droit public", "", "", "", "", "", "", "", "", ""),
(87, 6, "Institution/agence européenne ou organisation européenne", "", "", "", "", "", "", "", "", ""),
(88, 6, "Autre", "", "", "", "", "", "", "", "", ""),
(89, 7, "Services généraux des administrations publiques", "", "", "", "", "", "", "", "<servicesGenerauxAdmPub/>", ""),
(90, 7, "Défense", "", "", "", "", "", "", "", "<defense/>", ""),
(91, 7, "Ordre et sécurité publique\r\n", "", "", "", "", "", "", "", "<ordreSecuritePub/>", ""),
(92, 7, "Environnement\r\n", "", "", "", "", "", "", "", "<environnement/>", ""),
(93, 7, "Affaires économiques et financières", "", "", "", "", "", "", "", "<affairesEcoFi/>", ""),
(94, 7, "Santé", "", "", "", "", "", "", "", "<sante/>", ""),
(95, 8, "Production, transport et distribution de gaz ou de chaleur", "", "", "", "", "", "", "", "<gazChaleur/>", ""),
(96, 8, "Electricités", "", "", "", "", "", "", "", "<electricites/>", ""),
(97, 8, "Prospection et extraction de pétrole et de gaz\r\n", "", "", "", "", "", "", "", "<gazPetrole/>", ""),
(98, 8, "Prospection et extraction de charbon ou autres combustibles\r\n", "", "", "", "", "", "", "", "<charbon/>", ""),
(99, 8, "Eau", "", "", "", "", "", "", "", "<eau/>", ""),
(100, 8, "Services postaux", "", "", "", "", "", "", "", "<servicesPostaux/>", ""),
(101, 8, "Services de chemin de fer", "", "", "", "", "", "", "", "<servicesCheminFer/>", ""),
(102, 8, "Services de chemin de fer urbains, de tramway ou d''autobus\r\n", "", "", "", "", "", "", "", "<servicesCheminFerUrbains/>", ""),
(103, 8, "Activités portuaires\r\n", "", "", "", "", "", "", "", "<activitesPortuaires/>", ""),
(104, 8, "Activités aéroportuaires", "", "", "", "", "", "", "", "<activitesAeroportuaires/>", ""),
(105, 14, "RC - Règlement de consultation", "RC - Règlement de consultation", "", "", "", "", "", "", "", ""),
(106, 14, "DC1 - Lettre de candidature", "DC1 - Lettre de candidature", "", "", "", "", "", "", "", ""),
(107, 14, "DC2 - Déclaration du candidat ", "DC2 - Déclaration du candidat ", "", "", "", "", "", "", "", ""),
(108, 14, "DC4 - Déclaration de sous-traitance", "DC4 - Déclaration de sous-traitance", "", "", "", "", "", "", "", ""),
(109, 14, "BPU - Bordereau des prix unitaires", "BPU - Bordereau des prix unitaires", "", "", "", "", "", "", "", ""),
(110, 14, "CCAP - Cahier des clauses administratives particulières", "CCAP - Cahier des clauses administratives particulières", "", "", "", "", "", "", "", ""),
(111, 14, "CCTP - Cahier des clauses techniques particulières", "CCTP - Cahier des clauses techniques particulières", "", "", "", "", "", "", "", ""),
(112, 14, "CCAG - Cahier des clauses administratives générales", "CCAG - Cahier des clauses administratives générales", "", "", "", "", "", "", "", ""),
(113, 14, "CCTG - Cahier des clauses techniques générales", "CCTG - Cahier des clauses techniques générales", "", "", "", "", "", "", "", ""),
(114, 14, "DPGF - Décomposition du prix global et forfaitaire", "DPGF - Décomposition du prix global et forfaitaire", "", "", "", "", "", "", "", ""),
(115, 14, "DQE - Détail quantitatif estimatif", "DQE - Détail quantitatif estimatif", "", "", "", "", "", "", "", ""),
(116, 14, "Autres pièces", "Autres pièces", "", "", "", "", "", "", "", ""),
(175, 21, "En mois", "En mois", "", "", "", "", "", "", "dureeMois", ""),
(176, 21, "En jours", "En jours", "", "", "", "", "", "", "dureeJours", ""),
(178, 210, "Accès à la fonction de test de configuration du poste (passé avec succès)", "Accès à la fonction de test de configuration du poste (passé avec succès)", "", "", "", "", "", "", "DESCRIPTION2", ""),
(179, 210, "Accès à la fonction de test de configuration du poste (en échec)", "Accès à la fonction de test de configuration du poste (en échec)", "", "", "", "", "", "", "DESCRIPTION3", ""),
(180, 210, "Accès au détail de la consultation", "Accès au détail de la consultation", "", "", "", "", "", "", "DESCRIPTION4", ""),
(188, 210, "Accès au formulaire de demande de téléchargement du dossier de consultation", "Accès au formulaire de demande de téléchargement du dossier de consultation", "", "", "", "", "", "", "DESCRIPTION5", ""),
(189, 210, "Validation du formulaire de demande de téléchargement du dossier de consultation", "Validation du formulaire de demande de téléchargement du dossier de consultation", "", "", "", "", "", "", "DESCRIPTION6", ""),
(190, 210, "Téléchargement du dossier de consultation", "Téléchargement du dossier de consultation", "", "", "", "", "", "", "DESCRIPTION7", ""),
(191, 210, "Téléchargement du règlement de la consultation", "Téléchargement du règlement de la consultation", "", "", "", "", "", "", "DESCRIPTION8", ""),
(192, 210, "Accès à la fonction « Poser une question » de la consultation", "Accès à la fonction « Poser une question » de la consultation", "", "", "", "", "", "", "DESCRIPTION9", ""),
(193, 210, "Validation du formulaire d''identification préalable à la question", "Validation du formulaire d''identification préalable à la question", "", "", "", "", "", "", "DESCRIPTION10", ""),
(194, 210, "Envoi de la question sur la consultation", "Envoi de la question sur la consultation", "", "", "", "", "", "", "DESCRIPTION11", ""),
(195, 210, "Accès au formulaire de réponse de la consultation", "Accès au formulaire de réponse de la consultation", "", "", "", "", "", "", "DESCRIPTION12", ""),
(196, 210, "Ajout d''une ou plusieurs pièces libres au dossier [_typeEnveloppeLot_]", "Ajout d''une ou plusieurs pièces libres au dossier [_typeEnveloppeLot_]", "", "", "", "", "", "", "DESCRIPTION13", ""),
(198, 210, "Ajout du \"DC3 - Acte d''engagement\" au dossier [_typeEnveloppeLot_]", "Ajout du \"DC3 - Acte d''engagement\" au dossier [_typeEnveloppeLot_]", "", "", "", "", "", "", "DESCRIPTION14", ""),
(199, 210, "Retrait d''une pièce libre du dossier [_typeEnveloppeLot_]", "Retrait d''une pièce libre du dossier [_typeEnveloppeLot_]", "", "", "", "", "", "", "DESCRIPTION15", ""),
(200, 210, "Retrait du \"DC3 - Acte d''engagement \" du dossier [_typeEnveloppeLot_]", "Retrait du \"DC3 - Acte d''engagement\" du dossier [_typeEnveloppeLot_]", "", "", "", "", "", "", "DESCRIPTION16", ""),
(201, 210, "Signature d''une ou plusieurs pièces du formulaire de réponse de la consultation", "Signature d''une ou plusieurs pièces du formulaire de réponse de la consultation", "", "", "", "", "", "", "DESCRIPTION17", ""),
(202, 210, "Validation du formulaire de réponse de la consultation", "Validation du formulaire de réponse de la consultation", "", "", "", "", "", "", "DESCRIPTION18", ""),
(203, 210, "Annulation du formulaire de réponse de la consultation", "Annulation du formulaire de réponse de la consultation", "", "", "", "", "", "", "DESCRIPTION19", ""),
(204, 210, "Affichage de la page de confirmation du dépôt de la réponse", "Affichage de la page de confirmation du dépôt de la réponse", "", "", "", "", "", "", "DESCRIPTION20", ""),
(205, 210, "Erreur lors de la signature d''une pièce du formulaire de réponse de la consultation", "Erreur lors de la signature d''une pièce du formulaire de réponse de la consultation\r\n", "", "", "", "", "", "", "DESCRIPTION21", ""),
(208, 210, "Erreur de chargement de l''applet cyptographique", "Erreur de chargement de l''applet cyptographique", "", "", "", "", "", "", "DESCRIPTION22", ""),
(209, 210, "Erreur lors de l''ajout d''une pièce au formulaire de réponse de la consultation", "Erreur lors de l''ajout d''une pièce au formulaire de réponse de la consultation", "", "", "", "", "", "", "DESCRIPTION23", ""),
(210, 210, "Erreur technique rencontrée lors du dépôt de la réponse électronique", "Erreur technique rencontrée lors du dépôt de la réponse électronique", "", "", "", "", "", "", "DESCRIPTION24", "");

--
-- Initialisation des organisme de BASE
--
INSERT IGNORE INTO `Organisme` (`id`, `acronyme`, `sigle`,  `denomination_org`,  `type_article_org`, `categorie_insee`, `cp`, `ville`, `pays`,  `siren`, `complement`, `date_creation` ) VALUES
(1, 'a1a', 'OPR', 'Organisme de paramétrage de référence', '3', '7' , '75002', 'Paris', 'France', '440909562', '00033', CURRENT_TIMESTAMP),
(2, 'a2z', 'ATEXO', 'Organisme de test ATEXO', '3', '7' , '75002', 'Paris', 'France', '440909562', '00033', CURRENT_TIMESTAMP),
(3, 'e3r', 'FORM', 'Organisme de Formation', '3', '7' , '75002', 'Paris', 'France', '440909562', '00033', CURRENT_TIMESTAMP),
(4, 'a1t', 'TEST', 'Organisme de test pour les entreprises', '3', '7' , '75002', 'Paris', 'France', '440909562', '00033', CURRENT_TIMESTAMP);

INSERT IGNORE INTO `configuration_organisme` (`organisme`, `presence_elu`) VALUES
('a1a', '1'),
('a2z', '1'),
('e3r', '1'),
('a1t', '1');

-- Init a1a --

INSERT IGNORE INTO `CompteMoniteur` VALUES
(NULL, 'a1a' , 'Atexo', 'mdp08eT@', 'suivi.mpe@atexo.com', '1'),
(NULL, 'a1a' , 'Atexo', '@texo', 'dev.pmi@atexo.biz', '0');

INSERT IGNORE INTO `Mesure_Type_Procedure` VALUES
( NULL, 'a1a', 'Appel d''offres ouvert'),
(NULL, 'a1a' , 'Appel d''offres restreint'),
(NULL, 'a1a' , 'Dialogue compétitif'),
(NULL, 'a1a' , 'Marché à  procédure adaptée'),
(NULL, 'a1a' , 'Marché négocié avec publicité préalable et mise en concurrence'),
(NULL, 'a1a' , 'Concours'),
(NULL, 'a1a' , 'Procédure spécifique à certains marchés de défense nationale'),
(NULL, 'a1a' , 'Accord cadre'),
(NULL, 'a1a' , 'Système d''acquisition dynamique'),
(NULL ,'a1a' , 'Marché subséquent à accord cadre'),
(NULL ,'a1a' , 'Marché spécifique sur système acquisition dynamique');

INSERT IGNORE INTO `ReferentielDestinationFormXml` VALUES
(1,'a1a', 'BOAMP seul'),
(2,'a1a', 'BOAMP et JOUE'),
(3,'a1a', 'Groupe Moniteur');

INSERT IGNORE INTO `ReferentielTypeXml` ( `id` , `organisme` , `id_destinataire` , `libelle_type` ) VALUES
(1,'a1a', '1#3', 'XML 1.9 BOAMP3 - Avis de marché National'),
(2,'a1a', '1#3', 'XML 1.9 BOAMP6 - Avis de MAPA'),
(3,'a1a', '2#3', 'XML 1.9 JOUE_02 - Avis de marché'),
(4,'a1a', '2#3', 'XML 1.9 JOUE_03 - Avis d''attribution'),
(5,'a1a', '1#3', 'XML 1.9 BOAMP4 - Avis d''attribution'),
(6,'a1a', '1#3', 'XML 1.9 BOAMP2 - Avis rectificatif'),
(7,'a1a', '1#3', 'XML 1.9 BOAMP7 - Résultat de MAPA'),
(8,'a1a', '2#3', 'XML 1.9 JOUE_12 - Avis Concours'),
(9,'a1a', '2#3', 'XML 1.9 JOUE_14 - Formulaire rectificatif / annulation'),
(10,'a1a', '2#3', 'XML 1.9 JOUE_05 - Secteurs Spéciaux'),
(11,'a1a', '2', 'XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire'),
(12,'a1a', '2', 'XML 1.9 JOUE_13 - Résultat Concours'),
(13,'a1a', '2', 'XML 1.9 JOUE_10 - Concession de travaux publics'),
(14,'a1a', '2', 'XML 1.9 JOUE_01T - Avis de préinformation - Travaux'),
(15,'a1a', '2', 'XML 1.9 JOUE_01FS - Avis de préinformation - Fournitures / Services'),
(16,'a1a', '1', 'XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification'),
(17,'a1a', '2#3', 'XML 1.9 JOUE_06 -  Avis d''attribution - Secteurs Spéciaux'),
(18,'a1a', '2', 'XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire');

INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`) VALUES
(1, 'a1a', 'Appel d''offres ouvert', 'AOO', 1, 1, 1, 0, 2, 0, '0', '1', 'Appel d''offres ouvert', 'Open procedure', '', '', '', '', '', 0, '01', '0', '0', '<AppelOuvert/>', '1', '0', '0', '0', '0', '0', '1', 'AOO', 'Procedura apperta', '', '1', 'OPEN'),
(2, 'a1a', 'Appel d''offres restreint - Candidature', 'AOR-C', 2, 2, 2, 0, 2, 0, '0', '1', 'Appel d''offres restreint - Candidature', 'Restricted procedure - preselection', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARC', 'Procedura ristretta - qualificazione', '', '10', 'RESTRICTED'),
(3, 'a1a', 'Appel d''offres restreint - Offre', 'AOR-O', 2, 2, 2, 0, 2, 0, '0', '1', 'Appel d''offres restreint - Offre', 'Restricted procedure - bid', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARO', 'Procedura ristretta - offerta', '', '2', 'RESTRICTED'),
(4, 'a1a', 'Procédure adaptée < 90 k EUR HT', 'PA-INF', 23, 3, 3, 0, 2, 0, '1', '1', 'Procédure adaptée < 90 k EUR HT', 'Procedure below thresholds < 90 k EUR', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAI', 'Procedura sotto soglia < 90 k EUR', '', '9', ''),
(5, 'a1a', 'Procédure adaptée >  90 k EUR HT', 'PA-SUP', 23, 3, 3, 0, 2, 0, '1', '1', 'Procédure adaptée >  90 k EUR HT', 'Procedure below thresholds >  90 k EUR', '', '', '', '', '', 2, '', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAS', 'Procedura sotto soglia >  90 k EUR', '', '9', ''),
(6, 'a1a', 'Procédure adaptée spécifique article 30 < 90 k EUR HT', 'PA-A30-INF', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSI', '', '', '9', ''),
(7, 'a1a', 'Procédure adaptée spécifique article 30 >  90 k EUR HT', 'PA-A30-SUP', 23, 3, 0, 0, 2, 0, '1', '1', '', '', '', '', '', '', '', 2, '', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSS', '', '', '9', ''),
(8, 'a1a', 'Concours ouvert', 'CO', 7, 4, 4, 0, 2, 0, '0', '1', 'Concours ouvert', 'Open design contest', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursOuvert/>', '0', '0', '0', '1', '0', '0', '6', 'COO', 'Concorso apperto', '', '7', 'OPEN'),
(9, 'a1a', 'Concours restreint - Candidature', 'CR-C', 8, 5, 4, 0, 2, 0, '0', '1', 'Concours restreint - Candidature', 'Restricted design contest - preselection', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRC', 'Concorso ristretto - qualificazione', '', '10', 'RESTRICTED'),
(10, 'a1a', 'Concours restreint - Offre', 'CR-O', 8, 5, 4, 0, 2, 0, '0', '1', 'Concours restreint - Offre', 'Restricted design contest - bid', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRO', 'Concorso ristretto - offerta', '', '7', 'RESTRICTED'),
(11, 'a1a', 'Dialogue compétitif - Candidature', 'DC-C', 22, 7, 4, 0, 2, 0, '0', '1', 'Dialogue compétitif - Candidature', 'Competitive dialogue - preselection', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCC', 'Dialogo competitivo - qualificazione', '', '10', 'COMPETITIVE_DIALOGUE'),
(12, 'a1a', 'Dialogue compétitif - Offre', 'DC-O', 22, 7, 4, 0, 2, 0, '0', '1', 'Dialogue compétitif - Offre', 'Competitive dialogue - bid', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCO', 'Dialogo competitivo - offerta', '', '6', 'COMPETITIVE_DIALOGUE'),
(17, 'a1a', 'Procédure Autre', 'AUT', 24, 15, 4, 0, 2, 0, '0', '1', 'Procédure Autre', 'Other procedure', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '1', '0', '0', '', 'ATR', 'Altra procedura', '', '10', ''),
(18, 'a1a', 'Marché négocié', 'MN', 18, 8, 4, 0, 2, 0, '0', '1', 'Marché négocié', 'Negociated procedure', '', '', '', '', '', 0, '10', '0', '0', '<MarcheNegocie/>', '0', '1', '0', '0', '0', '0', '5', 'MNE', 'Procedura negoziata', '', '4', 'NEGOTIATED'),
(30, 'a1a', 'Accord-Cadre - 1 Sélection des Attributaires', 'AC-SA', 1, 30, 1, 0, 2, 0, '0', '1', 'Accord-Cadre - 1 Sélection des Attributaires', 'Framework agreement - 1 Selection of suppliers', '', '', '', '', '', 0, '10', '0', '1', '<Autres/>', '0', '0', '0', '0', '0', '1', '8', 'ACA', 'Accordo quadro - 1 Qualificazione dei titolare', '', '10', ''),
(31, 'a1a', 'Accord-Cadre - 2 Marché Subséquent', 'AC-MS', 2, 31, 2, 0, 2, 0, '0', '1', 'Accord-Cadre - 2 Marché Subséquent', 'Framework agreement - 2 Individual contract', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '0', '0', '1', '10', 'ACS', 'Accordo quadro - 2 Apalto', '', '10', ''),
(32, 'a1a', 'Système d''Acquisition Dynamique - Admission', 'SAD-A', 8, 32, 4, 0, 2, 0, '0', '1', 'Système d''Acquisition Dynamique - Admission', 'Dynamic purchasing system - Admission', '', '', '', '', '', 0, '08', '1', '1', '<Autres/>', '0', '0', '0', '0', '1', '0', '9', 'SAA', 'Sistema dinamico di acquisizione - Ammissione', '', '10', ''),
(33, 'a1a', 'Système d''Acquisition Dynamique - Marché Spécifique', 'SAD-MS', 8, 33, 4, 0, 2, 0, '0', '1', 'Système d''Acquisition Dynamique - Marché Spécifique','Dynamicpurchasing system - Specific contract', '', '', '', '', '', 0, '08', '0', '0', '<Autres/>', '0', '0', '0', '0', '1', '0', '11', 'SAM', 'Sistema dinamico di acquisizione - Appalto', '', '8', '');

INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`,`libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`,`id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
(1000, 'a1a', 'Procédure avec négociation - Candidature', 'PAN-C', 1, 1000, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', NULL, '0', NULL, '0', '0', '', '1', '0', '0', '0', '0', '1', '', 'PCN', '', '', '1', '', '');

INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`,`libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`,`id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
(1004, 'a1a', 'Procédure avec négociation - Offre', 'PAN-O', 1, 1000, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', NULL, '0', NULL, '0', '0', '', '1', '0', '0', '0', '0', '1', '', 'PCN', '', '', '1', '', '');



-- INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`,`delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
-- (1001, 'a1a', 'Procédure adaptée services sociaux et services spécifiques < 90 k EUR HT', 'PA-SS-INF', '23', '3', '3', '0', '2', '0', '1', '1', '', '', '', '', '', '', '', '1', '9', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAI', '', '', '9', '', ''),
-- (1002, 'a1a', 'Procédure adaptée services sociaux et services spécifiques > 90 k EUR HT', 'PA-SS-SUP', '23', '3', '3', '0', '2', '0', '1', '1', '', '', '', '', '', '', '', '1', '9', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAS', '', '', '9', '', '');

INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`) VALUES
(1, 'a1a', '+1', '+0', '1', '0', '1', '0', '+1', '+1', '+0', '-1', '-1', '1', '0', '0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '+0', '', '', '+0', '+0', '+0', '+0', '+1', '1', '+1'),
(2, 'a1a', '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(3, 'a1a', '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(4, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(5, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '+1', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '+1', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(6, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(7, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(8, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '1', '-1', '-1', '1', '+0', '+0', '+0', '+0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '+1', '', '', '+0', '+0', '+0', '-0', '+1', '0', '-1'),
(9, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '1', '0', '0', '-1', '-1', '1', '+1', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(10, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '-0', '1', '1', '-1', '-1', '0', '+0', '+1', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(11, 'a1a', '1', '0', '+1', '+0', '+1', '+0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(12, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '-0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(17, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+0', '+0', '+1', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '-1', '-1', '-1', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(18, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '+0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '0', '-1'),
(30, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '-0', '1', '+0', '+0', '1', '0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(31, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '-0', '+0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(32, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(33, 'a1a', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-0', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+0', '+1', '+1','+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '+0', '+1', '1', '-1');

INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES ('1000', 'a1a', '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '+1', '', 'DAY', '+1', '+0', '-0');
INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES ('1004', 'a1a', '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '+1', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '+1', '', 'DAY', '+1', '+0', '-0');

-- INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`,`env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES
-- ('1001', 'a1a', '+1', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '-0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '-1', '1', 'DAY', '+0', '+1', '+1'),
-- ('1002', 'a1a', '+1', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '-0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '-1', '1', 'DAY', '+0', '+1', '+1');

INSERT IGNORE INTO `ValeurReferentielOrg` (`id`, `organisme`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_valeur_referentiel_it`) VALUES
(1, 'a1a', 1, 'Etat', 'Etat', 'State', '', '', '', '', '', 'Stato'),
(2, 'a1a', 1, 'Région', 'Région', 'Region', '', '', '', '', '', 'Regione'),
(3, 'a1a', 1, 'Département', 'Département', 'County', '', '', '', '', '', 'Reparto'),
(4, 'a1a', 1, 'Commune', 'Commune', 'City', '', '', '', '', '', 'Città'),
(5, 'a1a', 1, 'Etablissement public national', 'Etablissement public national', 'National publi authority', '', '', '', '', '', 'Ente pubblico nazionale'),
(6, 'a1a', 1, 'Etablissement public territorial', 'Etablissement public territorial', 'Local public authority', '', '', '', '', '', 'Ente pubblico locale'),
(7, 'a1a', 1, 'Autre', 'Autre', 'Other', '', '', '', '', '', 'Altro'),
(9, 'a1a', 3, 'Non renseigné', 'Non renseigné', 'Not filled-in', '', '', '', '', '', 'Sconosciuto'),
(10, 'a1a', 3, 'Oui', 'Oui', 'Yes', '', '', '', '', '', 'Sì'),
(11, 'a1a', 3, 'Non', 'Non', 'No', '', '', '', '', '', 'Non'),
(12, 'a1a', 2, '4 000 - 19 999.99', '4 000 HT à 19 999,99 HT', '4 000 - 19 999.99', '', '', '', '', '', '4 000 - 19 999.99'),
(13, 'a1a', 2, '20 000 - 49 999.99', '20 000 HT à 49 999,99 HT', '20 000 - 49 999.99', '', '', '', '', '', '20 000 - 49 999.99'),
(14, 'a1a', 2, '50 000 - 89 999.99', '50 000 HT à 89 999,99 HT', '50 000 - 89 999.99', '', '', '', '', '', '50 000 - 89 999.99'),
(15, 'a1a', 2, '90 000 - 124 999.99', '90 000 HT à 124 999,99 HT', '90 000 - 124 999.99', '', '', '', '', '', '90 000 - 124 999.99'),
(16, 'a1a', 2, '125 000 - 192 999.99', '125 000 HT à 192 999,99 HT', '125 000 - 192 999.99', '', '', '', '', '', '125 000 - 192 999.99'),
(17, 'a1a', 2, '193 000 - 999 999.99', '193 000 HT à 999 999,99 HT', '193 000 - 999 999.99', '', '', '', '', '', '193 000 - 999 999.99'),
(18, 'a1a', 2, '1 000 000 - 2 999 999.99', '1 000 000 HT à 2 999 999,99 HT', '1 000 000 - 2 999 999.99', '', '', '', '', '', '1 000 000 - 2 999 999.99'),
(19, 'a1a', 2, '3 000 000 - 4 844 999.99', '3 000 000 HT à 4 844 999,99 HT', '3 000 000 - 4 844 999.99', '', '', '', '', '', '3 000 000 - 4 844 999.99'),
(20, 'a1a', 2, '4 845 000 - ...', '4 845 000 HT et plus.', '4 845 000 - ...', '', '', '', '', '', '4 845 000 - ...');

INSERT IGNORE INTO `ReferentielOrg` VALUES
(1,'a1a', 'Type organismes'),
(2,'a1a', 'Tranche budgétaire'),
(3,'a1a', 'pme pmi');

INSERT IGNORE INTO `Centrale_publication` ( `id` , `organisme` , `nom` , `mail` ) VALUES (NULL , 'a1a', 'MEDIALEX', 'technique@atexo.com');
INSERT IGNORE INTO `Journaux` ( `ID_JOURNAL` , `ORGANISME` , `ID_CENTRALE` , `NOM_JOURNAL` ) VALUES
(1 , 'a1a' , '1', 'Ouest France'),
(2 , 'a1a' , '1', 'Le maine'),
(3 , 'a1a' , '1', 'Presse Océan'),
(4 , 'a1a' , '1', 'La Presse de la Manche');


INSERT IGNORE INTO `ReferentielOrg` (`id_referentiel`, `organisme`, `libelle_referentiel`) VALUES
(15, 'a1a', 'Type contrat');

INSERT IGNORE INTO `ValeurReferentielOrg` (`id` ,`organisme`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_valeur_referentiel_it`) VALUES
(25, 'a1a', 15, 'Marché public', 'Marché public', '(en) Marché public', '', '', '', '', '', ''),
(26, 'a1a', 15, 'Accord cadre', 'Accord cadre', '(en) Accord cadre', '', '', '', '', '', ''),
(27, 'a1a', 15, 'Système d''Acquisition Dynamique', 'Système d''Acquisition Dynamique', '(en) Système d''Acquisition Dynamique', '', '', '', '', '', ''),
(28, 'a1a', 15, 'Concession / DSP', 'Concession / DSP', '(en) Concession / DSP', '', '', '', '', '', ''),
-- (29, 'a1a', 15, 'Accord-cadre à bon de commande', 'Accord-cadre à bon de commande', '(en) Accord-cadre à bon de commande', '', '', '', '', '', ''),
-- (30, 'a1a', 15, 'Accord-cadre à marché mixte', 'Accord-cadre à marché mixte', '(en) Accord-cadre à marché mixte', '', '', '', '', '', ''),
(31, 'a1a', 15, 'Marché subséquent à accord-cadre', 'Marché subséquent à accord-cadre', '(en) Marché subséquent à accord-cadre', '', '', '', '', '', ''),
(32, 'a1a', 15, 'Marché de partenariat', 'Marché de partenariat', '(en) Marché de partenariat', '', '', '', '', '', ''),
(33, 'a1a', 15, 'Partenariat d''innovation', 'Partenariat d''innovation', '(en) Partenariat d''innovation', '', '', '', '', '', ''),
(34, 'a1a', 15, 'Marché spécifique (suite SAD)', 'Marché spécifique (suite SAD)', '(en) Marché spécifique (suite SAD)', '', '', '', '', '', ''),
(35, 'a1a', 15, 'Marché public de conception réalisation', 'Marché public de conception réalisation', '(en) Marché public de conception réalisation', '', '', '', '', '', ''),
(36, 'a1a', 15, 'Autre', 'Autre', '(en) Autre', '', '', '', '', '', '');

--
-- Contenu de la table `TypeCommission`
--

INSERT IGNORE INTO `TypeCommission` (`id`, `organisme`, `libelle`) VALUES
(1, 'a1a', 'Commission d''ouverture des plis'),
(2, 'a1a', 'Commission d''appel d''offres'),
(3, 'a1a', 'Commission - Autre'),
(4, 'a1a', 'Jury - Phase 1'),
(5, 'a1a', 'Jury - Phase 2'),
(6, 'a1a', 'Jury - Autre');



INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 2, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 2, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 2, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 2, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 2, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 2, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 2, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 2, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,2, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,2, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,2, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,2, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,2, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,2, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,2, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 2), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 3, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 3, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 3, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 3, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 3, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 3, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 3, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 3, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 3, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 3, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 3, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 3, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 3, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 3, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 3, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 3), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 4, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 4, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 4, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 4, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 4, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 4, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 4, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 4, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 4, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 4, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 4, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 4, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 4, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 4, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 4, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 4), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 5, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 5, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 5, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 5, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 5, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 5, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 5, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 5, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 5, 'a1a',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 5, 'a1a',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 5, 'a1a',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 5, 'a1a',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 5, 'a1a',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 5, 'a1a',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 5, 'a1a',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 5), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 6, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 6, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 6, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 6, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 6, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 6, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 6, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 6, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 6, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 6, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 6, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 6, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 6, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 6, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 6, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 7, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 7, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 7, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 7, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 7, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 7, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 7, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 7, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 7, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 7, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 7, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 7, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 7, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 7, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 7, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 7), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 8, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 8, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 8, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 8, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 8, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 8, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 8, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 8, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 8, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 8, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 8, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 6), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 8, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 8, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 8, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 8, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 8), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (181, 9, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (182, 9, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (183, 9, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (184, 9, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (185, 9, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (186, 9, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (187, 9, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (188, 9, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (181,9, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (182,9, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (183,9, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (184,9, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (185,9, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (186,9, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (187,9, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 9), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 10, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 10, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 10, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 10, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 10, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 10, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 10, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 10, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 10, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 10, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 10, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 10, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 10, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 10, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 10, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 10), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 11, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 11, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 11, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 11, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 11, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 11, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 11, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 11, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 11, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 11, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 11, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 11, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 11, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 11, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 11, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 11), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 12, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 12, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 12, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 12, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 12, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 12, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 12, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 12, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,12, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,12, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,12, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,12, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,12, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,12, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,12, 'a1a' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 12), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 17, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 17, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 17, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 17, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 17, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 17, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 17, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 17, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,17, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,17, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,17, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,17, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,17, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,17, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,17, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 17), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 18, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 18, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 18, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 18, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 18, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 18, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 18, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 18, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,18, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,18, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,18, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,18, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,18, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,18, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,18, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 18), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 30, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 30, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 30, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 30, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 30, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 30, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 30, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 30, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,30, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,30, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,30, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,30, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,30, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,30, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,30, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 30), 0, -7);


INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 31, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 31, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 31, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 31, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 31, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 31, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 31, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 31, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,31, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,31, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,31, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,31, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,31, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,31, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,31, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 31), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 32, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 32, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 32, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 32, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 32, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 32, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 32, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 32, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,32, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,32, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,32, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,32, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,32, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,32, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,32, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 32), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 33, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 33, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 33, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 33, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 33, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 33, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 33, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 33, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,33, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,33, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,33, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,33, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,33, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,33, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,33, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 33), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1000, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1000, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1000, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1000, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1000, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1000, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1000, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1000, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1000, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1000, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1000, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1000, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1000, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1000, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME ='a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1000, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1004, 'a1a' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1004, 'a1a' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1004, 'a1a' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1004, 'a1a' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1004, 'a1a' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1004, 'a1a' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1004, 'a1a' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1004, 'a1a' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1004, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1004, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1004, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1004, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1004, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1004, 'a1a' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME ='a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1004, 'a1a' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1a' AND ID_TYPE_PROCEDURE = 1), 0, -7);


INSERT IGNORE INTO `Tranche_Article_133` ( `acronyme_org` , `millesime` , `Libelle_tranche_budgetaire` , `borne_inf`, `borne_sup` ) VALUES
('a1a', '2010', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1a', '2010', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1a', '2010', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1a', '2010', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1a', '2010', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1a', '2010', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1a', '2010', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1a', '2010', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1a', '2010', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1a', '2010', '4 845 000 HT et plus.', '4845000', ''),
('a1a', '2011', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1a', '2011', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1a', '2011', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1a', '2011', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1a', '2011', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1a', '2011', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1a', '2011', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1a', '2011', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1a', '2011', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1a', '2011', '4 845 000 HT et plus.', '4845000', ''),
('a1a', '2012', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1a', '2012', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1a', '2012', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1a', '2012', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1a', '2012', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1a', '2012', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1a', '2012', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1a', '2012', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1a', '2012', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1a', '2012', '4 845 000 HT et plus.', '4845000', ''),
('a1a', '2013', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1a', '2013', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1a', '2013', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1a', '2013', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1a', '2013', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1a', '2013', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1a', '2013', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1a', '2013', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1a', '2013', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1a', '2013', '4 845 000 HT et plus.', '4845000', ''),
('a1a', '2014', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1a', '2014', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1a', '2014', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1a', '2014', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1a', '2014', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1a', '2014', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1a', '2014', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1a', '2014', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1a', '2014', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1a', '2014', '4 845 000 HT et plus.', '4845000', ''),
('a1a', '2015', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1a', '2015', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1a', '2015', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1a', '2015', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1a', '2015', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1a', '2015', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1a', '2015', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1a', '2015', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1a', '2015', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1a', '2015', '4 845 000 HT et plus.', '4845000', '');

INSERT IGNORE INTO `t_type_contrat_et_procedure` (`id_type_contrat_et_procedure`,`organisme`, `id_type_contrat`, `id_type_procedure`) VALUES
(NULL,'a1a', 2, 1),
(NULL,'a1a', 4, 1),
(NULL,'a1a', 5, 1),
(NULL,'a1a', 6, 1),
(NULL,'a1a', 1, 1),
(NULL,'a1a', 5, 2),
(NULL,'a1a', 4, 2),
(NULL,'a1a', 11, 2),
(NULL,'a1a', 8, 2),
(NULL,'a1a', 6, 2),
(NULL,'a1a', 2, 2),
(NULL,'a1a', 1, 2),
(NULL,'a1a', 8, 3),
(NULL,'a1a', 4, 3),
(NULL,'a1a', 1, 3),
(NULL,'a1a', 6, 3),
(NULL,'a1a', 2, 3),
(NULL,'a1a', 5, 3),
(NULL,'a1a', 11, 3),
(NULL,'a1a', 6, 4),
(NULL,'a1a', 11, 4),
(NULL,'a1a', 1, 4),
(NULL,'a1a', 5, 4),
(NULL,'a1a', 9, 4),
(NULL,'a1a', 4, 4),
(NULL,'a1a', 2, 4),
(NULL,'a1a', 4, 5),
(NULL,'a1a', 1, 5),
(NULL,'a1a', 5, 5),
(NULL,'a1a', 6, 5),
(NULL,'a1a', 11, 5),
(NULL,'a1a', 9, 5),
(NULL,'a1a', 2, 5),
(NULL,'a1a', 6, 6),
(NULL,'a1a', 1, 6),
(NULL,'a1a', 5, 6),
(NULL,'a1a', 2, 6),
(NULL,'a1a', 5, 7),
(NULL,'a1a', 1, 7),
(NULL,'a1a', 2, 7),
(NULL,'a1a', 6, 7),
(NULL,'a1a', 1, 8),
(NULL,'a1a', 6, 8),
(NULL,'a1a', 1, 9),
(NULL,'a1a', 6, 9),
(NULL,'a1a', 1, 10),
(NULL,'a1a', 6, 10),
(NULL,'a1a', 11, 11),
(NULL,'a1a', 6, 11),
(NULL,'a1a', 8, 11),
(NULL,'a1a', 4, 11),
(NULL,'a1a', 2, 11),
(NULL,'a1a', 5, 11),
(NULL,'a1a', 1, 11),
(NULL,'a1a', 5, 12),
(NULL,'a1a', 8, 12),
(NULL,'a1a', 1, 12),
(NULL,'a1a', 6, 12),
(NULL,'a1a', 11, 12),
(NULL,'a1a', 4, 12),
(NULL,'a1a', 2, 12),
(NULL,'a1a', 12, 17),
(NULL,'a1a', 2, 18),
(NULL,'a1a', 1, 18),
(NULL,'a1a', 6, 18),
(NULL,'a1a', 4, 18),
(NULL,'a1a', 5, 18),
(NULL,'a1a', 6, 30),
(NULL,'a1a', 7, 30),
(NULL,'a1a', 8, 30),
(NULL,'a1a', 9, 30),
(NULL,'a1a', 10, 30),
(NULL,'a1a', 11, 30),
(NULL,'a1a', 5, 30),
(NULL,'a1a', 4, 30),
(NULL,'a1a', 3, 30),
(NULL,'a1a', 2, 30),
(NULL,'a1a', 1, 30),
(NULL,'a1a', 12, 30),
(NULL,'a1a', 7, 31),
(NULL,'a1a', 3, 32),
(NULL,'a1a', 10, 33);



INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (1,1000,'a1a');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (2,1000,'a1a');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (4,1000,'a1a');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (5,1000,'a1a');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (6,1000,'a1a');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (8,1000,'a1a');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (11,1000,'a1a');


UPDATE `configuration_organisme` SET `type_contrat` = '1' WHERE `configuration_organisme`.`organisme` = 'a1a';

--  a2z --

INSERT IGNORE INTO `CompteMoniteur` VALUES
(NULL, 'a2z' , 'Atexo', 'mdp08eT@', 'suivi.mpe@atexo.com', '1'),
(NULL, 'a2z' , 'Atexo', '@texo', 'dev.pmi@atexo.biz', '0');

INSERT IGNORE INTO `Mesure_Type_Procedure` VALUES
( NULL, 'a2z', 'Appel d''offres ouvert'),
(NULL, 'a2z' , 'Appel d''offres restreint'),
(NULL, 'a2z' , 'Dialogue compétitif'),
(NULL, 'a2z' , 'Marché à  procédure adaptée'),
(NULL, 'a2z' , 'Marché négocié avec publicité préalable et mise en concurrence'),
(NULL, 'a2z' , 'Concours'),
(NULL, 'a2z' , 'Procédure spécifique à certains marchés de défense nationale'),
(NULL, 'a2z' , 'Accord cadre'),
(NULL, 'a2z' , 'Système d''acquisition dynamique'),
(NULL ,'a2z' , 'Marché subséquent à accord cadre'),
(NULL ,'a2z' , 'Marché spécifique sur système acquisition dynamique');

INSERT IGNORE INTO `ReferentielDestinationFormXml` VALUES
(1,'a2z', 'BOAMP seul'),
(2,'a2z', 'BOAMP et JOUE'),
(3,'a2z', 'Groupe Moniteur');
INSERT IGNORE INTO `ReferentielTypeXml` ( `id` , `organisme` , `id_destinataire` , `libelle_type` ) VALUES
(1,'a2z', '1#3', 'XML 1.9 BOAMP3 - Avis de marché National'),
(2,'a2z', '1#3', 'XML 1.9 BOAMP6 - Avis de MAPA'),
(3,'a2z', '2#3', 'XML 1.9 JOUE_02 - Avis de marché'),
(4,'a2z', '2#3', 'XML 1.9 JOUE_03 - Avis d''attribution'),
(5,'a2z', '1#3', 'XML 1.9 BOAMP4 - Avis d''attribution'),
(6,'a2z', '1#3', 'XML 1.9 BOAMP2 - Avis rectificatif'),
(7,'a2z', '1#3', 'XML 1.9 BOAMP7 - Résultat de MAPA'),
(8,'a2z', '2#3', 'XML 1.9 JOUE_12 - Avis Concours'),
(9,'a2z', '2#3', 'XML 1.9 JOUE_14 - Formulaire rectificatif / annulation'),
(10,'a2z', '2#3', 'XML 1.9 JOUE_05 - Secteurs Spéciaux'),
(11,'a2z', '2', 'XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire'),
(12,'a2z', '2', 'XML 1.9 JOUE_13 - Résultat Concours'),
(13,'a2z', '2', 'XML 1.9 JOUE_10 - Concession de travaux publics'),
(14,'a2z', '2', 'XML 1.9 JOUE_01T - Avis de préinformation - Travaux'),
(15,'a2z', '2', 'XML 1.9 JOUE_01FS - Avis de préinformation - Fournitures / Services'),
(16,'a2z', '1', 'XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification'),
(17,'a2z', '2#3', 'XML 1.9 JOUE_06 -  Avis d''attribution - Secteurs Spéciaux'),
(18,'a2z', '2', 'XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire');
INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`) VALUES
(1, 'a2z', 'Appel d''offres ouvert', 'AOO', 1, 1, 1, 0, 2, 0, '0', '1', 'Appel d''offres ouvert', 'Open procedure', '', '', '', '', '', 0, '01', '0', '0', '<AppelOuvert/>', '1', '0', '0', '0', '0', '0', '1', 'AOO', 'Procedura apperta', '', '1', 'OPEN'),
(2, 'a2z', 'Appel d''offres restreint - Candidature', 'AOR-C', 2, 2, 2, 0, 2, 0, '0', '1', 'Appel d''offres restreint - Candidature', 'Restricted procedure - preselection', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARC', 'Procedura ristretta - qualificazione', '', '10', 'RESTRICTED'),
(3, 'a2z', 'Appel d''offres restreint - Offre', 'AOR-O', 2, 2, 2, 0, 2, 0, '0', '1', 'Appel d''offres restreint - Offre', 'Restricted procedure - bid', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARO', 'Procedura ristretta - offerta', '', '2', 'RESTRICTED'),
(4, 'a2z', 'Procédure adaptée < 90 k EUR HT', 'PA-INF', 23, 3, 3, 0, 2, 0, '1', '1', 'Procédure adaptée < 90 k EUR HT', 'Procedure below thresholds < 90 k EUR', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAI', 'Procedura sotto soglia < 90 k EUR', '', '9', ''),
(5, 'a2z', 'Procédure adaptée >  90 k EUR HT', 'PA-SUP', 23, 3, 3, 0, 2, 0, '1', '1', 'Procédure adaptée >  90 k EUR HT', 'Procedure below thresholds >  90 k EUR', '', '', '', '', '', 2, '', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAS', 'Procedura sotto soglia >  90 k EUR', '', '9', ''),
(6, 'a2z', 'Procédure adaptée spécifique article 30 < 90 k EUR HT', 'PA-A30-INF', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSI', '', '', '9', ''),
(7, 'a2z', 'Procédure adaptée spécifique article 30 >  90 k EUR HT', 'PA-A30-SUP', 23, 3, 0, 0, 2, 0, '1', '1', '', '', '', '', '', '', '', 2, '', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSS', '', '', '9', ''),
(8, 'a2z', 'Concours ouvert', 'CO', 7, 4, 4, 0, 2, 0, '0', '1', 'Concours ouvert', 'Open design contest', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursOuvert/>', '0', '0', '0', '1', '0', '0', '6', 'COO', 'Concorso apperto', '', '7', 'OPEN'),
(9, 'a2z', 'Concours restreint - Candidature', 'CR-C', 8, 5, 4, 0, 2, 0, '0', '1', 'Concours restreint - Candidature', 'Restricted design contest - preselection', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRC', 'Concorso ristretto - qualificazione', '', '10', 'RESTRICTED'),
(10, 'a2z', 'Concours restreint - Offre', 'CR-O', 8, 5, 4, 0, 2, 0, '0', '1', 'Concours restreint - Offre', 'Restricted design contest - bid', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRO', 'Concorso ristretto - offerta', '', '7', 'RESTRICTED'),
(11, 'a2z', 'Dialogue compétitif - Candidature', 'DC-C', 22, 7, 4, 0, 2, 0, '0', '1', 'Dialogue compétitif - Candidature', 'Competitive dialogue - preselection', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCC', 'Dialogo competitivo - qualificazione', '', '10', 'COMPETITIVE_DIALOGUE'),
(12, 'a2z', 'Dialogue compétitif - Offre', 'DC-O', 22, 7, 4, 0, 2, 0, '0', '1', 'Dialogue compétitif - Offre', 'Competitive dialogue - bid', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCO', 'Dialogo competitivo - offerta', '', '6', 'COMPETITIVE_DIALOGUE'),
(17, 'a2z', 'Procédure Autre', 'AUT', 24, 15, 4, 0, 2, 0, '0', '1', 'Procédure Autre', 'Other procedure', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '1', '0', '0', '', 'ATR', 'Altra procedura', '', '10', ''),
(18, 'a2z', 'Marché négocié', 'MN', 18, 8, 4, 0, 2, 0, '0', '1', 'Marché négocié', 'Negociated procedure', '', '', '', '', '', 0, '10', '0', '0', '<MarcheNegocie/>', '0', '1', '0', '0', '0', '0', '5', 'MNE', 'Procedura negoziata', '', '4', 'NEGOTIATED'),
(30, 'a2z', 'Accord-Cadre - 1 Sélection des Attributaires', 'AC-SA', 1, 30, 1, 0, 2, 0, '0', '1', 'Accord-Cadre - 1 Sélection des Attributaires', 'Framework agreement - 1 Selection of suppliers', '', '', '', '', '', 0, '10', '0', '1', '<Autres/>', '0', '0', '0', '0', '0', '1', '8', 'ACA', 'Accordo quadro - 1 Qualificazione dei titolare', '', '10', ''),
(31, 'a2z', 'Accord-Cadre - 2 Marché Subséquent', 'AC-MS', 2, 31, 2, 0, 2, 0, '0', '1', 'Accord-Cadre - 2 Marché Subséquent', 'Framework agreement - 2 Individual contract', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '0', '0', '1', '10', 'ACS', 'Accordo quadro - 2 Apalto', '', '10', ''),
(32, 'a2z', 'Système d''Acquisition Dynamique - Admission', 'SAD-A', 8, 32, 4, 0, 2, 0, '0', '1', 'Système d''Acquisition Dynamique - Admission', 'Dynamic purchasing system - Admission', '', '', '', '', '', 0, '08', '1', '1', '<Autres/>', '0', '0', '0', '0', '1', '0', '9', 'SAA', 'Sistema dinamico di acquisizione - Ammissione', '', '10', ''),
(33, 'a2z', 'Système d''Acquisition Dynamique - Marché Spécifique', 'SAD-MS', 8, 33, 4, 0, 2, 0, '0', '1', 'Système d''Acquisition Dynamique - Marché Spécifique','Dynamicpurchasing system - Specific contract', '', '', '', '', '', 0, '08', '0', '0', '<Autres/>', '0', '0', '0', '0', '1', '0', '11', 'SAM', 'Sistema dinamico di acquisizione - Appalto', '', '8', '');

INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`,`libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`,`id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
(1000, 'a2z', 'Procédure avec négociation - Candidature', 'PAN-C', 1, 1000, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', NULL, '0', NULL, '0', '0', '', '1', '0', '0', '0', '0', '1', '', 'PCN', '', '', '1', '', '');

INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`,`libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`,`id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
(1004, 'a2z', 'Procédure avec négociation - Offre', 'PAN-O', 1, 1000, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', NULL, '0', NULL, '0', '0', '', '1', '0', '0', '0', '0', '1', '', 'PCN', '', '', '1', '', '');



-- INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`,`delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
-- (1001, 'a2z', 'Procédure adaptée services sociaux et services spécifiques < 90 k EUR HT', 'PA-SS-INF', '23', '3', '3', '0', '2', '0', '1', '1', '', '', '', '', '', '', '', '1', '9', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAI', '', '', '9', '', ''),
-- (1002, 'a2z', 'Procédure adaptée services sociaux et services spécifiques > 90 k EUR HT', 'PA-SS-SUP', '23', '3', '3', '0', '2', '0', '1', '1', '', '', '', '', '', '', '', '1', '9', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAS', '', '', '9', '', '');

INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`) VALUES
(1, 'a2z', '+1', '+0', '1', '0', '1', '0', '+1', '+1', '+0', '-1', '-1', '1', '0', '0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '+0', '', '', '+0', '+0', '+0', '+0', '+1', '1', '+1'),
(2, 'a2z', '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(3, 'a2z', '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(4, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(5, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '+1', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '+1', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(6, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(7, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(8, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '1', '-1', '-1', '1', '+0', '+0', '+0', '+0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '+1', '', '', '+0', '+0', '+0', '-0', '+1', '0', '-1'),
(9, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '1', '0', '0', '-1', '-1', '1', '+1', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(10, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '-0', '1', '1', '-1', '-1', '0', '+0', '+1', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(11, 'a2z', '1', '0', '+1', '+0', '+1', '+0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(12, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '-0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(17, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+0', '+0', '+1', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '-1', '-1', '-1', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(18, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '+0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '0', '-1'),
(30, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '-0', '1', '+0', '+0', '1', '0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(31, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '-0', '+0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(32, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(33, 'a2z', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-0', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+0', '+1', '+1','+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '+0', '+1', '1', '-1');

INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES ('1000', 'a2z', '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '+1', '', 'DAY', '+1', '+0', '-0');
INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES ('1004', 'a2z', '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '+1', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '+1', '', 'DAY', '+1', '+0', '-0');

-- INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`,`env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES
-- ('1001', 'a2z', '+1', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '-0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '-1', '1', 'DAY', '+0', '+1', '+1'),
-- ('1002', 'a2z', '+1', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '-0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '-1', '1', 'DAY', '+0', '+1', '+1');

INSERT IGNORE INTO `ValeurReferentielOrg` (`id`, `organisme`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_valeur_referentiel_it`) VALUES
(1, 'a2z', 1, 'Etat', 'Etat', 'State', '', '', '', '', '', 'Stato'),
(2, 'a2z', 1, 'Région', 'Région', 'Region', '', '', '', '', '', 'Regione'),
(3, 'a2z', 1, 'Département', 'Département', 'County', '', '', '', '', '', 'Reparto'),
(4, 'a2z', 1, 'Commune', 'Commune', 'City', '', '', '', '', '', 'Città'),
(5, 'a2z', 1, 'Etablissement public national', 'Etablissement public national', 'National publi authority', '', '', '', '', '', 'Ente pubblico nazionale'),
(6, 'a2z', 1, 'Etablissement public territorial', 'Etablissement public territorial', 'Local public authority', '', '', '', '', '', 'Ente pubblico locale'),
(7, 'a2z', 1, 'Autre', 'Autre', 'Other', '', '', '', '', '', 'Altro'),
(9, 'a2z', 3, 'Non renseigné', 'Non renseigné', 'Not filled-in', '', '', '', '', '', 'Sconosciuto'),
(10, 'a2z', 3, 'Oui', 'Oui', 'Yes', '', '', '', '', '', 'Sì'),
(11, 'a2z', 3, 'Non', 'Non', 'No', '', '', '', '', '', 'Non'),
(12, 'a2z', 2, '4 000 - 19 999.99', '4 000 HT à 19 999,99 HT', '4 000 - 19 999.99', '', '', '', '', '', '4 000 - 19 999.99'),
(13, 'a2z', 2, '20 000 - 49 999.99', '20 000 HT à 49 999,99 HT', '20 000 - 49 999.99', '', '', '', '', '', '20 000 - 49 999.99'),
(14, 'a2z', 2, '50 000 - 89 999.99', '50 000 HT à 89 999,99 HT', '50 000 - 89 999.99', '', '', '', '', '', '50 000 - 89 999.99'),
(15, 'a2z', 2, '90 000 - 124 999.99', '90 000 HT à 124 999,99 HT', '90 000 - 124 999.99', '', '', '', '', '', '90 000 - 124 999.99'),
(16, 'a2z', 2, '125 000 - 192 999.99', '125 000 HT à 192 999,99 HT', '125 000 - 192 999.99', '', '', '', '', '', '125 000 - 192 999.99'),
(17, 'a2z', 2, '193 000 - 999 999.99', '193 000 HT à 999 999,99 HT', '193 000 - 999 999.99', '', '', '', '', '', '193 000 - 999 999.99'),
(18, 'a2z', 2, '1 000 000 - 2 999 999.99', '1 000 000 HT à 2 999 999,99 HT', '1 000 000 - 2 999 999.99', '', '', '', '', '', '1 000 000 - 2 999 999.99'),
(19, 'a2z', 2, '3 000 000 - 4 844 999.99', '3 000 000 HT à 4 844 999,99 HT', '3 000 000 - 4 844 999.99', '', '', '', '', '', '3 000 000 - 4 844 999.99'),
(20, 'a2z', 2, '4 845 000 - ...', '4 845 000 HT et plus.', '4 845 000 - ...', '', '', '', '', '', '4 845 000 - ...');

INSERT IGNORE INTO `ReferentielOrg` VALUES
(1,'a2z', 'Type organismes'),
(2,'a2z', 'Tranche budgétaire'),
(3,'a2z', 'pme pmi');

INSERT IGNORE INTO `Centrale_publication` ( `id` , `organisme` , `nom` , `mail` ) VALUES (NULL , 'a2z', 'MEDIALEX', 'technique@atexo.com');
INSERT IGNORE INTO `Journaux` ( `ID_JOURNAL` , `ORGANISME` , `ID_CENTRALE` , `NOM_JOURNAL` ) VALUES
(1 , 'a2z' , '1', 'Ouest France'),
(2 , 'a2z' , '1', 'Le maine'),
(3 , 'a2z' , '1', 'Presse Océan'),
(4 , 'a2z' , '1', 'La Presse de la Manche');


INSERT IGNORE INTO `ReferentielOrg` (`id_referentiel`, `organisme`, `libelle_referentiel`) VALUES
(15, 'a2z', 'Type contrat');

INSERT IGNORE INTO `ValeurReferentielOrg` (`id` ,`organisme`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_valeur_referentiel_it`) VALUES
(25, 'a2z', 15, 'Marché public', 'Marché public', '(en) Marché public', '', '', '', '', '', ''),
(26, 'a2z', 15, 'Accord cadre', 'Accord cadre', '(en) Accord cadre', '', '', '', '', '', ''),
(27, 'a2z', 15, 'Système d''Acquisition Dynamique', 'Système d''Acquisition Dynamique', '(en) Système d''Acquisition Dynamique', '', '', '', '', '', ''),
(28, 'a2z', 15, 'Concession / DSP', 'Concession / DSP', '(en) Concession / DSP', '', '', '', '', '', ''),
-- (29, 'a2z', 15, 'Accord-cadre à bon de commande', 'Accord-cadre à bon de commande', '(en) Accord-cadre à bon de commande', '', '', '', '', '', ''),
-- (30, 'a2z', 15, 'Accord-cadre à marché mixte', 'Accord-cadre à marché mixte', '(en) Accord-cadre à marché mixte', '', '', '', '', '', ''),
(31, 'a2z', 15, 'Marché subséquent à accord-cadre', 'Marché subséquent à accord-cadre', '(en) Marché subséquent à accord-cadre', '', '', '', '', '', ''),
(32, 'a2z', 15, 'Marché de partenariat', 'Marché de partenariat', '(en) Marché de partenariat', '', '', '', '', '', ''),
(33, 'a2z', 15, 'Partenariat d''innovation', 'Partenariat d''innovation', '(en) Partenariat d''innovation', '', '', '', '', '', ''),
(34, 'a2z', 15, 'Marché spécifique (suite SAD)', 'Marché spécifique (suite SAD)', '(en) Marché spécifique (suite SAD)', '', '', '', '', '', ''),
(35, 'a2z', 15, 'Marché public de conception réalisation', 'Marché public de conception réalisation', '(en) Marché public de conception réalisation', '', '', '', '', '', ''),
(36, 'a2z', 15, 'Autre', 'Autre', '(en) Autre', '', '', '', '', '', '');

--
-- Contenu de la table `TypeCommission`
--

INSERT IGNORE INTO `TypeCommission` (`id`, `organisme`, `libelle`) VALUES
(1, 'a2z', 'Commission d''ouverture des plis'),
(2, 'a2z', 'Commission d''appel d''offres'),
(3, 'a2z', 'Commission - Autre'),
(4, 'a2z', 'Jury - Phase 1'),
(5, 'a2z', 'Jury - Phase 2'),
(6, 'a2z', 'Jury - Autre');



INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 2, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 2, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 2, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 2, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 2, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 2, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 2, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 2, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,2, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,2, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,2, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,2, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,2, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,2, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,2, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 2), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 3, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 3, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 3, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 3, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 3, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 3, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 3, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 3, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 3, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 3, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 3, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 3, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 3, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 3, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 3, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 3), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 4, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 4, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 4, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 4, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 4, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 4, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 4, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 4, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 4, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 4, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 4, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 4, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 4, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 4, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 4, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 4), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 5, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 5, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 5, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 5, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 5, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 5, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 5, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 5, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 5, 'a2z',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 5, 'a2z',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 5, 'a2z',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 5, 'a2z',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 5, 'a2z',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 5, 'a2z',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 5, 'a2z',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 5), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 6, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 6, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 6, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 6, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 6, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 6, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 6, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 6, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 6, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 6, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 6, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 6, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 6, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 6, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 6, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 7, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 7, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 7, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 7, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 7, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 7, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 7, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 7, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 7, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 7, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 7, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 7, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 7, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 7, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 7, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 7), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 8, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 8, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 8, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 8, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 8, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 8, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 8, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 8, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 8, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 8, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 8, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 6), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 8, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 8, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 8, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 8, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 8), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (181, 9, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (182, 9, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (183, 9, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (184, 9, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (185, 9, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (186, 9, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (187, 9, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (188, 9, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (181,9, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (182,9, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (183,9, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (184,9, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (185,9, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (186,9, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (187,9, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 9), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 10, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 10, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 10, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 10, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 10, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 10, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 10, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 10, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 10, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 10, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 10, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 10, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 10, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 10, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 10, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 10), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 11, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 11, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 11, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 11, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 11, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 11, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 11, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 11, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 11, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 11, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 11, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 11, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 11, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 11, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 11, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 11), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 12, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 12, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 12, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 12, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 12, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 12, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 12, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 12, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,12, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,12, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,12, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,12, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,12, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,12, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,12, 'a2z' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 12), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 17, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 17, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 17, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 17, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 17, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 17, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 17, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 17, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,17, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,17, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,17, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,17, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,17, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,17, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,17, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 17), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 18, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 18, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 18, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 18, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 18, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 18, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 18, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 18, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,18, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,18, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,18, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,18, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,18, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,18, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,18, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 18), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 30, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 30, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 30, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 30, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 30, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 30, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 30, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 30, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,30, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,30, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,30, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,30, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,30, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,30, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,30, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 30), 0, -7);


INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 31, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 31, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 31, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 31, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 31, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 31, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 31, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 31, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,31, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,31, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,31, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,31, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,31, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,31, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,31, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 31), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 32, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 32, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 32, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 32, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 32, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 32, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 32, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 32, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,32, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,32, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,32, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,32, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,32, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,32, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,32, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 32), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 33, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 33, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 33, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 33, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 33, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 33, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 33, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 33, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,33, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,33, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,33, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,33, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,33, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,33, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,33, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 33), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1000, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1000, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1000, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1000, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1000, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1000, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1000, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1000, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1000, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1000, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1000, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1000, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1000, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1000, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME ='a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1000, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1004, 'a2z' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1004, 'a2z' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1004, 'a2z' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1004, 'a2z' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1004, 'a2z' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1004, 'a2z' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1004, 'a2z' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1004, 'a2z' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1004, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1004, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1004, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1004, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1004, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1004, 'a2z' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME ='a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1004, 'a2z' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a2z' AND ID_TYPE_PROCEDURE = 1), 0, -7);


INSERT IGNORE INTO `Tranche_Article_133` ( `acronyme_org` , `millesime` , `Libelle_tranche_budgetaire` , `borne_inf`, `borne_sup` ) VALUES
('a2z', '2010', '0 à 3 999,99 HT', '0', '3 999,99'),
('a2z', '2010', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a2z', '2010', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a2z', '2010', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a2z', '2010', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a2z', '2010', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a2z', '2010', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a2z', '2010', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a2z', '2010', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a2z', '2010', '4 845 000 HT et plus.', '4845000', ''),
('a2z', '2011', '0 à 3 999,99 HT', '0', '3 999,99'),
('a2z', '2011', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a2z', '2011', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a2z', '2011', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a2z', '2011', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a2z', '2011', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a2z', '2011', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a2z', '2011', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a2z', '2011', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a2z', '2011', '4 845 000 HT et plus.', '4845000', ''),
('a2z', '2012', '0 à 3 999,99 HT', '0', '3 999,99'),
('a2z', '2012', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a2z', '2012', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a2z', '2012', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a2z', '2012', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a2z', '2012', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a2z', '2012', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a2z', '2012', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a2z', '2012', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a2z', '2012', '4 845 000 HT et plus.', '4845000', ''),
('a2z', '2013', '0 à 3 999,99 HT', '0', '3 999,99'),
('a2z', '2013', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a2z', '2013', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a2z', '2013', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a2z', '2013', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a2z', '2013', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a2z', '2013', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a2z', '2013', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a2z', '2013', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a2z', '2013', '4 845 000 HT et plus.', '4845000', ''),
('a2z', '2014', '0 à 3 999,99 HT', '0', '3 999,99'),
('a2z', '2014', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a2z', '2014', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a2z', '2014', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a2z', '2014', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a2z', '2014', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a2z', '2014', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a2z', '2014', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a2z', '2014', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a2z', '2014', '4 845 000 HT et plus.', '4845000', ''),
('a2z', '2015', '0 à 3 999,99 HT', '0', '3 999,99'),
('a2z', '2015', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a2z', '2015', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a2z', '2015', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a2z', '2015', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a2z', '2015', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a2z', '2015', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a2z', '2015', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a2z', '2015', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a2z', '2015', '4 845 000 HT et plus.', '4845000', '');

INSERT IGNORE INTO `t_type_contrat_et_procedure` (`id_type_contrat_et_procedure`,`organisme`, `id_type_contrat`, `id_type_procedure`) VALUES
(NULL,'a2z', 2, 1),
(NULL,'a2z', 4, 1),
(NULL,'a2z', 5, 1),
(NULL,'a2z', 6, 1),
(NULL,'a2z', 1, 1),
(NULL,'a2z', 5, 2),
(NULL,'a2z', 4, 2),
(NULL,'a2z', 11, 2),
(NULL,'a2z', 8, 2),
(NULL,'a2z', 6, 2),
(NULL,'a2z', 2, 2),
(NULL,'a2z', 1, 2),
(NULL,'a2z', 8, 3),
(NULL,'a2z', 4, 3),
(NULL,'a2z', 1, 3),
(NULL,'a2z', 6, 3),
(NULL,'a2z', 2, 3),
(NULL,'a2z', 5, 3),
(NULL,'a2z', 11, 3),
(NULL,'a2z', 6, 4),
(NULL,'a2z', 11, 4),
(NULL,'a2z', 1, 4),
(NULL,'a2z', 5, 4),
(NULL,'a2z', 9, 4),
(NULL,'a2z', 4, 4),
(NULL,'a2z', 2, 4),
(NULL,'a2z', 4, 5),
(NULL,'a2z', 1, 5),
(NULL,'a2z', 5, 5),
(NULL,'a2z', 6, 5),
(NULL,'a2z', 11, 5),
(NULL,'a2z', 9, 5),
(NULL,'a2z', 2, 5),
(NULL,'a2z', 6, 6),
(NULL,'a2z', 1, 6),
(NULL,'a2z', 5, 6),
(NULL,'a2z', 2, 6),
(NULL,'a2z', 5, 7),
(NULL,'a2z', 1, 7),
(NULL,'a2z', 2, 7),
(NULL,'a2z', 6, 7),
(NULL,'a2z', 1, 8),
(NULL,'a2z', 6, 8),
(NULL,'a2z', 1, 9),
(NULL,'a2z', 6, 9),
(NULL,'a2z', 1, 10),
(NULL,'a2z', 6, 10),
(NULL,'a2z', 11, 11),
(NULL,'a2z', 6, 11),
(NULL,'a2z', 8, 11),
(NULL,'a2z', 4, 11),
(NULL,'a2z', 2, 11),
(NULL,'a2z', 5, 11),
(NULL,'a2z', 1, 11),
(NULL,'a2z', 5, 12),
(NULL,'a2z', 8, 12),
(NULL,'a2z', 1, 12),
(NULL,'a2z', 6, 12),
(NULL,'a2z', 11, 12),
(NULL,'a2z', 4, 12),
(NULL,'a2z', 2, 12),
(NULL,'a2z', 12, 17),
(NULL,'a2z', 2, 18),
(NULL,'a2z', 1, 18),
(NULL,'a2z', 6, 18),
(NULL,'a2z', 4, 18),
(NULL,'a2z', 5, 18),
(NULL,'a2z', 6, 30),
(NULL,'a2z', 7, 30),
(NULL,'a2z', 8, 30),
(NULL,'a2z', 9, 30),
(NULL,'a2z', 10, 30),
(NULL,'a2z', 11, 30),
(NULL,'a2z', 5, 30),
(NULL,'a2z', 4, 30),
(NULL,'a2z', 3, 30),
(NULL,'a2z', 2, 30),
(NULL,'a2z', 1, 30),
(NULL,'a2z', 12, 30),
(NULL,'a2z', 7, 31),
(NULL,'a2z', 3, 32),
(NULL,'a2z', 10, 33);



INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (1,1000,'a2z');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (2,1000,'a2z');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (4,1000,'a2z');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (5,1000,'a2z');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (6,1000,'a2z');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (8,1000,'a2z');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (11,1000,'a2z');


UPDATE `configuration_organisme` SET `type_contrat` = '1' WHERE `configuration_organisme`.`organisme` = 'a2z';


-- e3r --


INSERT IGNORE INTO `CompteMoniteur` VALUES
(NULL, 'e3r' , 'Atexo', 'mdp08eT@', 'suivi.mpe@atexo.com', '1'),
(NULL, 'e3r' , 'Atexo', '@texo', 'dev.pmi@atexo.biz', '0');

INSERT IGNORE INTO `Mesure_Type_Procedure` VALUES
( NULL, 'e3r', 'Appel d''offres ouvert'),
(NULL, 'e3r' , 'Appel d''offres restreint'),
(NULL, 'e3r' , 'Dialogue compétitif'),
(NULL, 'e3r' , 'Marché à  procédure adaptée'),
(NULL, 'e3r' , 'Marché négocié avec publicité préalable et mise en concurrence'),
(NULL, 'e3r' , 'Concours'),
(NULL, 'e3r' , 'Procédure spécifique à certains marchés de défense nationale'),
(NULL, 'e3r' , 'Accord cadre'),
(NULL, 'e3r' , 'Système d''acquisition dynamique'),
(NULL ,'e3r' , 'Marché subséquent à accord cadre'),
(NULL ,'e3r' , 'Marché spécifique sur système acquisition dynamique');

INSERT IGNORE INTO `ReferentielDestinationFormXml` VALUES
(1,'e3r', 'BOAMP seul'),
(2,'e3r', 'BOAMP et JOUE'),
(3,'e3r', 'Groupe Moniteur');
INSERT IGNORE INTO `ReferentielTypeXml` ( `id` , `organisme` , `id_destinataire` , `libelle_type` ) VALUES
(1,'e3r', '1#3', 'XML 1.9 BOAMP3 - Avis de marché National'),
(2,'e3r', '1#3', 'XML 1.9 BOAMP6 - Avis de MAPA'),
(3,'e3r', '2#3', 'XML 1.9 JOUE_02 - Avis de marché'),
(4,'e3r', '2#3', 'XML 1.9 JOUE_03 - Avis d''attribution'),
(5,'e3r', '1#3', 'XML 1.9 BOAMP4 - Avis d''attribution'),
(6,'e3r', '1#3', 'XML 1.9 BOAMP2 - Avis rectificatif'),
(7,'e3r', '1#3', 'XML 1.9 BOAMP7 - Résultat de MAPA'),
(8,'e3r', '2#3', 'XML 1.9 JOUE_12 - Avis Concours'),
(9,'e3r', '2#3', 'XML 1.9 JOUE_14 - Formulaire rectificatif / annulation'),
(10,'e3r', '2#3', 'XML 1.9 JOUE_05 - Secteurs Spéciaux'),
(11,'e3r', '2', 'XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire'),
(12,'e3r', '2', 'XML 1.9 JOUE_13 - Résultat Concours'),
(13,'e3r', '2', 'XML 1.9 JOUE_10 - Concession de travaux publics'),
(14,'e3r', '2', 'XML 1.9 JOUE_01T - Avis de préinformation - Travaux'),
(15,'e3r', '2', 'XML 1.9 JOUE_01FS - Avis de préinformation - Fournitures / Services'),
(16,'e3r', '1', 'XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification'),
(17,'e3r', '2#3', 'XML 1.9 JOUE_06 -  Avis d''attribution - Secteurs Spéciaux'),
(18,'e3r', '2', 'XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire');
INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`) VALUES
(1, 'e3r', 'Appel d''offres ouvert', 'AOO', 1, 1, 1, 0, 2, 0, '0', '1', 'Appel d''offres ouvert', 'Open procedure', '', '', '', '', '', 0, '01', '0', '0', '<AppelOuvert/>', '1', '0', '0', '0', '0', '0', '1', 'AOO', 'Procedura apperta', '', '1', 'OPEN'),
(2, 'e3r', 'Appel d''offres restreint - Candidature', 'AOR-C', 2, 2, 2, 0, 2, 0, '0', '1', 'Appel d''offres restreint - Candidature', 'Restricted procedure - preselection', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARC', 'Procedura ristretta - qualificazione', '', '10', 'RESTRICTED'),
(3, 'e3r', 'Appel d''offres restreint - Offre', 'AOR-O', 2, 2, 2, 0, 2, 0, '0', '1', 'Appel d''offres restreint - Offre', 'Restricted procedure - bid', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARO', 'Procedura ristretta - offerta', '', '2', 'RESTRICTED'),
(4, 'e3r', 'Procédure adaptée < 90 k EUR HT', 'PA-INF', 23, 3, 3, 0, 2, 0, '1', '1', 'Procédure adaptée < 90 k EUR HT', 'Procedure below thresholds < 90 k EUR', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAI', 'Procedura sotto soglia < 90 k EUR', '', '9', ''),
(5, 'e3r', 'Procédure adaptée >  90 k EUR HT', 'PA-SUP', 23, 3, 3, 0, 2, 0, '1', '1', 'Procédure adaptée >  90 k EUR HT', 'Procedure below thresholds >  90 k EUR', '', '', '', '', '', 2, '', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAS', 'Procedura sotto soglia >  90 k EUR', '', '9', ''),
(6, 'e3r', 'Procédure adaptée spécifique article 30 < 90 k EUR HT', 'PA-A30-INF', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSI', '', '', '9', ''),
(7, 'e3r', 'Procédure adaptée spécifique article 30 >  90 k EUR HT', 'PA-A30-SUP', 23, 3, 0, 0, 2, 0, '1', '1', '', '', '', '', '', '', '', 2, '', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSS', '', '', '9', ''),
(8, 'e3r', 'Concours ouvert', 'CO', 7, 4, 4, 0, 2, 0, '0', '1', 'Concours ouvert', 'Open design contest', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursOuvert/>', '0', '0', '0', '1', '0', '0', '6', 'COO', 'Concorso apperto', '', '7', 'OPEN'),
(9, 'e3r', 'Concours restreint - Candidature', 'CR-C', 8, 5, 4, 0, 2, 0, '0', '1', 'Concours restreint - Candidature', 'Restricted design contest - preselection', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRC', 'Concorso ristretto - qualificazione', '', '10', 'RESTRICTED'),
(10, 'e3r', 'Concours restreint - Offre', 'CR-O', 8, 5, 4, 0, 2, 0, '0', '1', 'Concours restreint - Offre', 'Restricted design contest - bid', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRO', 'Concorso ristretto - offerta', '', '7', 'RESTRICTED'),
(11, 'e3r', 'Dialogue compétitif - Candidature', 'DC-C', 22, 7, 4, 0, 2, 0, '0', '1', 'Dialogue compétitif - Candidature', 'Competitive dialogue - preselection', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCC', 'Dialogo competitivo - qualificazione', '', '10', 'COMPETITIVE_DIALOGUE'),
(12, 'e3r', 'Dialogue compétitif - Offre', 'DC-O', 22, 7, 4, 0, 2, 0, '0', '1', 'Dialogue compétitif - Offre', 'Competitive dialogue - bid', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCO', 'Dialogo competitivo - offerta', '', '6', 'COMPETITIVE_DIALOGUE'),
(17, 'e3r', 'Procédure Autre', 'AUT', 24, 15, 4, 0, 2, 0, '0', '1', 'Procédure Autre', 'Other procedure', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '1', '0', '0', '', 'ATR', 'Altra procedura', '', '10', ''),
(18, 'e3r', 'Marché négocié', 'MN', 18, 8, 4, 0, 2, 0, '0', '1', 'Marché négocié', 'Negociated procedure', '', '', '', '', '', 0, '10', '0', '0', '<MarcheNegocie/>', '0', '1', '0', '0', '0', '0', '5', 'MNE', 'Procedura negoziata', '', '4', 'NEGOTIATED'),
(30, 'e3r', 'Accord-Cadre - 1 Sélection des Attributaires', 'AC-SA', 1, 30, 1, 0, 2, 0, '0', '1', 'Accord-Cadre - 1 Sélection des Attributaires', 'Framework agreement - 1 Selection of suppliers', '', '', '', '', '', 0, '10', '0', '1', '<Autres/>', '0', '0', '0', '0', '0', '1', '8', 'ACA', 'Accordo quadro - 1 Qualificazione dei titolare', '', '10', ''),
(31, 'e3r', 'Accord-Cadre - 2 Marché Subséquent', 'AC-MS', 2, 31, 2, 0, 2, 0, '0', '1', 'Accord-Cadre - 2 Marché Subséquent', 'Framework agreement - 2 Individual contract', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '0', '0', '1', '10', 'ACS', 'Accordo quadro - 2 Apalto', '', '10', ''),
(32, 'e3r', 'Système d''Acquisition Dynamique - Admission', 'SAD-A', 8, 32, 4, 0, 2, 0, '0', '1', 'Système d''Acquisition Dynamique - Admission', 'Dynamic purchasing system - Admission', '', '', '', '', '', 0, '08', '1', '1', '<Autres/>', '0', '0', '0', '0', '1', '0', '9', 'SAA', 'Sistema dinamico di acquisizione - Ammissione', '', '10', ''),
(33, 'e3r', 'Système d''Acquisition Dynamique - Marché Spécifique', 'SAD-MS', 8, 33, 4, 0, 2, 0, '0', '1', 'Système d''Acquisition Dynamique - Marché Spécifique','Dynamicpurchasing system - Specific contract', '', '', '', '', '', 0, '08', '0', '0', '<Autres/>', '0', '0', '0', '0', '1', '0', '11', 'SAM', 'Sistema dinamico di acquisizione - Appalto', '', '8', '');

INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`,`libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`,`id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
(1000, 'e3r', 'Procédure avec négociation - Candidature', 'PAN-C', 1, 1000, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', NULL, '0', NULL, '0', '0', '', '1', '0', '0', '0', '0', '1', '', 'PCN', '', '', '1', '', '');

INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`,`libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`,`id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
(1004, 'e3r', 'Procédure avec négociation - Offre', 'PAN-O', 1, 1000, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', NULL, '0', NULL, '0', '0', '', '1', '0', '0', '0', '0', '1', '', 'PCN', '', '', '1', '', '');



-- INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`,`delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
-- (1001, 'e3r', 'Procédure adaptée services sociaux et services spécifiques < 90 k EUR HT', 'PA-SS-INF', '23', '3', '3', '0', '2', '0', '1', '1', '', '', '', '', '', '', '', '1', '9', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAI', '', '', '9', '', ''),
-- (1002, 'e3r', 'Procédure adaptée services sociaux et services spécifiques > 90 k EUR HT', 'PA-SS-SUP', '23', '3', '3', '0', '2', '0', '1', '1', '', '', '', '', '', '', '', '1', '9', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAS', '', '', '9', '', '');

INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`) VALUES
(1, 'e3r', '+1', '+0', '1', '0', '1', '0', '+1', '+1', '+0', '-1', '-1', '1', '0', '0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '+0', '', '', '+0', '+0', '+0', '+0', '+1', '1', '+1'),
(2, 'e3r', '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(3, 'e3r', '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(4, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(5, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '+1', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '+1', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(6, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(7, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(8, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '1', '-1', '-1', '1', '+0', '+0', '+0', '+0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '+1', '', '', '+0', '+0', '+0', '-0', '+1', '0', '-1'),
(9, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '1', '0', '0', '-1', '-1', '1', '+1', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(10, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '-0', '1', '1', '-1', '-1', '0', '+0', '+1', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(11, 'e3r', '1', '0', '+1', '+0', '+1', '+0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(12, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '-0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(17, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+0', '+0', '+1', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '-1', '-1', '-1', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(18, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '+0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '0', '-1'),
(30, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '-0', '1', '+0', '+0', '1', '0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(31, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '-0', '+0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(32, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(33, 'e3r', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-0', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+0', '+1', '+1','+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '+0', '+1', '1', '-1');

INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES ('1000', 'e3r', '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '+1', '', 'DAY', '+1', '+0', '-0');
INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES ('1004', 'e3r', '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '+1', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '+1', '', 'DAY', '+1', '+0', '-0');

-- INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`,`env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES
-- ('1001', 'e3r', '+1', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '-0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '-1', '1', 'DAY', '+0', '+1', '+1'),
-- ('1002', 'e3r', '+1', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '-0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '-1', '1', 'DAY', '+0', '+1', '+1');

INSERT IGNORE INTO `ValeurReferentielOrg` (`id`, `organisme`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_valeur_referentiel_it`) VALUES
(1, 'e3r', 1, 'Etat', 'Etat', 'State', '', '', '', '', '', 'Stato'),
(2, 'e3r', 1, 'Région', 'Région', 'Region', '', '', '', '', '', 'Regione'),
(3, 'e3r', 1, 'Département', 'Département', 'County', '', '', '', '', '', 'Reparto'),
(4, 'e3r', 1, 'Commune', 'Commune', 'City', '', '', '', '', '', 'Città'),
(5, 'e3r', 1, 'Etablissement public national', 'Etablissement public national', 'National publi authority', '', '', '', '', '', 'Ente pubblico nazionale'),
(6, 'e3r', 1, 'Etablissement public territorial', 'Etablissement public territorial', 'Local public authority', '', '', '', '', '', 'Ente pubblico locale'),
(7, 'e3r', 1, 'Autre', 'Autre', 'Other', '', '', '', '', '', 'Altro'),
(9, 'e3r', 3, 'Non renseigné', 'Non renseigné', 'Not filled-in', '', '', '', '', '', 'Sconosciuto'),
(10, 'e3r', 3, 'Oui', 'Oui', 'Yes', '', '', '', '', '', 'Sì'),
(11, 'e3r', 3, 'Non', 'Non', 'No', '', '', '', '', '', 'Non'),
(12, 'e3r', 2, '4 000 - 19 999.99', '4 000 HT à 19 999,99 HT', '4 000 - 19 999.99', '', '', '', '', '', '4 000 - 19 999.99'),
(13, 'e3r', 2, '20 000 - 49 999.99', '20 000 HT à 49 999,99 HT', '20 000 - 49 999.99', '', '', '', '', '', '20 000 - 49 999.99'),
(14, 'e3r', 2, '50 000 - 89 999.99', '50 000 HT à 89 999,99 HT', '50 000 - 89 999.99', '', '', '', '', '', '50 000 - 89 999.99'),
(15, 'e3r', 2, '90 000 - 124 999.99', '90 000 HT à 124 999,99 HT', '90 000 - 124 999.99', '', '', '', '', '', '90 000 - 124 999.99'),
(16, 'e3r', 2, '125 000 - 192 999.99', '125 000 HT à 192 999,99 HT', '125 000 - 192 999.99', '', '', '', '', '', '125 000 - 192 999.99'),
(17, 'e3r', 2, '193 000 - 999 999.99', '193 000 HT à 999 999,99 HT', '193 000 - 999 999.99', '', '', '', '', '', '193 000 - 999 999.99'),
(18, 'e3r', 2, '1 000 000 - 2 999 999.99', '1 000 000 HT à 2 999 999,99 HT', '1 000 000 - 2 999 999.99', '', '', '', '', '', '1 000 000 - 2 999 999.99'),
(19, 'e3r', 2, '3 000 000 - 4 844 999.99', '3 000 000 HT à 4 844 999,99 HT', '3 000 000 - 4 844 999.99', '', '', '', '', '', '3 000 000 - 4 844 999.99'),
(20, 'e3r', 2, '4 845 000 - ...', '4 845 000 HT et plus.', '4 845 000 - ...', '', '', '', '', '', '4 845 000 - ...');

INSERT IGNORE INTO `ReferentielOrg` VALUES
(1,'e3r', 'Type organismes'),
(2,'e3r', 'Tranche budgétaire'),
(3,'e3r', 'pme pmi');

INSERT IGNORE INTO `Centrale_publication` ( `id` , `organisme` , `nom` , `mail` ) VALUES (NULL , 'e3r', 'MEDIALEX', 'technique@atexo.com');
INSERT IGNORE INTO `Journaux` ( `ID_JOURNAL` , `ORGANISME` , `ID_CENTRALE` , `NOM_JOURNAL` ) VALUES
(1 , 'e3r' , '1', 'Ouest France'),
(2 , 'e3r' , '1', 'Le maine'),
(3 , 'e3r' , '1', 'Presse Océan'),
(4 , 'e3r' , '1', 'La Presse de la Manche');


INSERT IGNORE INTO `ReferentielOrg` (`id_referentiel`, `organisme`, `libelle_referentiel`) VALUES
(15, 'e3r', 'Type contrat');

INSERT IGNORE INTO `ValeurReferentielOrg` (`id` ,`organisme`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_valeur_referentiel_it`) VALUES
(25, 'e3r', 15, 'Marché public', 'Marché public', '(en) Marché public', '', '', '', '', '', ''),
(26, 'e3r', 15, 'Accord cadre', 'Accord cadre', '(en) Accord cadre', '', '', '', '', '', ''),
(27, 'e3r', 15, 'Système d''Acquisition Dynamique', 'Système d''Acquisition Dynamique', '(en) Système d''Acquisition Dynamique', '', '', '', '', '', ''),
(28, 'e3r', 15, 'Concession / DSP', 'Concession / DSP', '(en) Concession / DSP', '', '', '', '', '', ''),
-- (29, 'e3r', 15, 'Accord-cadre à bon de commande', 'Accord-cadre à bon de commande', '(en) Accord-cadre à bon de commande', '', '', '', '', '', ''),
-- (30, 'e3r', 15, 'Accord-cadre à marché mixte', 'Accord-cadre à marché mixte', '(en) Accord-cadre à marché mixte', '', '', '', '', '', ''),
(31, 'e3r', 15, 'Marché subséquent à accord-cadre', 'Marché subséquent à accord-cadre', '(en) Marché subséquent à accord-cadre', '', '', '', '', '', ''),
(32, 'e3r', 15, 'Marché de partenariat', 'Marché de partenariat', '(en) Marché de partenariat', '', '', '', '', '', ''),
(33, 'e3r', 15, 'Partenariat d''innovation', 'Partenariat d''innovation', '(en) Partenariat d''innovation', '', '', '', '', '', ''),
(34, 'e3r', 15, 'Marché spécifique (suite SAD)', 'Marché spécifique (suite SAD)', '(en) Marché spécifique (suite SAD)', '', '', '', '', '', ''),
(35, 'e3r', 15, 'Marché public de conception réalisation', 'Marché public de conception réalisation', '(en) Marché public de conception réalisation', '', '', '', '', '', ''),
(36, 'e3r', 15, 'Autre', 'Autre', '(en) Autre', '', '', '', '', '', '');

--
-- Contenu de la table `TypeCommission`
--

INSERT IGNORE INTO `TypeCommission` (`id`, `organisme`, `libelle`) VALUES
(1, 'e3r', 'Commission d''ouverture des plis'),
(2, 'e3r', 'Commission d''appel d''offres'),
(3, 'e3r', 'Commission - Autre'),
(4, 'e3r', 'Jury - Phase 1'),
(5, 'e3r', 'Jury - Phase 2'),
(6, 'e3r', 'Jury - Autre');



INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 2, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 2, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 2, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 2, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 2, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 2, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 2, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 2, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,2, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,2, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,2, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,2, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,2, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,2, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,2, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 2), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 3, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 3, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 3, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 3, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 3, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 3, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 3, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 3, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 3, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 3, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 3, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 3, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 3, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 3, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 3, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 3), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 4, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 4, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 4, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 4, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 4, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 4, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 4, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 4, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 4, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 4, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 4, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 4, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 4, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 4, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 4, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 4), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 5, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 5, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 5, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 5, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 5, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 5, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 5, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 5, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 5, 'e3r',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 5, 'e3r',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 5, 'e3r',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 5, 'e3r',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 5, 'e3r',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 5, 'e3r',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 5, 'e3r',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 5), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 6, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 6, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 6, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 6, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 6, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 6, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 6, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 6, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 6, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 6, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 6, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 6, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 6, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 6, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 6, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 7, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 7, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 7, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 7, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 7, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 7, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 7, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 7, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 7, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 7, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 7, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 7, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 7, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 7, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 7, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 7), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 8, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 8, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 8, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 8, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 8, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 8, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 8, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 8, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 8, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 8, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 8, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 6), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 8, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 8, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 8, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 8, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 8), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (181, 9, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (182, 9, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (183, 9, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (184, 9, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (185, 9, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (186, 9, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (187, 9, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (188, 9, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (181,9, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (182,9, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (183,9, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (184,9, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (185,9, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (186,9, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (187,9, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 9), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 10, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 10, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 10, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 10, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 10, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 10, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 10, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 10, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 10, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 10, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 10, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 10, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 10, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 10, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 10, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 10), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 11, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 11, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 11, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 11, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 11, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 11, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 11, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 11, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 11, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 11, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 11, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 11, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 11, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 11, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 11, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 11), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 12, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 12, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 12, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 12, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 12, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 12, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 12, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 12, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,12, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,12, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,12, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,12, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,12, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,12, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,12, 'e3r' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 12), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 17, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 17, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 17, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 17, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 17, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 17, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 17, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 17, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,17, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,17, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,17, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,17, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,17, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,17, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,17, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 17), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 18, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 18, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 18, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 18, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 18, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 18, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 18, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 18, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,18, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,18, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,18, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,18, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,18, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,18, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,18, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 18), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 30, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 30, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 30, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 30, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 30, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 30, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 30, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 30, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,30, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,30, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,30, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,30, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,30, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,30, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,30, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 30), 0, -7);


INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 31, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 31, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 31, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 31, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 31, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 31, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 31, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 31, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,31, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,31, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,31, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,31, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,31, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,31, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,31, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 31), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 32, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 32, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 32, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 32, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 32, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 32, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 32, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 32, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,32, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,32, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,32, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,32, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,32, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,32, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,32, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 32), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 33, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 33, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 33, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 33, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 33, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 33, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 33, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 33, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,33, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,33, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,33, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,33, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,33, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,33, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,33, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 33), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1000, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1000, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1000, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1000, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1000, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1000, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1000, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1000, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1000, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1000, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1000, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1000, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1000, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1000, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME ='e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1000, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1004, 'e3r' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1004, 'e3r' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1004, 'e3r' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1004, 'e3r' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1004, 'e3r' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1004, 'e3r' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1004, 'e3r' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1004, 'e3r' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1004, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1004, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1004, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1004, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1004, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1004, 'e3r' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME ='e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1004, 'e3r' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'e3r' AND ID_TYPE_PROCEDURE = 1), 0, -7);


INSERT IGNORE INTO `Tranche_Article_133` ( `acronyme_org` , `millesime` , `Libelle_tranche_budgetaire` , `borne_inf`, `borne_sup` ) VALUES
('e3r', '2010', '0 à 3 999,99 HT', '0', '3 999,99'),
('e3r', '2010', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('e3r', '2010', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('e3r', '2010', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('e3r', '2010', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('e3r', '2010', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('e3r', '2010', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('e3r', '2010', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('e3r', '2010', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('e3r', '2010', '4 845 000 HT et plus.', '4845000', ''),
('e3r', '2011', '0 à 3 999,99 HT', '0', '3 999,99'),
('e3r', '2011', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('e3r', '2011', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('e3r', '2011', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('e3r', '2011', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('e3r', '2011', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('e3r', '2011', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('e3r', '2011', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('e3r', '2011', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('e3r', '2011', '4 845 000 HT et plus.', '4845000', ''),
('e3r', '2012', '0 à 3 999,99 HT', '0', '3 999,99'),
('e3r', '2012', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('e3r', '2012', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('e3r', '2012', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('e3r', '2012', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('e3r', '2012', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('e3r', '2012', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('e3r', '2012', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('e3r', '2012', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('e3r', '2012', '4 845 000 HT et plus.', '4845000', ''),
('e3r', '2013', '0 à 3 999,99 HT', '0', '3 999,99'),
('e3r', '2013', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('e3r', '2013', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('e3r', '2013', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('e3r', '2013', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('e3r', '2013', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('e3r', '2013', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('e3r', '2013', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('e3r', '2013', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('e3r', '2013', '4 845 000 HT et plus.', '4845000', ''),
('e3r', '2014', '0 à 3 999,99 HT', '0', '3 999,99'),
('e3r', '2014', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('e3r', '2014', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('e3r', '2014', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('e3r', '2014', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('e3r', '2014', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('e3r', '2014', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('e3r', '2014', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('e3r', '2014', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('e3r', '2014', '4 845 000 HT et plus.', '4845000', ''),
('e3r', '2015', '0 à 3 999,99 HT', '0', '3 999,99'),
('e3r', '2015', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('e3r', '2015', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('e3r', '2015', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('e3r', '2015', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('e3r', '2015', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('e3r', '2015', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('e3r', '2015', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('e3r', '2015', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('e3r', '2015', '4 845 000 HT et plus.', '4845000', '');

INSERT IGNORE INTO `t_type_contrat_et_procedure` (`id_type_contrat_et_procedure`,`organisme`, `id_type_contrat`, `id_type_procedure`) VALUES
(NULL,'e3r', 2, 1),
(NULL,'e3r', 4, 1),
(NULL,'e3r', 5, 1),
(NULL,'e3r', 6, 1),
(NULL,'e3r', 1, 1),
(NULL,'e3r', 5, 2),
(NULL,'e3r', 4, 2),
(NULL,'e3r', 11, 2),
(NULL,'e3r', 8, 2),
(NULL,'e3r', 6, 2),
(NULL,'e3r', 2, 2),
(NULL,'e3r', 1, 2),
(NULL,'e3r', 8, 3),
(NULL,'e3r', 4, 3),
(NULL,'e3r', 1, 3),
(NULL,'e3r', 6, 3),
(NULL,'e3r', 2, 3),
(NULL,'e3r', 5, 3),
(NULL,'e3r', 11, 3),
(NULL,'e3r', 6, 4),
(NULL,'e3r', 11, 4),
(NULL,'e3r', 1, 4),
(NULL,'e3r', 5, 4),
(NULL,'e3r', 9, 4),
(NULL,'e3r', 4, 4),
(NULL,'e3r', 2, 4),
(NULL,'e3r', 4, 5),
(NULL,'e3r', 1, 5),
(NULL,'e3r', 5, 5),
(NULL,'e3r', 6, 5),
(NULL,'e3r', 11, 5),
(NULL,'e3r', 9, 5),
(NULL,'e3r', 2, 5),
(NULL,'e3r', 6, 6),
(NULL,'e3r', 1, 6),
(NULL,'e3r', 5, 6),
(NULL,'e3r', 2, 6),
(NULL,'e3r', 5, 7),
(NULL,'e3r', 1, 7),
(NULL,'e3r', 2, 7),
(NULL,'e3r', 6, 7),
(NULL,'e3r', 1, 8),
(NULL,'e3r', 6, 8),
(NULL,'e3r', 1, 9),
(NULL,'e3r', 6, 9),
(NULL,'e3r', 1, 10),
(NULL,'e3r', 6, 10),
(NULL,'e3r', 11, 11),
(NULL,'e3r', 6, 11),
(NULL,'e3r', 8, 11),
(NULL,'e3r', 4, 11),
(NULL,'e3r', 2, 11),
(NULL,'e3r', 5, 11),
(NULL,'e3r', 1, 11),
(NULL,'e3r', 5, 12),
(NULL,'e3r', 8, 12),
(NULL,'e3r', 1, 12),
(NULL,'e3r', 6, 12),
(NULL,'e3r', 11, 12),
(NULL,'e3r', 4, 12),
(NULL,'e3r', 2, 12),
(NULL,'e3r', 12, 17),
(NULL,'e3r', 2, 18),
(NULL,'e3r', 1, 18),
(NULL,'e3r', 6, 18),
(NULL,'e3r', 4, 18),
(NULL,'e3r', 5, 18),
(NULL,'e3r', 6, 30),
(NULL,'e3r', 7, 30),
(NULL,'e3r', 8, 30),
(NULL,'e3r', 9, 30),
(NULL,'e3r', 10, 30),
(NULL,'e3r', 11, 30),
(NULL,'e3r', 5, 30),
(NULL,'e3r', 4, 30),
(NULL,'e3r', 3, 30),
(NULL,'e3r', 2, 30),
(NULL,'e3r', 1, 30),
(NULL,'e3r', 12, 30),
(NULL,'e3r', 7, 31),
(NULL,'e3r', 3, 32),
(NULL,'e3r', 10, 33);



INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (1,1000,'e3r');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (2,1000,'e3r');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (4,1000,'e3r');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (5,1000,'e3r');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (6,1000,'e3r');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (8,1000,'e3r');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (11,1000,'e3r');


UPDATE `configuration_organisme` SET `type_contrat` = '1' WHERE `configuration_organisme`.`organisme` = 'e3r';


-- a1t --

INSERT IGNORE INTO `CompteMoniteur` VALUES
(NULL, 'a1t' , 'Atexo', 'mdp08eT@', 'suivi.mpe@atexo.com', '1'),
(NULL, 'a1t' , 'Atexo', '@texo', 'dev.pmi@atexo.biz', '0');

INSERT IGNORE INTO `Mesure_Type_Procedure` VALUES
( NULL, 'a1t', 'Appel d''offres ouvert'),
(NULL, 'a1t' , 'Appel d''offres restreint'),
(NULL, 'a1t' , 'Dialogue compétitif'),
(NULL, 'a1t' , 'Marché à  procédure adaptée'),
(NULL, 'a1t' , 'Marché négocié avec publicité préalable et mise en concurrence'),
(NULL, 'a1t' , 'Concours'),
(NULL, 'a1t' , 'Procédure spécifique à certains marchés de défense nationale'),
(NULL, 'a1t' , 'Accord cadre'),
(NULL, 'a1t' , 'Système d''acquisition dynamique'),
(NULL ,'a1t' , 'Marché subséquent à accord cadre'),
(NULL ,'a1t' , 'Marché spécifique sur système acquisition dynamique');

INSERT IGNORE INTO `ReferentielDestinationFormXml` VALUES
(1,'a1t', 'BOAMP seul'),
(2,'a1t', 'BOAMP et JOUE'),
(3,'a1t', 'Groupe Moniteur');
INSERT IGNORE INTO `ReferentielTypeXml` ( `id` , `organisme` , `id_destinataire` , `libelle_type` ) VALUES
(1,'a1t', '1#3', 'XML 1.9 BOAMP3 - Avis de marché National'),
(2,'a1t', '1#3', 'XML 1.9 BOAMP6 - Avis de MAPA'),
(3,'a1t', '2#3', 'XML 1.9 JOUE_02 - Avis de marché'),
(4,'a1t', '2#3', 'XML 1.9 JOUE_03 - Avis d''attribution'),
(5,'a1t', '1#3', 'XML 1.9 BOAMP4 - Avis d''attribution'),
(6,'a1t', '1#3', 'XML 1.9 BOAMP2 - Avis rectificatif'),
(7,'a1t', '1#3', 'XML 1.9 BOAMP7 - Résultat de MAPA'),
(8,'a1t', '2#3', 'XML 1.9 JOUE_12 - Avis Concours'),
(9,'a1t', '2#3', 'XML 1.9 JOUE_14 - Formulaire rectificatif / annulation'),
(10,'a1t', '2#3', 'XML 1.9 JOUE_05 - Secteurs Spéciaux'),
(11,'a1t', '2', 'XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire'),
(12,'a1t', '2', 'XML 1.9 JOUE_13 - Résultat Concours'),
(13,'a1t', '2', 'XML 1.9 JOUE_10 - Concession de travaux publics'),
(14,'a1t', '2', 'XML 1.9 JOUE_01T - Avis de préinformation - Travaux'),
(15,'a1t', '2', 'XML 1.9 JOUE_01FS - Avis de préinformation - Fournitures / Services'),
(16,'a1t', '1', 'XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification'),
(17,'a1t', '2#3', 'XML 1.9 JOUE_06 -  Avis d''attribution - Secteurs Spéciaux'),
(18,'a1t', '2', 'XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire');
INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`) VALUES
(1, 'a1t', 'Appel d''offres ouvert', 'AOO', 1, 1, 1, 0, 2, 0, '0', '1', 'Appel d''offres ouvert', 'Open procedure', '', '', '', '', '', 0, '01', '0', '0', '<AppelOuvert/>', '1', '0', '0', '0', '0', '0', '1', 'AOO', 'Procedura apperta', '', '1', 'OPEN'),
(2, 'a1t', 'Appel d''offres restreint - Candidature', 'AOR-C', 2, 2, 2, 0, 2, 0, '0', '1', 'Appel d''offres restreint - Candidature', 'Restricted procedure - preselection', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARC', 'Procedura ristretta - qualificazione', '', '10', 'RESTRICTED'),
(3, 'a1t', 'Appel d''offres restreint - Offre', 'AOR-O', 2, 2, 2, 0, 2, 0, '0', '1', 'Appel d''offres restreint - Offre', 'Restricted procedure - bid', '', '', '', '', '', 0, '02', '0', '0', '<AppelRestreint/>', '1', '0', '0', '0', '0', '0', '2', 'ARO', 'Procedura ristretta - offerta', '', '2', 'RESTRICTED'),
(4, 'a1t', 'Procédure adaptée < 90 k EUR HT', 'PA-INF', 23, 3, 3, 0, 2, 0, '1', '1', 'Procédure adaptée < 90 k EUR HT', 'Procedure below thresholds < 90 k EUR', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAI', 'Procedura sotto soglia < 90 k EUR', '', '9', ''),
(5, 'a1t', 'Procédure adaptée >  90 k EUR HT', 'PA-SUP', 23, 3, 3, 0, 2, 0, '1', '1', 'Procédure adaptée >  90 k EUR HT', 'Procedure below thresholds >  90 k EUR', '', '', '', '', '', 2, '', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAS', 'Procedura sotto soglia >  90 k EUR', '', '9', ''),
(6, 'a1t', 'Procédure adaptée spécifique article 30 < 90 k EUR HT', 'PA-A30-INF', 23, 3, 3, 0, 2, 0, '1', '1', '', '', '', '', '', '', '', 1, '09', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSI', '', '', '9', ''),
(7, 'a1t', 'Procédure adaptée spécifique article 30 >  90 k EUR HT', 'PA-A30-SUP', 23, 3, 0, 0, 2, 0, '1', '1', '', '', '', '', '', '', '', 2, '', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'PSS', '', '', '9', ''),
(8, 'a1t', 'Concours ouvert', 'CO', 7, 4, 4, 0, 2, 0, '0', '1', 'Concours ouvert', 'Open design contest', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursOuvert/>', '0', '0', '0', '1', '0', '0', '6', 'COO', 'Concorso apperto', '', '7', 'OPEN'),
(9, 'a1t', 'Concours restreint - Candidature', 'CR-C', 8, 5, 4, 0, 2, 0, '0', '1', 'Concours restreint - Candidature', 'Restricted design contest - preselection', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRC', 'Concorso ristretto - qualificazione', '', '10', 'RESTRICTED'),
(10, 'a1t', 'Concours restreint - Offre', 'CR-O', 8, 5, 4, 0, 2, 0, '0', '1', 'Concours restreint - Offre', 'Restricted design contest - bid', '', '', '', '', '', 0, '07', '0', '0', '<ConcoursRestreint/>', '0', '0', '0', '1', '0', '0', '6', 'CRO', 'Concorso ristretto - offerta', '', '7', 'RESTRICTED'),
(11, 'a1t', 'Dialogue compétitif - Candidature', 'DC-C', 22, 7, 4, 0, 2, 0, '0', '1', 'Dialogue compétitif - Candidature', 'Competitive dialogue - preselection', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCC', 'Dialogo competitivo - qualificazione', '', '10', 'COMPETITIVE_DIALOGUE'),
(12, 'a1t', 'Dialogue compétitif - Offre', 'DC-O', 22, 7, 4, 0, 2, 0, '0', '1', 'Dialogue compétitif - Offre', 'Competitive dialogue - bid', '', '', '', '', '', 0, '06', '0', '0', '<DialogueCompetitif/>', '0', '0', '1', '0', '0', '0', '3', 'DCO', 'Dialogo competitivo - offerta', '', '6', 'COMPETITIVE_DIALOGUE'),
(17, 'a1t', 'Procédure Autre', 'AUT', 24, 15, 4, 0, 2, 0, '0', '1', 'Procédure Autre', 'Other procedure', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '1', '0', '0', '', 'ATR', 'Altra procedura', '', '10', ''),
(18, 'a1t', 'Marché négocié', 'MN', 18, 8, 4, 0, 2, 0, '0', '1', 'Marché négocié', 'Negociated procedure', '', '', '', '', '', 0, '10', '0', '0', '<MarcheNegocie/>', '0', '1', '0', '0', '0', '0', '5', 'MNE', 'Procedura negoziata', '', '4', 'NEGOTIATED'),
(30, 'a1t', 'Accord-Cadre - 1 Sélection des Attributaires', 'AC-SA', 1, 30, 1, 0, 2, 0, '0', '1', 'Accord-Cadre - 1 Sélection des Attributaires', 'Framework agreement - 1 Selection of suppliers', '', '', '', '', '', 0, '10', '0', '1', '<Autres/>', '0', '0', '0', '0', '0', '1', '8', 'ACA', 'Accordo quadro - 1 Qualificazione dei titolare', '', '10', ''),
(31, 'a1t', 'Accord-Cadre - 2 Marché Subséquent', 'AC-MS', 2, 31, 2, 0, 2, 0, '0', '1', 'Accord-Cadre - 2 Marché Subséquent', 'Framework agreement - 2 Individual contract', '', '', '', '', '', 0, '10', '0', '0', '<Autres/>', '0', '0', '0', '0', '0', '1', '10', 'ACS', 'Accordo quadro - 2 Apalto', '', '10', ''),
(32, 'a1t', 'Système d''Acquisition Dynamique - Admission', 'SAD-A', 8, 32, 4, 0, 2, 0, '0', '1', 'Système d''Acquisition Dynamique - Admission', 'Dynamic purchasing system - Admission', '', '', '', '', '', 0, '08', '1', '1', '<Autres/>', '0', '0', '0', '0', '1', '0', '9', 'SAA', 'Sistema dinamico di acquisizione - Ammissione', '', '10', ''),
(33, 'a1t', 'Système d''Acquisition Dynamique - Marché Spécifique', 'SAD-MS', 8, 33, 4, 0, 2, 0, '0', '1', 'Système d''Acquisition Dynamique - Marché Spécifique','Dynamicpurchasing system - Specific contract', '', '', '', '', '', 0, '08', '0', '0', '<Autres/>', '0', '0', '0', '0', '1', '0', '11', 'SAM', 'Sistema dinamico di acquisizione - Appalto', '', '8', '');

INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`,`libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`,`id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
(1000, 'a1t', 'Procédure avec négociation - Candidature', 'PAN-C', 1, 1000, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', NULL, '0', NULL, '0', '0', '', '1', '0', '0', '0', '0', '1', '', 'PCN', '', '', '1', '', '');

INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`,`libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`,`id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
(1004, 'a1t', 'Procédure avec négociation - Offre', 'PAN-O', 1, 1000, 4, 0, 2, 0, '0', '1', '', '', '', '', '', '', NULL, '0', NULL, '0', '0', '', '1', '0', '0', '0', '0', '1', '', 'PCN', '', '', '1', '', '');



-- INSERT IGNORE INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`,`delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`) VALUES
-- (1001, 'a1t', 'Procédure adaptée services sociaux et services spécifiques < 90 k EUR HT', 'PA-SS-INF', '23', '3', '3', '0', '2', '0', '1', '1', '', '', '', '', '', '', '', '1', '9', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAI', '', '', '9', '', ''),
-- (1002, 'a1t', 'Procédure adaptée services sociaux et services spécifiques > 90 k EUR HT', 'PA-SS-SUP', '23', '3', '3', '0', '2', '0', '1', '1', '', '', '', '', '', '', '', '1', '9', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '4', 'MAS', '', '', '9', '', '');

INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`) VALUES
(1, 'a1t', '+1', '+0', '1', '0', '1', '0', '+1', '+1', '+0', '-1', '-1', '1', '0', '0', '0', '0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '+0', '', '', '+0', '+0', '+0', '+0', '+1', '1', '+1'),
(2, 'a1t', '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(3, 'a1t', '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(4, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(5, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '+1', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '+1', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(6, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(7, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(8, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '1', '-1', '-1', '1', '+0', '+0', '+0', '+0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '+1', '', '', '+0', '+0', '+0', '-0', '+1', '0', '-1'),
(9, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '1', '0', '0', '-1', '-1', '1', '+1', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(10, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '-0', '1', '1', '-1', '-1', '0', '+0', '+1', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(11, 'a1t', '1', '0', '+1', '+0', '+1', '+0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '-0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(12, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '-0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(17, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '+0', '+0', '+1', '+0', '+0', '-0', '-0', '-0', '-0', '+1', '+0', '-1', '-1', '-1', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(18, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '-0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '0', '1', '', '1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '+0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '0', '-1'),
(30, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '-0', '1', '+0', '+0', '1', '0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(31, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+1', '+1', '-0', '-1', '-1', '1', '+0', '+0', '0', '0', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '0', '', '+0', '-0', '+0', '+0', '-0', '-0', '+0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '', '+1', '1', '-1'),
(32, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-1', '0', '0', '1', '+0', '+1', '-0', '-0', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '+0', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '-0', '+1', '1', '-1'),
(33, 'a1t', '+1', '+0', '+1', '+0', '+1', '+0', '+0', '+1', '-0', '-1', '-0', '+1', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '-0', '1', '-0', '+1', '+0', '+0', '+0', '+1', '+1','+1', '-0', '1', '', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '', '', '+0', '+0', '+0', '+0', '+1', '1', '-1');

INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES ('1000', 'a1t', '1', '0', '1', '0', '1', '0', '1', '0', '-0', '-1', '-1', '1', '1', '0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '-0', '0', '', '+0', '-0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '+1', '', 'DAY', '+1', '+0', '-0');
INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES ('1004', 'a1t', '1', '0', '1', '0', '1', '0', '0', '1', '-0', '-1', '-1', '0', '0', '1', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '+1', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '+0', '+1', '+1', '', '+0', '+0', '+0', '+0', '-0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '+1', '', 'DAY', '+1', '+0', '-0');

-- INSERT IGNORE INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`,`env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`) VALUES
-- ('1001', 'a1t', '+1', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '-0', '-0', '+1', '+0', '+1', '+0', '+0', '-0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '-1', '1', 'DAY', '+0', '+1', '+1'),
-- ('1002', 'a1t', '+1', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '-0', '-1', '-1', '+1', '+0', '+0', '+0', '+0', '+0', '+1', '+0', '+0', '+1', '+0', '+1', '+0', '+0', '-0', '+1', '+1', '+1', '+0', '1', '', '+1', '+1', '+0', '+0', '+0', '+1', '+1', '-0', '-0', '+0', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '+0', '+0', '+0', '-0', '-0', '1', '-1', '1', 'DAY', '+0', '+1', '+1');

INSERT IGNORE INTO `ValeurReferentielOrg` (`id`, `organisme`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_valeur_referentiel_it`) VALUES
(1, 'a1t', 1, 'Etat', 'Etat', 'State', '', '', '', '', '', 'Stato'),
(2, 'a1t', 1, 'Région', 'Région', 'Region', '', '', '', '', '', 'Regione'),
(3, 'a1t', 1, 'Département', 'Département', 'County', '', '', '', '', '', 'Reparto'),
(4, 'a1t', 1, 'Commune', 'Commune', 'City', '', '', '', '', '', 'Città'),
(5, 'a1t', 1, 'Etablissement public national', 'Etablissement public national', 'National publi authority', '', '', '', '', '', 'Ente pubblico nazionale'),
(6, 'a1t', 1, 'Etablissement public territorial', 'Etablissement public territorial', 'Local public authority', '', '', '', '', '', 'Ente pubblico locale'),
(7, 'a1t', 1, 'Autre', 'Autre', 'Other', '', '', '', '', '', 'Altro'),
(9, 'a1t', 3, 'Non renseigné', 'Non renseigné', 'Not filled-in', '', '', '', '', '', 'Sconosciuto'),
(10, 'a1t', 3, 'Oui', 'Oui', 'Yes', '', '', '', '', '', 'Sì'),
(11, 'a1t', 3, 'Non', 'Non', 'No', '', '', '', '', '', 'Non'),
(12, 'a1t', 2, '4 000 - 19 999.99', '4 000 HT à 19 999,99 HT', '4 000 - 19 999.99', '', '', '', '', '', '4 000 - 19 999.99'),
(13, 'a1t', 2, '20 000 - 49 999.99', '20 000 HT à 49 999,99 HT', '20 000 - 49 999.99', '', '', '', '', '', '20 000 - 49 999.99'),
(14, 'a1t', 2, '50 000 - 89 999.99', '50 000 HT à 89 999,99 HT', '50 000 - 89 999.99', '', '', '', '', '', '50 000 - 89 999.99'),
(15, 'a1t', 2, '90 000 - 124 999.99', '90 000 HT à 124 999,99 HT', '90 000 - 124 999.99', '', '', '', '', '', '90 000 - 124 999.99'),
(16, 'a1t', 2, '125 000 - 192 999.99', '125 000 HT à 192 999,99 HT', '125 000 - 192 999.99', '', '', '', '', '', '125 000 - 192 999.99'),
(17, 'a1t', 2, '193 000 - 999 999.99', '193 000 HT à 999 999,99 HT', '193 000 - 999 999.99', '', '', '', '', '', '193 000 - 999 999.99'),
(18, 'a1t', 2, '1 000 000 - 2 999 999.99', '1 000 000 HT à 2 999 999,99 HT', '1 000 000 - 2 999 999.99', '', '', '', '', '', '1 000 000 - 2 999 999.99'),
(19, 'a1t', 2, '3 000 000 - 4 844 999.99', '3 000 000 HT à 4 844 999,99 HT', '3 000 000 - 4 844 999.99', '', '', '', '', '', '3 000 000 - 4 844 999.99'),
(20, 'a1t', 2, '4 845 000 - ...', '4 845 000 HT et plus.', '4 845 000 - ...', '', '', '', '', '', '4 845 000 - ...');

INSERT IGNORE INTO `ReferentielOrg` VALUES
(1,'a1t', 'Type organismes'),
(2,'a1t', 'Tranche budgétaire'),
(3,'a1t', 'pme pmi');

INSERT IGNORE INTO `Centrale_publication` ( `id` , `organisme` , `nom` , `mail` ) VALUES (NULL , 'a1t', 'MEDIALEX', 'technique@atexo.com');
INSERT IGNORE INTO `Journaux` ( `ID_JOURNAL` , `ORGANISME` , `ID_CENTRALE` , `NOM_JOURNAL` ) VALUES
(1 , 'a1t' , '1', 'Ouest France'),
(2 , 'a1t' , '1', 'Le maine'),
(3 , 'a1t' , '1', 'Presse Océan'),
(4 , 'a1t' , '1', 'La Presse de la Manche');


INSERT IGNORE INTO `ReferentielOrg` (`id_referentiel`, `organisme`, `libelle_referentiel`) VALUES
(15, 'a1t', 'Type contrat');

INSERT IGNORE INTO `ValeurReferentielOrg` (`id` ,`organisme`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_valeur_referentiel_it`) VALUES
(25, 'a1t', 15, 'Marché public', 'Marché public', '(en) Marché public', '', '', '', '', '', ''),
(26, 'a1t', 15, 'Accord cadre', 'Accord cadre', '(en) Accord cadre', '', '', '', '', '', ''),
(27, 'a1t', 15, 'Système d''Acquisition Dynamique', 'Système d''Acquisition Dynamique', '(en) Système d''Acquisition Dynamique', '', '', '', '', '', ''),
(28, 'a1t', 15, 'Concession / DSP', 'Concession / DSP', '(en) Concession / DSP', '', '', '', '', '', ''),
-- (29, 'a1t', 15, 'Accord-cadre à bon de commande', 'Accord-cadre à bon de commande', '(en) Accord-cadre à bon de commande', '', '', '', '', '', ''),
-- (30, 'a1t', 15, 'Accord-cadre à marché mixte', 'Accord-cadre à marché mixte', '(en) Accord-cadre à marché mixte', '', '', '', '', '', ''),
(31, 'a1t', 15, 'Marché subséquent à accord-cadre', 'Marché subséquent à accord-cadre', '(en) Marché subséquent à accord-cadre', '', '', '', '', '', ''),
(32, 'a1t', 15, 'Marché de partenariat', 'Marché de partenariat', '(en) Marché de partenariat', '', '', '', '', '', ''),
(33, 'a1t', 15, 'Partenariat d''innovation', 'Partenariat d''innovation', '(en) Partenariat d''innovation', '', '', '', '', '', ''),
(34, 'a1t', 15, 'Marché spécifique (suite SAD)', 'Marché spécifique (suite SAD)', '(en) Marché spécifique (suite SAD)', '', '', '', '', '', ''),
(35, 'a1t', 15, 'Marché public de conception réalisation', 'Marché public de conception réalisation', '(en) Marché public de conception réalisation', '', '', '', '', '', ''),
(36, 'a1t', 15, 'Autre', 'Autre', '(en) Autre', '', '', '', '', '', '');

--
-- Contenu de la table `TypeCommission`
--

INSERT IGNORE INTO `TypeCommission` (`id`, `organisme`, `libelle`) VALUES
(1, 'a1t', 'Commission d''ouverture des plis'),
(2, 'a1t', 'Commission d''appel d''offres'),
(3, 'a1t', 'Commission - Autre'),
(4, 'a1t', 'Jury - Phase 1'),
(5, 'a1t', 'Jury - Phase 2'),
(6, 'a1t', 'Jury - Autre');



INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 2, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 2, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 2, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 2, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 2, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 2, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 2, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 2, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,2, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,2, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,2, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,2, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,2, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,2, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,2, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 2), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 3, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 3, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 3, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 3, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 3, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 3, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 3, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 3, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 3, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 3, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 3, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 3, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 3, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 3, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 3, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 3), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 4, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 4, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 4, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 4, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 4, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 4, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 4, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 4, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 4, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 4, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 4, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 4, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 4, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 4, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 4, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 4), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 5, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 5, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 5, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 5, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 5, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 5, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 5, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 5, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 5, 'a1t',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 5, 'a1t',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 5, 'a1t',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 5, 'a1t',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 5, 'a1t',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 5, 'a1t',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 5, 'a1t',(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 5), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 6, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 6, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 6, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 6, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 6, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 6, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 6, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 6, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 6, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 6, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 6, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 6, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 6, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 6, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 6, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 7, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 7, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 7, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 7, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 7, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 7, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 7, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 7, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 7, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 7, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 7, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 7, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 7, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 7, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 7, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 7), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 8, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 8, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 8, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 8, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 8, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 8, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 8, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 8, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 8, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 8, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 8, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 6), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 8, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 8, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 8, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 8, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 8), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (181, 9, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (182, 9, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (183, 9, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (184, 9, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (185, 9, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (186, 9, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (187, 9, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (188, 9, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (181,9, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (182,9, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (183,9, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (184,9, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (185,9, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (186,9, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (187,9, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 9), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 10, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 10, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 10, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 10, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 10, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 10, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 10, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 10, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 10, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 10, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 10, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 10, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 10, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 10, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 10, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 10), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 11, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 11, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 11, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 11, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 11, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 11, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 11, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 11, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 11, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 11, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 11, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 11, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 11, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 11, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 11, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 11), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 12, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 12, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 12, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 12, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 12, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 12, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 12, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 12, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,12, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,12, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,12, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,12, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,12, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,12, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,12, 'a1t' ,  (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 12), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 17, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 17, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 17, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 17, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 17, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 17, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 17, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 17, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,17, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,17, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,17, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,17, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,17, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,17, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,17, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 17), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 18, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 18, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 18, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 18, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 18, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 18, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 18, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 18, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,18, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,18, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,18, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,18, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,18, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,18, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,18, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 18), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 30, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 30, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 30, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 30, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 30, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 30, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 30, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 30, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,30, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,30, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,30, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,30, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,30, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,30, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,30, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 30), 0, -7);


INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 31, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 31, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 31, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 31, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 31, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 31, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 31, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 31, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,31, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,31, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,31, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,31, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,31, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,31, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,31, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 31), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 32, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 32, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 32, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 32, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 32, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 32, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 32, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 32, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,32, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,32, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,32, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,32, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,32, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,32, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,32, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 32), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 33, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 33, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 33, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 33, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 33, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 33, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 33, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 33, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101,33, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102,33, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103,33, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104,33, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105,33, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106,33, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE, ORGANISME, ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107,33, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 33), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1000, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1000, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1000, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1000, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1000, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1000, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1000, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1000, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1000, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1000, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1000, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1000, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1000, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1000, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME ='a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1000, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, -7);

INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (101, 1004, 'a1t' , 'E1', 'Date de lancement', 0);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (102, 1004, 'a1t' , 'E2', 'Date limite de remise des plis', 1);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (103, 1004, 'a1t' , 'E3', 'Ouverture des plis', 2);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (104, 1004, 'a1t' , 'E4', 'Analyse des réponses', 3);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (105, 1004, 'a1t' , 'E5', 'Décision / Attribution', 4);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (106, 1004, 'a1t' , 'E6', 'Information des soumissionnaires non retenus', 5);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (107, 1004, 'a1t' , 'E7', 'Notification de la décision', 6);
INSERT IGNORE INTO t_calendrier_etape_referentiel(ID_ETAPE_CALENDRIER_REFERENTIEL, ID_TYPE_PROCEDURE, ORGANISME, CODE, LIBELLE, POSITION) VALUES (108, 1004, 'a1t' , 'E8', 'Avis d''attribution', 7);

INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (101, 1004, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E1' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), -40, -5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (102, 1004, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E2' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 60, 15);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (103, 1004, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E3' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, 5);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, VALEUR_FIXE, VALEUR_VARIABLE) VALUES (104, 1004, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E4' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 60, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (105, 1004, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E5' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, 12);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (106, 1004, 'a1t' , (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E6' AND ORGANISME ='a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, 0);
INSERT IGNORE INTO t_calendrier_transition_referentiel(ID_TRANSITION_CALENDRIER_REFERENTIEL,ID_TYPE_PROCEDURE ,ORGANISME,ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE,VALEUR_FIXE, VALEUR_VARIABLE) VALUES (107, 1004, 'a1t' ,(SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E7' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), (SELECT ID_ETAPE_CALENDRIER_REFERENTIEL FROM t_calendrier_etape_referentiel WHERE code='E8' AND ORGANISME = 'a1t' AND ID_TYPE_PROCEDURE = 1), 0, -7);


INSERT IGNORE INTO `Tranche_Article_133` ( `acronyme_org` , `millesime` , `Libelle_tranche_budgetaire` , `borne_inf`, `borne_sup` ) VALUES
('a1t', '2010', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1t', '2010', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1t', '2010', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1t', '2010', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1t', '2010', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1t', '2010', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1t', '2010', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1t', '2010', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1t', '2010', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1t', '2010', '4 845 000 HT et plus.', '4845000', ''),
('a1t', '2011', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1t', '2011', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1t', '2011', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1t', '2011', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1t', '2011', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1t', '2011', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1t', '2011', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1t', '2011', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1t', '2011', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1t', '2011', '4 845 000 HT et plus.', '4845000', ''),
('a1t', '2012', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1t', '2012', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1t', '2012', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1t', '2012', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1t', '2012', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1t', '2012', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1t', '2012', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1t', '2012', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1t', '2012', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1t', '2012', '4 845 000 HT et plus.', '4845000', ''),
('a1t', '2013', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1t', '2013', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1t', '2013', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1t', '2013', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1t', '2013', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1t', '2013', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1t', '2013', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1t', '2013', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1t', '2013', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1t', '2013', '4 845 000 HT et plus.', '4845000', ''),
('a1t', '2014', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1t', '2014', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1t', '2014', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1t', '2014', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1t', '2014', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1t', '2014', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1t', '2014', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1t', '2014', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1t', '2014', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1t', '2014', '4 845 000 HT et plus.', '4845000', ''),
('a1t', '2015', '0 à 3 999,99 HT', '0', '3 999,99'),
('a1t', '2015', '4 000 HT à 19 999,99 HT', '4000', ' 19 999,99 '),
('a1t', '2015', '20 000 HT à 49 999,99 HT', '20000', ' 49 999,99 '),
('a1t', '2015', '50 000 HT à 89 999,99 HT', '50000', ' 89 999,99 '),
('a1t', '2015', '90 000 HT à 124 999,99 HT', '90000', ' 124 999,99'),
('a1t', '2015', '125 000 HT à 192 999,99 HT', '125000', ' 192 999,99'),
('a1t', '2015', '193 000 HT à 999 999,99 HT', '193000', ' 999 999,99'),
('a1t', '2015', '1 000 000 HT à 2 999 999,99 HT', '1000000', ' 2 999 999,99 '),
('a1t', '2015', '3 000 000 HT à 4 844 999,99 HT', '3000000', ' 4 844 999,99'),
('a1t', '2015', '4 845 000 HT et plus.', '4845000', '');

INSERT IGNORE INTO `t_type_contrat_et_procedure` (`id_type_contrat_et_procedure`,`organisme`, `id_type_contrat`, `id_type_procedure`) VALUES
(NULL,'a1t', 2, 1),
(NULL,'a1t', 4, 1),
(NULL,'a1t', 5, 1),
(NULL,'a1t', 6, 1),
(NULL,'a1t', 1, 1),
(NULL,'a1t', 5, 2),
(NULL,'a1t', 4, 2),
(NULL,'a1t', 11, 2),
(NULL,'a1t', 8, 2),
(NULL,'a1t', 6, 2),
(NULL,'a1t', 2, 2),
(NULL,'a1t', 1, 2),
(NULL,'a1t', 8, 3),
(NULL,'a1t', 4, 3),
(NULL,'a1t', 1, 3),
(NULL,'a1t', 6, 3),
(NULL,'a1t', 2, 3),
(NULL,'a1t', 5, 3),
(NULL,'a1t', 11, 3),
(NULL,'a1t', 6, 4),
(NULL,'a1t', 11, 4),
(NULL,'a1t', 1, 4),
(NULL,'a1t', 5, 4),
(NULL,'a1t', 9, 4),
(NULL,'a1t', 4, 4),
(NULL,'a1t', 2, 4),
(NULL,'a1t', 4, 5),
(NULL,'a1t', 1, 5),
(NULL,'a1t', 5, 5),
(NULL,'a1t', 6, 5),
(NULL,'a1t', 11, 5),
(NULL,'a1t', 9, 5),
(NULL,'a1t', 2, 5),
(NULL,'a1t', 6, 6),
(NULL,'a1t', 1, 6),
(NULL,'a1t', 5, 6),
(NULL,'a1t', 2, 6),
(NULL,'a1t', 5, 7),
(NULL,'a1t', 1, 7),
(NULL,'a1t', 2, 7),
(NULL,'a1t', 6, 7),
(NULL,'a1t', 1, 8),
(NULL,'a1t', 6, 8),
(NULL,'a1t', 1, 9),
(NULL,'a1t', 6, 9),
(NULL,'a1t', 1, 10),
(NULL,'a1t', 6, 10),
(NULL,'a1t', 11, 11),
(NULL,'a1t', 6, 11),
(NULL,'a1t', 8, 11),
(NULL,'a1t', 4, 11),
(NULL,'a1t', 2, 11),
(NULL,'a1t', 5, 11),
(NULL,'a1t', 1, 11),
(NULL,'a1t', 5, 12),
(NULL,'a1t', 8, 12),
(NULL,'a1t', 1, 12),
(NULL,'a1t', 6, 12),
(NULL,'a1t', 11, 12),
(NULL,'a1t', 4, 12),
(NULL,'a1t', 2, 12),
(NULL,'a1t', 12, 17),
(NULL,'a1t', 2, 18),
(NULL,'a1t', 1, 18),
(NULL,'a1t', 6, 18),
(NULL,'a1t', 4, 18),
(NULL,'a1t', 5, 18),
(NULL,'a1t', 6, 30),
(NULL,'a1t', 7, 30),
(NULL,'a1t', 8, 30),
(NULL,'a1t', 9, 30),
(NULL,'a1t', 10, 30),
(NULL,'a1t', 11, 30),
(NULL,'a1t', 5, 30),
(NULL,'a1t', 4, 30),
(NULL,'a1t', 3, 30),
(NULL,'a1t', 2, 30),
(NULL,'a1t', 1, 30),
(NULL,'a1t', 12, 30),
(NULL,'a1t', 7, 31),
(NULL,'a1t', 3, 32),
(NULL,'a1t', 10, 33);



INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (1,1000,'a1t');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (2,1000,'a1t');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (4,1000,'a1t');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (5,1000,'a1t');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (6,1000,'a1t');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (8,1000,'a1t');
INSERT IGNORE INTO `t_type_contrat_et_procedure`(`id_type_contrat`,`id_type_procedure`,`organisme`) VALUES (11,1000,'a1t');


UPDATE `configuration_organisme` SET `type_contrat` = '1' WHERE `configuration_organisme`.`organisme` = 'a1t';

--
-- Insertion des données
--

--
-- Organisme_Service_Metier
--
INSERT IGNORE INTO `Organisme_Service_Metier` (`organisme`, `id_service_metier`) VALUES
('a1a', '1'),
('a2z', '1'),
('e3r', '1'),
('a1t', '1');

--
-- Agent
--
INSERT IGNORE INTO `Agent` (`id`, `login`, `password`, `email`, `nom`, `prenom`, `organisme`, `date_creation`, `date_modification`) VALUES
(1, 'hyperadmin', '15cb90385e95f227c2eff99e5627a848f507cb83', 'suivi.mpe@atexo.com', 'Admin', 'Hyper', 'a1a', CURRENT_TIME, CURRENT_TIME ),
(2, 'admin_a2z', '6bd6f312f96d541ac62096d7177f90a9a995d555', 'suivi.mpe@atexo.com', 'a2z', 'Admin', 'a2z', CURRENT_TIME, CURRENT_TIME ),
(3, 'admin_e3r', '6bc9ec1ca89d888528c64366f5332554b09de442', 'suivi.mpe@atexo.com', 'e3r', 'Admin', 'e3r', CURRENT_TIME, CURRENT_TIME ),
(4, 'admin_a1t', '4b6b9bd8ed9a3483daa6d0c3ba0a87212bacf115', 'suivi.mpe@atexo.com', 'a1t', 'Admin', 'a1t', CURRENT_TIME, CURRENT_TIME );

--
-- Habilitation
--

INSERT IGNORE INTO `HabilitationAgent` (`id_agent`, `hyper_admin`, `administrer_compte`, `gestion_agent_pole`, `gestion_agents`, `gestion_bi_cles`, `gestion_habilitations`, `gestion_type_validation`, `gestion_compte_jal`, `gestion_compte_boamp`, `administrer_procedure`, `administrer_procedures_formalisees`, `gestion_mapa` ) VALUES
('1', '1','1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
('2', '0','0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('3', '0','0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
('4', '0','0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

--
-- SocleHabilitationAgen
--
INSERT IGNORE INTO `Socle_Habilitation_Agent` (`id_agent`, `gestion_agent_pole_socle`, `gestion_agents_socle`, `droit_gestion_services_socle`) VALUES
('1', '1', '1', '1'),
('2', '1', '1', '1'),
('3', '1', '1', '1'),
('4', '1', '1', '1');

--
-- Agent_Service_Metier
--
INSERT IGNORE INTO `Agent_Service_Metier` (`id_agent`, `id_service_metier`, `id_profil_service`, `date_creation`, `date_modification`) VALUES
('1', '1', '1', CURRENT_TIME, CURRENT_TIME),
('2', '1', '1', CURRENT_TIME, CURRENT_TIME),
('3', '1', '1', CURRENT_TIME, CURRENT_TIME),
('4', '1', '1', CURRENT_TIME, CURRENT_TIME);


UPDATE `Administrateur` set  `organisme` = 'a1a' where id = 1;
UPDATE `Administrateur` set  `organisme` = 'a2z' where id = 2;
UPDATE `Administrateur` set  `organisme` = 'e3r' where id = 3;
UPDATE `Administrateur` set  `organisme` = 'a1t' where id = 4;

--
-- Contenu de la table `web_service`
--

INSERT IGNORE INTO `web_service` (`id`, `route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES
(1, '/api.php/ws/authentification/connexion/', 'GET', 'generation token (authentification)', 1),
(2, '/api.php/ws/annonceservice/create', 'POST', 'création annonce service', 1),
(3, '/api.php/ws/annonceservice/update', 'POST', 'update annonce service', 1),
(4, '/api.php/ws/annonceservice/get/', 'GET', 'get annonce service', 1),
(5, '/api.php/ws/consultation/create', 'POST', 'création consultation', 1),
(6, '/api.php/ws/consultation/update', 'POST', 'update consultation', 1),
(7, '/api.php/ws/consultation/get/', 'GET', 'get consultation', 1),
(8, '/api.php/ws/tableaudebord', 'POST', 'tableaubord', 1),
(9, '/api.php/ws/registre/questions/', 'GET', 'get questions', 1),
(10, '/api.php/ws/registre/question/', 'GET', 'get question', 1),
(11, '/api.php/ws/registre/retraitdces/', 'GET', 'get retrait DCES', 1),
(12, '/api.php/ws/registre/retraitdce/', 'GET', 'get retrait DCE', 1),
(13, '/api.php/ws/registre/depots/', 'GET', 'get depots', 1),
(14, '/api.php/ws/registre/depot/', 'GET', 'get depot', 1),
(15, '/api.php/ws/registre/depot/update', 'POST', 'update depots', 1),
(16, '/api.php/ws/registre/pli/', 'GET', 'get pli', 1),
(17, '/api.php/ws/registre/pli/', 'POST', 'post pli', 1),
(18, '/api.php/ws/contactsBySiret/get/', 'GET', 'get contacts par Siret', 1),
(19, '/api.php/ws/contactsByEntrepriseId/get/', 'GET', 'get contacts par id Entreprise', 1),
(20, '/api.php/ws/entrepriseByRaisonSociale/get/', 'GET', 'get entreprise par Raison Sociale', 1),
(21, '/api.php/ws/entrepriseBySiren/get/', 'GET', 'get entreprise par Siret', 1),
(22, '/api.php/ws/archive/listeid/', 'GET', 'get archive liste', 1),
(23, '/api.php/ws/archive/fichier/', 'GET', 'get archive fichier', 1),
(24, '/api.php/ws/organismes', 'GET', 'get organismes', 1),
(25, '/api.php/ws/organisme/', 'GET', 'get organisme', 1),
(26, '/api.php/ws/document/', 'GET', 'get document', 1),
(27, '/api.php/ws/document/', 'POST', 'post document', 1),
(28, '/api.php/ws/depot/admissibilite/', 'GET', 'get depot admissibilite', 1),
(29, '/api.php/ws/depot/decision/', 'GET', 'get depot decision', 1),
(30, '/api.php/ws/offre/admissibilite/', 'GET', 'get offre admissibilite', 1),
(31, '/api.php/ws/offre/decision/', 'GET', 'get offre decision', 1),
(32, '/api.php/ws/registre/echanges/', 'GET', 'get registre echanges', 1),
(33, '/api.php/ws/registre/echange/', 'GET', 'get registre echange', 1),
(34, '/api.php/ws/fichier/', 'GET', 'get fichier', 1),
(35, '/api.php/ws/document/synchro/', 'GET', 'get document synchro', 1),
(36, '/api.php/ws/', 'GET', 'get WS', 1),
(37, '/api.php/ws/', 'POST', 'post WS', 1),
(38, '/api.php/ws/', 'PUT', 'put WS', 1),
(39, '/api/{version}/referentiels/procedureContrats', 'GET', 'get procedure Contrats', 1),
(40, '/referentiels', 'GET', 'get referentiels', 1),
(41, '/api/{version}/agents.{format}', 'GET', 'get agents', 1),
(42, '/api/{version}/agents.{format}', 'POST', 'post agents', 1),
(43, '/api/{version}/agents.{format}', 'PUT', 'put agents', 1),
(44, '/api/{version}/agents.{format}/habilitations', 'GET', 'get agents habilitations', 1),
(45, '/api/{version}/contacts.{format}', 'GET', 'get contacts', 1),
(46, '/api/{version}/contrats.{format}', 'GET', 'get contrats', 1),
(47, '/api/{version}/donnees-essentielles/contrat/format-etendu', 'POST', 'post donnees-essentielles format-etendu', 1),
(48, '/api/{version}/donnees-essentielles/contrat/format-pivot', 'POST', 'post donnees-essentielles format-pivot', 1),
(49, '/api/{version}/entreprises.{format}', 'GET', 'get entreprises', 1),
(50, '/api/{version}/etablissements.{format}', 'GET', 'get etablissements', 1),
(51, '/api/{version}/monitoring/jmeter/hash/{scenario}.{format}', 'GET', 'get monitoring jmeter hash', 1),
(52, '/api/{version}/monitoring/jmeter/{scenario}', 'GET', 'get monitoring jmeter', 1),
(53, '/api/{version}/organismes.{format}', 'GET', 'get organismes (sous sf)', 1),
(54, '/api/{version}/services.{format}', 'GET', 'get services', 1),
(55, '/api/{version}/services.{format}', 'PATCH', 'PATCH services', 1),
(56, '/api/{version}/services.{format}', 'POST', 'POST services', 1),
(57, '/api/{version}/services.{format}', 'PUT', 'PUT services', 1),
(58, '/api/{version}/tableau-de-bord/consultations-en-cours.{format}', 'GET', 'get tableau-de-bord consultations-en-cours', 1),
(59, '/api/{version}/tableau-de-bord/contrats.{format}', 'GET', 'get tableau-de-bord contrats', 1),
(60, '/api/{version}/tableau-de-bord/questions.{format}', 'GET', 'get tableau-de-bord questions', 1),
(61, '/api/{version}/statistiques/interfaces.{format}', 'GET', 'GET Statistique interface', 1),
(62, '/api/{version}/statistiques/interfaces.{format}', 'POST', 'POST Statistique interface', 1),
(63, '/api/{version}/entreprises.{format}', 'POST', 'post entreprises', 1),
(64, '/api/{version}/entreprises.{format}', 'PUT', 'put entreprises', 1),
(65, '/api/{version}/espace-documentaire/rc/{idConsultation}.{format}', 'GET', 'get espacedocumentaire rc', 0),
(66, '/api/{version}/espace-documentaire/dce/autres-pieces/{idConsultation}.{format}', 'GET', 'get espacedocumentaire dce autres pieces', 0),
(67, '/api/{version}/espace-documentaire/plis/{idConsultation}.{format}', 'GET', 'get espacedocumentaire plis', 0),
(68, '/api/{version}/espace-documentaire/dce/{idConsultation}.{format}', 'GET', 'get dce', 0),
(69, '/api/{version}/espace-documentaire/dume/dume-acheteur/{idConsultation}.{format}', 'GET', 'get dume acheteur', 0),
(70, '/api/{version}/espace-documentaire/pieces-du-depots/{idOffre}/{idConsultation}.{format}', 'GET', 'get dume enveloppe', 0),
(71, '/api/{version}/espace-documentaire/publicite/{idConsultation}.{format}', 'GET', 'get publicite', 0),
(72, '/api/{version}/espace-documentaire/autres-pieces-consultation/{idConsultation}.{format}', 'GET', 'get autres pieces consultation', 0),
(73, '/api/{version}/espace-documentaire/autres-pieces-consultation/{idConsultation}.{format}', 'POST', 'post autres pieces consultation', 0),
(74, '/api/{version}/espace-documentaire/autres-pieces-consultation/{idConsultation}/{blobId}.{format}', 'DELETE', 'delete autres pieces consultation', 0),
(75, '/api/{version}/espace-documentaire/plis-attributaires/{idConsultation}.{format}', 'GET', 'get plis attributaires', 0),
(76, '/api/{version}/espace-documentaire/pieces-plis-attributaires/{idOffre}/{idConsultation}.{format}', 'GET', 'get pieces plis attributaires', 0),
(77, '/api/{version}/espace-documentaire/create-download/{idConsultation}.{format}', 'POST', 'post create download', 0),
(78, '/api/{version}/etablissements.{format}', 'POST', 'post etablissements', 1),
(79, '/api/{version}/etablissements.{format}', 'PUT', 'put etablissements', 1),
(80, '/api/{version}/inscrits.{format}', 'GET', 'get inscrits', 1),
(81, '/api/{version}/inscrits.{format}', 'POST', 'post inscrits', 1),
(82, '/api/{version}/inscrits.{format}', 'PUT', 'put inscrits', 1),
(83, '/api/{version}/messagerie/templates.{format}', 'GET', 'get messagerie template', 0),
(84, '/api/{version}/organismes.{format}', 'POST', 'post organismes', 1),
(85, '/api/{version}/organismes.{format}', 'PUT', 'put organismes', 1),
(86, '/api/{version}/dossier-volumineux/upload/logfile/{uuidTechnique}/{HASHMD5}/{ticket}', 'POST', 'post logfile envol', 1),
(87, '/api/{version}/dossier-volumineux/upload/descripteurfile/{uuidTechnique}/{HASHMD5}/{ticket}', 'POST', 'post descripteur envol', 1),
(88, '/api/{version}/dossier-volumineux/upload/metadata/{uuidTechnique}/{ticket}', 'POST', 'post metadata envol', 1),
(89, '/api/{version}/dossier-volumineux/upload/', 'POST', 'post update envol', 1),
(90, '/api/{version}/dossier-volumineux/event/{uuidReference}', 'POST', 'post event envol', 1),
(91, '/api/{version}/echange-documentaire/echange.{format}', 'GET', 'get échanges documentaires', 1),
(92, '/api/{version}/echange-documentaire/echange-historique.{format}', 'GET', 'get echange historique', 1),
(93, '/api/{version}/echange-documentaire/document.{format}', 'GET', 'get document', 1),
(94, '/api/{version}/echange-documentaire/update-statut-echange.{format}', 'PUT', 'update statut échange', 0);

--
-- Contenu de la table `Tiers`
--

INSERT IGNORE INTO `Tiers` (`id_tiers`, `login`, `password`, `denomination`, `fonctionnalite`, `organisme`) VALUES
(1, 'mpe_rec', '5Sq1d1dfzdsq', 'mpe_rec', '4', NULL),
(2, 'mpe_prep', 'dza12da3azd1', 'Crypto', '4', NULL),
(3, 'mpe_crypto_test', 'mpe_crypto_test', 'Crypto', '4', NULL),
(4, 'mpe_oversight', '759ec8276c2e77b', 'mpe_oversight', '4', NULL);



--
-- Contenu de la table `mail_type_group`
--

INSERT IGNORE INTO `mail_type_group` (`id`, `label`) VALUES
(1, 'MessecV2'),
(2, 'MPE');

--
-- Contenu de la table `mail_type`
--

INSERT IGNORE INTO `mail_type` (`id`, `mail_type_group_id`, `label`, `code`) VALUES
(1, 1, 'Courrier libre', 'COURRIER_LIBRE'),
(2, 1, 'Invitation à concourir', 'INVITATION_CONCOURIR'),
(3, 1, 'Information de modification de la consultation', 'MODIFICATION_CONSULTATION'),
(4, 1, 'Réponse à une question', 'REPONSE_QUESTION'),
(5, 1, 'Demande de complément', 'DEMANDE_COMPLEMENT'),
(6, 1, 'Courrier d\'attribution', 'COURRIER_ATTRIBUTION'),
(7, 1, 'Courrier de notification', 'COURRIER_NOTIFICATION'),
(8, 1, 'Courrier de rejet', 'COURRIER_REJET'),
(9, 1, 'Information d\'annulation de la consultation', 'COURRIER_ANNULATION');

--
-- Contenu de la table `mail_template`
--

INSERT IGNORE INTO `mail_template` (`id`, `mail_type_id`, `code`, `objet`, `corps`, `ordre_affichage`, `envoi_modalite`, `envoi_modalite_figee`, `reponse_attendue`, `reponse_attendue_figee`) VALUES
(1, 1, 'COURRIER_LIBRE', 'Courrier libre', '<p>Bonjour,</p><p><br></p><p><strong>[Contenu du message à renseigner]</strong></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>', 1, 'AVEC_AR', 0, 0, 0),
(2, 2, 'INVITATION_CONCOURIR', 'Invitation à concourir', '<p>Bonjour,</p><p><br></p><p>Vous êtes invité à concourir pour la consultation citée en référence.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>', 2, 'AVEC_AR', 0, 0, 0),
(3, 3, 'MODIFICATION_CONSULTATION', 'Information de modification de la consultation', '<p>Bonjour,</p><p><br></p><p>La consultation citée en référence a été modifiée.</p><p><br></p><p>Les éléments modifiés sont : </p><p><strong>[À préciser au cas par cas]</strong></p><p><br></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>', 3, 'AVEC_AR', 0, 0, 0),
(4, 4, 'REPONSE_QUESTION', 'Réponse à une question', '<p>Bonjour,</p><p><br></p><p><strong>[Réponse à la question posée]</strong></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>', 4, 'AVEC_AR', 0, 0, 0),
(5, 5, 'DEMANDE_COMPLEMENT', 'Demande de complément', '<p>Bonjour,</p><p><br></p><p>Nous vous remercions d\'avoir répondu à la consultation citée en référence.</p><p><br></p><p>Après analyse, il vous est demandé d\'apporter les précisions suivantes :</p><p><strong>[À préciser au cas par cas]</strong></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>', 5, 'AVEC_AR', 1, 1, 1),
(6, 6, 'COURRIER_ATTRIBUTION', 'Courrier d\'attribution', '<p>Bonjour,</p><p><br></p><p>J’ai l’honneur de vous informer que vous avez été désigné attributaire de la consultation citée en référence.</p><p>Je vous informe que le contrat ne sera valablement formé qu’après sa signature par un représentant habilité, et ne produira ses effets qu’au terme de la notification dudit marché.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>', 6, 'AVEC_AR', 1, 1, 0),
(7, 7, 'COURRIER_NOTIFICATION', 'Courrier de notification', '<p>Bonjour,</p><p><br></p><p>Votre entreprise a été déclarée attributaire de la consultation citée en référence.</p><p>Veuillez-trouver à titre de notification le marché signé par un représentant habilité.</p><p><br></p><p>L\'Accusé de réception de ce message vaut notification officielle du marché.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>', 7, 'AVEC_AR', 1, 0, 0),
(8, 8, 'COURRIER_REJET', 'Courrier de rejet', '<p>Bonjour,</p><p><br></p><p>Nous vous remercions d\'avoir répondu à la consultation citée en référence.</p><p>Nous sommes toutefois au regret de vous annoncer que votre réponse n\'a pas été retenue.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>', 8, 'AVEC_AR', 1, 0, 0),
(9, 9, 'COURRIER_ANNULATION', 'Information d\'annulation de la consultation', '<p>Bonjour,</p><p><br></p><p>La consultation citée en référence a été annulée.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>', 9, 'AVEC_AR', 0, 0, 0);

--
-- Contenu de la table `echange_doc_application`
--

INSERT IGNORE INTO `echange_doc_application` (`id`, `code`, `libelle`, `flux_actes`, `primo_signature`) VALUES
(1, 'EXPOSITION_WS', 'Exposition simple via Web Service', 0, 0),
(2, 'PASTELL_V2_PDM', 'Pastell v2.0 – Flux Pièce de marché', 0, 1),
(3, 'PASTELL_V2_ACTES', 'Pastell v2.0 – Flux ACTES', 1, 0);

--
-- Contenu de la table `echange_doc_application_client`
--

INSERT IGNORE INTO `echange_doc_application_client` (`id`, `echange_doc_application_id`, `code`, `libelle`, `actif`, `cheminement_signature`, `cheminement_ged`, `cheminement_sae`, `cheminement_tdt`, `classification_1`, `classification_2`, `classification_3`, `classification_4`, `classification_5`) VALUES
(1, 1, 'EXPOSITION_WS_1', 'Exposition par Web Service', 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(2, 2, 'PASTELL_V2_PDM_01', 'Pastell – Flux pièces de marché (Modèle)', 1, 1, 1, 1, 0, 'SOUS_TYPE_PARAPHEUR', 'EO', NULL, NULL, NULL),
(3, 3, 'PASTELL_V2_ACTES_01', 'Pastell – Flux ACTES (Modèle)', 1, 1, 1, 1, 1, 'SOUS_TYPE_PARAPHEUR', NULL, 'CC', '1.1', NULL);


--
-- Contenu de la table `echange_doc_type_piece_actes`
--

INSERT IGNORE INTO `echange_doc_type_piece_actes` (`id`, `code`, `libelle`, `type_nature`, `code_classification`) VALUES
(1, '11_AE', 'Acte d\'engagement', 'CC', '1.1'),
(2, '99_AU', 'Autre document', 'AU', '9.9'),
(3, '11_AC', 'Avis d\'appel public à concurrence', 'CC', '1.1'),
(4, '11_AV', 'Avis du jury de concours', 'CC', '1.1'),
(5, '11_BP', 'Bordereau des prix', 'CC', '1.1'),
(6, '11_AP', 'Cahier des clauses administratives particulières', 'CC', '1.1'),
(7, '11_TP', 'Cahier des clauses techniques particulières', 'CC', '1.1'),
(8, '10_DE', 'Délibération autorisant à passer le contrat', 'CC', '1.0'),
(9, '99_DC', 'Document contractuel', 'CC', '9.9'),
(10, '99_SE', 'Fichier de signature électronique', 'CC', '9.9'),
(11, '11_IN', 'Invitation des candidats à soumissionner', 'CC', '1.1'),
(12, '10_MT', 'Mémoire technique', 'CC', '1.0'),
(13, '10_AV', 'Modification du contrat', 'CC', '1.0'),
(14, '11_PV', 'Procès verbal de la commission d\'appel d\'offre ou du jury', 'CC', '1.1'),
(15, '11_RA', 'Rapport de la commission d\'appel d\'offre', 'CC', '1.1'),
(16, '11_RP', 'Rapport de présentation de l\'acheteur', 'CC', '1.1'),
(17, '11_JU', 'Rapport justifiant le choix du marché, les modalités et la procédure de passation', 'CC', '1.1'),
(18, '10_RD', 'Registre des dépôts des offres', 'CC', '1.0'),
(19, '10_RC', 'Règlement de la consultation', 'CC', '1.0'),
(20, '10_AT', 'Renseignements, attestations et déclarations fournies par l\'attributaire', 'CC', '1.0');

--
-- Contenu de la table `echange_doc_type_piece_standard`
--

INSERT IGNORE INTO `echange_doc_type_piece_standard` (`id`, `code`, `libelle`) VALUES
(1, 'ARN', 'Accusé de Réception de Notification'),
(2, 'AE', 'Acte d\'Engagement'),
(3, 'AN', 'Annexes'),
(4, 'AL', 'Annonces Légales'),
(5, 'AU', 'Autres'),
(6, 'APD', 'Avant Projet Détaillé'),
(7, 'APS', 'Avant Projet Sommaire'),
(8, 'AC', 'Avis d\'appel à la Concurrence'),
(9, 'BPU', 'Bordereau des Prix Unitaires'),
(10, 'CCAP', 'Cahier des Clauses Administratives Particulières'),
(11, 'CCTP', 'Cahier des Clauses Techniques Particulières'),
(12, 'CA', 'Courrier d\'Attribution'),
(13, 'DPGF', 'Décomposition du Prix Global et Forfaitaire'),
(14, 'DQE', 'Détail Quantitatif Estimatif'),
(15, 'DR', 'Dossier de Réponse'),
(16, 'EC', 'Échange en cours de Consultation'),
(17, 'E', 'Étude'),
(18, 'LR', 'Lettre de Rejet'),
(19, 'LC', 'Liste des Candidatures'),
(20, 'P', 'Programme'),
(21, 'A0', 'Rapport d\'Analyse des Offres'),
(22, 'CAO', 'Rapport de Commission d\'Appel d\'Offres'),
(23, 'COP', 'Rapport de Commission d\'Ouverture des Plis'),
(24, 'RP', 'Rapport de Présentation'),
(25, 'RDP', 'Récépissé de dépôt de pli'),
(26, 'RC', 'Règlement de la Consultation');


INSERT IGNORE INTO `echange_doc_application` (`id`, `code`, `libelle`) VALUES ('1', 'EXPOSITION_WS', 'Exposition simple via Web Service');
INSERT IGNORE INTO `echange_doc_application_client` (`id`, `echange_doc_application_id`, `code`, `libelle`, `actif`) VALUES ('1', '1', 'EXPOSITION_WS_1', 'Exposition par Web Service', '1');

INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`)
VALUES ( '/api/{version}/echange-documentaire/echange.{format}', 'GET', 'get échanges documentaires');

INSERT IGNORE INTO `echange_doc_type_piece_actes` (`id`, `code`, `libelle`, `type_nature`,
                                            `code_classification`) VALUES
(1, '11_AE', 'Acte d\'engagement', 'CC', '1.1'),
(2, '99_AU', 'Autre document', 'AU', '9.9'),
(3, '11_AC', 'Avis d\'appel public à concurrence', 'CC', '1.1'),
(4, '11_AV', 'Avis du jury de concours', 'CC', '1.1'),
(5, '11_BP', 'Bordereau des prix', 'CC', '1.1'),
(6, '11_AP', 'Cahier des clauses administratives particulières', 'CC', '1.1'),
(7, '11_TP', 'Cahier des clauses techniques particulières', 'CC', '1.1'),
(8, '10_DE', 'Délibération autorisant à passer le contrat', 'CC', '1.0'),
(9, '99_DC', 'Document contractuel', 'CC', '9.9'),
(10, '99_SE', 'Fichier de signature électronique', 'CC', '9.9'),
(11, '11_IN', 'Invitation des candidats à soumissionner', 'CC', '1.1'),
(12, '10_MT', 'Mémoire technique', 'CC', '1.0'),
(13, '10_AV', 'Modification du contrat', 'CC', '1.0'),
(14, '11_PV', 'Procès verbal de la commission d\'appel d\'offre ou du jury', 'CC', '1.1'),
(15, '11_RA', 'Rapport de la commission d\'appel d\'offre', 'CC', '1.1'),
(16, '11_RP', 'Rapport de présentation de l\'acheteur', 'CC', '1.1'),
(17, '11_JU', 'Rapport justifiant le choix du marché, les modalités et la procédure de passation', 'CC', '1.1'),
(18, '10_RD', 'Registre des dépôts des offres', 'CC', '1.0'),
(19, '10_RC', 'Règlement de la consultation', 'CC', '1.0'),
(20, '10_AT', 'Renseignements, attestations et déclarations fournies par l\'attributaire', 'CC', '1.0');

--
-- Contenu de la table `migration_manager`
--

INSERT IGNORE INTO `migration_manager` (`version`, `executed_at`, `execution_time`) VALUES
('App\\Migrations\\Version20200000000000', '2021-06-25 09:41:58', 1568),
('App\\Migrations\\Version20200115144225', '2021-06-25 09:43:09', 1),
('App\\Migrations\\Version20200115145525', '2021-06-25 09:43:09', 2),
('App\\Migrations\\Version20200115153029', '2021-06-25 09:43:09', 1),
('App\\Migrations\\Version20200115154025', '2021-06-25 09:43:10', 1),
('App\\Migrations\\Version20200115155015', '2021-06-25 09:43:10', 2),
('App\\Migrations\\Version20200115162030', '2021-06-25 09:43:10', 1),
('App\\Migrations\\Version20200115162500', '2021-06-25 09:43:11', 232),
('App\\Migrations\\Version20200115163550', '2021-06-25 09:43:11', 0),
('App\\Migrations\\Version20200115164540', '2021-06-25 09:43:11', 1),
('App\\Migrations\\Version20200115170555', '2021-06-25 09:43:12', 2),
('App\\Migrations\\Version20200323130906', '2021-06-25 09:43:12', 59),
('App\\Migrations\\Version20200324103406', '2021-06-25 09:43:12', 226),
('App\\Migrations\\Version20200324142422', '2021-06-25 09:43:12', 19096),
('App\\Migrations\\Version20200416102247', '2021-06-25 09:43:32', 4333),
('App\\Migrations\\Version20200416121412', '2021-06-25 09:43:36', 117),
('App\\Migrations\\Version20200430133002', '2021-06-25 09:43:36', 53),
('App\\Migrations\\Version20200504125257', '2021-06-25 09:43:37', 385),
('App\\Migrations\\Version20200706071457', '2021-06-25 09:43:37', 179),
('App\\Migrations\\Version20200706081900', '2021-06-25 09:43:38', 467),
('App\\Migrations\\Version20200706094850', '2021-06-25 09:43:38', 1214),
('App\\Migrations\\Version20200706150553', '2021-06-25 09:43:40', 989),
('App\\Migrations\\Version20200716190807', '2021-06-25 09:43:41', 553),
('App\\Migrations\\Version20200720085703', '2021-06-25 09:43:42', 201),
('App\\Migrations\\Version20200721155005', '2021-06-25 09:43:42', 1207),
('App\\Migrations\\Version20200722062056', '2021-06-25 09:43:43', 781),
('App\\Migrations\\Version20200724155924', '2021-06-25 09:43:44', 2228),
('App\\Migrations\\Version20200728081432', '2021-06-25 09:43:47', 42),
('App\\Migrations\\Version20200805125211', '2021-06-25 09:43:47', 75),
('App\\Migrations\\Version20200817134523', '2021-06-25 09:43:47', 35),
('App\\Migrations\\Version20200820121304', '2021-06-25 09:43:47', 511),
('App\\Migrations\\Version20200831151705', '2021-06-25 09:43:48', 9688),
('App\\Migrations\\Version20200911092645', '2021-06-25 09:43:58', 286),
('App\\Migrations\\Version20200921130729', '2021-06-25 09:43:58', 34),
('App\\Migrations\\Version20200924123725', '2021-06-25 09:43:58', 2336),
('App\\Migrations\\Version20200928124530', '2021-06-25 09:44:00', 369),
('App\\Migrations\\Version20201006074310', '2021-06-25 09:44:01', 755),
('App\\Migrations\\Version20201006095646', '2021-06-25 09:44:02', 111),
('App\\Migrations\\Version20201020184714', '2021-06-25 09:44:02', 4881),
('App\\Migrations\\Version20201110142826', '2021-06-25 09:44:08', 2662),
('App\\Migrations\\Version20200824094636', '2021-06-25 09:43:48', 1),
('App\\Migrations\\Version20201113091056', '2021-06-25 09:44:11', 35),
('App\\Migrations\\Version20201127174331', '2021-06-25 09:44:11', 60),
('App\\Migrations\\Version20201208172620', '2021-06-25 09:44:11', 13114),
('App\\Migrations\\Version20210107121710', '2021-06-25 09:44:25', 369),
('App\\Migrations\\Version20210115081816', '2021-06-25 09:44:25', 59),
('App\\Migrations\\Version20210115145400', '2021-06-25 09:44:26', 7202),
('App\\Migrations\\Version20210202133943', '2021-06-25 09:44:33', 132),
('App\\Migrations\\Version20210215150749', '2021-06-25 09:44:33', 59),
('App\\Migrations\\Version20210318132120', '2021-06-25 09:44:36', 2433),
('App\\Migrations\\Version20210319062412', '2021-06-25 09:44:39', 133),
('App\\Migrations\\Version20210323081323', '2021-06-25 09:44:39', 6345),
('App\\Migrations\\Version20210326124136', '2021-06-25 09:44:46', 98),
('App\\Migrations\\Version20210401165537', '2021-06-25 09:44:46', 2),
('App\\Migrations\\Version20210415133706', '2021-06-25 09:44:47', 84);


--
-- Contenu de la table `t_type_contrat`
--
INSERT IGNORE INTO `t_type_contrat` (`id_type_contrat`, `libelle_type_contrat`, `abreviation_type_contrat`, `type_contrat_statistique`, `multi`, `accord_cadre_sad`, `avec_chapeau`, `avec_montant`, `mode_echange_chorus`, `marche_subsequent`, `avec_montant_max`, `ordre_affichage`, `article_133`, `code_dume`, `concession`, `id_externe`) VALUES
(1, 'TYPE_CONTRAT_MARCHE', 'MA', 0, '0', '0', '0', '1', '1', '0', '0', 1, '1', '01', 0, 'mar'),
(2, 'TYPE_CONTRAT_AC_MONO_ATTRIBUTAIRE', 'AC', 3, '0', '1', '0', '0', '2', '0', '1', 4, '1', '03', 0, 'amo'),
(3, 'TYPE_CONTRAT_SYSTEME_ACQUISITION_DYNAMIQUE', 'SA', 4, '1', '2', '1', '0', '2', '0', '1', 10, '1', '06', 0, 'sad'),
(4, 'TYPE_CONTRAT_DELEGATION_DE_SERVICE_PUBLIC', 'AU', 5, '0', '0', '0', '1', '0', '0', '0', 7, '1', '06', 1, 'dsp'),
(5, 'TYPE_CONTRAT_AC_MULTI_ATTRIBUTAIRE', 'AM', 3, '1', '1', '1', '0', '2', '0', '1', 5, '1', '03', 0, 'amu'),
(6, 'TYPE_CONTRAT_MARCHE_PUBLIC_MULTI_ATTRIBUTAIRES', 'AM', 0, '1', '0', '1', '1', '1', '0', '0', 3, '1', '03', 0, 'marmu'),
(7, 'TYPE_CONTRAT_MARCHE_SUBSEQUENT', 'SU', 1, '0', '0', '0', '1', '1', '1', '0', 6, '1', '04', 0, 'msa'),
(8, 'TYPE_CONTRAT_PARTENARIAT_PUBLIC_PRIVE', 'AU', 5, '0', '0', '0', '1', '1', '0', '0', 8, '1', '02', 0, 'ppp'),
(9, 'TYPE_CONTRAT_PARTENARIAT_D_INNOVATION', 'AU', 5, '0', '0', '0', '1', '1', '0', '0', 9, '1', '06', 0, 'pdi'),
(10, 'TYPE_CONTRAT_MARCHE_SPECIFIQUE', 'SP', 5, '0', '0', '0', '1', '1', '2', '0', 11, '1', '06', 0, 'marsp'),
(11, 'TYPE_CONTRAT_MARCHE_DE_CONCEPTION_REALISATION', 'AU', 5, '0', '0', '0', '1', '1', '0', '0', 12, '1', '06', 0, 'ccm'),
(12, 'TYPE_CONTRAT_AUTRE', 'AU', 5, '0', '0', '0', '1', '1', '0', '0', 13, '1', '06', 0, 'aut'),
(13, 'TYPE_CONTRAT_AC_BON_COMMANDE_MONO_ATTRIBUTAIRE', 'AB', 0, '0', '0', '0', '1', '1', '0', '0', 2, '1', '03', 0, ''),
(14, 'TYPE_CONTRAT_MARCHE_GLOBAL_PERFORMANCE', 'AU', 0, '0', '0', '0', '1', '1', '0', '0', 14, '1', '06', 0, 'margp'),
(15, 'TYPE_CONTRAT_MARCHE_SECTORIEL', 'AU', 0, '0', '0', '0', '1', '1', '0', '0', 13, '1', '06', 0, 'margs');



--
-- Contenu de la table `type_procedure_concession_pivot`
--

INSERT IGNORE INTO `type_procedure_concession_pivot` (`id`, `libelle`) VALUES
(1, 'Procédure négociée ouverte'),
(2, 'Procédure non négociée ouverte'),
(3, 'Procédure négociée restreinte'),
(4, 'Procédure non négociée restreinte');

--
-- Contenu de la table `type_procedure_mpe_pivot`
--

INSERT IGNORE INTO `type_procedure_mpe_pivot` (`id`, `id_type_procedure_mpe`, `id_type_procedure_pivot`) VALUES
(1, 1, 2),
(2, 2, 3),
(3, 3, 1),
(4, 4, 2),
(5, 5, 3),
(6, 7, 7),
(7, 8, 6),
(8, 9, 4),
(9, 10, 2),
(10, 11, 2),
(11, 15, 2),
(12, 30, 2),
(13, 31, 2),
(14, 32, 2),
(15, 33, 2),
(16, 35, 2),
(17, 1000, 4);

--
-- Contenu de la table `type_procedure_pivot`
--

INSERT IGNORE INTO `type_procedure_pivot` (`id`, `libelle`) VALUES
(1, 'PROCEDURE_ADAPTEE'),
(2, 'TEXT_APPEL_OFFRE_OUVERT'),
(3, 'APPEL_OFFRES_RESTREINT'),
(4, 'TEXT_PROCEDURE_CONCURENTIELLE_AVEC_NEGOCIATION'),
(6, 'TEXT_PROCEDURE_CONCURENTIELLE_SANS_NEGOCIATION_NI_CONCURRENCE'),
(7, 'DIALOGUE_COMPETITIF');


--
-- Contenu de la table `type_contrat_pivot`
--

INSERT IGNORE INTO `type_contrat_pivot` (`id`, `libelle`) VALUES
(1, 'TYPE_CONTRAT_MARCHE'),
(2, 'TYPE_CONTRAT_PARTENARIAT_PUBLIC_PRIVE'),
(3, 'DEFINE_ACCORD_CADRES'),
(4, 'TEXT_MARCHE_SUBSEQUENT');


--
-- Contenu de la table `type_contrat_mpe_pivot`
--

INSERT IGNORE INTO `type_contrat_mpe_pivot` (`id`, `id_type_contrat_mpe`, `id_type_contrat_pivot`) VALUES
(1, 1, 1),
(2, 2, 3),
(3, 3, 3),
(4, 4, 1),
(5, 5, 3),
(6, 6, 3),
(7, 7, 4),
(8, 8, 2),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 3);

--
-- Contenu de la table `t_referentiel_mots_cles`
--

INSERT INTO `t_referentiel_mots_cles` (`id`, `libelle`, `valeur_sub`)
VALUES (1, 'Abonnement', '001-002'),
       (2, 'Abris', '002-001'),
       (3, 'Acier', '003-003'),
       (4, 'Aéronautique', '004-004'),
       (5, 'Aire d\'accueil', '005-430'),
       (6, 'Aire de jeux', '006-400'),
       (7, 'Alarme', '007-005'),
       (8, 'Alimentation en eau potable', '008-006'),
       (9, 'Aliments pour animaux', '009-007'),
       (10, 'Amiante (désamiantage)', '010-024'),
       (11, 'Amiante (diagnostic)', '011-431'),
       (12, 'Analyses médicales, biologiques', '012-008'),
       (13, 'Animaux', '013-009'),
       (14, 'Antenne', '014-010'),
       (15, 'Antipollution (équipement)', '015-432'),
       (16, 'Appareil élévateur', '016-011'),
       (17, 'Armement', '017-012'),
       (18, 'Arrosage', '018-013'),
       (19, 'Articles à usage unique', '019-015'),
       (20, 'Articles de bureau et articles scolaires', '020-016'),
       (21, 'Articles funéraires, cercueil', '021-401'),
       (22, 'Ascenseur', '022-017'),
       (23, 'Assainissement', '023-018'),
       (24, 'Assistance à maîtrise d\'ouvrage', '024-433'),
       (27, 'Assistance juridique', '025-434');

--
-- Contenu de la table `t_type_notification_agent`
--

INSERT INTO t_type_notification_agent (id_type, libelle) VALUES (1, 'TYPE_NOTIFICATION_CONSULTATION');

COMMIT;
