/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
UPDATE HabilitationProfil
SET espace_documentaire_consultation = 1
WHERE id in (SELECT id FROM `HabilitationProfil` WHERE ouvrir_offre_en_ligne = '1');

update GeolocalisationN2
set code_nuts = 'FR101'
where denomination2 = '75';
update GeolocalisationN2
set code_nuts = 'FR102'
where denomination2 = '77';
update GeolocalisationN2
set code_nuts = 'FR103'
where denomination2 = '78';
update GeolocalisationN2
set code_nuts = 'FR104'
where denomination2 = '91';
update GeolocalisationN2
set code_nuts = 'FR105'
where denomination2 = '92';
update GeolocalisationN2
set code_nuts = 'FR106'
where denomination2 = '93';
update GeolocalisationN2
set code_nuts = 'FR107'
where denomination2 = '94';
update GeolocalisationN2
set code_nuts = 'FR108'
where denomination2 = '95';
update GeolocalisationN2
set code_nuts = 'FRB01'
where denomination2 = '18';
update GeolocalisationN2
set code_nuts = 'FRB02'
where denomination2 = '28';
update GeolocalisationN2
set code_nuts = 'FRB03'
where denomination2 = '36';
update GeolocalisationN2
set code_nuts = 'FRB04'
where denomination2 = '37';
update GeolocalisationN2
set code_nuts = 'FRB05'
where denomination2 = '41';
update GeolocalisationN2
set code_nuts = 'FRB06'
where denomination2 = '45';
update GeolocalisationN2
set code_nuts = 'FRC11'
where denomination2 = '21';
update GeolocalisationN2
set code_nuts = 'FRC12'
where denomination2 = '58';
update GeolocalisationN2
set code_nuts = 'FRC13'
where denomination2 = '71';
update GeolocalisationN2
set code_nuts = 'FRC14'
where denomination2 = '89';
update GeolocalisationN2
set code_nuts = 'FRC21'
where denomination2 = '25';
update GeolocalisationN2
set code_nuts = 'FRC22'
where denomination2 = '39';
update GeolocalisationN2
set code_nuts = 'FRC23'
where denomination2 = '70';
update GeolocalisationN2
set code_nuts = 'FRC24'
where denomination2 = '90';
update GeolocalisationN2
set code_nuts = 'FRD11'
where denomination2 = '14';
update GeolocalisationN2
set code_nuts = 'FRD12'
where denomination2 = '50';
update GeolocalisationN2
set code_nuts = 'FRD13'
where denomination2 = '61';
update GeolocalisationN2
set code_nuts = 'FRD21'
where denomination2 = '27';
update GeolocalisationN2
set code_nuts = 'FRD22'
where denomination2 = '76';
update GeolocalisationN2
set code_nuts = 'FRE11'
where denomination2 = '59';
update GeolocalisationN2
set code_nuts = 'FRE12'
where denomination2 = '62';
update GeolocalisationN2
set code_nuts = 'FRE21'
where denomination2 = '02';
update GeolocalisationN2
set code_nuts = 'FRE22'
where denomination2 = '60';
update GeolocalisationN2
set code_nuts = 'FRE23'
where denomination2 = '80';
update GeolocalisationN2
set code_nuts = 'FRF11'
where denomination2 = '67';
update GeolocalisationN2
set code_nuts = 'FRF12'
where denomination2 = '68';
update GeolocalisationN2
set code_nuts = 'FRF21'
where denomination2 = '08';
update GeolocalisationN2
set code_nuts = 'FRF22'
where denomination2 = '10';
update GeolocalisationN2
set code_nuts = 'FRF23'
where denomination2 = '51';
update GeolocalisationN2
set code_nuts = 'FRF24'
where denomination2 = '52';
update GeolocalisationN2
set code_nuts = 'FRF31'
where denomination2 = '54';
update GeolocalisationN2
set code_nuts = 'FRF32'
where denomination2 = '55';
update GeolocalisationN2
set code_nuts = 'FRF33'
where denomination2 = '57';
update GeolocalisationN2
set code_nuts = 'FRF34'
where denomination2 = '88';
update GeolocalisationN2
set code_nuts = 'FRG01'
where denomination2 = '44';
update GeolocalisationN2
set code_nuts = 'FRG02'
where denomination2 = '49';
update GeolocalisationN2
set code_nuts = 'FRG03'
where denomination2 = '53';
update GeolocalisationN2
set code_nuts = 'FRG04'
where denomination2 = '72';
update GeolocalisationN2
set code_nuts = 'FRG05'
where denomination2 = '85';
update GeolocalisationN2
set code_nuts = 'FRH01'
where denomination2 = '22';
update GeolocalisationN2
set code_nuts = 'FRH02'
where denomination2 = '29';
update GeolocalisationN2
set code_nuts = 'FRH03'
where denomination2 = '35';
update GeolocalisationN2
set code_nuts = 'FRH04'
where denomination2 = '56';
update GeolocalisationN2
set code_nuts = 'FRI11'
where denomination2 = '24';
update GeolocalisationN2
set code_nuts = 'FRI12'
where denomination2 = '33';
update GeolocalisationN2
set code_nuts = 'FRI13'
where denomination2 = '40';
update GeolocalisationN2
set code_nuts = 'FRI14'
where denomination2 = '47';
update GeolocalisationN2
set code_nuts = 'FRI15'
where denomination2 = '64';
update GeolocalisationN2
set code_nuts = 'FRI21'
where denomination2 = '19';
update GeolocalisationN2
set code_nuts = 'FRI22'
where denomination2 = '23';
update GeolocalisationN2
set code_nuts = 'FRI23'
where denomination2 = '87';
update GeolocalisationN2
set code_nuts = 'FRI31'
where denomination2 = '16';
update GeolocalisationN2
set code_nuts = 'FRI32'
where denomination2 = '17';
update GeolocalisationN2
set code_nuts = 'FRI33'
where denomination2 = '79';
update GeolocalisationN2
set code_nuts = 'FRI34'
where denomination2 = '86';
update GeolocalisationN2
set code_nuts = 'FRJ11'
where denomination2 = '11';
update GeolocalisationN2
set code_nuts = 'FRJ12'
where denomination2 = '30';
update GeolocalisationN2
set code_nuts = 'FRJ13'
where denomination2 = '34';
update GeolocalisationN2
set code_nuts = 'FRJ14'
where denomination2 = '48';
update GeolocalisationN2
set code_nuts = 'FRJ15'
where denomination2 = '66';
update GeolocalisationN2
set code_nuts = 'FRJ21'
where denomination2 = '09';
update GeolocalisationN2
set code_nuts = 'FRJ22'
where denomination2 = '12';
update GeolocalisationN2
set code_nuts = 'FRJ23'
where denomination2 = '31';
update GeolocalisationN2
set code_nuts = 'FRJ24'
where denomination2 = '32';
update GeolocalisationN2
set code_nuts = 'FRJ25'
where denomination2 = '46';
update GeolocalisationN2
set code_nuts = 'FRJ26'
where denomination2 = '65';
update GeolocalisationN2
set code_nuts = 'FRJ27'
where denomination2 = '81';
update GeolocalisationN2
set code_nuts = 'FRJ28'
where denomination2 = '82';
update GeolocalisationN2
set code_nuts = 'FRK11'
where denomination2 = '03';
update GeolocalisationN2
set code_nuts = 'FRK12'
where denomination2 = '15';
update GeolocalisationN2
set code_nuts = 'FRK13'
where denomination2 = '43';
update GeolocalisationN2
set code_nuts = 'FRK14'
where denomination2 = '63';
update GeolocalisationN2
set code_nuts = 'FRK21'
where denomination2 = '01';
update GeolocalisationN2
set code_nuts = 'FRK22'
where denomination2 = '07';
update GeolocalisationN2
set code_nuts = 'FRK23'
where denomination2 = '26';
update GeolocalisationN2
set code_nuts = 'FRK24'
where denomination2 = '38';
update GeolocalisationN2
set code_nuts = 'FRK25'
where denomination2 = '42';
update GeolocalisationN2
set code_nuts = 'FRK26'
where denomination2 = '69';
update GeolocalisationN2
set code_nuts = 'FRK27'
where denomination2 = '73';
update GeolocalisationN2
set code_nuts = 'FRK28'
where denomination2 = '74';
update GeolocalisationN2
set code_nuts = 'FRL01'
where denomination2 = '04';
update GeolocalisationN2
set code_nuts = 'FRL02'
where denomination2 = '05';
update GeolocalisationN2
set code_nuts = 'FRL03'
where denomination2 = '06';
update GeolocalisationN2
set code_nuts = 'FRL04'
where denomination2 = '13';
update GeolocalisationN2
set code_nuts = 'FRL05'
where denomination2 = '83';
update GeolocalisationN2
set code_nuts = 'FRL06'
where denomination2 = '84';
update GeolocalisationN2
set code_nuts = 'FRM01'
where denomination2 = '2A';
update GeolocalisationN2
set code_nuts = 'FRM02'
where denomination2 = '2B';
update GeolocalisationN2
set code_nuts = 'FRY10'
where denomination2 = '971';
update GeolocalisationN2
set code_nuts = 'FRY20'
where denomination2 = '972';
update GeolocalisationN2
set code_nuts = 'FRY30'
where denomination2 = '973';
update GeolocalisationN2
set code_nuts = 'FRY40'
where denomination2 = '974';
update GeolocalisationN2
set code_nuts = 'FRY50'
where denomination2 = '976';
update GeolocalisationN2
set code_nuts = 'FRZZZ'
where denomination2 = '975';
update GeolocalisationN2
set code_nuts = 'FRY10'
where denomination2 = 'GP';
update GeolocalisationN2
set code_nuts = 'FRY2'
where denomination2 = 'MQ';
update GeolocalisationN2
set code_nuts = 'FRY4'
where denomination2 = 'RE';