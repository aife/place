-- MySQL dump 10.19  Distrib 10.3.28-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: place_recette_MPE_DB
-- ------------------------------------------------------
-- Server version	10.3.28-MariaDB-1:10.3.28+maria~xenial-log

SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
SET collation_connection = 'utf8_unicode_ci';

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `CategorieINSEE`
--

--  `CategorieINSEE`;


CREATE TABLE `CategorieINSEE`
(
    `id`         varchar(20)  NOT NULL DEFAULT '',
    `libelle`    varchar(100) NOT NULL DEFAULT '',
    `libelle_fr` varchar(100) NOT NULL,
    `libelle_en` varchar(100) NOT NULL,
    `libelle_es` varchar(100) NOT NULL,
    `libelle_su` varchar(100) NOT NULL,
    `libelle_du` varchar(100) NOT NULL,
    `libelle_cz` varchar(100) NOT NULL,
    `libelle_ar` varchar(100) NOT NULL,
    `libelle_it` varchar(100) NOT NULL,
    `code`       varchar(10)           DEFAULT NULL COMMENT 'Code de la catégorie INSEE correspondant',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Organisme`
--

--  `Organisme`;


CREATE TABLE `Organisme`
(
    `id`                        int(11)        NOT NULL AUTO_INCREMENT,
    `acronyme`                  varchar(30)    NOT NULL DEFAULT '',
    `type_article_org`          int(1)         NOT NULL DEFAULT 0,
    `denomination_org`          text                    DEFAULT NULL,
    `categorie_insee`           varchar(20)             DEFAULT NULL,
    `description_org`           mediumtext              DEFAULT NULL,
    `adresse`                   varchar(100)   NOT NULL DEFAULT '',
    `cp`                        varchar(5)     NOT NULL DEFAULT '',
    `ville`                     varchar(100)   NOT NULL DEFAULT '',
    `email`                     varchar(100)   NOT NULL DEFAULT '',
    `url`                       varchar(100)   NOT NULL DEFAULT '',
    `id_attrib_file`            varchar(11)             DEFAULT NULL,
    `attrib_file`               varchar(150)   NOT NULL DEFAULT '',
    `date_creation`             datetime       NOT NULL DEFAULT '0000-00-00 00:00:00',
    `active`                    char(1)        NOT NULL DEFAULT '1',
    `id_client_ANM`             varchar(32)    NOT NULL DEFAULT '0',
    `status`                    enum ('0','1') NOT NULL DEFAULT '0',
    `signataire_cao`            text                    DEFAULT NULL,
    `offset`                    char(3)        NOT NULL DEFAULT '0',
    `sigle`                     varchar(100)   NOT NULL DEFAULT '',
    `adresse2`                  varchar(100)   NOT NULL DEFAULT '',
    `tel`                       varchar(50)    NOT NULL DEFAULT '',
    `telecopie`                 varchar(50)    NOT NULL DEFAULT '',
    `pays`                      varchar(150)            DEFAULT NULL,
    `affichage_entite`          char(1)        NOT NULL DEFAULT '',
    `id_initial`                int(11)        NOT NULL DEFAULT 0,
    `denomination_org_ar`       varchar(100)   NOT NULL,
    `description_org_ar`        mediumtext     NOT NULL,
    `adresse_ar`                varchar(100)   NOT NULL,
    `ville_ar`                  varchar(100)   NOT NULL,
    `adresse2_ar`               varchar(100)   NOT NULL,
    `pays_ar`                   varchar(150)   NOT NULL,
    `denomination_org_fr`       varchar(100)   NOT NULL,
    `description_org_fr`        mediumtext     NOT NULL,
    `adresse_fr`                varchar(100)   NOT NULL,
    `ville_fr`                  varchar(100)   NOT NULL,
    `adresse2_fr`               varchar(100)   NOT NULL,
    `pays_fr`                   varchar(150)   NOT NULL,
    `denomination_org_es`       varchar(100)   NOT NULL,
    `description_org_es`        mediumtext     NOT NULL,
    `adresse_es`                varchar(100)   NOT NULL,
    `ville_es`                  varchar(100)   NOT NULL,
    `adresse2_es`               varchar(100)   NOT NULL,
    `pays_es`                   varchar(150)   NOT NULL,
    `denomination_org_en`       varchar(100)   NOT NULL,
    `description_org_en`        mediumtext     NOT NULL,
    `adresse_en`                varchar(100)   NOT NULL,
    `ville_en`                  varchar(100)   NOT NULL,
    `adresse2_en`               varchar(100)   NOT NULL,
    `pays_en`                   varchar(150)   NOT NULL,
    `denomination_org_su`       varchar(100)   NOT NULL,
    `description_org_su`        mediumtext     NOT NULL,
    `adresse_su`                varchar(100)   NOT NULL,
    `ville_su`                  varchar(100)   NOT NULL,
    `adresse2_su`               varchar(100)   NOT NULL,
    `pays_su`                   varchar(150)   NOT NULL,
    `denomination_org_du`       varchar(100)   NOT NULL,
    `description_org_du`        mediumtext     NOT NULL,
    `adresse_du`                varchar(100)   NOT NULL,
    `ville_du`                  varchar(100)   NOT NULL,
    `adresse2_du`               varchar(100)   NOT NULL,
    `pays_du`                   varchar(150)   NOT NULL,
    `denomination_org_cz`       varchar(100)   NOT NULL,
    `description_org_cz`        mediumtext     NOT NULL,
    `adresse_cz`                varchar(100)   NOT NULL,
    `ville_cz`                  varchar(100)   NOT NULL,
    `adresse2_cz`               varchar(100)   NOT NULL,
    `pays_cz`                   varchar(150)   NOT NULL,
    `denomination_org_it`       varchar(100)   NOT NULL DEFAULT '',
    `description_org_it`        mediumtext     NOT NULL,
    `adresse_it`                varchar(100)   NOT NULL DEFAULT '',
    `ville_it`                  varchar(100)   NOT NULL DEFAULT '',
    `adresse2_it`               varchar(100)   NOT NULL DEFAULT '',
    `pays_it`                   varchar(150)   NOT NULL DEFAULT '',
    `siren`                     varchar(9)     NOT NULL,
    `complement`                varchar(5)     NOT NULL,
    `moniteur_provenance`       int(11)                 DEFAULT 90 COMMENT 'contient la valeur de la balise PROV dans le fichier xml ''context'' du MOL',
    `code_acces_logiciel`       varchar(30)             DEFAULT 'Y8YG-69WD-4421-4G28' COMMENT 'contient le code boamp du logiciel',
    `decalage_horaire`          varchar(5)              DEFAULT NULL COMMENT 'Permet de renseigner le decalage horaire',
    `lieu_residence`            varchar(256)            DEFAULT NULL COMMENT 'Permet de renseigner le lieu de residence du serice',
    `activation_fuseau_horaire` enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''utilisation du fuseau horaire pour le service. Activéà 1 et desactivé à 0',
    `alerte`                    enum ('0','1') NOT NULL DEFAULT '0',
    `ordre`                     int(11)        NOT NULL DEFAULT 0 COMMENT 'permet d''afficher les consultation selon un ordre par organisme',
    `URL_INTERFACE_ANM`         varchar(100)            DEFAULT NULL COMMENT 'url utilisé par le Cli l''interface annonces marches ',
    `sous_type_organisme`       int(11)        NOT NULL DEFAULT 2 COMMENT '1: Etat et ses établissements publics - Autres que ceux ayant un caractère industriel et commercial 2 :Collectivités territoriales / EPL / EPS',
    `pf_url`                    varchar(256)            DEFAULT NULL,
    `tag_purge`                 tinyint(1)              DEFAULT 0 COMMENT 'Champs qui permet de définir si cette organisme doit être purger (0: on ne faite rien ,1: on purge)',
    `id_externe`                varchar(50)    NOT NULL DEFAULT '0',
    `id_entite`                 int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `acronyme` (`acronyme`),
    KEY `categorie_insee` (`categorie_insee`),
    KEY `Idx_Organisme_active` (`active`),
    CONSTRAINT `Organisme_ibfk_1` FOREIGN KEY (`categorie_insee`) REFERENCES `CategorieINSEE` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Agent`
--

--  `Agent`;


CREATE TABLE `Agent`
(
    `id`                                           int(11)        NOT NULL AUTO_INCREMENT,
    `login`                                        varchar(256)            DEFAULT NULL,
    `email`                                        varchar(100)   NOT NULL DEFAULT '',
    `mdp`                                          varchar(64)    NOT NULL,
    `certificat`                                   text                    DEFAULT NULL,
    `nom`                                          varchar(100)   NOT NULL DEFAULT '',
    `prenom`                                       varchar(100)   NOT NULL DEFAULT '',
    `tentatives_mdp`                               int(1)         NOT NULL DEFAULT 0,
    `organisme`                                    varchar(30)             DEFAULT NULL,
    `service_id`                                   int(11)        NOT NULL DEFAULT 0,
    `RECEVOIR_MAIL`                                enum ('0','1') NOT NULL DEFAULT '0',
    `elu`                                          char(1)        NOT NULL DEFAULT '0',
    `nom_fonction`                                 varchar(100)   NOT NULL DEFAULT '',
    `num_tel`                                      varchar(20)             DEFAULT NULL,
    `num_fax`                                      varchar(20)             DEFAULT NULL,
    `type_comm`                                    char(1)        NOT NULL DEFAULT '2',
    `adr_postale`                                  varchar(256)   NOT NULL DEFAULT '',
    `civilite`                                     varchar(256)   NOT NULL DEFAULT '',
    `alerte_reponse_electronique`                  enum ('0','1') NOT NULL DEFAULT '0',
    `alerte_cloture_consultation`                  enum ('0','1') NOT NULL DEFAULT '0',
    `alerte_reception_message`                     enum ('0','1') NOT NULL DEFAULT '0',
    `alerte_publication_boamp`                     enum ('0','1') NOT NULL DEFAULT '0',
    `alerte_echec_publication_boamp`               enum ('0','1') NOT NULL DEFAULT '0',
    `alerte_creation_modification_agent`           enum ('0','1') NOT NULL DEFAULT '0',
    `date_creation`                                varchar(20)             DEFAULT NULL,
    `date_modification`                            varchar(20)             DEFAULT NULL,
    `id_externe`                                   varchar(50)    NOT NULL DEFAULT '0',
    `id_profil_socle_externe`                      int(11)        NOT NULL,
    `lieu_execution`                               text                    DEFAULT NULL,
    `alerte_question_entreprise`                   enum ('0','1') NOT NULL DEFAULT '0',
    `actif`                                        enum ('0','1') NOT NULL DEFAULT '1',
    `codes_nuts`                                   text                    DEFAULT NULL COMMENT 'Contient les codes nuts, pour l''utilisation d''un référentiel externe',
    `num_certificat`                               varchar(64)             DEFAULT NULL,
    `alerte_validation_consultation`               enum ('0','1') NOT NULL DEFAULT '0',
    `alerte_chorus`                                enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à l''acheteur de s''abonner à l''ensemble des alertes chorus à chaque reception d''un flux Chorus par la PMI.',
    `password`                                     varchar(64)    NOT NULL,
    `code_theme`                                   varchar(256)            DEFAULT '0' COMMENT 'définit le thème qui sera utiliser par l''agent',
    `date_connexion`                               datetime                DEFAULT NULL COMMENT 'contient la date de la dernière connexion',
    `type_hash`                                    varchar(10)             DEFAULT 'SHA1',
    `technique`                                    tinyint(1)     NOT NULL DEFAULT 0 COMMENT 'Définit si l utilisateur est un utilisateur technique (accès WS)',
    `alerte_mes_consultations`                     enum ('0','1') NOT NULL DEFAULT '1',
    `alerte_consultations_mon_entite`              enum ('0','1') NOT NULL DEFAULT '1',
    `alerte_consultations_des_entites_dependantes` enum ('0','1') NOT NULL DEFAULT '1',
    `alerte_consultations_mes_entites_transverses` enum ('0','1') NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`),
    UNIQUE KEY `login` (`login`),
    KEY `organisme` (`organisme`),
    CONSTRAINT `Agent_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `Entreprise`
--

--  `Entreprise`;


CREATE TABLE `Entreprise`
(
    `id`                             int(11)        NOT NULL AUTO_INCREMENT,
    `admin_id`                       int(11)        NOT NULL DEFAULT 0,
    `siren`                          varchar(20)             DEFAULT NULL,
    `repmetiers`                     char(1)        NOT NULL DEFAULT '',
    `nom`                            text           NOT NULL,
    `adresse`                        varchar(80)    NOT NULL DEFAULT '',
    `codepostal`                     varchar(5)     NOT NULL DEFAULT '',
    `villeadresse`                   varchar(50)    NOT NULL DEFAULT '',
    `paysadresse`                    varchar(50)    NOT NULL DEFAULT '',
    `email`                          varchar(100)            DEFAULT NULL,
    `taille`                         int(2)                  DEFAULT NULL,
    `formejuridique`                 varchar(256)   NOT NULL DEFAULT '',
    `villeenregistrement`            varchar(50)             DEFAULT NULL,
    `motifNonIndNum`                 int(11)                 DEFAULT NULL,
    `ordreProfOuAgrement`            varchar(30)             DEFAULT NULL,
    `dateConstSociete`               date                    DEFAULT NULL,
    `nomOrgInscription`              varchar(30)             DEFAULT NULL,
    `adrOrgInscription`              varchar(80)             DEFAULT NULL,
    `dateConstAssoc`                 date                    DEFAULT NULL,
    `dateConstAssocEtrangere`        date                    DEFAULT NULL,
    `nomPersonnePublique`            varchar(30)             DEFAULT NULL,
    `nationalite`                    char(2)                 DEFAULT NULL,
    `redressement`                   int(11)                 DEFAULT NULL,
    `paysenregistrement`             varchar(50)             DEFAULT NULL,
    `sirenEtranger`                  varchar(20)             DEFAULT NULL,
    `numAssoEtrangere`               varchar(20)             DEFAULT NULL,
    `debutExerciceGlob1`             varchar(15)             DEFAULT '',
    `finExerciceGlob1`               varchar(15)             DEFAULT '',
    `debutExerciceGlob2`             varchar(15)             DEFAULT '',
    `finExerciceGlob2`               varchar(15)             DEFAULT '',
    `debutExerciceGlob3`             varchar(15)             DEFAULT '',
    `finExerciceGlob3`               varchar(15)             DEFAULT '',
    `ventesGlob1`                    varchar(10)             DEFAULT '',
    `ventesGlob2`                    varchar(10)             DEFAULT '',
    `ventesGlob3`                    varchar(10)             DEFAULT '',
    `biensGlob1`                     varchar(10)             DEFAULT '',
    `biensGlob2`                     varchar(10)             DEFAULT '',
    `biensGlob3`                     varchar(10)             DEFAULT '',
    `servicesGlob1`                  varchar(10)             DEFAULT '',
    `servicesGlob2`                  varchar(10)             DEFAULT '',
    `servicesGlob3`                  varchar(10)             DEFAULT '',
    `totalGlob1`                     varchar(10)             DEFAULT '',
    `totalGlob2`                     varchar(10)             DEFAULT '',
    `totalGlob3`                     varchar(10)             DEFAULT '',
    `codeape`                        varchar(20)    NOT NULL DEFAULT '',
    `libelle_ape`                    varchar(256)            DEFAULT NULL COMMENT 'Le libellé du code APE',
    `origine_compte`                 varchar(256)            DEFAULT NULL,
    `telephone`                      varchar(50)             DEFAULT NULL,
    `fax`                            varchar(50)             DEFAULT NULL,
    `site_internet`                  varchar(50)    NOT NULL DEFAULT '',
    `description_activite`           text                    DEFAULT NULL,
    `activite_domaine_defense`       text                    DEFAULT NULL,
    `annee_cloture_exercice1`        varchar(15)    NOT NULL DEFAULT '',
    `annee_cloture_exercice2`        varchar(15)    NOT NULL DEFAULT '',
    `annee_cloture_exercice3`        varchar(15)    NOT NULL DEFAULT '',
    `effectif_moyen1`                int(10)        NOT NULL DEFAULT 0,
    `effectif_moyen2`                int(10)        NOT NULL DEFAULT 0,
    `effectif_moyen3`                int(10)        NOT NULL DEFAULT 0,
    `effectif_encadrement1`          int(10)        NOT NULL DEFAULT 0,
    `effectif_encadrement2`          int(10)        NOT NULL DEFAULT 0,
    `effectif_encadrement3`          int(10)        NOT NULL DEFAULT 0,
    `pme1`                           enum ('1','0')          DEFAULT NULL,
    `pme2`                           enum ('1','0')          DEFAULT NULL,
    `pme3`                           enum ('1','0')          DEFAULT NULL,
    `adresse2`                       varchar(80)             DEFAULT NULL,
    `nicSiege`                       varchar(5)              DEFAULT NULL,
    `acronyme_pays`                  varchar(10)             DEFAULT NULL,
    `date_creation`                  varchar(20)    NOT NULL,
    `date_modification`              varchar(20)    NOT NULL,
    `id_initial`                     int(11)        NOT NULL DEFAULT 0,
    `region`                         varchar(250)            DEFAULT NULL,
    `province`                       varchar(250)            DEFAULT NULL,
    `telephone2`                     varchar(250)            DEFAULT NULL,
    `telephone3`                     varchar(250)            DEFAULT NULL,
    `cnss`                           varchar(250)            DEFAULT NULL,
    `rc_num`                         varchar(250)            DEFAULT NULL,
    `rc_ville`                       varchar(250)            DEFAULT NULL,
    `domaines_activites`             varchar(256)            DEFAULT NULL,
    `num_tax`                        varchar(250)            DEFAULT NULL,
    `documents_commerciaux`          int(11)                 DEFAULT NULL,
    `intitule_documents_commerciaux` varchar(256)            DEFAULT NULL,
    `taille_documents_commerciaux`   varchar(50)             DEFAULT NULL,
    `qualification`                  varchar(256)            DEFAULT NULL,
    `agrement`                       varchar(256)            DEFAULT NULL,
    `moyens_technique`               longtext                DEFAULT NULL,
    `moyens_humains`                 longtext                DEFAULT NULL,
    `compte_actif`                   int(1)         NOT NULL DEFAULT 1,
    `capital_social`                 varchar(50)    NOT NULL,
    `ifu`                            varchar(200)   NOT NULL,
    `id_agent_createur`              int(20)        NOT NULL,
    `nom_agent`                      varchar(200)   NOT NULL,
    `prenom_agent`                   varchar(200)   NOT NULL,
    `adresses_electroniques`         varchar(256)            DEFAULT NULL,
    `visible_bourse`                 char(1)        NOT NULL DEFAULT '0' COMMENT 'entreprise visible dans la bourse à la Co-traitance / Sous-traitance',
    `type_collaboration`             varchar(256)            DEFAULT NULL COMMENT 'les Type des collaboration ',
    `entreprise_EA`                  enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Entreprise est une EA (Entreprise Adaptée) ou un ESAT (Etablissement ou Service d''Aide par le Travail)',
    `entreprise_SIAE`                enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Entreprise est une SIAE (Structure de l''Insertion par l''Activité Economique)',
    `saisie_manuelle`                enum ('0','1')          DEFAULT '1',
    `created_from_decision`          enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de definir si l''entreprise est creee depuis la decision ou non',
    `id_code_effectif`               int(11)                 DEFAULT NULL COMMENT 'L''id de la valeur referenciel des tranches effectifs',
    `categorie_entreprise`           varchar(256)            DEFAULT NULL COMMENT 'La catégorie de l''entreprise exemple : PME',
    `etat_administratif`             char(1)                 DEFAULT NULL COMMENT 'etat administratif : A => active, C => Cessée',
    `date_cessation`                 datetime                DEFAULT NULL COMMENT 'date de cessation',
    `id_externe`                     varchar(50)    NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `region` (`region`),
    KEY `province` (`province`),
    KEY `domaines_activites` (`domaines_activites`),
    KEY `qualification` (`qualification`),
    KEY `Idx_Entreprise_siren` (`siren`(9))
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;





--
-- Table structure for table `Inscrit`
--

--  `Inscrit`;


CREATE TABLE `Inscrit`
(
    `id`                       int(11)             NOT NULL AUTO_INCREMENT,
    `entreprise_id`            int(11)             NOT NULL DEFAULT 0,
    `id_etablissement`         int(11)             NOT NULL DEFAULT 0,
    `login`                    varchar(50)                  DEFAULT NULL,
    `mdp`                      varchar(64)         NOT NULL,
    `num_cert`                 varchar(64)                  DEFAULT NULL,
    `cert`                     mediumtext                   DEFAULT NULL,
    `civilite`                 tinyint(1)          NOT NULL DEFAULT 0,
    `nom`                      varchar(30)         NOT NULL DEFAULT '',
    `prenom`                   varchar(30)         NOT NULL DEFAULT '',
    `adresse`                  varchar(80)         NOT NULL DEFAULT '',
    `codepostal`               varchar(20)         NOT NULL DEFAULT '',
    `ville`                    varchar(50)         NOT NULL DEFAULT '',
    `pays`                     varchar(50)         NOT NULL DEFAULT '',
    `email`                    varchar(100)        NOT NULL DEFAULT '',
    `telephone`                varchar(20)         NOT NULL DEFAULT '',
    `categorie`                text                         DEFAULT NULL,
    `motstitreresume`          mediumtext                   DEFAULT NULL,
    `periode`                  tinyint(3) unsigned NOT NULL DEFAULT 0,
    `siret`                    varchar(5)          NOT NULL,
    `fax`                      varchar(20)         NOT NULL DEFAULT '',
    `code_cpv`                 text                         DEFAULT NULL,
    `id_langue`                int(11)                      DEFAULT NULL,
    `profil`                   int(1)              NOT NULL DEFAULT 1,
    `adresse2`                 varchar(80)                  DEFAULT NULL,
    `bloque`                   enum ('0','1')      NOT NULL DEFAULT '0',
    `id_initial`               int(11)             NOT NULL DEFAULT 0,
    `inscrit_annuaire_defense` enum ('0','1')      NOT NULL,
    `date_creation`            varchar(20)         NOT NULL,
    `date_modification`        varchar(20)         NOT NULL,
    `tentatives_mdp`           int(1)              NOT NULL DEFAULT 0,
    `uid`                      varchar(50)                  DEFAULT NULL,
    `type_hash`                varchar(10)                  DEFAULT NULL,
    `id_externe`               varchar(50)         NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`, `entreprise_id`),
    UNIQUE KEY `email` (`email`),
    UNIQUE KEY `login` (`login`),
    KEY `entreprise_id` (`entreprise_id`),
    KEY `id_etablissement_idx` (`id_etablissement`),
    KEY `inscrit_id_idx` (`id`),
    CONSTRAINT `Inscrit_ibfk_1` FOREIGN KEY (`entreprise_id`) REFERENCES `Entreprise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Propriétés de l''entité Utilisateur d''Entreprise';


--
-- Table structure for table `dossier_volumineux`
--

--  `dossier_volumineux`;


CREATE TABLE `dossier_volumineux`
(
    `id`                  int(11)      NOT NULL AUTO_INCREMENT,
    `uuid_reference`      varchar(30)  NOT NULL,
    `nom`                 varchar(30)           DEFAULT NULL,
    `taille`              bigint(20) unsigned   DEFAULT NULL,
    `date_creation`       datetime     NOT NULL,
    `statut`              varchar(30)  NOT NULL DEFAULT 'init' COMMENT 'valauer possible : init, complet, incomplet',
    `id_agent`            int(11)               DEFAULT NULL,
    `id_inscrit`          int(11)               DEFAULT NULL,
    `id_blob_descripteur` int(11)               DEFAULT NULL,
    `uuid_technique`      varchar(256) NOT NULL,
    `actif`               tinyint(1)            DEFAULT 0,
    `id_blob_logfile`     int(11)               DEFAULT NULL,
    `date_modification`   datetime              DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uuid_technique` (`uuid_technique`),
    UNIQUE KEY `uuid_reference` (`uuid_reference`) USING BTREE,
    KEY `dossier_volumineux_agent_FK` (`id_agent`) USING BTREE,
    KEY `dossier_volumineux_inscrit_FK` (`id_inscrit`) USING BTREE,
    KEY `dossier_volumineux_id_blob_logfile_FK` (`id_blob_logfile`),
    KEY `dossier_volumineux_id_blob_descripteur_FK` (`id_blob_descripteur`),
    CONSTRAINT `dossier_volumineux_id_blob_descripteur_FK` FOREIGN KEY (`id_blob_descripteur`) REFERENCES `blob_file` (`id`),
    CONSTRAINT `dossier_volumineux_id_blob_logfile_FK` FOREIGN KEY (`id_blob_logfile`) REFERENCES `blob_file` (`id`),
    CONSTRAINT `id_agent_dv_FK` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`),
    CONSTRAINT `id_inscrit_FK` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `plateforme_virtuelle`
--

--  `plateforme_virtuelle`;


CREATE TABLE `plateforme_virtuelle`
(
    `id`           int(11) unsigned NOT NULL AUTO_INCREMENT,
    `domain`       varchar(256)     NOT NULL,
    `name`         varchar(256)     NOT NULL,
    `code_design`  varchar(25)               DEFAULT NULL,
    `protocole`    varchar(5)       NOT NULL DEFAULT 'https',
    `no_reply`     varchar(255)              DEFAULT NULL COMMENT 'Email de surcharge du "nepasrepondre@domaine.xx"',
    `footer_mail`  text                      DEFAULT NULL COMMENT 'text qui surcharge le paramètre applicatif : "PLACE_MARCHE_PUBLIC_INTERMINISTERE"',
    `from_pf_name` varchar(255)              DEFAULT NULL COMMENT 'Nom de la plate-forme dans le From du mail',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Operations`
--

--  `Operations`;


CREATE TABLE `Operations`
(
    `id_operation`      int(11)        NOT NULL AUTO_INCREMENT,
    `acronyme`          varchar(30)    NOT NULL,
    `id_service`        int(11)        NOT NULL,
    `id_agent`          int(11)        NOT NULL,
    `type`              int(2)         NOT NULL,
    `categorie`         int(2)                  DEFAULT NULL,
    `code`              varchar(256)   NOT NULL,
    `description`       longtext       NOT NULL,
    `annee_debut`       varchar(4)     NOT NULL,
    `annee_fin`         varchar(4)     NOT NULL,
    `budget`            varchar(40)    NOT NULL,
    `date_creation`     varchar(20)    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `date_modification` varchar(20)    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `supprime`          enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id_operation`),
    KEY `index_acronyme` (`acronyme`),
    KEY `index_service` (`id_service`),
    KEY `index_id_agent` (`id_agent`),
    KEY `index_type` (`type`),
    KEY `index_categorie` (`categorie`),
    KEY `index_code` (`code`),
    KEY `index_annee_fin` (`annee_fin`),
    KEY `index_annee_debut` (`annee_debut`),
    KEY `index_description` (`description`(255))
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `consultation`
--

--  `consultation`;


CREATE TABLE `consultation`
(
    `id`                                     int(11)                                     NOT NULL AUTO_INCREMENT,
    `organisme`                              varchar(30)                                 NOT NULL DEFAULT '',
    `reference`                              int(11)                                              DEFAULT NULL,
    `code_externe`                           varchar(225)                                         DEFAULT NULL,
    `reference_utilisateur`                  varchar(256)                                NOT NULL DEFAULT '',
    `categorie`                              varchar(30)                                 NOT NULL DEFAULT '0',
    `titre`                                  longtext                                    NOT NULL,
    `resume`                                 longtext                                    NOT NULL,
    `datedebut`                              date                                        NOT NULL DEFAULT '0000-00-00',
    `datefin`                                datetime                                    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `datevalidation`                         varchar(20)                                 NOT NULL DEFAULT '0000-00-00 00:00:00',
    `type_procedure`                         char(1)                                     NOT NULL DEFAULT '',
    `code_procedure`                         varchar(15)                                          DEFAULT NULL,
    `reponse_electronique`                   char(1)                                     NOT NULL DEFAULT '1',
    `num_procedure`                          int(1)                                      NOT NULL DEFAULT 0,
    `id_type_procedure`                      int(1)                                      NOT NULL DEFAULT 0,
    `id_type_avis`                           int(2)                                      NOT NULL DEFAULT 0,
    `lieu_execution`                         text                                        NOT NULL,
    `type_mise_en_ligne`                     int(11)                                     NOT NULL DEFAULT 1,
    `datemiseenligne`                        datetime                                    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `is_tiers_avis`                          char(1)                                     NOT NULL DEFAULT '0',
    `url`                                    varchar(256)                                NOT NULL DEFAULT '',
    `datefin_sad`                            datetime                                    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `is_sys_acq_dyn`                         int(11)                                     NOT NULL DEFAULT 0,
    `reference_consultation_init`            varchar(250)                                NOT NULL DEFAULT '',
    `signature_offre`                        char(1)                                              DEFAULT NULL,
    `id_type_validation`                     int(11)                                     NOT NULL DEFAULT 2,
    `etat_approbation`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `etat_validation`                        enum ('0','1')                              NOT NULL DEFAULT '0',
    `champ_supp_invisible`                   text                                        NOT NULL,
    `code_cpv_1`                             varchar(8)                                           DEFAULT NULL,
    `code_cpv_2`                             varchar(256)                                         DEFAULT NULL,
    `publication_europe`                     enum ('0','1')                                       DEFAULT '0',
    `etat_publication`                       int(11)                                     NOT NULL DEFAULT 0,
    `poursuivre_affichage`                   int(11)                                     NOT NULL DEFAULT 0,
    `poursuivre_affichage_unite`             enum ('MINUTE','HOUR','DAY','MONTH','YEAR') NOT NULL DEFAULT 'DAY' COMMENT 'Permet de gerer la poursuite de l''affichage de la consultation en ''MINUTE'', ''HOUR'', ''DAY'', ''MONTH'' ou ''YEAR''',
    `nbr_telechargement_dce`                 int(5)                                               DEFAULT NULL,
    `service_id`                             int(11)                                              DEFAULT NULL,
    `service_associe_id`                     int(11)                                              DEFAULT NULL,
    `detail_consultation`                    text                                        NOT NULL,
    `date_fin_affichage`                     varchar(20)                                 NOT NULL DEFAULT '0000-00-00 00:00:00',
    `depouillable_phase_consultation`        enum ('0','1')                              NOT NULL DEFAULT '0',
    `consultation_transverse`                enum ('0','1')                              NOT NULL DEFAULT '0',
    `consultation_achat_publique`            enum ('0','1')                              NOT NULL DEFAULT '0',
    `url_consultation_achat_publique`        text                                                 DEFAULT NULL,
    `partial_dce_download`                   char(1)                                     NOT NULL DEFAULT '0',
    `tirage_plan`                            int(11)                                     NOT NULL DEFAULT 0,
    `tireur_plan`                            int(11)                                     NOT NULL DEFAULT 0,
    `date_mise_en_ligne_calcule`             datetime                                             DEFAULT NULL,
    `accessibilite_en`                       char(1)                                     NOT NULL DEFAULT '0',
    `accessibilite_es`                       char(1)                                     NOT NULL DEFAULT '0',
    `nbr_reponse`                            int(5)                                               DEFAULT NULL,
    `id_type_procedure_org`                  int(1)                                      NOT NULL DEFAULT 0,
    `tirage_descriptif`                      mediumtext                                  NOT NULL,
    `organisme_consultation_init`            varchar(256)                                NOT NULL DEFAULT '',
    `date_validation_intermediaire`          varchar(20)                                          DEFAULT NULL,
    `accessibilite_fr`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `id_tr_accessibilite`                    int(11)                                              DEFAULT NULL COMMENT 'Identifiant de la traduction de l''accessibilité pour la traduction de la consultation par les entreprises',
    `accessibilite_cz`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `accessibilite_du`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `accessibilite_su`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `accessibilite_ar`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `alloti`                                 enum ('0','1')                              NOT NULL DEFAULT '0',
    `numero_phase`                           int(3)                                      NOT NULL DEFAULT 0,
    `consultation_externe`                   enum ('0','1')                              NOT NULL DEFAULT '0',
    `url_consultation_externe`               text                                                 DEFAULT NULL,
    `org_denomination`                       varchar(250)                                         DEFAULT NULL,
    `domaines_activites`                     varchar(250)                                         DEFAULT '',
    `id_affaire`                             int(11)                                              DEFAULT NULL,
    `adresse_retrais_dossiers`               longtext                                             DEFAULT NULL,
    `caution_provisoire`                     varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres`                   longtext                                             DEFAULT NULL,
    `lieu_ouverture_plis`                    longtext                                             DEFAULT NULL,
    `prix_aquisition_plans`                  varchar(256)                                         DEFAULT NULL,
    `qualification`                          varchar(256)                                         DEFAULT NULL,
    `agrements`                              varchar(256)                                         DEFAULT NULL,
    `add_echantillion`                       varchar(256)                                         DEFAULT NULL,
    `date_limite_echantillion`               varchar(50)                                          DEFAULT NULL,
    `add_reunion`                            varchar(256)                                         DEFAULT NULL,
    `date_reunion`                           varchar(50)                                          DEFAULT NULL,
    `variantes`                              char(1)                                              DEFAULT NULL,
    `adresse_depot_offres_ar`                varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_ar`                 varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_ar`            varchar(256)                                         DEFAULT NULL,
    `pieces_dossier_admin`                   text                                                 DEFAULT NULL,
    `pieces_dossier_admin_fr`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_en`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_es`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_su`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_du`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_cz`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_ar`                text                                                 DEFAULT NULL,
    `pieces_dossier_tech`                    text                                                 DEFAULT NULL,
    `pieces_dossier_tech_fr`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_en`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_es`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_su`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_du`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_cz`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_ar`                 text                                                 DEFAULT NULL,
    `pieces_dossier_additif`                 text                                                 DEFAULT NULL,
    `pieces_dossier_additif_fr`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_en`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_es`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_su`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_du`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_cz`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_ar`              text                                                 DEFAULT NULL,
    `id_rpa`                                 int(11)                                              DEFAULT NULL,
    `detail_consultation_fr`                 text                                                 DEFAULT NULL,
    `detail_consultation_en`                 text                                                 DEFAULT NULL,
    `detail_consultation_es`                 text                                                 DEFAULT NULL,
    `detail_consultation_su`                 text                                                 DEFAULT NULL,
    `detail_consultation_du`                 text                                                 DEFAULT NULL,
    `detail_consultation_cz`                 text                                                 DEFAULT NULL,
    `detail_consultation_ar`                 text                                                 DEFAULT NULL,
    `echantillon`                            enum ('0','1')                              NOT NULL,
    `reunion`                                enum ('0','1')                              NOT NULL,
    `visites_lieux`                          enum ('0','1')                              NOT NULL,
    `variante_calcule`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `adresse_retrais_dossiers_fr`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_en`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_es`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_su`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_du`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_cz`            varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_fr`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_en`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_es`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_su`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_du`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_cz`                varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_fr`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_en`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_es`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_su`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_du`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_cz`                 varchar(256)                                         DEFAULT NULL,
    `add_echantillion_fr`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_en`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_es`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_su`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_du`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_cz`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_ar`                    varchar(256)                                         DEFAULT NULL,
    `add_reunion_fr`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_en`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_es`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_su`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_du`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_cz`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_ar`                         varchar(256)                                         DEFAULT NULL,
    `mode_passation`                         varchar(1)                                           DEFAULT NULL,
    `consultation_annulee`                   enum ('0','1')                              NOT NULL DEFAULT '0',
    `accessibilite_it`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `adresse_depot_offres_it`                varchar(256)                                NOT NULL DEFAULT '',
    `lieu_ouverture_plis_it`                 varchar(256)                                NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_it`            varchar(256)                                NOT NULL DEFAULT '',
    `pieces_dossier_admin_it`                varchar(256)                                NOT NULL DEFAULT '',
    `pieces_dossier_tech_it`                 varchar(256)                                NOT NULL DEFAULT '',
    `pieces_dossier_additif_it`              varchar(256)                                NOT NULL DEFAULT '',
    `detail_consultation_it`                 text                                                 DEFAULT NULL,
    `add_echantillion_it`                    varchar(250)                                NOT NULL DEFAULT '',
    `add_reunion_it`                         varchar(250)                                NOT NULL DEFAULT '',
    `codes_nuts`                             text                                                 DEFAULT NULL COMMENT 'Contient les codes nuts, pour l''utilisation d''un referentiel externe',
    `intitule`                               longtext                                    NOT NULL,
    `id_tr_intitule`                         int(11)                                              DEFAULT NULL COMMENT 'Identifiant de la traduction de l''intitulé de la consultation',
    `objet`                                  longtext                                    NOT NULL,
    `id_tr_objet`                            int(11)                                              DEFAULT NULL COMMENT 'Identifiant de la traduction de l''objet de la consultation',
    `type_acces`                             char(1)                                     NOT NULL,
    `autoriser_reponse_electronique`         char(1)                                     NOT NULL DEFAULT '1',
    `regle_mise_en_ligne`                    int(11)                                     NOT NULL DEFAULT 1,
    `id_regle_validation`                    int(11)                                     NOT NULL DEFAULT 2,
    `intitule_fr`                            longtext                                             DEFAULT NULL,
    `intitule_en`                            longtext                                             DEFAULT NULL,
    `intitule_es`                            longtext                                             DEFAULT NULL,
    `intitule_su`                            longtext                                             DEFAULT NULL,
    `intitule_du`                            longtext                                             DEFAULT NULL,
    `intitule_cz`                            longtext                                             DEFAULT NULL,
    `intitule_ar`                            longtext                                             DEFAULT NULL,
    `intitule_it`                            longtext                                             DEFAULT NULL,
    `objet_fr`                               longtext                                             DEFAULT NULL,
    `objet_en`                               longtext                                             DEFAULT NULL,
    `objet_es`                               longtext                                             DEFAULT NULL,
    `objet_su`                               longtext                                             DEFAULT NULL,
    `objet_du`                               longtext                                             DEFAULT NULL,
    `objet_cz`                               longtext                                             DEFAULT NULL,
    `objet_ar`                               longtext                                             DEFAULT NULL,
    `objet_it`                               longtext                                             DEFAULT NULL,
    `date_decision`                          varchar(10)                                 NOT NULL DEFAULT '',
    `clause_sociale`                         enum ('0','1','2')                          NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non',
    `clause_environnementale`                enum ('0','1','2')                          NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non',
    `reponse_obligatoire`                    char(1)                                     NOT NULL DEFAULT '0',
    `Compte_Boamp_Associe`                   int(11)                                              DEFAULT NULL,
    `autoriser_publicite`                    int(2)                                      NOT NULL DEFAULT 1,
    `type_envoi`                             char(1)                                     NOT NULL DEFAULT '',
    `chiffrement_offre`                      char(1)                                     NOT NULL DEFAULT '',
    `env_candidature`                        int(1)                                      NOT NULL DEFAULT 0,
    `env_offre`                              int(3)                                      NOT NULL DEFAULT 0,
    `env_anonymat`                           int(1)                                      NOT NULL DEFAULT 0,
    `id_etat_consultation`                   int(11)                                     NOT NULL DEFAULT 0,
    `reference_connecteur`                   varchar(256)                                NOT NULL DEFAULT '',
    `cons_statut`                            char(1)                                     NOT NULL DEFAULT '0',
    `id_approbateur`                         int(11)                                     NOT NULL DEFAULT 0,
    `id_valideur`                            int(11)                                     NOT NULL DEFAULT 0,
    `service_validation`                     int(11)                                     NOT NULL DEFAULT 0,
    `id_createur`                            int(11)                                     NOT NULL DEFAULT 0,
    `nom_createur`                           varchar(100)                                         DEFAULT NULL,
    `prenom_createur`                        varchar(100)                                         DEFAULT NULL,
    `signature_acte_engagement`              enum ('0','1')                              NOT NULL DEFAULT '0',
    `archiveMetaDescription`                 longtext                                             DEFAULT NULL,
    `archiveMetaMotsClef`                    longtext                                             DEFAULT NULL,
    `archiveIdBlobZip`                       int(11)                                              DEFAULT NULL,
    `decision_partielle`                     enum ('0','1')                              NOT NULL DEFAULT '0',
    `type_decision_a_renseigner`             enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_attribution_marche`       enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_declaration_sans_suite`   enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_declaration_infructueux`  enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_selection_entreprise`     enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_attribution_accord_cadre` enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_admission_sad`            enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_autre`                    enum ('0','1')                              NOT NULL DEFAULT '1',
    `id_archiveur`                           int(11)                                              DEFAULT NULL,
    `prenom_nom_agent_telechargement_plis`   varchar(256)                                         DEFAULT NULL,
    `id_agent_telechargement_plis`           int(11)                                     NOT NULL DEFAULT 0,
    `path_telechargement_plis`               varchar(256)                                         DEFAULT NULL,
    `date_telechargement_plis`               varchar(20)                                          DEFAULT NULL,
    `service_validation_intermediaire`       int(11)                                              DEFAULT NULL,
    `env_offre_technique`                    int(3)                                      NOT NULL DEFAULT 0,
    `ref_org_partenaire`                     varchar(250)                                NOT NULL DEFAULT '',
    `date_archivage`                         varchar(20)                                          DEFAULT NULL COMMENT 'Précise la date de réalisation de l''archivage',
    `date_decision_annulation`               varchar(20)                                          DEFAULT NULL,
    `commentaire_annulation`                 text                                                 DEFAULT NULL,
    `date_mise_en_ligne_souhaitee`           varchar(20)                                          DEFAULT NULL,
    `etat_en_attente_validation`             enum ('0','1')                              NOT NULL DEFAULT '1',
    `dossier_additif`                        enum ('0','1')                              NOT NULL DEFAULT '0',
    `type_marche`                            int(11)                                     NOT NULL DEFAULT 0,
    `type_prestation`                        int(11)                                     NOT NULL DEFAULT 1,
    `date_modification`                      varchar(20)                                          DEFAULT NULL,
    `delai_partiel`                          enum ('0','1')                              NOT NULL DEFAULT '0',
    `dateFinLocale`                          varchar(20)                                          DEFAULT '0000-00-00 00:00:00' COMMENT 'Permet de renseigner la date limite de remise des plis locale en rapport avec le fuseau horaire',
    `lieuResidence`                          varchar(256)                                         DEFAULT NULL COMMENT 'Permet de renseigner le lieu de residence en rapport avec le fuseau horaire',
    `alerte`                                 enum ('0','1')                              NOT NULL DEFAULT '0',
    `doublon`                                enum ('0','1')                              NOT NULL DEFAULT '0',
    `denomination_adapte`                    varchar(250)                                         DEFAULT NULL,
    `url_consultation_avis_pub`              text                                        NOT NULL,
    `doublon_de`                             varchar(250)                                         DEFAULT NULL COMMENT 'Si 762 est le doublon de 14 alors doublon_de = 14, si doublon_de vide si je suis doublon de personne',
    `entite_adjudicatrice`                   enum ('0','1')                                       DEFAULT NULL COMMENT 'Permet de savoir si une Consultation est passée en tant qu''Entité adjudicatrice',
    `code_operation`                         varchar(256)                                         DEFAULT NULL COMMENT 'Permet de stocker la valeur du champ "Code opération"',
    `clause_sociale_condition_execution`     varchar(255)                                         DEFAULT '0' COMMENT 'Le marché comprend des clauses sociales d''insertion comme condition d''exécution (article 14 du Code des marchés publics)',
    `clause_sociale_insertion`               varchar(255)                                         DEFAULT '0' COMMENT 'Présence parmi les critères d''attribution d''un critère relatif à l''insertion (article 53 du code des marchés publics)',
    `clause_sociale_ateliers_proteges`       varchar(255)                                         DEFAULT '0' COMMENT 'Le marché est réservé à des ateliers protégés (article 15 du code des marchés publics)',
    `clause_sociale_siae`                    varchar(255)                                         DEFAULT '0' COMMENT 'Le marché est réservé à  SIAE',
    `clause_sociale_ess`                     varchar(255)                                         DEFAULT '0' COMMENT 'Le marché est réservé à  ESS',
    `clause_env_specs_techniques`            varchar(255)                                         DEFAULT '0' COMMENT 'pécifications techniques (article 6 du code des marchés publics)',
    `clause_env_cond_execution`              varchar(255)                                         DEFAULT '0' COMMENT 'Conditions d''exécution (article 14 du code des marchés publics)',
    `clause_env_criteres_select`             varchar(255)                                         DEFAULT '0' COMMENT 'Critère de sélection (article 53.1 du code des marchés publics)',
    `id_donnee_complementaire`               int(11)                                              DEFAULT NULL,
    `donnee_complementaire_obligatoire`      enum ('0','1')                              NOT NULL DEFAULT '0',
    `mode_ouverture_reponse`                 enum ('0','1')                              NOT NULL DEFAULT '0' COMMENT 'Enveloppe unique ou Multi-enveloppe',
    `id_fichier_annulation`                  int(11)                                              DEFAULT NULL,
    `idOperation`                            int(11)                                              DEFAULT NULL,
    `marche_public_simplifie`                enum ('0','1')                              NOT NULL DEFAULT '0' COMMENT 'Permet de savoir si une Consultation est passée en tant que marche public simplifie',
    `infos_blocs_atlas`                      varchar(10)                                 NOT NULL DEFAULT '0##0' COMMENT 'bloc a partir du quel on commence l''envoie##nbr des blocs existants',
    `DATE_FIN_UNIX`                          varchar(20)                                          DEFAULT '0' COMMENT 'contient : UNIX_TIMESTAMP( datefin ) + ( consultation.poursuivre_affichage * ( 24 *60 *60 ) ), permet d''optimisé la recherche en recuperant le resultat direct de ce champ',
    `numero_AC`                              varchar(256)                                         DEFAULT NULL,
    `id_contrat`                             int(11)                                              DEFAULT NULL,
    `donnee_publicite_obligatoire`           enum ('0','1')                                       DEFAULT NULL COMMENT 'permet de sauvegarder la valeur de la case à cocher "Je souhaite publier un avis depuis cette plate-forme pour cette consultation"',
    `pin_api_sgmap_mps`                      varchar(20)                                          DEFAULT NULL COMMENT 'Code PIN de la consultation via l''API MPS du SGMAP',
    `dume_demande`                           enum ('0','1')                              NOT NULL DEFAULT '0' COMMENT 'Permet de savoir si une Consultation permet la candidature DUME',
    `type_procedure_dume`                    int(11)                                     NOT NULL DEFAULT 0 COMMENT 'type procedure dume liÃ©',
    `marche_insertion`                       tinyint(1)                                           DEFAULT 0,
    `clause_specification_technique`         varchar(256)                                         DEFAULT '0',
    `type_formulaire_dume`                   char(2)                                              DEFAULT '0' COMMENT 'Permet de savoir le type de formulaire dume "0"=>standard,"1"=>simplifié',
    `source_externe`                         varchar(256)                                         DEFAULT NULL,
    `id_source_externe`                      int(11)                                              DEFAULT NULL,
    `numero_projet_achat`                    varchar(256)                                         DEFAULT NULL,
    `id_dossier_volumineux`                  int(11)                                              DEFAULT NULL,
    `attestation_consultation`               char(2)                                     NOT NULL DEFAULT '0' COMMENT 'Permet d''attester que le besoin n''est pas déjà couvert par un marché national ou local',
    `version_messagerie`                     int(11)                                     NOT NULL DEFAULT 1 COMMENT 'version de la messagerie',
    `plateforme_virtuelle_id`                int(10) unsigned                                     DEFAULT NULL,
    `is_envoi_publicite_validation`          tinyint(1)                                  NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `organisme` (`organisme`),
    KEY `reference` (`reference`),
    KEY `Idx_Consultation_Categorie` (`categorie`),
    KEY `Idx_Consultation_CAP` (`consultation_achat_publique`),
    KEY `Idx_Consultation_Datevalid` (`datevalidation`),
    KEY `Idx_Consultation_EA` (`etat_approbation`),
    KEY `Idx_Consultation_DVI` (`date_validation_intermediaire`),
    KEY `Idx_Consultation_RU` (`reference_utilisateur`),
    KEY `Idx_Consultation_Datefin` (`datefin`),
    KEY `Idx_Consultation_ITP` (`id_type_procedure`),
    KEY `Idx_Consultation_ITA` (`id_type_avis`),
    KEY `Idx_Consultation_TML` (`type_mise_en_ligne`),
    KEY `Idx_Consultation_DMLC` (`date_mise_en_ligne_calcule`),
    KEY `Idx_Consultation_Datemiseenligne` (`datemiseenligne`),
    KEY `Idx_Consultation_Code_CPV` (`code_cpv_1`, `code_cpv_2`),
    KEY `idx_consultation_alerte` (`alerte`),
    KEY `index_url_migration` (`url_consultation_externe`(255)),
    KEY `index_denomination_adapte` (`denomination_adapte`),
    KEY `Operations_fk` (`idOperation`),
    KEY `DATE_FIN_UNIX` (`DATE_FIN_UNIX`),
    KEY `consultation_id_regle_validation_idx` (`id_regle_validation`),
    KEY `consultation_id_etat_consultation_idx` (`id_etat_consultation`),
    KEY `consultation_doublon_idx` (`doublon`),
    KEY `Idx_Consultation_type_acces` (`type_acces`),
    KEY `Idx_Consultation_consultation_annulee` (`consultation_annulee`),
    KEY `id_dossier_volumineux_FK` (`id_dossier_volumineux`),
    KEY `reference_organisme` (`reference`, `organisme`),
    KEY `fk_plateforme_virtuelle_consultation_id` (`plateforme_virtuelle_id`),
    KEY `consultation_reference_consultation_init_idx` (`reference_consultation_init`),
    CONSTRAINT `Operations_fk` FOREIGN KEY (`idOperation`) REFERENCES `Operations` (`id_operation`),
    CONSTRAINT `consultation_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_plateforme_virtuelle_consultation_id` FOREIGN KEY (`plateforme_virtuelle_id`) REFERENCES `plateforme_virtuelle` (`id`) ON DELETE NO ACTION,
    CONSTRAINT `id_dossier_volumineux_FK` FOREIGN KEY (`id_dossier_volumineux`) REFERENCES `dossier_volumineux` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;





--
-- Table structure for table `AVIS`
--

--  `AVIS`;


CREATE TABLE `AVIS`
(
    `id`                  int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`           varchar(30)  NOT NULL,
    `consultation_ref`    varchar(256)          DEFAULT NULL,
    `avis`                int(11)      NOT NULL DEFAULT 0,
    `intitule_avis`       int(2)       NOT NULL DEFAULT 0,
    `nom_avis`            varchar(200) NOT NULL,
    `statut`              char(1)      NOT NULL DEFAULT '0',
    `nom_fichier`         varchar(200) NOT NULL,
    `horodatage`          longblob     NOT NULL,
    `untrusteddate`       datetime     NOT NULL DEFAULT '0000-00-00 00:00:00',
    `agent_id`            int(11)      NOT NULL DEFAULT 0,
    `avis_telechargeable` int(11)      NOT NULL DEFAULT 0,
    `url`                 text                  DEFAULT NULL,
    `type`                char(1)               DEFAULT '0',
    `date_creation`       varchar(20)  NOT NULL,
    `date_pub`            varchar(20)           DEFAULT NULL,
    `type_doc_genere`     int(11)      NOT NULL DEFAULT 0,
    `langue`              varchar(10)  NOT NULL,
    `type_avis_pub`       int(5)                DEFAULT NULL COMMENT 'Point vers le champ type_avis de la table Avis_Pub',
    `chemin`              text                  DEFAULT NULL,
    `consultation_id`     int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `AVIS_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_AVIS_consultation` (`consultation_id`),
    CONSTRAINT `FK_AVIS_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `AcheteurPublic`
--

--  `AcheteurPublic`;


CREATE TABLE `AcheteurPublic`
(
    `id`                        int(22)      NOT NULL AUTO_INCREMENT,
    `organisme`                 varchar(30)  NOT NULL DEFAULT '',
    `denomination`              varchar(100) NOT NULL DEFAULT '',
    `prm`                       varchar(100) NOT NULL DEFAULT '',
    `adresse`                   varchar(100) NOT NULL DEFAULT '',
    `cp`                        varchar(5)   NOT NULL DEFAULT '',
    `ville`                     varchar(100) NOT NULL DEFAULT '',
    `dept`                      char(3)      NOT NULL DEFAULT '',
    `type_org`                  char(1)      NOT NULL DEFAULT '',
    `telephone`                 varchar(100) NOT NULL DEFAULT '',
    `fax`                       varchar(100) NOT NULL DEFAULT '',
    `mail`                      varchar(100) NOT NULL DEFAULT '',
    `url`                       varchar(100) NOT NULL DEFAULT '',
    `url_acheteur`              varchar(100)          DEFAULT NULL,
    `facture_numero`            varchar(100) NOT NULL DEFAULT '',
    `facture_code`              varchar(100) NOT NULL DEFAULT '',
    `facture_denomination`      varchar(100) NOT NULL DEFAULT '',
    `facturation_service`       varchar(100)          DEFAULT NULL,
    `facture_adresse`           varchar(256) NOT NULL DEFAULT '',
    `facture_cp`                varchar(10)  NOT NULL DEFAULT '',
    `facture_ville`             varchar(100) NOT NULL DEFAULT '',
    `facture_pays`              varchar(100) NOT NULL DEFAULT '',
    `id_aapc`                   int(22)      NOT NULL DEFAULT 0,
    `boamp_login`               varchar(100) NOT NULL DEFAULT '',
    `boamp_password`            varchar(100) NOT NULL DEFAULT '',
    `boamp_mail`                varchar(100) NOT NULL DEFAULT '',
    `boamp_target`              char(1)      NOT NULL DEFAULT '0',
    `default_form_values`       longtext     NOT NULL,
    `defaut_form_am_boamp`      longtext     NOT NULL,
    `defaut_form_am_boamp_joue` longtext     NOT NULL,
    `defaut_form_mapa_boamp`    longtext     NOT NULL,
    `defaut_form_aconcours`     longtext     NOT NULL,
    `defaut_form_as_boamp_joue` longtext     NOT NULL,
    `defaut_form_aa_boamp`      longtext     NOT NULL,
    `defaut_form_ar_mapa_boamp` longtext     NOT NULL,
    `defaut_form_05_boamp`      longtext     NOT NULL,
    `defaut_form_rect`          longtext     NOT NULL,
    `defaut_form_aa_boamp_joue` longtext     NOT NULL,
    `id_service`                int(11)      NOT NULL DEFAULT 0,
    `livraison_service`         varchar(100)          DEFAULT NULL,
    `livraison_adresse`         varchar(100)          DEFAULT NULL,
    `livraison_code_postal`     varchar(5)            DEFAULT NULL,
    `livraison_ville`           varchar(100)          DEFAULT NULL,
    `livraison_pays`            varchar(100)          DEFAULT NULL,
    `type_pouvoir_activite`     text                  DEFAULT NULL,
    `code_nuts`                 text                  DEFAULT NULL,
    `modalites_financement`     text                  DEFAULT NULL,
    `moniteur_provenance`       int(11)      NOT NULL DEFAULT 90 COMMENT 'Contient la valeur de la valise code_prov dans le fichier xml ''content'' du MOL',
    `code_acces_logiciel`       varchar(256) NOT NULL DEFAULT 'Y8YG-69WD-4421-4G28' COMMENT 'Contient le code BOAMP du logiciel',
    `departement_mise_en_ligne` text                  DEFAULT NULL,
    `reference_commande`        varchar(256) NOT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Administrateur`
--

--  `Administrateur`;


CREATE TABLE `Administrateur`
(
    `id`             int(11)      NOT NULL AUTO_INCREMENT,
    `original_login` varchar(100) NOT NULL DEFAULT '',
    `login`          varchar(100) NOT NULL DEFAULT '',
    `certificat`     text                  DEFAULT NULL,
    `mdp`            varchar(40)  NOT NULL DEFAULT '',
    `nom`            varchar(100) NOT NULL DEFAULT '',
    `prenom`         varchar(100) NOT NULL DEFAULT '',
    `email`          varchar(100) NOT NULL DEFAULT '',
    `organisme`      varchar(30)           DEFAULT NULL,
    `admin_general`  char(1)      NOT NULL DEFAULT '0',
    `tentatives_mdp` int(1)       NOT NULL DEFAULT 0,
    `original_mdp`   varchar(40)           DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `organisme` (`organisme`),
    CONSTRAINT `Administrateur_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Admissibilite_Enveloppe_Lot`
--

--  `Admissibilite_Enveloppe_Lot`;


CREATE TABLE `Admissibilite_Enveloppe_Lot`
(
    `id_Offre`       int(22)     NOT NULL DEFAULT 0,
    `organisme`      varchar(30) NOT NULL DEFAULT '',
    `sous_pli`       int(3)      NOT NULL DEFAULT 0,
    `admissibilite`  int(1)      NOT NULL DEFAULT 0,
    `commentaire`    text                 DEFAULT NULL,
    `type_enveloppe` int(1)      NOT NULL DEFAULT 2,
    PRIMARY KEY (`id_Offre`, `sous_pli`, `type_enveloppe`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Admissibilite_Enveloppe_papier_Lot`
--

--  `Admissibilite_Enveloppe_papier_Lot`;


CREATE TABLE `Admissibilite_Enveloppe_papier_Lot`
(
    `id_offre_papier` int(22)     NOT NULL DEFAULT 0,
    `organisme`       varchar(30) NOT NULL DEFAULT '',
    `sous_pli`        int(3)      NOT NULL DEFAULT 0,
    `admissibilite`   int(1)      NOT NULL DEFAULT 0,
    `commentaire`     text                 DEFAULT NULL,
    `type_enveloppe`  int(1)      NOT NULL DEFAULT 2,
    PRIMARY KEY (`id_offre_papier`, `sous_pli`, `type_enveloppe`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `AdresseFacturationJal`
--

--  `AdresseFacturationJal`;


CREATE TABLE `AdresseFacturationJal`
(
    `id`                      int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`               varchar(30)    NOT NULL,
    `id_service`              int(11)        NOT NULL DEFAULT 0,
    `email_ar`                varchar(100)   NOT NULL,
    `telecopie`               varchar(20)    NOT NULL,
    `information_facturation` text           NOT NULL,
    `facturation_sip`         enum ('0','1') NOT NULL DEFAULT '0',
    `id_blob_logo`            int(11)                 DEFAULT NULL COMMENT 'Permet de stocker l''identifiant du blob correspondant au logo joint à l''adresse de facturation',
    `nom_fichier`             varchar(256)            DEFAULT NULL COMMENT 'Permet de stocker le nom du fichier logo joint à l''adresse de facturation',
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Service`
--

--  `Service`;


CREATE TABLE `Service`
(
    `id`                            int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`                     varchar(30)    NOT NULL DEFAULT '',
    `type_service`                  enum ('1','2') NOT NULL DEFAULT '2',
    `libelle`                       text           NOT NULL,
    `sigle`                         text           NOT NULL,
    `adresse`                       varchar(100)   NOT NULL DEFAULT '',
    `adresse_suite`                 varchar(100)   NOT NULL DEFAULT '',
    `cp`                            varchar(5)     NOT NULL DEFAULT '',
    `ville`                         varchar(100)   NOT NULL DEFAULT '',
    `telephone`                     varchar(100)   NOT NULL DEFAULT '',
    `fax`                           varchar(100)   NOT NULL DEFAULT '',
    `mail`                          varchar(100)   NOT NULL DEFAULT '',
    `pays`                          varchar(150)            DEFAULT NULL,
    `id_externe`                    varchar(50)    NOT NULL DEFAULT '0',
    `date_creation`                 varchar(20)             DEFAULT NULL,
    `date_modification`             varchar(20)             DEFAULT NULL,
    `siren`                         varchar(9)              DEFAULT NULL,
    `complement`                    varchar(5)              DEFAULT NULL,
    `libelle_ar`                    text           NOT NULL,
    `adresse_ar`                    varchar(100)   NOT NULL DEFAULT '',
    `adresse_suite_ar`              varchar(100)   NOT NULL DEFAULT '',
    `ville_ar`                      varchar(100)   NOT NULL DEFAULT '',
    `pays_ar`                       varchar(150)   NOT NULL DEFAULT 'NULL',
    `libelle_fr`                    text           NOT NULL,
    `adresse_fr`                    varchar(100)   NOT NULL DEFAULT '',
    `adresse_suite_fr`              varchar(100)   NOT NULL DEFAULT '',
    `ville_fr`                      varchar(100)   NOT NULL DEFAULT '',
    `pays_fr`                       varchar(150)   NOT NULL DEFAULT 'NULL',
    `libelle_es`                    text           NOT NULL,
    `adresse_es`                    varchar(100)   NOT NULL DEFAULT '',
    `adresse_suite_es`              varchar(100)   NOT NULL DEFAULT '',
    `ville_es`                      varchar(100)   NOT NULL DEFAULT '',
    `pays_es`                       varchar(150)   NOT NULL DEFAULT 'NULL',
    `libelle_en`                    text           NOT NULL,
    `adresse_en`                    varchar(100)   NOT NULL DEFAULT '',
    `adresse_suite_en`              varchar(100)   NOT NULL DEFAULT '',
    `ville_en`                      varchar(100)   NOT NULL DEFAULT '',
    `pays_en`                       varchar(150)   NOT NULL DEFAULT 'NULL',
    `libelle_su`                    text           NOT NULL,
    `adresse_su`                    varchar(100)   NOT NULL DEFAULT '',
    `adresse_suite_su`              varchar(100)   NOT NULL DEFAULT '',
    `ville_su`                      varchar(100)   NOT NULL DEFAULT '',
    `pays_su`                       varchar(150)   NOT NULL DEFAULT 'NULL',
    `libelle_du`                    text           NOT NULL,
    `adresse_du`                    varchar(100)   NOT NULL DEFAULT '',
    `adresse_suite_du`              varchar(100)   NOT NULL DEFAULT '',
    `ville_du`                      varchar(100)   NOT NULL DEFAULT '',
    `pays_du`                       varchar(150)   NOT NULL DEFAULT 'NULL',
    `libelle_cz`                    text           NOT NULL,
    `adresse_cz`                    varchar(100)   NOT NULL DEFAULT '',
    `adresse_suite_cz`              varchar(100)   NOT NULL DEFAULT '',
    `ville_cz`                      varchar(100)   NOT NULL DEFAULT '',
    `pays_cz`                       varchar(150)   NOT NULL DEFAULT '',
    `libelle_it`                    text           NOT NULL,
    `adresse_it`                    varchar(100)   NOT NULL DEFAULT '',
    `adresse_suite_it`              varchar(100)   NOT NULL DEFAULT '',
    `ville_it`                      varchar(100)   NOT NULL DEFAULT '',
    `pays_it`                       varchar(150)   NOT NULL DEFAULT 'NULL',
    `chemin_complet`                varchar(256)   NOT NULL DEFAULT '',
    `chemin_complet_fr`             varchar(256)   NOT NULL DEFAULT '',
    `chemin_complet_en`             varchar(256)   NOT NULL DEFAULT '',
    `chemin_complet_es`             varchar(256)   NOT NULL DEFAULT '',
    `chemin_complet_su`             varchar(256)   NOT NULL DEFAULT '',
    `chemin_complet_du`             varchar(256)   NOT NULL DEFAULT '',
    `chemin_complet_cz`             varchar(256)   NOT NULL DEFAULT '',
    `chemin_complet_ar`             varchar(256)   NOT NULL DEFAULT '',
    `chemin_complet_it`             varchar(256)   NOT NULL DEFAULT '',
    `nom_service_archiveur`         varchar(100)            DEFAULT NULL,
    `identifiant_service_archiveur` varchar(100)            DEFAULT NULL,
    `affichage_service`             enum ('0','1') NOT NULL DEFAULT '1',
    `activation_fuseau_horaire`     enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''utilisation du fuseau horaire pour le service',
    `decalage_horaire`              varchar(5)              DEFAULT NULL COMMENT 'Permet de renseigner le decalage horaire',
    `lieu_residence`                varchar(256)            DEFAULT NULL COMMENT 'Permet de renseigner le lieu de residence du serice',
    `alerte`                        enum ('0','1') NOT NULL DEFAULT '0',
    `acces_chorus`                  enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module Chorus pour le service',
    `forme_juridique`               varchar(100)   NOT NULL,
    `forme_juridique_code`          varchar(4)     NOT NULL,
    `synchronisation_exec`          enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet d''activer la synchronisation avec le module EXEC',
    `id_entite`                     int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1467
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `AffiliationService`
--

--  `AffiliationService`;


CREATE TABLE `AffiliationService`
(
    `organisme`  varchar(30) NOT NULL DEFAULT '',
    `id_pole`    int(11)     NOT NULL DEFAULT 0,
    `id_service` int(11)     NOT NULL DEFAULT 0,
    PRIMARY KEY (`id_service`, `id_pole`, `organisme`),
    KEY `id_service` (`id_service`),
    KEY `id_pole` (`id_pole`),
    KEY `organisme` (`organisme`),
    KEY `AffiliationService_Pole` (`id_pole`, `organisme`),
    KEY `AffiliationService_Service` (`id_service`, `organisme`),
    CONSTRAINT `AffiliationService_Pole` FOREIGN KEY (`id_pole`, `organisme`) REFERENCES `Service` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `AffiliationService_Service` FOREIGN KEY (`id_service`, `organisme`) REFERENCES `Service` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `AffiliationService_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Agent_Commission`
--

--  `Agent_Commission`;


CREATE TABLE `Agent_Commission`
(
    `organisme`          varchar(30) NOT NULL DEFAULT '',
    `id_commission`      int(11)     NOT NULL DEFAULT 0,
    `id_agent`           int(11)     NOT NULL DEFAULT 0,
    `convocation`        longblob             DEFAULT NULL,
    `convoc_send`        char(1)     NOT NULL DEFAULT '0',
    `nom_convoc`         varchar(256)         DEFAULT NULL,
    `contenu_envoi`      text                 DEFAULT NULL,
    `fichier_envoye`     longblob             DEFAULT NULL,
    `nom_fichier_envoye` varchar(256)         DEFAULT NULL,
    `date_envoi`         datetime             DEFAULT NULL,
    `type_voix`          varchar(50) NOT NULL,
    PRIMARY KEY (`id_commission`, `id_agent`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Service_Mertier`
--

--  `Service_Mertier`;


CREATE TABLE `Service_Mertier`
(
    `id`              int(11)      NOT NULL DEFAULT 0,
    `sigle`           varchar(50)           DEFAULT NULL,
    `denomination`    varchar(256)          DEFAULT NULL,
    `url_acces`       text                  DEFAULT NULL,
    `logo`            varchar(100) NOT NULL DEFAULT '',
    `url_deconnexion` text         NOT NULL,
    `ordre`           int(2)       NOT NULL DEFAULT 0 COMMENT 'Ordre d affichage des services metiers dans le socle (1:premier, 2:deuxième, ...)',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `Agent_Service_Metier`
--

--  `Agent_Service_Metier`;


CREATE TABLE `Agent_Service_Metier`
(
    `id_agent`          int(11) NOT NULL DEFAULT 0,
    `id_service_metier` int(11) NOT NULL DEFAULT 0,
    `id_profil_service` int(11) NOT NULL DEFAULT 0,
    `date_creation`     varchar(20)      DEFAULT NULL,
    `date_modification` varchar(20)      DEFAULT NULL,
    PRIMARY KEY (`id_agent`, `id_service_metier`),
    KEY `id_agent` (`id_agent`),
    KEY `id_service_metier` (`id_service_metier`),
    CONSTRAINT `Agent_Service_Metier_ibfk_1` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `Agent_Service_Metier_ibfk_2` FOREIGN KEY (`id_service_metier`) REFERENCES `Service_Mertier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Agrement`
--

--  `Agrement`;


CREATE TABLE `Agrement`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `libelle`    varchar(256) DEFAULT NULL,
    `libelle_ar` varchar(256) DEFAULT NULL,
    `libelle_fr` varchar(256) DEFAULT NULL,
    `libelle_en` varchar(256) DEFAULT NULL,
    `libelle_es` varchar(256) DEFAULT NULL,
    `libelle_su` varchar(256) DEFAULT NULL,
    `libelle_du` varchar(256) DEFAULT NULL,
    `libelle_cz` varchar(256) DEFAULT NULL,
    `libelle_it` varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Alerte`
--

--  `Alerte`;


CREATE TABLE `Alerte`
(
    `id`           int(11)      NOT NULL AUTO_INCREMENT,
    `id_inscrit`   int(11)      NOT NULL DEFAULT 0,
    `denomination` varchar(200) NOT NULL DEFAULT '',
    `periodicite`  char(1)      NOT NULL DEFAULT '',
    `xmlCriteria`  text                  DEFAULT NULL,
    `categorie`    varchar(30)           DEFAULT NULL,
    `id_initial`   int(11)      NOT NULL DEFAULT 0,
    `format`       varchar(3)   NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Annonce`
--

--  `Annonce`;


CREATE TABLE `Annonce`
(
    `id_boamp`         int(11)        NOT NULL DEFAULT 0,
    `organisme`        varchar(100)   NOT NULL DEFAULT '',
    `envoi_boamp`      int(11)        NOT NULL DEFAULT 0,
    `date_envoi`       varchar(20)    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `datepub`          varchar(10)    NOT NULL DEFAULT '0000-00-00',
    `consultation_ref` int(11)                 DEFAULT NULL,
    `nom_fichier_xml`  varchar(256)   NOT NULL DEFAULT '',
    `envoi_joue`       enum ('0','1') NOT NULL DEFAULT '0',
    `mapa`             enum ('0','1') NOT NULL DEFAULT '0',
    `implique_SAD`     enum ('0','1') NOT NULL DEFAULT '0',
    `date_maj`         date           NOT NULL DEFAULT '0000-00-00',
    `consultation_id`  int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id_boamp`, `organisme`),
    KEY `organisme` (`organisme`, `consultation_ref`),
    KEY `Idx_Annonce` (`consultation_ref`),
    KEY `FK_Annonce_consultation` (`consultation_id`),
    CONSTRAINT `FK_Annonce_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `AnnonceBoamp`
--

--  `AnnonceBoamp`;


CREATE TABLE `AnnonceBoamp`
(
    `id_boamp`                int(22)        NOT NULL AUTO_INCREMENT,
    `organisme`               varchar(30)    NOT NULL,
    `consultation_ref`        int(11)        NOT NULL DEFAULT 0,
    `envoi_boamp`             int(22)        NOT NULL DEFAULT 0,
    `date_envoi`              varchar(20)    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `ann_xml`                 longblob       NOT NULL,
    `ann_pdf`                 longblob       NOT NULL,
    `ann_form_values`         longblob       NOT NULL,
    `ann_error`               longtext       NOT NULL,
    `type_boamp`              text           NOT NULL,
    `type_ann`                int(11)        NOT NULL DEFAULT 0,
    `datepub`                 varchar(10)    NOT NULL DEFAULT '0000-00-00',
    `num_ann`                 int(11)        NOT NULL DEFAULT 0,
    `parution`                text           NOT NULL,
    `id_jo`                   varchar(256)   NOT NULL DEFAULT '',
    `erreurs`                 text           NOT NULL,
    `nom_fichier_xml`         varchar(256)   NOT NULL DEFAULT '',
    `envoi_joue`              enum ('0','1') NOT NULL DEFAULT '0',
    `mapa`                    enum ('0','1') NOT NULL DEFAULT '0',
    `implique_SAD`            enum ('0','1') NOT NULL DEFAULT '0',
    `date_maj`                datetime                DEFAULT NULL,
    `id_destination_form_xml` int(11)        NOT NULL DEFAULT 0,
    `id_form_xml`             int(11)        NOT NULL DEFAULT 0,
    `id_type_xml`             int(11)        NOT NULL DEFAULT 0,
    `statut_destinataire`     char(10)       NOT NULL,
    `accuse_reception`        char(1)        NOT NULL DEFAULT '',
    `lien`                    varchar(50)    NOT NULL DEFAULT '',
    `lien_boamp`              text                    DEFAULT NULL,
    `lien_pdf`                text                    DEFAULT NULL,
    `consultation_id`         int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id_boamp`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `id_destination_form_xml` (`id_destination_form_xml`),
    KEY `id_form_xml` (`id_form_xml`),
    KEY `id_type_xml` (`id_type_xml`),
    KEY `Idx_Annonce` (`consultation_ref`),
    KEY `AnnonceBoamp_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_AnnonceBoamp_consultation` (`consultation_id`),
    CONSTRAINT `FK_AnnonceBoamp_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `AnnonceJAL`
--

--  `AnnonceJAL`;


CREATE TABLE `AnnonceJAL`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL,
    `libelle_type`     varchar(250) NOT NULL DEFAULT '',
    `date_creation`    varchar(20)  NOT NULL,
    `objet`            varchar(256)          DEFAULT NULL,
    `texte`            text                  DEFAULT NULL,
    `consultation_ref` int(11)      NOT NULL DEFAULT 0,
    `option_envoi`     int(11)               DEFAULT NULL,
    `consultation_id`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `AnnonceJALPieceJointe`
--

--  `AnnonceJALPieceJointe`;


CREATE TABLE `AnnonceJALPieceJointe`
(
    `id`             int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`      varchar(30)  NOT NULL,
    `id_annonce_jal` int(11)      NOT NULL DEFAULT 0,
    `nom_fichier`    varchar(100) NOT NULL DEFAULT '',
    `piece`          int(11)      NOT NULL DEFAULT 0,
    `horodatage`     longblob     NOT NULL,
    `untrusteddate`  varchar(25)  NOT NULL DEFAULT '',
    `taille`         varchar(25)  NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_annonce_jal` (`id_annonce_jal`),
    CONSTRAINT `AnnonceJALPieceJointe_ibfk_1` FOREIGN KEY (`id_annonce_jal`) REFERENCES `AnnonceJAL` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ReferentielTypeXml`
--

--  `ReferentielTypeXml`;


CREATE TABLE `ReferentielTypeXml`
(
    `id`              int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`       varchar(30)  NOT NULL,
    `id_destinataire` varchar(50)  NOT NULL DEFAULT '',
    `libelle_type`    varchar(100) NOT NULL DEFAULT '',
    `libelle_type_fr` varchar(100) NOT NULL,
    `libelle_type_en` varchar(100) NOT NULL,
    `libelle_type_es` varchar(100) NOT NULL,
    `libelle_type_su` varchar(100) NOT NULL,
    `libelle_type_du` varchar(100) NOT NULL,
    `libelle_type_cz` varchar(100) NOT NULL,
    `libelle_type_ar` varchar(100) NOT NULL,
    `libelle_type_it` varchar(100) NOT NULL,
    `id_avis_marche`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ReferentielDestinationFormXml`
--

--  `ReferentielDestinationFormXml`;


CREATE TABLE `ReferentielDestinationFormXml`
(
    `id`           int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`    varchar(30)  NOT NULL,
    `destinataire` varchar(100) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `ReferentielFormXml`
--

--  `ReferentielFormXml`;


CREATE TABLE `ReferentielFormXml`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL,
    `id_type_xml`      int(11)      NOT NULL DEFAULT 0,
    `consultation_ref` varchar(100) NOT NULL DEFAULT '',
    `xml`              text         NOT NULL,
    `date_creation`    varchar(20)           DEFAULT NULL,
    `statut`           char(1)               DEFAULT NULL,
    `id_compte_boamp`  int(11)      NOT NULL DEFAULT 0,
    `consultation_id`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_type_xml` (`id_type_xml`),
    KEY `consultation_ref` (`consultation_ref`),
    CONSTRAINT `ReferentielFormXml_ibfk_1` FOREIGN KEY (`id_type_xml`) REFERENCES `ReferentielTypeXml` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `AnnonceMoniteur`
--

--  `AnnonceMoniteur`;


CREATE TABLE `AnnonceMoniteur`
(
    `id`                      int(22)                        NOT NULL AUTO_INCREMENT,
    `organisme`               varchar(30)                    NOT NULL,
    `date_envoi`              varchar(20)                    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `type_moniteur`           text                           NOT NULL,
    `statut_xml`              varchar(4)                     NOT NULL DEFAULT '0',
    `date_creation`           date                           NOT NULL DEFAULT '0000-00-00',
    `statut_web`              enum ('0','1','2','3','4','5') NOT NULL DEFAULT '0',
    `statut_arg`              enum ('0','1','2','3','4','5') NOT NULL DEFAULT '0',
    `statut_usn`              enum ('0','1','2','3','4','5') NOT NULL DEFAULT '0',
    `statut_01i`              enum ('0','1','2','3','4','5') NOT NULL DEFAULT '0',
    `datepub`                 varchar(10)                             DEFAULT NULL,
    `num_annonce`             int(11)                        NOT NULL DEFAULT 0,
    `message_error`           text                           NOT NULL,
    `timestamp`               blob                           NOT NULL,
    `nom_timestamp`           varchar(100)                   NOT NULL DEFAULT '',
    `xml_moniteur`            text                           NOT NULL,
    `id_annonce_boamp`        int(22)                        NOT NULL DEFAULT 0,
    `date_maj`                varchar(20)                             DEFAULT '0000-00-00 00:00:00',
    `id_form_xml`             int(11)                        NOT NULL,
    `accuse_reception`        char(1)                        NOT NULL,
    `id_destination_form_xml` int(11)                        NOT NULL,
    `statut_destinataire`     varchar(10)                             DEFAULT NULL,
    `commentaire`             text                                    DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_form_xml` (`id_form_xml`),
    KEY `id_destination_form_xml` (`id_destination_form_xml`),
    CONSTRAINT `AnnonceMoniteur_ibfk_1` FOREIGN KEY (`id_destination_form_xml`) REFERENCES `ReferentielDestinationFormXml` (`id`),
    CONSTRAINT `AnnonceMoniteur_ibfk_2` FOREIGN KEY (`id_form_xml`) REFERENCES `ReferentielFormXml` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Annonce_Press`
--

--  `Annonce_Press`;


CREATE TABLE `Annonce_Press`
(
    `id`                     int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`              varchar(30)  NOT NULL,
    `libelle_type`           varchar(250) NOT NULL DEFAULT '',
    `date_creation`          varchar(20)  NOT NULL,
    `objet`                  varchar(256)          DEFAULT NULL,
    `texte`                  text                  DEFAULT NULL,
    `consultation_ref`       int(11)      NOT NULL DEFAULT 0,
    `option_envoi`           int(11)               DEFAULT NULL,
    `id_Dest_Press`          int(11)               DEFAULT NULL COMMENT 'pointe sur id_dest',
    `id_adresse_facturation` int(11)               DEFAULT NULL,
    `consultation_id`        int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_Dest_Press` (`id_Dest_Press`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Annonce_Press_PieceJointe`
--

--  `Annonce_Press_PieceJointe`;


CREATE TABLE `Annonce_Press_PieceJointe`
(
    `id`               int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)    NOT NULL,
    `id_annonce_press` int(11)        NOT NULL DEFAULT 0,
    `nom_fichier`      varchar(100)   NOT NULL DEFAULT '',
    `piece`            int(11)        NOT NULL DEFAULT 0,
    `horodatage`       longblob       NOT NULL,
    `untrusteddate`    varchar(25)    NOT NULL DEFAULT '',
    `taille`           varchar(25)    NOT NULL DEFAULT '0',
    `fichier_genere`   enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de renseigner si la piece jointe est générée et joint automatiquement ou joint manuellement',
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_annonce_press` (`id_annonce_press`),
    CONSTRAINT `Annonce_Press_PieceJointe_ibfk_1` FOREIGN KEY (`id_annonce_press`) REFERENCES `Annonce_Press` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Articles`
--

--  `Articles`;


CREATE TABLE `Articles`
(
    `id`                int(11) NOT NULL AUTO_INCREMENT,
    `id_type_procedure` int(11) NOT NULL,
    `mode_passation`    varchar(1)   DEFAULT NULL,
    `libelle`           varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Autres_Pieces_Mise_Disposition`
--

--  `Autres_Pieces_Mise_Disposition`;


CREATE TABLE `Autres_Pieces_Mise_Disposition`
(
    `id`                    int(11)     NOT NULL AUTO_INCREMENT,
    `id_decision_enveloppe` int(11)     NOT NULL,
    `org`                   varchar(30) NOT NULL,
    `id_destinataire`       int(11)     NOT NULL,
    `id_blob`               int(11)     NOT NULL,
    `nom_fichier`           varchar(250)         DEFAULT NULL,
    `taille_fichier`        varchar(10)          DEFAULT NULL,
    `date_creation`         varchar(20)          DEFAULT NULL,
    `horodatage`            longblob    NOT NULL,
    `untrusteddate`         varchar(20) NOT NULL DEFAULT '0000-00-00 00:00:00',
    `agent_id`              int(11)     NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Contrat`
--

--  `Contrat`;


CREATE TABLE `Contrat`
(
    `id_contrat`                  int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`                   varchar(30) NOT NULL,
    `id_marche`                   int(11)     NOT NULL DEFAULT 0,
    `id_decision`                 int(11)     NOT NULL DEFAULT 0,
    `informaions_complementaires` text                 DEFAULT NULL,
    PRIMARY KEY (`id_contrat`, `organisme`),
    KEY `id_marche` (`id_marche`),
    KEY `id_decision` (`id_decision`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Avenant`
--

--  `Avenant`;


CREATE TABLE `Avenant`
(
    `id_avenant`                                       int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`                                        varchar(30) NOT NULL,
    `id_contrat`                                       int(11)     NOT NULL,
    `numero_avenant`                                   varchar(20)  DEFAULT NULL,
    `type_avenant`                                     int(5)       DEFAULT NULL,
    `objet_avenant`                                    varchar(100) DEFAULT NULL,
    `montant_avenant_ht`                               varchar(30)  DEFAULT NULL,
    `montant_avenant_ttc`                              varchar(30)  DEFAULT NULL,
    `pourcentage_augmentation_marche_initial`          char(5)      DEFAULT NULL,
    `pourcentage_augmentation_cumule`                  char(5)      DEFAULT NULL,
    `montant_total_marche_tout_avenant_cumule`         varchar(30)  DEFAULT NULL,
    `date_reception_projet_par_secretaire_cao`         varchar(10)  DEFAULT NULL,
    `date_reception_projet_par_charge_etude`           varchar(10)  DEFAULT NULL,
    `date_observation_par_sv`                          varchar(10)  DEFAULT NULL,
    `date_retour_projet`                               varchar(10)  DEFAULT NULL,
    `date_validation_projet`                           varchar(10)  DEFAULT NULL,
    `date_validation_projet_vu_par`                    int(5)       DEFAULT NULL,
    `date_cao`                                         varchar(10)  DEFAULT NULL,
    `avis_cao`                                         int(5)       DEFAULT NULL,
    `date_cp`                                          varchar(10)  DEFAULT NULL,
    `date_signature_avenant`                           varchar(10)  DEFAULT NULL,
    `date_reception_dossier`                           varchar(10)  DEFAULT NULL,
    `date_formulation_observation_par_sv_sur_sdossier` varchar(10)  DEFAULT NULL,
    `date_retour_dossier_finalise`                     varchar(10)  DEFAULT NULL,
    `date_validation_dossier_finalise`                 varchar(10) NOT NULL,
    `date_transmission_prefecture`                     varchar(10)  DEFAULT NULL,
    `date_notification`                                varchar(10)  DEFAULT NULL,
    `operation_validation_vues_par`                    int(5)       DEFAULT NULL,
    `commentaires`                                     text         DEFAULT NULL,
    PRIMARY KEY (`id_avenant`, `organisme`),
    KEY `id_contrat` (`id_contrat`),
    CONSTRAINT `Avenant_ibfk_1` FOREIGN KEY (`id_contrat`) REFERENCES `Contrat` (`id_contrat`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `AvisCao`
--

--  `AvisCao`;


CREATE TABLE `AvisCao`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `libelle`    varchar(100) DEFAULT NULL,
    `libelle_fr` varchar(100) DEFAULT NULL,
    `libelle_en` varchar(100) DEFAULT NULL,
    `libelle_es` varchar(100) DEFAULT NULL,
    `libelle_su` varchar(100) DEFAULT NULL,
    `libelle_du` varchar(100) DEFAULT NULL,
    `libelle_cz` varchar(100) DEFAULT NULL,
    `libelle_ar` varchar(100) DEFAULT NULL,
    `libelle_it` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Avis_Membres_CAO`
--

--  `Avis_Membres_CAO`;


CREATE TABLE `Avis_Membres_CAO`
(
    `id`                 int(11)      NOT NULL AUTO_INCREMENT,
    `id_agent`           int(11)      NOT NULL,
    `id_enveloppe`       int(22)      NOT NULL,
    `nom_agent`          varchar(100) NOT NULL,
    `prenom_agent`       varchar(100) NOT NULL,
    `date_action`        varchar(20)  NOT NULL,
    `admissibilite`      int(1)       NOT NULL DEFAULT 0,
    `commentaire`        text                  DEFAULT NULL,
    `sous_pli`           int(3)       NOT NULL DEFAULT 0,
    `type_depot_reponse` int(1)       NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Avis_Pub`
--

--  `Avis_Pub`;


CREATE TABLE `Avis_Pub`
(
    `id`                      int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`               varchar(30) NOT NULL,
    `consultation_ref`        int(11)              DEFAULT NULL,
    `statut`                  char(1)     NOT NULL DEFAULT '0',
    `id_agent`                int(11)     NOT NULL,
    `id_agent_validateur`     int(10)              DEFAULT NULL COMMENT 'Permet de stocker l id de l agent validateur de l avis',
    `id_agent_validateur_eco` int(10)              DEFAULT NULL COMMENT 'Permet de stocker l id de l agent validateur de l avis par l entitée coordinatrice',
    `id_agent_validateur_sip` int(10)              DEFAULT NULL COMMENT 'Permet de stocker l identifiant de l agent validateur SIP',
    `date_creation`           varchar(20) NOT NULL,
    `date_envoi`              varchar(20)          DEFAULT NULL,
    `type_avis`               int(11)     NOT NULL,
    `date_publication`        varchar(20)          DEFAULT NULL,
    `date_validation`         varchar(20)          DEFAULT NULL,
    `Sip`                     varchar(5)           DEFAULT NULL COMMENT 'Permet de préciser si l avis est éligible SIP ou non',
    `id_avis_portail`         int(5)               DEFAULT NULL COMMENT 'Permet de stocker l identifiant de l avis joint au portail',
    `id_avis_presse`          int(5)               DEFAULT NULL COMMENT 'Permet de stocker l id de l avis envoyée à envoyer à la presse',
    `id_avis_pdf_opoce`       int(5)               DEFAULT NULL COMMENT 'Permet de stocker l identifiant de l avis pdf recuperé depuis Opoce via le web service',
    `id_blob_logo`            int(11)              DEFAULT NULL COMMENT 'Permet de stocker l id du blob du logo à joindre au mail à envoyer à la presse',
    `consultation_id`         int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `Avis_Pub_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_AvisPub_consultation` (`consultation_id`),
    CONSTRAINT `FK_AvisPub_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `CG76_Domaine`
--

--  `CG76_Domaine`;


CREATE TABLE `CG76_Domaine`
(
    `Id`      int(11)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(150) NOT NULL,
    `Parent`  smallint(5)  NOT NULL,
    PRIMARY KEY (`Id`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `CG76_Donnee_Complementaire_Domaine`
--

--  `CG76_Donnee_Complementaire_Domaine`;


CREATE TABLE `CG76_Donnee_Complementaire_Domaine`
(
    `Id`                    int(11) NOT NULL AUTO_INCREMENT,
    `IdDonneeComlementaire` int(11) NOT NULL,
    `IdDomaine`             int(11) NOT NULL,
    PRIMARY KEY (`Id`),
    KEY `id_domaine` (`IdDomaine`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `CG76_Donnee_Complementaire_entreprise`
--

--  `CG76_Donnee_Complementaire_entreprise`;


CREATE TABLE `CG76_Donnee_Complementaire_entreprise`
(
    `Ref`                  smallint(6) NOT NULL AUTO_INCREMENT,
    `idInscrit`            smallint(5) NOT NULL DEFAULT 0,
    `IdEntreprise`         int(11)     NOT NULL DEFAULT 0,
    `type_formation`       varchar(100)         DEFAULT NULL,
    `email`                varchar(100)         DEFAULT NULL,
    `cout_moyen_journee`   varchar(10)          DEFAULT NULL,
    `collaboration_fpt`    int(1)      NOT NULL DEFAULT 0,
    `collaboration_fpe`    int(1)      NOT NULL DEFAULT 0,
    `centre_documentation` int(1)      NOT NULL DEFAULT 0,
    `service_reprographie` int(1)      NOT NULL DEFAULT 0,
    `salle_info`           int(1)      NOT NULL DEFAULT 0,
    `salle_cours`          int(1)      NOT NULL DEFAULT 0,
    `aire_geo_inter`       varchar(256)         DEFAULT NULL,
    `commentaire`          text                 DEFAULT NULL,
    PRIMARY KEY (`Ref`),
    KEY `idInscrit` (`idInscrit`),
    KEY `Entreprise` (`IdEntreprise`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `CG76_PieceJointe`
--

--  `CG76_PieceJointe`;


CREATE TABLE `CG76_PieceJointe`
(
    `Ref`          int(11) NOT NULL AUTO_INCREMENT,
    `IdPJ`         int(11) NOT NULL DEFAULT 0,
    `idEntreprise` int(11) NOT NULL DEFAULT 0,
    PRIMARY KEY (`Ref`),
    KEY `Entreprise` (`idEntreprise`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `CategorieConsultation`
--

--  `CategorieConsultation`;


CREATE TABLE `CategorieConsultation`
(
    `id`               varchar(30)  NOT NULL DEFAULT '',
    `libelle`          varchar(100) NOT NULL DEFAULT '',
    `libelle_fr`       varchar(100)          DEFAULT '',
    `libelle_en`       varchar(100)          DEFAULT '',
    `libelle_es`       varchar(100)          DEFAULT '',
    `libelle_su`       varchar(100)          DEFAULT '',
    `libelle_du`       varchar(100)          DEFAULT '',
    `libelle_cz`       varchar(100)          DEFAULT '',
    `libelle_ar`       varchar(100)          DEFAULT NULL,
    `id_categorie_ANM` varchar(11)  NOT NULL DEFAULT '0',
    `libelle_it`       varchar(100)          DEFAULT NULL,
    `code`             varchar(10)           DEFAULT NULL,
    `code_dume`        varchar(50)           DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `CategorieLot`
--

--  `CategorieLot`;


CREATE TABLE `CategorieLot`
(
    `id`                                 int(11)            NOT NULL AUTO_INCREMENT,
    `organisme`                          varchar(30)        NOT NULL DEFAULT '',
    `consultation_ref`                   int(11)                     DEFAULT NULL,
    `lot`                                int(11)            NOT NULL DEFAULT 0,
    `description`                        varchar(1000)      NOT NULL DEFAULT '',
    `id_tr_description`                  int(11)                     DEFAULT NULL COMMENT 'Identifiant de la traduction du champ "description"',
    `categorie`                          varchar(30)        NOT NULL DEFAULT '',
    `description_detail`                 varchar(1000)      NOT NULL DEFAULT '',
    `id_tr_description_detail`           int(11)                     DEFAULT NULL COMMENT 'Identifiant de la traduction du champ "description_detail"',
    `code_cpv_1`                         varchar(8)                  DEFAULT NULL,
    `code_cpv_2`                         varchar(256)                DEFAULT NULL,
    `description_fr`                     varchar(1000)      NOT NULL DEFAULT '',
    `description_en`                     varchar(256)       NOT NULL DEFAULT '',
    `description_es`                     varchar(256)       NOT NULL DEFAULT '',
    `description_su`                     varchar(256)       NOT NULL DEFAULT '',
    `description_du`                     varchar(256)       NOT NULL DEFAULT '',
    `description_cz`                     varchar(256)       NOT NULL DEFAULT '',
    `description_ar`                     longtext                    DEFAULT NULL,
    `description_detail_fr`              varchar(1000)      NOT NULL DEFAULT '',
    `description_detail_en`              varchar(256)                DEFAULT NULL,
    `description_detail_es`              varchar(256)                DEFAULT NULL,
    `description_detail_su`              varchar(256)                DEFAULT NULL,
    `description_detail_du`              varchar(256)                DEFAULT NULL,
    `description_detail_cz`              varchar(256)                DEFAULT NULL,
    `description_detail_ar`              longtext                    DEFAULT NULL,
    `id_lot_externe`                     int(11)                     DEFAULT NULL,
    `caution_provisoire`                 varchar(256)                DEFAULT NULL,
    `qualification`                      varchar(256)                DEFAULT NULL,
    `agrements`                          varchar(256)                DEFAULT NULL,
    `add_echantillion`                   varchar(256)                DEFAULT NULL,
    `date_limite_echantillion`           varchar(50)                 DEFAULT NULL,
    `add_reunion`                        varchar(256)                DEFAULT NULL,
    `date_reunion`                       varchar(50)                 DEFAULT NULL,
    `variantes`                          char(1)                     DEFAULT NULL,
    `echantillon`                        enum ('0','1')     NOT NULL DEFAULT '0',
    `reunion`                            enum ('0','1')     NOT NULL DEFAULT '0',
    `visites_lieux`                      enum ('0','1')     NOT NULL DEFAULT '0',
    `add_echantillion_fr`                varchar(256)                DEFAULT NULL,
    `add_echantillion_en`                varchar(256)                DEFAULT NULL,
    `add_echantillion_es`                varchar(256)                DEFAULT NULL,
    `add_echantillion_su`                varchar(256)                DEFAULT NULL,
    `add_echantillion_du`                varchar(256)                DEFAULT NULL,
    `add_echantillion_cz`                varchar(256)                DEFAULT NULL,
    `add_echantillion_ar`                varchar(256)                DEFAULT NULL,
    `add_reunion_fr`                     varchar(256)                DEFAULT NULL,
    `add_reunion_en`                     varchar(256)                DEFAULT NULL,
    `add_reunion_es`                     varchar(256)                DEFAULT NULL,
    `add_reunion_su`                     varchar(256)                DEFAULT NULL,
    `add_reunion_du`                     varchar(256)                DEFAULT NULL,
    `add_reunion_cz`                     varchar(256)                DEFAULT NULL,
    `add_reunion_ar`                     varchar(256)                DEFAULT NULL,
    `description_detail_it`              longtext                    DEFAULT NULL,
    `description_it`                     longtext                    DEFAULT NULL,
    `add_echantillion_it`                varchar(256)                DEFAULT NULL,
    `add_reunion_it`                     varchar(256)                DEFAULT NULL,
    `clause_sociale`                     enum ('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non',
    `clause_environnementale`            enum ('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non',
    `decision`                           enum ('0','1')     NOT NULL DEFAULT '0',
    `clause_sociale_condition_execution` varchar(255)                DEFAULT '0' COMMENT 'Le marché comprend des clauses sociales d insertion comme condition d exécution (article 14 du Code des marchés publics)',
    `clause_sociale_insertion`           varchar(255)                DEFAULT '0' COMMENT 'Présence parmi les critères d''attribution d''un critère relatif à l insertion (article 53 du code des marchés publics)',
    `clause_sociale_ateliers_proteges`   varchar(255)                DEFAULT '0' COMMENT 'Le marché est réservé à des ateliers protégés (article 15 du code des marchés publics)',
    `clause_sociale_siae`                varchar(255)                DEFAULT '0' COMMENT 'Le marché est réservé à SIAE',
    `clause_sociale_ess`                 varchar(255)                DEFAULT '0' COMMENT 'Le marché est réservé à ESS',
    `clause_env_specs_techniques`        varchar(255)                DEFAULT '0' COMMENT 'pécifications techniques (article 6 du code des marchés publics)',
    `clause_env_cond_execution`          varchar(255)                DEFAULT '0' COMMENT 'Conditions d''exécution (article 14 du code des marchés publics)',
    `clause_env_criteres_select`         varchar(255)                DEFAULT '0' COMMENT 'Critère de sélection (article 53.1 du code des marchés publics)',
    `id_donnee_complementaire`           int(11)                     DEFAULT NULL,
    `marche_insertion`                   tinyint(1)                  DEFAULT 0,
    `clause_specification_technique`     varchar(256)                DEFAULT '0',
    `consultation_id`                    int(11)                     DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_categorielot` (`consultation_id`, `lot`),
    KEY `organisme` (`organisme`, `consultation_ref`),
    KEY `Idx_CategorieLot` (`consultation_ref`),
    CONSTRAINT `FK_CategorieLot_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Centrale_publication`
--

--  `Centrale_publication`;


CREATE TABLE `Centrale_publication`
(
    `id`          int(20)     NOT NULL AUTO_INCREMENT,
    `organisme`   varchar(30) NOT NULL,
    `nom`         varchar(200) DEFAULT NULL,
    `mail`        varchar(200) DEFAULT NULL,
    `fax`         varchar(50)  DEFAULT NULL,
    `information` text         DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `CertificatChiffrement`
--

--  `CertificatChiffrement`;


CREATE TABLE `CertificatChiffrement`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `consultation_ref` int(11)              DEFAULT 0,
    `organisme`        varchar(30) NOT NULL DEFAULT '',
    `type_env`         int(1)      NOT NULL DEFAULT 0,
    `sous_pli`         int(3)      NOT NULL DEFAULT 0,
    `index_certificat` int(3)      NOT NULL DEFAULT 1,
    `certificat`       mediumtext  NOT NULL,
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `certificatchiffrement` (`consultation_id`, `type_env`, `sous_pli`, `index_certificat`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `CertificatPermanent`
--

--  `CertificatPermanent`;


CREATE TABLE `CertificatPermanent`
(
    `id`                     int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`              varchar(30)    NOT NULL DEFAULT '',
    `Titre`                  varchar(100)   NOT NULL DEFAULT '',
    `Prenom`                 varchar(100)   NOT NULL DEFAULT '',
    `Nom`                    varchar(100)   NOT NULL DEFAULT '',
    `EMail`                  varchar(100)   NOT NULL DEFAULT '',
    `Certificat`             mediumtext     NOT NULL,
    `service_id`             int(11)        NOT NULL DEFAULT -1,
    `master_key`             enum ('0','1') NOT NULL DEFAULT '0',
    `id_agent`               int(11)                 DEFAULT 0,
    `CSP`                    varchar(200)            DEFAULT '',
    `date_modification`      varchar(200)            DEFAULT '',
    `certificat_universelle` enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Certificat_Agent`
--

--  `Certificat_Agent`;


CREATE TABLE `Certificat_Agent`
(
    `id`               int(50)      NOT NULL AUTO_INCREMENT,
    `certificat`       text         NOT NULL,
    `date_debut`       varchar(20)  NOT NULL DEFAULT '',
    `date_fin`         varchar(20)  NOT NULL DEFAULT '',
    `id_agent`         int(20)               DEFAULT NULL,
    `nom_agent`        varchar(200) NOT NULL DEFAULT '',
    `prenom_agent`     varchar(200) NOT NULL DEFAULT '',
    `id_service`       int(20)               DEFAULT NULL,
    `statut_revoque`   varchar(20)  NOT NULL DEFAULT '',
    `date_revoquation` varchar(20)  NOT NULL DEFAULT '',
    `mail_agent`       varchar(20)  NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    KEY `id_service` (`id_service`),
    KEY `id_agent` (`id_agent`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Certificats_Entreprises`
--

--  `Certificats_Entreprises`;


CREATE TABLE `Certificats_Entreprises`
(
    `id`               int(50) NOT NULL AUTO_INCREMENT,
    `certificat`       text         DEFAULT NULL,
    `date_debut`       varchar(20)  DEFAULT NULL,
    `date_fin`         varchar(20)  DEFAULT NULL,
    `id_inscrit`       int(20)      DEFAULT NULL,
    `nom_inscrit`      varchar(200) DEFAULT NULL,
    `prenom_inscrit`   varchar(200) DEFAULT NULL,
    `id_entreprise`    int(20)      DEFAULT NULL,
    `statut_revoque`   varchar(20)  DEFAULT NULL,
    `date_revoquation` varchar(20)  DEFAULT NULL,
    `mail_inscrit`     varchar(20)  DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `id_entreprise` (`id_entreprise`),
    KEY `id_inscrit` (`id_inscrit`),
    CONSTRAINT `Certificats_Entreprises_ibfk_2` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


--
-- Table structure for table `Chorus_Code_Calcul_Interets`
--

--  `Chorus_Code_Calcul_Interets`;


CREATE TABLE `Chorus_Code_Calcul_Interets`
(
    `id`      int(50)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(200) NOT NULL,
    `code`    varchar(50)  NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_Fiche_Navette`
--

--  `Chorus_Fiche_Navette`;


CREATE TABLE `Chorus_Fiche_Navette`
(
    `id`                              int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`                       varchar(10)  NOT NULL,
    `id_chorus_echange`               varchar(256) NOT NULL DEFAULT '0' COMMENT 'Pointe vers la table Chorus_echange',
    `rpa`                             varchar(256)          DEFAULT NULL COMMENT 'Représentant du pouvoir adjudicateur',
    `objet`                           text                  DEFAULT NULL COMMENT 'Objet du marché',
    `fiche_immobilisation`            enum ('0','1')        DEFAULT NULL COMMENT 'Fiche immobilisation',
    `attente_pj`                      enum ('0','1')        DEFAULT NULL COMMENT 'En attente de PJ',
    `marche_partage`                  enum ('0','1')        DEFAULT NULL COMMENT 'Marché partagé',
    `id_accord_cadre`                 varchar(256)          DEFAULT NULL COMMENT 'Identifiant accord cadre',
    `lib_accord_cadre`                varchar(256)          DEFAULT NULL COMMENT 'Libellé accord cadre',
    `annee_creation_accord_cadre`     varchar(256)          DEFAULT NULL COMMENT 'Année de création de l''accord',
    `montant_ht`                      varchar(256)          DEFAULT NULL COMMENT 'Montant HT',
    `reconductible`                   enum ('0','1')        DEFAULT NULL COMMENT 'Reconductible',
    `visa_accf`                       enum ('0','1')        DEFAULT NULL COMMENT 'Visa ACCF',
    `visa_prefet`                     enum ('0','1')        DEFAULT NULL COMMENT 'Visa préfet',
    `retenue_garantie`                enum ('0','1')        DEFAULT NULL COMMENT 'Retenue de garantie',
    `montant_mini`                    varchar(256)          DEFAULT NULL COMMENT 'Montant mini',
    `montant_maxi`                    varchar(256)          DEFAULT NULL COMMENT 'Montant maxi',
    `montant_forfaitaire`             varchar(256)          DEFAULT NULL COMMENT 'Montant partie forfaitaire',
    `montant_bc`                      varchar(256)          DEFAULT NULL COMMENT 'Montant partie à BC',
    `taux_tva`                        varchar(256)          DEFAULT NULL COMMENT 'Taux TVA',
    `centre_cout`                     varchar(256)          DEFAULT NULL COMMENT 'Centre de coût (si unique)',
    `centre_financier`                varchar(256)          DEFAULT NULL COMMENT 'Centre financier (si unique',
    `activite`                        varchar(256)          DEFAULT NULL COMMENT 'Activité (si unique)',
    `domaine_fonctionnel`             varchar(256)          DEFAULT NULL COMMENT 'Domaine fonctionnel (si unique)',
    `fond`                            varchar(256)          DEFAULT NULL COMMENT 'Fond (si unique)',
    `localisation_interministerielle` varchar(256)          DEFAULT NULL COMMENT 'Localisation interministérielle (si unique)',
    `nature`                          varchar(256)          DEFAULT NULL COMMENT 'Nature (si unique)',
    `axe_ministeriel1`                varchar(256)          DEFAULT NULL COMMENT 'Axe ministériel 1 (si unique)',
    `projet_analytique`               varchar(256)          DEFAULT NULL COMMENT 'Projet analytique (si unique)',
    `localisation_ministerielle`      varchar(256)          DEFAULT NULL COMMENT 'Localisation ministérielle (si unique)',
    `axe_ministeriel2`                varchar(256)          DEFAULT NULL COMMENT 'Axe ministériel 2 (si unique)',
    `remarques`                       text                  DEFAULT NULL COMMENT 'Remarques',
    `id_document`                     int(11)               DEFAULT NULL COMMENT 'Permet de stocker l''identifiant du blob du document de la fiche navette',
    `nom_document`                    varchar(256)          DEFAULT NULL COMMENT 'Le nom du document de la fiche de navette',
    `nom_acheteur`                    varchar(256)          DEFAULT NULL COMMENT 'Le nom de l''acheteur à ''origine de l''envoi',
    `intitule_marche`                 varchar(256)          DEFAULT NULL COMMENT 'L''intitulé du contrat',
    `raison_sociale`                  text                  DEFAULT NULL COMMENT 'La raison sociale du titulaire du contrat ',
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_chorus_echange` (`id_chorus_echange`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_Regroupement_Comptable`
--

--  `Chorus_Regroupement_Comptable`;


CREATE TABLE `Chorus_Regroupement_Comptable`
(
    `id`        int(11)     NOT NULL AUTO_INCREMENT,
    `organisme` varchar(20) NOT NULL,
    `libelle`   varchar(256) DEFAULT NULL,
    `code`      varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_Type_Groupement`
--

--  `Chorus_Type_Groupement`;


CREATE TABLE `Chorus_Type_Groupement`
(
    `id`        int(11)     NOT NULL AUTO_INCREMENT,
    `organisme` varchar(20) NOT NULL,
    `libelle`   varchar(256) DEFAULT NULL,
    `code`      varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_acte_juridique`
--

--  `Chorus_acte_juridique`;


CREATE TABLE `Chorus_acte_juridique`
(
    `id`      int(10)      NOT NULL DEFAULT 0,
    `libelle` varchar(200) NOT NULL DEFAULT '',
    `code`    varchar(50)           DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_conditions_paiements`
--

--  `Chorus_conditions_paiements`;


CREATE TABLE `Chorus_conditions_paiements`
(
    `id`      int(50)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(200) NOT NULL,
    `code`    varchar(50)  NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_echange`
--

--  `Chorus_echange`;


CREATE TABLE `Chorus_echange`
(
    `id`                               int(20)        NOT NULL AUTO_INCREMENT,
    `organisme`                        varchar(30)    NOT NULL DEFAULT '',
    `id_decision`                      int(20)                 DEFAULT NULL,
    `num_ordre`                        varchar(20)             DEFAULT NULL,
    `nom_createur`                     varchar(200)            DEFAULT NULL,
    `prenom_createur`                  varchar(200)            DEFAULT NULL,
    `id_createur`                      int(20)                 DEFAULT NULL,
    `statutEchange`                    varchar(20)             DEFAULT NULL,
    `date_creation`                    varchar(20)             DEFAULT NULL,
    `date_envoi`                       varchar(20)             DEFAULT NULL,
    `retour_chorus`                    varchar(20)             DEFAULT NULL,
    `id_ej_appli_ext`                  varchar(20)    NOT NULL DEFAULT '',
    `id_oa`                            int(20)                 DEFAULT NULL,
    `id_ga`                            int(20)                 DEFAULT NULL,
    `id_type_marche`                   int(20)                 DEFAULT NULL,
    `id_type_groupement`               varchar(256)            DEFAULT NULL COMMENT 'Type de groupement',
    `id_regroupement_comptable`        varchar(256)            DEFAULT NULL COMMENT 'Regroupement comptable',
    `dce_items`                        varchar(200)            DEFAULT NULL,
    `dume_acheteur_items`              varchar(256)            DEFAULT NULL,
    `dume_oe_items`                    varchar(256)            DEFAULT NULL,
    `ids_env_ae`                       varchar(200)            DEFAULT NULL,
    `ids_env_items`                    varchar(200)            DEFAULT NULL,
    `ids_pieces_externes`              varchar(200)            DEFAULT NULL,
    `id_agent_envoi`                   int(200)                DEFAULT NULL,
    `nom_agent`                        varchar(200)            DEFAULT NULL,
    `prenom_agent`                     varchar(200)            DEFAULT NULL,
    `signACE`                          enum ('0','1') NOT NULL DEFAULT '0',
    `siren`                            varchar(9)              DEFAULT NULL,
    `siret`                            varchar(5)              DEFAULT NULL,
    `date_notification`                varchar(20)             DEFAULT NULL,
    `date_fin_marche`                  varchar(20)             DEFAULT NULL,
    `id_acte_juridique`                char(1)                 DEFAULT NULL,
    `cpv_1`                            varchar(20)             DEFAULT NULL,
    `cpv_2`                            text                    DEFAULT NULL,
    `cpv_3`                            varchar(20)             DEFAULT NULL,
    `cpv_4`                            varchar(20)             DEFAULT NULL,
    `id_type_procedure`                int(20)                 DEFAULT NULL,
    `id_forme_prix`                    char(1)                 DEFAULT NULL,
    `Nbr_entreprises_cotraitantes`     varchar(20)             DEFAULT NULL,
    `sous_traitance_declaree`          char(1)                 DEFAULT NULL,
    `carte_achat`                      char(1)                 DEFAULT NULL,
    `clause_sociale`                   char(1)                 DEFAULT NULL,
    `clause_environnementale`          char(1)                 DEFAULT NULL,
    `Nbr_proposition_recues`           varchar(20)             DEFAULT NULL,
    `Nbr_proposition_dematerialisees`  varchar(20)             DEFAULT NULL,
    `nom_fichier`                      varchar(200)            DEFAULT NULL,
    `erreur_rejet`                     text                    DEFAULT NULL,
    `code_cpv_libelle1`                varchar(250)            DEFAULT NULL,
    `code_cpv_libelle2`                varchar(250)            DEFAULT NULL,
    `code_cpv_libelle3`                varchar(250)            DEFAULT NULL,
    `code_cpv_libelle4`                varchar(250)            DEFAULT NULL,
    `pieces_notif_items`               varchar(200)            DEFAULT NULL COMMENT 'Permet de stocker les items des pieces de notification',
    `ids_blob_env`                     varchar(225)   NOT NULL DEFAULT '' COMMENT 'Permet de stocker les idBLob des fichiers enveloppes reponses',
    `ids_env_sign_items`               varchar(256)            DEFAULT NULL COMMENT 'Permet de stocker les identifiants des blocs des fichiers reponses signatures (pour les autres fichiers hors actes d''engagement)',
    `ids_blob_sign_env`                varchar(256)            DEFAULT NULL COMMENT 'Permet de stocker les identifiants blob des fichiers de signatures',
    `montant_ht`                       varchar(256)            DEFAULT NULL COMMENT 'Permet de stocker le montant hors taxe',
    `code_pays_titulaire`              varchar(256)            DEFAULT NULL COMMENT 'Code pays titulaire',
    `numero_siret_titulaire`           varchar(256)            DEFAULT NULL COMMENT 'numéro de siret titulaire ou mandataire',
    `numero_siren_titulaire`           varchar(256)            DEFAULT NULL COMMENT 'Numero de siren titulaire',
    `Codes_pays_co_titulaire`          varchar(256)            DEFAULT NULL COMMENT 'Codes pays co-titulaires',
    `numero_siret_co_titulaire`        varchar(256)            DEFAULT NULL COMMENT 'Numéro de siret des co-titulaires',
    `numero_siren_co_titulaire`        varchar(256)            DEFAULT NULL COMMENT 'Numero de siren titulaire des co-titulaires',
    `CCAG_reference`                   varchar(256)            DEFAULT NULL COMMENT 'CCAG de rattachement',
    `pourcentage_avance`               varchar(256)            DEFAULT NULL COMMENT 'Pourcentage de l''avance',
    `type_avance`                      varchar(256)            DEFAULT NULL COMMENT 'Type d''avance',
    `conditions_paiement`              varchar(256)            DEFAULT NULL COMMENT 'Conditions de paiement',
    `identifiant_accord_cadre`         varchar(256)            DEFAULT NULL COMMENT 'Identifiant de l''accord cadre',
    `date_notification_reelle`         varchar(20)             DEFAULT NULL,
    `date_fin_marche_reelle`           varchar(20)             DEFAULT NULL,
    `ids_rapport_signature`            varchar(256)            DEFAULT NULL,
    `type_contrat`                     int(10)                 DEFAULT NULL COMMENT 'Type de contrat',
    `intitule_contrat`                 varchar(256)            DEFAULT NULL COMMENT 'Intitulé du contrat',
    `objet_contrat`                    varchar(256)            DEFAULT NULL COMMENT 'Objet du contrat',
    `identifiant_accord_cadre_chapeau` varchar(256)            DEFAULT NULL COMMENT 'Identifiant de l accord cadre chapeau',
    `type_flux`                        int(2)                  DEFAULT NULL COMMENT 'Types de flux Chorus (1: FEN111, 2:FEN211)',
    `type_envoi`                       int(2)         NOT NULL DEFAULT 0 COMMENT '0=>valeur par defaut, 1=> type d''envoi pieces jointes, 2=>types d envoi acte modificatif',
    `tmp_file_name`                    varchar(256)            DEFAULT NULL COMMENT 'Chemin du fichier temporaire de stockage du flux apres la generation',
    `type_flux_a_envoyer`              int(10)                 DEFAULT NULL COMMENT '1=>FEN111, 2=>FEN211, 3=>FIR',
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_decision` (`id_decision`),
    KEY `ChorusEchange_decisionEnveloppe` (`id_decision`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_forme_prix`
--

--  `Chorus_forme_prix`;


CREATE TABLE `Chorus_forme_prix`
(
    `id`      int(20)      NOT NULL DEFAULT 0,
    `libelle` varchar(200) NOT NULL DEFAULT '',
    `code`    varchar(50)           DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_groupement_achat`
--

--  `Chorus_groupement_achat`;


CREATE TABLE `Chorus_groupement_achat`
(
    `id`         int(20)      NOT NULL AUTO_INCREMENT,
    `organisme`  varchar(10)  NOT NULL,
    `id_oa`      int(20)      NOT NULL DEFAULT 0,
    `libelle`    varchar(250) NOT NULL DEFAULT '',
    `code`       varchar(50)  NOT NULL DEFAULT '',
    `actif`      int(11)      NOT NULL DEFAULT 1,
    `id_service` int(11)      NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`, `organisme`, `id_oa`, `id_service`),
    KEY `id_oa` (`id_oa`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_numero_sequence`
--

--  `Chorus_numero_sequence`;


CREATE TABLE `Chorus_numero_sequence`
(
    `id`           int(50)     NOT NULL AUTO_INCREMENT,
    `numero`       varchar(20) NOT NULL,
    `type_fichier` varchar(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_organisation_achat`
--

--  `Chorus_organisation_achat`;


CREATE TABLE `Chorus_organisation_achat`
(
    `id`          int(20)        NOT NULL AUTO_INCREMENT,
    `organisme`   varchar(10)    NOT NULL,
    `libelle`     varchar(250)   NOT NULL DEFAULT '',
    `code`        varchar(50)    NOT NULL DEFAULT '',
    `actif`       int(11)        NOT NULL DEFAULT 1,
    `flux_fen111` enum ('0','1') NOT NULL DEFAULT '1',
    `flux_fen211` enum ('0','1') NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`, `organisme`),
    KEY `id` (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_pj`
--

--  `Chorus_pj`;


CREATE TABLE `Chorus_pj`
(
    `id`            int(20)      NOT NULL AUTO_INCREMENT,
    `organisme`     varchar(30)  NOT NULL DEFAULT '',
    `id_echange`    int(20)      NOT NULL DEFAULT 0,
    `nom_fichier`   varchar(300) NOT NULL DEFAULT '',
    `fichier`       varchar(20)  NOT NULL DEFAULT '',
    `horodatage`    longblob     NOT NULL,
    `untrusteddate` varchar(20)  NOT NULL DEFAULT '',
    `taille`        int(20)      NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_echange` (`id_echange`),
    KEY `Chorus_pj_Chorus_echange` (`id_echange`, `organisme`),
    CONSTRAINT `Chorus_pj_Chorus_echange` FOREIGN KEY (`id_echange`, `organisme`) REFERENCES `Chorus_echange` (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_type_marche`
--

--  `Chorus_type_marche`;


CREATE TABLE `Chorus_type_marche`
(
    `id`      int(20)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(250) NOT NULL DEFAULT '',
    `code`    varchar(50)  NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Chorus_type_procedure`
--

--  `Chorus_type_procedure`;


CREATE TABLE `Chorus_type_procedure`
(
    `id`      int(50)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(200) NOT NULL DEFAULT '',
    `code`    varchar(50)  NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Complement`
--

--  `Complement`;


CREATE TABLE `Complement`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL DEFAULT '',
    `consultation_ref` int(11)     NOT NULL DEFAULT 0,
    `complement`       int(11)     NOT NULL DEFAULT 0,
    `nom_fichier`      varchar(80) NOT NULL DEFAULT '',
    `statut`           char(1)     NOT NULL DEFAULT '1',
    `horodatage`       longblob    NOT NULL,
    `untrusteddate`    varchar(20) NOT NULL DEFAULT '0000-00-00 00:00:00',
    `agent_id`         int(1)      NOT NULL DEFAULT 0,
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `Complement_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_Complement_consultation` (`consultation_id`),
    CONSTRAINT `FK_Complement_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `CompteMoniteur`
--

--  `CompteMoniteur`;


CREATE TABLE `CompteMoniteur`
(
    `id`                int(22)      NOT NULL AUTO_INCREMENT,
    `organisme`         varchar(30)  NOT NULL,
    `moniteur_login`    varchar(100) NOT NULL DEFAULT '',
    `moniteur_password` varchar(100) NOT NULL DEFAULT '',
    `moniteur_mail`     varchar(100) NOT NULL DEFAULT '',
    `moniteur_target`   char(1)      NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ConsultationFormulaire`
--

--  `ConsultationFormulaire`;


CREATE TABLE `ConsultationFormulaire`
(
    `id`                int(11)      NOT NULL AUTO_INCREMENT,
    `reference`         varchar(256) NOT NULL,
    `nom`               varchar(30)  NOT NULL,
    `type_formulaire`   int(1)       NOT NULL,
    `id_type_procedure` int(1)       NOT NULL,
    `id_categorie`      int(10)      NOT NULL,
    `code_cpv_1`        varchar(8)            DEFAULT NULL,
    `code_cpv_2`        varchar(8)            DEFAULT NULL,
    `organisme`         varchar(30)           DEFAULT NULL,
    `service_id`        int(11)               DEFAULT NULL,
    `date_creation`     varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `date_modification` varchar(20)           DEFAULT '0000-00-00 00:00:00',
    `consultation_ref`  int(11)               DEFAULT NULL,
    `type_enveloppe`    int(11)      NOT NULL,
    `lot`               int(11)      NOT NULL,
    `id_modele`         int(11)      NOT NULL DEFAULT 0,
    `statut`            int(11)      NOT NULL DEFAULT 1,
    `consultation_id`   int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ConsultationHistoriqueEtat`
--

--  `ConsultationHistoriqueEtat`;


CREATE TABLE `ConsultationHistoriqueEtat`
(
    `id`                int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`         varchar(30) NOT NULL DEFAULT '',
    `id_agent`          int(11)     NOT NULL DEFAULT 0,
    `consultation_ref`  int(11)     NOT NULL DEFAULT 0,
    `id_etat`           int(11)     NOT NULL DEFAULT 0,
    `date_modification` varchar(20) NOT NULL DEFAULT '0000-00-00 00:00:00',
    `consultation_id`   int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Contact_Entreprise`
--

--  `Contact_Entreprise`;


CREATE TABLE `Contact_Entreprise`
(
    `id`            int(11)      NOT NULL AUTO_INCREMENT,
    `id_entreprise` int(11)      NOT NULL,
    `nom`           varchar(100) NOT NULL,
    `prenom`        varchar(100) NOT NULL,
    `adresse`       varchar(100) NOT NULL,
    `adresse_suite` varchar(100) NOT NULL,
    `codepostal`    varchar(5)   NOT NULL,
    `email`         varchar(100) NOT NULL,
    `ville`         varchar(50)  NOT NULL,
    `fonction`      varchar(50)  NOT NULL,
    `telephone`     varchar(50) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `id_entreprise` (`id_entreprise`),
    CONSTRAINT `Contact_Entreprise_Entreprise` FOREIGN KEY (`id_entreprise`) REFERENCES `Entreprise` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;





--
-- Table structure for table `Criteres_Evaluation`
--

--  `Criteres_Evaluation`;


CREATE TABLE `Criteres_Evaluation`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL,
    `consultation_ref` int(11)              DEFAULT NULL,
    `type_enveloppe`   int(11)     NOT NULL,
    `lot`              int(11)     NOT NULL,
    `Type_Critere`     int(11)     NOT NULL,
    `statut`           int(11)     NOT NULL DEFAULT 1,
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `DATEFIN`
--

--  `DATEFIN`;


CREATE TABLE `DATEFIN`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL DEFAULT '',
    `consultation_ref` int(11)     NOT NULL DEFAULT 0,
    `datefin`          datetime    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `statut`           char(1)     NOT NULL DEFAULT '1',
    `horodatage`       longblob    NOT NULL,
    `untrusteddate`    datetime    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `agent_id`         int(11)     NOT NULL DEFAULT 0,
    `dateFinLocale`    varchar(20) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Permet de stocker la date remise des plis heure locale',
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `DATEFIN_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_DATEFIN_consultation` (`consultation_id`),
    CONSTRAINT `FK_DATEFIN_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `DCE`
--

--  `DCE`;


CREATE TABLE `DCE`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL DEFAULT '',
    `consultation_ref` int(11)      NOT NULL DEFAULT 0,
    `dce`              int(11)      NOT NULL DEFAULT 0,
    `nom_dce`          varchar(150) NOT NULL,
    `statut`           char(1)      NOT NULL DEFAULT '1',
    `nom_fichier`      varchar(80)  NOT NULL DEFAULT '',
    `ancien_fichier`   varchar(80)  NOT NULL DEFAULT '',
    `horodatage`       longblob     NOT NULL,
    `untrusteddate`    datetime     NOT NULL DEFAULT '0000-00-00 00:00:00',
    `agent_id`         int(11)      NOT NULL DEFAULT 0,
    `taille_dce`       varchar(256)          DEFAULT NULL,
    `consultation_id`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `organisme` (`organisme`),
    KEY `DCE_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_DCE_consultation` (`consultation_id`),
    CONSTRAINT `FK_DCE_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `DateLancementCron`
--

--  `DateLancementCron`;


CREATE TABLE `DateLancementCron`
(
    `id`                int(11)     NOT NULL AUTO_INCREMENT,
    `mise_a_jour_socle` varchar(20) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='cette table enregistre la derniere date de lancement de cron';


--
-- Table structure for table `DecisionLot`
--

--  `DecisionLot`;


CREATE TABLE `DecisionLot`
(
    `id_decision_lot`  int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL DEFAULT '',
    `consultation_ref` int(11)     NOT NULL DEFAULT 0,
    `lot`              int(11)     NOT NULL DEFAULT 0,
    `id_type_decision` int(11)     NOT NULL DEFAULT 0,
    `autre_a_preciser` varchar(100)         DEFAULT NULL,
    `date_decision`    date                 DEFAULT NULL,
    `commentaire`      text                 DEFAULT NULL,
    `date_maj`         varchar(20)          DEFAULT NULL,
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id_decision_lot`, `organisme`),
    KEY `ref_Consultation` (`consultation_ref`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `DecisionPassationConsultation`
--

--  `DecisionPassationConsultation`;


CREATE TABLE `DecisionPassationConsultation`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `libelle`    varchar(100) DEFAULT NULL,
    `libelle_fr` varchar(100) DEFAULT NULL,
    `libelle_en` varchar(100) DEFAULT NULL,
    `libelle_es` varchar(100) DEFAULT NULL,
    `libelle_su` varchar(100) DEFAULT NULL,
    `libelle_du` varchar(100) DEFAULT NULL,
    `libelle_cz` varchar(100) DEFAULT NULL,
    `libelle_ar` varchar(100) DEFAULT NULL,
    `libelle_it` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `DecisionPassationMarcheAVenir`
--

--  `DecisionPassationMarcheAVenir`;


CREATE TABLE `DecisionPassationMarcheAVenir`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `libelle`    varchar(100) DEFAULT NULL,
    `libelle_fr` varchar(100) DEFAULT NULL,
    `libelle_en` varchar(100) DEFAULT NULL,
    `libelle_es` varchar(100) DEFAULT NULL,
    `libelle_su` varchar(100) DEFAULT NULL,
    `libelle_du` varchar(100) DEFAULT NULL,
    `libelle_cz` varchar(100) DEFAULT NULL,
    `libelle_ar` varchar(100) DEFAULT NULL,
    `libelle_it` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Departement`
--

--  `Departement`;


CREATE TABLE `Departement`
(
    `id_departement`  int(11)      NOT NULL AUTO_INCREMENT,
    `nom_departement` varchar(100) NOT NULL DEFAULT '',
    PRIMARY KEY (`id_departement`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `JAL`
--

--  `JAL`;


CREATE TABLE `JAL`
(
    `id`                      int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`               varchar(30)  NOT NULL,
    `id_service`              int(11)      NOT NULL DEFAULT 0,
    `nom`                     varchar(100) NOT NULL DEFAULT '',
    `email`                   varchar(100) NOT NULL DEFAULT '',
    `email_ar`                varchar(100) NOT NULL DEFAULT '',
    `telecopie`               varchar(20)  NOT NULL DEFAULT '',
    `information_facturation` text         NOT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `DestinataireAnnonceJAL`
--

--  `DestinataireAnnonceJAL`;


CREATE TABLE `DestinataireAnnonceJAL`
(
    `id`           int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`    varchar(30) NOT NULL,
    `idJAL`        int(11)     NOT NULL DEFAULT 0,
    `idAnnonceJAL` int(11)     NOT NULL DEFAULT 0,
    `date_envoi`   varchar(14) NOT NULL DEFAULT '',
    `date_pub`     varchar(14) NOT NULL DEFAULT '',
    `statut`       char(1)     NOT NULL DEFAULT '',
    `accuse`       char(1)     NOT NULL DEFAULT '',
    `id_echange`   int(11)              DEFAULT NULL,
    `date_ar`      varchar(25)          DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idJAL` (`idJAL`),
    KEY `idAnnonceJAL` (`idAnnonceJAL`),
    CONSTRAINT `destinataireAnnonceJAL_annonceJal_id` FOREIGN KEY (`idAnnonceJAL`) REFERENCES `AnnonceJAL` (`id`),
    CONSTRAINT `destinataireAnnonceJAL_jal_id` FOREIGN KEY (`idJAL`) REFERENCES `JAL` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Destinataire_Annonce_Press`
--

--  `Destinataire_Annonce_Press`;


CREATE TABLE `Destinataire_Annonce_Press`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL,
    `id_annonce_press` int(11)     NOT NULL DEFAULT 0,
    `id_jal`           int(11)     NOT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_annonce_press` (`id_annonce_press`),
    KEY `id_jal` (`id_jal`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Destinataire_Pub`
--

--  `Destinataire_Pub`;


CREATE TABLE `Destinataire_Pub`
(
    `id`                int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`         varchar(30) NOT NULL,
    `id_avis`           int(11)     NOT NULL,
    `id_support`        int(11)     NOT NULL,
    `etat`              char(3)     NOT NULL DEFAULT '0',
    `date_modification` varchar(20) NOT NULL,
    `date_publication`  varchar(20)          DEFAULT NULL,
    `id_dispositif`     int(11)     NOT NULL DEFAULT 0,
    `id_dossier`        int(11)     NOT NULL DEFAULT 0,
    `type_pub`          int(11)     NOT NULL DEFAULT 0 COMMENT '0 : Esender, 1: SUB, 2: TED ',
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_avis` (`id_avis`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `blobOrganisme_file`
--

--  `blobOrganisme_file`;


CREATE TABLE `blobOrganisme_file`
(
    `old_id`            int(11)              DEFAULT NULL,
    `organisme`         varchar(30) NOT NULL DEFAULT '',
    `name`              text        NOT NULL,
    `deletion_datetime` datetime             DEFAULT NULL,
    `statut_synchro`    int(11)     NOT NULL DEFAULT 0 COMMENT '0 : non vérifié / 1 : Vérification ok / 2 : fichier absent',
    `hash`              varchar(256)         DEFAULT 'ND' COMMENT 'Empreinte du blob, SHA1 du fichier / ND : Valeur par défaut pour non définit',
    `dossier`           varchar(256)         DEFAULT NULL COMMENT 'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.',
    `chemin`            text                 DEFAULT NULL,
    `extension`         varchar(50) NOT NULL DEFAULT '' COMMENT 'extension du fichier après -0 (.dce, .brouillon, .rc ...)',
    `id`                int(11)     NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`),
    KEY `blob_organisme_file_statut_synchro` (`statut_synchro`),
    KEY `blob_organisme_file_old_id` (`old_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 28130
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `DocumentExterne`
--

--  `DocumentExterne`;


CREATE TABLE `DocumentExterne`
(
    `id`              int(11) unsigned NOT NULL AUTO_INCREMENT,
    `refConsultation` int(11)                   DEFAULT NULL,
    `organisme`       varchar(30)      NOT NULL DEFAULT '',
    `idBlob`          int(11)                   DEFAULT NULL,
    `idEntreprise`    int(11)                   DEFAULT NULL,
    `type`            int(11)                   DEFAULT NULL,
    `nom`             varchar(256)     NOT NULL DEFAULT '',
    `description`     longtext         NOT NULL,
    `date`            date             NOT NULL DEFAULT '0000-00-00',
    `lot`             int(11)          NOT NULL DEFAULT 0,
    `consultation_id` int(11)                   DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idBlob` (`idBlob`),
    KEY `refConsultation` (`refConsultation`),
    KEY `DOC_EX_consultation` (`organisme`, `refConsultation`),
    KEY `FK_DOC_EX_consultation` (`consultation_id`),
    CONSTRAINT `DOC_EX_Blob` FOREIGN KEY (`idBlob`) REFERENCES `blobOrganisme_file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_DOC_EX_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `DocumentsAttaches`
--

--  `DocumentsAttaches`;


CREATE TABLE `DocumentsAttaches`
(
    `id_document`     int(11)      NOT NULL DEFAULT 0,
    `nom_document`    varchar(100) NOT NULL DEFAULT '',
    `nom_document_fr` varchar(100) NOT NULL,
    `nom_document_es` varchar(100) NOT NULL,
    `nom_document_en` varchar(100) NOT NULL,
    `nom_document_su` varchar(100) NOT NULL,
    `nom_document_du` varchar(100) NOT NULL,
    `nom_document_cz` varchar(100) NOT NULL,
    `nom_document_ar` varchar(100) NOT NULL,
    `nom_document_it` varchar(100) NOT NULL,
    PRIMARY KEY (`id_document`),
    KEY `id_document` (`id_document`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Echange`
--

--  `Echange`;


CREATE TABLE `Echange`
(
    `id`                            int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`                     varchar(30)  NOT NULL,
    `objet`                         varchar(256) NOT NULL DEFAULT '',
    `corps`                         text         NOT NULL,
    `expediteur`                    varchar(100) NOT NULL DEFAULT '',
    `id_createur`                   int(11)               DEFAULT 0,
    `ref_consultation`              int(11)      NOT NULL DEFAULT 0,
    `option_envoi`                  int(11)      NOT NULL DEFAULT 0,
    `date_message`                  varchar(25)  NOT NULL DEFAULT '',
    `format`                        int(11)      NOT NULL DEFAULT 0,
    `id_action_declencheur`         int(11)      NOT NULL DEFAULT 0,
    `status`                        int(2)       NOT NULL DEFAULT 0,
    `service_id`                    int(11)               DEFAULT 0,
    `email_expediteur`              varchar(100) NOT NULL DEFAULT '',
    `id_type_message`               int(11)      NOT NULL,
    `destinataires_retraits`        longtext              DEFAULT NULL,
    `destinataires_questions`       longtext              DEFAULT NULL,
    `destinataires_depots`          longtext              DEFAULT NULL,
    `destinataires_bd_fournisseurs` longtext              DEFAULT NULL,
    `destinataires_libres`          longtext              DEFAULT NULL,
    `page_source`                   varchar(256)          DEFAULT NULL,
    `destinataires`                 longtext              DEFAULT NULL,
    `id_dossier_volumineux`         int(11)               DEFAULT NULL,
    `consultation_id`               int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `echange_organisme_idx` (`organisme`),
    KEY `echange_ref_consultation_idx` (`ref_consultation`),
    KEY `echange_email_expediteur_idx` (`email_expediteur`),
    KEY `echange_dossier_volumineux_fk` (`id_dossier_volumineux`),
    CONSTRAINT `echange_dossier_volumineux_fk` FOREIGN KEY (`id_dossier_volumineux`) REFERENCES `dossier_volumineux` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EchangeDestinataire`
--

--  `EchangeDestinataire`;


CREATE TABLE `EchangeDestinataire`
(
    `id`                int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`         varchar(30)    NOT NULL,
    `id_echange`        int(11)        NOT NULL DEFAULT 0,
    `mail_destinataire` varchar(256)   NOT NULL DEFAULT '',
    `ar`                enum ('0','1') NOT NULL DEFAULT '0',
    `date_ar`           varchar(25)    NOT NULL DEFAULT '',
    `uid`               varchar(32)    NOT NULL DEFAULT '',
    `type_ar`           int(11)        NOT NULL DEFAULT 0,
    `id_inscrit`        int(11)        NOT NULL DEFAULT 0,
    `id_entreprise`     int(11)        NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_echange` (`id_echange`),
    KEY `echangeDestinataire_organisme_idx` (`organisme`),
    KEY `echangeDestinataire_mail_destinataire_idx` (`mail_destinataire`),
    KEY `echangeDestinataire_uid_idx` (`uid`),
    CONSTRAINT `EchangeDestinataire_ibfk_1` FOREIGN KEY (`id_echange`) REFERENCES `Echange` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EchangeFormat`
--

--  `EchangeFormat`;


CREATE TABLE `EchangeFormat`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `libelle`    varchar(100) NOT NULL DEFAULT '',
    `libelle_fr` varchar(100) NOT NULL,
    `libelle_en` varchar(100) NOT NULL,
    `libelle_es` varchar(100) NOT NULL,
    `libelle_su` varchar(100) NOT NULL,
    `libelle_du` varchar(100) NOT NULL,
    `libelle_cz` varchar(100) NOT NULL,
    `libelle_ar` varchar(100) NOT NULL,
    `libelle_it` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EchangePieceJointe`
--

--  `EchangePieceJointe`;


CREATE TABLE `EchangePieceJointe`
(
    `id`            int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`     varchar(30)  NOT NULL,
    `id_message`    int(11)      NOT NULL DEFAULT 0,
    `nom_fichier`   varchar(256) NOT NULL DEFAULT '',
    `extension`     varchar(100) NOT NULL DEFAULT '' COMMENT 'Extension du fichier',
    `piece`         int(11)      NOT NULL DEFAULT 0,
    `horodatage`    longblob     NOT NULL,
    `untrusteddate` varchar(25)  NOT NULL DEFAULT '',
    `taille`        varchar(25)  NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_message` (`id_message`),
    CONSTRAINT `EchangePieceJointe_ibfk_1` FOREIGN KEY (`id_message`) REFERENCES `Echange` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EchangeTypeAR`
--

--  `EchangeTypeAR`;


CREATE TABLE `EchangeTypeAR`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `libelle`    varchar(100) NOT NULL DEFAULT '',
    `libelle_fr` varchar(100) NOT NULL,
    `libelle_en` varchar(100) NOT NULL,
    `libelle_es` varchar(100) NOT NULL,
    `libelle_su` varchar(100) NOT NULL,
    `libelle_du` varchar(100) NOT NULL,
    `libelle_cz` varchar(100) NOT NULL,
    `libelle_ar` varchar(100) NOT NULL,
    `libelle_it` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EchangeTypeMessage`
--

--  `EchangeTypeMessage`;


CREATE TABLE `EchangeTypeMessage`
(
    `id`    int(11)      NOT NULL AUTO_INCREMENT,
    `code`  varchar(100) NOT NULL COMMENT 'Les contenus de ce champ sont en correspondance avec messages.xml',
    `corps` text         NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnchereEntreprisePmi`
--

--  `EnchereEntreprisePmi`;


CREATE TABLE `EnchereEntreprisePmi`
(
    `id`               int(10) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)      NOT NULL DEFAULT '',
    `idEnchere`        int(10) unsigned NOT NULL DEFAULT 0,
    `nom`              varchar(256)     NOT NULL DEFAULT '',
    `numeroAnonyme`    int(11) unsigned          DEFAULT NULL,
    `email`            varchar(256)     NOT NULL DEFAULT '',
    `mdp`              varchar(256)     NOT NULL DEFAULT '',
    `noteTechnique`    double                    DEFAULT NULL,
    `idEntreprise`     int(10) unsigned          DEFAULT NULL,
    `datePing`         datetime         NOT NULL DEFAULT '0000-00-00 00:00:00',
    `nomAgentConnecte` varchar(256)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `EnchereEntreprisePmi_EncherePmi_IdTech` (`idEnchere`, `organisme`),
    CONSTRAINT `EnchereEntreprisePmi_EncherePmi_IdTech` FOREIGN KEY (`idEnchere`, `organisme`) REFERENCES `EncherePmi` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `EncherePmi`
--

--  `EncherePmi`;


CREATE TABLE `EncherePmi`
(
    `id`                          int(10) unsigned       NOT NULL AUTO_INCREMENT,
    `organisme`                   varchar(30)            NOT NULL DEFAULT '',
    `refConsultation`             int(10) unsigned                DEFAULT NULL,
    `idEntiteeAssociee`           int(11)                         DEFAULT NULL,
    `referenceUtilisateur`        varchar(45)                     DEFAULT NULL,
    `auteur`                      varchar(256)                    DEFAULT NULL,
    `idLot`                       int(10) unsigned                DEFAULT NULL,
    `objet`                       longtext                        DEFAULT NULL,
    `dateDebut`                   datetime                        DEFAULT '0000-00-00 00:00:00',
    `dateFin`                     datetime                        DEFAULT '0000-00-00 00:00:00',
    `dateSuspension`              datetime                        DEFAULT '0000-00-00 00:00:00',
    `delaiProlongation`           int(11)                         DEFAULT NULL,
    `commentaire`                 longtext                        DEFAULT NULL,
    `meilleureEnchereObligatoire` enum ('0','1')         NOT NULL DEFAULT '0',
    `typeBaremeNETC`              enum ('1','2','3','4') NOT NULL DEFAULT '1',
    `typeBaremeEnchereGlobale`    enum ('1','2')         NOT NULL DEFAULT '1',
    `meilleurNoteHaute`           enum ('0','1')         NOT NULL DEFAULT '0',
    `nbrCandidatsVisible`         enum ('0','1')         NOT NULL DEFAULT '1',
    `listeCandidatsVisible`       enum ('0','1')         NOT NULL DEFAULT '1',
    `rangVisible`                 enum ('0','1')         NOT NULL DEFAULT '1',
    `meilleureOffreVisible`       enum ('0','1')         NOT NULL DEFAULT '1',
    `montantReserve`              double                          DEFAULT NULL,
    `noteMaxBaremeRelatif`        double                          DEFAULT NULL,
    `coeffA`                      double                          DEFAULT NULL,
    `coeffB`                      double                          DEFAULT NULL,
    `coeffC`                      double                          DEFAULT NULL,
    `mail`                        longtext                        DEFAULT NULL,
    `consultation_id`             int(11)                         DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    UNIQUE KEY `referenceUtilisateur` (`referenceUtilisateur`),
    KEY `refConsultation` (`refConsultation`),
    KEY `idEntiteeAssociee` (`idEntiteeAssociee`),
    CONSTRAINT `EncherePmi_ibfk_1` FOREIGN KEY (`idEntiteeAssociee`) REFERENCES `Service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnchereOffre`
--

--  `EnchereOffre`;


CREATE TABLE `EnchereOffre`
(
    `id`                  int(11)          NOT NULL AUTO_INCREMENT,
    `organisme`           varchar(30)      NOT NULL DEFAULT '',
    `idEnchere`           int(10) unsigned NOT NULL DEFAULT 0,
    `idEnchereEntreprise` int(10) unsigned NOT NULL DEFAULT 0,
    `date`                datetime                  DEFAULT '0000-00-00 00:00:00',
    `valeurTIC`           double                    DEFAULT NULL,
    `valeurTC`            double                    DEFAULT NULL,
    `valeurNETC`          double                    DEFAULT NULL,
    `valeurNGC`           double                    DEFAULT NULL,
    `rang`                int(11)                   DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idEnchereEntreprise` (`idEnchereEntreprise`),
    KEY `idEnchere` (`idEnchere`),
    KEY `EnchereOffre_EnchereEntreprisePmi` (`idEnchereEntreprise`, `organisme`),
    KEY `EnchereOffre_EncherePmi` (`idEnchere`, `organisme`),
    CONSTRAINT `EnchereOffre_EnchereEntreprisePmi` FOREIGN KEY (`idEnchereEntreprise`, `organisme`) REFERENCES `EnchereEntreprisePmi` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `EnchereOffre_EncherePmi` FOREIGN KEY (`idEnchere`, `organisme`) REFERENCES `EncherePmi` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `EnchereOffre_idEnchere` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnchereReference`
--

--  `EnchereReference`;


CREATE TABLE `EnchereReference`
(
    `id`                       int(10) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`                varchar(30)      NOT NULL DEFAULT '',
    `idEnchere`                int(10) unsigned NOT NULL DEFAULT 0,
    `libelle`                  varchar(256)     NOT NULL DEFAULT '',
    `quantite`                 double                    DEFAULT NULL,
    `isMontant`                enum ('0','1')   NOT NULL DEFAULT '1',
    `unite`                    varchar(20)               DEFAULT NULL,
    `pasMin`                   double                    DEFAULT 0,
    `pasMax`                   double                    DEFAULT NULL,
    `valeurReference`          double                    DEFAULT NULL,
    `valeurDepartCommune`      enum ('0','1')   NOT NULL DEFAULT '1',
    `valeurDepart`             double                    DEFAULT NULL,
    `typeBaremeReference`      enum ('1','2','3')        DEFAULT NULL,
    `ponderationNoteReference` double                    DEFAULT 1,
    `noteMaxBaremeRelatif`     double                    DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idEnchere` (`idEnchere`),
    KEY `EnchereReference_EncherePmi_idEnchere_org` (`idEnchere`, `organisme`),
    CONSTRAINT `EnchereReference_EncherePmi_idEnchere` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `EnchereReference_EncherePmi_idEnchere_org` FOREIGN KEY (`idEnchere`, `organisme`) REFERENCES `EncherePmi` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnchereOffreReference`
--

--  `EnchereOffreReference`;


CREATE TABLE `EnchereOffreReference`
(
    `id`                 int(10) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`          varchar(30)      NOT NULL DEFAULT '',
    `idEnchereOffre`     int(11)          NOT NULL DEFAULT 0,
    `idEnchereReference` int(10) unsigned NOT NULL DEFAULT 0,
    `valeur`             double           NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idEnchereOffre` (`idEnchereOffre`),
    KEY `idEnchereReference` (`idEnchereReference`),
    KEY `EnchereOffreReference_EnchereOffre` (`idEnchereOffre`, `organisme`),
    KEY `EnchereOffreReference_EnchereReference` (`idEnchereReference`, `organisme`),
    CONSTRAINT `EnchereOffreReference_EnchereOffre` FOREIGN KEY (`idEnchereOffre`, `organisme`) REFERENCES `EnchereOffre` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `EnchereOffreReference_EnchereOffre_idEnchereOffre` FOREIGN KEY (`idEnchereOffre`) REFERENCES `EnchereOffre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `EnchereOffreReference_EnchereReference` FOREIGN KEY (`idEnchereReference`, `organisme`) REFERENCES `EnchereReference` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `EnchereOffreReference_EnchereReference_idEnchereReference` FOREIGN KEY (`idEnchereReference`) REFERENCES `EnchereReference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `EnchereTrancheBaremeReference`
--

--  `EnchereTrancheBaremeReference`;


CREATE TABLE `EnchereTrancheBaremeReference`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`   varchar(30)      NOT NULL DEFAULT '',
    `idReference` int(10) unsigned NOT NULL DEFAULT 0,
    `borneInf`    double                    DEFAULT NULL,
    `borneSup`    double                    DEFAULT NULL,
    `note`        double                    DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idReference` (`idReference`),
    KEY `EnchereTrancheBaremeReference_EnchereReference_idRef_org` (`idReference`, `organisme`),
    CONSTRAINT `EnchereTrancheBaremeReference_EnchereReference_idRef` FOREIGN KEY (`idReference`) REFERENCES `EnchereReference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `EnchereTrancheBaremeReference_EnchereReference_idRef_org` FOREIGN KEY (`idReference`, `organisme`) REFERENCES `EnchereReference` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnchereTranchesBaremeNETC`
--

--  `EnchereTranchesBaremeNETC`;


CREATE TABLE `EnchereTranchesBaremeNETC`
(
    `id`        int(10) unsigned NOT NULL AUTO_INCREMENT,
    `organisme` varchar(30)      NOT NULL DEFAULT '',
    `idEnchere` int(10) unsigned NOT NULL DEFAULT 0,
    `borneInf`  double                    DEFAULT NULL,
    `borneSup`  double                    DEFAULT NULL,
    `note`      double                    DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idEnchere` (`idEnchere`),
    KEY `EnchereTranchesBaremeNETC_EncherePmi` (`idEnchere`, `organisme`),
    CONSTRAINT `EnchereTranchesBaremeNETC_EncherePmi` FOREIGN KEY (`idEnchere`, `organisme`) REFERENCES `EncherePmi` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `EnchereTranchesBaremeNETC_EncherePmi_idReference` FOREIGN KEY (`idEnchere`) REFERENCES `EncherePmi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnchereValeursInitiales`
--

--  `EnchereValeursInitiales`;


CREATE TABLE `EnchereValeursInitiales`
(
    `id`                  int(10) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`           varchar(30)      NOT NULL DEFAULT '',
    `idEnchereEntreprise` int(10) unsigned NOT NULL DEFAULT 0,
    `idEnchereReference`  int(10) unsigned NOT NULL DEFAULT 0,
    `valeur`              double                    DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `EnchereValeursInitiales_EnchereEntreprisePmi` (`idEnchereEntreprise`, `organisme`),
    KEY `EnchereValeursInitiales_EnchereReference` (`idEnchereReference`, `organisme`),
    CONSTRAINT `EnchereValeursInitiales_EnchereEntreprisePmi` FOREIGN KEY (`idEnchereEntreprise`, `organisme`) REFERENCES `EnchereEntreprisePmi` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `EnchereValeursInitiales_EnchereReference` FOREIGN KEY (`idEnchereReference`, `organisme`) REFERENCES `EnchereReference` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `EntrepriseInsee`
--

--  `EntrepriseInsee`;


CREATE TABLE `EntrepriseInsee`
(
    `id`                int(30) NOT NULL AUTO_INCREMENT,
    `siren`             varchar(9)   DEFAULT NULL,
    `raisonSociale`     varchar(200) DEFAULT NULL,
    `nbEtA`             int(1)       DEFAULT NULL,
    `etat`              char(1)      DEFAULT NULL,
    `etatLib`           varchar(40)  DEFAULT NULL,
    `etatDebDate`       varchar(20)  DEFAULT NULL,
    `eff3112Tr`         int(11)      DEFAULT NULL,
    `eff3112TrLib`      varchar(200) DEFAULT NULL,
    `effAn`             varchar(10)  DEFAULT NULL,
    `apen`              varchar(5)   DEFAULT NULL,
    `apenLib`           varchar(200) DEFAULT NULL,
    `cj`                varchar(4)   DEFAULT NULL,
    `cjLib`             varchar(200) DEFAULT NULL,
    `indNDC`            char(1)      DEFAULT NULL,
    `indDoublon`        char(1)      DEFAULT NULL,
    `indPurge`          char(1)      DEFAULT NULL,
    `nicSiege`          varchar(5)   DEFAULT '00000',
    `etatSiege`         char(1)      DEFAULT NULL,
    `etatSiegeLib`      varchar(10)  DEFAULT NULL,
    `etatDebDateSiege`  varchar(20)  DEFAULT NULL,
    `eff3112TrSiege`    int(11)      DEFAULT NULL,
    `eff3112TrSiegeLib` varchar(200) DEFAULT NULL,
    `effAnSiege`        varchar(10)  DEFAULT NULL,
    `apetSiege`         varchar(5)   DEFAULT NULL,
    `apetSiegeLib`      varchar(200) DEFAULT NULL,
    `trtDerDateSiege`   varchar(20)  DEFAULT NULL,
    `adrEtVoieNum`      varchar(100) DEFAULT NULL,
    `adrEtVoieType`     varchar(100) DEFAULT NULL,
    `adrEtVoieLib`      varchar(100) DEFAULT NULL,
    `adrEtDepCom`       varchar(100) DEFAULT NULL,
    `adrEtComLib`       varchar(100) DEFAULT NULL,
    `adrEtCodePost`     varchar(100) DEFAULT NULL,
    `adrEtPost1`        varchar(200) DEFAULT NULL,
    `adrEtPost2`        varchar(200) DEFAULT NULL,
    `adrEtPost3`        varchar(200) DEFAULT NULL,
    `nic`               varchar(5)   DEFAULT '00000',
    `etatEt`            char(2)      DEFAULT NULL,
    `etatDebDateEt`     varchar(15)  DEFAULT NULL,
    `catEt`             char(2)      DEFAULT NULL,
    `catEtLib`          varchar(200) DEFAULT NULL,
    `eff3112TrEt`       char(2)      DEFAULT NULL,
    `eff3112TrEtLib`    varchar(100) DEFAULT NULL,
    `effAnEt`           varchar(15)  DEFAULT NULL,
    `apet`              varchar(5)   DEFAULT NULL,
    `apetLib`           varchar(200) DEFAULT NULL,
    `trtDerDateEt`      varchar(15)  DEFAULT NULL,
    `etatEtLib`         varchar(20)  DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Entreprise_info_exercice`
--

--  `Entreprise_info_exercice`;


CREATE TABLE `Entreprise_info_exercice`
(
    `id_Entreprise`               int(11)     NOT NULL DEFAULT 0,
    `debutExerciceGlob`           varchar(15)          DEFAULT NULL,
    `finExerciceGlob`             varchar(15)          DEFAULT NULL,
    `ventesGlob`                  varchar(10)          DEFAULT '0',
    `biensGlob`                   varchar(10)          DEFAULT '0',
    `servicesGlob`                varchar(10)          DEFAULT '0',
    `totalGlob`                   varchar(10)          DEFAULT '0',
    `annee_cloture_exercice`      varchar(15) NOT NULL DEFAULT '',
    `effectif_moyen`              int(10)              DEFAULT 0,
    `effectif_encadrement`        int(10)              DEFAULT 0,
    `pme`                         enum ('1','0')       DEFAULT NULL,
    `chiffre_affaires`            varchar(256)         DEFAULT NULL,
    `besoin_excedent_financement` varchar(256)         DEFAULT NULL,
    `cash_flow`                   varchar(256)         DEFAULT NULL,
    `capacite_endettement`        varchar(256)         DEFAULT NULL,
    `saisie_manuelle`             enum ('0','1')       DEFAULT '1',
    PRIMARY KEY (`id_Entreprise`, `annee_cloture_exercice`),
    KEY `id_entreprise_idx` (`id_Entreprise`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Enveloppe`
--

--  `Enveloppe`;


CREATE TABLE `Enveloppe`
(
    `id_enveloppe_electro`             int(22)      NOT NULL AUTO_INCREMENT,
    `organisme`                        varchar(30)  NOT NULL DEFAULT '',
    `offre_id`                         int(22)      NOT NULL DEFAULT 0,
    `champs_optionnels`                longblob     NOT NULL,
    `fichier`                          int(11)      NOT NULL DEFAULT 0,
    `supprime`                         char(1)      NOT NULL DEFAULT '0',
    `cryptage`                         char(1)      NOT NULL DEFAULT '1',
    `nom_fichier`                      varchar(256) NOT NULL DEFAULT '',
    `hash`                             varchar(40)  NOT NULL DEFAULT '',
    `type_env`                         int(1)       NOT NULL DEFAULT 0,
    `sous_pli`                         int(3)       NOT NULL DEFAULT 0,
    `attribue`                         char(1)      NOT NULL DEFAULT '0',
    `dateheure_ouverture`              varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `agent_id_ouverture`               int(11)               DEFAULT NULL,
    `agent_id_ouverture2`              int(11)               DEFAULT NULL,
    `donnees_ouverture`                longblob     NOT NULL,
    `horodatage_donnees_ouverture`     longblob     NOT NULL,
    `statut_enveloppe`                 int(2)       NOT NULL DEFAULT 1,
    `agent_telechargement`             int(11)               DEFAULT NULL,
    `date_telechargement`              varchar(20)           DEFAULT NULL,
    `repertoire_telechargement`        varchar(100)          DEFAULT NULL,
    `nom_agent_ouverture`              varchar(100) NOT NULL DEFAULT '',
    `dateheure_ouverture_agent2`       varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `enveloppe_fictive`                enum ('0','1')        DEFAULT '0' COMMENT 'la valeur 1 si l''enveloppe est fictive si non 0',
    `integrite_fichier`                int(1)       NOT NULL DEFAULT 0,
    `verification_signature`           varchar(50)  NOT NULL DEFAULT '',
    `uid_response`                     text                  DEFAULT NULL COMMENT 'Unique ID du depot de l''offre',
    `resultat_verification_hash_files` char(1)               DEFAULT '1' COMMENT 'Contient le resultat de la vÃ©rification des hash des fichiers, 0=>KO(au moins un fichier non valide),1=>OK',
    `id_dossier_volumineux`            int(11)               DEFAULT NULL,
    `date_debut_dechiffrement`         datetime              DEFAULT NULL,
    PRIMARY KEY (`id_enveloppe_electro`, `organisme`),
    KEY `offre_id` (`offre_id`, `organisme`),
    KEY `enveloppe_id_enveloppe_electro_idx` (`id_enveloppe_electro`),
    KEY `enveloppe_statut_enveloppe` (`statut_enveloppe`),
    KEY `id_dv_FK` (`id_dossier_volumineux`),
    CONSTRAINT `id_dv_FK` FOREIGN KEY (`id_dossier_volumineux`) REFERENCES `dossier_volumineux` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnveloppeCritereEvaluation`
--

--  `EnveloppeCritereEvaluation`;


CREATE TABLE `EnveloppeCritereEvaluation`
(
    `id`                        int(11)      NOT NULL AUTO_INCREMENT,
    `idCritereEvaluation`       int(11)      NOT NULL,
    `rejet`                     int(11)      NOT NULL,
    `statut_critere_evaluation` int(11)      NOT NULL,
    `note_totale`               varchar(30)  NOT NULL,
    `commentaire_total`         varchar(100) NOT NULL,
    `id_agent`                  int(11)      NOT NULL,
    `id_enveloppe`              int(11)      NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnveloppeFormulaireConsultation`
--

--  `EnveloppeFormulaireConsultation`;


CREATE TABLE `EnveloppeFormulaireConsultation`
(
    `id`                         int(11) NOT NULL AUTO_INCREMENT,
    `id_Formulaire_consultation` int(11) NOT NULL,
    `statut`                     int(11) NOT NULL,
    `id_enveloppe`               int(11) NOT NULL,
    `total_bd_ht`                varchar(30) DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnveloppeItemCritereEvaluation`
--

--  `EnveloppeItemCritereEvaluation`;


CREATE TABLE `EnveloppeItemCritereEvaluation`
(
    `id`                           int(11)        NOT NULL AUTO_INCREMENT,
    `idItemCritereEvaluation`      int(11)        NOT NULL,
    `idEnveloppeCritereEvaluation` int(11)        NOT NULL,
    `libelle`                      text           NOT NULL,
    `prix`                         enum ('0','1') NOT NULL DEFAULT '0',
    `quantite`                     int(11)                 DEFAULT NULL,
    `ponderation`                  int(11)                 DEFAULT NULL,
    `numero`                       int(11)        NOT NULL,
    `regle_evaluation`             int(1)         NOT NULL,
    `type_item`                    int(11)        NOT NULL,
    `reponse_fournisseur`          varchar(100)   NOT NULL,
    `note`                         varchar(30)    NOT NULL,
    `commentaire_acheteur`         varchar(100)   NOT NULL,
    PRIMARY KEY (`id`),
    KEY `idEnveloppeCritereEvaluation` (`idEnveloppeCritereEvaluation`),
    CONSTRAINT `EnveloppeItemCritereEvaluation_ibfk_1` FOREIGN KEY (`idEnveloppeCritereEvaluation`) REFERENCES `EnveloppeCritereEvaluation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ItemFormulaireConsultation`
--

--  `ItemFormulaireConsultation`;


CREATE TABLE `ItemFormulaireConsultation`
(
    `id`                       int(11)        NOT NULL AUTO_INCREMENT,
    `idFormulaireConsultation` int(11)        NOT NULL,
    `libelle`                  text           NOT NULL,
    `commentaire_acheteur`     text           NOT NULL,
    `prix_unitaire`            enum ('0','1') NOT NULL DEFAULT '0',
    `quantite`                 varchar(11)    NOT NULL,
    `precision_entreprise`     int(1)         NOT NULL,
    `type_reponse`             varchar(30)    NOT NULL,
    `obligatoire`              enum ('0','1') NOT NULL DEFAULT '1',
    `numero`                   int(11)        NOT NULL,
    PRIMARY KEY (`id`),
    KEY `ItemFormulaireConsultation_ConsultationFormulaire` (`idFormulaireConsultation`),
    CONSTRAINT `ItemFormulaireConsultation_ConsultationFormulaire` FOREIGN KEY (`idFormulaireConsultation`) REFERENCES `ConsultationFormulaire` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EnveloppeItemFormulaireConsultationValues`
--

--  `EnveloppeItemFormulaireConsultationValues`;


CREATE TABLE `EnveloppeItemFormulaireConsultationValues`
(
    `id`                                int(11)      NOT NULL AUTO_INCREMENT,
    `idItemFormulaireConsultation`      int(11)      NOT NULL,
    `idEnveloppeFormulaireConsultation` int(11)      NOT NULL,
    `valeur`                            varchar(100) NOT NULL,
    `type_valeur`                       int(11)      NOT NULL,
    `precision_entreprise`              varchar(100) NOT NULL,
    `prix_unitaire`                     varchar(30)  NOT NULL,
    `tva`                               varchar(30)  NOT NULL,
    PRIMARY KEY (`id`),
    KEY `idItemFormulaireConsultation` (`idItemFormulaireConsultation`),
    CONSTRAINT `EnveloppeItemFormulaireConsultationValues_ibfk_1` FOREIGN KEY (`idItemFormulaireConsultation`) REFERENCES `ItemFormulaireConsultation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Enveloppe_papier`
--

--  `Enveloppe_papier`;


CREATE TABLE `Enveloppe_papier`
(
    `id_enveloppe_papier` int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`           varchar(30)    NOT NULL DEFAULT '',
    `offre_papier_id`     int(22)        NOT NULL DEFAULT 0,
    `statut`              int(11)        NOT NULL DEFAULT 0,
    `supprime`            char(1)        NOT NULL DEFAULT '0',
    `cryptage`            char(1)        NOT NULL DEFAULT '1',
    `is_send`             int(1)         NOT NULL DEFAULT 1,
    `type_env`            int(1)         NOT NULL DEFAULT 0,
    `sous_pli`            int(3)         NOT NULL DEFAULT 0,
    `champs_optionnels`   longtext                DEFAULT NULL,
    `agent_id_ouverture`  int(11)        NOT NULL DEFAULT 0,
    `dateheure_ouverture` datetime       NOT NULL DEFAULT '0000-00-00 00:00:00',
    `statut_enveloppe`    int(2)         NOT NULL DEFAULT 1,
    `enveloppe_postule`   enum ('0','1') NOT NULL DEFAULT '1',
    `nom_agent_ouverture` varchar(100)            DEFAULT NULL,
    PRIMARY KEY (`id_enveloppe_papier`, `organisme`),
    KEY `offre_papier_id` (`offre_papier_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Etape_cao`
--

--  `Etape_cao`;


CREATE TABLE `Etape_cao`
(
    `id_etape_cao`       int(11)      NOT NULL AUTO_INCREMENT,
    `intitule_etape_cao` varchar(250) NOT NULL DEFAULT '',
    PRIMARY KEY (`id_etape_cao`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `EtatConsultation`
--

--  `EtatConsultation`;


CREATE TABLE `EtatConsultation`
(
    `id_etat`          int(11)      NOT NULL AUTO_INCREMENT,
    `code_etat`        varchar(200) NOT NULL,
    `abreviation_etat` varchar(20)  NOT NULL DEFAULT '',
    `visible`          int(11)      NOT NULL DEFAULT 0,
    PRIMARY KEY (`id_etat`),
    KEY `code_etat` (`code_etat`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Faq_Entreprise`
--

--  `Faq_Entreprise`;


CREATE TABLE `Faq_Entreprise`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT,
    `question`    text         NOT NULL,
    `question_fr` text         NOT NULL,
    `question_en` text         NOT NULL,
    `question_es` text         NOT NULL,
    `reponse`     text         NOT NULL,
    `reponse_fr`  text         NOT NULL,
    `reponse_en`  text         NOT NULL,
    `reponse_es`  text         NOT NULL,
    `question_it` text         NOT NULL,
    `reponse_it`  text         NOT NULL,
    `theme`       varchar(256) NOT NULL,
    `ordre`       int(11)      NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Fcsp_Lieu`
--

--  `Fcsp_Lieu`;


CREATE TABLE `Fcsp_Lieu`
(
    `id_auto`   int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id`        int(11)          NOT NULL DEFAULT 0,
    `organisme` varchar(5)       NOT NULL,
    `libelle`   varchar(256)              DEFAULT NULL,
    PRIMARY KEY (`id_auto`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Fcsp_Mandataire`
--

--  `Fcsp_Mandataire`;


CREATE TABLE `Fcsp_Mandataire`
(
    `id_auto`   int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id`        int(11)          NOT NULL DEFAULT 0,
    `organisme` varchar(5)       NOT NULL,
    `libelle`   varchar(256)              DEFAULT NULL,
    PRIMARY KEY (`id_auto`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Fcsp_unite`
--

--  `Fcsp_unite`;


CREATE TABLE `Fcsp_unite`
(
    `id_auto`   int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id`        int(11)          NOT NULL DEFAULT 0,
    `organisme` varchar(5)       NOT NULL,
    `libelle`   varchar(256)              DEFAULT NULL,
    PRIMARY KEY (`id_auto`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `FormXmlDestinataireOpoce`
--

--  `FormXmlDestinataireOpoce`;


CREATE TABLE `FormXmlDestinataireOpoce`
(
    `id`                    int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`             varchar(30) NOT NULL,
    `id_destinataire_opoce` int(11)     NOT NULL,
    `xml`                   longtext    NOT NULL,
    `code_retour`           varchar(256) DEFAULT NULL COMMENT 'Permet de stocker le code retourne par OPOCE apres la reussite de l envoie de l xml. Ce code est appelé submission_id ou identifiant opoce',
    `message_retour`        varchar(256) DEFAULT NULL COMMENT 'Permet de stocker les retours (messages d erreur) des flux entre MPE et OPOCE',
    `id_joue`               varchar(256) DEFAULT NULL COMMENT 'Permet de stocker le numéro du document dans le journal officiel de l UE',
    `date_pub_joue`         varchar(256) DEFAULT NULL COMMENT 'Permet de stocker la date de publication du document dans le journal officiel de l UE',
    `lien_publication`      varchar(225) DEFAULT NULL COMMENT 'Permet de stocker le lien de publication de la publicité sur le site TED',
    `no_doc_ext`            varchar(256) DEFAULT NULL COMMENT 'Permet de stocker le numéro du document généré par esender',
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `GeolocalisationN0`
--

--  `GeolocalisationN0`;


CREATE TABLE `GeolocalisationN0`
(
    `id`                      int(11)        NOT NULL AUTO_INCREMENT,
    `denomination`            varchar(40)    NOT NULL DEFAULT '',
    `denomination_fr`         varchar(40)    NOT NULL,
    `denomination_en`         varchar(40)    NOT NULL,
    `denomination_es`         varchar(40)    NOT NULL,
    `type`                    int(11)        NOT NULL COMMENT 'mettre 0 pour les collectivites,1  pour les pays,2  pour la carte (france metropolitaine)',
    `actif`                   enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'mettre a 0 si on veut masquer ce type ',
    `libelle_selectionner`    varchar(40)    NOT NULL DEFAULT '',
    `libelle_selectionner_fr` varchar(40)    NOT NULL,
    `libelle_selectionner_en` varchar(40)    NOT NULL,
    `libelle_selectionner_es` varchar(40)    NOT NULL,
    `libelle_tous`            varchar(40)    NOT NULL DEFAULT '',
    `libelle_tous_fr`         varchar(40)    NOT NULL,
    `libelle_tous_en`         varchar(40)    NOT NULL,
    `libelle_tous_es`         varchar(40)    NOT NULL,
    `libelle_Aucun`           varchar(50)    NOT NULL DEFAULT '',
    `libelle_Aucun_fr`        varchar(50)    NOT NULL,
    `libelle_Aucun_en`        varchar(50)    NOT NULL,
    `libelle_Aucun_es`        varchar(50)    NOT NULL,
    `denomination_ar`         varchar(40)    NOT NULL,
    `libelle_selectionner_ar` varchar(40)    NOT NULL,
    `libelle_tous_ar`         varchar(40)    NOT NULL,
    `libelle_Aucun_ar`        varchar(40)    NOT NULL,
    `denomination_su`         varchar(40)    NOT NULL,
    `libelle_selectionner_su` varchar(40)    NOT NULL,
    `libelle_tous_su`         varchar(40)    NOT NULL,
    `libelle_Aucun_su`        varchar(40)    NOT NULL,
    `denomination_du`         varchar(40)    NOT NULL,
    `libelle_selectionner_du` varchar(40)    NOT NULL,
    `libelle_tous_du`         varchar(40)    NOT NULL,
    `libelle_Aucun_du`        varchar(40)    NOT NULL,
    `denomination_cz`         varchar(40)    NOT NULL,
    `libelle_selectionner_cz` varchar(40)    NOT NULL,
    `libelle_tous_cz`         varchar(40)    NOT NULL,
    `libelle_Aucun_cz`        varchar(40)    NOT NULL,
    `denomination_it`         varchar(40)    NOT NULL DEFAULT '',
    `libelle_selectionner_it` varchar(40)    NOT NULL DEFAULT '',
    `libelle_tous_it`         varchar(40)    NOT NULL DEFAULT '',
    `libelle_Aucun_it`        varchar(40)    NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `GeolocalisationN1`
--

--  `GeolocalisationN1`;


CREATE TABLE `GeolocalisationN1`
(
    `id`                   int(11)      NOT NULL AUTO_INCREMENT,
    `id_geolocalisationN0` int(11)      NOT NULL DEFAULT 0,
    `denomination1`        varchar(100) NOT NULL,
    `denomination2`        varchar(100) NOT NULL,
    `denomination1_ar`     varchar(100) NOT NULL,
    `denomination2_ar`     varchar(100) NOT NULL,
    `denomination1_fr`     varchar(100) NOT NULL,
    `denomination2_fr`     varchar(100) NOT NULL,
    `denomination1_en`     varchar(100) NOT NULL,
    `denomination2_en`     varchar(100) NOT NULL,
    `denomination1_es`     varchar(100) NOT NULL,
    `denomination2_es`     varchar(100) NOT NULL,
    `denomination1_su`     varchar(100) NOT NULL,
    `denomination2_su`     varchar(100) NOT NULL,
    `denomination1_du`     varchar(100) NOT NULL,
    `denomination2_du`     varchar(100) NOT NULL,
    `denomination1_cz`     varchar(100) NOT NULL,
    `denomination2_cz`     varchar(100) NOT NULL,
    `denomination1_it`     varchar(100) NOT NULL DEFAULT '',
    `denomination2_it`     varchar(100) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    KEY `id_geolocalisationN0` (`id_geolocalisationN0`),
    CONSTRAINT `GeolocalisationN1_ibfk_1` FOREIGN KEY (`id_geolocalisationN0`) REFERENCES `GeolocalisationN0` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `GeolocalisationN2`
--

--  `GeolocalisationN2`;


CREATE TABLE `GeolocalisationN2`
(
    `id`                         int(11)        NOT NULL AUTO_INCREMENT,
    `id_geolocalisationN1`       int(11)        NOT NULL DEFAULT 0,
    `denomination1`              varchar(50)    NOT NULL,
    `denomination2`              varchar(30)    NOT NULL DEFAULT '',
    `valeur_avec_sous_categorie` enum ('0','1') NOT NULL DEFAULT '1',
    `denomination1_ar`           varchar(50)    NOT NULL,
    `denomination2_ar`           varchar(30)    NOT NULL,
    `denomination1_fr`           varchar(50)    NOT NULL,
    `denomination2_fr`           varchar(30)    NOT NULL,
    `denomination1_en`           varchar(50)    NOT NULL,
    `denomination2_en`           varchar(30)    NOT NULL,
    `denomination1_es`           varchar(50)    NOT NULL,
    `denomination2_es`           varchar(30)    NOT NULL,
    `denomination1_su`           varchar(50)    NOT NULL,
    `denomination2_su`           varchar(30)    NOT NULL,
    `denomination1_du`           varchar(50)    NOT NULL,
    `denomination2_du`           varchar(30)    NOT NULL,
    `denomination1_cz`           varchar(50)    NOT NULL,
    `denomination2_cz`           varchar(30)    NOT NULL,
    `denomination1_it`           varchar(50)    NOT NULL DEFAULT '',
    `denomination2_it`           varchar(30)    NOT NULL DEFAULT '',
    `code_interface`             varchar(256)            DEFAULT NULL COMMENT 'contient le code ISO-3166 de chaque lieu',
    `valeur_sub`                 varchar(256)   NOT NULL,
    `type_lieu`                  char(1)        NOT NULL,
    `code_nuts`                  varchar(10)             DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `id_geolocalisationN1` (`id_geolocalisationN1`),
    CONSTRAINT `GeolocalisationN2_ibfk_1` FOREIGN KEY (`id_geolocalisationN1`) REFERENCES `GeolocalisationN1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Groupe_Moniteur`
--

--  `Groupe_Moniteur`;


CREATE TABLE `Groupe_Moniteur`
(
    `id`             int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`      varchar(30) NOT NULL DEFAULT '',
    `id_service`     int(11)     NOT NULL DEFAULT 0,
    `Identifiant`    varchar(50) NOT NULL DEFAULT '',
    `Mdp`            varchar(50) NOT NULL DEFAULT '',
    `Num_Abonnement` varchar(50)          DEFAULT NULL,
    `Num_Abonne`     varchar(50)          DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Guides`
--

--  `Guides`;


CREATE TABLE `Guides`
(
    `id_guide`    int(11)      NOT NULL AUTO_INCREMENT,
    `acces_from`  varchar(250) NOT NULL COMMENT 'mettre "entreprise" pour les guides coté Entreprise,mettre "agent" pour les guides coté acheteur public',
    `type`        varchar(250) NOT NULL,
    `libelle`     varchar(250) NOT NULL,
    `nom_fichier` varchar(250) NOT NULL,
    `langue`      varchar(2)   NOT NULL,
    PRIMARY KEY (`id_guide`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `HabilitationAgent`
--

--  `HabilitationAgent`;


CREATE TABLE `HabilitationAgent`
(
    `id_agent`                                                      int(11)        NOT NULL DEFAULT 0,
    `gestion_agent_pole`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_fournisseurs_envois_postaux`                           enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_bi_cles`                                               enum ('0','1') NOT NULL DEFAULT '0',
    `creer_consultation`                                            enum ('0','1') NOT NULL DEFAULT '1',
    `modifier_consultation`                                         enum ('0','1') NOT NULL DEFAULT '0',
    `valider_consultation`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `publier_consultation`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `suivre_consultation`                                           enum ('0','1') NOT NULL DEFAULT '1',
    `suivre_consultation_pole`                                      enum ('0','1') NOT NULL DEFAULT '0',
    `invite_permanent_entite_dependante`                            enum ('0','1') NOT NULL DEFAULT '0',
    `invite_permanent_mon_entite`                                   enum ('0','1') NOT NULL DEFAULT '0',
    `invite_permanent_transverse`                                   enum ('0','1') NOT NULL DEFAULT '0',
    `supprimer_enveloppe`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `supprimer_consultation`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `depouiller_candidature`                                        enum ('0','1') NOT NULL DEFAULT '1',
    `depouiller_offre`                                              enum ('0','1') NOT NULL DEFAULT '1',
    `messagerie_securisee`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `acces_registre_depots_papier`                                  enum ('0','1') NOT NULL DEFAULT '0',
    `acces_registre_retraits_papier`                                enum ('0','1') NOT NULL DEFAULT '0',
    `acces_registre_questions_papier`                               enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_encheres`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `suivre_encheres`                                               enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_entreprise`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `envoi_boamp`                                                   enum ('0','1') NOT NULL DEFAULT '0',
    `acces_classement_lot`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `connecteur_sis`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `connecteur_marco`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `repondre_aux_questions`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `appel_projet_formation`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `utiliser_client_CAO`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `notification_boamp`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `administrer_compte`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_mapa`                                                  enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_type_validation`                                       enum ('0','1') NOT NULL DEFAULT '0',
    `approuver_consultation`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `administrer_procedure`                                         enum ('0','1') NOT NULL DEFAULT '0',
    `restreindre_creation`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `creer_liste_marches`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_commissions`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_consultation`                                       enum ('0','1') NOT NULL DEFAULT '0',
    `attribution_marche`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `fiche_recensement`                                             enum ('0','1') NOT NULL DEFAULT '0',
    `declarer_infructueux`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `declarer_sans_suite`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `creer_consultation_transverse`                                 enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_candidature_en_ligne`                                   enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_candidature_a_distance`                                 enum ('0','1') NOT NULL DEFAULT '0',
    `refuser_enveloppe`                                             enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_admissibilite`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `restaurer_enveloppe`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_anonymat_en_ligne`                                      enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_offre_en_ligne`                                         enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_compte_boamp`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_agents`                                                enum ('1','0') NOT NULL DEFAULT '0',
    `gestion_habilitations`                                         enum ('1','0') NOT NULL DEFAULT '0',
    `gerer_mapa_inferieur_montant`                                  enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_mapa_superieur_montant`                                  enum ('0','1') NOT NULL DEFAULT '0',
    `modifier_consultation_avant_validation`                        enum ('0','1') NOT NULL DEFAULT '0',
    `modifier_consultation_apres_validation`                        enum ('0','1') NOT NULL DEFAULT '0',
    `acces_reponses`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `telechargement_groupe_anticipe_plis_chiffres`                  enum ('0','1') NOT NULL DEFAULT '0',
    `telechargement_unitaire_plis_chiffres`                         enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_offre_a_distance`                                       enum ('0','1') NOT NULL DEFAULT '0',
    `creer_annonce_information`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `saisie_marches`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `validation_marches`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `publication_marches`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_statistiques_metier`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_archives`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `administrer_procedures_formalisees`                            enum ('0','1') NOT NULL DEFAULT '0',
    `creer_annonce_attribution`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `acces_registre_retraits_electronique`                          enum ('0','1') NOT NULL DEFAULT '1',
    `acces_registre_questions_electronique`                         enum ('0','1') NOT NULL DEFAULT '1',
    `acces_registre_depots_electronique`                            enum ('0','1') NOT NULL DEFAULT '1',
    `validation_simple`                                             enum ('0','1') NOT NULL DEFAULT '1',
    `validation_intermediaire`                                      enum ('0','1') NOT NULL DEFAULT '1',
    `validation_finale`                                             enum ('0','1') NOT NULL DEFAULT '1',
    `creer_suite_consultation`                                      enum ('0','1') NOT NULL DEFAULT '1',
    `hyper_admin`                                                   enum ('0','1') NOT NULL DEFAULT '0',
    `droit_gestion_services`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_acces`                                                   enum ('0','1') NOT NULL DEFAULT '0',
    `statistiques_site`                                             enum ('0','1') NOT NULL DEFAULT '0',
    `statistiques_QoS`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_anonymat_a_distance`                                    enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_compte_jal`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_centrale_pub`                                          enum ('0','1')          DEFAULT '0',
    `Gestion_Compte_Groupe_Moniteur`                                enum ('0','1')          DEFAULT '0',
    `ouvrir_offre_technique_en_ligne`                               enum ('0','1') NOT NULL,
    `ouvrir_offre_technique_a_distance`                             enum ('0','1') NOT NULL,
    `activation_compte_entreprise`                                  enum ('0','1')          DEFAULT '0',
    `importer_enveloppe`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_registre_depots_papier`                             enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_registre_retraits_papier`                           enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_registre_questions_papier`                          enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_registre_depots_electronique`                       enum ('0','1') NOT NULL DEFAULT '1',
    `suivi_seul_registre_retraits_electronique`                     enum ('0','1') NOT NULL DEFAULT '1',
    `suivi_seul_registre_questions_electronique`                    enum ('0','1') NOT NULL DEFAULT '1',
    `modifier_consultation_mapa_inferieur_montant_apres_validation` enum ('0','1') NOT NULL DEFAULT '0',
    `modifier_consultation_mapa_superieur_montant_apres_validation` enum ('0','1') NOT NULL DEFAULT '0',
    `modifier_consultation_procedures_formalisees_apres_validation` enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_les_entreprises`                                         enum ('0','1')          DEFAULT '0',
    `portee_societes_exclues`                                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à l agent de creer une société exclue pour un organisme',
    `portee_societes_exclues_tous_organismes`                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à l agent de creer une société exclue pour un organisme pour l ensemble des organismes',
    `modifier_societes_exclues`                                     enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à l agent de modifier une societe exclue',
    `supprimer_societes_exclues`                                    enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à l agent de supprimer une societe exclue',
    `resultat_analyse`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_adresses_service`                                        enum ('0','1')          DEFAULT '0',
    `gerer_mon_service`                                             enum ('0','1')          DEFAULT '0',
    `download_archives`                                             enum ('0','1')          DEFAULT '0',
    `creer_annonce_extrait_pv`                                      enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Gère l habilitation pour la gestion des type d annonce Annonce_extrait_pv',
    `creer_annonce_rapport_achevement`                              enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Gère l habilitation pour la gestion des type d annonce Annonce_rapport_achevement',
    `gestion_certificats_agent`                                     enum ('0','1')          DEFAULT '0',
    `creer_avis_programme_previsionnel`                             enum ('0','1')          DEFAULT '0',
    `annuler_consultation`                                          enum ('0','1')          DEFAULT '0',
    `envoyer_publicite`                                             enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Conditionne l affichage du bouton "Envoyer" dans les colonnes "Actions" des tableaux "Liste des destinataires"',
    `liste_marches_notifies`                                        enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d accéder à la page de recherche avancée des marchés notifiés.',
    `suivre_message`                                                enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité de suivre les messages via l interface des détails de la consultation.',
    `envoyer_message`                                               enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité d envoyer des messages via l interface des details de la consultation.',
    `suivi_flux_chorus_transversal`                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d activer l accès à la fonctionnalité de recherche avancée des échanges',
    `gestion_mandataire`                                            enum ('0','1')          DEFAULT '0',
    `gerer_newsletter`                                              enum ('0','1')          DEFAULT '0' COMMENT 'permet de gerer la newsletter',
    `gestion_modeles_formulaire`                                    enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer les modeles de formulaires',
    `gestion_adresses_facturation_jal`                              enum ('0','1') NOT NULL DEFAULT '0',
    `administrer_adresses_facturation_jal`                          enum ('0','1') NOT NULL DEFAULT '0',
    `redaction_documents_redac`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `validation_documents_redac`                                    enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_mise_disposition_pieces_marche`                        enum ('0','1') NOT NULL DEFAULT '0',
    `annuaire_acheteur`                                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer la recherche des acheteur public',
    `reprendre_integralement_article`                               enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Reprendre intégralement un article REDAC',
    `administrer_clauses`                                           enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les clauses REDAC',
    `valider_clauses`                                               enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Valider les clauses REDAC',
    `administrer_canevas`                                           enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les canevas REDAC',
    `valider_canevas`                                               enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Valider les canevas REDAC',
    `administrer_clauses_entite_achats`                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les clauses Entité d Achats REDAC',
    `generer_pieces_format_odt`                                     enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Générer les pièces au format odt (éditable) REDAC',
    `publier_version_clausier_editeur`                              enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Publier une version de clausier Editeur REDAC',
    `administrer_clauses_editeur`                                   enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les clauses editeur REDAC',
    `valider_clauses_editeur`                                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Valider les clauses editeur REDAC',
    `administrer_canevas_editeur`                                   enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les canevas editeur REDAC',
    `valider_canevas_editeur`                                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Valider les canevas editeur REDAC',
    `decision_suivi_seul`                                           enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de télécharger les plis et accéder à la décision en lecture seule',
    `ouvrir_candidature_hors_ligne`                                 enum ('0','1') NOT NULL DEFAULT '1',
    `ouvrir_offre_hors_ligne`                                       enum ('0','1') NOT NULL DEFAULT '1',
    `ouvrir_offre_technique_hors_ligne`                             enum ('0','1') NOT NULL DEFAULT '1',
    `ouvrir_anonymat_hors_ligne`                                    enum ('0','1') NOT NULL DEFAULT '1',
    `espace_collaboratif_gestionnaire`                              enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet la création d''un Espace collaboratif',
    `espace_collaboratif_contributeur`                              enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet la visualisation d''un Espace collaboratif',
    `gerer_organismes`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_associations_agents`                                     enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Associer à un autre compte',
    `module_redaction_uniquement`                                   enum ('0','1')          DEFAULT '0',
    `historique_navigation_inscrits`                                enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Acceder a l historique de navigation des inscrits ',
    `telecharger_accords_cadres`                                    enum ('0','1') NOT NULL DEFAULT '0',
    `creer_annonce_decision_resiliation`                            enum ('0','1') NOT NULL DEFAULT '0',
    `telecharger_siret_acheteur`                                    enum ('0','1')          DEFAULT '0',
    `creer_annonce_synthese_rapport_audit`                          enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer la creation d un eannonce de synthèse du rapport de d audit',
    `gerer_operations`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_reouvertures_modification`                               enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Cette habilitation gère la présence ou non du bouton "Demande de complément"(AOF)',
    `acceder_tous_telechargements`                                  enum ('0','1')          DEFAULT '0',
    `gerer_newsletter_redac`                                        enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet d acceder à la page "Message aux redacteurs"',
    `creer_contrat`                                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise la creation d un contrat',
    `gerer_contrat`                                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise d Éditer le contrat et la Supprimer ',
    `consulter_contrat`                                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise de Consulter le contrat',
    `profil_rma`                                                    enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'rma: Responsable ministeriel d achat',
    `affectation_vision_rma`                                        enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'rma: Responsable ministeriel d achat',
    `gerer_gabarit_editeur`                                         enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer les gabarits de clausier éditeur',
    `gerer_gabarit`                                                 enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer les gabarits de clausier client',
    `gerer_gabarit_entite_achats`                                   enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer les gabarits de clausier entité d achat',
    `gerer_gabarit_agent`                                           enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer les gabarits de clausier agent',
    `gerer_messages_accueil`                                        enum ('0','1')          DEFAULT '0' COMMENT 'Permet de gerer la page de parametrage des messages d accueil ',
    `gerer_OA_GA`                                                   enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer la relation OA/GA pour chorus',
    `deplacer_service`                                              enum ('0','1')          DEFAULT '0' COMMENT 'Permet de gérer le déplacement d''un service vers un autre service',
    `activer_version_clausier`                                      enum ('0','1')          DEFAULT '0',
    `exec_voir_contrats`                                            enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d accéder au module EXEC et visualiser ses contrats',
    `exec_voir_contrats_ea`                                         enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de Visualiser les contrats de l Entité d''Achat au module EXEC',
    `exec_voir_contrats_ea_dependantes`                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de visualiser les contrats de l Entité d''Achat et des Entités d Achat dépendantes au module EXEC',
    `exec_voir_contrats_organisme`                                  enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de visualiser les contrats de l Organisme au module EXEC',
    `acces_ws`                                                      tinyint(1)     NOT NULL DEFAULT 0 COMMENT 'habilitation pour les accès au WS',
    `espace_documentaire_consultation`                              tinyint(1)     NOT NULL DEFAULT 0,
    `acces_echange_documentaire`                                    tinyint(1)     NOT NULL DEFAULT 0 COMMENT 'habilitation a l échange des documents',
    `administrer_organisme`                                         int(11)        NOT NULL DEFAULT 0,
    `exec_modification_contrat`                                     int(11)        NOT NULL DEFAULT 0,
    PRIMARY KEY (`id_agent`),
    CONSTRAINT `HabilitationAgent_ibfk_1` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `HabilitationProfil`
--

--  `HabilitationProfil`;


CREATE TABLE `HabilitationProfil`
(
    `id`                                                            int(11)        NOT NULL AUTO_INCREMENT,
    `libelle`                                                       varchar(100)   NOT NULL DEFAULT '',
    `gestion_agent_pole`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_fournisseurs_envois_postaux`                           enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_bi_cles`                                               enum ('0','1') NOT NULL DEFAULT '0',
    `creer_consultation`                                            enum ('0','1') NOT NULL DEFAULT '1',
    `modifier_consultation`                                         enum ('0','1') NOT NULL DEFAULT '0',
    `valider_consultation`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `publier_consultation`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `suivre_consultation`                                           enum ('0','1') NOT NULL DEFAULT '1',
    `suivre_consultation_pole`                                      enum ('0','1') NOT NULL DEFAULT '0',
    `invite_permanent_entite_dependante`                            enum ('0','1') NOT NULL DEFAULT '0',
    `invite_permanent_mon_entite`                                   enum ('0','1') NOT NULL DEFAULT '0',
    `invite_permanent_transverse`                                   enum ('0','1') NOT NULL DEFAULT '0',
    `supprimer_enveloppe`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `supprimer_consultation`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `depouiller_candidature`                                        enum ('0','1') NOT NULL DEFAULT '1',
    `depouiller_offre`                                              enum ('0','1') NOT NULL DEFAULT '1',
    `messagerie_securisee`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `acces_registre_depots_papier`                                  enum ('0','1') NOT NULL DEFAULT '0',
    `acces_registre_retraits_papier`                                enum ('0','1') NOT NULL DEFAULT '0',
    `acces_registre_questions_papier`                               enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_encheres`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `suivre_encheres`                                               enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_entreprise`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `envoi_boamp`                                                   enum ('0','1') NOT NULL DEFAULT '0',
    `acces_classement_lot`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `connecteur_sis`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `connecteur_marco`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `repondre_aux_questions`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `appel_projet_formation`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `utiliser_client_CAO`                                           enum ('0','1') NOT NULL DEFAULT '1',
    `notification_boamp`                                            enum ('0','1') NOT NULL DEFAULT '1',
    `administrer_compte`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_mapa`                                                  enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_type_validation`                                       enum ('0','1') NOT NULL DEFAULT '0',
    `approuver_consultation`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `administrer_procedure`                                         enum ('0','1') NOT NULL DEFAULT '0',
    `restreindre_creation`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `creer_liste_marches`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_commissions`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_consultation`                                       enum ('0','1') NOT NULL DEFAULT '0',
    `attribution_marche`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `fiche_recensement`                                             enum ('0','1') NOT NULL DEFAULT '0',
    `declarer_infructueux`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `declarer_sans_suite`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `creer_consultation_transverse`                                 enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_candidature_en_ligne`                                   enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_candidature_a_distance`                                 enum ('0','1') NOT NULL DEFAULT '0',
    `refuser_enveloppe`                                             enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_admissibilite`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `restaurer_enveloppe`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_offre_en_ligne`                                         enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_anonymat_en_ligne`                                      enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_compte_boamp`                                          enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_agents`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_habilitations`                                         enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_mapa_inferieur_montant`                                  enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_mapa_superieur_montant`                                  enum ('0','1') NOT NULL DEFAULT '0',
    `modifier_consultation_avant_validation`                        enum ('0','1') NOT NULL DEFAULT '0',
    `modifier_consultation_apres_validation`                        enum ('0','1') NOT NULL DEFAULT '0',
    `acces_reponses`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `telechargement_groupe_anticipe_plis_chiffres`                  enum ('0','1') NOT NULL DEFAULT '0',
    `telechargement_unitaire_plis_chiffres`                         enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_offre_a_distance`                                       enum ('0','1') NOT NULL DEFAULT '0',
    `creer_annonce_information`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `saisie_marches`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `validation_marches`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `publication_marches`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_statistiques_metier`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_archives`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `administrer_procedures_formalisees`                            enum ('0','1') NOT NULL DEFAULT '0',
    `creer_annonce_attribution`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `acces_registre_retraits_electronique`                          enum ('0','1') NOT NULL DEFAULT '1',
    `acces_registre_questions_electronique`                         enum ('0','1') NOT NULL DEFAULT '1',
    `acces_registre_depots_electronique`                            enum ('0','1') NOT NULL DEFAULT '1',
    `validation_simple`                                             enum ('0','1') NOT NULL DEFAULT '1',
    `validation_intermediaire`                                      enum ('0','1') NOT NULL DEFAULT '1',
    `validation_finale`                                             enum ('0','1') NOT NULL DEFAULT '1',
    `creer_suite_consultation`                                      enum ('0','1') NOT NULL DEFAULT '1',
    `hyper_admin`                                                   enum ('0','1') NOT NULL DEFAULT '0',
    `droit_gestion_services`                                        enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_acces`                                                   enum ('0','1') NOT NULL DEFAULT '0',
    `statistiques_site`                                             enum ('0','1') NOT NULL DEFAULT '0',
    `statistiques_QoS`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `ouvrir_anonymat_a_distance`                                    enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_compte_jal`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_centrale_pub`                                          enum ('0','1')          DEFAULT '0',
    `Gestion_Compte_Groupe_Moniteur`                                enum ('0','1') NOT NULL,
    `ouvrir_offre_technique_en_ligne`                               enum ('0','1') NOT NULL,
    `ouvrir_offre_technique_a_distance`                             enum ('0','1') NOT NULL,
    `activation_compte_entreprise`                                  enum ('0','1')          DEFAULT '0',
    `importer_enveloppe`                                            enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_registre_depots_papier`                             enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_registre_retraits_papier`                           enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_registre_questions_papier`                          enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_seul_registre_depots_electronique`                       enum ('0','1') NOT NULL DEFAULT '1',
    `suivi_seul_registre_retraits_electronique`                     enum ('0','1') NOT NULL DEFAULT '1',
    `suivi_seul_registre_questions_electronique`                    enum ('0','1') NOT NULL DEFAULT '1',
    `modifier_consultation_mapa_inferieur_montant_apres_validation` enum ('0','1') NOT NULL DEFAULT '1',
    `modifier_consultation_mapa_superieur_montant_apres_validation` enum ('0','1') NOT NULL DEFAULT '1',
    `modifier_consultation_procedures_formalisees_apres_validation` enum ('0','1') NOT NULL DEFAULT '1',
    `gerer_les_entreprises`                                         enum ('0','1')          DEFAULT '0',
    `portee_societes_exclues`                                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à l agent de creer une société exclue pour un organisme',
    `portee_societes_exclues_tous_organismes`                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à l agent de creer une société exclue pour un organisme pour l ensemble des organismes',
    `modifier_societes_exclues`                                     enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à l agent de modifier une societe exclue',
    `supprimer_societes_exclues`                                    enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à l agent de supprimer une societe exclue',
    `resultat_analyse`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_adresses_service`                                        enum ('0','1')          DEFAULT '0',
    `gerer_mon_service`                                             enum ('0','1')          DEFAULT '0',
    `download_archives`                                             enum ('0','1')          DEFAULT '0',
    `creer_annonce_extrait_pv`                                      enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Gère l habilitation pour la gestion des type d annonce Annonce_extrait_pv',
    `creer_annonce_rapport_achevement`                              enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Gère l habilitation pour la gestion des type d annonce Annonce_rapport_achevement',
    `gestion_certificats_agent`                                     enum ('0','1')          DEFAULT '0',
    `creer_avis_programme_previsionnel`                             enum ('0','1')          DEFAULT '0',
    `annuler_consultation`                                          enum ('0','1')          DEFAULT '0',
    `envoyer_publicite`                                             enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Conditionne l affichage du bouton "Envoyer" dans les colonnes "Actions" des tableaux "Liste des destinataires"',
    `liste_marches_notifies`                                        enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d accéder à la page de recherche avancée des marchés notifiés.',
    `suivre_message`                                                enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité de suivre les messages via l interface des détails de la consultation.',
    `envoyer_message`                                               enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité d envoyer des messages via l interface des details de la consultation.',
    `suivi_flux_chorus_transversal`                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d activer l''accès à la fonctionnalité de recherche avancée des échanges',
    `gestion_mandataire`                                            enum ('0','1')          DEFAULT '0',
    `gerer_newsletter`                                              enum ('0','1')          DEFAULT '0' COMMENT 'permet de gerer la newsletter',
    `gestion_modeles_formulaire`                                    enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer les modeles de formulaires',
    `gestion_adresses_facturation_jal`                              enum ('0','1') NOT NULL DEFAULT '0',
    `administrer_adresses_facturation_jal`                          enum ('0','1') NOT NULL DEFAULT '0',
    `redaction_documents_redac`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `validation_documents_redac`                                    enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_mise_disposition_pieces_marche`                        enum ('0','1') NOT NULL DEFAULT '0',
    `annuaire_acheteur`                                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer la recherche des acheteur public',
    `reprendre_integralement_article`                               enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Reprendre intégralement un article REDAC',
    `administrer_clauses`                                           enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les clauses REDAC',
    `valider_clauses`                                               enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Valider les clauses REDAC',
    `administrer_canevas`                                           enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les canevas REDAC',
    `valider_canevas`                                               enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Valider les canevas REDAC',
    `administrer_clauses_entite_achats`                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les clauses Entité d Achats REDAC',
    `generer_pieces_format_odt`                                     enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Générer les pièces au format odt (éditable) REDAC',
    `publier_version_clausier_editeur`                              enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Publier une version de clausier Editeur REDAC',
    `administrer_clauses_editeur`                                   enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les clauses editeur REDAC',
    `valider_clauses_editeur`                                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Valider les clauses editeur REDAC',
    `administrer_canevas_editeur`                                   enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Administrer les canevas editeur REDAC',
    `valider_canevas_editeur`                                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Valider les canevas editeur REDAC',
    `decision_suivi_seul`                                           enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de télécharger les plis et accéder à la décision en lecture seule',
    `ouvrir_candidature_hors_ligne`                                 enum ('0','1') NOT NULL DEFAULT '1',
    `ouvrir_offre_hors_ligne`                                       enum ('0','1') NOT NULL DEFAULT '1',
    `ouvrir_offre_technique_hors_ligne`                             enum ('0','1') NOT NULL DEFAULT '1',
    `ouvrir_anonymat_hors_ligne`                                    enum ('0','1') NOT NULL DEFAULT '1',
    `espace_collaboratif_gestionnaire`                              enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet la création d un Espace collaboratif',
    `espace_collaboratif_contributeur`                              enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet la visualisation d un Espace collaboratif',
    `gerer_organismes`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_associations_agents`                                     enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Associer à un autre compte',
    `module_redaction_uniquement`                                   enum ('0','1')          DEFAULT '0',
    `historique_navigation_inscrits`                                enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Acceder a l historique de navigation des inscrits ',
    `telecharger_accords_cadres`                                    enum ('0','1') NOT NULL DEFAULT '0',
    `creer_annonce_decision_resiliation`                            enum ('0','1') NOT NULL DEFAULT '0',
    `telecharger_siret_acheteur`                                    enum ('0','1')          DEFAULT '0',
    `creer_annonce_synthese_rapport_audit`                          enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_operations`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `gerer_reouvertures_modification`                               enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Cette habilitation gère la présence ou non du bouton "Demande de complément"(AOF)',
    `acceder_tous_telechargements`                                  enum ('0','1')          DEFAULT '0',
    `gerer_newsletter_redac`                                        enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet d acceder à la page "Message aux redacteurs"',
    `creer_contrat`                                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise la creation d un contrat',
    `gerer_contrat`                                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise d Éditer le contrat et la Supprimer ',
    `consulter_contrat`                                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Autorise de Consulter le contrat',
    `profil_rma`                                                    enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'rma: Responsable ministeriel d achat',
    `affectation_vision_rma`                                        enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'rma: Responsable ministeriel d achat',
    `gerer_gabarit_editeur`                                         enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer les gabarits de clausier éditeur',
    `gerer_gabarit`                                                 enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer les gabarits de clausier client',
    `gerer_gabarit_entite_achats`                                   enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer les gabarits de clausier entité d achat',
    `gerer_gabarit_agent`                                           enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer les gabarits de clausier agent',
    `gerer_messages_accueil`                                        enum ('0','1')          DEFAULT '0' COMMENT 'Permet de gerer la page de parametrage des messages d accueil ',
    `gerer_OA_GA`                                                   enum ('0','1')          DEFAULT '0' COMMENT 'Permet d administrer la relation OA/GA pour chorus',
    `deplacer_service`                                              enum ('0','1')          DEFAULT '0' COMMENT 'Permet de gérer le déplacement d un service vers un autre service',
    `activer_version_clausier`                                      enum ('0','1')          DEFAULT '0',
    `exec_voir_contrats`                                            enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d accéder au module EXEC et visualiser ses contrats',
    `exec_voir_contrats_ea`                                         enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de Visualiser les contrats de l Entité d Achat au module EXEC',
    `exec_voir_contrats_ea_dependantes`                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de visualiser les contrats de l Entité d Achat et des Entités d Achat dépendantes au module EXEC',
    `exec_voir_contrats_organisme`                                  enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de visualiser les contrats de l Organisme au module EXEC',
    `acces_echange_documentaire`                                    tinyint(1)     NOT NULL DEFAULT 0 COMMENT 'habilitation a l échange des documents',
    `espace_documentaire_consultation`                              tinyint(1)     NOT NULL DEFAULT 0 COMMENT 'habilitation a l espace documentaire',
    `administrer_organisme`                                         int(11)        NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Helios_cosignature`
--

--  `Helios_cosignature`;


CREATE TABLE `Helios_cosignature`
(
    `id_fichier`         int(20)     NOT NULL,
    `organisme`          varchar(30) NOT NULL DEFAULT '',
    `signature_acte`     varchar(20) NOT NULL,
    `horodatage_acte`    longblob    NOT NULL,
    `untrusteddate_acte` varchar(20) NOT NULL,
    `taille_acte`        varchar(20) NOT NULL,
    `id_Blob`            varchar(20) NOT NULL,
    `idEnveloppe`        int(20)     NOT NULL,
    PRIMARY KEY (`id_fichier`, `organisme`),
    KEY `id_fichier` (`id_fichier`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Helios_piece_publicite`
--

--  `Helios_piece_publicite`;


CREATE TABLE `Helios_piece_publicite`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL DEFAULT '',
    `nom_fichier`      varchar(100) NOT NULL DEFAULT '',
    `fichier`          int(11)      NOT NULL DEFAULT 0,
    `horodatage`       longblob     NOT NULL,
    `untrusteddate`    varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `taille`           varchar(20)  NOT NULL DEFAULT '1',
    `consultation_ref` int(11)      NOT NULL DEFAULT 0,
    `consultation_id`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `Helios_piece_publicite_Consultation` (`organisme`, `consultation_ref`),
    KEY `FK_Helios_piece_publicite_Consultation` (`consultation_id`),
    CONSTRAINT `FK_Helios_piece_publicite_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Helios_pv_consultation`
--

--  `Helios_pv_consultation`;


CREATE TABLE `Helios_pv_consultation`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL DEFAULT '',
    `consultation_ref` int(11)      NOT NULL DEFAULT 0,
    `nom_fichier`      varchar(100) NOT NULL DEFAULT '',
    `fichier`          int(11)      NOT NULL DEFAULT 0,
    `horodatage`       longblob     NOT NULL,
    `untrusteddate`    varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `taille`           varchar(20)  NOT NULL DEFAULT '0',
    `description`      text                  DEFAULT NULL,
    `consultation_id`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `Helios_pv_consultation_Consultation` (`organisme`, `consultation_ref`),
    KEY `FK_Helios_pv_consultation_Consultation` (`consultation_id`),
    CONSTRAINT `FK_Helios_pv_consultation_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Helios_rapport_prefet`
--

--  `Helios_rapport_prefet`;


CREATE TABLE `Helios_rapport_prefet`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL DEFAULT '',
    `consultation_ref` int(11)      NOT NULL DEFAULT 0,
    `nom_fichier`      varchar(100) NOT NULL DEFAULT '',
    `fichier`          int(11)      NOT NULL DEFAULT 0,
    `horodatage`       longblob     NOT NULL,
    `untrusteddate`    varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `taille`           varchar(20)  NOT NULL DEFAULT '0',
    `description`      text                  DEFAULT NULL,
    `consultation_id`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `Helios_rapport_prefet_Consultation` (`organisme`, `consultation_ref`),
    KEY `FK_Helios_rapport_prefet_Consultation` (`consultation_id`),
    CONSTRAINT `FK_Helios_rapport_prefet_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Helios_tableau_ar`
--

--  `Helios_tableau_ar`;


CREATE TABLE `Helios_tableau_ar`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL DEFAULT '',
    `consultation_ref` int(11)      NOT NULL DEFAULT 0,
    `nom_fichier`      varchar(100) NOT NULL DEFAULT '',
    `description`      longtext              DEFAULT NULL,
    `fichier`          int(11)      NOT NULL DEFAULT 0,
    `horodatage`       longblob     NOT NULL,
    `untrusteddate`    varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `taille`           varchar(20)  NOT NULL DEFAULT '0',
    `consultation_id`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `Helios_tableau_ar_Consultation` (`organisme`, `consultation_ref`),
    KEY `FK_Helios_tableau_ar_Consultation` (`consultation_id`),
    CONSTRAINT `FK_Helios_tableau_ar_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Helios_teletransmission`
--

--  `Helios_teletransmission`;


CREATE TABLE `Helios_teletransmission`
(
    `id`                    int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`             varchar(30)  NOT NULL DEFAULT '',
    `shown_id`              varchar(20)  NOT NULL DEFAULT '',
    `id_agent_creation`     int(11)      NOT NULL DEFAULT 0,
    `consultation_ref`      int(11)               DEFAULT NULL,
    `publicite_cons`        char(20)              DEFAULT NULL,
    `dce_items`             varchar(256)          DEFAULT NULL,
    `ids_pvs_cao`           varchar(256)          DEFAULT NULL,
    `ids_rapports_prefet`   varchar(256)          DEFAULT NULL,
    `deliberation_mapa`     char(1)               DEFAULT NULL,
    `tableau_ar`            int(11)               DEFAULT NULL,
    `date_creation`         varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `date_envoi`            varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `id_agent_envoi`        int(11)      NOT NULL DEFAULT 0,
    `nom_piecej1`           varchar(250) NOT NULL DEFAULT '',
    `piecej1`               int(11)      NOT NULL DEFAULT 0,
    `horodatage_piecej1`    longblob     NOT NULL,
    `untrusteddate_piecej1` varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `taille_piecej1`        varchar(20)  NOT NULL DEFAULT '',
    `nom_piecej2`           varchar(250) NOT NULL DEFAULT '',
    `piecej2`               int(11)      NOT NULL DEFAULT 0,
    `horodatage_piecej2`    longblob     NOT NULL,
    `untrusteddate_piecej2` varchar(20)  NOT NULL DEFAULT '0000-00-00 00:00:00',
    `taille_piecej2`        varchar(20)  NOT NULL DEFAULT '',
    `piecej1_desc`          longtext              DEFAULT NULL,
    `piecej2_desc`          longtext              DEFAULT NULL,
    `fichier_xml`           longblob     NOT NULL,
    `nom_dossierzip`        varchar(250) NOT NULL DEFAULT '',
    `taille_dossierzip`     varchar(250) NOT NULL DEFAULT '',
    `objet_libre`           varchar(250) NOT NULL DEFAULT '',
    `commentaire_libre`     varchar(250) NOT NULL DEFAULT '',
    `sig_piece_principale`  longblob     NOT NULL,
    `ref_utilisateur`       varchar(200) NOT NULL DEFAULT '',
    `consultation_id`       int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `Helios_teletransmission_Consultation` (`organisme`, `consultation_ref`),
    KEY `FK_Helios_teletransmission_Consultation` (`consultation_id`),
    CONSTRAINT `FK_Helios_teletransmission_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Helios_teletransmission_lot`
--

--  `Helios_teletransmission_lot`;


CREATE TABLE `Helios_teletransmission_lot`
(
    `id_teletransmission`       int(11)        NOT NULL DEFAULT 0,
    `organisme`                 varchar(30)    NOT NULL DEFAULT '',
    `lot`                       int(11)        NOT NULL DEFAULT 0,
    `numero_marche`             varchar(250)   NOT NULL DEFAULT '',
    `ids_env_actes_engagements` varchar(256)   NOT NULL DEFAULT '',
    `ids_enveloppes_items`      varchar(256)   NOT NULL DEFAULT '',
    `ids_docs_acceptes`         varchar(256)   NOT NULL DEFAULT '',
    `ids_msg_notif`             varchar(256)   NOT NULL DEFAULT '',
    `lot_in_transmission`       char(1)        NOT NULL DEFAULT '',
    `statut_envoi`              int(11)        NOT NULL DEFAULT 1,
    `id_zip_send`               varchar(40)             DEFAULT NULL,
    `nom_lettre_commande`       varchar(100)            DEFAULT NULL,
    `lettre_commande`           int(20)                 DEFAULT NULL,
    `horodatage`                varchar(20)    NOT NULL DEFAULT '',
    `untrusteddate`             varchar(20)    NOT NULL DEFAULT '',
    `taille`                    varchar(20)    NOT NULL DEFAULT '',
    `valider_comptable`         enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id_teletransmission`, `organisme`, `lot`),
    CONSTRAINT `Helios_teletransmission_lot_Helios_teletransmission` FOREIGN KEY (`id_teletransmission`, `organisme`) REFERENCES `Helios_teletransmission` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `HistoriqueNbrConsultationsPubliees`
--

--  `HistoriqueNbrConsultationsPubliees`;


CREATE TABLE `HistoriqueNbrConsultationsPubliees`
(
    `id_historique_nbr_cons`     int(11) NOT NULL AUTO_INCREMENT,
    `date_publication`           date    NOT NULL DEFAULT '0000-00-00',
    `nbr_consultations_publiees` int(11) NOT NULL,
    PRIMARY KEY (`id_historique_nbr_cons`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Historique_Avis_Pub`
--

--  `Historique_Avis_Pub`;


CREATE TABLE `Historique_Avis_Pub`
(
    `id`                int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`         varchar(30)  NOT NULL,
    `id_avis`           int(11)      NOT NULL,
    `date_modification` varchar(20)  NOT NULL,
    `detail_statut`     int(11)      NOT NULL,
    `motif_rejet`       varchar(256)          DEFAULT NULL COMMENT 'Permet de stocker le motif du rejet lorsque l avis est rejeté',
    `prenom_agent`      varchar(256) NOT NULL COMMENT 'Permet de renseigner le prénom de l agent qui éffectue l action',
    `nom_agent`         varchar(256) NOT NULL COMMENT 'Permet de renseigner le nom de l agent qui éffectue l action',
    `type_historique`   int(2)       NOT NULL DEFAULT 0 COMMENT 'Permet de differencier entre l historique de l avis global et des supports de l avis. 0 pour l avais et (1,2,3) pour les supports respectifs (opoce,presse,plateforme)',
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_avis` (`id_avis`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Historique_suppression_agent`
--

--  `Historique_suppression_agent`;


CREATE TABLE `Historique_suppression_agent`
(
    `id`                   int(11)     NOT NULL AUTO_INCREMENT,
    `id_agent_suppresseur` int(8)      NOT NULL DEFAULT 0,
    `id_service`           int(8)               DEFAULT NULL,
    `nom`                  varchar(100)         DEFAULT NULL,
    `prenom`               varchar(100)         DEFAULT NULL,
    `email`                varchar(100)         DEFAULT NULL,
    `organisme`            varchar(100)         DEFAULT NULL,
    `date_suppression`     varchar(20) NOT NULL DEFAULT '',
    `id_agent_supprime`    int(8)      NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `organisme` (`organisme`),
    KEY `service` (`id_service`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `InscritHistorique`
--

--  `InscritHistorique`;


CREATE TABLE `InscritHistorique`
(
    `id`            int(11)     NOT NULL AUTO_INCREMENT,
    `id_inscrit1`   int(11)     NOT NULL DEFAULT 0 COMMENT 'Identifiant de l inscrit qui effectue l action',
    `entreprise_id` int(11)     NOT NULL DEFAULT 0,
    `inscrit1`      varchar(64) NOT NULL DEFAULT '',
    `mail1`         varchar(64) NOT NULL DEFAULT '',
    `profil1`       int(11)     NOT NULL DEFAULT 0 COMMENT 'Le profil de l agent qui effectue l action',
    `inscrit2`      varchar(64) NOT NULL DEFAULT '',
    `mail2`         varchar(64) NOT NULL DEFAULT '',
    `date`          varchar(32) NOT NULL DEFAULT '',
    `action`        int(11)     NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `entreprise_id` (`entreprise_id`),
    KEY `id_inscrit1` (`id_inscrit1`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Historiques des actions ';


--
-- Table structure for table `InterfaceTypeProcedure`
--

--  `InterfaceTypeProcedure`;


CREATE TABLE `InterfaceTypeProcedure`
(
    `id`                   int(11) NOT NULL AUTO_INCREMENT,
    `type_procedure_sis`   varchar(10) DEFAULT NULL,
    `type_procedure_marco` varchar(10) DEFAULT NULL,
    `type_procedure`       varchar(10) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `InterneConsultation`
--

--  `InterneConsultation`;


CREATE TABLE `InterneConsultation`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL DEFAULT '',
    `interne_id`       int(11)     NOT NULL DEFAULT 0,
    `consultation_ref` int(11)     NOT NULL DEFAULT 0,
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `interne_id` (`interne_id`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `InterneConsultation_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_InterneConsultation_consultation` (`consultation_id`),
    CONSTRAINT `FK_InterneConsultation_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `InterneConsultationSuiviSeul`
--

--  `InterneConsultationSuiviSeul`;


CREATE TABLE `InterneConsultationSuiviSeul`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL DEFAULT '',
    `interne_id`       int(11)     NOT NULL DEFAULT 0,
    `consultation_ref` int(11)     NOT NULL DEFAULT 0,
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `interne_id` (`interne_id`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `InterneConsultationSuiviSeul_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_InterneConsultationSuiviSeul_consultation` (`consultation_id`),
    CONSTRAINT `FK_InterneConsultationSuiviSeul_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `InvitationConsultationTransverse`
--

--  `InvitationConsultationTransverse`;


CREATE TABLE `InvitationConsultationTransverse`
(
    `id`                   int(11)      NOT NULL AUTO_INCREMENT,
    `organisme_emetteur`   varchar(30)  NOT NULL DEFAULT '',
    `reference`            varchar(256) NOT NULL DEFAULT '',
    `organisme_invite`     varchar(30)  NOT NULL DEFAULT '',
    `lot`                  int(11)      NOT NULL DEFAULT 0,
    `date_decision`        date                  DEFAULT NULL,
    `id_contrat_titulaire` int(11)               DEFAULT NULL,
    `consultation_id`      int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `organisme_emetteur` (`organisme_emetteur`),
    KEY `organisme_invite` (`organisme_invite`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ItemCritereEvaluation`
--

--  `ItemCritereEvaluation`;


CREATE TABLE `ItemCritereEvaluation`
(
    `id`                  int(11)        NOT NULL AUTO_INCREMENT,
    `idCritereEvaluation` int(11)        NOT NULL,
    `libelle`             text           NOT NULL,
    `prix`                enum ('0','1') NOT NULL DEFAULT '0',
    `quantite`            int(11)                 DEFAULT NULL,
    `ponderation`         int(11)                 DEFAULT NULL,
    `numero`              int(11)        NOT NULL,
    `regle_evaluation`    int(1)         NOT NULL,
    `type_item`           int(11)        NOT NULL,
    PRIMARY KEY (`id`),
    KEY `ItemCritereEvaluation_CritereEvaluation` (`idCritereEvaluation`),
    CONSTRAINT `ItemCritereEvaluation_CritereEvaluation` FOREIGN KEY (`idCritereEvaluation`) REFERENCES `Criteres_Evaluation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ItemFormulaire`
--

--  `ItemFormulaire`;


CREATE TABLE `ItemFormulaire`
(
    `id`                   int(11)        NOT NULL AUTO_INCREMENT,
    `idModeleFormulaire`   int(11)        NOT NULL,
    `libelle`              text           NOT NULL,
    `commentaire_acheteur` text           NOT NULL,
    `prix_unitaire`        enum ('0','1') NOT NULL DEFAULT '0',
    `quantite`             varchar(11)    NOT NULL,
    `precision_entreprise` int(1)         NOT NULL,
    `type_reponse`         varchar(30)    NOT NULL,
    `obligatoire`          enum ('0','1') NOT NULL DEFAULT '1',
    `numero`               int(11)        NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Journaux`
--

--  `Journaux`;


CREATE TABLE `Journaux`
(
    `ID_JOURNAL`  int(20)     NOT NULL AUTO_INCREMENT,
    `ORGANISME`   varchar(30) NOT NULL,
    `ID_CENTRALE` int(20)     NOT NULL DEFAULT 0,
    `NOM_JOURNAL` varchar(200)         DEFAULT NULL,
    PRIMARY KEY (`ID_JOURNAL`, `ORGANISME`),
    KEY `ID_CENTRALE` (`ID_CENTRALE`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Justificatifs`
--

--  `Justificatifs`;


CREATE TABLE `Justificatifs`
(
    `id`                    int(11)        NOT NULL AUTO_INCREMENT,
    `intitule_justificatif` varchar(256)   NOT NULL DEFAULT '',
    `id_entreprise`         int(11)        NOT NULL DEFAULT 0,
    `taille`                varchar(80)    NOT NULL DEFAULT '',
    `justificatif`          int(11)        NOT NULL,
    `statut`                enum ('0','1') NOT NULL DEFAULT '0',
    `id_document`           int(11)                 DEFAULT NULL,
    `nom`                   varchar(100)            DEFAULT NULL,
    `nom_fr`                varchar(100)            DEFAULT NULL,
    `nom_en`                varchar(100)            DEFAULT NULL,
    `nom_es`                varchar(100)            DEFAULT NULL,
    `nom_su`                varchar(100)            DEFAULT NULL,
    `nom_du`                varchar(100)            DEFAULT NULL,
    `nom_cz`                varchar(100)            DEFAULT NULL,
    `nom_ar`                varchar(100)            DEFAULT NULL,
    `nom_it`                varchar(100)            DEFAULT NULL,
    `date_fin_validite`     date           NOT NULL DEFAULT '0000-00-00',
    `visible_par_agents`    enum ('0','1')          DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `id` (`id`),
    KEY `id_entreprise` (`id_entreprise`),
    KEY `id_document` (`id_document`),
    CONSTRAINT `Justificatifs_ibfk_1` FOREIGN KEY (`id_document`) REFERENCES `DocumentsAttaches` (`id_document`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Langue`
--

--  `Langue`;


CREATE TABLE `Langue`
(
    `id_langue`                                 int(11)        NOT NULL AUTO_INCREMENT,
    `langue`                                    varchar(10)    NOT NULL DEFAULT '',
    `active`                                    enum ('0','1') NOT NULL DEFAULT '0',
    `defaut`                                    enum ('0','1')          DEFAULT '0',
    `theme_specifique`                          enum ('0','1') NOT NULL DEFAULT '0',
    `obligatoire_pour_publication_consultation` enum ('0','1')          DEFAULT '0',
    PRIMARY KEY (`id_langue`),
    UNIQUE KEY `langue` (`langue`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `LieuExecution`
--

--  `LieuExecution`;


CREATE TABLE `LieuExecution`
(
    `id`             int(22)      NOT NULL DEFAULT 0,
    `libelle`        varchar(256) NOT NULL DEFAULT '',
    `CodePostal_ANM` varchar(5)   NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Lt_Referentiel`
--

--  `Lt_Referentiel`;


CREATE TABLE `Lt_Referentiel`
(
    `id`                       int(11)                    NOT NULL AUTO_INCREMENT,
    `code_libelle`             varchar(200)               NOT NULL,
    `entreprise`               enum ('0','1')             NOT NULL DEFAULT '0',
    `consultation`             enum ('0','1')             NOT NULL DEFAULT '0',
    `lot`                      enum ('0','1')             NOT NULL DEFAULT '0',
    `agent`                    enum ('0','1')             NOT NULL DEFAULT '0',
    `obligatoire`              enum ('0','1')             NOT NULL DEFAULT '0',
    `type_search`              enum ('0','1','2','3','4') NOT NULL DEFAULT '0' COMMENT '''0'' : recherche seulement du code fourni.// ''1'' : recherche du code fourni et ses ascendants.// ''2'' : recherche du code fourni et ses descendants.// ''3'' : recherche du code fourni, ses ascendants et ses descendants.//''4'' : recherche dans un interval donné',
    `pages`                    varchar(400)                        DEFAULT NULL,
    `path_config`              varchar(200)                        DEFAULT NULL,
    `mode_affichage`           varchar(20)                         DEFAULT NULL,
    `mode_modification`        varchar(20)                         DEFAULT NULL,
    `mode_recherche`           varchar(20)                         DEFAULT NULL,
    `Type`                     enum ('0','1','2')         NOT NULL DEFAULT '0',
    `organismes`               varchar(400)                        DEFAULT NULL COMMENT 'Mettre null si le référentiel pour tout les organismes sinon mettre la liste des organismes séparés par # exemple : #a1a#a2a# (commencer et terminer par #)',
    `libelle_info_bulle`       varchar(200)                        DEFAULT NULL COMMENT 'Contient le message d''info bulle pour ce référentiel',
    `logo`                     varchar(200)                        DEFAULT NULL,
    `defaultValue`             varchar(256)                        DEFAULT NULL COMMENT 'contient la valeur par defait du referentiel',
    `dependance_allotissement` enum ('0','1')             NOT NULL DEFAULT '0' COMMENT 'permet de gerer si le referentiel depond du lot si la consultation est allotie',
    `data_type`                varchar(256)                        DEFAULT NULL COMMENT 'Mettre le nom de type des données à enregistrer dans le champ ; exemple : MONTANT,TEXT,...',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Mandataire_service`
--

--  `Mandataire_service`;


CREATE TABLE `Mandataire_service`
(
    `id`          int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`   varchar(30) NOT NULL,
    `service_id`  int(11)     NOT NULL,
    `mandataire`  int(11)     NOT NULL,
    `commentaire` text DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `t_contrat_titulaire`
--

--  `t_contrat_titulaire`;


CREATE TABLE `t_contrat_titulaire`
(
    `id_contrat_titulaire`               int(11)        NOT NULL AUTO_INCREMENT,
    `id_type_contrat`                    int(11)                         DEFAULT NULL,
    `id_contact_contrat`                 int(11)                         DEFAULT NULL,
    `numero_contrat`                     varchar(256)                    DEFAULT NULL,
    `lien_AC_SAD`                        int(11)                         DEFAULT NULL,
    `id_contrat_multi`                   int(11)                         DEFAULT NULL,
    `organisme`                          varchar(45)                     DEFAULT NULL,
    `service_id`                         int(11)                         DEFAULT NULL,
    `id_titulaire`                       int(11)                         DEFAULT NULL,
    `id_titulaire_etab`                  int(11)                         DEFAULT NULL,
    `id_offre`                           int(11)                         DEFAULT NULL,
    `type_depot_reponse`                 char(1)                         DEFAULT NULL,
    `objet_contrat`                      text                            DEFAULT NULL,
    `intitule`                           varchar(256)                    DEFAULT '',
    `montant_contrat`                    double                          DEFAULT NULL,
    `id_tranche_budgetaire`              int(11)                         DEFAULT NULL,
    `montant_max_estime`                 double                          DEFAULT NULL,
    `publication_montant`                tinyint(4)                      DEFAULT NULL,
    `id_motif_non_publication_montant`   int(11)                         DEFAULT NULL,
    `desc_motif_non_publication_montant` varchar(256)                    DEFAULT NULL,
    `publication_contrat`                tinyint(4)                      DEFAULT NULL,
    `num_EJ`                             varchar(100)                    DEFAULT NULL,
    `statutEJ`                           text                            DEFAULT NULL,
    `num_long_OEAP`                      varchar(45)                     DEFAULT NULL,
    `reference_libre`                    varchar(45)                     DEFAULT NULL,
    `statut_contrat`                     int(11)                         DEFAULT 0 COMMENT 'permet de definir le statut du contrat : 0=>etape Type de decision,1=>etape Donnees du contrat - A saisir,2=>Numerotion autonome,3=>etape Notification,4=>etape Notification effectuee',
    `categorie`                          int(11)                         DEFAULT NULL,
    `ccag_applicable`                    int(11)                         DEFAULT NULL,
    `clause_sociale`                     varchar(255) CHARACTER SET utf8 DEFAULT NULL,
    `clause_sociale_condition_execution` varchar(225) CHARACTER SET utf8 DEFAULT NULL,
    `clause_sociale_insertion`           varchar(225) CHARACTER SET utf8 DEFAULT NULL,
    `clause_sociale_ateliers_proteges`   varchar(225) CHARACTER SET utf8 DEFAULT NULL,
    `clause_sociale_siae`                varchar(225) CHARACTER SET utf8 DEFAULT '0' COMMENT 'Le marché est réservé à SIAE',
    `clause_sociale_ess`                 varchar(225) CHARACTER SET utf8 DEFAULT '0' COMMENT 'Le marché est réservé à ESS',
    `clause_environnementale`            varchar(225) CHARACTER SET utf8 DEFAULT NULL,
    `clause_env_specs_techniques`        varchar(225) CHARACTER SET utf8 DEFAULT NULL,
    `clause_env_cond_execution`          varchar(225) CHARACTER SET utf8 DEFAULT NULL,
    `clause_env_criteres_select`         varchar(225) CHARACTER SET utf8 DEFAULT NULL,
    `date_prevue_notification`           date                            DEFAULT NULL,
    `date_prevue_fin_contrat`            date                            DEFAULT NULL,
    `date_prevue_max_fin_contrat`        date                            DEFAULT NULL,
    `date_notification`                  date                            DEFAULT NULL,
    `date_fin_contrat`                   date                            DEFAULT NULL,
    `date_max_fin_contrat`               date                            DEFAULT NULL,
    `date_attribution`                   date                            DEFAULT NULL,
    `date_creation`                      datetime                        DEFAULT NULL,
    `date_modification`                  datetime                        DEFAULT NULL,
    `envoi_interface`                    enum ('0','1') NOT NULL         DEFAULT '0' COMMENT 'Permet de specifier si la decision a ete exportee vers une interface',
    `contrat_class_key`                  int(1)         NOT NULL         DEFAULT 1 COMMENT 'utiliser pour utiliser l''heritage de propel la valeur 1 => l''objet contrat titulaire,2=> l''objet contrat multi',
    `pme_pmi`                            int(11)        NOT NULL         DEFAULT 0,
    `reference_consultation`             varchar(256)                    DEFAULT NULL,
    `hors_passation`                     int(1)         NOT NULL         DEFAULT 0 COMMENT 'Permet de savoir si le contrat est crée hors passation ( décision) ou non,0 => crée de puis passation,1 => hors passation ',
    `id_agent`                           int(11)                         DEFAULT NULL,
    `nom_agent`                          varchar(256)                    DEFAULT NULL,
    `prenom_agent`                       varchar(256)                    DEFAULT NULL,
    `lieu_execution`                     text                            DEFAULT NULL,
    `code_cpv_1`                         varchar(8)                      DEFAULT NULL,
    `code_cpv_2`                         varchar(256)                    DEFAULT NULL,
    `marche_insertion`                   tinyint(1)                      DEFAULT 0,
    `clause_specification_technique`     varchar(256)                    DEFAULT '0',
    `procedure_passation_pivot`          varchar(256)                    DEFAULT NULL,
    `nom_lieu_principal_execution`       varchar(256)   NOT NULL,
    `code_lieu_principal_execution`      varchar(20)    NOT NULL,
    `type_code_lieu_principal_execution` varchar(20)    NOT NULL,
    `duree_initiale_contrat`             int(11)                         DEFAULT NULL,
    `forme_prix`                         varchar(256)   NOT NULL,
    `date_publication_initiale_de`       datetime       NOT NULL         DEFAULT '0000-00-00 00:00:00',
    `num_id_unique_marche_public`        varchar(256)   NOT NULL,
    `libelle_type_contrat_pivot`         varchar(256)   NOT NULL,
    `siret_pa_accord_cadre`              varchar(256)                    DEFAULT NULL,
    `ac_marche_subsequent`               tinyint(1)     NOT NULL         DEFAULT 0,
    `libelle_type_procedure_mpe`         varchar(256)                    DEFAULT NULL,
    `nb_total_propositions_lot`          int(11)                         DEFAULT NULL,
    `nb_total_propositions_demat_lot`    int(11)                         DEFAULT NULL,
    `marche_defense`                     char(1)                         DEFAULT '0' COMMENT 'Permet de savoir si marché de défense est selectionné : 0=> Non renseigné,1=>Oui,2=>Non',
    `siret`                              varchar(256)                    DEFAULT NULL,
    `nom_entite_acheteur`                varchar(1000)                   DEFAULT NULL,
    `statut_publication_sn`              int(2)         NOT NULL         DEFAULT 0,
    `id_type_procedure_pivot`            int(11)                         DEFAULT NULL,
    `id_type_procedure_concession_pivot` int(11)                         DEFAULT NULL,
    `id_type_contrat_pivot`              int(11)                         DEFAULT NULL,
    `id_type_contrat_concession_pivot`   int(11)                         DEFAULT NULL,
    `montant_subvention_publique`        double                          DEFAULT NULL,
    `date_debut_execution`               date                            DEFAULT NULL,
    `uuid`                               varchar(36)                     DEFAULT NULL,
    PRIMARY KEY (`id_contrat_titulaire`),
    KEY `fk_Contrat_Titulaire_Contrat_Multi1_idx` (`id_contrat_multi`),
    KEY `fk_Contrat_Titulaire_Contact_Contrat` (`id_contact_contrat`),
    KEY `fk_Contrat_Titulaire_Type_Contrat` (`id_type_contrat`),
    KEY `Idx_contrat_num_EJ` (`num_EJ`),
    KEY `Idx_contrat_statut_EJ` (`statutEJ`(767)),
    KEY `id_type_procedure_pivot` (`id_type_procedure_pivot`),
    KEY `FK_id_type_procedure_concession_pivot` (`id_type_procedure_concession_pivot`),
    KEY `FK_id_type_contrat_pivot` (`id_type_contrat_pivot`),
    KEY `FK_id_type_contrat_concession_pivot` (`id_type_contrat_concession_pivot`),
    CONSTRAINT `FK_id_type_contrat_concession_pivot` FOREIGN KEY (`id_type_contrat_concession_pivot`) REFERENCES `type_contrat_concession_pivot` (`id`),
    CONSTRAINT `FK_id_type_contrat_pivot` FOREIGN KEY (`id_type_contrat_pivot`) REFERENCES `type_contrat_pivot` (`id`),
    CONSTRAINT `FK_id_type_procedure_concession_pivot` FOREIGN KEY (`id_type_procedure_concession_pivot`) REFERENCES `type_procedure_concession_pivot` (`id`),
    CONSTRAINT `fk_Contrat_Titulaire_Contact_Contrat` FOREIGN KEY (`id_contact_contrat`) REFERENCES `t_contact_contrat` (`id_contact_contrat`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_Contrat_Titulaire_Contrat_Multi1` FOREIGN KEY (`id_contrat_multi`) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_Contrat_Titulaire_Type_Contrat` FOREIGN KEY (`id_type_contrat`) REFERENCES `t_type_contrat` (`id_type_contrat`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `id_type_procedure_pivot` FOREIGN KEY (`id_type_procedure_pivot`) REFERENCES `TypeProcedure` (`id_type_procedure`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6272
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Marche`
--

--  `Marche`;


CREATE TABLE `Marche`
(
    `id`                               int(11) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`                        varchar(30)      NOT NULL DEFAULT '',
    `idMarcheTrancheBudgetaire`        int(11) unsigned          DEFAULT 0,
    `numeroMarcheAnnee`                int(11) unsigned          DEFAULT 0,
    `idService`                        int(11)                   DEFAULT 0,
    `nomAttributaire`                  varchar(256)              DEFAULT NULL,
    `dateNotification`                 date             NOT NULL DEFAULT '0000-00-00',
    `codePostal`                       varchar(20)               DEFAULT NULL,
    `natureMarche`                     varchar(30)               DEFAULT '1',
    `objetMarche`                      longtext                  DEFAULT NULL,
    `montantMarche`                    varchar(50)               DEFAULT '0',
    `valide`                           enum ('0','1')   NOT NULL DEFAULT '0',
    `isManuel`                         enum ('0','1')   NOT NULL DEFAULT '0',
    `pmePmi`                           int(11)                   DEFAULT 0,
    `id_decision_enveloppe`            int(11)          NOT NULL,
    `id_contrat_titulaire`             int(11)          NOT NULL,
    `ville`                            varchar(20)               DEFAULT NULL,
    `acronymePays_Attributaire`        varchar(10)               DEFAULT NULL,
    `pays_Attributaire`                varchar(50)               DEFAULT NULL,
    `siren_Attributaire`               varchar(20)               DEFAULT NULL,
    `nic_Attributaire`                 varchar(6)                DEFAULT NULL,
    `identifiantNational_Attributaire` varchar(50)               DEFAULT NULL,
    `rc_ville_attributaire`            varchar(250)              DEFAULT NULL,
    `rc_num_attributaire`              varchar(250)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `ListeMarches_FKIndex2` (`idMarcheTrancheBudgetaire`),
    KEY `ListeMarches_FKIndex3` (`idService`),
    KEY `numeroMarcheAnnee` (`numeroMarcheAnnee`),
    KEY `natureMarche` (`natureMarche`),
    KEY `id_decision_enveloppe` (`id_decision_enveloppe`),
    KEY `fk_Marche_contrat_titulaire` (`id_contrat_titulaire`),
    CONSTRAINT `Marche_ibfk_4` FOREIGN KEY (`natureMarche`) REFERENCES `CategorieConsultation` (`id`),
    CONSTRAINT `fk_Marche_contrat_titulaire` FOREIGN KEY (`id_contrat_titulaire`) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `MarchePublie`
--

--  `MarchePublie`;


CREATE TABLE `MarchePublie`
(
    `id`                int(11) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`         varchar(30)      NOT NULL DEFAULT '',
    `numeroMarcheAnnee` int(11) unsigned          DEFAULT 0,
    `idService`         int(11)                   DEFAULT 0,
    `isPubliee`         enum ('0','1')   NOT NULL DEFAULT '0',
    `isImportee`        enum ('0','1')   NOT NULL DEFAULT '0',
    `newVersion`        enum ('0','1')   NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`, `organisme`),
    KEY `ListeMarchePubliee_FKIndex1` (`idService`),
    KEY `ListeMarchePubliee_FKIndex2` (`numeroMarcheAnnee`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Mesure_Type_Procedure`
--

--  `Mesure_Type_Procedure`;


CREATE TABLE `Mesure_Type_Procedure`
(
    `id_tag_name`      int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL,
    `libelle_tag_name` varchar(256) NOT NULL DEFAULT '',
    PRIMARY KEY (`id_tag_name`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Mesure_avancement`
--

--  `Mesure_avancement`;


CREATE TABLE `Mesure_avancement`
(
    `id_mesure`                 int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`                 varchar(30)  NOT NULL,
    `id_service`                int(11)      NOT NULL,
    `annee`                     varchar(4)   NOT NULL,
    `trimestre`                 char(2)      NOT NULL,
    `siren`                     varchar(9)   NOT NULL,
    `nic`                       varchar(5)   NOT NULL,
    `identifiant_service`       varchar(100) NOT NULL,
    `mail`                      varchar(100) NOT NULL,
    `type_pouvoir_adjudicateur` int(11)      NOT NULL,
    `departement`               int(11)      NOT NULL,
    `date_envoi`                varchar(20)  DEFAULT NULL,
    `date_accuse`               varchar(20)  DEFAULT NULL,
    `etat`                      varchar(5)   DEFAULT 'BR',
    `date_creation`             varchar(20)  NOT NULL,
    `xml`                       longtext     DEFAULT NULL,
    `name_xml_genere`           varchar(225) DEFAULT NULL,
    PRIMARY KEY (`id_mesure`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ModeleFormulaire`
--

--  `ModeleFormulaire`;


CREATE TABLE `ModeleFormulaire`
(
    `id`                int(11)     NOT NULL AUTO_INCREMENT,
    `reference`         varchar(256)         DEFAULT NULL,
    `nom`               varchar(30) NOT NULL,
    `type_formulaire`   int(1)      NOT NULL,
    `id_type_procedure` int(1)               DEFAULT NULL,
    `id_categorie`      int(10)              DEFAULT NULL,
    `code_cpv_1`        varchar(8)           DEFAULT NULL,
    `code_cpv_2`        varchar(256)         DEFAULT NULL,
    `organisme`         varchar(30)          DEFAULT NULL,
    `service_id`        int(11)              DEFAULT NULL,
    `date_creation`     varchar(20) NOT NULL DEFAULT '0000-00-00 00:00:00',
    `date_modification` varchar(20)          DEFAULT '0000-00-00 00:00:00',
    `consultation_id`   int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Newsletter`
--

--  `Newsletter`;


CREATE TABLE `Newsletter`
(
    `id`                           int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`                    varchar(30)    NOT NULL,
    `objet`                        varchar(256)   NOT NULL DEFAULT '',
    `corps`                        text           NOT NULL,
    `id_service_redacteur`         int(11)        NOT NULL,
    `nom_redacteur`                varchar(100)   NOT NULL,
    `prenom_redacteur`             varchar(100)   NOT NULL,
    `envoye_par_nom`               varchar(100)            DEFAULT NULL,
    `envoye_par_prenom`            varchar(100)            DEFAULT NULL,
    `envoye_par_email`             varchar(100)            DEFAULT NULL,
    `date_creation`                varchar(25)    NOT NULL,
    `date_derniere_modification`   varchar(25)             DEFAULT NULL,
    `date_envoi`                   varchar(25)             DEFAULT NULL,
    `id_service_destinataire`      varchar(11)             DEFAULT NULL,
    `statut`                       int(2)                  DEFAULT NULL,
    `destinataires`                text                    DEFAULT NULL,
    `inclure_services_descendants` enum ('0','1') NOT NULL DEFAULT '0',
    `type_destinataire`            int(11)        NOT NULL DEFAULT 1,
    `is_redac`                     enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `NewsletterPieceJointe`
--

--  `NewsletterPieceJointe`;


CREATE TABLE `NewsletterPieceJointe`
(
    `id`            int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`     varchar(30)  NOT NULL,
    `id_newsletter` int(11)      NOT NULL DEFAULT 0,
    `nom_fichier`   varchar(100) NOT NULL DEFAULT '',
    `piece`         int(11)      NOT NULL DEFAULT 0,
    `horodatage`    longblob     NOT NULL,
    `untrusteddate` varchar(25)  NOT NULL DEFAULT '',
    `taille`        varchar(25)  NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_newsletter` (`id_newsletter`),
    CONSTRAINT `NewsletterPieceJointe_ibfk_1` FOREIGN KEY (`id_newsletter`) REFERENCES `Newsletter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Numerotation_ref_cons_auto`
--

--  `Numerotation_ref_cons_auto`;


CREATE TABLE `Numerotation_ref_cons_auto`
(
    `id_auto`      int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id_cons_auto` int(11)          NOT NULL DEFAULT 0,
    `organisme`    varchar(30)      NOT NULL DEFAULT '',
    `annee`        year(4)          NOT NULL DEFAULT 0000,
    `service_id`   int(11)                   DEFAULT NULL,
    PRIMARY KEY (`id_auto`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Offre_papier`
--

--  `Offre_papier`;


CREATE TABLE `Offre_papier`
(
    `id`                   int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`            varchar(30)    NOT NULL DEFAULT '',
    `consultation_ref`     int(11)        NOT NULL DEFAULT 0,
    `entreprise_id`        int(11)                 DEFAULT NULL,
    `id_etablissement`     int(11)                 DEFAULT NULL,
    `nom_entreprise`       varchar(30)    NOT NULL DEFAULT '',
    `date_depot`           datetime       NOT NULL DEFAULT '0000-00-00 00:00:00',
    `external_serial`      varchar(8)              DEFAULT NULL,
    `internal_serial`      varchar(8)              DEFAULT NULL,
    `offre_selectionnee`   int(11)        NOT NULL DEFAULT 0,
    `nom`                  varchar(80)             DEFAULT NULL,
    `prenom`               varchar(80)             DEFAULT NULL,
    `adresse`              varchar(100)            DEFAULT NULL,
    `adresse2`             varchar(100)            DEFAULT NULL,
    `telephone`            varchar(20)             DEFAULT NULL,
    `fax`                  varchar(30)             DEFAULT NULL,
    `code_postal`          varchar(5)              DEFAULT NULL,
    `ville`                varchar(50)             DEFAULT NULL,
    `pays`                 varchar(50)             DEFAULT NULL,
    `acronyme_pays`        varchar(10)             DEFAULT NULL,
    `siret`                varchar(14)             DEFAULT NULL,
    `identifiant_national` varchar(20)             DEFAULT NULL,
    `email`                varchar(100)            DEFAULT NULL,
    `observation`          text                    DEFAULT NULL,
    `date_annulation`      varchar(20)             DEFAULT NULL,
    `depot_annule`         enum ('0','1') NOT NULL DEFAULT '0',
    `offre_variante`       enum ('0','1')          DEFAULT NULL,
    `statut_offre_papier`  int(2)         NOT NULL DEFAULT 1,
    `numero_reponse`       int(11)                 DEFAULT NULL,
    `nom_agent_ouverture`  varchar(100)            DEFAULT NULL,
    `agent_id_ouverture`   int(11)                 DEFAULT NULL,
    `dateheure_ouverture`  varchar(20)    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `consultation_id`      int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `Offre_papier_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_Offre_papier_consultation` (`consultation_id`),
    CONSTRAINT `FK_Offre_papier_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `Ordre_Du_Jour`
--

--  `Ordre_Du_Jour`;


CREATE TABLE `Ordre_Du_Jour`
(
    `id`                     int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`              varchar(30)  NOT NULL,
    `id_seance`              int(11)      NOT NULL DEFAULT 0,
    `ref_consultation`       varchar(256)          DEFAULT NULL,
    `ref_libre`              varchar(50)           DEFAULT NULL,
    `id_etape`               int(11)      NOT NULL,
    `intitule_ordre_du_jour` longtext              DEFAULT NULL,
    `lots_odj_libre`         varchar(100)          DEFAULT NULL,
    `id_type_procedure`      int(11)               DEFAULT NULL,
    `type_procedure_libre`   varchar(256)          DEFAULT NULL,
    `date_cloture`           datetime              DEFAULT NULL,
    `type_env`               int(1)                DEFAULT NULL,
    `sous_pli`               int(2)                DEFAULT NULL,
    `heure`                  varchar(5)   NOT NULL DEFAULT '00',
    `etape_consultation`     varchar(200) NOT NULL DEFAULT '',
    `type_consultation`      varchar(200) NOT NULL DEFAULT '',
    `minutes`                char(2)      NOT NULL DEFAULT '',
    `service`                varchar(256)          DEFAULT NULL,
    `id_service`             int(10)               DEFAULT NULL,
    `date_debut`             varchar(256)          DEFAULT NULL,
    `consultation_id`        int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_commission` (`id_seance`),
    KEY `ref_Consultation` (`ref_consultation`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;





--
-- Table structure for table `Organisme_Service_Metier`
--

--  `Organisme_Service_Metier`;


CREATE TABLE `Organisme_Service_Metier`
(
    `id_auto`           int(11) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`         varchar(30)      NOT NULL DEFAULT '0',
    `id_service_metier` int(11)          NOT NULL DEFAULT 0,
    PRIMARY KEY (`id_auto`),
    KEY `organisme` (`organisme`),
    KEY `id_service_metier` (`id_service_metier`),
    CONSTRAINT `Organisme_Service_Metier_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `Organisme_Service_Metier_ibfk_2` FOREIGN KEY (`id_service_metier`) REFERENCES `Service_Mertier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Panier_Entreprise`
--

--  `Panier_Entreprise`;


CREATE TABLE `Panier_Entreprise`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL,
    `ref_consultation` int(11)      DEFAULT NULL,
    `id_entreprise`    int(11)     NOT NULL,
    `id_inscrit`       int(11)     NOT NULL,
    `date_ajout`       varchar(200) DEFAULT NULL,
    `consultation_id`  int(11)      DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `panierentrepriseunique` (`organisme`, `consultation_id`, `id_entreprise`, `id_inscrit`),
    KEY `panier_entreprise_organisme_idx` (`organisme`),
    KEY `panier_entreprise_ref_consultation_idx` (`ref_consultation`),
    KEY `panier_entreprise_id_entreprise_idx` (`id_entreprise`),
    KEY `panier_entreprise_id_inscrit_idx` (`id_inscrit`),
    KEY `PanierEntrepriseRef_ConsultationId` (`consultation_id`),
    CONSTRAINT `PanierEntrepriseIdEtp_EntrepriseId` FOREIGN KEY (`id_entreprise`) REFERENCES `Entreprise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `PanierEntrepriseIdInscrit_InscritId` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `PanierEntrepriseRef_ConsultationId` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Cette table lie l''inscrit d''une entreprise à une consultatio';


--
-- Table structure for table `Parametrage_Enchere`
--

--  `Parametrage_Enchere`;


CREATE TABLE `Parametrage_Enchere`
(
    `id`                          int(10) unsigned       NOT NULL AUTO_INCREMENT,
    `organisme`                   varchar(30)            NOT NULL DEFAULT '',
    `refConsultation`             int(10) unsigned                DEFAULT NULL,
    `idEntiteeAssociee`           int(11)                         DEFAULT NULL,
    `referenceUtilisateur`        varchar(45)                     DEFAULT NULL,
    `auteur`                      varchar(256)                    DEFAULT NULL,
    `idLot`                       int(10) unsigned                DEFAULT NULL,
    `objet`                       longtext                        DEFAULT NULL,
    `dateDebut`                   datetime                        DEFAULT '0000-00-00 00:00:00',
    `dateFin`                     datetime                        DEFAULT '0000-00-00 00:00:00',
    `dateSuspension`              datetime                        DEFAULT '0000-00-00 00:00:00',
    `delaiProlongation`           int(11)                         DEFAULT NULL,
    `commentaire`                 longtext                        DEFAULT NULL,
    `meilleureEnchereObligatoire` enum ('0','1')         NOT NULL DEFAULT '0',
    `typeBaremeNETC`              enum ('1','2','3','4') NOT NULL DEFAULT '1',
    `typeBaremeEnchereGlobale`    enum ('1','2')         NOT NULL DEFAULT '1',
    `meilleurNoteHaute`           enum ('0','1')         NOT NULL DEFAULT '0',
    `nbrCandidatsVisible`         enum ('0','1')         NOT NULL DEFAULT '1',
    `listeCandidatsVisible`       enum ('0','1')         NOT NULL DEFAULT '1',
    `rangVisible`                 enum ('0','1')         NOT NULL DEFAULT '1',
    `meilleureOffreVisible`       enum ('0','1')         NOT NULL DEFAULT '1',
    `montantReserve`              double                          DEFAULT NULL,
    `noteMaxBaremeRelatif`        double                          DEFAULT NULL,
    `coeffA`                      double                          DEFAULT NULL,
    `coeffB`                      double                          DEFAULT NULL,
    `coeffC`                      double                          DEFAULT NULL,
    `mail`                        longtext                        DEFAULT NULL,
    `note_entreprises`            varchar(50)                     DEFAULT NULL COMMENT 'Permet de stocker la note commune aux entreprises lors de la phase "Parametrage".',
    `consultation_id`             int(11)                         DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    UNIQUE KEY `referenceUtilisateur` (`referenceUtilisateur`),
    KEY `refConsultation` (`refConsultation`),
    KEY `idEntiteeAssociee` (`idEntiteeAssociee`),
    KEY `organismePE` (`organisme`),
    CONSTRAINT `relation_ParamEnch_organisme` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `relation_parametrage_Enchere_Service` FOREIGN KEY (`idEntiteeAssociee`) REFERENCES `Service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Parametrage_Enchere_Reference`
--

--  `Parametrage_Enchere_Reference`;


CREATE TABLE `Parametrage_Enchere_Reference`
(
    `id`                       int(10) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`                varchar(30)      NOT NULL DEFAULT '',
    `idEnchere`                int(10) unsigned NOT NULL DEFAULT 0,
    `libelle`                  varchar(256)     NOT NULL DEFAULT '',
    `quantite`                 double                    DEFAULT NULL,
    `isMontant`                enum ('0','1')   NOT NULL DEFAULT '1',
    `unite`                    varchar(20)               DEFAULT NULL,
    `pasMin`                   double                    DEFAULT 0,
    `pasMax`                   double                    DEFAULT NULL,
    `valeurReference`          double                    DEFAULT NULL,
    `valeurDepartCommune`      enum ('0','1')   NOT NULL DEFAULT '1',
    `valeurDepart`             double                    DEFAULT NULL,
    `typeBaremeReference`      enum ('1','2','3')        DEFAULT NULL,
    `ponderationNoteReference` double                    DEFAULT 1,
    `noteMaxBaremeRelatif`     double                    DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idEnchere` (`idEnchere`),
    KEY `relation_ParamEnchRef_ParamEnch_idOrg` (`idEnchere`, `organisme`),
    CONSTRAINT `relation_ParamEnchRef_ParamEnch_id` FOREIGN KEY (`idEnchere`) REFERENCES `Parametrage_Enchere` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `relation_ParamEnchRef_ParamEnch_idOrg` FOREIGN KEY (`idEnchere`, `organisme`) REFERENCES `Parametrage_Enchere` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Parametrage_Enchere_Tranche_Bareme_Reference`
--

--  `Parametrage_Enchere_Tranche_Bareme_Reference`;


CREATE TABLE `Parametrage_Enchere_Tranche_Bareme_Reference`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`   varchar(30)      NOT NULL DEFAULT '',
    `idReference` int(10) unsigned NOT NULL DEFAULT 0,
    `borneInf`    double                    DEFAULT NULL,
    `borneSup`    double                    DEFAULT NULL,
    `note`        double                    DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idReference` (`idReference`),
    KEY `Relation_ParamEnchTrancheBaremeRef_ParamEnchRef_IdOrg` (`idReference`, `organisme`),
    CONSTRAINT `Relation_ParamEnchTrancheBaremeRef_Id` FOREIGN KEY (`idReference`) REFERENCES `Parametrage_Enchere_Reference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `Relation_ParamEnchTrancheBaremeRef_ParamEnchRef_IdOrg` FOREIGN KEY (`idReference`, `organisme`) REFERENCES `Parametrage_Enchere_Reference` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Parametrage_Enchere_Tranches_Bareme_NETC`
--

--  `Parametrage_Enchere_Tranches_Bareme_NETC`;


CREATE TABLE `Parametrage_Enchere_Tranches_Bareme_NETC`
(
    `id`        int(10) unsigned NOT NULL AUTO_INCREMENT,
    `organisme` varchar(30)      NOT NULL DEFAULT '',
    `idEnchere` int(10) unsigned NOT NULL DEFAULT 0,
    `borneInf`  double                    DEFAULT NULL,
    `borneSup`  double                    DEFAULT NULL,
    `note`      double                    DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `idEnchere` (`idEnchere`),
    KEY `Relation_ParamEnchTranchesBaremeNETC_ParamEnch_IdOrg` (`idEnchere`, `organisme`),
    CONSTRAINT `Relation_ParamEnchTranchesBaremeNETC_ParamEnch_Id` FOREIGN KEY (`idEnchere`) REFERENCES `Parametrage_Enchere` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `Relation_ParamEnchTranchesBaremeNETC_ParamEnch_IdOrg` FOREIGN KEY (`idEnchere`, `organisme`) REFERENCES `Parametrage_Enchere` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Parametrage_Fiche_Weka`
--

--  `Parametrage_Fiche_Weka`;


CREATE TABLE `Parametrage_Fiche_Weka`
(
    `id_fiche` varchar(256) NOT NULL,
    `pages`    varchar(256) DEFAULT NULL,
    `titre`    varchar(256) DEFAULT NULL,
    `url`      varchar(256) DEFAULT NULL,
    `xml`      text         DEFAULT NULL,
    PRIMARY KEY (`id_fiche`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Partenaire`
--

--  `Partenaire`;


CREATE TABLE `Partenaire`
(
    `id`                 int(11)     NOT NULL AUTO_INCREMENT,
    `initials`           varchar(30) NOT NULL,
    `desc_partenaire`    text        NOT NULL,
    `desc_partenaire_fr` text DEFAULT NULL,
    `desc_partenaire_en` text DEFAULT NULL,
    `desc_partenaire_es` text DEFAULT NULL,
    `desc_partenaire_it` text DEFAULT NULL,
    `desc_partenaire_ar` text DEFAULT NULL,
    `desc_partenaire_su` text DEFAULT NULL,
    `lien_img`           text        NOT NULL,
    `lien_externe`       text        NOT NULL,
    `title`              text        NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Passation_consultation`
--

--  `Passation_consultation`;


CREATE TABLE `Passation_consultation`
(
    `id`                                                        int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`                                                 varchar(30) NOT NULL,
    `reference`                                                 int(11)     NOT NULL DEFAULT 0,
    `unite`                                                     int(5)               DEFAULT 0,
    `mandataire`                                                int(5)               DEFAULT 0,
    `lieu_detaille_predefini`                                   int(5)               DEFAULT 0,
    `lieux_detailles_non_definis`                               varchar(100)         DEFAULT NULL,
    `code_postal_lieu_principal_execution`                      varchar(20)          DEFAULT NULL,
    `Ville`                                                     varchar(50)          DEFAULT NULL,
    `montant_estime_consultation`                               varchar(20)          DEFAULT NULL,
    `commentaires`                                              text                 DEFAULT NULL,
    `numero_deliberation_financiere`                            varchar(20)          DEFAULT NULL,
    `date_deliberation_financiere`                              varchar(10)          DEFAULT NULL,
    `imputation_budgetaire`                                     text                 DEFAULT NULL,
    `numero_deliberation_autorisant_signature_marche`           varchar(20)          DEFAULT NULL,
    `date_deliberation_autorisant_signature_marche`             varchar(10)          DEFAULT NULL,
    `date_notification_previsionnelle`                          varchar(10)          DEFAULT NULL,
    `date_reception_projet_DCE_Service_Validateur`              varchar(10)          DEFAULT NULL,
    `date_formulations_premieres_observations`                  varchar(10)          DEFAULT NULL,
    `date_retour_projet_DCE_finalise`                           varchar(10)          DEFAULT NULL,
    `date_validation_projet_DCE_par_service_validateur`         varchar(10)          DEFAULT NULL,
    `date_validation_projet_DCE_vue_par`                        int(5)               DEFAULT 0,
    `date_reception_projet_AAPC_par_Service_Validateur`         varchar(10)          DEFAULT NULL,
    `date_formulations_premieres_observations_AAPC`             varchar(10)          DEFAULT NULL,
    `date_retour_projet_AAPC_finalise`                          varchar(10)          DEFAULT NULL,
    `date_validation_projet_AAPC_par_Service_Validateur`        varchar(10)          DEFAULT NULL,
    `date_validation_projet_AAPC_par_Service_Validateur_vu_par` int(5)               DEFAULT 0,
    `date_envoi_publicite`                                      varchar(10)          DEFAULT NULL,
    `date_envoi_invitations_remettre_offre`                     varchar(10)          DEFAULT NULL,
    `date_limite_remise_offres`                                 varchar(10)          DEFAULT NULL,
    `delai_validite_offres`                                     varchar(5)           DEFAULT NULL,
    `commentaires_phase_consultation`                           text                 DEFAULT NULL,
    `date_reunion_ouverture_candidatures`                       varchar(10)          DEFAULT NULL,
    `date_reunion_ouverture_offres`                             varchar(10)          DEFAULT NULL,
    `decision`                                                  int(5)               DEFAULT 0,
    `consultation_id`                                           int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Pieces_DCE`
--

--  `Pieces_DCE`;


CREATE TABLE `Pieces_DCE`
(
    `id`                     int(11)        NOT NULL AUTO_INCREMENT,
    `nom_Piece`              varchar(256)   NOT NULL,
    `id_blob`                int(11)        NOT NULL,
    `ref_consultation`       int(11)                 DEFAULT NULL,
    `organisme_consultation` varchar(256)   NOT NULL,
    `document_Redac`         enum ('0','1') NOT NULL DEFAULT '0',
    `id_Redac`               int(11)                 DEFAULT NULL,
    `statut`                 int(11)        NOT NULL DEFAULT 0,
    `Type_Piece`             varchar(256)            DEFAULT NULL,
    `consultation_id`        int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `ref_consultation` (`ref_consultation`),
    KEY `organisme_consultation` (`organisme_consultation`),
    KEY `Pieces_DCE_consultation` (`consultation_id`),
    CONSTRAINT `Pieces_DCE_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Pieces_Mise_Disposition`
--

--  `Pieces_Mise_Disposition`;


CREATE TABLE `Pieces_Mise_Disposition`
(
    `id`                    int(11)     NOT NULL AUTO_INCREMENT,
    `Id_type_doc`           int(11)     NOT NULL COMMENT 'les types de documents possibles',
    `id_decision_enveloppe` int(11)     NOT NULL COMMENT 'attributaire',
    `id_externe`            int(11)              DEFAULT NULL COMMENT 'cet id depond de la valeur du champ type : ex = si type = 0 (piece_dce id_externe = index enveloppe',
    `org`                   varchar(30) NOT NULL,
    `lot`                   int(11)              DEFAULT NULL,
    `consultation_ref`      int(11)              DEFAULT NULL,
    `Date_mise_disposition` datetime    NOT NULL,
    `Statut_disposition`    int(1)      NOT NULL DEFAULT 0 COMMENT '0= non mise à disposition , 1 = mise à disposition',
    `Date_recuperation`     datetime             DEFAULT NULL,
    `Statut_recuperation`   int(1)      NOT NULL DEFAULT 0 COMMENT '0=non recupére ,1 =recupére , 2 = non recupere suite à  une erreur  , 4= acquitte',
    `Message`               varchar(250)         DEFAULT NULL COMMENT 'Le message retourne par l''application externe',
    `Id_blob`               int(11)              DEFAULT NULL,
    `Id_destinataire`       int(11)     NOT NULL DEFAULT 0,
    `type`                  int(1)               DEFAULT NULL COMMENT '0 = piece_dce,1=piece_de _depot,2=piece_publicite,3=autres_pieces',
    `url_externe`           varchar(100)         DEFAULT NULL COMMENT 'Url retourné par le service externe ',
    `consultation_id`       int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `org`, `id_decision_enveloppe`, `Id_destinataire`),
    KEY `Pieces_Mise_Disposition_lot` (`lot`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `INDEX_Pieces_Mise_Disposition_org` (`org`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Prestation`
--

--  `Prestation`;


CREATE TABLE `Prestation`
(
    `id`                   int(11) NOT NULL AUTO_INCREMENT,
    `num_marche`           varchar(256) DEFAULT NULL,
    `type_procedure`       int(11)      DEFAULT NULL,
    `objet`                longtext     DEFAULT NULL,
    `montant`              varchar(256) DEFAULT NULL,
    `maitre_ouvrage`       varchar(256) DEFAULT NULL,
    `date_debut_execution` varchar(20)  DEFAULT NULL,
    `date_fin_execution`   varchar(20)  DEFAULT NULL,
    `id_entreprise`        int(11)      DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `id_entreprise` (`id_entreprise`),
    CONSTRAINT `Prestation_ibfk_1` FOREIGN KEY (`id_entreprise`) REFERENCES `Entreprise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ProcedureEquivalence`
--

--  `ProcedureEquivalence`;


CREATE TABLE `ProcedureEquivalence`
(
    `id_type_procedure`                      int(11)                                     NOT NULL DEFAULT 0,
    `organisme`                              varchar(30)                                 NOT NULL DEFAULT '',
    `elec_resp`                              char(2)                                     NOT NULL DEFAULT '',
    `no_elec_resp`                           char(2)                                     NOT NULL DEFAULT '',
    `cipher_enabled`                         char(2)                                     NOT NULL DEFAULT '',
    `cipher_disabled`                        char(2)                                     NOT NULL DEFAULT '',
    `signature_enabled`                      char(2)                                     NOT NULL DEFAULT '',
    `signature_disabled`                     char(2)                                     NOT NULL DEFAULT '',
    `env_candidature`                        char(2)                                     NOT NULL DEFAULT '',
    `env_offre`                              char(2)                                     NOT NULL DEFAULT '',
    `env_anonymat`                           char(2)                                     NOT NULL DEFAULT '',
    `envoi_complet`                          char(2)                                     NOT NULL DEFAULT '',
    `envoi_differe`                          char(2)                                     NOT NULL DEFAULT '',
    `procedure_publicite`                    char(2)                                     NOT NULL DEFAULT '',
    `procedure_restreinte_candidature`       char(2)                                     NOT NULL DEFAULT '',
    `procedure_restreinte_offre`             char(2)                                     NOT NULL DEFAULT '',
    `envoi_mail_par_mpe`                     char(2)                                     NOT NULL DEFAULT '0',
    `no_envoi_mail_par_mpe`                  char(2)                                     NOT NULL DEFAULT '0',
    `mise_en_ligne1`                         char(2)                                     NOT NULL DEFAULT '',
    `mise_en_ligne2`                         char(2)                                     NOT NULL DEFAULT '',
    `mise_en_ligne3`                         char(2)                                     NOT NULL DEFAULT '',
    `mise_en_ligne4`                         char(2)                                     NOT NULL DEFAULT '',
    `env_offre_type_unique`                  char(2)                                     NOT NULL DEFAULT '',
    `env_offre_type_multiple`                char(2)                                     NOT NULL DEFAULT '',
    `no_fichier_annonce`                     char(2)                                     NOT NULL DEFAULT '',
    `fichier_importe`                        char(2)                                     NOT NULL DEFAULT '',
    `fichier_boamp`                          char(2)                                     NOT NULL DEFAULT '',
    `reglement_cons`                         char(2)                                     NOT NULL DEFAULT '',
    `dossier_dce`                            char(2)                                     NOT NULL DEFAULT '',
    `partial_dce_download`                   char(2)                                     NOT NULL DEFAULT '',
    `service`                                char(2)                                     NOT NULL DEFAULT '',
    `constitution_dossier_reponse`           char(2)                                     NOT NULL DEFAULT '',
    `env_offre_type_unique2`                 char(2)                                     NOT NULL DEFAULT '',
    `env_offre_type_multiple2`               char(2)                                     NOT NULL DEFAULT '',
    `gestion_envois_postaux`                 char(2)                                     NOT NULL DEFAULT '',
    `tireur_plan_non`                        char(2)                                     NOT NULL DEFAULT '',
    `tireur_plan_oui`                        char(2)                                     NOT NULL DEFAULT '',
    `tireur_plan_papier`                     char(2)                                     NOT NULL DEFAULT '',
    `tireur_plan_cdrom`                      char(2)                                     NOT NULL DEFAULT '',
    `tireur_plan_nom`                        char(2)                                     NOT NULL DEFAULT '',
    `tirage_descriptif`                      char(2)                                     NOT NULL DEFAULT '',
    `delai_date_limite_remise_pli`           char(2)                                     NOT NULL DEFAULT '',
    `signature_propre`                       char(2)                                     NOT NULL DEFAULT '',
    `procedure_restreinte`                   char(2)                                     NOT NULL DEFAULT '',
    `ouverture_simultanee`                   char(2)                                     NOT NULL DEFAULT '',
    `type_decision_a_renseigner`             char(2)                                     NOT NULL DEFAULT '-0',
    `type_decision_attribution_marche`       char(2)                                     NOT NULL DEFAULT '-0',
    `type_decision_declaration_sans_suite`   char(2)                                     NOT NULL DEFAULT '-0',
    `type_decision_declaration_infructueux`  char(2)                                     NOT NULL DEFAULT '-0',
    `type_decision_selection_entreprise`     char(2)                                     NOT NULL DEFAULT '-0',
    `type_decision_attribution_accord_cadre` char(2)                                     NOT NULL DEFAULT '-0',
    `type_decision_admission_sad`            char(2)                                     NOT NULL DEFAULT '-0',
    `type_decision_autre`                    char(2)                                     NOT NULL DEFAULT '-0',
    `env_offre_technique`                    char(2)                                     NOT NULL DEFAULT '-0',
    `env_offre_technique_type_unique`        char(2)                                     NOT NULL DEFAULT '-0',
    `env_offre_technique_type_multiple`      char(2)                                     NOT NULL DEFAULT '-0',
    `rep_obligatoire`                        varchar(2)                                  NOT NULL DEFAULT '-0',
    `no_rep_obligatoire`                     varchar(2)                                  NOT NULL DEFAULT '-0',
    `autre_piece_cons`                       char(2)                                     NOT NULL DEFAULT '-0',
    `resp_elec_autre_plateforme`             char(2)                                     NOT NULL DEFAULT '-0',
    `mise_en_ligne_entite_coordinatrice`     char(2)                                     NOT NULL DEFAULT '-0',
    `autoriser_publicite`                    varchar(2)                                  NOT NULL DEFAULT '1',
    `poursuite_date_limite_remise_pli`       char(2)                                     NOT NULL,
    `delai_poursuite_affichage`              varchar(10)                                 NOT NULL COMMENT 'Permet de stocker le delai de poursuite de l''affiche',
    `delai_poursuivre_affichage_unite`       enum ('MINUTE','HOUR','DAY','MONTH','YEAR') NOT NULL DEFAULT 'DAY',
    `mode_ouverture_dossier`                 char(2)                                     NOT NULL DEFAULT '+1',
    `mode_ouverture_reponse`                 char(2)                                     NOT NULL DEFAULT '+0',
    `marche_public_simplifie`                varchar(2)                                  NOT NULL DEFAULT '-0',
    `dume_demande`                           varchar(2)                                  NOT NULL DEFAULT '-0',
    `type_procedure_dume`                    varchar(2)                                  NOT NULL DEFAULT '-0',
    `type_formulaire_dume_standard`          varchar(2)                                  NOT NULL DEFAULT '+1',
    `type_formulaire_dume_simplifie`         varchar(2)                                  NOT NULL DEFAULT '+0',
    `afficher_code_cpv`                      varchar(2)                                           DEFAULT '1',
    `code_cpv_obligatoire`                   varchar(2)                                           DEFAULT '1',
    `donnees_complementaire_non`             varchar(2)                                  NOT NULL DEFAULT '+0',
    `donnees_complementaire_oui`             varchar(2)                                  NOT NULL DEFAULT '+1',
    `signature_autoriser`                    char(2)                                     NOT NULL DEFAULT '+0',
    PRIMARY KEY (`id_type_procedure`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Qualification`
--

--  `Qualification`;


CREATE TABLE `Qualification`
(
    `id`                int(11)     NOT NULL AUTO_INCREMENT COMMENT 'clé primaire',
    `id_interne`        varchar(20) NOT NULL COMMENT 'identifiant interne pour gérer le classement des qualifications',
    `libelle`           varchar(256) DEFAULT NULL,
    `id_interne_parent` varchar(20)  DEFAULT NULL COMMENT 'id interne du parent',
    `libelle_ar`        varchar(256) DEFAULT NULL,
    `libelle_fr`        varchar(256) DEFAULT NULL,
    `libelle_en`        varchar(256) DEFAULT NULL,
    `libelle_es`        varchar(256) DEFAULT NULL,
    `libelle_su`        varchar(256) DEFAULT NULL,
    `libelle_du`        varchar(256) DEFAULT NULL,
    `libelle_cz`        varchar(256) DEFAULT NULL,
    `libelle_it`        varchar(256) DEFAULT NULL,
    `active`            int(11)      DEFAULT 1,
    PRIMARY KEY (`id`),
    KEY `id_interne` (`id_interne`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `QuestionDCE`
--

--  `QuestionDCE`;


CREATE TABLE `QuestionDCE`
(
    `id`               int(22)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL DEFAULT '',
    `consultation_ref` int(11)              DEFAULT NULL,
    `date_depot`       datetime    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `id_inscrit`       int(11)     NOT NULL DEFAULT 0,
    `id_entreprise`    int(11)     NOT NULL DEFAULT 0,
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `organisme` (`organisme`, `consultation_ref`),
    KEY `id_inscrit` (`id_inscrit`),
    KEY `Entreprise` (`id_entreprise`),
    KEY `FK_QuestionDCE_consultation` (`consultation_id`),
    CONSTRAINT `FK_QuestionDCE_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `RG`
--

--  `RG`;


CREATE TABLE `RG`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL DEFAULT '',
    `consultation_ref` int(11)      NOT NULL DEFAULT 0,
    `rg`               int(11)      NOT NULL DEFAULT 0,
    `nom_fichier`      varchar(150) NOT NULL,
    `statut`           char(1)      NOT NULL DEFAULT '1',
    `horodatage`       longblob     NOT NULL,
    `untrusteddate`    datetime     NOT NULL DEFAULT '0000-00-00 00:00:00',
    `agent_id`         int(11)      NOT NULL DEFAULT 0,
    `consultation_id`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `RG_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_RG_consultation` (`consultation_id`),
    CONSTRAINT `FK_RG_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `RPA`
--

--  `RPA`;


CREATE TABLE `RPA`
(
    `id`                  int(11)        NOT NULL AUTO_INCREMENT,
    `acronymeOrg`         varchar(200)   NOT NULL,
    `organisme`           varchar(30)    NOT NULL DEFAULT '',
    `nom`                 varchar(30)    NOT NULL DEFAULT '',
    `prenom`              varchar(30)    NOT NULL DEFAULT '',
    `adresse1`            varchar(80)    NOT NULL DEFAULT '',
    `adresse2`            varchar(80)    NOT NULL DEFAULT '',
    `codepostal`          varchar(20)    NOT NULL DEFAULT '',
    `ville`               varchar(50)    NOT NULL DEFAULT '',
    `id_service`          int(11)        NOT NULL DEFAULT 0,
    `Fonction`            varchar(200)            DEFAULT NULL,
    `pays`                varchar(150)            DEFAULT NULL,
    `date_creation`       varchar(20)             DEFAULT NULL,
    `date_modification`   varchar(20)             DEFAULT NULL,
    `responsable_archive` enum ('0','1') NOT NULL DEFAULT '0',
    `email`               varchar(50)             DEFAULT NULL,
    `telephone`           varchar(50)             DEFAULT NULL,
    `fax`                 varchar(50)             DEFAULT NULL,
    PRIMARY KEY (`id`, `acronymeOrg`),
    KEY `organisme` (`acronymeOrg`),
    CONSTRAINT `RPA_ORGANISME_ORG` FOREIGN KEY (`acronymeOrg`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Referentiel`
--

--  `Referentiel`;


CREATE TABLE `Referentiel`
(
    `id_referentiel`      int(10)      NOT NULL AUTO_INCREMENT,
    `libelle_referentiel` varchar(200) NOT NULL DEFAULT '',
    PRIMARY KEY (`id_referentiel`),
    KEY `id_referentiel` (`id_referentiel`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ReferentielOrg`
--

--  `ReferentielOrg`;


CREATE TABLE `ReferentielOrg`
(
    `id_referentiel`      int(10)      NOT NULL AUTO_INCREMENT,
    `organisme`           varchar(30)  NOT NULL DEFAULT '',
    `libelle_referentiel` varchar(200) NOT NULL DEFAULT '',
    PRIMARY KEY (`id_referentiel`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Referentiel_Agent`
--

--  `Referentiel_Agent`;


CREATE TABLE `Referentiel_Agent`
(
    `id`                               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`                        varchar(20) NOT NULL,
    `id_Agent`                         int(100)    NOT NULL,
    `id_Lt_Referentiel`                int(100)    NOT NULL,
    `valeur_Principale_Lt_Referentiel` text        NOT NULL,
    `valeur_Secondaire_Lt_Referentiel` text        NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Referentiel_Consultation`
--

--  `Referentiel_Consultation`;


CREATE TABLE `Referentiel_Consultation`
(
    `id`                               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`                        varchar(20) NOT NULL,
    `reference`                        int(100)             DEFAULT NULL,
    `id_Lt_Referentiel`                int(100)    NOT NULL,
    `lot`                              int(11)     NOT NULL DEFAULT 0,
    `valeur_Principale_Lt_Referentiel` text        NOT NULL,
    `valeur_Secondaire_Lt_Referentiel` text        NOT NULL,
    `consultation_id`                  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Referentiel_Entreprise`
--

--  `Referentiel_Entreprise`;


CREATE TABLE `Referentiel_Entreprise`
(
    `id`                               int(11)      NOT NULL AUTO_INCREMENT,
    `id_entreprise`                    int(100)     NOT NULL,
    `id_Lt_Referentiel`                int(100)     NOT NULL,
    `lot`                              int(11)      NOT NULL DEFAULT 0,
    `valeur_Principale_Lt_Referentiel` varchar(200) NOT NULL,
    `valeur_Secondaire_Lt_Referentiel` varchar(200) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Referentiel_org_denomination`
--

--  `Referentiel_org_denomination`;


CREATE TABLE `Referentiel_org_denomination`
(
    `id`                               int(11)        NOT NULL AUTO_INCREMENT,
    `denomination`                     varchar(250)   NOT NULL,
    `denomination_adapte`              varchar(250)            DEFAULT NULL,
    `denomination_normalise`           varchar(250)            DEFAULT NULL COMMENT 'La valeur propre utilisé dans l''auto complétion',
    `traite`                           enum ('0','1')          DEFAULT '0',
    `actif_recherche_avancee`          enum ('0','1') NOT NULL DEFAULT '0',
    `date_maj_actif_recherche_avancee` varchar(20)             DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `index_denomination_adapte` (`denomination_adapte`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Relation_Echange`
--

--  `Relation_Echange`;


CREATE TABLE `Relation_Echange`
(
    `id_auto`       int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id_echange`    int(11)          NOT NULL DEFAULT 0,
    `organisme`     varchar(30)      NOT NULL DEFAULT '',
    `id_externe`    int(11)          NOT NULL DEFAULT 0,
    `type_relation` int(11)          NOT NULL DEFAULT 0,
    `date_envoi`    varchar(50)               DEFAULT NULL,
    PRIMARY KEY (`id_auto`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ReponseInscritFormulaireConsultation`
--

--  `ReponseInscritFormulaireConsultation`;


CREATE TABLE `ReponseInscritFormulaireConsultation`
(
    `id`                         int(11) NOT NULL AUTO_INCREMENT,
    `id_Formulaire_consultation` int(11) NOT NULL,
    `statut`                     int(11) NOT NULL,
    `id_inscrit`                 int(11) NOT NULL,
    `id_entreprise`              int(11) NOT NULL,
    `total_bd_ht`                varchar(30) DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ReponseInscritItemFormulaireConsultationValues`
--

--  `ReponseInscritItemFormulaireConsultationValues`;


CREATE TABLE `ReponseInscritItemFormulaireConsultationValues`
(
    `id`                                         int(11)      NOT NULL AUTO_INCREMENT,
    `idItemFormulaireConsultation`               int(11)      NOT NULL,
    `idReponseInscritItemFormulaireConsultation` int(11)      NOT NULL,
    `valeur`                                     varchar(100) NOT NULL,
    `type_valeur`                                int(11)      NOT NULL,
    `precision_entreprise`                       varchar(100) NOT NULL,
    `prix_unitaire`                              varchar(30)  NOT NULL,
    `tva`                                        varchar(30)  NOT NULL,
    PRIMARY KEY (`id`),
    KEY `idItemFormulaireConsultation` (`idItemFormulaireConsultation`),
    CONSTRAINT `ReponseInscritItemFormulaireConsultationValues_ibfk_1` FOREIGN KEY (`idItemFormulaireConsultation`) REFERENCES `ItemFormulaireConsultation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Retrait_Papier`
--

--  `Retrait_Papier`;


CREATE TABLE `Retrait_Papier`
(
    `id`                   int(22)            NOT NULL AUTO_INCREMENT,
    `organisme`            varchar(30)        NOT NULL DEFAULT '',
    `consultation_ref`     int(11)            NOT NULL DEFAULT 0,
    `nom`                  varchar(100)       NOT NULL DEFAULT '',
    `adresse`              varchar(100)       NOT NULL DEFAULT '',
    `entreprise`           varchar(100)       NOT NULL DEFAULT '',
    `datetelechargement`   datetime           NOT NULL DEFAULT '0000-00-00 00:00:00',
    `codepostal`           varchar(5)         NOT NULL DEFAULT '0',
    `ville`                varchar(50)        NOT NULL DEFAULT '',
    `pays`                 varchar(50)        NOT NULL DEFAULT '',
    `telephone`            varchar(20)        NOT NULL DEFAULT '',
    `tirage_plan`          int(11)            NOT NULL DEFAULT 0,
    `siret`                varchar(14)                 DEFAULT NULL,
    `fax`                  varchar(30)        NOT NULL DEFAULT '',
    `email`                varchar(100)       NOT NULL DEFAULT '',
    `Observation`          text                        DEFAULT NULL,
    `prenom`               varchar(100)                DEFAULT NULL,
    `adresse2`             varchar(100)                DEFAULT NULL,
    `identifiant_national` varchar(20)                 DEFAULT NULL,
    `acronyme_pays`        varchar(10)                 DEFAULT NULL,
    `support`              enum ('1','2','3') NOT NULL DEFAULT '1',
    `consultation_id`      int(11)                     DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `Retrait_Papier_consultation` (`organisme`, `consultation_ref`),
    KEY `FK_Retrait_Papier_consultation` (`consultation_id`),
    CONSTRAINT `FK_Retrait_Papier_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;





--
-- Table structure for table `Service_Mertier_Profils`
--

--  `Service_Mertier_Profils`;


CREATE TABLE `Service_Mertier_Profils`
(
    `id_auto`                          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id_interne`                       int(11)          NOT NULL DEFAULT 0,
    `id_service_metier`                int(11)          NOT NULL DEFAULT 0,
    `id_externe`                       int(11)          NOT NULL DEFAULT 0,
    `libelle`                          varchar(256)              DEFAULT NULL,
    `libelle_fr`                       varchar(256)              DEFAULT NULL,
    `libelle_en`                       varchar(256)              DEFAULT NULL,
    `libelle_es`                       varchar(256)              DEFAULT NULL,
    `libelle_su`                       varchar(256)              DEFAULT NULL,
    `libelle_du`                       varchar(256)              DEFAULT NULL,
    `libelle_cz`                       varchar(256)              DEFAULT NULL,
    `libelle_ar`                       varchar(256)              DEFAULT NULL,
    `libelle_it`                       varchar(256)              DEFAULT NULL,
    `show_profile_for_hyperadmin_only` enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le profil juste pour l''hyperAdmin si la valeur egale 1 sinon il va s''afficher pour touts les agents',
    PRIMARY KEY (`id_auto`),
    KEY `id_service_metier` (`id_service_metier`),
    CONSTRAINT `Service_Mertier_Profils_ibfk_1` FOREIGN KEY (`id_service_metier`) REFERENCES `Service_Mertier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 17
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Societes_Exclues`
--

--  `Societes_Exclues`;


CREATE TABLE `Societes_Exclues`
(
    `id_societes_exclues`    int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clé primaire',
    `organisme_acronyme`     varchar(30)    DEFAULT NULL,
    `id_service`             int(11)        DEFAULT NULL,
    `id_agent`               int(11)        DEFAULT NULL,
    `nom_document`           varchar(256)   DEFAULT NULL,
    `id_blob`                int(11)        DEFAULT NULL,
    `taille_document`        varchar(80)    DEFAULT NULL,
    `identifiant_entreprise` varchar(20)    DEFAULT NULL,
    `raison_sociale`         varchar(256)   DEFAULT NULL,
    `motif`                  varchar(256)   DEFAULT NULL,
    `raison_sociale_fr`      varchar(256)   DEFAULT NULL,
    `motif_fr`               varchar(256)   DEFAULT NULL,
    `raison_sociale_ar`      varchar(256)   DEFAULT NULL,
    `motif_ar`               varchar(256)   DEFAULT NULL,
    `type_exclusion`         enum ('0','1') DEFAULT '0',
    `date_debut_exclusion`   varchar(20)    DEFAULT NULL,
    `date_fin_exclusion`     varchar(20)    DEFAULT NULL,
    `type_portee`            enum ('0','1') DEFAULT '0',
    `raison_sociale_it`      varchar(256)   DEFAULT NULL,
    `motif_it`               varchar(256)   DEFAULT NULL,
    PRIMARY KEY (`id_societes_exclues`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Socle_Habilitation_Agent`
--

--  `Socle_Habilitation_Agent`;


CREATE TABLE `Socle_Habilitation_Agent`
(
    `id_agent`                     int(11)        NOT NULL,
    `gestion_agent_pole_socle`     enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_agents_socle`         enum ('0','1') NOT NULL DEFAULT '0',
    `droit_gestion_services_socle` enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id_agent`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `SousCategorie`
--

--  `SousCategorie`;


CREATE TABLE `SousCategorie`
(
    `id`                varchar(30)    NOT NULL DEFAULT '',
    `id_categorie`      varchar(50)    NOT NULL DEFAULT '',
    `libelle`           varchar(250)   NOT NULL,
    `libelle_ar`        varchar(250)            DEFAULT NULL,
    `libelle_fr`        varchar(250)            DEFAULT NULL,
    `libelle_en`        varchar(250)            DEFAULT NULL,
    `libelle_es`        varchar(250)            DEFAULT NULL,
    `libelle_su`        varchar(250)            DEFAULT NULL,
    `libelle_du`        varchar(250)            DEFAULT NULL,
    `libelle_cz`        varchar(250)            DEFAULT NULL,
    `libelle_it`        varchar(250)            DEFAULT NULL,
    `date_modification` varchar(20)             DEFAULT NULL,
    `code`              varchar(10)             DEFAULT NULL,
    `active`            enum ('0','1') NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Sso_Tiers`
--

--  `Sso_Tiers`;


CREATE TABLE `Sso_Tiers`
(
    `id_sso_tiers`      varchar(50) NOT NULL,
    `id_tiers`          int(11)     NOT NULL,
    `id_fonctionnalite` varchar(50) NOT NULL,
    `date_connexion`    varchar(20) NOT NULL,
    `date_last_request` varchar(20) NOT NULL,
    PRIMARY KEY (`id_sso_tiers`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `StatutEnveloppe`
--

--  `StatutEnveloppe`;


CREATE TABLE `StatutEnveloppe`
(
    `id_statut`   int(2)       NOT NULL DEFAULT 0,
    `description` varchar(100) NOT NULL DEFAULT '',
    PRIMARY KEY (`id_statut`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `SuiviAcces`
--

--  `SuiviAcces`;


CREATE TABLE `SuiviAcces`
(
    `id_auto`    int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id_agent`   int(8)           NOT NULL DEFAULT 0,
    `date_acces` varchar(20)      NOT NULL DEFAULT '',
    `id_service` int(8)                    DEFAULT NULL,
    `nom`        varchar(100)              DEFAULT NULL,
    `prenom`     varchar(100)              DEFAULT NULL,
    `email`      varchar(100)              DEFAULT NULL,
    `organisme`  varchar(100)              DEFAULT NULL,
    PRIMARY KEY (`id_auto`),
    KEY `suivi_acces_date_acces` (`date_acces`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 11956
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `T_Illustration_Fond`
--

--  `T_Illustration_Fond`;


CREATE TABLE `T_Illustration_Fond`
(
    `id_illustration_fond` int(11)        NOT NULL AUTO_INCREMENT,
    `libelle`              varchar(256)   NOT NULL,
    `actif`                enum ('0','1') NOT NULL DEFAULT '0',
    `nom_image`            varchar(256)            DEFAULT NULL,
    `id_blob_image`        varchar(50)             DEFAULT NULL,
    `id_agent`             int(11)        NOT NULL DEFAULT 0,
    PRIMARY KEY (`id_illustration_fond`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `T_MesRecherches`
--

--  `T_MesRecherches`;


CREATE TABLE `T_MesRecherches`
(
    `id`                      int(11)        NOT NULL AUTO_INCREMENT,
    `id_createur`             int(11)        NOT NULL DEFAULT 0,
    `type_createur`           varchar(20)    NOT NULL,
    `denomination`            varchar(200)   NOT NULL DEFAULT '',
    `periodicite`             char(1)        NOT NULL DEFAULT '',
    `xmlCriteria`             text                    DEFAULT NULL,
    `categorie`               varchar(30)             DEFAULT NULL,
    `id_initial`              int(11)        NOT NULL DEFAULT 0,
    `format`                  varchar(3)     NOT NULL DEFAULT '1',
    `date_creation`           datetime                DEFAULT NULL,
    `date_modification`       datetime                DEFAULT NULL,
    `recherche`               enum ('0','1') NOT NULL DEFAULT '0',
    `alerte`                  enum ('0','1') NOT NULL DEFAULT '0',
    `type_avis`               int(2)                  DEFAULT 3,
    `plateforme_virtuelle_id` int(11) unsigned        DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `mes_recherches_id_createur_type_createur` (`id_createur`, `type_createur`),
    KEY `fk_plateforme_virtuelle_t_mesrecherches` (`plateforme_virtuelle_id`),
    CONSTRAINT `fk_plateforme_virtuelle_t_mesrecherches` FOREIGN KEY (`plateforme_virtuelle_id`) REFERENCES `plateforme_virtuelle` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 185
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `T_Profil_Joue`
--

--  `T_Profil_Joue`;


CREATE TABLE `T_Profil_Joue`
(
    `id`                                      int(11)                                                 NOT NULL AUTO_INCREMENT,
    `id_agent`                                int(11)                                                 NOT NULL,
    `organisme`                               varchar(10)   NOT NULL,
    `nom_officiel`                            varchar(256)  NOT NULL,
    `pays`                                    varchar(50)   NOT NULL,
    `ville`                                   varchar(50)   NOT NULL,
    `adresse`                                 varchar(50)   NOT NULL,
    `code_postal`                             varchar(50)  DEFAULT NULL,
    `point_contact`                           varchar(256)  NOT NULL,
    `a_attention_de`                          varchar(50)   NOT NULL,
    `telephone`                               varchar(50)  DEFAULT NULL,
    `fax`                                     varchar(50)  DEFAULT NULL,
    `email`                                   varchar(50)  DEFAULT NULL,
    `adresse_pouvoir_adjudicateur`            varchar(256) DEFAULT NULL,
    `adresse_profil_acheteur`                 varchar(256) DEFAULT NULL,
    `autorite_nationale`                      int(2)       DEFAULT NULL,
    `office_nationale`                        int(2)       DEFAULT NULL,
    `collectivite_territoriale`               int(2)       DEFAULT NULL,
    `office_regionale`                        int(2)       DEFAULT NULL,
    `organisme_public`                        int(2)       DEFAULT NULL,
    `organisation_europenne`                  int(2)       DEFAULT NULL,
    `autre_type_pouvoir_adjudicateur`         int(2)       DEFAULT NULL,
    `autre_libelle_type_pouvoir_adjudicateur` varchar(50)  DEFAULT NULL,
    `services_generaux`                       int(2)       DEFAULT NULL,
    `defense`                                 int(2)       DEFAULT NULL,
    `securite_public`                         int(2)       DEFAULT NULL,
    `environnement`                           int(2)       DEFAULT NULL,
    `affaires_economiques`                    int(2)       DEFAULT NULL,
    `sante`                                   int(2)       DEFAULT NULL,
    `developpement_collectif`                 int(2)       DEFAULT NULL,
    `protection_sociale`                      int(2)       DEFAULT NULL,
    `loisirs`                                 int(2)       DEFAULT NULL,
    `eduction`                                int(2)       DEFAULT NULL,
    `autre_activites_principales`             int(2)       DEFAULT NULL,
    `autre_libelle_activites_principales`     varchar(50)  DEFAULT NULL,
    `pouvoir_adjudicateur_agit`               int(2)       DEFAULT NULL,
    `pouvoir_adjudicateur_marche_couvert`     int(2)       DEFAULT NULL,
    `entite_adjudicatrice_marche_couvert`     int(2)                                                  NOT NULL,
    `numero_national_identification`          varchar(20)  DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `T_Telechargement_Asynchrone`
--

--  `T_Telechargement_Asynchrone`;


CREATE TABLE `T_Telechargement_Asynchrone`
(
    `id`                         int(11)        NOT NULL AUTO_INCREMENT,
    `id_agent`                   int(11)        NOT NULL DEFAULT 0 COMMENT 'Identifiant technique de l''agent qui effectue le téléchargement',
    `nom_prenom_agent`           varchar(50)    NOT NULL DEFAULT '',
    `email_agent`                varchar(50)    NOT NULL DEFAULT '',
    `id_service_agent`           int(11)        NOT NULL DEFAULT 0,
    `organisme_agent`            varchar(50)    NOT NULL DEFAULT '',
    `nom_fichier_telechargement` varchar(50)    NOT NULL DEFAULT '',
    `taille_fichier`             int(11)        NOT NULL DEFAULT 0,
    `date_generation`            datetime                DEFAULT NULL,
    `id_blob_fichier`            int(11)        NOT NULL DEFAULT 0,
    `tag_fichier_genere`         enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'A ''1'' si fichier généré, ''0'' sinon',
    `tag_fichier_supprime`       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'A ''1'' si fichier supprimé, ''0'' sinon',
    `type_telechargement`        int(11)        NOT NULL DEFAULT 0 COMMENT 'Précise la fonctionnalité à laquelle le téléchargement fait référence',
    PRIMARY KEY (`id`),
    KEY `id_agent` (`id_agent`),
    KEY `tag_fichier_supprime` (`tag_fichier_supprime`),
    KEY `tag_fichier_genere` (`tag_fichier_genere`),
    KEY `date_generation` (`date_generation`),
    KEY `id_service_agent` (`id_service_agent`),
    KEY `organisme_agent` (`organisme_agent`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Permet de gérer le téléchargement asynchrone des archives de consulattions';


--
-- Table structure for table `T_Telechargement_Asynchrone_fichier`
--

--  `T_Telechargement_Asynchrone_fichier`;


CREATE TABLE `T_Telechargement_Asynchrone_fichier`
(
    `id`                           int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire',
    `id_telechargement_asynchrone` int(11) NOT NULL DEFAULT 0 COMMENT 'Correspond à l''identifiant technique de la table "T_Telechargement_Asynchrone"',
    `id_reference_telechargement`  int(11) NOT NULL DEFAULT 0 COMMENT 'Correspond à l''identifiant auquel le téléchargement fait référence (exemple: ref_consultation)',
    PRIMARY KEY (`id`),
    KEY `id_telechargement_asynchrone` (`id_telechargement_asynchrone`),
    CONSTRAINT `id_telech_asynch_fichier_id_telech_asynch` FOREIGN KEY (`id_telechargement_asynchrone`) REFERENCES `T_Telechargement_Asynchrone` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Permet de definir les détails de téléchargements de la table';


--
-- Table structure for table `T_Theme_Graphique`
--

--  `T_Theme_Graphique`;


CREATE TABLE `T_Theme_Graphique`
(
    `id_theme_graphique` int(11)        NOT NULL AUTO_INCREMENT,
    `code`               varchar(256)   NOT NULL COMMENT 'ce code est utilisé pour désigner quel thème on va utiliser. Remarque : il ne faut pas ajouter un thème avec le code qu''on a pour le thème par défaut qui se trouve dans le fichier de paramétrage',
    `libelle`            varchar(256)   NOT NULL,
    `actif`              enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id_theme_graphique`),
    UNIQUE KEY `code` (`code`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `T_Traduction`
--

--  `T_Traduction`;


CREATE TABLE `T_Traduction`
(
    `langue`     varchar(2) NOT NULL,
    `id_libelle` int(11)    NOT NULL,
    `libelle`    text       NOT NULL,
    PRIMARY KEY (`id_libelle`, `langue`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `T_trace_operations_inscrit_details`
--

--  `T_trace_operations_inscrit_details`;


CREATE TABLE `T_trace_operations_inscrit_details`
(
    `id`                       int(11)        NOT NULL AUTO_INCREMENT,
    `id_trace`                 int(11)        NOT NULL,
    `date_debut_action`        datetime                DEFAULT NULL,
    `nom_action`               varchar(100)            DEFAULT NULL,
    `details`                  varchar(50)             DEFAULT NULL,
    `date_fin_action`          datetime                DEFAULT NULL,
    `id_description`           int(10)                 DEFAULT NULL,
    `afficher`                 enum ('0','1') NOT NULL DEFAULT '0',
    `descripton`               text           NOT NULL,
    `log_applet`               text           NOT NULL,
    `lien_download`            varchar(250)            DEFAULT NULL COMMENT 'contient l url de telechargement du pdf recap du mail du detais de la reponse envoyé à l''inscrit',
    `infos_browser`            text                    DEFAULT NULL COMMENT 'Contient les infos sur le navigateur de l''utilisateur',
    `date_debut_action_client` datetime                DEFAULT NULL COMMENT 'Contient la date de debut du poste client',
    `id_offre`                 int(11)                 DEFAULT NULL COMMENT 'Contient l''id de l''offre si elle existe',
    `debut_action_millisecond` bigint(20)              DEFAULT NULL COMMENT 'Contient le nombre de millisecondes correspondant a la date de debut',
    PRIMARY KEY (`id`),
    KEY `Fk_Trace` (`id_trace`),
    KEY `t_trace_operations_inscrit_details_date_debut_action` (`date_debut_action`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Telechargement`
--

--  `Telechargement`;


CREATE TABLE `Telechargement`
(
    `id`                      int(22)             NOT NULL AUTO_INCREMENT,
    `organisme`               varchar(30)         NOT NULL DEFAULT '',
    `consultation_ref`        int(11)                      DEFAULT NULL,
    `datetelechargement`      datetime            NOT NULL DEFAULT '0000-00-00 00:00:00',
    `tirage_plan`             int(11)             NOT NULL DEFAULT 0,
    `id_inscrit`              int(11)                      DEFAULT 0,
    `id_entreprise`           int(11)                      DEFAULT 0,
    `support`                 enum ('1','2','3')  NOT NULL DEFAULT '1',
    `nom`                     varchar(100)        NOT NULL DEFAULT '',
    `adresse`                 varchar(100)        NOT NULL DEFAULT '',
    `email`                   varchar(100)        NOT NULL DEFAULT '',
    `entreprise`              varchar(100)        NOT NULL DEFAULT '',
    `codepostal`              varchar(5)          NOT NULL DEFAULT '0',
    `ville`                   varchar(50)         NOT NULL DEFAULT '',
    `pays`                    varchar(50)                  DEFAULT NULL,
    `telephone`               varchar(20)         NOT NULL DEFAULT '',
    `siret`                   varchar(14)         NOT NULL DEFAULT '',
    `fax`                     varchar(30)         NOT NULL DEFAULT '',
    `lots`                    varchar(256)                 DEFAULT '',
    `sirenEtranger`           varchar(20)                  DEFAULT '0',
    `adresse2`                varchar(80)         NOT NULL DEFAULT '0',
    `prenom`                  varchar(11)                  DEFAULT '',
    `noms_fichiers_dce`       text                         DEFAULT NULL,
    `Observation`             text                         DEFAULT NULL,
    `acronyme_pays`           varchar(10)                  DEFAULT NULL,
    `poids_telechargement`    bigint(20) unsigned NOT NULL DEFAULT 0 COMMENT 'Taille des fichiers téléchargés',
    `consultation_id`         int(11)                      DEFAULT NULL,
    `plateforme_virtuelle_id` int(11) unsigned             DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `organisme` (`organisme`, `consultation_ref`),
    KEY `telechargement_datetelechargement` (`datetelechargement`),
    KEY `FK_Telechargement_consultation` (`consultation_id`),
    KEY `fk_plateforme_virtuelle_telechargement` (`plateforme_virtuelle_id`),
    CONSTRAINT `FK_Telechargement_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_plateforme_virtuelle_telechargement` FOREIGN KEY (`plateforme_virtuelle_id`) REFERENCES `plateforme_virtuelle` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `TelechargementAnonyme`
--

--  `TelechargementAnonyme`;


CREATE TABLE `TelechargementAnonyme`
(
    `id`                   int(22)             NOT NULL AUTO_INCREMENT,
    `organisme`            varchar(30)         NOT NULL DEFAULT '',
    `consultation_ref`     int(11)                      DEFAULT NULL,
    `datetelechargement`   datetime            NOT NULL,
    `tirage_plan`          int(11)             NOT NULL DEFAULT 0,
    `support`              enum ('1','2','3')  NOT NULL DEFAULT '1',
    `noms_fichiers_dce`    text                         DEFAULT NULL,
    `poids_telechargement` bigint(20) unsigned NOT NULL DEFAULT 0 COMMENT 'Taille des fichiers téléchargés',
    `consultation_id`      int(11)                      DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `organisme` (`organisme`, `consultation_ref`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `telechargementAnonyme_datetelechargement` (`datetelechargement`),
    KEY `FK_TelechargementAnonyme_consultation` (`consultation_id`),
    CONSTRAINT `FK_TelechargementAnonyme_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Tiers`
--

--  `Tiers`;


CREATE TABLE `Tiers`
(
    `id_tiers`       int(11)      NOT NULL AUTO_INCREMENT,
    `login`          varchar(50)  NOT NULL,
    `password`       varchar(50)  NOT NULL,
    `denomination`   varchar(250) NOT NULL,
    `fonctionnalite` varchar(50)  NOT NULL,
    `organisme`      varchar(30) DEFAULT NULL,
    PRIMARY KEY (`id_tiers`),
    KEY `Tiers_organisme_FK` (`organisme`),
    CONSTRAINT `Tiers_organisme_FK` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `TireurPlan`
--

--  `TireurPlan`;


CREATE TABLE `TireurPlan`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`  varchar(30)  NOT NULL DEFAULT '',
    `nom`        varchar(100) NOT NULL DEFAULT '',
    `email`      varchar(100) NOT NULL DEFAULT '',
    `id_service` int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Tranche_Article_133`
--

--  `Tranche_Article_133`;


CREATE TABLE `Tranche_Article_133`
(
    `id`                         int(11)      NOT NULL AUTO_INCREMENT,
    `acronyme_org`               varchar(30)  NOT NULL,
    `millesime`                  varchar(10)  NOT NULL,
    `Libelle_tranche_budgetaire` varchar(250) NOT NULL,
    `borne_inf`                  varchar(40)  NOT NULL,
    `borne_sup`                  varchar(40)  NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `TypeAvenant`
--

--  `TypeAvenant`;


CREATE TABLE `TypeAvenant`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `libelle`    varchar(100) DEFAULT NULL,
    `libelle_fr` varchar(100) DEFAULT NULL,
    `libelle_en` varchar(100) DEFAULT NULL,
    `libelle_es` varchar(100) DEFAULT NULL,
    `libelle_su` varchar(100) DEFAULT NULL,
    `libelle_du` varchar(100) DEFAULT NULL,
    `libelle_cz` varchar(100) DEFAULT NULL,
    `libelle_ar` varchar(100) DEFAULT NULL,
    `libelle_it` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `TypeAvis`
--

--  `TypeAvis`;


CREATE TABLE `TypeAvis`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `intitule_avis`    varchar(100) NOT NULL DEFAULT '',
    `intitule_avis_fr` varchar(100)          DEFAULT NULL,
    `intitule_avis_en` varchar(100)          DEFAULT NULL,
    `intitule_avis_es` varchar(100)          DEFAULT NULL,
    `intitule_avis_su` varchar(100)          DEFAULT NULL,
    `intitule_avis_du` varchar(100)          DEFAULT NULL,
    `intitule_avis_cz` varchar(100)          DEFAULT NULL,
    `intitule_avis_ar` varchar(100)          DEFAULT NULL,
    `abbreviation`     varchar(50)  NOT NULL DEFAULT '',
    `id_type_avis_ANM` int(11)      NOT NULL DEFAULT 0,
    `intitule_avis_it` varchar(100)          DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `TypeCommission`
--

--  `TypeCommission`;


CREATE TABLE `TypeCommission`
(
    `id`        int(5)      NOT NULL AUTO_INCREMENT,
    `organisme` varchar(10) NOT NULL,
    `libelle`   varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `TypeDecision`
--

--  `TypeDecision`;


CREATE TABLE `TypeDecision`
(
    `id_type_decision`   int(11)      NOT NULL AUTO_INCREMENT,
    `code_type_decision` varchar(100) NOT NULL COMMENT 'Pour obtenir le libellé, ce champ est mis en correspondance avec messages.xml',
    PRIMARY KEY (`id_type_decision`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `TypeProcedure`
--

--  `TypeProcedure`;


CREATE TABLE `TypeProcedure`
(
    `id_type_procedure`         int(11)        NOT NULL AUTO_INCREMENT,
    `libelle_type_procedure`    varchar(100)   NOT NULL DEFAULT '',
    `libelle_type_procedure_fr` varchar(100)            DEFAULT '',
    `libelle_type_procedure_en` varchar(100)            DEFAULT '',
    `libelle_type_procedure_es` varchar(100)            DEFAULT '',
    `libelle_type_procedure_su` varchar(100)            DEFAULT '',
    `libelle_type_procedure_du` varchar(100)            DEFAULT '',
    `libelle_type_procedure_cz` varchar(100)            DEFAULT '',
    `libelle_type_procedure_ar` varchar(100)            DEFAULT NULL,
    `abbreviation`              varchar(50)    NOT NULL DEFAULT '',
    `type_boamp`                int(11)        NOT NULL DEFAULT 0,
    `categorie_procedure`       int(11)        NOT NULL DEFAULT 0,
    `id_type_procedure_ANM`     int(11)        NOT NULL DEFAULT 0,
    `delai_alerte`              int(11)        NOT NULL DEFAULT 0,
    `mapa`                      enum ('0','1') NOT NULL DEFAULT '0',
    `consultation_transverse`   enum ('0','1') NOT NULL DEFAULT '0',
    `code_recensement`          char(3)                 DEFAULT NULL,
    `abbreviation_portail_ANM`  varchar(50)    NOT NULL,
    `id_modele`                 int(1)         NOT NULL DEFAULT 0,
    `libelle_type_procedure_it` varchar(100)            DEFAULT NULL,
    `value_binding_sub`         varchar(256)   NOT NULL DEFAULT '' COMMENT 'ce champs est utilisé afin de faire la correspondance entre valeur MPE et SUB',
    PRIMARY KEY (`id_type_procedure`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Type_Avis_Pub`
--

--  `Type_Avis_Pub`;


CREATE TABLE `Type_Avis_Pub`
(
    `id`                   int(11)      NOT NULL AUTO_INCREMENT,
    `libelle`              varchar(100) NOT NULL,
    `libelle_ar`           varchar(100)          DEFAULT NULL,
    `libelle_cz`           varchar(100)          DEFAULT NULL,
    `libelle_du`           varchar(100)          DEFAULT NULL,
    `libelle_en`           varchar(100)          DEFAULT NULL,
    `libelle_es`           varchar(100)          DEFAULT NULL,
    `libelle_fr`           varchar(100)          DEFAULT NULL,
    `libelle_it`           varchar(100)          DEFAULT NULL,
    `libelle_su`           varchar(100)          DEFAULT NULL,
    `region`               int(11)      NOT NULL,
    `resource_formulaire`  varchar(100) NOT NULL,
    `ressource_doc_presse` varchar(256)          DEFAULT NULL COMMENT 'Permet de stocker les noms des ressources des documens generés et envoyés à la presse',
    `nature_avis`          int(2)                DEFAULT NULL COMMENT 'Permet de préciser la nature de l''avis de publicité, 1 pour les avis pré information, 2 pour les avis initiaux',
    `id_dispositif`        int(11)      NOT NULL DEFAULT 0,
    `type_pub`             int(11)      NOT NULL DEFAULT 0 COMMENT '0 : Esender, 1: SUB, 2: TED ',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Type_Avis_Pub_Organisme`
--

--  `Type_Avis_Pub_Organisme`;


CREATE TABLE `Type_Avis_Pub_Organisme`
(
    `id_type_avis`        int(11)      NOT NULL,
    `organisme`           varchar(30)  NOT NULL,
    `id_type_procedure`   int(11)      NOT NULL,
    `libelle`             varchar(100) NOT NULL,
    `libelle_ar`          varchar(100) DEFAULT NULL,
    `libelle_cz`          varchar(100) DEFAULT NULL,
    `libelle_du`          varchar(100) DEFAULT NULL,
    `libelle_en`          varchar(100) DEFAULT NULL,
    `libelle_es`          varchar(100) DEFAULT NULL,
    `libelle_fr`          varchar(100) DEFAULT NULL,
    `libelle_it`          varchar(100) DEFAULT NULL,
    `libelle_su`          varchar(100) DEFAULT NULL,
    `region`              int(11)      NOT NULL,
    `resource_formulaire` varchar(100) NOT NULL,
    PRIMARY KEY (`id_type_avis`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Type_Avis_Pub_Procedure`
--

--  `Type_Avis_Pub_Procedure`;


CREATE TABLE `Type_Avis_Pub_Procedure`
(
    `id_type_avis`      int(11)     NOT NULL,
    `organisme`         varchar(30) NOT NULL,
    `id_type_procedure` int(11)     NOT NULL,
    PRIMARY KEY (`id_type_avis`, `organisme`, `id_type_procedure`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Type_Procedure_Organisme`
--

--  `Type_Procedure_Organisme`;


CREATE TABLE `Type_Procedure_Organisme`
(
    `id_type_procedure`               int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`                       varchar(30)    NOT NULL DEFAULT '',
    `libelle_type_procedure`          varchar(100)   NOT NULL DEFAULT '',
    `abbreviation`                    varchar(50)    NOT NULL DEFAULT '',
    `type_boamp`                      int(11)        NOT NULL DEFAULT 0,
    `id_type_procedure_portail`       int(11)        NOT NULL DEFAULT 0,
    `categorie_procedure`             int(11)        NOT NULL DEFAULT 0,
    `delai_alerte`                    int(11)        NOT NULL DEFAULT 0,
    `id_type_validation`              int(11)        NOT NULL DEFAULT 2,
    `service_validation`              int(11)        NOT NULL DEFAULT 0,
    `mapa`                            enum ('0','1') NOT NULL DEFAULT '0',
    `activer_mapa`                    enum ('0','1') NOT NULL DEFAULT '1',
    `libelle_type_procedure_fr`       varchar(100)            DEFAULT '',
    `libelle_type_procedure_en`       varchar(100)            DEFAULT '',
    `libelle_type_procedure_es`       varchar(100)            DEFAULT '',
    `libelle_type_procedure_su`       varchar(100)            DEFAULT '',
    `libelle_type_procedure_du`       varchar(100)            DEFAULT '',
    `libelle_type_procedure_cz`       varchar(100)            DEFAULT '',
    `libelle_type_procedure_ar`       varchar(100)            DEFAULT NULL,
    `id_montant_mapa`                 int(2)         NOT NULL DEFAULT 0,
    `code_recensement`                char(3)                 DEFAULT NULL,
    `depouillable_phase_consultation` enum ('0','1') NOT NULL DEFAULT '0',
    `consultation_transverse`         enum ('0','1') NOT NULL DEFAULT '0',
    `tag_Boamp`                       varchar(50)    NOT NULL DEFAULT '',
    `ao`                              enum ('0','1') NOT NULL DEFAULT '0',
    `mn`                              enum ('0','1') NOT NULL DEFAULT '0',
    `dc`                              enum ('0','1') NOT NULL DEFAULT '0',
    `autre`                           enum ('0','1') NOT NULL DEFAULT '0',
    `sad`                             enum ('0','1') NOT NULL DEFAULT '0',
    `accord_cadre`                    enum ('0','1') NOT NULL DEFAULT '0',
    `pn`                              enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'tag de la procédure "Procédure avec négociation"',
    `tag_name_mesure_avancement`      varchar(100)   NOT NULL,
    `abreviation_interface`           varchar(50)             DEFAULT NULL,
    `libelle_type_procedure_it`       varchar(100)            DEFAULT NULL,
    `publicite_types_form_xml`        text           NOT NULL COMMENT 'Permet de parametrer les types de formulaires au format xml dans la publicité en fonction du type de procedure. Les valeurs de ce champ doivent etre separés par des ''#''',
    `tag_name_chorus`                 varchar(100)   NOT NULL,
    `equivalent_opoce`                varchar(100)   NOT NULL,
    `equivalent_boamp`                varchar(100)   NOT NULL COMMENT 'Représente le type de procédure envoyé dans le fichier xml du BOAMP',
    `ordre_affichage`                 int(11)                 DEFAULT 0 COMMENT 'Permet de définir l''ordre d''affichage du type de procédure par ordre décroissant ',
    `id_externe`                      varchar(256)            DEFAULT '' COMMENT 'Id unique sauf pour les MAPA',
    PRIMARY KEY (`id_type_procedure`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `Type_support`
--

--  `Type_support`;


CREATE TABLE `Type_support`
(
    `id_type_avis`    int(11) NOT NULL,
    `id_type_support` int(11) NOT NULL COMMENT 'valeur possible pour type destinataire: opoce->1 // press->2 // simap2->3',
    PRIMARY KEY (`id_type_avis`, `id_type_support`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ValeurReferentiel`
--

--  `ValeurReferentiel`;


CREATE TABLE `ValeurReferentiel`
(
    `id`                            int(10)      NOT NULL AUTO_INCREMENT,
    `id_referentiel`                int(10)      NOT NULL DEFAULT 0,
    `libelle_valeur_referentiel`    text         NOT NULL,
    `libelle_valeur_referentiel_fr` text         NOT NULL,
    `libelle_valeur_referentiel_en` text         NOT NULL,
    `libelle_valeur_referentiel_es` text         NOT NULL,
    `libelle_valeur_referentiel_su` text         NOT NULL,
    `libelle_valeur_referentiel_du` text         NOT NULL,
    `libelle_valeur_referentiel_cz` text         NOT NULL,
    `libelle_valeur_referentiel_ar` text         NOT NULL,
    `libelle_2`                     varchar(200) NOT NULL DEFAULT '',
    `libelle_valeur_referentiel_it` text         NOT NULL,
    `valeur_sub`                    varchar(256) NOT NULL,
    `ordre_affichage`               int(11)      NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`, `id_referentiel`),
    KEY `id_referentiel` (`id_referentiel`),
    CONSTRAINT `ValeurReferentiel_ibfk_1` FOREIGN KEY (`id_referentiel`) REFERENCES `Referentiel` (`id_referentiel`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ValeurReferentielOrg`
--

--  `ValeurReferentielOrg`;


CREATE TABLE `ValeurReferentielOrg`
(
    `id`                            int(10)      NOT NULL AUTO_INCREMENT,
    `organisme`                     varchar(30)  NOT NULL DEFAULT '',
    `id_referentiel`                int(10)      NOT NULL DEFAULT 0,
    `libelle_valeur_referentiel`    text         NOT NULL,
    `libelle_valeur_referentiel_fr` text         NOT NULL,
    `libelle_valeur_referentiel_en` text         NOT NULL,
    `libelle_valeur_referentiel_es` text         NOT NULL,
    `libelle_valeur_referentiel_su` text         NOT NULL,
    `libelle_valeur_referentiel_du` text         NOT NULL,
    `libelle_valeur_referentiel_cz` text         NOT NULL,
    `libelle_valeur_referentiel_ar` text         NOT NULL,
    `libelle_valeur_referentiel_it` text         NOT NULL,
    `valeur_sub`                    varchar(256) NOT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Temporary table structure for view `_Vue_Consultation`
--

DROP TABLE IF EXISTS `_Vue_Consultation`;
/*!50001 DROP VIEW IF EXISTS `_Vue_Consultation`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `_Vue_Consultation`
(
    `datefin`                             tinyint NOT NULL,
    `date_mise_en_ligne_calcule`          tinyint NOT NULL,
    `reference`                           tinyint NOT NULL,
    `organisme`                           tinyint NOT NULL,
    `Ref_Orga`                            tinyint NOT NULL,
    `consultation restreinte`             tinyint NOT NULL,
    `Nom_organisme`                       tinyint NOT NULL,
    `active`                              tinyint NOT NULL,
    `service_id`                          tinyint NOT NULL,
    `Nom_service`                         tinyint NOT NULL,
    `reference_utilisateur`               tinyint NOT NULL,
    `categorie`                           tinyint NOT NULL,
    `Nom_categorie`                       tinyint NOT NULL,
    `id_type_procedure_org`               tinyint NOT NULL,
    `Nom_Procedure_Agent`                 tinyint NOT NULL,
    `SUBSTRING(consultation.objet,1,100)` tinyint NOT NULL,
    `marche_public_simplifie`             tinyint NOT NULL,
    `id_type_avis`                        tinyint NOT NULL,
    `alloti`                              tinyint NOT NULL,
    `nbre_lots`                           tinyint NOT NULL,
    `Equiv_Lots`                          tinyint NOT NULL,
    `Taille_DCE_Mo`                       tinyint NOT NULL,
    `nbre_retrait_NON_ANO`                tinyint NOT NULL,
    `nbre_retrait_ANONYME`                tinyint NOT NULL,
    `nbre_reponse_Elec`                   tinyint NOT NULL,
    `dont_reponse_MPS`                    tinyint NOT NULL,
    `Poids des réponses en Mo`            tinyint NOT NULL
) ENGINE = MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `_Vue_Consultation_sup_a_2015`
--

DROP TABLE IF EXISTS `_Vue_Consultation_sup_a_2015`;
/*!50001 DROP VIEW IF EXISTS `_Vue_Consultation_sup_a_2015`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `_Vue_Consultation_sup_a_2015`
(
    `datefin`                             tinyint NOT NULL,
    `date_mise_en_ligne_calcule`          tinyint NOT NULL,
    `reference`                           tinyint NOT NULL,
    `organisme`                           tinyint NOT NULL,
    `Ref_Orga`                            tinyint NOT NULL,
    `consultation restreinte`             tinyint NOT NULL,
    `Nom_organisme`                       tinyint NOT NULL,
    `active`                              tinyint NOT NULL,
    `service_id`                          tinyint NOT NULL,
    `Nom_service`                         tinyint NOT NULL,
    `reference_utilisateur`               tinyint NOT NULL,
    `categorie`                           tinyint NOT NULL,
    `Nom_categorie`                       tinyint NOT NULL,
    `id_type_procedure_org`               tinyint NOT NULL,
    `Nom_Procedure_Agent`                 tinyint NOT NULL,
    `SUBSTRING(consultation.objet,1,100)` tinyint NOT NULL,
    `marche_public_simplifie`             tinyint NOT NULL,
    `id_type_avis`                        tinyint NOT NULL,
    `alloti`                              tinyint NOT NULL,
    `nbre_lots`                           tinyint NOT NULL,
    `Equiv_Lots`                          tinyint NOT NULL,
    `Taille_DCE_Mo`                       tinyint NOT NULL,
    `nbre_retrait_NON_ANO`                tinyint NOT NULL,
    `nbre_retrait_ANONYME`                tinyint NOT NULL,
    `nbre_reponse_Elec`                   tinyint NOT NULL,
    `dont_reponse_MPS`                    tinyint NOT NULL,
    `Poids des réponses en Mo`            tinyint NOT NULL,
    `idAgent`                             tinyint NOT NULL,
    `nomPrenomAgent`                      tinyint NOT NULL,
    `mailAgent`                           tinyint NOT NULL,
    `telephoneAgent`                      tinyint NOT NULL
) ENGINE = MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `_Vue_Offre`
--

DROP TABLE IF EXISTS `_Vue_Offre`;
/*!50001 DROP VIEW IF EXISTS `_Vue_Offre`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `_Vue_Offre`
(
    `id`                     tinyint NOT NULL,
    `borneInf_DateDepot`     tinyint NOT NULL,
    `borneSup_DateDepot`     tinyint NOT NULL,
    `organisme`              tinyint NOT NULL,
    `Nom_organisme`          tinyint NOT NULL,
    `active`                 tinyint NOT NULL,
    `consultation_ref`       tinyint NOT NULL,
    `Ref_Agent`              tinyint NOT NULL,
    `Objet`                  tinyint NOT NULL,
    `entreprise_id`          tinyint NOT NULL,
    `nom_entreprise_inscrit` tinyint NOT NULL,
    `reponse_MPS`            tinyint NOT NULL,
    `Poids_réponse_Mo`       tinyint NOT NULL,
    `Nbr_Env`                tinyint NOT NULL,
    `Nbr_Fichiers`           tinyint NOT NULL,
    `uid_offre`              tinyint NOT NULL,
    `statut_offres`          tinyint NOT NULL,
    `envoi_complet`          tinyint NOT NULL,
    `created_at`             tinyint NOT NULL,
    `date_depot`             tinyint NOT NULL
) ENGINE = MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `_Vue_Offre_sup_a_2015`
--

DROP TABLE IF EXISTS `_Vue_Offre_sup_a_2015`;
/*!50001 DROP VIEW IF EXISTS `_Vue_Offre_sup_a_2015`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `_Vue_Offre_sup_a_2015`
(
    `borneInf_DateDepot`     tinyint NOT NULL,
    `borneSup_DateDepot`     tinyint NOT NULL,
    `id`                     tinyint NOT NULL,
    `organisme`              tinyint NOT NULL,
    `Nom_organisme`          tinyint NOT NULL,
    `active`                 tinyint NOT NULL,
    `consultation_ref`       tinyint NOT NULL,
    `Ref_Agent`              tinyint NOT NULL,
    `Objet`                  tinyint NOT NULL,
    `entreprise_id`          tinyint NOT NULL,
    `nom_entreprise_inscrit` tinyint NOT NULL,
    `reponse_MPS`            tinyint NOT NULL,
    `Poids_réponse_Mo`       tinyint NOT NULL,
    `Nbr_Env`                tinyint NOT NULL,
    `Nbr_Fichiers`           tinyint NOT NULL,
    `uid_offre`              tinyint NOT NULL,
    `statut_offres`          tinyint NOT NULL,
    `envoi_complet`          tinyint NOT NULL,
    `created_at`             tinyint NOT NULL,
    `date_depot`             tinyint NOT NULL,
    `Horo_Moins_Depot`       tinyint NOT NULL,
    `horo_Moins_Creation`    tinyint NOT NULL
) ENGINE = MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `_Vue_Telechargement`
--

DROP TABLE IF EXISTS `_Vue_Telechargement`;
/*!50001 DROP VIEW IF EXISTS `_Vue_Telechargement`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `_Vue_Telechargement`
(
    `id`                          tinyint NOT NULL,
    `borneInf_DateTelechargement` tinyint NOT NULL,
    `borneSup_DateTelechargement` tinyint NOT NULL,
    `organisme`                   tinyint NOT NULL,
    `Nom_organisme`               tinyint NOT NULL,
    `active`                      tinyint NOT NULL,
    `consultation_ref`            tinyint NOT NULL,
    `Ref_Agent`                   tinyint NOT NULL,
    `Objet`                       tinyint NOT NULL,
    `id_entreprise`               tinyint NOT NULL,
    `entreprise`                  tinyint NOT NULL,
    `nom`                         tinyint NOT NULL,
    `email`                       tinyint NOT NULL,
    `codepostal`                  tinyint NOT NULL,
    `ville`                       tinyint NOT NULL,
    `pays`                        tinyint NOT NULL
) ENGINE = MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `_Vue_Telechargement_Anonyme`
--

DROP TABLE IF EXISTS `_Vue_Telechargement_Anonyme`;
/*!50001 DROP VIEW IF EXISTS `_Vue_Telechargement_Anonyme`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `_Vue_Telechargement_Anonyme`
(
    `id`                          tinyint NOT NULL,
    `borneInf_DateTelechargement` tinyint NOT NULL,
    `borneSup_DateTelechargement` tinyint NOT NULL,
    `organisme`                   tinyint NOT NULL,
    `Nom_organisme`               tinyint NOT NULL,
    `active`                      tinyint NOT NULL,
    `consultation_ref`            tinyint NOT NULL,
    `Ref_Agent`                   tinyint NOT NULL,
    `Objet`                       tinyint NOT NULL
) ENGINE = MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `_Vue_Telechargement_Anonyme_sup_a_2015`
--

DROP TABLE IF EXISTS `_Vue_Telechargement_Anonyme_sup_a_2015`;
/*!50001 DROP VIEW IF EXISTS `_Vue_Telechargement_Anonyme_sup_a_2015`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `_Vue_Telechargement_Anonyme_sup_a_2015`
(
    `id`                          tinyint NOT NULL,
    `borneInf_DateTelechargement` tinyint NOT NULL,
    `borneSup_DateTelechargement` tinyint NOT NULL,
    `organisme`                   tinyint NOT NULL,
    `Nom_organisme`               tinyint NOT NULL,
    `active`                      tinyint NOT NULL,
    `consultation_ref`            tinyint NOT NULL,
    `Ref_Agent`                   tinyint NOT NULL,
    `Objet`                       tinyint NOT NULL
) ENGINE = MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `_Vue_Telechargement_sup_a_2015`
--

DROP TABLE IF EXISTS `_Vue_Telechargement_sup_a_2015`;
/*!50001 DROP VIEW IF EXISTS `_Vue_Telechargement_sup_a_2015`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `_Vue_Telechargement_sup_a_2015`
(
    `id`                          tinyint NOT NULL,
    `borneInf_DateTelechargement` tinyint NOT NULL,
    `borneSup_DateTelechargement` tinyint NOT NULL,
    `organisme`                   tinyint NOT NULL,
    `Nom_organisme`               tinyint NOT NULL,
    `active`                      tinyint NOT NULL,
    `consultation_ref`            tinyint NOT NULL,
    `Ref_Agent`                   tinyint NOT NULL,
    `Objet`                       tinyint NOT NULL,
    `id_entreprise`               tinyint NOT NULL,
    `entreprise`                  tinyint NOT NULL,
    `nom`                         tinyint NOT NULL,
    `email`                       tinyint NOT NULL,
    `codepostal`                  tinyint NOT NULL,
    `ville`                       tinyint NOT NULL,
    `pays`                        tinyint NOT NULL
) ENGINE = MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `agent_technique_association`
--

--  `agent_technique_association`;


CREATE TABLE `agent_technique_association`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `id_agent`   int(11) NOT NULL,
    `organisme`  varchar(50)      DEFAULT NULL,
    `id_service` int(11) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE KEY `association_unique` (`id_agent`, `organisme`, `id_service`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `agent_technique_token`
--

--  `agent_technique_token`;


CREATE TABLE `agent_technique_token`
(
    `id`              int(11)      NOT NULL AUTO_INCREMENT,
    `agent_id`        int(11)      NOT NULL,
    `token`           varchar(256) NOT NULL,
    `create_at`       datetime     NOT NULL,
    `update_at`       datetime     NOT NULL,
    `date_expiration` datetime     NOT NULL,
    PRIMARY KEY (`id`),
    KEY `agent_technique_token_agent_id_fk` (`agent_id`),
    CONSTRAINT `agent_technique_token_agent_id_fk` FOREIGN KEY (`agent_id`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `alerte_metier`
--

--  `alerte_metier`;


CREATE TABLE `alerte_metier`
(
    `id`             int(11)            NOT NULL AUTO_INCREMENT,
    `id_alerte`      int(11)            NOT NULL,
    `date_creation`  datetime                    DEFAULT NULL,
    `date_cloture`   datetime                    DEFAULT NULL,
    `organisme`      varchar(30)                 DEFAULT NULL,
    `reference_cons` int(11)                     DEFAULT NULL,
    `lot`            int(11)                     DEFAULT NULL,
    `service_id`     int(11)                     DEFAULT NULL,
    `cloturee`       enum ('0','1','2') NOT NULL DEFAULT '2',
    `message`        longtext                    DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `api_user`
--

--  `api_user`;


CREATE TABLE `api_user`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `username`   varchar(50)  NOT NULL,
    `password`   varchar(50)  NOT NULL,
    `roles`      varchar(256) NOT NULL,
    `token`      varchar(256) DEFAULT NULL,
    `expires_at` datetime     DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `archive_arcade`
--

--  `archive_arcade`;


CREATE TABLE `archive_arcade`
(
    `id`                  int(11)    NOT NULL AUTO_INCREMENT,
    `annee`               int(11)    NOT NULL,
    `num_semaine`         int(11)    NOT NULL,
    `poids_archive`       int(11)             DEFAULT NULL,
    `chemin_fichier`      varchar(256)        DEFAULT NULL,
    `date_envoi_debut`    datetime            DEFAULT NULL,
    `date_envoi_fin`      datetime            DEFAULT NULL,
    `status_transmission` tinyint(1) NOT NULL DEFAULT 0,
    `erreur`              varchar(256)        DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `IDX_ANNNEE_SEMAINE` (`annee`, `num_semaine`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


--
-- Table structure for table `autre_piece_consultation`
--

--  `autre_piece_consultation`;


CREATE TABLE `autre_piece_consultation`
(
    `id`              int(11)  NOT NULL AUTO_INCREMENT,
    `blob_id`         int(11)  NOT NULL,
    `consultation_id` int(11)  NOT NULL,
    `created_at`      datetime NOT NULL,
    `updated_at`      datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `autre_piece_consultation_consultation_id_fk` (`consultation_id`),
    KEY `autre_piece_consultation_blob_id_fk` (`blob_id`),
    CONSTRAINT `autre_piece_consultation_blob_id_fk` FOREIGN KEY (`blob_id`) REFERENCES `blobOrganisme_file` (`id`),
    CONSTRAINT `autre_piece_consultation_consultation_id_fk` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `backup_Intervenant_Externe`
--

--  `backup_Intervenant_Externe`;


CREATE TABLE `backup_Intervenant_Externe`
(
    `id`              int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`       varchar(30)  NOT NULL,
    `nom`             varchar(30)  NOT NULL DEFAULT '',
    `prenom`          varchar(30)  NOT NULL DEFAULT '',
    `organisation`    varchar(30)  NOT NULL DEFAULT '',
    `fonction`        varchar(200) NOT NULL DEFAULT '',
    `adresse`         varchar(256) NOT NULL DEFAULT '',
    `code_postal`     varchar(50)  NOT NULL,
    `ville`           varchar(256) NOT NULL,
    `mail`            varchar(100) NOT NULL DEFAULT '',
    `type_invitation` int(1)       NOT NULL DEFAULT 1,
    `civilite`        varchar(256)          DEFAULT '',
    `type_voix`       varchar(50)  NOT NULL,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `backup_Ordre_Du_Jour`
--

--  `backup_Ordre_Du_Jour`;


CREATE TABLE `backup_Ordre_Du_Jour`
(
    `id`                     int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`              varchar(30)  NOT NULL,
    `id_commission`          int(11)      NOT NULL DEFAULT 0,
    `ref_consultation`       varchar(256)          DEFAULT NULL,
    `ref_libre`              varchar(50)           DEFAULT NULL,
    `id_etape`               int(11)      NOT NULL,
    `intitule_ordre_du_jour` longtext              DEFAULT NULL,
    `lots_odj_libre`         varchar(100)          DEFAULT NULL,
    `id_type_procedure`      int(11)               DEFAULT NULL,
    `type_procedure_libre`   varchar(256)          DEFAULT NULL,
    `date_cloture`           datetime              DEFAULT NULL,
    `type_env`               int(1)                DEFAULT NULL,
    `sous_pli`               int(2)                DEFAULT NULL,
    `heure`                  varchar(5)   NOT NULL DEFAULT '00',
    `etape_consultation`     varchar(200) NOT NULL DEFAULT '',
    `type_consultation`      varchar(200) NOT NULL DEFAULT '',
    `minutes`                char(2)      NOT NULL DEFAULT '',
    `service`                varchar(256)          DEFAULT NULL,
    `id_service`             int(10)               DEFAULT NULL,
    `date_debut`             varchar(256)          DEFAULT NULL,
    `consultation_id`        int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_commission` (`id_commission`),
    KEY `ref_Consultation` (`ref_consultation`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_CAO_Commission`
--

--  `t_CAO_Commission`;


CREATE TABLE `t_CAO_Commission`
(
    `id_commission` bigint(20)                                              NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de la commission',
    `organisme`     varchar(30)                                             NOT NULL COMMENT 'Acronyme de l''organisme à laquelle appartient la commission',
    `sigle`         char(10)     NOT NULL COMMENT 'Sigle de la commission',
    `intitule`      varchar(256)  NOT NULL COMMENT 'Intitulé de la commission',
    PRIMARY KEY (`id_commission`, `organisme`),
    KEY `sigle_idx` (`sigle`),
    KEY `organisme_idx` (`organisme`),
    CONSTRAINT `t_CAO_Commission_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  




--
-- Table structure for table `t_CAO_Seance`
--

--  `t_CAO_Seance`;


CREATE TABLE `t_CAO_Seance`
(
    `id_seance`         bigint(20)   NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de la séance',
    `organisme`         varchar(30)  NOT NULL COMMENT 'Acronyme de l''organisme pour lequel a lieu la séance',
    `id_commission`     bigint(11)   NOT NULL COMMENT 'Identifiant de la commission définissant la séance',
    `date`              datetime     NOT NULL COMMENT 'Horaire de la séance',
    `lieu`              varchar(200) NOT NULL,
    `salle`             varchar(100) NOT NULL,
    `id_ref_val_statut` int(11)      NOT NULL COMMENT 'Statut à l''issue de la séance',
    PRIMARY KEY (`id_seance`, `organisme`),
    KEY `lieu_idx` (`lieu`),
    KEY `date_idx` (`date`),
    KEY `id_commission_idx` (`id_commission`),
    KEY `organisme_idx` (`organisme`),
    CONSTRAINT `t_CAO_Seance_id_commission_fk` FOREIGN KEY (`id_commission`) REFERENCES `t_CAO_Commission` (`id_commission`),
    CONSTRAINT `t_CAO_Seance_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `t_CAO_Commission_Consultation`
--

--  `t_CAO_Commission_Consultation`;


CREATE TABLE `t_CAO_Commission_Consultation`
(
    `id_commission_consultation` bigint(20)                                          NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de la consultation de commission',
    `id_commission`              bigint(20)                                          NOT NULL COMMENT 'Identifiant de la commission',
    `ref_consultation`           int(11) DEFAULT NULL,
    `ref_libre`                  varchar(50)                                         NOT NULL COMMENT 'Référence de la consultation saisie manuellement',
    `organisme`                  varchar(30)                                         NOT NULL COMMENT 'Acronyme de l''organisme à laquelle appartient la commission',
    `intitule`                   longtext  NOT NULL COMMENT 'Intitulé de la consultation de commission',
    `id_type_procedure`          int(10)                                             NOT NULL COMMENT 'Identifiant du type de procédure de la consultation',
    `id_categorie`               tinyint(4)                                          NOT NULL COMMENT 'Id de la catégorie de la consultation',
    `service_gestionnaire`       varchar(256)                                        NOT NULL COMMENT 'Service gestionnaire de la consultation saisi manuellement',
    `id_service_gestionnaire`    int(10)                                             NOT NULL COMMENT 'Identifiant du service gestionnaire de la consultation',
    `service_associe`            varchar(256)                                        NOT NULL COMMENT 'Service associé de la consultation saisi manuellement',
    `id_service_associe`         int(10)                                             NOT NULL COMMENT 'Identifiant du service associé de la consultation',
    `date_cloture`               datetime                                            NOT NULL COMMENT 'Date de clôture de la consultation',
    `date_mise_en_ligne`         datetime                                            NOT NULL COMMENT 'Date de mise en ligne de la consultation',
    `consultation_id`            int(11) DEFAULT NULL,
    PRIMARY KEY (`id_commission_consultation`, `organisme`),
    KEY `ref_consultation_idx` (`ref_consultation`),
    KEY `id_commission_idx` (`id_commission`),
    KEY `organisme_idx` (`organisme`),
    CONSTRAINT `t_CAO_Commission_Consultation_id_commission_fk` FOREIGN KEY (`id_commission`) REFERENCES `t_CAO_Commission` (`id_commission`),
    CONSTRAINT `t_CAO_Commission_Consultation_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `t_CAO_Intervenant_Externe`
--

--  `t_CAO_Intervenant_Externe`;


CREATE TABLE `t_CAO_Intervenant_Externe`
(
    `id_intervenant_externe`        bigint(20)   NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l''intervenant externe',
    `organisme`                     varchar(30)  NOT NULL,
    `id_ref_val_civilite`           int(11)      NOT NULL,
    `nom`                           varchar(50)  NOT NULL,
    `prenom`                        varchar(50)  NOT NULL,
    `organisation`                  varchar(50)  NOT NULL,
    `fonction`                      varchar(100) NOT NULL,
    `id_ref_val_type_voix_defaut`   int(11)      NOT NULL COMMENT 'Type de voix par défaut de l''intervenant externe',
    `adresse`                       varchar(256) NOT NULL DEFAULT '',
    `code_postal`                   char(15)     NOT NULL,
    `ville`                         varchar(100) NOT NULL,
    `email`                         varchar(100) NOT NULL DEFAULT '',
    `id_ref_val_mode_communication` tinyint(4)   NOT NULL COMMENT 'Mode de communication avec l''intervenant externe',
    PRIMARY KEY (`id_intervenant_externe`, `organisme`),
    KEY `nom_idx` (`nom`),
    KEY `organisation_idx` (`organisation`),
    KEY `organisme_idx` (`organisme`),
    CONSTRAINT `t_CAO_Intervenant_Externe_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `blob_file`
--

--  `blob_file`;


CREATE TABLE `blob_file`
(
    `old_id`            int(11)              DEFAULT NULL,
    `name`              text        NOT NULL,
    `deletion_datetime` datetime             DEFAULT NULL,
    `statut_synchro`    int(11)     NOT NULL DEFAULT 0 COMMENT '0 : non vérifié / 1 : Vérification ok / 2 : fichier absent',
    `hash`              varchar(256)         DEFAULT 'ND' COMMENT 'Empreinte du blob, SHA1 du fichier / ND : Valeur par défaut pour non définit',
    `dossier`           varchar(256)         DEFAULT NULL COMMENT 'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.',
    `chemin`            text                 DEFAULT NULL,
    `extension`         varchar(50) NOT NULL DEFAULT '' COMMENT 'extension du fichier après -0 (.dce, .brouillon, .rc ...)',
    `id`                int(11)     NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`),
    KEY `blob_file_statut_synchro` (`statut_synchro`),
    KEY `blob_file_old_id` (`old_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `fichierEnveloppe`
--

--  `fichierEnveloppe`;


CREATE TABLE `fichierEnveloppe`
(
    `id_fichier`                    int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`                     varchar(30)    NOT NULL DEFAULT '',
    `id_enveloppe`                  int(11)        NOT NULL DEFAULT 0,
    `type_fichier`                  char(3)        NOT NULL DEFAULT '',
    `num_ordre_fichier`             int(5)         NOT NULL DEFAULT 0,
    `nom_fichier`                   text           NOT NULL,
    `taille_fichier`                varchar(50)    NOT NULL DEFAULT '',
    `signature_fichier`             mediumtext              DEFAULT NULL,
    `hash`                          text           NOT NULL,
    `verification_certificat`       varchar(5)     NOT NULL DEFAULT '',
    `id_blob`                       int(11)                 DEFAULT NULL,
    `id_blob_signature`             int(11)                 DEFAULT NULL COMMENT 'Permet de stocker l''identifiant du blob de la signature du fichier',
    `type_piece`                    int(10)        NOT NULL DEFAULT 3 COMMENT '1: pièces jointes, 2: editions SUB, 3: pièces libres, 4: pièces typées',
    `id_type_piece`                 int(10)        NOT NULL DEFAULT 0 COMMENT 'Permet de distinguer les pièces typées (typpes pièces=4)',
    `is_hash`                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Ce champ est à ''1'' si le fichier est de nature hash, ''0'' sinon',
    `nom_referentiel_certificat`    varchar(256)            DEFAULT NULL COMMENT 'Le nom de réferentiel qui contient le certificat',
    `statut_referentiel_certificat` int(1)                  DEFAULT NULL COMMENT 'Le statut du réferentiel du certificat( OK,NOK...)',
    `nom_referentiel_fonctionnel`   varchar(256)            DEFAULT NULL,
    `message`                       varchar(256)            DEFAULT NULL,
    `date_signature`                varchar(20)             DEFAULT NULL,
    `signature_infos`               text                    DEFAULT NULL COMMENT 'Permet de stocker les infos signatures renvoyées par le serveur crypto',
    `signature_infos_date`          datetime                DEFAULT NULL COMMENT 'Permet de stocker la date de sauvegarde des infos de la signature (champ ''signature_infos'')',
    `id_fichier_signature`          int(11)                 DEFAULT NULL COMMENT 'Contient l''id du fichier signature du fichier courant',
    `uid_response`                  text                    DEFAULT NULL COMMENT 'Unique ID du depot de l''offre',
    `type_signature_fichier`        varchar(50)    NOT NULL DEFAULT '' COMMENT 'Type signature : XADES, CADES ou PADES',
    `hash256`                       text           NOT NULL COMMENT 'hash 256 du fichier',
    `resultat_verification_hash`    char(1)                 DEFAULT '1' COMMENT 'Contient le resultat de la vÃ©rification du hash du fichier, 0=>KO,1=>OK',
    PRIMARY KEY (`id_fichier`, `organisme`),
    KEY `id_enveloppe` (`id_enveloppe`),
    KEY `fichierEnveloppe_organisme_idx` (`organisme`),
    KEY `fichierEnveloppe_id_fichier_signature_idx` (`id_fichier_signature`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `blocFichierEnveloppe`
--

--  `blocFichierEnveloppe`;


CREATE TABLE `blocFichierEnveloppe`
(
    `id_bloc_fichier`   int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`         varchar(30) NOT NULL DEFAULT '',
    `id_fichier`        int(11)     NOT NULL DEFAULT 0,
    `numero_ordre_bloc` int(5)      NOT NULL DEFAULT 0,
    `id_blob_chiffre`   int(11)     NOT NULL DEFAULT 0,
    `id_blob_dechiffre` int(11)              DEFAULT NULL,
    PRIMARY KEY (`id_bloc_fichier`, `organisme`),
    KEY `id_fichier` (`id_fichier`),
    KEY `blocFichierEnveloppe_fichierEnveloppe` (`id_fichier`, `organisme`),
    CONSTRAINT `blocFichierEnveloppe_fichierEnveloppe` FOREIGN KEY (`id_fichier`, `organisme`) REFERENCES `fichierEnveloppe` (`id_fichier`, `organisme`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `blocFichierEnveloppeTemporaire`
--

--  `blocFichierEnveloppeTemporaire`;


CREATE TABLE `blocFichierEnveloppeTemporaire`
(
    `id`                   int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`            varchar(30) NOT NULL DEFAULT '',
    `uid`                  varchar(50) NOT NULL DEFAULT '',
    `type_env`             int(1)      NOT NULL DEFAULT 0,
    `sous_pli`             int(3)      NOT NULL DEFAULT 0,
    `type_fichier`         char(3)     NOT NULL DEFAULT '',
    `numero_ordre_fichier` int(5)      NOT NULL DEFAULT 0,
    `numero_ordre_bloc`    int(5)      NOT NULL DEFAULT 0,
    `id_blob`              int(11)     NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `categories_considerations_sociales`
--

--  `categories_considerations_sociales`;


CREATE TABLE `categories_considerations_sociales`
(
    `id`    int(11) NOT NULL AUTO_INCREMENT,
    `label` text    NOT NULL,
    `code`  text DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `ccag_applicable`
--

--  `ccag_applicable`;


CREATE TABLE `ccag_applicable`
(
    `id_auto`   int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id`        int(11)          NOT NULL DEFAULT 0,
    `organisme` varchar(5)       NOT NULL,
    `libelle`   varchar(50)               DEFAULT NULL,
    PRIMARY KEY (`id_auto`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `changement_heure`
--

--  `changement_heure`;


CREATE TABLE `changement_heure`
(
    `organisme`       varchar(30) NOT NULL DEFAULT '',
    `date_debut_zone` datetime    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `date_fin_zone`   datetime    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `decalage`        char(3)     NOT NULL DEFAULT '0',
    PRIMARY KEY (`date_debut_zone`, `date_fin_zone`, `organisme`),
    KEY `organisme` (`organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `chorus_noms_fichiers`
--

--  `chorus_noms_fichiers`;


CREATE TABLE `chorus_noms_fichiers`
(
    `id_echange`         int(11)      NOT NULL,
    `numero_ordre`       int(10)      NOT NULL COMMENT 'Numéro d''ordre pour le cas des FSO',
    `nom_fichier`        varchar(200) NOT NULL,
    `acronyme_organisme` varchar(200) NOT NULL,
    `type_fichier`       char(3)      NOT NULL DEFAULT 'FEN',
    `date_ajout`         varchar(20)           DEFAULT NULL COMMENT 'Renseigne la date d''ajout d''un nouveau fichier',
    PRIMARY KEY (`id_echange`, `nom_fichier`),
    KEY `organisme` (`acronyme_organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `chorus_numeros_marches`
--

--  `chorus_numeros_marches`;


CREATE TABLE `chorus_numeros_marches`
(
    `numero_marche`      varchar(250) NOT NULL DEFAULT '',
    `acronyme_organisme` varchar(100) NOT NULL DEFAULT '',
    `id_decision`        int(11)      NOT NULL DEFAULT 0,
    PRIMARY KEY (`numero_marche`),
    KEY `organisme` (`acronyme_organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `code_retour`
--

--  `code_retour`;


CREATE TABLE `code_retour`
(
    `code`        int(11) NOT NULL,
    `description` text    NOT NULL,
    PRIMARY KEY (`code`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `compte_centrale`
--

--  `compte_centrale`;


CREATE TABLE `compte_centrale`
(
    `ID_COMPTE`           int(20)     NOT NULL AUTO_INCREMENT,
    `ORGANISME`           varchar(30) NOT NULL,
    `ID_CENTRALE`         int(20)      DEFAULT NULL,
    `MAIL`                varchar(200) DEFAULT NULL,
    `FAX`                 varchar(20)  DEFAULT NULL,
    `INFO_CIMPLEMENTAIRE` text         DEFAULT NULL,
    `ID_SERVICE`          int(11)      DEFAULT 0,
    PRIMARY KEY (`ID_COMPTE`, `ORGANISME`),
    KEY `ID_CENTRALE` (`ID_CENTRALE`),
    CONSTRAINT `compte_centrale_ibfk_1` FOREIGN KEY (`ID_CENTRALE`) REFERENCES `Centrale_publication` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `comptes_agents_associes`
--

--  `comptes_agents_associes`;


CREATE TABLE `comptes_agents_associes`
(
    `compte_principal`                    int(11)        NOT NULL COMMENT 'ID du compte agent principal',
    `compte_secondaire`                   int(11)        NOT NULL COMMENT 'ID du compte agent secondaire',
    `statut_activation_compte_secondaire` enum ('0','1') NOT NULL COMMENT '"0" = En attente d''approbation  ,"1" = Approuvé (l''association a été confirmée)',
    `UID_ECHANGE`                         varchar(32) DEFAULT NULL COMMENT 'ID utilisé durant l''échange pour la confirmation de l''association',
    PRIMARY KEY (`compte_principal`, `compte_secondaire`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `configuration_alerte`
--

--  `configuration_alerte`;


CREATE TABLE `configuration_alerte`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `libelle`    varchar(256)   DEFAULT NULL,
    `operation`  varchar(256)   DEFAULT NULL,
    `bloquant`   enum ('0','1') DEFAULT '0',
    `active`     enum ('0','1') DEFAULT NULL,
    `xml`        longtext       DEFAULT NULL,
    `message`    longtext       DEFAULT NULL,
    `type_objet` varchar(100)   DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `configuration_client`
--

--  `configuration_client`;


CREATE TABLE `configuration_client`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `parameter`  varchar(300) NOT NULL,
    `value`      text         NOT NULL,
    `updated_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `configuration_messages_traduction`
--

--  `configuration_messages_traduction`;


CREATE TABLE `configuration_messages_traduction`
(
    `id`        int(11)      NOT NULL AUTO_INCREMENT,
    `source`    varchar(300) NOT NULL,
    `target`    text         NOT NULL,
    `langue_id` int(11)      NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_languages_langue_id` (`langue_id`),
    CONSTRAINT `fk_languages_langue_id` FOREIGN KEY (`langue_id`) REFERENCES `Langue` (`id_langue`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `configuration_organisme`
--

--  `configuration_organisme`;


CREATE TABLE `configuration_organisme`
(
    `organisme`                                                 varchar(30)    NOT NULL DEFAULT '',
    `encheres`                                                  enum ('0','1') NOT NULL DEFAULT '0',
    `consultation_pj_autres_pieces_telechargeables`             enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet à l''acheteur public d''afficher le bloc ''Autres pièces téléchargeables par les entreprises'' et de pouvoir joindre une pièce lors de la création de la consultation. Ce module s''active par organisme.',
    `no_activex`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `gestion_mapa`                                              enum ('0','1') NOT NULL DEFAULT '1',
    `article_133_upload_fichier`                                enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet pour un organisme de gérer la visibilité du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction d''import de la liste des marchés à partir de fichier.',
    `centrale_publication`                                      enum ('0','1') NOT NULL DEFAULT '0',
    `organisation_centralisee`                                  enum ('0','1') NOT NULL DEFAULT '1',
    `presence_elu`                                              enum ('0','1') NOT NULL DEFAULT '0',
    `traduire_consultation`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `suivi_passation`                                           enum ('0','1') NOT NULL DEFAULT '0',
    `numerotation_ref_cons`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `pmi_lien_portail_defense_agent`                            enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer pour l''organisme le lien d''accès aux portails de la défense. Fonctionnalité accessible dans la page d''accueil des acheteurs publics.',
    `interface_archive_arcade_pmi`                              enum ('0','1') NOT NULL DEFAULT '0',
    `desarchivage_consultation`                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la fonction de desarchivage d''une consultation',
    `alimentation_automatique_liste_invites`                    enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Alimentation automatique de la liste des invités d''une consultation par la désignation du service associé',
    `interface_chorus_pmi`                                      enum ('0','1') NOT NULL DEFAULT '0',
    `archivage_consultation_sur_pf`                             enum ('0','1') NOT NULL DEFAULT '0',
    `autoriser_modification_apres_phase_consultation`           enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Empêche la modification d''une consultation après phase ''consultation''',
    `importer_enveloppe`                                        enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Ce module agent est activable par organisme. Il permet d''accéder à la fonction "importer une enveloppe" en phase décision s''il est actif et si l''agent dispose de l''habilitation',
    `export_marches_notifies`                                   enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet à un agent d''un organisme ou d''un service d''accéder à la page de recherche avancée et d''export des marchés notifiés.',
    `acces_agents_cfe_bd_fournisseur`                           enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Ce module lorsqu''il est activé permet d''avoir accès aux documents rendus visibles du coffre fort entreprise à partir de la fonction visualiser les entreprises.',
    `acces_agents_cfe_ouverture_analyse`                        enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'ce module permet à l''agent d''accéder aux documents rendus visibles du coffre fort entreprise à partir de la page ouverture et analyse',
    `utiliser_parametrage_encheres`                             enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet d''utiliser le module enchères parametrées par défaut sur une enchère inversée.',
    `gestion_mandataire`                                        enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de gérer les mandataires.',
    `verifier_compte_boamp`                                     enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la validation du compte BOAMP lors de sa création',
    `four_eyes`                                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'module permetant la gestion de l''ouverture en deux temps des offres électroniques',
    `interface_module_rsem`                                     enum ('0','1') NOT NULL DEFAULT '0',
    `archivage_consultation_sae_externe_envoi_archive`          enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet d''envoyer les archives au système d''archivage externe de la plate-forme',
    `archivage_consultation_sae_externe_telechargement_archive` enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet à un agent de télécharger une archive (DAC pas le DIC) depuis système d''archivage externe de la plate-forme',
    `agent_verification_certificat_peppol`                      enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'ce module permet de présenter à l''agent dans la page de détail de pli la fonction vérifier le certificat de signature avec PEPPOL',
    `fuseau_horaire`                                            enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de définir le lieu de résidence ainsi que le fuseau horaire par rapport à l''heure GMT. Ces informations seront exploitées sur la plateforme',
    `fiche_weka`                                                enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''integrer les informations weka au niveau de MPE',
    `mise_disposition_pieces_marche`                            enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Mise à disposition des pièces de marchés',
    `base_dce`                                                  enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer la fonctionnalite BASE_DCE',
    `avis_membres_commision`                                    enum ('0','1') NOT NULL DEFAULT '0',
    `Donnees_Redac`                                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'gestion des menus et habilitations REDAC  sous MPE',
    `Personnaliser_Affichage_Theme_Et_Illustration`             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'gestion d''affichage personnaliser pour les agents ( choix de thème et l''illustration de fond)',
    `type_contrat`                                              enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilité du type de contrat sur le formulaire consultation',
    `entite_adjudicatrice`                                      enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de gérer la visibilité du champ "Consultation passée en tant qu''Entité adjudicatrice"',
    `gestion_operations`                                        enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de gérer les opérations',
    `calendrier_de_la_consultation`                             enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Afficher ou masquer le calendrier previsionnel et reel',
    `donnees_complementaires`                                   enum ('0','1') NOT NULL DEFAULT '0',
    `espace_collaboratif`                                       enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer la fonctionnalités des espaces collaboratifs',
    `historique_navigation_inscrits`                            enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Permettre a un agent habilite d acceder a l historique de navigation des inscrits sur une consultation donnee.',
    `Identification_contrat`                                    enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de gerer le format de l''identifiant du contrat ',
    `extraction_accords_cadres`                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'gere la possibilité de telecharger la liste des accord-cadres',
    `marche_public_simplifie`                                   enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de gérer la visibilité du champ "Marche Public Simplifie dans la creation de la consultation"',
    `extraction_siret_acheteur`                                 enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de gérer la liste des Siret Acheteurs',
    `recherches_favorites_agent`                                enum ('0','1') NOT NULL COMMENT 'permet d''activer sur la pf la fonctionalité de sauvegarde des recherches favorites pour l''agent',
    `profil_rma`                                                enum ('0','1') NOT NULL DEFAULT '0',
    `filtre_contrat_ac_sad`                                     enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le filtre AS/SAD et Mon entité dans la recherche des contrats',
    `affichage_nom_service_pere`                                enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le nom du premier servrice pere  au dessous de l''organisme',
    `mode_applet`                                               enum ('0','1') NOT NULL DEFAULT '0',
    `marche_defense`                                            enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher ou non le bloc ''marché de défense ou sécurité'' pour les contrats',
    `num_donnees_essentielles_manuel`                           enum ('0','1')          DEFAULT '0' COMMENT 'donne la  possibilite pour un acheteur de saisir manuellement le numéro d''identification, via le champ "Numéro de contrat" (colonne reference_libre de la table t_contrat_titulaire) de la popup PopupNumerotationContrat',
    `numero_projet_achat`                                       enum ('0','1') NOT NULL DEFAULT '0',
    `module_exec`                                               enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la communication avec le module EXEC',
    `espace_documentaire`                                       tinyint(1)     NOT NULL DEFAULT 1,
    `echanges_documents`                                        tinyint(1)     NOT NULL DEFAULT 0 COMMENT 'active le module d échange des documents',
    `heure_limite_de_remise_de_plis_par_defaut`                 varchar(10)             DEFAULT NULL,
    `saisie_manuelle_id_externe`                                tinyint(1)     NOT NULL DEFAULT 0 COMMENT 'Permet d’activer la saisie manuelle de l’ID externe des agents et des services',
    `module_sourcing`                                           tinyint(1)     NOT NULL DEFAULT 0 COMMENT 'active le module sourcing',
    PRIMARY KEY (`organisme`),
    CONSTRAINT `configuration_organisme_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `configuration_plateforme`
--

--  `configuration_plateforme`;


CREATE TABLE `configuration_plateforme`
(
    `id_auto`                                                        int(11) unsigned NOT NULL AUTO_INCREMENT,
    `code_cpv`                                                       enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Rend obligatoire la saisie du code cpv à la création de la consultation pour le bloc principal et les lots. 1=saisie cpv obligatoire',
    `multi_linguisme_entreprise`                                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Rend l''application MPE multilingue du côté entreprise. L''activation de ce module se fait en parallèle du paramétrage de la table Langue dans la base commune',
    `gestion_fournisseurs_docs_mes_sous_services`                    enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les fournisseurs de documents, de gérer les fournisseurs de documents de ses sous services.',
    `authenticate_inscrit_by_cert`                                   enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Rend visible le bloc authentification par certificat du côté entreprise et active de cette manière la fonction d''authentification par certificat',
    `authenticate_inscrit_by_login`                                  enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Rend visible le bloc authentification par identifiant / mot de passe du côté entreprise et active de cette manière la fonction d''authentification par identifiant',
    `base_qualifiee_entreprise_insee`                                enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de pré-remplir les informations d''une entreprise lors de l''inscription à partir de la base INSEE. L''interconnexion avec la base INSEE se faisant par Web Services. Ce module est utilisable uniquement en France.',
    `gestion_boamp_mes_sous_services`                                enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les comptes BOAMP, de gérer les comptes BOAMP de ses sous services.',
    `gestion_bi_cle_mes_sous_services`                               enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les clés de chiffrement, de gérer les clés de chiffrements de ses sous services.',
    `nom_entreprise_toujours_visible`                                enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le nom de l''entreprise dans la phase d''ouverture et d''analyse au niveau du tableau de réponses et de la page de téléchargement des plis chiffrés',
    `gestion_jal_mes_sous_services`                                  enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Donne la possibilité à un administrateur ayant l''habilitation de gérer les JAL, de gérer les JAL de ses sous services',
    `choix_langue_affichage_consultation`                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher dans la page de résultat de recherche des consultations du côté entreprise, les informations relatives à la consultation dans les autres langues. Ce module s''utilise avec le module organisme traduire_consultation.',
    `compte_entreprise_donnees_complementaires`                      enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Donne accès à un lien ''Données complémentaires - Organisme de formation'' dans le compte d''une entreprise. Ce lien redirige vers une page qui permet à l''entreprise de préciser son activité dans le domaine de la formation.',
    `annuaire_entites_achat_visible_par_entreprise`                  enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet d''activer le lien ''Visualiser les services'' côté entreprise. C''est l''annuaire des entités d''achat de la plate-forme. Les organismes qui apparaissent dans la liste déroulante sont celles qui ont active et affichage_entite=1 dans la table organisme.',
    `affichage_recherche_avancee_agent_ac_sad_transversaux`          enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''accéder aux Accords-cadres et Systèmes d''acquisition dynamique transversaux, en vue de passer un marché subséquent ou spécifique dans le moteur de recherche avancé côté agent.',
    `encheres_entreprise`                                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module enchère côté entreprise. Ce module s''utilise avec le module encheres de la table ModuleOrganisme.',
    `socle_interne`                                                  enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de transformer l''application MPE comme un socle de portail d''administration électronique du côté des agents. Le socle interne est à activer quand une application différente de MPE est intégrée en sus au portail (ex: parapheur).',
    `module_certificat`                                              enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer un lien dans le menu gauche agent dans l''aide>outils informatique qui affiche une page permettant de télécharger le certificat racine de la plate-forme afin de pouvoir utiliser les outils de vérifications de jeton d''horodatage.',
    `socle_externe_agent`                                            enum ('0','1')   NOT NULL DEFAULT '0',
    `afficher_image_organisme`                                       enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de gérer un logo par organisme qui s''affichera dans le bandeau à droite de la plate-forme quand on y accède avec l''acronyme et dans les résultats du moteur de recherche côté entreprise. L''administration du logo se fait dans /administration.',
    `socle_externe_entreprise`                                       enum ('0','1')   NOT NULL DEFAULT '0',
    `portail_defense_entreprise`                                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Pemet l''Intégration de la gestion de l''inscription des entreprises sur les portails du MINDEF ainsi que le sso en provenance des portails MINDEF.',
    `compte_entreprise_province`                                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ province dans le formulaire de creation des entreprises, la liste est alimentée à partir de la table GeolocalisationN2',
    `compte_entreprise_telephone3`                                   enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ telephone 3 dans le formulaire de creation des entreprises''',
    `compte_entreprise_tax_prof`                                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ numero de la taxe professionnelle dans le formulaire de creation des entreprises''',
    `compte_entreprise_rcville`                                      enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher la ville associée au champ du numero du registe du commerce dans le formulaire de creation des entreprises''',
    `compte_entreprise_declaration_honneur`                          enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ declaration sur l''''honneur dans le formulaire de creation des entreprises'' que l''inscrit coche lors de l''enregistrement.',
    `compte_entreprise_qualification`                                enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur qualification qui sont organisées hiérarchiquement. Ce module utilise la table Qualification de la base commune.',
    `compte_entreprise_moyens_techniques`                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ moyen techniques dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent',
    `compte_entreprise_prestations_realisees`                        enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ prestations réalisés dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent',
    `compte_entreprise_chiffre_affaire_production_biens_services`    enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le tableau relatif aux donnees financieres de l''entreprise relativement à la vente de marchandises, à la production et aux services sur les 3 dernieres années',
    `enveloppe_offre_technique`                                      enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''associer une enveloppe d''offre technique',
    `compte_inscrit_choix_profil`                                    enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher lors de la création d''un compte inscrit le choix du type de compte utilisateur ou administrateur dans le formulaire',
    `procedure_adaptee`                                              enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilité des MAPA dans la plate-forme: accès aux fonctions relatives à la gestion des MAPA, aux statistiques MAPA et les habilitations',
    `compte_entreprise_siren`                                        enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ siren et siret dans le formulaire de creation des entreprises',
    `compte_entreprise_activation_inscription_par_agent`             enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''enregistrer les demandes d''inscription des entreprises sans activer le compte au moment de l''enregistrement. L''activation se faisant par un agent habilité.',
    `menu_entreprise_consultations_en_cours`                         enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'affiche le lien ''consultations en cours'' dans le menu gauche entreprise',
    `compte_entreprise_capital_social`                               enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Module pour activer le compte entreprise pour qu''il affiche le champs capital social ',
    `mail_activation_compte_inscrit_entreprise`                      enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de gérer l''envoi d''un mail spécifique lors de l''inscription d''un compte inscrit avec des informations sur le compte entreprise et la procedure pour activer le compte.',
    `decision_date_notification`                                     enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Module pour afficher la date de notification dans la decision attributaire',
    `decision_pmi_pme`                                               enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Module pour afficher le champ pmi /pme dans la decision attributaire',
    `decision_nature_prestations`                                    enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Module pour afficher la nature de Prestations dans la decision attributaire',
    `decision_objet_marche`                                          enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Module pour afficher objet marche dans la decision attributaire',
    `decision_note`                                                  enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Module pour afficherla note dans la decision attributaire',
    `decision_fiche_recensement`                                     enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Module pour afficher le lien pour telecharger la fiche de recensement dans la decision attributaire',
    `registre_papier_mail_obligatoire`                               enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de rendre obligatoire l''adresse électronique lors de l''ajout d''un dépôt papier par l''acheteur public au niveau des registres.',
    `menu_entreprise_indicateurs_cles`                               enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Affiche les ''indicateurs clés'' dans le menu de gauche sur la plateforme des entreprises.',
    `ajout_rpa_champ_email`                                          enum ('0','1')   NOT NULL DEFAULT '0',
    `ajout_rpa_champ_telephone`                                      enum ('0','1')   NOT NULL DEFAULT '0',
    `ajout_rpa_champ_fax`                                            enum ('0','1')   NOT NULL DEFAULT '0',
    `entreprise_poser_question_sans_pj`                              enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet de masquer le bloc d''ajout de fichier joint lorsque l''inscrit de l''entreprise pose une question. Module actif à 1.',
    `url_demarche_agent`                                             enum ('0','1')   NOT NULL DEFAULT '0',
    `url_demarche_entreprise`                                        enum ('0','1')   NOT NULL DEFAULT '0',
    `siret_detail_entite_achat`                                      enum ('0','1')   NOT NULL DEFAULT '1',
    `presence_elu`                                                   enum ('0','1')   NOT NULL DEFAULT '1',
    `gerer_mon_service`                                              enum ('0','1')   NOT NULL DEFAULT '0',
    `depouillement_enveloppe_depend_RAT_enveloppe_precedente`        enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_adresse_retrais_dossiers`                          enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_adresse_depot_offres`                              enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_caution_provisoire`                                enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_lieu_ouverture_plis`                               enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_qualification`                                     enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_agrement`                                          enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_echantillons_demandes`                             enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_reunion`                                           enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_visite_des_lieux`                                  enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_prix_acquisition`                                  enum ('0','1')   NOT NULL DEFAULT '0',
    `resultat_analyse_avant_decision`                                enum ('0','1')   NOT NULL DEFAULT '0',
    `creation_inscrit_par_ates`                                      enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_variantes_autorisees`                              enum ('0','1')   NOT NULL DEFAULT '0',
    `recherche_avancee_par_type_org`                                 enum ('0','1')   NOT NULL DEFAULT '0',
    `menu_agent_societes_exclues`                                    enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Donne accès à la fonctionnalité qui permet d''exclure une ou des sociétés à des consultations pour divers motifs sur la plateforme. Accès à partir du menu de gauche.',
    `recherche_avancee_par_domaines_activite`                        enum ('0','1')   NOT NULL DEFAULT '0',
    `recherche_avancee_par_qualification`                            enum ('0','1')   NOT NULL DEFAULT '0',
    `recherche_avancee_par_agrement`                                 enum ('0','1')   NOT NULL DEFAULT '0',
    `contact_administratif_dans_detail_consultation_cote_entreprise` enum ('0','1')   NOT NULL DEFAULT '0',
    `consultation_pieces_dossiers`                                   enum ('0','1')   NOT NULL DEFAULT '0',
    `gerer_adresses_service`                                         enum ('0','1')   NOT NULL DEFAULT '0',
    `traduire_annonces`                                              enum ('0','1')   NOT NULL DEFAULT '0',
    `afficher_bloc_actions_dans_details_annonces`                    enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le bloc ''Mes actions'' dans les détails des annonces de types autres que consultations sur la plateforme des acheteurs publics.',
    `autoriser_une_seule_reponse_principale_par_entreprise`          enum ('0','1')   NOT NULL DEFAULT '0',
    `generation_avis`                                                enum ('0','1')   NOT NULL DEFAULT '0',
    `passation_appliquer_donnees_ensemble_lots`                      enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de reprendre automatiquement pour tous les lots de la consultation, les données spécifiques saisies pour un lot, dans les données complémentaires de passation.',
    `autre_annonce_extrait_pv`                                       enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Pour la gestion du type d''annonce "Extrait de PV"',
    `autre_annonce_rapport_achevement`                               enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Pour la gestion du type d''annonce "Rapport d''achevement"',
    `ajout_fichier_joint_autre_annonce`                              enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Pour l''ajout des pièces jointes dans les autres annonces',
    `consultation_mode_passation`                                    enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'afficher les choix de mode passation au rabais et sur offre de prix',
    `compte_entreprise_identifiant_unique`                           enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Autorise les entreprises à s''inscrire avec un identifiant unique national rattaché à une ville d''enregistrement au registre de commerce. L''activation de ce module cache l''inscription avec SIREN/SIRET. Il impacte également les fonctions de recherche des en',
    `gerer_certificats_agent`                                        enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'gestion des certificats pour les agents',
    `autre_annonce_programme_previsionnel`                           enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Creer autre type annonce programme previsionnel',
    `annuler_consultation`                                           enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'annuler une consultation',
    `cfe_entreprise_accessible_par_agent`                            enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Ce module permet de rendre les documents du Coffre Fort des entreprises visibles par les agents. Il faut que le module agent acces_agents_cfe soit actif également.',
    `compte_entreprise_code_nace_referentiel`                        enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ code NACE dans le formulaire de création des entreprises et dans la base de données fournisseurs, en passant par un référentiel externe',
    `code_nut_lt_referentiel`                                        enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ ''code nut'' dans le formulaire de création et de recherche avancée des consultations. Ce module fait appel à un referentiel externe',
    `lieux_execution`                                                enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher les lieux d''exécution d''une consultation en utilisant la nomenclature classique.',
    `compte_entreprise_domaine_activite_lt_referentiel`              enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise un référentiel externe.',
    `consultation_domaines_activites_lt_referentiel`                 enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher les domaines d''activités lors de la création de la consultation en utilisant LT-Référentiel',
    `compte_entreprise_agrement_lt_referentiel`                      enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur certification qui sont organisées hiérarchiquement. Ce module utilise un référentiel externe.',
    `compte_entreprise_qualification_lt_referentiel`                 enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur qualification qui sont organisées hiérarchiquement. Ce module utilise un référentiel externe.',
    `reponse_pas_a_pas`                                              enum ('0','1')   NOT NULL DEFAULT '0' COMMENT '0004961: PMI : évolution cinématique de signature et de réponse',
    `agent_controle_format_mot_de_passe`                             enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'ce module permet de controler le format du mot de passe saisit par l''agent, 8 caractères minimum pris dans au moins 3 types de caractères parmi les 4 suivants : Minuscules, Majuscules, Chiffres, Caractères spéciaux.',
    `entreprise_validation_email_inscription`                        enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'ce module permet de valider l''adresse email saisie par l''entreprise lors de la création du compte.A la création du compte, un mail est automatiquement envoyé par la plateforme à l''entreprise. Ce mail permet de valider l''adresse mail de l''entreprise.',
    `telecharger_dce_avec_authentification`                          enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Ce module implique que le téléchargement des DCE doit être fait en étant être inscrit et authentifié.',
    `authentification_basic`                                         enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'ce module permet d''utiliser l''authentification basic de la norme RFC2617',
    `reglement_consultation`                                         enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Ce module permet d''ajouter le Réglement de la consultation à la création consultation.',
    `annonces_marches`                                               enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de transformer la plate-forme MPE comme un portail d''annonces regroupant les consultations en provenance de plusieurs sources. Si ce module est activé, certaines fonctions du côté entreprise sont adapatées par rapport à une PF MPE classique.',
    `cfe_date_fin_validite_obligatoire`                              enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Champ date de fin de validité est obligatoire quand on ajoute un document dans le coffre-fort',
    `associer_documents_cfe_consultation`                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''''associer les documents du coffre fort électronique à une consultation, dans ce cas le document est visible par l''agent pendant la phase d''analyse des offres et dans l''archive.',
    `compte_entreprise_region`                                       enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ région dans le formulaire de création des entreprises, la liste est alimentée à partir de la table GeolocalisationN1',
    `compte_entreprise_telephone2`                                   enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ telephone 2 dans le formulaire de creation des entreprises''',
    `compte_entreprise_cnss`                                         enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ numero du cnss dans le formulaire de creation des entreprises''',
    `compte_entreprise_rcnum`                                        enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ du numero du registe du commerce dans le formulaire de creation des entreprises''',
    `compte_entreprise_domaine_activite`                             enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise la table Category, SousCategory de la base commune.',
    `compte_inscrit_code_nic`                                        enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ code nic dans le formulaire de creation du compte d''un inscrit',
    `compte_entreprise_code_ape`                                     enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ code ape dans le formulaire de creation des entreprises''',
    `compte_entreprise_documents_commerciaux`                        enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ ajout de documents commerciaux dans la page du compte entreprise et leur téléchargement dans la page détail entreprise du côté Agent',
    `compte_entreprise_agrement`                                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification des entreprises en fonction de leur agréement qui sont organisées hiérarchiquement. Ce module utilise la table Agrement de la base commune.',
    `compte_entreprise_moyens_humains`                               enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le champ moyen humains dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent',
    `compte_entreprise_activite_domaine_defense`                     enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le champ description activités domaine défense dans lequel seront saisies les informations au niveau du compte entreprise. Ces informations sont visible dans la page detail entreprise du côté Agent',
    `compte_entreprise_donnees_financieres`                          enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''afficher le tableau relatif aux donnees financieres de l''entreprise relativement au CA, au besoin de financemet, au cash flow et à la capacite d\\''endettement'' sur les 3 dernieres années',
    `enveloppe_anonymat`                                             enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet d''associer une enveloppe d''anonymat',
    `publicite_format_xml`                                           enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet de gerer la visibilite de la publicite au format xml dans le menu gauche agent ainsi que les fonctions associées (gestion compte boamp, moniteur, statistiques avis publiés, typologie de mise en ligne de la consultation, habilitations, alertes agent',
    `article_133_generation_pf`                                      enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction de génération de la liste des marchés à partir de la plate-forme',
    `entreprise_repondre_consultation_apres_cloture`                 enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet à une entreprise de répondre à la consultation pendant l''affichage prolongé, si l''affichage prolongé a été paramétré pour la consultation',
    `telechargement_outil_verif_horodatage`                          enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Affiche un lien permettant de télécharger l''outil de vérification des jetons d''horodatages à partir de la page Autres Outils Informatique',
    `affichage_code_cpv`                                             enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet l''utilisation du module Code CPV dans MPE',
    `consultation_domaines_activites`                                enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le module qui gère la classification d''une consultation en fonction de domaines d''activités qui sont organisées hiérarchiquement. Ce module utilise la table Category, SousCategory de la base commune.',
    `statistiques_mesure_demat`                                      enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet de gérer la visibilité de la fonction Mesure Avancement Dematerialisation dans la plate-forme',
    `publication_procure`                                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Gère la visibilité du module ''''Rendre accessible cette consultation dans toutes les langues au portail européen PROCURE"',
    `menu_entreprise_toutes_les_consultations`                       enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'affiche le lien ''toutes les consultations'' dans le menu gauche entreprise',
    `compte_entreprise_cp_obligatoire`                               enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Module pour activer le compte entreprise pour qu''il affiche le champs code postal n''est pas obligatoire ',
    `annuler_depot`                                                  enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la fonctionnalité d''annulation de dépôt électronique par l''inscrit de l''entreprise dans "mon compteà Mes réponses" et de dépôt papier par l''acheteur public dans les registres de dépôt de la consultation.',
    `traduire_entite_achat_arabe`                                    enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Module pour activer la visibilité des champs à traduire en arabe (côté agent et administration, gestion de l''organisme)',
    `traduire_organisme_arabe`                                       enum ('0','1')            DEFAULT '0',
    `decision_cp`                                                    enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Module pour afficher le code postal dans la decision attributaire',
    `decision_tranche_budgetaire`                                    enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Module pour afficher la tranche budgetaire dans la decision attributaire',
    `decision_classement`                                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Module pour afficher le classement dans la decision attributaire',
    `decision_afficher_detail_candidat_par_defaut`                   enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Ce module permet d''afficher le détail du candidat sans avoir à cliquer sur la case à cocher ''attributaire''',
    `article_133_upload_fichier`                                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de gérer la visibilite du lien ''''liste des marchés (articles 133)''''dans le menu gauche coté agent: fonction d''import de la liste des marchés à partir de fichier',
    `multi_linguisme_agent`                                          enum ('0','1')   NOT NULL DEFAULT '0',
    `compte_entreprise_ifu`                                          enum ('0','1')   NOT NULL DEFAULT '0',
    `gestion_organisme_par_agent`                                    enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet à l Hyper admin de gérer les services par organismes. A besoin de mettre habilitation HyperAdmin à 1',
    `utiliser_lucene`                                                enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'à 1 pour utiliser lucène, à 0 pour ne pas utilliser lucène',
    `utiliser_page_html_lieux_execution`                             enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'à 1 pour utiliser la page .html des lieux executions, à 0 pour utiliser la page commun.LieuxExecution',
    `prado_validateur_format_date`                                   enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'à 1 pour garder actifs les validateurs prado, à 0 sinon',
    `prado_validateur_format_email`                                  enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'à 1 pour garder actifs les validateur email, à 0 sinon',
    `socle_externe_ppp`                                              enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Socle externe PPP de e-bourgogne',
    `validation_format_champs_stricte`                               enum ('0','1')   NOT NULL DEFAULT '0',
    `poser_question_necessite_authentification`                      enum ('0','1')   NOT NULL DEFAULT '0',
    `autoriser_modif_profil_inscrit_ates`                            enum ('0','1')            DEFAULT '0' COMMENT 'Ce module permet à l''entreprise qui s''enregistre de choisir son profil (Utilisateur Simple ou Administrateur). Si 0 alors pas de choix possible, le premier inscrit est Administrateur et les autres US.',
    `unicite_reference_consultation`                                 enum ('0','1')   NOT NULL DEFAULT '0',
    `registre_papier_rcnum_rcville_obligatoires`                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de rendre obligatoire la saisie de la ville et du numéro du registre de commerce. Ce module nécessite l''activation des modules ''compte_entreprise_rcnum'' et ''compte_entreprise_rcville''.',
    `registre_papier_adresse_cp_ville_obligatoires`                  enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de rendre obligatoire l''adresse, le code postal et la ville lors d''un dépôt papier au niveau des registres. Module actif à 1.',
    `telecharger_dce_sans_identification`                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'possiblité de télécharger le DCE anonymement',
    `gestion_entreprise_par_agent`                                   enum ('0','1')   NOT NULL DEFAULT '0',
    `autoriser_caracteres_speciaux_dans_reference`                   enum ('0','1')   NOT NULL DEFAULT '0',
    `inscription_libre_entreprise`                                   enum ('0','1')   NOT NULL DEFAULT '1',
    `afficher_code_service`                                          enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'ce module permet d''afficher le code service/entité achat. la valeur est enregistrer dans le champs siren',
    `authenticate_agent_by_login`                                    enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Rend visible le bloc authentification par identifiant / mot de passe du côté agent et active de cette manière la fonction d''authentification par identifiant',
    `authenticate_agent_by_cert`                                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'ce module permet l''authentification par certificat côté agent',
    `generer_acte_dengagement`                                       enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Ce module permet d''activer la génération de l''acte d''engagement lors de l''envoi de la réponse',
    `entreprise_controle_format_mot_de_passe`                        enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'ce module permet de controler le format du mot de passe saisit par l''entreprise, 8 caractères minimum pris dans au moins 3 types de caractères parmi les 4 suivants : Minuscules, Majuscules, Chiffres, Caractères spéciaux.',
    `autre_annonce_information`                                      enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet de créer et gérer les annonces de type ''Information'' sur la plateforme. Fonctionnalité accessible dans le menu de gauche, les détails des annonces et la recherche avancée.',
    `creer_autre_annonce`                                            enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet de créer une nouvelle annonce autre que consultation dans le menu de gauche des agents.',
    `consultation_clause`                                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'ce module permet de renseigner les clauses de la consultation',
    `panier_entreprise`                                              enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer sur la plateforme la fonctionnalité de gestion des paniers des entreprises.',
    `parametrage_publicite_par_type_procedure`                       enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Ce module permet de gerer la publcite d''une consultation ',
    `export_decision`                                                enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Ce module permet de gerer l''export de la decision d''une consultation',
    `lieu_ouverture_plis_obligatoire`                                enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet de determiner si ''lieuOuverturePlis'' est obligatoire ou pas',
    `regle_mise_en_ligne_par_entite_coordinatrice`                   enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Ce module permet d''''indiquer si la gestion des publicites se fait par une entité type boamp',
    `gestion_newsletter`                                             enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet de gÃ©rer les newsletter',
    `publicite_opoce`                                                enum ('0','1')   NOT NULL DEFAULT '0',
    `gestion_modeles_formulaire`                                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet de gerer les modeles de formulaires',
    `gestion_adresses_facturation_JAL`                               enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Ce module permet de gerer les facturations JAL',
    `publicite_marches_en_ligne`                                     enum ('0','1')   NOT NULL DEFAULT '1',
    `dossier_additif`                                                enum ('0','1')   NOT NULL DEFAULT '0',
    `type_marche`                                                    enum ('0','1')   NOT NULL DEFAULT '0',
    `type_prestation`                                                enum ('0','1')   NOT NULL DEFAULT '0',
    `afficher_tjr_bloc_caracteristique_reponse`                      enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet d''afficher tjr le bloque des caractéristiques de la reponse même si la réponse electronique n''est pas autorisé',
    `alerte_metier`                                                  enum ('0','1')   NOT NULL DEFAULT '0',
    `bourse_a_la_sous_traitance`                                     enum ('0','1')   NOT NULL DEFAULT '0',
    `partager_consultation`                                          enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de partager la consultation sur des réseaux sociaux',
    `annuaire_acheteurs_publics`                                     enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet de gerer l''annuaire des acheteurs publics ',
    `entreprise_actions_groupees`                                    enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet de gerer les actoins groupees pour les entreprises',
    `publier_guides`                                                 enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet de gerer la publication des guide MPE',
    `recherche_auto_completion`                                      enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Dans la recherche des consultations permettre la sélection de la dénomination de l''organisme par un champ auto complétion au lieu d''une liste pré rempli ',
    `statut_compte_entreprise`                                       enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de préciser le statut de l''entreprise par exemple (Entreprise Adaptée,Service d''Aide par le Travail..etc)',
    `gestion_organismes`                                             enum ('0','1')   NOT NULL DEFAULT '0',
    `accueil_entreprise_personnalise`                                enum ('0','1')   NOT NULL DEFAULT '0',
    `interface_module_sub`                                           enum ('0','1')   NOT NULL DEFAULT '0',
    `authentification_agent_multi_organismes`                        enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Module a pour but de permettre à un agent d''associer deux comptes l''un à l''autre, l''un étant désigné comme étant le compte principal, l''autre comme étant le compte secondaire.',
    `lieux_execution_carte`                                          enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'permet de gerer l''affichage de la carte pour le choix des lieux d''execution',
    `surcharge_referentiels`                                         enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de définir si on va prendre en compte la validité du réferentiel de certificat pour afficher le résultat du contrôle "Chaîne de certification".',
    `Mode_Restriction_RGS`                                           enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'ce module permet d''activer le modeRestrictionRGS de l''applet',
    `autre_annonce_decision_resiliation`                             enum ('0','1')   NOT NULL DEFAULT '0',
    `autre_annonce_synthese_rapport_audit`                           enum ('0','1')   NOT NULL DEFAULT '0',
    `fiche_weka`                                                     enum ('0','1')   NOT NULL DEFAULT '0',
    `generation_automatique_mdp_agent`                               enum ('0','1')   NOT NULL DEFAULT '0' COMMENT '0=> le pwd est saisie manuelement par l''administrateur.1=> le pwd est generé automatiquement àl''enregistrement',
    `generation_automatique_mdp_inscrit`                             enum ('0','1')   NOT NULL DEFAULT '0' COMMENT '0=> le pwd est saisie manuelement par l''''administrateur.1=> le pwd est generé automatiquement àl''''enregistrement',
    `liste_ac_rgs`                                                   enum ('0','1')   NOT NULL DEFAULT '0',
    `liste_cons_org`                                                 enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet de gerer l''affichage du frame de la liste des consultation d''un organisme',
    `marche_public_simplifie_entreprise`                             enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de gérer la visibilité du champ "Marche Public Simplifie coté entreprise"',
    `archive_par_lot`                                                enum ('0','1')   NOT NULL DEFAULT '0',
    `documents_reference`                                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la fonctionnalité des documents de référence',
    `recherches_favorites`                                           enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet d''activer sur la pf la fonctionalité de sauvegarde des recherches favorites',
    `synchronisation_SGMAP`                                          enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet de faire la synchronisation avec SGMAP via le web service',
    `donnees_candidat`                                               enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'gerer l''affichage des attestation de l''entreprise',
    `autoriser_creation_entreprise_etrangere`                        enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet d''autoriser la création d''une entreprise etrangère',
    `bourse_cotraitance`                                             enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet de gerer la bourse à la cotraitance',
    `ac_sad_transversaux`                                            enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer la gestion des entités éligibles',
    `web_service_par_silo`                                           enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'lorsqu,il est à 1 le filtre par silo organisme au niveau du WS est activé',
    `groupement`                                                     enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet de gerer le bloc groupement des entreprises',
    `notifications_agent`                                            enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Permet de gerer les notifications pour l''agent',
    `publicite`                                                      enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'permet d''activer la publicité via les formulaires SUB et le concentrateur',
    `interface_dume`                                                 enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer le DUME',
    `entreprise_duree_vie_mot_de_passe`                              int(2)           NOT NULL DEFAULT 0,
    `entreprise_mots_de_passe_historises`                            int(2)           NOT NULL DEFAULT 0,
    `plateforme_editeur`                                             enum ('0','1')   NOT NULL DEFAULT '1' COMMENT 'Configuration RÉDAC permettant de définir l''environnement comme Lecteur du clausier éditeur (0) ou éditeur du clausier éditeur (1).',
    `donnees_essentielles_suivi_sn`                                  enum ('0','1')   NOT NULL DEFAULT '0',
    `token_api_sgmap_api`                                            text                      DEFAULT NULL,
    `masquer_elements_mps`                                           enum ('0','1')   NOT NULL DEFAULT '1',
    `masquer_atexo_sign`                                             enum ('0','1')   NOT NULL DEFAULT '0',
    `messagerie_v2`                                                  tinyint(1)       NOT NULL DEFAULT 1 COMMENT 'active la messagerie sécurisé v2',
    `redac_echos`                                                    enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''affichage des champs necessaires pour la publicité BOAMP et les Echos',
    `dossier_volumineux`                                             enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''upload et download des fichiers volumineux',
    `afficher_valeur_estimee`                                        enum ('0','1')   NOT NULL DEFAULT '0',
    `case_attestation_consultation`                                  enum ('0','1')   NOT NULL DEFAULT '0' COMMENT 'Permet activer une case à cocher dans le formulaire de la consultation afin d''attester que le besoin n''est pas déjà couvert par un marché national ou local',
    `menu_agent_complet`                                             tinyint(1)                DEFAULT 1,
    `unicite_mail_agent`                                             tinyint(1)       NOT NULL DEFAULT 1 COMMENT 'Permet de désactiver le contrôle d’unicité sur le mail des agent',
    PRIMARY KEY (`id_auto`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `consultation_alertes`
--

--  `consultation_alertes`;


CREATE TABLE `consultation_alertes`
(
    `organisme`                              varchar(30)                                 NOT NULL DEFAULT '',
    `reference`                              int(11)                                     NOT NULL AUTO_INCREMENT,
    `code_externe`                           varchar(225)                                         DEFAULT NULL,
    `reference_utilisateur`                  varchar(256)                                NOT NULL DEFAULT '',
    `categorie`                              varchar(30)                                 NOT NULL DEFAULT '0',
    `titre`                                  longtext                                    NOT NULL,
    `resume`                                 longtext                                    NOT NULL,
    `datedebut`                              date                                        NOT NULL DEFAULT '0000-00-00',
    `datefin`                                datetime                                    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `datevalidation`                         varchar(20)                                 NOT NULL DEFAULT '0000-00-00 00:00:00',
    `type_procedure`                         char(1)                                     NOT NULL DEFAULT '',
    `code_procedure`                         varchar(15)                                          DEFAULT NULL,
    `reponse_electronique`                   char(1)                                     NOT NULL DEFAULT '1',
    `num_procedure`                          int(1)                                      NOT NULL DEFAULT 0,
    `id_type_procedure`                      int(1)                                      NOT NULL DEFAULT 0,
    `id_type_avis`                           int(2)                                      NOT NULL DEFAULT 0,
    `lieu_execution`                         text                                        NOT NULL,
    `type_mise_en_ligne`                     int(11)                                     NOT NULL DEFAULT 1,
    `datemiseenligne`                        datetime                                    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `is_tiers_avis`                          char(1)                                     NOT NULL DEFAULT '0',
    `url`                                    varchar(256)                                NOT NULL DEFAULT '',
    `datefin_sad`                            datetime                                    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `is_sys_acq_dyn`                         int(11)                                     NOT NULL DEFAULT 0,
    `reference_consultation_init`            varchar(250)                                NOT NULL DEFAULT '',
    `signature_offre`                        char(1)                                              DEFAULT NULL,
    `id_type_validation`                     int(11)                                     NOT NULL DEFAULT 2,
    `etat_approbation`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `etat_validation`                        enum ('0','1')                              NOT NULL DEFAULT '0',
    `champ_supp_invisible`                   text                                        NOT NULL,
    `code_cpv_1`                             varchar(8)                                           DEFAULT NULL,
    `code_cpv_2`                             varchar(256)                                         DEFAULT NULL,
    `publication_europe`                     enum ('0','1')                                       DEFAULT '0',
    `etat_publication`                       int(11)                                     NOT NULL DEFAULT 0,
    `poursuivre_affichage`                   int(11)                                     NOT NULL DEFAULT 0,
    `poursuivre_affichage_unite`             enum ('MINUTE','HOUR','DAY','MONTH','YEAR') NOT NULL DEFAULT 'DAY' COMMENT 'Permet de gerer la poursuite de l''affichage de la consultation en ''MINUTE'', ''HOUR'', ''DAY'', ''MONTH'' ou ''YEAR''',
    `nbr_telechargement_dce`                 int(5)                                               DEFAULT NULL,
    `service_id`                             int(11)                                              DEFAULT NULL,
    `service_associe_id`                     int(11)                                              DEFAULT NULL,
    `detail_consultation`                    text                                        NOT NULL,
    `date_fin_affichage`                     varchar(20)                                 NOT NULL DEFAULT '0000-00-00 00:00:00',
    `depouillable_phase_consultation`        enum ('0','1')                              NOT NULL DEFAULT '0',
    `consultation_transverse`                enum ('0','1')                              NOT NULL DEFAULT '0',
    `consultation_achat_publique`            enum ('0','1')                              NOT NULL DEFAULT '0',
    `url_consultation_achat_publique`        text                                                 DEFAULT NULL,
    `partial_dce_download`                   char(1)                                     NOT NULL DEFAULT '0',
    `tirage_plan`                            int(11)                                     NOT NULL DEFAULT 0,
    `tireur_plan`                            int(11)                                     NOT NULL DEFAULT 0,
    `date_mise_en_ligne_calcule`             datetime                                             DEFAULT NULL,
    `accessibilite_en`                       char(1)                                     NOT NULL DEFAULT '0',
    `accessibilite_es`                       char(1)                                     NOT NULL DEFAULT '0',
    `nbr_reponse`                            int(5)                                               DEFAULT NULL,
    `id_type_procedure_org`                  int(1)                                      NOT NULL DEFAULT 0,
    `tirage_descriptif`                      mediumtext                                  NOT NULL,
    `organisme_consultation_init`            varchar(256)                                NOT NULL DEFAULT '',
    `date_validation_intermediaire`          varchar(20)                                          DEFAULT NULL,
    `accessibilite_fr`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `id_tr_accessibilite`                    int(11)                                              DEFAULT NULL COMMENT 'Identifiant de la traduction de l''accessibilité pour la traduction de la consultation par les entreprises',
    `accessibilite_cz`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `accessibilite_du`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `accessibilite_su`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `accessibilite_ar`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `alloti`                                 enum ('0','1')                              NOT NULL DEFAULT '0',
    `numero_phase`                           int(3)                                      NOT NULL DEFAULT 0,
    `consultation_externe`                   enum ('0','1')                              NOT NULL DEFAULT '0',
    `url_consultation_externe`               text                                                 DEFAULT NULL,
    `org_denomination`                       varchar(250)                                         DEFAULT NULL,
    `domaines_activites`                     varchar(250)                                         DEFAULT '',
    `id_affaire`                             int(11)                                              DEFAULT NULL,
    `adresse_retrais_dossiers`               longtext                                             DEFAULT NULL,
    `caution_provisoire`                     varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres`                   longtext                                             DEFAULT NULL,
    `lieu_ouverture_plis`                    longtext                                             DEFAULT NULL,
    `prix_aquisition_plans`                  varchar(256)                                         DEFAULT NULL,
    `qualification`                          varchar(256)                                         DEFAULT NULL,
    `agrements`                              varchar(256)                                         DEFAULT NULL,
    `add_echantillion`                       varchar(256)                                         DEFAULT NULL,
    `date_limite_echantillion`               varchar(50)                                          DEFAULT NULL,
    `add_reunion`                            varchar(256)                                         DEFAULT NULL,
    `date_reunion`                           varchar(50)                                          DEFAULT NULL,
    `variantes`                              char(1)                                              DEFAULT NULL,
    `adresse_depot_offres_ar`                varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_ar`                 varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_ar`            varchar(256)                                         DEFAULT NULL,
    `pieces_dossier_admin`                   text                                                 DEFAULT NULL,
    `pieces_dossier_admin_fr`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_en`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_es`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_su`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_du`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_cz`                text                                                 DEFAULT NULL,
    `pieces_dossier_admin_ar`                text                                                 DEFAULT NULL,
    `pieces_dossier_tech`                    text                                                 DEFAULT NULL,
    `pieces_dossier_tech_fr`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_en`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_es`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_su`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_du`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_cz`                 text                                                 DEFAULT NULL,
    `pieces_dossier_tech_ar`                 text                                                 DEFAULT NULL,
    `pieces_dossier_additif`                 text                                                 DEFAULT NULL,
    `pieces_dossier_additif_fr`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_en`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_es`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_su`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_du`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_cz`              text                                                 DEFAULT NULL,
    `pieces_dossier_additif_ar`              text                                                 DEFAULT NULL,
    `id_rpa`                                 int(11)                                              DEFAULT NULL,
    `detail_consultation_fr`                 text                                                 DEFAULT NULL,
    `detail_consultation_en`                 text                                                 DEFAULT NULL,
    `detail_consultation_es`                 text                                                 DEFAULT NULL,
    `detail_consultation_su`                 text                                                 DEFAULT NULL,
    `detail_consultation_du`                 text                                                 DEFAULT NULL,
    `detail_consultation_cz`                 text                                                 DEFAULT NULL,
    `detail_consultation_ar`                 text                                                 DEFAULT NULL,
    `echantillon`                            enum ('0','1')                              NOT NULL,
    `reunion`                                enum ('0','1')                              NOT NULL,
    `visites_lieux`                          enum ('0','1')                              NOT NULL,
    `variante_calcule`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `adresse_retrais_dossiers_fr`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_en`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_es`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_su`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_du`            varchar(256)                                         DEFAULT NULL,
    `adresse_retrais_dossiers_cz`            varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_fr`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_en`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_es`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_su`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_du`                varchar(256)                                         DEFAULT NULL,
    `adresse_depot_offres_cz`                varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_fr`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_en`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_es`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_su`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_du`                 varchar(256)                                         DEFAULT NULL,
    `lieu_ouverture_plis_cz`                 varchar(256)                                         DEFAULT NULL,
    `add_echantillion_fr`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_en`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_es`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_su`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_du`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_cz`                    varchar(256)                                         DEFAULT NULL,
    `add_echantillion_ar`                    varchar(256)                                         DEFAULT NULL,
    `add_reunion_fr`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_en`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_es`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_su`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_du`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_cz`                         varchar(256)                                         DEFAULT NULL,
    `add_reunion_ar`                         varchar(256)                                         DEFAULT NULL,
    `mode_passation`                         varchar(1)                                           DEFAULT NULL,
    `consultation_annulee`                   enum ('0','1')                              NOT NULL DEFAULT '0',
    `accessibilite_it`                       enum ('0','1')                              NOT NULL DEFAULT '0',
    `adresse_depot_offres_it`                varchar(256)                                NOT NULL DEFAULT '',
    `lieu_ouverture_plis_it`                 varchar(256)                                NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_it`            varchar(256)                                NOT NULL DEFAULT '',
    `pieces_dossier_admin_it`                varchar(256)                                NOT NULL DEFAULT '',
    `pieces_dossier_tech_it`                 varchar(256)                                NOT NULL DEFAULT '',
    `pieces_dossier_additif_it`              varchar(256)                                NOT NULL DEFAULT '',
    `detail_consultation_it`                 text                                                 DEFAULT NULL,
    `add_echantillion_it`                    varchar(250)                                NOT NULL DEFAULT '',
    `add_reunion_it`                         varchar(250)                                NOT NULL DEFAULT '',
    `codes_nuts`                             text                                                 DEFAULT NULL COMMENT 'Contient les codes nuts, pour l''utilisation d''un referentiel externe',
    `intitule`                               longtext                                    NOT NULL,
    `id_tr_intitule`                         int(11)                                              DEFAULT NULL COMMENT 'Identifiant de la traduction de l''intitulé de la consultation',
    `objet`                                  longtext                                    NOT NULL,
    `id_tr_objet`                            int(11)                                              DEFAULT NULL COMMENT 'Identifiant de la traduction de l''objet de la consultation',
    `type_acces`                             char(1)                                     NOT NULL,
    `autoriser_reponse_electronique`         char(1)                                     NOT NULL DEFAULT '1',
    `regle_mise_en_ligne`                    int(11)                                     NOT NULL DEFAULT 1,
    `id_regle_validation`                    int(11)                                     NOT NULL DEFAULT 2,
    `intitule_fr`                            longtext                                             DEFAULT NULL,
    `intitule_en`                            longtext                                             DEFAULT NULL,
    `intitule_es`                            longtext                                             DEFAULT NULL,
    `intitule_su`                            longtext                                             DEFAULT NULL,
    `intitule_du`                            longtext                                             DEFAULT NULL,
    `intitule_cz`                            longtext                                             DEFAULT NULL,
    `intitule_ar`                            longtext                                             DEFAULT NULL,
    `intitule_it`                            longtext                                             DEFAULT NULL,
    `objet_fr`                               longtext                                             DEFAULT NULL,
    `objet_en`                               longtext                                             DEFAULT NULL,
    `objet_es`                               longtext                                             DEFAULT NULL,
    `objet_su`                               longtext                                             DEFAULT NULL,
    `objet_du`                               longtext                                             DEFAULT NULL,
    `objet_cz`                               longtext                                             DEFAULT NULL,
    `objet_ar`                               longtext                                             DEFAULT NULL,
    `objet_it`                               longtext                                             DEFAULT NULL,
    `date_decision`                          varchar(10)                                 NOT NULL DEFAULT '',
    `clause_sociale`                         enum ('0','1','2')                          NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non',
    `clause_environnementale`                enum ('0','1','2')                          NOT NULL DEFAULT '0' COMMENT '0 : Non renseigné, 1: oui, 2 : non',
    `reponse_obligatoire`                    char(1)                                     NOT NULL DEFAULT '0',
    `Compte_Boamp_Associe`                   int(11)                                              DEFAULT NULL,
    `autoriser_publicite`                    int(2)                                      NOT NULL DEFAULT 1,
    `type_envoi`                             char(1)                                     NOT NULL DEFAULT '',
    `chiffrement_offre`                      char(1)                                     NOT NULL DEFAULT '',
    `env_candidature`                        int(1)                                      NOT NULL DEFAULT 0,
    `env_offre`                              int(3)                                      NOT NULL DEFAULT 0,
    `env_anonymat`                           int(1)                                      NOT NULL DEFAULT 0,
    `id_etat_consultation`                   int(11)                                     NOT NULL DEFAULT 0,
    `reference_connecteur`                   varchar(256)                                NOT NULL DEFAULT '',
    `cons_statut`                            char(1)                                     NOT NULL DEFAULT '0',
    `id_approbateur`                         int(11)                                     NOT NULL DEFAULT 0,
    `id_valideur`                            int(11)                                     NOT NULL DEFAULT 0,
    `service_validation`                     int(11)                                     NOT NULL DEFAULT 0,
    `id_createur`                            int(11)                                     NOT NULL DEFAULT 0,
    `nom_createur`                           varchar(100)                                         DEFAULT NULL,
    `prenom_createur`                        varchar(100)                                         DEFAULT NULL,
    `signature_acte_engagement`              enum ('0','1')                              NOT NULL DEFAULT '0',
    `archiveMetaDescription`                 longtext                                             DEFAULT NULL,
    `archiveMetaMotsClef`                    longtext                                             DEFAULT NULL,
    `archiveIdBlobZip`                       int(11)                                              DEFAULT NULL,
    `decision_partielle`                     enum ('0','1')                              NOT NULL DEFAULT '0',
    `type_decision_a_renseigner`             enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_attribution_marche`       enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_declaration_sans_suite`   enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_declaration_infructueux`  enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_selection_entreprise`     enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_attribution_accord_cadre` enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_admission_sad`            enum ('0','1')                              NOT NULL DEFAULT '1',
    `type_decision_autre`                    enum ('0','1')                              NOT NULL DEFAULT '1',
    `id_archiveur`                           int(11)                                              DEFAULT NULL,
    `prenom_nom_agent_telechargement_plis`   varchar(256)                                         DEFAULT NULL,
    `id_agent_telechargement_plis`           int(11)                                     NOT NULL DEFAULT 0,
    `path_telechargement_plis`               varchar(256)                                         DEFAULT NULL,
    `date_telechargement_plis`               varchar(20)                                          DEFAULT NULL,
    `service_validation_intermediaire`       int(11)                                              DEFAULT NULL,
    `env_offre_technique`                    int(3)                                      NOT NULL DEFAULT 0,
    `ref_org_partenaire`                     varchar(250)                                NOT NULL DEFAULT '',
    `date_archivage`                         varchar(20)                                          DEFAULT NULL COMMENT 'Précise la date de réalisation de l''archivage',
    `date_decision_annulation`               varchar(20)                                          DEFAULT NULL,
    `commentaire_annulation`                 text                                                 DEFAULT NULL,
    `date_mise_en_ligne_souhaitee`           varchar(20)                                          DEFAULT NULL,
    `etat_en_attente_validation`             enum ('0','1')                              NOT NULL DEFAULT '1',
    `dossier_additif`                        enum ('0','1')                              NOT NULL DEFAULT '0',
    `type_marche`                            int(11)                                     NOT NULL DEFAULT 0,
    `type_prestation`                        int(11)                                     NOT NULL DEFAULT 1,
    `date_modification`                      varchar(20)                                          DEFAULT NULL,
    `delai_partiel`                          enum ('0','1')                              NOT NULL DEFAULT '0',
    `dateFinLocale`                          varchar(20)                                          DEFAULT '0000-00-00 00:00:00' COMMENT 'Permet de renseigner la date limite de remise des plis locale en rapport avec le fuseau horaire',
    `lieuResidence`                          varchar(256)                                         DEFAULT NULL COMMENT 'Permet de renseigner le lieu de residence en rapport avec le fuseau horaire',
    `alerte`                                 enum ('0','1')                              NOT NULL DEFAULT '0',
    `doublon`                                enum ('0','1')                              NOT NULL DEFAULT '0',
    `denomination_adapte`                    varchar(250)                                         DEFAULT NULL,
    `url_consultation_avis_pub`              text                                        NOT NULL,
    `doublon_de`                             varchar(250)                                         DEFAULT NULL COMMENT 'Si 762 est le doublon de 14 alors doublon_de = 14, si doublon_de vide si je suis doublon de personne',
    `entite_adjudicatrice`                   enum ('0','1')                                       DEFAULT NULL COMMENT 'Permet de savoir si une Consultation est passée en tant qu''Entité adjudicatrice',
    `code_operation`                         varchar(256)                                         DEFAULT NULL COMMENT 'Permet de stocker la valeur du champ "Code opération"',
    `clause_sociale_condition_execution`     varchar(256)                                         DEFAULT '0' COMMENT 'Le marché comprend des clauses sociales d''insertion comme condition d''exécution (article 14 du Code des marchés publics)',
    `clause_sociale_insertion`               varchar(256)                                         DEFAULT '0' COMMENT 'Présence parmi les critères d''attribution d''un critère relatif à l''insertion (article 53 du code des marchés publics)',
    `clause_sociale_ateliers_proteges`       varchar(256)                                         DEFAULT '0' COMMENT 'Le marché est réservé à des ateliers protégés (article 15 du code des marchés publics)',
    `clause_sociale_siae`                    varchar(256)                                         DEFAULT '0' COMMENT 'Le marché est réservé à  SIAE',
    `clause_sociale_ess`                     varchar(256)                                         DEFAULT '0' COMMENT 'Le marché est réservé à  ESS',
    `clause_env_specs_techniques`            varchar(256)                                         DEFAULT '0' COMMENT 'pécifications techniques (article 6 du code des marchés publics)',
    `clause_env_cond_execution`              varchar(256)                                         DEFAULT '0' COMMENT 'Conditions d''exécution (article 14 du code des marchés publics)',
    `clause_env_criteres_select`             varchar(256)                                         DEFAULT '0' COMMENT 'Critère de sélection (article 53.1 du code des marchés publics)',
    `id_donnee_complementaire`               int(11)                                              DEFAULT NULL,
    `donnee_complementaire_obligatoire`      enum ('0','1')                              NOT NULL DEFAULT '0',
    `mode_ouverture_reponse`                 enum ('0','1')                              NOT NULL DEFAULT '0' COMMENT 'Enveloppe unique ou Multi-enveloppe',
    `id_fichier_annulation`                  int(11)                                              DEFAULT NULL,
    `idOperation`                            int(11)                                              DEFAULT NULL,
    `marche_public_simplifie`                enum ('0','1')                              NOT NULL DEFAULT '0' COMMENT 'Permet de savoir si une Consultation est passée en tant que marche public simplifie',
    `infos_blocs_atlas`                      varchar(10)                                 NOT NULL DEFAULT '0##0' COMMENT 'bloc a partir du quel on commence l''envoie##nbr des blocs existants',
    `DATE_FIN_UNIX`                          varchar(20)                                          DEFAULT '0' COMMENT 'contient : UNIX_TIMESTAMP( datefin ) + ( consultation.poursuivre_affichage * ( 24 *60 *60 ) ), permet d''optimisé la recherche en recuperant le resultat direct de ce champ',
    `numero_AC`                              varchar(256)                                         DEFAULT NULL,
    `id_contrat`                             int(11)                                              DEFAULT NULL,
    `donnee_publicite_obligatoire`           enum ('0','1')                                       DEFAULT NULL COMMENT 'permet de sauvegarder la valeur de la case à cocher "Je souhaite publier un avis depuis cette plate-forme pour cette consultation"',
    `pin_api_sgmap_mps`                      varchar(20)                                          DEFAULT NULL COMMENT 'Code PIN de la consultation via l''API MPS du SGMAP',
    `dume_demande`                           enum ('0','1')                              NOT NULL DEFAULT '0' COMMENT 'Permet de savoir si une Consultation permet la candidature DUME',
    `type_procedure_dume`                    int(11)                                     NOT NULL DEFAULT 0 COMMENT 'type procedure dume liÃ©',
    `marche_insertion`                       tinyint(1)                                           DEFAULT 0,
    `clause_specification_technique`         varchar(256)                                         DEFAULT '0',
    `type_formulaire_dume`                   char(2)                                              DEFAULT '0' COMMENT 'Permet de savoir le type de formulaire dume "0"=>standard,"1"=>simplifié',
    `source_externe`                         varchar(256)                                         DEFAULT NULL,
    `id_source_externe`                      int(11)                                              DEFAULT NULL,
    `numero_projet_achat`                    varchar(256)                                         DEFAULT NULL,
    PRIMARY KEY (`reference`, `organisme`),
    KEY `organisme` (`organisme`),
    KEY `reference` (`reference`),
    KEY `Idx_Consultation_Categorie` (`categorie`),
    KEY `Idx_Consultation_CAP` (`consultation_achat_publique`),
    KEY `Idx_Consultation_Datevalid` (`datevalidation`),
    KEY `Idx_Consultation_EA` (`etat_approbation`),
    KEY `Idx_Consultation_DVI` (`date_validation_intermediaire`),
    KEY `Idx_Consultation_RU` (`reference_utilisateur`),
    KEY `Idx_Consultation_Datefin` (`datefin`),
    KEY `Idx_Consultation_ITP` (`id_type_procedure`),
    KEY `Idx_Consultation_ITA` (`id_type_avis`),
    KEY `Idx_Consultation_TML` (`type_mise_en_ligne`),
    KEY `Idx_Consultation_DMLC` (`date_mise_en_ligne_calcule`),
    KEY `Idx_Consultation_Datemiseenligne` (`datemiseenligne`),
    KEY `Idx_Consultation_Code_CPV` (`code_cpv_1`, `code_cpv_2`),
    KEY `idx_consultation_alerte` (`alerte`),
    KEY `index_url_migration` (`url_consultation_externe`(255)),
    KEY `index_denomination_adapte` (`denomination_adapte`),
    KEY `Operations_fk` (`idOperation`),
    KEY `DATE_FIN_UNIX` (`DATE_FIN_UNIX`),
    KEY `consultation_id_regle_validation_idx` (`id_regle_validation`),
    KEY `consultation_id_etat_consultation_idx` (`id_etat_consultation`),
    KEY `consultation_doublon_idx` (`doublon`),
    KEY `Idx_Consultation_type_acces` (`type_acces`),
    KEY `Idx_Consultation_consultation_annulee` (`consultation_annulee`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `consultation_archive`
--

--  `consultation_archive`;


CREATE TABLE `consultation_archive`
(
    `id`                          int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`                   varchar(30) NOT NULL,
    `consultation_ref`            int(11)     NOT NULL,
    `chemin_fichier`              varchar(256) DEFAULT NULL,
    `date_archivage`              datetime     DEFAULT NULL,
    `poids_archivage`             int(11)      DEFAULT NULL,
    `annee_creation_consultation` int(11)      DEFAULT NULL,
    `status_global_transmission`  varchar(50) NOT NULL,
    `status_fragmentation`        tinyint(1)   DEFAULT 0,
    `nombre_bloc`                 int(11)      DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `unicite_consultation` (`consultation_ref`, `organisme`),
    KEY `status_global_transmission` (`status_global_transmission`(12)),
    KEY `consultation_archive_consultation` (`organisme`, `consultation_ref`),
    KEY `consultation_archive_status_fragmentation` (`status_fragmentation`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `consultation_archive_arcade`
--

--  `consultation_archive_arcade`;


CREATE TABLE `consultation_archive_arcade`
(
    `consultation_archive_id` int(11) NOT NULL,
    `archive_arcade_id`       int(11) DEFAULT NULL,
    PRIMARY KEY (`consultation_archive_id`),
    KEY `archive_arcade_id` (`archive_arcade_id`),
    CONSTRAINT `FK_archive_arcade_id` FOREIGN KEY (`archive_arcade_id`) REFERENCES `archive_arcade` (`id`),
    CONSTRAINT `FK_consultation_archive_id` FOREIGN KEY (`consultation_archive_id`) REFERENCES `consultation_archive` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


--
-- Table structure for table `consultation_archive_atlas`
--

--  `consultation_archive_atlas`;


CREATE TABLE `consultation_archive_atlas`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `doc_id`           varchar(256) NOT NULL,
    `numero_bloc`      int(11)      NOT NULL,
    `comp_id`          varchar(256) NOT NULL,
    `consultation_ref` int(11)      NOT NULL,
    `organisme`        varchar(256) NOT NULL,
    `taille`           int(11)      NOT NULL,
    `date_envoi`       datetime     NOT NULL,
    PRIMARY KEY (`id`),
    KEY `reference_organisme` (`consultation_ref`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `consultation_archive_bloc`
--

--  `consultation_archive_bloc`;


CREATE TABLE `consultation_archive_bloc`
(
    `id`                      int(11)      NOT NULL AUTO_INCREMENT,
    `consultation_archive_id` int(11)      NOT NULL,
    `doc_id`                  varchar(256) NOT NULL,
    `chemin_fichier`          varchar(256)          DEFAULT NULL,
    `numero_bloc`             int(11)               DEFAULT NULL,
    `poids_bloc`              int(11)               DEFAULT NULL,
    `date_envoi_debut`        datetime              DEFAULT NULL,
    `date_envoi_fin`          datetime              DEFAULT NULL,
    `status_transmission`     tinyint(1)   NOT NULL DEFAULT 0,
    `erreur`                  varchar(256)          DEFAULT NULL,
    `comp_id`                 varchar(256)          DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `doc_id` (`doc_id`),
    KEY `IDX_EFDAB250179E9C92` (`consultation_archive_id`),
    CONSTRAINT `FK_EFDAB250179E9C92` FOREIGN KEY (`consultation_archive_id`) REFERENCES `consultation_archive` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `consultation_archive_fichier`
--

--  `consultation_archive_fichier`;


CREATE TABLE `consultation_archive_fichier`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL,
    `consultation_ref` int(11)     NOT NULL,
    `chemin_fichier`   varchar(256) DEFAULT NULL,
    `poids`            int(11)      DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `reference_organisme` (`consultation_ref`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `consultation_document_cfe`
--

--  `consultation_document_cfe`;


CREATE TABLE `consultation_document_cfe`
(
    `id`                     int(11)      NOT NULL AUTO_INCREMENT,
    `id_justificatif`        int(11)      NOT NULL,
    `ref_consultation`       int(11)               DEFAULT NULL,
    `organisme_consultation` varchar(30)  NOT NULL,
    `nom_fichier`            varchar(256) NOT NULL,
    `id_entreprise`          int(11)               DEFAULT NULL,
    `taille_document`        varchar(80)           DEFAULT NULL,
    `id_blob`                int(11)      NOT NULL,
    `type_document`          varchar(256)          DEFAULT NULL,
    `date_fin_validite`      varchar(20)           DEFAULT '',
    `horodatage`             longblob     NOT NULL,
    `untrusteddate`          datetime     NOT NULL DEFAULT '0000-00-00 00:00:00',
    `consultation_id`        int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `id_justificatif` (`id_justificatif`, `ref_consultation`, `id_entreprise`),
    KEY `id_entreprise` (`id_entreprise`),
    KEY `ref_consultation` (`ref_consultation`),
    KEY `organisme_consultation` (`organisme_consultation`),
    KEY `blob` (`id_blob`),
    KEY `consultation_document_cfe_consultation` (`consultation_id`),
    CONSTRAINT `consultation_document_cfe_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Pour associer des documents du coffre-fort à une consultation';


--
-- Table structure for table `consultation_favoris`
--

--  `consultation_favoris`;


CREATE TABLE `consultation_favoris`
(
    `id`              int(11) NOT NULL AUTO_INCREMENT,
    `id_consultation` int(11) NOT NULL,
    `id_agent`        int(11) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `consultation_favoris_id_consultation_id_agent_unique` (`id_consultation`, `id_agent`),
    KEY `consultation_favoris_id_agent_FK` (`id_agent`),
    CONSTRAINT `consultation_favoris_id_agent_FK` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`) ON DELETE CASCADE,
    CONSTRAINT `consultation_favoris_id_consultation_FK` FOREIGN KEY (`id_consultation`) REFERENCES `consultation` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `decisionEnveloppe`
--

--  `decisionEnveloppe`;


CREATE TABLE `decisionEnveloppe`
(
    `id_decision_enveloppe`            int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`                        varchar(30)    NOT NULL DEFAULT '',
    `consultation_ref`                 int(11)        NOT NULL DEFAULT 0,
    `id_offre`                         int(11)        NOT NULL DEFAULT 0,
    `lot`                              int(11)        NOT NULL DEFAULT 0,
    `decision`                         enum ('0','1') NOT NULL DEFAULT '0',
    `date_notification`                varchar(10)             DEFAULT NULL,
    `code_postal`                      varchar(5)              DEFAULT NULL,
    `pme_pmi`                          int(11)        NOT NULL DEFAULT 0,
    `tranche_budgetaire`               int(11)                 DEFAULT NULL,
    `montant_marche`                   varchar(50)             DEFAULT NULL,
    `categorie`                        int(11)                 DEFAULT NULL,
    `objet_marche`                     text                    DEFAULT NULL,
    `commentaire`                      text                    DEFAULT NULL,
    `fichier_joint`                    int(11)                 DEFAULT NULL,
    `nom_fichier_joint`                varchar(100)            DEFAULT NULL,
    `type_enveloppe`                   enum ('1','2')          DEFAULT NULL,
    `numero_marche`                    text                    DEFAULT NULL,
    `statutEJ`                         text                    DEFAULT NULL,
    `note`                             varchar(200)   NOT NULL DEFAULT '',
    `classement`                       varchar(200)   NOT NULL DEFAULT '',
    `id_blob_pieces_notification`      varchar(20)    NOT NULL COMMENT 'Permet de stocker les idBLob des pièces de notification',
    `date_fin_marche_previsionnel`     varchar(20)             DEFAULT NULL,
    `ville`                            varchar(50)             DEFAULT NULL,
    `acronymePays_Attributaire`        varchar(10)             DEFAULT NULL,
    `pays_Attributaire`                varchar(50)             DEFAULT NULL,
    `siren_Attributaire`               varchar(20)             DEFAULT NULL,
    `nic_Attributaire`                 varchar(6)              DEFAULT NULL,
    `identifiantNational_Attributaire` varchar(50)             DEFAULT NULL,
    `rc_ville_attributaire`            varchar(250)            DEFAULT NULL,
    `rc_num_attributaire`              varchar(250)            DEFAULT NULL,
    `date_notification_reelle`         varchar(20)             DEFAULT NULL,
    `date_fin_marche_reelle`           varchar(20)             DEFAULT NULL,
    `envoi_interface`                  enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet de spécifier si la decision a été exportée vers une interface',
    `consultation_id`                  int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id_decision_enveloppe`, `organisme`),
    KEY `ref_Consultation` (`consultation_ref`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `destinataire_centrale_pub`
--

--  `destinataire_centrale_pub`;


CREATE TABLE `destinataire_centrale_pub`
(
    `id`             int(20)     NOT NULL AUTO_INCREMENT,
    `organisme`      varchar(30) NOT NULL,
    `id_annonce_jal` int(20)      DEFAULT NULL,
    `id_compte`      int(20)      DEFAULT NULL,
    `ids_journaux`   varchar(200) DEFAULT NULL,
    `statut`         char(1)      DEFAULT NULL,
    `date_envoi`     varchar(20)  DEFAULT '0000-00-00 00:00:00',
    `date_pub`       varchar(20)  DEFAULT '0000-00-00 00:00:00',
    `accuse`         varchar(20)  DEFAULT NULL,
    `id_echange`     int(11)      DEFAULT NULL,
    `date_ar`        varchar(25)  DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_annonce_jal` (`id_annonce_jal`),
    KEY `id_compte` (`id_compte`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `destinataire_mise_disposition`
--

--  `destinataire_mise_disposition`;


CREATE TABLE `destinataire_mise_disposition`
(
    `id`        int(11)      NOT NULL AUTO_INCREMENT,
    `organisme` varchar(30)  NOT NULL,
    `libelle`   varchar(256) NOT NULL,
    `code`      varchar(30)  NOT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `organisme` (`organisme`),
    CONSTRAINT `destinataire_mise_disposition_ibfk_1` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `document_serveur_docs`
--

--  `document_serveur_docs`;


CREATE TABLE `document_serveur_docs`
(
    `id`        int(11)      NOT NULL AUTO_INCREMENT,
    `nom`       varchar(100) NOT NULL,
    `poids`     int(11)      NOT NULL,
    `type`      varchar(20) DEFAULT NULL,
    `extension` varchar(10)  NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `document_template`
--

--  `document_template`;


CREATE TABLE `document_template`
(
    `id`           int(11)                              NOT NULL AUTO_INCREMENT,
    `document`     varchar(50) COLLATE utf8_unicode_ci  NOT NULL,
    `nom`          varchar(50) COLLATE utf8_unicode_ci  NOT NULL,
    `nom_afficher` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `donnees_annuelles_concession`
--

--  `donnees_annuelles_concession`;


CREATE TABLE `donnees_annuelles_concession`
(
    `id`                   int(11) NOT NULL AUTO_INCREMENT,
    `id_contrat`           int(11) NOT NULL,
    `valeur_depense`       double           DEFAULT NULL,
    `date_saisie`          datetime         DEFAULT NULL,
    `num_ordre`            int(11)          DEFAULT NULL,
    `suivi_publication_sn` int(2)  NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `FK_id_contrat_donnees_annuelles_concession` (`id_contrat`),
    CONSTRAINT `FK_id_contrat_donnees_annuelles_concession` FOREIGN KEY (`id_contrat`) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `donnees_annuelles_concession_tarif`
--

--  `donnees_annuelles_concession_tarif`;


CREATE TABLE `donnees_annuelles_concession_tarif`
(
    `id`                  int(11) NOT NULL AUTO_INCREMENT,
    `id_donnees_annuelle` int(11) NOT NULL,
    `intitule_tarif`      varchar(256) DEFAULT NULL,
    `montant`             double       DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_id_donnees_annuelle_tarif` (`id_donnees_annuelle`),
    CONSTRAINT `FK_id_donnees_annuelle_tarif` FOREIGN KEY (`id_donnees_annuelle`) REFERENCES `donnees_annuelles_concession` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `echange_doc_application_client`
--

--  `echange_doc_application_client`;


CREATE TABLE `echange_doc_application_client`
(
    `id`                         int(11)                              NOT NULL AUTO_INCREMENT,
    `echange_doc_application_id` int(11)                                       DEFAULT NULL,
    `code`                       varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `libelle`                    varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `actif`                      tinyint(1)                           NOT NULL DEFAULT 1,
    `cheminement_signature`      tinyint(1)                                    DEFAULT 0,
    `cheminement_ged`            tinyint(1)                                    DEFAULT 0,
    `cheminement_sae`            tinyint(1)                                    DEFAULT 0,
    `cheminement_tdt`            tinyint(1)                                    DEFAULT 0,
    `classification_1`           varchar(255) COLLATE utf8_unicode_ci          DEFAULT NULL,
    `classification_2`           varchar(255) COLLATE utf8_unicode_ci          DEFAULT NULL,
    `classification_3`           varchar(255) COLLATE utf8_unicode_ci          DEFAULT NULL,
    `classification_4`           varchar(255) COLLATE utf8_unicode_ci          DEFAULT NULL,
    `classification_5`           varchar(255) COLLATE utf8_unicode_ci          DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `IDX_echange_doc_application_id` (`echange_doc_application_id`),
    CONSTRAINT `FK_1E7B3AAF4A7843DC` FOREIGN KEY (`echange_doc_application_id`) REFERENCES `echange_doc_application` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `echange_doc`
--

--  `echange_doc`;


CREATE TABLE `echange_doc`
(
    `id`                                int(11)                              NOT NULL AUTO_INCREMENT,
    `echange_doc_application_client_id` int(11)                             DEFAULT NULL,
    `objet`                             varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `description`                       varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `consultation_id`                   int(11)                             DEFAULT NULL,
    `agent_id`                          int(11)                             DEFAULT NULL,
    `statut`                            varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `created_at`                        datetime                            DEFAULT NULL,
    `updated_at`                        datetime                            DEFAULT NULL,
    `cheminement_signature`             tinyint(1)                          DEFAULT 0,
    `cheminement_ged`                   tinyint(1)                          DEFAULT 0,
    `cheminement_sae`                   tinyint(1)                          DEFAULT 0,
    `cheminement_tdt`                   tinyint(1)                          DEFAULT 0,
    `id_contrat_titulaire`              int(11)                             DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `IDX_echange_doc_application_client_id` (`echange_doc_application_client_id`),
    KEY `IDX_consultation_id` (`consultation_id`),
    KEY `IDX_agent_id` (`agent_id`),
    KEY `FK_ECHANGE_DOC_CONTRAT_TITULAIRE` (`id_contrat_titulaire`),
    CONSTRAINT `FK_ECHANGE_DOC_CONTRAT_TITULAIRE` FOREIGN KEY (`id_contrat_titulaire`) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`),
    CONSTRAINT `FK_FEEA7C993414710B` FOREIGN KEY (`agent_id`) REFERENCES `Agent` (`id`),
    CONSTRAINT `FK_FEEA7C99561BA9BE` FOREIGN KEY (`echange_doc_application_client_id`) REFERENCES `echange_doc_application_client` (`id`),
    CONSTRAINT `FK_FEEA7C9962FF6CDF` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `echange_doc_application`
--

--  `echange_doc_application`;


CREATE TABLE `echange_doc_application`
(
    `id`              int(11)                              NOT NULL AUTO_INCREMENT,
    `code`            varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `libelle`         varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `flux_actes`      tinyint(1) DEFAULT 0,
    `primo_signature` tinyint(1) DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `echange_doc_application_client_organisme`
--

--  `echange_doc_application_client_organisme`;


CREATE TABLE `echange_doc_application_client_organisme`
(
    `id`                                int(11) NOT NULL AUTO_INCREMENT,
    `echange_doc_application_client_id` int(11) DEFAULT NULL,
    `organisme_id`                      int(11) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `IDX_echange_doc_application_client_id` (`echange_doc_application_client_id`),
    KEY `IDX_organisme_id` (`organisme_id`),
    CONSTRAINT `FK_B504956B561BA9BE` FOREIGN KEY (`echange_doc_application_client_id`) REFERENCES `echange_doc_application_client` (`id`),
    CONSTRAINT `FK_B504956B5DDD38F5` FOREIGN KEY (`organisme_id`) REFERENCES `Organisme` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `echange_doc_type_piece_standard`
--

--  `echange_doc_type_piece_standard`;


CREATE TABLE `echange_doc_type_piece_standard`
(
    `id`      int(11)                              NOT NULL AUTO_INCREMENT,
    `code`    varchar(20) COLLATE utf8_unicode_ci  NOT NULL,
    `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `echange_doc_blob`
--

--  `echange_doc_blob`;


CREATE TABLE `echange_doc_blob`
(
    `id`                             int(11) NOT NULL AUTO_INCREMENT,
    `echange_doc_id`                 int(11)                          DEFAULT NULL,
    `blob_organisme_id`              int(11)                          DEFAULT NULL,
    `chemin`                         longtext COLLATE utf8_unicode_ci DEFAULT NULL,
    `poids`                          int(11)                          DEFAULT NULL,
    `checksum`                       char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
    `echange_type_piece_actes_id`    int(11)                          DEFAULT NULL,
    `echange_type_piece_standard_id` int(11)                          DEFAULT NULL,
    `categorie_piece`                int(11) NOT NULL COMMENT '1:Principal, 2:Jeton, 3:Annexes',
    PRIMARY KEY (`id`),
    KEY `IDX_echange_doc_id` (`echange_doc_id`),
    KEY `IDX_blob_organisme_id` (`blob_organisme_id`),
    KEY `FK_ECHANGE_TYPE_PIECE_STANDARD` (`echange_type_piece_standard_id`),
    KEY `FK_ECHANGE_TYPE_PIECE_ACTES` (`echange_type_piece_actes_id`),
    CONSTRAINT `FK_3EEF16D757D67B2D` FOREIGN KEY (`echange_doc_id`) REFERENCES `echange_doc` (`id`),
    CONSTRAINT `FK_3EEF16D783A2B91F` FOREIGN KEY (`blob_organisme_id`) REFERENCES `blobOrganisme_file` (`id`),
    CONSTRAINT `FK_ECHANGE_TYPE_PIECE_STANDARD` FOREIGN KEY (`echange_type_piece_standard_id`) REFERENCES `echange_doc_type_piece_standard` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `echange_doc_historique`
--

--  `echange_doc_historique`;


CREATE TABLE `echange_doc_historique`
(
    `id`                  int(11)                             NOT NULL AUTO_INCREMENT,
    `echange_doc_id`      int(11)                               DEFAULT NULL,
    `agent_id`            int(11)                               DEFAULT NULL,
    `statut`              varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `created_at`          datetime                            NOT NULL,
    `message_fonctionnel` varchar(500) COLLATE utf8_unicode_ci  DEFAULT NULL,
    `message_technique`   varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `IDX_echange_doc_historique_id` (`id`),
    KEY `FK_ECHANGE_DOC_HISTORIQUE_AGENT` (`agent_id`),
    KEY `FK_ECHANGE_DOC_HISTORIQUE_ENCHANGE_DOC` (`echange_doc_id`),
    CONSTRAINT `FK_ECHANGE_DOC_HISTORIQUE_AGENT` FOREIGN KEY (`agent_id`) REFERENCES `Agent` (`id`),
    CONSTRAINT `FK_ECHANGE_DOC_HISTORIQUE_ENCHANGE_DOC` FOREIGN KEY (`echange_doc_id`) REFERENCES `echange_doc` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `echange_doc_type_piece_actes`
--

--  `echange_doc_type_piece_actes`;


CREATE TABLE `echange_doc_type_piece_actes`
(
    `id`                  int(11)                              NOT NULL AUTO_INCREMENT,
    `code`                varchar(20) COLLATE utf8_unicode_ci  NOT NULL,
    `libelle`             varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `type_nature`         varchar(20) COLLATE utf8_unicode_ci  NOT NULL,
    `code_classification` varchar(20) COLLATE utf8_unicode_ci  NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `echanges_interfaces`
--

--  `echanges_interfaces`;


CREATE TABLE `echanges_interfaces`
(
    `id`                     int(11)      NOT NULL AUTO_INCREMENT,
    `code_retour`            int(11)      NOT NULL,
    `service`                varchar(256) NOT NULL,
    `nom_batch`              varchar(256) NOT NULL,
    `variables_entree`       longtext     DEFAULT NULL,
    `resultat`               longtext     DEFAULT NULL,
    `type_flux`              varchar(256) DEFAULT NULL,
    `poids`                  int(11)      DEFAULT NULL,
    `information_metier`     longtext     DEFAULT NULL,
    `nb_flux`                int(11)      DEFAULT NULL,
    `debut_execution`        datetime     NOT NULL,
    `fin_execution`          datetime     DEFAULT NULL,
    `id_echanges_interfaces` int(11)      DEFAULT NULL,
    `nom_interface`          varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_code_retour` (`code_retour`),
    KEY `id_echanges_interfaces` (`id_echanges_interfaces`),
    CONSTRAINT `fk_code_retour` FOREIGN KEY (`code_retour`) REFERENCES `code_retour` (`code`),
    CONSTRAINT `fk_id_echanges_interfaces` FOREIGN KEY (`id_echanges_interfaces`) REFERENCES `echanges_interfaces` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `fichiers_liste_marches`
--

--  `fichiers_liste_marches`;


CREATE TABLE `fichiers_liste_marches`
(
    `id`            int(20)     NOT NULL AUTO_INCREMENT,
    `organisme`     varchar(30) NOT NULL DEFAULT '',
    `nom_fichier`   varchar(50) NOT NULL DEFAULT '',
    `fichier`       varchar(20) NOT NULL DEFAULT '',
    `horodatage`    longblob    NOT NULL,
    `untrusteddate` varchar(20) NOT NULL DEFAULT '',
    `taille`        int(20)     NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `formejuridique`
--

--  `formejuridique`;


CREATE TABLE `formejuridique`
(
    `formejuridique`            varchar(256) NOT NULL DEFAULT '',
    `ordre`                     int(5)                DEFAULT NULL,
    `libelle_formejuridique`    varchar(256) NOT NULL,
    `libelle_formejuridique_fr` varchar(256) NOT NULL,
    `libelle_formejuridique_en` varchar(256) NOT NULL,
    `libelle_formejuridique_es` varchar(256) NOT NULL,
    `libelle_formejuridique_su` varchar(256) NOT NULL,
    `libelle_formejuridique_du` varchar(256) NOT NULL,
    `libelle_formejuridique_cz` varchar(256) NOT NULL,
    `libelle_formejuridique_ar` varchar(256) NOT NULL,
    `libelle_formejuridique_it` varchar(256) NOT NULL,
    PRIMARY KEY (`formejuridique`),
    KEY `ordre` (`ordre`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `gestion_adresses`
--

--  `gestion_adresses`;


CREATE TABLE `gestion_adresses`
(
    `id`                          int(50)      NOT NULL AUTO_INCREMENT,
    `organisme`                   varchar(30)  NOT NULL DEFAULT '',
    `id_service`                  int(11)      NOT NULL DEFAULT 0,
    `id_agent`                    int(11)      NOT NULL DEFAULT 0,
    `nom_agent`                   varchar(250) NOT NULL DEFAULT '',
    `prenom_agent`                varchar(250) NOT NULL DEFAULT '',
    `adresse_depot_offres`        varchar(250) NOT NULL DEFAULT '',
    `adresse_retrais_dossiers`    varchar(250) NOT NULL DEFAULT '',
    `lieu_ouverture_plis`         varchar(250) NOT NULL DEFAULT '',
    `adresse_depot_offres_fr`     varchar(250) NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_fr` varchar(250) NOT NULL DEFAULT '',
    `lieu_ouverture_plis_fr`      varchar(250) NOT NULL DEFAULT '',
    `adresse_depot_offres_ar`     varchar(250) NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_ar` varchar(250) NOT NULL DEFAULT '',
    `lieu_ouverture_plis_ar`      varchar(250) NOT NULL DEFAULT '',
    `adresse_depot_offres_en`     varchar(250) NOT NULL DEFAULT '',
    `adresse_depot_offres_es`     varchar(250) NOT NULL DEFAULT '',
    `adresse_depot_offres_su`     varchar(250) NOT NULL DEFAULT '',
    `adresse_depot_offres_du`     varchar(250) NOT NULL DEFAULT '',
    `adresse_depot_offres_cz`     varchar(250) NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_en` varchar(250) NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_es` varchar(250) NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_su` varchar(250) NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_du` varchar(250) NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_cz` varchar(250) NOT NULL DEFAULT '',
    `lieu_ouverture_plis_en`      varchar(250) NOT NULL DEFAULT '',
    `lieu_ouverture_plis_es`      varchar(250) NOT NULL DEFAULT '',
    `lieu_ouverture_plis_su`      varchar(250) NOT NULL DEFAULT '',
    `lieu_ouverture_plis_du`      varchar(250) NOT NULL DEFAULT '',
    `lieu_ouverture_plis_cz`      varchar(250) NOT NULL DEFAULT '',
    `adresse_depot_offres_it`     varchar(250) NOT NULL DEFAULT '',
    `adresse_retrais_dossiers_it` varchar(250) NOT NULL DEFAULT '',
    `lieu_ouverture_plis_it`      varchar(250) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `web_service`
--

--  `web_service`;


CREATE TABLE `web_service`
(
    `id`                   int(11)      NOT NULL AUTO_INCREMENT,
    `route_ws`             varchar(256) NOT NULL,
    `method_ws`            varchar(256) NOT NULL,
    `nom_ws`               varchar(256) NOT NULL,
    `uniquement_technique` tinyint(1)   NOT NULL DEFAULT 1 COMMENT 'Définit si l accès du Ws n est qu accessible que par un agent technique',
    PRIMARY KEY (`id`),
    UNIQUE KEY `web_service_route_ws_method_ws` (`route_ws`, `method_ws`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `habilitation_agent_ws`
--

--  `habilitation_agent_ws`;


CREATE TABLE `habilitation_agent_ws`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `agent_id`       int(11) NOT NULL,
    `web_service_id` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `habilitation_agent_ws_id_agent_fk` (`agent_id`),
    KEY `habilitation_agent_ws_id_web_service_fk` (`web_service_id`),
    CONSTRAINT `habilitation_agent_ws_id_agent_fk` FOREIGN KEY (`agent_id`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `habilitation_agent_ws_id_web_service_fk` FOREIGN KEY (`web_service_id`) REFERENCES `web_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `historiques_consultation`
--

--  `historiques_consultation`;


CREATE TABLE `historiques_consultation`
(
    `id`               int(50)      NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30)  NOT NULL DEFAULT '',
    `ref_consultation` int(50)      NOT NULL DEFAULT 0,
    `statut`           char(1)      NOT NULL DEFAULT '',
    `id_agent`         int(50)      NOT NULL DEFAULT 0,
    `nom_agent`        varchar(250) NOT NULL DEFAULT '',
    `prenom_agent`     varchar(250) NOT NULL DEFAULT '',
    `nom_element`      varchar(250) NOT NULL DEFAULT '',
    `valeur`           varchar(250) NOT NULL DEFAULT '',
    `valeur_detail_1`  varchar(250) NOT NULL DEFAULT '',
    `valeur_detail_2`  varchar(250) NOT NULL DEFAULT '',
    `numero_lot`       varchar(250) NOT NULL DEFAULT '',
    `horodatage`       longblob              DEFAULT NULL,
    `untrusteddate`    varchar(20)           DEFAULT '0000-00-00 00:00:00',
    `consultation_id`  int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `ref_Consultation` (`ref_consultation`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `historisation_mot_de_passe`
--

--  `historisation_mot_de_passe`;


CREATE TABLE `historisation_mot_de_passe`
(
    `id`                  int(11)     NOT NULL AUTO_INCREMENT,
    `ancien_mot_de_passe` varchar(64) NOT NULL,
    `date_modification`   datetime    NOT NULL,
    `id_inscrit`          int(11)     NOT NULL,
    PRIMARY KEY (`id`),
    KEY `id_inscrit_histo_mdp_FK` (`id_inscrit`),
    CONSTRAINT `id_inscrit_histo_mdp_FK` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `invite_permanent_contrat`
--

--  `invite_permanent_contrat`;


CREATE TABLE `invite_permanent_contrat`
(
    `id`                   int(11) unsigned NOT NULL AUTO_INCREMENT,
    `organisme`            varchar(256) DEFAULT NULL,
    `service`              int(11)      DEFAULT NULL,
    `lot`                  varchar(256) DEFAULT NULL,
    `id_consultation`      int(11)      DEFAULT NULL,
    `id_contrat_titulaire` int(11)      DEFAULT NULL,
    `date_decision`        datetime     DEFAULT NULL,
    `created_at`           datetime     DEFAULT NULL,
    `updated_at`           datetime     DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `invite_permanent_transverse`
--

--  `invite_permanent_transverse`;


CREATE TABLE `invite_permanent_transverse`
(
    `id`         int(11)     NOT NULL AUTO_INCREMENT,
    `service_id` int(11) DEFAULT NULL,
    `agent_id`   int(11)     NOT NULL,
    `acronyme`   varchar(30) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `invitePermanentTransverse_service_idx` (`service_id`),
    KEY `invitePermanentTransverse_agent_idx` (`agent_id`),
    KEY `invite_permanent_transverse_organisme_acronyme_FK` (`acronyme`),
    CONSTRAINT `invite_permanent_transverse_organisme_acronyme_FK` FOREIGN KEY (`acronyme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `t_invite_permanent_transverse_agent_id_FK` FOREIGN KEY (`agent_id`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `t_invite_permanent_transverse_service_id_FK` FOREIGN KEY (`service_id`) REFERENCES `Service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `mail_type_group`
--

--  `mail_type_group`;


CREATE TABLE `mail_type_group`
(
    `id`    int(11)      NOT NULL AUTO_INCREMENT,
    `label` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `mail_type`
--

--  `mail_type`;


CREATE TABLE `mail_type`
(
    `id`                 int(11)      NOT NULL AUTO_INCREMENT,
    `mail_type_group_id` int(11)      NOT NULL,
    `label`              varchar(255) NOT NULL,
    `code`               varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `code` (`code`),
    KEY `mail_type_mail_type_group_id` (`mail_type_group_id`),
    CONSTRAINT `mail_type_mail_type_group_id` FOREIGN KEY (`mail_type_group_id`) REFERENCES `mail_type_group` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `mail_template`
--

--  `mail_template`;


CREATE TABLE `mail_template`
(
    `id`                     int(11)      NOT NULL AUTO_INCREMENT,
    `mail_type_id`           int(11)      NOT NULL,
    `code`                   varchar(255) NOT NULL,
    `objet`                  varchar(255) NOT NULL,
    `corps`                  longtext     NOT NULL,
    `ordre_affichage`        int(11)               DEFAULT NULL,
    `envoi_modalite`         varchar(255) NOT NULL DEFAULT 'AVEC_AR' COMMENT 'AVEC_AR | SANS_AR',
    `envoi_modalite_figee`   tinyint(1)   NOT NULL DEFAULT 0,
    `reponse_attendue`       tinyint(1)   NOT NULL DEFAULT 0,
    `reponse_attendue_figee` tinyint(1)   NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE KEY `code` (`code`),
    KEY `mail_template_mail_type_id` (`mail_type_id`),
    CONSTRAINT `mail_template_mail_type_id` FOREIGN KEY (`mail_type_id`) REFERENCES `mail_type` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `migration_manager`
--

--  `migration_manager`;


CREATE TABLE `migration_manager`
(
    `version`        varchar(200) COLLATE utf8_unicode_ci NOT NULL,
    `executed_at`    datetime DEFAULT NULL,
    `execution_time` int(11)  DEFAULT NULL,
    PRIMARY KEY (`version`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `mode_execution_contrat`
--

--  `mode_execution_contrat`;


CREATE TABLE `mode_execution_contrat`
(
    `id`         int(11) NOT NULL DEFAULT 0,
    `libelle`    varchar(256)     DEFAULT NULL,
    `libelle_fr` varchar(256)     DEFAULT NULL,
    `libelle_en` varchar(256)     DEFAULT NULL,
    `libelle_es` varchar(256)     DEFAULT NULL,
    `libelle_su` varchar(256)     DEFAULT NULL,
    `libelle_du` varchar(256)     DEFAULT NULL,
    `libelle_cz` varchar(256)     DEFAULT NULL,
    `libelle_ar` varchar(256)     DEFAULT NULL,
    `libelle_it` varchar(256)     DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_etablissement`
--

--  `t_etablissement`;


CREATE TABLE `t_etablissement`
(
    `id_etablissement`         int(11)        NOT NULL AUTO_INCREMENT,
    `id_entreprise`            int(11)        NOT NULL,
    `code_etablissement`       char(5)        NOT NULL,
    `est_siege`                enum ('0','1') NOT NULL DEFAULT '0',
    `adresse`                  varchar(80)    NOT NULL,
    `adresse2`                 varchar(80)             DEFAULT NULL,
    `code_postal`              varchar(20)    NOT NULL,
    `ville`                    varchar(50)    NOT NULL,
    `pays`                     varchar(50)    NOT NULL,
    `saisie_manuelle`          enum ('0','1') NOT NULL DEFAULT '0',
    `id_initial`               int(11)        NOT NULL,
    `date_creation`            datetime       NOT NULL,
    `date_modification`        datetime       NOT NULL,
    `date_suppression`         datetime                DEFAULT NULL,
    `statut_actif`             enum ('0','1') NOT NULL DEFAULT '1',
    `inscrit_annuaire_defense` enum ('0','1') NOT NULL DEFAULT '0',
    `long`                     double                  DEFAULT NULL COMMENT 'Longitude',
    `lat`                      double                  DEFAULT NULL COMMENT 'Latitude',
    `maj_long_lat`             datetime                DEFAULT NULL COMMENT 'Date de mise à jour de la latitude et longitude',
    `tva_intracommunautaire`   varchar(20)             DEFAULT NULL,
    `etat_administratif`       char(1)                 DEFAULT NULL COMMENT 'etat administratif : A => active, F => fermé',
    `date_fermeture`           datetime                DEFAULT NULL COMMENT 'timestamp de la date de fermeture',
    `id_externe`               varchar(50)    NOT NULL DEFAULT '0',
    PRIMARY KEY (`id_etablissement`),
    KEY `id_entreprise_idx` (`id_entreprise`),
    CONSTRAINT `t_etablissement_id_entreprise_fk` FOREIGN KEY (`id_entreprise`) REFERENCES `Entreprise` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `modification_contrat`
--

--  `modification_contrat`;


CREATE TABLE `modification_contrat`
(
    `id`                    int(11)      NOT NULL AUTO_INCREMENT,
    `id_contrat_titulaire`  int(11)      NOT NULL,
    `num_ordre`             int(11)      NOT NULL,
    `date_creation`         datetime     NOT NULL,
    `date_modification`     datetime     NOT NULL,
    `id_agent`              int(11)      NOT NULL,
    `objet_modification`    varchar(256) NOT NULL,
    `date_signature`        datetime     NOT NULL,
    `montant`               varchar(256)          DEFAULT NULL,
    `id_etablissement`      int(11)               DEFAULT NULL,
    `duree_marche`          int(11)               DEFAULT NULL,
    `statut_publication_sn` int(2)       NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `id_contrat_titulaire_FK` (`id_contrat_titulaire`),
    KEY `id_agent_FK` (`id_agent`),
    KEY `id_etablissement_FK` (`id_etablissement`),
    CONSTRAINT `id_agent_FK` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`),
    CONSTRAINT `id_contrat_titulaire_FK` FOREIGN KEY (`id_contrat_titulaire`) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`),
    CONSTRAINT `id_etablissement_FK` FOREIGN KEY (`id_etablissement`) REFERENCES `t_etablissement` (`id_etablissement`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `nature_acte_juridique`
--

--  `nature_acte_juridique`;


CREATE TABLE `nature_acte_juridique`
(
    `id`         int(11) NOT NULL DEFAULT 0,
    `libelle`    varchar(50)      DEFAULT NULL,
    `libelle_fr` varchar(50)      DEFAULT NULL,
    `libelle_en` varchar(50)      DEFAULT NULL,
    `libelle_es` varchar(50)      DEFAULT NULL,
    `libelle_su` varchar(50)      DEFAULT NULL,
    `libelle_du` varchar(50)      DEFAULT NULL,
    `libelle_cz` varchar(50)      DEFAULT NULL,
    `libelle_ar` varchar(50)      DEFAULT NULL,
    `libelle_it` varchar(50)      DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `passation_marche_a_venir`
--

--  `passation_marche_a_venir`;


CREATE TABLE `passation_marche_a_venir`
(
    `id`                                          int(11)        NOT NULL AUTO_INCREMENT,
    `organisme`                                   varchar(30)    NOT NULL,
    `id_passation_consultation`                   int(11)        NOT NULL DEFAULT 0,
    `lot`                                         int(11)        NOT NULL DEFAULT 0,
    `montant_estime`                              varchar(11)    NOT NULL DEFAULT '0',
    `id_nature_acte_juridique`                    int(11)        NOT NULL DEFAULT 0,
    `forme_groupement`                            int(11)        NOT NULL DEFAULT 0,
    `variante_autorisee`                          enum ('0','1') NOT NULL DEFAULT '0',
    `variante_technique_obligatoire`              enum ('0','1') NOT NULL DEFAULT '0',
    `nombre_variante_technique`                   int(11)        NOT NULL DEFAULT 0,
    `description_variante_technique`              text           NOT NULL,
    `mode_execution_contrat`                      int(11)        NOT NULL DEFAULT 0,
    `type_bon_commande`                           int(11)        NOT NULL DEFAULT 0,
    `min_bon_commande`                            varchar(11)    NOT NULL DEFAULT '0',
    `max_bon_commande`                            varchar(11)    NOT NULL DEFAULT '0',
    `duree_execution_marche_hors_reconduction`    int(11)        NOT NULL DEFAULT 0,
    `nombre_reconduction`                         int(11)        NOT NULL DEFAULT 0,
    `duree_total_marche`                          int(11)        NOT NULL DEFAULT 0,
    `ccag_applicable`                             int(11)        NOT NULL DEFAULT 0,
    `marche_transversal`                          enum ('0','1') NOT NULL DEFAULT '0',
    `date_reception_analyse_offre`                varchar(10)             DEFAULT NULL,
    `date_formulation_observation_projet_rapport` varchar(10)             DEFAULT NULL,
    `date_retour_projet_rapport_finalise`         varchar(10)             DEFAULT NULL,
    `date_validation_projet_rapport`              varchar(10)             DEFAULT NULL,
    `projet_rapport_vu_par`                       int(11)                 DEFAULT NULL,
    `date_reunion_attribution`                    varchar(10)             DEFAULT NULL,
    `decision`                                    int(11)                 DEFAULT NULL,
    `date_envoi_courrier_condidat_non_retenu`     varchar(10)             DEFAULT NULL,
    `date_signature_marche_pa`                    varchar(10)             DEFAULT NULL,
    `date_reception_controle_legalite`            varchar(10)             DEFAULT NULL,
    `date_formulation_observation_dossier`        varchar(10)             DEFAULT NULL,
    `date_retour_dossier_finalise`                varchar(10)             DEFAULT NULL,
    `date_transmission_prefecture`                varchar(10)             DEFAULT NULL,
    `dossier_vu_par`                              int(11)                 DEFAULT NULL,
    `date_validation_rapport_information`         varchar(10)             DEFAULT NULL,
    `commentaire`                                 text                    DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `id_passation_consultation` (`id_passation_consultation`),
    CONSTRAINT `passation_marche_a_venir_fk` FOREIGN KEY (`id_passation_consultation`) REFERENCES `Passation_consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `piece_genere_consultation`
--

--  `piece_genere_consultation`;


CREATE TABLE `piece_genere_consultation`
(
    `id`              int(11)  NOT NULL AUTO_INCREMENT,
    `blob_id`         int(11)  NOT NULL,
    `consultation_id` int(11)  NOT NULL,
    `created_at`      datetime NOT NULL,
    `updated_at`      datetime                             DEFAULT NULL,
    `token`           varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `entreprise_id`   int(11)                              DEFAULT NULL,
    `lot_id`          int(11)                              DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `genere_piece_consultation_consultation_id_fk` (`consultation_id`),
    KEY `genere_piece_consultation_blob_id_fk` (`blob_id`),
    KEY `genere_piece_consultation_entreprise_id_fk` (`entreprise_id`),
    KEY `genere_piece_consultation_CategorieLot_lot_fk` (`lot_id`),
    CONSTRAINT `genere_piece_consultation_CategorieLot_lot_fk` FOREIGN KEY (`lot_id`) REFERENCES `CategorieLot` (`id`) ON DELETE NO ACTION,
    CONSTRAINT `genere_piece_consultation_blob_id_fk` FOREIGN KEY (`blob_id`) REFERENCES `blobOrganisme_file` (`id`),
    CONSTRAINT `genere_piece_consultation_consultation_id_fk` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`),
    CONSTRAINT `genere_piece_consultation_entreprise_id_fk` FOREIGN KEY (`entreprise_id`) REFERENCES `Entreprise` (`id`) ON DELETE NO ACTION
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `plateforme_virtuelle_organisme`
--

--  `plateforme_virtuelle_organisme`;


CREATE TABLE `plateforme_virtuelle_organisme`
(
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT,
    `plateforme_id` int(11) unsigned NOT NULL,
    `organisme_id`  int(11)          NOT NULL,
    PRIMARY KEY (`id`),
    KEY `plateforme_id_fk` (`plateforme_id`),
    KEY `organisme_id_fk` (`organisme_id`),
    CONSTRAINT `organisme_id_fk` FOREIGN KEY (`organisme_id`) REFERENCES `Organisme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `plateforme_id_fk` FOREIGN KEY (`plateforme_id`) REFERENCES `plateforme_virtuelle` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `programme_previsionnel`
--

--  `programme_previsionnel`;


CREATE TABLE `programme_previsionnel`
(
    `id`            int(20)        NOT NULL AUTO_INCREMENT,
    `organisme`     varchar(30)    NOT NULL DEFAULT '',
    `nom_fichier`   varchar(50)    NOT NULL DEFAULT '',
    `fichier`       varchar(20)    NOT NULL DEFAULT '',
    `date`          varchar(20)    NOT NULL DEFAULT '',
    `horodatage`    longblob       NOT NULL,
    `untrusteddate` varchar(20)    NOT NULL DEFAULT '',
    `taille`        int(20)        NOT NULL DEFAULT 0,
    `annee`         varchar(20)    NOT NULL,
    `service_id`    int(20)        NOT NULL DEFAULT 0,
    `afficher`      enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`, `organisme`),
    KEY `service_id` (`service_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `questions_dce`
--

--  `questions_dce`;


CREATE TABLE `questions_dce`
(
    `id`                      int(22)        NOT NULL AUTO_INCREMENT,
    `organisme`               varchar(30)    NOT NULL DEFAULT '',
    `consultation_ref`        int(11)        NOT NULL DEFAULT 0,
    `date_depot`              datetime       NOT NULL DEFAULT '0000-00-00 00:00:00',
    `email`                   varchar(100)   NOT NULL DEFAULT '',
    `nom`                     varchar(80)             DEFAULT NULL,
    `entreprise`              varchar(50)             DEFAULT NULL,
    `adresse`                 varchar(200)            DEFAULT NULL,
    `cp`                      varchar(100)            DEFAULT NULL,
    `ville`                   varchar(150)            DEFAULT NULL,
    `tel`                     varchar(150)            DEFAULT NULL,
    `fax`                     varchar(150)            DEFAULT NULL,
    `question`                longtext       NOT NULL,
    `statut`                  int(11)        NOT NULL DEFAULT 0,
    `date_reponse`            date           NOT NULL DEFAULT '0000-00-00',
    `personne_repondu`        text                    DEFAULT NULL,
    `type_depot`              enum ('1','2') NOT NULL DEFAULT '1',
    `pays`                    varchar(150)            DEFAULT NULL,
    `Observation`             text                    DEFAULT NULL,
    `siret`                   varchar(14)             DEFAULT NULL,
    `identifiant_national`    varchar(20)             DEFAULT NULL,
    `acronyme_pays`           varchar(10)             DEFAULT NULL,
    `adresse2`                varchar(80)             DEFAULT NULL,
    `id_fichier`              int(11)                 DEFAULT NULL,
    `nom_fichier`             varchar(100)            DEFAULT NULL,
    `prenom`                  varchar(80)             DEFAULT NULL,
    `siret_etranger`          varchar(20)             DEFAULT NULL,
    `id_inscrit`              int(11)        NOT NULL DEFAULT 0,
    `id_entreprise`           int(11)        NOT NULL DEFAULT 0,
    `consultation_id`         int(11)                 DEFAULT NULL,
    `plateforme_virtuelle_id` int(11) unsigned        DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `id_inscrit` (`id_inscrit`),
    KEY `Entreprise` (`id_entreprise`),
    KEY `fk_plateforme_virtuelle_questions_dce` (`plateforme_virtuelle_id`),
    CONSTRAINT `fk_plateforme_virtuelle_questions_dce` FOREIGN KEY (`plateforme_virtuelle_id`) REFERENCES `plateforme_virtuelle` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `renseignements_boamp`
--

--  `renseignements_boamp`;


CREATE TABLE `renseignements_boamp`
(
    `id`                      int(50)     NOT NULL AUTO_INCREMENT,
    `acronymeOrg`             varchar(30) NOT NULL,
    `id_type`                 int(50)     NOT NULL,
    `id_compte`               int(50)     NOT NULL,
    `correspondant`           varchar(200) DEFAULT NULL,
    `organisme`               varchar(200) DEFAULT NULL,
    `adresse`                 varchar(200) DEFAULT NULL,
    `cp`                      varchar(200) DEFAULT NULL,
    `ville`                   varchar(200) DEFAULT NULL,
    `pays`                    varchar(200) DEFAULT NULL,
    `telephone`               varchar(200) DEFAULT NULL,
    `poste`                   varchar(200) DEFAULT NULL,
    `fax`                     varchar(200) DEFAULT NULL,
    `mail`                    varchar(200) DEFAULT NULL,
    `url`                     text         DEFAULT NULL,
    `organe_charge_procedure` text         DEFAULT NULL,
    PRIMARY KEY (`id`, `acronymeOrg`),
    KEY `id_type` (`id_type`, `id_compte`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `responsableengagement`
--

--  `responsableengagement`;


CREATE TABLE `responsableengagement`
(
    `id`            int(11) NOT NULL AUTO_INCREMENT,
    `entreprise_id` int(11) NOT NULL DEFAULT 0,
    `nom`           varchar(30)      DEFAULT NULL,
    `prenom`        varchar(30)      DEFAULT NULL,
    `qualite`       varchar(32)      DEFAULT NULL,
    `email`         varchar(100)     DEFAULT NULL,
    `telephone`     varchar(100)     DEFAULT NULL,
    `fax`           varchar(100)     DEFAULT NULL,
    `id_initial`    int(11)          DEFAULT 0,
    PRIMARY KEY (`id`, `entreprise_id`),
    KEY `entreprise_id` (`entreprise_id`),
    CONSTRAINT `responsableengagement_ibfk_1` FOREIGN KEY (`entreprise_id`) REFERENCES `Entreprise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `resultat_analyse`
--

--  `resultat_analyse`;


CREATE TABLE `resultat_analyse`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`        varchar(30) NOT NULL DEFAULT '',
    `consultation_ref` int(11)     NOT NULL DEFAULT 0,
    `id_offre`         int(11)     NOT NULL DEFAULT 0,
    `lot`              int(11)     NOT NULL DEFAULT 0,
    `montant_offre`    varchar(200)         DEFAULT NULL,
    `note`             varchar(200)         DEFAULT NULL,
    `classement`       tinyint(200)         DEFAULT NULL,
    `observation`      varchar(200)         DEFAULT NULL,
    `type_enveloppe`   int(1)      NOT NULL DEFAULT 0,
    `offre_variante`   enum ('0','1')       DEFAULT NULL COMMENT 'Ce champ permet de definir le montant de bese de ceux de variantes: null pour le montant d''une offre normale, 0 pour le montant de base et 1 pour les montants de variantes',
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `ref_Consultation` (`consultation_ref`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `resultat_analyse_decision`
--

--  `resultat_analyse_decision`;


CREATE TABLE `resultat_analyse_decision`
(
    `id`                  int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`           varchar(30) NOT NULL DEFAULT '',
    `consultation_ref`    int(11)     NOT NULL DEFAULT 0,
    `lot`                 int(11)     NOT NULL DEFAULT 0,
    `date_decision`       varchar(20)          DEFAULT NULL,
    `type_decision`       varchar(200)         DEFAULT NULL,
    `commentaire`         text                 DEFAULT NULL,
    `autre_type_decision` text                 DEFAULT NULL,
    `autre`               text                 DEFAULT NULL,
    `consultation_id`     int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `ref_Consultation` (`consultation_ref`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `sso_agent`
--

--  `sso_agent`;


CREATE TABLE `sso_agent`
(
    `id_sso`            varchar(40) NOT NULL,
    `id_agent`          int(20)     NOT NULL,
    `organisme`         varchar(20) NOT NULL,
    `id_service`        int(20)     NOT NULL,
    `date_connexion`    varchar(20) NOT NULL,
    `date_last_request` varchar(20) NOT NULL,
    PRIMARY KEY (`id_sso`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `sso_entreprise`
--

--  `sso_entreprise`;


CREATE TABLE `sso_entreprise`
(
    `id_sso`            varchar(40) NOT NULL,
    `id_inscrit`        int(11)     NOT NULL,
    `date_connexion`    varchar(20) NOT NULL,
    `date_last_request` varchar(20) NOT NULL,
    `login`             varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id_sso`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `supervision_interface`
--

--  `supervision_interface`;


CREATE TABLE `supervision_interface`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `created_date`     date        NOT NULL COMMENT 'Date de création',
    `webservice_batch` varchar(50) NOT NULL COMMENT 'Nom du batch ou du webservice',
    `service`          varchar(50) NOT NULL,
    `nom_interface`    varchar(50) DEFAULT NULL COMMENT 'Nom de l''interface utilisé par le rapports',
    `total`            int(11)     NOT NULL,
    `total_ok`         int(11)     DEFAULT NULL,
    `poids`            bigint(20)  DEFAULT NULL,
    `poids_ok`         bigint(20)  DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_CAO_Commission_Agent`
--

--  `t_CAO_Commission_Agent`;


CREATE TABLE `t_CAO_Commission_Agent`
(
    `id_commission_agent`         bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l''agent de commission',
    `id_agent`                    int(11)     NOT NULL COMMENT 'Identifiant de l''agent',
    `organisme`                   varchar(30) NOT NULL COMMENT 'Acronyme de l''organisme',
    `id_commission`               bigint(20)  NOT NULL COMMENT 'Identifiant de la commission à laquelle appartient l''agent',
    `id_ref_val_type_voix_defaut` int(11)     NOT NULL COMMENT 'Type de voix défini par défaut pour la commission',
    `type_compte`                 tinyint(4)  NOT NULL COMMENT 'Rappel du type de compte de l''agent "non élu" ou "élu" ',
    PRIMARY KEY (`id_commission_agent`, `organisme`),
    KEY `id_commission_idx` (`id_commission`),
    KEY `organisme_idx` (`organisme`),
    KEY `id_agent_idx` (`id_agent`),
    CONSTRAINT `t_CAO_Commission_Agent_id_agent_fk` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`),
    CONSTRAINT `t_CAO_Commission_Agent_id_commission_fk` FOREIGN KEY (`id_commission`) REFERENCES `t_CAO_Commission` (`id_commission`),
    CONSTRAINT `t_CAO_Commission_Agent_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `t_CAO_Commission_Intervenant_Externe`
--

--  `t_CAO_Commission_Intervenant_Externe`;


CREATE TABLE `t_CAO_Commission_Intervenant_Externe`
(
    `id_commission_intervenant_externe` bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l''intervenant externe pour la commission',
    `id_intervenant_externe`            bigint(20)  NOT NULL COMMENT 'Identifiant de l''intervenant externe de l''organisme',
    `id_commission`                     bigint(20)  NOT NULL COMMENT 'Identifiant de la commission référencée',
    `organisme`                         varchar(30) NOT NULL COMMENT 'Acronyme de l''organisme',
    `id_ref_val_type_voix_defaut`       int(11)     NOT NULL COMMENT 'Type de voix de l''intervenant pour la commission',
    PRIMARY KEY (`id_commission_intervenant_externe`, `organisme`),
    KEY `organisme_idx` (`organisme`),
    KEY `id_intervenant_externe_idx` (`id_intervenant_externe`),
    KEY `id_commission_idx` (`id_commission`),
    CONSTRAINT `t_CAO_Commission_Intervenant_Externe_id_commission_fk` FOREIGN KEY (`id_commission`) REFERENCES `t_CAO_Commission` (`id_commission`),
    CONSTRAINT `t_CAO_Commission_Intervenant_Externe_id_intervenant_externe_fk` FOREIGN KEY (`id_intervenant_externe`) REFERENCES `t_CAO_Intervenant_Externe` (`id_intervenant_externe`),
    CONSTRAINT `t_CAO_Commission_Intervenant_Externe_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_CAO_Ordre_De_Passage`
--

--  `t_CAO_Ordre_De_Passage`;


CREATE TABLE `t_CAO_Ordre_De_Passage`
(
    `id_ordre_de_passage`        bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l''ordre de passage en commission de la consultation',
    `organisme`                  varchar(30) NOT NULL COMMENT 'Acronyme de l''organisme pour lequel a lieu la séance',
    `id_seance`                  bigint(20)  NOT NULL COMMENT 'Identifiant de la séance',
    `id_commission`              bigint(20)  NOT NULL COMMENT 'Identifiant de la commission définissant la séance',
    `id_commission_consultation` bigint(11)  NOT NULL COMMENT 'Identifiant de la consultation passant en commission',
    `ordre_de_passage`           tinyint(4)  NOT NULL COMMENT 'Numéro de l''ordre de passage de la consultation en commission',
    `id_ref_org_val_etape`       int(11)     NOT NULL COMMENT 'Valeur référentielle de l''etape du passage en commission',
    `date_seance`                datetime    NOT NULL COMMENT 'Rappel de la date de la séance de commission',
    `date_passage`               datetime    NOT NULL COMMENT 'Date de passage de la consultation à l''ordre du jour de la séance',
    PRIMARY KEY (`id_ordre_de_passage`, `organisme`),
    KEY `id_commission_idx` (`id_commission`),
    KEY `id_seance_idx` (`id_seance`),
    KEY `id_commission_consultation_idx` (`id_commission_consultation`),
    KEY `organisme_idx` (`organisme`),
    KEY `date_seance_idx` (`date_seance`),
    KEY `date_passage_idx` (`date_passage`),
    CONSTRAINT `t_CAO_Ordre_De_Passage_date_seance_fk` FOREIGN KEY (`date_seance`) REFERENCES `t_CAO_Seance` (`date`) ON UPDATE CASCADE,
    CONSTRAINT `t_CAO_Ordre_De_Passage_id_commission_consultation_fk` FOREIGN KEY (`id_commission_consultation`) REFERENCES `t_CAO_Commission_Consultation` (`id_commission_consultation`),
    CONSTRAINT `t_CAO_Ordre_De_Passage_id_commission_fk` FOREIGN KEY (`id_commission`) REFERENCES `t_CAO_Commission` (`id_commission`),
    CONSTRAINT `t_CAO_Ordre_De_Passage_id_seance_fk` FOREIGN KEY (`id_seance`) REFERENCES `t_CAO_Seance` (`id_seance`),
    CONSTRAINT `t_CAO_Ordre_De_Passage_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Ordre de passage dans une séance de commission';


--
-- Table structure for table `t_CAO_Ordre_Du_Jour`
--

--  `t_CAO_Ordre_Du_Jour`;


CREATE TABLE `t_CAO_Ordre_Du_Jour`
(
    `id_ordre_du_jour`           bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l''ordre du jour de la séance',
    `organisme`                  varchar(30) NOT NULL COMMENT 'Acronyme de l''organisme pour lequel a lieu la séance',
    `id_seance`                  bigint(20)  NOT NULL COMMENT 'Identifiant de la séance',
    `id_commission`              bigint(20)  NOT NULL COMMENT 'Identifiant de la commission définissant la séance',
    `id_commission_consultation` bigint(11)  NOT NULL COMMENT 'Identifiant de la consultation passant en commission',
    `numero`                     tinyint(4)  NOT NULL COMMENT 'Numéro de l''ordre du jour de la séance',
    `id_ref_org_val_etape`       int(11)     NOT NULL COMMENT 'Valeur référentielle de l''etape du passage en commission',
    `intitule`                   longtext    NOT NULL COMMENT 'Objet de l''ordre du jour qui correspond à l''intitule de la consultation',
    `date_seance`                datetime    NOT NULL COMMENT 'Rappel de la date de la séence de l''ordre du jour',
    `heure_passage`              time        NOT NULL COMMENT 'Heure de passage de la consultation à l''ordre du jour de la séance',
    PRIMARY KEY (`id_ordre_du_jour`, `organisme`),
    KEY `id_commission_idx` (`id_commission`),
    KEY `id_seance_idx` (`id_seance`),
    KEY `id_commission_consultation_idx` (`id_commission_consultation`),
    KEY `organisme_idx` (`organisme`),
    KEY `date_seance_idx` (`date_seance`),
    KEY `heure_passage_idx` (`heure_passage`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_CAO_Ordre_Du_Jour_Intervenant`
--

--  `t_CAO_Ordre_Du_Jour_Intervenant`;


CREATE TABLE `t_CAO_Ordre_Du_Jour_Intervenant`
(
    `id_ordre_du_jour_intervenant` bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l''intervenant de l''ordre du jour',
    `organisme`                    varchar(30) NOT NULL COMMENT 'Acronyme de l''organisme auquel l''intervenant est rattaché',
    `id_ordre_de_passage`          bigint(20)  NOT NULL COMMENT 'Identifiant de l''ordre de passage en commission de la consultation',
    `id_intervenant_externe`       bigint(20)  NOT NULL COMMENT 'Identifiant de l''intervenant',
    `id_agent`                     int(11)     NOT NULL COMMENT 'Identifiant de l''agent',
    `id_ref_val_type_voix`         int(11)     NOT NULL COMMENT 'Valeur référentielle du type de voix de l''intervenant',
    PRIMARY KEY (`id_ordre_du_jour_intervenant`, `organisme`),
    KEY `id_intervenant_externe_idx` (`id_intervenant_externe`),
    KEY `id_agent_idx` (`id_agent`),
    KEY `organisme_idx` (`organisme`),
    KEY `id_ordre_de_passage_idx` (`id_ordre_de_passage`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `t_CAO_Seance_Agent`
--

--  `t_CAO_Seance_Agent`;


CREATE TABLE `t_CAO_Seance_Agent`
(
    `id_seance_agent`      bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l''agent de la séance',
    `id_seance`            bigint(20)  NOT NULL COMMENT 'Identifiant de la séance',
    `id_agent`             int(11)     NOT NULL COMMENT 'Identifiant l''agent',
    `organisme`            varchar(30) NOT NULL DEFAULT '',
    `id_ref_val_type_voix` int(11)     NOT NULL COMMENT 'Type de voix de l''agent pour la séance',
    `type_compte`          tinyint(4)  NOT NULL DEFAULT 0 COMMENT 'Rappel du type de compte de l''agent "non élu" ou "élu"',
    PRIMARY KEY (`id_seance_agent`, `organisme`),
    KEY `id_seance_idx` (`id_seance`),
    KEY `id_agent_idx` (`id_agent`),
    KEY `organisme_idx` (`organisme`),
    CONSTRAINT `t_CAO_Seance_Agent_id_agent_fk` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`),
    CONSTRAINT `t_CAO_Seance_Agent_id_seance_fk` FOREIGN KEY (`id_seance`) REFERENCES `t_CAO_Seance` (`id_seance`),
    CONSTRAINT `t_CAO_Seance_Agent_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_CAO_Seance_Intervenant_Externe`
--

--  `t_CAO_Seance_Intervenant_Externe`;


CREATE TABLE `t_CAO_Seance_Intervenant_Externe`
(
    `id_seance_intervenant_externe` bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l''intervenant externe de la séance',
    `id_seance`                     bigint(20)  NOT NULL COMMENT 'Identifiant de la séance',
    `id_intervenant_externe`        bigint(20)  NOT NULL COMMENT 'Identifiant l''intervenant externe',
    `organisme`                     varchar(30) NOT NULL,
    `id_ref_val_type_voix`          int(11)     NOT NULL COMMENT 'Type de voix de l''intervenant externe pour la séance',
    PRIMARY KEY (`id_seance_intervenant_externe`, `organisme`),
    KEY `id_seance_idx` (`id_seance`),
    KEY `id_intervenant_externe_idx` (`id_intervenant_externe`),
    KEY `organisme_idx` (`organisme`),
    CONSTRAINT `t_CAO_Seance_Intervenant_Externe_id_intervenant_externe_fk` FOREIGN KEY (`id_intervenant_externe`) REFERENCES `t_CAO_Intervenant_Externe` (`id_intervenant_externe`),
    CONSTRAINT `t_CAO_Seance_Intervenant_Externe_id_seance_fk` FOREIGN KEY (`id_seance`) REFERENCES `t_CAO_Seance` (`id_seance`),
    CONSTRAINT `t_CAO_Seance_Intervenant_Externe_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_CAO_Seance_Invite`
--

--  `t_CAO_Seance_Invite`;


CREATE TABLE `t_CAO_Seance_Invite`
(
    `id_seance_invite`       bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l''invité du passage en commission de la consultation',
    `organisme`              varchar(30) NOT NULL COMMENT 'Acronyme de l''organisme auquel l''intervenant est rattaché',
    `id_ordre_de_passage`    bigint(20)  NOT NULL COMMENT 'Identifiant de l''ordre de passage en commission de la consultation',
    `id_intervenant_externe` bigint(20) DEFAULT NULL COMMENT 'Identifiant de l''intervenant',
    `id_agent`               int(11)    DEFAULT NULL COMMENT 'Identifiant de l''agent',
    `id_ref_val_type_voix`   int(11)     NOT NULL COMMENT 'Valeur référentielle du type de voix de l''intervenant',
    PRIMARY KEY (`id_seance_invite`, `organisme`),
    KEY `id_intervenant_externe_idx` (`id_intervenant_externe`),
    KEY `id_agent_idx` (`id_agent`),
    KEY `organisme_idx` (`organisme`),
    KEY `id_ordre_de_passage_idx` (`id_ordre_de_passage`),
    CONSTRAINT `t_CAO_Seance_Invite_id_agent_fk` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`),
    CONSTRAINT `t_CAO_Seance_Invite_id_intervenant_externe_fk` FOREIGN KEY (`id_intervenant_externe`) REFERENCES `t_CAO_Intervenant_Externe` (`id_intervenant_externe`),
    CONSTRAINT `t_CAO_Seance_Invite_id_ordre_de_passage_fk` FOREIGN KEY (`id_ordre_de_passage`) REFERENCES `t_CAO_Ordre_De_Passage` (`id_ordre_de_passage`),
    CONSTRAINT `t_CAO_Seance_Invite_organisme_fk` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Invité d''une séance de commission pour une consultation';



--
-- Table structure for table `t_support_publication`
--

--  `t_support_publication`;


CREATE TABLE `t_support_publication`
(
    `id`                              int(11)        NOT NULL AUTO_INCREMENT,
    `image_logo`                      varchar(256)            DEFAULT NULL COMMENT 'nom de l''image du logo dans le dossier thèmes',
    `nom`                             varchar(256)   NOT NULL,
    `visible`                         enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'affichage coté acheteur',
    `ordre`                           int(11)                 DEFAULT NULL COMMENT 'permet de personnalisé l''ordre d''affichage des support',
    `default_value`                   enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'parametrer la valeau par defaut la première fois l''agent accède au formulaire de choix des préferences',
    `code`                            varchar(256)            DEFAULT NULL COMMENT 'Code donné au support pour faciliter sa recuperation. Ce code est ajouté pour des fins purement techniques',
    `actif`                           enum ('0','1') NOT NULL DEFAULT '0' COMMENT '0 : inactif, 1 : actif',
    `url`                             varchar(256)            DEFAULT NULL,
    `groupe`                          varchar(50)             DEFAULT NULL COMMENT 'Permet de regrouper des supports',
    `tag_debut_fin_groupe`            varchar(50)             DEFAULT NULL COMMENT 'Permet de determiner le debut et la fin d un groupe valeur possible 1 => debut, 2 => mellieu, 3 => fin',
    `description`                     text                    DEFAULT NULL COMMENT 'description du support',
    `nbre_total_groupe`               int(11)        NOT NULL DEFAULT 1 COMMENT 'nbre total des suppots du group du support',
    `lieux_execution`                 text                    DEFAULT NULL COMMENT 'Permet de parametrer les lieux d''execution il faut utiliser le separateur # et tjr commencer # et terminer par #',
    `detail_info`                     text                    DEFAULT NULL,
    `type_info`                       char(1)                 DEFAULT NULL COMMENT 'Designe le type d''info, 1=>texte, 2=>image, 3=>url',
    `affichage_infos`                 enum ('0','1')          DEFAULT '0' COMMENT 'permet d''afficher ou nn le detail d''info du support',
    `affichage_message_support`       enum ('0','1')          DEFAULT '0' COMMENT 'permet d''afficher ou non le champ message pour le support',
    `selection_departements_parution` enum ('0','1')          DEFAULT '0',
    `departements_parution`           varchar(256)            DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_Offre_Support_Publicite`
--

--  `t_Offre_Support_Publicite`;


CREATE TABLE `t_Offre_Support_Publicite`
(
    `id`            int(11)        NOT NULL AUTO_INCREMENT,
    `libelle_offre` varchar(256)            DEFAULT NULL,
    `id_support`    int(11)        NOT NULL,
    `actif`         enum ('0','1') NOT NULL DEFAULT '0',
    `logo`          varchar(256)            DEFAULT NULL,
    `mapa`          int(11)        NOT NULL DEFAULT 0 COMMENT '0: procedure formalise, 1: mapa, 2: mapa et procedure formalise',
    `montant_inf`   int(11)        NOT NULL DEFAULT 0,
    `montant_max`   int(11)        NOT NULL DEFAULT 0,
    `code`          varchar(256)   NOT NULL DEFAULT '',
    `ordre`         int(11)                 DEFAULT NULL COMMENT 'ordre affichage des offres',
    PRIMARY KEY (`id`),
    KEY `FK_t_support_publication` (`id_support`),
    CONSTRAINT `FK_t_support_publication` FOREIGN KEY (`id_support`) REFERENCES `t_support_publication` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_access_token`
--

--  `t_access_token`;


CREATE TABLE `t_access_token`
(
    `id`         char(36)    NOT NULL COMMENT '(DC2Type:uuid)',
    `data`       longtext    NOT NULL COMMENT '(DC2Type:json_array)',
    `ip`         varchar(15) NOT NULL,
    `user_id`    int(11) DEFAULT NULL,
    `user_type`  int(11)     NOT NULL,
    `created_at` datetime    NOT NULL,
    PRIMARY KEY (`id`),
    KEY `idenfifiant_acces_token` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_type_annonce_consultation`
--

--  `t_type_annonce_consultation`;


CREATE TABLE `t_type_annonce_consultation`
(
    `id`      int(11)        NOT NULL AUTO_INCREMENT,
    `libelle` varchar(256)   NOT NULL,
    `visible` enum ('0','1') NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_annonce_consultation`
--

--  `t_annonce_consultation`;


CREATE TABLE `t_annonce_consultation`
(
    `id`                   int(11)        NOT NULL AUTO_INCREMENT,
    `ref_consultation`     int(11)                 DEFAULT NULL,
    `organisme`            varchar(30)    NOT NULL,
    `id_dossier_sub`       int(11)        NOT NULL,
    `id_dispositif`        int(11)        NOT NULL,
    `id_compte_boamp`      int(11)        NOT NULL COMMENT 'Identifiant du compte BOAMP associé',
    `statut`               varchar(256)   NOT NULL,
    `id_type_annonce`      int(11)                 DEFAULT NULL COMMENT 'Identifiant du type d''annonce',
    `id_agent`             int(11)        NOT NULL COMMENT 'Identifiant de l''agent createur de la publicite',
    `prenom_nom_agent`     varchar(256)   NOT NULL COMMENT 'Prénom nom de l''agent createur de la publicite',
    `date_creation`        datetime       NOT NULL COMMENT 'Date de creation de la publicite',
    `date_modification`    datetime       NOT NULL COMMENT 'Date de modification de la publicite',
    `date_statut`          datetime       NOT NULL COMMENT 'Date de mise a jour du statut',
    `motif_statut`         varchar(256)            DEFAULT NULL COMMENT 'Motif justifiant le statut',
    `id_dossier_parent`    int(11)                 DEFAULT NULL COMMENT 'Identifiant du dossier parent',
    `id_dispositif_parent` int(11)                 DEFAULT NULL COMMENT 'Identifiant du dispositif parent',
    `publicite_simplifie`  enum ('0','1') NOT NULL DEFAULT '0',
    `consultation_id`      int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `ref_consultation` (`ref_consultation`),
    KEY `organisme` (`organisme`),
    KEY `id_dossier_sub` (`id_dossier_sub`),
    KEY `id_dispositif` (`id_dispositif`),
    KEY `statut` (`statut`),
    KEY `fk_tAnnonceConsultation_reference_tTypeAnnonceConsultation` (`id_type_annonce`),
    KEY `fk_tAnnonceConsultation_Consultation` (`consultation_id`),
    CONSTRAINT `fk_tAnnonceConsultation_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_tAnnonceConsultation_reference_tTypeAnnonceConsultation` FOREIGN KEY (`id_type_annonce`) REFERENCES `t_type_annonce_consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_attestations_offres`
--

--  `t_attestations_offres`;


CREATE TABLE `t_attestations_offres`
(
    `id`                     int(11) NOT NULL AUTO_INCREMENT,
    `id_offre`               int(11) NOT NULL,
    `id_document_entreprise` int(11) DEFAULT NULL,
    `id_document_version`    int(11) DEFAULT NULL,
    `id_type_document`       int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `t_attestations_offres_id_offre` (`id_offre`),
    KEY `t_attestations_offres_id_type_document` (`id_type_document`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_bourse_cotraitance`
--

--  `t_bourse_cotraitance`;


CREATE TABLE `t_bourse_cotraitance`
(
    `id_auto_BC`                      int(11)        NOT NULL AUTO_INCREMENT,
    `reference_consultation`          int(11)        NOT NULL,
    `id_entreprise`                   int(11)        NOT NULL,
    `id_etablissement_inscrite`       int(11)        NOT NULL,
    `nom_inscrit`                     varchar(100)   NOT NULL,
    `prenom_inscrit`                  varchar(100)   NOT NULL,
    `adresse_inscrit`                 varchar(100)   NOT NULL,
    `adresse2_incsrit`                varchar(100)            DEFAULT NULL,
    `cp_inscrit`                      varchar(20)    NOT NULL,
    `ville_inscrit`                   varchar(100)   NOT NULL,
    `pays_inscrit`                    varchar(250)            DEFAULT NULL,
    `fonction_inscrit`                varchar(100)   NOT NULL,
    `email_inscrit`                   varchar(100)   NOT NULL,
    `tel_fixe_inscrit`                varchar(100)            DEFAULT NULL,
    `tel_mobile_inscrit`              varchar(100)            DEFAULT NULL,
    `mandataire_groupement`           enum ('0','1') NOT NULL DEFAULT '0',
    `cotraitant_solidaire`            enum ('0','1') NOT NULL DEFAULT '0',
    `cotraitant_conjoint`             enum ('0','1') NOT NULL DEFAULT '0',
    `desc_mon_apport_marche`          text                    DEFAULT NULL,
    `desc_type_cotraitance_recherche` text                    DEFAULT NULL,
    `clause_social`                   enum ('0','1') NOT NULL DEFAULT '0',
    `entreprise_adapte`               enum ('0','1') NOT NULL DEFAULT '0',
    `long`                            double                  DEFAULT NULL COMMENT 'Longitude',
    `lat`                             double                  DEFAULT NULL COMMENT 'Latitude',
    `maj_long_lat`                    datetime                DEFAULT NULL COMMENT 'Date de mise à jour de la latitude et longitude',
    `sous_traitant`                   enum ('0','1')          DEFAULT '0',
    PRIMARY KEY (`id_auto_BC`),
    KEY `Idx_bourse_cotraitance_reference_consultation` (`reference_consultation`),
    KEY `Idx_bourse_cotraitance_id_entreprise` (`id_entreprise`),
    KEY `Idx_bourse_cotraitance_id_etablissement_inscrite` (`id_etablissement_inscrite`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_calendrier`
--

--  `t_calendrier`;


CREATE TABLE `t_calendrier`
(
    `ID_CALENDRIER`   int(11)     NOT NULL AUTO_INCREMENT,
    `REFERENCE`       int(11) DEFAULT NULL,
    `ORGANISME`       varchar(30) NOT NULL,
    `consultation_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`ID_CALENDRIER`),
    UNIQUE KEY `T_CALENDRIER_ID_CONSULTATION_UK` (`REFERENCE`, `ORGANISME`),
    KEY `T_CALENDRIER_ID_CONSULTATION_FK` (`ORGANISME`, `REFERENCE`),
    KEY `FK_T_CALENDRIER_ID_CONSULTATION_FK` (`consultation_id`),
    CONSTRAINT `FK_T_CALENDRIER_ID_CONSULTATION_FK` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_calendrier_etape`
--

--  `t_calendrier_etape`;


CREATE TABLE `t_calendrier_etape`
(
    `ID_CALENDRIER_ETAPE`   int(11)        NOT NULL AUTO_INCREMENT,
    `ID_CALENDRIER`         int(11)                 DEFAULT NULL,
    `CODE`                  varchar(3)              DEFAULT NULL,
    `LIBELLE`               varchar(100)            DEFAULT NULL,
    `DATE_INITIALE`         date                    DEFAULT NULL,
    `DATE_PREVUE`           date                    DEFAULT NULL,
    `POSITION`              int(11)                 DEFAULT NULL,
    `LIBRE`                 tinyint(1)     NOT NULL DEFAULT 0 COMMENT 'TRUE si etape libre false si etape predefinie',
    `DATE_REELLE_CONFIRMEE` enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`ID_CALENDRIER_ETAPE`),
    KEY `T_CALENDRIER_ETAPE_ID_CALENDRIER_FK` (`ID_CALENDRIER`),
    CONSTRAINT `t_calendrier_etape_ibfk_1` FOREIGN KEY (`ID_CALENDRIER`) REFERENCES `t_calendrier` (`ID_CALENDRIER`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_calendrier_etape_referentiel`
--

--  `t_calendrier_etape_referentiel`;


CREATE TABLE `t_calendrier_etape_referentiel`
(
    `ID_ETAPE_CALENDRIER_REFERENTIEL` int(11)     NOT NULL AUTO_INCREMENT,
    `ID_TYPE_PROCEDURE`               int(11)     NOT NULL DEFAULT 0,
    `ORGANISME`                       varchar(30) NOT NULL,
    `CODE`                            varchar(3)           DEFAULT NULL COMMENT 'code de l''étape',
    `LIBELLE`                         varchar(100)         DEFAULT NULL,
    `POSITION`                        int(11)              DEFAULT NULL COMMENT 'Position de l''étape dans le calendrier la première étape est numéroté à 0',
    PRIMARY KEY (`ID_ETAPE_CALENDRIER_REFERENTIEL`, `ID_TYPE_PROCEDURE`, `ORGANISME`),
    KEY `T_CALENDRIER_ETAPE_REFERENTIEL_ID_PROCEDURE` (`ID_TYPE_PROCEDURE`, `ORGANISME`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_calendrier_transition`
--

--  `t_calendrier_transition`;


CREATE TABLE `t_calendrier_transition`
(
    `ID_CALENDRIER_TRANSITION` int(11) NOT NULL AUTO_INCREMENT,
    `ID_ETAPE_SOURCE`          int(11)          DEFAULT NULL,
    `ID_ETAPE_CIBLE`           int(11)          DEFAULT NULL,
    `VALEUR_FIXE`              int(11) NOT NULL DEFAULT 0,
    `VALEUR_VARIABLE_INITIALE` int(11) NOT NULL DEFAULT 0 COMMENT 'Valeur gérée au niveau du calendrier initiale',
    `VALEUR_VARIABLE_REELLE`   int(11) NOT NULL DEFAULT 0 COMMENT 'Valeur gérée au niveau du calendrier réelle',
    PRIMARY KEY (`ID_CALENDRIER_TRANSITION`),
    KEY `T_CALENDRIER_TRANSITION_ID_ETAPE_CIBLE_FK` (`ID_ETAPE_CIBLE`),
    KEY `T_CALENDRIER_TRANSITION_ID_ETAPE_SOURCE_FK` (`ID_ETAPE_SOURCE`),
    CONSTRAINT `t_calendrier_transition_ibfk_1` FOREIGN KEY (`ID_ETAPE_SOURCE`) REFERENCES `t_calendrier_etape` (`ID_CALENDRIER_ETAPE`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `t_calendrier_transition_ibfk_2` FOREIGN KEY (`ID_ETAPE_CIBLE`) REFERENCES `t_calendrier_etape` (`ID_CALENDRIER_ETAPE`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_calendrier_transition_referentiel`
--

--  `t_calendrier_transition_referentiel`;


CREATE TABLE `t_calendrier_transition_referentiel`
(
    `ID_TRANSITION_CALENDRIER_REFERENTIEL`   int(11)     NOT NULL AUTO_INCREMENT,
    `ID_TYPE_PROCEDURE`                      int(11)     NOT NULL,
    `ORGANISME`                              varchar(30) NOT NULL,
    `ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE` int(11)              DEFAULT NULL,
    `ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE`  int(11)              DEFAULT NULL,
    `VALEUR_FIXE`                            int(11)     NOT NULL DEFAULT 0 COMMENT 'Valeur ne pouvant pas etre modifié par l''AGENT au moment de la création du calendrier associé à sa consultation',
    `VALEUR_VARIABLE`                        int(11)     NOT NULL DEFAULT 0 COMMENT 'Valeur gérée au niveau du calendrier initiale',
    PRIMARY KEY (`ID_TRANSITION_CALENDRIER_REFERENTIEL`, `ID_TYPE_PROCEDURE`, `ORGANISME`),
    KEY `T_CALENDRIER_TRANSITION_REFERENTIEL_ID_ETAPE_CIBLE_FK` (`ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE`),
    KEY `T_CALENDRIER_TRANSITION_REFERENTIEL_ID_ETAPE_SOURCE_FK` (`ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_dume_contexte`
--

--  `t_dume_contexte`;


CREATE TABLE `t_dume_contexte`
(
    `id`                  int(11)     NOT NULL AUTO_INCREMENT,
    `ref_consultation`    int(11)              DEFAULT NULL,
    `organisme`           varchar(45) NOT NULL,
    `contexte_lt_dume_id` varchar(256)         DEFAULT NULL,
    `type_dume`           varchar(45)          DEFAULT NULL,
    `status`              int(2)      NOT NULL DEFAULT 99 COMMENT '99 : brouillon, 1 : valide, 2 : en attente publication, 3 : publie, 4 : supprimer, 5 : remplacer',
    `is_standard`         tinyint(1)  NOT NULL DEFAULT 1,
    `date_creation`       timestamp   NOT NULL DEFAULT current_timestamp(),
    `date_modification`   timestamp   NULL     DEFAULT NULL,
    `consultation_id`     int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_t_dume_contexte_organisme` (`organisme`),
    KEY `FK_t_dume_contexte_consultation` (`consultation_id`),
    CONSTRAINT `FK_t_dume_contexte_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_t_dume_contexte_organisme` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `Offres`
--

--  `Offres`;


CREATE TABLE `Offres`
(
    `id`                                   int(22)            NOT NULL AUTO_INCREMENT,
    `organisme`                            varchar(30)        NOT NULL DEFAULT '',
    `consultation_ref`                     int(11)            NOT NULL DEFAULT 0,
    `entreprise_id`                        int(22)            NOT NULL DEFAULT 0,
    `id_etablissement`                     int(11)            NOT NULL,
    `inscrit_id`                           int(22)            NOT NULL DEFAULT 0,
    `signatureenvxml`                      longblob           NOT NULL,
    `horodatage`                           longblob           NOT NULL,
    `mailsignataire`                       varchar(80)        NOT NULL DEFAULT '',
    `untrusteddate`                        datetime           NOT NULL DEFAULT '0000-00-00 00:00:00',
    `untrustedserial`                      varchar(40)        NOT NULL DEFAULT '',
    `envoi_complet`                        char(1)            NOT NULL DEFAULT '',
    `date_depot_differe`                   datetime           NOT NULL DEFAULT '0000-00-00 00:00:00',
    `horodatage_envoi_differe`             longblob           NOT NULL,
    `signatureenvxml_envoi_differe`        longblob           NOT NULL,
    `external_serial`                      varchar(8)                  DEFAULT NULL,
    `internal_serial`                      varchar(8)                  DEFAULT NULL,
    `uid_offre`                            varchar(40)        NOT NULL DEFAULT '',
    `offre_selectionnee`                   int(11)            NOT NULL DEFAULT 0,
    `Observation`                          text                        DEFAULT NULL,
    `xml_string`                           longtext                    DEFAULT NULL,
    `nom_entreprise_inscrit`               varchar(30)                 DEFAULT NULL,
    `nom_inscrit`                          varchar(80)                 DEFAULT NULL,
    `prenom_inscrit`                       varchar(80)                 DEFAULT NULL,
    `adresse_inscrit`                      varchar(100)                DEFAULT NULL,
    `adresse2_inscrit`                     varchar(100)                DEFAULT NULL,
    `telephone_inscrit`                    varchar(20)                 DEFAULT NULL,
    `fax_inscrit`                          varchar(30)                 DEFAULT NULL,
    `code_postal_inscrit`                  varchar(5)                  DEFAULT NULL,
    `ville_inscrit`                        varchar(50)                 DEFAULT NULL,
    `pays_inscrit`                         varchar(50)                 DEFAULT NULL,
    `acronyme_pays`                        varchar(10)                 DEFAULT NULL,
    `siret_entreprise`                     varchar(14)                 DEFAULT NULL,
    `identifiant_national`                 varchar(20)                 DEFAULT NULL,
    `email_inscrit`                        varchar(100)                DEFAULT NULL,
    `siret_inscrit`                        varchar(14)                 DEFAULT NULL,
    `nom_entreprise`                       varchar(30)                 DEFAULT NULL,
    `horodatage_annulation`                longblob                    DEFAULT NULL,
    `date_annulation`                      varchar(20)                 DEFAULT NULL,
    `signature_annulation`                 text                        DEFAULT NULL,
    `depot_annule`                         enum ('0','1')              DEFAULT '0',
    `string_annulation`                    text                        DEFAULT NULL,
    `verification_certificat_annulation`   varchar(5)                  DEFAULT NULL,
    `offre_variante`                       enum ('0','1')              DEFAULT NULL,
    `reponse_pas_a_pas`                    enum ('0','1')     NOT NULL DEFAULT '0',
    `numero_reponse`                       int(11)            NOT NULL,
    `statut_offres`                        int(2)                      DEFAULT NULL,
    `date_heure_ouverture`                 varchar(20)        NOT NULL DEFAULT '0000-00-00 00:00:00',
    `agentid_ouverture`                    int(11)                     DEFAULT NULL,
    `agentid_ouverture2`                   int(11)                     DEFAULT NULL,
    `date_heure_ouverture_agent2`          varchar(20)        NOT NULL DEFAULT '0000-00-00 00:00:00',
    `cryptage_reponse`                     char(1)            NOT NULL DEFAULT '1',
    `nom_agent_ouverture`                  varchar(100)                DEFAULT NULL,
    `agent_telechargement_offre`           int(11)                     DEFAULT NULL,
    `date_telechargement_offre`            varchar(20)                 DEFAULT NULL,
    `repertoire_telechargement_offre`      varchar(100)                DEFAULT NULL,
    `candidature_id_externe`               int(22)                     DEFAULT NULL COMMENT 'permet de stocker l''identifiant de candidature en provenance du site du SGMAP quand le module PF_MPS_EXTERNE est actif',
    `etat_chiffrement`                     int(1)             NOT NULL DEFAULT 0 COMMENT 'Permet de stocker l''etat du chiffrement de l''offre. Valeurs possibles : 0 => pour les offres deja existantes avant cette evolution, 1 => etat_chiffrement_ok, 2 => etat_chiffrement_ko_error_rest, 3 => etat_chiffrement_ko_error_horodatage, 4 => etat_chiffrement_ko_error_json ',
    `erreur_chiffrement`                   longtext                    DEFAULT NULL COMMENT 'Permet de stocker le message d''erreur en cas de chiffrement KO',
    `date_fin_chiffrement`                 datetime                    DEFAULT NULL,
    `date_horodatage`                      datetime                    DEFAULT NULL COMMENT 'date extraite depuis l''horodatage (ne pas utiliser)',
    `verification_hotodatage`              enum ('0','1','2') NOT NULL DEFAULT '0' COMMENT '(0 : pas encore vérifier, 1 : vérification OK, 2 : vérification KO)',
    `id_pdf_echange_accuse`                int(11)                     DEFAULT NULL,
    `verification_signature_offre`         varchar(50)        NOT NULL DEFAULT '',
    `horodatage_hash_fichiers`             text                        DEFAULT NULL COMMENT 'Permet de stocker le base64 xml de l ensemble des hash de l offre',
    `created_at`                           datetime                    DEFAULT NULL,
    `uid_response`                         text                        DEFAULT NULL COMMENT 'Unique ID du depot de l''offre',
    `date_depot`                           datetime                    DEFAULT NULL COMMENT ' c''est la date de la validation du dï¿½pï¿½t',
    `date_demande_ouverture`               datetime                    DEFAULT NULL COMMENT 'Date de reception du statut en cours de dechiffrement/ouverture',
    `resultat_verification_hash_all_files` char(1)                     DEFAULT '1' COMMENT 'Contient le resultat de la vÃ©rification des hash des fichiers, 0=>KO(au moins un fichier non valide),1=>OK',
    `id_blob_horodatage_hash`              int(11)                     DEFAULT NULL COMMENT 'id blob fichier hash256 des fichiers de la reponse',
    `id_blob_xml_reponse`                  int(11)                     DEFAULT NULL,
    `consultation_id`                      int(11)                     DEFAULT NULL,
    `plateforme_virtuelle_id`              int(11) unsigned            DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    UNIQUE KEY `untrustedserial` (`untrustedserial`),
    KEY `inscrit_id` (`inscrit_id`),
    KEY `consultation_ref` (`consultation_ref`),
    KEY `entreprise_id` (`entreprise_id`),
    KEY `Offres_consultation` (`organisme`, `consultation_ref`),
    KEY `candidature_id_externe_idx` (`candidature_id_externe`),
    KEY `offres_id_blob_xml_reponse` (`id_blob_xml_reponse`),
    KEY `offres_statut_offres` (`statut_offres`),
    KEY `FK_Offres_consultation` (`consultation_id`),
    KEY `fk_plateforme_virtuelle_offres` (`plateforme_virtuelle_id`),
    CONSTRAINT `FK_Offres_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_plateforme_virtuelle_offres` FOREIGN KEY (`plateforme_virtuelle_id`) REFERENCES `plateforme_virtuelle` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_candidature`
--

--  `t_candidature`;


CREATE TABLE `t_candidature`
(
    `id`                            int(11)     NOT NULL AUTO_INCREMENT,
    `ref_consultation`              int(11)              DEFAULT NULL,
    `organisme`                     varchar(45) NOT NULL,
    `id_inscrit`                    int(11)     NOT NULL,
    `id_entreprise`                 int(11)     NOT NULL,
    `id_etablissement`              int(11)     NOT NULL,
    `status`                        int(2)      NOT NULL DEFAULT 99,
    `type_candidature`              varchar(45)          DEFAULT NULL,
    `type_candidature_dume`         varchar(45)          DEFAULT NULL,
    `id_dume_contexte`              int(11)              DEFAULT NULL,
    `id_offre`                      int(11)              DEFAULT NULL,
    `role_inscrit`                  int(11)     NOT NULL DEFAULT 1 COMMENT '1 = mandataire / 2 = co-traitant',
    `date_derniere_validation_dume` datetime             DEFAULT NULL,
    `consultation_id`               int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_t_candidature_organisme` (`organisme`),
    KEY `FK_t_candidature_dume_contexte` (`id_dume_contexte`),
    KEY `FK_t_candidature_offres` (`id_offre`),
    KEY `FK_t_candidature_consultation` (`consultation_id`),
    CONSTRAINT `FK_t_candidature_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_t_candidature_dume_contexte` FOREIGN KEY (`id_dume_contexte`) REFERENCES `t_dume_contexte` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_t_candidature_offres` FOREIGN KEY (`id_offre`) REFERENCES `Offres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_t_candidature_organisme` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_candidature_mps`
--

--  `t_candidature_mps`;


CREATE TABLE `t_candidature_mps`
(
    `id_candidature`   int(11)     NOT NULL AUTO_INCREMENT,
    `id_entreprise`    int(11)     NOT NULL,
    `id_inscrit`       int(11)     NOT NULL,
    `ref_consultation` int(11)              DEFAULT NULL,
    `organisme`        varchar(30) NOT NULL,
    `id_offre`         int(11)              DEFAULT NULL,
    `id_blob`          int(11)     NOT NULL,
    `horodatage`       longblob    NOT NULL,
    `untrusted_date`   datetime    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `untrusted_serial` varchar(40) NOT NULL,
    `taille_fichier`   int(11)     NOT NULL,
    `liste_lots`       text                 DEFAULT NULL,
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id_candidature`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_chorus_ccag`
--

--  `t_chorus_ccag`;


CREATE TABLE `t_chorus_ccag`
(
    `id_ccag`               int(11)      NOT NULL AUTO_INCREMENT,
    `reference_ccag_chorus` varchar(3)   NOT NULL,
    `libelle_ccag`          varchar(250) NOT NULL,
    `reference_ccag_orme`   int(11)      NOT NULL,
    PRIMARY KEY (`id_ccag`),
    UNIQUE KEY `reference_ccag_chorus` (`reference_ccag_chorus`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_chorus_fiche_modificative`
--

--  `t_chorus_fiche_modificative`;


CREATE TABLE `t_chorus_fiche_modificative`
(
    `id_fiche_modificative`         int(11)            NOT NULL AUTO_INCREMENT,
    `id_echange`                    int(11)            NOT NULL,
    `organisme`                     varchar(30)        NOT NULL DEFAULT '',
    `type_modification`             int(11)                     DEFAULT NULL,
    `date_prevue_notification`      date                        DEFAULT NULL,
    `date_fin_marche`               date                        DEFAULT NULL,
    `date_fin_marche_modifie`       date                        DEFAULT NULL,
    `montant_marche`                double                      DEFAULT NULL,
    `montant_acte`                  double                      DEFAULT NULL,
    `taux_tva`                      varchar(10)                 DEFAULT NULL,
    `nombre_fournisseur_cotraitant` varchar(250)                DEFAULT NULL,
    `localites_fournisseurs`        varchar(250)                DEFAULT NULL,
    `siren_fournisseur`             text                        DEFAULT NULL,
    `siret_fournisseur`             text                        DEFAULT NULL,
    `nom_fournisseur`               text                        DEFAULT NULL,
    `type_fournisseur`              text                        DEFAULT NULL,
    `visa_accf`                     enum ('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Non renseigné,1=>oui,2=>non',
    `visa_prefet`                   enum ('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=>Non renseigné,1=>oui,2=>non',
    `remarque`                      text                        DEFAULT NULL,
    `id_blob_piece_justificatives`  text                        DEFAULT NULL,
    `id_blob_fiche_modificative`    text                        DEFAULT NULL,
    `date_creation`                 datetime                    DEFAULT NULL,
    `date_modification`             datetime                    DEFAULT NULL,
    PRIMARY KEY (`id_fiche_modificative`),
    KEY `chorus_echange_chorus_fiche_modificative_fk` (`id_echange`, `organisme`),
    CONSTRAINT `chorus_echange_chorus_fiche_modificative_fk` FOREIGN KEY (`id_echange`, `organisme`) REFERENCES `Chorus_echange` (`id`, `organisme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_chorus_fiche_modificative_pj`
--

--  `t_chorus_fiche_modificative_pj`;


CREATE TABLE `t_chorus_fiche_modificative_pj`
(
    `id_fiche_modificative_pj` int(20)      NOT NULL AUTO_INCREMENT,
    `id_fiche_modificative`    int(11)      NOT NULL DEFAULT 0,
    `nom_fichier`              varchar(300) NOT NULL DEFAULT '',
    `fichier`                  varchar(20)  NOT NULL DEFAULT '',
    `horodatage`               longblob     NOT NULL,
    `untrusteddate`            varchar(20)  NOT NULL DEFAULT '',
    `taille`                   int(20)      NOT NULL DEFAULT 0,
    PRIMARY KEY (`id_fiche_modificative_pj`),
    KEY `chorus_fiche_modificative_pj_chorus_fiche_modificative_fk` (`id_fiche_modificative`),
    CONSTRAINT `chorus_fiche_modificative_pj_chorus_fiche_modificative_fk` FOREIGN KEY (`id_fiche_modificative`) REFERENCES `t_chorus_fiche_modificative` (`id_fiche_modificative`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_chorus_tier_rejete`
--

--  `t_chorus_tier_rejete`;


CREATE TABLE `t_chorus_tier_rejete`
(
    `id`                 int(20)        NOT NULL AUTO_INCREMENT,
    `id_echange`         int(20)        NOT NULL,
    `id_tier`            varchar(14)    NOT NULL,
    `code_rejet`         varchar(256)            DEFAULT NULL,
    `organisation_achat` varchar(256)            DEFAULT NULL,
    `active`             enum ('0','1') NOT NULL DEFAULT '0',
    `date_creation`      varchar(20)             DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `index_id_echange` (`id_echange`),
    KEY `index_id_tier` (`id_tier`),
    KEY `index_code_rejet` (`code_rejet`),
    KEY `index_rejet_active` (`active`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_reponse_elec_formulaire`
--

--  `t_reponse_elec_formulaire`;


CREATE TABLE `t_reponse_elec_formulaire`
(
    `id_reponse_elec_formulaire` int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`                  varchar(30) NOT NULL,
    `consultation_ref`           int(11) DEFAULT NULL,
    `id_entreprise`              int(11)     NOT NULL,
    `id_inscrit`                 int(11)     NOT NULL,
    `statut_validation_globale`  int(1)      NOT NULL,
    `consultation_id`            int(11) DEFAULT NULL,
    PRIMARY KEY (`id_reponse_elec_formulaire`),
    KEY `consultation_t_reponse_elec_form_consultation_ref_fk` (`organisme`, `consultation_ref`),
    KEY `Entreprise_t_reponse_elec_form_id_entreprise_fk` (`id_entreprise`),
    KEY `Inscrit_t_reponse_elec_form_id_inscrit_fk` (`id_inscrit`),
    KEY `FK_consultation_t_reponse_elec_form_consultation_ref_fk` (`consultation_id`),
    CONSTRAINT `Entreprise_t_reponse_elec_form_id_entreprise_fk` FOREIGN KEY (`id_entreprise`) REFERENCES `Entreprise` (`id`),
    CONSTRAINT `FK_consultation_t_reponse_elec_form_consultation_ref_fk` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`),
    CONSTRAINT `Inscrit_t_reponse_elec_form_id_inscrit_fk` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_dossier_formulaire`
--

--  `t_dossier_formulaire`;


CREATE TABLE `t_dossier_formulaire`
(
    `id_dossier_formulaire`      int(11)            NOT NULL AUTO_INCREMENT,
    `id_reponse_elec_formulaire` int(11)            NOT NULL,
    `id_lot`                     int(11)                     DEFAULT NULL,
    `type_enveloppe`             int(1)             NOT NULL,
    `libelle_forrmulaire`        varchar(256)       NOT NULL,
    `cle_externe_dispositif`     int(11)            NOT NULL,
    `cle_externe_dossier`        int(11)                     DEFAULT NULL,
    `statut_validation`          enum ('1','2','3') NOT NULL,
    `date_creation`              varchar(256)                DEFAULT NULL,
    `date_modif`                 varchar(256)                DEFAULT NULL,
    `date_validation`            varchar(256)                DEFAULT NULL,
    `statut_generation_globale`  int(1)                      DEFAULT NULL,
    `type_reponse`               int(11)                     DEFAULT 1 COMMENT 'Permet de spécifier si le dossier est initial ou bien correspond à une demande de complément. Les valeurs correspondantes sont dans application.xml',
    `cle_externe_formulaire`     int(11)                     DEFAULT NULL COMMENT 'Permet de stocker l''identifiant du formulaire SUB (id_formulaire) recupérée après la validation',
    `formulaire_depose`          enum ('0','1')     NOT NULL DEFAULT '0' COMMENT 'Ce champ est flagué à 1 lorsqu''une reponsée est deposée sur le dossier',
    `id_dossier_pere`            int(11)                     DEFAULT NULL,
    `reference_dossier_sub`      varchar(256)                DEFAULT NULL COMMENT 'Permet de recuperer le numéro (référence) du dossier SUB',
    PRIMARY KEY (`id_dossier_formulaire`),
    KEY `t_reponse_elec_form_t_dossier_form_id_reponse_elec_form_fk` (`id_reponse_elec_formulaire`),
    CONSTRAINT `t_reponse_elec_form_t_dossier_form_id_reponse_elec_form_fk` FOREIGN KEY (`id_reponse_elec_formulaire`) REFERENCES `t_reponse_elec_formulaire` (`id_reponse_elec_formulaire`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_complement_formulaire`
--

--  `t_complement_formulaire`;


CREATE TABLE `t_complement_formulaire`
(
    `id_complement_formulaire` int(11)        NOT NULL AUTO_INCREMENT,
    `id_dossier_formulaire`    int(11)        NOT NULL,
    `numero_complement`        int(11)                 DEFAULT NULL,
    `date_a_remettre`          varchar(50)             DEFAULT NULL,
    `date_remis_le`            varchar(50)             DEFAULT NULL,
    `motif`                    varchar(256)            DEFAULT NULL,
    `commentaire`              text                    DEFAULT NULL,
    `statut_demande`           enum ('1','2') NOT NULL DEFAULT '1' COMMENT '1 : en cours ou 2 : envoyé',
    `date_envoi_1er_mail`      varchar(256)            DEFAULT NULL COMMENT 'Date d''envoi du 1er message envoyé pour la demande de reouverture',
    `date_1er_ar`              varchar(256)            DEFAULT NULL COMMENT 'Date du 1er accusé de reception',
    PRIMARY KEY (`id_complement_formulaire`),
    KEY `id_dossier_formulaire` (`id_dossier_formulaire`),
    CONSTRAINT `t_complement_formulaire_ibfk_id_dossier_formulaire` FOREIGN KEY (`id_dossier_formulaire`) REFERENCES `t_dossier_formulaire` (`id_dossier_formulaire`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_cons_lot_contrat`
--

--  `t_cons_lot_contrat`;


CREATE TABLE `t_cons_lot_contrat`
(
    `id_cons_lot_contrat`  int(11) NOT NULL AUTO_INCREMENT,
    `organisme`            varchar(45) DEFAULT NULL,
    `consultation_ref`     int(11)     DEFAULT NULL,
    `lot`                  int(11)     DEFAULT NULL,
    `id_contrat_titulaire` int(11) NOT NULL,
    `consultation_id`      int(11)     DEFAULT NULL,
    PRIMARY KEY (`id_cons_lot_contrat`),
    KEY `fk_Lot_Contrat_Contrat_Titulaire_idx` (`id_contrat_titulaire`),
    KEY `fk_t_cons_lot_contrat_lot1_idx` (`lot`),
    CONSTRAINT `fk_Lot_Contrat_Contrat_Titulaire` FOREIGN KEY (`id_contrat_titulaire`) REFERENCES `t_contrat_titulaire` (`id_contrat_titulaire`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `t_donnee_complementaire`
--

--  `t_donnee_complementaire`;


CREATE TABLE `t_donnee_complementaire`
(
    `id_donnee_complementaire`               int(11)        NOT NULL AUTO_INCREMENT,
    `id_tranche_type_prix`                   int(11)                 DEFAULT NULL,
    `id_forme_prix`                          int(11)                 DEFAULT NULL,
    `id_ccag_reference`                      int(11)                 DEFAULT NULL,
    `reconductible`                          tinyint(1)              DEFAULT NULL,
    `nombre_reconductions`                   int(11)                 DEFAULT NULL,
    `modalites_reconduction`                 varchar(1000)           DEFAULT NULL,
    `variantes_autorisees`                   tinyint(1)              DEFAULT NULL,
    `variante_exigee`                        tinyint(1)              DEFAULT NULL,
    `variantes_techniques_obligatoires`      tinyint(1)              DEFAULT NULL,
    `variantes_techniques_description`       varchar(100)            DEFAULT NULL,
    `decomposition_lots_techniques`          tinyint(1)              DEFAULT NULL,
    `id_duree_delai_description`             int(11)                 DEFAULT NULL,
    `estimation_pf_att_pressenti`            decimal(30, 2)          DEFAULT NULL,
    `estimation_bc_min_att_pressenti`        decimal(30, 2)          DEFAULT NULL,
    `estimation_bc_max_att_pressenti`        decimal(30, 2)          DEFAULT NULL,
    `estimation_pf_tab_ouv_offre`            decimal(30, 2)          DEFAULT NULL,
    `estimation_dqe_tab_ouv_offre`           decimal(30, 2)          DEFAULT NULL,
    `avis_attribution_present`               tinyint(1)              DEFAULT 0,
    `estimation_pf_preinscription`           decimal(30, 2)          DEFAULT NULL,
    `estimation_bc_min_preinscription`       decimal(30, 2)          DEFAULT NULL,
    `estimation_bc_max_preinscription`       decimal(30, 2)          DEFAULT NULL,
    `estimation_date_valeur_preinscription`  date                    DEFAULT NULL,
    `lieu_execution`                         varchar(40)             DEFAULT NULL,
    `duree_marche`                           int(11)                 DEFAULT NULL,
    `duree_date_debut`                       date                    DEFAULT NULL,
    `duree_date_fin`                         date                    DEFAULT NULL,
    `duree_description`                      varchar(200)            DEFAULT NULL,
    `id_choix_mois_jour`                     int(11)                 DEFAULT NULL,
    `id_unite`                               int(11)                 DEFAULT NULL,
    `id_nb_candidats_admis`                  int(11)                 DEFAULT NULL,
    `nombre_candidats_fixe`                  int(11)                 DEFAULT NULL,
    `nombre_candidats_min`                   int(11)                 DEFAULT NULL,
    `nombre_candidats_max`                   int(11)                 DEFAULT NULL,
    `delai_validite_offres`                  int(11)                 DEFAULT NULL,
    `phase_successive`                       tinyint(1)              DEFAULT NULL,
    `id_groupement_attributaire`             int(11)                 DEFAULT NULL,
    `id_critere_attribution`                 int(11)                 DEFAULT NULL,
    `type_prestation`                        int(11)        NOT NULL DEFAULT 1,
    `delai_partiel`                          enum ('0','1') NOT NULL DEFAULT '0',
    `adresse_retrais_dossiers`               longtext                DEFAULT NULL,
    `adresse_depot_offres`                   longtext                DEFAULT NULL,
    `lieu_ouverture_plis`                    longtext                DEFAULT NULL,
    `pieces_dossier_admin`                   text                    DEFAULT NULL,
    `id_tr_pieces_dossier_admin`             int(11)                 DEFAULT NULL,
    `pieces_dossier_tech`                    text                    DEFAULT NULL,
    `id_tr_pieces_dossier_tech`              int(11)                 DEFAULT NULL,
    `pieces_dossier_additif`                 text                    DEFAULT NULL,
    `id_tr_pieces_dossier_additif`           int(11)                 DEFAULT NULL,
    `id_tr_adresse_retrais_dossiers`         int(11)                 DEFAULT NULL,
    `id_tr_adresse_depot_offres`             int(11)                 DEFAULT NULL,
    `id_tr_lieu_ouverture_plis`              int(11)                 DEFAULT NULL,
    `caution_provisoire`                     varchar(256)            DEFAULT NULL,
    `prix_aquisition_plans`                  varchar(256)            DEFAULT NULL,
    `add_echantillon`                        varchar(256)            DEFAULT NULL,
    `id_tr_add_echantillon`                  int(11)                 DEFAULT NULL COMMENT 'Identifiant de la traduction de l''échantillon',
    `date_limite_echantillon`                varchar(50)             DEFAULT NULL,
    `add_reunion`                            varchar(256)            DEFAULT NULL,
    `id_tr_add_reunion`                      int(11)                 DEFAULT NULL COMMENT 'Identifiant de la traduction de la réunion',
    `date_reunion`                           varchar(50)             DEFAULT NULL,
    `reunion`                                enum ('0','1') NOT NULL DEFAULT '0',
    `visites_lieux`                          enum ('0','1') NOT NULL DEFAULT '0',
    `echantillon`                            enum ('0','1') NOT NULL DEFAULT '0',
    `variantes`                              char(1)                 DEFAULT NULL,
    `variante_calcule`                       enum ('0','1') NOT NULL DEFAULT '0',
    `criteres_identiques`                    enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'permet de savoir si les critères sont identiques pour tous les lots',
    `justification_non_alloti`               varchar(256)            DEFAULT NULL,
    `id_ccag_dpi`                            int(11)                 DEFAULT NULL,
    `montant_marche`                         double                  DEFAULT NULL,
    `mots_cles`                              varchar(256)            DEFAULT NULL,
    `procedure_accord_marches_publics_omc`   tinyint(1)              DEFAULT NULL,
    `cautionnement_regime_financier`         longtext                DEFAULT NULL,
    `modalites_financement_regime_financier` longtext                DEFAULT NULL,
    `publication_montant_estimation`         enum ('0','1') NOT NULL DEFAULT '0' COMMENT 'publication du montant estime 0 => non,  1 => oui',
    `valeur_montant_estimation_publiee`      double                  DEFAULT NULL,
    `projet_finance_fonds_union_europeenne`  enum ('0','1') NOT NULL,
    `identification_projet`                  text           NOT NULL,
    `nature_marche`                          int(11)                 DEFAULT NULL,
    `article_29`                             enum ('0','1') NOT NULL DEFAULT '0',
    `article_30`                             enum ('0','1') NOT NULL DEFAULT '0',
    `article_147`                            enum ('0','1') NOT NULL DEFAULT '0',
    `article_148`                            enum ('0','1') NOT NULL DEFAULT '0',
    `produits_associes`                      varchar(256)            DEFAULT NULL,
    PRIMARY KEY (`id_donnee_complementaire`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_consultation_compte_pub`
--

--  `t_consultation_compte_pub`;


CREATE TABLE `t_consultation_compte_pub`
(
    `id`                         int(22)      NOT NULL AUTO_INCREMENT,
    `id_donnees_complementaires` int(22)      NOT NULL,
    `organisme`                  varchar(30)  NOT NULL,
    `id_compte_pub`              int(22)      NOT NULL,
    `boamp_login`                varchar(100) NOT NULL DEFAULT '',
    `boamp_password`             varchar(100) NOT NULL DEFAULT '',
    `boamp_mail`                 varchar(100) NOT NULL DEFAULT '',
    `boamp_target`               char(1)      NOT NULL DEFAULT '0',
    `denomination`               varchar(100) NOT NULL DEFAULT '',
    `prm`                        varchar(100) NOT NULL DEFAULT '',
    `adresse`                    varchar(100) NOT NULL DEFAULT '',
    `cp`                         varchar(5)   NOT NULL DEFAULT '',
    `ville`                      varchar(100) NOT NULL DEFAULT '',
    `url`                        varchar(100) NOT NULL DEFAULT '',
    `facture_denomination`       varchar(100)          DEFAULT NULL,
    `facture_adresse`            varchar(256) NOT NULL DEFAULT '',
    `facture_cp`                 varchar(10)  NOT NULL DEFAULT '',
    `facture_ville`              varchar(100) NOT NULL DEFAULT '',
    `instance_recours_organisme` varchar(200)          DEFAULT NULL,
    `instance_recours_adresse`   varchar(200)          DEFAULT NULL,
    `instance_recours_cp`        varchar(200)          DEFAULT NULL,
    `instance_recours_ville`     varchar(200)          DEFAULT NULL,
    `instance_recours_url`       text                  DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `id_donnees_complementaires` (`id_donnees_complementaires`),
    KEY `id_compte_pub` (`id_compte_pub`),
    CONSTRAINT `t_consultation_compte_pub_ref_idDonneesComplementaires` FOREIGN KEY (`id_donnees_complementaires`) REFERENCES `t_donnee_complementaire` (`id_donnee_complementaire`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_contact_contrat`
--

--  `t_contact_contrat`;


CREATE TABLE `t_contact_contrat`
(
    `id_contact_contrat` int(11) NOT NULL AUTO_INCREMENT,
    `id_entreprise`      int(11)      DEFAULT NULL,
    `id_etablissement`   int(11)      DEFAULT NULL,
    `id_inscrit`         int(11)      DEFAULT NULL,
    `nom`                varchar(80)  DEFAULT NULL,
    `prenom`             varchar(80)  DEFAULT NULL,
    `email`              varchar(100) DEFAULT NULL,
    `telephone`          varchar(20)  DEFAULT NULL,
    `fax`                varchar(30)  DEFAULT NULL,
    `created_at`         datetime     DEFAULT NULL,
    `updated_at`         datetime     DEFAULT NULL,
    PRIMARY KEY (`id_contact_contrat`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;




--
-- Table structure for table `t_contrat_titulaire_favori`
--

--  `t_contrat_titulaire_favori`;


CREATE TABLE `t_contrat_titulaire_favori`
(
    `id_contrat_titulaire_favori` int(11)     NOT NULL AUTO_INCREMENT,
    `id_contrat_titulaire`        int(11)     NOT NULL,
    `organisme`                   varchar(45) NOT NULL,
    `id_agent`                    int(11)     NOT NULL,
    PRIMARY KEY (`id_contrat_titulaire_favori`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_critere_attribution`
--

--  `t_critere_attribution`;


CREATE TABLE `t_critere_attribution`
(
    `id_critere_attribution`   int(11) NOT NULL AUTO_INCREMENT,
    `enonce`                   text             DEFAULT NULL,
    `ordre`                    int(11) NOT NULL DEFAULT 0,
    `ponderation`              decimal(5, 2)    DEFAULT 0.00,
    `id_donnee_complementaire` int(11)          DEFAULT NULL,
    PRIMARY KEY (`id_critere_attribution`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_decision_selection_entreprise`
--

--  `t_decision_selection_entreprise`;


CREATE TABLE `t_decision_selection_entreprise`
(
    `id`                 int(11) NOT NULL AUTO_INCREMENT,
    `consultation_ref`   int(11)     DEFAULT NULL,
    `organisme`          varchar(45) DEFAULT NULL,
    `service_id`         int(11)     DEFAULT NULL,
    `lot`                int(11)     DEFAULT NULL,
    `id_entreprise`      int(11)     DEFAULT NULL,
    `id_etablissement`   int(11)     DEFAULT NULL,
    `id_offre`           int(11)     DEFAULT NULL,
    `type_depot_reponse` char(1)     DEFAULT NULL,
    `date_creation`      datetime    DEFAULT NULL,
    `date_modification`  datetime    DEFAULT NULL,
    `id_contact_contrat` int(11)     DEFAULT NULL,
    `consultation_id`    int(11)     DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_Decision_Selection_Entreprise_Contact_Contrat` (`id_contact_contrat`),
    CONSTRAINT `fk_Decision_Selection_Entreprise_Contact_Contrat` FOREIGN KEY (`id_contact_contrat`) REFERENCES `t_contact_contrat` (`id_contact_contrat`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_derniers_appels_valides_ws_sgmap_documents`
--

--  `t_derniers_appels_valides_ws_sgmap_documents`;


CREATE TABLE `t_derniers_appels_valides_ws_sgmap_documents`
(
    `id`                        int(11)    NOT NULL AUTO_INCREMENT,
    `identifiant`               bigint(20) NOT NULL COMMENT 'Siren (9 caractères) pour l''entreprise ou siret (14 caractères) pour l''etablissement',
    `id_type_document`          int(11)    NOT NULL,
    `id_entreprise`             int(11)    NOT NULL,
    `id_agent`                  int(11)    NOT NULL,
    `date_dernier_appel_valide` datetime   NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_dispositif_annonce`
--

--  `t_dispositif_annonce`;


CREATE TABLE `t_dispositif_annonce`
(
    `id`                         int(11)        NOT NULL AUTO_INCREMENT,
    `libelle`                    varchar(256)   NOT NULL,
    `id_externe`                 int(11)        NOT NULL,
    `type`                       int(11)                 DEFAULT NULL COMMENT 'Le type de dispositif ',
    `visible`                    enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Gère la visibilité du dispositif (0=> non visible, 1=> visible)',
    `id_dispositif_rectificatif` int(11)                 DEFAULT NULL COMMENT 'Identifiant du dispositif SUB rectificatif pour ce dispositif',
    `id_dispositif_annulation`   int(11)                 DEFAULT NULL COMMENT 'Identifiant du dispositif SUB d''annulation pour ce dispositif',
    `sigle`                      varchar(256)            DEFAULT NULL COMMENT 'Sigle du dispositif',
    `id_type_avis_rectificatif`  int(11)                 DEFAULT NULL COMMENT 'Identifiant du type d''avis rectificatif associé',
    `id_type_avis_annulation`    int(11)                 DEFAULT NULL COMMENT 'Identifiant du type d''avis d''annulation associé',
    `libelle_formulaire_propose` varchar(256)   NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_document`
--

--  `t_document`;


CREATE TABLE `t_document`
(
    `id_document`      int(11) NOT NULL AUTO_INCREMENT,
    `nom`              varchar(256) DEFAULT NULL,
    `taille`           varchar(50)  DEFAULT NULL,
    `organisme`        varchar(45)  DEFAULT NULL,
    `consultation_ref` int(11)      DEFAULT NULL,
    `lot`              int(11)      DEFAULT 0,
    `empreinte`        text         DEFAULT NULL,
    `type_empreinte`   varchar(256) DEFAULT NULL,
    `id_type_document` int(11)      DEFAULT NULL,
    `date_creation`    datetime     DEFAULT NULL,
    `id_fichier`       int(11)      DEFAULT NULL,
    `consultation_id`  int(11)      DEFAULT NULL,
    PRIMARY KEY (`id_document`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_document_type`
--

--  `t_document_type`;


CREATE TABLE `t_document_type`
(
    `id_type_document`                  int(11)        NOT NULL AUTO_INCREMENT,
    `nom_type_document`                 varchar(100)   NOT NULL,
    `code`                              varchar(50)             DEFAULT NULL COMMENT 'Permet de faire le lien entre le type de document et le retour du ws agrégateur',
    `type_doc_entreprise_etablissement` enum ('1','2')          DEFAULT NULL COMMENT 'Précise si le document est de type entreprise ou etablissement (1: entreprise, 2: etablissement)',
    `uri`                               varchar(100)            DEFAULT NULL COMMENT 'URI de la requete ws',
    `params_uri`                        varchar(100)   NOT NULL COMMENT 'Les parametres doivent etre separes par des virgules',
    `class_name`                        varchar(100)   NOT NULL COMMENT 'Le nom de la classe a utiliser lors de l''appel du web service',
    `nature_document`                   enum ('1','2') NOT NULL DEFAULT '1' COMMENT 'Precise si le type de document est principal ou supplementaire (1=>principal, 2=>supplementaire)',
    `type_retour_ws`                    char(1)                 DEFAULT '1' COMMENT 'Permet de precisier la nature de retour de ws, 1 : fichier , 2: metadonnee',
    `synchro_actif`                     enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet d''activer la synchronisation l''attestation ou la desactiver',
    `message_desactivation_synchro`     varchar(256)   NOT NULL DEFAULT '' COMMENT 'Message lié à la désactivation de la synchro',
    `afficher_type_doc`                 enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'Permet d''afficher le type doc au niveau de la liste des documents',
    PRIMARY KEY (`id_type_document`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_document_entreprise`
--

--  `t_document_entreprise`;


CREATE TABLE `t_document_entreprise`
(
    `id_document`         int(11)      NOT NULL AUTO_INCREMENT,
    `id_entreprise`       int(11)      NOT NULL,
    `id_etablissement`    int(11)      NOT NULL,
    `nom_document`        varchar(100) NOT NULL,
    `id_type_document`    int(11)      NOT NULL,
    `id_derniere_version` int(11) DEFAULT NULL,
    PRIMARY KEY (`id_document`),
    KEY `entreprise_document` (`id_entreprise`),
    KEY `entreprise_document_type` (`id_type_document`),
    CONSTRAINT `entreprise_document` FOREIGN KEY (`id_entreprise`) REFERENCES `Entreprise` (`id`),
    CONSTRAINT `entreprise_document_type` FOREIGN KEY (`id_type_document`) REFERENCES `t_document_type` (`id_type_document`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_donnees_consultation`
--

--  `t_donnees_consultation`;


CREATE TABLE `t_donnees_consultation`
(
    `id_donnees_consultation`     int(11) NOT NULL AUTO_INCREMENT,
    `reference_consultation`      int(11)      DEFAULT NULL,
    `id_contrat_titulaire`        int(11)      DEFAULT NULL,
    `organisme`                   varchar(45)  DEFAULT NULL,
    `service_id`                  int(11)      DEFAULT NULL,
    `id_type_procedure`           int(11)      DEFAULT NULL,
    `libelle_type_procedure`      varchar(100) DEFAULT NULL,
    `nbre_offres_recues`          int(11)      DEFAULT NULL,
    `nbre_offres_dematerialisees` int(11)      DEFAULT NULL,
    `signature_offre`             char(1)      DEFAULT NULL,
    PRIMARY KEY (`id_donnees_consultation`),
    KEY `fk_Donnees_Consultation_Contrat_Titulaire` (`id_contrat_titulaire`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_dume_numero`
--

--  `t_dume_numero`;


CREATE TABLE `t_dume_numero`
(
    `id`                    int(11) NOT NULL AUTO_INCREMENT,
    `numero_dume_national`  varchar(100) DEFAULT NULL,
    `blob_id`               int(11)      DEFAULT NULL,
    `list_lot`              varchar(256) DEFAULT NULL,
    `id_dume_contexte`      int(11)      DEFAULT NULL,
    `date_recuperation_pdf` datetime     DEFAULT NULL,
    `blob_id_xml`           int(11)      DEFAULT NULL,
    `date_recuperation_xml` datetime     DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_t_dume_contexte_numero_dume_contexte` (`id_dume_contexte`),
    CONSTRAINT `FK_t_dume_contexte_numero_dume_contexte` FOREIGN KEY (`id_dume_contexte`) REFERENCES `t_dume_contexte` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1178
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_edition_formulaire`
--

--  `t_edition_formulaire`;


CREATE TABLE `t_edition_formulaire`
(
    `id_edition_formulaire` int(11)      NOT NULL AUTO_INCREMENT,
    `id_dossier_formulaire` int(11)      NOT NULL,
    `cle_externe_edition`   int(11)      NOT NULL,
    `libelle`               varchar(256) NOT NULL,
    `extension`             varchar(3)   NOT NULL,
    `path`                  varchar(256) DEFAULT NULL,
    `hash`                  varchar(40)  DEFAULT NULL,
    `signature`             text         DEFAULT NULL,
    `date_generation`       varchar(256) DEFAULT NULL,
    `date_depot`            varchar(256) DEFAULT NULL,
    `statut_generation`     int(1)       NOT NULL,
    `type`                  int(11)      DEFAULT NULL,
    `nom_fichier`           varchar(256) NOT NULL,
    PRIMARY KEY (`id_edition_formulaire`),
    KEY `t_dossier_form_t_edition_form_id_dossier_form_fk` (`id_dossier_formulaire`),
    CONSTRAINT `t_dossier_form_t_edition_form_id_dossier_form_fk` FOREIGN KEY (`id_dossier_formulaire`) REFERENCES `t_dossier_formulaire` (`id_dossier_formulaire`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_entreprise_document_version`
--

--  `t_entreprise_document_version`;


CREATE TABLE `t_entreprise_document_version`
(
    `id_version_document` int(11)  NOT NULL AUTO_INCREMENT,
    `id_document`         int(11)  NOT NULL,
    `date_recuperation`   datetime NOT NULL,
    `hash`                varchar(500) DEFAULT NULL,
    `id_blob`             int(11)      DEFAULT NULL,
    `taille_document`     varchar(50)  DEFAULT NULL,
    `extension_document`  varchar(50)  DEFAULT NULL,
    `jeton_document`      text         DEFAULT NULL COMMENT 'contient le json retourner par le ws document dans le cas ou le ws ne retourne pas un fichier',
    `json_document`       text         DEFAULT NULL COMMENT 'contient le json retourner par le ws document dans le cas ou le ws ne retourne pas un fichier',
    PRIMARY KEY (`id_version_document`),
    KEY `entreprise_document_version` (`id_document`),
    CONSTRAINT `entreprise_document_version` FOREIGN KEY (`id_document`) REFERENCES `t_document_entreprise` (`id_document`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_enveloppe_dossier_formulaire`
--

--  `t_enveloppe_dossier_formulaire`;


CREATE TABLE `t_enveloppe_dossier_formulaire`
(
    `id_enveloppe_dossier_formulaire` int(11)            NOT NULL AUTO_INCREMENT,
    `id_dossier_formulaire`           int(11)            NOT NULL,
    `id_enveloppe`                    int(11)            NOT NULL,
    `organisme`                       varchar(30)        NOT NULL,
    `id_lot`                          int(11)      DEFAULT NULL,
    `type_enveloppe`                  int(1)             NOT NULL,
    `libelle_forrmulaire`             varchar(256)       NOT NULL,
    `cle_externe_dispositif`          int(11)            NOT NULL,
    `cle_externe_dossier`             int(11)      DEFAULT NULL,
    `statut_validation`               enum ('1','2','3') NOT NULL,
    `date_creation`                   varchar(256) DEFAULT NULL,
    `date_modif`                      varchar(256) DEFAULT NULL,
    `date_validation`                 varchar(256) DEFAULT NULL,
    `statut_generation_globale`       int(1)       DEFAULT NULL,
    `type_reponse`                    int(11)      DEFAULT 1 COMMENT 'Permet de spécifier si le dossier est initial ou bien correspond à une demande de complément. Les valeurs correspondantes sont dans application.xml',
    `cle_externe_formulaire`          int(11)      DEFAULT NULL COMMENT 'Permet de stocker l''identifiant du formulaire SUB (id_formulaire) recupérée après la validation',
    PRIMARY KEY (`id_enveloppe_dossier_formulaire`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_espace_collaboratif`
--

--  `t_espace_collaboratif`;


CREATE TABLE `t_espace_collaboratif`
(
    `id_auto`         int(11) unsigned NOT NULL AUTO_INCREMENT,
    `reference`       int(11) DEFAULT NULL,
    `organisme`       varchar(20)      NOT NULL,
    `nom_espace`      varchar(100)     NOT NULL,
    `consultation_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`id_auto`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Contient la liste des espaces collaboratifs créés';


--
-- Table structure for table `t_flux_rss`
--

--  `t_flux_rss`;


CREATE TABLE `t_flux_rss`
(
    `id`                int(11)        NOT NULL AUTO_INCREMENT,
    `tender_xml`        text           NOT NULL,
    `nom_fichier`       varchar(256)   NOT NULL,
    `libelle`           varchar(256)            DEFAULT NULL,
    `module`            varchar(256)            DEFAULT NULL,
    `afficher_flux_rss` enum ('0','1') NOT NULL DEFAULT '1' COMMENT 'ce paramettre permet d''afficher le flux rss dans la page des FluxRss',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_forme_prix`
--

--  `t_forme_prix`;


CREATE TABLE `t_forme_prix`
(
    `id_forme_prix`     int(11)               NOT NULL AUTO_INCREMENT,
    `forme_prix`        enum ('PU','PF','PM') NOT NULL,
    `pf_estimation_ht`  decimal(30, 2)                           DEFAULT NULL,
    `pf_estimation_ttc` decimal(30, 2)                           DEFAULT NULL,
    `pf_date_valeur`    date                                     DEFAULT NULL,
    `id_min_max`        int(11)                                  DEFAULT NULL,
    `modalite`          enum ('bon_commande','quantite_definie') DEFAULT NULL,
    `pu_min`            decimal(30, 2)                           DEFAULT NULL,
    `pu_max`            decimal(30, 2)                           DEFAULT NULL,
    `pu_min_ttc`        decimal(30, 2)                           DEFAULT NULL,
    `pu_max_ttc`        decimal(30, 2)                           DEFAULT NULL,
    `pu_estimation_ht`  decimal(30, 2)                           DEFAULT NULL,
    `pu_estimation_ttc` decimal(30, 2)                           DEFAULT NULL,
    `pu_date_valeur`    date                                     DEFAULT NULL,
    PRIMARY KEY (`id_forme_prix`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_forme_prix_has_ref_type_prix`
--

--  `t_forme_prix_has_ref_type_prix`;


CREATE TABLE `t_forme_prix_has_ref_type_prix`
(
    `id_forme_prix` int(11) NOT NULL,
    `id_type_prix`  int(11) NOT NULL,
    PRIMARY KEY (`id_type_prix`, `id_forme_prix`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_forme_prix_pf_has_ref_variation`
--

--  `t_forme_prix_pf_has_ref_variation`;


CREATE TABLE `t_forme_prix_pf_has_ref_variation`
(
    `id_variation`  int(11) NOT NULL,
    `id_forme_prix` int(11) NOT NULL,
    PRIMARY KEY (`id_variation`, `id_forme_prix`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_forme_prix_pu_has_ref_variation`
--

--  `t_forme_prix_pu_has_ref_variation`;


CREATE TABLE `t_forme_prix_pu_has_ref_variation`
(
    `id_variation`  int(11) NOT NULL,
    `id_forme_prix` int(11) NOT NULL,
    PRIMARY KEY (`id_variation`, `id_forme_prix`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_fusionner_services`
--

--  `t_fusionner_services`;


CREATE TABLE `t_fusionner_services`
(
    `id`                 int(11) NOT NULL AUTO_INCREMENT,
    `id_service_source`  int(11)        DEFAULT NULL,
    `id_service_cible`   int(11)        DEFAULT NULL,
    `organisme`          varchar(50)    DEFAULT NULL,
    `id_agent`           int(11)        DEFAULT NULL,
    `date_creation`      datetime       DEFAULT NULL,
    `date_fusion`        datetime       DEFAULT NULL,
    `donnees_fusionnees` enum ('0','1') DEFAULT '0' COMMENT '0=> données pas encore fusionnées, 1=> données sont fusionnées',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_type_groupement_entreprise`
--

--  `t_type_groupement_entreprise`;


CREATE TABLE `t_type_groupement_entreprise`
(
    `id_type_groupement`      int(11) NOT NULL AUTO_INCREMENT,
    `libelle_type_groupement` varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id_type_groupement`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_groupement_entreprise`
--

--  `t_groupement_entreprise`;


CREATE TABLE `t_groupement_entreprise`
(
    `id_groupement_entreprise` int(11) NOT NULL AUTO_INCREMENT,
    `id_type_groupement`       int(11)  DEFAULT NULL,
    `id_offre`                 int(11)  DEFAULT NULL,
    `date_creation`            datetime DEFAULT NULL,
    `id_candidature`           int(11)  DEFAULT NULL,
    PRIMARY KEY (`id_groupement_entreprise`),
    KEY `fk_Groupement_Entreprise_Type_Groupement` (`id_type_groupement`),
    KEY `fk_Groupement_Entreprise_Offres` (`id_offre`),
    KEY `fk_groupement_entreprise_candidature` (`id_candidature`),
    CONSTRAINT `fk_Groupement_Entreprise_Offres` FOREIGN KEY (`id_offre`) REFERENCES `Offres` (`id`),
    CONSTRAINT `fk_Groupement_Entreprise_Type_Groupement` FOREIGN KEY (`id_type_groupement`) REFERENCES `t_type_groupement_entreprise` (`id_type_groupement`),
    CONSTRAINT `fk_groupement_entreprise_candidature` FOREIGN KEY (`id_candidature`) REFERENCES `t_candidature` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_historique_annonce`
--

--  `t_historique_annonce`;


CREATE TABLE `t_historique_annonce`
(
    `id`                int(11)      NOT NULL AUTO_INCREMENT,
    `id_annonce`        int(11)      NOT NULL COMMENT 'Identifiant de l''annonce de publicité',
    `statut`            varchar(256) NOT NULL COMMENT 'Statut de l''annonce de publicité',
    `motif`             varchar(256)          DEFAULT NULL COMMENT 'Motif du statut de l''annonce de publicité',
    `date_modification` datetime     NOT NULL COMMENT 'Date de mise a jour',
    `id_support`        int(11)      NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `id_annonce` (`id_annonce`),
    CONSTRAINT `t_historique_annonce_id` FOREIGN KEY (`id_annonce`) REFERENCES `t_annonce_consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_historique_synchronisation_SGMAP`
--

--  `t_historique_synchronisation_SGMAP`;


CREATE TABLE `t_historique_synchronisation_SGMAP`
(
    `id_historique` int(11) NOT NULL AUTO_INCREMENT,
    `id_objet`      int(11)      DEFAULT NULL,
    `type_objet`    varchar(256) DEFAULT NULL,
    `code`          varchar(14)  DEFAULT NULL,
    `jeton`         text         DEFAULT NULL,
    `date_creation` varchar(20)  DEFAULT NULL,
    PRIMARY KEY (`id_historique`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_information_modification_password`
--

--  `t_information_modification_password`;


CREATE TABLE `t_information_modification_password`
(
    `id`                        int(11)        NOT NULL AUTO_INCREMENT,
    `id_user`                   int(11)        NOT NULL,
    `type_user`                 varchar(20)    NOT NULL,
    `email`                     varchar(100)   NOT NULL,
    `date_demande_modification` varchar(20)    NOT NULL,
    `date_fin_validite`         varchar(20)    NOT NULL,
    `jeton`                     varchar(256)   NOT NULL,
    `modification_faite`        enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_lancement_unique_cli`
--

--  `t_lancement_unique_cli`;


CREATE TABLE `t_lancement_unique_cli`
(
    `id_lancement_unique_cli` int(11)        NOT NULL AUTO_INCREMENT,
    `nom_cli`                 varchar(256)   NOT NULL,
    `en_cours`                enum ('0','1') NOT NULL DEFAULT '0',
    `date_lancement`          varchar(50)             DEFAULT NULL,
    `date_fin`                varchar(50)             DEFAULT NULL,
    PRIMARY KEY (`id_lancement_unique_cli`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_liste_lots_candidature`
--

--  `t_liste_lots_candidature`;


CREATE TABLE `t_liste_lots_candidature`
(
    `id`               int(11)     NOT NULL AUTO_INCREMENT,
    `ref_consultation` int(11)              DEFAULT NULL,
    `organisme`        varchar(45) NOT NULL,
    `id_inscrit`       int(11)     NOT NULL,
    `id_entreprise`    int(11)     NOT NULL,
    `id_etablissement` int(11)     NOT NULL,
    `status`           int(2)      NOT NULL DEFAULT 99,
    `id_candidature`   int(11)              DEFAULT NULL,
    `num_lot`          int(11)     NOT NULL,
    `consultation_id`  int(11)              DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_t_liste_lots_candidature_organisme` (`organisme`),
    KEY `FK_t_liste_lots_candidature_candidature` (`id_candidature`),
    KEY `FK_t_liste_lots_candidature_consultation` (`consultation_id`),
    CONSTRAINT `FK_t_liste_lots_candidature_candidature` FOREIGN KEY (`id_candidature`) REFERENCES `t_candidature` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_t_liste_lots_candidature_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_t_liste_lots_candidature_organisme` FOREIGN KEY (`organisme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_lot_technique`
--

--  `t_lot_technique`;


CREATE TABLE `t_lot_technique`
(
    `id_lot_technique`         int(11) NOT NULL AUTO_INCREMENT,
    `id_donnee_complementaire` int(11) NOT NULL,
    `numero_lot`               varchar(3)    DEFAULT NULL,
    `intitule_lot`             varchar(1000) DEFAULT NULL,
    `principal`                tinyint(1)    DEFAULT NULL,
    PRIMARY KEY (`id_lot_technique`),
    KEY `t_lot_technique_id_donnee_complementaire_fk` (`id_donnee_complementaire`),
    CONSTRAINT `T_LOT_TECHNIQUE_ID_DONNEE_COMPLEMENTAIRE_FK` FOREIGN KEY (`id_donnee_complementaire`) REFERENCES `t_donnee_complementaire` (`id_donnee_complementaire`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_tranche`
--

--  `t_tranche`;


CREATE TABLE `t_tranche`
(
    `id_tranche`               int(11) NOT NULL AUTO_INCREMENT,
    `id_forme_prix`            int(11)      DEFAULT NULL,
    `id_donnee_complementaire` int(11)      DEFAULT NULL,
    `nature_tranche`           varchar(100) DEFAULT NULL,
    `code_tranche`             int(11)      DEFAULT NULL,
    `intitule_tranche`         varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id_tranche`),
    KEY `T_TRANCHE_DONNEE_COMPLEMENTAIRE_FK` (`id_donnee_complementaire`),
    KEY `T_TRANCHE_FORME_PRIX_FK` (`id_forme_prix`),
    CONSTRAINT `T_TRANCHE_DONNEE_COMPLEMENTAIRE_FK` FOREIGN KEY (`id_donnee_complementaire`) REFERENCES `t_donnee_complementaire` (`id_donnee_complementaire`),
    CONSTRAINT `T_TRANCHE_FORME_PRIX_FK` FOREIGN KEY (`id_forme_prix`) REFERENCES `t_forme_prix` (`id_forme_prix`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_lot_technique_has_tranche`
--

--  `t_lot_technique_has_tranche`;


CREATE TABLE `t_lot_technique_has_tranche`
(
    `id_lot_technique` int(11) NOT NULL,
    `id_tranche`       int(11) NOT NULL,
    PRIMARY KEY (`id_lot_technique`, `id_tranche`),
    KEY `t_lot_technique_has_tranche_id_tranche_fk` (`id_tranche`),
    CONSTRAINT `T_LOT_TECHNIQUE_HAS_TRANCHE_ID_LOT_TECHNIQUE_FK` FOREIGN KEY (`id_lot_technique`) REFERENCES `t_lot_technique` (`id_lot_technique`),
    CONSTRAINT `T_LOT_TECHNIQUE_HAS_TRANCHE_ID_TRANCHE_FK` FOREIGN KEY (`id_tranche`) REFERENCES `t_tranche` (`id_tranche`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_role_juridique`
--

--  `t_role_juridique`;


CREATE TABLE `t_role_juridique`
(
    `id_role_juridique`      int(11) NOT NULL AUTO_INCREMENT,
    `libelle_role_juridique` varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id_role_juridique`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_membre_groupement_entreprise`
--

--  `t_membre_groupement_entreprise`;


CREATE TABLE `t_membre_groupement_entreprise`
(
    `id_membre_groupement_entreprise` int(11) NOT NULL AUTO_INCREMENT,
    `id_role_juridique`               int(11)      DEFAULT NULL,
    `id_groupement_entreprise`        int(11)      DEFAULT NULL,
    `id_entreprise`                   int(11)      DEFAULT NULL,
    `id_etablissement`                int(11)      DEFAULT NULL,
    `id_membre_parent`                int(11)      DEFAULT NULL,
    `numeroSN`                        varchar(256) DEFAULT NULL,
    `email`                           varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id_membre_groupement_entreprise`),
    KEY `fk_Membre_Groupement_Entreprise_Role_Juridique` (`id_role_juridique`),
    KEY `fk_Membre_Groupement_Entreprise_Groupement` (`id_groupement_entreprise`),
    KEY `fk_Membre_Groupement_Entreprise_Entreprise` (`id_entreprise`),
    KEY `fk_Membre_Groupement_Entreprise_Etablissement` (`id_etablissement`),
    KEY `fk_Membre_Groupement_Entreprise_Perent` (`id_membre_parent`),
    CONSTRAINT `fk_Membre_Groupement_Entreprise_Entreprise` FOREIGN KEY (`id_entreprise`) REFERENCES `Entreprise` (`id`),
    CONSTRAINT `fk_Membre_Groupement_Entreprise_Etablissement` FOREIGN KEY (`id_etablissement`) REFERENCES `t_etablissement` (`id_etablissement`),
    CONSTRAINT `fk_Membre_Groupement_Entreprise_Groupement` FOREIGN KEY (`id_groupement_entreprise`) REFERENCES `t_groupement_entreprise` (`id_groupement_entreprise`),
    CONSTRAINT `fk_Membre_Groupement_Entreprise_Perent` FOREIGN KEY (`id_membre_parent`) REFERENCES `t_membre_groupement_entreprise` (`id_membre_groupement_entreprise`),
    CONSTRAINT `fk_Membre_Groupement_Entreprise_Role_Juridique` FOREIGN KEY (`id_role_juridique`) REFERENCES `t_role_juridique` (`id_role_juridique`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_message_accueil`
--

--  `t_message_accueil`;


CREATE TABLE `t_message_accueil`
(
    `id`           int(11)      NOT NULL AUTO_INCREMENT,
    `type_message` varchar(256) NOT NULL DEFAULT '' COMMENT 'le style du message, prend les valeurs info, avertissement, erreur',
    `contenu`      text         NOT NULL,
    `destinataire` varchar(50)  NOT NULL DEFAULT '' COMMENT 'entreprise , agent ',
    `authentifier` int(1)                DEFAULT NULL COMMENT '0=>non authentifié(e) ,1=>authentifié(e)',
    `config`       varchar(256)          DEFAULT '' COMMENT 'on l utilise dans le cas que plusieurs pf utilise la même base de donnée (cas : PLACE et ORME)',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_type_notification_agent`
--

--  `t_type_notification_agent`;


CREATE TABLE `t_type_notification_agent`
(
    `id_type` int(11) NOT NULL AUTO_INCREMENT,
    `libelle` varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id_type`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


--
-- Table structure for table `t_notification_agent`
--

--  `t_notification_agent`;


CREATE TABLE `t_notification_agent`
(
    `id_notification`      int(11)        NOT NULL AUTO_INCREMENT,
    `id_agent`             int(11)                 DEFAULT NULL,
    `id_type`              int(11)        NOT NULL,
    `service_id`           int(11)                 DEFAULT NULL,
    `organisme`            varchar(45)             DEFAULT NULL,
    `reference_objet`      varchar(256)            DEFAULT NULL,
    `libelle_notification` varchar(256)            DEFAULT NULL,
    `description`          text                    DEFAULT NULL,
    `notification_lue`     enum ('0','1') NOT NULL DEFAULT '0',
    `notification_actif`   enum ('0','1') NOT NULL DEFAULT '1',
    `url_notification`     varchar(500)            DEFAULT NULL,
    `date_creation`        datetime                DEFAULT NULL,
    `date_lecture`         datetime                DEFAULT NULL,
    PRIMARY KEY (`id_notification`),
    KEY `fk_Type_Notification_Agent` (`id_type`),
    CONSTRAINT `fk_Type_Notification_Agent` FOREIGN KEY (`id_type`) REFERENCES `t_type_notification_agent` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_numero_reponse`
--

--  `t_numero_reponse`;


CREATE TABLE `t_numero_reponse`
(
    `id`               int(11) NOT NULL,
    `consultation_ref` int(11)     DEFAULT NULL,
    `numero_reponse`   int(11) NOT NULL,
    `organisme`        varchar(50) DEFAULT NULL,
    `consultation_id`  int(11)     DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_numerotation_automatique`
--

--  `t_numerotation_automatique`;


CREATE TABLE `t_numerotation_automatique`
(
    `id`                   int(11)     NOT NULL AUTO_INCREMENT,
    `organisme`            varchar(45) NOT NULL,
    `service_id`           int(11)     DEFAULT NULL,
    `annee`                varchar(10) DEFAULT NULL,
    `contrat_multi`        varchar(20) DEFAULT NULL,
    `contrat_SA_dynamique` varchar(20) DEFAULT NULL,
    `contrat_titulaire`    varchar(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_param_dossier_formulaire`
--

--  `t_param_dossier_formulaire`;


CREATE TABLE `t_param_dossier_formulaire`
(
    `id_param_dossier_formulaire` int(11)      NOT NULL AUTO_INCREMENT,
    `organisme`                   varchar(30)  NOT NULL,
    `consultation_ref`            int(11) DEFAULT NULL,
    `type_enveloppe`              int(1)       NOT NULL,
    `libelle_formulaire`          varchar(256) NOT NULL,
    `cle_externe_dispositif`      int(11)      NOT NULL,
    `consultation_id`             int(11) DEFAULT NULL,
    PRIMARY KEY (`id_param_dossier_formulaire`),
    KEY `consultation_t_param_dossier_form_consultation_ref_fk` (`organisme`, `consultation_ref`),
    KEY `FK_consultation_t_param_dossier_form_consultation_ref_fk` (`consultation_id`),
    CONSTRAINT `FK_consultation_t_param_dossier_form_consultation_ref_fk` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_parametrage_formulaire_publicite`
--

--  `t_parametrage_formulaire_publicite`;


CREATE TABLE `t_parametrage_formulaire_publicite`
(
    `id`              int(11) NOT NULL AUTO_INCREMENT,
    `id_support`      int(11) NOT NULL,
    `id_type_annonce` int(11) NOT NULL,
    `ids_formulaire`  varchar(256) DEFAULT NULL COMMENT 'Les ids des formulaires doivent etre separes par des virgules',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_preference_offre_support_publication`
--

--  `t_preference_offre_support_publication`;


CREATE TABLE `t_preference_offre_support_publication`
(
    `id`            int(11)        NOT NULL AUTO_INCREMENT,
    `id_preference` int(11)        NOT NULL,
    `id_support`    int(11)        NOT NULL,
    `id_offre`      int(11)        NOT NULL,
    `active`        enum ('0','1') NOT NULL DEFAULT '0',
    `id_agent`      int(11)                 DEFAULT NULL,
    `organisme`     int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_preference_support_publication`
--

--  `t_preference_support_publication`;


CREATE TABLE `t_preference_support_publication`
(
    `id`         int(11)        NOT NULL AUTO_INCREMENT,
    `id_support` int(11)        NOT NULL,
    `id_agent`   int(11)        NOT NULL,
    `organisme`  varchar(30)    NOT NULL,
    `active`     enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_procedure_equivalence_dume`
--

--  `t_procedure_equivalence_dume`;


CREATE TABLE `t_procedure_equivalence_dume`
(
    `id`                     int(11)        NOT NULL AUTO_INCREMENT,
    `id_type_procedure`      int(11)        NOT NULL DEFAULT 0,
    `organisme`              varchar(50)    NOT NULL DEFAULT '',
    `id_type_procedure_dume` int(11)        NOT NULL DEFAULT 0,
    `afficher`               enum ('0','1') NOT NULL DEFAULT '0',
    `figer`                  enum ('0','1') NOT NULL DEFAULT '0',
    `selectionner`           enum ('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_publicite_favoris`
--

--  `t_publicite_favoris`;


CREATE TABLE `t_publicite_favoris`
(
    `id`                                     int(22)      NOT NULL AUTO_INCREMENT,
    `organisme`                              varchar(30)  NOT NULL,
    `service_id`                             int(22)      NOT NULL,
    `libelle`                                varchar(100) NOT NULL DEFAULT '',
    `regime_financier_cautionnement`         longtext              DEFAULT NULL,
    `regime_financier_modalites_financement` longtext              DEFAULT NULL,
    `acheteur_correspondant`                 varchar(100) NOT NULL DEFAULT '',
    `acheteur_nom_organisme`                 varchar(200)          DEFAULT NULL,
    `acheteur_adresse`                       varchar(100) NOT NULL DEFAULT '',
    `acheteur_cp`                            varchar(5)   NOT NULL DEFAULT '',
    `acheteur_ville`                         varchar(100) NOT NULL DEFAULT '',
    `acheteur_url`                           varchar(100) NOT NULL DEFAULT '',
    `facture_denomination`                   varchar(100)          DEFAULT NULL,
    `facture_adresse`                        varchar(256) NOT NULL DEFAULT '',
    `facture_cp`                             varchar(10)  NOT NULL DEFAULT '',
    `facture_ville`                          varchar(100) NOT NULL DEFAULT '',
    `instance_recours_organisme`             varchar(200)          DEFAULT NULL,
    `instance_recours_adresse`               varchar(200)          DEFAULT NULL,
    `instance_recours_cp`                    varchar(200)          DEFAULT NULL,
    `instance_recours_ville`                 varchar(200)          DEFAULT NULL,
    `instance_recours_url`                   text                  DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_referentiel_certificat`
--

--  `t_referentiel_certificat`;


CREATE TABLE `t_referentiel_certificat`
(
    `id`                            int(11)      NOT NULL AUTO_INCREMENT,
    `nom_referentiel_certificat`    varchar(256) NOT NULL,
    `statut_referentiel_certificat` int(1)       NOT NULL,
    `nom_referentiel_fonctionnel`   varchar(256) DEFAULT NULL,
    `message`                       varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Cette table contient les référentiel des certificats et leurs validités ';


--
-- Table structure for table `t_referentiel_mots_cles`
--

--  `t_referentiel_mots_cles`;


CREATE TABLE `t_referentiel_mots_cles`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `libelle`    varchar(256) NOT NULL,
    `valeur_sub` varchar(256) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_referentiel_nace`
--

--  `t_referentiel_nace`;


CREATE TABLE `t_referentiel_nace`
(
    `id`                         int(11)      NOT NULL AUTO_INCREMENT,
    `code_nace_5`                varchar(256) NOT NULL,
    `libelle_activite_detaillee` text         NOT NULL,
    `code_nace_2`                varchar(256) NOT NULL,
    `libelle_activite_general`   text         NOT NULL,
    PRIMARY KEY (`id`),
    KEY `code_nace_5` (`code_nace_5`, `code_nace_2`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_sous_critere_attribution`
--

--  `t_sous_critere_attribution`;


CREATE TABLE `t_sous_critere_attribution`
(
    `id_sous_critere_attribution` int(50) NOT NULL AUTO_INCREMENT,
    `enonce`                      text          DEFAULT NULL,
    `ponderation`                 decimal(5, 2) DEFAULT 0.00,
    `id_critere_attribution`      int(11)       DEFAULT NULL,
    PRIMARY KEY (`id_sous_critere_attribution`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_support_annonce_consultation`
--

--  `t_support_annonce_consultation`;


CREATE TABLE `t_support_annonce_consultation`
(
    `id`                            int(11)      NOT NULL AUTO_INCREMENT,
    `id_support`                    int(11)      NOT NULL,
    `id_annonce_cons`               int(11)      NOT NULL,
    `prenom_nom_agent_createur`     varchar(256) NOT NULL,
    `id_agent`                      int(11)      NOT NULL,
    `date_creation`                 datetime     NOT NULL,
    `statut`                        varchar(256) NOT NULL,
    `date_statut`                   datetime     NOT NULL,
    `numero_avis`                   varchar(256) DEFAULT NULL COMMENT 'Numero de l''avis envoyé par le concentrateur',
    `message_statut`                varchar(256) DEFAULT NULL COMMENT 'Message correspondant au statut',
    `lien_publication`              text         DEFAULT NULL COMMENT 'lien de publication de l''annonce',
    `date_envoi_support`            datetime     DEFAULT NULL COMMENT 'Date d''envoi de la publicité au support',
    `date_publication_support`      datetime     DEFAULT NULL COMMENT 'Date de publication de la publicité au support',
    `numero_avis_parent`            varchar(256) DEFAULT NULL COMMENT 'Numero de l''avis parent',
    `id_offre`                      int(11)      DEFAULT NULL,
    `message_acheteur`              varchar(256) DEFAULT NULL,
    `departements_parution_annonce` varchar(256) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `id_support` (`id_support`),
    KEY `id_annonce_cons` (`id_annonce_cons`),
    CONSTRAINT `fk_tSupportAnnonceCons_reference_tAnnonceConsultation` FOREIGN KEY (`id_annonce_cons`) REFERENCES `t_annonce_consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_tSupportAnnonceCons_reference_tSupportPublication` FOREIGN KEY (`id_support`) REFERENCES `t_support_publication` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_synthese_rapport_audit`
--

--  `t_synthese_rapport_audit`;


CREATE TABLE `t_synthese_rapport_audit`
(
    `id`            int(20)     NOT NULL AUTO_INCREMENT,
    `organisme`     varchar(30) NOT NULL DEFAULT '',
    `nom_fichier`   varchar(50) NOT NULL DEFAULT '',
    `fichier`       varchar(20) NOT NULL DEFAULT '',
    `date`          varchar(20) NOT NULL DEFAULT '',
    `horodatage`    longblob    NOT NULL,
    `untrusteddate` varchar(20) NOT NULL DEFAULT '',
    `taille`        int(20)     NOT NULL DEFAULT 0,
    `annee`         varchar(20) NOT NULL,
    `service_id`    int(20)     NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`, `organisme`),
    KEY `service_id` (`service_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_type_contrat`
--

--  `t_type_contrat`;


CREATE TABLE `t_type_contrat`
(
    `id_type_contrat`          int(11)            NOT NULL AUTO_INCREMENT,
    `libelle_type_contrat`     varchar(256)       NOT NULL,
    `abreviation_type_contrat` char(2)                     DEFAULT NULL COMMENT 'Abreviation des types de contrats',
    `type_contrat_statistique` smallint(6)        NOT NULL DEFAULT 0 COMMENT '0:Marché public, 1:Marché subsequent, 2:Marché spécifique, 3:Accord cadre, 4:SAD',
    `multi`                    enum ('0','1')     NOT NULL DEFAULT '0' COMMENT 'ce champ est egale a un si ce type accepte contrat multi',
    `accord_cadre_sad`         enum ('0','1','2') NOT NULL DEFAULT '0' COMMENT '0: ni accord-cadre ni sad, 1: accord-cadre, 2: sad',
    `avec_chapeau`             enum ('0','1')     NOT NULL DEFAULT '0',
    `avec_montant`             enum ('0','1')     NOT NULL DEFAULT '0',
    `mode_echange_chorus`      char(1)            NOT NULL DEFAULT '0',
    `marche_subsequent`        char(1)            NOT NULL DEFAULT '0' COMMENT 'permet de définir le type de marché : 0=>pas de spécification sur le type, 1=>marché subséquent,2=>marché spécifique',
    `avec_montant_max`         enum ('0','1')     NOT NULL DEFAULT '0' COMMENT 'permet d''afficher ou non le montant max dans le chapeau',
    `ordre_affichage`          int(11)                     DEFAULT NULL,
    `article_133`              enum ('0','1')     NOT NULL DEFAULT '0',
    `code_dume`                varchar(50)                 DEFAULT NULL,
    `concession`               tinyint(1)         NOT NULL DEFAULT 0,
    `id_externe`               varchar(256)                DEFAULT '' COMMENT 'Id unique sauf pour les MAPA',
    PRIMARY KEY (`id_type_contrat`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_type_contrat_et_procedure`
--

--  `t_type_contrat_et_procedure`;


CREATE TABLE `t_type_contrat_et_procedure`
(
    `id_type_contrat_et_procedure` int(11) NOT NULL AUTO_INCREMENT,
    `id_type_contrat`              int(11) NOT NULL,
    `id_type_procedure`            int(11) NOT NULL,
    `organisme`                    varchar(30) DEFAULT NULL,
    PRIMARY KEY (`id_type_contrat_et_procedure`),
    UNIQUE KEY `Type_Contrat_Procedure_Organisme_Index` (`id_type_contrat`, `id_type_procedure`, `organisme`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_type_procedure_dume`
--

--  `t_type_procedure_dume`;


CREATE TABLE `t_type_procedure_dume`
(
    `id`        int(11)        NOT NULL AUTO_INCREMENT,
    `libelle`   varchar(256)   NOT NULL DEFAULT '',
    `active`    enum ('0','1') NOT NULL DEFAULT '1',
    `code_dume` varchar(50)             DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `t_vision_rma_agent_organisme`
--

--  `t_vision_rma_agent_organisme`;


CREATE TABLE `t_vision_rma_agent_organisme`
(
    `id`       int(11)     NOT NULL AUTO_INCREMENT,
    `id_agent` int(11)     NOT NULL,
    `acronyme` varchar(30) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `t_vision_rma_agent_organisme_ID_AGENT_FK` (`id_agent`),
    KEY `t_vision_rma_agent_organisme_ACRONYME_FK` (`acronyme`),
    CONSTRAINT `t_vision_rma_agent_organisme_ACRONYME_FK` FOREIGN KEY (`acronyme`) REFERENCES `Organisme` (`acronyme`),
    CONSTRAINT `t_vision_rma_agent_organisme_ID_AGENT_FK` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `tmp_siret_incorrect`
--

--  `tmp_siret_incorrect`;


CREATE TABLE `tmp_siret_incorrect`
(
    `id`              int(10) unsigned                     NOT NULL AUTO_INCREMENT,
    `id_inscrit`      int(11)                              NOT NULL,
    `email`           varchar(256) COLLATE utf8_unicode_ci NOT NULL,
    `siret`           varchar(256) COLLATE utf8_unicode_ci NOT NULL,
    `is_send_message` tinyint(1)                           NOT NULL,
    `send_date`       datetime DEFAULT NULL,
    `create_date`     datetime                             NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `UNIQ_4703DC41E7927C74` (`email`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
  



--
-- Table structure for table `trace_operations_inscrit`
--

--  `trace_operations_inscrit`;


CREATE TABLE `trace_operations_inscrit`
(
    `id_trace`         int(11)        NOT NULL AUTO_INCREMENT,
    `id_inscrit`       int(11)        NOT NULL,
    `id_entreprise`    int(11)        NOT NULL,
    `addr_ip`          varchar(15)    NOT NULL,
    `date`             date           NOT NULL,
    `operations`       text           NOT NULL,
    `organisme`        varchar(30)             DEFAULT NULL,
    `ref_consultation` varchar(30)             DEFAULT NULL,
    `afficher`         enum ('0','1') NOT NULL DEFAULT '0',
    `consultation_id`  int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id_trace`),
    KEY `recherche` (`id_inscrit`, `id_entreprise`, `addr_ip`, `date`),
    KEY `ref_consutation` (`organisme`, `ref_consultation`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `type_contrat_concession_pivot`
--

--  `type_contrat_concession_pivot`;


CREATE TABLE `type_contrat_concession_pivot`
(
    `id`      int(11)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(256) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `type_contrat_mpe_pivot`
--

--  `type_contrat_mpe_pivot`;


CREATE TABLE `type_contrat_mpe_pivot`
(
    `id`                    int(11) NOT NULL AUTO_INCREMENT,
    `id_type_contrat_mpe`   int(11) NOT NULL,
    `id_type_contrat_pivot` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `type_contrat_pivot`
--

--  `type_contrat_pivot`;


CREATE TABLE `type_contrat_pivot`
(
    `id`      int(11)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(256) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `type_procedure_concession_pivot`
--

--  `type_procedure_concession_pivot`;


CREATE TABLE `type_procedure_concession_pivot`
(
    `id`      int(11)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(256) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `type_procedure_mpe_pivot`
--

--  `type_procedure_mpe_pivot`;


CREATE TABLE `type_procedure_mpe_pivot`
(
    `id`                      int(11) NOT NULL AUTO_INCREMENT,
    `id_type_procedure_mpe`   int(11) NOT NULL,
    `id_type_procedure_pivot` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `type_procedure_pivot`
--

--  `type_procedure_pivot`;


CREATE TABLE `type_procedure_pivot`
(
    `id`      int(11)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(256) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Table structure for table `visite_lieux`
--

--  `visite_lieux`;


CREATE TABLE `visite_lieux`
(
    `id`              int(50)      NOT NULL AUTO_INCREMENT,
    `organisme`       varchar(30)  NOT NULL DEFAULT '',
    `reference`       int(50)      NOT NULL DEFAULT 0,
    `adresse`         varchar(250) NOT NULL DEFAULT '',
    `id_tr_adresse`   int(11)               DEFAULT NULL COMMENT 'Identifiant de la traduction de l''adresse des visites des lieux',
    `date`            varchar(50)  NOT NULL DEFAULT '',
    `lot`             char(1)      NOT NULL DEFAULT '',
    `adresse_fr`      varchar(256)          DEFAULT NULL,
    `adresse_en`      varchar(256)          DEFAULT NULL,
    `adresse_es`      varchar(256)          DEFAULT NULL,
    `adresse_su`      varchar(256)          DEFAULT NULL,
    `adresse_du`      varchar(256)          DEFAULT NULL,
    `adresse_cz`      varchar(256)          DEFAULT NULL,
    `adresse_ar`      varchar(256)          DEFAULT NULL,
    `adresse_it`      varchar(256)          DEFAULT NULL,
    `consultation_id` int(11)               DEFAULT NULL,
    PRIMARY KEY (`id`, `organisme`),
    KEY `reference` (`reference`),
    KEY `visite_lieux_consultation` (`organisme`, `reference`),
    KEY `FK_visite_lieux_consultation` (`consultation_id`),
    CONSTRAINT `FK_visite_lieux_consultation` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



--
-- Dumping routines for database 'place_recette_MPE_DB'
--

--
-- Final view structure for view `_Vue_Consultation`
--

/*!50001 DROP TABLE IF EXISTS `_Vue_Consultation`*/;
/*!50001 DROP VIEW IF EXISTS `_Vue_Consultation`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8mb4 */;
/*!50001 SET character_set_results = utf8mb4 */;
/*!50001 SET collation_connection = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */ /*!50001 VIEW `_Vue_Consultation` AS
select `consultation`.`datefin`                                                                                                            AS `datefin`,
       `consultation`.`date_mise_en_ligne_calcule`                                                                                         AS `date_mise_en_ligne_calcule`,
       `consultation`.`reference`                                                                                                          AS `reference`,
       `consultation`.`organisme`                                                                                                          AS `organisme`,
       concat(`consultation`.`reference`, '_', `consultation`.`organisme`)                                                                 AS `Ref_Orga`,
       `consultation`.`type_acces` - 1                                                                                                     AS `consultation restreinte`,
       (select `Organisme`.`denomination_org`
        from `Organisme`
        where `Organisme`.`acronyme` = `consultation`.`organisme`)                                                                         AS `Nom_organisme`,
       (select `Organisme`.`active`
        from `Organisme`
        where `Organisme`.`acronyme` = `consultation`.`organisme`)                                                                         AS `active`,
       `consultation`.`service_id`                                                                                                         AS `service_id`,
       (select `Service`.`libelle`
        from `Service`
        where `Service`.`id` = `consultation`.`service_id`
          and `Service`.`organisme` = `consultation`.`organisme`)                                                                          AS `Nom_service`,
       `consultation`.`reference_utilisateur`                                                                                              AS `reference_utilisateur`,
       `consultation`.`categorie`                                                                                                          AS `categorie`,
       (select `CategorieConsultation`.`libelle`
        from `CategorieConsultation`
        where `CategorieConsultation`.`id` = `consultation`.`categorie`)                                                                   AS `Nom_categorie`,
       `consultation`.`id_type_procedure_org`                                                                                              AS `id_type_procedure_org`,
       (select `Type_Procedure_Organisme`.`libelle_type_procedure`
        from `Type_Procedure_Organisme`
        where `Type_Procedure_Organisme`.`id_type_procedure` = `consultation`.`id_type_procedure_org`
          and `Type_Procedure_Organisme`.`organisme` = `consultation`.`organisme`)                                                         AS `Nom_Procedure_Agent`,
       substr(`consultation`.`objet`, 1, 100)                                                                                              AS `SUBSTRING(consultation.objet,1,100)`,
       `consultation`.`marche_public_simplifie`                                                                                            AS `marche_public_simplifie`,
       `consultation`.`id_type_avis`                                                                                                       AS `id_type_avis`,
       `consultation`.`alloti`                                                                                                             AS `alloti`,
       (select count(0)
        from `CategorieLot`
        where `CategorieLot`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `CategorieLot`.`organisme`)                                                                     AS `nbre_lots`,
       1 - (`consultation`.`alloti` - 1) + (`consultation`.`alloti` - 1) * (select count(0)
                                                                            from `CategorieLot`
                                                                            where `CategorieLot`.`consultation_ref` = `consultation`.`reference`
                                                                              and `consultation`.`organisme` = `CategorieLot`.`organisme`) AS `Equiv_Lots`,
       (select `DCE`.`taille_dce` / 1000000
        from `DCE`
        where `consultation`.`organisme` = `DCE`.`organisme`
          and `consultation`.`reference` = `DCE`.`consultation_ref`
        order by `DCE`.`untrusteddate` desc
        limit 0,1)                                                                                                                         AS `Taille_DCE_Mo`,
       (select count(0)
        from `Telechargement`
        where `Telechargement`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `Telechargement`.`organisme`)                                                                   AS `nbre_retrait_NON_ANO`,
       (select count(0)
        from `TelechargementAnonyme`
        where `TelechargementAnonyme`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `TelechargementAnonyme`.`organisme`)                                                            AS `nbre_retrait_ANONYME`,
       (select count(0)
        from `Offres`
        where `Offres`.`envoi_complet` = 1
          and `Offres`.`statut_offres` <> '99'
          and `Offres`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `Offres`.`organisme`)                                                                           AS `nbre_reponse_Elec`,
       (select count(distinct `t_candidature_mps`.`id_entreprise`)
        from `t_candidature_mps`
        where `t_candidature_mps`.`ref_consultation` = `consultation`.`reference`
          and `t_candidature_mps`.`id_offre` <> 'NULL'
          and `t_candidature_mps`.`id_entreprise` in (select `Offres`.`entreprise_id`
                                                      from `Offres`
                                                      where `Offres`.`envoi_complet` = 1
                                                        and `Offres`.`statut_offres` <> '99'
                                                        and `Offres`.`consultation_ref` = `t_candidature_mps`.`ref_consultation`))         AS `dont_reponse_MPS`,
       (select sum(`fichierEnveloppe`.`taille_fichier`) / 1000000
        from ((`Offres` join `Enveloppe`)
                 join `fichierEnveloppe`)
        where `Offres`.`id` = `Enveloppe`.`offre_id`
          and `Offres`.`organisme` = `Enveloppe`.`organisme`
          and `Enveloppe`.`id_enveloppe_electro` = `fichierEnveloppe`.`id_enveloppe`
          and `Enveloppe`.`organisme` = `fichierEnveloppe`.`organisme`
          and `Offres`.`envoi_complet` = '1'
          and `Offres`.`statut_offres` <> '99'
          and `Offres`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `Offres`.`organisme`)                                                                           AS `Poids des réponses en Mo`
from `consultation`
where `consultation`.`id_type_avis` = '3'
  and `consultation`.`organisme` in (select `Organisme`.`acronyme` from `Organisme` where `Organisme`.`active` = 1)
  and `consultation`.`date_mise_en_ligne_calcule` < `consultation`.`datefin`
  and `consultation`.`date_mise_en_ligne_calcule` <> '0000-00-00 00:00:00'
  and `consultation`.`date_mise_en_ligne_calcule` is not null
        */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;

--
-- Final view structure for view `_Vue_Consultation_sup_a_2015`
--

/*!50001 DROP TABLE IF EXISTS `_Vue_Consultation_sup_a_2015`*/;
/*!50001 DROP VIEW IF EXISTS `_Vue_Consultation_sup_a_2015`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8 */;
/*!50001 SET character_set_results = utf8 */;
/*!50001 SET collation_connection = utf8_general_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */ /*!50001 VIEW `_Vue_Consultation_sup_a_2015` AS
select `consultation`.`datefin`                                                                                                            AS `datefin`,
       `consultation`.`date_mise_en_ligne_calcule`                                                                                         AS `date_mise_en_ligne_calcule`,
       `consultation`.`reference`                                                                                                          AS `reference`,
       `consultation`.`organisme`                                                                                                          AS `organisme`,
       concat(`consultation`.`reference`, '_', `consultation`.`organisme`)                                                                 AS `Ref_Orga`,
       `consultation`.`type_acces` - 1                                                                                                     AS `consultation restreinte`,
       (select `Organisme`.`denomination_org`
        from `Organisme`
        where `Organisme`.`acronyme` = `consultation`.`organisme`)                                                                         AS `Nom_organisme`,
       (select `Organisme`.`active`
        from `Organisme`
        where `Organisme`.`acronyme` = `consultation`.`organisme`)                                                                         AS `active`,
       `consultation`.`service_id`                                                                                                         AS `service_id`,
       (select `ser`.`libelle`
        from `Service` `ser`
        where `ser`.`id` = `consultation`.`service_id`
          and `ser`.`organisme` = `consultation`.`organisme`)                                                                              AS `Nom_service`,
       `consultation`.`reference_utilisateur`                                                                                              AS `reference_utilisateur`,
       `consultation`.`categorie`                                                                                                          AS `categorie`,
       (select `CategorieConsultation`.`libelle`
        from `CategorieConsultation`
        where `CategorieConsultation`.`id` = `consultation`.`categorie`)                                                                   AS `Nom_categorie`,
       `consultation`.`id_type_procedure_org`                                                                                              AS `id_type_procedure_org`,
       (select `tpo`.`libelle_type_procedure`
        from `Type_Procedure_Organisme` `tpo`
        where `tpo`.`id_type_procedure` = `consultation`.`id_type_procedure_org`
          and `tpo`.`organisme` = `consultation`.`organisme`)                                                                              AS `Nom_Procedure_Agent`,
       substr(`consultation`.`objet`, 1, 100)                                                                                              AS `SUBSTRING(consultation.objet,1,100)`,
       `consultation`.`marche_public_simplifie`                                                                                            AS `marche_public_simplifie`,
       `consultation`.`id_type_avis`                                                                                                       AS `id_type_avis`,
       `consultation`.`alloti`                                                                                                             AS `alloti`,
       (select count(0)
        from `CategorieLot`
        where `CategorieLot`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `CategorieLot`.`organisme`)                                                                     AS `nbre_lots`,
       1 - (`consultation`.`alloti` - 1) + (`consultation`.`alloti` - 1) * (select count(0)
                                                                            from `CategorieLot`
                                                                            where `CategorieLot`.`consultation_ref` = `consultation`.`reference`
                                                                              and `consultation`.`organisme` = `CategorieLot`.`organisme`) AS `Equiv_Lots`,
       (select `DCE`.`taille_dce` / 1000000
        from `DCE`
        where `consultation`.`organisme` = `DCE`.`organisme`
          and `consultation`.`reference` = `DCE`.`consultation_ref`
        order by `DCE`.`untrusteddate` desc
        limit 0,1)                                                                                                                         AS `Taille_DCE_Mo`,
       (select count(0)
        from `Telechargement`
        where `Telechargement`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `Telechargement`.`organisme`)                                                                   AS `nbre_retrait_NON_ANO`,
       (select count(0)
        from `TelechargementAnonyme`
        where `TelechargementAnonyme`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `TelechargementAnonyme`.`organisme`)                                                            AS `nbre_retrait_ANONYME`,
       (select count(0)
        from `Offres`
        where `Offres`.`envoi_complet` = 1
          and `Offres`.`statut_offres` <> '99'
          and `Offres`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `Offres`.`organisme`)                                                                           AS `nbre_reponse_Elec`,
       (select count(distinct `t_candidature_mps`.`id_entreprise`)
        from `t_candidature_mps`
        where `t_candidature_mps`.`ref_consultation` = `consultation`.`reference`
          and `t_candidature_mps`.`id_offre` <> 'NULL'
          and `t_candidature_mps`.`id_entreprise` in (select `Offres`.`entreprise_id`
                                                      from `Offres`
                                                      where `Offres`.`envoi_complet` = 1
                                                        and `Offres`.`statut_offres` <> '99'
                                                        and `Offres`.`consultation_ref` = `t_candidature_mps`.`ref_consultation`))         AS `dont_reponse_MPS`,
       (select sum(`fichierEnveloppe`.`taille_fichier`) / 1000000
        from ((`Offres` join `Enveloppe`)
                 join `fichierEnveloppe`)
        where `Offres`.`id` = `Enveloppe`.`offre_id`
          and `Offres`.`organisme` = `Enveloppe`.`organisme`
          and `Enveloppe`.`id_enveloppe_electro` = `fichierEnveloppe`.`id_enveloppe`
          and `Enveloppe`.`organisme` = `fichierEnveloppe`.`organisme`
          and `Offres`.`envoi_complet` = '1'
          and `Offres`.`statut_offres` <> '99'
          and `Offres`.`consultation_ref` = `consultation`.`reference`
          and `consultation`.`organisme` = `Offres`.`organisme`)                                                                           AS `Poids des réponses en Mo`,
       `age`.`id`                                                                                                                          AS `idAgent`,
       concat(`age`.`nom`, ' ', `age`.`prenom`)                                                                                            AS `nomPrenomAgent`,
       `age`.`email`                                                                                                                       AS `mailAgent`,
       `age`.`num_tel`                                                                                                                     AS `telephoneAgent`
from (`consultation`
         left join `Agent` `age` on (`age`.`id` = `consultation`.`id_createur`))
where `consultation`.`id_type_avis` = '3'
  and `consultation`.`organisme` in (select `Organisme`.`acronyme` from `Organisme` where `Organisme`.`active` = 1)
  and `consultation`.`date_mise_en_ligne_calcule` < `consultation`.`datefin`
  and `consultation`.`date_mise_en_ligne_calcule` <> '0000-00-00 00:00:00'
  and `consultation`.`date_mise_en_ligne_calcule` is not null
  and `consultation`.`datefin` >= '2015-01-01 00:00:00'
        */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;

--
-- Final view structure for view `_Vue_Offre`
--

/*!50001 DROP TABLE IF EXISTS `_Vue_Offre`*/;
/*!50001 DROP VIEW IF EXISTS `_Vue_Offre`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8mb4 */;
/*!50001 SET character_set_results = utf8mb4 */;
/*!50001 SET collation_connection = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */ /*!50001 VIEW `_Vue_Offre` AS
select `Offres`.`id`                                                                                                AS `id`,
       `Offres`.`untrusteddate`                                                                                     AS `borneInf_DateDepot`,
       `Offres`.`untrusteddate`                                                                                     AS `borneSup_DateDepot`,
       `Offres`.`organisme`                                                                                         AS `organisme`,
       (select `Organisme`.`denomination_org`
        from `Organisme`
        where `Organisme`.`acronyme` = `Offres`.`organisme`)                                                        AS `Nom_organisme`,
       (select `Organisme`.`active`
        from `Organisme`
        where `Organisme`.`acronyme` = `Offres`.`organisme`)                                                        AS `active`,
       `Offres`.`consultation_ref`                                                                                  AS `consultation_ref`,
       (select `consultation`.`reference_utilisateur`
        from `consultation`
        where `consultation`.`reference` = `Offres`.`consultation_ref`
          and `consultation`.`organisme` = `Offres`.`organisme`)                                                    AS `Ref_Agent`,
       (select substr(`consultation`.`objet`, 1, 100)
        from `consultation`
        where `consultation`.`reference` = `Offres`.`consultation_ref`
          and `consultation`.`organisme` = `Offres`.`organisme`)                                                    AS `Objet`,
       `Offres`.`entreprise_id`                                                                                     AS `entreprise_id`,
       `Offres`.`nom_entreprise_inscrit`                                                                            AS `nom_entreprise_inscrit`,
       (select count(distinct `t_candidature_mps`.`id_entreprise`)
        from `t_candidature_mps`
        where `t_candidature_mps`.`ref_consultation` = `Offres`.`consultation_ref`
          and `t_candidature_mps`.`id_offre` <> 'NULL'
          and `t_candidature_mps`.`id_entreprise` = `Offres`.`entreprise_id`)                                       AS `reponse_MPS`,
       (select sum(`fichierEnveloppe`.`taille_fichier`) / 1000000
        from (`Enveloppe`
                 join `fichierEnveloppe`)
        where `Offres`.`id` = `Enveloppe`.`offre_id`
          and `Offres`.`organisme` = `Enveloppe`.`organisme`
          and `Enveloppe`.`id_enveloppe_electro` = `fichierEnveloppe`.`id_enveloppe`
          and `Enveloppe`.`organisme` = `fichierEnveloppe`.`organisme`)                                             AS `Poids_réponse_Mo`,
       (select count(`Enveloppe`.`offre_id`)
        from `Enveloppe`
        where `Offres`.`id` = `Enveloppe`.`offre_id`
          and `Offres`.`organisme` = `Enveloppe`.`organisme`)                                                       AS `Nbr_Env`,
       (select count(distinct `fichierEnveloppe`.`id_fichier`)
        from (`Enveloppe`
                 join `fichierEnveloppe`)
        where `Offres`.`id` = `Enveloppe`.`offre_id`
          and `Enveloppe`.`id_enveloppe_electro` = `fichierEnveloppe`.`id_enveloppe`
          and `Enveloppe`.`organisme` = `fichierEnveloppe`.`organisme`
          and `Offres`.`organisme` = `Enveloppe`.`organisme`)                                                       AS `Nbr_Fichiers`,
       `Offres`.`uid_offre`                                                                                         AS `uid_offre`,
       `Offres`.`statut_offres`                                                                                     AS `statut_offres`,
       `Offres`.`envoi_complet`                                                                                     AS `envoi_complet`,
       `Offres`.`created_at`                                                                                        AS `created_at`,
       `Offres`.`date_depot`                                                                                        AS `date_depot`
from `Offres`
where `Offres`.`envoi_complet` = '1'
  and `Offres`.`statut_offres` <> '99'
  and `Offres`.`organisme` in (select `Organisme`.`acronyme` from `Organisme` where `Organisme`.`active` = 1)
        */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;

--
-- Final view structure for view `_Vue_Offre_sup_a_2015`
--

/*!50001 DROP TABLE IF EXISTS `_Vue_Offre_sup_a_2015`*/;
/*!50001 DROP VIEW IF EXISTS `_Vue_Offre_sup_a_2015`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8 */;
/*!50001 SET character_set_results = utf8 */;
/*!50001 SET collation_connection = utf8_general_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */ /*!50001 VIEW `_Vue_Offre_sup_a_2015` AS
select `Offres`.`untrusteddate`                                                                                     AS `borneInf_DateDepot`,
       `Offres`.`untrusteddate`                                                                                     AS `borneSup_DateDepot`,
       `Offres`.`id`                                                                                                AS `id`,
       `Offres`.`organisme`                                                                                         AS `organisme`,
       (select `Organisme`.`denomination_org`
        from `Organisme`
        where `Organisme`.`acronyme` = `Offres`.`organisme`)                                                        AS `Nom_organisme`,
       (select `Organisme`.`active`
        from `Organisme`
        where `Organisme`.`acronyme` = `Offres`.`organisme`)                                                        AS `active`,
       `Offres`.`consultation_ref`                                                                                  AS `consultation_ref`,
       (select `consultation`.`reference_utilisateur`
        from `consultation`
        where `consultation`.`reference` = `Offres`.`consultation_ref`
          and `consultation`.`organisme` = `Offres`.`organisme`)                                                    AS `Ref_Agent`,
       (select substr(`consultation`.`objet`, 1, 100)
        from `consultation`
        where `consultation`.`reference` = `Offres`.`consultation_ref`
          and `consultation`.`organisme` = `Offres`.`organisme`)                                                    AS `Objet`,
       `Offres`.`entreprise_id`                                                                                     AS `entreprise_id`,
       `Offres`.`nom_entreprise_inscrit`                                                                            AS `nom_entreprise_inscrit`,
       (select count(distinct `t_candidature_mps`.`id_entreprise`)
        from `t_candidature_mps`
        where `t_candidature_mps`.`ref_consultation` = `Offres`.`consultation_ref`
          and `t_candidature_mps`.`id_offre` <> 'NULL'
          and `t_candidature_mps`.`id_entreprise` = `Offres`.`entreprise_id`)                                       AS `reponse_MPS`,
       (select sum(`fichierEnveloppe`.`taille_fichier`) / 1000000
        from (`Enveloppe`
                 join `fichierEnveloppe`)
        where `Offres`.`id` = `Enveloppe`.`offre_id`
          and `Offres`.`organisme` = `Enveloppe`.`organisme`
          and `Enveloppe`.`id_enveloppe_electro` = `fichierEnveloppe`.`id_enveloppe`
          and `Enveloppe`.`organisme` = `fichierEnveloppe`.`organisme`)                                             AS `Poids_réponse_Mo`,
       (select count(`Enveloppe`.`offre_id`)
        from `Enveloppe`
        where `Offres`.`id` = `Enveloppe`.`offre_id`
          and `Offres`.`organisme` = `Enveloppe`.`organisme`)                                                       AS `Nbr_Env`,
       (select count(distinct `fichierEnveloppe`.`id_fichier`)
        from (`Enveloppe`
                 join `fichierEnveloppe`)
        where `Offres`.`id` = `Enveloppe`.`offre_id`
          and `Enveloppe`.`id_enveloppe_electro` = `fichierEnveloppe`.`id_enveloppe`
          and `Enveloppe`.`organisme` = `fichierEnveloppe`.`organisme`
          and `Offres`.`organisme` = `Enveloppe`.`organisme`)                                                       AS `Nbr_Fichiers`,
       `Offres`.`uid_offre`                                                                                         AS `uid_offre`,
       `Offres`.`statut_offres`                                                                                     AS `statut_offres`,
       `Offres`.`envoi_complet`                                                                                     AS `envoi_complet`,
       `Offres`.`created_at`                                                                                        AS `created_at`,
       `Offres`.`date_depot`                                                                                        AS `date_depot`,
       to_days(`Offres`.`untrusteddate`) - to_days(`Offres`.`date_depot`)                                           AS `Horo_Moins_Depot`,
       to_days(`Offres`.`untrusteddate`) - to_days(`Offres`.`created_at`)                                           AS `horo_Moins_Creation`
from `Offres`
where `Offres`.`envoi_complet` = '1'
  and `Offres`.`statut_offres` <> '99'
  and `Offres`.`organisme` in (select `Organisme`.`acronyme` from `Organisme` where `Organisme`.`active` = 1)
  and `Offres`.`untrusteddate` >= '2015-01-01 00:00:00'
        */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;

--
-- Final view structure for view `_Vue_Telechargement`
--

/*!50001 DROP TABLE IF EXISTS `_Vue_Telechargement`*/;
/*!50001 DROP VIEW IF EXISTS `_Vue_Telechargement`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8mb4 */;
/*!50001 SET character_set_results = utf8mb4 */;
/*!50001 SET collation_connection = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */ /*!50001 VIEW `_Vue_Telechargement` AS
select `Telechargement`.`id`                                                                                      AS `id`,
       `Telechargement`.`datetelechargement`                                                                      AS `borneInf_DateTelechargement`,
       `Telechargement`.`datetelechargement`                                                                      AS `borneSup_DateTelechargement`,
       `Telechargement`.`organisme`                                                                               AS `organisme`,
       (select `Organisme`.`denomination_org`
        from `Organisme`
        where `Organisme`.`acronyme` = `Telechargement`.`organisme`)                                              AS `Nom_organisme`,
       (select `Organisme`.`active`
        from `Organisme`
        where `Organisme`.`acronyme` = `Telechargement`.`organisme`)                                              AS `active`,
       `Telechargement`.`consultation_ref`                                                                        AS `consultation_ref`,
       (select `consultation`.`reference_utilisateur`
        from `consultation`
        where `consultation`.`reference` = `Telechargement`.`consultation_ref`
          and `consultation`.`organisme` = `Telechargement`.`organisme`)                                          AS `Ref_Agent`,
       (select substr(`consultation`.`objet`, 1, 100)
        from `consultation`
        where `consultation`.`reference` = `Telechargement`.`consultation_ref`
          and `consultation`.`organisme` = `Telechargement`.`organisme`)                                          AS `Objet`,
       `Telechargement`.`id_entreprise`                                                                           AS `id_entreprise`,
       `Telechargement`.`entreprise`                                                                              AS `entreprise`,
       `Telechargement`.`nom`                                                                                     AS `nom`,
       `Telechargement`.`email`                                                                                   AS `email`,
       `Telechargement`.`codepostal`                                                                              AS `codepostal`,
       `Telechargement`.`ville`                                                                                   AS `ville`,
       `Telechargement`.`pays`                                                                                    AS `pays`
from `Telechargement`
where `Telechargement`.`organisme` in (select `Organisme`.`acronyme` from `Organisme` where `Organisme`.`active` = 1)
        */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;

--
-- Final view structure for view `_Vue_Telechargement_Anonyme`
--

/*!50001 DROP TABLE IF EXISTS `_Vue_Telechargement_Anonyme`*/;
/*!50001 DROP VIEW IF EXISTS `_Vue_Telechargement_Anonyme`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8mb4 */;
/*!50001 SET character_set_results = utf8mb4 */;
/*!50001 SET collation_connection = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */ /*!50001 VIEW `_Vue_Telechargement_Anonyme` AS
select `TelechargementAnonyme`.`id`                                             AS `id`,
       `TelechargementAnonyme`.`datetelechargement`                             AS `borneInf_DateTelechargement`,
       `TelechargementAnonyme`.`datetelechargement`                             AS `borneSup_DateTelechargement`,
       `TelechargementAnonyme`.`organisme`                                      AS `organisme`,
       (select `Organisme`.`denomination_org`
        from `Organisme`
        where `Organisme`.`acronyme` = `TelechargementAnonyme`.`organisme`)     AS `Nom_organisme`,
       (select `Organisme`.`active`
        from `Organisme`
        where `Organisme`.`acronyme` = `TelechargementAnonyme`.`organisme`)     AS `active`,
       `TelechargementAnonyme`.`consultation_ref`                               AS `consultation_ref`,
       (select `consultation`.`reference_utilisateur`
        from `consultation`
        where `consultation`.`reference` = `TelechargementAnonyme`.`consultation_ref`
          and `consultation`.`organisme` = `TelechargementAnonyme`.`organisme`) AS `Ref_Agent`,
       (select substr(`consultation`.`objet`, 1, 100)
        from `consultation`
        where `consultation`.`reference` = `TelechargementAnonyme`.`consultation_ref`
          and `consultation`.`organisme` = `TelechargementAnonyme`.`organisme`) AS `Objet`
from `TelechargementAnonyme`
where `TelechargementAnonyme`.`organisme` in
      (select `Organisme`.`acronyme` from `Organisme` where `Organisme`.`active` = 1)
        */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;

--
-- Final view structure for view `_Vue_Telechargement_Anonyme_sup_a_2015`
--

/*!50001 DROP TABLE IF EXISTS `_Vue_Telechargement_Anonyme_sup_a_2015`*/;
/*!50001 DROP VIEW IF EXISTS `_Vue_Telechargement_Anonyme_sup_a_2015`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8 */;
/*!50001 SET character_set_results = utf8 */;
/*!50001 SET collation_connection = utf8_general_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */ /*!50001 VIEW `_Vue_Telechargement_Anonyme_sup_a_2015` AS
select `TelechargementAnonyme`.`id`                                             AS `id`,
       `TelechargementAnonyme`.`datetelechargement`                             AS `borneInf_DateTelechargement`,
       `TelechargementAnonyme`.`datetelechargement`                             AS `borneSup_DateTelechargement`,
       `TelechargementAnonyme`.`organisme`                                      AS `organisme`,
       (select `Organisme`.`denomination_org`
        from `Organisme`
        where `Organisme`.`acronyme` = `TelechargementAnonyme`.`organisme`)     AS `Nom_organisme`,
       (select `Organisme`.`active`
        from `Organisme`
        where `Organisme`.`acronyme` = `TelechargementAnonyme`.`organisme`)     AS `active`,
       `TelechargementAnonyme`.`consultation_ref`                               AS `consultation_ref`,
       (select `consultation`.`reference_utilisateur`
        from `consultation`
        where `consultation`.`reference` = `TelechargementAnonyme`.`consultation_ref`
          and `consultation`.`organisme` = `TelechargementAnonyme`.`organisme`) AS `Ref_Agent`,
       (select substr(`consultation`.`objet`, 1, 100)
        from `consultation`
        where `consultation`.`reference` = `TelechargementAnonyme`.`consultation_ref`
          and `consultation`.`organisme` = `TelechargementAnonyme`.`organisme`) AS `Objet`
from `TelechargementAnonyme`
where `TelechargementAnonyme`.`organisme` in
      (select `Organisme`.`acronyme` from `Organisme` where `Organisme`.`active` = 1)
  and `TelechargementAnonyme`.`datetelechargement` >= '2015-01-01 00:00:00'
        */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;

--
-- Final view structure for view `_Vue_Telechargement_sup_a_2015`
--

/*!50001 DROP TABLE IF EXISTS `_Vue_Telechargement_sup_a_2015`*/;
/*!50001 DROP VIEW IF EXISTS `_Vue_Telechargement_sup_a_2015`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8 */;
/*!50001 SET character_set_results = utf8 */;
/*!50001 SET collation_connection = utf8_general_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */ /*!50001 VIEW `_Vue_Telechargement_sup_a_2015` AS
select `Telechargement`.`id`                                                                                      AS `id`,
       `Telechargement`.`datetelechargement`                                                                      AS `borneInf_DateTelechargement`,
       `Telechargement`.`datetelechargement`                                                                      AS `borneSup_DateTelechargement`,
       `Telechargement`.`organisme`                                                                               AS `organisme`,
       (select `Organisme`.`denomination_org`
        from `Organisme`
        where `Organisme`.`acronyme` = `Telechargement`.`organisme`)                                              AS `Nom_organisme`,
       (select `Organisme`.`active`
        from `Organisme`
        where `Organisme`.`acronyme` = `Telechargement`.`organisme`)                                              AS `active`,
       `Telechargement`.`consultation_ref`                                                                        AS `consultation_ref`,
       (select `consultation`.`reference_utilisateur`
        from `consultation`
        where `consultation`.`reference` = `Telechargement`.`consultation_ref`
          and `consultation`.`organisme` = `Telechargement`.`organisme`)                                          AS `Ref_Agent`,
       (select substr(`consultation`.`objet`, 1, 100)
        from `consultation`
        where `consultation`.`reference` = `Telechargement`.`consultation_ref`
          and `consultation`.`organisme` = `Telechargement`.`organisme`)                                          AS `Objet`,
       `Telechargement`.`id_entreprise`                                                                           AS `id_entreprise`,
       `Telechargement`.`entreprise`                                                                              AS `entreprise`,
       `Telechargement`.`nom`                                                                                     AS `nom`,
       `Telechargement`.`email`                                                                                   AS `email`,
       `Telechargement`.`codepostal`                                                                              AS `codepostal`,
       `Telechargement`.`ville`                                                                                   AS `ville`,
       `Telechargement`.`pays`                                                                                    AS `pays`
from `Telechargement`
where `Telechargement`.`organisme` in (select `Organisme`.`acronyme` from `Organisme` where `Organisme`.`active` = 1)
  and `Telechargement`.`datetelechargement` >= '2015-01-01 00:00:00'
        */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

COMMIT;