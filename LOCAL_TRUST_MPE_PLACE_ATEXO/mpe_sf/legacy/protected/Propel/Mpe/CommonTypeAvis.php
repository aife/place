<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonTypeAvis;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Traduction;

/**
 * Skeleton subclass for representing a row from the 'TypeAvis' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonTypeAvis extends BaseCommonTypeAvis {


	public function getIntituleAvisTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
		$intituleAvis = "intitule_avis_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$intituleAvis){
            return $this->intitule_avis;
        } else {
            return $this->$intituleAvis;
        }
	}

	/**
     * 
     * Permet de traduire un libellé
     * @param string $idLibelle
     * @param string $libelle: le libellé à traduire
     * @param string $langue
     * @return retourne le libellé traduit
     */
    public function getTraduction($libelle, $langue = null) {
    	$codeLibelle = 'code_'.$libelle;
    	$libelleTraduit = (new Atexo_Traduction())->traduire($this->$codeLibelle, $langue);	
    	if($libelleTraduit) {
    		return $libelleTraduit;
    	} else {
    		return $this->$libelle;	
    	}	
    }
} // CommonTypeAvis
