<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonHistorisationMotDePasseQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Config\Atexo_Config_Exception;

/**
 * Skeleton subclass for performing query and update operations on the 'historisation_mot_de_passe' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonHistorisationMotDePasseQuery extends BaseCommonHistorisationMotDePasseQuery
{
    /**
     * récupère la liste des X dernier mot de passe modifié pour un inscrit
     * @param $idInscrit
     * @param $limit
     * @return array
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function getLastUpdate($idInscrit,$limit){
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->add(CommonHistorisationMotDePassePeer::ID_INSCRIT,$idInscrit);
        $this->addDescendingOrderByColumn(CommonHistorisationMotDePassePeer::DATE_MODIFICATION);
        $this->limit($limit);

        return (array)$this->find($connexion);
    }
}
