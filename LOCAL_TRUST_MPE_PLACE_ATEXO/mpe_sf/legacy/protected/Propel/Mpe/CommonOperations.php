<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\Om\BaseCommonOperations;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_ValeursReferentielles;



/**
 * Skeleton subclass for representing a row from the 'Operations' table.
 *
 * 
 *
 * This class was autogenerated by Propel on:
 *
 * Thu Mar 20 11:49:01 2014
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonOperations extends BaseCommonOperations {

	public function getTypeLibelle(){
		$libelle = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRefAndOrg($this->getType(), Atexo_Config::getParameter("REFERENTIEL_ORG_TYPE_OPERATION"), $this->getAcronyme(), true);
		return $libelle;
	}

} // CommonOperations
