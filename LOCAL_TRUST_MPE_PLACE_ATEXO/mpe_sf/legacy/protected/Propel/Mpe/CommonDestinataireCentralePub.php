<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Service\Atexo\Atexo_Config;

use Application\Propel\Mpe\CommonAnnonceJALPeer;




use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonDestinataireCentralePub;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'destinataire_centrale_pub' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonDestinataireCentralePub extends BaseCommonDestinataireCentralePub {

	public function retreiveDateEnvoi()
 	{
 		if($this->getDateEnvoi()!='0000-00-00 00:00:00' && $this->getDateEnvoi()!='0000-00-00') {
 			return Atexo_Util::iso2frnDateTime($this->getDateEnvoi(),false);
 		}
 		else {
 		    return 'NA';
 		}
 	}

     public function retreiveDatePublication()
 	{
 		if($this->getDatepub()!='0000-00-00 00:00:00' && $this->getDatepub()!='0000-00-00') {
 			return Atexo_Util::iso2frnDate($this->getDatepub());
 		}
 		else {
 		    return 'NA';
 		}
 	}

	public function retreiveDateAr()
 	{
 		if($this->getDateAr()!='') {
 			return Atexo_Util::iso2frnDateTime($this->getDateAr());
 		}
 		else {
 		return Prado::localize('ACCUSE_RECEPTION_NA');
 		}
 	}	

	public function getAnnonceJAL(CommonDestinataireCentralePub $objetDestinataireCentralePub, $connexion = null) 
 	{
 	    if(!$connexion) {
 	        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

 	    }
        return CommonAnnonceJALPeer::retrieveByPK($objetDestinataireCentralePub->getIdAnnonceJal(), $objetDestinataireCentralePub->getOrganisme(), $connexion);
 	}
} // CommonDestinataireCentralePub
