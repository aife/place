<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonCG76PieceJointe;

/**
 * Skeleton subclass for representing a row from the 'CG76_PieceJointe' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonCG76PieceJointe extends BaseCommonCG76PieceJointe {

} // CommonCG76PieceJointe
