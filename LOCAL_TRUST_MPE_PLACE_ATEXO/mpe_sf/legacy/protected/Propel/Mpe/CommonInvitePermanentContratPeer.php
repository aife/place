<?php

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\Om\BaseCommonInvitePermanentContratPeer;


/**
 * Skeleton subclass for performing query and update operations on the 'invite_permanent_contrat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.Application.Propel.Mpe
 */
class CommonInvitePermanentContratPeer extends BaseCommonInvitePermanentContratPeer
{
}
