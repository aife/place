<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use \PDO;
use \Exception;

use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonChorusEchangePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Chorus\Atexo_Chorus_CriteriaVo;

// include base peer class

  // include object class


/**
 * Skeleton subclass for performing query and update operations on the 'Chorus_echange' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonChorusEchangePeer extends BaseCommonChorusEchangePeer {

    /**
     * @param  Atexo_Chorus_CriteriaVo $criteriaVo
     * @param  PropelPDO $conx
     * @return CollectionObject
     */
    public function getChorusEchangesByCriteres($criteriaVo, $conx = null) {
        $params = [];
								try {
            // Au moins l'organisme et la reference consultation sont obligatoire
            if ( $criteriaVo->getOrganisme () && $criteriaVo->getReferenceConsultation () ) {
                $statutsEnvoye = array ( Atexo_Config::getParameter ( "STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_TECHNIQUE" ),
                    Atexo_Config::getParameter ( "STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_FOURNISSEUR" ),
                    Atexo_Config::getParameter ( "STATUT_ECHANGE_CHORUS_ENVOI_PLANIFIE" ),
                    Atexo_Config::getParameter ( "STATUS_ECHANGE_CHORUS_NON_ENVOYER" ),
                    Atexo_Config::getParameter ( "STATUS_ECHANGE_CHORUS_ENVOYER" ),
                    Atexo_Config::getParameter ( "STATUT_TIERS_CHORUS_EN_COURS_VERFICATION" ),
                );
                $statutsEnvoye = implode ( "' , '", $statutsEnvoye );

                // Requete sql
                $sql = <<<REQUETE
                    SELECT DISTINCT CE.id, CE.organisme
                    FROM `Chorus_echange` CE
                    JOIN `t_cons_lot_contrat` CLC ON CE.`organisme` = CLC.`organisme` AND CE.`id_decision` = CLC.`id_contrat_titulaire`
                    WHERE CLC.`consultation_id` = :refConsultation
                    AND CLC.`organisme` = :organisme
                    AND CE.`statutEchange` IN ('$statutsEnvoye');
REQUETE;
                $params[ ":organisme" ] = $criteriaVo->getOrganisme ();
                $params[ ":refConsultation" ] = $criteriaVo->getReferenceConsultation ();

                if ( $conx === null ) {
                    $conx = Propel::getConnection ( Atexo_Config::getParameter ( "COMMON_DB" ) . Atexo_Config::getParameter ( "CONST_READ_ONLY" ) );
                }
                //echo $sql;exit;
                $stmt = $conx->prepare ( $sql );
                $stmt->setFetchMode ( PDO::FETCH_NUM );
                $stmt->execute ( $params );
                if ( $criteriaVo->getCount () ) {
                    return $stmt->rowCount ();
                } else {
                    return $stmt->fetchAll ();
                }
            }
        }catch(Exception $e) {echo $e->getMessage();exit;
            throw $e;
        }
    }
} // CommonChorusEchangePeer
