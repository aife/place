<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonDossierVolumineuxQuery;
use Application\Service\Atexo\Atexo_Config;

class CommonDossierVolumineuxQuery extends BaseCommonDossierVolumineuxQuery
{
    public function getDossierVolumineux($id){
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->add(CommonDossierVolumineuxPeer::ID,$id);
        return $this->findOne($connexion);
    }
    public function getActiveDossiersVolumineuxFromCurrentUser($id,$isAgent){
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));


        if($isAgent){
            $this->add(CommonDossierVolumineuxPeer::ID_AGENT,$id);
        }else{
            $this->add(CommonDossierVolumineuxPeer::ID_INSCRIT,$id);
        }

        $this->add(CommonDossierVolumineuxPeer::ACTIF,1);
        return (array)$this->find($connexion);
    }



}
