<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonConsultation;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CPV;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Traduction;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Parametrage;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Registres\Atexo_Registres_Questions;
use DateTime;
use DOMDocument;
use Exception;
use PDO;
use Prado\Prado;
use Application\Library\Propel\Collection\PropelObjectCollection;


/**
 * Skeleton subclass for representing a row from the 'consultation' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */
class CommonConsultation extends BaseCommonConsultation {

	protected  string $_libelleCategorieConsultation='';
	protected string $_numLotConsultationTransverse= '0';
	protected $_statusConsultation;
    protected $_currentUserReadOnly;
    protected ?bool $_currentUserValidationOnly=false;
    protected bool $_modifHabilitation=false;
    protected bool $_publicationHabilitation=false;
    protected bool $_depotHabilitation=false;
    protected bool $_retraiteHabilitation=false;
    protected bool $_questionHabilitation=false;
    protected bool $_attributionHabilitation=false;
    protected bool $_decisionSuiviSeulHabilitation=false;
    protected bool $_depouillerHabilitation=false;
    protected bool $_validateWithChiffrement=false;
    protected bool $_validateWithoutChiffrement=false;
    protected bool $_redactionDocumentsRedac=false;
    protected bool $_approve=false;
    protected string $_lot='0';
    protected $_decisionLot;
    protected bool $_validationHabilitation=false;
    protected bool $_resultatAnalyseHabilitation=false;
    protected bool $_resultatAnalyseHabilitationDetailConsultation=false;

	protected ?array $_collCommonDce = null;
    protected ?array $_collCommonRg = null;
    protected ?array $_collCommonDateFin = null;
    protected ?array $_collCommonComplement = null;
    protected ?array $_collCommonVisiteLieux = null;
    protected ?array $_collCommonCategorieLot = null;
    protected ?array $_collCommonInterneConsultationSuiviSeul = null;
    protected ?array $_collCommonInterneConsultation = null;

    protected ?array $_collCommonReferentielConsultation = null;

    protected $_abbreviationTypeProcedure;
    protected ?\Application\Service\Atexo\Atexo_CPV $_cpv = null;
    protected $marcheReserve;
    protected $justificationNonAllotissement;

    protected $consultationAvals = [];
    protected ?\Application\Propel\Mpe\CommonConsultation $consultationAmont = null;


	public function addCommonReferentielConsultation(CommonReferentielConsultation $referentielConsultation)
    {
       $this->_collCommonReferentielConsultation[] = $referentielConsultation;
    }

	public function addCommonDCE(CommonDCE $dce)
    {
       $this->_collCommonDce[] = $dce;
    }

	public function addCommonRG(CommonRG $rg)
    {
       $this->_collCommonRg[] = $rg;
    }

	public function addCommonDATEFIN(CommonDATEFIN $dateFin)
    {
       $this->_collCommonDateFin[] = $dateFin;
    }

	public function addCommonComplement(CommonComplement $complement)
    {
       $this->_collCommonComplement[] = $complement;
    }

	public function addCommonVisiteLieux(CommonVisiteLieux $visiteLieux)
    {
       $this->_collCommonVisiteLieux[] = $visiteLieux;
    }

	public function addCommonCategorieLot(CommonCategorieLot $lot)
    {
       $this->_collCommonCategorieLot[] = $lot;
    }

	public function addCommonInterneConsultationSuiviSeul(CommonInterneConsultationSuiviSeul $interneSuiviSeul)
    {
       $this->_collCommonInterneConsultationSuiviSeul[] = $interneSuiviSeul;
    }

	public function addCommonInterneConsultation(CommonInterneConsultation $interne)
    {
       $this->_collCommonInterneConsultation[] = $interne;
    }

	public function getCommonCategorieLots($criteria = null, ?PropelPDO $con = null)
    {
       return $this->_collCommonCategorieLot;
    }

	public function getNumLotConsultationTransverse()
    {
        return $this->_numLotConsultationTransverse;
    }

    public function setNumLotConsultationTransverse($value)
    {
        $this->_numLotConsultationTransverse = $value;
    }

    public function setAbbreviationTypeProcedure($value)
    {
    	$this->_abbreviationTypeProcedure = $value;
    }
	public function getAbbreviationTypeProcedure()
    {
    	return $this->_abbreviationTypeProcedure;
    }

	public function setCpv(Atexo_CPV $value)
    {
    	$this->_cpv = $value;
    }
	public function getCpv()
    {
    	return $this->_cpv;
    }

    public function getLibelleCategorieConsultation()
    {
        if($this->_libelleCategorieConsultation) {
        	return $this->_libelleCategorieConsultation;
        }
        $categorie = Atexo_Consultation_Category::retrieveCategorie($this->getCategorie(),true);
        if($categorie) {
            return $categorie->getLibelle();
        } else {
            return "";
        }
    }

    public function setLibelleCategorieConsultation($libelleCategorie)
    {
    	$this->_libelleCategorieConsultation=$libelleCategorie;
    }

    public function getDenominationOrganisme($langue=null, $withCPVille=false)
    {
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($this->getOrganisme());
        if($organisme) {
			$denominationOrganisme = "";
			$villeOrganisme = "";
			$cpOrganisme = "";
			$result = "";

            $getDenominationOrg="getDenominationOrg".Atexo_Languages::getLanguageAbbreviation($langue);

            if($langue==Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')
                   || !method_exists($organisme, $getDenominationOrg) || !$organisme->$getDenominationOrg()) {
					$denominationOrganisme = $organisme->getDenominationOrg();
			}
			else {
					$denominationOrganisme = $organisme->$getDenominationOrg();
 	 		}

			if ($withCPVille) {
				$getVilleOrg = "getVille".Atexo_Languages::getLanguageAbbreviation($langue);

				if($langue==Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')
                   || !method_exists($organisme, $getVilleOrg) || !$organisme->$getVilleOrg()) {
					$villeOrganisme = $organisme->getVille();
				}
				else {
					$villeOrganisme = $organisme->$getVilleOrg();
				}

				$result = $denominationOrganisme." (".$organisme->getCp()." - ".$villeOrganisme.")";
			}
			else {
				$result = $denominationOrganisme;
			}

			return $result;
        } else {
            return "";
        }
    }

    public function getTypeProcedure()
    {
        $typeProcedureQuery = new CommonTypeProcedureQuery();
        $typeProcedure = $typeProcedureQuery->getTypeProcedureById($this->getIdTypeProcedure());

        if($typeProcedure) {
            return $typeProcedure;
        }
        return new CommonTypeProcedure();
    }
    public function getTypeProcedureOrg($organisme = null)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeProcedureOrganismePeer::ID_TYPE_PROCEDURE,$this->getIdTypeProcedureOrg());
        if ($organisme !== null) {
            $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
        }
        $typeProcedure = CommonTypeProcedureOrganismePeer::doSelectOne($c,$connexion);
        if($typeProcedure) {
            return $typeProcedure;
        }
    }

    public function getLibelleTypeAnnonce()
    {
        $typeAvis=(new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($this->getIdTypeAvis());
        if ($typeAvis) {
            return $typeAvis->getIntituleAvis();
        } else {
            return "";
        }
    }

    public function getLibelleLieuxExecution($langue=null)
    {
        $arIds =  explode(",",$this->getLieuExecution());
        return Atexo_Geolocalisation_GeolocalisationN2::getLibelleLieuExecution($arIds, $langue);
    }

    /**
     * Affiche le contenu d'une consultation
     */
    public function toString()
    {
        return "ID = ".$this->getId()
        . " Référence = ".$this->getReferenceUtilisateur()."<br/>";
    }
     /**
     * Retourne un tableau d'objet CategorieLot de la consultation appellante
     */
    public function getAllLots($connexionCom=null,$organisme=null,$consultationId=null, $count = false)
	{
	    if(!$connexionCom) {
			$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		}
		$c=new Criteria();

		if($consultationId && $organisme)
		{
		    $c->add(CommonCategorieLotPeer::CONSULTATION_ID,$consultationId);
		    $c->add(CommonCategorieLotPeer::ORGANISME,$organisme);
		}else
		{
		    $c->add(CommonCategorieLotPeer::CONSULTATION_ID,$this->getId());
		    $c->add(CommonCategorieLotPeer::ORGANISME,$this->getOrganisme());
		}
        if($count) {
            return CommonCategorieLotPeer::doCount($c,$connexionCom);
        }
		$categorieLots=CommonCategorieLotPeer::doSelect($c,$connexionCom);
		if ($categorieLots) {
			return $categorieLots;
		} else {
			return  array();
		}
	}


	public function getInvitationTransvese()
	{
    	$connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c =new Criteria();
        $c->add(CommonInvitationConsultationTransversePeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonInvitationConsultationTransversePeer::ORGANISME_EMETTEUR,$this->getOrganisme());
        $inv = CommonInvitationConsultationTransversePeer::doSelectOne($c,$connexionCommon);
        if($inv) {
            return $inv;
        }
        else {
            return false;
        }
	}

	public function getOrganismeInvite()
	{
        $org = Atexo_Organismes::retrieveOrganismeByAcronyme($this->getOrganisme());
        if ($org) {
            return $org->getSigle();
        }
        else {
            return $org->getDenominationOrg();
        }
	}
    /**
     * retourne la valeur du champ type de procedure de la consultation
     *
     */
    public function getTypePublicite()
    {
        return $this->getTypeAcces();
    }

	/**
     * retourne le titre de la consultation avec l'option traduction
     *
     */
    public function getTitreTraduit()
    {
        $langue = Atexo_CurrentUser::readFromSession("lang");
 	    $titre = "titre_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$titre){
            return $this->titre;
        } else {
            return $this->$titre;
        }
    }

	/**
     * Renvoie la traduction de l'intitulé dans la langue spécifiée
     * @param string $langue: langue de naviguation
     */
 	public function setDetailConsultationTraduit($langue, $libelle)
    {
        if(!$langue){
        	$langue = Atexo_CurrentUser::readFromSession("lang");
        }
    	if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
   			$this->detail_consultation = $libelle;
   	    }else {
   	    	$titre = "detail_consultation_".$langue;
   	    	$this->$titre = $libelle;
   	    }
        /*$idTraduction = Atexo_Traduction::setTraduction($this->getIdTr(), $langue, $libelle);
        $this->setIdTrIntitule($idTraduction);*/
    }
    /**
     * Renvoie la traduction de l'intitulé dans la langue spécifiée
     * @param string $langue: langue de naviguation
     */
 	public function getIntituleTraduit($langue = null)
    {
        if (!$langue) {
        	$langue = Atexo_CurrentUser::readFromSession("lang");
        }
        if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
        	$traduction = $this->getIntitule();
        }else {
            $traduction = Atexo_Traduction::getTraduction($this->getIdTrIntitule(), $langue);
            if (!$traduction) {
            	$traduction = $this->getIntitule();
            }
    	}
    	return $traduction;
    }

    /**
	 * Enregistre la traduction de l'intitulé
	 * @param string $langue: langue de naviguation
	 * @param string $libelle: le libellé de la traduction
	 */
	public function setIntituleTraduit($langue, $libelle)
    {
        if(!$langue){
        	$langue = Atexo_CurrentUser::readFromSession("lang");
        }
    	if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
   			$this->setIntitule($libelle);
   	    }
        $idTraduction = (new Atexo_Traduction())->setTraduction($this->getIdTrIntitule(), $langue, $libelle);
        $this->setIdTrIntitule($idTraduction);
    }

    public function getResumeTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $resume = "resume_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$resume){
            return $this->resume;
        } else {
            return $this->$resume;
        }
	}

	/**
     * Renvoie la traduction de l'objet dans la langue spécifiée
     * @param string $langue: langue de naviguation
    */
 	public function getObjetTraduit($langue = null)
	{
        if (!$langue) {
        	$langue = Atexo_CurrentUser::readFromSession("lang");
        }
        if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
        	$traduction = $this->getObjet();
        }else {
            $traduction = Atexo_Traduction::getTraduction($this->getIdTrObjet(), $langue);
            if (!$traduction) {
            	$traduction = $this->getObjet();
            }
    	}
    	return $traduction;
	}

	/**
	 * Enregistre la traduction de l'objet
	 * @param string $langue: langue de naviguation
	 * @param string $libelle: le libellé de la traduction
	 */
    public function setObjetTraduit($langue, $libelle)
    {
        if(!$langue){
        	$langue = Atexo_CurrentUser::readFromSession("lang");
        }
    	if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
   			$this->setObjet($libelle);
   	    }
        $idTraduction = (new Atexo_Traduction())->setTraduction($this->getIdTrObjet(), $langue, $libelle);
        $this->setIdTrObjet($idTraduction);
    }

	public function getAdresseRetraisDossiersTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $adresse_retrais_dossiers = "adresse_retrais_dossiers_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$adresse_retrais_dossiers){
            return $this->adresse_retrais_dossiers;
        } else {
            return $this->$adresse_retrais_dossiers;
        }
	}

	public function getAdresseDepotOffresTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $adresse_depot_offres = "adresse_depot_offres_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$adresse_depot_offres){
            return $this->adresse_depot_offres;
        } else {
            return $this->$adresse_depot_offres;
        }
	}
	public function getLieuOuverturePlisTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $lieu_ouverture_plis = "lieu_ouverture_plis_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$lieu_ouverture_plis){
            return $this->lieu_ouverture_plis;
        } else {
            return $this->$lieu_ouverture_plis;
        }
	}

	public function getAddEchantillionTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $add_echantillion = "add_echantillion_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$add_echantillion){
            return $this->add_echantillion;
        } else {
            return $this->$add_echantillion;
        }
	}

	public function getAddReunionTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $add_reunion = "add_reunion_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$add_reunion){
            return $this->add_reunion;
        } else {
            return $this->$add_reunion;
        }
	}

 	public function getDetailConsultationTraduit($langue=null)
	{
	   if(!$langue) {
	   	$langue = Atexo_CurrentUser::readFromSession("lang");
	   }
	   $detail_consultation = "detail_consultation_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$detail_consultation){
            return $this->detail_consultation;
        } else {
            return $this->$detail_consultation;
        }
	}

	public function getPiecesDossierAdminTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $pieces_dossier_admin = "pieces_dossier_admin_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$pieces_dossier_admin){
            return $this->pieces_dossier_admin;
        } else {
            return $this->$pieces_dossier_admin;
        }
	}

	public function getPiecesDossierTechTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $pieces_dossier_tech = "pieces_dossier_tech_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$pieces_dossier_tech){
            return $this->pieces_dossier_tech;
        } else {
            return $this->$pieces_dossier_tech;
        }
	}

	public function getPiecesDossierAdditifTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $pieces_dossier_additif = "pieces_dossier_additif_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$pieces_dossier_additif){
            return $this->pieces_dossier_additif;
        } else {
            return $this->$pieces_dossier_additif;
        }
	}
	/*
	 * Retourne la liste des libellé des codes nuts passés en parametre
	 * @params: codes nuts et la langue de la session
	 * @return: tableau de libellés
	 */
	public function getLibelleCodesNuts($codesNuts,$langue) {

		$atexoRef=new Atexo_Ref();
		$atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));

		if ($langue == "su"){
			$langue="sv";
		}

		if($atexoRef instanceof Atexo_Ref){
		$atexoRef->setLocale($langue);
		$arrayConfigXml=$atexoRef->getArrayConfigXml();
		if($arrayConfigXml) {
			$dataRefFile="./".$arrayConfigXml['cheminData'].$arrayConfigXml['fichierDonnee'].$langue.".csv";
		}

		$consulationSecondaires=array();
			if($codesNuts) {
				$arrayCodesNuts=explode('#',$codesNuts);
				if(is_array($arrayCodesNuts) && $arrayCodesNuts) {
					$i=0;
					foreach($arrayCodesNuts as $nut) {
						if($nut != $arrayCodesNuts[0] && $nut) {
							if(is_file($dataRefFile)) {
								$consulationSecondaires[$i]=$atexoRef->getLibelleRef($nut,$dataRefFile);
							}
							$i++;
						}
					}
				}
			}
			return $consulationSecondaires;
		}

	}
	/*
	 * Retourne le type de procedure traduit dans la langue de naviguation
	 */
	public function getLibelleTypeProcedure()
    {
        return (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($this->getIdTypeProcedure(), true, null, Atexo_CurrentUser::readFromSession("lang"));
    }

	public function getLibelleTypeProcedureOrg()
    {
         return (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($this->getIdTypeProcedureOrg(), false, $this->getOrganisme());
    }

    /*
     * Retourne la categorie de la consultation traduite dans la langue de naviguation
     */
	public function getLibelleCategorieConsultationTraduit()
    {
        if($this->_libelleCategorieConsultation) {
        	return $this->_libelleCategorieConsultation;
        }

        $langue = Atexo_CurrentUser::readFromSession("lang");
        $getLibelle = "getLibelle".Atexo_Languages::getLanguageAbbreviation($langue);
        $categorie = Atexo_Consultation_Category::retrieveCategorie($this->getCategorie(),true);
        if($categorie) {
            	if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
            		|| !$getLibelle){
            		return $categorie->getLibelle();
            	} else {
            		return $categorie->$getLibelle();
            	}
        } else {
            return "";
        }
    }

    /**
     * Retourne le nombre de questions papier
     * Fonction temporaire en attendant d'ajouter un compteur de questions papier dans la table consultation
     */
    public function getNombreQuestionPapierDces($connexion=null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        return (new Atexo_Registres_Questions())->getNbQuestionsByConsultation($this->getId(), $this->getOrganisme(), Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER'), $connexion);
    }

    /**
     * Retourne le nombre de questions electronique
     * Fonction temporaire en attendant d'ajouter un compteur de questions electronique dans la table consultation
     */
    public function getNombreQuestionElectroniqueDces($connexionCommom=null)
    {
        $connexionCommom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        return (new Atexo_Registres_Questions())->getNbQuestionsByConsultation($this->getId(), $this->getOrganisme(), Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE'), $connexionCommom);
    }

    public function getStatusConsultation()
    {
        return $this->_statusConsultation;
    }

    public function setStatusConsultation($statusConsultation)
    {
        $this->_statusConsultation=$statusConsultation;
    }

    public function getCurrentUserReadOnly()
    {
        return $this->_currentUserReadOnly;
    }

    public function setCurrentUserReadOnly($currentUserReadOnly)
    {
        $this->_currentUserReadOnly=$currentUserReadOnly;
    }

    public function getCurrentUserValidationOnly()
    {
        return $this->_currentUserValidationOnly;
    }

    public function setCurrentUserValidationOnly($value)
    {
        $this->_currentUserValidationOnly=$value;
    }


    public function getModifHabilitation()
    {
        return $this->_modifHabilitation;
    }

    public function setModifHabilitation($modifHabilitation)
    {
        $this->_modifHabilitation=$modifHabilitation;
    }

    public function getPublicationHabilitation()
    {
        return $this->_publicationHabilitation;
    }

    public function setPublicationHabilitation($publicationHabilitation)
    {
        $this->_publicationHabilitation=$publicationHabilitation;
    }

    public function getDepotHabilitation()
    {
        return $this->_depotHabilitation;
    }

    public function setDepotHabilitation($depotHabilitation)
    {
        $this->_depotHabilitation=$depotHabilitation;
    }

    public function getRetraiteHabilitation()
    {
        return $this->_retraiteHabilitation;
    }

    public function setRetraiteHabilitation($retraiteHabilitation)
    {
        $this->_retraiteHabilitation=$retraiteHabilitation;
    }

    public function getQuestionHabilitation()
    {
        return $this->_questionHabilitation;
    }

    public function setQuestionHabilitation($questionHabilitation)
    {
        $this->_questionHabilitation=$questionHabilitation;
    }

    public function getAttributionHabilitation()
    {
        return $this->_attributionHabilitation;
    }

    public function setAttributionHabilitation($attributionHabilitation)
    {
        $this->_attributionHabilitation=$attributionHabilitation;
    }
 	public function getDecisionSuiviSeulHabilitation()
    {
        return $this->_decisionSuiviSeulHabilitation;
    }

    public function setDecisionSuiviSeulHabilitation($decisionSuiviSeulHabilitation)
    {
        $this->_decisionSuiviSeulHabilitation=$decisionSuiviSeulHabilitation;
    }
    public function getValidateWithChiffrement()
    {
        return $this->_validateWithChiffrement;
    }

    public function setValidateWithChiffrement($validateWithChiffrement)
    {
        $this->_validateWithChiffrement=$validateWithChiffrement;
    }

    public function getValidateWithoutChiffrement()
    {
        return $this->_validateWithoutChiffrement;
    }

    public function setValidateWithoutChiffrement($validateWithoutChiffrement)
    {
        $this->_validateWithoutChiffrement=$validateWithoutChiffrement;
    }

    public function getApprove()
    {
        return $this->_approve;
    }

    public function setApprove($approve)
    {
        $this->_approve=$approve;
    }


    /**
     * Retourne la reference utilisateur sans les suffixes type de procedure et type d'annonce
     *
     * @return string
     */
    public function getRealReferenceUtilisateur()
    {
        return Atexo_Consultation::explodeUserReference($this->getReferenceUtilisateur());
    }

    public function getAllPoles()
    {
        return Atexo_EntityPurchase::getAllParents($this->getServiceId(),$this->getOrganisme());
    }

    public function getDepouillerHabilitation()
    {
        return $this->_depouillerHabilitation;
    }

    public function setDepouillerHabilitation($depouillerHabilitation)
    {
        $this->_depouillerHabilitation=$depouillerHabilitation;
    }

    /**
     * retourne abreviation du type de procedure de la consultation en cours
     */
    public function getAbreviationTypeProcedure()
    {
        return (new Atexo_Consultation_ProcedureType())->retrieveAcronymeTypeProcedure($this->getIdTypeProcedureOrg(), false);
    }


    /**
     * retourne l'acronyme du type de procedure de la consultation en cours
     */
    public function getAcronymeTypeProcedure()
    {
        return (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($this->getIdTypeProcedureOrg(), false);
    }


	/**
     * Retourne la premiere annonce publiee pour la consultation en cours.
     * Attention cette fonction retrounera un objet Annonce vide si la consultation n'a aucune Annonce.
     * @param : Optionel : la connexion a utiliser (par defaut utilisera la connexion vers l'organisme de l'agent connecte)
     * @return : Objet Annonce
     */
    public function getFirstAnnoncePubliee($connexion=null)
    {
        if(!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c=new Criteria();
        $c->add(CommonAnnonceBoampPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonAnnonceBoampPeer::ORGANISME,$this->getOrganisme());
        $c->add(CommonAnnonceBoampPeer::DATEPUB,'0000-00-00 00:00:00',Criteria::NOT_EQUAL);
        $c->add(CommonAnnonceBoampPeer::DATEPUB,'0000-00-00',Criteria::NOT_EQUAL);
        $c->add(CommonAnnonceBoampPeer::STATUT_DESTINATAIRE ,Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU'));
        $c->addAscendingOrderByColumn(CommonAnnonceBoampPeer::DATEPUB);
        $annonces= CommonAnnonceBoampPeer::doSelect($c, $connexion);
        if ($annonces) {
            return $annonces[0];
        } else {
            return  new CommonAnnonceBoamp();
        }
    }


	/**
     * Retourne la premiere annonce publiee pour la consultation en cours.
     * Attention cette fonction retrounera un objet Annonce vide si la consultation n'a aucune Annonce.
     * @param : Optionel : la connexion a utiliser (par defaut utilisera la connexion vers l'organisme de l'agent connecte)
     * @return : Objet Annonce
     */
    public function getFirstAnnonceEnvoyee($connexion=null)
    {
     if(!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c=new Criteria();
        $c->add(CommonAnnonceBoampPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonAnnonceBoampPeer::ORGANISME,$this->getOrganisme());
        $c->add(CommonAnnonceBoampPeer::DATE_ENVOI,'0000-00-00 00:00:00',Criteria::NOT_EQUAL);
        $c->add(CommonAnnonceBoampPeer::DATE_ENVOI,'0000-00-00',Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(CommonAnnonceBoampPeer::DATE_ENVOI);
        $annonces= CommonAnnonceBoampPeer::doSelect($c, $connexion);
        if ($annonces) {
            return $annonces[0];
        } else {
            return  new CommonAnnonceBoamp();
        }
    }
    /**
     * Retourne le nombre de Telechargement electronique
     * Fonction temporaire en attendant d'ajouter un compteur de Telechargement electronique  dans la table consultation
     */
    public function getNombreTelecharegementElectronique($organisme=null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c=new Criteria();
        $c->add(CommonTelechargementPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonTelechargementPeer::ORGANISME,$this->getOrganisme());
        return CommonTelechargementPeer::doCount($c, $connexion);
    }

    /**
     * Retourne le nombre de Telechargement papier
     * Fonction temporaire en attendant d'ajouter un compteur de Telechargement papier  dans la table consultation
     */
    public function getNombreTelecharegementPapier($organisme=null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c=new Criteria();
        $c->add(CommonRetraitPapierPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonRetraitPapierPeer::ORGANISME,$this->getOrganisme());
        return CommonRetraitPapierPeer::doCount($c, $connexion);
    }

    /**
     * Retourne le nombre d'offres electronique
     * Fonction temporaire en attendant d'ajouter un compteur d'offres electronique  dans la table consultation
     */
    public function getNombreOffreElectronique($connexionOrg=null)
    {
         return $this->getNbOffres();
    }

    /**
     * récupère le nombre d'offre de la consultation courant
     * @param null $connexion
     * @return int
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function getNbOffres($connexion=null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c=new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonOffresPeer::ORGANISME,$this->getOrganisme());
        $c->add(CommonOffresPeer::STATUT_OFFRES,  Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);
        return $nbOffresElectronique = CommonOffresPeer::doCount($c, $connexion);


    }


    public function getOffres($connexion=null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c=new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonOffresPeer::ORGANISME,$this->getOrganisme());
        $c->add(CommonOffresPeer::STATUT_OFFRES,  Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);
        $offresElectronique= CommonOffresPeer::doSelect($c, $connexion);
        if ($offresElectronique) {
            return $offresElectronique;
        } else {
            return array();
        }
    }



    /**
     * Retourne le nombre de pli electroniques
     */
    public function getNombrePlisElectronique($connexionOrg=null)
    {
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c=new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonOffresPeer::ORGANISME, $this->getOrganisme());
		$offresElectronique= CommonOffresPeer::doSelect($c, $connexion);
        $res = 0;
        if ($offresElectronique) {
            foreach ($offresElectronique as $offre) {
                $res += $offre->countCommonEnveloppes(null, true, $connexion);
            }
            return $res;
        } else {
            return 0;
        }
    }

    public function getSizePlis($connexion=null)
    {
        try {
        if(!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $tailleTotale = 0;
        $offresElectronique=$this->getOffres();
        if ($offresElectronique) {
            foreach ($offresElectronique as $offre) {
                $xml = $offre->getXmlString();
                if ($xml) {
                    $doc = (new DOMDocument())->loadXML($xml);
                    if(is_object($doc)) {
                       $fichiers = $doc->getElementsByTagName("Fichier");
                       if ($fichiers) {
                           foreach ($fichiers as $fichier) {
                               $tailleTotale += $fichier->getAttribute("tailleEnClair");
                           }
                        }
                    }
                } else {
                    return 0;
                }
            }
        }
        } catch(Exception) {
        	$tailleTotale=1024;
        }
        return Atexo_Util::arrondirSizeFile($tailleTotale / 1024);
    }

    /**
     * Retourne le nombre d'offres papier
     * Fonction temporaire en attendant d'ajouter un compteur d'offres papier  dans la table consultation
     */
    public function getNombreOffrePapier($organisme=null)
    {
        return $this->getNbOffrePapiers();
    }

 	public function getOffrePapiers()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c=new Criteria();
        $c->add(CommonOffrePapierPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonOffrePapierPeer::ORGANISME,$this->getOrganisme());
        $offresPapier= CommonOffrePapierPeer::doSelect($c, $connexion);
        if ($offresPapier) {
            return $offresPapier;
        } else {
            return array();
        }
    }

    public function getNbOffrePapiers()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c=new Criteria();
        $c->add(CommonOffrePapierPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonOffrePapierPeer::ORGANISME,$this->getOrganisme());
        return CommonOffrePapierPeer::doCount($c, $connexion);
    }

    public function getLibelleService()
    {
        $service=Atexo_EntityPurchase::retrieveEntityById($this->getServiceId(),$this->getOrganisme());
        if ($service) {
            return $service->getService();
        }
        return "";
    }


    /**
     * Cette fonction retourne l'attribut $_lot qui est egale a vide (sans possibilite de faire un set)
     * L'objet Consultation peut alors etre utilise comme l'objet CategorieLot
     */
    public function getLot()
    {
        return $this->_lot;
    }

    /**
     * Cette fonction retourne le titre de la consultation
     * L'objet Consultation peut alors etre utilise comme l'objet CategorieLot
     */
    public function getDescription()
    {
        return $this->getIntitule();
    }

    /**
     * Cette fonction retourne le titre de la consultation
     * L'objet Consultation peut alors etre utilise comme l'objet CategorieLot
     */
    public function getConsultationId()
    {
        return $this->getReference();
    }

    public function getDecisionLot()
    {
        return $this->_decisionLot;
    }

    public function setDecisionLot($decisionLot)
    {
        $this->_decisionLot=$decisionLot;
    }

    public function setValidationHabilitation($habilite)
    {
        $this->_validationHabilitation=$habilite;
    }

    public function  getValidationHabilitation()
    {
        //Conditions pour que la consultation puisse être valider
		//Si consu avec Validation intermediaire
		// FIXME /!\ Attention : chercher l'habiulitation de l'agent passé dans le criteriaVO et non le Atexo_CurrentUser
		if($this->getDatefin()<date('Y-m-d H:i:s') || $this->getEtatEnAttenteValidation() == "0") {
			return false;
		} else if($this->getIdRegleValidation()==Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_5')
        ) {
            if((!$this->getDateValidationIntermediaire() || substr($this->getDateValidationIntermediaire(),0,10)=='0000-00-00')
                && Atexo_CurrentUser::hasHabilitation('ValidationIntermediaire')
            ) {
                return true;
            }

            if((!$this->getDatevalidation() || substr($this->getDatevalidation(),0,10)=='0000-00-00')
                && $this->getDateValidationIntermediaire()
                && substr($this->getDateValidationIntermediaire(),0,10)!='0000-00-00'
                && Atexo_CurrentUser::hasHabilitation('ValidationFinale')
                ) {
                return true;
            }

        } else if($this->getIdRegleValidation()==Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_4')
		   ) {
				if((!$this->getDateValidationIntermediaire() || substr($this->getDateValidationIntermediaire(),0,10)=='0000-00-00')
				   && Atexo_CurrentUser::hasHabilitation('ValidationIntermediaire')
				   ) {
					return true;
				}

				if((!$this->getDatevalidation() || substr($this->getDatevalidation(),0,10)=='0000-00-00')
				   && $this->getDateValidationIntermediaire()
				   && substr($this->getDateValidationIntermediaire(),0,10)!='0000-00-00'
				   && Atexo_CurrentUser::hasHabilitation('ValidationFinale')
				   && (new Atexo_EntityPurchase())->isEntityAbleToValidate($this->getServiceValidation(), Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism())) {
					return true;
			   	}

	   } else if($this->getIdRegleValidation()==Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_3')) {
		   		if((!$this->getDatevalidation() || substr($this->getDatevalidation(),0,10)=='0000-00-00')
		   		   && Atexo_CurrentUser::hasHabilitation('ValidationFinale')
		   		   && (new Atexo_EntityPurchase())->isEntityAbleToValidate($this->getServiceValidation(), Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism())) {
					return true;
		   		}
	   } else if($this->getIdRegleValidation()==Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_2')) {
            if((!$this->getDatevalidation() || substr($this->getDatevalidation(),0,10)=='0000-00-00')
                && Atexo_CurrentUser::hasHabilitation('ValidationFinale')
                ) {
                return true;
            }
        } else {
	   		    if((!$this->getDatevalidation() || substr($this->getDatevalidation(),0,10)=='0000-00-00')
					&& Atexo_CurrentUser::hasHabilitation('ValidationSimple')
		   			) {
					return true;
		   		}
	   }

        //return $this->_validationHabilitation;
    }

    public function setResultatAnalyseHabilitation($habilite)
    {
        $this->_resultatAnalyseHabilitation=$habilite;
    }

    public function  getResultatAnalyseHabilitation()
    {
        return $this->_resultatAnalyseHabilitation;
    }


    public function getDescriptionTraduite()
    {
        return $this->getIntituleTraduit();
    }

	public function getAcronymeTypeProcedureTraduit()
    {
        $langue = Atexo_CurrentUser::readFromSession("lang");
        return (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($this->getIdTypeProcedureOrg(), false, null, $langue);
    }

    public function getReferenceUtilisateur($escapeSpecialChars=false)
    {
    	if($escapeSpecialChars) {
    		return str_replace("/","_", $this->reference_utilisateur);
    	}

    	return $this->reference_utilisateur;
    }

    public function getResultatAnalyseHabilitationDetailConsultation()
    {
    	return $this->_resultatAnalyseHabilitationDetailConsultation;
    }

    public function setResultatAnalyseHabilitationDetailConsultation($value)
    {
    	$this->_resultatAnalyseHabilitationDetailConsultation=$value;
    }

	/**
     * verifie si l'agent connecte a l'habilitation: MODIFIER UNE CONSULTATION APRES VALIDATION
     * @return boolean true si l'agent a l'habilitation false sinon
     */
    public function hasHabilitationModifierApresValidation()
    {
        $typeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureById($this->getIdTypeProcedureOrg(), Atexo_CurrentUser::getCurrentOrganism());
        if($typeProcedure) {
            if($typeProcedure->getMapa() == Atexo_Config::getParameter('IS_MAPA')) {
                if(($typeProcedure->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_INF_90'))
                    && (Atexo_CurrentUser::hasHabilitation('ModifierConsultationMapaInferieurMontantApresValidation'))){
                    return true;
                } else if(($typeProcedure->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_SUP_90'))
                    && (Atexo_CurrentUser::hasHabilitation('ModifierConsultationMapaSuperieurMontantApresValidation'))){
                       return true;
                    } else {
                        return false;
                    }
            } else {
                if(Atexo_CurrentUser::hasHabilitation('ModifierConsultationProceduresFormaliseesApresValidation')) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }

    }

	 /**
     * Retourne un tableau d'objet CategorieLot de la consultation appellante avec les lots qui accepte la variante
     */
    public function getAllLotsWithVariante($connexion=null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $c=new Criteria();
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonCategorieLotPeer::ORGANISME,$this->getOrganisme());
        $c->add(CommonCategorieLotPeer::VARIANTES,"1");
        $c->addAscendingOrderByColumn(CommonCategorieLotPeer::LOT);
        $categorieLots= CommonCategorieLotPeer::doSelect($c, $connexion);
        if ($categorieLots) {
            return $categorieLots;
        } else {
            return  array();
        }
    }


	public function getDateDecisionAnnulationFr()
	{
		return Atexo_Util::iso2frnDate($this->getDateDecisionAnnulation());
	}
	/*
	 *  Verifie si les conditions de téléchargement du DIC sont respectées pour la consultation
	 * @return: true si oui, false sinon
	 */
	public function isDicTelechargeable()
	{
		if($this->id_etat_consultation <= Atexo_Config::getParameter('STATUS_A_ARCHIVER')){
			return true;
		}

		return false;
	}

	/*
	 * Verifie si la consultation est un MAPA INF 90k euro
	 * @return: true si MAPA Inf, false sinon
	 * @param: id_type_procedure
	 */
	public function isMapaInf($idTypeProcedure)
	{
	    $typeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureById($idTypeProcedure, Atexo_CurrentUser::getCurrentOrganism());
        if($typeProcedure) {
            if($typeProcedure->getMapa() == Atexo_Config::getParameter('IS_MAPA') && ($typeProcedure->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_INF_90'))) {
				return true;
            }
        }
        return false;
    }

/*
 *  Verifie si les conditions de téléchargement du DAC sont respectées pour la consultation
 * @return: true si oui, false sinon
 */
	public function isDacTelechargeable()
	{
		$dateArchivage = Atexo_Util::iso2frnDate(substr($this->date_archivage,0,10));

		if($this->id_etat_consultation == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE') && $dateArchivage && Atexo_Util::diffJours(date('d/m/Y'),$dateArchivage) < Atexo_Config::getParameter('DELAI_ACCESSIBILITE_DIC_APRES_ARCHIVAGE')) {
			return true;
		}
		return false;
	}

	public function serviceConsHasServiceArchivageDefinitive()
	{
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$serviceId = $this->getServiceId();
		if($serviceId){
			$c = new Criteria();
	        $c->add(CommonServicePeer::ID, $serviceId);
	        $c->add(CommonServicePeer::ORGANISME, $this->getOrganisme());
	        $service = CommonServicePeer::doSelectOne($c, $connexion);
	        if(!$service || !$service->getNomServiceArchiveur() || !$service->getIdentifiantServiceArchiveur()){
	        	return true;
	        }
		}
        return false;
	}

	public function saveConsultation($connexion){

		$dce = null;
		$rg = null;
		$this->setDateModification(date('Y-m-d H:i:s'));
		$this->setDateFinUnix(strtotime($this->getDatefin()." + ".$this->getPoursuivreAffichage()." ".$this->getPoursuivreAffichageUnite()));
		$this->setEnvOffreTechnique(0);
        if(empty($this->getUuid())) {
            $this->setUuid((new Atexo_MpeSf())->uuid());
        }
		$affectedRows = parent::save($connexion);
		//ltReferentiel
		if($this->_collCommonReferentielConsultation){
			foreach ($this->_collCommonReferentielConsultation as $referentiel){
				$referentiel->setConsultationId($this->getId());
				$referentiel->setOrganisme($this->getOrganisme());
				$referentiel->save($connexion);
				$affectedRows++;
			}

		}
		if($this->_collCommonDce){
            $oldDce = (new Atexo_Consultation_Dce())->getDce($this->getReference(), $this->getOrganisme());
			foreach ($this->_collCommonDce as $dce){
				$dce->setConsultationId($this->getId());
				$dce->setOrganisme($this->getOrganisme());
                $dceSize = Atexo_Util::getTailleBlob($this->getOrganisme(),$dce->getDce(),false);
                $dce->setTailleDce($dceSize);
				$dce->save($connexion);
				$affectedRows++;
			}
			if(!empty($dce)) {
                // TODO for roadmap modification de getReference par getId
                if ($oldDce instanceof CommonDCE) {
                    (new Atexo_Blob())->changeExtensionBlob($oldDce->getDce(), $oldDce->getOrganisme(), Atexo_Config::getParameter("EXTENSION_DCE_HISTORIQUE"));
                }
            }
		}
		if($this->_collCommonRg){
            $oldRg = (new Atexo_Consultation_Rg())->getReglement($this->getReference(), $this->getOrganisme());
			foreach ($this->_collCommonRg as $rg){
				$rg->setConsultationId($this->getId());
				$rg->setOrganisme($this->getOrganisme());
				$rg->save($connexion);
				$affectedRows++;
			}
            if(!empty($rg)) {
                // TODO for roadmap modification de getReference par getId
                if ($oldRg instanceof CommonRG) {
                    (new Atexo_Blob())->changeExtensionBlob($oldRg->getRg(), $oldRg->getOrganisme(), Atexo_Config::getParameter("EXTENSION_RC_HISTORIQUE"));
                }
            }

		}
		if($this->_collCommonDateFin){
			foreach ($this->_collCommonDateFin as $dateFin){
				$dateFin->setConsultationId($this->getId());
				$dateFin->setOrganisme($this->getOrganisme());
				$dateFin->save($connexion);
				$affectedRows++;
			}
		}
		if($this->_collCommonComplement){
			foreach ($this->_collCommonComplement as $complement){
				$complement->setConsultationId($this->getId());
				$complement->setOrganisme($this->getOrganisme());
				$complement->save($connexion);
				$affectedRows++;
			}
		}
		if($this->_collCommonVisiteLieux){
			foreach ($this->_collCommonVisiteLieux as $visitesLieux){
				$visitesLieux->setConsultationId($this->getId());
				$visitesLieux->setOrganisme($this->getOrganisme());
				$visitesLieux->save($connexion);
				$affectedRows++;
			}
		}
		if($this->_collCommonInterneConsultationSuiviSeul){
			foreach ($this->_collCommonInterneConsultationSuiviSeul as $interneConsultationSuiviSeul){
				$interneConsultationSuiviSeul->setConsultationId($this->getId());
				$interneConsultationSuiviSeul->setOrganisme($this->getOrganisme());
				$interneConsultationSuiviSeul->save($connexion);
				$affectedRows++;
			}
		}
		if($this->_collCommonInterneConsultation){
			foreach ($this->_collCommonInterneConsultation as $interneConsultation){
				$interneConsultation->setConsultationId($this->getId());
				$interneConsultation->setOrganisme($this->getOrganisme());
				$interneConsultation->save($connexion);
				$affectedRows++;
			}
		}
		return $affectedRows;

	}

    public function save(PropelPDO $con = null)
    {
        $this->setDateFinUnix(strtotime($this->getDatefin()." + ".$this->getPoursuivreAffichage()." ".$this->getPoursuivreAffichageUnite()));
        return parent::save($con);
    }

	public function countCategorielots(){
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$c = new Criteria();
		$c->add(CommonCategorieLotPeer::ORGANISME,$this->getOrganisme());
		$c->add(CommonCategorieLotPeer::CONSULTATION_ID,$this->getId());
		$categories = CommonCategorieLotPeer::doSelect($c, $connexion);
		return $categories;
	}

	public function getRedactionDocumentsRedac()
    {
        return $this->_redactionDocumentsRedac;
    }

    public function setRedactionDocumentsRedac($value)
    {
        $this->_redactionDocumentsRedac=$value;
    }

	public function getCommonVisiteLieuxs($criteria = null, ?PropelPDO $con = null){
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$c = new Criteria();
		$c->add(CommonVisiteLieuxPeer::ORGANISME,$this->getOrganisme());
		$c->add(CommonVisiteLieuxPeer::CONSULTATION_ID,$this->getId());
		$visiteLieux = CommonVisiteLieuxPeer::doSelect($c, $connexion);
		return $visiteLieux;
	}

	public function getAlertes($open = NULL){
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$c = new Criteria();
		$c->add(CommonAlerteMetierPeer::ORGANISME,$this->getOrganisme());
		$c->add(CommonAlerteMetierPeer::REFERENCE_CONS,$this->getId());
		if($open !== NULL){
			$c->add(CommonAlerteMetierPeer::CLOTUREE,2);
		}
		$alertes = CommonAlerteMetierPeer::doSelect($c, $connexion);
		return $alertes;
	}

	public function getAllReunion(){
		$listeR = array();
		if($this->getAlloti()){
			$lots = $this->getAllLots();
			foreach($lots as $lot){
					$listeR[] = $lot;
			}
		}else{
				$listeR[0] = $this;
		}
		return $listeR;

	}

	public function getDCE(){
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$c = new Criteria();
		$c->add(CommonDCEPeer::ORGANISME,$this->getOrganisme());
		$c->add(CommonDCEPeer::CONSULTATION_ID,$this->getId());
		$DCECons = CommonDCEPeer::doSelect($c, $connexion);
		return $DCECons;
	}

	public function getAnnoncePublieeByTypeAvis($idTypeAvis){
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::ORGANISME,$this->getOrganisme());
        $c->add(CommonConsultationPeer::SERVICE_ID,$this->getServiceId());
        $c->add(CommonConsultationPeer::REFERENCE_UTILISATEUR,$this->getReferenceUtilisateur());
		$c->add(CommonConsultationPeer::ID_TYPE_AVIS,$idTypeAvis);
		$c->add(CommonConsultationPeer::DATE_MISE_EN_LIGNE_CALCULE,"",Criteria::NOT_EQUAL);
		$c->add(CommonConsultationPeer::DATE_MISE_EN_LIGNE_CALCULE,"0000-00-00 00:00:00",Criteria::NOT_EQUAL);
        $annonce = CommonConsultationPeer::doSelect($c, $connexion);
        return $annonce;
    }

	public function getCommonValeurLtReferentiel($idLtReferentiel){
        $referenciel = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($this->getId(), 0, $idLtReferentiel, $this->getOrganisme());
        if($referenciel){

        	return $referenciel->getValeurPrincipaleLtReferentiel();
        }
        return 0;
	}
	//-- cette fonction renvoie la valeur de l'estimation de la consultaion , la somme des estimations des lots dans le cas alloti
  public function getEstimationConsultation ($idLtReferentiel)
   {
     $valeur = 0;
     if ($this->getAlloti()) {
      $lots = $this->getAllLots();
      foreach($lots as $lot){
     $referenciel = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($this->getId(), $lot->getLot(), $idLtReferentiel, $this->getOrganisme());
          if($referenciel){
           $valeur += $referenciel->getValeurPrincipaleLtReferentiel();
          }
   }
  }else {
   $valeur = $this->getCommonValeurLtReferentiel($idLtReferentiel);
  }
  return $valeur;
 }

	 public function getOrganismeObject()
    {
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($this->getOrganisme());
    return $organisme;
    }

	 public function getUrlOrganisme()
    {
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($this->getOrganisme());
        if($organisme) {
        	 if($organisme->getUrl()){
        	 	return $organisme->getUrl();
        	 } else {
        	 	return "";
        	 }
        } else {
            return "";
        }
    }

    /*
     * Methode permet de vérifier si une consultation est dans le panier de l'entreprise connectée
     * @param $idEntreprise,$idInscrit
     *
     */
    public function isInPanier($idEntreprise,$idInscrit)
    {
        if($idEntreprise==0 || $idInscrit==0) return false;

    	$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonPanierEntreprisePeer::ORGANISME,$this->getOrganisme());
        $c->add(CommonPanierEntreprisePeer::CONSULTATION_ID,$this->getId());
        $c->add(CommonPanierEntreprisePeer::ID_ENTREPRISE,$idEntreprise);
		$c->add(CommonPanierEntreprisePeer::ID_INSCRIT,$idInscrit);
        $panierEntreprise = CommonPanierEntreprisePeer::doSelect($c, $connexion);
        if(is_array($panierEntreprise) && count($panierEntreprise)){
        	return true;
        }
        return false;

    }

    /**
     * Permet de retourner si la consultation à une clause donnée
     * @param $clauseName
     *
     */
	public function hasThisClause($clauseName) {
		$getClause = "get".ucfirst($clauseName);
		if($this->getAlloti()){
			$lots = $this->getAllLots();
			foreach($lots as $lot){
					if($lot->hasThisClause($clauseName)){
						return true;
					}
			}
		}else{
			if($this-> $getClause()){
				return true;
			}
		}
		return false;
    }

	/*
	 * retourne le donné complementaire du lot
	 */
	public function getDonneComplementaire()
	{
	    if($this->getIdDonneeComplementaire() !== null) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonTDonneeComplementairePeer::ID_DONNEE_COMPLEMENTAIRE, $this->getIdDonneeComplementaire());
            $donneeComplaimentaire = CommonTDonneeComplementairePeer::doSelectOne($c, $connexion);
            if ($donneeComplaimentaire) {
                return $donneeComplaimentaire;
            }
        }
		return false;
	}
	/*
	 * retourne true si les un des lots d'une consultation ont une variante sinon false
	 */
	public function lotsHasVariante(){
		if($this->getAlloti()){
			$lots = $this->getAllLots();
			foreach($lots as $lot){
				if($lot->getDonneComplementaire() && $lot->getDonneComplementaire()->getVariantes() == '1'){
					return true;
				}
			}
		}
		return false;
	}

	/**
     * Permet de setter L'objet Atexo_CPV  à partir de la consultation
     *
     * @author LEZ <loubna.ezziani@atexom.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function setObejtCpv($max = null)
    {
    		$cpv = new Atexo_CPV();
    		$cpv->setCodePrincipal($this->getCodeCpv1());
    		if($this->getCodeCpv2()) {
				$arrayCodesCPV=explode('#',$this->getCodeCpv2());
				if(is_array($arrayCodesCPV) && $arrayCodesCPV) {
					$i = 1;
					foreach($arrayCodesCPV as $codeCpv) {
						if($codeCpv) {
							$setCodeSecondaire = "setCodeSecondaire".$i;
							$cpv->$setCodeSecondaire($codeCpv);
							$i++;
							if($max != null && $max < $i) {
							    break;
                            }
						}
					}
				}
			}
			$this->setCpv($cpv);
    }

    /**
     * Permet d'ajouter les parametres de formulaire SUB
     *
     * @param Integer $typeEnveloppe le type d'enveloppe
     * @param Integer $idCleExterne la cle externe
     * @return void
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function addParametrageFormulaireSub($typeEnveloppe,$idCleExterne)
    {
        $paramFormulaire = (new Atexo_FormulaireSub_Parametrage())->retrieveParametrageEnveloppe($this->getId(), $this->getOrganisme(), $typeEnveloppe);
        if (!$paramFormulaire) {
            $paramFormulaire = new CommonTParamDossierFormulaire();
            $paramFormulaire->setTypeEnveloppe($typeEnveloppe);
            $paramFormulaire->setConsultationId($this->getId());
            $paramFormulaire->setOrganisme($this->getOrganisme());
        }
        $paramFormulaire->setCleExterneDispositif($idCleExterne);
        $this->addCommonTParamDossierFormulaire($paramFormulaire);
    }

    /**
     * Permet de verifier si la consultation est allotie avec au moins 1 lot
     *
     * @return bool true si consultation allotie, false sinon
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function isAllotie()
    {
        if( $this->getAlloti() && $this->isConsultationHasLots() ) {
            return true;
        }
        return false;
    }

    /**
     * Permet de verifier si la consultation a au moins 1 lot
     *
     * @return bool true si la consultation a au moins 1 lot, false sinon
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function isConsultationHasLots()
    {
        if( is_array($this->getAllLots()) && count($this->getAllLots()) ) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getMarcheReserve()
    {
        return ($this->getClauseSocialeAteliersProteges() || $this->getClauseSocialeSiae() || $this->getClauseSocialeEss());
    }

    /**
     * @param mixed $marcheReserve
     */
    public function setMarcheReserve($marcheReserve)
    {
        $this->marcheReserve = $marcheReserve;
    }

    /**
     * @return mixed
     */
    public function getJustificationNonAllotissement()
    {
        if(!$this->justificationNonAllotissement && !$this->getAlloti()){
            $donneeComplementaire = $this->getDonneComplementaire();
            if($donneeComplementaire instanceof CommonTDonneeComplementaire){
                $this->justificationNonAllotissement = $donneeComplementaire->getJustificationNonAlloti();
            }
        }
        return $this->justificationNonAllotissement;


    }

    /**
     * @param mixed $justificationNonAllotissement
     */
    public function setJustificationNonAllotissement($justificationNonAllotissement)
    {

        $this->justificationNonAllotissement = $justificationNonAllotissement;
    }



    /**
     * Permet d'avoir le libelle de type de contrat
     *
     * @return String libelle de type de contrat
     * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
     * @version 1.0
     * @since 4.11.0
     * @copyright Atexo 2015
     */
    public function getLibelleTypeContrat()
    {
        $typeContratQuery = new CommonTTypeContratQuery();
        $typeContrat = $typeContratQuery->getTypeContratById($this->getTypeMarche());
        if($typeContrat instanceof CommonTTypeContrat){
            return Prado::Localize($typeContrat->getLibelleTypeContrat());
        }
        return "-";
    }

    /**
     * Permet de recuperer l'objet EtatConsultation
     *
     * @param bool $callFromEntreprise : precise si la methode est appelee depuis l'acces entreprise
     *     => true  si appelee depuis l'acces entreprise
     *     => false sinon
     * @return CommonEtatConsultation
     * @throws
     */
    public function getObjetEtatConsultation($callFromEntreprise)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        return CommonEtatConsultationQuery::create()->filterByIdEtat(Atexo_Consultation::getStatus($this,$callFromEntreprise))->findOne($connexion);
    }

    public function hasContrat(){
        $consLotConrat = (array)(new Atexo_Consultation_Contrat())->getListeConsLotsContratByConsultation($this->getId(), $this->getOrganisme());
        return !empty($consLotConrat);
    }


    /**
     * TODO LE
     */
    public function getCertificatChiffrements($typeEnv = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c=new Criteria();
        $c->add(CommonCertificatChiffrementPeer::CONSULTATION_ID, $this->getId());
        $c->add(CommonCertificatChiffrementPeer::ORGANISME, $this->getOrganisme());
        if($typeEnv) {
            $c->add(CommonCertificatChiffrementPeer::TYPE_ENV, $typeEnv);
        }
        $certificats=CommonCertificatChiffrementPeer::doSelect($c, $connexion);
        if($certificats) {
            return $certificats;
        } else {
            return array();
        }
    }

    /**
     * Permet de recuperer la premiere annonce publiee sur le support BOAMP
     *
     * @param string $statut : statut de la date a recuperer
     * @return DateTime|null
     * @author OKO <oumar.konate@atexo.com>
     * @version 1.0
     * @since esr-2015
     * @copyright Atexo 2015
     * @package mpe
     */
    public function getFirstAnnonceBoamp($statut)
    {
        $params = [];
								$logger = Atexo_LoggerManager::getLogger('app');
        try {
            $query = "
                        SELECT
                            SAC.`date_envoi_support`, SAC.`date_publication_support`
                        FROM
                            `consultation` C, `t_annonce_consultation` AC, `t_support_annonce_consultation` SAC, `t_support_publication` SP
                        WHERE
                            C.`reference` = AC.`consultation_id`
                            AND C.`organisme` = AC.`organisme`
                            AND AC.`id` = SAC.`id_annonce_cons`
                            AND SAC.`id_support` = SP.`id`
                            AND C.`organisme` = :organisme AND C.`reference` = :reference
                            AND SP.`code` = :code
                     ";
            if($statut == Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION')) {
                $query .= " AND SAC.`date_envoi_support` IS NOT NULL AND SAC.`date_envoi_support` NOT LIKE '0000-00-00 00:00:00' ";
            } else if($statut == Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE')) {
                $query .= " AND SAC.`date_publication_support` IS NOT NULL AND SAC.`date_publication_support` NOT LIKE '0000-00-00 00:00:00' ";
            }

            $params[':organisme'] = $this->getOrganisme();
            $params[':reference'] = $this->getId();
            $params[':code'] = 'BOAMP';

            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            $statement->execute($params);

            $date = null;
            while($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                if($statut == Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION')) {
                    if (!empty($row['date_envoi_support'])) {
                        $date = $row['date_envoi_support'];
                        break;
                    }
                } else if($statut == Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE')) {
                    if (!empty($row['date_publication_support'])) {
                        $date = $row['date_publication_support'];
                        break;
                    }
                }
            }
            return $date;
        } catch (Exception $e) {
            $logger->error("Erreur lors de la recuperation de la date '$statut' de la premiere annonce $statut sur le support BOAMP : \n\nRequete = $query \n\nErreur = " . $e->getMessage() . " \n\nTrace: " . $e->getTraceAsString() . " \n\nMethode = CommonConsultation::getFirstAnnoncePublieeSurBoamp");
        }
    }

    public function getCertificatChiffrementsNonDouble()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c=new Criteria();
        $c->add(CommonCertificatChiffrementPeer::CONSULTATION_ID, $this->getId());
        $c->add(CommonCertificatChiffrementPeer::ORGANISME, $this->getOrganisme());
        $c->addGroupByColumn(CommonCertificatChiffrementPeer::CERTIFICAT);
        $c->addGroupByColumn(CommonCertificatChiffrementPeer::TYPE_ENV);
        $certificats=CommonCertificatChiffrementPeer::doSelect($c, $connexion);
        if($certificats) {
            return $certificats;
        } else {
            return array();
        }
    }

    /**
     * Permet de recuperer l'objet du type procedure dume de la consultation
     *
     * @return CommonTTypeProcedureDume|null
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     * @version 1.0
     * @since 2017-esr
     * @copyright Atexo 2018
     */
    public function getTypeProcedureDumeObject(){
        return CommonTTypeProcedureDumeQuery::create()->findOneById($this->getTypeProcedureDume());
    }

    public function getMailAgentCreateur(){
        return (new Atexo_Agent())->getMailAgentById($this->getIdCreateur());
    }

    /**
     * Permet de recuperer le siret de l'acheteur
     *
     * @return String
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     * @version 1.0
     * @since 2017-esr
     * @copyright Atexo 2018
     */
    public function getSiretAcheteur()
    {
        $siret = "";

        if (Atexo_Module::isEnabled('OrganisationCentralisee', $this->getOrganisme()) || $this->getServiceId() == 0) {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($this->getOrganisme());
            if ($organisme instanceof CommonOrganisme) {
                $siret = $organisme->getSiren() . $organisme->getComplement();
            }
        } else {
            $service = Atexo_EntityPurchase::retrieveEntityById($this->getServiceId(), $this->getOrganisme());
            if ($service instanceof CommonService) {
                $siret = $service->getSiren() . $service->getComplement();
            }
        }
        return $siret;
    }

    /**
     * Permet de recuperer le code de categorie de la consultation
     *
     * @return String
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     * @version 1.0
     * @since 2017-esr
     * @copyright Atexo 2018
     */
    public function getCodeDumeCategorieConsultation(){
        $code = "";
        $categorie = Atexo_Consultation_Category::retrieveCategorie($this->getCategorie(),true);
        if($categorie instanceof CommonCategorieConsultation && $categorie->getCodeDume()) {
            $code = $categorie->getCodeDume();
        }
        return $code;
    }
     /**
     * Permet de recuperer le code de type de contrat de la consultation
     *
     * @return String
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     * @version 1.0
     * @since 2017-esr
     * @copyright Atexo 2018
     */
    public function getCodeDumeTypeContrat(){
        $code = "";
        $typeContratQuery = new CommonTTypeContratQuery();
        $typeContrat = $typeContratQuery->getTypeContratById($this->getTypeMarche());
        if($typeContrat instanceof CommonTTypeContrat && $typeContrat->getCodeDume()){
            $code = $typeContrat->getCodeDume();
        }
        return $code;
    }

    /**
     * Permet de recuperer les info sur le dume
     *
     * @return String
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     * @version 1.0
     * @since 2018-roadmap
     * @copyright Atexo 2018
     */
    public function getInfoDumeAcheteur()
    {
        $return = "";
        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $tDume = Atexo_Dume_AtexoDume::getTDumeContexte($this);
            if ($tDume instanceof CommonTDumeContexte) {
                $img = '<img src="%s/images/%s" alt="%s" title="%s" class="%s"/>';
                $cheminTheme = Atexo_Controller_Front::t();
                if ($this->getTypeFormulaireDume() == 1) {
                    $image = "logo-dume-non-publie.png";
                    $msg = Prado::localize('DEFINE_DUME_ACHETEUR_SIMPLIFIE');
                    $class = "indent-5 inline-img";
                } else {
                    $image = "logo-dume-publie.png";
                    $msg = Prado::localize('DEFINE_DUME_ACHETEUR_PUBLIE');
                    $class = "indent-5 inline-img";
                }
                $return = sprintf($img, $cheminTheme, $image, $msg, $msg, $class);
            }
        }
        return $return;
    }

    /**
     * Permet de recuperer les info sur le dume
     *
     * @return bool
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     * @version 1.0
     * @since 2018-roadmap
     * @copyright Atexo 2018
     */
    public function hasDumeAcheteurStandard()
    {
        $return = false;
        if (Atexo_Module::isEnabled('InterfaceDume') && $this->getDumeDemande()) {
            $tDume = Atexo_Dume_AtexoDume::getTDumeContexte($this);
            if ($tDume instanceof CommonTDumeContexte) {
                $return = boolval($tDume->getIsStandard());
            }
        }
        return $return;
    }


    /**
     * @return array
     */
    public function getConsultationAvals() : ?PropelObjectCollection
    {
        $this->consultationAvals = null;
        $consultationAvales= (new CommonConsultationQuery())
            ->findByReferenceConsultationInit($this->getId());
        if ( $consultationAvales instanceof PropelObjectCollection) {
            $this->consultationAvals = $consultationAvales;
        }
        return $this->consultationAvals;
    }

    public function isConsultationAval(): bool
    {
        $bool =false;
        if (($this->getConsultationAvals() === null ? 0 : count($this->getConsultationAvals())) > 0) {
            $bool =true;
        }
        return $bool;
    }


    /**
     * @return CommonConsultation|CommonConsultation[]|null
     * @throws PropelException
     *
     */
    public function getConsultationAmont(): ?CommonConsultation
    {
        $this->consultationAmont = null;
        if ($this->getReferenceConsultationInit() != '') {
            $this->consultationAmont = (new CommonConsultationQuery())
                ->findOneById($this->getReferenceConsultationInit());
            if ($this->consultationAmont === null) {
                $this->consultationAmont = (new CommonConsultationQuery())
                    ->findOneByReference($this->getReferenceConsultationInit());
            }
        }
        return $this->consultationAmont;
    }

    public function isEnvolActivation()
    {
        return $this->envol_activation;
    }

    public function getACommonPlateformeVirtuelle()
    {
        return $this->aCommonPlateformeVirtuelle;
    }

    public function getCollCommonAnnexeFinancieres()
    {
        return $this->collCommonAnnexeFinancieres;
    }

    public function getCollCommonAnnexeFinancieresPartial()
    {
        return $this->collCommonAnnexeFinancieresPartial;
    }

    public function getCollCommonPieceGenereConsultations()
    {
        return $this->collCommonPieceGenereConsultations;
    }

    public function getCollCommonPieceGenereConsultationsPartial()
    {
        return $this->collCommonPieceGenereConsultationsPartial;
    }

    public function getCommonAnnexeFinancieresScheduledForDeletion()
    {
        return $this->commonAnnexeFinancieresScheduledForDeletion;
    }

    public function getCommonPieceGenereConsultationsScheduledForDeletion()
    {
        return $this->commonPieceGenereConsultationsScheduledForDeletion;
    }

    /**
     * Nous avons volontairement mis 0 pour désactiver définitivement les enveloppes techniques.
     */
    public function setEnvOffreTechnique($v)
    {
        return parent::setEnvOffreTechnique(0);
    }

    /**
     * Nous avons volontairement mis 0 pour désactiver définitivement les enveloppes techniques.
     */
    public function getEnvOffreTechnique()
    {
        return 0;
    }
} // CommonConsultation