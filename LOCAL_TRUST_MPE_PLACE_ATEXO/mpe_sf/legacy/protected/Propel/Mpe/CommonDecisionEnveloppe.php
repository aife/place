<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use AtexoDume\Dto\Lot;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonDecisionEnveloppePeer;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonDecisionEnveloppe;
use Application\Service\Atexo\Atexo_Config;


/**
 * Skeleton subclass for representing a row from the 'decisionEnveloppe' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonDecisionEnveloppe extends BaseCommonDecisionEnveloppe {
	protected $_tailleFichier;
	protected $nomEntreprise;
	protected  $_libelleCategorieConsultation;

	public function getTailleFichier()
	{
		return $this->_tailleFichier;
	}

	public function setTailleFichier($tailleFichier)
	{
		$this->_tailleFichier = $tailleFichier;
	}

	public function getNomEntreprise()
	{
		return $this->nomEntreprise;
	}

	public function setNomEntreprise($nomEntreprise)
	{
		$this->nomEntreprise = $nomEntreprise;
	}
	/*
	 * Permet d'avoir l'offre electronique lie à l'enveloppe de decsion
	 */
	public function getOffres()
	{
		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$criteria = new Criteria();
		$criteria->add(CommonOffresPeer::ID, $this->getIdOffre());
		$criteria->add(CommonOffresPeer::ORGANISME, $this->getOrganisme());
		return CommonOffresPeer::doSelectOne($criteria, $connexionCom);
	}

	/*
	 * Permet d'avoir les enveloppes lies à la decsion
	 */
	public function getEnveloppes()
	{
		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$criteria = new Criteria();
		$criteria->add(CommonEnveloppePeer::OFFRE_ID, $this->getIdOffre());
		$criteria->add(CommonEnveloppePeer::ORGANISME, $this->getOrganisme());
		if($this->getLot()){
			$crit0 = $criteria->getNewCriterion(CommonEnveloppePeer::SOUS_PLI, $this->getLot());
       		$crit1 = $criteria->getNewCriterion(CommonEnveloppePeer::SOUS_PLI, 0);
       		$crit0->addOr($crit1);
       		$criteria->add($crit0);
		}
		return CommonEnveloppePeer::doSelect($criteria, $connexionCom);
	}
	/*
	 * Permet de recuperer la decision enveloppe par IdOffre et Lot
	 */
	public function getDecisionByIdOffreAndLot($idOffre,$lot){
		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$criteria = new Criteria();
		$criteria->add(CommonDecisionEnveloppePeer::ID_OFFRE, $idOffre);
		$criteria->add(CommonDecisionEnveloppePeer::LOT, $lot);
		return CommonDecisionEnveloppePeer::doSelectOne($criteria, $connexionCom);
	}

	/**
	 * @param mixed $libelleCategorieConsultation
	 */
	public function setLibelleCategorieConsultation($libelleCategorieConsultation)
	{
		$this->_libelleCategorieConsultation = $libelleCategorieConsultation;
	}



	public function getLibelleCategorieConsultation()
	{
		if($this->_libelleCategorieConsultation) {
			return $this->_libelleCategorieConsultation;
		}
		$categorie = Atexo_Consultation_Category::retrieveCategorie($this->getCategorie(),true);
		if($categorie) {
			return $categorie->getLibelle();
		} else {
			return "";
		}
	}



} // CommonDecisionEnveloppe
