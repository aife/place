<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonInvitePermanentTransverseQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'invite_permanent_transverse' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonInvitePermanentTransverseQuery extends BaseCommonInvitePermanentTransverseQuery
{
    /**
     * Cette méthode permet de rechercher la liste des identifiants des services pour lesquels l'identifiant de l'agent passé en paramètre est invité permanent transverse
     * @param $agentId identifiant de l'agent
     * @return array liste des id des services pour lesquels l'agent est invité transverse
     */
    public function getServicesIdByAgentId($agentId) {
        $listOfServicesIdInvitePermanentTransverse = array();

        if($agentId>0) {
            $listTInvitePermanentTransverse = $this->findByAgentId($agentId);
            foreach ($listTInvitePermanentTransverse as $item) {
                if($item->getServiceId()=== null) $listOfServicesIdInvitePermanentTransverse [] = 0;
                else $listOfServicesIdInvitePermanentTransverse [] = $item->getServiceId();
            }
        }

        return $listOfServicesIdInvitePermanentTransverse;

    }


    public function getSelectedService($agentId) {
        $listOfServicesIdInvitePermanentTransverse = array();

        if($agentId>0) {
            $listTInvitePermanentTransverse = $this->findByAgentId($agentId);
            foreach ($listTInvitePermanentTransverse as $item) {
                $listOfServicesIdInvitePermanentTransverseInter = [];
                $listOfServicesIdInvitePermanentTransverseInter['serviceId'] = $item->getServiceId();
                $listOfServicesIdInvitePermanentTransverseInter['agentId'] = $item->getAgentId();
                $listOfServicesIdInvitePermanentTransverseInter['acronyme'] = $item->getAcronyme();
                $listOfServicesIdInvitePermanentTransverse[] = $listOfServicesIdInvitePermanentTransverseInter;
            }
        }

        return $listOfServicesIdInvitePermanentTransverse;

    }
}
