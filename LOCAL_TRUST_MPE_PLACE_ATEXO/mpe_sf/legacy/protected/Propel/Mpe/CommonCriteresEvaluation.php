<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonCriteresEvaluation;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'Criteres_Evaluation' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonCriteresEvaluation extends BaseCommonCriteresEvaluation {

	public function getLibelleTypeCritere() {
		$typeForm = $this->getTypeCritere();
 		if($typeForm == Atexo_Config::getParameter('GRILLE_EVALUATION')) {
 			return Prado::localize('TEXT_GRILLE_EVALUATION');
 		}
 		if($typeForm == Atexo_Config::getParameter('OFFRE_MOINS_DISSNTE')) {
 			return Prado::localize('TEXT_OFFRE_MOINS_DISSNTE');
 		}
 		if($typeForm == Atexo_Config::getParameter('HORS_SYSTEM')) {
 			return Prado::localize('TEXT_HORS_SYSTEM');
 		}
 	}

 	public function getLibelleTypeEnveloppe() {
 		$typeEnveloppe = $this->getTypeEnveloppe();
 		if($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
 			return Prado::localize('CANDIDATURE');
 		}
 		if($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
 			return Prado::localize('OFFRE');
 		}
 		if($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
 			return Prado::localize('ANONYMAT_3_ENVELOPPE');
 		}
 		if($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
 			return Prado::localize('OFFRE_TECHNIQUE');
 		}
 	}



} // CommonCriteresEvaluation
