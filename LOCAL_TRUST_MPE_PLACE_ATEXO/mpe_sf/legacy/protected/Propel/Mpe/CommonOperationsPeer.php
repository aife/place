<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonOperations;

  // include base peer class
  use Application\Propel\Mpe\Om\BaseCommonOperationsPeer;


/**
 * Skeleton subclass for performing query and update operations on the 'Operations' table.
 *
 * 
 *
 * This class was autogenerated by Propel on:
 *
 * Thu Mar 20 11:49:01 2014
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonOperationsPeer extends BaseCommonOperationsPeer {

} // CommonOperationsPeer
