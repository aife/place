<?php

namespace Application\Propel\Mpe;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonInvitePermanentContratQuery;
use Application\Service\Atexo\Atexo_Config;


/**
 * Skeleton subclass for performing query and update operations on the 'invite_permanent_contrat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.Application.Propel.Mpe
 */
class CommonInvitePermanentContratQuery extends BaseCommonInvitePermanentContratQuery
{
    public function getSelectedServiceForContrat($idContrat) {
        $listOfServicesIdInvitePermanentContrat = array();

        if($idContrat>0) {

            $listTInvitePermanentContrat = $this->findByIdContratTitulaire($idContrat);
            foreach ($listTInvitePermanentContrat as $item) {
                $listOfServicesIdInvitePermanentContratInter = [];
                $listOfServicesIdInvitePermanentContratInter['serviceId'] = $item->getServiceId();
                $listOfServicesIdInvitePermanentContratInter['acronyme'] = $item->getAcronyme();
                $listOfServicesIdInvitePermanentContrat[] = $listOfServicesIdInvitePermanentContratInter;
            }
        }

        return $listOfServicesIdInvitePermanentContrat;

    }

    public function getSelectedServiceForConsultation($idConsultation, $lot, $organisme, $idContrat =null) {
           return  $this->findByArray([
                'id_consultation' => $idConsultation,
                'lot' => $lot,
                'organisme' => $organisme,
                'id_titulaire_contrat' => $idContrat
            ]);
    }

    public function deleteForConsutlation($idConsultation, $lot, $organisme, $idContrat =null) {
        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $listInv = (new CommonInvitePermanentContratQuery())->getSelectedServiceForConsultation($idConsultation, $lot, $organisme, $idContrat);
        if($listInv) {
            foreach($listInv as $oneInv)
            {
                (new CommonInvitePermanentContratQuery())->doDelete($oneInv->getid());
            }
        }
    }
}
