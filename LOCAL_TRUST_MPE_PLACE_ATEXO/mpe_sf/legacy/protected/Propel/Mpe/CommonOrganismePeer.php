<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonOrganismePeer;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * Skeleton subclass for performing query and update operations on the 'Organisme' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonOrganismePeer extends BaseCommonOrganismePeer {

    public static function getListDenominationOrganismeByIdAgent($idAgent, $acronymeDiff = false, $conx = null)
    {
        if($conx === null) {
            $conx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();
        $c->addJoin(CommonOrganismePeer::ACRONYME, CommonTVisionRmaAgentOrganismePeer::ACRONYME);
        $c->add ( CommonTVisionRmaAgentOrganismePeer::ID_AGENT, $idAgent );
        if($acronymeDiff) {
            $c->add ( CommonTVisionRmaAgentOrganismePeer::ACRONYME, $acronymeDiff, Criteria::NOT_EQUAL );
        }
        $organismes = (array) CommonOrganismePeer::doSelect ( $c, $conx );
        if ( isset($organismes) && is_array($organismes) ) {
            return $organismes;
        } else {
            return [];
        }
    }

    public static function retrieveOrganismeByAcronyme($acronyme, $activeSeulement = false) {

        $actifSeul = "N";
        if($activeSeulement) $actifSeul = "O";

        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonOrganismePeer::ACRONYME, $acronyme, Criteria::EQUAL);
        if ($activeSeulement) {
            $c->add(CommonOrganismePeer::ACTIVE, 1, Criteria::EQUAL);
        }
        $cachedOrganisme = CommonOrganismePeer::doSelectOne($c, $connexionCommon);
        Prado::getApplication()->Cache->set('cachedOrganisme'.$acronyme.$actifSeul, $cachedOrganisme,Atexo_Config::getParameter('TTL_PRADO_CACHE'));

        return $cachedOrganisme;
    }

    public static function retrieveAllOrganismes() {

        $cachedAllOrganismes = Prado::getApplication()->Cache->get('cachedAllOrganismes');

        if(!$cachedAllOrganismes) {

            $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->addAscendingOrderByColumn(CommonOrganismePeer::DENOMINATION_ORG);
            $cachedAllOrganismes = CommonOrganismePeer::doSelect($c, $connexionCommon);

            Prado::getApplication()->Cache->set('cachedAllOrganismes', $cachedAllOrganismes,Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }

        return $cachedAllOrganismes;
    }

    public function retrieveAcronymeAllInactifsOrganismes() {

        $cachedAcronymeAllInactifsOrganismes = Prado::getApplication()->Cache->get('cachedAcronymeAllInactifsOrganismes');

        if(!$cachedAcronymeAllInactifsOrganismes) {
            $allOrganismes = self::retrieveAllOrganismes();
            $cachedAcronymeAllInactifsOrganismes = array();
            foreach ($allOrganismes as $organisme) {
                if($organisme->getActive() === "0") {
                    $cachedAcronymeAllInactifsOrganismes [] = $organisme->getAcronyme();
                }
            }
            Prado::getApplication()->Cache->set('cachedAcronymeAllInactifsOrganismes', $cachedAcronymeAllInactifsOrganismes,Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }

        return $cachedAcronymeAllInactifsOrganismes;
    }
} // CommonOrganismePeer
