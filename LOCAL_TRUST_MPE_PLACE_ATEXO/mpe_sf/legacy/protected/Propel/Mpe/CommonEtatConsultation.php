<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonEtatConsultation;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'EtatConsultation' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEtatConsultation extends BaseCommonEtatConsultation {
	/*
	 * Permet de faire la correspondance entre le contenu du champ code_etat et messages.xml
	 */
	public function getLibelleEtat() {
		return Prado::localize($this->code_etat);
	}

} // CommonEtatConsultation
