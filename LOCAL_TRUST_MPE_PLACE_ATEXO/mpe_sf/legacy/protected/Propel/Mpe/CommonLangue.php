<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonLangue;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'Langue' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonLangue extends BaseCommonLangue {

	/*
	 * retourne le code du libellé de la langue en fonction de la langue choisi. Ce code sera recuperé dans les messages
	 */
	public function getLibelle() {
		return Prado::localize('DEFINE_LANGUE_'.strtoupper($this->langue));
	}

} // CommonLangue
