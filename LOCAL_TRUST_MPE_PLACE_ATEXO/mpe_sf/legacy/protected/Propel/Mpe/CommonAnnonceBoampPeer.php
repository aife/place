<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonReferentielDestinationFormXmlPeer;

use Application\Propel\Mpe\CommonAnnonceBoamp;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonAnnonceBoampPeer;

// include base peer class

  // include object class


/**
 * Skeleton subclass for performing query and update operations on the 'AnnonceBoamp' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAnnonceBoampPeer extends BaseCommonAnnonceBoampPeer {


	public static function doSelectJoinCommonReferentielDestinationFormXml(Criteria $c, $con, $organisme)
	{
		$c->addJoin(CommonAnnonceBoampPeer::ID_DESTINATION_FORM_XML, CommonReferentielDestinationFormXmlPeer::ID);
		$annoncesBoamp = CommonAnnonceBoampPeer::doSelect($c, $con);

		$resultat = array();
		if($annoncesBoamp[0] instanceof CommonAnnonceBoamp) {
			if(is_array($annoncesBoamp) && count($annoncesBoamp)) {
				foreach($annoncesBoamp as $annonceBoamp) {
				    $referentielDestinationFormXml = CommonReferentielDestinationFormXmlPeer::retrieveByPK($annonceBoamp->getIdDestinationFormXml(), $organisme, $con);
					//$referentielDestinationFormXml->addCommonAnnonceBoamp($annonceBoamp);  
					$annonceBoamp->setCommonReferentielDestinationFormXml($referentielDestinationFormXml);
					$resultat[] = $annonceBoamp;
				}
			}
		}
		return $resultat;
	}

} // CommonAnnonceBoampPeer
