<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonHabilitationAgent;

use Application\Propel\Mpe\CommonAgent;
use Application\Library\Propel\Collection\PropelObjectCollection;

  // include base peer class

  // include object class
  use Application\Library\Propel\Connection\PropelPDO;
  use Application\Library\Propel\Query\Criteria;
  use Application\Propel\Mpe\Om\BaseCommonAgentPeer;


  /**
 * Skeleton subclass for performing query and update operations on the 'Agent' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAgentPeer extends BaseCommonAgentPeer {

    /**
     * Permet de recuperer les agents joints à leurs habilitations
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con : objet connexion
     * @return     array : liste des agents
     * @author     Oumar KONATE <oumar.konate@atexo.com>
     * @version    1.0
     * @since      4.11.0
     * @copyright  Atexo 2015
     */
    public static function doSelectJoinCommonHabilitationAgent(Criteria $criteria, $con = null)
    {
        $criteria = clone $criteria;

        $agents = CommonAgentPeer::doSelect($criteria, $con);
        $listeAgents = array();
        if(is_array($agents) && count($agents)) {
            foreach($agents as $agent) {
                if($agent instanceof CommonAgent) {
                    $habilitationAgent = CommonHabilitationAgentQuery::create()->findByIdAgent($agent->getId());
                    //if($habilitationAgent instanceof PropelObjectCollection && $habilitationAgent->getData()[0] instanceof CommonHabilitationAgent) {
                        $agent->setCommonHabilitationAgent($habilitationAgent->getData()[0]);
                        $listeAgents[] = $agent;
                    //}
                }
            }
        }
        return $listeAgents;
    }

} // CommonAgentPeer
