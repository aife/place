<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonDonneesAnnuellesConcessionTarifQuery;
use Application\Service\Atexo\Atexo_Config;


/**
 * Skeleton subclass for performing query and update operations on the 'donnees_annuelles_concession_tarif' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonDonneesAnnuellesConcessionTarifQuery extends BaseCommonDonneesAnnuellesConcessionTarifQuery
{

    public function getLastIdDonneesAnnuellesConcessionTarif($idDonneesAnnuelles,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        return static::create()->select(CommonDonneesAnnuellesConcessionTarifPeer::ID)->filterByIdDonneesAnnuelle($idDonneesAnnuelles)->orderById(Criteria::DESC)->findOne($connexion);


    }
}
