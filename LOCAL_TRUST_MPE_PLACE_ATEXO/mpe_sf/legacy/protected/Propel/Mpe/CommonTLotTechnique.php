<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonTLotTechniquePeer;
use Application\Propel\Mpe\CommonTLotTechniqueHasTranchePeer;





use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonTLotTechnique;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for representing a row from the 't_lot_technique' table.
 *
 * 
 *
 * This class was autogenerated by Propel on:
 *
 * Fri Mar 29 12:49:26 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonTLotTechnique extends BaseCommonTLotTechnique {
	public $indexLotTech;
	public function retrieveLotTechniqueByDonneComplementaire($idDonneComp){
	    if ($idDonneComp !== null) {
            $c = new Criteria();
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c->add(CommonTLotTechniquePeer::ID_DONNEE_COMPLEMENTAIRE, $idDonneComp);
            $lotTechnique = CommonTLotTechniquePeer::doSelect($c, $connexion);
            if ($lotTechnique) {
                return $lotTechnique;
            }
        }
        return false;
	}

	public function getAllLotTechniqueTranche()
	{
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
		$c=new Criteria();
		$c->add(CommonTLotTechniqueHasTranchePeer::ID_LOT_TECHNIQUE,$this->getIdLotTechnique());
        $LotsTechniqueHasTranche = CommonTLotTechniqueHasTranchePeer::doSelect($c,$connexion);
        return $LotsTechniqueHasTranche;
	}
} // CommonTLotTechnique
