<?php

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\Om\BaseCommonInvitePermanentContrat;


/**
 * Skeleton subclass for representing a row from the 'invite_permanent_contrat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.Application.Propel.Mpe
 */
class CommonInvitePermanentContrat extends BaseCommonInvitePermanentContrat
{
}
