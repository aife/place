<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonEchangeTypeAR;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Skeleton subclass for representing a row from the 'EchangeTypeAR' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEchangeTypeAR extends BaseCommonEchangeTypeAR {

	/*
	 * retourne le libelle traduit dans la langue de naviguation
	 */
	public function getLibelle()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $libelle = "libelle_".$langue;
 	    if(!$this->$libelle){
            return $this->libelle;
        } else {
            return $this->$libelle;
        }
	}

} // CommonEchangeTypeAR
