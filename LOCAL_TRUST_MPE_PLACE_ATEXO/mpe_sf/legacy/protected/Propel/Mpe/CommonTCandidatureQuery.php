<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonTCandidatureQuery;
use Application\Service\Atexo\Atexo_Config;


/**
 * Skeleton subclass for performing query and update operations on the 't_candidature' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTCandidatureQuery extends BaseCommonTCandidatureQuery
{

    /**
     * Permet de retourner une candidature dume online
     *
     * @param integer $reference la reference de la consultation
     * @param string $organisme l'acronyme de l'organisme
     * @param string $typeCandidature le type de candidature dume
     * @param string $statut le statut de dume,defaul null
     * @param $connexion
     * @return array of CommonTcandidature
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2017-esr
     * @copyright Atexo 2018
     */
    public function  getCandidatureDumeByConsultationEtType($reference,$organisme,$typeCandidature,$statut =  null,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        if($statut !== null){
            $this->filterByStatus($statut);
        }
        return (array)$this->filterByRefConsultation($reference)->filterByOrganisme($organisme)->filterByTypeCandidatureDume($typeCandidature)->filterByTypeCandidature(Atexo_Config::getParameter('TYPE_CANDIDATUE_DUME'))->find($connexion);
    }

    /**
     * Permet de retourner une candidature dume online
     *
     * @param integer $reference la reference de la consultation
     * @param string $organisme l'acronyme de l'organisme
     * @param string $typeCandidature le type de candidature dume
     * @param string $statut le statut de dume,defaul null
     * @param $connexion
     * @return array of CommonTcandidature
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2018-esr
     * @copyright Atexo 2019
     */
    public function  getCandidatureDumeByRoleIscrit($reference,$organisme,$idEntreprise,$idInscrit,$roleIscrit,$typeCandidature,$statut =  null,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        if($statut !== null){
            $this->filterByStatus($statut);
        }
        return (array)$this->filterByRefConsultation($reference)
            ->filterByOrganisme($organisme)
            ->filterByTypeCandidatureDume($typeCandidature)
            ->filterByTypeCandidature(Atexo_Config::getParameter('TYPE_CANDIDATUE_DUME'))
            ->filterByRoleInscrit($roleIscrit)
            ->filterByIdEntreprise($idEntreprise)
            ->filterByIdInscrit($idInscrit)
            ->find($connexion);
    }
}
