<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonAgentPeer;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\Classes\Util;
use Application\Propel\Mpe\Om\BaseCommonHistoriquesConsultation;
use Application\Service\Atexo\Atexo_Agrement;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;


/*****
 * la régle à suivre pour l'affichage de l'historique de chaque consultation est comme suite:
 *   sur les colonnes :
 * 			'Adresse de reatrait des dossier' 
 * 			'Adresse de dépôt des offres'  	  
 * 			'lieu d'ouverture des plis'   
 * 									==> sur la base le champ 'Valeur' contient la valeur '1'
 * 									==> sur la base le champ 'datail1' contient l'adresse saisie
 * 								    ==> sur la base le champ 'detail2' sera vide
 * 
 * 
 *  Pour les colonnes : 
 * 			'Echantillon'
 * 			'Réunion'
 *				  					==> sur la base le champ 'Valeur' contient soit la valeur '1' si oui soit la valeor '0' si 'non'
 *				 					==> sur la base le champ 'datail1' contient l'adresse saisie si (valeur ==1)
 *				  					==> sur la base le champ 'detail2' contient la date en format iso si (valeur ==1)
 * 
 *  Pour la colonne 'visite des lieux'
 * 									==> sur la base le champ 'Valeur' contient soit la valeur '1' si "oui" soit la valeor '0' si "non"
 *				 					==> sur la base le champ 'datail1' contient la combinaison suivante: (adresse1#_#date1(iso)#*_*#adresse2#_#date2)si (valeur ==1)
 *				  					==> sur la base le champ 'detail2' sera toujour vide
 * Pour la 'variante'
 * 								    ==>sur la  base le champ 'valeur' contient soit la valeur '1' si "oui" soit la valeur '0' si "non"
 * 									==>'detail1' toujours vide
 * 									==>'detail2' toujours vide
 * Pour la 'caution provisoire'
 * 									==>sur la base le champ 'valeur' positionné tjr sur la valeur '1'
 * 									==>sur la base le champ 'detail1' contient la valeur de la cantion
 * 									==>sur la base le champ 'detail2' sera toujour vide
 */         


/**
 * Skeleton subclass for representing a row from the 'historiques_consultation' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonHistoriquesConsultation extends BaseCommonHistoriquesConsultation {

	private $_contentHorodatage = null;
	/****
	 * Retourner le Text qui doit être afficher sur la colonne qui contient la valeur définie par l'action
	 *
	 * @param le message a afficher si l'action n'existe pas $msg
	 * @return le meassage à afficher
	 */

	public function getRightValue($msg)
	{
		$value = null;
		$arrayDetail1="";
		if($this->getValeur()=="1") {
			if($this->getNumeroLot()!='0') {
				$value="<strong>". Prado::localize('TEXT_LOT').' '.$this->getNumeroLot().'</strong><br/>';
			}

			$arrayDetail1=explode("#*_*#",$this->getValeurDetail1());
			if(count($arrayDetail1)>1) { // pour les visites  des lieux
				for($i=0;$i<count($arrayDetail1);$i++) { 
					if($arrayDetail1[$i]) {
						$arrayVisite=explode("#_#",$arrayDetail1[$i]);
						$value .=$arrayVisite[0].'<br/>'.Application\Service\Atexo\Atexo_Util::iso2frnDateTime($arrayVisite[1]).'<div class="spacer-small"></div>';
					}

				}
				$value = rtrim($value,'<div class="spacer-small"></div>');

			}else {
				if($this->getNomElement() == Atexo_Config::getParameter('ID_HISTORIQUE_AGREMENT')) { //agrement
				    $agIds=explode(',',$this->getValeurDetail1()); 
				     foreach($agIds as $oneId) {
				     	$value .=(new Atexo_Agrement())->retrieveLibelleAgrementsByIds($oneId)."<br/>";
				     }

				}
				elseif($this->getNomElement() == Atexo_Config::getParameter('ID_HISTORIQUE_REPONSE_OBLIGATOIRE'))
				{ 
					if($this->getValeurDetail1() == "0") {
						$value = Prado::localize('DEFINE_OBLIGATOIRE_NON');
					}elseif($this->getValeurDetail1() == "1"){
						$value = Prado::localize('DEFINE_OBLIGATOIRE_OUI');
					}

				}
				elseif($this->getNomElement() == Atexo_Config::getParameter('ID_HISTORIQUE_QUALIFICATION')) { //qualification
				    $arIds=explode("#",$this->getValeurDetail1());
				    foreach($arIds as $oneId) {
				    	$value .=(new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($oneId)."<br/>&nbsp;<br/>";
				    }

				}elseif($this->getNomElement() == Atexo_Config::getParameter('ID_HISTORIQUE_VARIANTES')) { //Variante
					$value .=Prado::localize('TEXT_OUI');

				}elseif($this->getNomElement() == Atexo_Config::getParameter('ID_HISTORIQUE_ACCES_INFORMATIONS_DCE_ENTREPRISES'))
				{ 
					if($this->getValeurDetail1() == "0") {
						$value = Prado::localize('RESTREINT');
					}elseif($this->getValeurDetail1() == "1"){
						$value = Prado::localize('PUBLIC');
					}

				}elseif($this->getNomElement() == Atexo_Config::getParameter('ID_HISTORIQUE_REPONSE_ELECTRONIQUE'))
				{ 
					if($this->getValeurDetail1() == "0") {
						$value = Prado::localize('DEFINE_REFUSEE');
					}elseif($this->getValeurDetail1() == "1"){
						$value = Prado::localize('DEFINE_AUTORISEE');

					}

				}elseif($this->getNomElement() == Atexo_Config::getParameter('ID_HISTORIQUE_CARACTERISTIQUES_REPONSES_ELECTRONIQUES'))
				{ 
					[$sign,$chiff] = explode(",",$this->getValeurDetail1());

					$value = Prado::localize('DEFINE_SIGNATURE_ENVELOPPES') . " : " ;
					if($sign == "0") {
						$value .= Prado::localize('TEXT_NON');
					}elseif($sign == "1"){
						$value .= Prado::localize('TEXT_OUI');
					}

					$value .= "<br/>" . Prado::localize('DEFINE_CHIFFREMENT_ENVELOPPES_ELECTRONIQUES') . " : " ;

					if($chiff == "0") {
						$value .= Prado::localize('TEXT_NON');
					}elseif($chiff == "1"){
						$value .= Prado::localize('TEXT_OUI');
					}

				}elseif($this->getNomElement() == Atexo_Config::getParameter('ID_HISTORIQUE_CONSTITUTION_DOSSIERS_REPONSES'))
				{ 
					[$Candidature,$offreTech,$offre,$troisiemeEnv] = explode(",",$this->getValeurDetail1());

					if($Candidature == "0") {
						$value .= "";
					}elseif($Candidature == "1"){
						$value .= Prado::localize('TEXT_ENV_CAND') . "<br/>" ;
					}

					if($offreTech == "0") {
						$value .= "" ;
					}elseif($offreTech == "1"){
						$value .= " " . Prado::localize('DEFINE_ENVELOPPE_OFFRE_TECHNIQUE') . "<br/> " ;
					}

					if($offre == "0") {
						$value .= "";
					}elseif($offre == "1"){
						$value .= " " . Prado::localize('DEFINE_ENVELOPPE_OFFRE') . "<br/>" ;
					}

					if($troisiemeEnv == "0") {
						$value .= "";
					}elseif($troisiemeEnv == "1"){
						$value .= " " . Prado::localize('DEFINE_TROISIEME_ENVELOPPE');
					}

				}elseif($this->getNomElement() == Atexo_Config::getParameter('ID_HISTORIQUE_LISTES_INVITES')){
					 $list = explode(",",$this->getValeurDetail1());
					 foreach($list as $l){
					 	[$id,$perm] = explode("_",$l);
					 	$ids[] = $id;
					 	$permanant[$id] =  $perm;
					 }

					 $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')); 
					 $c = new Criteria();
					 $c->add(CommonAgentPeer::ID, $ids,Criteria::IN);
					 $agents = CommonAgentPeer::doSelect($c,$connexionCom);

					 foreach($agents as $agent){
					 	$value .= $agent->getPrenom() . " " . $agent->getNom() . ' : ';
					 	if($permanant[$agent->getId()]){
					 		$value .=  Prado::localize('DEFINE_INVITE_PERMANENT') . "<br/>";
					 	}else{
					 		$value .=  "   -<br/>";
					 	}
					 }
				}else {
					$value .= str_replace('#_#','<br/>',$this->getValeurDetail1());
				}
			}
			if ($this->getValeurDetail2()) {
				$value .= "<br/>".str_replace('#_#','<br/>',Atexo_Util::iso2frnDateTime($this->getValeurDetail2()));
			}
			return $value;
		}else {
			$lot="";
			if($this->getNumeroLot()!='0') {
				$lot='<strong>'. Prado::localize('TEXT_LOT').' '.$this->getNumeroLot().'</strong><br/>';
			}
			return $lot.$msg;
		}
	}

	public function getHorodatage() {
		if(!$this->_contentHorodatage) {
			$this->_contentHorodatage = stream_get_contents( parent::getHorodatage());
		}
		return $this->_contentHorodatage;
	}


} // CommonHistoriquesConsultation
