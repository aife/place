<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonInscritHistorique;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'InscritHistorique' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonInscritHistorique extends BaseCommonInscritHistorique {
    public function getActionText()
    {
        $textAction = match ($this->getAction()) {
									5 => Prado::localize('ENTREPRISE_INSCRIPTION_COMME_ADMIN'),
									6 => Prado::localize('ENTREPRISE_INSCRIPTION_COMME_INSCRIT_SIMPLE'),
									1 => Prado::localize('TEXT_MODIFICATION_COMPTE'),
									4 => Prado::localize('TEXT_SUPPRESSION_COMPTE'),
									3 => Prado::localize('TEXT_DEBLOCAGE_COMPTE'),
									2 => Prado::localize('TEXT_VERROUILLAGE_COMPTE'),
									7 => Prado::localize('TEXT_RETRAIT_ATES'),
									default => '-',
								};
								return $textAction;
    }
    public function getSurLeCompte($separateur=' ')
    {
        $textSurLeCompte="-";
        $textSurLeCompte=$this->getInscrit2() . $separateur . '(' . $this->getMail2() . ')';
        return $textSurLeCompte;
    }

} // CommonInscritHistorique
