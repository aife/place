<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonTEtablissement;



use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\Om\BaseCommonTMembreGroupementEntreprise;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 't_membre_groupement_entreprise' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTMembreGroupementEntreprise extends BaseCommonTMembreGroupementEntreprise
{

    protected $entreprise;
    protected $etablissement;
    protected bool $hasChild = false;
    protected $idEntrepriseParent;
    protected $sousMembres = array();

    /**
     * Permet de retourner le libelle de role juridique de groupement
     *
     * @return mixed|string
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getLibelleRoleJuridique()
    {
        $roleJuridique = $this->getCommonTRoleJuridique();
        if($roleJuridique instanceof CommonTRoleJuridique) {
            return  Prado::Localize($roleJuridique->getLibelleRoleJuridique());
        }
    }

    /**
     * recuperer le nom de l'entreprise du membre
     *
     * @return string le nom de l'entreprise
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getNomEntreprise()
    {
        $entreprise = $this->getEntreprise();
        if ($entreprise instanceof Entreprise ){
            return $entreprise->getNom();
        }
        return "-";
    }

    /**
     * recuperer le code postal de l'etablissement du membre
     *
     * @return string code postal
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getCodePostalEtablissement()
    {
        $etablissement = $this->getCommonTEtablissement();
        if ($etablissement instanceof CommonTEtablissement ){
            return $etablissement->getCodePostal();
        }else{
            return "-";
        }
    }

    /**
     * recuperer le pays de l'etablissement du membre
     *
     * @return string pays
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getPaysEtablissement()
    {
        $etablissement = $this->getCommonTEtablissement();
        if ($etablissement instanceof CommonTEtablissement ){
            return $etablissement->getPays();
        }else{
            return "-";
        }
    }

    /**
     * recuperer la ville de l'etablissement du membre
     *
     * @return string ville
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getVilleEtablissement()
    {
        $etablissement = $this->getCommonTEtablissement();
        if ($etablissement instanceof CommonTEtablissement ){
            return $etablissement->getVille();
        }else{
            return "-";
        }
    }

    /**
     * recuperer la ville de l'etablissement du membre
     *
     * @return string ville
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getSaisiManuel()
    {
        $entreprise = $this->getEntreprise();
        return ($entreprise instanceof Entreprise ) ? ($entreprise->getSaisieManuelle()) :false;
    }

    /**
     * @return mixed
     */
    public function getSousMembres()
    {
        return $this->sousMembres;
    }

    /**
     * @param mixed $sousMemebres
     */
    public function setSousMembres($sousMembres)
    {
        $this->sousMembres = $sousMembres;
    }

    /**
     * @param mixed $sousMemebres
     */
    public function addSousMembres($sousMembres)
    {
        $this->sousMembres[$sousMembres->getIdEntreprise()] = $sousMembres;
    }

    /**
     * @return mixed
     */
    public function getIdEntrepriseParent()
    {
        return $this->idEntrepriseParent;
    }

    /**
     * @param mixed $idEntrepriseParent
     */
    public function setIdEntrepriseParent($idEntrepriseParent)
    {
        $this->idEntrepriseParent = $idEntrepriseParent;
    }

    /**
     * @return boolean
     */
    public function isHasChild()
    {
        return $this->hasChild;
    }

    /**
     * @param boolean $hasChild
     */
    public function setHasChild($hasChild)
    {
        $this->hasChild = $hasChild;
    }
}
