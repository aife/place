<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonHeliosTeletransmission;

/**
 * Skeleton subclass for representing a row from the 'Helios_teletransmission' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonHeliosTeletransmission extends BaseCommonHeliosTeletransmission {

	private $_contentHorodatagePiecej1 = null;
	private $_contentHorodatagePiecej2 = null;
	private $_contentFichierXml = null;
	private $_contentSigPiecePrincipale = null;

	public function getHorodatagePiecej1() {
		if(!$this->_contentHorodatagePiecej1) {
			$this->_contentHorodatagePiecej1 = stream_get_contents( parent::getHorodatagePiecej1());
		}
		return $this->_contentHorodatagePiecej1;
	}
	public function getHorodatagePiecej2() {
		if(!$this->_contentHorodatagePiecej2) {
			$this->_contentHorodatagePiecej2 = stream_get_contents( parent::getHorodatagePiecej2());
		}
		return $this->_contentHorodatagePiecej2;
	}
	public function getFichierXml() {
		if(!$this->_contentFichierXml) {
			$this->_contentFichierXml = stream_get_contents( parent::getFichierXml());
		}
		return $this->_contentFichierXml;
	}
	public function getSigPiecePrincipale() {
		if(!$this->_contentSigPiecePrincipale) {
			$this->_contentSigPiecePrincipale = stream_get_contents( parent::getSigPiecePrincipale());
		}
		return $this->_contentSigPiecePrincipale;
	}

} // CommonHeliosTeletransmission
