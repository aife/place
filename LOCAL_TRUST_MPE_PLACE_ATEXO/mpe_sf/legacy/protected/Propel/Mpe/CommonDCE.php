<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonDCE;

/**
 * Skeleton subclass for representing a row from the 'DCE' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonDCE extends BaseCommonDCE {

	private string $_nomAgent="";
	private $_contentHorodatage = null;

	public function getNomAgent()
	{
		return $this->_nomAgent;
	}

	public function setNomAgent($nomAgent)
	{
		$this->_nomAgent=$nomAgent;
	}
	public function getHorodatage() {
		if(!$this->_contentHorodatage) {
			$this->_contentHorodatage = stream_get_contents( parent::getHorodatage());
		}
		return $this->_contentHorodatage;
	}

} // CommonDCE
