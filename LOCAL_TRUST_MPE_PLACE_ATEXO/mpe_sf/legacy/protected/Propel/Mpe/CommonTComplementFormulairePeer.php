<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonTComplementFormulaire;

use Application\Propel\Mpe\Om\BaseCommonTComplementFormulairePeer;


/**
 * Skeleton subclass for performing query and update operations on the 't_complement_formulaire' table.
 *
 * 
 *
 * This class was autogenerated by Propel on:
 *
 * Wed Jul 17 11:59:10 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonTComplementFormulairePeer extends BaseCommonTComplementFormulairePeer {

} // CommonTComplementFormulairePeer
