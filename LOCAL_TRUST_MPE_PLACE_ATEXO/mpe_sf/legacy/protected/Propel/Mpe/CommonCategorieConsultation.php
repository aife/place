<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonCategorieConsultationQuery;




use Application\Propel\Mpe\Om\BaseCommonCategorieConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'CategorieConsultation' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonCategorieConsultation extends BaseCommonCategorieConsultation {

	public function getLibelleTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $libelle = "libelle_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$libelle){
            return $this->libelle;
        } else {
            return $this->$libelle;
        }
	}
	public static function getCategories($org)
    {
        $categorieConsultationQuery = new CommonCategorieConsultationQuery();
        $categoriesObjects = $categorieConsultationQuery->find();
        $res = array();
        if ($categoriesObjects) {
            $res[0] = "--- " . Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUTES_LES_CATEGORIES') . " ---"; 
	        foreach($categoriesObjects as $categorie) {
	            $res[$categorie->getId()] = $categorie->getLibelle();
	        }   
        }
        return $res;
    }
} // CommonCategorieConsultation
