<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Service\Atexo\Atexo_Config;
use Application\Propel\Mpe\CommonJALPeer;

use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonJAL;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Library\Propel\Propel;


/**
 * Skeleton subclass for representing a row from the 'JAL' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonJAL extends BaseCommonJAL {
	public function getEmailDestForSendPress($org,$arrayIdService, $idServiceCons){

 		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        if(!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonJALPeer::ORGANISME ,$org);
        $c->add(CommonJALPeer::SERVICE_ID ,$idServiceCons);
        if($arrayIdService && (is_countable($arrayIdService) ? count($arrayIdService) : 0)>0){
        	foreach($arrayIdService as $service) {
        		if($service != $arrayIdService[0]) {
        			$c->addOr(CommonJALPeer::SERVICE_ID ,$service);
        		}

        	}
        }
        $c->addAscendingOrderByColumn(CommonJALPeer::NOM);
        $listDest = CommonJALPeer::doSelect($c, $connexionCom);
        if ($listDest) {
            return $listDest;
        }
        else {
            return array();
        }

 	}
 	 public function getEmailByIdJAl($idJal) {
 	 	$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonJALPeer::ORGANISME ,Atexo_CurrentUser::getCurrentOrganism());
        $c->add(CommonJALPeer::ID ,$idJal);
        $listDest = CommonJALPeer::doSelectOne($c, $connexionCom);
        if($listDest) {
        	return $listDest->getEmail();
        }
 	 }
 	  public function getNomByIdJAl($idJal) {
 	 	$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonJALPeer::ORGANISME ,Atexo_CurrentUser::getCurrentOrganism());
        $c->add(CommonJALPeer::ID ,$idJal);
        $listDest = CommonJALPeer::doSelectOne($c, $connexionCom);
        if($listDest) {
        	return $listDest->getNom();
        }
 	 }
} // CommonJAL
