<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonProgrammePrevisionnelPeer;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonProgrammePrevisionnel;
use Application\Service\Atexo\Atexo_Config;


/**
 * Skeleton subclass for representing a row from the 'programme_previsionnel' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonProgrammePrevisionnel extends BaseCommonProgrammePrevisionnel {

	private $_contentHorodatage = null;
    private $_pathService;
	public function getPathService(){
		return $this->_pathService;
	}
	public function setPathService($value){
		$this->_pathService = $value;
	}
	/**
	 * retourne l liste des marchés d'organisme
	 * 
	 */
	public function retreiveProgrammesPrevisionnels($organisme,$serviceId="")
	{
		if(!$organisme) {
			return array();
		}

		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$c = new Criteria();
		$c->add(CommonProgrammePrevisionnelPeer::ORGANISME, $organisme);
		if($serviceId!="" || $serviceId == 0){
			$c->add(CommonProgrammePrevisionnelPeer::SERVICE_ID, $serviceId, Criteria::EQUAL);
		}
		$result = CommonProgrammePrevisionnelPeer::doSelect($c, $connexionCom);
		if($result) {
			return $result;
		}
		else {
			return array();
		}
	}

	/**
	 * suppression d'un marché
	 * 
	 */
	public function doDeletePP($identifiant,$organisme)
	{
		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$c = new Criteria();
		$c->add(CommonProgrammePrevisionnelPeer::ORGANISME, $organisme, Criteria::EQUAL);
		$c->add(CommonProgrammePrevisionnelPeer::ID, $identifiant, Criteria::EQUAL);
		CommonProgrammePrevisionnelPeer::doDelete($c,$connexionCom);
	}

	public function getHorodatage() {
		if(!$this->_contentHorodatage) {
			$this->_contentHorodatage = stream_get_contents( parent::getHorodatage());
		}
		return $this->_contentHorodatage;
	}

} // CommonProgrammePrevisionnel
