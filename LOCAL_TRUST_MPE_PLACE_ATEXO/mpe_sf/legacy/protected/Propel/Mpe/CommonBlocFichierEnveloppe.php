<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;




use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonBlocFichierEnveloppe;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for representing a row from the 'blocFichierEnveloppe' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonBlocFichierEnveloppe extends BaseCommonBlocFichierEnveloppe {

    public function getCommonFichierEnveloppe(PropelPDO $connexionCom = null, $doQuery = true)
	{

		$criteria = new Criteria();

		if(!$connexionCom){
        	$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
		$criteria->add(CommonFichierEnveloppePeer::ORGANISME, $this->getOrganisme());
		$criteria->add(CommonFichierEnveloppePeer::ID_FICHIER, $this->id_fichier);

		$fichierEnveloppe = CommonFichierEnveloppePeer::doSelectOne($criteria,$connexionCom);
		return !empty($fichierEnveloppe) ? $fichierEnveloppe : null;
	}
} // CommonBlocFichierEnveloppe
