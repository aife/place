<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonTDumeContexteQuery;
use Application\Service\Atexo\Atexo_Config;


/**
 * Skeleton subclass for performing query and update operations on the 't_dume_contexte' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTDumeContexteQuery extends BaseCommonTDumeContexteQuery
{
    /**
     * Permet de retourner l'objet t_dume_contexte
     *
     * @param $reference
     * @param $organisme
     * @param $type
     * @param null $isStandard
     * @param null $connexion
     * @return CommonTDumeContexte
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @copyright Atexo 2018
     */
    public function getTDumeContexteByType($reference,$organisme ,$type,$isStandard = null,$connexion = null){
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $query = static::create()->filterByRefConsultation($reference)->filterByOrganisme($organisme)->filterByTypeDume($type);
        if($isStandard !== null){
            $query->filterByIsStandard($isStandard);
        }
        return $query->orderById(Criteria::DESC)->findOne($connexion);

    }


}
