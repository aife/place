<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonEnchereOffre;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use PDO;


/**
 * Skeleton subclass for representing a row from the 'EnchereOffre' table.
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */
class CommonEnchereOffre extends BaseCommonEnchereOffre
{

    protected $collCommonEnchereOffreReferences;
    protected $aCommonEncherePmi;
    protected $aCommonEnchereEntreprisePmi;

    //TODO unused function(commented in EntrepriseParticipationEnchere line 740)
    public static function getSimuRang($idEnchereEntreprise, $idEnchere, $noteGlobalCandidat, $con = null)
    {
        $org = null;
        if ($noteGlobalCandidat == null) {
            return null;
        }

        if ($con == null) {
            $con = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
            );
        }

        $sql = "SELECT valeurNGC, MAX(date) FROM EnchereOffre
        WHERE idEnchere = '" . (new Atexo_Db())->quote($idEnchere) . "'
        AND idEnchereEntreprise <> '" . (new Atexo_Db())->quote($idEnchereEntreprise) . "'
        AND organisme='" . $org . "'
        GROUP BY idEnchereEntreprise
        ORDER BY valeurNGC ASC";


        $stmt = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_NUM);

        $rang = 1;
        if ($rs) {
            foreach ($rs as $row) {
                if ($row[0] < $noteGlobalCandidat) {
                    $rang++;
                }
            }
        }
        return $rang;
    }


    /**
     * Renvoie la dernière EnchereOffre de chaque entreprise, classé par note
     * (meilleure en premier)
     *
     * @return array
     */
    public static function getLastOffreOrdered($idEnchere, $tri, $org, $connexion = null)
    {
        // Requete Imbriquée version MySQL4...
        $tmpTable = "CREATE TEMPORARY TABLE lastoffre_tmp (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
						idEnchereEntreprise int(11),
						organisme  varchar( 30 ) ,
						maximum datetime,
                        PRIMARY KEY (`id`)
						)";
        $lock = "LOCK TABLES EnchereOffre READ";
        $insert = "INSERT INTO lastoffre_tmp(`idEnchereEntreprise`, `organisme`, `maximum`)
        SELECT idEnchereEntreprise,organisme, MAX(date) FROM EnchereOffre
        WHERE idEnchere = " . (new Atexo_Db())->quote($idEnchere) . " AND organisme='" . $org . "'
        GROUP BY idEnchereEntreprise";
        $requete = "SELECT EnchereOffre.* FROM EnchereOffre, lastoffre_tmp
        WHERE EnchereOffre.idEnchereEntreprise=lastoffre_tmp.idEnchereEntreprise AND EnchereOffre.organisme=lastoffre_tmp.organisme
        AND EnchereOffre.date=lastoffre_tmp.maximum AND EnchereOffre.organisme='" . $org . "'
        ORDER BY valeurNGC $tri";
        $unlock = "UNLOCK TABLES";
        $delTmpTable = "DROP TABLE lastoffre_tmp";


        if (empty($connexion)) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
            );
        }
        $stmt = $connexion->prepare($tmpTable);
        $stmt->execute();
        $stmt = $connexion->prepare($lock);
        $stmt->execute();
        $stmt = $connexion->prepare($insert);
        $stmt->execute();
        $rs = $connexion->prepare($requete);
        $rs->execute();
        $stmt = $connexion->prepare($unlock);
        $stmt->execute();
        $stmt = $connexion->prepare($delTmpTable);
        $stmt->execute();


        $tmpEnchereOffres = CommonEnchereOffrePeer::populateObjects($rs);
        $currentRang = 1;
        foreach ($tmpEnchereOffres as $enchereOffre) {
            $enchereOffre->setRang($currentRang);
            $currentRang++;
        }
        return $tmpEnchereOffres;
    }


    /**
     * Renvoie la dernière EnchereOffre de chaque entreprise, classé par note
     * (meilleure en premier)
     *
     * @return array
     */
    public static function getLastOffreOrderedByIdEnchereEntreprise($idEnchereEntreprise, $tri, $org, $connexion = null)
    {
        // Requete Imbriquée version MySQL4...
        $tmpTable = "CREATE TEMPORARY TABLE lastoffre_tmp (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
						idEnchereEntreprise int(11),
						organisme  varchar( 30 ) ,
						maximum datetime,
                        PRIMARY KEY (`id`)
						)";
        $lock = "LOCK TABLES EnchereOffre READ";
        $insert = "INSERT INTO lastoffre_tmp(`idEnchereEntreprise`, `organisme`, `maximum`)
        SELECT idEnchereEntreprise,organisme, MAX(date) FROM EnchereOffre
        WHERE idEnchereEntreprise = " . (new Atexo_Db())->quote($idEnchereEntreprise) . " AND organisme='" . $org . "'
        GROUP BY idEnchereEntreprise";
        $requete = "SELECT EnchereOffre.* FROM EnchereOffre, lastoffre_tmp
        WHERE EnchereOffre.idEnchereEntreprise=lastoffre_tmp.idEnchereEntreprise AND EnchereOffre.organisme=lastoffre_tmp.organisme
        AND EnchereOffre.date=lastoffre_tmp.maximum AND EnchereOffre.organisme='" . $org . "'
        ORDER BY valeurNGC $tri";
        $unlock = "UNLOCK TABLES";
        $delTmpTable = "DROP TABLE lastoffre_tmp";


        if (empty($connexion)) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
            );
        }
        $stmt = $connexion->prepare($tmpTable);
        $stmt->execute();
        $stmt = $connexion->prepare($lock);
        $stmt->execute();
        $stmt = $connexion->prepare($insert);
        $stmt->execute();
        $rs = $connexion->prepare($requete);
        $rs->execute();
        $stmt = $connexion->prepare($unlock);
        $stmt->execute();
        $stmt = $connexion->prepare($delTmpTable);
        $stmt->execute();


        $tmpEnchereOffres = CommonEnchereOffrePeer::populateObjects($rs);
        $currentRang = 1;
        foreach ($tmpEnchereOffres as $enchereOffre) {
            $enchereOffre->setRang($currentRang);
            $currentRang++;
        }
        return $tmpEnchereOffres;
    }


    /**
     * Renvoie la dernière EnchereOffre de chaque entreprise
     *
     * @return array
     */
    public static function getLastOffreUnOrdered($idEnchere, $org)
    {
        $tmpTable = "CREATE TEMPORARY TABLE lastoffre_tmp (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
						idEnchereEntreprise int(11),
						organisme  varchar( 30 ) ,
						maximum datetime,
                        PRIMARY KEY (`id`)
						)";
        $lock = "LOCK TABLES EnchereOffre READ";
        $insert = "INSERT INTO lastoffre_tmp(`idEnchereEntreprise`, `organisme`, `maximum`)
        SELECT idEnchereEntreprise,organisme,  MAX(date) FROM EnchereOffre
        WHERE idEnchere = " . (new Atexo_Db())->quote($idEnchere) . " AND organisme='" . $org . "'
        GROUP BY idEnchereEntreprise";
        $requete = "SELECT EnchereOffre.* FROM EnchereOffre, lastoffre_tmp
					WHERE EnchereOffre.idEnchereEntreprise=lastoffre_tmp.idEnchereEntreprise  AND EnchereOffre.organisme=lastoffre_tmp.organisme
					AND EnchereOffre.date=lastoffre_tmp.maximum  AND EnchereOffre.organisme='" . $org . "'
					ORDER BY EnchereOffre.idEnchereEntreprise";
        $unlock = "UNLOCK TABLES";
        $delTmpTable = "DROP TABLE lastoffre_tmp";

        $stmt = Atexo_Db::getLinkCommon()->prepare($tmpTable);
        $stmt->execute();
        $stmt = Atexo_Db::getLinkCommon()->prepare($lock);
        $stmt->execute();
        $stmt = Atexo_Db::getLinkCommon()->prepare($insert);
        $stmt->execute();
        $rs = Atexo_Db::getLinkCommon()->prepare($requete);
        $rs->execute();
        $stmt = Atexo_Db::getLinkCommon()->prepare($unlock);
        $stmt->execute();
        $stmt = Atexo_Db::getLinkCommon()->prepare($delTmpTable);
        $stmt->execute();

        $tmpEnchereOffres = CommonEnchereOffrePeer::populateObjects($rs);
        foreach ($tmpEnchereOffres as $enchereOffre) {
            $enchereOffre->setRang(0);
        }
        return $tmpEnchereOffres;
    }


    /**
     * Renvoi la dernière offre de l'entreprise d'id $idEntreprise
     *
     * @param int $idEnchere
     * @param int $idEntreprise
     * @param bool $displayRang
     * @return CommonEnchereOffre
     */
    public static function getMyOffre($idEnchere, $idEntreprise, $displayRang, $org, $tri = 'DESC')
    {
        $con = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
        );

        $lastOffres = self::getLastOffreOrdered($idEnchere, $tri, $org);
        foreach ($lastOffres as $offre) {
            if ($offre->getCommonEnchereEntreprisePmi($con)->getId() == $idEntreprise) {
                if (!$displayRang) {
                    $offre->setRang(0);
                }
                return array($offre);
            }
        }
        return null;
    }


    /**
     * Renvoi la dernière offre de l'entreprise d'id $idEntreprise
     * et la meilleur
     *
     * @param int $idEnchere
     * @param int $idEntreprise
     * @param bool $displayRang
     * @param string $tri
     * @return array
     */
    public static function getBestAndMyOffre($idEnchere, $idEntreprise, $displayRang, $tri, $org)
    {
        $lastOffres = self::getLastOffreOrdered($idEnchere, $tri, $org);
        $bestOffre = $lastOffres[0];
        $myTmpOffre = self::getMyOffre($idEnchere, $idEntreprise, $displayRang, $org, $tri);
        $myOffre = $myTmpOffre[0];
        if (!isset($myOffre) && !isset($bestOffre)) {
            return; // Si ni offre ni meilleure offre, alors on retourne vide
        }
        if (!isset($myOffre)) {
            return array($bestOffre); // si il n'y a que une meilleure offre, on la retourne
        }
        if ($myOffre->getId() == $bestOffre->getId()) {
            $myOffre->setRang(1);
            return array($myOffre);
        } else {
            return array($bestOffre, $myOffre);
        }
    }

    /**
     * retourne la meilleur Offre de l'enchere $idEnchere
     *
     * @param int $idEnchere
     * @return Enchereoffre
     */
    public static function getBestOffre($idEnchere, $tri, $org, $connexionCom = null)
    {
        $lastOffres = self::getLastOffreOrdered($idEnchere, $tri, $org, $connexionCom);
        $bestOffre = $lastOffres[0];
        if ($bestOffre) {
            return $bestOffre;
        } else {
            return null;
        }
    }

    /**
     * retourne la meilleur Offre de l'enchere $idEnchere
     *
     * @param int $idEnchere
     * @return Enchereoffre
     */
    public static function getBestOffreNonObligatoire($idEnchereEntreprise, $tri, $org, $connexionCom = null)
    {
        $lastOffres = self::getLastOffreOrderedByIdEnchereEntreprise($idEnchereEntreprise, $tri, $org, $connexionCom);
        $bestOffre = $lastOffres[0];
        if ($bestOffre) {
            return $bestOffre;
        } else {
            return null;
        }
    }

    public static function getMinTIC($idEnchere, $idEntreprise, $org, $con = null)
    {
        if (empty($con)) {
            $con = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
            );
        }
        $tmpTable = "CREATE TEMPORARY TABLE lastoffre_tmp (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
						idEnchereEntreprise int(11),
						organisme  varchar( 30 ) ,
						maximum datetime,
                        PRIMARY KEY (`id`)
						)";
        $lock = "LOCK TABLES EnchereOffre READ";
        $insert = "INSERT INTO lastoffre_tmp(`idEnchereEntreprise`, `organisme`, `maximum`)
        SELECT idEnchereEntreprise, organisme, MAX(date) FROM EnchereOffre
        WHERE idEnchereEntreprise <> " . (new Atexo_Db())->quote($idEntreprise) . " AND organisme='" . $org . "'
        AND idEnchere = " . (new Atexo_Db())->quote($idEnchere) . "
        GROUP BY idEnchereEntreprise";
        $requete = "SELECT MIN(EnchereOffre.valeurTIC) FROM EnchereOffre, lastoffre_tmp
					WHERE EnchereOffre.idEnchereEntreprise=lastoffre_tmp.idEnchereEntreprise  AND EnchereOffre.organisme=lastoffre_tmp.organisme
					AND EnchereOffre.date=lastoffre_tmp.maximum AND EnchereOffre.organisme='" . $org . "'
					ORDER BY valeurNGC ASC";
        $unlock = "UNLOCK TABLES";
        $delTmpTable = "DROP TABLE lastoffre_tmp";

        $stmt = $con->prepare($tmpTable);
        $stmt->execute();
        $stmt = $con->prepare($lock);
        $stmt->execute();
        $stmt = $con->prepare($insert);
        $stmt->execute();
        $stmt = $con->prepare($requete);
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_NUM);
        $stmt = $con->prepare($unlock);
        $stmt->execute();
        $stmt = $con->prepare($delTmpTable);
        $stmt->execute();

        if ($rs) {
            foreach ($rs as $row) {
                return $row[0];
            }
        } else {
            return null;
        }
    }

    /**
     * Met a jour les notes (relatives !) NETC et NGC des entreprises dans le cas
     * du bareme relatif pour l'enchère
     *
     * @param CommonEncherePmi $enchere : l'enchere en cours
     * @param unknown_type $idEntreprise : id de l'entreprise au TICminimum (à ne pas mettre à jour...)
     * @param unknown_type $ticMin : le nouveau TIC minimum
     */
    public static function updateRelativesNotes(CommonEncherePmi $enchere, $idEntreprise, $ticMin, $org, $con = null)
    {
        if (empty($con)) {
            $con = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
            );
        }

        $noteMaxBaremeRelatif = $enchere->getNotemaxbaremerelatif();
        $idEnchere = $enchere->getId();

        $tmpTable = "CREATE TEMPORARY TABLE lastoffre_tmp (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
						idEnchereEntreprise int(11),
						organisme  varchar( 30 ) ,
						maximum datetime,
                        PRIMARY KEY (`id`)
						)";
        $insert = "INSERT INTO lastoffre_tmp(`idEnchereEntreprise`, `organisme`, `maximum`)
        SELECT idEnchereEntreprise, organisme, MAX(date) FROM EnchereOffre
        WHERE idEnchereEntreprise <> " . (new Atexo_Db())->quote($idEntreprise) . " AND organisme='" . $org . "'
        AND idEnchere = " . (new Atexo_Db())->quote($idEnchere) . "
        GROUP BY idEnchereEntreprise";
        $requete = "SELECT EnchereOffre.id, EnchereOffre.valeurTIC, EnchereOffre.idEnchereEntreprise FROM EnchereOffre, lastoffre_tmp
					WHERE EnchereOffre.idEnchereEntreprise=lastoffre_tmp.idEnchereEntreprise AND EnchereOffre.organisme=lastoffre_tmp.organisme
					AND EnchereOffre.date=lastoffre_tmp.maximum AND EnchereOffre.organisme='" . $org . "'
					ORDER BY valeurNGC ASC";
        $delTmpTable = "DROP TABLE lastoffre_tmp";

        $stmt = $con->prepare($tmpTable);
        $stmt->execute();
        $stmt = $con->prepare($insert);
        $stmt->execute();
        $stmt = $con->prepare($requete);
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_NUM);

        foreach ($rs as $row) {
            $tic = $row[1];
            $noteEnchereTotalCandidat = $noteMaxBaremeRelatif * $ticMin / $tic;
            $enchereOffreCourante = CommonEnchereOffrePeer::retrieveByPK($row[0], $con);
            $entreprise = CommonEnchereEntreprisePmiPeer::retrieveByPK($row[2]);

            // Calcul de la note globales :
            // Si Bareme somme pondérée (par coeff a et b) des 2 notes NETC et Note Technique du Candidat
            if (strcmp(
                    $enchere->getTypebaremeenchereglobale(),
                    Atexo_Config::getParameter('BAREME_GLOBAL_SOMME_PONDEREE')
                ) == 0) {
                $noteGlobaleCandidat = $noteEnchereTotalCandidat * $enchere->getCoeffa(
                    ) + $entreprise->getNotetechnique() * $enchere->getCoeffb();
            } // Si Bareme corrigé par note technique
            else {
                if (strcmp(
                        $enchere->getTypebaremeenchereglobale(),
                        Atexo_Config::getParameter('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE')
                    ) == 0) {
                    $noteGlobaleCandidat = $noteEnchereTotalCandidat * (1 - $enchere->getCoeffc(
                            ) * $entreprise->getNotetechnique());
                } else {
                    $noteGlobaleCandidat = null;
                }
            }

            $enchereOffreCourante->setValeurnetc($noteEnchereTotalCandidat);
            $enchereOffreCourante->setValeurngc($noteGlobaleCandidat);
            $enchereOffreCourante->save($con);
        }

        //$stmt->executeQuery($delTmpTable, ResultSet::FETCHMODE_NUM);
        $stmt = $con->prepare($delTmpTable);
        $stmt->execute();
    }

    /**
     * Met à jours les notes NETC et NGC de tout les enchérisseurs de l'enchère
     * dans le cas ou l'entrprise d'id $idEntreprise a amélioré (au moins) une offre sur une
     * référence en barème relatif
     *
     * @param string $tri : ASC ou DESC
     * @param int $idEntreprise
     * @param array $valeureMin
     * @param connexion $con
     */
    public static function updateRelativesNotesReferences(
        CommonEncherePmi $enchere,
        $tri,
        $idEntreprise,
        $newNoteGlobaleCandidat,
        $valeureMin,
        $org,
        $con = null
    ) {
        $lastOffres = CommonEnchereOffre::getLastOffreOrdered($enchere->getId(), $tri, $org, $con);

        $con = $con ?: Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $saveNeeded = true;
        foreach ($lastOffres as $offreToUpdate) {
            if ($offreToUpdate->getCommonEnchereEntreprisePmi($con)->getId() == $idEntreprise) {
                // Pas besoins de mettre l'offre de l'entreprise ayant f
                continue;
            }

            // On récupère chaque offre sur chaque référence pour recalculer les notes global et NETC
            // Le TIC et le TC ne sont pas changés ...
            $noteEnchereTotalCandidat = 0;
            foreach ($offreToUpdate->getCommonEnchereOffreReferences(null, $con) as $enchereOffreReference) {
                $reference = $enchereOffreReference->getCommonEnchereReference($con);
                $valeurReferenceANoterCandidat = $enchereOffreReference->getValeur() * $reference->getQuantite();

                // Bareme applicable à la référence
                // Si bareme prix référence
                if (strcmp(
                        $reference->getTypebaremereference(),
                        Atexo_Config::getParameter('BAREME_REFERENCE_PRIX_REFERENCE')
                    ) == 0) {
                    $noteReferenceCandidat = $valeurReferenceANoterCandidat;
                } // Si bareme par tranche
                else {
                    if (strcmp(
                            $reference->getTypebaremereference(),
                            Atexo_Config::getParameter('BAREME_REFERENCE_TRANCHES')
                        ) == 0) {
                        $tranches = $reference->getCommonEnchereTrancheBaremeReferences(null, $con);
                        foreach ($tranches as $tranche) {
                            if ($tranche->getBorneinf(
                                ) < $valeurReferenceANoterCandidat && $valeurReferenceANoterCandidat <= $tranche->getBornesup(
                                )) {
                                $noteReferenceCandidat = $tranche->getNote();
                            }
                        }
                    } // Si bareme relatif (le cas qui diffère)
                    else {
                        if (strcmp(
                                $reference->getTypebaremereference(),
                                Atexo_Config::getParameter('BAREME_REFERENCE_RELATIF')
                            ) == 0) {
                            //$newOffreReference = $enchereOffreReferences[$reference->getId()];
                            $noteReferenceCandidat = $reference->getNotemaxbaremerelatif(
                                ) * $valeureMin[$reference->getId()] / $enchereOffreReference->getValeur();
                        }
                    }
                }

                // Calcul de la NETC (bareme somme pondéré... les autres etant basé sur le TIC) :
                $noteEnchereTotalCandidat += $noteReferenceCandidat * $reference->getPonderationnotereference();
            }

            // Calcul de la note globales :
            // Si Bareme somme pondérée (par coeff a et b) des 2 notes NETC et Note Technique du Candidat
            if (strcmp(
                    $enchere->getTypebaremeenchereglobale(),
                    Atexo_Config::getParameter('BAREME_GLOBAL_SOMME_PONDEREE')
                ) == 0) {
                $noteGlobaleCandidat = $noteEnchereTotalCandidat * $enchere->getCoeffa(
                    ) + $offreToUpdate->getCommonEnchereEntreprisePmi($con)->getNotetechnique() * $enchere->getCoeffb();
            } // Si Bareme corrigé par note technique
            else {
                if (strcmp(
                        $enchere->getTypebaremeenchereglobale(),
                        Atexo_Config::getParameter('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE')
                    ) == 0) {
                    $noteGlobaleCandidat = $noteEnchereTotalCandidat * (1 - $enchere->getCoeffc(
                            ) * $offreToUpdate->getCommonEnchereEntreprisePmi($con)->getNotetechnique());
                } else {
                    $noteGlobaleCandidat = null;
                }
            }


            $offreToUpdate->setValeurngc($noteGlobaleCandidat);
            $offreToUpdate->setValeurnetc($noteEnchereTotalCandidat);

            if ($enchere->getMeilleureenchereobligatoire()) {
                if ($enchere->getMeilleurnotehaute() == '1') {
                    $meilleureOffre = -1;
                } else {
                    $meilleureOffre = 1;
                }
                if ($meilleureOffre * $noteGlobaleCandidat < $meilleureOffre * $newNoteGlobaleCandidat) {
                    $saveNeeded = false;
                }
            }
        }

        // Si l'offre est valide (si elle est effectivement meilleure que les autres)
        if ($saveNeeded) {
            foreach ($lastOffres as $offreToUpdate) {
                $offreToUpdate->save($con);
            }
        }
    }

    public function getCommonEnchereEntreprisePmi(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonEnchereEntreprisePmi === null && ($this->idenchereentreprise !== null) && ($this->organisme !== null)) {
            $this->aCommonEnchereEntreprisePmi = CommonEnchereEntreprisePmiPeer::retrieveByPK(
                $this->idenchereentreprise,
                $con
            );
        }
        return $this->aCommonEnchereEntreprisePmi;
    }

    public function getEcartAbsolu($con)
    {
        return $this->getValeurtc() - $this->getCommonEncherePmi($con)->getMontantreserve();
    }

    public function getEcartRelatif($con)
    {
        return floor($this->getEcartAbsolu($con) / $this->getCommonEncherePmi($con)->getMontantreserve() * 100);
    }

    public function getCommonEncherePmi(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonEncherepmi === null && ($this->idenchere !== null) && ($this->organisme !== null)) {
            $this->aCommonEncherepmi = CommonEncherePmiPeer::retrieveByPK($this->idenchere, $con);
        }
        return $this->aCommonEncherepmi;
    }

    public function addCommonEnchereOffreReference(CommonEnchereOffreReference $l)
    {
        $this->collCommonEnchereOffreReferences[] = $l;
        $l->setCommonEnchereOffre($this);
    }

    public function getCommonEnchereOffreReferences($criteria = null, $con = null)
    {
        if ($criteria === null) {
            $criteria = new Criteria();
        } elseif ($criteria instanceof Criteria) {
            $criteria = clone $criteria;
        }

        $criteria->add(CommonEnchereOffreReferencePeer::ID_ENCHERE_OFFRE, $this->getId());
        $criteria->add(CommonEnchereOffreReferencePeer::ORGANISME, $this->getOrganisme());

        $this->collCommonEnchereOffreReferences = CommonEnchereOffreReferencePeer::doSelect($criteria, $con);

        return $this->collCommonEnchereOffreReferences;
    }

    public function setCommonEncherePmi(CommonEncherePmi $v = null)
    {
        if ($v === null) {
            $this->setIdenchere('0');
        } else {
            $this->setIdenchere($v->getId());
        }
        $this->aCommonEncherePmi = $v;
    }

    public function setCommonEnchereEntreprisePmi(CommonEnchereEntreprisePmi $v = null)
    {
        if ($v === null) {
            $this->setIdenchereentreprise('0');
        } else {
            $this->setIdenchereentreprise($v->getId());
        }
        $this->aCommonEnchereEntreprisePmi = $v;
    }
} // CommonEnchereOffre
