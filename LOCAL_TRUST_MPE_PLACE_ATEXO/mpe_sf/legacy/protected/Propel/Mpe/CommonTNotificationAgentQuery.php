<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonTNotificationAgent;



use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonTNotificationAgentQuery;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for performing query and update operations on the 't_notification_agent' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTNotificationAgentQuery extends BaseCommonTNotificationAgentQuery
{

    /**
     * permet de recuperer les notifications actifs d'un agent
     *
     * @param $idAgent
     * @param $connexion
     * @return array CommonTNotificationAgent
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function getNotificationActifByIdAgent($idAgent,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        return (array) $this->filterByIdAgent($idAgent)->filterByNotificationActif('1')->orderByNotificationLue('ASC')->orderByDateCreation('DESC')->find($connexion);
    }

    /**
     * permet de recuperer les notifications actifs d'un agent
     *
     * @param $idAgent
     * @param $connexion
     * @return array CommonTNotificationAgent
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function getNombreNotificationActifByIdAgent($idAgent,$notificationLue = null,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $this->filterByIdAgent($idAgent);
        $this->filterByNotificationActif('1');
        if($notificationLue !== null){
            $this->filterByNotificationLue($notificationLue);
        }
        return $this->count($connexion);
    }

    /**
     * permet de recuperer la notifications by son id
     *
     * @param $idNotification
     * @param $connexion
     * @return array CommonTNotificationAgent
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function getNotificationById($idNotification,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        return $this->findOneByIdNotification($idNotification,$connexion);
    }
}
