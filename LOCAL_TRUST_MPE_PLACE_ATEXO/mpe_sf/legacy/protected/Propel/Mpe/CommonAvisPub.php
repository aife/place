<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\Om\BaseCommonAvisPub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Prado\Prado;


/**
 * Skeleton subclass for representing a row from the 'Avis_Pub' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAvisPub extends BaseCommonAvisPub {

    /**
     * Retourne le statut de l'avis
     */
    public function getLibelleStatut($objetPub = null)
    {
    	if(!$objetPub) {
    		return (new Atexo_Publicite_AvisPub())->getStatutAvisEtSupports($this->statut);    
    	} else if($objetPub instanceof CommonHistoriqueAvisPub) {
    	    if($objetPub->getTypeHistorique() == '0') {
    			return (new Atexo_Publicite_AvisPub())->getStatutAvisEtSupports($objetPub->getDetailStatut());        
    	    } else {
    	    	return (new Atexo_Publicite_AvisPub())->getLibelleEtatDestinataire($objetPub->getDetailStatut());    
    	    }
    	} else {
    		return Prado::localize('NON_RENSEIGNE');
    	}
    }

} // CommonAvisPub
