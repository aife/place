<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonEnchereReferencePeer;

use Application\Propel\Mpe\CommonEnchereOffrePeer;




use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonEncherePmi;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'EncherePmi' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEncherePmi extends BaseCommonEncherePmi {

	protected $collCommonEnchereOffres = null;
    public function getCommonEnchereEntreprisePmis($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonEnchereEntreprisePmiPeer::IDENCHERE, $this->getId());
		$criteria->add(CommonEnchereEntreprisePmiPeer::ORGANISME, $this->getOrganisme());
		return CommonEnchereEntreprisePmiPeer::doSelect($criteria, $con);
	}

	public function getCommonEncherereferences($criteria = null, $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonEnchereReferencePeer::IDENCHERE, $this->getId());
		$criteria->add(CommonEnchereReferencePeer::ORGANISME, $this->getOrganisme());

		return CommonEnchereReferencePeer::doSelect($criteria, $con);
	}

	public function getCommonEncheretranchesbaremenetcs($criteria = null, $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonEnchereTranchesBaremeNETCPeer::IDENCHERE, $this->getId());
		$criteria->add(CommonEnchereTranchesBaremeNETCPeer::ORGANISME, $this->getOrganisme());


		return CommonEnchereTranchesBaremeNETCPeer::doSelect($criteria, $con);
	}

	public function getTsTempsRestant()
    {
        $tsTempsRestant = null;
								// TEST
        //$enchere = EncherepmiPeer::retrieveByPK($this->getId());

        $tsNow = time();
        $dateSuspention = $this->getDatesuspension();
        $tsDateDebut = strtotime($this->getDatedebut());
        $tsDateFin = strtotime($this->getDatefin());
        $tsDateFinAccessible = $tsDateFin + 60 * 60 * 24;

        if (strcmp($dateSuspention, '0000-00-00 00:00:00') == 0) {
            $tsSuspention = null;
        } else  {
            $tsSuspention = strtotime($this->getDatesuspension());
        }
        $tempsRestant = array();
        if ($tsSuspention != null) {
            $tsTempsRestant = $tsDateFin - $tsSuspention;
            $tempsRestant['css'] = 'text-warning text-big';
        } else if ($tsDateDebut < $tsNow && $tsNow < $tsDateFin) {
            $tsTempsRestant = $tsDateFin - time();
            $tempsRestant['css'] = 'text-success text-big';
        } else if ($tsNow < $tsDateDebut) {
            $tsTempsRestant = $tsDateDebut - $tsNow;
            $tempsRestant['css'] = 'text-danger text-big';
        } else if ($tsDateFin < $tsNow && $tsNow < $tsDateFinAccessible) {
            $tsTempsRestant = $tsDateFinAccessible - $tsNow;
            $tempsRestant['css'] = 'text-danger text-big';
        } else  if ($tsDateFinAccessible < $tsNow){
            $tsTempsRestant = null;
            $tempsRestant['css'] = 'text-danger text-big';
        }

        if ($tsTempsRestant != null) {
	        $secondes = $tsTempsRestant%60;
	        $totalMinutes = (($tsTempsRestant - $secondes) / 60);
	        $minutes = $totalMinutes%60;
	        $heures = ($totalMinutes - $minutes) / 60;

	        $str = "";
	        if ($heures > 0) {
	            $str .= $heures . " heure(s) ";
	        }
	        if ($minutes > 0) {
	            $str .= $minutes . " min ";
	        }
	        $str .= $secondes . " sec";
	        $tempsRestant['text'] = $str;
        } else {
            $tempsRestant['text'] = "-";
        }
        return $tempsRestant;
   }

    public function getStatusEnchere($option = null)
    {
        $status = [];
								// TEST
        //$enchere = EncherepmiPeer::retrieveByPK($this->getId());
        $tsNow = time();
        $dateSuspention = $this->getDatesuspension();
        if (strcmp($dateSuspention, '0000-00-00 00:00:00') == 0) {
            $tsSuspention = null;
        } else  {
            $tsSuspention = strtotime($this->getDatesuspension());
        }

        $tsDateDebut = strtotime($this->getDatedebut());
        $tsDateFin = strtotime($this->getDatefin());

        if ($tsSuspention != null) {
            $status['text'] = Prado::localize('SUSPENDUE');
            $status['css'] = 'text-warning';
        } else if ($tsDateDebut < $tsNow && $tsNow < $tsDateFin) {
            $status['text'] = Prado::localize('EN_COURS');
            $status['css'] = 'text-success';
        } else if ($tsNow < $tsDateDebut) {
            $status['text'] = Prado::localize('EN_ATTENTE');
            $status['css'] = 'text-secondary';
        } else if ($tsDateFin < $tsNow) {
            $status['text'] = Prado::localize('CLOTUREE');
            $status['css'] = 'text-danger';
        } else {
            $status['text'] = "-";
            $status['css'] = "";
        }

        // applé de la page recherche
        if ($option == 'css') {
            return $status['css'];
        } else if ($option == 'text') {
            return $status['text'];
        } else {
            $status['css'] .= ' text-big';
            return $status;
        }


    }

    public function isBidAllowed()
    {
        // TEST
        //$enchereUpdated = EncherepmiPeer::retrieveByPK($this->getId());

        $tsNow = time();
        $dateSuspention = $this->getDatesuspension();
        $tsDateDebut = strtotime($this->getDatedebut());
        $tsDateFin = strtotime($this->getDatefin());

        if (strcmp($dateSuspention, '0000-00-00 00:00:00') != 0) {
            return false;
        }
        if ($tsDateDebut < $tsNow && $tsNow < $tsDateFin) {
            return true;
        } else {
            return false;
        }

    }

    public function isCloturee()
    {
        $tsNow = time();
        $dateSuspention = $this->getDatesuspension();
        $tsDateFin = strtotime($this->getDatefin());

        if (strcmp($dateSuspention, '0000-00-00 00:00:00') != 0) {
            return false;
        }
        if ($tsDateFin < $tsNow) {
            return true;
        } else {
            return false;
        }
    }

    public function getDateLimiteAccessible()
    {
        $tsDateFin = strtotime($this->getDatefin());
        $tsDateFinAccessible = $tsDateFin + 60 * 60 * 24;
        return strftime("%d/%m/%Y %H:%M", $tsDateFinAccessible);
    }

    public function isAccessible()
    {
        $tsNow = time();
        $tsDateFin = strtotime($this->getDatefin());
        $tsDateFinAccessible = $tsDateFin + 60 * 60 * 24;
        if ($tsNow > $tsDateFinAccessible) {
            return false;
        } else {
            return true;
        }
    }

    public function reprendre($connexionCom)
    {
        // Si l'enchere n'est pas suspendu, on ne fait rien 
        if (strcmp($this->getDatesuspension(), "0000-00-00 00:00:00") == 0) {
            return;
        }

        // Sinon on met a jour la date de fin
        $tsNow = time();
        $tsDateFin = strtotime($this->getDatefin());
        $tsDateSuspension = strtotime($this->getDatesuspension());

        $tsNewDateFin = $tsDateFin + ( $tsNow - $tsDateSuspension);

        $this->setDatefin(strftime("%Y-%m-%d %H:%M:%S", $tsNewDateFin));
        $this->setDatesuspension("0000-00-00 00:00:00");
        $this->save($connexionCom);
    }

    public function suspendre($connexionCom)
    {
        // Si l'enchere est deja suspendu, on ne fait rien 
        if (strcmp($this->getDatesuspension(), "0000-00-00 00:00:00") != 0) {
            return;
        }

        // Si l'enchere n'est pas commencée, on ne fait rien
        $tsNow = time();
        $tsDateDebut = strtotime($this->getDatedebut());
        if ($tsNow <= $tsDateDebut) {
            return;
        }

        $isoDateNow = strftime("%Y-%m-%d %H:%M:%S", time());
        $this->setDatesuspension($isoDateNow);
        $this->save($connexionCom);
    }

    public function displayDateAccessible()
    {
        $tsNow = time();
        $dateSuspention = $this->getDatesuspension();
        $tsDateFin = strtotime($this->getDatefin());
        $tsDateFinAccessible = $tsDateFin + 3600 * 24;

        if (strcmp($dateSuspention, '0000-00-00 00:00:00') != 0) {
            return false;
        }
        if ($tsDateFin < $tsNow && $tsNow < $tsDateFinAccessible) {
            return true;
        } else {
            return false;
        }
    }

    public function getLibelleBaremeNETC()
    {
        if ($this->getTypebaremenetc() == Atexo_Config::getParameter('BAREME_ENCHERE_RELATIF')) {
            return Prado::localize('BAREME_RELATIF_LIBELLE');
        } else if ($this->getTypebaremenetc() == Atexo_Config::getParameter('BAREME_ENCHERE_TRANCHES')) {
            return Prado::localize('BAREME_TRANCHES_LIBELLE');
        } else if ($this->getTypebaremenetc() == Atexo_Config::getParameter('BAREME_ENCHERE_PRIX_REFERENCE')) {
            return Prado::localize('BAREME_PRIX_REFERENCE_LIBELLE');
        } else if ($this->getTypebaremenetc() == Atexo_Config::getParameter('BAREME_ENCHERE_SOMME_PONDEREE')) {
            return Prado::localize('BAREME_SOMME_PONDEREE_LIBELLE');
        } else {
            return "";
        }
    }

    public function getLibelleBaremeGlobal()
    {
        if ($this->getTypebaremeenchereglobale() == Atexo_Config::getParameter('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE')) {
            return Prado::localize('BAREME_GLOBAL_CORRIGE_NOTE_TECHNIQUE_LIBELLE');
        } else if ($this->getTypebaremeenchereglobale() == Atexo_Config::getParameter('BAREME_GLOBAL_SOMME_PONDEREE')) {
            return Prado::localize('BAREME_GLOBAL_SOMME_PONDEREE_LIBELLE');
        } else {
            return "";
        }
    }


    public function getObjetSummary()
    {
        return substr($this->getObjet(), 0, 60);
    }

    public function clean($connexionCom)
    {    
        // Suppression des references :
        $referencesToDelete = $this->getCommonEncherereferences(null, $connexionCom);
        $reference = "";
        foreach ($referencesToDelete as $reference) {
        	$id = $reference->getId();
        	CommonEnchereOffreReferenceQuery::create()
			->filterByIdencherereference($id)
			->delete($connexionCom);

			CommonEnchereTrancheBaremeReferenceQuery::create()
			->filterByIdreference($id)
			->delete($connexionCom);

            $c=new Criteria();
	 		$c->add(CommonEnchereReferencePeer::IDENCHERE,$this->getId());
	 		$c->add(CommonEnchereReferencePeer::ORGANISME,$this->getOrganisme());
	 		CommonEnchereReferencePeer::doDelete($c,$connexionCom);
        }

        // Suppression des tranches du bareme de l'enchere
        $tranchesEnchereToDelete = $this->getCommonEncheretranchesbaremenetcs(null, $connexionCom);
        $tranches = "";
        foreach ($tranchesEnchereToDelete as $tranches) {
            $c=new Criteria();
	 		$c->add(CommonEnchereTranchesBaremeNETCPeer::IDENCHERE,$this->getId());
	 		$c->add(CommonEnchereTranchesBaremeNETCPeer::ORGANISME,$this->getOrganisme());
	 		CommonEnchereTranchesBaremeNETCPeer::doDelete($c,$connexionCom);
        }
    }

	public function getService($con = null)
	{

		if ($this->aCommonService === null && ($this->service_id !== null)&& ($this->organisme !== null)) {
			$this->aCommonService = CommonServicePeer::retrieveByPK($this->service_id, $con);
		}
		return $this->aCommonService;
	}

	public function countCommonEnchereReferences($criteria = null, $distinct = false, $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonEnchereReferencePeer::IDENCHERE, $this->getId());
		$criteria->add(CommonEnchereReferencePeer::ORGANISME, $this->getOrganisme());

		return CommonEnchereReferencePeer::doCount($criteria, $distinct, $con);
	}

	public function getCommonEnchereOffres($criteria = null, $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonEnchereOffrePeer::IDENCHERE, $this->getId());
		$criteria->add(CommonEnchereOffrePeer::ORGANISME, $this->getOrganisme());
		$this->collCommonEnchereOffres = CommonEnchereOffrePeer::doSelect($criteria, $con);
		return $this->collCommonEnchereOffres;
	}

	public function countCommonEnchereOffres($criteria = null, $distinct = false, $con = null)
	{
		// include the Peer class

		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonEnchereOffrePeer::IDENCHERE, $this->getId());
		$criteria->add(CommonEnchereOffrePeer::ORGANISME, $this->getOrganisme());

		return CommonEnchereOffrePeer::doCount($criteria, $distinct, $con);
	}
} // CommonEncherePmi
