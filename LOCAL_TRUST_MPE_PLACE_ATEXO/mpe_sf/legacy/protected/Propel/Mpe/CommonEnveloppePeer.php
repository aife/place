<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use AtexoCrypto\Dto\Enveloppe;


  // include base peer class


  // include object class
  use Application\Propel\Mpe\Om\BaseCommonEnveloppePeer;


  /**
 * Skeleton subclass for performing query and update operations on the 'Enveloppe' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEnveloppePeer extends BaseCommonEnveloppePeer {

} // CommonEnveloppePeer
