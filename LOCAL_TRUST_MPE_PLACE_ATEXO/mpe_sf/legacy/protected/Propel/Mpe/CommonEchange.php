<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Collection\PropelObjectCollection;


use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonEchange;
use Application\Service\Atexo\Atexo_LoggerManager;
use \Exception;

/**
 * Skeleton subclass for representing a row from the 'Echange' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEchange extends BaseCommonEchange {

	protected array $destinatairesEchange = array();
	protected array $destinatairesRetraitEchange = array();
	protected array $destinatairesQuestionsEchange = array();
	protected array $destinatairesDepotsEchange = array();
	protected array $destinatairesBdFournisseursEchange = array();
	protected array $destinatairesLibresEchange = array();

	/**
	 * La méthode getCommonEchangePieceJointes() se trouvant dans le fichier BaseCommonEchange.php retourne un 
	 * objet collection "PropelObjectCollection". C'est pour cette raison que nous surchargeons la méthode pour
	 * retourner un tableau d'objets
	 * @param Criteria $criteria: critéria
     * @param $con: objet collection
     * @return: retourne un tableau d'objet recupéré dans la collection
	 */
	public function getCommonEchangePieceJointes($criteria = null, $con = null)
    {
    	$collectionEchangePiecesJointes = parent::getCommonEchangePieceJointes($criteria, $con);
    	if(is_object($collectionEchangePiecesJointes) && $collectionEchangePiecesJointes->getData()) {
    		return $collectionEchangePiecesJointes->getData();
    	}
		return array();
    }

	/**
	 * La méthode getCommonEchangeDestinataires() se trouvant dans le fichier BaseCommonEchange.php retourne un 
	 * objet collection "PropelObjectCollection". C'est pour cette raison que nous surchargeons la méthode pour
	 * retourner un tableau d'objets
	 * @param Criteria $criteria: critéria
     * @param $con: objet collection
     * @return: retourne un tableau d'objet recupéré dans la collection
	 */
	public function getCommonEchangeDestinataires($criteria = null, $con = null)
    {
    	$collectionEchangeDestinataires = parent::getCommonEchangeDestinataires($criteria, $con);
    	if(is_object($collectionEchangeDestinataires) && $collectionEchangeDestinataires->getData()) {
    		return $collectionEchangeDestinataires->getData();
    	}
    }

	/**
	 * @return mixed
	 */
	public function getDestinatairesEchange()
	{
		$destinatairesEchange = array();
		$destinataires = $this->getDestinataires();
		if($destinataires){
			$destinatairesEchange = explode(",", $destinataires);
		}
		return $destinatairesEchange;
	}

	/**
	 * @param mixed $destinatairesEchange
	 */
	public function setDestinatairesEchange($destinatairesEchange)
	{
		$this->destinatairesEchange = $destinatairesEchange;
	}

	/**
	 * @return array
	 */
	public function getDestinatairesRetraitEchange()
	{
		$destinatairesEchange = array();
		$destinataires = $this->getDestinatairesRetraits();
		if($destinataires){
			$destinatairesEchange = explode(",", $destinataires);
		}
		return $destinatairesEchange;
	}

	/**
	 * @param array $destinatairesRetraitEchange
	 */
	public function setDestinatairesRetraitEchange($destinatairesRetraitEchange)
	{
		$this->destinatairesRetraitEchange = $destinatairesRetraitEchange;
	}

	/**
	 * @return array
	 */
	public function getDestinatairesQuestionsEchange()
	{
		$destinatairesEchange = array();
		$destinataires = $this->getDestinatairesQuestions();
		if($destinataires){
			$destinatairesEchange = explode(",", $destinataires);
		}
		return $destinatairesEchange;
	}

	/**
	 * @param array $destinatairesQuestionsEchange
	 */
	public function setDestinatairesQuestionsEchange($destinatairesQuestionsEchange)
	{
		$this->destinatairesQuestionsEchange = $destinatairesQuestionsEchange;
	}

	/**
	 * @return array
	 */
	public function getDestinatairesDepotsEchange()
	{
		$destinatairesEchange = array();
		$destinataires = $this->getDestinatairesDepots();
		if($destinataires){
			$destinatairesEchange = explode(",", $destinataires);
		}
		return $destinatairesEchange;
	}

	/**
	 * @param array $destinatairesDepotsEchange
	 */
	public function setDestinatairesDepotsEchange($destinatairesDepotsEchange)
	{
		$this->destinatairesDepotsEchange = $destinatairesDepotsEchange;
	}

	/**
	 * @return array
	 */
	public function getDestinatairesBdFournisseursEchange()
	{
		$destinatairesEchange = array();
		$destinataires = $this->getDestinatairesBdFournisseurs();
		if($destinataires){
			$destinatairesEchange = explode(",", $destinataires);
		}
		return $destinatairesEchange;
	}

	/**
	 * @param array $destinatairesBdFournisseursEchange
	 */
	public function setDestinatairesBdFournisseursEchange($destinatairesBdFournisseursEchange)
	{
		$this->destinatairesBdFournisseursEchange = $destinatairesBdFournisseursEchange;
	}

	/**
	 * @return array
	 */
	public function getDestinatairesLibresEchange()
	{
		$destinatairesEchange = array();
		$destinataires = $this->getDestinatairesLibres();
		if($destinataires){
			$destinatairesEchange = explode(",", $destinataires);
		}
		return $destinatairesEchange;
	}

	/**
	 * @param array $destinatairesLibresEchange
	 */
	public function setDestinatairesLibresEchange($destinatairesLibresEchange)
	{
		$this->destinatairesLibresEchange = $destinatairesLibresEchange;
	}

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        $logger = Atexo_LoggerManager::getLogger('messec');
        $logger->info(
            'Début de la sauvegarde de l\'échange. Id de l\'échange : ' .
            $this->getId() . 'Consultation id : ' . $this->getConsultationId()
        );
        if ($this->isDeleted()) {
            $logger->error('Erreur sur l\'échange : Vous ne pouvez pas enregistrer un objet qui a été supprimé.');
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $logger->info('Echange : ouverture d\'une nouvelle connexion');
            $con = Propel::getConnection(CommonEchangePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $logger->info('Echange : beginTransaction');
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonEchangePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();
            $logger->info('Echange : Commit effectuer');

            return $affectedRows;
        } catch (Exception $e) {
            $logger->error(
                "Erreur lors de la sauvegarde." .
                PHP_EOL .
                "Erreur: " .
                $e->getMessage() .
                PHP_EOL .
                "Trace: " .
                $e->getTraceAsString()
            );

            $con->rollBack();
            throw $e;
        }
    }



} // CommonEchange
