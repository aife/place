<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonAdmissibiliteEnveloppePapierLot;

/**
 * Skeleton subclass for representing a row from the 'Admissibilite_Enveloppe_papier_Lot' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAdmissibiliteEnveloppePapierLot extends BaseCommonAdmissibiliteEnveloppePapierLot
{

	private $_intituleLot;
	private $_offreCrypte;
	private $_enveloppePostule;

	public function getIntiuleLot()
	{
		return $this->_intituleLot;
	}

	public function setIntiuleLot($intituleLot)
	{
		$this->_intituleLot=$intituleLot;
	}


	public function getOffreCrypte()
	{
		return $this->_offreCrypte;
	}

	public function setOffreCrypte($offreCrypte)
	{
		$this->_offreCrypte=$offreCrypte;
	}

	public function getEnveloppePostule() 
	{
		return $this->_enveloppePostule;
	}

	public function setEnveloppePostule($enveloppePostule) 
	{
		$this->_enveloppePostule=$enveloppePostule;
	}
} // CommonAdmissibiliteEnveloppePapierLot
