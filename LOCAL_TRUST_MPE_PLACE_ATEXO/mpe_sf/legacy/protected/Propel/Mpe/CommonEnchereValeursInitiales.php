<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonEnchereValeursInitiales;

/**
 * Skeleton subclass for representing a row from the 'EnchereValeursInitiales' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEnchereValeursInitiales extends BaseCommonEnchereValeursInitiales {

	public $_emailEntreprise;
    public $_nomEntreprise;

    public function setEmailentreprise($email)
    {
        $this->_emailEntreprise = $email;
    }
    public function getEmailentreprise()
    {
        return $this->_emailEntreprise;
    }

    public function setNomentreprise($nom)
    {
        $this->_nomEntreprise = $nom;
    }
    public function getNomentreprise()
    {
        return $this->_nomEntreprise;
    }
} // CommonEnchereValeursInitiales
