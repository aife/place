<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonTEntrepriseDocumentVersionPeer;

/**
 * Skeleton subclass for performing query and update operations on the 't_entreprise_document_version' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.7.0-dev on:
 *
 * Mon Feb  2 18:14:39 2015
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTEntrepriseDocumentVersionPeer extends BaseCommonTEntrepriseDocumentVersionPeer
{
}
