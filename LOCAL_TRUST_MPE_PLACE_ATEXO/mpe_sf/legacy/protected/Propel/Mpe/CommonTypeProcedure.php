<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonTypeProcedure;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Skeleton subclass for representing a row from the 'TypeProcedure' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonTypeProcedure extends BaseCommonTypeProcedure {
	public function getLibeleTypeProcedureTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $libelle_type_procedure= "libelle_type_procedure_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$libelle_type_procedure){
            return $this->libelle_type_procedure;
        } else {
            return $this->$libelle_type_procedure;
        }
	}
} // CommonTypeProcedure
