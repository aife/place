<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Service\Atexo\Atexo_Config;
use Application\Propel\Mpe\CommonJALPeer;
use Application\Propel\Mpe\CommonJAL;
use Application\Propel\Mpe\CommonDestinataireAnnoncePressPeer;

use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonDestinataireAnnoncePress;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Library\Propel\Propel;

/**
 * Skeleton subclass for representing a row from the 'DestinataireAnnoncePress' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonDestinataireAnnoncePress extends BaseCommonDestinataireAnnoncePress {
	public function getEmailDestForTableDest($idAnnonce,$org,$arrayIdService=null, $destinataireInfos = false){

 		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        if(!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->setDistinct();
        $c->add(CommonDestinataireAnnoncePressPeer::ORGANISME ,$org);
        $c->add(CommonDestinataireAnnoncePressPeer::ID_ANNONCE_PRESS ,$idAnnonce);
        if($arrayIdService && (is_countable($arrayIdService) ? count($arrayIdService) : 0)>0){
        	$c->addJoin(CommonJALPeer::ID, CommonDestinataireAnnoncePressPeer::ID_JAL);
        	$c->addAlias('DestinataireAnnoncePress', CommonDestinataireAnnoncePressPeer::TABLE_NAME);
			$c->addJoin(CommonJALPeer::ORGANISME,CommonDestinataireAnnoncePressPeer::alias('DestinataireAnnoncePress', CommonDestinataireAnnoncePressPeer::ORGANISME));
        	//$c->addJoin(CommonJALPeer::ORGANISME, CommonDestinataireAnnoncePressPeer::ORGANISME);
        	$c->add(CommonJALPeer::SERVICE_ID ,$arrayIdService[0]);
        	foreach($arrayIdService as $service) {
        		if($service != $arrayIdService[0]) {
        			$c->addOr(CommonJALPeer::SERVICE_ID ,$service);
        		}

        	}
        }
        if(!$destinataireInfos) {
        	$listDest = CommonJALPeer::doSelect($c, $connexionCom);
        } else {
        	$listDest = CommonDestinataireAnnoncePressPeer::doSelect($c, $connexionCom);    
        }
        if ($listDest) {
            return $listDest;
        }
        else {
            return array();
        }

 	}

 	public function getEmail() {
 		return (new CommonJAL())->getEmailByIdJAl($this->getIdJal());
 	}
 	public function getNom() {
 		return (new CommonJAL())->getNomByIdJAl($this->getIdJal());
 	}
} // CommonDestinataireAnnoncePress
