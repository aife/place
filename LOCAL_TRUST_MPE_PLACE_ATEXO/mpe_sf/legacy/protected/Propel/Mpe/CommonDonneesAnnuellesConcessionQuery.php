<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonDonneesAnnuellesConcessionQuery;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for performing query and update operations on the 'donnees_annuelles_concession' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonDonneesAnnuellesConcessionQuery extends BaseCommonDonneesAnnuellesConcessionQuery
{


    public function getDonneesAnnuellesConcession($idContrat,$limit = null,$offset = null,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }

        $this->filterByIdContrat($idContrat);
        if($limit){
            $this->limit($limit);
        }
        if($offset){
            $this->offset($offset);
        }
        $this->orderByNumOrdre(Criteria::ASC);
        return (array) $this->find($connexion);
    }

    public function getDonneesAnnuellesConcessionById($id,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }

        return $this->findOneById($id,$connexion);
    }

    public function getLastNumOrdreDonneesAnnuellesConcession($idContrat,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        return static::create()->select(CommonDonneesAnnuellesConcessionPeer::NUM_ORDRE)->filterByIdContrat($idContrat)->orderByNumOrdre(Criteria::DESC)->findOne($connexion);
    }
}
