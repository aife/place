<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonEchangeTypeMessage;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'EchangeTypeMessage' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEchangeTypeMessage extends BaseCommonEchangeTypeMessage {

	/*
	 * Rétourne le libellé du code message passé en parametre
	 */
	public function getLibelle() 
	{
		return Prado::localize($this->code);
	}

} // CommonEchangeTypeMessage
