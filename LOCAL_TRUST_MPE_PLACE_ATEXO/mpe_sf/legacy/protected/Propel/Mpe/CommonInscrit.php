<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\EntrepriseQuery;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\CommonTEtablissement;

use Application\Library\Propel\Exception\PropelException;




use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonInscrit;
use Application\Propel\Mpe\Om\BaseCommonInscritPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Echange;

/**
 * Skeleton subclass for representing a row from the 'Inscrit' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonInscrit extends BaseCommonInscrit {

	protected $codeEtablissement = "";
	protected string $adresseEtablissement = "";
    /**
     * @var bool : precise si le login est modifie
     */
    protected bool $modifiedLogin = false;
    /**
     * @var string : contient l'ancienne valeur du login en cas de modification
     */
    protected string $oldLogin = "";

    protected $etatAdministratifEtablissement;

    public function getetatAdministratifEtablissement(){
            return $this->etatAdministratifEtablissement;
    }

    public function setEtatAdministratifEtablissement($etatAdministratifEtablissement){
        $this->etatAdministratifEtablissement = $etatAdministratifEtablissement;
    }

	public function getCodeEtablissement(){
		if($this->getCommonTEtablissement()){
		$this->codeEtablissement = $this->getCommonTEtablissement()->getCodeEtablissement();
		return $this->codeEtablissement;
		}
	}

    public function setCodeEtablissement($codeEtablissement){
       $this->codeEtablissement = $codeEtablissement;
    }


	public function getAdresseEtablissement($separateur = ' ')
	{
			$etablissement = $this->getCommonTEtablissement();
		if($etablissement){
			$this->adresseEtablissement = $etablissement->getAdresse() . $separateur . $etablissement->getAdresse2() . $separateur;
			$this->adresseEtablissement .= $etablissement->getCodePostal() . '  ' . $etablissement->getVille() .$separateur. $etablissement->getPays() ;
			return $this->adresseEtablissement;
		}
	}
    /**
     * Get the associated Entreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Entreprise The associated Entreprise object.
     * @throws PropelException
     */
    public function getCommonEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        return $this->getEntreprise($con,$doQuery);
    }

    public function getSitrenEntreprise()
    {
    	if($this->getCommonEntreprise()){
       	 return $this->getCommonEntreprise()->getSiren();
    	}
    }

    /**
     * Actions a effectuer a l'enregistrement en base de donnees, juste avant la mise a jour de la base de donnees
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @version 1.0
     * @since esr-2015
     * @copyright Atexo 2015
     */
    public function preUpdate(PropelPDO $con = null)
    {
        if($this->isColumnModified(BaseCommonInscritPeer::LOGIN)) {
            $this->modifiedLogin = true;
        }
        if(Atexo_Module::isEnabled('InterfaceModuleSub') && $this->modifiedLogin) {
            $this->oldLogin = Atexo_Entreprise_Inscrit::retrieveInscritLogin($this->getId());
        }
        return true;
    }

    /**
     * Actions a effectuer a l'enregistrement en base de donnees, juste apres la mise a jour des donnees
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @version 1.0
     * @since esr-2015
     * @copyright Atexo 2015
     */
    public function postUpdate(PropelPDO $con = null)
    {
        if(Atexo_Module::isEnabled('InterfaceModuleSub') && $this->modifiedLogin) {
            (new Atexo_FormulaireSub_Echange())->updateUserInfos($this->oldLogin, $this);
        }
        return true;
    }

   /* Recupere le siren de l'entreprise a laquelle l'inscrit est rattache
    *
    * @return string : siren
    * @author Oumar KONATE <oumar.konate@atexo.com>
    * @version 1.0
    * @since esr-2015
    * @copyright Atexo 2016
    */
    public function getSirenEntrepriseInscrit()
    {
        $entrepriseQuery = null;
								try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $entrepriseQuery = EntrepriseQuery::create()->filterByCommonInscrit($this);
            $entreprise = $entrepriseQuery->findOne($connexion);
            $siren = null;
            if ($entreprise instanceof Entreprise) {
                $siren = $entreprise->getSiren();
            }
            return $siren;
        } catch(\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de la recuperation du siren de l'entreprise de l'inscrit \n\nRequete : " . $entrepriseQuery->toString() . "\n\nErreur : " . $e->getMessage() . "\n\trace: " . $e->getTraceAsString());
        }
    }

    /* Recupere le siret (sur 14 caracteres) de l'etablissement auquel l'inscrit est rattache
    *
    * @return string : siret
    * @author Oumar KONATE <oumar.konate@atexo.com>
    * @version 1.0
    * @since esr-2015
    * @copyright Atexo 2016
    */
    public function getSiretEtablissementInscrit()
    {
        $etablissementQuery = null;
								try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $etablissementQuery = CommonTEtablissementQuery::create()->filterByCommonInscrit($this);
            $etablissement = $etablissementQuery->findOne($connexion);
            $siret = null;
            if ($etablissement instanceof CommonTEtablissement) {
                $siret = $etablissement->getCodeEtablissement();
            }
            return $siret;
        } catch(\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $req = ($etablissementQuery instanceof CommonTEtablissementQuery)? $etablissementQuery->toString():'';
            $logger->error("Erreur lors de la recuperation du siret de l'etablissement de l'inscrit \n\nRequete : " . $req . "\n\nErreur : " . $e->getMessage() . "\n\trace: " . $e->getTraceAsString());
        }
    }

    public function getIdInitial(){
            return self::getIdExterne();
    }

    public function setIdInitial($value){
        self::setIdExterne($value);
    }

    public function getCollCommonDossierVolumineuxs()
    {
        return $this->collCommonDossierVolumineuxs;
    }

    public function getCollCommonDossierVolumineuxsPartial()
    {
        return $this->collCommonDossierVolumineuxsPartial;
    }

    public function getCollCommonHistorisationMotDePasses()
    {
        return $this->collCommonHistorisationMotDePasses;
    }

    public function getCollCommonHistorisationMotDePassesPartial()
    {
        return $this->collCommonHistorisationMotDePassesPartial;
    }

    public function getCommonDossierVolumineuxsScheduledForDeletion()
    {
        return $this->commonDossierVolumineuxsScheduledForDeletion;
    }

    public function getCommonHistorisationMotDePassesScheduledForDeletion()
    {
        return $this->commonHistorisationMotDePassesScheduledForDeletion;
    }

} // CommonInscrit
