<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonDonneesAnnuellesConcession;

/**
 * Skeleton subclass for representing a row from the 'donnees_annuelles_concession' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonDonneesAnnuellesConcession extends BaseCommonDonneesAnnuellesConcession
{
}
