<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonItemFormulaire;

/**
 * Skeleton subclass for representing a row from the 'ItemFormulaire' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonItemFormulaire extends BaseCommonItemFormulaire {
	private bool $_isVisible = true;

	public function setIsVisible($value)
	{
		 $this->_isVisible = $value;
	}
	public function getIsVisible()
	{
		return $this->_isVisible;
	}
	public function getIsObligatoire()
	{
		return ($this->getObligatoire()== '1');
	}
	public function setIsObligatoire($value)
	{
		$this->setObligatoire($value);
	}



} // CommonItemFormulaire
