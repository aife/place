<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonTPreferenceOffreSupportPublicationQuery;
use Application\Propel\Mpe\CommonTPreferenceOffreSupportPublication;




use Application\Propel\Mpe\Om\BaseCommonTOffreSupportPublicite;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;

/**
 * Skeleton subclass for representing a row from the 't_Offre_Support_Publicite' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTOffreSupportPublicite extends BaseCommonTOffreSupportPublicite
{
    /**
     * permet de reccuperer le chemin du logo
     *
     * @return string chemin du logo
     * @author AME <amal.elbekkaoui@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function getUrlLogo(){
        $return = "";
        if($this->getLogo()) {
            $return = Atexo_Controller_Front::t() . "/images/" . $this->getLogo();
        }
        return $return;
    }

    /**
     * permet de reccuperer une  preference d'un agent pour un offre support
     *
     * @param Integer $idPrefrence
     * @return boolean la preference de l'agent pour ce support
     * @author AME <amal.elbekkaoui@atexo.com>
     * @version 1.0
     * @since roadmap-2015
     * @copyright Atexo 2015
     */
    public function getPreference($idPrefrence){
        $return = true;
        $pref = CommonTPreferenceOffreSupportPublicationQuery::create()->filterByIdPreference($idPrefrence)->filterByIdSupport($this->getIdSupport())->filterByIdOffre($this->getId())->findOne();
        if($pref instanceof CommonTPreferenceOffreSupportPublication){
            $return = $pref->getActive();
        }
        return $return;
    }
}
