<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Service\Atexo\Config\Atexo_Config_Exception;

use Application\Propel\Mpe\CommonBlobFile;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonBlobFilePeer;
use Application\Service\Atexo\Atexo_Config;

// include base peer class

  // include object class


/**
 * Skeleton subclass for performing query and update operations on the 'blob_file' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonBlobFilePeer extends BaseCommonBlobFilePeer {

} // CommonBlobFilePeer
