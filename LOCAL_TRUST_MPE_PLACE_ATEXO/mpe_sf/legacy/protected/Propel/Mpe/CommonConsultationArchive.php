<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonConsultationArchive;

/**
 * Skeleton subclass for representing a row from the 'consultation_archive' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonConsultationArchive extends BaseCommonConsultationArchive {

    /**
     * @param string $cheminFichier
     */
    public function setCheminFichier($cheminFichier)
    {
        $s = urlencode($cheminFichier);
        $cheminFichier = str_replace('%2F', '/', $s);
        parent::setCheminFichier($cheminFichier);
    }

    /**
     * @return string
     */
    public function getCheminFichier()
    {
        return urldecode(parent::getCheminFichier());
    }

} // CommonConsultationArchive
