<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDocumentEntreprise;
use Application\Propel\Mpe\CommonTDocumentEntreprisePeer;
use Application\Propel\Mpe\CommonTDocumentEntrepriseQuery;
use Application\Propel\Mpe\CommonTDocumentType;
use Application\Propel\Mpe\CommonTDocumentTypeQuery;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersion;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersionQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntrepriseQuery;

/**
 * Base class that represents a row from the 't_document_entreprise' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDocumentEntreprise extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTDocumentEntreprisePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTDocumentEntreprisePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_document field.
     * @var        int
     */
    protected $id_document;

    /**
     * The value for the id_entreprise field.
     * @var        int
     */
    protected $id_entreprise;

    /**
     * The value for the id_etablissement field.
     * @var        int
     */
    protected $id_etablissement;

    /**
     * The value for the nom_document field.
     * @var        string
     */
    protected $nom_document;

    /**
     * The value for the id_type_document field.
     * @var        int
     */
    protected $id_type_document;

    /**
     * The value for the id_derniere_version field.
     * @var        int
     */
    protected $id_derniere_version;

    /**
     * @var        Entreprise
     */
    protected $aEntreprise;

    /**
     * @var        CommonTDocumentType
     */
    protected $aCommonTDocumentType;

    /**
     * @var        PropelObjectCollection|CommonTEntrepriseDocumentVersion[] Collection to store aggregation of CommonTEntrepriseDocumentVersion objects.
     */
    protected $collCommonTEntrepriseDocumentVersions;
    protected $collCommonTEntrepriseDocumentVersionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTEntrepriseDocumentVersionsScheduledForDeletion = null;

    /**
     * Get the [id_document] column value.
     *
     * @return int
     */
    public function getIdDocument()
    {

        return $this->id_document;
    }

    /**
     * Get the [id_entreprise] column value.
     *
     * @return int
     */
    public function getIdEntreprise()
    {

        return $this->id_entreprise;
    }

    /**
     * Get the [id_etablissement] column value.
     *
     * @return int
     */
    public function getIdEtablissement()
    {

        return $this->id_etablissement;
    }

    /**
     * Get the [nom_document] column value.
     *
     * @return string
     */
    public function getNomDocument()
    {

        return $this->nom_document;
    }

    /**
     * Get the [id_type_document] column value.
     *
     * @return int
     */
    public function getIdTypeDocument()
    {

        return $this->id_type_document;
    }

    /**
     * Get the [id_derniere_version] column value.
     *
     * @return int
     */
    public function getIdDerniereVersion()
    {

        return $this->id_derniere_version;
    }

    /**
     * Set the value of [id_document] column.
     *
     * @param int $v new value
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     */
    public function setIdDocument($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_document !== $v) {
            $this->id_document = $v;
            $this->modifiedColumns[] = CommonTDocumentEntreprisePeer::ID_DOCUMENT;
        }


        return $this;
    } // setIdDocument()

    /**
     * Set the value of [id_entreprise] column.
     *
     * @param int $v new value
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     */
    public function setIdEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_entreprise !== $v) {
            $this->id_entreprise = $v;
            $this->modifiedColumns[] = CommonTDocumentEntreprisePeer::ID_ENTREPRISE;
        }

        if ($this->aEntreprise !== null && $this->aEntreprise->getId() !== $v) {
            $this->aEntreprise = null;
        }


        return $this;
    } // setIdEntreprise()

    /**
     * Set the value of [id_etablissement] column.
     *
     * @param int $v new value
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     */
    public function setIdEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_etablissement !== $v) {
            $this->id_etablissement = $v;
            $this->modifiedColumns[] = CommonTDocumentEntreprisePeer::ID_ETABLISSEMENT;
        }


        return $this;
    } // setIdEtablissement()

    /**
     * Set the value of [nom_document] column.
     *
     * @param string $v new value
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     */
    public function setNomDocument($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_document !== $v) {
            $this->nom_document = $v;
            $this->modifiedColumns[] = CommonTDocumentEntreprisePeer::NOM_DOCUMENT;
        }


        return $this;
    } // setNomDocument()

    /**
     * Set the value of [id_type_document] column.
     *
     * @param int $v new value
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     */
    public function setIdTypeDocument($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_document !== $v) {
            $this->id_type_document = $v;
            $this->modifiedColumns[] = CommonTDocumentEntreprisePeer::ID_TYPE_DOCUMENT;
        }

        if ($this->aCommonTDocumentType !== null && $this->aCommonTDocumentType->getIdTypeDocument() !== $v) {
            $this->aCommonTDocumentType = null;
        }


        return $this;
    } // setIdTypeDocument()

    /**
     * Set the value of [id_derniere_version] column.
     *
     * @param int $v new value
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     */
    public function setIdDerniereVersion($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_derniere_version !== $v) {
            $this->id_derniere_version = $v;
            $this->modifiedColumns[] = CommonTDocumentEntreprisePeer::ID_DERNIERE_VERSION;
        }


        return $this;
    } // setIdDerniereVersion()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_document = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_entreprise = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_etablissement = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->nom_document = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->id_type_document = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->id_derniere_version = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 6; // 6 = CommonTDocumentEntreprisePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTDocumentEntreprise object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aEntreprise !== null && $this->id_entreprise !== $this->aEntreprise->getId()) {
            $this->aEntreprise = null;
        }
        if ($this->aCommonTDocumentType !== null && $this->id_type_document !== $this->aCommonTDocumentType->getIdTypeDocument()) {
            $this->aCommonTDocumentType = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTDocumentEntreprisePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEntreprise = null;
            $this->aCommonTDocumentType = null;
            $this->collCommonTEntrepriseDocumentVersions = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTDocumentEntrepriseQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTDocumentEntreprisePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntreprise !== null) {
                if ($this->aEntreprise->isModified() || $this->aEntreprise->isNew()) {
                    $affectedRows += $this->aEntreprise->save($con);
                }
                $this->setEntreprise($this->aEntreprise);
            }

            if ($this->aCommonTDocumentType !== null) {
                if ($this->aCommonTDocumentType->isModified() || $this->aCommonTDocumentType->isNew()) {
                    $affectedRows += $this->aCommonTDocumentType->save($con);
                }
                $this->setCommonTDocumentType($this->aCommonTDocumentType);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTEntrepriseDocumentVersionsScheduledForDeletion !== null) {
                if (!$this->commonTEntrepriseDocumentVersionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTEntrepriseDocumentVersionsScheduledForDeletion as $commonTEntrepriseDocumentVersion) {
                        // need to save related object because we set the relation to null
                        $commonTEntrepriseDocumentVersion->save($con);
                    }
                    $this->commonTEntrepriseDocumentVersionsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTEntrepriseDocumentVersions !== null) {
                foreach ($this->collCommonTEntrepriseDocumentVersions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTDocumentEntreprisePeer::ID_DOCUMENT;
        if (null !== $this->id_document) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTDocumentEntreprisePeer::ID_DOCUMENT . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_DOCUMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_document`';
        }
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entreprise`';
        }
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_etablissement`';
        }
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::NOM_DOCUMENT)) {
            $modifiedColumns[':p' . $index++]  = '`nom_document`';
        }
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_TYPE_DOCUMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_document`';
        }
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_DERNIERE_VERSION)) {
            $modifiedColumns[':p' . $index++]  = '`id_derniere_version`';
        }

        $sql = sprintf(
            'INSERT INTO `t_document_entreprise` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_document`':
                        $stmt->bindValue($identifier, $this->id_document, PDO::PARAM_INT);
                        break;
                    case '`id_entreprise`':
                        $stmt->bindValue($identifier, $this->id_entreprise, PDO::PARAM_INT);
                        break;
                    case '`id_etablissement`':
                        $stmt->bindValue($identifier, $this->id_etablissement, PDO::PARAM_INT);
                        break;
                    case '`nom_document`':
                        $stmt->bindValue($identifier, $this->nom_document, PDO::PARAM_STR);
                        break;
                    case '`id_type_document`':
                        $stmt->bindValue($identifier, $this->id_type_document, PDO::PARAM_INT);
                        break;
                    case '`id_derniere_version`':
                        $stmt->bindValue($identifier, $this->id_derniere_version, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdDocument($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntreprise !== null) {
                if (!$this->aEntreprise->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aEntreprise->getValidationFailures());
                }
            }

            if ($this->aCommonTDocumentType !== null) {
                if (!$this->aCommonTDocumentType->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTDocumentType->getValidationFailures());
                }
            }


            if (($retval = CommonTDocumentEntreprisePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTEntrepriseDocumentVersions !== null) {
                    foreach ($this->collCommonTEntrepriseDocumentVersions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDocumentEntreprisePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdDocument();
                break;
            case 1:
                return $this->getIdEntreprise();
                break;
            case 2:
                return $this->getIdEtablissement();
                break;
            case 3:
                return $this->getNomDocument();
                break;
            case 4:
                return $this->getIdTypeDocument();
                break;
            case 5:
                return $this->getIdDerniereVersion();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTDocumentEntreprise'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTDocumentEntreprise'][$this->getPrimaryKey()] = true;
        $keys = CommonTDocumentEntreprisePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdDocument(),
            $keys[1] => $this->getIdEntreprise(),
            $keys[2] => $this->getIdEtablissement(),
            $keys[3] => $this->getNomDocument(),
            $keys[4] => $this->getIdTypeDocument(),
            $keys[5] => $this->getIdDerniereVersion(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aEntreprise) {
                $result['Entreprise'] = $this->aEntreprise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTDocumentType) {
                $result['CommonTDocumentType'] = $this->aCommonTDocumentType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTEntrepriseDocumentVersions) {
                $result['CommonTEntrepriseDocumentVersions'] = $this->collCommonTEntrepriseDocumentVersions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDocumentEntreprisePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdDocument($value);
                break;
            case 1:
                $this->setIdEntreprise($value);
                break;
            case 2:
                $this->setIdEtablissement($value);
                break;
            case 3:
                $this->setNomDocument($value);
                break;
            case 4:
                $this->setIdTypeDocument($value);
                break;
            case 5:
                $this->setIdDerniereVersion($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTDocumentEntreprisePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdDocument($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdEntreprise($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdEtablissement($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNomDocument($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdTypeDocument($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setIdDerniereVersion($arr[$keys[5]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTDocumentEntreprisePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_DOCUMENT)) $criteria->add(CommonTDocumentEntreprisePeer::ID_DOCUMENT, $this->id_document);
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_ENTREPRISE)) $criteria->add(CommonTDocumentEntreprisePeer::ID_ENTREPRISE, $this->id_entreprise);
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_ETABLISSEMENT)) $criteria->add(CommonTDocumentEntreprisePeer::ID_ETABLISSEMENT, $this->id_etablissement);
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::NOM_DOCUMENT)) $criteria->add(CommonTDocumentEntreprisePeer::NOM_DOCUMENT, $this->nom_document);
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_TYPE_DOCUMENT)) $criteria->add(CommonTDocumentEntreprisePeer::ID_TYPE_DOCUMENT, $this->id_type_document);
        if ($this->isColumnModified(CommonTDocumentEntreprisePeer::ID_DERNIERE_VERSION)) $criteria->add(CommonTDocumentEntreprisePeer::ID_DERNIERE_VERSION, $this->id_derniere_version);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTDocumentEntreprisePeer::DATABASE_NAME);
        $criteria->add(CommonTDocumentEntreprisePeer::ID_DOCUMENT, $this->id_document);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdDocument();
    }

    /**
     * Generic method to set the primary key (id_document column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdDocument($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdDocument();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTDocumentEntreprise (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdEntreprise($this->getIdEntreprise());
        $copyObj->setIdEtablissement($this->getIdEtablissement());
        $copyObj->setNomDocument($this->getNomDocument());
        $copyObj->setIdTypeDocument($this->getIdTypeDocument());
        $copyObj->setIdDerniereVersion($this->getIdDerniereVersion());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTEntrepriseDocumentVersions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTEntrepriseDocumentVersion($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdDocument(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTDocumentEntreprise Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTDocumentEntreprisePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTDocumentEntreprisePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Entreprise object.
     *
     * @param   Entreprise $v
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntreprise(Entreprise $v = null)
    {
        if ($v === null) {
            $this->setIdEntreprise(NULL);
        } else {
            $this->setIdEntreprise($v->getId());
        }

        $this->aEntreprise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Entreprise object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTDocumentEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated Entreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Entreprise The associated Entreprise object.
     * @throws PropelException
     */
    public function getEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aEntreprise === null && ($this->id_entreprise !== null) && $doQuery) {
            $this->aEntreprise = EntrepriseQuery::create()->findPk($this->id_entreprise, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntreprise->addCommonTDocumentEntreprises($this);
             */
        }

        return $this->aEntreprise;
    }

    /**
     * Declares an association between this object and a CommonTDocumentType object.
     *
     * @param   CommonTDocumentType $v
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTDocumentType(CommonTDocumentType $v = null)
    {
        if ($v === null) {
            $this->setIdTypeDocument(NULL);
        } else {
            $this->setIdTypeDocument($v->getIdTypeDocument());
        }

        $this->aCommonTDocumentType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTDocumentType object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTDocumentEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTDocumentType object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTDocumentType The associated CommonTDocumentType object.
     * @throws PropelException
     */
    public function getCommonTDocumentType(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTDocumentType === null && ($this->id_type_document !== null) && $doQuery) {
            $this->aCommonTDocumentType = CommonTDocumentTypeQuery::create()->findPk($this->id_type_document, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTDocumentType->addCommonTDocumentEntreprises($this);
             */
        }

        return $this->aCommonTDocumentType;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTEntrepriseDocumentVersion' == $relationName) {
            $this->initCommonTEntrepriseDocumentVersions();
        }
    }

    /**
     * Clears out the collCommonTEntrepriseDocumentVersions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     * @see        addCommonTEntrepriseDocumentVersions()
     */
    public function clearCommonTEntrepriseDocumentVersions()
    {
        $this->collCommonTEntrepriseDocumentVersions = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTEntrepriseDocumentVersionsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTEntrepriseDocumentVersions collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTEntrepriseDocumentVersions($v = true)
    {
        $this->collCommonTEntrepriseDocumentVersionsPartial = $v;
    }

    /**
     * Initializes the collCommonTEntrepriseDocumentVersions collection.
     *
     * By default this just sets the collCommonTEntrepriseDocumentVersions collection to an empty array (like clearcollCommonTEntrepriseDocumentVersions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTEntrepriseDocumentVersions($overrideExisting = true)
    {
        if (null !== $this->collCommonTEntrepriseDocumentVersions && !$overrideExisting) {
            return;
        }
        $this->collCommonTEntrepriseDocumentVersions = new PropelObjectCollection();
        $this->collCommonTEntrepriseDocumentVersions->setModel('CommonTEntrepriseDocumentVersion');
    }

    /**
     * Gets an array of CommonTEntrepriseDocumentVersion objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTDocumentEntreprise is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTEntrepriseDocumentVersion[] List of CommonTEntrepriseDocumentVersion objects
     * @throws PropelException
     */
    public function getCommonTEntrepriseDocumentVersions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTEntrepriseDocumentVersionsPartial && !$this->isNew();
        if (null === $this->collCommonTEntrepriseDocumentVersions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTEntrepriseDocumentVersions) {
                // return empty collection
                $this->initCommonTEntrepriseDocumentVersions();
            } else {
                $collCommonTEntrepriseDocumentVersions = CommonTEntrepriseDocumentVersionQuery::create(null, $criteria)
                    ->filterByCommonTDocumentEntreprise($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTEntrepriseDocumentVersionsPartial && count($collCommonTEntrepriseDocumentVersions)) {
                      $this->initCommonTEntrepriseDocumentVersions(false);

                      foreach ($collCommonTEntrepriseDocumentVersions as $obj) {
                        if (false == $this->collCommonTEntrepriseDocumentVersions->contains($obj)) {
                          $this->collCommonTEntrepriseDocumentVersions->append($obj);
                        }
                      }

                      $this->collCommonTEntrepriseDocumentVersionsPartial = true;
                    }

                    $collCommonTEntrepriseDocumentVersions->getInternalIterator()->rewind();

                    return $collCommonTEntrepriseDocumentVersions;
                }

                if ($partial && $this->collCommonTEntrepriseDocumentVersions) {
                    foreach ($this->collCommonTEntrepriseDocumentVersions as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTEntrepriseDocumentVersions[] = $obj;
                        }
                    }
                }

                $this->collCommonTEntrepriseDocumentVersions = $collCommonTEntrepriseDocumentVersions;
                $this->collCommonTEntrepriseDocumentVersionsPartial = false;
            }
        }

        return $this->collCommonTEntrepriseDocumentVersions;
    }

    /**
     * Sets a collection of CommonTEntrepriseDocumentVersion objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTEntrepriseDocumentVersions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     */
    public function setCommonTEntrepriseDocumentVersions(PropelCollection $commonTEntrepriseDocumentVersions, PropelPDO $con = null)
    {
        $commonTEntrepriseDocumentVersionsToDelete = $this->getCommonTEntrepriseDocumentVersions(new Criteria(), $con)->diff($commonTEntrepriseDocumentVersions);


        $this->commonTEntrepriseDocumentVersionsScheduledForDeletion = $commonTEntrepriseDocumentVersionsToDelete;

        foreach ($commonTEntrepriseDocumentVersionsToDelete as $commonTEntrepriseDocumentVersionRemoved) {
            $commonTEntrepriseDocumentVersionRemoved->setCommonTDocumentEntreprise(null);
        }

        $this->collCommonTEntrepriseDocumentVersions = null;
        foreach ($commonTEntrepriseDocumentVersions as $commonTEntrepriseDocumentVersion) {
            $this->addCommonTEntrepriseDocumentVersion($commonTEntrepriseDocumentVersion);
        }

        $this->collCommonTEntrepriseDocumentVersions = $commonTEntrepriseDocumentVersions;
        $this->collCommonTEntrepriseDocumentVersionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTEntrepriseDocumentVersion objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTEntrepriseDocumentVersion objects.
     * @throws PropelException
     */
    public function countCommonTEntrepriseDocumentVersions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTEntrepriseDocumentVersionsPartial && !$this->isNew();
        if (null === $this->collCommonTEntrepriseDocumentVersions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTEntrepriseDocumentVersions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTEntrepriseDocumentVersions());
            }
            $query = CommonTEntrepriseDocumentVersionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTDocumentEntreprise($this)
                ->count($con);
        }

        return count($this->collCommonTEntrepriseDocumentVersions);
    }

    /**
     * Method called to associate a CommonTEntrepriseDocumentVersion object to this object
     * through the CommonTEntrepriseDocumentVersion foreign key attribute.
     *
     * @param   CommonTEntrepriseDocumentVersion $l CommonTEntrepriseDocumentVersion
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     */
    public function addCommonTEntrepriseDocumentVersion(CommonTEntrepriseDocumentVersion $l)
    {
        if ($this->collCommonTEntrepriseDocumentVersions === null) {
            $this->initCommonTEntrepriseDocumentVersions();
            $this->collCommonTEntrepriseDocumentVersionsPartial = true;
        }
        if (!in_array($l, $this->collCommonTEntrepriseDocumentVersions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTEntrepriseDocumentVersion($l);
        }

        return $this;
    }

    /**
     * @param	CommonTEntrepriseDocumentVersion $commonTEntrepriseDocumentVersion The commonTEntrepriseDocumentVersion object to add.
     */
    protected function doAddCommonTEntrepriseDocumentVersion($commonTEntrepriseDocumentVersion)
    {
        $this->collCommonTEntrepriseDocumentVersions[]= $commonTEntrepriseDocumentVersion;
        $commonTEntrepriseDocumentVersion->setCommonTDocumentEntreprise($this);
    }

    /**
     * @param	CommonTEntrepriseDocumentVersion $commonTEntrepriseDocumentVersion The commonTEntrepriseDocumentVersion object to remove.
     * @return CommonTDocumentEntreprise The current object (for fluent API support)
     */
    public function removeCommonTEntrepriseDocumentVersion($commonTEntrepriseDocumentVersion)
    {
        if ($this->getCommonTEntrepriseDocumentVersions()->contains($commonTEntrepriseDocumentVersion)) {
            $this->collCommonTEntrepriseDocumentVersions->remove($this->collCommonTEntrepriseDocumentVersions->search($commonTEntrepriseDocumentVersion));
            if (null === $this->commonTEntrepriseDocumentVersionsScheduledForDeletion) {
                $this->commonTEntrepriseDocumentVersionsScheduledForDeletion = clone $this->collCommonTEntrepriseDocumentVersions;
                $this->commonTEntrepriseDocumentVersionsScheduledForDeletion->clear();
            }
            $this->commonTEntrepriseDocumentVersionsScheduledForDeletion[]= clone $commonTEntrepriseDocumentVersion;
            $commonTEntrepriseDocumentVersion->setCommonTDocumentEntreprise(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_document = null;
        $this->id_entreprise = null;
        $this->id_etablissement = null;
        $this->nom_document = null;
        $this->id_type_document = null;
        $this->id_derniere_version = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTEntrepriseDocumentVersions) {
                foreach ($this->collCommonTEntrepriseDocumentVersions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aEntreprise instanceof Persistent) {
              $this->aEntreprise->clearAllReferences($deep);
            }
            if ($this->aCommonTDocumentType instanceof Persistent) {
              $this->aCommonTDocumentType->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTEntrepriseDocumentVersions instanceof PropelCollection) {
            $this->collCommonTEntrepriseDocumentVersions->clearIterator();
        }
        $this->collCommonTEntrepriseDocumentVersions = null;
        $this->aEntreprise = null;
        $this->aCommonTDocumentType = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTDocumentEntreprisePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
