<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDispositifAnnonce;
use Application\Propel\Mpe\CommonTDispositifAnnoncePeer;
use Application\Propel\Mpe\CommonTDispositifAnnonceQuery;

/**
 * Base class that represents a row from the 't_dispositif_annonce' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDispositifAnnonce extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTDispositifAnnoncePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTDispositifAnnoncePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the libelle field.
     * @var        string
     */
    protected $libelle;

    /**
     * The value for the id_externe field.
     * @var        int
     */
    protected $id_externe;

    /**
     * The value for the type field.
     * @var        int
     */
    protected $type;

    /**
     * The value for the visible field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $visible;

    /**
     * The value for the id_dispositif_rectificatif field.
     * @var        int
     */
    protected $id_dispositif_rectificatif;

    /**
     * The value for the id_dispositif_annulation field.
     * @var        int
     */
    protected $id_dispositif_annulation;

    /**
     * The value for the sigle field.
     * @var        string
     */
    protected $sigle;

    /**
     * The value for the id_type_avis_rectificatif field.
     * @var        int
     */
    protected $id_type_avis_rectificatif;

    /**
     * The value for the id_type_avis_annulation field.
     * @var        int
     */
    protected $id_type_avis_annulation;

    /**
     * The value for the libelle_formulaire_propose field.
     * @var        string
     */
    protected $libelle_formulaire_propose;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->visible = '1';
    }

    /**
     * Initializes internal state of BaseCommonTDispositifAnnonce object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [libelle] column value.
     *
     * @return string
     */
    public function getLibelle()
    {

        return $this->libelle;
    }

    /**
     * Get the [id_externe] column value.
     *
     * @return int
     */
    public function getIdExterne()
    {

        return $this->id_externe;
    }

    /**
     * Get the [type] column value.
     *
     * @return int
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * Get the [visible] column value.
     *
     * @return string
     */
    public function getVisible()
    {

        return $this->visible;
    }

    /**
     * Get the [id_dispositif_rectificatif] column value.
     *
     * @return int
     */
    public function getIdDispositifRectificatif()
    {

        return $this->id_dispositif_rectificatif;
    }

    /**
     * Get the [id_dispositif_annulation] column value.
     *
     * @return int
     */
    public function getIdDispositifAnnulation()
    {

        return $this->id_dispositif_annulation;
    }

    /**
     * Get the [sigle] column value.
     *
     * @return string
     */
    public function getSigle()
    {

        return $this->sigle;
    }

    /**
     * Get the [id_type_avis_rectificatif] column value.
     *
     * @return int
     */
    public function getIdTypeAvisRectificatif()
    {

        return $this->id_type_avis_rectificatif;
    }

    /**
     * Get the [id_type_avis_annulation] column value.
     *
     * @return int
     */
    public function getIdTypeAvisAnnulation()
    {

        return $this->id_type_avis_annulation;
    }

    /**
     * Get the [libelle_formulaire_propose] column value.
     *
     * @return string
     */
    public function getLibelleFormulairePropose()
    {

        return $this->libelle_formulaire_propose;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [libelle] column.
     *
     * @param string $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setLibelle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle !== $v) {
            $this->libelle = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::LIBELLE;
        }


        return $this;
    } // setLibelle()

    /**
     * Set the value of [id_externe] column.
     *
     * @param int $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setIdExterne($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_externe !== $v) {
            $this->id_externe = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::ID_EXTERNE;
        }


        return $this;
    } // setIdExterne()

    /**
     * Set the value of [type] column.
     *
     * @param int $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::TYPE;
        }


        return $this;
    } // setType()

    /**
     * Set the value of [visible] column.
     *
     * @param string $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setVisible($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->visible !== $v) {
            $this->visible = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::VISIBLE;
        }


        return $this;
    } // setVisible()

    /**
     * Set the value of [id_dispositif_rectificatif] column.
     *
     * @param int $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setIdDispositifRectificatif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_dispositif_rectificatif !== $v) {
            $this->id_dispositif_rectificatif = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::ID_DISPOSITIF_RECTIFICATIF;
        }


        return $this;
    } // setIdDispositifRectificatif()

    /**
     * Set the value of [id_dispositif_annulation] column.
     *
     * @param int $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setIdDispositifAnnulation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_dispositif_annulation !== $v) {
            $this->id_dispositif_annulation = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::ID_DISPOSITIF_ANNULATION;
        }


        return $this;
    } // setIdDispositifAnnulation()

    /**
     * Set the value of [sigle] column.
     *
     * @param string $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setSigle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sigle !== $v) {
            $this->sigle = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::SIGLE;
        }


        return $this;
    } // setSigle()

    /**
     * Set the value of [id_type_avis_rectificatif] column.
     *
     * @param int $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setIdTypeAvisRectificatif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_avis_rectificatif !== $v) {
            $this->id_type_avis_rectificatif = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_RECTIFICATIF;
        }


        return $this;
    } // setIdTypeAvisRectificatif()

    /**
     * Set the value of [id_type_avis_annulation] column.
     *
     * @param int $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setIdTypeAvisAnnulation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_avis_annulation !== $v) {
            $this->id_type_avis_annulation = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_ANNULATION;
        }


        return $this;
    } // setIdTypeAvisAnnulation()

    /**
     * Set the value of [libelle_formulaire_propose] column.
     *
     * @param string $v new value
     * @return CommonTDispositifAnnonce The current object (for fluent API support)
     */
    public function setLibelleFormulairePropose($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_formulaire_propose !== $v) {
            $this->libelle_formulaire_propose = $v;
            $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::LIBELLE_FORMULAIRE_PROPOSE;
        }


        return $this;
    } // setLibelleFormulairePropose()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->visible !== '1') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->libelle = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->id_externe = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->type = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->visible = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->id_dispositif_rectificatif = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->id_dispositif_annulation = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->sigle = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->id_type_avis_rectificatif = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->id_type_avis_annulation = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->libelle_formulaire_propose = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 11; // 11 = CommonTDispositifAnnoncePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTDispositifAnnonce object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDispositifAnnoncePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTDispositifAnnoncePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDispositifAnnoncePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTDispositifAnnonceQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDispositifAnnoncePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTDispositifAnnoncePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTDispositifAnnoncePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTDispositifAnnoncePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::LIBELLE)) {
            $modifiedColumns[':p' . $index++]  = '`libelle`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_EXTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`id_externe`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::TYPE)) {
            $modifiedColumns[':p' . $index++]  = '`type`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::VISIBLE)) {
            $modifiedColumns[':p' . $index++]  = '`visible`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_RECTIFICATIF)) {
            $modifiedColumns[':p' . $index++]  = '`id_dispositif_rectificatif`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_ANNULATION)) {
            $modifiedColumns[':p' . $index++]  = '`id_dispositif_annulation`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::SIGLE)) {
            $modifiedColumns[':p' . $index++]  = '`sigle`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_RECTIFICATIF)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_avis_rectificatif`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_ANNULATION)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_avis_annulation`';
        }
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::LIBELLE_FORMULAIRE_PROPOSE)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_formulaire_propose`';
        }

        $sql = sprintf(
            'INSERT INTO `t_dispositif_annonce` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`libelle`':
                        $stmt->bindValue($identifier, $this->libelle, PDO::PARAM_STR);
                        break;
                    case '`id_externe`':
                        $stmt->bindValue($identifier, $this->id_externe, PDO::PARAM_INT);
                        break;
                    case '`type`':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_INT);
                        break;
                    case '`visible`':
                        $stmt->bindValue($identifier, $this->visible, PDO::PARAM_STR);
                        break;
                    case '`id_dispositif_rectificatif`':
                        $stmt->bindValue($identifier, $this->id_dispositif_rectificatif, PDO::PARAM_INT);
                        break;
                    case '`id_dispositif_annulation`':
                        $stmt->bindValue($identifier, $this->id_dispositif_annulation, PDO::PARAM_INT);
                        break;
                    case '`sigle`':
                        $stmt->bindValue($identifier, $this->sigle, PDO::PARAM_STR);
                        break;
                    case '`id_type_avis_rectificatif`':
                        $stmt->bindValue($identifier, $this->id_type_avis_rectificatif, PDO::PARAM_INT);
                        break;
                    case '`id_type_avis_annulation`':
                        $stmt->bindValue($identifier, $this->id_type_avis_annulation, PDO::PARAM_INT);
                        break;
                    case '`libelle_formulaire_propose`':
                        $stmt->bindValue($identifier, $this->libelle_formulaire_propose, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonTDispositifAnnoncePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDispositifAnnoncePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getLibelle();
                break;
            case 2:
                return $this->getIdExterne();
                break;
            case 3:
                return $this->getType();
                break;
            case 4:
                return $this->getVisible();
                break;
            case 5:
                return $this->getIdDispositifRectificatif();
                break;
            case 6:
                return $this->getIdDispositifAnnulation();
                break;
            case 7:
                return $this->getSigle();
                break;
            case 8:
                return $this->getIdTypeAvisRectificatif();
                break;
            case 9:
                return $this->getIdTypeAvisAnnulation();
                break;
            case 10:
                return $this->getLibelleFormulairePropose();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['CommonTDispositifAnnonce'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTDispositifAnnonce'][$this->getPrimaryKey()] = true;
        $keys = CommonTDispositifAnnoncePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getLibelle(),
            $keys[2] => $this->getIdExterne(),
            $keys[3] => $this->getType(),
            $keys[4] => $this->getVisible(),
            $keys[5] => $this->getIdDispositifRectificatif(),
            $keys[6] => $this->getIdDispositifAnnulation(),
            $keys[7] => $this->getSigle(),
            $keys[8] => $this->getIdTypeAvisRectificatif(),
            $keys[9] => $this->getIdTypeAvisAnnulation(),
            $keys[10] => $this->getLibelleFormulairePropose(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDispositifAnnoncePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setLibelle($value);
                break;
            case 2:
                $this->setIdExterne($value);
                break;
            case 3:
                $this->setType($value);
                break;
            case 4:
                $this->setVisible($value);
                break;
            case 5:
                $this->setIdDispositifRectificatif($value);
                break;
            case 6:
                $this->setIdDispositifAnnulation($value);
                break;
            case 7:
                $this->setSigle($value);
                break;
            case 8:
                $this->setIdTypeAvisRectificatif($value);
                break;
            case 9:
                $this->setIdTypeAvisAnnulation($value);
                break;
            case 10:
                $this->setLibelleFormulairePropose($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTDispositifAnnoncePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setLibelle($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdExterne($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setType($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setVisible($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setIdDispositifRectificatif($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setIdDispositifAnnulation($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setSigle($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setIdTypeAvisRectificatif($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setIdTypeAvisAnnulation($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLibelleFormulairePropose($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTDispositifAnnoncePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID)) $criteria->add(CommonTDispositifAnnoncePeer::ID, $this->id);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::LIBELLE)) $criteria->add(CommonTDispositifAnnoncePeer::LIBELLE, $this->libelle);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_EXTERNE)) $criteria->add(CommonTDispositifAnnoncePeer::ID_EXTERNE, $this->id_externe);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::TYPE)) $criteria->add(CommonTDispositifAnnoncePeer::TYPE, $this->type);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::VISIBLE)) $criteria->add(CommonTDispositifAnnoncePeer::VISIBLE, $this->visible);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_RECTIFICATIF)) $criteria->add(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_RECTIFICATIF, $this->id_dispositif_rectificatif);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_ANNULATION)) $criteria->add(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_ANNULATION, $this->id_dispositif_annulation);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::SIGLE)) $criteria->add(CommonTDispositifAnnoncePeer::SIGLE, $this->sigle);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_RECTIFICATIF)) $criteria->add(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_RECTIFICATIF, $this->id_type_avis_rectificatif);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_ANNULATION)) $criteria->add(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_ANNULATION, $this->id_type_avis_annulation);
        if ($this->isColumnModified(CommonTDispositifAnnoncePeer::LIBELLE_FORMULAIRE_PROPOSE)) $criteria->add(CommonTDispositifAnnoncePeer::LIBELLE_FORMULAIRE_PROPOSE, $this->libelle_formulaire_propose);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTDispositifAnnoncePeer::DATABASE_NAME);
        $criteria->add(CommonTDispositifAnnoncePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTDispositifAnnonce (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLibelle($this->getLibelle());
        $copyObj->setIdExterne($this->getIdExterne());
        $copyObj->setType($this->getType());
        $copyObj->setVisible($this->getVisible());
        $copyObj->setIdDispositifRectificatif($this->getIdDispositifRectificatif());
        $copyObj->setIdDispositifAnnulation($this->getIdDispositifAnnulation());
        $copyObj->setSigle($this->getSigle());
        $copyObj->setIdTypeAvisRectificatif($this->getIdTypeAvisRectificatif());
        $copyObj->setIdTypeAvisAnnulation($this->getIdTypeAvisAnnulation());
        $copyObj->setLibelleFormulairePropose($this->getLibelleFormulairePropose());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTDispositifAnnonce Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTDispositifAnnoncePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTDispositifAnnoncePeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->libelle = null;
        $this->id_externe = null;
        $this->type = null;
        $this->visible = null;
        $this->id_dispositif_rectificatif = null;
        $this->id_dispositif_annulation = null;
        $this->sigle = null;
        $this->id_type_avis_rectificatif = null;
        $this->id_type_avis_annulation = null;
        $this->libelle_formulaire_propose = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTDispositifAnnoncePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
