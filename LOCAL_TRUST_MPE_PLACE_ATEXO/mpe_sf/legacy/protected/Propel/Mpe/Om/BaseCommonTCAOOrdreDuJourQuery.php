<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTCAOCommission;
use Application\Propel\Mpe\CommonTCAOCommissionConsultation;
use Application\Propel\Mpe\CommonTCAOOrdreDuJour;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourPeer;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourQuery;
use Application\Propel\Mpe\CommonTCAOSeance;

/**
 * Base class that represents a query for the 't_CAO_Ordre_Du_Jour' table.
 *
 *
 *
 * @method CommonTCAOOrdreDuJourQuery orderByIdOrdreDuJour($order = Criteria::ASC) Order by the id_ordre_du_jour column
 * @method CommonTCAOOrdreDuJourQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTCAOOrdreDuJourQuery orderByIdSeance($order = Criteria::ASC) Order by the id_seance column
 * @method CommonTCAOOrdreDuJourQuery orderByIdCommission($order = Criteria::ASC) Order by the id_commission column
 * @method CommonTCAOOrdreDuJourQuery orderByIdCommissionConsultation($order = Criteria::ASC) Order by the id_commission_consultation column
 * @method CommonTCAOOrdreDuJourQuery orderByNumero($order = Criteria::ASC) Order by the numero column
 * @method CommonTCAOOrdreDuJourQuery orderByIdRefOrgValEtape($order = Criteria::ASC) Order by the id_ref_org_val_etape column
 * @method CommonTCAOOrdreDuJourQuery orderByIntitule($order = Criteria::ASC) Order by the intitule column
 * @method CommonTCAOOrdreDuJourQuery orderByDateSeance($order = Criteria::ASC) Order by the date_seance column
 * @method CommonTCAOOrdreDuJourQuery orderByHeurePassage($order = Criteria::ASC) Order by the heure_passage column
 *
 * @method CommonTCAOOrdreDuJourQuery groupByIdOrdreDuJour() Group by the id_ordre_du_jour column
 * @method CommonTCAOOrdreDuJourQuery groupByOrganisme() Group by the organisme column
 * @method CommonTCAOOrdreDuJourQuery groupByIdSeance() Group by the id_seance column
 * @method CommonTCAOOrdreDuJourQuery groupByIdCommission() Group by the id_commission column
 * @method CommonTCAOOrdreDuJourQuery groupByIdCommissionConsultation() Group by the id_commission_consultation column
 * @method CommonTCAOOrdreDuJourQuery groupByNumero() Group by the numero column
 * @method CommonTCAOOrdreDuJourQuery groupByIdRefOrgValEtape() Group by the id_ref_org_val_etape column
 * @method CommonTCAOOrdreDuJourQuery groupByIntitule() Group by the intitule column
 * @method CommonTCAOOrdreDuJourQuery groupByDateSeance() Group by the date_seance column
 * @method CommonTCAOOrdreDuJourQuery groupByHeurePassage() Group by the heure_passage column
 *
 * @method CommonTCAOOrdreDuJourQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTCAOOrdreDuJourQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTCAOOrdreDuJourQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTCAOOrdreDuJourQuery leftJoinCommonTCAOSeanceRelatedByDateSeance($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCAOSeanceRelatedByDateSeance relation
 * @method CommonTCAOOrdreDuJourQuery rightJoinCommonTCAOSeanceRelatedByDateSeance($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCAOSeanceRelatedByDateSeance relation
 * @method CommonTCAOOrdreDuJourQuery innerJoinCommonTCAOSeanceRelatedByDateSeance($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCAOSeanceRelatedByDateSeance relation
 *
 * @method CommonTCAOOrdreDuJourQuery leftJoinCommonTCAOCommissionConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCAOCommissionConsultation relation
 * @method CommonTCAOOrdreDuJourQuery rightJoinCommonTCAOCommissionConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCAOCommissionConsultation relation
 * @method CommonTCAOOrdreDuJourQuery innerJoinCommonTCAOCommissionConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCAOCommissionConsultation relation
 *
 * @method CommonTCAOOrdreDuJourQuery leftJoinCommonTCAOCommission($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCAOCommission relation
 * @method CommonTCAOOrdreDuJourQuery rightJoinCommonTCAOCommission($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCAOCommission relation
 * @method CommonTCAOOrdreDuJourQuery innerJoinCommonTCAOCommission($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCAOCommission relation
 *
 * @method CommonTCAOOrdreDuJourQuery leftJoinCommonTCAOSeanceRelatedByIdSeance($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCAOSeanceRelatedByIdSeance relation
 * @method CommonTCAOOrdreDuJourQuery rightJoinCommonTCAOSeanceRelatedByIdSeance($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCAOSeanceRelatedByIdSeance relation
 * @method CommonTCAOOrdreDuJourQuery innerJoinCommonTCAOSeanceRelatedByIdSeance($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCAOSeanceRelatedByIdSeance relation
 *
 * @method CommonTCAOOrdreDuJourQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTCAOOrdreDuJourQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTCAOOrdreDuJourQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonTCAOOrdreDuJour findOne(PropelPDO $con = null) Return the first CommonTCAOOrdreDuJour matching the query
 * @method CommonTCAOOrdreDuJour findOneOrCreate(PropelPDO $con = null) Return the first CommonTCAOOrdreDuJour matching the query, or a new CommonTCAOOrdreDuJour object populated from the query conditions when no match is found
 *
 * @method CommonTCAOOrdreDuJour findOneByIdOrdreDuJour(string $id_ordre_du_jour) Return the first CommonTCAOOrdreDuJour filtered by the id_ordre_du_jour column
 * @method CommonTCAOOrdreDuJour findOneByOrganisme(string $organisme) Return the first CommonTCAOOrdreDuJour filtered by the organisme column
 * @method CommonTCAOOrdreDuJour findOneByIdSeance(string $id_seance) Return the first CommonTCAOOrdreDuJour filtered by the id_seance column
 * @method CommonTCAOOrdreDuJour findOneByIdCommission(string $id_commission) Return the first CommonTCAOOrdreDuJour filtered by the id_commission column
 * @method CommonTCAOOrdreDuJour findOneByIdCommissionConsultation(string $id_commission_consultation) Return the first CommonTCAOOrdreDuJour filtered by the id_commission_consultation column
 * @method CommonTCAOOrdreDuJour findOneByNumero(int $numero) Return the first CommonTCAOOrdreDuJour filtered by the numero column
 * @method CommonTCAOOrdreDuJour findOneByIdRefOrgValEtape(int $id_ref_org_val_etape) Return the first CommonTCAOOrdreDuJour filtered by the id_ref_org_val_etape column
 * @method CommonTCAOOrdreDuJour findOneByIntitule(string $intitule) Return the first CommonTCAOOrdreDuJour filtered by the intitule column
 * @method CommonTCAOOrdreDuJour findOneByDateSeance(string $date_seance) Return the first CommonTCAOOrdreDuJour filtered by the date_seance column
 * @method CommonTCAOOrdreDuJour findOneByHeurePassage(string $heure_passage) Return the first CommonTCAOOrdreDuJour filtered by the heure_passage column
 *
 * @method array findByIdOrdreDuJour(string $id_ordre_du_jour) Return CommonTCAOOrdreDuJour objects filtered by the id_ordre_du_jour column
 * @method array findByOrganisme(string $organisme) Return CommonTCAOOrdreDuJour objects filtered by the organisme column
 * @method array findByIdSeance(string $id_seance) Return CommonTCAOOrdreDuJour objects filtered by the id_seance column
 * @method array findByIdCommission(string $id_commission) Return CommonTCAOOrdreDuJour objects filtered by the id_commission column
 * @method array findByIdCommissionConsultation(string $id_commission_consultation) Return CommonTCAOOrdreDuJour objects filtered by the id_commission_consultation column
 * @method array findByNumero(int $numero) Return CommonTCAOOrdreDuJour objects filtered by the numero column
 * @method array findByIdRefOrgValEtape(int $id_ref_org_val_etape) Return CommonTCAOOrdreDuJour objects filtered by the id_ref_org_val_etape column
 * @method array findByIntitule(string $intitule) Return CommonTCAOOrdreDuJour objects filtered by the intitule column
 * @method array findByDateSeance(string $date_seance) Return CommonTCAOOrdreDuJour objects filtered by the date_seance column
 * @method array findByHeurePassage(string $heure_passage) Return CommonTCAOOrdreDuJour objects filtered by the heure_passage column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTCAOOrdreDuJourQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTCAOOrdreDuJourQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTCAOOrdreDuJour', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTCAOOrdreDuJourQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTCAOOrdreDuJourQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTCAOOrdreDuJourQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTCAOOrdreDuJourQuery) {
            return $criteria;
        }
        $query = new CommonTCAOOrdreDuJourQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$id_ordre_du_jour, $organisme]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTCAOOrdreDuJour|CommonTCAOOrdreDuJour[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTCAOOrdreDuJourPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTCAOOrdreDuJour A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_ordre_du_jour`, `organisme`, `id_seance`, `id_commission`, `id_commission_consultation`, `numero`, `id_ref_org_val_etape`, `intitule`, `date_seance`, `heure_passage` FROM `t_CAO_Ordre_Du_Jour` WHERE `id_ordre_du_jour` = :p0 AND `organisme` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTCAOOrdreDuJour();
            $obj->hydrate($row);
            CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTCAOOrdreDuJour|CommonTCAOOrdreDuJour[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTCAOOrdreDuJour[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ORGANISME, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(CommonTCAOOrdreDuJourPeer::ORGANISME, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_ordre_du_jour column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOrdreDuJour(1234); // WHERE id_ordre_du_jour = 1234
     * $query->filterByIdOrdreDuJour(array(12, 34)); // WHERE id_ordre_du_jour IN (12, 34)
     * $query->filterByIdOrdreDuJour(array('min' => 12)); // WHERE id_ordre_du_jour >= 12
     * $query->filterByIdOrdreDuJour(array('max' => 12)); // WHERE id_ordre_du_jour <= 12
     * </code>
     *
     * @param     mixed $idOrdreDuJour The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByIdOrdreDuJour($idOrdreDuJour = null, $comparison = null)
    {
        if (is_array($idOrdreDuJour)) {
            $useMinMax = false;
            if (isset($idOrdreDuJour['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR, $idOrdreDuJour['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOrdreDuJour['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR, $idOrdreDuJour['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR, $idOrdreDuJour, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the id_seance column
     *
     * Example usage:
     * <code>
     * $query->filterByIdSeance(1234); // WHERE id_seance = 1234
     * $query->filterByIdSeance(array(12, 34)); // WHERE id_seance IN (12, 34)
     * $query->filterByIdSeance(array('min' => 12)); // WHERE id_seance >= 12
     * $query->filterByIdSeance(array('max' => 12)); // WHERE id_seance <= 12
     * </code>
     *
     * @see       filterByCommonTCAOSeanceRelatedByIdSeance()
     *
     * @param     mixed $idSeance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByIdSeance($idSeance = null, $comparison = null)
    {
        if (is_array($idSeance)) {
            $useMinMax = false;
            if (isset($idSeance['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_SEANCE, $idSeance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idSeance['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_SEANCE, $idSeance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_SEANCE, $idSeance, $comparison);
    }

    /**
     * Filter the query on the id_commission column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCommission(1234); // WHERE id_commission = 1234
     * $query->filterByIdCommission(array(12, 34)); // WHERE id_commission IN (12, 34)
     * $query->filterByIdCommission(array('min' => 12)); // WHERE id_commission >= 12
     * $query->filterByIdCommission(array('max' => 12)); // WHERE id_commission <= 12
     * </code>
     *
     * @see       filterByCommonTCAOCommission()
     *
     * @param     mixed $idCommission The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByIdCommission($idCommission = null, $comparison = null)
    {
        if (is_array($idCommission)) {
            $useMinMax = false;
            if (isset($idCommission['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, $idCommission['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCommission['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, $idCommission['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, $idCommission, $comparison);
    }

    /**
     * Filter the query on the id_commission_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCommissionConsultation(1234); // WHERE id_commission_consultation = 1234
     * $query->filterByIdCommissionConsultation(array(12, 34)); // WHERE id_commission_consultation IN (12, 34)
     * $query->filterByIdCommissionConsultation(array('min' => 12)); // WHERE id_commission_consultation >= 12
     * $query->filterByIdCommissionConsultation(array('max' => 12)); // WHERE id_commission_consultation <= 12
     * </code>
     *
     * @see       filterByCommonTCAOCommissionConsultation()
     *
     * @param     mixed $idCommissionConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByIdCommissionConsultation($idCommissionConsultation = null, $comparison = null)
    {
        if (is_array($idCommissionConsultation)) {
            $useMinMax = false;
            if (isset($idCommissionConsultation['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, $idCommissionConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCommissionConsultation['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, $idCommissionConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, $idCommissionConsultation, $comparison);
    }

    /**
     * Filter the query on the numero column
     *
     * Example usage:
     * <code>
     * $query->filterByNumero(1234); // WHERE numero = 1234
     * $query->filterByNumero(array(12, 34)); // WHERE numero IN (12, 34)
     * $query->filterByNumero(array('min' => 12)); // WHERE numero >= 12
     * $query->filterByNumero(array('max' => 12)); // WHERE numero <= 12
     * </code>
     *
     * @param     mixed $numero The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByNumero($numero = null, $comparison = null)
    {
        if (is_array($numero)) {
            $useMinMax = false;
            if (isset($numero['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::NUMERO, $numero['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numero['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::NUMERO, $numero['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::NUMERO, $numero, $comparison);
    }

    /**
     * Filter the query on the id_ref_org_val_etape column
     *
     * Example usage:
     * <code>
     * $query->filterByIdRefOrgValEtape(1234); // WHERE id_ref_org_val_etape = 1234
     * $query->filterByIdRefOrgValEtape(array(12, 34)); // WHERE id_ref_org_val_etape IN (12, 34)
     * $query->filterByIdRefOrgValEtape(array('min' => 12)); // WHERE id_ref_org_val_etape >= 12
     * $query->filterByIdRefOrgValEtape(array('max' => 12)); // WHERE id_ref_org_val_etape <= 12
     * </code>
     *
     * @param     mixed $idRefOrgValEtape The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByIdRefOrgValEtape($idRefOrgValEtape = null, $comparison = null)
    {
        if (is_array($idRefOrgValEtape)) {
            $useMinMax = false;
            if (isset($idRefOrgValEtape['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_REF_ORG_VAL_ETAPE, $idRefOrgValEtape['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idRefOrgValEtape['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_REF_ORG_VAL_ETAPE, $idRefOrgValEtape['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_REF_ORG_VAL_ETAPE, $idRefOrgValEtape, $comparison);
    }

    /**
     * Filter the query on the intitule column
     *
     * Example usage:
     * <code>
     * $query->filterByIntitule('fooValue');   // WHERE intitule = 'fooValue'
     * $query->filterByIntitule('%fooValue%'); // WHERE intitule LIKE '%fooValue%'
     * </code>
     *
     * @param     string $intitule The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByIntitule($intitule = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($intitule)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $intitule)) {
                $intitule = str_replace('*', '%', $intitule);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::INTITULE, $intitule, $comparison);
    }

    /**
     * Filter the query on the date_seance column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSeance('2011-03-14'); // WHERE date_seance = '2011-03-14'
     * $query->filterByDateSeance('now'); // WHERE date_seance = '2011-03-14'
     * $query->filterByDateSeance(array('max' => 'yesterday')); // WHERE date_seance > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateSeance The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByDateSeance($dateSeance = null, $comparison = null)
    {
        if (is_array($dateSeance)) {
            $useMinMax = false;
            if (isset($dateSeance['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, $dateSeance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSeance['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, $dateSeance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, $dateSeance, $comparison);
    }

    /**
     * Filter the query on the heure_passage column
     *
     * Example usage:
     * <code>
     * $query->filterByHeurePassage('2011-03-14'); // WHERE heure_passage = '2011-03-14'
     * $query->filterByHeurePassage('now'); // WHERE heure_passage = '2011-03-14'
     * $query->filterByHeurePassage(array('max' => 'yesterday')); // WHERE heure_passage > '2011-03-13'
     * </code>
     *
     * @param     mixed $heurePassage The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function filterByHeurePassage($heurePassage = null, $comparison = null)
    {
        if (is_array($heurePassage)) {
            $useMinMax = false;
            if (isset($heurePassage['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::HEURE_PASSAGE, $heurePassage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($heurePassage['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::HEURE_PASSAGE, $heurePassage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourPeer::HEURE_PASSAGE, $heurePassage, $comparison);
    }

    /**
     * Filter the query by a related CommonTCAOSeance object
     *
     * @param   CommonTCAOSeance|PropelObjectCollection $commonTCAOSeance The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCAOSeanceRelatedByDateSeance($commonTCAOSeance, $comparison = null)
    {
        if ($commonTCAOSeance instanceof CommonTCAOSeance) {
            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, $commonTCAOSeance->getDate(), $comparison);
        } elseif ($commonTCAOSeance instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, $commonTCAOSeance->toKeyValue('Date', 'Date'), $comparison);
        } else {
            throw new PropelException('filterByCommonTCAOSeanceRelatedByDateSeance() only accepts arguments of type CommonTCAOSeance or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCAOSeanceRelatedByDateSeance relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function joinCommonTCAOSeanceRelatedByDateSeance($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCAOSeanceRelatedByDateSeance');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCAOSeanceRelatedByDateSeance');
        }

        return $this;
    }

    /**
     * Use the CommonTCAOSeanceRelatedByDateSeance relation CommonTCAOSeance object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCAOSeanceQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCAOSeanceRelatedByDateSeanceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTCAOSeanceRelatedByDateSeance($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCAOSeanceRelatedByDateSeance', '\Application\Propel\Mpe\CommonTCAOSeanceQuery');
    }

    /**
     * Filter the query by a related CommonTCAOCommissionConsultation object
     *
     * @param   CommonTCAOCommissionConsultation|PropelObjectCollection $commonTCAOCommissionConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCAOCommissionConsultation($commonTCAOCommissionConsultation, $comparison = null)
    {
        if ($commonTCAOCommissionConsultation instanceof CommonTCAOCommissionConsultation) {
            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, $commonTCAOCommissionConsultation->getIdCommissionConsultation(), $comparison);
        } elseif ($commonTCAOCommissionConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, $commonTCAOCommissionConsultation->toKeyValue('IdCommissionConsultation', 'IdCommissionConsultation'), $comparison);
        } else {
            throw new PropelException('filterByCommonTCAOCommissionConsultation() only accepts arguments of type CommonTCAOCommissionConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCAOCommissionConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function joinCommonTCAOCommissionConsultation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCAOCommissionConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCAOCommissionConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonTCAOCommissionConsultation relation CommonTCAOCommissionConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCAOCommissionConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCAOCommissionConsultationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTCAOCommissionConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCAOCommissionConsultation', '\Application\Propel\Mpe\CommonTCAOCommissionConsultationQuery');
    }

    /**
     * Filter the query by a related CommonTCAOCommission object
     *
     * @param   CommonTCAOCommission|PropelObjectCollection $commonTCAOCommission The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCAOCommission($commonTCAOCommission, $comparison = null)
    {
        if ($commonTCAOCommission instanceof CommonTCAOCommission) {
            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, $commonTCAOCommission->getIdCommission(), $comparison);
        } elseif ($commonTCAOCommission instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, $commonTCAOCommission->toKeyValue('IdCommission', 'IdCommission'), $comparison);
        } else {
            throw new PropelException('filterByCommonTCAOCommission() only accepts arguments of type CommonTCAOCommission or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCAOCommission relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function joinCommonTCAOCommission($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCAOCommission');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCAOCommission');
        }

        return $this;
    }

    /**
     * Use the CommonTCAOCommission relation CommonTCAOCommission object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCAOCommissionQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCAOCommissionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTCAOCommission($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCAOCommission', '\Application\Propel\Mpe\CommonTCAOCommissionQuery');
    }

    /**
     * Filter the query by a related CommonTCAOSeance object
     *
     * @param   CommonTCAOSeance|PropelObjectCollection $commonTCAOSeance The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCAOSeanceRelatedByIdSeance($commonTCAOSeance, $comparison = null)
    {
        if ($commonTCAOSeance instanceof CommonTCAOSeance) {
            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_SEANCE, $commonTCAOSeance->getIdSeance(), $comparison);
        } elseif ($commonTCAOSeance instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::ID_SEANCE, $commonTCAOSeance->toKeyValue('IdSeance', 'IdSeance'), $comparison);
        } else {
            throw new PropelException('filterByCommonTCAOSeanceRelatedByIdSeance() only accepts arguments of type CommonTCAOSeance or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCAOSeanceRelatedByIdSeance relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function joinCommonTCAOSeanceRelatedByIdSeance($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCAOSeanceRelatedByIdSeance');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCAOSeanceRelatedByIdSeance');
        }

        return $this;
    }

    /**
     * Use the CommonTCAOSeanceRelatedByIdSeance relation CommonTCAOSeance object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCAOSeanceQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCAOSeanceRelatedByIdSeanceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTCAOSeanceRelatedByIdSeance($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCAOSeanceRelatedByIdSeance', '\Application\Propel\Mpe\CommonTCAOSeanceQuery');
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::ORGANISME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourPeer::ORGANISME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTCAOOrdreDuJour $commonTCAOOrdreDuJour Object to remove from the list of results
     *
     * @return CommonTCAOOrdreDuJourQuery The current query, for fluid interface
     */
    public function prune($commonTCAOOrdreDuJour = null)
    {
        if ($commonTCAOOrdreDuJour) {
            $this->addCond('pruneCond0', $this->getAliasedColName(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR), $commonTCAOOrdreDuJour->getIdOrdreDuJour(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(CommonTCAOOrdreDuJourPeer::ORGANISME), $commonTCAOOrdreDuJour->getOrganisme(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
