<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonLangueRubrique;
use Application\Propel\Mpe\CommonModuleAutoformation;
use Application\Propel\Mpe\CommonRubrique;
use Application\Propel\Mpe\CommonRubriquePeer;
use Application\Propel\Mpe\CommonRubriqueQuery;

/**
 * Base class that represents a query for the 'rubrique' table.
 *
 *
 *
 * @method CommonRubriqueQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonRubriqueQuery orderByIsAgent($order = Criteria::ASC) Order by the is_agent column
 *
 * @method CommonRubriqueQuery groupById() Group by the id column
 * @method CommonRubriqueQuery groupByIsAgent() Group by the is_agent column
 *
 * @method CommonRubriqueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonRubriqueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonRubriqueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonRubriqueQuery leftJoinCommonLangueRubrique($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonLangueRubrique relation
 * @method CommonRubriqueQuery rightJoinCommonLangueRubrique($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonLangueRubrique relation
 * @method CommonRubriqueQuery innerJoinCommonLangueRubrique($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonLangueRubrique relation
 *
 * @method CommonRubriqueQuery leftJoinCommonModuleAutoformation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonModuleAutoformation relation
 * @method CommonRubriqueQuery rightJoinCommonModuleAutoformation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonModuleAutoformation relation
 * @method CommonRubriqueQuery innerJoinCommonModuleAutoformation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonModuleAutoformation relation
 *
 * @method CommonRubrique findOne(PropelPDO $con = null) Return the first CommonRubrique matching the query
 * @method CommonRubrique findOneOrCreate(PropelPDO $con = null) Return the first CommonRubrique matching the query, or a new CommonRubrique object populated from the query conditions when no match is found
 *
 * @method CommonRubrique findOneByIsAgent(boolean $is_agent) Return the first CommonRubrique filtered by the is_agent column
 *
 * @method array findById(int $id) Return CommonRubrique objects filtered by the id column
 * @method array findByIsAgent(boolean $is_agent) Return CommonRubrique objects filtered by the is_agent column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonRubriqueQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonRubriqueQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonRubrique', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonRubriqueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonRubriqueQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonRubriqueQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonRubriqueQuery) {
            return $criteria;
        }
        $query = new CommonRubriqueQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonRubrique|CommonRubrique[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonRubriquePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonRubriquePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonRubrique A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonRubrique A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `is_agent` FROM `rubrique` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonRubrique();
            $obj->hydrate($row);
            CommonRubriquePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonRubrique|CommonRubrique[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonRubrique[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonRubriqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonRubriquePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonRubriqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonRubriquePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonRubriqueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonRubriquePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonRubriquePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonRubriquePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the is_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIsAgent(true); // WHERE is_agent = true
     * $query->filterByIsAgent('yes'); // WHERE is_agent = true
     * </code>
     *
     * @param     boolean|string $isAgent The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonRubriqueQuery The current query, for fluid interface
     */
    public function filterByIsAgent($isAgent = null, $comparison = null)
    {
        if (is_string($isAgent)) {
            $isAgent = in_array(strtolower($isAgent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonRubriquePeer::IS_AGENT, $isAgent, $comparison);
    }

    /**
     * Filter the query by a related CommonLangueRubrique object
     *
     * @param   CommonLangueRubrique|PropelObjectCollection $commonLangueRubrique  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonRubriqueQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonLangueRubrique($commonLangueRubrique, $comparison = null)
    {
        if ($commonLangueRubrique instanceof CommonLangueRubrique) {
            return $this
                ->addUsingAlias(CommonRubriquePeer::ID, $commonLangueRubrique->getRubriqueId(), $comparison);
        } elseif ($commonLangueRubrique instanceof PropelObjectCollection) {
            return $this
                ->useCommonLangueRubriqueQuery()
                ->filterByPrimaryKeys($commonLangueRubrique->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonLangueRubrique() only accepts arguments of type CommonLangueRubrique or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonLangueRubrique relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonRubriqueQuery The current query, for fluid interface
     */
    public function joinCommonLangueRubrique($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonLangueRubrique');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonLangueRubrique');
        }

        return $this;
    }

    /**
     * Use the CommonLangueRubrique relation CommonLangueRubrique object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonLangueRubriqueQuery A secondary query class using the current class as primary query
     */
    public function useCommonLangueRubriqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonLangueRubrique($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonLangueRubrique', '\Application\Propel\Mpe\CommonLangueRubriqueQuery');
    }

    /**
     * Filter the query by a related CommonModuleAutoformation object
     *
     * @param   CommonModuleAutoformation|PropelObjectCollection $commonModuleAutoformation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonRubriqueQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonModuleAutoformation($commonModuleAutoformation, $comparison = null)
    {
        if ($commonModuleAutoformation instanceof CommonModuleAutoformation) {
            return $this
                ->addUsingAlias(CommonRubriquePeer::ID, $commonModuleAutoformation->getRubriqueId(), $comparison);
        } elseif ($commonModuleAutoformation instanceof PropelObjectCollection) {
            return $this
                ->useCommonModuleAutoformationQuery()
                ->filterByPrimaryKeys($commonModuleAutoformation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonModuleAutoformation() only accepts arguments of type CommonModuleAutoformation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonModuleAutoformation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonRubriqueQuery The current query, for fluid interface
     */
    public function joinCommonModuleAutoformation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonModuleAutoformation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonModuleAutoformation');
        }

        return $this;
    }

    /**
     * Use the CommonModuleAutoformation relation CommonModuleAutoformation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonModuleAutoformationQuery A secondary query class using the current class as primary query
     */
    public function useCommonModuleAutoformationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonModuleAutoformation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonModuleAutoformation', '\Application\Propel\Mpe\CommonModuleAutoformationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonRubrique $commonRubrique Object to remove from the list of results
     *
     * @return CommonRubriqueQuery The current query, for fluid interface
     */
    public function prune($commonRubrique = null)
    {
        if ($commonRubrique) {
            $this->addUsingAlias(CommonRubriquePeer::ID, $commonRubrique->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
