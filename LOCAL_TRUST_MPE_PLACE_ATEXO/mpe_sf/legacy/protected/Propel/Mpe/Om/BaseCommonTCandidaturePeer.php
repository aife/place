<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidaturePeer;
use Application\Propel\Mpe\CommonTDumeContextePeer;
use Application\Propel\Mpe\Map\CommonTCandidatureTableMap;

/**
 * Base static class for performing query and update operations on the 't_candidature' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTCandidaturePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_candidature';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTCandidature';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTCandidatureTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 15;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 15;

    /** the column name for the id field */
    const ID = 't_candidature.id';

    /** the column name for the ref_consultation field */
    const REF_CONSULTATION = 't_candidature.ref_consultation';

    /** the column name for the organisme field */
    const ORGANISME = 't_candidature.organisme';

    /** the column name for the old_id_inscrit field */
    const OLD_ID_INSCRIT = 't_candidature.old_id_inscrit';

    /** the column name for the id_entreprise field */
    const ID_ENTREPRISE = 't_candidature.id_entreprise';

    /** the column name for the id_etablissement field */
    const ID_ETABLISSEMENT = 't_candidature.id_etablissement';

    /** the column name for the status field */
    const STATUS = 't_candidature.status';

    /** the column name for the type_candidature field */
    const TYPE_CANDIDATURE = 't_candidature.type_candidature';

    /** the column name for the type_candidature_dume field */
    const TYPE_CANDIDATURE_DUME = 't_candidature.type_candidature_dume';

    /** the column name for the id_dume_contexte field */
    const ID_DUME_CONTEXTE = 't_candidature.id_dume_contexte';

    /** the column name for the id_offre field */
    const ID_OFFRE = 't_candidature.id_offre';

    /** the column name for the role_inscrit field */
    const ROLE_INSCRIT = 't_candidature.role_inscrit';

    /** the column name for the date_derniere_validation_dume field */
    const DATE_DERNIERE_VALIDATION_DUME = 't_candidature.date_derniere_validation_dume';

    /** the column name for the consultation_id field */
    const CONSULTATION_ID = 't_candidature.consultation_id';

    /** the column name for the id_inscrit field */
    const ID_INSCRIT = 't_candidature.id_inscrit';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTCandidature objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTCandidature[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTCandidaturePeer::$fieldNames[CommonTCandidaturePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'RefConsultation', 'Organisme', 'OldIdInscrit', 'IdEntreprise', 'IdEtablissement', 'Status', 'TypeCandidature', 'TypeCandidatureDume', 'IdDumeContexte', 'IdOffre', 'RoleInscrit', 'DateDerniereValidationDume', 'ConsultationId', 'IdInscrit', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'refConsultation', 'organisme', 'oldIdInscrit', 'idEntreprise', 'idEtablissement', 'status', 'typeCandidature', 'typeCandidatureDume', 'idDumeContexte', 'idOffre', 'roleInscrit', 'dateDerniereValidationDume', 'consultationId', 'idInscrit', ),
        BasePeer::TYPE_COLNAME => array (CommonTCandidaturePeer::ID, CommonTCandidaturePeer::REF_CONSULTATION, CommonTCandidaturePeer::ORGANISME, CommonTCandidaturePeer::OLD_ID_INSCRIT, CommonTCandidaturePeer::ID_ENTREPRISE, CommonTCandidaturePeer::ID_ETABLISSEMENT, CommonTCandidaturePeer::STATUS, CommonTCandidaturePeer::TYPE_CANDIDATURE, CommonTCandidaturePeer::TYPE_CANDIDATURE_DUME, CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTCandidaturePeer::ID_OFFRE, CommonTCandidaturePeer::ROLE_INSCRIT, CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME, CommonTCandidaturePeer::CONSULTATION_ID, CommonTCandidaturePeer::ID_INSCRIT, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'REF_CONSULTATION', 'ORGANISME', 'OLD_ID_INSCRIT', 'ID_ENTREPRISE', 'ID_ETABLISSEMENT', 'STATUS', 'TYPE_CANDIDATURE', 'TYPE_CANDIDATURE_DUME', 'ID_DUME_CONTEXTE', 'ID_OFFRE', 'ROLE_INSCRIT', 'DATE_DERNIERE_VALIDATION_DUME', 'CONSULTATION_ID', 'ID_INSCRIT', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'ref_consultation', 'organisme', 'old_id_inscrit', 'id_entreprise', 'id_etablissement', 'status', 'type_candidature', 'type_candidature_dume', 'id_dume_contexte', 'id_offre', 'role_inscrit', 'date_derniere_validation_dume', 'consultation_id', 'id_inscrit', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTCandidaturePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'RefConsultation' => 1, 'Organisme' => 2, 'OldIdInscrit' => 3, 'IdEntreprise' => 4, 'IdEtablissement' => 5, 'Status' => 6, 'TypeCandidature' => 7, 'TypeCandidatureDume' => 8, 'IdDumeContexte' => 9, 'IdOffre' => 10, 'RoleInscrit' => 11, 'DateDerniereValidationDume' => 12, 'ConsultationId' => 13, 'IdInscrit' => 14, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'refConsultation' => 1, 'organisme' => 2, 'oldIdInscrit' => 3, 'idEntreprise' => 4, 'idEtablissement' => 5, 'status' => 6, 'typeCandidature' => 7, 'typeCandidatureDume' => 8, 'idDumeContexte' => 9, 'idOffre' => 10, 'roleInscrit' => 11, 'dateDerniereValidationDume' => 12, 'consultationId' => 13, 'idInscrit' => 14, ),
        BasePeer::TYPE_COLNAME => array (CommonTCandidaturePeer::ID => 0, CommonTCandidaturePeer::REF_CONSULTATION => 1, CommonTCandidaturePeer::ORGANISME => 2, CommonTCandidaturePeer::OLD_ID_INSCRIT => 3, CommonTCandidaturePeer::ID_ENTREPRISE => 4, CommonTCandidaturePeer::ID_ETABLISSEMENT => 5, CommonTCandidaturePeer::STATUS => 6, CommonTCandidaturePeer::TYPE_CANDIDATURE => 7, CommonTCandidaturePeer::TYPE_CANDIDATURE_DUME => 8, CommonTCandidaturePeer::ID_DUME_CONTEXTE => 9, CommonTCandidaturePeer::ID_OFFRE => 10, CommonTCandidaturePeer::ROLE_INSCRIT => 11, CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME => 12, CommonTCandidaturePeer::CONSULTATION_ID => 13, CommonTCandidaturePeer::ID_INSCRIT => 14, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'REF_CONSULTATION' => 1, 'ORGANISME' => 2, 'OLD_ID_INSCRIT' => 3, 'ID_ENTREPRISE' => 4, 'ID_ETABLISSEMENT' => 5, 'STATUS' => 6, 'TYPE_CANDIDATURE' => 7, 'TYPE_CANDIDATURE_DUME' => 8, 'ID_DUME_CONTEXTE' => 9, 'ID_OFFRE' => 10, 'ROLE_INSCRIT' => 11, 'DATE_DERNIERE_VALIDATION_DUME' => 12, 'CONSULTATION_ID' => 13, 'ID_INSCRIT' => 14, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'ref_consultation' => 1, 'organisme' => 2, 'old_id_inscrit' => 3, 'id_entreprise' => 4, 'id_etablissement' => 5, 'status' => 6, 'type_candidature' => 7, 'type_candidature_dume' => 8, 'id_dume_contexte' => 9, 'id_offre' => 10, 'role_inscrit' => 11, 'date_derniere_validation_dume' => 12, 'consultation_id' => 13, 'id_inscrit' => 14, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTCandidaturePeer::getFieldNames($toType);
        $key = isset(CommonTCandidaturePeer::$fieldKeys[$fromType][$name]) ? CommonTCandidaturePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTCandidaturePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTCandidaturePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTCandidaturePeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTCandidaturePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTCandidaturePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTCandidaturePeer::ID);
            $criteria->addSelectColumn(CommonTCandidaturePeer::REF_CONSULTATION);
            $criteria->addSelectColumn(CommonTCandidaturePeer::ORGANISME);
            $criteria->addSelectColumn(CommonTCandidaturePeer::OLD_ID_INSCRIT);
            $criteria->addSelectColumn(CommonTCandidaturePeer::ID_ENTREPRISE);
            $criteria->addSelectColumn(CommonTCandidaturePeer::ID_ETABLISSEMENT);
            $criteria->addSelectColumn(CommonTCandidaturePeer::STATUS);
            $criteria->addSelectColumn(CommonTCandidaturePeer::TYPE_CANDIDATURE);
            $criteria->addSelectColumn(CommonTCandidaturePeer::TYPE_CANDIDATURE_DUME);
            $criteria->addSelectColumn(CommonTCandidaturePeer::ID_DUME_CONTEXTE);
            $criteria->addSelectColumn(CommonTCandidaturePeer::ID_OFFRE);
            $criteria->addSelectColumn(CommonTCandidaturePeer::ROLE_INSCRIT);
            $criteria->addSelectColumn(CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME);
            $criteria->addSelectColumn(CommonTCandidaturePeer::CONSULTATION_ID);
            $criteria->addSelectColumn(CommonTCandidaturePeer::ID_INSCRIT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.ref_consultation');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.old_id_inscrit');
            $criteria->addSelectColumn($alias . '.id_entreprise');
            $criteria->addSelectColumn($alias . '.id_etablissement');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.type_candidature');
            $criteria->addSelectColumn($alias . '.type_candidature_dume');
            $criteria->addSelectColumn($alias . '.id_dume_contexte');
            $criteria->addSelectColumn($alias . '.id_offre');
            $criteria->addSelectColumn($alias . '.role_inscrit');
            $criteria->addSelectColumn($alias . '.date_derniere_validation_dume');
            $criteria->addSelectColumn($alias . '.consultation_id');
            $criteria->addSelectColumn($alias . '.id_inscrit');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTCandidature
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTCandidaturePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTCandidaturePeer::populateObjects(CommonTCandidaturePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTCandidature $obj A CommonTCandidature object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTCandidaturePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTCandidature object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTCandidature) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTCandidature object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTCandidaturePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTCandidature Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTCandidaturePeer::$instances[$key])) {
                return CommonTCandidaturePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTCandidaturePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTCandidaturePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_candidature
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTCandidaturePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTCandidaturePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTCandidaturePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTCandidature object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTCandidaturePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTCandidaturePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTCandidaturePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTDumeContexte table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTDumeContexte(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonInscrit table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonInscrit(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOffres table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonOffres(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOrganisme table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonOrganisme(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with their CommonConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;
        CommonConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to $obj2 (CommonConsultation)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with their CommonTDumeContexte objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTDumeContexte(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;
        CommonTDumeContextePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTDumeContextePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTDumeContextePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTDumeContextePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTDumeContextePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to $obj2 (CommonTDumeContexte)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with their CommonInscrit objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonInscrit(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;
        CommonInscritPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonInscritPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonInscritPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonInscritPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to $obj2 (CommonInscrit)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with their CommonOffres objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonOffres(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;
        CommonOffresPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonOffresPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonOffresPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonOffresPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to $obj2 (CommonOffres)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with their CommonOrganisme objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonOrganisme(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;
        CommonOrganismePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonOrganismePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonOrganismePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to $obj2 (CommonOrganisme)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTCandidature objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTDumeContextePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTDumeContextePeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonConsultation rows

            $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonTCandidature($obj1);
            } // if joined row not null

            // Add objects for joined CommonTDumeContexte rows

            $key3 = CommonTDumeContextePeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonTDumeContextePeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonTDumeContextePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTDumeContextePeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj3 (CommonTDumeContexte)
                $obj3->addCommonTCandidature($obj1);
            } // if joined row not null

            // Add objects for joined CommonInscrit rows

            $key4 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonInscritPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonInscritPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonInscritPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj4 (CommonInscrit)
                $obj4->addCommonTCandidature($obj1);
            } // if joined row not null

            // Add objects for joined CommonOffres rows

            $key5 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = CommonOffresPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = CommonOffresPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOffresPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj5 (CommonOffres)
                $obj5->addCommonTCandidature($obj1);
            } // if joined row not null

            // Add objects for joined CommonOrganisme rows

            $key6 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = CommonOrganismePeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonOrganismePeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj6 (CommonOrganisme)
                $obj6->addCommonTCandidature($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTDumeContexte table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTDumeContexte(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonInscrit table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonInscrit(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOffres table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonOffres(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOrganisme table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonOrganisme(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with all related objects except CommonConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonTDumeContextePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTDumeContextePeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTDumeContexte rows

                $key2 = CommonTDumeContextePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTDumeContextePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTDumeContextePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTDumeContextePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj2 (CommonTDumeContexte)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key3 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonInscritPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonInscritPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj3 (CommonInscrit)
                $obj3->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOffres rows

                $key4 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonOffresPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonOffresPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonOffresPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj4 (CommonOffres)
                $obj4->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key5 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOrganismePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOrganismePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj5 (CommonOrganisme)
                $obj5->addCommonTCandidature($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with all related objects except CommonTDumeContexte.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTDumeContexte(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonConsultation rows

                $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key3 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonInscritPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonInscritPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj3 (CommonInscrit)
                $obj3->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOffres rows

                $key4 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonOffresPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonOffresPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonOffresPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj4 (CommonOffres)
                $obj4->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key5 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOrganismePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOrganismePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj5 (CommonOrganisme)
                $obj5->addCommonTCandidature($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with all related objects except CommonInscrit.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonInscrit(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTDumeContextePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTDumeContextePeer::NUM_HYDRATE_COLUMNS;

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonConsultation rows

                $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTDumeContexte rows

                $key3 = CommonTDumeContextePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTDumeContextePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTDumeContextePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTDumeContextePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj3 (CommonTDumeContexte)
                $obj3->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOffres rows

                $key4 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonOffresPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonOffresPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonOffresPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj4 (CommonOffres)
                $obj4->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key5 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOrganismePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOrganismePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj5 (CommonOrganisme)
                $obj5->addCommonTCandidature($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with all related objects except CommonOffres.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonOffres(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTDumeContextePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTDumeContextePeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonConsultation rows

                $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTDumeContexte rows

                $key3 = CommonTDumeContextePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTDumeContextePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTDumeContextePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTDumeContextePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj3 (CommonTDumeContexte)
                $obj3->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key4 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonInscritPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonInscritPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj4 (CommonInscrit)
                $obj4->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key5 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOrganismePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOrganismePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj5 (CommonOrganisme)
                $obj5->addCommonTCandidature($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCandidature objects pre-filled with all related objects except CommonOrganisme.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonOrganisme(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);
        }

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTDumeContextePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTDumeContextePeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_DUME_CONTEXTE, CommonTDumeContextePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTCandidaturePeer::ID_OFFRE, CommonOffresPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonConsultation rows

                $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTDumeContexte rows

                $key3 = CommonTDumeContextePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTDumeContextePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTDumeContextePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTDumeContextePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj3 (CommonTDumeContexte)
                $obj3->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key4 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonInscritPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonInscritPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj4 (CommonInscrit)
                $obj4->addCommonTCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOffres rows

                $key5 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOffresPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOffresPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOffresPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTCandidature) to the collection in $obj5 (CommonOffres)
                $obj5->addCommonTCandidature($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTCandidaturePeer::DATABASE_NAME)->getTable(CommonTCandidaturePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTCandidaturePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTCandidaturePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTCandidatureTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTCandidaturePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTCandidature or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTCandidature object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTCandidature object
        }

        if ($criteria->containsKey(CommonTCandidaturePeer::ID) && $criteria->keyContainsValue(CommonTCandidaturePeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTCandidaturePeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTCandidature or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTCandidature object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTCandidaturePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTCandidaturePeer::ID);
            $value = $criteria->remove(CommonTCandidaturePeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTCandidaturePeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTCandidaturePeer::TABLE_NAME);
            }

        } else { // $values is CommonTCandidature object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_candidature table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTCandidaturePeer::TABLE_NAME, $con, CommonTCandidaturePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTCandidaturePeer::clearInstancePool();
            CommonTCandidaturePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTCandidature or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTCandidature object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTCandidaturePeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTCandidature) { // it's a model object
            // invalidate the cache for this single object
            CommonTCandidaturePeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTCandidaturePeer::DATABASE_NAME);
            $criteria->add(CommonTCandidaturePeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTCandidaturePeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTCandidaturePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTCandidaturePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTCandidature object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTCandidature $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTCandidaturePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTCandidaturePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTCandidaturePeer::DATABASE_NAME, CommonTCandidaturePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTCandidature
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTCandidaturePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTCandidaturePeer::DATABASE_NAME);
        $criteria->add(CommonTCandidaturePeer::ID, $pk);

        $v = CommonTCandidaturePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTCandidature[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTCandidaturePeer::DATABASE_NAME);
            $criteria->add(CommonTCandidaturePeer::ID, $pks, Criteria::IN);
            $objs = CommonTCandidaturePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTCandidaturePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTCandidaturePeer::buildTableMap();

