<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationClausesN2;
use Application\Propel\Mpe\CommonConsultationClausesN3;
use Application\Propel\Mpe\CommonConsultationClausesN3Peer;
use Application\Propel\Mpe\CommonConsultationClausesN3Query;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN3;

/**
 * Base class that represents a query for the 'consultation_clauses_n3' table.
 *
 *
 *
 * @method CommonConsultationClausesN3Query orderById($order = Criteria::ASC) Order by the id column
 * @method CommonConsultationClausesN3Query orderByClauseN2Id($order = Criteria::ASC) Order by the clause_n2_id column
 * @method CommonConsultationClausesN3Query orderByReferentielClauseN3Id($order = Criteria::ASC) Order by the referentiel_clause_n3_id column
 *
 * @method CommonConsultationClausesN3Query groupById() Group by the id column
 * @method CommonConsultationClausesN3Query groupByClauseN2Id() Group by the clause_n2_id column
 * @method CommonConsultationClausesN3Query groupByReferentielClauseN3Id() Group by the referentiel_clause_n3_id column
 *
 * @method CommonConsultationClausesN3Query leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConsultationClausesN3Query rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConsultationClausesN3Query innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConsultationClausesN3Query leftJoinCommonConsultationClausesN2($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationClausesN2 relation
 * @method CommonConsultationClausesN3Query rightJoinCommonConsultationClausesN2($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationClausesN2 relation
 * @method CommonConsultationClausesN3Query innerJoinCommonConsultationClausesN2($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationClausesN2 relation
 *
 * @method CommonConsultationClausesN3Query leftJoinCommonReferentielConsultationClausesN3($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonReferentielConsultationClausesN3 relation
 * @method CommonConsultationClausesN3Query rightJoinCommonReferentielConsultationClausesN3($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonReferentielConsultationClausesN3 relation
 * @method CommonConsultationClausesN3Query innerJoinCommonReferentielConsultationClausesN3($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonReferentielConsultationClausesN3 relation
 *
 * @method CommonConsultationClausesN3 findOne(PropelPDO $con = null) Return the first CommonConsultationClausesN3 matching the query
 * @method CommonConsultationClausesN3 findOneOrCreate(PropelPDO $con = null) Return the first CommonConsultationClausesN3 matching the query, or a new CommonConsultationClausesN3 object populated from the query conditions when no match is found
 *
 * @method CommonConsultationClausesN3 findOneByClauseN2Id(int $clause_n2_id) Return the first CommonConsultationClausesN3 filtered by the clause_n2_id column
 * @method CommonConsultationClausesN3 findOneByReferentielClauseN3Id(int $referentiel_clause_n3_id) Return the first CommonConsultationClausesN3 filtered by the referentiel_clause_n3_id column
 *
 * @method array findById(int $id) Return CommonConsultationClausesN3 objects filtered by the id column
 * @method array findByClauseN2Id(int $clause_n2_id) Return CommonConsultationClausesN3 objects filtered by the clause_n2_id column
 * @method array findByReferentielClauseN3Id(int $referentiel_clause_n3_id) Return CommonConsultationClausesN3 objects filtered by the referentiel_clause_n3_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationClausesN3Query extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConsultationClausesN3Query object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConsultationClausesN3', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConsultationClausesN3Query object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConsultationClausesN3Query|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConsultationClausesN3Query
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConsultationClausesN3Query) {
            return $criteria;
        }
        $query = new CommonConsultationClausesN3Query();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConsultationClausesN3|CommonConsultationClausesN3[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConsultationClausesN3Peer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN3Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationClausesN3 A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationClausesN3 A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `clause_n2_id`, `referentiel_clause_n3_id` FROM `consultation_clauses_n3` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConsultationClausesN3();
            $obj->hydrate($row);
            CommonConsultationClausesN3Peer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConsultationClausesN3|CommonConsultationClausesN3[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConsultationClausesN3[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConsultationClausesN3Query The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConsultationClausesN3Peer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConsultationClausesN3Query The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConsultationClausesN3Peer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN3Query The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN3Peer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN3Peer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN3Peer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the clause_n2_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseN2Id(1234); // WHERE clause_n2_id = 1234
     * $query->filterByClauseN2Id(array(12, 34)); // WHERE clause_n2_id IN (12, 34)
     * $query->filterByClauseN2Id(array('min' => 12)); // WHERE clause_n2_id >= 12
     * $query->filterByClauseN2Id(array('max' => 12)); // WHERE clause_n2_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultationClausesN2()
     *
     * @param     mixed $clauseN2Id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN3Query The current query, for fluid interface
     */
    public function filterByClauseN2Id($clauseN2Id = null, $comparison = null)
    {
        if (is_array($clauseN2Id)) {
            $useMinMax = false;
            if (isset($clauseN2Id['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN3Peer::CLAUSE_N2_ID, $clauseN2Id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clauseN2Id['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN3Peer::CLAUSE_N2_ID, $clauseN2Id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN3Peer::CLAUSE_N2_ID, $clauseN2Id, $comparison);
    }

    /**
     * Filter the query on the referentiel_clause_n3_id column
     *
     * Example usage:
     * <code>
     * $query->filterByReferentielClauseN3Id(1234); // WHERE referentiel_clause_n3_id = 1234
     * $query->filterByReferentielClauseN3Id(array(12, 34)); // WHERE referentiel_clause_n3_id IN (12, 34)
     * $query->filterByReferentielClauseN3Id(array('min' => 12)); // WHERE referentiel_clause_n3_id >= 12
     * $query->filterByReferentielClauseN3Id(array('max' => 12)); // WHERE referentiel_clause_n3_id <= 12
     * </code>
     *
     * @see       filterByCommonReferentielConsultationClausesN3()
     *
     * @param     mixed $referentielClauseN3Id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN3Query The current query, for fluid interface
     */
    public function filterByReferentielClauseN3Id($referentielClauseN3Id = null, $comparison = null)
    {
        if (is_array($referentielClauseN3Id)) {
            $useMinMax = false;
            if (isset($referentielClauseN3Id['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN3Peer::REFERENTIEL_CLAUSE_N3_ID, $referentielClauseN3Id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($referentielClauseN3Id['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN3Peer::REFERENTIEL_CLAUSE_N3_ID, $referentielClauseN3Id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN3Peer::REFERENTIEL_CLAUSE_N3_ID, $referentielClauseN3Id, $comparison);
    }

    /**
     * Filter the query by a related CommonConsultationClausesN2 object
     *
     * @param   CommonConsultationClausesN2|PropelObjectCollection $commonConsultationClausesN2 The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN3Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationClausesN2($commonConsultationClausesN2, $comparison = null)
    {
        if ($commonConsultationClausesN2 instanceof CommonConsultationClausesN2) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN3Peer::CLAUSE_N2_ID, $commonConsultationClausesN2->getId(), $comparison);
        } elseif ($commonConsultationClausesN2 instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationClausesN3Peer::CLAUSE_N2_ID, $commonConsultationClausesN2->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultationClausesN2() only accepts arguments of type CommonConsultationClausesN2 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationClausesN2 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN3Query The current query, for fluid interface
     */
    public function joinCommonConsultationClausesN2($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationClausesN2');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationClausesN2');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationClausesN2 relation CommonConsultationClausesN2 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationClausesN2Query A secondary query class using the current class as primary query
     */
    public function useCommonConsultationClausesN2Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationClausesN2($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationClausesN2', '\Application\Propel\Mpe\CommonConsultationClausesN2Query');
    }

    /**
     * Filter the query by a related CommonReferentielConsultationClausesN3 object
     *
     * @param   CommonReferentielConsultationClausesN3|PropelObjectCollection $commonReferentielConsultationClausesN3 The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN3Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonReferentielConsultationClausesN3($commonReferentielConsultationClausesN3, $comparison = null)
    {
        if ($commonReferentielConsultationClausesN3 instanceof CommonReferentielConsultationClausesN3) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN3Peer::REFERENTIEL_CLAUSE_N3_ID, $commonReferentielConsultationClausesN3->getId(), $comparison);
        } elseif ($commonReferentielConsultationClausesN3 instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationClausesN3Peer::REFERENTIEL_CLAUSE_N3_ID, $commonReferentielConsultationClausesN3->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonReferentielConsultationClausesN3() only accepts arguments of type CommonReferentielConsultationClausesN3 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonReferentielConsultationClausesN3 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN3Query The current query, for fluid interface
     */
    public function joinCommonReferentielConsultationClausesN3($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonReferentielConsultationClausesN3');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonReferentielConsultationClausesN3');
        }

        return $this;
    }

    /**
     * Use the CommonReferentielConsultationClausesN3 relation CommonReferentielConsultationClausesN3 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonReferentielConsultationClausesN3Query A secondary query class using the current class as primary query
     */
    public function useCommonReferentielConsultationClausesN3Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonReferentielConsultationClausesN3($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonReferentielConsultationClausesN3', '\Application\Propel\Mpe\CommonReferentielConsultationClausesN3Query');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConsultationClausesN3 $commonConsultationClausesN3 Object to remove from the list of results
     *
     * @return CommonConsultationClausesN3Query The current query, for fluid interface
     */
    public function prune($commonConsultationClausesN3 = null)
    {
        if ($commonConsultationClausesN3) {
            $this->addUsingAlias(CommonConsultationClausesN3Peer::ID, $commonConsultationClausesN3->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
