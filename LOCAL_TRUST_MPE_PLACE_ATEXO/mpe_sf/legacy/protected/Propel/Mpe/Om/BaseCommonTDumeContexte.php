<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidatureQuery;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTDumeContextePeer;
use Application\Propel\Mpe\CommonTDumeContexteQuery;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Propel\Mpe\CommonTDumeNumeroQuery;

/**
 * Base class that represents a row from the 't_dume_contexte' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDumeContexte extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTDumeContextePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTDumeContextePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the ref_consultation field.
     * @var        int
     */
    protected $ref_consultation;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the contexte_lt_dume_id field.
     * @var        string
     */
    protected $contexte_lt_dume_id;

    /**
     * The value for the type_dume field.
     * @var        string
     */
    protected $type_dume;

    /**
     * The value for the status field.
     * Note: this column has a database default value of: 99
     * @var        int
     */
    protected $status;

    /**
     * The value for the is_standard field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $is_standard;

    /**
     * The value for the date_creation field.
     * Note: this column has a database default value of: '2022-06-15 17:08:22'
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the consultation_id field.
     * @var        int
     */
    protected $consultation_id;

    /**
     * @var        CommonConsultation
     */
    protected $aCommonConsultation;

    /**
     * @var        CommonOrganisme
     */
    protected $aCommonOrganisme;

    /**
     * @var        PropelObjectCollection|CommonTCandidature[] Collection to store aggregation of CommonTCandidature objects.
     */
    protected $collCommonTCandidatures;
    protected $collCommonTCandidaturesPartial;

    /**
     * @var        PropelObjectCollection|CommonTDumeNumero[] Collection to store aggregation of CommonTDumeNumero objects.
     */
    protected $collCommonTDumeNumeros;
    protected $collCommonTDumeNumerosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCandidaturesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTDumeNumerosScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->status = 99;
        $this->is_standard = true;
        $this->date_creation = '2022-06-15 17:08:22';
    }

    /**
     * Initializes internal state of BaseCommonTDumeContexte object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [ref_consultation] column value.
     *
     * @return int
     */
    public function getRefConsultation()
    {

        return $this->ref_consultation;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [contexte_lt_dume_id] column value.
     *
     * @return string
     */
    public function getContexteLtDumeId()
    {

        return $this->contexte_lt_dume_id;
    }

    /**
     * Get the [type_dume] column value.
     *
     * @return string
     */
    public function getTypeDume()
    {

        return $this->type_dume;
    }

    /**
     * Get the [status] column value.
     *
     * @return int
     */
    public function getStatus()
    {

        return $this->status;
    }

    /**
     * Get the [is_standard] column value.
     *
     * @return boolean
     */
    public function getIsStandard()
    {

        return $this->is_standard;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_modification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModification($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification === null) {
            return null;
        }

        if ($this->date_modification === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [consultation_id] column value.
     *
     * @return int
     */
    public function getConsultationId()
    {

        return $this->consultation_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTDumeContextePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [ref_consultation] column.
     *
     * @param int $v new value
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setRefConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ref_consultation !== $v) {
            $this->ref_consultation = $v;
            $this->modifiedColumns[] = CommonTDumeContextePeer::REF_CONSULTATION;
        }


        return $this;
    } // setRefConsultation()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTDumeContextePeer::ORGANISME;
        }

        if ($this->aCommonOrganisme !== null && $this->aCommonOrganisme->getAcronyme() !== $v) {
            $this->aCommonOrganisme = null;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [contexte_lt_dume_id] column.
     *
     * @param string $v new value
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setContexteLtDumeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->contexte_lt_dume_id !== $v) {
            $this->contexte_lt_dume_id = $v;
            $this->modifiedColumns[] = CommonTDumeContextePeer::CONTEXTE_LT_DUME_ID;
        }


        return $this;
    } // setContexteLtDumeId()

    /**
     * Set the value of [type_dume] column.
     *
     * @param string $v new value
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setTypeDume($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_dume !== $v) {
            $this->type_dume = $v;
            $this->modifiedColumns[] = CommonTDumeContextePeer::TYPE_DUME;
        }


        return $this;
    } // setTypeDume()

    /**
     * Set the value of [status] column.
     *
     * @param int $v new value
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = CommonTDumeContextePeer::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Sets the value of the [is_standard] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setIsStandard($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_standard !== $v) {
            $this->is_standard = $v;
            $this->modifiedColumns[] = CommonTDumeContextePeer::IS_STANDARD;
        }


        return $this;
    } // setIsStandard()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ( ($currentDateAsString !== $newDateAsString) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s') === '2022-06-15 17:08:22') // or the entered value matches the default
                 ) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTDumeContextePeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Sets the value of [date_modification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification !== null && $tmpDt = new DateTime($this->date_modification)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification = $newDateAsString;
                $this->modifiedColumns[] = CommonTDumeContextePeer::DATE_MODIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDateModification()

    /**
     * Set the value of [consultation_id] column.
     *
     * @param int $v new value
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setConsultationId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_id !== $v) {
            $this->consultation_id = $v;
            $this->modifiedColumns[] = CommonTDumeContextePeer::CONSULTATION_ID;
        }

        if ($this->aCommonConsultation !== null && $this->aCommonConsultation->getId() !== $v) {
            $this->aCommonConsultation = null;
        }


        return $this;
    } // setConsultationId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->status !== 99) {
                return false;
            }

            if ($this->is_standard !== true) {
                return false;
            }

            if ($this->date_creation !== '2022-06-15 17:08:22') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->ref_consultation = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->organisme = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->contexte_lt_dume_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->type_dume = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->status = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->is_standard = ($row[$startcol + 6] !== null) ? (boolean) $row[$startcol + 6] : null;
            $this->date_creation = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->date_modification = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->consultation_id = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = CommonTDumeContextePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTDumeContexte object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonOrganisme !== null && $this->organisme !== $this->aCommonOrganisme->getAcronyme()) {
            $this->aCommonOrganisme = null;
        }
        if ($this->aCommonConsultation !== null && $this->consultation_id !== $this->aCommonConsultation->getId()) {
            $this->aCommonConsultation = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDumeContextePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTDumeContextePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonConsultation = null;
            $this->aCommonOrganisme = null;
            $this->collCommonTCandidatures = null;

            $this->collCommonTDumeNumeros = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDumeContextePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTDumeContexteQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDumeContextePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTDumeContextePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonConsultation !== null) {
                if ($this->aCommonConsultation->isModified() || $this->aCommonConsultation->isNew()) {
                    $affectedRows += $this->aCommonConsultation->save($con);
                }
                $this->setCommonConsultation($this->aCommonConsultation);
            }

            if ($this->aCommonOrganisme !== null) {
                if ($this->aCommonOrganisme->isModified() || $this->aCommonOrganisme->isNew()) {
                    $affectedRows += $this->aCommonOrganisme->save($con);
                }
                $this->setCommonOrganisme($this->aCommonOrganisme);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTCandidaturesScheduledForDeletion !== null) {
                if (!$this->commonTCandidaturesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTCandidatureQuery::create()
                        ->filterByPrimaryKeys($this->commonTCandidaturesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTCandidaturesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCandidatures !== null) {
                foreach ($this->collCommonTCandidatures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTDumeNumerosScheduledForDeletion !== null) {
                if (!$this->commonTDumeNumerosScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTDumeNumeroQuery::create()
                        ->filterByPrimaryKeys($this->commonTDumeNumerosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTDumeNumerosScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTDumeNumeros !== null) {
                foreach ($this->collCommonTDumeNumeros as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTDumeContextePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTDumeContextePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTDumeContextePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTDumeContextePeer::REF_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`ref_consultation`';
        }
        if ($this->isColumnModified(CommonTDumeContextePeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTDumeContextePeer::CONTEXTE_LT_DUME_ID)) {
            $modifiedColumns[':p' . $index++]  = '`contexte_lt_dume_id`';
        }
        if ($this->isColumnModified(CommonTDumeContextePeer::TYPE_DUME)) {
            $modifiedColumns[':p' . $index++]  = '`type_dume`';
        }
        if ($this->isColumnModified(CommonTDumeContextePeer::STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`status`';
        }
        if ($this->isColumnModified(CommonTDumeContextePeer::IS_STANDARD)) {
            $modifiedColumns[':p' . $index++]  = '`is_standard`';
        }
        if ($this->isColumnModified(CommonTDumeContextePeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTDumeContextePeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonTDumeContextePeer::CONSULTATION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_id`';
        }

        $sql = sprintf(
            'INSERT INTO `t_dume_contexte` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`ref_consultation`':
                        $stmt->bindValue($identifier, $this->ref_consultation, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`contexte_lt_dume_id`':
                        $stmt->bindValue($identifier, $this->contexte_lt_dume_id, PDO::PARAM_STR);
                        break;
                    case '`type_dume`':
                        $stmt->bindValue($identifier, $this->type_dume, PDO::PARAM_STR);
                        break;
                    case '`status`':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_INT);
                        break;
                    case '`is_standard`':
                        $stmt->bindValue($identifier, (int) $this->is_standard, PDO::PARAM_INT);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`consultation_id`':
                        $stmt->bindValue($identifier, $this->consultation_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonConsultation !== null) {
                if (!$this->aCommonConsultation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonConsultation->getValidationFailures());
                }
            }

            if ($this->aCommonOrganisme !== null) {
                if (!$this->aCommonOrganisme->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonOrganisme->getValidationFailures());
                }
            }


            if (($retval = CommonTDumeContextePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTCandidatures !== null) {
                    foreach ($this->collCommonTCandidatures as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTDumeNumeros !== null) {
                    foreach ($this->collCommonTDumeNumeros as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDumeContextePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getRefConsultation();
                break;
            case 2:
                return $this->getOrganisme();
                break;
            case 3:
                return $this->getContexteLtDumeId();
                break;
            case 4:
                return $this->getTypeDume();
                break;
            case 5:
                return $this->getStatus();
                break;
            case 6:
                return $this->getIsStandard();
                break;
            case 7:
                return $this->getDateCreation();
                break;
            case 8:
                return $this->getDateModification();
                break;
            case 9:
                return $this->getConsultationId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTDumeContexte'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTDumeContexte'][$this->getPrimaryKey()] = true;
        $keys = CommonTDumeContextePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getRefConsultation(),
            $keys[2] => $this->getOrganisme(),
            $keys[3] => $this->getContexteLtDumeId(),
            $keys[4] => $this->getTypeDume(),
            $keys[5] => $this->getStatus(),
            $keys[6] => $this->getIsStandard(),
            $keys[7] => $this->getDateCreation(),
            $keys[8] => $this->getDateModification(),
            $keys[9] => $this->getConsultationId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonConsultation) {
                $result['CommonConsultation'] = $this->aCommonConsultation->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonOrganisme) {
                $result['CommonOrganisme'] = $this->aCommonOrganisme->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTCandidatures) {
                $result['CommonTCandidatures'] = $this->collCommonTCandidatures->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTDumeNumeros) {
                $result['CommonTDumeNumeros'] = $this->collCommonTDumeNumeros->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDumeContextePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setRefConsultation($value);
                break;
            case 2:
                $this->setOrganisme($value);
                break;
            case 3:
                $this->setContexteLtDumeId($value);
                break;
            case 4:
                $this->setTypeDume($value);
                break;
            case 5:
                $this->setStatus($value);
                break;
            case 6:
                $this->setIsStandard($value);
                break;
            case 7:
                $this->setDateCreation($value);
                break;
            case 8:
                $this->setDateModification($value);
                break;
            case 9:
                $this->setConsultationId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTDumeContextePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setRefConsultation($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setOrganisme($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setContexteLtDumeId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setTypeDume($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setStatus($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setIsStandard($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDateCreation($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setDateModification($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setConsultationId($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTDumeContextePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTDumeContextePeer::ID)) $criteria->add(CommonTDumeContextePeer::ID, $this->id);
        if ($this->isColumnModified(CommonTDumeContextePeer::REF_CONSULTATION)) $criteria->add(CommonTDumeContextePeer::REF_CONSULTATION, $this->ref_consultation);
        if ($this->isColumnModified(CommonTDumeContextePeer::ORGANISME)) $criteria->add(CommonTDumeContextePeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTDumeContextePeer::CONTEXTE_LT_DUME_ID)) $criteria->add(CommonTDumeContextePeer::CONTEXTE_LT_DUME_ID, $this->contexte_lt_dume_id);
        if ($this->isColumnModified(CommonTDumeContextePeer::TYPE_DUME)) $criteria->add(CommonTDumeContextePeer::TYPE_DUME, $this->type_dume);
        if ($this->isColumnModified(CommonTDumeContextePeer::STATUS)) $criteria->add(CommonTDumeContextePeer::STATUS, $this->status);
        if ($this->isColumnModified(CommonTDumeContextePeer::IS_STANDARD)) $criteria->add(CommonTDumeContextePeer::IS_STANDARD, $this->is_standard);
        if ($this->isColumnModified(CommonTDumeContextePeer::DATE_CREATION)) $criteria->add(CommonTDumeContextePeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTDumeContextePeer::DATE_MODIFICATION)) $criteria->add(CommonTDumeContextePeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonTDumeContextePeer::CONSULTATION_ID)) $criteria->add(CommonTDumeContextePeer::CONSULTATION_ID, $this->consultation_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTDumeContextePeer::DATABASE_NAME);
        $criteria->add(CommonTDumeContextePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTDumeContexte (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setRefConsultation($this->getRefConsultation());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setContexteLtDumeId($this->getContexteLtDumeId());
        $copyObj->setTypeDume($this->getTypeDume());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setIsStandard($this->getIsStandard());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setConsultationId($this->getConsultationId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTCandidatures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCandidature($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTDumeNumeros() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTDumeNumero($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTDumeContexte Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTDumeContextePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTDumeContextePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonConsultation object.
     *
     * @param   CommonConsultation $v
     * @return CommonTDumeContexte The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonConsultation(CommonConsultation $v = null)
    {
        if ($v === null) {
            $this->setConsultationId(NULL);
        } else {
            $this->setConsultationId($v->getId());
        }

        $this->aCommonConsultation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonConsultation object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTDumeContexte($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonConsultation object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonConsultation The associated CommonConsultation object.
     * @throws PropelException
     */
    public function getCommonConsultation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonConsultation === null && ($this->consultation_id !== null) && $doQuery) {
            $this->aCommonConsultation = CommonConsultationQuery::create()->findPk($this->consultation_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonConsultation->addCommonTDumeContextes($this);
             */
        }

        return $this->aCommonConsultation;
    }

    /**
     * Declares an association between this object and a CommonOrganisme object.
     *
     * @param   CommonOrganisme $v
     * @return CommonTDumeContexte The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonOrganisme(CommonOrganisme $v = null)
    {
        if ($v === null) {
            $this->setOrganisme(NULL);
        } else {
            $this->setOrganisme($v->getAcronyme());
        }

        $this->aCommonOrganisme = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonOrganisme object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTDumeContexte($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonOrganisme object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonOrganisme The associated CommonOrganisme object.
     * @throws PropelException
     */
    public function getCommonOrganisme(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonOrganisme === null && (($this->organisme !== "" && $this->organisme !== null)) && $doQuery) {
            $this->aCommonOrganisme = CommonOrganismeQuery::create()
                ->filterByCommonTDumeContexte($this) // here
                ->findOne($con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonOrganisme->addCommonTDumeContextes($this);
             */
        }

        return $this->aCommonOrganisme;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTCandidature' == $relationName) {
            $this->initCommonTCandidatures();
        }
        if ('CommonTDumeNumero' == $relationName) {
            $this->initCommonTDumeNumeros();
        }
    }

    /**
     * Clears out the collCommonTCandidatures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTDumeContexte The current object (for fluent API support)
     * @see        addCommonTCandidatures()
     */
    public function clearCommonTCandidatures()
    {
        $this->collCommonTCandidatures = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCandidaturesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCandidatures collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCandidatures($v = true)
    {
        $this->collCommonTCandidaturesPartial = $v;
    }

    /**
     * Initializes the collCommonTCandidatures collection.
     *
     * By default this just sets the collCommonTCandidatures collection to an empty array (like clearcollCommonTCandidatures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCandidatures($overrideExisting = true)
    {
        if (null !== $this->collCommonTCandidatures && !$overrideExisting) {
            return;
        }
        $this->collCommonTCandidatures = new PropelObjectCollection();
        $this->collCommonTCandidatures->setModel('CommonTCandidature');
    }

    /**
     * Gets an array of CommonTCandidature objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTDumeContexte is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     * @throws PropelException
     */
    public function getCommonTCandidatures($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatures || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatures) {
                // return empty collection
                $this->initCommonTCandidatures();
            } else {
                $collCommonTCandidatures = CommonTCandidatureQuery::create(null, $criteria)
                    ->filterByCommonTDumeContexte($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCandidaturesPartial && count($collCommonTCandidatures)) {
                      $this->initCommonTCandidatures(false);

                      foreach ($collCommonTCandidatures as $obj) {
                        if (false == $this->collCommonTCandidatures->contains($obj)) {
                          $this->collCommonTCandidatures->append($obj);
                        }
                      }

                      $this->collCommonTCandidaturesPartial = true;
                    }

                    $collCommonTCandidatures->getInternalIterator()->rewind();

                    return $collCommonTCandidatures;
                }

                if ($partial && $this->collCommonTCandidatures) {
                    foreach ($this->collCommonTCandidatures as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCandidatures[] = $obj;
                        }
                    }
                }

                $this->collCommonTCandidatures = $collCommonTCandidatures;
                $this->collCommonTCandidaturesPartial = false;
            }
        }

        return $this->collCommonTCandidatures;
    }

    /**
     * Sets a collection of CommonTCandidature objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCandidatures A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setCommonTCandidatures(PropelCollection $commonTCandidatures, PropelPDO $con = null)
    {
        $commonTCandidaturesToDelete = $this->getCommonTCandidatures(new Criteria(), $con)->diff($commonTCandidatures);


        $this->commonTCandidaturesScheduledForDeletion = $commonTCandidaturesToDelete;

        foreach ($commonTCandidaturesToDelete as $commonTCandidatureRemoved) {
            $commonTCandidatureRemoved->setCommonTDumeContexte(null);
        }

        $this->collCommonTCandidatures = null;
        foreach ($commonTCandidatures as $commonTCandidature) {
            $this->addCommonTCandidature($commonTCandidature);
        }

        $this->collCommonTCandidatures = $commonTCandidatures;
        $this->collCommonTCandidaturesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCandidature objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCandidature objects.
     * @throws PropelException
     */
    public function countCommonTCandidatures(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCandidatures());
            }
            $query = CommonTCandidatureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTDumeContexte($this)
                ->count($con);
        }

        return count($this->collCommonTCandidatures);
    }

    /**
     * Method called to associate a CommonTCandidature object to this object
     * through the CommonTCandidature foreign key attribute.
     *
     * @param   CommonTCandidature $l CommonTCandidature
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function addCommonTCandidature(CommonTCandidature $l)
    {
        if ($this->collCommonTCandidatures === null) {
            $this->initCommonTCandidatures();
            $this->collCommonTCandidaturesPartial = true;
        }
        if (!in_array($l, $this->collCommonTCandidatures->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCandidature($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCandidature $commonTCandidature The commonTCandidature object to add.
     */
    protected function doAddCommonTCandidature($commonTCandidature)
    {
        $this->collCommonTCandidatures[]= $commonTCandidature;
        $commonTCandidature->setCommonTDumeContexte($this);
    }

    /**
     * @param	CommonTCandidature $commonTCandidature The commonTCandidature object to remove.
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function removeCommonTCandidature($commonTCandidature)
    {
        if ($this->getCommonTCandidatures()->contains($commonTCandidature)) {
            $this->collCommonTCandidatures->remove($this->collCommonTCandidatures->search($commonTCandidature));
            if (null === $this->commonTCandidaturesScheduledForDeletion) {
                $this->commonTCandidaturesScheduledForDeletion = clone $this->collCommonTCandidatures;
                $this->commonTCandidaturesScheduledForDeletion->clear();
            }
            $this->commonTCandidaturesScheduledForDeletion[]= $commonTCandidature;
            $commonTCandidature->setCommonTDumeContexte(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTDumeContexte is new, it will return
     * an empty collection; or if this CommonTDumeContexte has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTDumeContexte.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTDumeContexte is new, it will return
     * an empty collection; or if this CommonTDumeContexte has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTDumeContexte.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTDumeContexte is new, it will return
     * an empty collection; or if this CommonTDumeContexte has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTDumeContexte.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonOffres($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonOffres', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTDumeContexte is new, it will return
     * an empty collection; or if this CommonTDumeContexte has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTDumeContexte.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }

    /**
     * Clears out the collCommonTDumeNumeros collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTDumeContexte The current object (for fluent API support)
     * @see        addCommonTDumeNumeros()
     */
    public function clearCommonTDumeNumeros()
    {
        $this->collCommonTDumeNumeros = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTDumeNumerosPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTDumeNumeros collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTDumeNumeros($v = true)
    {
        $this->collCommonTDumeNumerosPartial = $v;
    }

    /**
     * Initializes the collCommonTDumeNumeros collection.
     *
     * By default this just sets the collCommonTDumeNumeros collection to an empty array (like clearcollCommonTDumeNumeros());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTDumeNumeros($overrideExisting = true)
    {
        if (null !== $this->collCommonTDumeNumeros && !$overrideExisting) {
            return;
        }
        $this->collCommonTDumeNumeros = new PropelObjectCollection();
        $this->collCommonTDumeNumeros->setModel('CommonTDumeNumero');
    }

    /**
     * Gets an array of CommonTDumeNumero objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTDumeContexte is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTDumeNumero[] List of CommonTDumeNumero objects
     * @throws PropelException
     */
    public function getCommonTDumeNumeros($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDumeNumerosPartial && !$this->isNew();
        if (null === $this->collCommonTDumeNumeros || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTDumeNumeros) {
                // return empty collection
                $this->initCommonTDumeNumeros();
            } else {
                $collCommonTDumeNumeros = CommonTDumeNumeroQuery::create(null, $criteria)
                    ->filterByCommonTDumeContexte($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTDumeNumerosPartial && count($collCommonTDumeNumeros)) {
                      $this->initCommonTDumeNumeros(false);

                      foreach ($collCommonTDumeNumeros as $obj) {
                        if (false == $this->collCommonTDumeNumeros->contains($obj)) {
                          $this->collCommonTDumeNumeros->append($obj);
                        }
                      }

                      $this->collCommonTDumeNumerosPartial = true;
                    }

                    $collCommonTDumeNumeros->getInternalIterator()->rewind();

                    return $collCommonTDumeNumeros;
                }

                if ($partial && $this->collCommonTDumeNumeros) {
                    foreach ($this->collCommonTDumeNumeros as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTDumeNumeros[] = $obj;
                        }
                    }
                }

                $this->collCommonTDumeNumeros = $collCommonTDumeNumeros;
                $this->collCommonTDumeNumerosPartial = false;
            }
        }

        return $this->collCommonTDumeNumeros;
    }

    /**
     * Sets a collection of CommonTDumeNumero objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTDumeNumeros A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function setCommonTDumeNumeros(PropelCollection $commonTDumeNumeros, PropelPDO $con = null)
    {
        $commonTDumeNumerosToDelete = $this->getCommonTDumeNumeros(new Criteria(), $con)->diff($commonTDumeNumeros);


        $this->commonTDumeNumerosScheduledForDeletion = $commonTDumeNumerosToDelete;

        foreach ($commonTDumeNumerosToDelete as $commonTDumeNumeroRemoved) {
            $commonTDumeNumeroRemoved->setCommonTDumeContexte(null);
        }

        $this->collCommonTDumeNumeros = null;
        foreach ($commonTDumeNumeros as $commonTDumeNumero) {
            $this->addCommonTDumeNumero($commonTDumeNumero);
        }

        $this->collCommonTDumeNumeros = $commonTDumeNumeros;
        $this->collCommonTDumeNumerosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTDumeNumero objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTDumeNumero objects.
     * @throws PropelException
     */
    public function countCommonTDumeNumeros(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDumeNumerosPartial && !$this->isNew();
        if (null === $this->collCommonTDumeNumeros || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTDumeNumeros) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTDumeNumeros());
            }
            $query = CommonTDumeNumeroQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTDumeContexte($this)
                ->count($con);
        }

        return count($this->collCommonTDumeNumeros);
    }

    /**
     * Method called to associate a CommonTDumeNumero object to this object
     * through the CommonTDumeNumero foreign key attribute.
     *
     * @param   CommonTDumeNumero $l CommonTDumeNumero
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function addCommonTDumeNumero(CommonTDumeNumero $l)
    {
        if ($this->collCommonTDumeNumeros === null) {
            $this->initCommonTDumeNumeros();
            $this->collCommonTDumeNumerosPartial = true;
        }
        if (!in_array($l, $this->collCommonTDumeNumeros->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTDumeNumero($l);
        }

        return $this;
    }

    /**
     * @param	CommonTDumeNumero $commonTDumeNumero The commonTDumeNumero object to add.
     */
    protected function doAddCommonTDumeNumero($commonTDumeNumero)
    {
        $this->collCommonTDumeNumeros[]= $commonTDumeNumero;
        $commonTDumeNumero->setCommonTDumeContexte($this);
    }

    /**
     * @param	CommonTDumeNumero $commonTDumeNumero The commonTDumeNumero object to remove.
     * @return CommonTDumeContexte The current object (for fluent API support)
     */
    public function removeCommonTDumeNumero($commonTDumeNumero)
    {
        if ($this->getCommonTDumeNumeros()->contains($commonTDumeNumero)) {
            $this->collCommonTDumeNumeros->remove($this->collCommonTDumeNumeros->search($commonTDumeNumero));
            if (null === $this->commonTDumeNumerosScheduledForDeletion) {
                $this->commonTDumeNumerosScheduledForDeletion = clone $this->collCommonTDumeNumeros;
                $this->commonTDumeNumerosScheduledForDeletion->clear();
            }
            $this->commonTDumeNumerosScheduledForDeletion[]= $commonTDumeNumero;
            $commonTDumeNumero->setCommonTDumeContexte(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->ref_consultation = null;
        $this->organisme = null;
        $this->contexte_lt_dume_id = null;
        $this->type_dume = null;
        $this->status = null;
        $this->is_standard = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->consultation_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTCandidatures) {
                foreach ($this->collCommonTCandidatures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTDumeNumeros) {
                foreach ($this->collCommonTDumeNumeros as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonConsultation instanceof Persistent) {
              $this->aCommonConsultation->clearAllReferences($deep);
            }
            if ($this->aCommonOrganisme instanceof Persistent) {
              $this->aCommonOrganisme->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTCandidatures instanceof PropelCollection) {
            $this->collCommonTCandidatures->clearIterator();
        }
        $this->collCommonTCandidatures = null;
        if ($this->collCommonTDumeNumeros instanceof PropelCollection) {
            $this->collCommonTDumeNumeros->clearIterator();
        }
        $this->collCommonTDumeNumeros = null;
        $this->aCommonConsultation = null;
        $this->aCommonOrganisme = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTDumeContextePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
