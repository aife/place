<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTBourseCotraitance;
use Application\Propel\Mpe\CommonTBourseCotraitancePeer;
use Application\Propel\Mpe\Map\CommonTBourseCotraitanceTableMap;

/**
 * Base static class for performing query and update operations on the 't_bourse_cotraitance' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTBourseCotraitancePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_bourse_cotraitance';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTBourseCotraitance';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTBourseCotraitanceTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 26;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 26;

    /** the column name for the id_auto_BC field */
    const ID_AUTO_BC = 't_bourse_cotraitance.id_auto_BC';

    /** the column name for the reference_consultation field */
    const REFERENCE_CONSULTATION = 't_bourse_cotraitance.reference_consultation';

    /** the column name for the id_entreprise field */
    const ID_ENTREPRISE = 't_bourse_cotraitance.id_entreprise';

    /** the column name for the id_etablissement_inscrite field */
    const ID_ETABLISSEMENT_INSCRITE = 't_bourse_cotraitance.id_etablissement_inscrite';

    /** the column name for the nom_inscrit field */
    const NOM_INSCRIT = 't_bourse_cotraitance.nom_inscrit';

    /** the column name for the prenom_inscrit field */
    const PRENOM_INSCRIT = 't_bourse_cotraitance.prenom_inscrit';

    /** the column name for the adresse_inscrit field */
    const ADRESSE_INSCRIT = 't_bourse_cotraitance.adresse_inscrit';

    /** the column name for the adresse2_incsrit field */
    const ADRESSE2_INCSRIT = 't_bourse_cotraitance.adresse2_incsrit';

    /** the column name for the cp_inscrit field */
    const CP_INSCRIT = 't_bourse_cotraitance.cp_inscrit';

    /** the column name for the ville_inscrit field */
    const VILLE_INSCRIT = 't_bourse_cotraitance.ville_inscrit';

    /** the column name for the pays_inscrit field */
    const PAYS_INSCRIT = 't_bourse_cotraitance.pays_inscrit';

    /** the column name for the fonction_inscrit field */
    const FONCTION_INSCRIT = 't_bourse_cotraitance.fonction_inscrit';

    /** the column name for the email_inscrit field */
    const EMAIL_INSCRIT = 't_bourse_cotraitance.email_inscrit';

    /** the column name for the tel_fixe_inscrit field */
    const TEL_FIXE_INSCRIT = 't_bourse_cotraitance.tel_fixe_inscrit';

    /** the column name for the tel_mobile_inscrit field */
    const TEL_MOBILE_INSCRIT = 't_bourse_cotraitance.tel_mobile_inscrit';

    /** the column name for the mandataire_groupement field */
    const MANDATAIRE_GROUPEMENT = 't_bourse_cotraitance.mandataire_groupement';

    /** the column name for the cotraitant_solidaire field */
    const COTRAITANT_SOLIDAIRE = 't_bourse_cotraitance.cotraitant_solidaire';

    /** the column name for the cotraitant_conjoint field */
    const COTRAITANT_CONJOINT = 't_bourse_cotraitance.cotraitant_conjoint';

    /** the column name for the desc_mon_apport_marche field */
    const DESC_MON_APPORT_MARCHE = 't_bourse_cotraitance.desc_mon_apport_marche';

    /** the column name for the desc_type_cotraitance_recherche field */
    const DESC_TYPE_COTRAITANCE_RECHERCHE = 't_bourse_cotraitance.desc_type_cotraitance_recherche';

    /** the column name for the clause_social field */
    const CLAUSE_SOCIAL = 't_bourse_cotraitance.clause_social';

    /** the column name for the entreprise_adapte field */
    const ENTREPRISE_ADAPTE = 't_bourse_cotraitance.entreprise_adapte';

    /** the column name for the long field */
    const LONG = 't_bourse_cotraitance.long';

    /** the column name for the lat field */
    const LAT = 't_bourse_cotraitance.lat';

    /** the column name for the maj_long_lat field */
    const MAJ_LONG_LAT = 't_bourse_cotraitance.maj_long_lat';

    /** the column name for the sous_traitant field */
    const SOUS_TRAITANT = 't_bourse_cotraitance.sous_traitant';

    /** The enumerated values for the mandataire_groupement field */
    const MANDATAIRE_GROUPEMENT_0 = '0';
    const MANDATAIRE_GROUPEMENT_1 = '1';

    /** The enumerated values for the cotraitant_solidaire field */
    const COTRAITANT_SOLIDAIRE_0 = '0';
    const COTRAITANT_SOLIDAIRE_1 = '1';

    /** The enumerated values for the cotraitant_conjoint field */
    const COTRAITANT_CONJOINT_0 = '0';
    const COTRAITANT_CONJOINT_1 = '1';

    /** The enumerated values for the clause_social field */
    const CLAUSE_SOCIAL_0 = '0';
    const CLAUSE_SOCIAL_1 = '1';

    /** The enumerated values for the entreprise_adapte field */
    const ENTREPRISE_ADAPTE_0 = '0';
    const ENTREPRISE_ADAPTE_1 = '1';

    /** The enumerated values for the sous_traitant field */
    const SOUS_TRAITANT_0 = '0';
    const SOUS_TRAITANT_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTBourseCotraitance objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTBourseCotraitance[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTBourseCotraitancePeer::$fieldNames[CommonTBourseCotraitancePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdAutoBc', 'ReferenceConsultation', 'IdEntreprise', 'IdEtablissementInscrite', 'NomInscrit', 'PrenomInscrit', 'AdresseInscrit', 'Adresse2Incsrit', 'CpInscrit', 'VilleInscrit', 'PaysInscrit', 'FonctionInscrit', 'EmailInscrit', 'TelFixeInscrit', 'TelMobileInscrit', 'MandataireGroupement', 'CotraitantSolidaire', 'CotraitantConjoint', 'DescMonApportMarche', 'DescTypeCotraitanceRecherche', 'ClauseSocial', 'EntrepriseAdapte', 'Long', 'Lat', 'MajLongLat', 'SousTraitant', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idAutoBc', 'referenceConsultation', 'idEntreprise', 'idEtablissementInscrite', 'nomInscrit', 'prenomInscrit', 'adresseInscrit', 'adresse2Incsrit', 'cpInscrit', 'villeInscrit', 'paysInscrit', 'fonctionInscrit', 'emailInscrit', 'telFixeInscrit', 'telMobileInscrit', 'mandataireGroupement', 'cotraitantSolidaire', 'cotraitantConjoint', 'descMonApportMarche', 'descTypeCotraitanceRecherche', 'clauseSocial', 'entrepriseAdapte', 'long', 'lat', 'majLongLat', 'sousTraitant', ),
        BasePeer::TYPE_COLNAME => array (CommonTBourseCotraitancePeer::ID_AUTO_BC, CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION, CommonTBourseCotraitancePeer::ID_ENTREPRISE, CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE, CommonTBourseCotraitancePeer::NOM_INSCRIT, CommonTBourseCotraitancePeer::PRENOM_INSCRIT, CommonTBourseCotraitancePeer::ADRESSE_INSCRIT, CommonTBourseCotraitancePeer::ADRESSE2_INCSRIT, CommonTBourseCotraitancePeer::CP_INSCRIT, CommonTBourseCotraitancePeer::VILLE_INSCRIT, CommonTBourseCotraitancePeer::PAYS_INSCRIT, CommonTBourseCotraitancePeer::FONCTION_INSCRIT, CommonTBourseCotraitancePeer::EMAIL_INSCRIT, CommonTBourseCotraitancePeer::TEL_FIXE_INSCRIT, CommonTBourseCotraitancePeer::TEL_MOBILE_INSCRIT, CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT, CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE, CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT, CommonTBourseCotraitancePeer::DESC_MON_APPORT_MARCHE, CommonTBourseCotraitancePeer::DESC_TYPE_COTRAITANCE_RECHERCHE, CommonTBourseCotraitancePeer::CLAUSE_SOCIAL, CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE, CommonTBourseCotraitancePeer::LONG, CommonTBourseCotraitancePeer::LAT, CommonTBourseCotraitancePeer::MAJ_LONG_LAT, CommonTBourseCotraitancePeer::SOUS_TRAITANT, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_AUTO_BC', 'REFERENCE_CONSULTATION', 'ID_ENTREPRISE', 'ID_ETABLISSEMENT_INSCRITE', 'NOM_INSCRIT', 'PRENOM_INSCRIT', 'ADRESSE_INSCRIT', 'ADRESSE2_INCSRIT', 'CP_INSCRIT', 'VILLE_INSCRIT', 'PAYS_INSCRIT', 'FONCTION_INSCRIT', 'EMAIL_INSCRIT', 'TEL_FIXE_INSCRIT', 'TEL_MOBILE_INSCRIT', 'MANDATAIRE_GROUPEMENT', 'COTRAITANT_SOLIDAIRE', 'COTRAITANT_CONJOINT', 'DESC_MON_APPORT_MARCHE', 'DESC_TYPE_COTRAITANCE_RECHERCHE', 'CLAUSE_SOCIAL', 'ENTREPRISE_ADAPTE', 'LONG', 'LAT', 'MAJ_LONG_LAT', 'SOUS_TRAITANT', ),
        BasePeer::TYPE_FIELDNAME => array ('id_auto_BC', 'reference_consultation', 'id_entreprise', 'id_etablissement_inscrite', 'nom_inscrit', 'prenom_inscrit', 'adresse_inscrit', 'adresse2_incsrit', 'cp_inscrit', 'ville_inscrit', 'pays_inscrit', 'fonction_inscrit', 'email_inscrit', 'tel_fixe_inscrit', 'tel_mobile_inscrit', 'mandataire_groupement', 'cotraitant_solidaire', 'cotraitant_conjoint', 'desc_mon_apport_marche', 'desc_type_cotraitance_recherche', 'clause_social', 'entreprise_adapte', 'long', 'lat', 'maj_long_lat', 'sous_traitant', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTBourseCotraitancePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdAutoBc' => 0, 'ReferenceConsultation' => 1, 'IdEntreprise' => 2, 'IdEtablissementInscrite' => 3, 'NomInscrit' => 4, 'PrenomInscrit' => 5, 'AdresseInscrit' => 6, 'Adresse2Incsrit' => 7, 'CpInscrit' => 8, 'VilleInscrit' => 9, 'PaysInscrit' => 10, 'FonctionInscrit' => 11, 'EmailInscrit' => 12, 'TelFixeInscrit' => 13, 'TelMobileInscrit' => 14, 'MandataireGroupement' => 15, 'CotraitantSolidaire' => 16, 'CotraitantConjoint' => 17, 'DescMonApportMarche' => 18, 'DescTypeCotraitanceRecherche' => 19, 'ClauseSocial' => 20, 'EntrepriseAdapte' => 21, 'Long' => 22, 'Lat' => 23, 'MajLongLat' => 24, 'SousTraitant' => 25, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idAutoBc' => 0, 'referenceConsultation' => 1, 'idEntreprise' => 2, 'idEtablissementInscrite' => 3, 'nomInscrit' => 4, 'prenomInscrit' => 5, 'adresseInscrit' => 6, 'adresse2Incsrit' => 7, 'cpInscrit' => 8, 'villeInscrit' => 9, 'paysInscrit' => 10, 'fonctionInscrit' => 11, 'emailInscrit' => 12, 'telFixeInscrit' => 13, 'telMobileInscrit' => 14, 'mandataireGroupement' => 15, 'cotraitantSolidaire' => 16, 'cotraitantConjoint' => 17, 'descMonApportMarche' => 18, 'descTypeCotraitanceRecherche' => 19, 'clauseSocial' => 20, 'entrepriseAdapte' => 21, 'long' => 22, 'lat' => 23, 'majLongLat' => 24, 'sousTraitant' => 25, ),
        BasePeer::TYPE_COLNAME => array (CommonTBourseCotraitancePeer::ID_AUTO_BC => 0, CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION => 1, CommonTBourseCotraitancePeer::ID_ENTREPRISE => 2, CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE => 3, CommonTBourseCotraitancePeer::NOM_INSCRIT => 4, CommonTBourseCotraitancePeer::PRENOM_INSCRIT => 5, CommonTBourseCotraitancePeer::ADRESSE_INSCRIT => 6, CommonTBourseCotraitancePeer::ADRESSE2_INCSRIT => 7, CommonTBourseCotraitancePeer::CP_INSCRIT => 8, CommonTBourseCotraitancePeer::VILLE_INSCRIT => 9, CommonTBourseCotraitancePeer::PAYS_INSCRIT => 10, CommonTBourseCotraitancePeer::FONCTION_INSCRIT => 11, CommonTBourseCotraitancePeer::EMAIL_INSCRIT => 12, CommonTBourseCotraitancePeer::TEL_FIXE_INSCRIT => 13, CommonTBourseCotraitancePeer::TEL_MOBILE_INSCRIT => 14, CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT => 15, CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE => 16, CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT => 17, CommonTBourseCotraitancePeer::DESC_MON_APPORT_MARCHE => 18, CommonTBourseCotraitancePeer::DESC_TYPE_COTRAITANCE_RECHERCHE => 19, CommonTBourseCotraitancePeer::CLAUSE_SOCIAL => 20, CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE => 21, CommonTBourseCotraitancePeer::LONG => 22, CommonTBourseCotraitancePeer::LAT => 23, CommonTBourseCotraitancePeer::MAJ_LONG_LAT => 24, CommonTBourseCotraitancePeer::SOUS_TRAITANT => 25, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_AUTO_BC' => 0, 'REFERENCE_CONSULTATION' => 1, 'ID_ENTREPRISE' => 2, 'ID_ETABLISSEMENT_INSCRITE' => 3, 'NOM_INSCRIT' => 4, 'PRENOM_INSCRIT' => 5, 'ADRESSE_INSCRIT' => 6, 'ADRESSE2_INCSRIT' => 7, 'CP_INSCRIT' => 8, 'VILLE_INSCRIT' => 9, 'PAYS_INSCRIT' => 10, 'FONCTION_INSCRIT' => 11, 'EMAIL_INSCRIT' => 12, 'TEL_FIXE_INSCRIT' => 13, 'TEL_MOBILE_INSCRIT' => 14, 'MANDATAIRE_GROUPEMENT' => 15, 'COTRAITANT_SOLIDAIRE' => 16, 'COTRAITANT_CONJOINT' => 17, 'DESC_MON_APPORT_MARCHE' => 18, 'DESC_TYPE_COTRAITANCE_RECHERCHE' => 19, 'CLAUSE_SOCIAL' => 20, 'ENTREPRISE_ADAPTE' => 21, 'LONG' => 22, 'LAT' => 23, 'MAJ_LONG_LAT' => 24, 'SOUS_TRAITANT' => 25, ),
        BasePeer::TYPE_FIELDNAME => array ('id_auto_BC' => 0, 'reference_consultation' => 1, 'id_entreprise' => 2, 'id_etablissement_inscrite' => 3, 'nom_inscrit' => 4, 'prenom_inscrit' => 5, 'adresse_inscrit' => 6, 'adresse2_incsrit' => 7, 'cp_inscrit' => 8, 'ville_inscrit' => 9, 'pays_inscrit' => 10, 'fonction_inscrit' => 11, 'email_inscrit' => 12, 'tel_fixe_inscrit' => 13, 'tel_mobile_inscrit' => 14, 'mandataire_groupement' => 15, 'cotraitant_solidaire' => 16, 'cotraitant_conjoint' => 17, 'desc_mon_apport_marche' => 18, 'desc_type_cotraitance_recherche' => 19, 'clause_social' => 20, 'entreprise_adapte' => 21, 'long' => 22, 'lat' => 23, 'maj_long_lat' => 24, 'sous_traitant' => 25, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT => array(
            CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT_0,
            CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT_1,
        ),
        CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE => array(
            CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE_0,
            CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE_1,
        ),
        CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT => array(
            CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT_0,
            CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT_1,
        ),
        CommonTBourseCotraitancePeer::CLAUSE_SOCIAL => array(
            CommonTBourseCotraitancePeer::CLAUSE_SOCIAL_0,
            CommonTBourseCotraitancePeer::CLAUSE_SOCIAL_1,
        ),
        CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE => array(
            CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE_0,
            CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE_1,
        ),
        CommonTBourseCotraitancePeer::SOUS_TRAITANT => array(
            CommonTBourseCotraitancePeer::SOUS_TRAITANT_0,
            CommonTBourseCotraitancePeer::SOUS_TRAITANT_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTBourseCotraitancePeer::getFieldNames($toType);
        $key = isset(CommonTBourseCotraitancePeer::$fieldKeys[$fromType][$name]) ? CommonTBourseCotraitancePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTBourseCotraitancePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTBourseCotraitancePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTBourseCotraitancePeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTBourseCotraitancePeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTBourseCotraitancePeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTBourseCotraitancePeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTBourseCotraitancePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTBourseCotraitancePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::ID_AUTO_BC);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::ID_ENTREPRISE);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::NOM_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::PRENOM_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::ADRESSE_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::ADRESSE2_INCSRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::CP_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::VILLE_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::PAYS_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::FONCTION_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::EMAIL_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::TEL_FIXE_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::TEL_MOBILE_INSCRIT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::DESC_MON_APPORT_MARCHE);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::DESC_TYPE_COTRAITANCE_RECHERCHE);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::CLAUSE_SOCIAL);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::LONG);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::LAT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::MAJ_LONG_LAT);
            $criteria->addSelectColumn(CommonTBourseCotraitancePeer::SOUS_TRAITANT);
        } else {
            $criteria->addSelectColumn($alias . '.id_auto_BC');
            $criteria->addSelectColumn($alias . '.reference_consultation');
            $criteria->addSelectColumn($alias . '.id_entreprise');
            $criteria->addSelectColumn($alias . '.id_etablissement_inscrite');
            $criteria->addSelectColumn($alias . '.nom_inscrit');
            $criteria->addSelectColumn($alias . '.prenom_inscrit');
            $criteria->addSelectColumn($alias . '.adresse_inscrit');
            $criteria->addSelectColumn($alias . '.adresse2_incsrit');
            $criteria->addSelectColumn($alias . '.cp_inscrit');
            $criteria->addSelectColumn($alias . '.ville_inscrit');
            $criteria->addSelectColumn($alias . '.pays_inscrit');
            $criteria->addSelectColumn($alias . '.fonction_inscrit');
            $criteria->addSelectColumn($alias . '.email_inscrit');
            $criteria->addSelectColumn($alias . '.tel_fixe_inscrit');
            $criteria->addSelectColumn($alias . '.tel_mobile_inscrit');
            $criteria->addSelectColumn($alias . '.mandataire_groupement');
            $criteria->addSelectColumn($alias . '.cotraitant_solidaire');
            $criteria->addSelectColumn($alias . '.cotraitant_conjoint');
            $criteria->addSelectColumn($alias . '.desc_mon_apport_marche');
            $criteria->addSelectColumn($alias . '.desc_type_cotraitance_recherche');
            $criteria->addSelectColumn($alias . '.clause_social');
            $criteria->addSelectColumn($alias . '.entreprise_adapte');
            $criteria->addSelectColumn($alias . '.long');
            $criteria->addSelectColumn($alias . '.lat');
            $criteria->addSelectColumn($alias . '.maj_long_lat');
            $criteria->addSelectColumn($alias . '.sous_traitant');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTBourseCotraitancePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTBourseCotraitancePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTBourseCotraitancePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTBourseCotraitance
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTBourseCotraitancePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTBourseCotraitancePeer::populateObjects(CommonTBourseCotraitancePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTBourseCotraitancePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTBourseCotraitancePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTBourseCotraitance $obj A CommonTBourseCotraitance object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getIdAutoBc();
            } // if key === null
            CommonTBourseCotraitancePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTBourseCotraitance object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTBourseCotraitance) {
                $key = (string) $value->getIdAutoBc();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTBourseCotraitance object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTBourseCotraitancePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTBourseCotraitance Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTBourseCotraitancePeer::$instances[$key])) {
                return CommonTBourseCotraitancePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTBourseCotraitancePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTBourseCotraitancePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_bourse_cotraitance
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTBourseCotraitancePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTBourseCotraitancePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTBourseCotraitancePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTBourseCotraitancePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTBourseCotraitance object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTBourseCotraitancePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTBourseCotraitancePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTBourseCotraitancePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTBourseCotraitancePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTBourseCotraitancePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTBourseCotraitancePeer::DATABASE_NAME)->getTable(CommonTBourseCotraitancePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTBourseCotraitancePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTBourseCotraitancePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTBourseCotraitanceTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTBourseCotraitancePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTBourseCotraitance or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTBourseCotraitance object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTBourseCotraitance object
        }

        if ($criteria->containsKey(CommonTBourseCotraitancePeer::ID_AUTO_BC) && $criteria->keyContainsValue(CommonTBourseCotraitancePeer::ID_AUTO_BC) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTBourseCotraitancePeer::ID_AUTO_BC.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTBourseCotraitancePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTBourseCotraitance or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTBourseCotraitance object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTBourseCotraitancePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTBourseCotraitancePeer::ID_AUTO_BC);
            $value = $criteria->remove(CommonTBourseCotraitancePeer::ID_AUTO_BC);
            if ($value) {
                $selectCriteria->add(CommonTBourseCotraitancePeer::ID_AUTO_BC, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTBourseCotraitancePeer::TABLE_NAME);
            }

        } else { // $values is CommonTBourseCotraitance object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTBourseCotraitancePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_bourse_cotraitance table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTBourseCotraitancePeer::TABLE_NAME, $con, CommonTBourseCotraitancePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTBourseCotraitancePeer::clearInstancePool();
            CommonTBourseCotraitancePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTBourseCotraitance or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTBourseCotraitance object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTBourseCotraitancePeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTBourseCotraitance) { // it's a model object
            // invalidate the cache for this single object
            CommonTBourseCotraitancePeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTBourseCotraitancePeer::DATABASE_NAME);
            $criteria->add(CommonTBourseCotraitancePeer::ID_AUTO_BC, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTBourseCotraitancePeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTBourseCotraitancePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTBourseCotraitancePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTBourseCotraitance object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTBourseCotraitance $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTBourseCotraitancePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTBourseCotraitancePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTBourseCotraitancePeer::DATABASE_NAME, CommonTBourseCotraitancePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTBourseCotraitance
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTBourseCotraitancePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTBourseCotraitancePeer::DATABASE_NAME);
        $criteria->add(CommonTBourseCotraitancePeer::ID_AUTO_BC, $pk);

        $v = CommonTBourseCotraitancePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTBourseCotraitance[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTBourseCotraitancePeer::DATABASE_NAME);
            $criteria->add(CommonTBourseCotraitancePeer::ID_AUTO_BC, $pks, Criteria::IN);
            $objs = CommonTBourseCotraitancePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTBourseCotraitancePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTBourseCotraitancePeer::buildTableMap();

