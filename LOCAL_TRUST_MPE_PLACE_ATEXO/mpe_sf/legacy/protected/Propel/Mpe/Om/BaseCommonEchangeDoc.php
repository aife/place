<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocApplicationClient;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientQuery;
use Application\Propel\Mpe\CommonEchangeDocBlob;
use Application\Propel\Mpe\CommonEchangeDocBlobQuery;
use Application\Propel\Mpe\CommonEchangeDocHistorique;
use Application\Propel\Mpe\CommonEchangeDocHistoriqueQuery;
use Application\Propel\Mpe\CommonEchangeDocPeer;
use Application\Propel\Mpe\CommonEchangeDocQuery;
use Application\Propel\Mpe\CommonReferentielSousTypeParapheur;
use Application\Propel\Mpe\CommonReferentielSousTypeParapheurQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;

/**
 * Base class that represents a row from the 'echange_doc' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDoc extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonEchangeDocPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonEchangeDocPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the echange_doc_application_client_id field.
     * @var        int
     */
    protected $echange_doc_application_client_id;

    /**
     * The value for the objet field.
     * @var        string
     */
    protected $objet;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the consultation_id field.
     * @var        int
     */
    protected $consultation_id;

    /**
     * The value for the agent_id field.
     * @var        int
     */
    protected $agent_id;

    /**
     * The value for the statut field.
     * @var        string
     */
    protected $statut;

    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     * @var        string
     */
    protected $updated_at;

    /**
     * The value for the cheminement_signature field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $cheminement_signature;

    /**
     * The value for the cheminement_ged field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $cheminement_ged;

    /**
     * The value for the cheminement_sae field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $cheminement_sae;

    /**
     * The value for the cheminement_tdt field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $cheminement_tdt;

    /**
     * The value for the id_contrat_titulaire field.
     * @var        int
     */
    protected $id_contrat_titulaire;

    /**
     * The value for the referentiel_sous_type_parapheur_id field.
     * @var        int
     */
    protected $referentiel_sous_type_parapheur_id;

    /**
     * @var        CommonReferentielSousTypeParapheur
     */
    protected $aCommonReferentielSousTypeParapheur;

    /**
     * @var        CommonTContratTitulaire
     */
    protected $aCommonTContratTitulaire;

    /**
     * @var        CommonAgent
     */
    protected $aCommonAgent;

    /**
     * @var        CommonEchangeDocApplicationClient
     */
    protected $aCommonEchangeDocApplicationClient;

    /**
     * @var        CommonConsultation
     */
    protected $aCommonConsultation;

    /**
     * @var        PropelObjectCollection|CommonEchangeDocBlob[] Collection to store aggregation of CommonEchangeDocBlob objects.
     */
    protected $collCommonEchangeDocBlobs;
    protected $collCommonEchangeDocBlobsPartial;

    /**
     * @var        PropelObjectCollection|CommonEchangeDocHistorique[] Collection to store aggregation of CommonEchangeDocHistorique objects.
     */
    protected $collCommonEchangeDocHistoriques;
    protected $collCommonEchangeDocHistoriquesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocBlobsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocHistoriquesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->cheminement_signature = false;
        $this->cheminement_ged = false;
        $this->cheminement_sae = false;
        $this->cheminement_tdt = false;
    }

    /**
     * Initializes internal state of BaseCommonEchangeDoc object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [echange_doc_application_client_id] column value.
     *
     * @return int
     */
    public function getEchangeDocApplicationClientId()
    {

        return $this->echange_doc_application_client_id;
    }

    /**
     * Get the [objet] column value.
     *
     * @return string
     */
    public function getObjet()
    {

        return $this->objet;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [consultation_id] column value.
     *
     * @return int
     */
    public function getConsultationId()
    {

        return $this->consultation_id;
    }

    /**
     * Get the [agent_id] column value.
     *
     * @return int
     */
    public function getAgentId()
    {

        return $this->agent_id;
    }

    /**
     * Get the [statut] column value.
     *
     * @return string
     */
    public function getStatut()
    {

        return $this->statut;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = 'Y-m-d H:i:s')
    {
        if ($this->created_at === null) {
            return null;
        }

        if ($this->created_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = 'Y-m-d H:i:s')
    {
        if ($this->updated_at === null) {
            return null;
        }

        if ($this->updated_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->updated_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [cheminement_signature] column value.
     *
     * @return boolean
     */
    public function getCheminementSignature()
    {

        return $this->cheminement_signature;
    }

    /**
     * Get the [cheminement_ged] column value.
     *
     * @return boolean
     */
    public function getCheminementGed()
    {

        return $this->cheminement_ged;
    }

    /**
     * Get the [cheminement_sae] column value.
     *
     * @return boolean
     */
    public function getCheminementSae()
    {

        return $this->cheminement_sae;
    }

    /**
     * Get the [cheminement_tdt] column value.
     *
     * @return boolean
     */
    public function getCheminementTdt()
    {

        return $this->cheminement_tdt;
    }

    /**
     * Get the [id_contrat_titulaire] column value.
     *
     * @return int
     */
    public function getIdContratTitulaire()
    {

        return $this->id_contrat_titulaire;
    }

    /**
     * Get the [referentiel_sous_type_parapheur_id] column value.
     *
     * @return int
     */
    public function getReferentielSousTypeParapheurId()
    {

        return $this->referentiel_sous_type_parapheur_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [echange_doc_application_client_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setEchangeDocApplicationClientId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->echange_doc_application_client_id !== $v) {
            $this->echange_doc_application_client_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID;
        }

        if ($this->aCommonEchangeDocApplicationClient !== null && $this->aCommonEchangeDocApplicationClient->getId() !== $v) {
            $this->aCommonEchangeDocApplicationClient = null;
        }


        return $this;
    } // setEchangeDocApplicationClientId()

    /**
     * Set the value of [objet] column.
     *
     * @param string $v new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setObjet($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->objet !== $v) {
            $this->objet = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::OBJET;
        }


        return $this;
    } // setObjet()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [consultation_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setConsultationId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_id !== $v) {
            $this->consultation_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::CONSULTATION_ID;
        }

        if ($this->aCommonConsultation !== null && $this->aCommonConsultation->getId() !== $v) {
            $this->aCommonConsultation = null;
        }


        return $this;
    } // setConsultationId()

    /**
     * Set the value of [agent_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setAgentId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->agent_id !== $v) {
            $this->agent_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::AGENT_ID;
        }

        if ($this->aCommonAgent !== null && $this->aCommonAgent->getId() !== $v) {
            $this->aCommonAgent = null;
        }


        return $this;
    } // setAgentId()

    /**
     * Set the value of [statut] column.
     *
     * @param string $v new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setStatut($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->statut !== $v) {
            $this->statut = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::STATUT;
        }


        return $this;
    } // setStatut()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = CommonEchangeDocPeer::CREATED_AT;
            }
        } // if either are not null


        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            $currentDateAsString = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->updated_at = $newDateAsString;
                $this->modifiedColumns[] = CommonEchangeDocPeer::UPDATED_AT;
            }
        } // if either are not null


        return $this;
    } // setUpdatedAt()

    /**
     * Sets the value of the [cheminement_signature] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setCheminementSignature($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cheminement_signature !== $v) {
            $this->cheminement_signature = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::CHEMINEMENT_SIGNATURE;
        }


        return $this;
    } // setCheminementSignature()

    /**
     * Sets the value of the [cheminement_ged] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setCheminementGed($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cheminement_ged !== $v) {
            $this->cheminement_ged = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::CHEMINEMENT_GED;
        }


        return $this;
    } // setCheminementGed()

    /**
     * Sets the value of the [cheminement_sae] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setCheminementSae($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cheminement_sae !== $v) {
            $this->cheminement_sae = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::CHEMINEMENT_SAE;
        }


        return $this;
    } // setCheminementSae()

    /**
     * Sets the value of the [cheminement_tdt] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setCheminementTdt($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cheminement_tdt !== $v) {
            $this->cheminement_tdt = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::CHEMINEMENT_TDT;
        }


        return $this;
    } // setCheminementTdt()

    /**
     * Set the value of [id_contrat_titulaire] column.
     *
     * @param int $v new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setIdContratTitulaire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_contrat_titulaire !== $v) {
            $this->id_contrat_titulaire = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE;
        }

        if ($this->aCommonTContratTitulaire !== null && $this->aCommonTContratTitulaire->getIdContratTitulaire() !== $v) {
            $this->aCommonTContratTitulaire = null;
        }


        return $this;
    } // setIdContratTitulaire()

    /**
     * Set the value of [referentiel_sous_type_parapheur_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setReferentielSousTypeParapheurId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->referentiel_sous_type_parapheur_id !== $v) {
            $this->referentiel_sous_type_parapheur_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID;
        }

        if ($this->aCommonReferentielSousTypeParapheur !== null && $this->aCommonReferentielSousTypeParapheur->getId() !== $v) {
            $this->aCommonReferentielSousTypeParapheur = null;
        }


        return $this;
    } // setReferentielSousTypeParapheurId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->cheminement_signature !== false) {
                return false;
            }

            if ($this->cheminement_ged !== false) {
                return false;
            }

            if ($this->cheminement_sae !== false) {
                return false;
            }

            if ($this->cheminement_tdt !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->echange_doc_application_client_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->objet = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->description = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->consultation_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->agent_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->statut = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->created_at = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->updated_at = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->cheminement_signature = ($row[$startcol + 9] !== null) ? (boolean) $row[$startcol + 9] : null;
            $this->cheminement_ged = ($row[$startcol + 10] !== null) ? (boolean) $row[$startcol + 10] : null;
            $this->cheminement_sae = ($row[$startcol + 11] !== null) ? (boolean) $row[$startcol + 11] : null;
            $this->cheminement_tdt = ($row[$startcol + 12] !== null) ? (boolean) $row[$startcol + 12] : null;
            $this->id_contrat_titulaire = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->referentiel_sous_type_parapheur_id = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 15; // 15 = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonEchangeDoc object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonEchangeDocApplicationClient !== null && $this->echange_doc_application_client_id !== $this->aCommonEchangeDocApplicationClient->getId()) {
            $this->aCommonEchangeDocApplicationClient = null;
        }
        if ($this->aCommonConsultation !== null && $this->consultation_id !== $this->aCommonConsultation->getId()) {
            $this->aCommonConsultation = null;
        }
        if ($this->aCommonAgent !== null && $this->agent_id !== $this->aCommonAgent->getId()) {
            $this->aCommonAgent = null;
        }
        if ($this->aCommonTContratTitulaire !== null && $this->id_contrat_titulaire !== $this->aCommonTContratTitulaire->getIdContratTitulaire()) {
            $this->aCommonTContratTitulaire = null;
        }
        if ($this->aCommonReferentielSousTypeParapheur !== null && $this->referentiel_sous_type_parapheur_id !== $this->aCommonReferentielSousTypeParapheur->getId()) {
            $this->aCommonReferentielSousTypeParapheur = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonEchangeDocPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonReferentielSousTypeParapheur = null;
            $this->aCommonTContratTitulaire = null;
            $this->aCommonAgent = null;
            $this->aCommonEchangeDocApplicationClient = null;
            $this->aCommonConsultation = null;
            $this->collCommonEchangeDocBlobs = null;

            $this->collCommonEchangeDocHistoriques = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonEchangeDocQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonEchangeDocPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonReferentielSousTypeParapheur !== null) {
                if ($this->aCommonReferentielSousTypeParapheur->isModified() || $this->aCommonReferentielSousTypeParapheur->isNew()) {
                    $affectedRows += $this->aCommonReferentielSousTypeParapheur->save($con);
                }
                $this->setCommonReferentielSousTypeParapheur($this->aCommonReferentielSousTypeParapheur);
            }

            if ($this->aCommonTContratTitulaire !== null) {
                if ($this->aCommonTContratTitulaire->isModified() || $this->aCommonTContratTitulaire->isNew()) {
                    $affectedRows += $this->aCommonTContratTitulaire->save($con);
                }
                $this->setCommonTContratTitulaire($this->aCommonTContratTitulaire);
            }

            if ($this->aCommonAgent !== null) {
                if ($this->aCommonAgent->isModified() || $this->aCommonAgent->isNew()) {
                    $affectedRows += $this->aCommonAgent->save($con);
                }
                $this->setCommonAgent($this->aCommonAgent);
            }

            if ($this->aCommonEchangeDocApplicationClient !== null) {
                if ($this->aCommonEchangeDocApplicationClient->isModified() || $this->aCommonEchangeDocApplicationClient->isNew()) {
                    $affectedRows += $this->aCommonEchangeDocApplicationClient->save($con);
                }
                $this->setCommonEchangeDocApplicationClient($this->aCommonEchangeDocApplicationClient);
            }

            if ($this->aCommonConsultation !== null) {
                if ($this->aCommonConsultation->isModified() || $this->aCommonConsultation->isNew()) {
                    $affectedRows += $this->aCommonConsultation->save($con);
                }
                $this->setCommonConsultation($this->aCommonConsultation);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonEchangeDocBlobsScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocBlobsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocBlobsScheduledForDeletion as $commonEchangeDocBlob) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDocBlob->save($con);
                    }
                    $this->commonEchangeDocBlobsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocBlobs !== null) {
                foreach ($this->collCommonEchangeDocBlobs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEchangeDocHistoriquesScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocHistoriquesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocHistoriquesScheduledForDeletion as $commonEchangeDocHistorique) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDocHistorique->save($con);
                    }
                    $this->commonEchangeDocHistoriquesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocHistoriques !== null) {
                foreach ($this->collCommonEchangeDocHistoriques as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonEchangeDocPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonEchangeDocPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonEchangeDocPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`echange_doc_application_client_id`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::OBJET)) {
            $modifiedColumns[':p' . $index++]  = '`objet`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`description`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::CONSULTATION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_id`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::AGENT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`agent_id`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`statut`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::CHEMINEMENT_SIGNATURE)) {
            $modifiedColumns[':p' . $index++]  = '`cheminement_signature`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::CHEMINEMENT_GED)) {
            $modifiedColumns[':p' . $index++]  = '`cheminement_ged`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::CHEMINEMENT_SAE)) {
            $modifiedColumns[':p' . $index++]  = '`cheminement_sae`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::CHEMINEMENT_TDT)) {
            $modifiedColumns[':p' . $index++]  = '`cheminement_tdt`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_contrat_titulaire`';
        }
        if ($this->isColumnModified(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID)) {
            $modifiedColumns[':p' . $index++]  = '`referentiel_sous_type_parapheur_id`';
        }

        $sql = sprintf(
            'INSERT INTO `echange_doc` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`echange_doc_application_client_id`':
                        $stmt->bindValue($identifier, $this->echange_doc_application_client_id, PDO::PARAM_INT);
                        break;
                    case '`objet`':
                        $stmt->bindValue($identifier, $this->objet, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`consultation_id`':
                        $stmt->bindValue($identifier, $this->consultation_id, PDO::PARAM_INT);
                        break;
                    case '`agent_id`':
                        $stmt->bindValue($identifier, $this->agent_id, PDO::PARAM_INT);
                        break;
                    case '`statut`':
                        $stmt->bindValue($identifier, $this->statut, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at, PDO::PARAM_STR);
                        break;
                    case '`cheminement_signature`':
                        $stmt->bindValue($identifier, (int) $this->cheminement_signature, PDO::PARAM_INT);
                        break;
                    case '`cheminement_ged`':
                        $stmt->bindValue($identifier, (int) $this->cheminement_ged, PDO::PARAM_INT);
                        break;
                    case '`cheminement_sae`':
                        $stmt->bindValue($identifier, (int) $this->cheminement_sae, PDO::PARAM_INT);
                        break;
                    case '`cheminement_tdt`':
                        $stmt->bindValue($identifier, (int) $this->cheminement_tdt, PDO::PARAM_INT);
                        break;
                    case '`id_contrat_titulaire`':
                        $stmt->bindValue($identifier, $this->id_contrat_titulaire, PDO::PARAM_INT);
                        break;
                    case '`referentiel_sous_type_parapheur_id`':
                        $stmt->bindValue($identifier, $this->referentiel_sous_type_parapheur_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonReferentielSousTypeParapheur !== null) {
                if (!$this->aCommonReferentielSousTypeParapheur->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonReferentielSousTypeParapheur->getValidationFailures());
                }
            }

            if ($this->aCommonTContratTitulaire !== null) {
                if (!$this->aCommonTContratTitulaire->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTContratTitulaire->getValidationFailures());
                }
            }

            if ($this->aCommonAgent !== null) {
                if (!$this->aCommonAgent->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonAgent->getValidationFailures());
                }
            }

            if ($this->aCommonEchangeDocApplicationClient !== null) {
                if (!$this->aCommonEchangeDocApplicationClient->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonEchangeDocApplicationClient->getValidationFailures());
                }
            }

            if ($this->aCommonConsultation !== null) {
                if (!$this->aCommonConsultation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonConsultation->getValidationFailures());
                }
            }


            if (($retval = CommonEchangeDocPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonEchangeDocBlobs !== null) {
                    foreach ($this->collCommonEchangeDocBlobs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEchangeDocHistoriques !== null) {
                    foreach ($this->collCommonEchangeDocHistoriques as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonEchangeDocPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getEchangeDocApplicationClientId();
                break;
            case 2:
                return $this->getObjet();
                break;
            case 3:
                return $this->getDescription();
                break;
            case 4:
                return $this->getConsultationId();
                break;
            case 5:
                return $this->getAgentId();
                break;
            case 6:
                return $this->getStatut();
                break;
            case 7:
                return $this->getCreatedAt();
                break;
            case 8:
                return $this->getUpdatedAt();
                break;
            case 9:
                return $this->getCheminementSignature();
                break;
            case 10:
                return $this->getCheminementGed();
                break;
            case 11:
                return $this->getCheminementSae();
                break;
            case 12:
                return $this->getCheminementTdt();
                break;
            case 13:
                return $this->getIdContratTitulaire();
                break;
            case 14:
                return $this->getReferentielSousTypeParapheurId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonEchangeDoc'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonEchangeDoc'][$this->getPrimaryKey()] = true;
        $keys = CommonEchangeDocPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getEchangeDocApplicationClientId(),
            $keys[2] => $this->getObjet(),
            $keys[3] => $this->getDescription(),
            $keys[4] => $this->getConsultationId(),
            $keys[5] => $this->getAgentId(),
            $keys[6] => $this->getStatut(),
            $keys[7] => $this->getCreatedAt(),
            $keys[8] => $this->getUpdatedAt(),
            $keys[9] => $this->getCheminementSignature(),
            $keys[10] => $this->getCheminementGed(),
            $keys[11] => $this->getCheminementSae(),
            $keys[12] => $this->getCheminementTdt(),
            $keys[13] => $this->getIdContratTitulaire(),
            $keys[14] => $this->getReferentielSousTypeParapheurId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonReferentielSousTypeParapheur) {
                $result['CommonReferentielSousTypeParapheur'] = $this->aCommonReferentielSousTypeParapheur->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTContratTitulaire) {
                $result['CommonTContratTitulaire'] = $this->aCommonTContratTitulaire->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonAgent) {
                $result['CommonAgent'] = $this->aCommonAgent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonEchangeDocApplicationClient) {
                $result['CommonEchangeDocApplicationClient'] = $this->aCommonEchangeDocApplicationClient->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonConsultation) {
                $result['CommonConsultation'] = $this->aCommonConsultation->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonEchangeDocBlobs) {
                $result['CommonEchangeDocBlobs'] = $this->collCommonEchangeDocBlobs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEchangeDocHistoriques) {
                $result['CommonEchangeDocHistoriques'] = $this->collCommonEchangeDocHistoriques->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonEchangeDocPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setEchangeDocApplicationClientId($value);
                break;
            case 2:
                $this->setObjet($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
            case 4:
                $this->setConsultationId($value);
                break;
            case 5:
                $this->setAgentId($value);
                break;
            case 6:
                $this->setStatut($value);
                break;
            case 7:
                $this->setCreatedAt($value);
                break;
            case 8:
                $this->setUpdatedAt($value);
                break;
            case 9:
                $this->setCheminementSignature($value);
                break;
            case 10:
                $this->setCheminementGed($value);
                break;
            case 11:
                $this->setCheminementSae($value);
                break;
            case 12:
                $this->setCheminementTdt($value);
                break;
            case 13:
                $this->setIdContratTitulaire($value);
                break;
            case 14:
                $this->setReferentielSousTypeParapheurId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonEchangeDocPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setEchangeDocApplicationClientId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setObjet($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDescription($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setConsultationId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setAgentId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setStatut($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setCreatedAt($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setUpdatedAt($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setCheminementSignature($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setCheminementGed($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setCheminementSae($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setCheminementTdt($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setIdContratTitulaire($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setReferentielSousTypeParapheurId($arr[$keys[14]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonEchangeDocPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonEchangeDocPeer::ID)) $criteria->add(CommonEchangeDocPeer::ID, $this->id);
        if ($this->isColumnModified(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID)) $criteria->add(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $this->echange_doc_application_client_id);
        if ($this->isColumnModified(CommonEchangeDocPeer::OBJET)) $criteria->add(CommonEchangeDocPeer::OBJET, $this->objet);
        if ($this->isColumnModified(CommonEchangeDocPeer::DESCRIPTION)) $criteria->add(CommonEchangeDocPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(CommonEchangeDocPeer::CONSULTATION_ID)) $criteria->add(CommonEchangeDocPeer::CONSULTATION_ID, $this->consultation_id);
        if ($this->isColumnModified(CommonEchangeDocPeer::AGENT_ID)) $criteria->add(CommonEchangeDocPeer::AGENT_ID, $this->agent_id);
        if ($this->isColumnModified(CommonEchangeDocPeer::STATUT)) $criteria->add(CommonEchangeDocPeer::STATUT, $this->statut);
        if ($this->isColumnModified(CommonEchangeDocPeer::CREATED_AT)) $criteria->add(CommonEchangeDocPeer::CREATED_AT, $this->created_at);
        if ($this->isColumnModified(CommonEchangeDocPeer::UPDATED_AT)) $criteria->add(CommonEchangeDocPeer::UPDATED_AT, $this->updated_at);
        if ($this->isColumnModified(CommonEchangeDocPeer::CHEMINEMENT_SIGNATURE)) $criteria->add(CommonEchangeDocPeer::CHEMINEMENT_SIGNATURE, $this->cheminement_signature);
        if ($this->isColumnModified(CommonEchangeDocPeer::CHEMINEMENT_GED)) $criteria->add(CommonEchangeDocPeer::CHEMINEMENT_GED, $this->cheminement_ged);
        if ($this->isColumnModified(CommonEchangeDocPeer::CHEMINEMENT_SAE)) $criteria->add(CommonEchangeDocPeer::CHEMINEMENT_SAE, $this->cheminement_sae);
        if ($this->isColumnModified(CommonEchangeDocPeer::CHEMINEMENT_TDT)) $criteria->add(CommonEchangeDocPeer::CHEMINEMENT_TDT, $this->cheminement_tdt);
        if ($this->isColumnModified(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE)) $criteria->add(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, $this->id_contrat_titulaire);
        if ($this->isColumnModified(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID)) $criteria->add(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, $this->referentiel_sous_type_parapheur_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonEchangeDocPeer::DATABASE_NAME);
        $criteria->add(CommonEchangeDocPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonEchangeDoc (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setEchangeDocApplicationClientId($this->getEchangeDocApplicationClientId());
        $copyObj->setObjet($this->getObjet());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setConsultationId($this->getConsultationId());
        $copyObj->setAgentId($this->getAgentId());
        $copyObj->setStatut($this->getStatut());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setCheminementSignature($this->getCheminementSignature());
        $copyObj->setCheminementGed($this->getCheminementGed());
        $copyObj->setCheminementSae($this->getCheminementSae());
        $copyObj->setCheminementTdt($this->getCheminementTdt());
        $copyObj->setIdContratTitulaire($this->getIdContratTitulaire());
        $copyObj->setReferentielSousTypeParapheurId($this->getReferentielSousTypeParapheurId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonEchangeDocBlobs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDocBlob($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEchangeDocHistoriques() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDocHistorique($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonEchangeDoc Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonEchangeDocPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonEchangeDocPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonReferentielSousTypeParapheur object.
     *
     * @param   CommonReferentielSousTypeParapheur $v
     * @return CommonEchangeDoc The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonReferentielSousTypeParapheur(CommonReferentielSousTypeParapheur $v = null)
    {
        if ($v === null) {
            $this->setReferentielSousTypeParapheurId(NULL);
        } else {
            $this->setReferentielSousTypeParapheurId($v->getId());
        }

        $this->aCommonReferentielSousTypeParapheur = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonReferentielSousTypeParapheur object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDoc($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonReferentielSousTypeParapheur object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonReferentielSousTypeParapheur The associated CommonReferentielSousTypeParapheur object.
     * @throws PropelException
     */
    public function getCommonReferentielSousTypeParapheur(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonReferentielSousTypeParapheur === null && ($this->referentiel_sous_type_parapheur_id !== null) && $doQuery) {
            $this->aCommonReferentielSousTypeParapheur = CommonReferentielSousTypeParapheurQuery::create()->findPk($this->referentiel_sous_type_parapheur_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonReferentielSousTypeParapheur->addCommonEchangeDocs($this);
             */
        }

        return $this->aCommonReferentielSousTypeParapheur;
    }

    /**
     * Declares an association between this object and a CommonTContratTitulaire object.
     *
     * @param   CommonTContratTitulaire $v
     * @return CommonEchangeDoc The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTContratTitulaire(CommonTContratTitulaire $v = null)
    {
        if ($v === null) {
            $this->setIdContratTitulaire(NULL);
        } else {
            $this->setIdContratTitulaire($v->getIdContratTitulaire());
        }

        $this->aCommonTContratTitulaire = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTContratTitulaire object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDoc($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTContratTitulaire object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTContratTitulaire The associated CommonTContratTitulaire object.
     * @throws PropelException
     */
    public function getCommonTContratTitulaire(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTContratTitulaire === null && ($this->id_contrat_titulaire !== null) && $doQuery) {
            $this->aCommonTContratTitulaire = CommonTContratTitulaireQuery::create()->findPk($this->id_contrat_titulaire, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTContratTitulaire->addCommonEchangeDocs($this);
             */
        }

        return $this->aCommonTContratTitulaire;
    }

    /**
     * Declares an association between this object and a CommonAgent object.
     *
     * @param   CommonAgent $v
     * @return CommonEchangeDoc The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonAgent(CommonAgent $v = null)
    {
        if ($v === null) {
            $this->setAgentId(NULL);
        } else {
            $this->setAgentId($v->getId());
        }

        $this->aCommonAgent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonAgent object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDoc($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonAgent object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonAgent The associated CommonAgent object.
     * @throws PropelException
     */
    public function getCommonAgent(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonAgent === null && ($this->agent_id !== null) && $doQuery) {
            $this->aCommonAgent = CommonAgentQuery::create()->findPk($this->agent_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonAgent->addCommonEchangeDocs($this);
             */
        }

        return $this->aCommonAgent;
    }

    /**
     * Declares an association between this object and a CommonEchangeDocApplicationClient object.
     *
     * @param   CommonEchangeDocApplicationClient $v
     * @return CommonEchangeDoc The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonEchangeDocApplicationClient(CommonEchangeDocApplicationClient $v = null)
    {
        if ($v === null) {
            $this->setEchangeDocApplicationClientId(NULL);
        } else {
            $this->setEchangeDocApplicationClientId($v->getId());
        }

        $this->aCommonEchangeDocApplicationClient = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonEchangeDocApplicationClient object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDoc($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonEchangeDocApplicationClient object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonEchangeDocApplicationClient The associated CommonEchangeDocApplicationClient object.
     * @throws PropelException
     */
    public function getCommonEchangeDocApplicationClient(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonEchangeDocApplicationClient === null && ($this->echange_doc_application_client_id !== null) && $doQuery) {
            $this->aCommonEchangeDocApplicationClient = CommonEchangeDocApplicationClientQuery::create()->findPk($this->echange_doc_application_client_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonEchangeDocApplicationClient->addCommonEchangeDocs($this);
             */
        }

        return $this->aCommonEchangeDocApplicationClient;
    }

    /**
     * Declares an association between this object and a CommonConsultation object.
     *
     * @param   CommonConsultation $v
     * @return CommonEchangeDoc The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonConsultation(CommonConsultation $v = null)
    {
        if ($v === null) {
            $this->setConsultationId(NULL);
        } else {
            $this->setConsultationId($v->getId());
        }

        $this->aCommonConsultation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonConsultation object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDoc($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonConsultation object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonConsultation The associated CommonConsultation object.
     * @throws PropelException
     */
    public function getCommonConsultation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonConsultation === null && ($this->consultation_id !== null) && $doQuery) {
            $this->aCommonConsultation = CommonConsultationQuery::create()->findPk($this->consultation_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonConsultation->addCommonEchangeDocs($this);
             */
        }

        return $this->aCommonConsultation;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonEchangeDocBlob' == $relationName) {
            $this->initCommonEchangeDocBlobs();
        }
        if ('CommonEchangeDocHistorique' == $relationName) {
            $this->initCommonEchangeDocHistoriques();
        }
    }

    /**
     * Clears out the collCommonEchangeDocBlobs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonEchangeDoc The current object (for fluent API support)
     * @see        addCommonEchangeDocBlobs()
     */
    public function clearCommonEchangeDocBlobs()
    {
        $this->collCommonEchangeDocBlobs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocBlobsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocBlobs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocBlobs($v = true)
    {
        $this->collCommonEchangeDocBlobsPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocBlobs collection.
     *
     * By default this just sets the collCommonEchangeDocBlobs collection to an empty array (like clearcollCommonEchangeDocBlobs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocBlobs($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocBlobs && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocBlobs = new PropelObjectCollection();
        $this->collCommonEchangeDocBlobs->setModel('CommonEchangeDocBlob');
    }

    /**
     * Gets an array of CommonEchangeDocBlob objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonEchangeDoc is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     * @throws PropelException
     */
    public function getCommonEchangeDocBlobs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocBlobsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocBlobs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocBlobs) {
                // return empty collection
                $this->initCommonEchangeDocBlobs();
            } else {
                $collCommonEchangeDocBlobs = CommonEchangeDocBlobQuery::create(null, $criteria)
                    ->filterByCommonEchangeDoc($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocBlobsPartial && count($collCommonEchangeDocBlobs)) {
                      $this->initCommonEchangeDocBlobs(false);

                      foreach ($collCommonEchangeDocBlobs as $obj) {
                        if (false == $this->collCommonEchangeDocBlobs->contains($obj)) {
                          $this->collCommonEchangeDocBlobs->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocBlobsPartial = true;
                    }

                    $collCommonEchangeDocBlobs->getInternalIterator()->rewind();

                    return $collCommonEchangeDocBlobs;
                }

                if ($partial && $this->collCommonEchangeDocBlobs) {
                    foreach ($this->collCommonEchangeDocBlobs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocBlobs[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocBlobs = $collCommonEchangeDocBlobs;
                $this->collCommonEchangeDocBlobsPartial = false;
            }
        }

        return $this->collCommonEchangeDocBlobs;
    }

    /**
     * Sets a collection of CommonEchangeDocBlob objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocBlobs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setCommonEchangeDocBlobs(PropelCollection $commonEchangeDocBlobs, PropelPDO $con = null)
    {
        $commonEchangeDocBlobsToDelete = $this->getCommonEchangeDocBlobs(new Criteria(), $con)->diff($commonEchangeDocBlobs);


        $this->commonEchangeDocBlobsScheduledForDeletion = $commonEchangeDocBlobsToDelete;

        foreach ($commonEchangeDocBlobsToDelete as $commonEchangeDocBlobRemoved) {
            $commonEchangeDocBlobRemoved->setCommonEchangeDoc(null);
        }

        $this->collCommonEchangeDocBlobs = null;
        foreach ($commonEchangeDocBlobs as $commonEchangeDocBlob) {
            $this->addCommonEchangeDocBlob($commonEchangeDocBlob);
        }

        $this->collCommonEchangeDocBlobs = $commonEchangeDocBlobs;
        $this->collCommonEchangeDocBlobsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDocBlob objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDocBlob objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocBlobs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocBlobsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocBlobs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocBlobs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocBlobs());
            }
            $query = CommonEchangeDocBlobQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonEchangeDoc($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocBlobs);
    }

    /**
     * Method called to associate a CommonEchangeDocBlob object to this object
     * through the CommonEchangeDocBlob foreign key attribute.
     *
     * @param   CommonEchangeDocBlob $l CommonEchangeDocBlob
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function addCommonEchangeDocBlob(CommonEchangeDocBlob $l)
    {
        if ($this->collCommonEchangeDocBlobs === null) {
            $this->initCommonEchangeDocBlobs();
            $this->collCommonEchangeDocBlobsPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocBlobs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDocBlob($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDocBlob $commonEchangeDocBlob The commonEchangeDocBlob object to add.
     */
    protected function doAddCommonEchangeDocBlob($commonEchangeDocBlob)
    {
        $this->collCommonEchangeDocBlobs[]= $commonEchangeDocBlob;
        $commonEchangeDocBlob->setCommonEchangeDoc($this);
    }

    /**
     * @param	CommonEchangeDocBlob $commonEchangeDocBlob The commonEchangeDocBlob object to remove.
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function removeCommonEchangeDocBlob($commonEchangeDocBlob)
    {
        if ($this->getCommonEchangeDocBlobs()->contains($commonEchangeDocBlob)) {
            $this->collCommonEchangeDocBlobs->remove($this->collCommonEchangeDocBlobs->search($commonEchangeDocBlob));
            if (null === $this->commonEchangeDocBlobsScheduledForDeletion) {
                $this->commonEchangeDocBlobsScheduledForDeletion = clone $this->collCommonEchangeDocBlobs;
                $this->commonEchangeDocBlobsScheduledForDeletion->clear();
            }
            $this->commonEchangeDocBlobsScheduledForDeletion[]= $commonEchangeDocBlob;
            $commonEchangeDocBlob->setCommonEchangeDoc(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDoc is new, it will return
     * an empty collection; or if this CommonEchangeDoc has previously
     * been saved, it will retrieve related CommonEchangeDocBlobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDoc.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     */
    public function getCommonEchangeDocBlobsJoinCommonBlobOrganismeFile($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocBlobQuery::create(null, $criteria);
        $query->joinWith('CommonBlobOrganismeFile', $join_behavior);

        return $this->getCommonEchangeDocBlobs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDoc is new, it will return
     * an empty collection; or if this CommonEchangeDoc has previously
     * been saved, it will retrieve related CommonEchangeDocBlobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDoc.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     */
    public function getCommonEchangeDocBlobsJoinCommonEchangeDocBlobRelatedByDocBlobPrincipalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocBlobQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDocBlobRelatedByDocBlobPrincipalId', $join_behavior);

        return $this->getCommonEchangeDocBlobs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDoc is new, it will return
     * an empty collection; or if this CommonEchangeDoc has previously
     * been saved, it will retrieve related CommonEchangeDocBlobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDoc.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     */
    public function getCommonEchangeDocBlobsJoinCommonEchangeDocTypePieceStandard($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocBlobQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDocTypePieceStandard', $join_behavior);

        return $this->getCommonEchangeDocBlobs($query, $con);
    }

    /**
     * Clears out the collCommonEchangeDocHistoriques collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonEchangeDoc The current object (for fluent API support)
     * @see        addCommonEchangeDocHistoriques()
     */
    public function clearCommonEchangeDocHistoriques()
    {
        $this->collCommonEchangeDocHistoriques = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocHistoriquesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocHistoriques collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocHistoriques($v = true)
    {
        $this->collCommonEchangeDocHistoriquesPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocHistoriques collection.
     *
     * By default this just sets the collCommonEchangeDocHistoriques collection to an empty array (like clearcollCommonEchangeDocHistoriques());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocHistoriques($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocHistoriques && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocHistoriques = new PropelObjectCollection();
        $this->collCommonEchangeDocHistoriques->setModel('CommonEchangeDocHistorique');
    }

    /**
     * Gets an array of CommonEchangeDocHistorique objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonEchangeDoc is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDocHistorique[] List of CommonEchangeDocHistorique objects
     * @throws PropelException
     */
    public function getCommonEchangeDocHistoriques($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocHistoriquesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocHistoriques || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocHistoriques) {
                // return empty collection
                $this->initCommonEchangeDocHistoriques();
            } else {
                $collCommonEchangeDocHistoriques = CommonEchangeDocHistoriqueQuery::create(null, $criteria)
                    ->filterByCommonEchangeDoc($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocHistoriquesPartial && count($collCommonEchangeDocHistoriques)) {
                      $this->initCommonEchangeDocHistoriques(false);

                      foreach ($collCommonEchangeDocHistoriques as $obj) {
                        if (false == $this->collCommonEchangeDocHistoriques->contains($obj)) {
                          $this->collCommonEchangeDocHistoriques->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocHistoriquesPartial = true;
                    }

                    $collCommonEchangeDocHistoriques->getInternalIterator()->rewind();

                    return $collCommonEchangeDocHistoriques;
                }

                if ($partial && $this->collCommonEchangeDocHistoriques) {
                    foreach ($this->collCommonEchangeDocHistoriques as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocHistoriques[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocHistoriques = $collCommonEchangeDocHistoriques;
                $this->collCommonEchangeDocHistoriquesPartial = false;
            }
        }

        return $this->collCommonEchangeDocHistoriques;
    }

    /**
     * Sets a collection of CommonEchangeDocHistorique objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocHistoriques A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function setCommonEchangeDocHistoriques(PropelCollection $commonEchangeDocHistoriques, PropelPDO $con = null)
    {
        $commonEchangeDocHistoriquesToDelete = $this->getCommonEchangeDocHistoriques(new Criteria(), $con)->diff($commonEchangeDocHistoriques);


        $this->commonEchangeDocHistoriquesScheduledForDeletion = $commonEchangeDocHistoriquesToDelete;

        foreach ($commonEchangeDocHistoriquesToDelete as $commonEchangeDocHistoriqueRemoved) {
            $commonEchangeDocHistoriqueRemoved->setCommonEchangeDoc(null);
        }

        $this->collCommonEchangeDocHistoriques = null;
        foreach ($commonEchangeDocHistoriques as $commonEchangeDocHistorique) {
            $this->addCommonEchangeDocHistorique($commonEchangeDocHistorique);
        }

        $this->collCommonEchangeDocHistoriques = $commonEchangeDocHistoriques;
        $this->collCommonEchangeDocHistoriquesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDocHistorique objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDocHistorique objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocHistoriques(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocHistoriquesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocHistoriques || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocHistoriques) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocHistoriques());
            }
            $query = CommonEchangeDocHistoriqueQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonEchangeDoc($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocHistoriques);
    }

    /**
     * Method called to associate a CommonEchangeDocHistorique object to this object
     * through the CommonEchangeDocHistorique foreign key attribute.
     *
     * @param   CommonEchangeDocHistorique $l CommonEchangeDocHistorique
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function addCommonEchangeDocHistorique(CommonEchangeDocHistorique $l)
    {
        if ($this->collCommonEchangeDocHistoriques === null) {
            $this->initCommonEchangeDocHistoriques();
            $this->collCommonEchangeDocHistoriquesPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocHistoriques->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDocHistorique($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDocHistorique $commonEchangeDocHistorique The commonEchangeDocHistorique object to add.
     */
    protected function doAddCommonEchangeDocHistorique($commonEchangeDocHistorique)
    {
        $this->collCommonEchangeDocHistoriques[]= $commonEchangeDocHistorique;
        $commonEchangeDocHistorique->setCommonEchangeDoc($this);
    }

    /**
     * @param	CommonEchangeDocHistorique $commonEchangeDocHistorique The commonEchangeDocHistorique object to remove.
     * @return CommonEchangeDoc The current object (for fluent API support)
     */
    public function removeCommonEchangeDocHistorique($commonEchangeDocHistorique)
    {
        if ($this->getCommonEchangeDocHistoriques()->contains($commonEchangeDocHistorique)) {
            $this->collCommonEchangeDocHistoriques->remove($this->collCommonEchangeDocHistoriques->search($commonEchangeDocHistorique));
            if (null === $this->commonEchangeDocHistoriquesScheduledForDeletion) {
                $this->commonEchangeDocHistoriquesScheduledForDeletion = clone $this->collCommonEchangeDocHistoriques;
                $this->commonEchangeDocHistoriquesScheduledForDeletion->clear();
            }
            $this->commonEchangeDocHistoriquesScheduledForDeletion[]= $commonEchangeDocHistorique;
            $commonEchangeDocHistorique->setCommonEchangeDoc(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDoc is new, it will return
     * an empty collection; or if this CommonEchangeDoc has previously
     * been saved, it will retrieve related CommonEchangeDocHistoriques from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDoc.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocHistorique[] List of CommonEchangeDocHistorique objects
     */
    public function getCommonEchangeDocHistoriquesJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocHistoriqueQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonEchangeDocHistoriques($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->echange_doc_application_client_id = null;
        $this->objet = null;
        $this->description = null;
        $this->consultation_id = null;
        $this->agent_id = null;
        $this->statut = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->cheminement_signature = null;
        $this->cheminement_ged = null;
        $this->cheminement_sae = null;
        $this->cheminement_tdt = null;
        $this->id_contrat_titulaire = null;
        $this->referentiel_sous_type_parapheur_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonEchangeDocBlobs) {
                foreach ($this->collCommonEchangeDocBlobs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEchangeDocHistoriques) {
                foreach ($this->collCommonEchangeDocHistoriques as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonReferentielSousTypeParapheur instanceof Persistent) {
              $this->aCommonReferentielSousTypeParapheur->clearAllReferences($deep);
            }
            if ($this->aCommonTContratTitulaire instanceof Persistent) {
              $this->aCommonTContratTitulaire->clearAllReferences($deep);
            }
            if ($this->aCommonAgent instanceof Persistent) {
              $this->aCommonAgent->clearAllReferences($deep);
            }
            if ($this->aCommonEchangeDocApplicationClient instanceof Persistent) {
              $this->aCommonEchangeDocApplicationClient->clearAllReferences($deep);
            }
            if ($this->aCommonConsultation instanceof Persistent) {
              $this->aCommonConsultation->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonEchangeDocBlobs instanceof PropelCollection) {
            $this->collCommonEchangeDocBlobs->clearIterator();
        }
        $this->collCommonEchangeDocBlobs = null;
        if ($this->collCommonEchangeDocHistoriques instanceof PropelCollection) {
            $this->collCommonEchangeDocHistoriques->clearIterator();
        }
        $this->collCommonEchangeDocHistoriques = null;
        $this->aCommonReferentielSousTypeParapheur = null;
        $this->aCommonTContratTitulaire = null;
        $this->aCommonAgent = null;
        $this->aCommonEchangeDocApplicationClient = null;
        $this->aCommonConsultation = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonEchangeDocPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
