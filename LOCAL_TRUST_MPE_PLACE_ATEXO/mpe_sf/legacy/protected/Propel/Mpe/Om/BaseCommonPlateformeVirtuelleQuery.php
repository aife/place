<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonPlateformeVirtuelleOrganisme;
use Application\Propel\Mpe\CommonPlateformeVirtuellePeer;
use Application\Propel\Mpe\CommonPlateformeVirtuelleQuery;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonTMesRecherches;
use Application\Propel\Mpe\CommonTelechargement;

/**
 * Base class that represents a query for the 'plateforme_virtuelle' table.
 *
 *
 *
 * @method CommonPlateformeVirtuelleQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonPlateformeVirtuelleQuery orderByDomain($order = Criteria::ASC) Order by the domain column
 * @method CommonPlateformeVirtuelleQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method CommonPlateformeVirtuelleQuery orderByCodeDesign($order = Criteria::ASC) Order by the code_design column
 * @method CommonPlateformeVirtuelleQuery orderByProtocole($order = Criteria::ASC) Order by the protocole column
 * @method CommonPlateformeVirtuelleQuery orderByNoReply($order = Criteria::ASC) Order by the no_reply column
 * @method CommonPlateformeVirtuelleQuery orderByFooterMail($order = Criteria::ASC) Order by the footer_mail column
 * @method CommonPlateformeVirtuelleQuery orderByFromPfName($order = Criteria::ASC) Order by the from_pf_name column
 *
 * @method CommonPlateformeVirtuelleQuery groupById() Group by the id column
 * @method CommonPlateformeVirtuelleQuery groupByDomain() Group by the domain column
 * @method CommonPlateformeVirtuelleQuery groupByName() Group by the name column
 * @method CommonPlateformeVirtuelleQuery groupByCodeDesign() Group by the code_design column
 * @method CommonPlateformeVirtuelleQuery groupByProtocole() Group by the protocole column
 * @method CommonPlateformeVirtuelleQuery groupByNoReply() Group by the no_reply column
 * @method CommonPlateformeVirtuelleQuery groupByFooterMail() Group by the footer_mail column
 * @method CommonPlateformeVirtuelleQuery groupByFromPfName() Group by the from_pf_name column
 *
 * @method CommonPlateformeVirtuelleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonPlateformeVirtuelleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonPlateformeVirtuelleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonPlateformeVirtuelleQuery leftJoinCommonOffres($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOffres relation
 * @method CommonPlateformeVirtuelleQuery rightJoinCommonOffres($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOffres relation
 * @method CommonPlateformeVirtuelleQuery innerJoinCommonOffres($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOffres relation
 *
 * @method CommonPlateformeVirtuelleQuery leftJoinCommonTMesRecherches($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTMesRecherches relation
 * @method CommonPlateformeVirtuelleQuery rightJoinCommonTMesRecherches($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTMesRecherches relation
 * @method CommonPlateformeVirtuelleQuery innerJoinCommonTMesRecherches($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTMesRecherches relation
 *
 * @method CommonPlateformeVirtuelleQuery leftJoinCommonTelechargement($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTelechargement relation
 * @method CommonPlateformeVirtuelleQuery rightJoinCommonTelechargement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTelechargement relation
 * @method CommonPlateformeVirtuelleQuery innerJoinCommonTelechargement($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTelechargement relation
 *
 * @method CommonPlateformeVirtuelleQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonPlateformeVirtuelleQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonPlateformeVirtuelleQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonPlateformeVirtuelleQuery leftJoinCommonPlateformeVirtuelleOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonPlateformeVirtuelleOrganisme relation
 * @method CommonPlateformeVirtuelleQuery rightJoinCommonPlateformeVirtuelleOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonPlateformeVirtuelleOrganisme relation
 * @method CommonPlateformeVirtuelleQuery innerJoinCommonPlateformeVirtuelleOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonPlateformeVirtuelleOrganisme relation
 *
 * @method CommonPlateformeVirtuelleQuery leftJoinCommonQuestionsDce($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonQuestionsDce relation
 * @method CommonPlateformeVirtuelleQuery rightJoinCommonQuestionsDce($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonQuestionsDce relation
 * @method CommonPlateformeVirtuelleQuery innerJoinCommonQuestionsDce($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonQuestionsDce relation
 *
 * @method CommonPlateformeVirtuelle findOne(PropelPDO $con = null) Return the first CommonPlateformeVirtuelle matching the query
 * @method CommonPlateformeVirtuelle findOneOrCreate(PropelPDO $con = null) Return the first CommonPlateformeVirtuelle matching the query, or a new CommonPlateformeVirtuelle object populated from the query conditions when no match is found
 *
 * @method CommonPlateformeVirtuelle findOneByDomain(string $domain) Return the first CommonPlateformeVirtuelle filtered by the domain column
 * @method CommonPlateformeVirtuelle findOneByName(string $name) Return the first CommonPlateformeVirtuelle filtered by the name column
 * @method CommonPlateformeVirtuelle findOneByCodeDesign(string $code_design) Return the first CommonPlateformeVirtuelle filtered by the code_design column
 * @method CommonPlateformeVirtuelle findOneByProtocole(string $protocole) Return the first CommonPlateformeVirtuelle filtered by the protocole column
 * @method CommonPlateformeVirtuelle findOneByNoReply(string $no_reply) Return the first CommonPlateformeVirtuelle filtered by the no_reply column
 * @method CommonPlateformeVirtuelle findOneByFooterMail(string $footer_mail) Return the first CommonPlateformeVirtuelle filtered by the footer_mail column
 * @method CommonPlateformeVirtuelle findOneByFromPfName(string $from_pf_name) Return the first CommonPlateformeVirtuelle filtered by the from_pf_name column
 *
 * @method array findById(int $id) Return CommonPlateformeVirtuelle objects filtered by the id column
 * @method array findByDomain(string $domain) Return CommonPlateformeVirtuelle objects filtered by the domain column
 * @method array findByName(string $name) Return CommonPlateformeVirtuelle objects filtered by the name column
 * @method array findByCodeDesign(string $code_design) Return CommonPlateformeVirtuelle objects filtered by the code_design column
 * @method array findByProtocole(string $protocole) Return CommonPlateformeVirtuelle objects filtered by the protocole column
 * @method array findByNoReply(string $no_reply) Return CommonPlateformeVirtuelle objects filtered by the no_reply column
 * @method array findByFooterMail(string $footer_mail) Return CommonPlateformeVirtuelle objects filtered by the footer_mail column
 * @method array findByFromPfName(string $from_pf_name) Return CommonPlateformeVirtuelle objects filtered by the from_pf_name column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonPlateformeVirtuelleQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonPlateformeVirtuelleQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonPlateformeVirtuelle', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonPlateformeVirtuelleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonPlateformeVirtuelleQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonPlateformeVirtuelleQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonPlateformeVirtuelleQuery) {
            return $criteria;
        }
        $query = new CommonPlateformeVirtuelleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonPlateformeVirtuelle|CommonPlateformeVirtuelle[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonPlateformeVirtuellePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonPlateformeVirtuellePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonPlateformeVirtuelle A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonPlateformeVirtuelle A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `domain`, `name`, `code_design`, `protocole`, `no_reply`, `footer_mail`, `from_pf_name` FROM `plateforme_virtuelle` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonPlateformeVirtuelle();
            $obj->hydrate($row);
            CommonPlateformeVirtuellePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonPlateformeVirtuelle|CommonPlateformeVirtuelle[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonPlateformeVirtuelle[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the domain column
     *
     * Example usage:
     * <code>
     * $query->filterByDomain('fooValue');   // WHERE domain = 'fooValue'
     * $query->filterByDomain('%fooValue%'); // WHERE domain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $domain The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterByDomain($domain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($domain)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $domain)) {
                $domain = str_replace('*', '%', $domain);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::DOMAIN, $domain, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the code_design column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeDesign('fooValue');   // WHERE code_design = 'fooValue'
     * $query->filterByCodeDesign('%fooValue%'); // WHERE code_design LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeDesign The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterByCodeDesign($codeDesign = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeDesign)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeDesign)) {
                $codeDesign = str_replace('*', '%', $codeDesign);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::CODE_DESIGN, $codeDesign, $comparison);
    }

    /**
     * Filter the query on the protocole column
     *
     * Example usage:
     * <code>
     * $query->filterByProtocole('fooValue');   // WHERE protocole = 'fooValue'
     * $query->filterByProtocole('%fooValue%'); // WHERE protocole LIKE '%fooValue%'
     * </code>
     *
     * @param     string $protocole The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterByProtocole($protocole = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($protocole)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $protocole)) {
                $protocole = str_replace('*', '%', $protocole);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::PROTOCOLE, $protocole, $comparison);
    }

    /**
     * Filter the query on the no_reply column
     *
     * Example usage:
     * <code>
     * $query->filterByNoReply('fooValue');   // WHERE no_reply = 'fooValue'
     * $query->filterByNoReply('%fooValue%'); // WHERE no_reply LIKE '%fooValue%'
     * </code>
     *
     * @param     string $noReply The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterByNoReply($noReply = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($noReply)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $noReply)) {
                $noReply = str_replace('*', '%', $noReply);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::NO_REPLY, $noReply, $comparison);
    }

    /**
     * Filter the query on the footer_mail column
     *
     * Example usage:
     * <code>
     * $query->filterByFooterMail('fooValue');   // WHERE footer_mail = 'fooValue'
     * $query->filterByFooterMail('%fooValue%'); // WHERE footer_mail LIKE '%fooValue%'
     * </code>
     *
     * @param     string $footerMail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterByFooterMail($footerMail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($footerMail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $footerMail)) {
                $footerMail = str_replace('*', '%', $footerMail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::FOOTER_MAIL, $footerMail, $comparison);
    }

    /**
     * Filter the query on the from_pf_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFromPfName('fooValue');   // WHERE from_pf_name = 'fooValue'
     * $query->filterByFromPfName('%fooValue%'); // WHERE from_pf_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fromPfName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function filterByFromPfName($fromPfName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fromPfName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fromPfName)) {
                $fromPfName = str_replace('*', '%', $fromPfName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuellePeer::FROM_PF_NAME, $fromPfName, $comparison);
    }

    /**
     * Filter the query by a related CommonOffres object
     *
     * @param   CommonOffres|PropelObjectCollection $commonOffres  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPlateformeVirtuelleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOffres($commonOffres, $comparison = null)
    {
        if ($commonOffres instanceof CommonOffres) {
            return $this
                ->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $commonOffres->getPlateformeVirtuelleId(), $comparison);
        } elseif ($commonOffres instanceof PropelObjectCollection) {
            return $this
                ->useCommonOffresQuery()
                ->filterByPrimaryKeys($commonOffres->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonOffres() only accepts arguments of type CommonOffres or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOffres relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function joinCommonOffres($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOffres');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOffres');
        }

        return $this;
    }

    /**
     * Use the CommonOffres relation CommonOffres object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOffresQuery A secondary query class using the current class as primary query
     */
    public function useCommonOffresQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonOffres($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOffres', '\Application\Propel\Mpe\CommonOffresQuery');
    }

    /**
     * Filter the query by a related CommonTMesRecherches object
     *
     * @param   CommonTMesRecherches|PropelObjectCollection $commonTMesRecherches  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPlateformeVirtuelleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTMesRecherches($commonTMesRecherches, $comparison = null)
    {
        if ($commonTMesRecherches instanceof CommonTMesRecherches) {
            return $this
                ->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $commonTMesRecherches->getPlateformeVirtuelleId(), $comparison);
        } elseif ($commonTMesRecherches instanceof PropelObjectCollection) {
            return $this
                ->useCommonTMesRecherchesQuery()
                ->filterByPrimaryKeys($commonTMesRecherches->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTMesRecherches() only accepts arguments of type CommonTMesRecherches or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTMesRecherches relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function joinCommonTMesRecherches($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTMesRecherches');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTMesRecherches');
        }

        return $this;
    }

    /**
     * Use the CommonTMesRecherches relation CommonTMesRecherches object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTMesRecherchesQuery A secondary query class using the current class as primary query
     */
    public function useCommonTMesRecherchesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTMesRecherches($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTMesRecherches', '\Application\Propel\Mpe\CommonTMesRecherchesQuery');
    }

    /**
     * Filter the query by a related CommonTelechargement object
     *
     * @param   CommonTelechargement|PropelObjectCollection $commonTelechargement  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPlateformeVirtuelleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTelechargement($commonTelechargement, $comparison = null)
    {
        if ($commonTelechargement instanceof CommonTelechargement) {
            return $this
                ->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $commonTelechargement->getPlateformeVirtuelleId(), $comparison);
        } elseif ($commonTelechargement instanceof PropelObjectCollection) {
            return $this
                ->useCommonTelechargementQuery()
                ->filterByPrimaryKeys($commonTelechargement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTelechargement() only accepts arguments of type CommonTelechargement or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTelechargement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function joinCommonTelechargement($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTelechargement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTelechargement');
        }

        return $this;
    }

    /**
     * Use the CommonTelechargement relation CommonTelechargement object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTelechargementQuery A secondary query class using the current class as primary query
     */
    public function useCommonTelechargementQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTelechargement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTelechargement', '\Application\Propel\Mpe\CommonTelechargementQuery');
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPlateformeVirtuelleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $commonConsultation->getPlateformeVirtuelleId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationQuery()
                ->filterByPrimaryKeys($commonConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Filter the query by a related CommonPlateformeVirtuelleOrganisme object
     *
     * @param   CommonPlateformeVirtuelleOrganisme|PropelObjectCollection $commonPlateformeVirtuelleOrganisme  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPlateformeVirtuelleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonPlateformeVirtuelleOrganisme($commonPlateformeVirtuelleOrganisme, $comparison = null)
    {
        if ($commonPlateformeVirtuelleOrganisme instanceof CommonPlateformeVirtuelleOrganisme) {
            return $this
                ->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $commonPlateformeVirtuelleOrganisme->getPlateformeId(), $comparison);
        } elseif ($commonPlateformeVirtuelleOrganisme instanceof PropelObjectCollection) {
            return $this
                ->useCommonPlateformeVirtuelleOrganismeQuery()
                ->filterByPrimaryKeys($commonPlateformeVirtuelleOrganisme->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonPlateformeVirtuelleOrganisme() only accepts arguments of type CommonPlateformeVirtuelleOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonPlateformeVirtuelleOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function joinCommonPlateformeVirtuelleOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonPlateformeVirtuelleOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonPlateformeVirtuelleOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonPlateformeVirtuelleOrganisme relation CommonPlateformeVirtuelleOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonPlateformeVirtuelleOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonPlateformeVirtuelleOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonPlateformeVirtuelleOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonPlateformeVirtuelleOrganisme', '\Application\Propel\Mpe\CommonPlateformeVirtuelleOrganismeQuery');
    }

    /**
     * Filter the query by a related CommonQuestionsDce object
     *
     * @param   CommonQuestionsDce|PropelObjectCollection $commonQuestionsDce  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPlateformeVirtuelleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonQuestionsDce($commonQuestionsDce, $comparison = null)
    {
        if ($commonQuestionsDce instanceof CommonQuestionsDce) {
            return $this
                ->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $commonQuestionsDce->getPlateformeVirtuelleId(), $comparison);
        } elseif ($commonQuestionsDce instanceof PropelObjectCollection) {
            return $this
                ->useCommonQuestionsDceQuery()
                ->filterByPrimaryKeys($commonQuestionsDce->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonQuestionsDce() only accepts arguments of type CommonQuestionsDce or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonQuestionsDce relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function joinCommonQuestionsDce($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonQuestionsDce');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonQuestionsDce');
        }

        return $this;
    }

    /**
     * Use the CommonQuestionsDce relation CommonQuestionsDce object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonQuestionsDceQuery A secondary query class using the current class as primary query
     */
    public function useCommonQuestionsDceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonQuestionsDce($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonQuestionsDce', '\Application\Propel\Mpe\CommonQuestionsDceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonPlateformeVirtuelle $commonPlateformeVirtuelle Object to remove from the list of results
     *
     * @return CommonPlateformeVirtuelleQuery The current query, for fluid interface
     */
    public function prune($commonPlateformeVirtuelle = null)
    {
        if ($commonPlateformeVirtuelle) {
            $this->addUsingAlias(CommonPlateformeVirtuellePeer::ID, $commonPlateformeVirtuelle->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
