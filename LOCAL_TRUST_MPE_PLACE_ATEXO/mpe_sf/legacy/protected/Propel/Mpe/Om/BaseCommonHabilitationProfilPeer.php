<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonHabilitationProfil;
use Application\Propel\Mpe\CommonHabilitationProfilPeer;
use Application\Propel\Mpe\Map\CommonHabilitationProfilTableMap;

/**
 * Base static class for performing query and update operations on the 'HabilitationProfil' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonHabilitationProfilPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'HabilitationProfil';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonHabilitationProfil';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonHabilitationProfilTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 208;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 208;

    /** the column name for the id field */
    const ID = 'HabilitationProfil.id';

    /** the column name for the libelle field */
    const LIBELLE = 'HabilitationProfil.libelle';

    /** the column name for the gestion_agent_pole field */
    const GESTION_AGENT_POLE = 'HabilitationProfil.gestion_agent_pole';

    /** the column name for the gestion_fournisseurs_envois_postaux field */
    const GESTION_FOURNISSEURS_ENVOIS_POSTAUX = 'HabilitationProfil.gestion_fournisseurs_envois_postaux';

    /** the column name for the gestion_bi_cles field */
    const GESTION_BI_CLES = 'HabilitationProfil.gestion_bi_cles';

    /** the column name for the creer_consultation field */
    const CREER_CONSULTATION = 'HabilitationProfil.creer_consultation';

    /** the column name for the modifier_consultation field */
    const MODIFIER_CONSULTATION = 'HabilitationProfil.modifier_consultation';

    /** the column name for the valider_consultation field */
    const VALIDER_CONSULTATION = 'HabilitationProfil.valider_consultation';

    /** the column name for the publier_consultation field */
    const PUBLIER_CONSULTATION = 'HabilitationProfil.publier_consultation';

    /** the column name for the suivre_consultation field */
    const SUIVRE_CONSULTATION = 'HabilitationProfil.suivre_consultation';

    /** the column name for the suivre_consultation_pole field */
    const SUIVRE_CONSULTATION_POLE = 'HabilitationProfil.suivre_consultation_pole';

    /** the column name for the invite_permanent_entite_dependante field */
    const INVITE_PERMANENT_ENTITE_DEPENDANTE = 'HabilitationProfil.invite_permanent_entite_dependante';

    /** the column name for the invite_permanent_mon_entite field */
    const INVITE_PERMANENT_MON_ENTITE = 'HabilitationProfil.invite_permanent_mon_entite';

    /** the column name for the invite_permanent_transverse field */
    const INVITE_PERMANENT_TRANSVERSE = 'HabilitationProfil.invite_permanent_transverse';

    /** the column name for the supprimer_enveloppe field */
    const SUPPRIMER_ENVELOPPE = 'HabilitationProfil.supprimer_enveloppe';

    /** the column name for the supprimer_consultation field */
    const SUPPRIMER_CONSULTATION = 'HabilitationProfil.supprimer_consultation';

    /** the column name for the depouiller_candidature field */
    const DEPOUILLER_CANDIDATURE = 'HabilitationProfil.depouiller_candidature';

    /** the column name for the depouiller_offre field */
    const DEPOUILLER_OFFRE = 'HabilitationProfil.depouiller_offre';

    /** the column name for the messagerie_securisee field */
    const MESSAGERIE_SECURISEE = 'HabilitationProfil.messagerie_securisee';

    /** the column name for the acces_registre_depots_papier field */
    const ACCES_REGISTRE_DEPOTS_PAPIER = 'HabilitationProfil.acces_registre_depots_papier';

    /** the column name for the acces_registre_retraits_papier field */
    const ACCES_REGISTRE_RETRAITS_PAPIER = 'HabilitationProfil.acces_registre_retraits_papier';

    /** the column name for the acces_registre_questions_papier field */
    const ACCES_REGISTRE_QUESTIONS_PAPIER = 'HabilitationProfil.acces_registre_questions_papier';

    /** the column name for the gerer_encheres field */
    const GERER_ENCHERES = 'HabilitationProfil.gerer_encheres';

    /** the column name for the suivre_encheres field */
    const SUIVRE_ENCHERES = 'HabilitationProfil.suivre_encheres';

    /** the column name for the suivi_entreprise field */
    const SUIVI_ENTREPRISE = 'HabilitationProfil.suivi_entreprise';

    /** the column name for the envoi_boamp field */
    const ENVOI_BOAMP = 'HabilitationProfil.envoi_boamp';

    /** the column name for the acces_classement_lot field */
    const ACCES_CLASSEMENT_LOT = 'HabilitationProfil.acces_classement_lot';

    /** the column name for the connecteur_sis field */
    const CONNECTEUR_SIS = 'HabilitationProfil.connecteur_sis';

    /** the column name for the connecteur_marco field */
    const CONNECTEUR_MARCO = 'HabilitationProfil.connecteur_marco';

    /** the column name for the repondre_aux_questions field */
    const REPONDRE_AUX_QUESTIONS = 'HabilitationProfil.repondre_aux_questions';

    /** the column name for the appel_projet_formation field */
    const APPEL_PROJET_FORMATION = 'HabilitationProfil.appel_projet_formation';

    /** the column name for the utiliser_client_CAO field */
    const UTILISER_CLIENT_CAO = 'HabilitationProfil.utiliser_client_CAO';

    /** the column name for the notification_boamp field */
    const NOTIFICATION_BOAMP = 'HabilitationProfil.notification_boamp';

    /** the column name for the administrer_compte field */
    const ADMINISTRER_COMPTE = 'HabilitationProfil.administrer_compte';

    /** the column name for the gestion_mapa field */
    const GESTION_MAPA = 'HabilitationProfil.gestion_mapa';

    /** the column name for the gestion_type_validation field */
    const GESTION_TYPE_VALIDATION = 'HabilitationProfil.gestion_type_validation';

    /** the column name for the approuver_consultation field */
    const APPROUVER_CONSULTATION = 'HabilitationProfil.approuver_consultation';

    /** the column name for the administrer_procedure field */
    const ADMINISTRER_PROCEDURE = 'HabilitationProfil.administrer_procedure';

    /** the column name for the restreindre_creation field */
    const RESTREINDRE_CREATION = 'HabilitationProfil.restreindre_creation';

    /** the column name for the creer_liste_marches field */
    const CREER_LISTE_MARCHES = 'HabilitationProfil.creer_liste_marches';

    /** the column name for the gestion_commissions field */
    const GESTION_COMMISSIONS = 'HabilitationProfil.gestion_commissions';

    /** the column name for the suivi_seul_consultation field */
    const SUIVI_SEUL_CONSULTATION = 'HabilitationProfil.suivi_seul_consultation';

    /** the column name for the attribution_marche field */
    const ATTRIBUTION_MARCHE = 'HabilitationProfil.attribution_marche';

    /** the column name for the fiche_recensement field */
    const FICHE_RECENSEMENT = 'HabilitationProfil.fiche_recensement';

    /** the column name for the declarer_infructueux field */
    const DECLARER_INFRUCTUEUX = 'HabilitationProfil.declarer_infructueux';

    /** the column name for the declarer_sans_suite field */
    const DECLARER_SANS_SUITE = 'HabilitationProfil.declarer_sans_suite';

    /** the column name for the creer_consultation_transverse field */
    const CREER_CONSULTATION_TRANSVERSE = 'HabilitationProfil.creer_consultation_transverse';

    /** the column name for the ouvrir_candidature_en_ligne field */
    const OUVRIR_CANDIDATURE_EN_LIGNE = 'HabilitationProfil.ouvrir_candidature_en_ligne';

    /** the column name for the ouvrir_candidature_a_distance field */
    const OUVRIR_CANDIDATURE_A_DISTANCE = 'HabilitationProfil.ouvrir_candidature_a_distance';

    /** the column name for the refuser_enveloppe field */
    const REFUSER_ENVELOPPE = 'HabilitationProfil.refuser_enveloppe';

    /** the column name for the gerer_admissibilite field */
    const GERER_ADMISSIBILITE = 'HabilitationProfil.gerer_admissibilite';

    /** the column name for the restaurer_enveloppe field */
    const RESTAURER_ENVELOPPE = 'HabilitationProfil.restaurer_enveloppe';

    /** the column name for the ouvrir_offre_en_ligne field */
    const OUVRIR_OFFRE_EN_LIGNE = 'HabilitationProfil.ouvrir_offre_en_ligne';

    /** the column name for the ouvrir_anonymat_en_ligne field */
    const OUVRIR_ANONYMAT_EN_LIGNE = 'HabilitationProfil.ouvrir_anonymat_en_ligne';

    /** the column name for the gestion_compte_boamp field */
    const GESTION_COMPTE_BOAMP = 'HabilitationProfil.gestion_compte_boamp';

    /** the column name for the gestion_agents field */
    const GESTION_AGENTS = 'HabilitationProfil.gestion_agents';

    /** the column name for the gestion_habilitations field */
    const GESTION_HABILITATIONS = 'HabilitationProfil.gestion_habilitations';

    /** the column name for the gerer_mapa_inferieur_montant field */
    const GERER_MAPA_INFERIEUR_MONTANT = 'HabilitationProfil.gerer_mapa_inferieur_montant';

    /** the column name for the gerer_mapa_superieur_montant field */
    const GERER_MAPA_SUPERIEUR_MONTANT = 'HabilitationProfil.gerer_mapa_superieur_montant';

    /** the column name for the modifier_consultation_avant_validation field */
    const MODIFIER_CONSULTATION_AVANT_VALIDATION = 'HabilitationProfil.modifier_consultation_avant_validation';

    /** the column name for the modifier_consultation_apres_validation field */
    const MODIFIER_CONSULTATION_APRES_VALIDATION = 'HabilitationProfil.modifier_consultation_apres_validation';

    /** the column name for the acces_reponses field */
    const ACCES_REPONSES = 'HabilitationProfil.acces_reponses';

    /** the column name for the telechargement_groupe_anticipe_plis_chiffres field */
    const TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES = 'HabilitationProfil.telechargement_groupe_anticipe_plis_chiffres';

    /** the column name for the telechargement_unitaire_plis_chiffres field */
    const TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES = 'HabilitationProfil.telechargement_unitaire_plis_chiffres';

    /** the column name for the ouvrir_offre_a_distance field */
    const OUVRIR_OFFRE_A_DISTANCE = 'HabilitationProfil.ouvrir_offre_a_distance';

    /** the column name for the creer_annonce_information field */
    const CREER_ANNONCE_INFORMATION = 'HabilitationProfil.creer_annonce_information';

    /** the column name for the saisie_marches field */
    const SAISIE_MARCHES = 'HabilitationProfil.saisie_marches';

    /** the column name for the validation_marches field */
    const VALIDATION_MARCHES = 'HabilitationProfil.validation_marches';

    /** the column name for the publication_marches field */
    const PUBLICATION_MARCHES = 'HabilitationProfil.publication_marches';

    /** the column name for the gerer_statistiques_metier field */
    const GERER_STATISTIQUES_METIER = 'HabilitationProfil.gerer_statistiques_metier';

    /** the column name for the gerer_archives field */
    const GERER_ARCHIVES = 'HabilitationProfil.gerer_archives';

    /** the column name for the administrer_procedures_formalisees field */
    const ADMINISTRER_PROCEDURES_FORMALISEES = 'HabilitationProfil.administrer_procedures_formalisees';

    /** the column name for the creer_annonce_attribution field */
    const CREER_ANNONCE_ATTRIBUTION = 'HabilitationProfil.creer_annonce_attribution';

    /** the column name for the acces_registre_retraits_electronique field */
    const ACCES_REGISTRE_RETRAITS_ELECTRONIQUE = 'HabilitationProfil.acces_registre_retraits_electronique';

    /** the column name for the acces_registre_questions_electronique field */
    const ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE = 'HabilitationProfil.acces_registre_questions_electronique';

    /** the column name for the acces_registre_depots_electronique field */
    const ACCES_REGISTRE_DEPOTS_ELECTRONIQUE = 'HabilitationProfil.acces_registre_depots_electronique';

    /** the column name for the validation_simple field */
    const VALIDATION_SIMPLE = 'HabilitationProfil.validation_simple';

    /** the column name for the validation_intermediaire field */
    const VALIDATION_INTERMEDIAIRE = 'HabilitationProfil.validation_intermediaire';

    /** the column name for the validation_finale field */
    const VALIDATION_FINALE = 'HabilitationProfil.validation_finale';

    /** the column name for the creer_suite_consultation field */
    const CREER_SUITE_CONSULTATION = 'HabilitationProfil.creer_suite_consultation';

    /** the column name for the hyper_admin field */
    const HYPER_ADMIN = 'HabilitationProfil.hyper_admin';

    /** the column name for the droit_gestion_services field */
    const DROIT_GESTION_SERVICES = 'HabilitationProfil.droit_gestion_services';

    /** the column name for the suivi_acces field */
    const SUIVI_ACCES = 'HabilitationProfil.suivi_acces';

    /** the column name for the statistiques_site field */
    const STATISTIQUES_SITE = 'HabilitationProfil.statistiques_site';

    /** the column name for the statistiques_QoS field */
    const STATISTIQUES_QOS = 'HabilitationProfil.statistiques_QoS';

    /** the column name for the ouvrir_anonymat_a_distance field */
    const OUVRIR_ANONYMAT_A_DISTANCE = 'HabilitationProfil.ouvrir_anonymat_a_distance';

    /** the column name for the gestion_compte_jal field */
    const GESTION_COMPTE_JAL = 'HabilitationProfil.gestion_compte_jal';

    /** the column name for the gestion_centrale_pub field */
    const GESTION_CENTRALE_PUB = 'HabilitationProfil.gestion_centrale_pub';

    /** the column name for the Gestion_Compte_Groupe_Moniteur field */
    const GESTION_COMPTE_GROUPE_MONITEUR = 'HabilitationProfil.Gestion_Compte_Groupe_Moniteur';

    /** the column name for the ouvrir_offre_technique_en_ligne field */
    const OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE = 'HabilitationProfil.ouvrir_offre_technique_en_ligne';

    /** the column name for the ouvrir_offre_technique_a_distance field */
    const OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE = 'HabilitationProfil.ouvrir_offre_technique_a_distance';

    /** the column name for the activation_compte_entreprise field */
    const ACTIVATION_COMPTE_ENTREPRISE = 'HabilitationProfil.activation_compte_entreprise';

    /** the column name for the importer_enveloppe field */
    const IMPORTER_ENVELOPPE = 'HabilitationProfil.importer_enveloppe';

    /** the column name for the suivi_seul_registre_depots_papier field */
    const SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER = 'HabilitationProfil.suivi_seul_registre_depots_papier';

    /** the column name for the suivi_seul_registre_retraits_papier field */
    const SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER = 'HabilitationProfil.suivi_seul_registre_retraits_papier';

    /** the column name for the suivi_seul_registre_questions_papier field */
    const SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER = 'HabilitationProfil.suivi_seul_registre_questions_papier';

    /** the column name for the suivi_seul_registre_depots_electronique field */
    const SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE = 'HabilitationProfil.suivi_seul_registre_depots_electronique';

    /** the column name for the suivi_seul_registre_retraits_electronique field */
    const SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE = 'HabilitationProfil.suivi_seul_registre_retraits_electronique';

    /** the column name for the suivi_seul_registre_questions_electronique field */
    const SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE = 'HabilitationProfil.suivi_seul_registre_questions_electronique';

    /** the column name for the modifier_consultation_mapa_inferieur_montant_apres_validation field */
    const MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION = 'HabilitationProfil.modifier_consultation_mapa_inferieur_montant_apres_validation';

    /** the column name for the modifier_consultation_mapa_superieur_montant_apres_validation field */
    const MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION = 'HabilitationProfil.modifier_consultation_mapa_superieur_montant_apres_validation';

    /** the column name for the modifier_consultation_procedures_formalisees_apres_validation field */
    const MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION = 'HabilitationProfil.modifier_consultation_procedures_formalisees_apres_validation';

    /** the column name for the gerer_les_entreprises field */
    const GERER_LES_ENTREPRISES = 'HabilitationProfil.gerer_les_entreprises';

    /** the column name for the portee_societes_exclues field */
    const PORTEE_SOCIETES_EXCLUES = 'HabilitationProfil.portee_societes_exclues';

    /** the column name for the portee_societes_exclues_tous_organismes field */
    const PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES = 'HabilitationProfil.portee_societes_exclues_tous_organismes';

    /** the column name for the modifier_societes_exclues field */
    const MODIFIER_SOCIETES_EXCLUES = 'HabilitationProfil.modifier_societes_exclues';

    /** the column name for the supprimer_societes_exclues field */
    const SUPPRIMER_SOCIETES_EXCLUES = 'HabilitationProfil.supprimer_societes_exclues';

    /** the column name for the resultat_analyse field */
    const RESULTAT_ANALYSE = 'HabilitationProfil.resultat_analyse';

    /** the column name for the gerer_adresses_service field */
    const GERER_ADRESSES_SERVICE = 'HabilitationProfil.gerer_adresses_service';

    /** the column name for the gerer_mon_service field */
    const GERER_MON_SERVICE = 'HabilitationProfil.gerer_mon_service';

    /** the column name for the download_archives field */
    const DOWNLOAD_ARCHIVES = 'HabilitationProfil.download_archives';

    /** the column name for the creer_annonce_extrait_pv field */
    const CREER_ANNONCE_EXTRAIT_PV = 'HabilitationProfil.creer_annonce_extrait_pv';

    /** the column name for the creer_annonce_rapport_achevement field */
    const CREER_ANNONCE_RAPPORT_ACHEVEMENT = 'HabilitationProfil.creer_annonce_rapport_achevement';

    /** the column name for the gestion_certificats_agent field */
    const GESTION_CERTIFICATS_AGENT = 'HabilitationProfil.gestion_certificats_agent';

    /** the column name for the creer_avis_programme_previsionnel field */
    const CREER_AVIS_PROGRAMME_PREVISIONNEL = 'HabilitationProfil.creer_avis_programme_previsionnel';

    /** the column name for the annuler_consultation field */
    const ANNULER_CONSULTATION = 'HabilitationProfil.annuler_consultation';

    /** the column name for the envoyer_publicite field */
    const ENVOYER_PUBLICITE = 'HabilitationProfil.envoyer_publicite';

    /** the column name for the liste_marches_notifies field */
    const LISTE_MARCHES_NOTIFIES = 'HabilitationProfil.liste_marches_notifies';

    /** the column name for the suivre_message field */
    const SUIVRE_MESSAGE = 'HabilitationProfil.suivre_message';

    /** the column name for the envoyer_message field */
    const ENVOYER_MESSAGE = 'HabilitationProfil.envoyer_message';

    /** the column name for the suivi_flux_chorus_transversal field */
    const SUIVI_FLUX_CHORUS_TRANSVERSAL = 'HabilitationProfil.suivi_flux_chorus_transversal';

    /** the column name for the gestion_mandataire field */
    const GESTION_MANDATAIRE = 'HabilitationProfil.gestion_mandataire';

    /** the column name for the gerer_newsletter field */
    const GERER_NEWSLETTER = 'HabilitationProfil.gerer_newsletter';

    /** the column name for the gestion_modeles_formulaire field */
    const GESTION_MODELES_FORMULAIRE = 'HabilitationProfil.gestion_modeles_formulaire';

    /** the column name for the gestion_adresses_facturation_jal field */
    const GESTION_ADRESSES_FACTURATION_JAL = 'HabilitationProfil.gestion_adresses_facturation_jal';

    /** the column name for the administrer_adresses_facturation_jal field */
    const ADMINISTRER_ADRESSES_FACTURATION_JAL = 'HabilitationProfil.administrer_adresses_facturation_jal';

    /** the column name for the redaction_documents_redac field */
    const REDACTION_DOCUMENTS_REDAC = 'HabilitationProfil.redaction_documents_redac';

    /** the column name for the validation_documents_redac field */
    const VALIDATION_DOCUMENTS_REDAC = 'HabilitationProfil.validation_documents_redac';

    /** the column name for the gestion_mise_disposition_pieces_marche field */
    const GESTION_MISE_DISPOSITION_PIECES_MARCHE = 'HabilitationProfil.gestion_mise_disposition_pieces_marche';

    /** the column name for the annuaire_acheteur field */
    const ANNUAIRE_ACHETEUR = 'HabilitationProfil.annuaire_acheteur';

    /** the column name for the reprendre_integralement_article field */
    const REPRENDRE_INTEGRALEMENT_ARTICLE = 'HabilitationProfil.reprendre_integralement_article';

    /** the column name for the administrer_clauses field */
    const ADMINISTRER_CLAUSES = 'HabilitationProfil.administrer_clauses';

    /** the column name for the valider_clauses field */
    const VALIDER_CLAUSES = 'HabilitationProfil.valider_clauses';

    /** the column name for the administrer_canevas field */
    const ADMINISTRER_CANEVAS = 'HabilitationProfil.administrer_canevas';

    /** the column name for the valider_canevas field */
    const VALIDER_CANEVAS = 'HabilitationProfil.valider_canevas';

    /** the column name for the administrer_clauses_entite_achats field */
    const ADMINISTRER_CLAUSES_ENTITE_ACHATS = 'HabilitationProfil.administrer_clauses_entite_achats';

    /** the column name for the generer_pieces_format_odt field */
    const GENERER_PIECES_FORMAT_ODT = 'HabilitationProfil.generer_pieces_format_odt';

    /** the column name for the publier_version_clausier_editeur field */
    const PUBLIER_VERSION_CLAUSIER_EDITEUR = 'HabilitationProfil.publier_version_clausier_editeur';

    /** the column name for the administrer_clauses_editeur field */
    const ADMINISTRER_CLAUSES_EDITEUR = 'HabilitationProfil.administrer_clauses_editeur';

    /** the column name for the valider_clauses_editeur field */
    const VALIDER_CLAUSES_EDITEUR = 'HabilitationProfil.valider_clauses_editeur';

    /** the column name for the administrer_canevas_editeur field */
    const ADMINISTRER_CANEVAS_EDITEUR = 'HabilitationProfil.administrer_canevas_editeur';

    /** the column name for the valider_canevas_editeur field */
    const VALIDER_CANEVAS_EDITEUR = 'HabilitationProfil.valider_canevas_editeur';

    /** the column name for the decision_suivi_seul field */
    const DECISION_SUIVI_SEUL = 'HabilitationProfil.decision_suivi_seul';

    /** the column name for the ouvrir_candidature_hors_ligne field */
    const OUVRIR_CANDIDATURE_HORS_LIGNE = 'HabilitationProfil.ouvrir_candidature_hors_ligne';

    /** the column name for the ouvrir_offre_hors_ligne field */
    const OUVRIR_OFFRE_HORS_LIGNE = 'HabilitationProfil.ouvrir_offre_hors_ligne';

    /** the column name for the ouvrir_offre_technique_hors_ligne field */
    const OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE = 'HabilitationProfil.ouvrir_offre_technique_hors_ligne';

    /** the column name for the ouvrir_anonymat_hors_ligne field */
    const OUVRIR_ANONYMAT_HORS_LIGNE = 'HabilitationProfil.ouvrir_anonymat_hors_ligne';

    /** the column name for the espace_collaboratif_gestionnaire field */
    const ESPACE_COLLABORATIF_GESTIONNAIRE = 'HabilitationProfil.espace_collaboratif_gestionnaire';

    /** the column name for the espace_collaboratif_contributeur field */
    const ESPACE_COLLABORATIF_CONTRIBUTEUR = 'HabilitationProfil.espace_collaboratif_contributeur';

    /** the column name for the gerer_organismes field */
    const GERER_ORGANISMES = 'HabilitationProfil.gerer_organismes';

    /** the column name for the gerer_associations_agents field */
    const GERER_ASSOCIATIONS_AGENTS = 'HabilitationProfil.gerer_associations_agents';

    /** the column name for the module_redaction_uniquement field */
    const MODULE_REDACTION_UNIQUEMENT = 'HabilitationProfil.module_redaction_uniquement';

    /** the column name for the historique_navigation_inscrits field */
    const HISTORIQUE_NAVIGATION_INSCRITS = 'HabilitationProfil.historique_navigation_inscrits';

    /** the column name for the telecharger_accords_cadres field */
    const TELECHARGER_ACCORDS_CADRES = 'HabilitationProfil.telecharger_accords_cadres';

    /** the column name for the creer_annonce_decision_resiliation field */
    const CREER_ANNONCE_DECISION_RESILIATION = 'HabilitationProfil.creer_annonce_decision_resiliation';

    /** the column name for the creer_annonce_synthese_rapport_audit field */
    const CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT = 'HabilitationProfil.creer_annonce_synthese_rapport_audit';

    /** the column name for the gerer_operations field */
    const GERER_OPERATIONS = 'HabilitationProfil.gerer_operations';

    /** the column name for the telecharger_siret_acheteur field */
    const TELECHARGER_SIRET_ACHETEUR = 'HabilitationProfil.telecharger_siret_acheteur';

    /** the column name for the gerer_reouvertures_modification field */
    const GERER_REOUVERTURES_MODIFICATION = 'HabilitationProfil.gerer_reouvertures_modification';

    /** the column name for the acceder_tous_telechargements field */
    const ACCEDER_TOUS_TELECHARGEMENTS = 'HabilitationProfil.acceder_tous_telechargements';

    /** the column name for the creer_contrat field */
    const CREER_CONTRAT = 'HabilitationProfil.creer_contrat';

    /** the column name for the modifier_contrat field */
    const MODIFIER_CONTRAT = 'HabilitationProfil.modifier_contrat';

    /** the column name for the consulter_contrat field */
    const CONSULTER_CONTRAT = 'HabilitationProfil.consulter_contrat';

    /** the column name for the gerer_newsletter_redac field */
    const GERER_NEWSLETTER_REDAC = 'HabilitationProfil.gerer_newsletter_redac';

    /** the column name for the profil_rma field */
    const PROFIL_RMA = 'HabilitationProfil.profil_rma';

    /** the column name for the affectation_vision_rma field */
    const AFFECTATION_VISION_RMA = 'HabilitationProfil.affectation_vision_rma';

    /** the column name for the gerer_gabarit_editeur field */
    const GERER_GABARIT_EDITEUR = 'HabilitationProfil.gerer_gabarit_editeur';

    /** the column name for the gerer_gabarit field */
    const GERER_GABARIT = 'HabilitationProfil.gerer_gabarit';

    /** the column name for the gerer_gabarit_entite_achats field */
    const GERER_GABARIT_ENTITE_ACHATS = 'HabilitationProfil.gerer_gabarit_entite_achats';

    /** the column name for the gerer_gabarit_agent field */
    const GERER_GABARIT_AGENT = 'HabilitationProfil.gerer_gabarit_agent';

    /** the column name for the gerer_messages_accueil field */
    const GERER_MESSAGES_ACCUEIL = 'HabilitationProfil.gerer_messages_accueil';

    /** the column name for the gerer_OA_GA field */
    const GERER_OA_GA = 'HabilitationProfil.gerer_OA_GA';

    /** the column name for the deplacer_service field */
    const DEPLACER_SERVICE = 'HabilitationProfil.deplacer_service';

    /** the column name for the activer_version_clausier field */
    const ACTIVER_VERSION_CLAUSIER = 'HabilitationProfil.activer_version_clausier';

    /** the column name for the exec_voir_contrats_ea field */
    const EXEC_VOIR_CONTRATS_EA = 'HabilitationProfil.exec_voir_contrats_ea';

    /** the column name for the exec_voir_contrats_ea_dependantes field */
    const EXEC_VOIR_CONTRATS_EA_DEPENDANTES = 'HabilitationProfil.exec_voir_contrats_ea_dependantes';

    /** the column name for the exec_voir_contrats_organisme field */
    const EXEC_VOIR_CONTRATS_ORGANISME = 'HabilitationProfil.exec_voir_contrats_organisme';

    /** the column name for the acces_ws field */
    const ACCES_WS = 'HabilitationProfil.acces_ws';

    /** the column name for the acces_echange_documentaire field */
    const ACCES_ECHANGE_DOCUMENTAIRE = 'HabilitationProfil.acces_echange_documentaire';

    /** the column name for the espace_documentaire_consultation field */
    const ESPACE_DOCUMENTAIRE_CONSULTATION = 'HabilitationProfil.espace_documentaire_consultation';

    /** the column name for the administrer_organisme field */
    const ADMINISTRER_ORGANISME = 'HabilitationProfil.administrer_organisme';

    /** the column name for the exec_modification_contrat field */
    const EXEC_MODIFICATION_CONTRAT = 'HabilitationProfil.exec_modification_contrat';

    /** the column name for the besoin_unitaire_consultation field */
    const BESOIN_UNITAIRE_CONSULTATION = 'HabilitationProfil.besoin_unitaire_consultation';

    /** the column name for the besoin_unitaire_creation_modification field */
    const BESOIN_UNITAIRE_CREATION_MODIFICATION = 'HabilitationProfil.besoin_unitaire_creation_modification';

    /** the column name for the demande_achat_consultation field */
    const DEMANDE_ACHAT_CONSULTATION = 'HabilitationProfil.demande_achat_consultation';

    /** the column name for the demande_achat_creation_modification field */
    const DEMANDE_ACHAT_CREATION_MODIFICATION = 'HabilitationProfil.demande_achat_creation_modification';

    /** the column name for the projet_achat_consultation field */
    const PROJET_ACHAT_CONSULTATION = 'HabilitationProfil.projet_achat_consultation';

    /** the column name for the projet_achat_creation_modification field */
    const PROJET_ACHAT_CREATION_MODIFICATION = 'HabilitationProfil.projet_achat_creation_modification';

    /** the column name for the validation_opportunite field */
    const VALIDATION_OPPORTUNITE = 'HabilitationProfil.validation_opportunite';

    /** the column name for the validation_achat field */
    const VALIDATION_ACHAT = 'HabilitationProfil.validation_achat';

    /** the column name for the validation_budget field */
    const VALIDATION_BUDGET = 'HabilitationProfil.validation_budget';

    /** the column name for the strategie_achat_gestion field */
    const STRATEGIE_ACHAT_GESTION = 'HabilitationProfil.strategie_achat_gestion';

    /** the column name for the recensement_programmation_administration field */
    const RECENSEMENT_PROGRAMMATION_ADMINISTRATION = 'HabilitationProfil.recensement_programmation_administration';

    /** the column name for the gestion_envol field */
    const GESTION_ENVOL = 'HabilitationProfil.gestion_envol';

    /** the column name for the valider_projet_achat field */
    const VALIDER_PROJET_ACHAT = 'HabilitationProfil.valider_projet_achat';

    /** the column name for the gestion_spaser_consultations field */
    const GESTION_SPASER_CONSULTATIONS = 'HabilitationProfil.gestion_spaser_consultations';

    /** the column name for the gestion_validation_eco field */
    const GESTION_VALIDATION_ECO = 'HabilitationProfil.gestion_validation_eco';

    /** the column name for the gestion_validation_sip field */
    const GESTION_VALIDATION_SIP = 'HabilitationProfil.gestion_validation_sip';

    /** the column name for the rattachement_service field */
    const RATTACHEMENT_SERVICE = 'HabilitationProfil.rattachement_service';

    /** the column name for the duplication_consultations field */
    const DUPLICATION_CONSULTATIONS = 'HabilitationProfil.duplication_consultations';

    /** the column name for the projet_achat_lancement_sourcing field */
    const PROJET_ACHAT_LANCEMENT_SOURCING = 'HabilitationProfil.projet_achat_lancement_sourcing';

    /** the column name for the projet_achat_invalidation field */
    const PROJET_ACHAT_INVALIDATION = 'HabilitationProfil.projet_achat_invalidation';

    /** the column name for the projet_achat_annulation field */
    const PROJET_ACHAT_ANNULATION = 'HabilitationProfil.projet_achat_annulation';

    /** the column name for the lancement_procedure field */
    const LANCEMENT_PROCEDURE = 'HabilitationProfil.lancement_procedure';

    /** the column name for the recensement_invalider_projet_achat field */
    const RECENSEMENT_INVALIDER_PROJET_ACHAT = 'HabilitationProfil.recensement_invalider_projet_achat';

    /** the column name for the recensement_annuler_projet_achat field */
    const RECENSEMENT_ANNULER_PROJET_ACHAT = 'HabilitationProfil.recensement_annuler_projet_achat';

    /** the column name for the administration_documents_modeles field */
    const ADMINISTRATION_DOCUMENTS_MODELES = 'HabilitationProfil.administration_documents_modeles';

    /** the column name for the supprimer_contrat field */
    const SUPPRIMER_CONTRAT = 'HabilitationProfil.supprimer_contrat';

    /** The enumerated values for the gestion_agent_pole field */
    const GESTION_AGENT_POLE_0 = '0';
    const GESTION_AGENT_POLE_1 = '1';

    /** The enumerated values for the gestion_fournisseurs_envois_postaux field */
    const GESTION_FOURNISSEURS_ENVOIS_POSTAUX_0 = '0';
    const GESTION_FOURNISSEURS_ENVOIS_POSTAUX_1 = '1';

    /** The enumerated values for the gestion_bi_cles field */
    const GESTION_BI_CLES_0 = '0';
    const GESTION_BI_CLES_1 = '1';

    /** The enumerated values for the creer_consultation field */
    const CREER_CONSULTATION_0 = '0';
    const CREER_CONSULTATION_1 = '1';

    /** The enumerated values for the modifier_consultation field */
    const MODIFIER_CONSULTATION_0 = '0';
    const MODIFIER_CONSULTATION_1 = '1';

    /** The enumerated values for the valider_consultation field */
    const VALIDER_CONSULTATION_0 = '0';
    const VALIDER_CONSULTATION_1 = '1';

    /** The enumerated values for the publier_consultation field */
    const PUBLIER_CONSULTATION_0 = '0';
    const PUBLIER_CONSULTATION_1 = '1';

    /** The enumerated values for the suivre_consultation field */
    const SUIVRE_CONSULTATION_0 = '0';
    const SUIVRE_CONSULTATION_1 = '1';

    /** The enumerated values for the suivre_consultation_pole field */
    const SUIVRE_CONSULTATION_POLE_0 = '0';
    const SUIVRE_CONSULTATION_POLE_1 = '1';

    /** The enumerated values for the invite_permanent_entite_dependante field */
    const INVITE_PERMANENT_ENTITE_DEPENDANTE_0 = '0';
    const INVITE_PERMANENT_ENTITE_DEPENDANTE_1 = '1';

    /** The enumerated values for the invite_permanent_mon_entite field */
    const INVITE_PERMANENT_MON_ENTITE_0 = '0';
    const INVITE_PERMANENT_MON_ENTITE_1 = '1';

    /** The enumerated values for the invite_permanent_transverse field */
    const INVITE_PERMANENT_TRANSVERSE_0 = '0';
    const INVITE_PERMANENT_TRANSVERSE_1 = '1';

    /** The enumerated values for the supprimer_enveloppe field */
    const SUPPRIMER_ENVELOPPE_0 = '0';
    const SUPPRIMER_ENVELOPPE_1 = '1';

    /** The enumerated values for the supprimer_consultation field */
    const SUPPRIMER_CONSULTATION_0 = '0';
    const SUPPRIMER_CONSULTATION_1 = '1';

    /** The enumerated values for the depouiller_candidature field */
    const DEPOUILLER_CANDIDATURE_0 = '0';
    const DEPOUILLER_CANDIDATURE_1 = '1';

    /** The enumerated values for the depouiller_offre field */
    const DEPOUILLER_OFFRE_0 = '0';
    const DEPOUILLER_OFFRE_1 = '1';

    /** The enumerated values for the messagerie_securisee field */
    const MESSAGERIE_SECURISEE_0 = '0';
    const MESSAGERIE_SECURISEE_1 = '1';

    /** The enumerated values for the acces_registre_depots_papier field */
    const ACCES_REGISTRE_DEPOTS_PAPIER_0 = '0';
    const ACCES_REGISTRE_DEPOTS_PAPIER_1 = '1';

    /** The enumerated values for the acces_registre_retraits_papier field */
    const ACCES_REGISTRE_RETRAITS_PAPIER_0 = '0';
    const ACCES_REGISTRE_RETRAITS_PAPIER_1 = '1';

    /** The enumerated values for the acces_registre_questions_papier field */
    const ACCES_REGISTRE_QUESTIONS_PAPIER_0 = '0';
    const ACCES_REGISTRE_QUESTIONS_PAPIER_1 = '1';

    /** The enumerated values for the gerer_encheres field */
    const GERER_ENCHERES_0 = '0';
    const GERER_ENCHERES_1 = '1';

    /** The enumerated values for the suivre_encheres field */
    const SUIVRE_ENCHERES_0 = '0';
    const SUIVRE_ENCHERES_1 = '1';

    /** The enumerated values for the suivi_entreprise field */
    const SUIVI_ENTREPRISE_0 = '0';
    const SUIVI_ENTREPRISE_1 = '1';

    /** The enumerated values for the envoi_boamp field */
    const ENVOI_BOAMP_0 = '0';
    const ENVOI_BOAMP_1 = '1';

    /** The enumerated values for the acces_classement_lot field */
    const ACCES_CLASSEMENT_LOT_0 = '0';
    const ACCES_CLASSEMENT_LOT_1 = '1';

    /** The enumerated values for the connecteur_sis field */
    const CONNECTEUR_SIS_0 = '0';
    const CONNECTEUR_SIS_1 = '1';

    /** The enumerated values for the connecteur_marco field */
    const CONNECTEUR_MARCO_0 = '0';
    const CONNECTEUR_MARCO_1 = '1';

    /** The enumerated values for the repondre_aux_questions field */
    const REPONDRE_AUX_QUESTIONS_0 = '0';
    const REPONDRE_AUX_QUESTIONS_1 = '1';

    /** The enumerated values for the appel_projet_formation field */
    const APPEL_PROJET_FORMATION_0 = '0';
    const APPEL_PROJET_FORMATION_1 = '1';

    /** The enumerated values for the utiliser_client_CAO field */
    const UTILISER_CLIENT_CAO_0 = '0';
    const UTILISER_CLIENT_CAO_1 = '1';

    /** The enumerated values for the notification_boamp field */
    const NOTIFICATION_BOAMP_0 = '0';
    const NOTIFICATION_BOAMP_1 = '1';

    /** The enumerated values for the administrer_compte field */
    const ADMINISTRER_COMPTE_0 = '0';
    const ADMINISTRER_COMPTE_1 = '1';

    /** The enumerated values for the gestion_mapa field */
    const GESTION_MAPA_0 = '0';
    const GESTION_MAPA_1 = '1';

    /** The enumerated values for the gestion_type_validation field */
    const GESTION_TYPE_VALIDATION_0 = '0';
    const GESTION_TYPE_VALIDATION_1 = '1';

    /** The enumerated values for the approuver_consultation field */
    const APPROUVER_CONSULTATION_0 = '0';
    const APPROUVER_CONSULTATION_1 = '1';

    /** The enumerated values for the administrer_procedure field */
    const ADMINISTRER_PROCEDURE_0 = '0';
    const ADMINISTRER_PROCEDURE_1 = '1';

    /** The enumerated values for the restreindre_creation field */
    const RESTREINDRE_CREATION_0 = '0';
    const RESTREINDRE_CREATION_1 = '1';

    /** The enumerated values for the creer_liste_marches field */
    const CREER_LISTE_MARCHES_0 = '0';
    const CREER_LISTE_MARCHES_1 = '1';

    /** The enumerated values for the gestion_commissions field */
    const GESTION_COMMISSIONS_0 = '0';
    const GESTION_COMMISSIONS_1 = '1';

    /** The enumerated values for the suivi_seul_consultation field */
    const SUIVI_SEUL_CONSULTATION_0 = '0';
    const SUIVI_SEUL_CONSULTATION_1 = '1';

    /** The enumerated values for the attribution_marche field */
    const ATTRIBUTION_MARCHE_0 = '0';
    const ATTRIBUTION_MARCHE_1 = '1';

    /** The enumerated values for the fiche_recensement field */
    const FICHE_RECENSEMENT_0 = '0';
    const FICHE_RECENSEMENT_1 = '1';

    /** The enumerated values for the declarer_infructueux field */
    const DECLARER_INFRUCTUEUX_0 = '0';
    const DECLARER_INFRUCTUEUX_1 = '1';

    /** The enumerated values for the declarer_sans_suite field */
    const DECLARER_SANS_SUITE_0 = '0';
    const DECLARER_SANS_SUITE_1 = '1';

    /** The enumerated values for the creer_consultation_transverse field */
    const CREER_CONSULTATION_TRANSVERSE_0 = '0';
    const CREER_CONSULTATION_TRANSVERSE_1 = '1';

    /** The enumerated values for the ouvrir_candidature_en_ligne field */
    const OUVRIR_CANDIDATURE_EN_LIGNE_0 = '0';
    const OUVRIR_CANDIDATURE_EN_LIGNE_1 = '1';

    /** The enumerated values for the ouvrir_candidature_a_distance field */
    const OUVRIR_CANDIDATURE_A_DISTANCE_0 = '0';
    const OUVRIR_CANDIDATURE_A_DISTANCE_1 = '1';

    /** The enumerated values for the refuser_enveloppe field */
    const REFUSER_ENVELOPPE_0 = '0';
    const REFUSER_ENVELOPPE_1 = '1';

    /** The enumerated values for the gerer_admissibilite field */
    const GERER_ADMISSIBILITE_0 = '0';
    const GERER_ADMISSIBILITE_1 = '1';

    /** The enumerated values for the restaurer_enveloppe field */
    const RESTAURER_ENVELOPPE_0 = '0';
    const RESTAURER_ENVELOPPE_1 = '1';

    /** The enumerated values for the ouvrir_offre_en_ligne field */
    const OUVRIR_OFFRE_EN_LIGNE_0 = '0';
    const OUVRIR_OFFRE_EN_LIGNE_1 = '1';

    /** The enumerated values for the ouvrir_anonymat_en_ligne field */
    const OUVRIR_ANONYMAT_EN_LIGNE_0 = '0';
    const OUVRIR_ANONYMAT_EN_LIGNE_1 = '1';

    /** The enumerated values for the gestion_compte_boamp field */
    const GESTION_COMPTE_BOAMP_0 = '0';
    const GESTION_COMPTE_BOAMP_1 = '1';

    /** The enumerated values for the gestion_agents field */
    const GESTION_AGENTS_0 = '0';
    const GESTION_AGENTS_1 = '1';

    /** The enumerated values for the gestion_habilitations field */
    const GESTION_HABILITATIONS_0 = '0';
    const GESTION_HABILITATIONS_1 = '1';

    /** The enumerated values for the gerer_mapa_inferieur_montant field */
    const GERER_MAPA_INFERIEUR_MONTANT_0 = '0';
    const GERER_MAPA_INFERIEUR_MONTANT_1 = '1';

    /** The enumerated values for the gerer_mapa_superieur_montant field */
    const GERER_MAPA_SUPERIEUR_MONTANT_0 = '0';
    const GERER_MAPA_SUPERIEUR_MONTANT_1 = '1';

    /** The enumerated values for the modifier_consultation_avant_validation field */
    const MODIFIER_CONSULTATION_AVANT_VALIDATION_0 = '0';
    const MODIFIER_CONSULTATION_AVANT_VALIDATION_1 = '1';

    /** The enumerated values for the modifier_consultation_apres_validation field */
    const MODIFIER_CONSULTATION_APRES_VALIDATION_0 = '0';
    const MODIFIER_CONSULTATION_APRES_VALIDATION_1 = '1';

    /** The enumerated values for the acces_reponses field */
    const ACCES_REPONSES_0 = '0';
    const ACCES_REPONSES_1 = '1';

    /** The enumerated values for the telechargement_groupe_anticipe_plis_chiffres field */
    const TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES_0 = '0';
    const TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES_1 = '1';

    /** The enumerated values for the telechargement_unitaire_plis_chiffres field */
    const TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES_0 = '0';
    const TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES_1 = '1';

    /** The enumerated values for the ouvrir_offre_a_distance field */
    const OUVRIR_OFFRE_A_DISTANCE_0 = '0';
    const OUVRIR_OFFRE_A_DISTANCE_1 = '1';

    /** The enumerated values for the creer_annonce_information field */
    const CREER_ANNONCE_INFORMATION_0 = '0';
    const CREER_ANNONCE_INFORMATION_1 = '1';

    /** The enumerated values for the saisie_marches field */
    const SAISIE_MARCHES_0 = '0';
    const SAISIE_MARCHES_1 = '1';

    /** The enumerated values for the validation_marches field */
    const VALIDATION_MARCHES_0 = '0';
    const VALIDATION_MARCHES_1 = '1';

    /** The enumerated values for the publication_marches field */
    const PUBLICATION_MARCHES_0 = '0';
    const PUBLICATION_MARCHES_1 = '1';

    /** The enumerated values for the gerer_statistiques_metier field */
    const GERER_STATISTIQUES_METIER_0 = '0';
    const GERER_STATISTIQUES_METIER_1 = '1';

    /** The enumerated values for the gerer_archives field */
    const GERER_ARCHIVES_0 = '0';
    const GERER_ARCHIVES_1 = '1';

    /** The enumerated values for the administrer_procedures_formalisees field */
    const ADMINISTRER_PROCEDURES_FORMALISEES_0 = '0';
    const ADMINISTRER_PROCEDURES_FORMALISEES_1 = '1';

    /** The enumerated values for the creer_annonce_attribution field */
    const CREER_ANNONCE_ATTRIBUTION_0 = '0';
    const CREER_ANNONCE_ATTRIBUTION_1 = '1';

    /** The enumerated values for the acces_registre_retraits_electronique field */
    const ACCES_REGISTRE_RETRAITS_ELECTRONIQUE_0 = '0';
    const ACCES_REGISTRE_RETRAITS_ELECTRONIQUE_1 = '1';

    /** The enumerated values for the acces_registre_questions_electronique field */
    const ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE_0 = '0';
    const ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE_1 = '1';

    /** The enumerated values for the acces_registre_depots_electronique field */
    const ACCES_REGISTRE_DEPOTS_ELECTRONIQUE_0 = '0';
    const ACCES_REGISTRE_DEPOTS_ELECTRONIQUE_1 = '1';

    /** The enumerated values for the validation_simple field */
    const VALIDATION_SIMPLE_0 = '0';
    const VALIDATION_SIMPLE_1 = '1';

    /** The enumerated values for the validation_intermediaire field */
    const VALIDATION_INTERMEDIAIRE_0 = '0';
    const VALIDATION_INTERMEDIAIRE_1 = '1';

    /** The enumerated values for the validation_finale field */
    const VALIDATION_FINALE_0 = '0';
    const VALIDATION_FINALE_1 = '1';

    /** The enumerated values for the creer_suite_consultation field */
    const CREER_SUITE_CONSULTATION_0 = '0';
    const CREER_SUITE_CONSULTATION_1 = '1';

    /** The enumerated values for the hyper_admin field */
    const HYPER_ADMIN_0 = '0';
    const HYPER_ADMIN_1 = '1';

    /** The enumerated values for the droit_gestion_services field */
    const DROIT_GESTION_SERVICES_0 = '0';
    const DROIT_GESTION_SERVICES_1 = '1';

    /** The enumerated values for the suivi_acces field */
    const SUIVI_ACCES_0 = '0';
    const SUIVI_ACCES_1 = '1';

    /** The enumerated values for the statistiques_site field */
    const STATISTIQUES_SITE_0 = '0';
    const STATISTIQUES_SITE_1 = '1';

    /** The enumerated values for the statistiques_QoS field */
    const STATISTIQUES_QOS_0 = '0';
    const STATISTIQUES_QOS_1 = '1';

    /** The enumerated values for the ouvrir_anonymat_a_distance field */
    const OUVRIR_ANONYMAT_A_DISTANCE_0 = '0';
    const OUVRIR_ANONYMAT_A_DISTANCE_1 = '1';

    /** The enumerated values for the gestion_compte_jal field */
    const GESTION_COMPTE_JAL_0 = '0';
    const GESTION_COMPTE_JAL_1 = '1';

    /** The enumerated values for the gestion_centrale_pub field */
    const GESTION_CENTRALE_PUB_0 = '0';
    const GESTION_CENTRALE_PUB_1 = '1';

    /** The enumerated values for the Gestion_Compte_Groupe_Moniteur field */
    const GESTION_COMPTE_GROUPE_MONITEUR_0 = '0';
    const GESTION_COMPTE_GROUPE_MONITEUR_1 = '1';

    /** The enumerated values for the ouvrir_offre_technique_en_ligne field */
    const OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE_0 = '0';
    const OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE_1 = '1';

    /** The enumerated values for the ouvrir_offre_technique_a_distance field */
    const OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE_0 = '0';
    const OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE_1 = '1';

    /** The enumerated values for the activation_compte_entreprise field */
    const ACTIVATION_COMPTE_ENTREPRISE_0 = '0';
    const ACTIVATION_COMPTE_ENTREPRISE_1 = '1';

    /** The enumerated values for the importer_enveloppe field */
    const IMPORTER_ENVELOPPE_0 = '0';
    const IMPORTER_ENVELOPPE_1 = '1';

    /** The enumerated values for the suivi_seul_registre_depots_papier field */
    const SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER_0 = '0';
    const SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER_1 = '1';

    /** The enumerated values for the suivi_seul_registre_retraits_papier field */
    const SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER_0 = '0';
    const SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER_1 = '1';

    /** The enumerated values for the suivi_seul_registre_questions_papier field */
    const SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER_0 = '0';
    const SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER_1 = '1';

    /** The enumerated values for the suivi_seul_registre_depots_electronique field */
    const SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE_0 = '0';
    const SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE_1 = '1';

    /** The enumerated values for the suivi_seul_registre_retraits_electronique field */
    const SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE_0 = '0';
    const SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE_1 = '1';

    /** The enumerated values for the suivi_seul_registre_questions_electronique field */
    const SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE_0 = '0';
    const SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE_1 = '1';

    /** The enumerated values for the modifier_consultation_mapa_inferieur_montant_apres_validation field */
    const MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION_0 = '0';
    const MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION_1 = '1';

    /** The enumerated values for the modifier_consultation_mapa_superieur_montant_apres_validation field */
    const MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION_0 = '0';
    const MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION_1 = '1';

    /** The enumerated values for the modifier_consultation_procedures_formalisees_apres_validation field */
    const MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION_0 = '0';
    const MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION_1 = '1';

    /** The enumerated values for the gerer_les_entreprises field */
    const GERER_LES_ENTREPRISES_0 = '0';
    const GERER_LES_ENTREPRISES_1 = '1';

    /** The enumerated values for the portee_societes_exclues field */
    const PORTEE_SOCIETES_EXCLUES_0 = '0';
    const PORTEE_SOCIETES_EXCLUES_1 = '1';

    /** The enumerated values for the portee_societes_exclues_tous_organismes field */
    const PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES_0 = '0';
    const PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES_1 = '1';

    /** The enumerated values for the modifier_societes_exclues field */
    const MODIFIER_SOCIETES_EXCLUES_0 = '0';
    const MODIFIER_SOCIETES_EXCLUES_1 = '1';

    /** The enumerated values for the supprimer_societes_exclues field */
    const SUPPRIMER_SOCIETES_EXCLUES_0 = '0';
    const SUPPRIMER_SOCIETES_EXCLUES_1 = '1';

    /** The enumerated values for the resultat_analyse field */
    const RESULTAT_ANALYSE_0 = '0';
    const RESULTAT_ANALYSE_1 = '1';

    /** The enumerated values for the gerer_adresses_service field */
    const GERER_ADRESSES_SERVICE_0 = '0';
    const GERER_ADRESSES_SERVICE_1 = '1';

    /** The enumerated values for the gerer_mon_service field */
    const GERER_MON_SERVICE_0 = '0';
    const GERER_MON_SERVICE_1 = '1';

    /** The enumerated values for the download_archives field */
    const DOWNLOAD_ARCHIVES_0 = '0';
    const DOWNLOAD_ARCHIVES_1 = '1';

    /** The enumerated values for the creer_annonce_extrait_pv field */
    const CREER_ANNONCE_EXTRAIT_PV_0 = '0';
    const CREER_ANNONCE_EXTRAIT_PV_1 = '1';

    /** The enumerated values for the creer_annonce_rapport_achevement field */
    const CREER_ANNONCE_RAPPORT_ACHEVEMENT_0 = '0';
    const CREER_ANNONCE_RAPPORT_ACHEVEMENT_1 = '1';

    /** The enumerated values for the gestion_certificats_agent field */
    const GESTION_CERTIFICATS_AGENT_0 = '0';
    const GESTION_CERTIFICATS_AGENT_1 = '1';

    /** The enumerated values for the creer_avis_programme_previsionnel field */
    const CREER_AVIS_PROGRAMME_PREVISIONNEL_0 = '0';
    const CREER_AVIS_PROGRAMME_PREVISIONNEL_1 = '1';

    /** The enumerated values for the annuler_consultation field */
    const ANNULER_CONSULTATION_0 = '0';
    const ANNULER_CONSULTATION_1 = '1';

    /** The enumerated values for the envoyer_publicite field */
    const ENVOYER_PUBLICITE_0 = '0';
    const ENVOYER_PUBLICITE_1 = '1';

    /** The enumerated values for the liste_marches_notifies field */
    const LISTE_MARCHES_NOTIFIES_0 = '0';
    const LISTE_MARCHES_NOTIFIES_1 = '1';

    /** The enumerated values for the suivre_message field */
    const SUIVRE_MESSAGE_0 = '0';
    const SUIVRE_MESSAGE_1 = '1';

    /** The enumerated values for the envoyer_message field */
    const ENVOYER_MESSAGE_0 = '0';
    const ENVOYER_MESSAGE_1 = '1';

    /** The enumerated values for the suivi_flux_chorus_transversal field */
    const SUIVI_FLUX_CHORUS_TRANSVERSAL_0 = '0';
    const SUIVI_FLUX_CHORUS_TRANSVERSAL_1 = '1';

    /** The enumerated values for the gestion_mandataire field */
    const GESTION_MANDATAIRE_0 = '0';
    const GESTION_MANDATAIRE_1 = '1';

    /** The enumerated values for the gerer_newsletter field */
    const GERER_NEWSLETTER_0 = '0';
    const GERER_NEWSLETTER_1 = '1';

    /** The enumerated values for the gestion_modeles_formulaire field */
    const GESTION_MODELES_FORMULAIRE_0 = '0';
    const GESTION_MODELES_FORMULAIRE_1 = '1';

    /** The enumerated values for the gestion_adresses_facturation_jal field */
    const GESTION_ADRESSES_FACTURATION_JAL_0 = '0';
    const GESTION_ADRESSES_FACTURATION_JAL_1 = '1';

    /** The enumerated values for the administrer_adresses_facturation_jal field */
    const ADMINISTRER_ADRESSES_FACTURATION_JAL_0 = '0';
    const ADMINISTRER_ADRESSES_FACTURATION_JAL_1 = '1';

    /** The enumerated values for the redaction_documents_redac field */
    const REDACTION_DOCUMENTS_REDAC_0 = '0';
    const REDACTION_DOCUMENTS_REDAC_1 = '1';

    /** The enumerated values for the validation_documents_redac field */
    const VALIDATION_DOCUMENTS_REDAC_0 = '0';
    const VALIDATION_DOCUMENTS_REDAC_1 = '1';

    /** The enumerated values for the gestion_mise_disposition_pieces_marche field */
    const GESTION_MISE_DISPOSITION_PIECES_MARCHE_0 = '0';
    const GESTION_MISE_DISPOSITION_PIECES_MARCHE_1 = '1';

    /** The enumerated values for the annuaire_acheteur field */
    const ANNUAIRE_ACHETEUR_0 = '0';
    const ANNUAIRE_ACHETEUR_1 = '1';

    /** The enumerated values for the reprendre_integralement_article field */
    const REPRENDRE_INTEGRALEMENT_ARTICLE_0 = '0';
    const REPRENDRE_INTEGRALEMENT_ARTICLE_1 = '1';

    /** The enumerated values for the administrer_clauses field */
    const ADMINISTRER_CLAUSES_0 = '0';
    const ADMINISTRER_CLAUSES_1 = '1';

    /** The enumerated values for the valider_clauses field */
    const VALIDER_CLAUSES_0 = '0';
    const VALIDER_CLAUSES_1 = '1';

    /** The enumerated values for the administrer_canevas field */
    const ADMINISTRER_CANEVAS_0 = '0';
    const ADMINISTRER_CANEVAS_1 = '1';

    /** The enumerated values for the valider_canevas field */
    const VALIDER_CANEVAS_0 = '0';
    const VALIDER_CANEVAS_1 = '1';

    /** The enumerated values for the administrer_clauses_entite_achats field */
    const ADMINISTRER_CLAUSES_ENTITE_ACHATS_0 = '0';
    const ADMINISTRER_CLAUSES_ENTITE_ACHATS_1 = '1';

    /** The enumerated values for the generer_pieces_format_odt field */
    const GENERER_PIECES_FORMAT_ODT_0 = '0';
    const GENERER_PIECES_FORMAT_ODT_1 = '1';

    /** The enumerated values for the publier_version_clausier_editeur field */
    const PUBLIER_VERSION_CLAUSIER_EDITEUR_0 = '0';
    const PUBLIER_VERSION_CLAUSIER_EDITEUR_1 = '1';

    /** The enumerated values for the administrer_clauses_editeur field */
    const ADMINISTRER_CLAUSES_EDITEUR_0 = '0';
    const ADMINISTRER_CLAUSES_EDITEUR_1 = '1';

    /** The enumerated values for the valider_clauses_editeur field */
    const VALIDER_CLAUSES_EDITEUR_0 = '0';
    const VALIDER_CLAUSES_EDITEUR_1 = '1';

    /** The enumerated values for the administrer_canevas_editeur field */
    const ADMINISTRER_CANEVAS_EDITEUR_0 = '0';
    const ADMINISTRER_CANEVAS_EDITEUR_1 = '1';

    /** The enumerated values for the valider_canevas_editeur field */
    const VALIDER_CANEVAS_EDITEUR_0 = '0';
    const VALIDER_CANEVAS_EDITEUR_1 = '1';

    /** The enumerated values for the decision_suivi_seul field */
    const DECISION_SUIVI_SEUL_0 = '0';
    const DECISION_SUIVI_SEUL_1 = '1';

    /** The enumerated values for the ouvrir_candidature_hors_ligne field */
    const OUVRIR_CANDIDATURE_HORS_LIGNE_0 = '0';
    const OUVRIR_CANDIDATURE_HORS_LIGNE_1 = '1';

    /** The enumerated values for the ouvrir_offre_hors_ligne field */
    const OUVRIR_OFFRE_HORS_LIGNE_0 = '0';
    const OUVRIR_OFFRE_HORS_LIGNE_1 = '1';

    /** The enumerated values for the ouvrir_offre_technique_hors_ligne field */
    const OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE_0 = '0';
    const OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE_1 = '1';

    /** The enumerated values for the ouvrir_anonymat_hors_ligne field */
    const OUVRIR_ANONYMAT_HORS_LIGNE_0 = '0';
    const OUVRIR_ANONYMAT_HORS_LIGNE_1 = '1';

    /** The enumerated values for the espace_collaboratif_gestionnaire field */
    const ESPACE_COLLABORATIF_GESTIONNAIRE_0 = '0';
    const ESPACE_COLLABORATIF_GESTIONNAIRE_1 = '1';

    /** The enumerated values for the espace_collaboratif_contributeur field */
    const ESPACE_COLLABORATIF_CONTRIBUTEUR_0 = '0';
    const ESPACE_COLLABORATIF_CONTRIBUTEUR_1 = '1';

    /** The enumerated values for the gerer_organismes field */
    const GERER_ORGANISMES_0 = '0';
    const GERER_ORGANISMES_1 = '1';

    /** The enumerated values for the gerer_associations_agents field */
    const GERER_ASSOCIATIONS_AGENTS_0 = '0';
    const GERER_ASSOCIATIONS_AGENTS_1 = '1';

    /** The enumerated values for the module_redaction_uniquement field */
    const MODULE_REDACTION_UNIQUEMENT_0 = '0';
    const MODULE_REDACTION_UNIQUEMENT_1 = '1';

    /** The enumerated values for the historique_navigation_inscrits field */
    const HISTORIQUE_NAVIGATION_INSCRITS_0 = '0';
    const HISTORIQUE_NAVIGATION_INSCRITS_1 = '1';

    /** The enumerated values for the telecharger_accords_cadres field */
    const TELECHARGER_ACCORDS_CADRES_0 = '0';
    const TELECHARGER_ACCORDS_CADRES_1 = '1';

    /** The enumerated values for the creer_annonce_decision_resiliation field */
    const CREER_ANNONCE_DECISION_RESILIATION_0 = '0';
    const CREER_ANNONCE_DECISION_RESILIATION_1 = '1';

    /** The enumerated values for the creer_annonce_synthese_rapport_audit field */
    const CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT_0 = '0';
    const CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT_1 = '1';

    /** The enumerated values for the gerer_operations field */
    const GERER_OPERATIONS_0 = '0';
    const GERER_OPERATIONS_1 = '1';

    /** The enumerated values for the telecharger_siret_acheteur field */
    const TELECHARGER_SIRET_ACHETEUR_0 = '0';
    const TELECHARGER_SIRET_ACHETEUR_1 = '1';

    /** The enumerated values for the gerer_reouvertures_modification field */
    const GERER_REOUVERTURES_MODIFICATION_0 = '0';
    const GERER_REOUVERTURES_MODIFICATION_1 = '1';

    /** The enumerated values for the acceder_tous_telechargements field */
    const ACCEDER_TOUS_TELECHARGEMENTS_0 = '0';
    const ACCEDER_TOUS_TELECHARGEMENTS_1 = '1';

    /** The enumerated values for the creer_contrat field */
    const CREER_CONTRAT_0 = '0';
    const CREER_CONTRAT_1 = '1';

    /** The enumerated values for the modifier_contrat field */
    const MODIFIER_CONTRAT_0 = '0';
    const MODIFIER_CONTRAT_1 = '1';

    /** The enumerated values for the consulter_contrat field */
    const CONSULTER_CONTRAT_0 = '0';
    const CONSULTER_CONTRAT_1 = '1';

    /** The enumerated values for the gerer_newsletter_redac field */
    const GERER_NEWSLETTER_REDAC_0 = '0';
    const GERER_NEWSLETTER_REDAC_1 = '1';

    /** The enumerated values for the profil_rma field */
    const PROFIL_RMA_0 = '0';
    const PROFIL_RMA_1 = '1';

    /** The enumerated values for the affectation_vision_rma field */
    const AFFECTATION_VISION_RMA_0 = '0';
    const AFFECTATION_VISION_RMA_1 = '1';

    /** The enumerated values for the gerer_gabarit_editeur field */
    const GERER_GABARIT_EDITEUR_0 = '0';
    const GERER_GABARIT_EDITEUR_1 = '1';

    /** The enumerated values for the gerer_gabarit field */
    const GERER_GABARIT_0 = '0';
    const GERER_GABARIT_1 = '1';

    /** The enumerated values for the gerer_gabarit_entite_achats field */
    const GERER_GABARIT_ENTITE_ACHATS_0 = '0';
    const GERER_GABARIT_ENTITE_ACHATS_1 = '1';

    /** The enumerated values for the gerer_gabarit_agent field */
    const GERER_GABARIT_AGENT_0 = '0';
    const GERER_GABARIT_AGENT_1 = '1';

    /** The enumerated values for the gerer_messages_accueil field */
    const GERER_MESSAGES_ACCUEIL_0 = '0';
    const GERER_MESSAGES_ACCUEIL_1 = '1';

    /** The enumerated values for the gerer_OA_GA field */
    const GERER_OA_GA_0 = '0';
    const GERER_OA_GA_1 = '1';

    /** The enumerated values for the deplacer_service field */
    const DEPLACER_SERVICE_0 = '0';
    const DEPLACER_SERVICE_1 = '1';

    /** The enumerated values for the activer_version_clausier field */
    const ACTIVER_VERSION_CLAUSIER_0 = '0';
    const ACTIVER_VERSION_CLAUSIER_1 = '1';

    /** The enumerated values for the exec_voir_contrats_ea field */
    const EXEC_VOIR_CONTRATS_EA_0 = '0';
    const EXEC_VOIR_CONTRATS_EA_1 = '1';

    /** The enumerated values for the exec_voir_contrats_ea_dependantes field */
    const EXEC_VOIR_CONTRATS_EA_DEPENDANTES_0 = '0';
    const EXEC_VOIR_CONTRATS_EA_DEPENDANTES_1 = '1';

    /** The enumerated values for the exec_voir_contrats_organisme field */
    const EXEC_VOIR_CONTRATS_ORGANISME_0 = '0';
    const EXEC_VOIR_CONTRATS_ORGANISME_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonHabilitationProfil objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonHabilitationProfil[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonHabilitationProfilPeer::$fieldNames[CommonHabilitationProfilPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'Libelle', 'GestionAgentPole', 'GestionFournisseursEnvoisPostaux', 'GestionBiCles', 'CreerConsultation', 'ModifierConsultation', 'ValiderConsultation', 'PublierConsultation', 'SuivreConsultation', 'SuivreConsultationPole', 'InvitePermanentEntiteDependante', 'InvitePermanentMonEntite', 'InvitePermanentTransverse', 'SupprimerEnveloppe', 'SupprimerConsultation', 'DepouillerCandidature', 'DepouillerOffre', 'MessagerieSecurisee', 'AccesRegistreDepotsPapier', 'AccesRegistreRetraitsPapier', 'AccesRegistreQuestionsPapier', 'GererEncheres', 'SuivreEncheres', 'SuiviEntreprise', 'EnvoiBoamp', 'AccesClassementLot', 'ConnecteurSis', 'ConnecteurMarco', 'RepondreAuxQuestions', 'AppelProjetFormation', 'UtiliserClientCao', 'NotificationBoamp', 'AdministrerCompte', 'GestionMapa', 'GestionTypeValidation', 'ApprouverConsultation', 'AdministrerProcedure', 'RestreindreCreation', 'CreerListeMarches', 'GestionCommissions', 'SuiviSeulConsultation', 'AttributionMarche', 'FicheRecensement', 'DeclarerInfructueux', 'DeclarerSansSuite', 'CreerConsultationTransverse', 'OuvrirCandidatureEnLigne', 'OuvrirCandidatureADistance', 'RefuserEnveloppe', 'GererAdmissibilite', 'RestaurerEnveloppe', 'OuvrirOffreEnLigne', 'OuvrirAnonymatEnLigne', 'GestionCompteBoamp', 'GestionAgents', 'GestionHabilitations', 'GererMapaInferieurMontant', 'GererMapaSuperieurMontant', 'ModifierConsultationAvantValidation', 'ModifierConsultationApresValidation', 'AccesReponses', 'TelechargementGroupeAnticipePlisChiffres', 'TelechargementUnitairePlisChiffres', 'OuvrirOffreADistance', 'CreerAnnonceInformation', 'SaisieMarches', 'ValidationMarches', 'PublicationMarches', 'GererStatistiquesMetier', 'GererArchives', 'AdministrerProceduresFormalisees', 'CreerAnnonceAttribution', 'AccesRegistreRetraitsElectronique', 'AccesRegistreQuestionsElectronique', 'AccesRegistreDepotsElectronique', 'ValidationSimple', 'ValidationIntermediaire', 'ValidationFinale', 'CreerSuiteConsultation', 'HyperAdmin', 'DroitGestionServices', 'SuiviAcces', 'StatistiquesSite', 'StatistiquesQos', 'OuvrirAnonymatADistance', 'GestionCompteJal', 'GestionCentralePub', 'GestionCompteGroupeMoniteur', 'OuvrirOffreTechniqueEnLigne', 'OuvrirOffreTechniqueADistance', 'ActivationCompteEntreprise', 'ImporterEnveloppe', 'SuiviSeulRegistreDepotsPapier', 'SuiviSeulRegistreRetraitsPapier', 'SuiviSeulRegistreQuestionsPapier', 'SuiviSeulRegistreDepotsElectronique', 'SuiviSeulRegistreRetraitsElectronique', 'SuiviSeulRegistreQuestionsElectronique', 'ModifierConsultationMapaInferieurMontantApresValidation', 'ModifierConsultationMapaSuperieurMontantApresValidation', 'ModifierConsultationProceduresFormaliseesApresValidation', 'GererLesEntreprises', 'PorteeSocietesExclues', 'PorteeSocietesExcluesTousOrganismes', 'ModifierSocietesExclues', 'SupprimerSocietesExclues', 'ResultatAnalyse', 'GererAdressesService', 'GererMonService', 'DownloadArchives', 'CreerAnnonceExtraitPv', 'CreerAnnonceRapportAchevement', 'GestionCertificatsAgent', 'CreerAvisProgrammePrevisionnel', 'AnnulerConsultation', 'EnvoyerPublicite', 'ListeMarchesNotifies', 'SuivreMessage', 'EnvoyerMessage', 'SuiviFluxChorusTransversal', 'GestionMandataire', 'GererNewsletter', 'GestionModelesFormulaire', 'GestionAdressesFacturationJal', 'AdministrerAdressesFacturationJal', 'RedactionDocumentsRedac', 'ValidationDocumentsRedac', 'GestionMiseDispositionPiecesMarche', 'AnnuaireAcheteur', 'ReprendreIntegralementArticle', 'AdministrerClauses', 'ValiderClauses', 'AdministrerCanevas', 'ValiderCanevas', 'AdministrerClausesEntiteAchats', 'GenererPiecesFormatOdt', 'PublierVersionClausierEditeur', 'AdministrerClausesEditeur', 'ValiderClausesEditeur', 'AdministrerCanevasEditeur', 'ValiderCanevasEditeur', 'DecisionSuiviSeul', 'OuvrirCandidatureHorsLigne', 'OuvrirOffreHorsLigne', 'OuvrirOffreTechniqueHorsLigne', 'OuvrirAnonymatHorsLigne', 'EspaceCollaboratifGestionnaire', 'EspaceCollaboratifContributeur', 'GererOrganismes', 'GererAssociationsAgents', 'ModuleRedactionUniquement', 'HistoriqueNavigationInscrits', 'TelechargerAccordsCadres', 'CreerAnnonceDecisionResiliation', 'CreerAnnonceSyntheseRapportAudit', 'GererOperations', 'TelechargerSiretAcheteur', 'GererReouverturesModification', 'AccederTousTelechargements', 'CreerContrat', 'ModifierContrat', 'ConsulterContrat', 'GererNewsletterRedac', 'ProfilRma', 'AffectationVisionRma', 'GererGabaritEditeur', 'GererGabarit', 'GererGabaritEntiteAchats', 'GererGabaritAgent', 'GererMessagesAccueil', 'GererOaGa', 'DeplacerService', 'ActiverVersionClausier', 'ExecVoirContratsEa', 'ExecVoirContratsEaDependantes', 'ExecVoirContratsOrganisme', 'AccesWs', 'AccesEchangeDocumentaire', 'EspaceDocumentaireConsultation', 'AdministrerOrganisme', 'ExecModificationContrat', 'BesoinUnitaireConsultation', 'BesoinUnitaireCreationModification', 'DemandeAchatConsultation', 'DemandeAchatCreationModification', 'ProjetAchatConsultation', 'ProjetAchatCreationModification', 'ValidationOpportunite', 'ValidationAchat', 'ValidationBudget', 'StrategieAchatGestion', 'RecensementProgrammationAdministration', 'GestionEnvol', 'ValiderProjetAchat', 'GestionSpaserConsultations', 'GestionValidationEco', 'GestionValidationSip', 'RattachementService', 'DuplicationConsultations', 'ProjetAchatLancementSourcing', 'ProjetAchatInvalidation', 'ProjetAchatAnnulation', 'LancementProcedure', 'RecensementInvaliderProjetAchat', 'RecensementAnnulerProjetAchat', 'AdministrationDocumentsModeles', 'SupprimerContrat', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'libelle', 'gestionAgentPole', 'gestionFournisseursEnvoisPostaux', 'gestionBiCles', 'creerConsultation', 'modifierConsultation', 'validerConsultation', 'publierConsultation', 'suivreConsultation', 'suivreConsultationPole', 'invitePermanentEntiteDependante', 'invitePermanentMonEntite', 'invitePermanentTransverse', 'supprimerEnveloppe', 'supprimerConsultation', 'depouillerCandidature', 'depouillerOffre', 'messagerieSecurisee', 'accesRegistreDepotsPapier', 'accesRegistreRetraitsPapier', 'accesRegistreQuestionsPapier', 'gererEncheres', 'suivreEncheres', 'suiviEntreprise', 'envoiBoamp', 'accesClassementLot', 'connecteurSis', 'connecteurMarco', 'repondreAuxQuestions', 'appelProjetFormation', 'utiliserClientCao', 'notificationBoamp', 'administrerCompte', 'gestionMapa', 'gestionTypeValidation', 'approuverConsultation', 'administrerProcedure', 'restreindreCreation', 'creerListeMarches', 'gestionCommissions', 'suiviSeulConsultation', 'attributionMarche', 'ficheRecensement', 'declarerInfructueux', 'declarerSansSuite', 'creerConsultationTransverse', 'ouvrirCandidatureEnLigne', 'ouvrirCandidatureADistance', 'refuserEnveloppe', 'gererAdmissibilite', 'restaurerEnveloppe', 'ouvrirOffreEnLigne', 'ouvrirAnonymatEnLigne', 'gestionCompteBoamp', 'gestionAgents', 'gestionHabilitations', 'gererMapaInferieurMontant', 'gererMapaSuperieurMontant', 'modifierConsultationAvantValidation', 'modifierConsultationApresValidation', 'accesReponses', 'telechargementGroupeAnticipePlisChiffres', 'telechargementUnitairePlisChiffres', 'ouvrirOffreADistance', 'creerAnnonceInformation', 'saisieMarches', 'validationMarches', 'publicationMarches', 'gererStatistiquesMetier', 'gererArchives', 'administrerProceduresFormalisees', 'creerAnnonceAttribution', 'accesRegistreRetraitsElectronique', 'accesRegistreQuestionsElectronique', 'accesRegistreDepotsElectronique', 'validationSimple', 'validationIntermediaire', 'validationFinale', 'creerSuiteConsultation', 'hyperAdmin', 'droitGestionServices', 'suiviAcces', 'statistiquesSite', 'statistiquesQos', 'ouvrirAnonymatADistance', 'gestionCompteJal', 'gestionCentralePub', 'gestionCompteGroupeMoniteur', 'ouvrirOffreTechniqueEnLigne', 'ouvrirOffreTechniqueADistance', 'activationCompteEntreprise', 'importerEnveloppe', 'suiviSeulRegistreDepotsPapier', 'suiviSeulRegistreRetraitsPapier', 'suiviSeulRegistreQuestionsPapier', 'suiviSeulRegistreDepotsElectronique', 'suiviSeulRegistreRetraitsElectronique', 'suiviSeulRegistreQuestionsElectronique', 'modifierConsultationMapaInferieurMontantApresValidation', 'modifierConsultationMapaSuperieurMontantApresValidation', 'modifierConsultationProceduresFormaliseesApresValidation', 'gererLesEntreprises', 'porteeSocietesExclues', 'porteeSocietesExcluesTousOrganismes', 'modifierSocietesExclues', 'supprimerSocietesExclues', 'resultatAnalyse', 'gererAdressesService', 'gererMonService', 'downloadArchives', 'creerAnnonceExtraitPv', 'creerAnnonceRapportAchevement', 'gestionCertificatsAgent', 'creerAvisProgrammePrevisionnel', 'annulerConsultation', 'envoyerPublicite', 'listeMarchesNotifies', 'suivreMessage', 'envoyerMessage', 'suiviFluxChorusTransversal', 'gestionMandataire', 'gererNewsletter', 'gestionModelesFormulaire', 'gestionAdressesFacturationJal', 'administrerAdressesFacturationJal', 'redactionDocumentsRedac', 'validationDocumentsRedac', 'gestionMiseDispositionPiecesMarche', 'annuaireAcheteur', 'reprendreIntegralementArticle', 'administrerClauses', 'validerClauses', 'administrerCanevas', 'validerCanevas', 'administrerClausesEntiteAchats', 'genererPiecesFormatOdt', 'publierVersionClausierEditeur', 'administrerClausesEditeur', 'validerClausesEditeur', 'administrerCanevasEditeur', 'validerCanevasEditeur', 'decisionSuiviSeul', 'ouvrirCandidatureHorsLigne', 'ouvrirOffreHorsLigne', 'ouvrirOffreTechniqueHorsLigne', 'ouvrirAnonymatHorsLigne', 'espaceCollaboratifGestionnaire', 'espaceCollaboratifContributeur', 'gererOrganismes', 'gererAssociationsAgents', 'moduleRedactionUniquement', 'historiqueNavigationInscrits', 'telechargerAccordsCadres', 'creerAnnonceDecisionResiliation', 'creerAnnonceSyntheseRapportAudit', 'gererOperations', 'telechargerSiretAcheteur', 'gererReouverturesModification', 'accederTousTelechargements', 'creerContrat', 'modifierContrat', 'consulterContrat', 'gererNewsletterRedac', 'profilRma', 'affectationVisionRma', 'gererGabaritEditeur', 'gererGabarit', 'gererGabaritEntiteAchats', 'gererGabaritAgent', 'gererMessagesAccueil', 'gererOaGa', 'deplacerService', 'activerVersionClausier', 'execVoirContratsEa', 'execVoirContratsEaDependantes', 'execVoirContratsOrganisme', 'accesWs', 'accesEchangeDocumentaire', 'espaceDocumentaireConsultation', 'administrerOrganisme', 'execModificationContrat', 'besoinUnitaireConsultation', 'besoinUnitaireCreationModification', 'demandeAchatConsultation', 'demandeAchatCreationModification', 'projetAchatConsultation', 'projetAchatCreationModification', 'validationOpportunite', 'validationAchat', 'validationBudget', 'strategieAchatGestion', 'recensementProgrammationAdministration', 'gestionEnvol', 'validerProjetAchat', 'gestionSpaserConsultations', 'gestionValidationEco', 'gestionValidationSip', 'rattachementService', 'duplicationConsultations', 'projetAchatLancementSourcing', 'projetAchatInvalidation', 'projetAchatAnnulation', 'lancementProcedure', 'recensementInvaliderProjetAchat', 'recensementAnnulerProjetAchat', 'administrationDocumentsModeles', 'supprimerContrat', ),
        BasePeer::TYPE_COLNAME => array (CommonHabilitationProfilPeer::ID, CommonHabilitationProfilPeer::LIBELLE, CommonHabilitationProfilPeer::GESTION_AGENT_POLE, CommonHabilitationProfilPeer::GESTION_FOURNISSEURS_ENVOIS_POSTAUX, CommonHabilitationProfilPeer::GESTION_BI_CLES, CommonHabilitationProfilPeer::CREER_CONSULTATION, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION, CommonHabilitationProfilPeer::VALIDER_CONSULTATION, CommonHabilitationProfilPeer::PUBLIER_CONSULTATION, CommonHabilitationProfilPeer::SUIVRE_CONSULTATION, CommonHabilitationProfilPeer::SUIVRE_CONSULTATION_POLE, CommonHabilitationProfilPeer::INVITE_PERMANENT_ENTITE_DEPENDANTE, CommonHabilitationProfilPeer::INVITE_PERMANENT_MON_ENTITE, CommonHabilitationProfilPeer::INVITE_PERMANENT_TRANSVERSE, CommonHabilitationProfilPeer::SUPPRIMER_ENVELOPPE, CommonHabilitationProfilPeer::SUPPRIMER_CONSULTATION, CommonHabilitationProfilPeer::DEPOUILLER_CANDIDATURE, CommonHabilitationProfilPeer::DEPOUILLER_OFFRE, CommonHabilitationProfilPeer::MESSAGERIE_SECURISEE, CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_PAPIER, CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_PAPIER, CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_PAPIER, CommonHabilitationProfilPeer::GERER_ENCHERES, CommonHabilitationProfilPeer::SUIVRE_ENCHERES, CommonHabilitationProfilPeer::SUIVI_ENTREPRISE, CommonHabilitationProfilPeer::ENVOI_BOAMP, CommonHabilitationProfilPeer::ACCES_CLASSEMENT_LOT, CommonHabilitationProfilPeer::CONNECTEUR_SIS, CommonHabilitationProfilPeer::CONNECTEUR_MARCO, CommonHabilitationProfilPeer::REPONDRE_AUX_QUESTIONS, CommonHabilitationProfilPeer::APPEL_PROJET_FORMATION, CommonHabilitationProfilPeer::UTILISER_CLIENT_CAO, CommonHabilitationProfilPeer::NOTIFICATION_BOAMP, CommonHabilitationProfilPeer::ADMINISTRER_COMPTE, CommonHabilitationProfilPeer::GESTION_MAPA, CommonHabilitationProfilPeer::GESTION_TYPE_VALIDATION, CommonHabilitationProfilPeer::APPROUVER_CONSULTATION, CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURE, CommonHabilitationProfilPeer::RESTREINDRE_CREATION, CommonHabilitationProfilPeer::CREER_LISTE_MARCHES, CommonHabilitationProfilPeer::GESTION_COMMISSIONS, CommonHabilitationProfilPeer::SUIVI_SEUL_CONSULTATION, CommonHabilitationProfilPeer::ATTRIBUTION_MARCHE, CommonHabilitationProfilPeer::FICHE_RECENSEMENT, CommonHabilitationProfilPeer::DECLARER_INFRUCTUEUX, CommonHabilitationProfilPeer::DECLARER_SANS_SUITE, CommonHabilitationProfilPeer::CREER_CONSULTATION_TRANSVERSE, CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_EN_LIGNE, CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_A_DISTANCE, CommonHabilitationProfilPeer::REFUSER_ENVELOPPE, CommonHabilitationProfilPeer::GERER_ADMISSIBILITE, CommonHabilitationProfilPeer::RESTAURER_ENVELOPPE, CommonHabilitationProfilPeer::OUVRIR_OFFRE_EN_LIGNE, CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_EN_LIGNE, CommonHabilitationProfilPeer::GESTION_COMPTE_BOAMP, CommonHabilitationProfilPeer::GESTION_AGENTS, CommonHabilitationProfilPeer::GESTION_HABILITATIONS, CommonHabilitationProfilPeer::GERER_MAPA_INFERIEUR_MONTANT, CommonHabilitationProfilPeer::GERER_MAPA_SUPERIEUR_MONTANT, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_AVANT_VALIDATION, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_APRES_VALIDATION, CommonHabilitationProfilPeer::ACCES_REPONSES, CommonHabilitationProfilPeer::TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES, CommonHabilitationProfilPeer::TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES, CommonHabilitationProfilPeer::OUVRIR_OFFRE_A_DISTANCE, CommonHabilitationProfilPeer::CREER_ANNONCE_INFORMATION, CommonHabilitationProfilPeer::SAISIE_MARCHES, CommonHabilitationProfilPeer::VALIDATION_MARCHES, CommonHabilitationProfilPeer::PUBLICATION_MARCHES, CommonHabilitationProfilPeer::GERER_STATISTIQUES_METIER, CommonHabilitationProfilPeer::GERER_ARCHIVES, CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURES_FORMALISEES, CommonHabilitationProfilPeer::CREER_ANNONCE_ATTRIBUTION, CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_ELECTRONIQUE, CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE, CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_ELECTRONIQUE, CommonHabilitationProfilPeer::VALIDATION_SIMPLE, CommonHabilitationProfilPeer::VALIDATION_INTERMEDIAIRE, CommonHabilitationProfilPeer::VALIDATION_FINALE, CommonHabilitationProfilPeer::CREER_SUITE_CONSULTATION, CommonHabilitationProfilPeer::HYPER_ADMIN, CommonHabilitationProfilPeer::DROIT_GESTION_SERVICES, CommonHabilitationProfilPeer::SUIVI_ACCES, CommonHabilitationProfilPeer::STATISTIQUES_SITE, CommonHabilitationProfilPeer::STATISTIQUES_QOS, CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_A_DISTANCE, CommonHabilitationProfilPeer::GESTION_COMPTE_JAL, CommonHabilitationProfilPeer::GESTION_CENTRALE_PUB, CommonHabilitationProfilPeer::GESTION_COMPTE_GROUPE_MONITEUR, CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE, CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE, CommonHabilitationProfilPeer::ACTIVATION_COMPTE_ENTREPRISE, CommonHabilitationProfilPeer::IMPORTER_ENVELOPPE, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION, CommonHabilitationProfilPeer::GERER_LES_ENTREPRISES, CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES, CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES, CommonHabilitationProfilPeer::MODIFIER_SOCIETES_EXCLUES, CommonHabilitationProfilPeer::SUPPRIMER_SOCIETES_EXCLUES, CommonHabilitationProfilPeer::RESULTAT_ANALYSE, CommonHabilitationProfilPeer::GERER_ADRESSES_SERVICE, CommonHabilitationProfilPeer::GERER_MON_SERVICE, CommonHabilitationProfilPeer::DOWNLOAD_ARCHIVES, CommonHabilitationProfilPeer::CREER_ANNONCE_EXTRAIT_PV, CommonHabilitationProfilPeer::CREER_ANNONCE_RAPPORT_ACHEVEMENT, CommonHabilitationProfilPeer::GESTION_CERTIFICATS_AGENT, CommonHabilitationProfilPeer::CREER_AVIS_PROGRAMME_PREVISIONNEL, CommonHabilitationProfilPeer::ANNULER_CONSULTATION, CommonHabilitationProfilPeer::ENVOYER_PUBLICITE, CommonHabilitationProfilPeer::LISTE_MARCHES_NOTIFIES, CommonHabilitationProfilPeer::SUIVRE_MESSAGE, CommonHabilitationProfilPeer::ENVOYER_MESSAGE, CommonHabilitationProfilPeer::SUIVI_FLUX_CHORUS_TRANSVERSAL, CommonHabilitationProfilPeer::GESTION_MANDATAIRE, CommonHabilitationProfilPeer::GERER_NEWSLETTER, CommonHabilitationProfilPeer::GESTION_MODELES_FORMULAIRE, CommonHabilitationProfilPeer::GESTION_ADRESSES_FACTURATION_JAL, CommonHabilitationProfilPeer::ADMINISTRER_ADRESSES_FACTURATION_JAL, CommonHabilitationProfilPeer::REDACTION_DOCUMENTS_REDAC, CommonHabilitationProfilPeer::VALIDATION_DOCUMENTS_REDAC, CommonHabilitationProfilPeer::GESTION_MISE_DISPOSITION_PIECES_MARCHE, CommonHabilitationProfilPeer::ANNUAIRE_ACHETEUR, CommonHabilitationProfilPeer::REPRENDRE_INTEGRALEMENT_ARTICLE, CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES, CommonHabilitationProfilPeer::VALIDER_CLAUSES, CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS, CommonHabilitationProfilPeer::VALIDER_CANEVAS, CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_ENTITE_ACHATS, CommonHabilitationProfilPeer::GENERER_PIECES_FORMAT_ODT, CommonHabilitationProfilPeer::PUBLIER_VERSION_CLAUSIER_EDITEUR, CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_EDITEUR, CommonHabilitationProfilPeer::VALIDER_CLAUSES_EDITEUR, CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS_EDITEUR, CommonHabilitationProfilPeer::VALIDER_CANEVAS_EDITEUR, CommonHabilitationProfilPeer::DECISION_SUIVI_SEUL, CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_HORS_LIGNE, CommonHabilitationProfilPeer::OUVRIR_OFFRE_HORS_LIGNE, CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE, CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_HORS_LIGNE, CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_GESTIONNAIRE, CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_CONTRIBUTEUR, CommonHabilitationProfilPeer::GERER_ORGANISMES, CommonHabilitationProfilPeer::GERER_ASSOCIATIONS_AGENTS, CommonHabilitationProfilPeer::MODULE_REDACTION_UNIQUEMENT, CommonHabilitationProfilPeer::HISTORIQUE_NAVIGATION_INSCRITS, CommonHabilitationProfilPeer::TELECHARGER_ACCORDS_CADRES, CommonHabilitationProfilPeer::CREER_ANNONCE_DECISION_RESILIATION, CommonHabilitationProfilPeer::CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT, CommonHabilitationProfilPeer::GERER_OPERATIONS, CommonHabilitationProfilPeer::TELECHARGER_SIRET_ACHETEUR, CommonHabilitationProfilPeer::GERER_REOUVERTURES_MODIFICATION, CommonHabilitationProfilPeer::ACCEDER_TOUS_TELECHARGEMENTS, CommonHabilitationProfilPeer::CREER_CONTRAT, CommonHabilitationProfilPeer::MODIFIER_CONTRAT, CommonHabilitationProfilPeer::CONSULTER_CONTRAT, CommonHabilitationProfilPeer::GERER_NEWSLETTER_REDAC, CommonHabilitationProfilPeer::PROFIL_RMA, CommonHabilitationProfilPeer::AFFECTATION_VISION_RMA, CommonHabilitationProfilPeer::GERER_GABARIT_EDITEUR, CommonHabilitationProfilPeer::GERER_GABARIT, CommonHabilitationProfilPeer::GERER_GABARIT_ENTITE_ACHATS, CommonHabilitationProfilPeer::GERER_GABARIT_AGENT, CommonHabilitationProfilPeer::GERER_MESSAGES_ACCUEIL, CommonHabilitationProfilPeer::GERER_OA_GA, CommonHabilitationProfilPeer::DEPLACER_SERVICE, CommonHabilitationProfilPeer::ACTIVER_VERSION_CLAUSIER, CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA, CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA_DEPENDANTES, CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_ORGANISME, CommonHabilitationProfilPeer::ACCES_WS, CommonHabilitationProfilPeer::ACCES_ECHANGE_DOCUMENTAIRE, CommonHabilitationProfilPeer::ESPACE_DOCUMENTAIRE_CONSULTATION, CommonHabilitationProfilPeer::ADMINISTRER_ORGANISME, CommonHabilitationProfilPeer::EXEC_MODIFICATION_CONTRAT, CommonHabilitationProfilPeer::BESOIN_UNITAIRE_CONSULTATION, CommonHabilitationProfilPeer::BESOIN_UNITAIRE_CREATION_MODIFICATION, CommonHabilitationProfilPeer::DEMANDE_ACHAT_CONSULTATION, CommonHabilitationProfilPeer::DEMANDE_ACHAT_CREATION_MODIFICATION, CommonHabilitationProfilPeer::PROJET_ACHAT_CONSULTATION, CommonHabilitationProfilPeer::PROJET_ACHAT_CREATION_MODIFICATION, CommonHabilitationProfilPeer::VALIDATION_OPPORTUNITE, CommonHabilitationProfilPeer::VALIDATION_ACHAT, CommonHabilitationProfilPeer::VALIDATION_BUDGET, CommonHabilitationProfilPeer::STRATEGIE_ACHAT_GESTION, CommonHabilitationProfilPeer::RECENSEMENT_PROGRAMMATION_ADMINISTRATION, CommonHabilitationProfilPeer::GESTION_ENVOL, CommonHabilitationProfilPeer::VALIDER_PROJET_ACHAT, CommonHabilitationProfilPeer::GESTION_SPASER_CONSULTATIONS, CommonHabilitationProfilPeer::GESTION_VALIDATION_ECO, CommonHabilitationProfilPeer::GESTION_VALIDATION_SIP, CommonHabilitationProfilPeer::RATTACHEMENT_SERVICE, CommonHabilitationProfilPeer::DUPLICATION_CONSULTATIONS, CommonHabilitationProfilPeer::PROJET_ACHAT_LANCEMENT_SOURCING, CommonHabilitationProfilPeer::PROJET_ACHAT_INVALIDATION, CommonHabilitationProfilPeer::PROJET_ACHAT_ANNULATION, CommonHabilitationProfilPeer::LANCEMENT_PROCEDURE, CommonHabilitationProfilPeer::RECENSEMENT_INVALIDER_PROJET_ACHAT, CommonHabilitationProfilPeer::RECENSEMENT_ANNULER_PROJET_ACHAT, CommonHabilitationProfilPeer::ADMINISTRATION_DOCUMENTS_MODELES, CommonHabilitationProfilPeer::SUPPRIMER_CONTRAT, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'LIBELLE', 'GESTION_AGENT_POLE', 'GESTION_FOURNISSEURS_ENVOIS_POSTAUX', 'GESTION_BI_CLES', 'CREER_CONSULTATION', 'MODIFIER_CONSULTATION', 'VALIDER_CONSULTATION', 'PUBLIER_CONSULTATION', 'SUIVRE_CONSULTATION', 'SUIVRE_CONSULTATION_POLE', 'INVITE_PERMANENT_ENTITE_DEPENDANTE', 'INVITE_PERMANENT_MON_ENTITE', 'INVITE_PERMANENT_TRANSVERSE', 'SUPPRIMER_ENVELOPPE', 'SUPPRIMER_CONSULTATION', 'DEPOUILLER_CANDIDATURE', 'DEPOUILLER_OFFRE', 'MESSAGERIE_SECURISEE', 'ACCES_REGISTRE_DEPOTS_PAPIER', 'ACCES_REGISTRE_RETRAITS_PAPIER', 'ACCES_REGISTRE_QUESTIONS_PAPIER', 'GERER_ENCHERES', 'SUIVRE_ENCHERES', 'SUIVI_ENTREPRISE', 'ENVOI_BOAMP', 'ACCES_CLASSEMENT_LOT', 'CONNECTEUR_SIS', 'CONNECTEUR_MARCO', 'REPONDRE_AUX_QUESTIONS', 'APPEL_PROJET_FORMATION', 'UTILISER_CLIENT_CAO', 'NOTIFICATION_BOAMP', 'ADMINISTRER_COMPTE', 'GESTION_MAPA', 'GESTION_TYPE_VALIDATION', 'APPROUVER_CONSULTATION', 'ADMINISTRER_PROCEDURE', 'RESTREINDRE_CREATION', 'CREER_LISTE_MARCHES', 'GESTION_COMMISSIONS', 'SUIVI_SEUL_CONSULTATION', 'ATTRIBUTION_MARCHE', 'FICHE_RECENSEMENT', 'DECLARER_INFRUCTUEUX', 'DECLARER_SANS_SUITE', 'CREER_CONSULTATION_TRANSVERSE', 'OUVRIR_CANDIDATURE_EN_LIGNE', 'OUVRIR_CANDIDATURE_A_DISTANCE', 'REFUSER_ENVELOPPE', 'GERER_ADMISSIBILITE', 'RESTAURER_ENVELOPPE', 'OUVRIR_OFFRE_EN_LIGNE', 'OUVRIR_ANONYMAT_EN_LIGNE', 'GESTION_COMPTE_BOAMP', 'GESTION_AGENTS', 'GESTION_HABILITATIONS', 'GERER_MAPA_INFERIEUR_MONTANT', 'GERER_MAPA_SUPERIEUR_MONTANT', 'MODIFIER_CONSULTATION_AVANT_VALIDATION', 'MODIFIER_CONSULTATION_APRES_VALIDATION', 'ACCES_REPONSES', 'TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES', 'TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES', 'OUVRIR_OFFRE_A_DISTANCE', 'CREER_ANNONCE_INFORMATION', 'SAISIE_MARCHES', 'VALIDATION_MARCHES', 'PUBLICATION_MARCHES', 'GERER_STATISTIQUES_METIER', 'GERER_ARCHIVES', 'ADMINISTRER_PROCEDURES_FORMALISEES', 'CREER_ANNONCE_ATTRIBUTION', 'ACCES_REGISTRE_RETRAITS_ELECTRONIQUE', 'ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE', 'ACCES_REGISTRE_DEPOTS_ELECTRONIQUE', 'VALIDATION_SIMPLE', 'VALIDATION_INTERMEDIAIRE', 'VALIDATION_FINALE', 'CREER_SUITE_CONSULTATION', 'HYPER_ADMIN', 'DROIT_GESTION_SERVICES', 'SUIVI_ACCES', 'STATISTIQUES_SITE', 'STATISTIQUES_QOS', 'OUVRIR_ANONYMAT_A_DISTANCE', 'GESTION_COMPTE_JAL', 'GESTION_CENTRALE_PUB', 'GESTION_COMPTE_GROUPE_MONITEUR', 'OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE', 'OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE', 'ACTIVATION_COMPTE_ENTREPRISE', 'IMPORTER_ENVELOPPE', 'SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER', 'SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER', 'SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER', 'SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE', 'SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE', 'SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE', 'MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION', 'MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION', 'MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION', 'GERER_LES_ENTREPRISES', 'PORTEE_SOCIETES_EXCLUES', 'PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES', 'MODIFIER_SOCIETES_EXCLUES', 'SUPPRIMER_SOCIETES_EXCLUES', 'RESULTAT_ANALYSE', 'GERER_ADRESSES_SERVICE', 'GERER_MON_SERVICE', 'DOWNLOAD_ARCHIVES', 'CREER_ANNONCE_EXTRAIT_PV', 'CREER_ANNONCE_RAPPORT_ACHEVEMENT', 'GESTION_CERTIFICATS_AGENT', 'CREER_AVIS_PROGRAMME_PREVISIONNEL', 'ANNULER_CONSULTATION', 'ENVOYER_PUBLICITE', 'LISTE_MARCHES_NOTIFIES', 'SUIVRE_MESSAGE', 'ENVOYER_MESSAGE', 'SUIVI_FLUX_CHORUS_TRANSVERSAL', 'GESTION_MANDATAIRE', 'GERER_NEWSLETTER', 'GESTION_MODELES_FORMULAIRE', 'GESTION_ADRESSES_FACTURATION_JAL', 'ADMINISTRER_ADRESSES_FACTURATION_JAL', 'REDACTION_DOCUMENTS_REDAC', 'VALIDATION_DOCUMENTS_REDAC', 'GESTION_MISE_DISPOSITION_PIECES_MARCHE', 'ANNUAIRE_ACHETEUR', 'REPRENDRE_INTEGRALEMENT_ARTICLE', 'ADMINISTRER_CLAUSES', 'VALIDER_CLAUSES', 'ADMINISTRER_CANEVAS', 'VALIDER_CANEVAS', 'ADMINISTRER_CLAUSES_ENTITE_ACHATS', 'GENERER_PIECES_FORMAT_ODT', 'PUBLIER_VERSION_CLAUSIER_EDITEUR', 'ADMINISTRER_CLAUSES_EDITEUR', 'VALIDER_CLAUSES_EDITEUR', 'ADMINISTRER_CANEVAS_EDITEUR', 'VALIDER_CANEVAS_EDITEUR', 'DECISION_SUIVI_SEUL', 'OUVRIR_CANDIDATURE_HORS_LIGNE', 'OUVRIR_OFFRE_HORS_LIGNE', 'OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE', 'OUVRIR_ANONYMAT_HORS_LIGNE', 'ESPACE_COLLABORATIF_GESTIONNAIRE', 'ESPACE_COLLABORATIF_CONTRIBUTEUR', 'GERER_ORGANISMES', 'GERER_ASSOCIATIONS_AGENTS', 'MODULE_REDACTION_UNIQUEMENT', 'HISTORIQUE_NAVIGATION_INSCRITS', 'TELECHARGER_ACCORDS_CADRES', 'CREER_ANNONCE_DECISION_RESILIATION', 'CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT', 'GERER_OPERATIONS', 'TELECHARGER_SIRET_ACHETEUR', 'GERER_REOUVERTURES_MODIFICATION', 'ACCEDER_TOUS_TELECHARGEMENTS', 'CREER_CONTRAT', 'MODIFIER_CONTRAT', 'CONSULTER_CONTRAT', 'GERER_NEWSLETTER_REDAC', 'PROFIL_RMA', 'AFFECTATION_VISION_RMA', 'GERER_GABARIT_EDITEUR', 'GERER_GABARIT', 'GERER_GABARIT_ENTITE_ACHATS', 'GERER_GABARIT_AGENT', 'GERER_MESSAGES_ACCUEIL', 'GERER_OA_GA', 'DEPLACER_SERVICE', 'ACTIVER_VERSION_CLAUSIER', 'EXEC_VOIR_CONTRATS_EA', 'EXEC_VOIR_CONTRATS_EA_DEPENDANTES', 'EXEC_VOIR_CONTRATS_ORGANISME', 'ACCES_WS', 'ACCES_ECHANGE_DOCUMENTAIRE', 'ESPACE_DOCUMENTAIRE_CONSULTATION', 'ADMINISTRER_ORGANISME', 'EXEC_MODIFICATION_CONTRAT', 'BESOIN_UNITAIRE_CONSULTATION', 'BESOIN_UNITAIRE_CREATION_MODIFICATION', 'DEMANDE_ACHAT_CONSULTATION', 'DEMANDE_ACHAT_CREATION_MODIFICATION', 'PROJET_ACHAT_CONSULTATION', 'PROJET_ACHAT_CREATION_MODIFICATION', 'VALIDATION_OPPORTUNITE', 'VALIDATION_ACHAT', 'VALIDATION_BUDGET', 'STRATEGIE_ACHAT_GESTION', 'RECENSEMENT_PROGRAMMATION_ADMINISTRATION', 'GESTION_ENVOL', 'VALIDER_PROJET_ACHAT', 'GESTION_SPASER_CONSULTATIONS', 'GESTION_VALIDATION_ECO', 'GESTION_VALIDATION_SIP', 'RATTACHEMENT_SERVICE', 'DUPLICATION_CONSULTATIONS', 'PROJET_ACHAT_LANCEMENT_SOURCING', 'PROJET_ACHAT_INVALIDATION', 'PROJET_ACHAT_ANNULATION', 'LANCEMENT_PROCEDURE', 'RECENSEMENT_INVALIDER_PROJET_ACHAT', 'RECENSEMENT_ANNULER_PROJET_ACHAT', 'ADMINISTRATION_DOCUMENTS_MODELES', 'SUPPRIMER_CONTRAT', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'libelle', 'gestion_agent_pole', 'gestion_fournisseurs_envois_postaux', 'gestion_bi_cles', 'creer_consultation', 'modifier_consultation', 'valider_consultation', 'publier_consultation', 'suivre_consultation', 'suivre_consultation_pole', 'invite_permanent_entite_dependante', 'invite_permanent_mon_entite', 'invite_permanent_transverse', 'supprimer_enveloppe', 'supprimer_consultation', 'depouiller_candidature', 'depouiller_offre', 'messagerie_securisee', 'acces_registre_depots_papier', 'acces_registre_retraits_papier', 'acces_registre_questions_papier', 'gerer_encheres', 'suivre_encheres', 'suivi_entreprise', 'envoi_boamp', 'acces_classement_lot', 'connecteur_sis', 'connecteur_marco', 'repondre_aux_questions', 'appel_projet_formation', 'utiliser_client_CAO', 'notification_boamp', 'administrer_compte', 'gestion_mapa', 'gestion_type_validation', 'approuver_consultation', 'administrer_procedure', 'restreindre_creation', 'creer_liste_marches', 'gestion_commissions', 'suivi_seul_consultation', 'attribution_marche', 'fiche_recensement', 'declarer_infructueux', 'declarer_sans_suite', 'creer_consultation_transverse', 'ouvrir_candidature_en_ligne', 'ouvrir_candidature_a_distance', 'refuser_enveloppe', 'gerer_admissibilite', 'restaurer_enveloppe', 'ouvrir_offre_en_ligne', 'ouvrir_anonymat_en_ligne', 'gestion_compte_boamp', 'gestion_agents', 'gestion_habilitations', 'gerer_mapa_inferieur_montant', 'gerer_mapa_superieur_montant', 'modifier_consultation_avant_validation', 'modifier_consultation_apres_validation', 'acces_reponses', 'telechargement_groupe_anticipe_plis_chiffres', 'telechargement_unitaire_plis_chiffres', 'ouvrir_offre_a_distance', 'creer_annonce_information', 'saisie_marches', 'validation_marches', 'publication_marches', 'gerer_statistiques_metier', 'gerer_archives', 'administrer_procedures_formalisees', 'creer_annonce_attribution', 'acces_registre_retraits_electronique', 'acces_registre_questions_electronique', 'acces_registre_depots_electronique', 'validation_simple', 'validation_intermediaire', 'validation_finale', 'creer_suite_consultation', 'hyper_admin', 'droit_gestion_services', 'suivi_acces', 'statistiques_site', 'statistiques_QoS', 'ouvrir_anonymat_a_distance', 'gestion_compte_jal', 'gestion_centrale_pub', 'Gestion_Compte_Groupe_Moniteur', 'ouvrir_offre_technique_en_ligne', 'ouvrir_offre_technique_a_distance', 'activation_compte_entreprise', 'importer_enveloppe', 'suivi_seul_registre_depots_papier', 'suivi_seul_registre_retraits_papier', 'suivi_seul_registre_questions_papier', 'suivi_seul_registre_depots_electronique', 'suivi_seul_registre_retraits_electronique', 'suivi_seul_registre_questions_electronique', 'modifier_consultation_mapa_inferieur_montant_apres_validation', 'modifier_consultation_mapa_superieur_montant_apres_validation', 'modifier_consultation_procedures_formalisees_apres_validation', 'gerer_les_entreprises', 'portee_societes_exclues', 'portee_societes_exclues_tous_organismes', 'modifier_societes_exclues', 'supprimer_societes_exclues', 'resultat_analyse', 'gerer_adresses_service', 'gerer_mon_service', 'download_archives', 'creer_annonce_extrait_pv', 'creer_annonce_rapport_achevement', 'gestion_certificats_agent', 'creer_avis_programme_previsionnel', 'annuler_consultation', 'envoyer_publicite', 'liste_marches_notifies', 'suivre_message', 'envoyer_message', 'suivi_flux_chorus_transversal', 'gestion_mandataire', 'gerer_newsletter', 'gestion_modeles_formulaire', 'gestion_adresses_facturation_jal', 'administrer_adresses_facturation_jal', 'redaction_documents_redac', 'validation_documents_redac', 'gestion_mise_disposition_pieces_marche', 'annuaire_acheteur', 'reprendre_integralement_article', 'administrer_clauses', 'valider_clauses', 'administrer_canevas', 'valider_canevas', 'administrer_clauses_entite_achats', 'generer_pieces_format_odt', 'publier_version_clausier_editeur', 'administrer_clauses_editeur', 'valider_clauses_editeur', 'administrer_canevas_editeur', 'valider_canevas_editeur', 'decision_suivi_seul', 'ouvrir_candidature_hors_ligne', 'ouvrir_offre_hors_ligne', 'ouvrir_offre_technique_hors_ligne', 'ouvrir_anonymat_hors_ligne', 'espace_collaboratif_gestionnaire', 'espace_collaboratif_contributeur', 'gerer_organismes', 'gerer_associations_agents', 'module_redaction_uniquement', 'historique_navigation_inscrits', 'telecharger_accords_cadres', 'creer_annonce_decision_resiliation', 'creer_annonce_synthese_rapport_audit', 'gerer_operations', 'telecharger_siret_acheteur', 'gerer_reouvertures_modification', 'acceder_tous_telechargements', 'creer_contrat', 'modifier_contrat', 'consulter_contrat', 'gerer_newsletter_redac', 'profil_rma', 'affectation_vision_rma', 'gerer_gabarit_editeur', 'gerer_gabarit', 'gerer_gabarit_entite_achats', 'gerer_gabarit_agent', 'gerer_messages_accueil', 'gerer_OA_GA', 'deplacer_service', 'activer_version_clausier', 'exec_voir_contrats_ea', 'exec_voir_contrats_ea_dependantes', 'exec_voir_contrats_organisme', 'acces_ws', 'acces_echange_documentaire', 'espace_documentaire_consultation', 'administrer_organisme', 'exec_modification_contrat', 'besoin_unitaire_consultation', 'besoin_unitaire_creation_modification', 'demande_achat_consultation', 'demande_achat_creation_modification', 'projet_achat_consultation', 'projet_achat_creation_modification', 'validation_opportunite', 'validation_achat', 'validation_budget', 'strategie_achat_gestion', 'recensement_programmation_administration', 'gestion_envol', 'valider_projet_achat', 'gestion_spaser_consultations', 'gestion_validation_eco', 'gestion_validation_sip', 'rattachement_service', 'duplication_consultations', 'projet_achat_lancement_sourcing', 'projet_achat_invalidation', 'projet_achat_annulation', 'lancement_procedure', 'recensement_invalider_projet_achat', 'recensement_annuler_projet_achat', 'administration_documents_modeles', 'supprimer_contrat', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonHabilitationProfilPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Libelle' => 1, 'GestionAgentPole' => 2, 'GestionFournisseursEnvoisPostaux' => 3, 'GestionBiCles' => 4, 'CreerConsultation' => 5, 'ModifierConsultation' => 6, 'ValiderConsultation' => 7, 'PublierConsultation' => 8, 'SuivreConsultation' => 9, 'SuivreConsultationPole' => 10, 'InvitePermanentEntiteDependante' => 11, 'InvitePermanentMonEntite' => 12, 'InvitePermanentTransverse' => 13, 'SupprimerEnveloppe' => 14, 'SupprimerConsultation' => 15, 'DepouillerCandidature' => 16, 'DepouillerOffre' => 17, 'MessagerieSecurisee' => 18, 'AccesRegistreDepotsPapier' => 19, 'AccesRegistreRetraitsPapier' => 20, 'AccesRegistreQuestionsPapier' => 21, 'GererEncheres' => 22, 'SuivreEncheres' => 23, 'SuiviEntreprise' => 24, 'EnvoiBoamp' => 25, 'AccesClassementLot' => 26, 'ConnecteurSis' => 27, 'ConnecteurMarco' => 28, 'RepondreAuxQuestions' => 29, 'AppelProjetFormation' => 30, 'UtiliserClientCao' => 31, 'NotificationBoamp' => 32, 'AdministrerCompte' => 33, 'GestionMapa' => 34, 'GestionTypeValidation' => 35, 'ApprouverConsultation' => 36, 'AdministrerProcedure' => 37, 'RestreindreCreation' => 38, 'CreerListeMarches' => 39, 'GestionCommissions' => 40, 'SuiviSeulConsultation' => 41, 'AttributionMarche' => 42, 'FicheRecensement' => 43, 'DeclarerInfructueux' => 44, 'DeclarerSansSuite' => 45, 'CreerConsultationTransverse' => 46, 'OuvrirCandidatureEnLigne' => 47, 'OuvrirCandidatureADistance' => 48, 'RefuserEnveloppe' => 49, 'GererAdmissibilite' => 50, 'RestaurerEnveloppe' => 51, 'OuvrirOffreEnLigne' => 52, 'OuvrirAnonymatEnLigne' => 53, 'GestionCompteBoamp' => 54, 'GestionAgents' => 55, 'GestionHabilitations' => 56, 'GererMapaInferieurMontant' => 57, 'GererMapaSuperieurMontant' => 58, 'ModifierConsultationAvantValidation' => 59, 'ModifierConsultationApresValidation' => 60, 'AccesReponses' => 61, 'TelechargementGroupeAnticipePlisChiffres' => 62, 'TelechargementUnitairePlisChiffres' => 63, 'OuvrirOffreADistance' => 64, 'CreerAnnonceInformation' => 65, 'SaisieMarches' => 66, 'ValidationMarches' => 67, 'PublicationMarches' => 68, 'GererStatistiquesMetier' => 69, 'GererArchives' => 70, 'AdministrerProceduresFormalisees' => 71, 'CreerAnnonceAttribution' => 72, 'AccesRegistreRetraitsElectronique' => 73, 'AccesRegistreQuestionsElectronique' => 74, 'AccesRegistreDepotsElectronique' => 75, 'ValidationSimple' => 76, 'ValidationIntermediaire' => 77, 'ValidationFinale' => 78, 'CreerSuiteConsultation' => 79, 'HyperAdmin' => 80, 'DroitGestionServices' => 81, 'SuiviAcces' => 82, 'StatistiquesSite' => 83, 'StatistiquesQos' => 84, 'OuvrirAnonymatADistance' => 85, 'GestionCompteJal' => 86, 'GestionCentralePub' => 87, 'GestionCompteGroupeMoniteur' => 88, 'OuvrirOffreTechniqueEnLigne' => 89, 'OuvrirOffreTechniqueADistance' => 90, 'ActivationCompteEntreprise' => 91, 'ImporterEnveloppe' => 92, 'SuiviSeulRegistreDepotsPapier' => 93, 'SuiviSeulRegistreRetraitsPapier' => 94, 'SuiviSeulRegistreQuestionsPapier' => 95, 'SuiviSeulRegistreDepotsElectronique' => 96, 'SuiviSeulRegistreRetraitsElectronique' => 97, 'SuiviSeulRegistreQuestionsElectronique' => 98, 'ModifierConsultationMapaInferieurMontantApresValidation' => 99, 'ModifierConsultationMapaSuperieurMontantApresValidation' => 100, 'ModifierConsultationProceduresFormaliseesApresValidation' => 101, 'GererLesEntreprises' => 102, 'PorteeSocietesExclues' => 103, 'PorteeSocietesExcluesTousOrganismes' => 104, 'ModifierSocietesExclues' => 105, 'SupprimerSocietesExclues' => 106, 'ResultatAnalyse' => 107, 'GererAdressesService' => 108, 'GererMonService' => 109, 'DownloadArchives' => 110, 'CreerAnnonceExtraitPv' => 111, 'CreerAnnonceRapportAchevement' => 112, 'GestionCertificatsAgent' => 113, 'CreerAvisProgrammePrevisionnel' => 114, 'AnnulerConsultation' => 115, 'EnvoyerPublicite' => 116, 'ListeMarchesNotifies' => 117, 'SuivreMessage' => 118, 'EnvoyerMessage' => 119, 'SuiviFluxChorusTransversal' => 120, 'GestionMandataire' => 121, 'GererNewsletter' => 122, 'GestionModelesFormulaire' => 123, 'GestionAdressesFacturationJal' => 124, 'AdministrerAdressesFacturationJal' => 125, 'RedactionDocumentsRedac' => 126, 'ValidationDocumentsRedac' => 127, 'GestionMiseDispositionPiecesMarche' => 128, 'AnnuaireAcheteur' => 129, 'ReprendreIntegralementArticle' => 130, 'AdministrerClauses' => 131, 'ValiderClauses' => 132, 'AdministrerCanevas' => 133, 'ValiderCanevas' => 134, 'AdministrerClausesEntiteAchats' => 135, 'GenererPiecesFormatOdt' => 136, 'PublierVersionClausierEditeur' => 137, 'AdministrerClausesEditeur' => 138, 'ValiderClausesEditeur' => 139, 'AdministrerCanevasEditeur' => 140, 'ValiderCanevasEditeur' => 141, 'DecisionSuiviSeul' => 142, 'OuvrirCandidatureHorsLigne' => 143, 'OuvrirOffreHorsLigne' => 144, 'OuvrirOffreTechniqueHorsLigne' => 145, 'OuvrirAnonymatHorsLigne' => 146, 'EspaceCollaboratifGestionnaire' => 147, 'EspaceCollaboratifContributeur' => 148, 'GererOrganismes' => 149, 'GererAssociationsAgents' => 150, 'ModuleRedactionUniquement' => 151, 'HistoriqueNavigationInscrits' => 152, 'TelechargerAccordsCadres' => 153, 'CreerAnnonceDecisionResiliation' => 154, 'CreerAnnonceSyntheseRapportAudit' => 155, 'GererOperations' => 156, 'TelechargerSiretAcheteur' => 157, 'GererReouverturesModification' => 158, 'AccederTousTelechargements' => 159, 'CreerContrat' => 160, 'ModifierContrat' => 161, 'ConsulterContrat' => 162, 'GererNewsletterRedac' => 163, 'ProfilRma' => 164, 'AffectationVisionRma' => 165, 'GererGabaritEditeur' => 166, 'GererGabarit' => 167, 'GererGabaritEntiteAchats' => 168, 'GererGabaritAgent' => 169, 'GererMessagesAccueil' => 170, 'GererOaGa' => 171, 'DeplacerService' => 172, 'ActiverVersionClausier' => 173, 'ExecVoirContratsEa' => 174, 'ExecVoirContratsEaDependantes' => 175, 'ExecVoirContratsOrganisme' => 176, 'AccesWs' => 177, 'AccesEchangeDocumentaire' => 178, 'EspaceDocumentaireConsultation' => 179, 'AdministrerOrganisme' => 180, 'ExecModificationContrat' => 181, 'BesoinUnitaireConsultation' => 182, 'BesoinUnitaireCreationModification' => 183, 'DemandeAchatConsultation' => 184, 'DemandeAchatCreationModification' => 185, 'ProjetAchatConsultation' => 186, 'ProjetAchatCreationModification' => 187, 'ValidationOpportunite' => 188, 'ValidationAchat' => 189, 'ValidationBudget' => 190, 'StrategieAchatGestion' => 191, 'RecensementProgrammationAdministration' => 192, 'GestionEnvol' => 193, 'ValiderProjetAchat' => 194, 'GestionSpaserConsultations' => 195, 'GestionValidationEco' => 196, 'GestionValidationSip' => 197, 'RattachementService' => 198, 'DuplicationConsultations' => 199, 'ProjetAchatLancementSourcing' => 200, 'ProjetAchatInvalidation' => 201, 'ProjetAchatAnnulation' => 202, 'LancementProcedure' => 203, 'RecensementInvaliderProjetAchat' => 204, 'RecensementAnnulerProjetAchat' => 205, 'AdministrationDocumentsModeles' => 206, 'SupprimerContrat' => 207, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'libelle' => 1, 'gestionAgentPole' => 2, 'gestionFournisseursEnvoisPostaux' => 3, 'gestionBiCles' => 4, 'creerConsultation' => 5, 'modifierConsultation' => 6, 'validerConsultation' => 7, 'publierConsultation' => 8, 'suivreConsultation' => 9, 'suivreConsultationPole' => 10, 'invitePermanentEntiteDependante' => 11, 'invitePermanentMonEntite' => 12, 'invitePermanentTransverse' => 13, 'supprimerEnveloppe' => 14, 'supprimerConsultation' => 15, 'depouillerCandidature' => 16, 'depouillerOffre' => 17, 'messagerieSecurisee' => 18, 'accesRegistreDepotsPapier' => 19, 'accesRegistreRetraitsPapier' => 20, 'accesRegistreQuestionsPapier' => 21, 'gererEncheres' => 22, 'suivreEncheres' => 23, 'suiviEntreprise' => 24, 'envoiBoamp' => 25, 'accesClassementLot' => 26, 'connecteurSis' => 27, 'connecteurMarco' => 28, 'repondreAuxQuestions' => 29, 'appelProjetFormation' => 30, 'utiliserClientCao' => 31, 'notificationBoamp' => 32, 'administrerCompte' => 33, 'gestionMapa' => 34, 'gestionTypeValidation' => 35, 'approuverConsultation' => 36, 'administrerProcedure' => 37, 'restreindreCreation' => 38, 'creerListeMarches' => 39, 'gestionCommissions' => 40, 'suiviSeulConsultation' => 41, 'attributionMarche' => 42, 'ficheRecensement' => 43, 'declarerInfructueux' => 44, 'declarerSansSuite' => 45, 'creerConsultationTransverse' => 46, 'ouvrirCandidatureEnLigne' => 47, 'ouvrirCandidatureADistance' => 48, 'refuserEnveloppe' => 49, 'gererAdmissibilite' => 50, 'restaurerEnveloppe' => 51, 'ouvrirOffreEnLigne' => 52, 'ouvrirAnonymatEnLigne' => 53, 'gestionCompteBoamp' => 54, 'gestionAgents' => 55, 'gestionHabilitations' => 56, 'gererMapaInferieurMontant' => 57, 'gererMapaSuperieurMontant' => 58, 'modifierConsultationAvantValidation' => 59, 'modifierConsultationApresValidation' => 60, 'accesReponses' => 61, 'telechargementGroupeAnticipePlisChiffres' => 62, 'telechargementUnitairePlisChiffres' => 63, 'ouvrirOffreADistance' => 64, 'creerAnnonceInformation' => 65, 'saisieMarches' => 66, 'validationMarches' => 67, 'publicationMarches' => 68, 'gererStatistiquesMetier' => 69, 'gererArchives' => 70, 'administrerProceduresFormalisees' => 71, 'creerAnnonceAttribution' => 72, 'accesRegistreRetraitsElectronique' => 73, 'accesRegistreQuestionsElectronique' => 74, 'accesRegistreDepotsElectronique' => 75, 'validationSimple' => 76, 'validationIntermediaire' => 77, 'validationFinale' => 78, 'creerSuiteConsultation' => 79, 'hyperAdmin' => 80, 'droitGestionServices' => 81, 'suiviAcces' => 82, 'statistiquesSite' => 83, 'statistiquesQos' => 84, 'ouvrirAnonymatADistance' => 85, 'gestionCompteJal' => 86, 'gestionCentralePub' => 87, 'gestionCompteGroupeMoniteur' => 88, 'ouvrirOffreTechniqueEnLigne' => 89, 'ouvrirOffreTechniqueADistance' => 90, 'activationCompteEntreprise' => 91, 'importerEnveloppe' => 92, 'suiviSeulRegistreDepotsPapier' => 93, 'suiviSeulRegistreRetraitsPapier' => 94, 'suiviSeulRegistreQuestionsPapier' => 95, 'suiviSeulRegistreDepotsElectronique' => 96, 'suiviSeulRegistreRetraitsElectronique' => 97, 'suiviSeulRegistreQuestionsElectronique' => 98, 'modifierConsultationMapaInferieurMontantApresValidation' => 99, 'modifierConsultationMapaSuperieurMontantApresValidation' => 100, 'modifierConsultationProceduresFormaliseesApresValidation' => 101, 'gererLesEntreprises' => 102, 'porteeSocietesExclues' => 103, 'porteeSocietesExcluesTousOrganismes' => 104, 'modifierSocietesExclues' => 105, 'supprimerSocietesExclues' => 106, 'resultatAnalyse' => 107, 'gererAdressesService' => 108, 'gererMonService' => 109, 'downloadArchives' => 110, 'creerAnnonceExtraitPv' => 111, 'creerAnnonceRapportAchevement' => 112, 'gestionCertificatsAgent' => 113, 'creerAvisProgrammePrevisionnel' => 114, 'annulerConsultation' => 115, 'envoyerPublicite' => 116, 'listeMarchesNotifies' => 117, 'suivreMessage' => 118, 'envoyerMessage' => 119, 'suiviFluxChorusTransversal' => 120, 'gestionMandataire' => 121, 'gererNewsletter' => 122, 'gestionModelesFormulaire' => 123, 'gestionAdressesFacturationJal' => 124, 'administrerAdressesFacturationJal' => 125, 'redactionDocumentsRedac' => 126, 'validationDocumentsRedac' => 127, 'gestionMiseDispositionPiecesMarche' => 128, 'annuaireAcheteur' => 129, 'reprendreIntegralementArticle' => 130, 'administrerClauses' => 131, 'validerClauses' => 132, 'administrerCanevas' => 133, 'validerCanevas' => 134, 'administrerClausesEntiteAchats' => 135, 'genererPiecesFormatOdt' => 136, 'publierVersionClausierEditeur' => 137, 'administrerClausesEditeur' => 138, 'validerClausesEditeur' => 139, 'administrerCanevasEditeur' => 140, 'validerCanevasEditeur' => 141, 'decisionSuiviSeul' => 142, 'ouvrirCandidatureHorsLigne' => 143, 'ouvrirOffreHorsLigne' => 144, 'ouvrirOffreTechniqueHorsLigne' => 145, 'ouvrirAnonymatHorsLigne' => 146, 'espaceCollaboratifGestionnaire' => 147, 'espaceCollaboratifContributeur' => 148, 'gererOrganismes' => 149, 'gererAssociationsAgents' => 150, 'moduleRedactionUniquement' => 151, 'historiqueNavigationInscrits' => 152, 'telechargerAccordsCadres' => 153, 'creerAnnonceDecisionResiliation' => 154, 'creerAnnonceSyntheseRapportAudit' => 155, 'gererOperations' => 156, 'telechargerSiretAcheteur' => 157, 'gererReouverturesModification' => 158, 'accederTousTelechargements' => 159, 'creerContrat' => 160, 'modifierContrat' => 161, 'consulterContrat' => 162, 'gererNewsletterRedac' => 163, 'profilRma' => 164, 'affectationVisionRma' => 165, 'gererGabaritEditeur' => 166, 'gererGabarit' => 167, 'gererGabaritEntiteAchats' => 168, 'gererGabaritAgent' => 169, 'gererMessagesAccueil' => 170, 'gererOaGa' => 171, 'deplacerService' => 172, 'activerVersionClausier' => 173, 'execVoirContratsEa' => 174, 'execVoirContratsEaDependantes' => 175, 'execVoirContratsOrganisme' => 176, 'accesWs' => 177, 'accesEchangeDocumentaire' => 178, 'espaceDocumentaireConsultation' => 179, 'administrerOrganisme' => 180, 'execModificationContrat' => 181, 'besoinUnitaireConsultation' => 182, 'besoinUnitaireCreationModification' => 183, 'demandeAchatConsultation' => 184, 'demandeAchatCreationModification' => 185, 'projetAchatConsultation' => 186, 'projetAchatCreationModification' => 187, 'validationOpportunite' => 188, 'validationAchat' => 189, 'validationBudget' => 190, 'strategieAchatGestion' => 191, 'recensementProgrammationAdministration' => 192, 'gestionEnvol' => 193, 'validerProjetAchat' => 194, 'gestionSpaserConsultations' => 195, 'gestionValidationEco' => 196, 'gestionValidationSip' => 197, 'rattachementService' => 198, 'duplicationConsultations' => 199, 'projetAchatLancementSourcing' => 200, 'projetAchatInvalidation' => 201, 'projetAchatAnnulation' => 202, 'lancementProcedure' => 203, 'recensementInvaliderProjetAchat' => 204, 'recensementAnnulerProjetAchat' => 205, 'administrationDocumentsModeles' => 206, 'supprimerContrat' => 207, ),
        BasePeer::TYPE_COLNAME => array (CommonHabilitationProfilPeer::ID => 0, CommonHabilitationProfilPeer::LIBELLE => 1, CommonHabilitationProfilPeer::GESTION_AGENT_POLE => 2, CommonHabilitationProfilPeer::GESTION_FOURNISSEURS_ENVOIS_POSTAUX => 3, CommonHabilitationProfilPeer::GESTION_BI_CLES => 4, CommonHabilitationProfilPeer::CREER_CONSULTATION => 5, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION => 6, CommonHabilitationProfilPeer::VALIDER_CONSULTATION => 7, CommonHabilitationProfilPeer::PUBLIER_CONSULTATION => 8, CommonHabilitationProfilPeer::SUIVRE_CONSULTATION => 9, CommonHabilitationProfilPeer::SUIVRE_CONSULTATION_POLE => 10, CommonHabilitationProfilPeer::INVITE_PERMANENT_ENTITE_DEPENDANTE => 11, CommonHabilitationProfilPeer::INVITE_PERMANENT_MON_ENTITE => 12, CommonHabilitationProfilPeer::INVITE_PERMANENT_TRANSVERSE => 13, CommonHabilitationProfilPeer::SUPPRIMER_ENVELOPPE => 14, CommonHabilitationProfilPeer::SUPPRIMER_CONSULTATION => 15, CommonHabilitationProfilPeer::DEPOUILLER_CANDIDATURE => 16, CommonHabilitationProfilPeer::DEPOUILLER_OFFRE => 17, CommonHabilitationProfilPeer::MESSAGERIE_SECURISEE => 18, CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_PAPIER => 19, CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_PAPIER => 20, CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_PAPIER => 21, CommonHabilitationProfilPeer::GERER_ENCHERES => 22, CommonHabilitationProfilPeer::SUIVRE_ENCHERES => 23, CommonHabilitationProfilPeer::SUIVI_ENTREPRISE => 24, CommonHabilitationProfilPeer::ENVOI_BOAMP => 25, CommonHabilitationProfilPeer::ACCES_CLASSEMENT_LOT => 26, CommonHabilitationProfilPeer::CONNECTEUR_SIS => 27, CommonHabilitationProfilPeer::CONNECTEUR_MARCO => 28, CommonHabilitationProfilPeer::REPONDRE_AUX_QUESTIONS => 29, CommonHabilitationProfilPeer::APPEL_PROJET_FORMATION => 30, CommonHabilitationProfilPeer::UTILISER_CLIENT_CAO => 31, CommonHabilitationProfilPeer::NOTIFICATION_BOAMP => 32, CommonHabilitationProfilPeer::ADMINISTRER_COMPTE => 33, CommonHabilitationProfilPeer::GESTION_MAPA => 34, CommonHabilitationProfilPeer::GESTION_TYPE_VALIDATION => 35, CommonHabilitationProfilPeer::APPROUVER_CONSULTATION => 36, CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURE => 37, CommonHabilitationProfilPeer::RESTREINDRE_CREATION => 38, CommonHabilitationProfilPeer::CREER_LISTE_MARCHES => 39, CommonHabilitationProfilPeer::GESTION_COMMISSIONS => 40, CommonHabilitationProfilPeer::SUIVI_SEUL_CONSULTATION => 41, CommonHabilitationProfilPeer::ATTRIBUTION_MARCHE => 42, CommonHabilitationProfilPeer::FICHE_RECENSEMENT => 43, CommonHabilitationProfilPeer::DECLARER_INFRUCTUEUX => 44, CommonHabilitationProfilPeer::DECLARER_SANS_SUITE => 45, CommonHabilitationProfilPeer::CREER_CONSULTATION_TRANSVERSE => 46, CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_EN_LIGNE => 47, CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_A_DISTANCE => 48, CommonHabilitationProfilPeer::REFUSER_ENVELOPPE => 49, CommonHabilitationProfilPeer::GERER_ADMISSIBILITE => 50, CommonHabilitationProfilPeer::RESTAURER_ENVELOPPE => 51, CommonHabilitationProfilPeer::OUVRIR_OFFRE_EN_LIGNE => 52, CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_EN_LIGNE => 53, CommonHabilitationProfilPeer::GESTION_COMPTE_BOAMP => 54, CommonHabilitationProfilPeer::GESTION_AGENTS => 55, CommonHabilitationProfilPeer::GESTION_HABILITATIONS => 56, CommonHabilitationProfilPeer::GERER_MAPA_INFERIEUR_MONTANT => 57, CommonHabilitationProfilPeer::GERER_MAPA_SUPERIEUR_MONTANT => 58, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_AVANT_VALIDATION => 59, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_APRES_VALIDATION => 60, CommonHabilitationProfilPeer::ACCES_REPONSES => 61, CommonHabilitationProfilPeer::TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES => 62, CommonHabilitationProfilPeer::TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES => 63, CommonHabilitationProfilPeer::OUVRIR_OFFRE_A_DISTANCE => 64, CommonHabilitationProfilPeer::CREER_ANNONCE_INFORMATION => 65, CommonHabilitationProfilPeer::SAISIE_MARCHES => 66, CommonHabilitationProfilPeer::VALIDATION_MARCHES => 67, CommonHabilitationProfilPeer::PUBLICATION_MARCHES => 68, CommonHabilitationProfilPeer::GERER_STATISTIQUES_METIER => 69, CommonHabilitationProfilPeer::GERER_ARCHIVES => 70, CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURES_FORMALISEES => 71, CommonHabilitationProfilPeer::CREER_ANNONCE_ATTRIBUTION => 72, CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_ELECTRONIQUE => 73, CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE => 74, CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_ELECTRONIQUE => 75, CommonHabilitationProfilPeer::VALIDATION_SIMPLE => 76, CommonHabilitationProfilPeer::VALIDATION_INTERMEDIAIRE => 77, CommonHabilitationProfilPeer::VALIDATION_FINALE => 78, CommonHabilitationProfilPeer::CREER_SUITE_CONSULTATION => 79, CommonHabilitationProfilPeer::HYPER_ADMIN => 80, CommonHabilitationProfilPeer::DROIT_GESTION_SERVICES => 81, CommonHabilitationProfilPeer::SUIVI_ACCES => 82, CommonHabilitationProfilPeer::STATISTIQUES_SITE => 83, CommonHabilitationProfilPeer::STATISTIQUES_QOS => 84, CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_A_DISTANCE => 85, CommonHabilitationProfilPeer::GESTION_COMPTE_JAL => 86, CommonHabilitationProfilPeer::GESTION_CENTRALE_PUB => 87, CommonHabilitationProfilPeer::GESTION_COMPTE_GROUPE_MONITEUR => 88, CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE => 89, CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE => 90, CommonHabilitationProfilPeer::ACTIVATION_COMPTE_ENTREPRISE => 91, CommonHabilitationProfilPeer::IMPORTER_ENVELOPPE => 92, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER => 93, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER => 94, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER => 95, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE => 96, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE => 97, CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE => 98, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION => 99, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION => 100, CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION => 101, CommonHabilitationProfilPeer::GERER_LES_ENTREPRISES => 102, CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES => 103, CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES => 104, CommonHabilitationProfilPeer::MODIFIER_SOCIETES_EXCLUES => 105, CommonHabilitationProfilPeer::SUPPRIMER_SOCIETES_EXCLUES => 106, CommonHabilitationProfilPeer::RESULTAT_ANALYSE => 107, CommonHabilitationProfilPeer::GERER_ADRESSES_SERVICE => 108, CommonHabilitationProfilPeer::GERER_MON_SERVICE => 109, CommonHabilitationProfilPeer::DOWNLOAD_ARCHIVES => 110, CommonHabilitationProfilPeer::CREER_ANNONCE_EXTRAIT_PV => 111, CommonHabilitationProfilPeer::CREER_ANNONCE_RAPPORT_ACHEVEMENT => 112, CommonHabilitationProfilPeer::GESTION_CERTIFICATS_AGENT => 113, CommonHabilitationProfilPeer::CREER_AVIS_PROGRAMME_PREVISIONNEL => 114, CommonHabilitationProfilPeer::ANNULER_CONSULTATION => 115, CommonHabilitationProfilPeer::ENVOYER_PUBLICITE => 116, CommonHabilitationProfilPeer::LISTE_MARCHES_NOTIFIES => 117, CommonHabilitationProfilPeer::SUIVRE_MESSAGE => 118, CommonHabilitationProfilPeer::ENVOYER_MESSAGE => 119, CommonHabilitationProfilPeer::SUIVI_FLUX_CHORUS_TRANSVERSAL => 120, CommonHabilitationProfilPeer::GESTION_MANDATAIRE => 121, CommonHabilitationProfilPeer::GERER_NEWSLETTER => 122, CommonHabilitationProfilPeer::GESTION_MODELES_FORMULAIRE => 123, CommonHabilitationProfilPeer::GESTION_ADRESSES_FACTURATION_JAL => 124, CommonHabilitationProfilPeer::ADMINISTRER_ADRESSES_FACTURATION_JAL => 125, CommonHabilitationProfilPeer::REDACTION_DOCUMENTS_REDAC => 126, CommonHabilitationProfilPeer::VALIDATION_DOCUMENTS_REDAC => 127, CommonHabilitationProfilPeer::GESTION_MISE_DISPOSITION_PIECES_MARCHE => 128, CommonHabilitationProfilPeer::ANNUAIRE_ACHETEUR => 129, CommonHabilitationProfilPeer::REPRENDRE_INTEGRALEMENT_ARTICLE => 130, CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES => 131, CommonHabilitationProfilPeer::VALIDER_CLAUSES => 132, CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS => 133, CommonHabilitationProfilPeer::VALIDER_CANEVAS => 134, CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_ENTITE_ACHATS => 135, CommonHabilitationProfilPeer::GENERER_PIECES_FORMAT_ODT => 136, CommonHabilitationProfilPeer::PUBLIER_VERSION_CLAUSIER_EDITEUR => 137, CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_EDITEUR => 138, CommonHabilitationProfilPeer::VALIDER_CLAUSES_EDITEUR => 139, CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS_EDITEUR => 140, CommonHabilitationProfilPeer::VALIDER_CANEVAS_EDITEUR => 141, CommonHabilitationProfilPeer::DECISION_SUIVI_SEUL => 142, CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_HORS_LIGNE => 143, CommonHabilitationProfilPeer::OUVRIR_OFFRE_HORS_LIGNE => 144, CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE => 145, CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_HORS_LIGNE => 146, CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_GESTIONNAIRE => 147, CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_CONTRIBUTEUR => 148, CommonHabilitationProfilPeer::GERER_ORGANISMES => 149, CommonHabilitationProfilPeer::GERER_ASSOCIATIONS_AGENTS => 150, CommonHabilitationProfilPeer::MODULE_REDACTION_UNIQUEMENT => 151, CommonHabilitationProfilPeer::HISTORIQUE_NAVIGATION_INSCRITS => 152, CommonHabilitationProfilPeer::TELECHARGER_ACCORDS_CADRES => 153, CommonHabilitationProfilPeer::CREER_ANNONCE_DECISION_RESILIATION => 154, CommonHabilitationProfilPeer::CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT => 155, CommonHabilitationProfilPeer::GERER_OPERATIONS => 156, CommonHabilitationProfilPeer::TELECHARGER_SIRET_ACHETEUR => 157, CommonHabilitationProfilPeer::GERER_REOUVERTURES_MODIFICATION => 158, CommonHabilitationProfilPeer::ACCEDER_TOUS_TELECHARGEMENTS => 159, CommonHabilitationProfilPeer::CREER_CONTRAT => 160, CommonHabilitationProfilPeer::MODIFIER_CONTRAT => 161, CommonHabilitationProfilPeer::CONSULTER_CONTRAT => 162, CommonHabilitationProfilPeer::GERER_NEWSLETTER_REDAC => 163, CommonHabilitationProfilPeer::PROFIL_RMA => 164, CommonHabilitationProfilPeer::AFFECTATION_VISION_RMA => 165, CommonHabilitationProfilPeer::GERER_GABARIT_EDITEUR => 166, CommonHabilitationProfilPeer::GERER_GABARIT => 167, CommonHabilitationProfilPeer::GERER_GABARIT_ENTITE_ACHATS => 168, CommonHabilitationProfilPeer::GERER_GABARIT_AGENT => 169, CommonHabilitationProfilPeer::GERER_MESSAGES_ACCUEIL => 170, CommonHabilitationProfilPeer::GERER_OA_GA => 171, CommonHabilitationProfilPeer::DEPLACER_SERVICE => 172, CommonHabilitationProfilPeer::ACTIVER_VERSION_CLAUSIER => 173, CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA => 174, CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA_DEPENDANTES => 175, CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_ORGANISME => 176, CommonHabilitationProfilPeer::ACCES_WS => 177, CommonHabilitationProfilPeer::ACCES_ECHANGE_DOCUMENTAIRE => 178, CommonHabilitationProfilPeer::ESPACE_DOCUMENTAIRE_CONSULTATION => 179, CommonHabilitationProfilPeer::ADMINISTRER_ORGANISME => 180, CommonHabilitationProfilPeer::EXEC_MODIFICATION_CONTRAT => 181, CommonHabilitationProfilPeer::BESOIN_UNITAIRE_CONSULTATION => 182, CommonHabilitationProfilPeer::BESOIN_UNITAIRE_CREATION_MODIFICATION => 183, CommonHabilitationProfilPeer::DEMANDE_ACHAT_CONSULTATION => 184, CommonHabilitationProfilPeer::DEMANDE_ACHAT_CREATION_MODIFICATION => 185, CommonHabilitationProfilPeer::PROJET_ACHAT_CONSULTATION => 186, CommonHabilitationProfilPeer::PROJET_ACHAT_CREATION_MODIFICATION => 187, CommonHabilitationProfilPeer::VALIDATION_OPPORTUNITE => 188, CommonHabilitationProfilPeer::VALIDATION_ACHAT => 189, CommonHabilitationProfilPeer::VALIDATION_BUDGET => 190, CommonHabilitationProfilPeer::STRATEGIE_ACHAT_GESTION => 191, CommonHabilitationProfilPeer::RECENSEMENT_PROGRAMMATION_ADMINISTRATION => 192, CommonHabilitationProfilPeer::GESTION_ENVOL => 193, CommonHabilitationProfilPeer::VALIDER_PROJET_ACHAT => 194, CommonHabilitationProfilPeer::GESTION_SPASER_CONSULTATIONS => 195, CommonHabilitationProfilPeer::GESTION_VALIDATION_ECO => 196, CommonHabilitationProfilPeer::GESTION_VALIDATION_SIP => 197, CommonHabilitationProfilPeer::RATTACHEMENT_SERVICE => 198, CommonHabilitationProfilPeer::DUPLICATION_CONSULTATIONS => 199, CommonHabilitationProfilPeer::PROJET_ACHAT_LANCEMENT_SOURCING => 200, CommonHabilitationProfilPeer::PROJET_ACHAT_INVALIDATION => 201, CommonHabilitationProfilPeer::PROJET_ACHAT_ANNULATION => 202, CommonHabilitationProfilPeer::LANCEMENT_PROCEDURE => 203, CommonHabilitationProfilPeer::RECENSEMENT_INVALIDER_PROJET_ACHAT => 204, CommonHabilitationProfilPeer::RECENSEMENT_ANNULER_PROJET_ACHAT => 205, CommonHabilitationProfilPeer::ADMINISTRATION_DOCUMENTS_MODELES => 206, CommonHabilitationProfilPeer::SUPPRIMER_CONTRAT => 207, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'LIBELLE' => 1, 'GESTION_AGENT_POLE' => 2, 'GESTION_FOURNISSEURS_ENVOIS_POSTAUX' => 3, 'GESTION_BI_CLES' => 4, 'CREER_CONSULTATION' => 5, 'MODIFIER_CONSULTATION' => 6, 'VALIDER_CONSULTATION' => 7, 'PUBLIER_CONSULTATION' => 8, 'SUIVRE_CONSULTATION' => 9, 'SUIVRE_CONSULTATION_POLE' => 10, 'INVITE_PERMANENT_ENTITE_DEPENDANTE' => 11, 'INVITE_PERMANENT_MON_ENTITE' => 12, 'INVITE_PERMANENT_TRANSVERSE' => 13, 'SUPPRIMER_ENVELOPPE' => 14, 'SUPPRIMER_CONSULTATION' => 15, 'DEPOUILLER_CANDIDATURE' => 16, 'DEPOUILLER_OFFRE' => 17, 'MESSAGERIE_SECURISEE' => 18, 'ACCES_REGISTRE_DEPOTS_PAPIER' => 19, 'ACCES_REGISTRE_RETRAITS_PAPIER' => 20, 'ACCES_REGISTRE_QUESTIONS_PAPIER' => 21, 'GERER_ENCHERES' => 22, 'SUIVRE_ENCHERES' => 23, 'SUIVI_ENTREPRISE' => 24, 'ENVOI_BOAMP' => 25, 'ACCES_CLASSEMENT_LOT' => 26, 'CONNECTEUR_SIS' => 27, 'CONNECTEUR_MARCO' => 28, 'REPONDRE_AUX_QUESTIONS' => 29, 'APPEL_PROJET_FORMATION' => 30, 'UTILISER_CLIENT_CAO' => 31, 'NOTIFICATION_BOAMP' => 32, 'ADMINISTRER_COMPTE' => 33, 'GESTION_MAPA' => 34, 'GESTION_TYPE_VALIDATION' => 35, 'APPROUVER_CONSULTATION' => 36, 'ADMINISTRER_PROCEDURE' => 37, 'RESTREINDRE_CREATION' => 38, 'CREER_LISTE_MARCHES' => 39, 'GESTION_COMMISSIONS' => 40, 'SUIVI_SEUL_CONSULTATION' => 41, 'ATTRIBUTION_MARCHE' => 42, 'FICHE_RECENSEMENT' => 43, 'DECLARER_INFRUCTUEUX' => 44, 'DECLARER_SANS_SUITE' => 45, 'CREER_CONSULTATION_TRANSVERSE' => 46, 'OUVRIR_CANDIDATURE_EN_LIGNE' => 47, 'OUVRIR_CANDIDATURE_A_DISTANCE' => 48, 'REFUSER_ENVELOPPE' => 49, 'GERER_ADMISSIBILITE' => 50, 'RESTAURER_ENVELOPPE' => 51, 'OUVRIR_OFFRE_EN_LIGNE' => 52, 'OUVRIR_ANONYMAT_EN_LIGNE' => 53, 'GESTION_COMPTE_BOAMP' => 54, 'GESTION_AGENTS' => 55, 'GESTION_HABILITATIONS' => 56, 'GERER_MAPA_INFERIEUR_MONTANT' => 57, 'GERER_MAPA_SUPERIEUR_MONTANT' => 58, 'MODIFIER_CONSULTATION_AVANT_VALIDATION' => 59, 'MODIFIER_CONSULTATION_APRES_VALIDATION' => 60, 'ACCES_REPONSES' => 61, 'TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES' => 62, 'TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES' => 63, 'OUVRIR_OFFRE_A_DISTANCE' => 64, 'CREER_ANNONCE_INFORMATION' => 65, 'SAISIE_MARCHES' => 66, 'VALIDATION_MARCHES' => 67, 'PUBLICATION_MARCHES' => 68, 'GERER_STATISTIQUES_METIER' => 69, 'GERER_ARCHIVES' => 70, 'ADMINISTRER_PROCEDURES_FORMALISEES' => 71, 'CREER_ANNONCE_ATTRIBUTION' => 72, 'ACCES_REGISTRE_RETRAITS_ELECTRONIQUE' => 73, 'ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE' => 74, 'ACCES_REGISTRE_DEPOTS_ELECTRONIQUE' => 75, 'VALIDATION_SIMPLE' => 76, 'VALIDATION_INTERMEDIAIRE' => 77, 'VALIDATION_FINALE' => 78, 'CREER_SUITE_CONSULTATION' => 79, 'HYPER_ADMIN' => 80, 'DROIT_GESTION_SERVICES' => 81, 'SUIVI_ACCES' => 82, 'STATISTIQUES_SITE' => 83, 'STATISTIQUES_QOS' => 84, 'OUVRIR_ANONYMAT_A_DISTANCE' => 85, 'GESTION_COMPTE_JAL' => 86, 'GESTION_CENTRALE_PUB' => 87, 'GESTION_COMPTE_GROUPE_MONITEUR' => 88, 'OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE' => 89, 'OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE' => 90, 'ACTIVATION_COMPTE_ENTREPRISE' => 91, 'IMPORTER_ENVELOPPE' => 92, 'SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER' => 93, 'SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER' => 94, 'SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER' => 95, 'SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE' => 96, 'SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE' => 97, 'SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE' => 98, 'MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION' => 99, 'MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION' => 100, 'MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION' => 101, 'GERER_LES_ENTREPRISES' => 102, 'PORTEE_SOCIETES_EXCLUES' => 103, 'PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES' => 104, 'MODIFIER_SOCIETES_EXCLUES' => 105, 'SUPPRIMER_SOCIETES_EXCLUES' => 106, 'RESULTAT_ANALYSE' => 107, 'GERER_ADRESSES_SERVICE' => 108, 'GERER_MON_SERVICE' => 109, 'DOWNLOAD_ARCHIVES' => 110, 'CREER_ANNONCE_EXTRAIT_PV' => 111, 'CREER_ANNONCE_RAPPORT_ACHEVEMENT' => 112, 'GESTION_CERTIFICATS_AGENT' => 113, 'CREER_AVIS_PROGRAMME_PREVISIONNEL' => 114, 'ANNULER_CONSULTATION' => 115, 'ENVOYER_PUBLICITE' => 116, 'LISTE_MARCHES_NOTIFIES' => 117, 'SUIVRE_MESSAGE' => 118, 'ENVOYER_MESSAGE' => 119, 'SUIVI_FLUX_CHORUS_TRANSVERSAL' => 120, 'GESTION_MANDATAIRE' => 121, 'GERER_NEWSLETTER' => 122, 'GESTION_MODELES_FORMULAIRE' => 123, 'GESTION_ADRESSES_FACTURATION_JAL' => 124, 'ADMINISTRER_ADRESSES_FACTURATION_JAL' => 125, 'REDACTION_DOCUMENTS_REDAC' => 126, 'VALIDATION_DOCUMENTS_REDAC' => 127, 'GESTION_MISE_DISPOSITION_PIECES_MARCHE' => 128, 'ANNUAIRE_ACHETEUR' => 129, 'REPRENDRE_INTEGRALEMENT_ARTICLE' => 130, 'ADMINISTRER_CLAUSES' => 131, 'VALIDER_CLAUSES' => 132, 'ADMINISTRER_CANEVAS' => 133, 'VALIDER_CANEVAS' => 134, 'ADMINISTRER_CLAUSES_ENTITE_ACHATS' => 135, 'GENERER_PIECES_FORMAT_ODT' => 136, 'PUBLIER_VERSION_CLAUSIER_EDITEUR' => 137, 'ADMINISTRER_CLAUSES_EDITEUR' => 138, 'VALIDER_CLAUSES_EDITEUR' => 139, 'ADMINISTRER_CANEVAS_EDITEUR' => 140, 'VALIDER_CANEVAS_EDITEUR' => 141, 'DECISION_SUIVI_SEUL' => 142, 'OUVRIR_CANDIDATURE_HORS_LIGNE' => 143, 'OUVRIR_OFFRE_HORS_LIGNE' => 144, 'OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE' => 145, 'OUVRIR_ANONYMAT_HORS_LIGNE' => 146, 'ESPACE_COLLABORATIF_GESTIONNAIRE' => 147, 'ESPACE_COLLABORATIF_CONTRIBUTEUR' => 148, 'GERER_ORGANISMES' => 149, 'GERER_ASSOCIATIONS_AGENTS' => 150, 'MODULE_REDACTION_UNIQUEMENT' => 151, 'HISTORIQUE_NAVIGATION_INSCRITS' => 152, 'TELECHARGER_ACCORDS_CADRES' => 153, 'CREER_ANNONCE_DECISION_RESILIATION' => 154, 'CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT' => 155, 'GERER_OPERATIONS' => 156, 'TELECHARGER_SIRET_ACHETEUR' => 157, 'GERER_REOUVERTURES_MODIFICATION' => 158, 'ACCEDER_TOUS_TELECHARGEMENTS' => 159, 'CREER_CONTRAT' => 160, 'MODIFIER_CONTRAT' => 161, 'CONSULTER_CONTRAT' => 162, 'GERER_NEWSLETTER_REDAC' => 163, 'PROFIL_RMA' => 164, 'AFFECTATION_VISION_RMA' => 165, 'GERER_GABARIT_EDITEUR' => 166, 'GERER_GABARIT' => 167, 'GERER_GABARIT_ENTITE_ACHATS' => 168, 'GERER_GABARIT_AGENT' => 169, 'GERER_MESSAGES_ACCUEIL' => 170, 'GERER_OA_GA' => 171, 'DEPLACER_SERVICE' => 172, 'ACTIVER_VERSION_CLAUSIER' => 173, 'EXEC_VOIR_CONTRATS_EA' => 174, 'EXEC_VOIR_CONTRATS_EA_DEPENDANTES' => 175, 'EXEC_VOIR_CONTRATS_ORGANISME' => 176, 'ACCES_WS' => 177, 'ACCES_ECHANGE_DOCUMENTAIRE' => 178, 'ESPACE_DOCUMENTAIRE_CONSULTATION' => 179, 'ADMINISTRER_ORGANISME' => 180, 'EXEC_MODIFICATION_CONTRAT' => 181, 'BESOIN_UNITAIRE_CONSULTATION' => 182, 'BESOIN_UNITAIRE_CREATION_MODIFICATION' => 183, 'DEMANDE_ACHAT_CONSULTATION' => 184, 'DEMANDE_ACHAT_CREATION_MODIFICATION' => 185, 'PROJET_ACHAT_CONSULTATION' => 186, 'PROJET_ACHAT_CREATION_MODIFICATION' => 187, 'VALIDATION_OPPORTUNITE' => 188, 'VALIDATION_ACHAT' => 189, 'VALIDATION_BUDGET' => 190, 'STRATEGIE_ACHAT_GESTION' => 191, 'RECENSEMENT_PROGRAMMATION_ADMINISTRATION' => 192, 'GESTION_ENVOL' => 193, 'VALIDER_PROJET_ACHAT' => 194, 'GESTION_SPASER_CONSULTATIONS' => 195, 'GESTION_VALIDATION_ECO' => 196, 'GESTION_VALIDATION_SIP' => 197, 'RATTACHEMENT_SERVICE' => 198, 'DUPLICATION_CONSULTATIONS' => 199, 'PROJET_ACHAT_LANCEMENT_SOURCING' => 200, 'PROJET_ACHAT_INVALIDATION' => 201, 'PROJET_ACHAT_ANNULATION' => 202, 'LANCEMENT_PROCEDURE' => 203, 'RECENSEMENT_INVALIDER_PROJET_ACHAT' => 204, 'RECENSEMENT_ANNULER_PROJET_ACHAT' => 205, 'ADMINISTRATION_DOCUMENTS_MODELES' => 206, 'SUPPRIMER_CONTRAT' => 207, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'libelle' => 1, 'gestion_agent_pole' => 2, 'gestion_fournisseurs_envois_postaux' => 3, 'gestion_bi_cles' => 4, 'creer_consultation' => 5, 'modifier_consultation' => 6, 'valider_consultation' => 7, 'publier_consultation' => 8, 'suivre_consultation' => 9, 'suivre_consultation_pole' => 10, 'invite_permanent_entite_dependante' => 11, 'invite_permanent_mon_entite' => 12, 'invite_permanent_transverse' => 13, 'supprimer_enveloppe' => 14, 'supprimer_consultation' => 15, 'depouiller_candidature' => 16, 'depouiller_offre' => 17, 'messagerie_securisee' => 18, 'acces_registre_depots_papier' => 19, 'acces_registre_retraits_papier' => 20, 'acces_registre_questions_papier' => 21, 'gerer_encheres' => 22, 'suivre_encheres' => 23, 'suivi_entreprise' => 24, 'envoi_boamp' => 25, 'acces_classement_lot' => 26, 'connecteur_sis' => 27, 'connecteur_marco' => 28, 'repondre_aux_questions' => 29, 'appel_projet_formation' => 30, 'utiliser_client_CAO' => 31, 'notification_boamp' => 32, 'administrer_compte' => 33, 'gestion_mapa' => 34, 'gestion_type_validation' => 35, 'approuver_consultation' => 36, 'administrer_procedure' => 37, 'restreindre_creation' => 38, 'creer_liste_marches' => 39, 'gestion_commissions' => 40, 'suivi_seul_consultation' => 41, 'attribution_marche' => 42, 'fiche_recensement' => 43, 'declarer_infructueux' => 44, 'declarer_sans_suite' => 45, 'creer_consultation_transverse' => 46, 'ouvrir_candidature_en_ligne' => 47, 'ouvrir_candidature_a_distance' => 48, 'refuser_enveloppe' => 49, 'gerer_admissibilite' => 50, 'restaurer_enveloppe' => 51, 'ouvrir_offre_en_ligne' => 52, 'ouvrir_anonymat_en_ligne' => 53, 'gestion_compte_boamp' => 54, 'gestion_agents' => 55, 'gestion_habilitations' => 56, 'gerer_mapa_inferieur_montant' => 57, 'gerer_mapa_superieur_montant' => 58, 'modifier_consultation_avant_validation' => 59, 'modifier_consultation_apres_validation' => 60, 'acces_reponses' => 61, 'telechargement_groupe_anticipe_plis_chiffres' => 62, 'telechargement_unitaire_plis_chiffres' => 63, 'ouvrir_offre_a_distance' => 64, 'creer_annonce_information' => 65, 'saisie_marches' => 66, 'validation_marches' => 67, 'publication_marches' => 68, 'gerer_statistiques_metier' => 69, 'gerer_archives' => 70, 'administrer_procedures_formalisees' => 71, 'creer_annonce_attribution' => 72, 'acces_registre_retraits_electronique' => 73, 'acces_registre_questions_electronique' => 74, 'acces_registre_depots_electronique' => 75, 'validation_simple' => 76, 'validation_intermediaire' => 77, 'validation_finale' => 78, 'creer_suite_consultation' => 79, 'hyper_admin' => 80, 'droit_gestion_services' => 81, 'suivi_acces' => 82, 'statistiques_site' => 83, 'statistiques_QoS' => 84, 'ouvrir_anonymat_a_distance' => 85, 'gestion_compte_jal' => 86, 'gestion_centrale_pub' => 87, 'Gestion_Compte_Groupe_Moniteur' => 88, 'ouvrir_offre_technique_en_ligne' => 89, 'ouvrir_offre_technique_a_distance' => 90, 'activation_compte_entreprise' => 91, 'importer_enveloppe' => 92, 'suivi_seul_registre_depots_papier' => 93, 'suivi_seul_registre_retraits_papier' => 94, 'suivi_seul_registre_questions_papier' => 95, 'suivi_seul_registre_depots_electronique' => 96, 'suivi_seul_registre_retraits_electronique' => 97, 'suivi_seul_registre_questions_electronique' => 98, 'modifier_consultation_mapa_inferieur_montant_apres_validation' => 99, 'modifier_consultation_mapa_superieur_montant_apres_validation' => 100, 'modifier_consultation_procedures_formalisees_apres_validation' => 101, 'gerer_les_entreprises' => 102, 'portee_societes_exclues' => 103, 'portee_societes_exclues_tous_organismes' => 104, 'modifier_societes_exclues' => 105, 'supprimer_societes_exclues' => 106, 'resultat_analyse' => 107, 'gerer_adresses_service' => 108, 'gerer_mon_service' => 109, 'download_archives' => 110, 'creer_annonce_extrait_pv' => 111, 'creer_annonce_rapport_achevement' => 112, 'gestion_certificats_agent' => 113, 'creer_avis_programme_previsionnel' => 114, 'annuler_consultation' => 115, 'envoyer_publicite' => 116, 'liste_marches_notifies' => 117, 'suivre_message' => 118, 'envoyer_message' => 119, 'suivi_flux_chorus_transversal' => 120, 'gestion_mandataire' => 121, 'gerer_newsletter' => 122, 'gestion_modeles_formulaire' => 123, 'gestion_adresses_facturation_jal' => 124, 'administrer_adresses_facturation_jal' => 125, 'redaction_documents_redac' => 126, 'validation_documents_redac' => 127, 'gestion_mise_disposition_pieces_marche' => 128, 'annuaire_acheteur' => 129, 'reprendre_integralement_article' => 130, 'administrer_clauses' => 131, 'valider_clauses' => 132, 'administrer_canevas' => 133, 'valider_canevas' => 134, 'administrer_clauses_entite_achats' => 135, 'generer_pieces_format_odt' => 136, 'publier_version_clausier_editeur' => 137, 'administrer_clauses_editeur' => 138, 'valider_clauses_editeur' => 139, 'administrer_canevas_editeur' => 140, 'valider_canevas_editeur' => 141, 'decision_suivi_seul' => 142, 'ouvrir_candidature_hors_ligne' => 143, 'ouvrir_offre_hors_ligne' => 144, 'ouvrir_offre_technique_hors_ligne' => 145, 'ouvrir_anonymat_hors_ligne' => 146, 'espace_collaboratif_gestionnaire' => 147, 'espace_collaboratif_contributeur' => 148, 'gerer_organismes' => 149, 'gerer_associations_agents' => 150, 'module_redaction_uniquement' => 151, 'historique_navigation_inscrits' => 152, 'telecharger_accords_cadres' => 153, 'creer_annonce_decision_resiliation' => 154, 'creer_annonce_synthese_rapport_audit' => 155, 'gerer_operations' => 156, 'telecharger_siret_acheteur' => 157, 'gerer_reouvertures_modification' => 158, 'acceder_tous_telechargements' => 159, 'creer_contrat' => 160, 'modifier_contrat' => 161, 'consulter_contrat' => 162, 'gerer_newsletter_redac' => 163, 'profil_rma' => 164, 'affectation_vision_rma' => 165, 'gerer_gabarit_editeur' => 166, 'gerer_gabarit' => 167, 'gerer_gabarit_entite_achats' => 168, 'gerer_gabarit_agent' => 169, 'gerer_messages_accueil' => 170, 'gerer_OA_GA' => 171, 'deplacer_service' => 172, 'activer_version_clausier' => 173, 'exec_voir_contrats_ea' => 174, 'exec_voir_contrats_ea_dependantes' => 175, 'exec_voir_contrats_organisme' => 176, 'acces_ws' => 177, 'acces_echange_documentaire' => 178, 'espace_documentaire_consultation' => 179, 'administrer_organisme' => 180, 'exec_modification_contrat' => 181, 'besoin_unitaire_consultation' => 182, 'besoin_unitaire_creation_modification' => 183, 'demande_achat_consultation' => 184, 'demande_achat_creation_modification' => 185, 'projet_achat_consultation' => 186, 'projet_achat_creation_modification' => 187, 'validation_opportunite' => 188, 'validation_achat' => 189, 'validation_budget' => 190, 'strategie_achat_gestion' => 191, 'recensement_programmation_administration' => 192, 'gestion_envol' => 193, 'valider_projet_achat' => 194, 'gestion_spaser_consultations' => 195, 'gestion_validation_eco' => 196, 'gestion_validation_sip' => 197, 'rattachement_service' => 198, 'duplication_consultations' => 199, 'projet_achat_lancement_sourcing' => 200, 'projet_achat_invalidation' => 201, 'projet_achat_annulation' => 202, 'lancement_procedure' => 203, 'recensement_invalider_projet_achat' => 204, 'recensement_annuler_projet_achat' => 205, 'administration_documents_modeles' => 206, 'supprimer_contrat' => 207, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonHabilitationProfilPeer::GESTION_AGENT_POLE => array(
            CommonHabilitationProfilPeer::GESTION_AGENT_POLE_0,
            CommonHabilitationProfilPeer::GESTION_AGENT_POLE_1,
        ),
        CommonHabilitationProfilPeer::GESTION_FOURNISSEURS_ENVOIS_POSTAUX => array(
            CommonHabilitationProfilPeer::GESTION_FOURNISSEURS_ENVOIS_POSTAUX_0,
            CommonHabilitationProfilPeer::GESTION_FOURNISSEURS_ENVOIS_POSTAUX_1,
        ),
        CommonHabilitationProfilPeer::GESTION_BI_CLES => array(
            CommonHabilitationProfilPeer::GESTION_BI_CLES_0,
            CommonHabilitationProfilPeer::GESTION_BI_CLES_1,
        ),
        CommonHabilitationProfilPeer::CREER_CONSULTATION => array(
            CommonHabilitationProfilPeer::CREER_CONSULTATION_0,
            CommonHabilitationProfilPeer::CREER_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::MODIFIER_CONSULTATION => array(
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_0,
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::VALIDER_CONSULTATION => array(
            CommonHabilitationProfilPeer::VALIDER_CONSULTATION_0,
            CommonHabilitationProfilPeer::VALIDER_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::PUBLIER_CONSULTATION => array(
            CommonHabilitationProfilPeer::PUBLIER_CONSULTATION_0,
            CommonHabilitationProfilPeer::PUBLIER_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::SUIVRE_CONSULTATION => array(
            CommonHabilitationProfilPeer::SUIVRE_CONSULTATION_0,
            CommonHabilitationProfilPeer::SUIVRE_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::SUIVRE_CONSULTATION_POLE => array(
            CommonHabilitationProfilPeer::SUIVRE_CONSULTATION_POLE_0,
            CommonHabilitationProfilPeer::SUIVRE_CONSULTATION_POLE_1,
        ),
        CommonHabilitationProfilPeer::INVITE_PERMANENT_ENTITE_DEPENDANTE => array(
            CommonHabilitationProfilPeer::INVITE_PERMANENT_ENTITE_DEPENDANTE_0,
            CommonHabilitationProfilPeer::INVITE_PERMANENT_ENTITE_DEPENDANTE_1,
        ),
        CommonHabilitationProfilPeer::INVITE_PERMANENT_MON_ENTITE => array(
            CommonHabilitationProfilPeer::INVITE_PERMANENT_MON_ENTITE_0,
            CommonHabilitationProfilPeer::INVITE_PERMANENT_MON_ENTITE_1,
        ),
        CommonHabilitationProfilPeer::INVITE_PERMANENT_TRANSVERSE => array(
            CommonHabilitationProfilPeer::INVITE_PERMANENT_TRANSVERSE_0,
            CommonHabilitationProfilPeer::INVITE_PERMANENT_TRANSVERSE_1,
        ),
        CommonHabilitationProfilPeer::SUPPRIMER_ENVELOPPE => array(
            CommonHabilitationProfilPeer::SUPPRIMER_ENVELOPPE_0,
            CommonHabilitationProfilPeer::SUPPRIMER_ENVELOPPE_1,
        ),
        CommonHabilitationProfilPeer::SUPPRIMER_CONSULTATION => array(
            CommonHabilitationProfilPeer::SUPPRIMER_CONSULTATION_0,
            CommonHabilitationProfilPeer::SUPPRIMER_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::DEPOUILLER_CANDIDATURE => array(
            CommonHabilitationProfilPeer::DEPOUILLER_CANDIDATURE_0,
            CommonHabilitationProfilPeer::DEPOUILLER_CANDIDATURE_1,
        ),
        CommonHabilitationProfilPeer::DEPOUILLER_OFFRE => array(
            CommonHabilitationProfilPeer::DEPOUILLER_OFFRE_0,
            CommonHabilitationProfilPeer::DEPOUILLER_OFFRE_1,
        ),
        CommonHabilitationProfilPeer::MESSAGERIE_SECURISEE => array(
            CommonHabilitationProfilPeer::MESSAGERIE_SECURISEE_0,
            CommonHabilitationProfilPeer::MESSAGERIE_SECURISEE_1,
        ),
        CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_PAPIER => array(
            CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_PAPIER_0,
            CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_PAPIER_1,
        ),
        CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_PAPIER => array(
            CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_PAPIER_0,
            CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_PAPIER_1,
        ),
        CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_PAPIER => array(
            CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_PAPIER_0,
            CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_PAPIER_1,
        ),
        CommonHabilitationProfilPeer::GERER_ENCHERES => array(
            CommonHabilitationProfilPeer::GERER_ENCHERES_0,
            CommonHabilitationProfilPeer::GERER_ENCHERES_1,
        ),
        CommonHabilitationProfilPeer::SUIVRE_ENCHERES => array(
            CommonHabilitationProfilPeer::SUIVRE_ENCHERES_0,
            CommonHabilitationProfilPeer::SUIVRE_ENCHERES_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_ENTREPRISE => array(
            CommonHabilitationProfilPeer::SUIVI_ENTREPRISE_0,
            CommonHabilitationProfilPeer::SUIVI_ENTREPRISE_1,
        ),
        CommonHabilitationProfilPeer::ENVOI_BOAMP => array(
            CommonHabilitationProfilPeer::ENVOI_BOAMP_0,
            CommonHabilitationProfilPeer::ENVOI_BOAMP_1,
        ),
        CommonHabilitationProfilPeer::ACCES_CLASSEMENT_LOT => array(
            CommonHabilitationProfilPeer::ACCES_CLASSEMENT_LOT_0,
            CommonHabilitationProfilPeer::ACCES_CLASSEMENT_LOT_1,
        ),
        CommonHabilitationProfilPeer::CONNECTEUR_SIS => array(
            CommonHabilitationProfilPeer::CONNECTEUR_SIS_0,
            CommonHabilitationProfilPeer::CONNECTEUR_SIS_1,
        ),
        CommonHabilitationProfilPeer::CONNECTEUR_MARCO => array(
            CommonHabilitationProfilPeer::CONNECTEUR_MARCO_0,
            CommonHabilitationProfilPeer::CONNECTEUR_MARCO_1,
        ),
        CommonHabilitationProfilPeer::REPONDRE_AUX_QUESTIONS => array(
            CommonHabilitationProfilPeer::REPONDRE_AUX_QUESTIONS_0,
            CommonHabilitationProfilPeer::REPONDRE_AUX_QUESTIONS_1,
        ),
        CommonHabilitationProfilPeer::APPEL_PROJET_FORMATION => array(
            CommonHabilitationProfilPeer::APPEL_PROJET_FORMATION_0,
            CommonHabilitationProfilPeer::APPEL_PROJET_FORMATION_1,
        ),
        CommonHabilitationProfilPeer::UTILISER_CLIENT_CAO => array(
            CommonHabilitationProfilPeer::UTILISER_CLIENT_CAO_0,
            CommonHabilitationProfilPeer::UTILISER_CLIENT_CAO_1,
        ),
        CommonHabilitationProfilPeer::NOTIFICATION_BOAMP => array(
            CommonHabilitationProfilPeer::NOTIFICATION_BOAMP_0,
            CommonHabilitationProfilPeer::NOTIFICATION_BOAMP_1,
        ),
        CommonHabilitationProfilPeer::ADMINISTRER_COMPTE => array(
            CommonHabilitationProfilPeer::ADMINISTRER_COMPTE_0,
            CommonHabilitationProfilPeer::ADMINISTRER_COMPTE_1,
        ),
        CommonHabilitationProfilPeer::GESTION_MAPA => array(
            CommonHabilitationProfilPeer::GESTION_MAPA_0,
            CommonHabilitationProfilPeer::GESTION_MAPA_1,
        ),
        CommonHabilitationProfilPeer::GESTION_TYPE_VALIDATION => array(
            CommonHabilitationProfilPeer::GESTION_TYPE_VALIDATION_0,
            CommonHabilitationProfilPeer::GESTION_TYPE_VALIDATION_1,
        ),
        CommonHabilitationProfilPeer::APPROUVER_CONSULTATION => array(
            CommonHabilitationProfilPeer::APPROUVER_CONSULTATION_0,
            CommonHabilitationProfilPeer::APPROUVER_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURE => array(
            CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURE_0,
            CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURE_1,
        ),
        CommonHabilitationProfilPeer::RESTREINDRE_CREATION => array(
            CommonHabilitationProfilPeer::RESTREINDRE_CREATION_0,
            CommonHabilitationProfilPeer::RESTREINDRE_CREATION_1,
        ),
        CommonHabilitationProfilPeer::CREER_LISTE_MARCHES => array(
            CommonHabilitationProfilPeer::CREER_LISTE_MARCHES_0,
            CommonHabilitationProfilPeer::CREER_LISTE_MARCHES_1,
        ),
        CommonHabilitationProfilPeer::GESTION_COMMISSIONS => array(
            CommonHabilitationProfilPeer::GESTION_COMMISSIONS_0,
            CommonHabilitationProfilPeer::GESTION_COMMISSIONS_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_SEUL_CONSULTATION => array(
            CommonHabilitationProfilPeer::SUIVI_SEUL_CONSULTATION_0,
            CommonHabilitationProfilPeer::SUIVI_SEUL_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::ATTRIBUTION_MARCHE => array(
            CommonHabilitationProfilPeer::ATTRIBUTION_MARCHE_0,
            CommonHabilitationProfilPeer::ATTRIBUTION_MARCHE_1,
        ),
        CommonHabilitationProfilPeer::FICHE_RECENSEMENT => array(
            CommonHabilitationProfilPeer::FICHE_RECENSEMENT_0,
            CommonHabilitationProfilPeer::FICHE_RECENSEMENT_1,
        ),
        CommonHabilitationProfilPeer::DECLARER_INFRUCTUEUX => array(
            CommonHabilitationProfilPeer::DECLARER_INFRUCTUEUX_0,
            CommonHabilitationProfilPeer::DECLARER_INFRUCTUEUX_1,
        ),
        CommonHabilitationProfilPeer::DECLARER_SANS_SUITE => array(
            CommonHabilitationProfilPeer::DECLARER_SANS_SUITE_0,
            CommonHabilitationProfilPeer::DECLARER_SANS_SUITE_1,
        ),
        CommonHabilitationProfilPeer::CREER_CONSULTATION_TRANSVERSE => array(
            CommonHabilitationProfilPeer::CREER_CONSULTATION_TRANSVERSE_0,
            CommonHabilitationProfilPeer::CREER_CONSULTATION_TRANSVERSE_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_EN_LIGNE => array(
            CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_EN_LIGNE_0,
            CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_EN_LIGNE_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_A_DISTANCE => array(
            CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_A_DISTANCE_0,
            CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_A_DISTANCE_1,
        ),
        CommonHabilitationProfilPeer::REFUSER_ENVELOPPE => array(
            CommonHabilitationProfilPeer::REFUSER_ENVELOPPE_0,
            CommonHabilitationProfilPeer::REFUSER_ENVELOPPE_1,
        ),
        CommonHabilitationProfilPeer::GERER_ADMISSIBILITE => array(
            CommonHabilitationProfilPeer::GERER_ADMISSIBILITE_0,
            CommonHabilitationProfilPeer::GERER_ADMISSIBILITE_1,
        ),
        CommonHabilitationProfilPeer::RESTAURER_ENVELOPPE => array(
            CommonHabilitationProfilPeer::RESTAURER_ENVELOPPE_0,
            CommonHabilitationProfilPeer::RESTAURER_ENVELOPPE_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_OFFRE_EN_LIGNE => array(
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_EN_LIGNE_0,
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_EN_LIGNE_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_EN_LIGNE => array(
            CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_EN_LIGNE_0,
            CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_EN_LIGNE_1,
        ),
        CommonHabilitationProfilPeer::GESTION_COMPTE_BOAMP => array(
            CommonHabilitationProfilPeer::GESTION_COMPTE_BOAMP_0,
            CommonHabilitationProfilPeer::GESTION_COMPTE_BOAMP_1,
        ),
        CommonHabilitationProfilPeer::GESTION_AGENTS => array(
            CommonHabilitationProfilPeer::GESTION_AGENTS_0,
            CommonHabilitationProfilPeer::GESTION_AGENTS_1,
        ),
        CommonHabilitationProfilPeer::GESTION_HABILITATIONS => array(
            CommonHabilitationProfilPeer::GESTION_HABILITATIONS_0,
            CommonHabilitationProfilPeer::GESTION_HABILITATIONS_1,
        ),
        CommonHabilitationProfilPeer::GERER_MAPA_INFERIEUR_MONTANT => array(
            CommonHabilitationProfilPeer::GERER_MAPA_INFERIEUR_MONTANT_0,
            CommonHabilitationProfilPeer::GERER_MAPA_INFERIEUR_MONTANT_1,
        ),
        CommonHabilitationProfilPeer::GERER_MAPA_SUPERIEUR_MONTANT => array(
            CommonHabilitationProfilPeer::GERER_MAPA_SUPERIEUR_MONTANT_0,
            CommonHabilitationProfilPeer::GERER_MAPA_SUPERIEUR_MONTANT_1,
        ),
        CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_AVANT_VALIDATION => array(
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_AVANT_VALIDATION_0,
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_AVANT_VALIDATION_1,
        ),
        CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_APRES_VALIDATION => array(
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_APRES_VALIDATION_0,
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_APRES_VALIDATION_1,
        ),
        CommonHabilitationProfilPeer::ACCES_REPONSES => array(
            CommonHabilitationProfilPeer::ACCES_REPONSES_0,
            CommonHabilitationProfilPeer::ACCES_REPONSES_1,
        ),
        CommonHabilitationProfilPeer::TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES => array(
            CommonHabilitationProfilPeer::TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES_0,
            CommonHabilitationProfilPeer::TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES_1,
        ),
        CommonHabilitationProfilPeer::TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES => array(
            CommonHabilitationProfilPeer::TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES_0,
            CommonHabilitationProfilPeer::TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_OFFRE_A_DISTANCE => array(
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_A_DISTANCE_0,
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_A_DISTANCE_1,
        ),
        CommonHabilitationProfilPeer::CREER_ANNONCE_INFORMATION => array(
            CommonHabilitationProfilPeer::CREER_ANNONCE_INFORMATION_0,
            CommonHabilitationProfilPeer::CREER_ANNONCE_INFORMATION_1,
        ),
        CommonHabilitationProfilPeer::SAISIE_MARCHES => array(
            CommonHabilitationProfilPeer::SAISIE_MARCHES_0,
            CommonHabilitationProfilPeer::SAISIE_MARCHES_1,
        ),
        CommonHabilitationProfilPeer::VALIDATION_MARCHES => array(
            CommonHabilitationProfilPeer::VALIDATION_MARCHES_0,
            CommonHabilitationProfilPeer::VALIDATION_MARCHES_1,
        ),
        CommonHabilitationProfilPeer::PUBLICATION_MARCHES => array(
            CommonHabilitationProfilPeer::PUBLICATION_MARCHES_0,
            CommonHabilitationProfilPeer::PUBLICATION_MARCHES_1,
        ),
        CommonHabilitationProfilPeer::GERER_STATISTIQUES_METIER => array(
            CommonHabilitationProfilPeer::GERER_STATISTIQUES_METIER_0,
            CommonHabilitationProfilPeer::GERER_STATISTIQUES_METIER_1,
        ),
        CommonHabilitationProfilPeer::GERER_ARCHIVES => array(
            CommonHabilitationProfilPeer::GERER_ARCHIVES_0,
            CommonHabilitationProfilPeer::GERER_ARCHIVES_1,
        ),
        CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURES_FORMALISEES => array(
            CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURES_FORMALISEES_0,
            CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURES_FORMALISEES_1,
        ),
        CommonHabilitationProfilPeer::CREER_ANNONCE_ATTRIBUTION => array(
            CommonHabilitationProfilPeer::CREER_ANNONCE_ATTRIBUTION_0,
            CommonHabilitationProfilPeer::CREER_ANNONCE_ATTRIBUTION_1,
        ),
        CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_ELECTRONIQUE => array(
            CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_ELECTRONIQUE_0,
            CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_ELECTRONIQUE_1,
        ),
        CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE => array(
            CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE_0,
            CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE_1,
        ),
        CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_ELECTRONIQUE => array(
            CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_ELECTRONIQUE_0,
            CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_ELECTRONIQUE_1,
        ),
        CommonHabilitationProfilPeer::VALIDATION_SIMPLE => array(
            CommonHabilitationProfilPeer::VALIDATION_SIMPLE_0,
            CommonHabilitationProfilPeer::VALIDATION_SIMPLE_1,
        ),
        CommonHabilitationProfilPeer::VALIDATION_INTERMEDIAIRE => array(
            CommonHabilitationProfilPeer::VALIDATION_INTERMEDIAIRE_0,
            CommonHabilitationProfilPeer::VALIDATION_INTERMEDIAIRE_1,
        ),
        CommonHabilitationProfilPeer::VALIDATION_FINALE => array(
            CommonHabilitationProfilPeer::VALIDATION_FINALE_0,
            CommonHabilitationProfilPeer::VALIDATION_FINALE_1,
        ),
        CommonHabilitationProfilPeer::CREER_SUITE_CONSULTATION => array(
            CommonHabilitationProfilPeer::CREER_SUITE_CONSULTATION_0,
            CommonHabilitationProfilPeer::CREER_SUITE_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::HYPER_ADMIN => array(
            CommonHabilitationProfilPeer::HYPER_ADMIN_0,
            CommonHabilitationProfilPeer::HYPER_ADMIN_1,
        ),
        CommonHabilitationProfilPeer::DROIT_GESTION_SERVICES => array(
            CommonHabilitationProfilPeer::DROIT_GESTION_SERVICES_0,
            CommonHabilitationProfilPeer::DROIT_GESTION_SERVICES_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_ACCES => array(
            CommonHabilitationProfilPeer::SUIVI_ACCES_0,
            CommonHabilitationProfilPeer::SUIVI_ACCES_1,
        ),
        CommonHabilitationProfilPeer::STATISTIQUES_SITE => array(
            CommonHabilitationProfilPeer::STATISTIQUES_SITE_0,
            CommonHabilitationProfilPeer::STATISTIQUES_SITE_1,
        ),
        CommonHabilitationProfilPeer::STATISTIQUES_QOS => array(
            CommonHabilitationProfilPeer::STATISTIQUES_QOS_0,
            CommonHabilitationProfilPeer::STATISTIQUES_QOS_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_A_DISTANCE => array(
            CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_A_DISTANCE_0,
            CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_A_DISTANCE_1,
        ),
        CommonHabilitationProfilPeer::GESTION_COMPTE_JAL => array(
            CommonHabilitationProfilPeer::GESTION_COMPTE_JAL_0,
            CommonHabilitationProfilPeer::GESTION_COMPTE_JAL_1,
        ),
        CommonHabilitationProfilPeer::GESTION_CENTRALE_PUB => array(
            CommonHabilitationProfilPeer::GESTION_CENTRALE_PUB_0,
            CommonHabilitationProfilPeer::GESTION_CENTRALE_PUB_1,
        ),
        CommonHabilitationProfilPeer::GESTION_COMPTE_GROUPE_MONITEUR => array(
            CommonHabilitationProfilPeer::GESTION_COMPTE_GROUPE_MONITEUR_0,
            CommonHabilitationProfilPeer::GESTION_COMPTE_GROUPE_MONITEUR_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE => array(
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE_0,
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE => array(
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE_0,
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE_1,
        ),
        CommonHabilitationProfilPeer::ACTIVATION_COMPTE_ENTREPRISE => array(
            CommonHabilitationProfilPeer::ACTIVATION_COMPTE_ENTREPRISE_0,
            CommonHabilitationProfilPeer::ACTIVATION_COMPTE_ENTREPRISE_1,
        ),
        CommonHabilitationProfilPeer::IMPORTER_ENVELOPPE => array(
            CommonHabilitationProfilPeer::IMPORTER_ENVELOPPE_0,
            CommonHabilitationProfilPeer::IMPORTER_ENVELOPPE_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER => array(
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER_0,
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER => array(
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER_0,
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER => array(
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER_0,
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE => array(
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE_0,
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE => array(
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE_0,
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE => array(
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE_0,
            CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE_1,
        ),
        CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION => array(
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION_0,
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION_1,
        ),
        CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION => array(
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION_0,
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION_1,
        ),
        CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION => array(
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION_0,
            CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION_1,
        ),
        CommonHabilitationProfilPeer::GERER_LES_ENTREPRISES => array(
            CommonHabilitationProfilPeer::GERER_LES_ENTREPRISES_0,
            CommonHabilitationProfilPeer::GERER_LES_ENTREPRISES_1,
        ),
        CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES => array(
            CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES_0,
            CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES_1,
        ),
        CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES => array(
            CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES_0,
            CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES_1,
        ),
        CommonHabilitationProfilPeer::MODIFIER_SOCIETES_EXCLUES => array(
            CommonHabilitationProfilPeer::MODIFIER_SOCIETES_EXCLUES_0,
            CommonHabilitationProfilPeer::MODIFIER_SOCIETES_EXCLUES_1,
        ),
        CommonHabilitationProfilPeer::SUPPRIMER_SOCIETES_EXCLUES => array(
            CommonHabilitationProfilPeer::SUPPRIMER_SOCIETES_EXCLUES_0,
            CommonHabilitationProfilPeer::SUPPRIMER_SOCIETES_EXCLUES_1,
        ),
        CommonHabilitationProfilPeer::RESULTAT_ANALYSE => array(
            CommonHabilitationProfilPeer::RESULTAT_ANALYSE_0,
            CommonHabilitationProfilPeer::RESULTAT_ANALYSE_1,
        ),
        CommonHabilitationProfilPeer::GERER_ADRESSES_SERVICE => array(
            CommonHabilitationProfilPeer::GERER_ADRESSES_SERVICE_0,
            CommonHabilitationProfilPeer::GERER_ADRESSES_SERVICE_1,
        ),
        CommonHabilitationProfilPeer::GERER_MON_SERVICE => array(
            CommonHabilitationProfilPeer::GERER_MON_SERVICE_0,
            CommonHabilitationProfilPeer::GERER_MON_SERVICE_1,
        ),
        CommonHabilitationProfilPeer::DOWNLOAD_ARCHIVES => array(
            CommonHabilitationProfilPeer::DOWNLOAD_ARCHIVES_0,
            CommonHabilitationProfilPeer::DOWNLOAD_ARCHIVES_1,
        ),
        CommonHabilitationProfilPeer::CREER_ANNONCE_EXTRAIT_PV => array(
            CommonHabilitationProfilPeer::CREER_ANNONCE_EXTRAIT_PV_0,
            CommonHabilitationProfilPeer::CREER_ANNONCE_EXTRAIT_PV_1,
        ),
        CommonHabilitationProfilPeer::CREER_ANNONCE_RAPPORT_ACHEVEMENT => array(
            CommonHabilitationProfilPeer::CREER_ANNONCE_RAPPORT_ACHEVEMENT_0,
            CommonHabilitationProfilPeer::CREER_ANNONCE_RAPPORT_ACHEVEMENT_1,
        ),
        CommonHabilitationProfilPeer::GESTION_CERTIFICATS_AGENT => array(
            CommonHabilitationProfilPeer::GESTION_CERTIFICATS_AGENT_0,
            CommonHabilitationProfilPeer::GESTION_CERTIFICATS_AGENT_1,
        ),
        CommonHabilitationProfilPeer::CREER_AVIS_PROGRAMME_PREVISIONNEL => array(
            CommonHabilitationProfilPeer::CREER_AVIS_PROGRAMME_PREVISIONNEL_0,
            CommonHabilitationProfilPeer::CREER_AVIS_PROGRAMME_PREVISIONNEL_1,
        ),
        CommonHabilitationProfilPeer::ANNULER_CONSULTATION => array(
            CommonHabilitationProfilPeer::ANNULER_CONSULTATION_0,
            CommonHabilitationProfilPeer::ANNULER_CONSULTATION_1,
        ),
        CommonHabilitationProfilPeer::ENVOYER_PUBLICITE => array(
            CommonHabilitationProfilPeer::ENVOYER_PUBLICITE_0,
            CommonHabilitationProfilPeer::ENVOYER_PUBLICITE_1,
        ),
        CommonHabilitationProfilPeer::LISTE_MARCHES_NOTIFIES => array(
            CommonHabilitationProfilPeer::LISTE_MARCHES_NOTIFIES_0,
            CommonHabilitationProfilPeer::LISTE_MARCHES_NOTIFIES_1,
        ),
        CommonHabilitationProfilPeer::SUIVRE_MESSAGE => array(
            CommonHabilitationProfilPeer::SUIVRE_MESSAGE_0,
            CommonHabilitationProfilPeer::SUIVRE_MESSAGE_1,
        ),
        CommonHabilitationProfilPeer::ENVOYER_MESSAGE => array(
            CommonHabilitationProfilPeer::ENVOYER_MESSAGE_0,
            CommonHabilitationProfilPeer::ENVOYER_MESSAGE_1,
        ),
        CommonHabilitationProfilPeer::SUIVI_FLUX_CHORUS_TRANSVERSAL => array(
            CommonHabilitationProfilPeer::SUIVI_FLUX_CHORUS_TRANSVERSAL_0,
            CommonHabilitationProfilPeer::SUIVI_FLUX_CHORUS_TRANSVERSAL_1,
        ),
        CommonHabilitationProfilPeer::GESTION_MANDATAIRE => array(
            CommonHabilitationProfilPeer::GESTION_MANDATAIRE_0,
            CommonHabilitationProfilPeer::GESTION_MANDATAIRE_1,
        ),
        CommonHabilitationProfilPeer::GERER_NEWSLETTER => array(
            CommonHabilitationProfilPeer::GERER_NEWSLETTER_0,
            CommonHabilitationProfilPeer::GERER_NEWSLETTER_1,
        ),
        CommonHabilitationProfilPeer::GESTION_MODELES_FORMULAIRE => array(
            CommonHabilitationProfilPeer::GESTION_MODELES_FORMULAIRE_0,
            CommonHabilitationProfilPeer::GESTION_MODELES_FORMULAIRE_1,
        ),
        CommonHabilitationProfilPeer::GESTION_ADRESSES_FACTURATION_JAL => array(
            CommonHabilitationProfilPeer::GESTION_ADRESSES_FACTURATION_JAL_0,
            CommonHabilitationProfilPeer::GESTION_ADRESSES_FACTURATION_JAL_1,
        ),
        CommonHabilitationProfilPeer::ADMINISTRER_ADRESSES_FACTURATION_JAL => array(
            CommonHabilitationProfilPeer::ADMINISTRER_ADRESSES_FACTURATION_JAL_0,
            CommonHabilitationProfilPeer::ADMINISTRER_ADRESSES_FACTURATION_JAL_1,
        ),
        CommonHabilitationProfilPeer::REDACTION_DOCUMENTS_REDAC => array(
            CommonHabilitationProfilPeer::REDACTION_DOCUMENTS_REDAC_0,
            CommonHabilitationProfilPeer::REDACTION_DOCUMENTS_REDAC_1,
        ),
        CommonHabilitationProfilPeer::VALIDATION_DOCUMENTS_REDAC => array(
            CommonHabilitationProfilPeer::VALIDATION_DOCUMENTS_REDAC_0,
            CommonHabilitationProfilPeer::VALIDATION_DOCUMENTS_REDAC_1,
        ),
        CommonHabilitationProfilPeer::GESTION_MISE_DISPOSITION_PIECES_MARCHE => array(
            CommonHabilitationProfilPeer::GESTION_MISE_DISPOSITION_PIECES_MARCHE_0,
            CommonHabilitationProfilPeer::GESTION_MISE_DISPOSITION_PIECES_MARCHE_1,
        ),
        CommonHabilitationProfilPeer::ANNUAIRE_ACHETEUR => array(
            CommonHabilitationProfilPeer::ANNUAIRE_ACHETEUR_0,
            CommonHabilitationProfilPeer::ANNUAIRE_ACHETEUR_1,
        ),
        CommonHabilitationProfilPeer::REPRENDRE_INTEGRALEMENT_ARTICLE => array(
            CommonHabilitationProfilPeer::REPRENDRE_INTEGRALEMENT_ARTICLE_0,
            CommonHabilitationProfilPeer::REPRENDRE_INTEGRALEMENT_ARTICLE_1,
        ),
        CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES => array(
            CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_0,
            CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_1,
        ),
        CommonHabilitationProfilPeer::VALIDER_CLAUSES => array(
            CommonHabilitationProfilPeer::VALIDER_CLAUSES_0,
            CommonHabilitationProfilPeer::VALIDER_CLAUSES_1,
        ),
        CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS => array(
            CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS_0,
            CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS_1,
        ),
        CommonHabilitationProfilPeer::VALIDER_CANEVAS => array(
            CommonHabilitationProfilPeer::VALIDER_CANEVAS_0,
            CommonHabilitationProfilPeer::VALIDER_CANEVAS_1,
        ),
        CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_ENTITE_ACHATS => array(
            CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_ENTITE_ACHATS_0,
            CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_ENTITE_ACHATS_1,
        ),
        CommonHabilitationProfilPeer::GENERER_PIECES_FORMAT_ODT => array(
            CommonHabilitationProfilPeer::GENERER_PIECES_FORMAT_ODT_0,
            CommonHabilitationProfilPeer::GENERER_PIECES_FORMAT_ODT_1,
        ),
        CommonHabilitationProfilPeer::PUBLIER_VERSION_CLAUSIER_EDITEUR => array(
            CommonHabilitationProfilPeer::PUBLIER_VERSION_CLAUSIER_EDITEUR_0,
            CommonHabilitationProfilPeer::PUBLIER_VERSION_CLAUSIER_EDITEUR_1,
        ),
        CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_EDITEUR => array(
            CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_EDITEUR_0,
            CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_EDITEUR_1,
        ),
        CommonHabilitationProfilPeer::VALIDER_CLAUSES_EDITEUR => array(
            CommonHabilitationProfilPeer::VALIDER_CLAUSES_EDITEUR_0,
            CommonHabilitationProfilPeer::VALIDER_CLAUSES_EDITEUR_1,
        ),
        CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS_EDITEUR => array(
            CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS_EDITEUR_0,
            CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS_EDITEUR_1,
        ),
        CommonHabilitationProfilPeer::VALIDER_CANEVAS_EDITEUR => array(
            CommonHabilitationProfilPeer::VALIDER_CANEVAS_EDITEUR_0,
            CommonHabilitationProfilPeer::VALIDER_CANEVAS_EDITEUR_1,
        ),
        CommonHabilitationProfilPeer::DECISION_SUIVI_SEUL => array(
            CommonHabilitationProfilPeer::DECISION_SUIVI_SEUL_0,
            CommonHabilitationProfilPeer::DECISION_SUIVI_SEUL_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_HORS_LIGNE => array(
            CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_HORS_LIGNE_0,
            CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_HORS_LIGNE_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_OFFRE_HORS_LIGNE => array(
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_HORS_LIGNE_0,
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_HORS_LIGNE_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE => array(
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE_0,
            CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE_1,
        ),
        CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_HORS_LIGNE => array(
            CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_HORS_LIGNE_0,
            CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_HORS_LIGNE_1,
        ),
        CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_GESTIONNAIRE => array(
            CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_GESTIONNAIRE_0,
            CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_GESTIONNAIRE_1,
        ),
        CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_CONTRIBUTEUR => array(
            CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_CONTRIBUTEUR_0,
            CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_CONTRIBUTEUR_1,
        ),
        CommonHabilitationProfilPeer::GERER_ORGANISMES => array(
            CommonHabilitationProfilPeer::GERER_ORGANISMES_0,
            CommonHabilitationProfilPeer::GERER_ORGANISMES_1,
        ),
        CommonHabilitationProfilPeer::GERER_ASSOCIATIONS_AGENTS => array(
            CommonHabilitationProfilPeer::GERER_ASSOCIATIONS_AGENTS_0,
            CommonHabilitationProfilPeer::GERER_ASSOCIATIONS_AGENTS_1,
        ),
        CommonHabilitationProfilPeer::MODULE_REDACTION_UNIQUEMENT => array(
            CommonHabilitationProfilPeer::MODULE_REDACTION_UNIQUEMENT_0,
            CommonHabilitationProfilPeer::MODULE_REDACTION_UNIQUEMENT_1,
        ),
        CommonHabilitationProfilPeer::HISTORIQUE_NAVIGATION_INSCRITS => array(
            CommonHabilitationProfilPeer::HISTORIQUE_NAVIGATION_INSCRITS_0,
            CommonHabilitationProfilPeer::HISTORIQUE_NAVIGATION_INSCRITS_1,
        ),
        CommonHabilitationProfilPeer::TELECHARGER_ACCORDS_CADRES => array(
            CommonHabilitationProfilPeer::TELECHARGER_ACCORDS_CADRES_0,
            CommonHabilitationProfilPeer::TELECHARGER_ACCORDS_CADRES_1,
        ),
        CommonHabilitationProfilPeer::CREER_ANNONCE_DECISION_RESILIATION => array(
            CommonHabilitationProfilPeer::CREER_ANNONCE_DECISION_RESILIATION_0,
            CommonHabilitationProfilPeer::CREER_ANNONCE_DECISION_RESILIATION_1,
        ),
        CommonHabilitationProfilPeer::CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT => array(
            CommonHabilitationProfilPeer::CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT_0,
            CommonHabilitationProfilPeer::CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT_1,
        ),
        CommonHabilitationProfilPeer::GERER_OPERATIONS => array(
            CommonHabilitationProfilPeer::GERER_OPERATIONS_0,
            CommonHabilitationProfilPeer::GERER_OPERATIONS_1,
        ),
        CommonHabilitationProfilPeer::TELECHARGER_SIRET_ACHETEUR => array(
            CommonHabilitationProfilPeer::TELECHARGER_SIRET_ACHETEUR_0,
            CommonHabilitationProfilPeer::TELECHARGER_SIRET_ACHETEUR_1,
        ),
        CommonHabilitationProfilPeer::GERER_REOUVERTURES_MODIFICATION => array(
            CommonHabilitationProfilPeer::GERER_REOUVERTURES_MODIFICATION_0,
            CommonHabilitationProfilPeer::GERER_REOUVERTURES_MODIFICATION_1,
        ),
        CommonHabilitationProfilPeer::ACCEDER_TOUS_TELECHARGEMENTS => array(
            CommonHabilitationProfilPeer::ACCEDER_TOUS_TELECHARGEMENTS_0,
            CommonHabilitationProfilPeer::ACCEDER_TOUS_TELECHARGEMENTS_1,
        ),
        CommonHabilitationProfilPeer::CREER_CONTRAT => array(
            CommonHabilitationProfilPeer::CREER_CONTRAT_0,
            CommonHabilitationProfilPeer::CREER_CONTRAT_1,
        ),
        CommonHabilitationProfilPeer::MODIFIER_CONTRAT => array(
            CommonHabilitationProfilPeer::MODIFIER_CONTRAT_0,
            CommonHabilitationProfilPeer::MODIFIER_CONTRAT_1,
        ),
        CommonHabilitationProfilPeer::CONSULTER_CONTRAT => array(
            CommonHabilitationProfilPeer::CONSULTER_CONTRAT_0,
            CommonHabilitationProfilPeer::CONSULTER_CONTRAT_1,
        ),
        CommonHabilitationProfilPeer::GERER_NEWSLETTER_REDAC => array(
            CommonHabilitationProfilPeer::GERER_NEWSLETTER_REDAC_0,
            CommonHabilitationProfilPeer::GERER_NEWSLETTER_REDAC_1,
        ),
        CommonHabilitationProfilPeer::PROFIL_RMA => array(
            CommonHabilitationProfilPeer::PROFIL_RMA_0,
            CommonHabilitationProfilPeer::PROFIL_RMA_1,
        ),
        CommonHabilitationProfilPeer::AFFECTATION_VISION_RMA => array(
            CommonHabilitationProfilPeer::AFFECTATION_VISION_RMA_0,
            CommonHabilitationProfilPeer::AFFECTATION_VISION_RMA_1,
        ),
        CommonHabilitationProfilPeer::GERER_GABARIT_EDITEUR => array(
            CommonHabilitationProfilPeer::GERER_GABARIT_EDITEUR_0,
            CommonHabilitationProfilPeer::GERER_GABARIT_EDITEUR_1,
        ),
        CommonHabilitationProfilPeer::GERER_GABARIT => array(
            CommonHabilitationProfilPeer::GERER_GABARIT_0,
            CommonHabilitationProfilPeer::GERER_GABARIT_1,
        ),
        CommonHabilitationProfilPeer::GERER_GABARIT_ENTITE_ACHATS => array(
            CommonHabilitationProfilPeer::GERER_GABARIT_ENTITE_ACHATS_0,
            CommonHabilitationProfilPeer::GERER_GABARIT_ENTITE_ACHATS_1,
        ),
        CommonHabilitationProfilPeer::GERER_GABARIT_AGENT => array(
            CommonHabilitationProfilPeer::GERER_GABARIT_AGENT_0,
            CommonHabilitationProfilPeer::GERER_GABARIT_AGENT_1,
        ),
        CommonHabilitationProfilPeer::GERER_MESSAGES_ACCUEIL => array(
            CommonHabilitationProfilPeer::GERER_MESSAGES_ACCUEIL_0,
            CommonHabilitationProfilPeer::GERER_MESSAGES_ACCUEIL_1,
        ),
        CommonHabilitationProfilPeer::GERER_OA_GA => array(
            CommonHabilitationProfilPeer::GERER_OA_GA_0,
            CommonHabilitationProfilPeer::GERER_OA_GA_1,
        ),
        CommonHabilitationProfilPeer::DEPLACER_SERVICE => array(
            CommonHabilitationProfilPeer::DEPLACER_SERVICE_0,
            CommonHabilitationProfilPeer::DEPLACER_SERVICE_1,
        ),
        CommonHabilitationProfilPeer::ACTIVER_VERSION_CLAUSIER => array(
            CommonHabilitationProfilPeer::ACTIVER_VERSION_CLAUSIER_0,
            CommonHabilitationProfilPeer::ACTIVER_VERSION_CLAUSIER_1,
        ),
        CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA => array(
            CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA_0,
            CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA_1,
        ),
        CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA_DEPENDANTES => array(
            CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA_DEPENDANTES_0,
            CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA_DEPENDANTES_1,
        ),
        CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_ORGANISME => array(
            CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_ORGANISME_0,
            CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_ORGANISME_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonHabilitationProfilPeer::getFieldNames($toType);
        $key = isset(CommonHabilitationProfilPeer::$fieldKeys[$fromType][$name]) ? CommonHabilitationProfilPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonHabilitationProfilPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonHabilitationProfilPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonHabilitationProfilPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonHabilitationProfilPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonHabilitationProfilPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonHabilitationProfilPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonHabilitationProfilPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonHabilitationProfilPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ID);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::LIBELLE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_AGENT_POLE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_FOURNISSEURS_ENVOIS_POSTAUX);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_BI_CLES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MODIFIER_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDER_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PUBLIER_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVRE_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVRE_CONSULTATION_POLE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::INVITE_PERMANENT_ENTITE_DEPENDANTE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::INVITE_PERMANENT_MON_ENTITE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::INVITE_PERMANENT_TRANSVERSE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUPPRIMER_ENVELOPPE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUPPRIMER_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DEPOUILLER_CANDIDATURE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DEPOUILLER_OFFRE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MESSAGERIE_SECURISEE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_PAPIER);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_PAPIER);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_PAPIER);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_ENCHERES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVRE_ENCHERES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_ENTREPRISE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ENVOI_BOAMP);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_CLASSEMENT_LOT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CONNECTEUR_SIS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CONNECTEUR_MARCO);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::REPONDRE_AUX_QUESTIONS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::APPEL_PROJET_FORMATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::UTILISER_CLIENT_CAO);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::NOTIFICATION_BOAMP);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_COMPTE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_MAPA);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_TYPE_VALIDATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::APPROUVER_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::RESTREINDRE_CREATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_LISTE_MARCHES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_COMMISSIONS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_SEUL_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ATTRIBUTION_MARCHE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::FICHE_RECENSEMENT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DECLARER_INFRUCTUEUX);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DECLARER_SANS_SUITE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_CONSULTATION_TRANSVERSE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_EN_LIGNE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_A_DISTANCE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::REFUSER_ENVELOPPE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_ADMISSIBILITE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::RESTAURER_ENVELOPPE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_OFFRE_EN_LIGNE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_EN_LIGNE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_COMPTE_BOAMP);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_AGENTS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_HABILITATIONS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_MAPA_INFERIEUR_MONTANT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_MAPA_SUPERIEUR_MONTANT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_AVANT_VALIDATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_APRES_VALIDATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_REPONSES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::TELECHARGEMENT_UNITAIRE_PLIS_CHIFFRES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_OFFRE_A_DISTANCE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_ANNONCE_INFORMATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SAISIE_MARCHES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDATION_MARCHES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PUBLICATION_MARCHES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_STATISTIQUES_METIER);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_ARCHIVES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_PROCEDURES_FORMALISEES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_ANNONCE_ATTRIBUTION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_REGISTRE_RETRAITS_ELECTRONIQUE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_REGISTRE_QUESTIONS_ELECTRONIQUE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_REGISTRE_DEPOTS_ELECTRONIQUE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDATION_SIMPLE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDATION_INTERMEDIAIRE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDATION_FINALE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_SUITE_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::HYPER_ADMIN);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DROIT_GESTION_SERVICES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_ACCES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::STATISTIQUES_SITE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::STATISTIQUES_QOS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_A_DISTANCE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_COMPTE_JAL);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_CENTRALE_PUB);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_COMPTE_GROUPE_MONITEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_EN_LIGNE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_A_DISTANCE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACTIVATION_COMPTE_ENTREPRISE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::IMPORTER_ENVELOPPE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_PAPIER);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_PAPIER);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_PAPIER);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_DEPOTS_ELECTRONIQUE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_RETRAITS_ELECTRONIQUE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_SEUL_REGISTRE_QUESTIONS_ELECTRONIQUE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_LES_ENTREPRISES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MODIFIER_SOCIETES_EXCLUES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUPPRIMER_SOCIETES_EXCLUES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::RESULTAT_ANALYSE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_ADRESSES_SERVICE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_MON_SERVICE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DOWNLOAD_ARCHIVES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_ANNONCE_EXTRAIT_PV);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_ANNONCE_RAPPORT_ACHEVEMENT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_CERTIFICATS_AGENT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_AVIS_PROGRAMME_PREVISIONNEL);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ANNULER_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ENVOYER_PUBLICITE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::LISTE_MARCHES_NOTIFIES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVRE_MESSAGE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ENVOYER_MESSAGE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUIVI_FLUX_CHORUS_TRANSVERSAL);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_MANDATAIRE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_NEWSLETTER);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_MODELES_FORMULAIRE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_ADRESSES_FACTURATION_JAL);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_ADRESSES_FACTURATION_JAL);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::REDACTION_DOCUMENTS_REDAC);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDATION_DOCUMENTS_REDAC);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_MISE_DISPOSITION_PIECES_MARCHE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ANNUAIRE_ACHETEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::REPRENDRE_INTEGRALEMENT_ARTICLE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDER_CLAUSES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDER_CANEVAS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_ENTITE_ACHATS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GENERER_PIECES_FORMAT_ODT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PUBLIER_VERSION_CLAUSIER_EDITEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_CLAUSES_EDITEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDER_CLAUSES_EDITEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_CANEVAS_EDITEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDER_CANEVAS_EDITEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DECISION_SUIVI_SEUL);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_CANDIDATURE_HORS_LIGNE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_OFFRE_HORS_LIGNE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_OFFRE_TECHNIQUE_HORS_LIGNE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::OUVRIR_ANONYMAT_HORS_LIGNE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_GESTIONNAIRE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ESPACE_COLLABORATIF_CONTRIBUTEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_ORGANISMES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_ASSOCIATIONS_AGENTS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MODULE_REDACTION_UNIQUEMENT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::HISTORIQUE_NAVIGATION_INSCRITS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::TELECHARGER_ACCORDS_CADRES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_ANNONCE_DECISION_RESILIATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_ANNONCE_SYNTHESE_RAPPORT_AUDIT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_OPERATIONS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::TELECHARGER_SIRET_ACHETEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_REOUVERTURES_MODIFICATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCEDER_TOUS_TELECHARGEMENTS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CREER_CONTRAT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::MODIFIER_CONTRAT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::CONSULTER_CONTRAT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_NEWSLETTER_REDAC);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PROFIL_RMA);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::AFFECTATION_VISION_RMA);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_GABARIT_EDITEUR);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_GABARIT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_GABARIT_ENTITE_ACHATS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_GABARIT_AGENT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_MESSAGES_ACCUEIL);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GERER_OA_GA);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DEPLACER_SERVICE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACTIVER_VERSION_CLAUSIER);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_EA_DEPENDANTES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::EXEC_VOIR_CONTRATS_ORGANISME);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_WS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ACCES_ECHANGE_DOCUMENTAIRE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ESPACE_DOCUMENTAIRE_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRER_ORGANISME);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::EXEC_MODIFICATION_CONTRAT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::BESOIN_UNITAIRE_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::BESOIN_UNITAIRE_CREATION_MODIFICATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DEMANDE_ACHAT_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DEMANDE_ACHAT_CREATION_MODIFICATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PROJET_ACHAT_CONSULTATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PROJET_ACHAT_CREATION_MODIFICATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDATION_OPPORTUNITE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDATION_ACHAT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDATION_BUDGET);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::STRATEGIE_ACHAT_GESTION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::RECENSEMENT_PROGRAMMATION_ADMINISTRATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_ENVOL);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::VALIDER_PROJET_ACHAT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_SPASER_CONSULTATIONS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_VALIDATION_ECO);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::GESTION_VALIDATION_SIP);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::RATTACHEMENT_SERVICE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::DUPLICATION_CONSULTATIONS);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PROJET_ACHAT_LANCEMENT_SOURCING);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PROJET_ACHAT_INVALIDATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::PROJET_ACHAT_ANNULATION);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::LANCEMENT_PROCEDURE);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::RECENSEMENT_INVALIDER_PROJET_ACHAT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::RECENSEMENT_ANNULER_PROJET_ACHAT);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::ADMINISTRATION_DOCUMENTS_MODELES);
            $criteria->addSelectColumn(CommonHabilitationProfilPeer::SUPPRIMER_CONTRAT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.libelle');
            $criteria->addSelectColumn($alias . '.gestion_agent_pole');
            $criteria->addSelectColumn($alias . '.gestion_fournisseurs_envois_postaux');
            $criteria->addSelectColumn($alias . '.gestion_bi_cles');
            $criteria->addSelectColumn($alias . '.creer_consultation');
            $criteria->addSelectColumn($alias . '.modifier_consultation');
            $criteria->addSelectColumn($alias . '.valider_consultation');
            $criteria->addSelectColumn($alias . '.publier_consultation');
            $criteria->addSelectColumn($alias . '.suivre_consultation');
            $criteria->addSelectColumn($alias . '.suivre_consultation_pole');
            $criteria->addSelectColumn($alias . '.invite_permanent_entite_dependante');
            $criteria->addSelectColumn($alias . '.invite_permanent_mon_entite');
            $criteria->addSelectColumn($alias . '.invite_permanent_transverse');
            $criteria->addSelectColumn($alias . '.supprimer_enveloppe');
            $criteria->addSelectColumn($alias . '.supprimer_consultation');
            $criteria->addSelectColumn($alias . '.depouiller_candidature');
            $criteria->addSelectColumn($alias . '.depouiller_offre');
            $criteria->addSelectColumn($alias . '.messagerie_securisee');
            $criteria->addSelectColumn($alias . '.acces_registre_depots_papier');
            $criteria->addSelectColumn($alias . '.acces_registre_retraits_papier');
            $criteria->addSelectColumn($alias . '.acces_registre_questions_papier');
            $criteria->addSelectColumn($alias . '.gerer_encheres');
            $criteria->addSelectColumn($alias . '.suivre_encheres');
            $criteria->addSelectColumn($alias . '.suivi_entreprise');
            $criteria->addSelectColumn($alias . '.envoi_boamp');
            $criteria->addSelectColumn($alias . '.acces_classement_lot');
            $criteria->addSelectColumn($alias . '.connecteur_sis');
            $criteria->addSelectColumn($alias . '.connecteur_marco');
            $criteria->addSelectColumn($alias . '.repondre_aux_questions');
            $criteria->addSelectColumn($alias . '.appel_projet_formation');
            $criteria->addSelectColumn($alias . '.utiliser_client_CAO');
            $criteria->addSelectColumn($alias . '.notification_boamp');
            $criteria->addSelectColumn($alias . '.administrer_compte');
            $criteria->addSelectColumn($alias . '.gestion_mapa');
            $criteria->addSelectColumn($alias . '.gestion_type_validation');
            $criteria->addSelectColumn($alias . '.approuver_consultation');
            $criteria->addSelectColumn($alias . '.administrer_procedure');
            $criteria->addSelectColumn($alias . '.restreindre_creation');
            $criteria->addSelectColumn($alias . '.creer_liste_marches');
            $criteria->addSelectColumn($alias . '.gestion_commissions');
            $criteria->addSelectColumn($alias . '.suivi_seul_consultation');
            $criteria->addSelectColumn($alias . '.attribution_marche');
            $criteria->addSelectColumn($alias . '.fiche_recensement');
            $criteria->addSelectColumn($alias . '.declarer_infructueux');
            $criteria->addSelectColumn($alias . '.declarer_sans_suite');
            $criteria->addSelectColumn($alias . '.creer_consultation_transverse');
            $criteria->addSelectColumn($alias . '.ouvrir_candidature_en_ligne');
            $criteria->addSelectColumn($alias . '.ouvrir_candidature_a_distance');
            $criteria->addSelectColumn($alias . '.refuser_enveloppe');
            $criteria->addSelectColumn($alias . '.gerer_admissibilite');
            $criteria->addSelectColumn($alias . '.restaurer_enveloppe');
            $criteria->addSelectColumn($alias . '.ouvrir_offre_en_ligne');
            $criteria->addSelectColumn($alias . '.ouvrir_anonymat_en_ligne');
            $criteria->addSelectColumn($alias . '.gestion_compte_boamp');
            $criteria->addSelectColumn($alias . '.gestion_agents');
            $criteria->addSelectColumn($alias . '.gestion_habilitations');
            $criteria->addSelectColumn($alias . '.gerer_mapa_inferieur_montant');
            $criteria->addSelectColumn($alias . '.gerer_mapa_superieur_montant');
            $criteria->addSelectColumn($alias . '.modifier_consultation_avant_validation');
            $criteria->addSelectColumn($alias . '.modifier_consultation_apres_validation');
            $criteria->addSelectColumn($alias . '.acces_reponses');
            $criteria->addSelectColumn($alias . '.telechargement_groupe_anticipe_plis_chiffres');
            $criteria->addSelectColumn($alias . '.telechargement_unitaire_plis_chiffres');
            $criteria->addSelectColumn($alias . '.ouvrir_offre_a_distance');
            $criteria->addSelectColumn($alias . '.creer_annonce_information');
            $criteria->addSelectColumn($alias . '.saisie_marches');
            $criteria->addSelectColumn($alias . '.validation_marches');
            $criteria->addSelectColumn($alias . '.publication_marches');
            $criteria->addSelectColumn($alias . '.gerer_statistiques_metier');
            $criteria->addSelectColumn($alias . '.gerer_archives');
            $criteria->addSelectColumn($alias . '.administrer_procedures_formalisees');
            $criteria->addSelectColumn($alias . '.creer_annonce_attribution');
            $criteria->addSelectColumn($alias . '.acces_registre_retraits_electronique');
            $criteria->addSelectColumn($alias . '.acces_registre_questions_electronique');
            $criteria->addSelectColumn($alias . '.acces_registre_depots_electronique');
            $criteria->addSelectColumn($alias . '.validation_simple');
            $criteria->addSelectColumn($alias . '.validation_intermediaire');
            $criteria->addSelectColumn($alias . '.validation_finale');
            $criteria->addSelectColumn($alias . '.creer_suite_consultation');
            $criteria->addSelectColumn($alias . '.hyper_admin');
            $criteria->addSelectColumn($alias . '.droit_gestion_services');
            $criteria->addSelectColumn($alias . '.suivi_acces');
            $criteria->addSelectColumn($alias . '.statistiques_site');
            $criteria->addSelectColumn($alias . '.statistiques_QoS');
            $criteria->addSelectColumn($alias . '.ouvrir_anonymat_a_distance');
            $criteria->addSelectColumn($alias . '.gestion_compte_jal');
            $criteria->addSelectColumn($alias . '.gestion_centrale_pub');
            $criteria->addSelectColumn($alias . '.Gestion_Compte_Groupe_Moniteur');
            $criteria->addSelectColumn($alias . '.ouvrir_offre_technique_en_ligne');
            $criteria->addSelectColumn($alias . '.ouvrir_offre_technique_a_distance');
            $criteria->addSelectColumn($alias . '.activation_compte_entreprise');
            $criteria->addSelectColumn($alias . '.importer_enveloppe');
            $criteria->addSelectColumn($alias . '.suivi_seul_registre_depots_papier');
            $criteria->addSelectColumn($alias . '.suivi_seul_registre_retraits_papier');
            $criteria->addSelectColumn($alias . '.suivi_seul_registre_questions_papier');
            $criteria->addSelectColumn($alias . '.suivi_seul_registre_depots_electronique');
            $criteria->addSelectColumn($alias . '.suivi_seul_registre_retraits_electronique');
            $criteria->addSelectColumn($alias . '.suivi_seul_registre_questions_electronique');
            $criteria->addSelectColumn($alias . '.modifier_consultation_mapa_inferieur_montant_apres_validation');
            $criteria->addSelectColumn($alias . '.modifier_consultation_mapa_superieur_montant_apres_validation');
            $criteria->addSelectColumn($alias . '.modifier_consultation_procedures_formalisees_apres_validation');
            $criteria->addSelectColumn($alias . '.gerer_les_entreprises');
            $criteria->addSelectColumn($alias . '.portee_societes_exclues');
            $criteria->addSelectColumn($alias . '.portee_societes_exclues_tous_organismes');
            $criteria->addSelectColumn($alias . '.modifier_societes_exclues');
            $criteria->addSelectColumn($alias . '.supprimer_societes_exclues');
            $criteria->addSelectColumn($alias . '.resultat_analyse');
            $criteria->addSelectColumn($alias . '.gerer_adresses_service');
            $criteria->addSelectColumn($alias . '.gerer_mon_service');
            $criteria->addSelectColumn($alias . '.download_archives');
            $criteria->addSelectColumn($alias . '.creer_annonce_extrait_pv');
            $criteria->addSelectColumn($alias . '.creer_annonce_rapport_achevement');
            $criteria->addSelectColumn($alias . '.gestion_certificats_agent');
            $criteria->addSelectColumn($alias . '.creer_avis_programme_previsionnel');
            $criteria->addSelectColumn($alias . '.annuler_consultation');
            $criteria->addSelectColumn($alias . '.envoyer_publicite');
            $criteria->addSelectColumn($alias . '.liste_marches_notifies');
            $criteria->addSelectColumn($alias . '.suivre_message');
            $criteria->addSelectColumn($alias . '.envoyer_message');
            $criteria->addSelectColumn($alias . '.suivi_flux_chorus_transversal');
            $criteria->addSelectColumn($alias . '.gestion_mandataire');
            $criteria->addSelectColumn($alias . '.gerer_newsletter');
            $criteria->addSelectColumn($alias . '.gestion_modeles_formulaire');
            $criteria->addSelectColumn($alias . '.gestion_adresses_facturation_jal');
            $criteria->addSelectColumn($alias . '.administrer_adresses_facturation_jal');
            $criteria->addSelectColumn($alias . '.redaction_documents_redac');
            $criteria->addSelectColumn($alias . '.validation_documents_redac');
            $criteria->addSelectColumn($alias . '.gestion_mise_disposition_pieces_marche');
            $criteria->addSelectColumn($alias . '.annuaire_acheteur');
            $criteria->addSelectColumn($alias . '.reprendre_integralement_article');
            $criteria->addSelectColumn($alias . '.administrer_clauses');
            $criteria->addSelectColumn($alias . '.valider_clauses');
            $criteria->addSelectColumn($alias . '.administrer_canevas');
            $criteria->addSelectColumn($alias . '.valider_canevas');
            $criteria->addSelectColumn($alias . '.administrer_clauses_entite_achats');
            $criteria->addSelectColumn($alias . '.generer_pieces_format_odt');
            $criteria->addSelectColumn($alias . '.publier_version_clausier_editeur');
            $criteria->addSelectColumn($alias . '.administrer_clauses_editeur');
            $criteria->addSelectColumn($alias . '.valider_clauses_editeur');
            $criteria->addSelectColumn($alias . '.administrer_canevas_editeur');
            $criteria->addSelectColumn($alias . '.valider_canevas_editeur');
            $criteria->addSelectColumn($alias . '.decision_suivi_seul');
            $criteria->addSelectColumn($alias . '.ouvrir_candidature_hors_ligne');
            $criteria->addSelectColumn($alias . '.ouvrir_offre_hors_ligne');
            $criteria->addSelectColumn($alias . '.ouvrir_offre_technique_hors_ligne');
            $criteria->addSelectColumn($alias . '.ouvrir_anonymat_hors_ligne');
            $criteria->addSelectColumn($alias . '.espace_collaboratif_gestionnaire');
            $criteria->addSelectColumn($alias . '.espace_collaboratif_contributeur');
            $criteria->addSelectColumn($alias . '.gerer_organismes');
            $criteria->addSelectColumn($alias . '.gerer_associations_agents');
            $criteria->addSelectColumn($alias . '.module_redaction_uniquement');
            $criteria->addSelectColumn($alias . '.historique_navigation_inscrits');
            $criteria->addSelectColumn($alias . '.telecharger_accords_cadres');
            $criteria->addSelectColumn($alias . '.creer_annonce_decision_resiliation');
            $criteria->addSelectColumn($alias . '.creer_annonce_synthese_rapport_audit');
            $criteria->addSelectColumn($alias . '.gerer_operations');
            $criteria->addSelectColumn($alias . '.telecharger_siret_acheteur');
            $criteria->addSelectColumn($alias . '.gerer_reouvertures_modification');
            $criteria->addSelectColumn($alias . '.acceder_tous_telechargements');
            $criteria->addSelectColumn($alias . '.creer_contrat');
            $criteria->addSelectColumn($alias . '.modifier_contrat');
            $criteria->addSelectColumn($alias . '.consulter_contrat');
            $criteria->addSelectColumn($alias . '.gerer_newsletter_redac');
            $criteria->addSelectColumn($alias . '.profil_rma');
            $criteria->addSelectColumn($alias . '.affectation_vision_rma');
            $criteria->addSelectColumn($alias . '.gerer_gabarit_editeur');
            $criteria->addSelectColumn($alias . '.gerer_gabarit');
            $criteria->addSelectColumn($alias . '.gerer_gabarit_entite_achats');
            $criteria->addSelectColumn($alias . '.gerer_gabarit_agent');
            $criteria->addSelectColumn($alias . '.gerer_messages_accueil');
            $criteria->addSelectColumn($alias . '.gerer_OA_GA');
            $criteria->addSelectColumn($alias . '.deplacer_service');
            $criteria->addSelectColumn($alias . '.activer_version_clausier');
            $criteria->addSelectColumn($alias . '.exec_voir_contrats_ea');
            $criteria->addSelectColumn($alias . '.exec_voir_contrats_ea_dependantes');
            $criteria->addSelectColumn($alias . '.exec_voir_contrats_organisme');
            $criteria->addSelectColumn($alias . '.acces_ws');
            $criteria->addSelectColumn($alias . '.acces_echange_documentaire');
            $criteria->addSelectColumn($alias . '.espace_documentaire_consultation');
            $criteria->addSelectColumn($alias . '.administrer_organisme');
            $criteria->addSelectColumn($alias . '.exec_modification_contrat');
            $criteria->addSelectColumn($alias . '.besoin_unitaire_consultation');
            $criteria->addSelectColumn($alias . '.besoin_unitaire_creation_modification');
            $criteria->addSelectColumn($alias . '.demande_achat_consultation');
            $criteria->addSelectColumn($alias . '.demande_achat_creation_modification');
            $criteria->addSelectColumn($alias . '.projet_achat_consultation');
            $criteria->addSelectColumn($alias . '.projet_achat_creation_modification');
            $criteria->addSelectColumn($alias . '.validation_opportunite');
            $criteria->addSelectColumn($alias . '.validation_achat');
            $criteria->addSelectColumn($alias . '.validation_budget');
            $criteria->addSelectColumn($alias . '.strategie_achat_gestion');
            $criteria->addSelectColumn($alias . '.recensement_programmation_administration');
            $criteria->addSelectColumn($alias . '.gestion_envol');
            $criteria->addSelectColumn($alias . '.valider_projet_achat');
            $criteria->addSelectColumn($alias . '.gestion_spaser_consultations');
            $criteria->addSelectColumn($alias . '.gestion_validation_eco');
            $criteria->addSelectColumn($alias . '.gestion_validation_sip');
            $criteria->addSelectColumn($alias . '.rattachement_service');
            $criteria->addSelectColumn($alias . '.duplication_consultations');
            $criteria->addSelectColumn($alias . '.projet_achat_lancement_sourcing');
            $criteria->addSelectColumn($alias . '.projet_achat_invalidation');
            $criteria->addSelectColumn($alias . '.projet_achat_annulation');
            $criteria->addSelectColumn($alias . '.lancement_procedure');
            $criteria->addSelectColumn($alias . '.recensement_invalider_projet_achat');
            $criteria->addSelectColumn($alias . '.recensement_annuler_projet_achat');
            $criteria->addSelectColumn($alias . '.administration_documents_modeles');
            $criteria->addSelectColumn($alias . '.supprimer_contrat');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonHabilitationProfilPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonHabilitationProfilPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonHabilitationProfilPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationProfilPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonHabilitationProfil
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonHabilitationProfilPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonHabilitationProfilPeer::populateObjects(CommonHabilitationProfilPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationProfilPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonHabilitationProfilPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonHabilitationProfilPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonHabilitationProfil $obj A CommonHabilitationProfil object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonHabilitationProfilPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonHabilitationProfil object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonHabilitationProfil) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonHabilitationProfil object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonHabilitationProfilPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonHabilitationProfil Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonHabilitationProfilPeer::$instances[$key])) {
                return CommonHabilitationProfilPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonHabilitationProfilPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonHabilitationProfilPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to HabilitationProfil
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonHabilitationProfilPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonHabilitationProfilPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonHabilitationProfilPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonHabilitationProfilPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonHabilitationProfil object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonHabilitationProfilPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonHabilitationProfilPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonHabilitationProfilPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonHabilitationProfilPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonHabilitationProfilPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonHabilitationProfilPeer::DATABASE_NAME)->getTable(CommonHabilitationProfilPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonHabilitationProfilPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonHabilitationProfilPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonHabilitationProfilTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonHabilitationProfilPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonHabilitationProfil or Criteria object.
     *
     * @param      mixed $values Criteria or CommonHabilitationProfil object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationProfilPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonHabilitationProfil object
        }

        if ($criteria->containsKey(CommonHabilitationProfilPeer::ID) && $criteria->keyContainsValue(CommonHabilitationProfilPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonHabilitationProfilPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonHabilitationProfilPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonHabilitationProfil or Criteria object.
     *
     * @param      mixed $values Criteria or CommonHabilitationProfil object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationProfilPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonHabilitationProfilPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonHabilitationProfilPeer::ID);
            $value = $criteria->remove(CommonHabilitationProfilPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonHabilitationProfilPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonHabilitationProfilPeer::TABLE_NAME);
            }

        } else { // $values is CommonHabilitationProfil object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonHabilitationProfilPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the HabilitationProfil table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationProfilPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonHabilitationProfilPeer::TABLE_NAME, $con, CommonHabilitationProfilPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonHabilitationProfilPeer::clearInstancePool();
            CommonHabilitationProfilPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonHabilitationProfil or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonHabilitationProfil object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationProfilPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonHabilitationProfilPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonHabilitationProfil) { // it's a model object
            // invalidate the cache for this single object
            CommonHabilitationProfilPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonHabilitationProfilPeer::DATABASE_NAME);
            $criteria->add(CommonHabilitationProfilPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonHabilitationProfilPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonHabilitationProfilPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonHabilitationProfilPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonHabilitationProfil object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonHabilitationProfil $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonHabilitationProfilPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonHabilitationProfilPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonHabilitationProfilPeer::DATABASE_NAME, CommonHabilitationProfilPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonHabilitationProfil
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonHabilitationProfilPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationProfilPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonHabilitationProfilPeer::DATABASE_NAME);
        $criteria->add(CommonHabilitationProfilPeer::ID, $pk);

        $v = CommonHabilitationProfilPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonHabilitationProfil[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationProfilPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonHabilitationProfilPeer::DATABASE_NAME);
            $criteria->add(CommonHabilitationProfilPeer::ID, $pks, Criteria::IN);
            $objs = CommonHabilitationProfilPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonHabilitationProfilPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonHabilitationProfilPeer::buildTableMap();

