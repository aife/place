<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonBlobOrganismeFilePeer;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonPlateformeVirtuellePeer;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Propel\Mpe\Map\CommonOffresTableMap;

/**
 * Base static class for performing query and update operations on the 'Offres' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonOffresPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'Offres';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonOffres';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonOffresTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 76;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 76;

    /** the column name for the id field */
    const ID = 'Offres.id';

    /** the column name for the organisme field */
    const ORGANISME = 'Offres.organisme';

    /** the column name for the consultation_ref field */
    const CONSULTATION_REF = 'Offres.consultation_ref';

    /** the column name for the entreprise_id field */
    const ENTREPRISE_ID = 'Offres.entreprise_id';

    /** the column name for the id_etablissement field */
    const ID_ETABLISSEMENT = 'Offres.id_etablissement';

    /** the column name for the old_inscrit_id field */
    const OLD_INSCRIT_ID = 'Offres.old_inscrit_id';

    /** the column name for the signatureenvxml field */
    const SIGNATUREENVXML = 'Offres.signatureenvxml';

    /** the column name for the horodatage field */
    const HORODATAGE = 'Offres.horodatage';

    /** the column name for the mailsignataire field */
    const MAILSIGNATAIRE = 'Offres.mailsignataire';

    /** the column name for the untrusteddate field */
    const UNTRUSTEDDATE = 'Offres.untrusteddate';

    /** the column name for the untrustedserial field */
    const UNTRUSTEDSERIAL = 'Offres.untrustedserial';

    /** the column name for the envoi_complet field */
    const ENVOI_COMPLET = 'Offres.envoi_complet';

    /** the column name for the date_depot_differe field */
    const DATE_DEPOT_DIFFERE = 'Offres.date_depot_differe';

    /** the column name for the horodatage_envoi_differe field */
    const HORODATAGE_ENVOI_DIFFERE = 'Offres.horodatage_envoi_differe';

    /** the column name for the signatureenvxml_envoi_differe field */
    const SIGNATUREENVXML_ENVOI_DIFFERE = 'Offres.signatureenvxml_envoi_differe';

    /** the column name for the external_serial field */
    const EXTERNAL_SERIAL = 'Offres.external_serial';

    /** the column name for the internal_serial field */
    const INTERNAL_SERIAL = 'Offres.internal_serial';

    /** the column name for the uid_offre field */
    const UID_OFFRE = 'Offres.uid_offre';

    /** the column name for the offre_selectionnee field */
    const OFFRE_SELECTIONNEE = 'Offres.offre_selectionnee';

    /** the column name for the Observation field */
    const OBSERVATION = 'Offres.Observation';

    /** the column name for the xml_string field */
    const XML_STRING = 'Offres.xml_string';

    /** the column name for the nom_entreprise_inscrit field */
    const NOM_ENTREPRISE_INSCRIT = 'Offres.nom_entreprise_inscrit';

    /** the column name for the nom_inscrit field */
    const NOM_INSCRIT = 'Offres.nom_inscrit';

    /** the column name for the prenom_inscrit field */
    const PRENOM_INSCRIT = 'Offres.prenom_inscrit';

    /** the column name for the adresse_inscrit field */
    const ADRESSE_INSCRIT = 'Offres.adresse_inscrit';

    /** the column name for the adresse2_inscrit field */
    const ADRESSE2_INSCRIT = 'Offres.adresse2_inscrit';

    /** the column name for the telephone_inscrit field */
    const TELEPHONE_INSCRIT = 'Offres.telephone_inscrit';

    /** the column name for the fax_inscrit field */
    const FAX_INSCRIT = 'Offres.fax_inscrit';

    /** the column name for the code_postal_inscrit field */
    const CODE_POSTAL_INSCRIT = 'Offres.code_postal_inscrit';

    /** the column name for the ville_inscrit field */
    const VILLE_INSCRIT = 'Offres.ville_inscrit';

    /** the column name for the pays_inscrit field */
    const PAYS_INSCRIT = 'Offres.pays_inscrit';

    /** the column name for the acronyme_pays field */
    const ACRONYME_PAYS = 'Offres.acronyme_pays';

    /** the column name for the siret_entreprise field */
    const SIRET_ENTREPRISE = 'Offres.siret_entreprise';

    /** the column name for the identifiant_national field */
    const IDENTIFIANT_NATIONAL = 'Offres.identifiant_national';

    /** the column name for the email_inscrit field */
    const EMAIL_INSCRIT = 'Offres.email_inscrit';

    /** the column name for the siret_inscrit field */
    const SIRET_INSCRIT = 'Offres.siret_inscrit';

    /** the column name for the nom_entreprise field */
    const NOM_ENTREPRISE = 'Offres.nom_entreprise';

    /** the column name for the horodatage_annulation field */
    const HORODATAGE_ANNULATION = 'Offres.horodatage_annulation';

    /** the column name for the date_annulation field */
    const DATE_ANNULATION = 'Offres.date_annulation';

    /** the column name for the signature_annulation field */
    const SIGNATURE_ANNULATION = 'Offres.signature_annulation';

    /** the column name for the depot_annule field */
    const DEPOT_ANNULE = 'Offres.depot_annule';

    /** the column name for the string_annulation field */
    const STRING_ANNULATION = 'Offres.string_annulation';

    /** the column name for the verification_certificat_annulation field */
    const VERIFICATION_CERTIFICAT_ANNULATION = 'Offres.verification_certificat_annulation';

    /** the column name for the offre_variante field */
    const OFFRE_VARIANTE = 'Offres.offre_variante';

    /** the column name for the reponse_pas_a_pas field */
    const REPONSE_PAS_A_PAS = 'Offres.reponse_pas_a_pas';

    /** the column name for the numero_reponse field */
    const NUMERO_REPONSE = 'Offres.numero_reponse';

    /** the column name for the statut_offres field */
    const STATUT_OFFRES = 'Offres.statut_offres';

    /** the column name for the date_heure_ouverture field */
    const DATE_HEURE_OUVERTURE = 'Offres.date_heure_ouverture';

    /** the column name for the agentid_ouverture field */
    const AGENTID_OUVERTURE = 'Offres.agentid_ouverture';

    /** the column name for the agentid_ouverture2 field */
    const AGENTID_OUVERTURE2 = 'Offres.agentid_ouverture2';

    /** the column name for the date_heure_ouverture_agent2 field */
    const DATE_HEURE_OUVERTURE_AGENT2 = 'Offres.date_heure_ouverture_agent2';

    /** the column name for the cryptage_reponse field */
    const CRYPTAGE_REPONSE = 'Offres.cryptage_reponse';

    /** the column name for the nom_agent_ouverture field */
    const NOM_AGENT_OUVERTURE = 'Offres.nom_agent_ouverture';

    /** the column name for the agent_telechargement_offre field */
    const AGENT_TELECHARGEMENT_OFFRE = 'Offres.agent_telechargement_offre';

    /** the column name for the date_telechargement_offre field */
    const DATE_TELECHARGEMENT_OFFRE = 'Offres.date_telechargement_offre';

    /** the column name for the repertoire_telechargement_offre field */
    const REPERTOIRE_TELECHARGEMENT_OFFRE = 'Offres.repertoire_telechargement_offre';

    /** the column name for the candidature_id_externe field */
    const CANDIDATURE_ID_EXTERNE = 'Offres.candidature_id_externe';

    /** the column name for the etat_chiffrement field */
    const ETAT_CHIFFREMENT = 'Offres.etat_chiffrement';

    /** the column name for the erreur_chiffrement field */
    const ERREUR_CHIFFREMENT = 'Offres.erreur_chiffrement';

    /** the column name for the date_fin_chiffrement field */
    const DATE_FIN_CHIFFREMENT = 'Offres.date_fin_chiffrement';

    /** the column name for the date_horodatage field */
    const DATE_HORODATAGE = 'Offres.date_horodatage';

    /** the column name for the verification_hotodatage field */
    const VERIFICATION_HOTODATAGE = 'Offres.verification_hotodatage';

    /** the column name for the verification_signature_offre field */
    const VERIFICATION_SIGNATURE_OFFRE = 'Offres.verification_signature_offre';

    /** the column name for the horodatage_hash_fichiers field */
    const HORODATAGE_HASH_FICHIERS = 'Offres.horodatage_hash_fichiers';

    /** the column name for the id_pdf_echange_accuse field */
    const ID_PDF_ECHANGE_ACCUSE = 'Offres.id_pdf_echange_accuse';

    /** the column name for the created_at field */
    const CREATED_AT = 'Offres.created_at';

    /** the column name for the uid_response field */
    const UID_RESPONSE = 'Offres.uid_response';

    /** the column name for the date_depot field */
    const DATE_DEPOT = 'Offres.date_depot';

    /** the column name for the resultat_verification_hash_all_files field */
    const RESULTAT_VERIFICATION_HASH_ALL_FILES = 'Offres.resultat_verification_hash_all_files';

    /** the column name for the id_blob_horodatage_hash field */
    const ID_BLOB_HORODATAGE_HASH = 'Offres.id_blob_horodatage_hash';

    /** the column name for the id_blob_xml_reponse field */
    const ID_BLOB_XML_REPONSE = 'Offres.id_blob_xml_reponse';

    /** the column name for the consultation_id field */
    const CONSULTATION_ID = 'Offres.consultation_id';

    /** the column name for the plateforme_virtuelle_id field */
    const PLATEFORME_VIRTUELLE_ID = 'Offres.plateforme_virtuelle_id';

    /** the column name for the inscrit_id field */
    const INSCRIT_ID = 'Offres.inscrit_id';

    /** the column name for the taux_production_france field */
    const TAUX_PRODUCTION_FRANCE = 'Offres.taux_production_france';

    /** the column name for the taux_production_europe field */
    const TAUX_PRODUCTION_EUROPE = 'Offres.taux_production_europe';

    /** The enumerated values for the depot_annule field */
    const DEPOT_ANNULE_0 = '0';
    const DEPOT_ANNULE_1 = '1';

    /** The enumerated values for the offre_variante field */
    const OFFRE_VARIANTE_0 = '0';
    const OFFRE_VARIANTE_1 = '1';

    /** The enumerated values for the reponse_pas_a_pas field */
    const REPONSE_PAS_A_PAS_0 = '0';
    const REPONSE_PAS_A_PAS_1 = '1';

    /** The enumerated values for the verification_hotodatage field */
    const VERIFICATION_HOTODATAGE_0 = '0';
    const VERIFICATION_HOTODATAGE_1 = '1';
    const VERIFICATION_HOTODATAGE_2 = '2';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonOffres objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonOffres[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonOffresPeer::$fieldNames[CommonOffresPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'Organisme', 'ConsultationRef', 'EntrepriseId', 'IdEtablissement', 'OldInscritId', 'Signatureenvxml', 'Horodatage', 'Mailsignataire', 'Untrusteddate', 'Untrustedserial', 'EnvoiComplet', 'DateDepotDiffere', 'HorodatageEnvoiDiffere', 'SignatureenvxmlEnvoiDiffere', 'ExternalSerial', 'InternalSerial', 'UidOffre', 'OffreSelectionnee', 'Observation', 'XmlString', 'NomEntrepriseInscrit', 'NomInscrit', 'PrenomInscrit', 'AdresseInscrit', 'Adresse2Inscrit', 'TelephoneInscrit', 'FaxInscrit', 'CodePostalInscrit', 'VilleInscrit', 'PaysInscrit', 'AcronymePays', 'SiretEntreprise', 'IdentifiantNational', 'EmailInscrit', 'SiretInscrit', 'NomEntreprise', 'HorodatageAnnulation', 'DateAnnulation', 'SignatureAnnulation', 'DepotAnnule', 'StringAnnulation', 'VerificationCertificatAnnulation', 'OffreVariante', 'ReponsePasAPas', 'NumeroReponse', 'StatutOffres', 'DateHeureOuverture', 'AgentidOuverture', 'AgentidOuverture2', 'DateHeureOuvertureAgent2', 'CryptageReponse', 'NomAgentOuverture', 'AgentTelechargementOffre', 'DateTelechargementOffre', 'RepertoireTelechargementOffre', 'CandidatureIdExterne', 'EtatChiffrement', 'ErreurChiffrement', 'DateFinChiffrement', 'DateHorodatage', 'VerificationHotodatage', 'VerificationSignatureOffre', 'HorodatageHashFichiers', 'IdPdfEchangeAccuse', 'CreatedAt', 'UidResponse', 'DateDepot', 'ResultatVerificationHashAllFiles', 'IdBlobHorodatageHash', 'IdBlobXmlReponse', 'ConsultationId', 'PlateformeVirtuelleId', 'InscritId', 'TauxProductionFrance', 'TauxProductionEurope', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'organisme', 'consultationRef', 'entrepriseId', 'idEtablissement', 'oldInscritId', 'signatureenvxml', 'horodatage', 'mailsignataire', 'untrusteddate', 'untrustedserial', 'envoiComplet', 'dateDepotDiffere', 'horodatageEnvoiDiffere', 'signatureenvxmlEnvoiDiffere', 'externalSerial', 'internalSerial', 'uidOffre', 'offreSelectionnee', 'observation', 'xmlString', 'nomEntrepriseInscrit', 'nomInscrit', 'prenomInscrit', 'adresseInscrit', 'adresse2Inscrit', 'telephoneInscrit', 'faxInscrit', 'codePostalInscrit', 'villeInscrit', 'paysInscrit', 'acronymePays', 'siretEntreprise', 'identifiantNational', 'emailInscrit', 'siretInscrit', 'nomEntreprise', 'horodatageAnnulation', 'dateAnnulation', 'signatureAnnulation', 'depotAnnule', 'stringAnnulation', 'verificationCertificatAnnulation', 'offreVariante', 'reponsePasAPas', 'numeroReponse', 'statutOffres', 'dateHeureOuverture', 'agentidOuverture', 'agentidOuverture2', 'dateHeureOuvertureAgent2', 'cryptageReponse', 'nomAgentOuverture', 'agentTelechargementOffre', 'dateTelechargementOffre', 'repertoireTelechargementOffre', 'candidatureIdExterne', 'etatChiffrement', 'erreurChiffrement', 'dateFinChiffrement', 'dateHorodatage', 'verificationHotodatage', 'verificationSignatureOffre', 'horodatageHashFichiers', 'idPdfEchangeAccuse', 'createdAt', 'uidResponse', 'dateDepot', 'resultatVerificationHashAllFiles', 'idBlobHorodatageHash', 'idBlobXmlReponse', 'consultationId', 'plateformeVirtuelleId', 'inscritId', 'tauxProductionFrance', 'tauxProductionEurope', ),
        BasePeer::TYPE_COLNAME => array (CommonOffresPeer::ID, CommonOffresPeer::ORGANISME, CommonOffresPeer::CONSULTATION_REF, CommonOffresPeer::ENTREPRISE_ID, CommonOffresPeer::ID_ETABLISSEMENT, CommonOffresPeer::OLD_INSCRIT_ID, CommonOffresPeer::SIGNATUREENVXML, CommonOffresPeer::HORODATAGE, CommonOffresPeer::MAILSIGNATAIRE, CommonOffresPeer::UNTRUSTEDDATE, CommonOffresPeer::UNTRUSTEDSERIAL, CommonOffresPeer::ENVOI_COMPLET, CommonOffresPeer::DATE_DEPOT_DIFFERE, CommonOffresPeer::HORODATAGE_ENVOI_DIFFERE, CommonOffresPeer::SIGNATUREENVXML_ENVOI_DIFFERE, CommonOffresPeer::EXTERNAL_SERIAL, CommonOffresPeer::INTERNAL_SERIAL, CommonOffresPeer::UID_OFFRE, CommonOffresPeer::OFFRE_SELECTIONNEE, CommonOffresPeer::OBSERVATION, CommonOffresPeer::XML_STRING, CommonOffresPeer::NOM_ENTREPRISE_INSCRIT, CommonOffresPeer::NOM_INSCRIT, CommonOffresPeer::PRENOM_INSCRIT, CommonOffresPeer::ADRESSE_INSCRIT, CommonOffresPeer::ADRESSE2_INSCRIT, CommonOffresPeer::TELEPHONE_INSCRIT, CommonOffresPeer::FAX_INSCRIT, CommonOffresPeer::CODE_POSTAL_INSCRIT, CommonOffresPeer::VILLE_INSCRIT, CommonOffresPeer::PAYS_INSCRIT, CommonOffresPeer::ACRONYME_PAYS, CommonOffresPeer::SIRET_ENTREPRISE, CommonOffresPeer::IDENTIFIANT_NATIONAL, CommonOffresPeer::EMAIL_INSCRIT, CommonOffresPeer::SIRET_INSCRIT, CommonOffresPeer::NOM_ENTREPRISE, CommonOffresPeer::HORODATAGE_ANNULATION, CommonOffresPeer::DATE_ANNULATION, CommonOffresPeer::SIGNATURE_ANNULATION, CommonOffresPeer::DEPOT_ANNULE, CommonOffresPeer::STRING_ANNULATION, CommonOffresPeer::VERIFICATION_CERTIFICAT_ANNULATION, CommonOffresPeer::OFFRE_VARIANTE, CommonOffresPeer::REPONSE_PAS_A_PAS, CommonOffresPeer::NUMERO_REPONSE, CommonOffresPeer::STATUT_OFFRES, CommonOffresPeer::DATE_HEURE_OUVERTURE, CommonOffresPeer::AGENTID_OUVERTURE, CommonOffresPeer::AGENTID_OUVERTURE2, CommonOffresPeer::DATE_HEURE_OUVERTURE_AGENT2, CommonOffresPeer::CRYPTAGE_REPONSE, CommonOffresPeer::NOM_AGENT_OUVERTURE, CommonOffresPeer::AGENT_TELECHARGEMENT_OFFRE, CommonOffresPeer::DATE_TELECHARGEMENT_OFFRE, CommonOffresPeer::REPERTOIRE_TELECHARGEMENT_OFFRE, CommonOffresPeer::CANDIDATURE_ID_EXTERNE, CommonOffresPeer::ETAT_CHIFFREMENT, CommonOffresPeer::ERREUR_CHIFFREMENT, CommonOffresPeer::DATE_FIN_CHIFFREMENT, CommonOffresPeer::DATE_HORODATAGE, CommonOffresPeer::VERIFICATION_HOTODATAGE, CommonOffresPeer::VERIFICATION_SIGNATURE_OFFRE, CommonOffresPeer::HORODATAGE_HASH_FICHIERS, CommonOffresPeer::ID_PDF_ECHANGE_ACCUSE, CommonOffresPeer::CREATED_AT, CommonOffresPeer::UID_RESPONSE, CommonOffresPeer::DATE_DEPOT, CommonOffresPeer::RESULTAT_VERIFICATION_HASH_ALL_FILES, CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonOffresPeer::CONSULTATION_ID, CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonOffresPeer::INSCRIT_ID, CommonOffresPeer::TAUX_PRODUCTION_FRANCE, CommonOffresPeer::TAUX_PRODUCTION_EUROPE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ORGANISME', 'CONSULTATION_REF', 'ENTREPRISE_ID', 'ID_ETABLISSEMENT', 'OLD_INSCRIT_ID', 'SIGNATUREENVXML', 'HORODATAGE', 'MAILSIGNATAIRE', 'UNTRUSTEDDATE', 'UNTRUSTEDSERIAL', 'ENVOI_COMPLET', 'DATE_DEPOT_DIFFERE', 'HORODATAGE_ENVOI_DIFFERE', 'SIGNATUREENVXML_ENVOI_DIFFERE', 'EXTERNAL_SERIAL', 'INTERNAL_SERIAL', 'UID_OFFRE', 'OFFRE_SELECTIONNEE', 'OBSERVATION', 'XML_STRING', 'NOM_ENTREPRISE_INSCRIT', 'NOM_INSCRIT', 'PRENOM_INSCRIT', 'ADRESSE_INSCRIT', 'ADRESSE2_INSCRIT', 'TELEPHONE_INSCRIT', 'FAX_INSCRIT', 'CODE_POSTAL_INSCRIT', 'VILLE_INSCRIT', 'PAYS_INSCRIT', 'ACRONYME_PAYS', 'SIRET_ENTREPRISE', 'IDENTIFIANT_NATIONAL', 'EMAIL_INSCRIT', 'SIRET_INSCRIT', 'NOM_ENTREPRISE', 'HORODATAGE_ANNULATION', 'DATE_ANNULATION', 'SIGNATURE_ANNULATION', 'DEPOT_ANNULE', 'STRING_ANNULATION', 'VERIFICATION_CERTIFICAT_ANNULATION', 'OFFRE_VARIANTE', 'REPONSE_PAS_A_PAS', 'NUMERO_REPONSE', 'STATUT_OFFRES', 'DATE_HEURE_OUVERTURE', 'AGENTID_OUVERTURE', 'AGENTID_OUVERTURE2', 'DATE_HEURE_OUVERTURE_AGENT2', 'CRYPTAGE_REPONSE', 'NOM_AGENT_OUVERTURE', 'AGENT_TELECHARGEMENT_OFFRE', 'DATE_TELECHARGEMENT_OFFRE', 'REPERTOIRE_TELECHARGEMENT_OFFRE', 'CANDIDATURE_ID_EXTERNE', 'ETAT_CHIFFREMENT', 'ERREUR_CHIFFREMENT', 'DATE_FIN_CHIFFREMENT', 'DATE_HORODATAGE', 'VERIFICATION_HOTODATAGE', 'VERIFICATION_SIGNATURE_OFFRE', 'HORODATAGE_HASH_FICHIERS', 'ID_PDF_ECHANGE_ACCUSE', 'CREATED_AT', 'UID_RESPONSE', 'DATE_DEPOT', 'RESULTAT_VERIFICATION_HASH_ALL_FILES', 'ID_BLOB_HORODATAGE_HASH', 'ID_BLOB_XML_REPONSE', 'CONSULTATION_ID', 'PLATEFORME_VIRTUELLE_ID', 'INSCRIT_ID', 'TAUX_PRODUCTION_FRANCE', 'TAUX_PRODUCTION_EUROPE', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'organisme', 'consultation_ref', 'entreprise_id', 'id_etablissement', 'old_inscrit_id', 'signatureenvxml', 'horodatage', 'mailsignataire', 'untrusteddate', 'untrustedserial', 'envoi_complet', 'date_depot_differe', 'horodatage_envoi_differe', 'signatureenvxml_envoi_differe', 'external_serial', 'internal_serial', 'uid_offre', 'offre_selectionnee', 'Observation', 'xml_string', 'nom_entreprise_inscrit', 'nom_inscrit', 'prenom_inscrit', 'adresse_inscrit', 'adresse2_inscrit', 'telephone_inscrit', 'fax_inscrit', 'code_postal_inscrit', 'ville_inscrit', 'pays_inscrit', 'acronyme_pays', 'siret_entreprise', 'identifiant_national', 'email_inscrit', 'siret_inscrit', 'nom_entreprise', 'horodatage_annulation', 'date_annulation', 'signature_annulation', 'depot_annule', 'string_annulation', 'verification_certificat_annulation', 'offre_variante', 'reponse_pas_a_pas', 'numero_reponse', 'statut_offres', 'date_heure_ouverture', 'agentid_ouverture', 'agentid_ouverture2', 'date_heure_ouverture_agent2', 'cryptage_reponse', 'nom_agent_ouverture', 'agent_telechargement_offre', 'date_telechargement_offre', 'repertoire_telechargement_offre', 'candidature_id_externe', 'etat_chiffrement', 'erreur_chiffrement', 'date_fin_chiffrement', 'date_horodatage', 'verification_hotodatage', 'verification_signature_offre', 'horodatage_hash_fichiers', 'id_pdf_echange_accuse', 'created_at', 'uid_response', 'date_depot', 'resultat_verification_hash_all_files', 'id_blob_horodatage_hash', 'id_blob_xml_reponse', 'consultation_id', 'plateforme_virtuelle_id', 'inscrit_id', 'taux_production_france', 'taux_production_europe', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonOffresPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Organisme' => 1, 'ConsultationRef' => 2, 'EntrepriseId' => 3, 'IdEtablissement' => 4, 'OldInscritId' => 5, 'Signatureenvxml' => 6, 'Horodatage' => 7, 'Mailsignataire' => 8, 'Untrusteddate' => 9, 'Untrustedserial' => 10, 'EnvoiComplet' => 11, 'DateDepotDiffere' => 12, 'HorodatageEnvoiDiffere' => 13, 'SignatureenvxmlEnvoiDiffere' => 14, 'ExternalSerial' => 15, 'InternalSerial' => 16, 'UidOffre' => 17, 'OffreSelectionnee' => 18, 'Observation' => 19, 'XmlString' => 20, 'NomEntrepriseInscrit' => 21, 'NomInscrit' => 22, 'PrenomInscrit' => 23, 'AdresseInscrit' => 24, 'Adresse2Inscrit' => 25, 'TelephoneInscrit' => 26, 'FaxInscrit' => 27, 'CodePostalInscrit' => 28, 'VilleInscrit' => 29, 'PaysInscrit' => 30, 'AcronymePays' => 31, 'SiretEntreprise' => 32, 'IdentifiantNational' => 33, 'EmailInscrit' => 34, 'SiretInscrit' => 35, 'NomEntreprise' => 36, 'HorodatageAnnulation' => 37, 'DateAnnulation' => 38, 'SignatureAnnulation' => 39, 'DepotAnnule' => 40, 'StringAnnulation' => 41, 'VerificationCertificatAnnulation' => 42, 'OffreVariante' => 43, 'ReponsePasAPas' => 44, 'NumeroReponse' => 45, 'StatutOffres' => 46, 'DateHeureOuverture' => 47, 'AgentidOuverture' => 48, 'AgentidOuverture2' => 49, 'DateHeureOuvertureAgent2' => 50, 'CryptageReponse' => 51, 'NomAgentOuverture' => 52, 'AgentTelechargementOffre' => 53, 'DateTelechargementOffre' => 54, 'RepertoireTelechargementOffre' => 55, 'CandidatureIdExterne' => 56, 'EtatChiffrement' => 57, 'ErreurChiffrement' => 58, 'DateFinChiffrement' => 59, 'DateHorodatage' => 60, 'VerificationHotodatage' => 61, 'VerificationSignatureOffre' => 62, 'HorodatageHashFichiers' => 63, 'IdPdfEchangeAccuse' => 64, 'CreatedAt' => 65, 'UidResponse' => 66, 'DateDepot' => 67, 'ResultatVerificationHashAllFiles' => 68, 'IdBlobHorodatageHash' => 69, 'IdBlobXmlReponse' => 70, 'ConsultationId' => 71, 'PlateformeVirtuelleId' => 72, 'InscritId' => 73, 'TauxProductionFrance' => 74, 'TauxProductionEurope' => 75, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'organisme' => 1, 'consultationRef' => 2, 'entrepriseId' => 3, 'idEtablissement' => 4, 'oldInscritId' => 5, 'signatureenvxml' => 6, 'horodatage' => 7, 'mailsignataire' => 8, 'untrusteddate' => 9, 'untrustedserial' => 10, 'envoiComplet' => 11, 'dateDepotDiffere' => 12, 'horodatageEnvoiDiffere' => 13, 'signatureenvxmlEnvoiDiffere' => 14, 'externalSerial' => 15, 'internalSerial' => 16, 'uidOffre' => 17, 'offreSelectionnee' => 18, 'observation' => 19, 'xmlString' => 20, 'nomEntrepriseInscrit' => 21, 'nomInscrit' => 22, 'prenomInscrit' => 23, 'adresseInscrit' => 24, 'adresse2Inscrit' => 25, 'telephoneInscrit' => 26, 'faxInscrit' => 27, 'codePostalInscrit' => 28, 'villeInscrit' => 29, 'paysInscrit' => 30, 'acronymePays' => 31, 'siretEntreprise' => 32, 'identifiantNational' => 33, 'emailInscrit' => 34, 'siretInscrit' => 35, 'nomEntreprise' => 36, 'horodatageAnnulation' => 37, 'dateAnnulation' => 38, 'signatureAnnulation' => 39, 'depotAnnule' => 40, 'stringAnnulation' => 41, 'verificationCertificatAnnulation' => 42, 'offreVariante' => 43, 'reponsePasAPas' => 44, 'numeroReponse' => 45, 'statutOffres' => 46, 'dateHeureOuverture' => 47, 'agentidOuverture' => 48, 'agentidOuverture2' => 49, 'dateHeureOuvertureAgent2' => 50, 'cryptageReponse' => 51, 'nomAgentOuverture' => 52, 'agentTelechargementOffre' => 53, 'dateTelechargementOffre' => 54, 'repertoireTelechargementOffre' => 55, 'candidatureIdExterne' => 56, 'etatChiffrement' => 57, 'erreurChiffrement' => 58, 'dateFinChiffrement' => 59, 'dateHorodatage' => 60, 'verificationHotodatage' => 61, 'verificationSignatureOffre' => 62, 'horodatageHashFichiers' => 63, 'idPdfEchangeAccuse' => 64, 'createdAt' => 65, 'uidResponse' => 66, 'dateDepot' => 67, 'resultatVerificationHashAllFiles' => 68, 'idBlobHorodatageHash' => 69, 'idBlobXmlReponse' => 70, 'consultationId' => 71, 'plateformeVirtuelleId' => 72, 'inscritId' => 73, 'tauxProductionFrance' => 74, 'tauxProductionEurope' => 75, ),
        BasePeer::TYPE_COLNAME => array (CommonOffresPeer::ID => 0, CommonOffresPeer::ORGANISME => 1, CommonOffresPeer::CONSULTATION_REF => 2, CommonOffresPeer::ENTREPRISE_ID => 3, CommonOffresPeer::ID_ETABLISSEMENT => 4, CommonOffresPeer::OLD_INSCRIT_ID => 5, CommonOffresPeer::SIGNATUREENVXML => 6, CommonOffresPeer::HORODATAGE => 7, CommonOffresPeer::MAILSIGNATAIRE => 8, CommonOffresPeer::UNTRUSTEDDATE => 9, CommonOffresPeer::UNTRUSTEDSERIAL => 10, CommonOffresPeer::ENVOI_COMPLET => 11, CommonOffresPeer::DATE_DEPOT_DIFFERE => 12, CommonOffresPeer::HORODATAGE_ENVOI_DIFFERE => 13, CommonOffresPeer::SIGNATUREENVXML_ENVOI_DIFFERE => 14, CommonOffresPeer::EXTERNAL_SERIAL => 15, CommonOffresPeer::INTERNAL_SERIAL => 16, CommonOffresPeer::UID_OFFRE => 17, CommonOffresPeer::OFFRE_SELECTIONNEE => 18, CommonOffresPeer::OBSERVATION => 19, CommonOffresPeer::XML_STRING => 20, CommonOffresPeer::NOM_ENTREPRISE_INSCRIT => 21, CommonOffresPeer::NOM_INSCRIT => 22, CommonOffresPeer::PRENOM_INSCRIT => 23, CommonOffresPeer::ADRESSE_INSCRIT => 24, CommonOffresPeer::ADRESSE2_INSCRIT => 25, CommonOffresPeer::TELEPHONE_INSCRIT => 26, CommonOffresPeer::FAX_INSCRIT => 27, CommonOffresPeer::CODE_POSTAL_INSCRIT => 28, CommonOffresPeer::VILLE_INSCRIT => 29, CommonOffresPeer::PAYS_INSCRIT => 30, CommonOffresPeer::ACRONYME_PAYS => 31, CommonOffresPeer::SIRET_ENTREPRISE => 32, CommonOffresPeer::IDENTIFIANT_NATIONAL => 33, CommonOffresPeer::EMAIL_INSCRIT => 34, CommonOffresPeer::SIRET_INSCRIT => 35, CommonOffresPeer::NOM_ENTREPRISE => 36, CommonOffresPeer::HORODATAGE_ANNULATION => 37, CommonOffresPeer::DATE_ANNULATION => 38, CommonOffresPeer::SIGNATURE_ANNULATION => 39, CommonOffresPeer::DEPOT_ANNULE => 40, CommonOffresPeer::STRING_ANNULATION => 41, CommonOffresPeer::VERIFICATION_CERTIFICAT_ANNULATION => 42, CommonOffresPeer::OFFRE_VARIANTE => 43, CommonOffresPeer::REPONSE_PAS_A_PAS => 44, CommonOffresPeer::NUMERO_REPONSE => 45, CommonOffresPeer::STATUT_OFFRES => 46, CommonOffresPeer::DATE_HEURE_OUVERTURE => 47, CommonOffresPeer::AGENTID_OUVERTURE => 48, CommonOffresPeer::AGENTID_OUVERTURE2 => 49, CommonOffresPeer::DATE_HEURE_OUVERTURE_AGENT2 => 50, CommonOffresPeer::CRYPTAGE_REPONSE => 51, CommonOffresPeer::NOM_AGENT_OUVERTURE => 52, CommonOffresPeer::AGENT_TELECHARGEMENT_OFFRE => 53, CommonOffresPeer::DATE_TELECHARGEMENT_OFFRE => 54, CommonOffresPeer::REPERTOIRE_TELECHARGEMENT_OFFRE => 55, CommonOffresPeer::CANDIDATURE_ID_EXTERNE => 56, CommonOffresPeer::ETAT_CHIFFREMENT => 57, CommonOffresPeer::ERREUR_CHIFFREMENT => 58, CommonOffresPeer::DATE_FIN_CHIFFREMENT => 59, CommonOffresPeer::DATE_HORODATAGE => 60, CommonOffresPeer::VERIFICATION_HOTODATAGE => 61, CommonOffresPeer::VERIFICATION_SIGNATURE_OFFRE => 62, CommonOffresPeer::HORODATAGE_HASH_FICHIERS => 63, CommonOffresPeer::ID_PDF_ECHANGE_ACCUSE => 64, CommonOffresPeer::CREATED_AT => 65, CommonOffresPeer::UID_RESPONSE => 66, CommonOffresPeer::DATE_DEPOT => 67, CommonOffresPeer::RESULTAT_VERIFICATION_HASH_ALL_FILES => 68, CommonOffresPeer::ID_BLOB_HORODATAGE_HASH => 69, CommonOffresPeer::ID_BLOB_XML_REPONSE => 70, CommonOffresPeer::CONSULTATION_ID => 71, CommonOffresPeer::PLATEFORME_VIRTUELLE_ID => 72, CommonOffresPeer::INSCRIT_ID => 73, CommonOffresPeer::TAUX_PRODUCTION_FRANCE => 74, CommonOffresPeer::TAUX_PRODUCTION_EUROPE => 75, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ORGANISME' => 1, 'CONSULTATION_REF' => 2, 'ENTREPRISE_ID' => 3, 'ID_ETABLISSEMENT' => 4, 'OLD_INSCRIT_ID' => 5, 'SIGNATUREENVXML' => 6, 'HORODATAGE' => 7, 'MAILSIGNATAIRE' => 8, 'UNTRUSTEDDATE' => 9, 'UNTRUSTEDSERIAL' => 10, 'ENVOI_COMPLET' => 11, 'DATE_DEPOT_DIFFERE' => 12, 'HORODATAGE_ENVOI_DIFFERE' => 13, 'SIGNATUREENVXML_ENVOI_DIFFERE' => 14, 'EXTERNAL_SERIAL' => 15, 'INTERNAL_SERIAL' => 16, 'UID_OFFRE' => 17, 'OFFRE_SELECTIONNEE' => 18, 'OBSERVATION' => 19, 'XML_STRING' => 20, 'NOM_ENTREPRISE_INSCRIT' => 21, 'NOM_INSCRIT' => 22, 'PRENOM_INSCRIT' => 23, 'ADRESSE_INSCRIT' => 24, 'ADRESSE2_INSCRIT' => 25, 'TELEPHONE_INSCRIT' => 26, 'FAX_INSCRIT' => 27, 'CODE_POSTAL_INSCRIT' => 28, 'VILLE_INSCRIT' => 29, 'PAYS_INSCRIT' => 30, 'ACRONYME_PAYS' => 31, 'SIRET_ENTREPRISE' => 32, 'IDENTIFIANT_NATIONAL' => 33, 'EMAIL_INSCRIT' => 34, 'SIRET_INSCRIT' => 35, 'NOM_ENTREPRISE' => 36, 'HORODATAGE_ANNULATION' => 37, 'DATE_ANNULATION' => 38, 'SIGNATURE_ANNULATION' => 39, 'DEPOT_ANNULE' => 40, 'STRING_ANNULATION' => 41, 'VERIFICATION_CERTIFICAT_ANNULATION' => 42, 'OFFRE_VARIANTE' => 43, 'REPONSE_PAS_A_PAS' => 44, 'NUMERO_REPONSE' => 45, 'STATUT_OFFRES' => 46, 'DATE_HEURE_OUVERTURE' => 47, 'AGENTID_OUVERTURE' => 48, 'AGENTID_OUVERTURE2' => 49, 'DATE_HEURE_OUVERTURE_AGENT2' => 50, 'CRYPTAGE_REPONSE' => 51, 'NOM_AGENT_OUVERTURE' => 52, 'AGENT_TELECHARGEMENT_OFFRE' => 53, 'DATE_TELECHARGEMENT_OFFRE' => 54, 'REPERTOIRE_TELECHARGEMENT_OFFRE' => 55, 'CANDIDATURE_ID_EXTERNE' => 56, 'ETAT_CHIFFREMENT' => 57, 'ERREUR_CHIFFREMENT' => 58, 'DATE_FIN_CHIFFREMENT' => 59, 'DATE_HORODATAGE' => 60, 'VERIFICATION_HOTODATAGE' => 61, 'VERIFICATION_SIGNATURE_OFFRE' => 62, 'HORODATAGE_HASH_FICHIERS' => 63, 'ID_PDF_ECHANGE_ACCUSE' => 64, 'CREATED_AT' => 65, 'UID_RESPONSE' => 66, 'DATE_DEPOT' => 67, 'RESULTAT_VERIFICATION_HASH_ALL_FILES' => 68, 'ID_BLOB_HORODATAGE_HASH' => 69, 'ID_BLOB_XML_REPONSE' => 70, 'CONSULTATION_ID' => 71, 'PLATEFORME_VIRTUELLE_ID' => 72, 'INSCRIT_ID' => 73, 'TAUX_PRODUCTION_FRANCE' => 74, 'TAUX_PRODUCTION_EUROPE' => 75, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'organisme' => 1, 'consultation_ref' => 2, 'entreprise_id' => 3, 'id_etablissement' => 4, 'old_inscrit_id' => 5, 'signatureenvxml' => 6, 'horodatage' => 7, 'mailsignataire' => 8, 'untrusteddate' => 9, 'untrustedserial' => 10, 'envoi_complet' => 11, 'date_depot_differe' => 12, 'horodatage_envoi_differe' => 13, 'signatureenvxml_envoi_differe' => 14, 'external_serial' => 15, 'internal_serial' => 16, 'uid_offre' => 17, 'offre_selectionnee' => 18, 'Observation' => 19, 'xml_string' => 20, 'nom_entreprise_inscrit' => 21, 'nom_inscrit' => 22, 'prenom_inscrit' => 23, 'adresse_inscrit' => 24, 'adresse2_inscrit' => 25, 'telephone_inscrit' => 26, 'fax_inscrit' => 27, 'code_postal_inscrit' => 28, 'ville_inscrit' => 29, 'pays_inscrit' => 30, 'acronyme_pays' => 31, 'siret_entreprise' => 32, 'identifiant_national' => 33, 'email_inscrit' => 34, 'siret_inscrit' => 35, 'nom_entreprise' => 36, 'horodatage_annulation' => 37, 'date_annulation' => 38, 'signature_annulation' => 39, 'depot_annule' => 40, 'string_annulation' => 41, 'verification_certificat_annulation' => 42, 'offre_variante' => 43, 'reponse_pas_a_pas' => 44, 'numero_reponse' => 45, 'statut_offres' => 46, 'date_heure_ouverture' => 47, 'agentid_ouverture' => 48, 'agentid_ouverture2' => 49, 'date_heure_ouverture_agent2' => 50, 'cryptage_reponse' => 51, 'nom_agent_ouverture' => 52, 'agent_telechargement_offre' => 53, 'date_telechargement_offre' => 54, 'repertoire_telechargement_offre' => 55, 'candidature_id_externe' => 56, 'etat_chiffrement' => 57, 'erreur_chiffrement' => 58, 'date_fin_chiffrement' => 59, 'date_horodatage' => 60, 'verification_hotodatage' => 61, 'verification_signature_offre' => 62, 'horodatage_hash_fichiers' => 63, 'id_pdf_echange_accuse' => 64, 'created_at' => 65, 'uid_response' => 66, 'date_depot' => 67, 'resultat_verification_hash_all_files' => 68, 'id_blob_horodatage_hash' => 69, 'id_blob_xml_reponse' => 70, 'consultation_id' => 71, 'plateforme_virtuelle_id' => 72, 'inscrit_id' => 73, 'taux_production_france' => 74, 'taux_production_europe' => 75, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonOffresPeer::DEPOT_ANNULE => array(
            CommonOffresPeer::DEPOT_ANNULE_0,
            CommonOffresPeer::DEPOT_ANNULE_1,
        ),
        CommonOffresPeer::OFFRE_VARIANTE => array(
            CommonOffresPeer::OFFRE_VARIANTE_0,
            CommonOffresPeer::OFFRE_VARIANTE_1,
        ),
        CommonOffresPeer::REPONSE_PAS_A_PAS => array(
            CommonOffresPeer::REPONSE_PAS_A_PAS_0,
            CommonOffresPeer::REPONSE_PAS_A_PAS_1,
        ),
        CommonOffresPeer::VERIFICATION_HOTODATAGE => array(
            CommonOffresPeer::VERIFICATION_HOTODATAGE_0,
            CommonOffresPeer::VERIFICATION_HOTODATAGE_1,
            CommonOffresPeer::VERIFICATION_HOTODATAGE_2,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonOffresPeer::getFieldNames($toType);
        $key = isset(CommonOffresPeer::$fieldKeys[$fromType][$name]) ? CommonOffresPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonOffresPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonOffresPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonOffresPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonOffresPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonOffresPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonOffresPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonOffresPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonOffresPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonOffresPeer::ID);
            $criteria->addSelectColumn(CommonOffresPeer::ORGANISME);
            $criteria->addSelectColumn(CommonOffresPeer::CONSULTATION_REF);
            $criteria->addSelectColumn(CommonOffresPeer::ENTREPRISE_ID);
            $criteria->addSelectColumn(CommonOffresPeer::ID_ETABLISSEMENT);
            $criteria->addSelectColumn(CommonOffresPeer::OLD_INSCRIT_ID);
            $criteria->addSelectColumn(CommonOffresPeer::SIGNATUREENVXML);
            $criteria->addSelectColumn(CommonOffresPeer::HORODATAGE);
            $criteria->addSelectColumn(CommonOffresPeer::MAILSIGNATAIRE);
            $criteria->addSelectColumn(CommonOffresPeer::UNTRUSTEDDATE);
            $criteria->addSelectColumn(CommonOffresPeer::UNTRUSTEDSERIAL);
            $criteria->addSelectColumn(CommonOffresPeer::ENVOI_COMPLET);
            $criteria->addSelectColumn(CommonOffresPeer::DATE_DEPOT_DIFFERE);
            $criteria->addSelectColumn(CommonOffresPeer::HORODATAGE_ENVOI_DIFFERE);
            $criteria->addSelectColumn(CommonOffresPeer::SIGNATUREENVXML_ENVOI_DIFFERE);
            $criteria->addSelectColumn(CommonOffresPeer::EXTERNAL_SERIAL);
            $criteria->addSelectColumn(CommonOffresPeer::INTERNAL_SERIAL);
            $criteria->addSelectColumn(CommonOffresPeer::UID_OFFRE);
            $criteria->addSelectColumn(CommonOffresPeer::OFFRE_SELECTIONNEE);
            $criteria->addSelectColumn(CommonOffresPeer::OBSERVATION);
            $criteria->addSelectColumn(CommonOffresPeer::XML_STRING);
            $criteria->addSelectColumn(CommonOffresPeer::NOM_ENTREPRISE_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::NOM_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::PRENOM_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::ADRESSE_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::ADRESSE2_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::TELEPHONE_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::FAX_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::CODE_POSTAL_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::VILLE_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::PAYS_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::ACRONYME_PAYS);
            $criteria->addSelectColumn(CommonOffresPeer::SIRET_ENTREPRISE);
            $criteria->addSelectColumn(CommonOffresPeer::IDENTIFIANT_NATIONAL);
            $criteria->addSelectColumn(CommonOffresPeer::EMAIL_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::SIRET_INSCRIT);
            $criteria->addSelectColumn(CommonOffresPeer::NOM_ENTREPRISE);
            $criteria->addSelectColumn(CommonOffresPeer::HORODATAGE_ANNULATION);
            $criteria->addSelectColumn(CommonOffresPeer::DATE_ANNULATION);
            $criteria->addSelectColumn(CommonOffresPeer::SIGNATURE_ANNULATION);
            $criteria->addSelectColumn(CommonOffresPeer::DEPOT_ANNULE);
            $criteria->addSelectColumn(CommonOffresPeer::STRING_ANNULATION);
            $criteria->addSelectColumn(CommonOffresPeer::VERIFICATION_CERTIFICAT_ANNULATION);
            $criteria->addSelectColumn(CommonOffresPeer::OFFRE_VARIANTE);
            $criteria->addSelectColumn(CommonOffresPeer::REPONSE_PAS_A_PAS);
            $criteria->addSelectColumn(CommonOffresPeer::NUMERO_REPONSE);
            $criteria->addSelectColumn(CommonOffresPeer::STATUT_OFFRES);
            $criteria->addSelectColumn(CommonOffresPeer::DATE_HEURE_OUVERTURE);
            $criteria->addSelectColumn(CommonOffresPeer::AGENTID_OUVERTURE);
            $criteria->addSelectColumn(CommonOffresPeer::AGENTID_OUVERTURE2);
            $criteria->addSelectColumn(CommonOffresPeer::DATE_HEURE_OUVERTURE_AGENT2);
            $criteria->addSelectColumn(CommonOffresPeer::CRYPTAGE_REPONSE);
            $criteria->addSelectColumn(CommonOffresPeer::NOM_AGENT_OUVERTURE);
            $criteria->addSelectColumn(CommonOffresPeer::AGENT_TELECHARGEMENT_OFFRE);
            $criteria->addSelectColumn(CommonOffresPeer::DATE_TELECHARGEMENT_OFFRE);
            $criteria->addSelectColumn(CommonOffresPeer::REPERTOIRE_TELECHARGEMENT_OFFRE);
            $criteria->addSelectColumn(CommonOffresPeer::CANDIDATURE_ID_EXTERNE);
            $criteria->addSelectColumn(CommonOffresPeer::ETAT_CHIFFREMENT);
            $criteria->addSelectColumn(CommonOffresPeer::ERREUR_CHIFFREMENT);
            $criteria->addSelectColumn(CommonOffresPeer::DATE_FIN_CHIFFREMENT);
            $criteria->addSelectColumn(CommonOffresPeer::DATE_HORODATAGE);
            $criteria->addSelectColumn(CommonOffresPeer::VERIFICATION_HOTODATAGE);
            $criteria->addSelectColumn(CommonOffresPeer::VERIFICATION_SIGNATURE_OFFRE);
            $criteria->addSelectColumn(CommonOffresPeer::HORODATAGE_HASH_FICHIERS);
            $criteria->addSelectColumn(CommonOffresPeer::ID_PDF_ECHANGE_ACCUSE);
            $criteria->addSelectColumn(CommonOffresPeer::CREATED_AT);
            $criteria->addSelectColumn(CommonOffresPeer::UID_RESPONSE);
            $criteria->addSelectColumn(CommonOffresPeer::DATE_DEPOT);
            $criteria->addSelectColumn(CommonOffresPeer::RESULTAT_VERIFICATION_HASH_ALL_FILES);
            $criteria->addSelectColumn(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH);
            $criteria->addSelectColumn(CommonOffresPeer::ID_BLOB_XML_REPONSE);
            $criteria->addSelectColumn(CommonOffresPeer::CONSULTATION_ID);
            $criteria->addSelectColumn(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID);
            $criteria->addSelectColumn(CommonOffresPeer::INSCRIT_ID);
            $criteria->addSelectColumn(CommonOffresPeer::TAUX_PRODUCTION_FRANCE);
            $criteria->addSelectColumn(CommonOffresPeer::TAUX_PRODUCTION_EUROPE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.consultation_ref');
            $criteria->addSelectColumn($alias . '.entreprise_id');
            $criteria->addSelectColumn($alias . '.id_etablissement');
            $criteria->addSelectColumn($alias . '.old_inscrit_id');
            $criteria->addSelectColumn($alias . '.signatureenvxml');
            $criteria->addSelectColumn($alias . '.horodatage');
            $criteria->addSelectColumn($alias . '.mailsignataire');
            $criteria->addSelectColumn($alias . '.untrusteddate');
            $criteria->addSelectColumn($alias . '.untrustedserial');
            $criteria->addSelectColumn($alias . '.envoi_complet');
            $criteria->addSelectColumn($alias . '.date_depot_differe');
            $criteria->addSelectColumn($alias . '.horodatage_envoi_differe');
            $criteria->addSelectColumn($alias . '.signatureenvxml_envoi_differe');
            $criteria->addSelectColumn($alias . '.external_serial');
            $criteria->addSelectColumn($alias . '.internal_serial');
            $criteria->addSelectColumn($alias . '.uid_offre');
            $criteria->addSelectColumn($alias . '.offre_selectionnee');
            $criteria->addSelectColumn($alias . '.Observation');
            $criteria->addSelectColumn($alias . '.xml_string');
            $criteria->addSelectColumn($alias . '.nom_entreprise_inscrit');
            $criteria->addSelectColumn($alias . '.nom_inscrit');
            $criteria->addSelectColumn($alias . '.prenom_inscrit');
            $criteria->addSelectColumn($alias . '.adresse_inscrit');
            $criteria->addSelectColumn($alias . '.adresse2_inscrit');
            $criteria->addSelectColumn($alias . '.telephone_inscrit');
            $criteria->addSelectColumn($alias . '.fax_inscrit');
            $criteria->addSelectColumn($alias . '.code_postal_inscrit');
            $criteria->addSelectColumn($alias . '.ville_inscrit');
            $criteria->addSelectColumn($alias . '.pays_inscrit');
            $criteria->addSelectColumn($alias . '.acronyme_pays');
            $criteria->addSelectColumn($alias . '.siret_entreprise');
            $criteria->addSelectColumn($alias . '.identifiant_national');
            $criteria->addSelectColumn($alias . '.email_inscrit');
            $criteria->addSelectColumn($alias . '.siret_inscrit');
            $criteria->addSelectColumn($alias . '.nom_entreprise');
            $criteria->addSelectColumn($alias . '.horodatage_annulation');
            $criteria->addSelectColumn($alias . '.date_annulation');
            $criteria->addSelectColumn($alias . '.signature_annulation');
            $criteria->addSelectColumn($alias . '.depot_annule');
            $criteria->addSelectColumn($alias . '.string_annulation');
            $criteria->addSelectColumn($alias . '.verification_certificat_annulation');
            $criteria->addSelectColumn($alias . '.offre_variante');
            $criteria->addSelectColumn($alias . '.reponse_pas_a_pas');
            $criteria->addSelectColumn($alias . '.numero_reponse');
            $criteria->addSelectColumn($alias . '.statut_offres');
            $criteria->addSelectColumn($alias . '.date_heure_ouverture');
            $criteria->addSelectColumn($alias . '.agentid_ouverture');
            $criteria->addSelectColumn($alias . '.agentid_ouverture2');
            $criteria->addSelectColumn($alias . '.date_heure_ouverture_agent2');
            $criteria->addSelectColumn($alias . '.cryptage_reponse');
            $criteria->addSelectColumn($alias . '.nom_agent_ouverture');
            $criteria->addSelectColumn($alias . '.agent_telechargement_offre');
            $criteria->addSelectColumn($alias . '.date_telechargement_offre');
            $criteria->addSelectColumn($alias . '.repertoire_telechargement_offre');
            $criteria->addSelectColumn($alias . '.candidature_id_externe');
            $criteria->addSelectColumn($alias . '.etat_chiffrement');
            $criteria->addSelectColumn($alias . '.erreur_chiffrement');
            $criteria->addSelectColumn($alias . '.date_fin_chiffrement');
            $criteria->addSelectColumn($alias . '.date_horodatage');
            $criteria->addSelectColumn($alias . '.verification_hotodatage');
            $criteria->addSelectColumn($alias . '.verification_signature_offre');
            $criteria->addSelectColumn($alias . '.horodatage_hash_fichiers');
            $criteria->addSelectColumn($alias . '.id_pdf_echange_accuse');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.uid_response');
            $criteria->addSelectColumn($alias . '.date_depot');
            $criteria->addSelectColumn($alias . '.resultat_verification_hash_all_files');
            $criteria->addSelectColumn($alias . '.id_blob_horodatage_hash');
            $criteria->addSelectColumn($alias . '.id_blob_xml_reponse');
            $criteria->addSelectColumn($alias . '.consultation_id');
            $criteria->addSelectColumn($alias . '.plateforme_virtuelle_id');
            $criteria->addSelectColumn($alias . '.inscrit_id');
            $criteria->addSelectColumn($alias . '.taux_production_france');
            $criteria->addSelectColumn($alias . '.taux_production_europe');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonOffres
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonOffresPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonOffresPeer::populateObjects(CommonOffresPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonOffresPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonOffres $obj A CommonOffres object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getId(), (string) $obj->getOrganisme()));
            } // if key === null
            CommonOffresPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonOffres object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonOffres) {
                $key = serialize(array((string) $value->getId(), (string) $value->getOrganisme()));
            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonOffres object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonOffresPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonOffres Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonOffresPeer::$instances[$key])) {
                return CommonOffresPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonOffresPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonOffresPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to Offres
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((int) $row[$startcol], (string) $row[$startcol + 1]);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonOffresPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonOffresPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonOffresPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonOffres object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonOffresPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonOffresPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonOffresPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonOffresPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonOffresPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonBlobOrganismeFileRelatedByIdBlobHorodatageHash table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonInscrit table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonInscrit(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Entreprise table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinEntreprise(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonPlateformeVirtuelle table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonPlateformeVirtuelle(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonBlobOrganismeFileRelatedByIdBlobXmlReponse table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonBlobOrganismeFileRelatedByIdBlobXmlReponse(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with their CommonBlobOrganismeFile objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol = CommonOffresPeer::NUM_HYDRATE_COLUMNS;
        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonOffres) to $obj2 (CommonBlobOrganismeFile)
                $obj2->addCommonOffresRelatedByIdBlobHorodatageHash($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with their CommonConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol = CommonOffresPeer::NUM_HYDRATE_COLUMNS;
        CommonConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonOffres) to $obj2 (CommonConsultation)
                $obj2->addCommonOffres($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with their CommonInscrit objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonInscrit(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol = CommonOffresPeer::NUM_HYDRATE_COLUMNS;
        CommonInscritPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonInscritPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonInscritPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonInscritPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonOffres) to $obj2 (CommonInscrit)
                $obj2->addCommonOffres($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with their Entreprise objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol = CommonOffresPeer::NUM_HYDRATE_COLUMNS;
        EntreprisePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonOffres) to $obj2 (Entreprise)
                $obj2->addCommonOffres($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with their CommonPlateformeVirtuelle objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonPlateformeVirtuelle(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol = CommonOffresPeer::NUM_HYDRATE_COLUMNS;
        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonOffres) to $obj2 (CommonPlateformeVirtuelle)
                $obj2->addCommonOffres($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with their CommonBlobOrganismeFile objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonBlobOrganismeFileRelatedByIdBlobXmlReponse(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol = CommonOffresPeer::NUM_HYDRATE_COLUMNS;
        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonOffres) to $obj2 (CommonBlobOrganismeFile)
                $obj2->addCommonOffresRelatedByIdBlobXmlReponse($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonOffres objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol2 = CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonBlobOrganismeFile rows

            $key2 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj2 (CommonBlobOrganismeFile)
                $obj2->addCommonOffresRelatedByIdBlobHorodatageHash($obj1);
            } // if joined row not null

            // Add objects for joined CommonConsultation rows

            $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonOffres($obj1);
            } // if joined row not null

            // Add objects for joined CommonInscrit rows

            $key4 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonInscritPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonInscritPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonInscritPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj4 (CommonInscrit)
                $obj4->addCommonOffres($obj1);
            } // if joined row not null

            // Add objects for joined Entreprise rows

            $key5 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = EntreprisePeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = EntreprisePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    EntreprisePeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj5 (Entreprise)
                $obj5->addCommonOffres($obj1);
            } // if joined row not null

            // Add objects for joined CommonPlateformeVirtuelle rows

            $key6 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj6 (CommonPlateformeVirtuelle)
                $obj6->addCommonOffres($obj1);
            } // if joined row not null

            // Add objects for joined CommonBlobOrganismeFile rows

            $key7 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol7);
            if ($key7 !== null) {
                $obj7 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key7);
                if (!$obj7) {

                    $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj7, $key7);
                } // if obj7 loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj7 (CommonBlobOrganismeFile)
                $obj7->addCommonOffresRelatedByIdBlobXmlReponse($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonBlobOrganismeFileRelatedByIdBlobHorodatageHash table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonInscrit table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonInscrit(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Entreprise table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptEntreprise(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonPlateformeVirtuelle table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonPlateformeVirtuelle(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonBlobOrganismeFileRelatedByIdBlobXmlReponse table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonBlobOrganismeFileRelatedByIdBlobXmlReponse(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonOffresPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with all related objects except CommonBlobOrganismeFileRelatedByIdBlobHorodatageHash.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol2 = CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonConsultation rows

                $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key3 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonInscritPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonInscritPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj3 (CommonInscrit)
                $obj3->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined Entreprise rows

                $key4 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = EntreprisePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    EntreprisePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj4 (Entreprise)
                $obj4->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key5 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj5 (CommonPlateformeVirtuelle)
                $obj5->addCommonOffres($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with all related objects except CommonConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol2 = CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonBlobOrganismeFile rows

                $key2 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj2 (CommonBlobOrganismeFile)
                $obj2->addCommonOffresRelatedByIdBlobHorodatageHash($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key3 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonInscritPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonInscritPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj3 (CommonInscrit)
                $obj3->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined Entreprise rows

                $key4 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = EntreprisePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    EntreprisePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj4 (Entreprise)
                $obj4->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key5 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj5 (CommonPlateformeVirtuelle)
                $obj5->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonBlobOrganismeFile rows

                $key6 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj6 (CommonBlobOrganismeFile)
                $obj6->addCommonOffresRelatedByIdBlobXmlReponse($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with all related objects except CommonInscrit.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonInscrit(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol2 = CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonBlobOrganismeFile rows

                $key2 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj2 (CommonBlobOrganismeFile)
                $obj2->addCommonOffresRelatedByIdBlobHorodatageHash($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined Entreprise rows

                $key4 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = EntreprisePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    EntreprisePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj4 (Entreprise)
                $obj4->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key5 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj5 (CommonPlateformeVirtuelle)
                $obj5->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonBlobOrganismeFile rows

                $key6 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj6 (CommonBlobOrganismeFile)
                $obj6->addCommonOffresRelatedByIdBlobXmlReponse($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with all related objects except Entreprise.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol2 = CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonBlobOrganismeFile rows

                $key2 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj2 (CommonBlobOrganismeFile)
                $obj2->addCommonOffresRelatedByIdBlobHorodatageHash($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key4 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonInscritPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonInscritPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj4 (CommonInscrit)
                $obj4->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key5 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj5 (CommonPlateformeVirtuelle)
                $obj5->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonBlobOrganismeFile rows

                $key6 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj6 (CommonBlobOrganismeFile)
                $obj6->addCommonOffresRelatedByIdBlobXmlReponse($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with all related objects except CommonPlateformeVirtuelle.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonPlateformeVirtuelle(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol2 = CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ID_BLOB_XML_REPONSE, CommonBlobOrganismeFilePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonBlobOrganismeFile rows

                $key2 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj2 (CommonBlobOrganismeFile)
                $obj2->addCommonOffresRelatedByIdBlobHorodatageHash($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key4 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonInscritPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonInscritPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj4 (CommonInscrit)
                $obj4->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined Entreprise rows

                $key5 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = EntreprisePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    EntreprisePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj5 (Entreprise)
                $obj5->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonBlobOrganismeFile rows

                $key6 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj6 (CommonBlobOrganismeFile)
                $obj6->addCommonOffresRelatedByIdBlobXmlReponse($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonOffres objects pre-filled with all related objects except CommonBlobOrganismeFileRelatedByIdBlobXmlReponse.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonOffres objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonBlobOrganismeFileRelatedByIdBlobXmlReponse(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);
        }

        CommonOffresPeer::addSelectColumns($criteria);
        $startcol2 = CommonOffresPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonOffresPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::INSCRIT_ID, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonOffresPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonOffresPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonOffresPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonOffresPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonConsultation rows

                $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key3 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonInscritPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonInscritPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj3 (CommonInscrit)
                $obj3->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined Entreprise rows

                $key4 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = EntreprisePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    EntreprisePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj4 (Entreprise)
                $obj4->addCommonOffres($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key5 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonOffres) to the collection in $obj5 (CommonPlateformeVirtuelle)
                $obj5->addCommonOffres($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonOffresPeer::DATABASE_NAME)->getTable(CommonOffresPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonOffresPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonOffresPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonOffresTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonOffresPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonOffres or Criteria object.
     *
     * @param      mixed $values Criteria or CommonOffres object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonOffres object
        }

        if ($criteria->containsKey(CommonOffresPeer::ID) && $criteria->keyContainsValue(CommonOffresPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonOffresPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonOffres or Criteria object.
     *
     * @param      mixed $values Criteria or CommonOffres object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonOffresPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonOffresPeer::ID);
            $value = $criteria->remove(CommonOffresPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonOffresPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(CommonOffresPeer::ORGANISME);
            $value = $criteria->remove(CommonOffresPeer::ORGANISME);
            if ($value) {
                $selectCriteria->add(CommonOffresPeer::ORGANISME, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonOffresPeer::TABLE_NAME);
            }

        } else { // $values is CommonOffres object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the Offres table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonOffresPeer::TABLE_NAME, $con, CommonOffresPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonOffresPeer::clearInstancePool();
            CommonOffresPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonOffres or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonOffres object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonOffresPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonOffres) { // it's a model object
            // invalidate the cache for this single object
            CommonOffresPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonOffresPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(CommonOffresPeer::ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(CommonOffresPeer::ORGANISME, $value[1]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                CommonOffresPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonOffresPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonOffresPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonOffres object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonOffres $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonOffresPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonOffresPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonOffresPeer::DATABASE_NAME, CommonOffresPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   int $id
     * @param   string $organisme
     * @param      PropelPDO $con
     * @return   CommonOffres
     */
    public static function retrieveByPK($id, $organisme, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $id, (string) $organisme));
         if (null !== ($obj = CommonOffresPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(CommonOffresPeer::DATABASE_NAME);
        $criteria->add(CommonOffresPeer::ID, $id);
        $criteria->add(CommonOffresPeer::ORGANISME, $organisme);
        $v = CommonOffresPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BaseCommonOffresPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonOffresPeer::buildTableMap();

