<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentTechniqueToken;
use Application\Propel\Mpe\CommonAgentTechniqueTokenPeer;
use Application\Propel\Mpe\CommonAgentTechniqueTokenQuery;

/**
 * Base class that represents a query for the 'agent_technique_token' table.
 *
 *
 *
 * @method CommonAgentTechniqueTokenQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonAgentTechniqueTokenQuery orderByAgentId($order = Criteria::ASC) Order by the agent_id column
 * @method CommonAgentTechniqueTokenQuery orderByToken($order = Criteria::ASC) Order by the token column
 * @method CommonAgentTechniqueTokenQuery orderByCreateAt($order = Criteria::ASC) Order by the create_at column
 * @method CommonAgentTechniqueTokenQuery orderByUpdateAt($order = Criteria::ASC) Order by the update_at column
 * @method CommonAgentTechniqueTokenQuery orderByDateExpiration($order = Criteria::ASC) Order by the date_expiration column
 *
 * @method CommonAgentTechniqueTokenQuery groupById() Group by the id column
 * @method CommonAgentTechniqueTokenQuery groupByAgentId() Group by the agent_id column
 * @method CommonAgentTechniqueTokenQuery groupByToken() Group by the token column
 * @method CommonAgentTechniqueTokenQuery groupByCreateAt() Group by the create_at column
 * @method CommonAgentTechniqueTokenQuery groupByUpdateAt() Group by the update_at column
 * @method CommonAgentTechniqueTokenQuery groupByDateExpiration() Group by the date_expiration column
 *
 * @method CommonAgentTechniqueTokenQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonAgentTechniqueTokenQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonAgentTechniqueTokenQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonAgentTechniqueTokenQuery leftJoinCommonAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgent relation
 * @method CommonAgentTechniqueTokenQuery rightJoinCommonAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgent relation
 * @method CommonAgentTechniqueTokenQuery innerJoinCommonAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgent relation
 *
 * @method CommonAgentTechniqueToken findOne(PropelPDO $con = null) Return the first CommonAgentTechniqueToken matching the query
 * @method CommonAgentTechniqueToken findOneOrCreate(PropelPDO $con = null) Return the first CommonAgentTechniqueToken matching the query, or a new CommonAgentTechniqueToken object populated from the query conditions when no match is found
 *
 * @method CommonAgentTechniqueToken findOneByAgentId(int $agent_id) Return the first CommonAgentTechniqueToken filtered by the agent_id column
 * @method CommonAgentTechniqueToken findOneByToken(string $token) Return the first CommonAgentTechniqueToken filtered by the token column
 * @method CommonAgentTechniqueToken findOneByCreateAt(string $create_at) Return the first CommonAgentTechniqueToken filtered by the create_at column
 * @method CommonAgentTechniqueToken findOneByUpdateAt(string $update_at) Return the first CommonAgentTechniqueToken filtered by the update_at column
 * @method CommonAgentTechniqueToken findOneByDateExpiration(string $date_expiration) Return the first CommonAgentTechniqueToken filtered by the date_expiration column
 *
 * @method array findById(int $id) Return CommonAgentTechniqueToken objects filtered by the id column
 * @method array findByAgentId(int $agent_id) Return CommonAgentTechniqueToken objects filtered by the agent_id column
 * @method array findByToken(string $token) Return CommonAgentTechniqueToken objects filtered by the token column
 * @method array findByCreateAt(string $create_at) Return CommonAgentTechniqueToken objects filtered by the create_at column
 * @method array findByUpdateAt(string $update_at) Return CommonAgentTechniqueToken objects filtered by the update_at column
 * @method array findByDateExpiration(string $date_expiration) Return CommonAgentTechniqueToken objects filtered by the date_expiration column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonAgentTechniqueTokenQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonAgentTechniqueTokenQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonAgentTechniqueToken', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonAgentTechniqueTokenQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonAgentTechniqueTokenQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonAgentTechniqueTokenQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonAgentTechniqueTokenQuery) {
            return $criteria;
        }
        $query = new CommonAgentTechniqueTokenQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonAgentTechniqueToken|CommonAgentTechniqueToken[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonAgentTechniqueTokenPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonAgentTechniqueTokenPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonAgentTechniqueToken A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonAgentTechniqueToken A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `agent_id`, `token`, `create_at`, `update_at`, `date_expiration` FROM `agent_technique_token` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonAgentTechniqueToken();
            $obj->hydrate($row);
            CommonAgentTechniqueTokenPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonAgentTechniqueToken|CommonAgentTechniqueToken[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonAgentTechniqueToken[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonAgentTechniqueTokenPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonAgentTechniqueTokenPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentTechniqueTokenPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the agent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAgentId(1234); // WHERE agent_id = 1234
     * $query->filterByAgentId(array(12, 34)); // WHERE agent_id IN (12, 34)
     * $query->filterByAgentId(array('min' => 12)); // WHERE agent_id >= 12
     * $query->filterByAgentId(array('max' => 12)); // WHERE agent_id <= 12
     * </code>
     *
     * @see       filterByCommonAgent()
     *
     * @param     mixed $agentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function filterByAgentId($agentId = null, $comparison = null)
    {
        if (is_array($agentId)) {
            $useMinMax = false;
            if (isset($agentId['min'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::AGENT_ID, $agentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($agentId['max'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::AGENT_ID, $agentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentTechniqueTokenPeer::AGENT_ID, $agentId, $comparison);
    }

    /**
     * Filter the query on the token column
     *
     * Example usage:
     * <code>
     * $query->filterByToken('fooValue');   // WHERE token = 'fooValue'
     * $query->filterByToken('%fooValue%'); // WHERE token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $token The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function filterByToken($token = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($token)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $token)) {
                $token = str_replace('*', '%', $token);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentTechniqueTokenPeer::TOKEN, $token, $comparison);
    }

    /**
     * Filter the query on the create_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateAt('2011-03-14'); // WHERE create_at = '2011-03-14'
     * $query->filterByCreateAt('now'); // WHERE create_at = '2011-03-14'
     * $query->filterByCreateAt(array('max' => 'yesterday')); // WHERE create_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function filterByCreateAt($createAt = null, $comparison = null)
    {
        if (is_array($createAt)) {
            $useMinMax = false;
            if (isset($createAt['min'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::CREATE_AT, $createAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createAt['max'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::CREATE_AT, $createAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentTechniqueTokenPeer::CREATE_AT, $createAt, $comparison);
    }

    /**
     * Filter the query on the update_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdateAt('2011-03-14'); // WHERE update_at = '2011-03-14'
     * $query->filterByUpdateAt('now'); // WHERE update_at = '2011-03-14'
     * $query->filterByUpdateAt(array('max' => 'yesterday')); // WHERE update_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updateAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function filterByUpdateAt($updateAt = null, $comparison = null)
    {
        if (is_array($updateAt)) {
            $useMinMax = false;
            if (isset($updateAt['min'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::UPDATE_AT, $updateAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updateAt['max'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::UPDATE_AT, $updateAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentTechniqueTokenPeer::UPDATE_AT, $updateAt, $comparison);
    }

    /**
     * Filter the query on the date_expiration column
     *
     * Example usage:
     * <code>
     * $query->filterByDateExpiration('2011-03-14'); // WHERE date_expiration = '2011-03-14'
     * $query->filterByDateExpiration('now'); // WHERE date_expiration = '2011-03-14'
     * $query->filterByDateExpiration(array('max' => 'yesterday')); // WHERE date_expiration > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateExpiration The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function filterByDateExpiration($dateExpiration = null, $comparison = null)
    {
        if (is_array($dateExpiration)) {
            $useMinMax = false;
            if (isset($dateExpiration['min'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::DATE_EXPIRATION, $dateExpiration['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateExpiration['max'])) {
                $this->addUsingAlias(CommonAgentTechniqueTokenPeer::DATE_EXPIRATION, $dateExpiration['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentTechniqueTokenPeer::DATE_EXPIRATION, $dateExpiration, $comparison);
    }

    /**
     * Filter the query by a related CommonAgent object
     *
     * @param   CommonAgent|PropelObjectCollection $commonAgent The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentTechniqueTokenQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgent($commonAgent, $comparison = null)
    {
        if ($commonAgent instanceof CommonAgent) {
            return $this
                ->addUsingAlias(CommonAgentTechniqueTokenPeer::AGENT_ID, $commonAgent->getId(), $comparison);
        } elseif ($commonAgent instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonAgentTechniqueTokenPeer::AGENT_ID, $commonAgent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonAgent() only accepts arguments of type CommonAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function joinCommonAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgent');
        }

        return $this;
    }

    /**
     * Use the CommonAgent relation CommonAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgent', '\Application\Propel\Mpe\CommonAgentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonAgentTechniqueToken $commonAgentTechniqueToken Object to remove from the list of results
     *
     * @return CommonAgentTechniqueTokenQuery The current query, for fluid interface
     */
    public function prune($commonAgentTechniqueToken = null)
    {
        if ($commonAgentTechniqueToken) {
            $this->addUsingAlias(CommonAgentTechniqueTokenPeer::ID, $commonAgentTechniqueToken->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
