<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidatureQuery;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTGroupementEntreprisePeer;
use Application\Propel\Mpe\CommonTGroupementEntrepriseQuery;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery;
use Application\Propel\Mpe\CommonTTypeGroupementEntreprise;
use Application\Propel\Mpe\CommonTTypeGroupementEntrepriseQuery;

/**
 * Base class that represents a row from the 't_groupement_entreprise' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTGroupementEntreprise extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTGroupementEntreprisePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTGroupementEntreprisePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_groupement_entreprise field.
     * @var        int
     */
    protected $id_groupement_entreprise;

    /**
     * The value for the id_type_groupement field.
     * @var        int
     */
    protected $id_type_groupement;

    /**
     * The value for the id_offre field.
     * @var        int
     */
    protected $id_offre;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the id_candidature field.
     * @var        int
     */
    protected $id_candidature;

    /**
     * @var        CommonOffres
     */
    protected $aCommonOffres;

    /**
     * @var        CommonTTypeGroupementEntreprise
     */
    protected $aCommonTTypeGroupementEntreprise;

    /**
     * @var        CommonTCandidature
     */
    protected $aCommonTCandidature;

    /**
     * @var        PropelObjectCollection|CommonTMembreGroupementEntreprise[] Collection to store aggregation of CommonTMembreGroupementEntreprise objects.
     */
    protected $collCommonTMembreGroupementEntreprises;
    protected $collCommonTMembreGroupementEntreprisesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTMembreGroupementEntreprisesScheduledForDeletion = null;

    /**
     * Get the [id_groupement_entreprise] column value.
     *
     * @return int
     */
    public function getIdGroupementEntreprise()
    {

        return $this->id_groupement_entreprise;
    }

    /**
     * Get the [id_type_groupement] column value.
     *
     * @return int
     */
    public function getIdTypeGroupement()
    {

        return $this->id_type_groupement;
    }

    /**
     * Get the [id_offre] column value.
     *
     * @return int
     */
    public function getIdOffre()
    {

        return $this->id_offre;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [id_candidature] column value.
     *
     * @return int
     */
    public function getIdCandidature()
    {

        return $this->id_candidature;
    }

    /**
     * Set the value of [id_groupement_entreprise] column.
     *
     * @param int $v new value
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdGroupementEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_groupement_entreprise !== $v) {
            $this->id_groupement_entreprise = $v;
            $this->modifiedColumns[] = CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE;
        }


        return $this;
    } // setIdGroupementEntreprise()

    /**
     * Set the value of [id_type_groupement] column.
     *
     * @param int $v new value
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdTypeGroupement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_groupement !== $v) {
            $this->id_type_groupement = $v;
            $this->modifiedColumns[] = CommonTGroupementEntreprisePeer::ID_TYPE_GROUPEMENT;
        }

        if ($this->aCommonTTypeGroupementEntreprise !== null && $this->aCommonTTypeGroupementEntreprise->getIdTypeGroupement() !== $v) {
            $this->aCommonTTypeGroupementEntreprise = null;
        }


        return $this;
    } // setIdTypeGroupement()

    /**
     * Set the value of [id_offre] column.
     *
     * @param int $v new value
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_offre !== $v) {
            $this->id_offre = $v;
            $this->modifiedColumns[] = CommonTGroupementEntreprisePeer::ID_OFFRE;
        }

        if ($this->aCommonOffres !== null && $this->aCommonOffres->getId() !== $v) {
            $this->aCommonOffres = null;
        }


        return $this;
    } // setIdOffre()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTGroupementEntreprisePeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Set the value of [id_candidature] column.
     *
     * @param int $v new value
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdCandidature($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_candidature !== $v) {
            $this->id_candidature = $v;
            $this->modifiedColumns[] = CommonTGroupementEntreprisePeer::ID_CANDIDATURE;
        }

        if ($this->aCommonTCandidature !== null && $this->aCommonTCandidature->getId() !== $v) {
            $this->aCommonTCandidature = null;
        }


        return $this;
    } // setIdCandidature()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_groupement_entreprise = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_type_groupement = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_offre = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->date_creation = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->id_candidature = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 5; // 5 = CommonTGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTGroupementEntreprise object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTTypeGroupementEntreprise !== null && $this->id_type_groupement !== $this->aCommonTTypeGroupementEntreprise->getIdTypeGroupement()) {
            $this->aCommonTTypeGroupementEntreprise = null;
        }
        if ($this->aCommonOffres !== null && $this->id_offre !== $this->aCommonOffres->getId()) {
            $this->aCommonOffres = null;
        }
        if ($this->aCommonTCandidature !== null && $this->id_candidature !== $this->aCommonTCandidature->getId()) {
            $this->aCommonTCandidature = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTGroupementEntreprisePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonOffres = null;
            $this->aCommonTTypeGroupementEntreprise = null;
            $this->aCommonTCandidature = null;
            $this->collCommonTMembreGroupementEntreprises = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTGroupementEntrepriseQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTGroupementEntreprisePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonOffres !== null) {
                if ($this->aCommonOffres->isModified() || $this->aCommonOffres->isNew()) {
                    $affectedRows += $this->aCommonOffres->save($con);
                }
                $this->setCommonOffres($this->aCommonOffres);
            }

            if ($this->aCommonTTypeGroupementEntreprise !== null) {
                if ($this->aCommonTTypeGroupementEntreprise->isModified() || $this->aCommonTTypeGroupementEntreprise->isNew()) {
                    $affectedRows += $this->aCommonTTypeGroupementEntreprise->save($con);
                }
                $this->setCommonTTypeGroupementEntreprise($this->aCommonTTypeGroupementEntreprise);
            }

            if ($this->aCommonTCandidature !== null) {
                if ($this->aCommonTCandidature->isModified() || $this->aCommonTCandidature->isNew()) {
                    $affectedRows += $this->aCommonTCandidature->save($con);
                }
                $this->setCommonTCandidature($this->aCommonTCandidature);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTMembreGroupementEntreprisesScheduledForDeletion !== null) {
                if (!$this->commonTMembreGroupementEntreprisesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTMembreGroupementEntreprisesScheduledForDeletion as $commonTMembreGroupementEntreprise) {
                        // need to save related object because we set the relation to null
                        $commonTMembreGroupementEntreprise->save($con);
                    }
                    $this->commonTMembreGroupementEntreprisesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTMembreGroupementEntreprises !== null) {
                foreach ($this->collCommonTMembreGroupementEntreprises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE;
        if (null !== $this->id_groupement_entreprise) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_groupement_entreprise`';
        }
        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::ID_TYPE_GROUPEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_groupement`';
        }
        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::ID_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_offre`';
        }
        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::ID_CANDIDATURE)) {
            $modifiedColumns[':p' . $index++]  = '`id_candidature`';
        }

        $sql = sprintf(
            'INSERT INTO `t_groupement_entreprise` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_groupement_entreprise`':
                        $stmt->bindValue($identifier, $this->id_groupement_entreprise, PDO::PARAM_INT);
                        break;
                    case '`id_type_groupement`':
                        $stmt->bindValue($identifier, $this->id_type_groupement, PDO::PARAM_INT);
                        break;
                    case '`id_offre`':
                        $stmt->bindValue($identifier, $this->id_offre, PDO::PARAM_INT);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`id_candidature`':
                        $stmt->bindValue($identifier, $this->id_candidature, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdGroupementEntreprise($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonOffres !== null) {
                if (!$this->aCommonOffres->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonOffres->getValidationFailures());
                }
            }

            if ($this->aCommonTTypeGroupementEntreprise !== null) {
                if (!$this->aCommonTTypeGroupementEntreprise->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTTypeGroupementEntreprise->getValidationFailures());
                }
            }

            if ($this->aCommonTCandidature !== null) {
                if (!$this->aCommonTCandidature->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTCandidature->getValidationFailures());
                }
            }


            if (($retval = CommonTGroupementEntreprisePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTMembreGroupementEntreprises !== null) {
                    foreach ($this->collCommonTMembreGroupementEntreprises as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTGroupementEntreprisePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdGroupementEntreprise();
                break;
            case 1:
                return $this->getIdTypeGroupement();
                break;
            case 2:
                return $this->getIdOffre();
                break;
            case 3:
                return $this->getDateCreation();
                break;
            case 4:
                return $this->getIdCandidature();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTGroupementEntreprise'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTGroupementEntreprise'][$this->getPrimaryKey()] = true;
        $keys = CommonTGroupementEntreprisePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdGroupementEntreprise(),
            $keys[1] => $this->getIdTypeGroupement(),
            $keys[2] => $this->getIdOffre(),
            $keys[3] => $this->getDateCreation(),
            $keys[4] => $this->getIdCandidature(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonOffres) {
                $result['CommonOffres'] = $this->aCommonOffres->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTTypeGroupementEntreprise) {
                $result['CommonTTypeGroupementEntreprise'] = $this->aCommonTTypeGroupementEntreprise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTCandidature) {
                $result['CommonTCandidature'] = $this->aCommonTCandidature->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTMembreGroupementEntreprises) {
                $result['CommonTMembreGroupementEntreprises'] = $this->collCommonTMembreGroupementEntreprises->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTGroupementEntreprisePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdGroupementEntreprise($value);
                break;
            case 1:
                $this->setIdTypeGroupement($value);
                break;
            case 2:
                $this->setIdOffre($value);
                break;
            case 3:
                $this->setDateCreation($value);
                break;
            case 4:
                $this->setIdCandidature($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTGroupementEntreprisePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdGroupementEntreprise($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdTypeGroupement($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdOffre($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDateCreation($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdCandidature($arr[$keys[4]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTGroupementEntreprisePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE)) $criteria->add(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $this->id_groupement_entreprise);
        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::ID_TYPE_GROUPEMENT)) $criteria->add(CommonTGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $this->id_type_groupement);
        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::ID_OFFRE)) $criteria->add(CommonTGroupementEntreprisePeer::ID_OFFRE, $this->id_offre);
        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::DATE_CREATION)) $criteria->add(CommonTGroupementEntreprisePeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTGroupementEntreprisePeer::ID_CANDIDATURE)) $criteria->add(CommonTGroupementEntreprisePeer::ID_CANDIDATURE, $this->id_candidature);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTGroupementEntreprisePeer::DATABASE_NAME);
        $criteria->add(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $this->id_groupement_entreprise);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdGroupementEntreprise();
    }

    /**
     * Generic method to set the primary key (id_groupement_entreprise column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdGroupementEntreprise($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdGroupementEntreprise();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTGroupementEntreprise (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdTypeGroupement($this->getIdTypeGroupement());
        $copyObj->setIdOffre($this->getIdOffre());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setIdCandidature($this->getIdCandidature());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTMembreGroupementEntreprises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTMembreGroupementEntreprise($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdGroupementEntreprise(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTGroupementEntreprise Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTGroupementEntreprisePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTGroupementEntreprisePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonOffres object.
     *
     * @param   CommonOffres $v
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonOffres(CommonOffres $v = null)
    {
        if ($v === null) {
            $this->setIdOffre(NULL);
        } else {
            $this->setIdOffre($v->getId());
        }

        $this->aCommonOffres = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonOffres object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTGroupementEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonOffres object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonOffres The associated CommonOffres object.
     * @throws PropelException
     */
    public function getCommonOffres(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonOffres === null && ($this->id_offre !== null) && $doQuery) {
            $this->aCommonOffres = CommonOffresQuery::create()
                ->filterByCommonTGroupementEntreprise($this) // here
                ->findOne($con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonOffres->addCommonTGroupementEntreprises($this);
             */
        }

        return $this->aCommonOffres;
    }

    /**
     * Declares an association between this object and a CommonTTypeGroupementEntreprise object.
     *
     * @param   CommonTTypeGroupementEntreprise $v
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTTypeGroupementEntreprise(CommonTTypeGroupementEntreprise $v = null)
    {
        if ($v === null) {
            $this->setIdTypeGroupement(NULL);
        } else {
            $this->setIdTypeGroupement($v->getIdTypeGroupement());
        }

        $this->aCommonTTypeGroupementEntreprise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTTypeGroupementEntreprise object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTGroupementEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTTypeGroupementEntreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTTypeGroupementEntreprise The associated CommonTTypeGroupementEntreprise object.
     * @throws PropelException
     */
    public function getCommonTTypeGroupementEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTTypeGroupementEntreprise === null && ($this->id_type_groupement !== null) && $doQuery) {
            $this->aCommonTTypeGroupementEntreprise = CommonTTypeGroupementEntrepriseQuery::create()->findPk($this->id_type_groupement, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTTypeGroupementEntreprise->addCommonTGroupementEntreprises($this);
             */
        }

        return $this->aCommonTTypeGroupementEntreprise;
    }

    /**
     * Declares an association between this object and a CommonTCandidature object.
     *
     * @param   CommonTCandidature $v
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTCandidature(CommonTCandidature $v = null)
    {
        if ($v === null) {
            $this->setIdCandidature(NULL);
        } else {
            $this->setIdCandidature($v->getId());
        }

        $this->aCommonTCandidature = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTCandidature object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTGroupementEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTCandidature object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTCandidature The associated CommonTCandidature object.
     * @throws PropelException
     */
    public function getCommonTCandidature(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTCandidature === null && ($this->id_candidature !== null) && $doQuery) {
            $this->aCommonTCandidature = CommonTCandidatureQuery::create()->findPk($this->id_candidature, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTCandidature->addCommonTGroupementEntreprises($this);
             */
        }

        return $this->aCommonTCandidature;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTMembreGroupementEntreprise' == $relationName) {
            $this->initCommonTMembreGroupementEntreprises();
        }
    }

    /**
     * Clears out the collCommonTMembreGroupementEntreprises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     * @see        addCommonTMembreGroupementEntreprises()
     */
    public function clearCommonTMembreGroupementEntreprises()
    {
        $this->collCommonTMembreGroupementEntreprises = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTMembreGroupementEntreprisesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTMembreGroupementEntreprises collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTMembreGroupementEntreprises($v = true)
    {
        $this->collCommonTMembreGroupementEntreprisesPartial = $v;
    }

    /**
     * Initializes the collCommonTMembreGroupementEntreprises collection.
     *
     * By default this just sets the collCommonTMembreGroupementEntreprises collection to an empty array (like clearcollCommonTMembreGroupementEntreprises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTMembreGroupementEntreprises($overrideExisting = true)
    {
        if (null !== $this->collCommonTMembreGroupementEntreprises && !$overrideExisting) {
            return;
        }
        $this->collCommonTMembreGroupementEntreprises = new PropelObjectCollection();
        $this->collCommonTMembreGroupementEntreprises->setModel('CommonTMembreGroupementEntreprise');
    }

    /**
     * Gets an array of CommonTMembreGroupementEntreprise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTGroupementEntreprise is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     * @throws PropelException
     */
    public function getCommonTMembreGroupementEntreprises($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTMembreGroupementEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTMembreGroupementEntreprises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTMembreGroupementEntreprises) {
                // return empty collection
                $this->initCommonTMembreGroupementEntreprises();
            } else {
                $collCommonTMembreGroupementEntreprises = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria)
                    ->filterByCommonTGroupementEntreprise($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTMembreGroupementEntreprisesPartial && count($collCommonTMembreGroupementEntreprises)) {
                      $this->initCommonTMembreGroupementEntreprises(false);

                      foreach ($collCommonTMembreGroupementEntreprises as $obj) {
                        if (false == $this->collCommonTMembreGroupementEntreprises->contains($obj)) {
                          $this->collCommonTMembreGroupementEntreprises->append($obj);
                        }
                      }

                      $this->collCommonTMembreGroupementEntreprisesPartial = true;
                    }

                    $collCommonTMembreGroupementEntreprises->getInternalIterator()->rewind();

                    return $collCommonTMembreGroupementEntreprises;
                }

                if ($partial && $this->collCommonTMembreGroupementEntreprises) {
                    foreach ($this->collCommonTMembreGroupementEntreprises as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTMembreGroupementEntreprises[] = $obj;
                        }
                    }
                }

                $this->collCommonTMembreGroupementEntreprises = $collCommonTMembreGroupementEntreprises;
                $this->collCommonTMembreGroupementEntreprisesPartial = false;
            }
        }

        return $this->collCommonTMembreGroupementEntreprises;
    }

    /**
     * Sets a collection of CommonTMembreGroupementEntreprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTMembreGroupementEntreprises A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     */
    public function setCommonTMembreGroupementEntreprises(PropelCollection $commonTMembreGroupementEntreprises, PropelPDO $con = null)
    {
        $commonTMembreGroupementEntreprisesToDelete = $this->getCommonTMembreGroupementEntreprises(new Criteria(), $con)->diff($commonTMembreGroupementEntreprises);


        $this->commonTMembreGroupementEntreprisesScheduledForDeletion = $commonTMembreGroupementEntreprisesToDelete;

        foreach ($commonTMembreGroupementEntreprisesToDelete as $commonTMembreGroupementEntrepriseRemoved) {
            $commonTMembreGroupementEntrepriseRemoved->setCommonTGroupementEntreprise(null);
        }

        $this->collCommonTMembreGroupementEntreprises = null;
        foreach ($commonTMembreGroupementEntreprises as $commonTMembreGroupementEntreprise) {
            $this->addCommonTMembreGroupementEntreprise($commonTMembreGroupementEntreprise);
        }

        $this->collCommonTMembreGroupementEntreprises = $commonTMembreGroupementEntreprises;
        $this->collCommonTMembreGroupementEntreprisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTMembreGroupementEntreprise objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTMembreGroupementEntreprise objects.
     * @throws PropelException
     */
    public function countCommonTMembreGroupementEntreprises(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTMembreGroupementEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTMembreGroupementEntreprises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTMembreGroupementEntreprises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTMembreGroupementEntreprises());
            }
            $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTGroupementEntreprise($this)
                ->count($con);
        }

        return count($this->collCommonTMembreGroupementEntreprises);
    }

    /**
     * Method called to associate a CommonTMembreGroupementEntreprise object to this object
     * through the CommonTMembreGroupementEntreprise foreign key attribute.
     *
     * @param   CommonTMembreGroupementEntreprise $l CommonTMembreGroupementEntreprise
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     */
    public function addCommonTMembreGroupementEntreprise(CommonTMembreGroupementEntreprise $l)
    {
        if ($this->collCommonTMembreGroupementEntreprises === null) {
            $this->initCommonTMembreGroupementEntreprises();
            $this->collCommonTMembreGroupementEntreprisesPartial = true;
        }
        if (!in_array($l, $this->collCommonTMembreGroupementEntreprises->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTMembreGroupementEntreprise($l);
        }

        return $this;
    }

    /**
     * @param	CommonTMembreGroupementEntreprise $commonTMembreGroupementEntreprise The commonTMembreGroupementEntreprise object to add.
     */
    protected function doAddCommonTMembreGroupementEntreprise($commonTMembreGroupementEntreprise)
    {
        $this->collCommonTMembreGroupementEntreprises[]= $commonTMembreGroupementEntreprise;
        $commonTMembreGroupementEntreprise->setCommonTGroupementEntreprise($this);
    }

    /**
     * @param	CommonTMembreGroupementEntreprise $commonTMembreGroupementEntreprise The commonTMembreGroupementEntreprise object to remove.
     * @return CommonTGroupementEntreprise The current object (for fluent API support)
     */
    public function removeCommonTMembreGroupementEntreprise($commonTMembreGroupementEntreprise)
    {
        if ($this->getCommonTMembreGroupementEntreprises()->contains($commonTMembreGroupementEntreprise)) {
            $this->collCommonTMembreGroupementEntreprises->remove($this->collCommonTMembreGroupementEntreprises->search($commonTMembreGroupementEntreprise));
            if (null === $this->commonTMembreGroupementEntreprisesScheduledForDeletion) {
                $this->commonTMembreGroupementEntreprisesScheduledForDeletion = clone $this->collCommonTMembreGroupementEntreprises;
                $this->commonTMembreGroupementEntreprisesScheduledForDeletion->clear();
            }
            $this->commonTMembreGroupementEntreprisesScheduledForDeletion[]= $commonTMembreGroupementEntreprise;
            $commonTMembreGroupementEntreprise->setCommonTGroupementEntreprise(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTGroupementEntreprise is new, it will return
     * an empty collection; or if this CommonTGroupementEntreprise has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTGroupementEntreprise.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprises($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTGroupementEntreprise is new, it will return
     * an empty collection; or if this CommonTGroupementEntreprise has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTGroupementEntreprise.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesJoinCommonTEtablissement($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTEtablissement', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprises($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTGroupementEntreprise is new, it will return
     * an empty collection; or if this CommonTGroupementEntreprise has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTGroupementEntreprise.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesJoinCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTMembreGroupementEntrepriseRelatedByIdMembreParent', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprises($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTGroupementEntreprise is new, it will return
     * an empty collection; or if this CommonTGroupementEntreprise has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTGroupementEntreprise.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesJoinCommonTRoleJuridique($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTRoleJuridique', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprises($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_groupement_entreprise = null;
        $this->id_type_groupement = null;
        $this->id_offre = null;
        $this->date_creation = null;
        $this->id_candidature = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTMembreGroupementEntreprises) {
                foreach ($this->collCommonTMembreGroupementEntreprises as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonOffres instanceof Persistent) {
              $this->aCommonOffres->clearAllReferences($deep);
            }
            if ($this->aCommonTTypeGroupementEntreprise instanceof Persistent) {
              $this->aCommonTTypeGroupementEntreprise->clearAllReferences($deep);
            }
            if ($this->aCommonTCandidature instanceof Persistent) {
              $this->aCommonTCandidature->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTMembreGroupementEntreprises instanceof PropelCollection) {
            $this->collCommonTMembreGroupementEntreprises->clearIterator();
        }
        $this->collCommonTMembreGroupementEntreprises = null;
        $this->aCommonOffres = null;
        $this->aCommonTTypeGroupementEntreprise = null;
        $this->aCommonTCandidature = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTGroupementEntreprisePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
