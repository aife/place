<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationClausesN1;
use Application\Propel\Mpe\CommonConsultationClausesN1Peer;
use Application\Propel\Mpe\CommonConsultationClausesN1Query;
use Application\Propel\Mpe\CommonConsultationClausesN2;
use Application\Propel\Mpe\CommonConsultationClausesN2Query;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN1;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN1Query;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;

/**
 * Base class that represents a row from the 'consultation_clauses_n1' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationClausesN1 extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonConsultationClausesN1Peer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonConsultationClausesN1Peer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the consultation_id field.
     * @var        int
     */
    protected $consultation_id;

    /**
     * The value for the lot_id field.
     * @var        int
     */
    protected $lot_id;

    /**
     * The value for the referentiel_clause_n1_id field.
     * @var        int
     */
    protected $referentiel_clause_n1_id;

    /**
     * The value for the contrat_id field.
     * @var        int
     */
    protected $contrat_id;

    /**
     * @var        CommonTContratTitulaire
     */
    protected $aCommonTContratTitulaire;

    /**
     * @var        CommonReferentielConsultationClausesN1
     */
    protected $aCommonReferentielConsultationClausesN1;

    /**
     * @var        CommonConsultation
     */
    protected $aCommonConsultation;

    /**
     * @var        CommonCategorieLot
     */
    protected $aCommonCategorieLot;

    /**
     * @var        PropelObjectCollection|CommonConsultationClausesN2[] Collection to store aggregation of CommonConsultationClausesN2 objects.
     */
    protected $collCommonConsultationClausesN2s;
    protected $collCommonConsultationClausesN2sPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationClausesN2sScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [consultation_id] column value.
     *
     * @return int
     */
    public function getConsultationId()
    {

        return $this->consultation_id;
    }

    /**
     * Get the [lot_id] column value.
     *
     * @return int
     */
    public function getLotId()
    {

        return $this->lot_id;
    }

    /**
     * Get the [referentiel_clause_n1_id] column value.
     *
     * @return int
     */
    public function getReferentielClauseN1Id()
    {

        return $this->referentiel_clause_n1_id;
    }

    /**
     * Get the [contrat_id] column value.
     *
     * @return int
     */
    public function getContratId()
    {

        return $this->contrat_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonConsultationClausesN1Peer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [consultation_id] column.
     *
     * @param int $v new value
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     */
    public function setConsultationId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_id !== $v) {
            $this->consultation_id = $v;
            $this->modifiedColumns[] = CommonConsultationClausesN1Peer::CONSULTATION_ID;
        }

        if ($this->aCommonConsultation !== null && $this->aCommonConsultation->getId() !== $v) {
            $this->aCommonConsultation = null;
        }


        return $this;
    } // setConsultationId()

    /**
     * Set the value of [lot_id] column.
     *
     * @param int $v new value
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     */
    public function setLotId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->lot_id !== $v) {
            $this->lot_id = $v;
            $this->modifiedColumns[] = CommonConsultationClausesN1Peer::LOT_ID;
        }

        if ($this->aCommonCategorieLot !== null && $this->aCommonCategorieLot->getId() !== $v) {
            $this->aCommonCategorieLot = null;
        }


        return $this;
    } // setLotId()

    /**
     * Set the value of [referentiel_clause_n1_id] column.
     *
     * @param int $v new value
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     */
    public function setReferentielClauseN1Id($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->referentiel_clause_n1_id !== $v) {
            $this->referentiel_clause_n1_id = $v;
            $this->modifiedColumns[] = CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID;
        }

        if ($this->aCommonReferentielConsultationClausesN1 !== null && $this->aCommonReferentielConsultationClausesN1->getId() !== $v) {
            $this->aCommonReferentielConsultationClausesN1 = null;
        }


        return $this;
    } // setReferentielClauseN1Id()

    /**
     * Set the value of [contrat_id] column.
     *
     * @param int $v new value
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     */
    public function setContratId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->contrat_id !== $v) {
            $this->contrat_id = $v;
            $this->modifiedColumns[] = CommonConsultationClausesN1Peer::CONTRAT_ID;
        }

        if ($this->aCommonTContratTitulaire !== null && $this->aCommonTContratTitulaire->getIdContratTitulaire() !== $v) {
            $this->aCommonTContratTitulaire = null;
        }


        return $this;
    } // setContratId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->consultation_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->lot_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->referentiel_clause_n1_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->contrat_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 5; // 5 = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonConsultationClausesN1 object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonConsultation !== null && $this->consultation_id !== $this->aCommonConsultation->getId()) {
            $this->aCommonConsultation = null;
        }
        if ($this->aCommonCategorieLot !== null && $this->lot_id !== $this->aCommonCategorieLot->getId()) {
            $this->aCommonCategorieLot = null;
        }
        if ($this->aCommonReferentielConsultationClausesN1 !== null && $this->referentiel_clause_n1_id !== $this->aCommonReferentielConsultationClausesN1->getId()) {
            $this->aCommonReferentielConsultationClausesN1 = null;
        }
        if ($this->aCommonTContratTitulaire !== null && $this->contrat_id !== $this->aCommonTContratTitulaire->getIdContratTitulaire()) {
            $this->aCommonTContratTitulaire = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonConsultationClausesN1Peer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTContratTitulaire = null;
            $this->aCommonReferentielConsultationClausesN1 = null;
            $this->aCommonConsultation = null;
            $this->aCommonCategorieLot = null;
            $this->collCommonConsultationClausesN2s = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonConsultationClausesN1Query::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonConsultationClausesN1Peer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTContratTitulaire !== null) {
                if ($this->aCommonTContratTitulaire->isModified() || $this->aCommonTContratTitulaire->isNew()) {
                    $affectedRows += $this->aCommonTContratTitulaire->save($con);
                }
                $this->setCommonTContratTitulaire($this->aCommonTContratTitulaire);
            }

            if ($this->aCommonReferentielConsultationClausesN1 !== null) {
                if ($this->aCommonReferentielConsultationClausesN1->isModified() || $this->aCommonReferentielConsultationClausesN1->isNew()) {
                    $affectedRows += $this->aCommonReferentielConsultationClausesN1->save($con);
                }
                $this->setCommonReferentielConsultationClausesN1($this->aCommonReferentielConsultationClausesN1);
            }

            if ($this->aCommonConsultation !== null) {
                if ($this->aCommonConsultation->isModified() || $this->aCommonConsultation->isNew()) {
                    $affectedRows += $this->aCommonConsultation->save($con);
                }
                $this->setCommonConsultation($this->aCommonConsultation);
            }

            if ($this->aCommonCategorieLot !== null) {
                if ($this->aCommonCategorieLot->isModified() || $this->aCommonCategorieLot->isNew()) {
                    $affectedRows += $this->aCommonCategorieLot->save($con);
                }
                $this->setCommonCategorieLot($this->aCommonCategorieLot);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonConsultationClausesN2sScheduledForDeletion !== null) {
                if (!$this->commonConsultationClausesN2sScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationClausesN2sScheduledForDeletion as $commonConsultationClausesN2) {
                        // need to save related object because we set the relation to null
                        $commonConsultationClausesN2->save($con);
                    }
                    $this->commonConsultationClausesN2sScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultationClausesN2s !== null) {
                foreach ($this->collCommonConsultationClausesN2s as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonConsultationClausesN1Peer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonConsultationClausesN1Peer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonConsultationClausesN1Peer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonConsultationClausesN1Peer::CONSULTATION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_id`';
        }
        if ($this->isColumnModified(CommonConsultationClausesN1Peer::LOT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`lot_id`';
        }
        if ($this->isColumnModified(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID)) {
            $modifiedColumns[':p' . $index++]  = '`referentiel_clause_n1_id`';
        }
        if ($this->isColumnModified(CommonConsultationClausesN1Peer::CONTRAT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`contrat_id`';
        }

        $sql = sprintf(
            'INSERT INTO `consultation_clauses_n1` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`consultation_id`':
                        $stmt->bindValue($identifier, $this->consultation_id, PDO::PARAM_INT);
                        break;
                    case '`lot_id`':
                        $stmt->bindValue($identifier, $this->lot_id, PDO::PARAM_INT);
                        break;
                    case '`referentiel_clause_n1_id`':
                        $stmt->bindValue($identifier, $this->referentiel_clause_n1_id, PDO::PARAM_INT);
                        break;
                    case '`contrat_id`':
                        $stmt->bindValue($identifier, $this->contrat_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTContratTitulaire !== null) {
                if (!$this->aCommonTContratTitulaire->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTContratTitulaire->getValidationFailures());
                }
            }

            if ($this->aCommonReferentielConsultationClausesN1 !== null) {
                if (!$this->aCommonReferentielConsultationClausesN1->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonReferentielConsultationClausesN1->getValidationFailures());
                }
            }

            if ($this->aCommonConsultation !== null) {
                if (!$this->aCommonConsultation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonConsultation->getValidationFailures());
                }
            }

            if ($this->aCommonCategorieLot !== null) {
                if (!$this->aCommonCategorieLot->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonCategorieLot->getValidationFailures());
                }
            }


            if (($retval = CommonConsultationClausesN1Peer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonConsultationClausesN2s !== null) {
                    foreach ($this->collCommonConsultationClausesN2s as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonConsultationClausesN1Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getConsultationId();
                break;
            case 2:
                return $this->getLotId();
                break;
            case 3:
                return $this->getReferentielClauseN1Id();
                break;
            case 4:
                return $this->getContratId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonConsultationClausesN1'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonConsultationClausesN1'][$this->getPrimaryKey()] = true;
        $keys = CommonConsultationClausesN1Peer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getConsultationId(),
            $keys[2] => $this->getLotId(),
            $keys[3] => $this->getReferentielClauseN1Id(),
            $keys[4] => $this->getContratId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTContratTitulaire) {
                $result['CommonTContratTitulaire'] = $this->aCommonTContratTitulaire->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonReferentielConsultationClausesN1) {
                $result['CommonReferentielConsultationClausesN1'] = $this->aCommonReferentielConsultationClausesN1->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonConsultation) {
                $result['CommonConsultation'] = $this->aCommonConsultation->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonCategorieLot) {
                $result['CommonCategorieLot'] = $this->aCommonCategorieLot->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonConsultationClausesN2s) {
                $result['CommonConsultationClausesN2s'] = $this->collCommonConsultationClausesN2s->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonConsultationClausesN1Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setConsultationId($value);
                break;
            case 2:
                $this->setLotId($value);
                break;
            case 3:
                $this->setReferentielClauseN1Id($value);
                break;
            case 4:
                $this->setContratId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonConsultationClausesN1Peer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setConsultationId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setLotId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setReferentielClauseN1Id($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setContratId($arr[$keys[4]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($this->isColumnModified(CommonConsultationClausesN1Peer::ID)) $criteria->add(CommonConsultationClausesN1Peer::ID, $this->id);
        if ($this->isColumnModified(CommonConsultationClausesN1Peer::CONSULTATION_ID)) $criteria->add(CommonConsultationClausesN1Peer::CONSULTATION_ID, $this->consultation_id);
        if ($this->isColumnModified(CommonConsultationClausesN1Peer::LOT_ID)) $criteria->add(CommonConsultationClausesN1Peer::LOT_ID, $this->lot_id);
        if ($this->isColumnModified(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID)) $criteria->add(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, $this->referentiel_clause_n1_id);
        if ($this->isColumnModified(CommonConsultationClausesN1Peer::CONTRAT_ID)) $criteria->add(CommonConsultationClausesN1Peer::CONTRAT_ID, $this->contrat_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonConsultationClausesN1Peer::DATABASE_NAME);
        $criteria->add(CommonConsultationClausesN1Peer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonConsultationClausesN1 (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setConsultationId($this->getConsultationId());
        $copyObj->setLotId($this->getLotId());
        $copyObj->setReferentielClauseN1Id($this->getReferentielClauseN1Id());
        $copyObj->setContratId($this->getContratId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonConsultationClausesN2s() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultationClausesN2($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonConsultationClausesN1 Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonConsultationClausesN1Peer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonConsultationClausesN1Peer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonTContratTitulaire object.
     *
     * @param   CommonTContratTitulaire $v
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTContratTitulaire(CommonTContratTitulaire $v = null)
    {
        if ($v === null) {
            $this->setContratId(NULL);
        } else {
            $this->setContratId($v->getIdContratTitulaire());
        }

        $this->aCommonTContratTitulaire = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTContratTitulaire object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonConsultationClausesN1($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTContratTitulaire object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTContratTitulaire The associated CommonTContratTitulaire object.
     * @throws PropelException
     */
    public function getCommonTContratTitulaire(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTContratTitulaire === null && ($this->contrat_id !== null) && $doQuery) {
            $this->aCommonTContratTitulaire = CommonTContratTitulaireQuery::create()->findPk($this->contrat_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTContratTitulaire->addCommonConsultationClausesN1s($this);
             */
        }

        return $this->aCommonTContratTitulaire;
    }

    /**
     * Declares an association between this object and a CommonReferentielConsultationClausesN1 object.
     *
     * @param   CommonReferentielConsultationClausesN1 $v
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonReferentielConsultationClausesN1(CommonReferentielConsultationClausesN1 $v = null)
    {
        if ($v === null) {
            $this->setReferentielClauseN1Id(NULL);
        } else {
            $this->setReferentielClauseN1Id($v->getId());
        }

        $this->aCommonReferentielConsultationClausesN1 = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonReferentielConsultationClausesN1 object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonConsultationClausesN1($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonReferentielConsultationClausesN1 object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonReferentielConsultationClausesN1 The associated CommonReferentielConsultationClausesN1 object.
     * @throws PropelException
     */
    public function getCommonReferentielConsultationClausesN1(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonReferentielConsultationClausesN1 === null && ($this->referentiel_clause_n1_id !== null) && $doQuery) {
            $this->aCommonReferentielConsultationClausesN1 = CommonReferentielConsultationClausesN1Query::create()->findPk($this->referentiel_clause_n1_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonReferentielConsultationClausesN1->addCommonConsultationClausesN1s($this);
             */
        }

        return $this->aCommonReferentielConsultationClausesN1;
    }

    /**
     * Declares an association between this object and a CommonConsultation object.
     *
     * @param   CommonConsultation $v
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonConsultation(CommonConsultation $v = null)
    {
        if ($v === null) {
            $this->setConsultationId(NULL);
        } else {
            $this->setConsultationId($v->getId());
        }

        $this->aCommonConsultation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonConsultation object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonConsultationClausesN1($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonConsultation object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonConsultation The associated CommonConsultation object.
     * @throws PropelException
     */
    public function getCommonConsultation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonConsultation === null && ($this->consultation_id !== null) && $doQuery) {
            $this->aCommonConsultation = CommonConsultationQuery::create()->findPk($this->consultation_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonConsultation->addCommonConsultationClausesN1s($this);
             */
        }

        return $this->aCommonConsultation;
    }

    /**
     * Declares an association between this object and a CommonCategorieLot object.
     *
     * @param   CommonCategorieLot $v
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonCategorieLot(CommonCategorieLot $v = null)
    {
        if ($v === null) {
            $this->setLotId(NULL);
        } else {
            $this->setLotId($v->getId());
        }

        $this->aCommonCategorieLot = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonCategorieLot object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonConsultationClausesN1($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonCategorieLot object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonCategorieLot The associated CommonCategorieLot object.
     * @throws PropelException
     */
    public function getCommonCategorieLot(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonCategorieLot === null && ($this->lot_id !== null) && $doQuery) {
            $this->aCommonCategorieLot = CommonCategorieLotQuery::create()->findPk($this->lot_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonCategorieLot->addCommonConsultationClausesN1s($this);
             */
        }

        return $this->aCommonCategorieLot;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonConsultationClausesN2' == $relationName) {
            $this->initCommonConsultationClausesN2s();
        }
    }

    /**
     * Clears out the collCommonConsultationClausesN2s collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     * @see        addCommonConsultationClausesN2s()
     */
    public function clearCommonConsultationClausesN2s()
    {
        $this->collCommonConsultationClausesN2s = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationClausesN2sPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultationClausesN2s collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultationClausesN2s($v = true)
    {
        $this->collCommonConsultationClausesN2sPartial = $v;
    }

    /**
     * Initializes the collCommonConsultationClausesN2s collection.
     *
     * By default this just sets the collCommonConsultationClausesN2s collection to an empty array (like clearcollCommonConsultationClausesN2s());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultationClausesN2s($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultationClausesN2s && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultationClausesN2s = new PropelObjectCollection();
        $this->collCommonConsultationClausesN2s->setModel('CommonConsultationClausesN2');
    }

    /**
     * Gets an array of CommonConsultationClausesN2 objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonConsultationClausesN1 is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultationClausesN2[] List of CommonConsultationClausesN2 objects
     * @throws PropelException
     */
    public function getCommonConsultationClausesN2s($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationClausesN2sPartial && !$this->isNew();
        if (null === $this->collCommonConsultationClausesN2s || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationClausesN2s) {
                // return empty collection
                $this->initCommonConsultationClausesN2s();
            } else {
                $collCommonConsultationClausesN2s = CommonConsultationClausesN2Query::create(null, $criteria)
                    ->filterByCommonConsultationClausesN1($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationClausesN2sPartial && count($collCommonConsultationClausesN2s)) {
                      $this->initCommonConsultationClausesN2s(false);

                      foreach ($collCommonConsultationClausesN2s as $obj) {
                        if (false == $this->collCommonConsultationClausesN2s->contains($obj)) {
                          $this->collCommonConsultationClausesN2s->append($obj);
                        }
                      }

                      $this->collCommonConsultationClausesN2sPartial = true;
                    }

                    $collCommonConsultationClausesN2s->getInternalIterator()->rewind();

                    return $collCommonConsultationClausesN2s;
                }

                if ($partial && $this->collCommonConsultationClausesN2s) {
                    foreach ($this->collCommonConsultationClausesN2s as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultationClausesN2s[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultationClausesN2s = $collCommonConsultationClausesN2s;
                $this->collCommonConsultationClausesN2sPartial = false;
            }
        }

        return $this->collCommonConsultationClausesN2s;
    }

    /**
     * Sets a collection of CommonConsultationClausesN2 objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultationClausesN2s A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     */
    public function setCommonConsultationClausesN2s(PropelCollection $commonConsultationClausesN2s, PropelPDO $con = null)
    {
        $commonConsultationClausesN2sToDelete = $this->getCommonConsultationClausesN2s(new Criteria(), $con)->diff($commonConsultationClausesN2s);


        $this->commonConsultationClausesN2sScheduledForDeletion = $commonConsultationClausesN2sToDelete;

        foreach ($commonConsultationClausesN2sToDelete as $commonConsultationClausesN2Removed) {
            $commonConsultationClausesN2Removed->setCommonConsultationClausesN1(null);
        }

        $this->collCommonConsultationClausesN2s = null;
        foreach ($commonConsultationClausesN2s as $commonConsultationClausesN2) {
            $this->addCommonConsultationClausesN2($commonConsultationClausesN2);
        }

        $this->collCommonConsultationClausesN2s = $commonConsultationClausesN2s;
        $this->collCommonConsultationClausesN2sPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultationClausesN2 objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultationClausesN2 objects.
     * @throws PropelException
     */
    public function countCommonConsultationClausesN2s(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationClausesN2sPartial && !$this->isNew();
        if (null === $this->collCommonConsultationClausesN2s || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationClausesN2s) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultationClausesN2s());
            }
            $query = CommonConsultationClausesN2Query::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonConsultationClausesN1($this)
                ->count($con);
        }

        return count($this->collCommonConsultationClausesN2s);
    }

    /**
     * Method called to associate a CommonConsultationClausesN2 object to this object
     * through the CommonConsultationClausesN2 foreign key attribute.
     *
     * @param   CommonConsultationClausesN2 $l CommonConsultationClausesN2
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     */
    public function addCommonConsultationClausesN2(CommonConsultationClausesN2 $l)
    {
        if ($this->collCommonConsultationClausesN2s === null) {
            $this->initCommonConsultationClausesN2s();
            $this->collCommonConsultationClausesN2sPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultationClausesN2s->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultationClausesN2($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultationClausesN2 $commonConsultationClausesN2 The commonConsultationClausesN2 object to add.
     */
    protected function doAddCommonConsultationClausesN2($commonConsultationClausesN2)
    {
        $this->collCommonConsultationClausesN2s[]= $commonConsultationClausesN2;
        $commonConsultationClausesN2->setCommonConsultationClausesN1($this);
    }

    /**
     * @param	CommonConsultationClausesN2 $commonConsultationClausesN2 The commonConsultationClausesN2 object to remove.
     * @return CommonConsultationClausesN1 The current object (for fluent API support)
     */
    public function removeCommonConsultationClausesN2($commonConsultationClausesN2)
    {
        if ($this->getCommonConsultationClausesN2s()->contains($commonConsultationClausesN2)) {
            $this->collCommonConsultationClausesN2s->remove($this->collCommonConsultationClausesN2s->search($commonConsultationClausesN2));
            if (null === $this->commonConsultationClausesN2sScheduledForDeletion) {
                $this->commonConsultationClausesN2sScheduledForDeletion = clone $this->collCommonConsultationClausesN2s;
                $this->commonConsultationClausesN2sScheduledForDeletion->clear();
            }
            $this->commonConsultationClausesN2sScheduledForDeletion[]= clone $commonConsultationClausesN2;
            $commonConsultationClausesN2->setCommonConsultationClausesN1(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonConsultationClausesN1 is new, it will return
     * an empty collection; or if this CommonConsultationClausesN1 has previously
     * been saved, it will retrieve related CommonConsultationClausesN2s from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonConsultationClausesN1.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultationClausesN2[] List of CommonConsultationClausesN2 objects
     */
    public function getCommonConsultationClausesN2sJoinCommonReferentielConsultationClausesN2($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationClausesN2Query::create(null, $criteria);
        $query->joinWith('CommonReferentielConsultationClausesN2', $join_behavior);

        return $this->getCommonConsultationClausesN2s($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->consultation_id = null;
        $this->lot_id = null;
        $this->referentiel_clause_n1_id = null;
        $this->contrat_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonConsultationClausesN2s) {
                foreach ($this->collCommonConsultationClausesN2s as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonTContratTitulaire instanceof Persistent) {
              $this->aCommonTContratTitulaire->clearAllReferences($deep);
            }
            if ($this->aCommonReferentielConsultationClausesN1 instanceof Persistent) {
              $this->aCommonReferentielConsultationClausesN1->clearAllReferences($deep);
            }
            if ($this->aCommonConsultation instanceof Persistent) {
              $this->aCommonConsultation->clearAllReferences($deep);
            }
            if ($this->aCommonCategorieLot instanceof Persistent) {
              $this->aCommonCategorieLot->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonConsultationClausesN2s instanceof PropelCollection) {
            $this->collCommonConsultationClausesN2s->clearIterator();
        }
        $this->collCommonConsultationClausesN2s = null;
        $this->aCommonTContratTitulaire = null;
        $this->aCommonReferentielConsultationClausesN1 = null;
        $this->aCommonConsultation = null;
        $this->aCommonCategorieLot = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonConsultationClausesN1Peer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
