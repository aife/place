<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\CommonTGroupementEntreprisePeer;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprisePeer;
use Application\Propel\Mpe\CommonTRoleJuridiquePeer;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Propel\Mpe\Map\CommonTMembreGroupementEntrepriseTableMap;

/**
 * Base static class for performing query and update operations on the 't_membre_groupement_entreprise' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTMembreGroupementEntreprisePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_membre_groupement_entreprise';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTMembreGroupementEntreprise';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTMembreGroupementEntrepriseTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 8;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 8;

    /** the column name for the id_membre_groupement_entreprise field */
    const ID_MEMBRE_GROUPEMENT_ENTREPRISE = 't_membre_groupement_entreprise.id_membre_groupement_entreprise';

    /** the column name for the id_role_juridique field */
    const ID_ROLE_JURIDIQUE = 't_membre_groupement_entreprise.id_role_juridique';

    /** the column name for the id_groupement_entreprise field */
    const ID_GROUPEMENT_ENTREPRISE = 't_membre_groupement_entreprise.id_groupement_entreprise';

    /** the column name for the id_entreprise field */
    const ID_ENTREPRISE = 't_membre_groupement_entreprise.id_entreprise';

    /** the column name for the id_etablissement field */
    const ID_ETABLISSEMENT = 't_membre_groupement_entreprise.id_etablissement';

    /** the column name for the id_membre_parent field */
    const ID_MEMBRE_PARENT = 't_membre_groupement_entreprise.id_membre_parent';

    /** the column name for the numeroSN field */
    const NUMEROSN = 't_membre_groupement_entreprise.numeroSN';

    /** the column name for the email field */
    const EMAIL = 't_membre_groupement_entreprise.email';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTMembreGroupementEntreprise objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTMembreGroupementEntreprise[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTMembreGroupementEntreprisePeer::$fieldNames[CommonTMembreGroupementEntreprisePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdMembreGroupementEntreprise', 'IdRoleJuridique', 'IdGroupementEntreprise', 'IdEntreprise', 'IdEtablissement', 'IdMembreParent', 'Numerosn', 'Email', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idMembreGroupementEntreprise', 'idRoleJuridique', 'idGroupementEntreprise', 'idEntreprise', 'idEtablissement', 'idMembreParent', 'numerosn', 'email', ),
        BasePeer::TYPE_COLNAME => array (CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT, CommonTMembreGroupementEntreprisePeer::NUMEROSN, CommonTMembreGroupementEntreprisePeer::EMAIL, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_MEMBRE_GROUPEMENT_ENTREPRISE', 'ID_ROLE_JURIDIQUE', 'ID_GROUPEMENT_ENTREPRISE', 'ID_ENTREPRISE', 'ID_ETABLISSEMENT', 'ID_MEMBRE_PARENT', 'NUMEROSN', 'EMAIL', ),
        BasePeer::TYPE_FIELDNAME => array ('id_membre_groupement_entreprise', 'id_role_juridique', 'id_groupement_entreprise', 'id_entreprise', 'id_etablissement', 'id_membre_parent', 'numeroSN', 'email', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTMembreGroupementEntreprisePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdMembreGroupementEntreprise' => 0, 'IdRoleJuridique' => 1, 'IdGroupementEntreprise' => 2, 'IdEntreprise' => 3, 'IdEtablissement' => 4, 'IdMembreParent' => 5, 'Numerosn' => 6, 'Email' => 7, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idMembreGroupementEntreprise' => 0, 'idRoleJuridique' => 1, 'idGroupementEntreprise' => 2, 'idEntreprise' => 3, 'idEtablissement' => 4, 'idMembreParent' => 5, 'numerosn' => 6, 'email' => 7, ),
        BasePeer::TYPE_COLNAME => array (CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE => 0, CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE => 1, CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE => 2, CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE => 3, CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT => 4, CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT => 5, CommonTMembreGroupementEntreprisePeer::NUMEROSN => 6, CommonTMembreGroupementEntreprisePeer::EMAIL => 7, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_MEMBRE_GROUPEMENT_ENTREPRISE' => 0, 'ID_ROLE_JURIDIQUE' => 1, 'ID_GROUPEMENT_ENTREPRISE' => 2, 'ID_ENTREPRISE' => 3, 'ID_ETABLISSEMENT' => 4, 'ID_MEMBRE_PARENT' => 5, 'NUMEROSN' => 6, 'EMAIL' => 7, ),
        BasePeer::TYPE_FIELDNAME => array ('id_membre_groupement_entreprise' => 0, 'id_role_juridique' => 1, 'id_groupement_entreprise' => 2, 'id_entreprise' => 3, 'id_etablissement' => 4, 'id_membre_parent' => 5, 'numeroSN' => 6, 'email' => 7, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTMembreGroupementEntreprisePeer::getFieldNames($toType);
        $key = isset(CommonTMembreGroupementEntreprisePeer::$fieldKeys[$fromType][$name]) ? CommonTMembreGroupementEntreprisePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTMembreGroupementEntreprisePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTMembreGroupementEntreprisePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTMembreGroupementEntreprisePeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTMembreGroupementEntreprisePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTMembreGroupementEntreprisePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE);
            $criteria->addSelectColumn(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE);
            $criteria->addSelectColumn(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE);
            $criteria->addSelectColumn(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE);
            $criteria->addSelectColumn(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT);
            $criteria->addSelectColumn(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT);
            $criteria->addSelectColumn(CommonTMembreGroupementEntreprisePeer::NUMEROSN);
            $criteria->addSelectColumn(CommonTMembreGroupementEntreprisePeer::EMAIL);
        } else {
            $criteria->addSelectColumn($alias . '.id_membre_groupement_entreprise');
            $criteria->addSelectColumn($alias . '.id_role_juridique');
            $criteria->addSelectColumn($alias . '.id_groupement_entreprise');
            $criteria->addSelectColumn($alias . '.id_entreprise');
            $criteria->addSelectColumn($alias . '.id_etablissement');
            $criteria->addSelectColumn($alias . '.id_membre_parent');
            $criteria->addSelectColumn($alias . '.numeroSN');
            $criteria->addSelectColumn($alias . '.email');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTMembreGroupementEntreprise
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTMembreGroupementEntreprisePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTMembreGroupementEntreprisePeer::populateObjects(CommonTMembreGroupementEntreprisePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTMembreGroupementEntreprise $obj A CommonTMembreGroupementEntreprise object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getIdMembreGroupementEntreprise();
            } // if key === null
            CommonTMembreGroupementEntreprisePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTMembreGroupementEntreprise object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTMembreGroupementEntreprise) {
                $key = (string) $value->getIdMembreGroupementEntreprise();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTMembreGroupementEntreprise object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTMembreGroupementEntreprisePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTMembreGroupementEntreprise Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTMembreGroupementEntreprisePeer::$instances[$key])) {
                return CommonTMembreGroupementEntreprisePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTMembreGroupementEntreprisePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTMembreGroupementEntreprisePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_membre_groupement_entreprise
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTMembreGroupementEntreprise object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTMembreGroupementEntreprisePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related Entreprise table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinEntreprise(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTEtablissement table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTEtablissement(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTGroupementEntreprise table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTGroupementEntreprise(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTRoleJuridique table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTRoleJuridique(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with their Entreprise objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;
        EntreprisePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to $obj2 (Entreprise)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with their CommonTEtablissement objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTEtablissement(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;
        CommonTEtablissementPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTEtablissementPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTEtablissementPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTEtablissementPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to $obj2 (CommonTEtablissement)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with their CommonTGroupementEntreprise objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTGroupementEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;
        CommonTGroupementEntreprisePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTGroupementEntreprisePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTGroupementEntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTGroupementEntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to $obj2 (CommonTGroupementEntreprise)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with their CommonTRoleJuridique objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTRoleJuridique(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;
        CommonTRoleJuridiquePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTRoleJuridiquePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTRoleJuridiquePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTRoleJuridiquePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTRoleJuridiquePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to $obj2 (CommonTRoleJuridique)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol2 = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        CommonTGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTRoleJuridiquePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTRoleJuridiquePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Entreprise rows

            $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj2 (Entreprise)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);
            } // if joined row not null

            // Add objects for joined CommonTEtablissement rows

            $key3 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonTEtablissementPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonTEtablissementPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTEtablissementPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj3 (CommonTEtablissement)
                $obj3->addCommonTMembreGroupementEntreprise($obj1);
            } // if joined row not null

            // Add objects for joined CommonTGroupementEntreprise rows

            $key4 = CommonTGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonTGroupementEntreprisePeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonTGroupementEntreprisePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTGroupementEntreprisePeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj4 (CommonTGroupementEntreprise)
                $obj4->addCommonTMembreGroupementEntreprise($obj1);
            } // if joined row not null

            // Add objects for joined CommonTRoleJuridique rows

            $key5 = CommonTRoleJuridiquePeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = CommonTRoleJuridiquePeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = CommonTRoleJuridiquePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTRoleJuridiquePeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj5 (CommonTRoleJuridique)
                $obj5->addCommonTMembreGroupementEntreprise($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Entreprise table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptEntreprise(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTEtablissement table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTEtablissement(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTGroupementEntreprise table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTGroupementEntreprise(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTMembreGroupementEntrepriseRelatedByIdMembreParent table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTMembreGroupementEntrepriseRelatedByIdMembreParent(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTRoleJuridique table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTRoleJuridique(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with all related objects except Entreprise.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol2 = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        CommonTGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTRoleJuridiquePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTRoleJuridiquePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTEtablissement rows

                $key2 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTEtablissementPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTEtablissementPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTEtablissementPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj2 (CommonTEtablissement)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTGroupementEntreprise rows

                $key3 = CommonTGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTGroupementEntreprisePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTGroupementEntreprisePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTGroupementEntreprisePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj3 (CommonTGroupementEntreprise)
                $obj3->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTRoleJuridique rows

                $key4 = CommonTRoleJuridiquePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTRoleJuridiquePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTRoleJuridiquePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTRoleJuridiquePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj4 (CommonTRoleJuridique)
                $obj4->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with all related objects except CommonTEtablissement.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTEtablissement(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol2 = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTRoleJuridiquePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTRoleJuridiquePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Entreprise rows

                $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj2 (Entreprise)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTGroupementEntreprise rows

                $key3 = CommonTGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTGroupementEntreprisePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTGroupementEntreprisePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTGroupementEntreprisePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj3 (CommonTGroupementEntreprise)
                $obj3->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTRoleJuridique rows

                $key4 = CommonTRoleJuridiquePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTRoleJuridiquePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTRoleJuridiquePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTRoleJuridiquePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj4 (CommonTRoleJuridique)
                $obj4->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with all related objects except CommonTGroupementEntreprise.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTGroupementEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol2 = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        CommonTRoleJuridiquePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTRoleJuridiquePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Entreprise rows

                $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj2 (Entreprise)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTEtablissement rows

                $key3 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTEtablissementPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTEtablissementPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTEtablissementPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj3 (CommonTEtablissement)
                $obj3->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTRoleJuridique rows

                $key4 = CommonTRoleJuridiquePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTRoleJuridiquePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTRoleJuridiquePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTRoleJuridiquePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj4 (CommonTRoleJuridique)
                $obj4->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with all related objects except CommonTMembreGroupementEntrepriseRelatedByIdMembreParent.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTMembreGroupementEntrepriseRelatedByIdMembreParent(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol2 = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        CommonTGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTRoleJuridiquePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTRoleJuridiquePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Entreprise rows

                $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj2 (Entreprise)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTEtablissement rows

                $key3 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTEtablissementPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTEtablissementPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTEtablissementPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj3 (CommonTEtablissement)
                $obj3->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTGroupementEntreprise rows

                $key4 = CommonTGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTGroupementEntreprisePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTGroupementEntreprisePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTGroupementEntreprisePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj4 (CommonTGroupementEntreprise)
                $obj4->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTRoleJuridique rows

                $key5 = CommonTRoleJuridiquePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTRoleJuridiquePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTRoleJuridiquePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTRoleJuridiquePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj5 (CommonTRoleJuridique)
                $obj5->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTMembreGroupementEntreprise objects pre-filled with all related objects except CommonTRoleJuridique.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMembreGroupementEntreprise objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTRoleJuridique(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        }

        CommonTMembreGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol2 = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        CommonTGroupementEntreprisePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $criteria->addJoin(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMembreGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTMembreGroupementEntreprisePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Entreprise rows

                $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj2 (Entreprise)
                $obj2->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTEtablissement rows

                $key3 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTEtablissementPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTEtablissementPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTEtablissementPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj3 (CommonTEtablissement)
                $obj3->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTGroupementEntreprise rows

                $key4 = CommonTGroupementEntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTGroupementEntreprisePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTGroupementEntreprisePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTGroupementEntreprisePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTMembreGroupementEntreprise) to the collection in $obj4 (CommonTGroupementEntreprise)
                $obj4->addCommonTMembreGroupementEntreprise($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME)->getTable(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTMembreGroupementEntreprisePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTMembreGroupementEntrepriseTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTMembreGroupementEntreprisePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTMembreGroupementEntreprise or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTMembreGroupementEntreprise object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTMembreGroupementEntreprise object
        }

        if ($criteria->containsKey(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE) && $criteria->keyContainsValue(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTMembreGroupementEntreprise or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTMembreGroupementEntreprise object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE);
            $value = $criteria->remove(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE);
            if ($value) {
                $selectCriteria->add(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);
            }

        } else { // $values is CommonTMembreGroupementEntreprise object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_membre_groupement_entreprise table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTMembreGroupementEntreprisePeer::TABLE_NAME, $con, CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTMembreGroupementEntreprisePeer::clearInstancePool();
            CommonTMembreGroupementEntreprisePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTMembreGroupementEntreprise or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTMembreGroupementEntreprise object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTMembreGroupementEntreprisePeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTMembreGroupementEntreprise) { // it's a model object
            // invalidate the cache for this single object
            CommonTMembreGroupementEntreprisePeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
            $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTMembreGroupementEntreprisePeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTMembreGroupementEntreprisePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTMembreGroupementEntreprise object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTMembreGroupementEntreprise $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTMembreGroupementEntreprisePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, CommonTMembreGroupementEntreprisePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTMembreGroupementEntreprise
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $pk);

        $v = CommonTMembreGroupementEntreprisePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTMembreGroupementEntreprise[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
            $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $pks, Criteria::IN);
            $objs = CommonTMembreGroupementEntreprisePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTMembreGroupementEntreprisePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTMembreGroupementEntreprisePeer::buildTableMap();

