<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTFusionnerServices;
use Application\Propel\Mpe\CommonTFusionnerServicesPeer;
use Application\Propel\Mpe\CommonTFusionnerServicesQuery;

/**
 * Base class that represents a query for the 't_fusionner_services' table.
 *
 *
 *
 * @method CommonTFusionnerServicesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTFusionnerServicesQuery orderByIdServiceSource($order = Criteria::ASC) Order by the id_service_source column
 * @method CommonTFusionnerServicesQuery orderByIdServiceCible($order = Criteria::ASC) Order by the id_service_cible column
 * @method CommonTFusionnerServicesQuery orderByOldIdServiceSource($order = Criteria::ASC) Order by the old_id_service_source column
 * @method CommonTFusionnerServicesQuery orderByOldIdServiceCible($order = Criteria::ASC) Order by the old_id_service_cible column
 * @method CommonTFusionnerServicesQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTFusionnerServicesQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonTFusionnerServicesQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTFusionnerServicesQuery orderByDateFusion($order = Criteria::ASC) Order by the date_fusion column
 * @method CommonTFusionnerServicesQuery orderByDonneesFusionnees($order = Criteria::ASC) Order by the donnees_fusionnees column
 *
 * @method CommonTFusionnerServicesQuery groupById() Group by the id column
 * @method CommonTFusionnerServicesQuery groupByIdServiceSource() Group by the id_service_source column
 * @method CommonTFusionnerServicesQuery groupByIdServiceCible() Group by the id_service_cible column
 * @method CommonTFusionnerServicesQuery groupByOldIdServiceSource() Group by the old_id_service_source column
 * @method CommonTFusionnerServicesQuery groupByOldIdServiceCible() Group by the old_id_service_cible column
 * @method CommonTFusionnerServicesQuery groupByOrganisme() Group by the organisme column
 * @method CommonTFusionnerServicesQuery groupByIdAgent() Group by the id_agent column
 * @method CommonTFusionnerServicesQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTFusionnerServicesQuery groupByDateFusion() Group by the date_fusion column
 * @method CommonTFusionnerServicesQuery groupByDonneesFusionnees() Group by the donnees_fusionnees column
 *
 * @method CommonTFusionnerServicesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTFusionnerServicesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTFusionnerServicesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTFusionnerServicesQuery leftJoinCommonServiceRelatedByIdServiceCible($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonServiceRelatedByIdServiceCible relation
 * @method CommonTFusionnerServicesQuery rightJoinCommonServiceRelatedByIdServiceCible($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonServiceRelatedByIdServiceCible relation
 * @method CommonTFusionnerServicesQuery innerJoinCommonServiceRelatedByIdServiceCible($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonServiceRelatedByIdServiceCible relation
 *
 * @method CommonTFusionnerServicesQuery leftJoinCommonServiceRelatedByIdServiceSource($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonServiceRelatedByIdServiceSource relation
 * @method CommonTFusionnerServicesQuery rightJoinCommonServiceRelatedByIdServiceSource($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonServiceRelatedByIdServiceSource relation
 * @method CommonTFusionnerServicesQuery innerJoinCommonServiceRelatedByIdServiceSource($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonServiceRelatedByIdServiceSource relation
 *
 * @method CommonTFusionnerServices findOne(PropelPDO $con = null) Return the first CommonTFusionnerServices matching the query
 * @method CommonTFusionnerServices findOneOrCreate(PropelPDO $con = null) Return the first CommonTFusionnerServices matching the query, or a new CommonTFusionnerServices object populated from the query conditions when no match is found
 *
 * @method CommonTFusionnerServices findOneByIdServiceSource(string $id_service_source) Return the first CommonTFusionnerServices filtered by the id_service_source column
 * @method CommonTFusionnerServices findOneByIdServiceCible(string $id_service_cible) Return the first CommonTFusionnerServices filtered by the id_service_cible column
 * @method CommonTFusionnerServices findOneByOldIdServiceSource(int $old_id_service_source) Return the first CommonTFusionnerServices filtered by the old_id_service_source column
 * @method CommonTFusionnerServices findOneByOldIdServiceCible(int $old_id_service_cible) Return the first CommonTFusionnerServices filtered by the old_id_service_cible column
 * @method CommonTFusionnerServices findOneByOrganisme(string $organisme) Return the first CommonTFusionnerServices filtered by the organisme column
 * @method CommonTFusionnerServices findOneByIdAgent(int $id_agent) Return the first CommonTFusionnerServices filtered by the id_agent column
 * @method CommonTFusionnerServices findOneByDateCreation(string $date_creation) Return the first CommonTFusionnerServices filtered by the date_creation column
 * @method CommonTFusionnerServices findOneByDateFusion(string $date_fusion) Return the first CommonTFusionnerServices filtered by the date_fusion column
 * @method CommonTFusionnerServices findOneByDonneesFusionnees(string $donnees_fusionnees) Return the first CommonTFusionnerServices filtered by the donnees_fusionnees column
 *
 * @method array findById(int $id) Return CommonTFusionnerServices objects filtered by the id column
 * @method array findByIdServiceSource(string $id_service_source) Return CommonTFusionnerServices objects filtered by the id_service_source column
 * @method array findByIdServiceCible(string $id_service_cible) Return CommonTFusionnerServices objects filtered by the id_service_cible column
 * @method array findByOldIdServiceSource(int $old_id_service_source) Return CommonTFusionnerServices objects filtered by the old_id_service_source column
 * @method array findByOldIdServiceCible(int $old_id_service_cible) Return CommonTFusionnerServices objects filtered by the old_id_service_cible column
 * @method array findByOrganisme(string $organisme) Return CommonTFusionnerServices objects filtered by the organisme column
 * @method array findByIdAgent(int $id_agent) Return CommonTFusionnerServices objects filtered by the id_agent column
 * @method array findByDateCreation(string $date_creation) Return CommonTFusionnerServices objects filtered by the date_creation column
 * @method array findByDateFusion(string $date_fusion) Return CommonTFusionnerServices objects filtered by the date_fusion column
 * @method array findByDonneesFusionnees(string $donnees_fusionnees) Return CommonTFusionnerServices objects filtered by the donnees_fusionnees column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTFusionnerServicesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTFusionnerServicesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTFusionnerServices', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTFusionnerServicesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTFusionnerServicesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTFusionnerServicesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTFusionnerServicesQuery) {
            return $criteria;
        }
        $query = new CommonTFusionnerServicesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTFusionnerServices|CommonTFusionnerServices[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTFusionnerServicesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTFusionnerServices A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTFusionnerServices A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_service_source`, `id_service_cible`, `old_id_service_source`, `old_id_service_cible`, `organisme`, `id_agent`, `date_creation`, `date_fusion`, `donnees_fusionnees` FROM `t_fusionner_services` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTFusionnerServices();
            $obj->hydrate($row);
            CommonTFusionnerServicesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTFusionnerServices|CommonTFusionnerServices[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTFusionnerServices[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_service_source column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServiceSource(1234); // WHERE id_service_source = 1234
     * $query->filterByIdServiceSource(array(12, 34)); // WHERE id_service_source IN (12, 34)
     * $query->filterByIdServiceSource(array('min' => 12)); // WHERE id_service_source >= 12
     * $query->filterByIdServiceSource(array('max' => 12)); // WHERE id_service_source <= 12
     * </code>
     *
     * @see       filterByCommonServiceRelatedByIdServiceSource()
     *
     * @param     mixed $idServiceSource The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByIdServiceSource($idServiceSource = null, $comparison = null)
    {
        if (is_array($idServiceSource)) {
            $useMinMax = false;
            if (isset($idServiceSource['min'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, $idServiceSource['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServiceSource['max'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, $idServiceSource['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, $idServiceSource, $comparison);
    }

    /**
     * Filter the query on the id_service_cible column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServiceCible(1234); // WHERE id_service_cible = 1234
     * $query->filterByIdServiceCible(array(12, 34)); // WHERE id_service_cible IN (12, 34)
     * $query->filterByIdServiceCible(array('min' => 12)); // WHERE id_service_cible >= 12
     * $query->filterByIdServiceCible(array('max' => 12)); // WHERE id_service_cible <= 12
     * </code>
     *
     * @see       filterByCommonServiceRelatedByIdServiceCible()
     *
     * @param     mixed $idServiceCible The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByIdServiceCible($idServiceCible = null, $comparison = null)
    {
        if (is_array($idServiceCible)) {
            $useMinMax = false;
            if (isset($idServiceCible['min'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, $idServiceCible['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServiceCible['max'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, $idServiceCible['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, $idServiceCible, $comparison);
    }

    /**
     * Filter the query on the old_id_service_source column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdServiceSource(1234); // WHERE old_id_service_source = 1234
     * $query->filterByOldIdServiceSource(array(12, 34)); // WHERE old_id_service_source IN (12, 34)
     * $query->filterByOldIdServiceSource(array('min' => 12)); // WHERE old_id_service_source >= 12
     * $query->filterByOldIdServiceSource(array('max' => 12)); // WHERE old_id_service_source <= 12
     * </code>
     *
     * @param     mixed $oldIdServiceSource The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByOldIdServiceSource($oldIdServiceSource = null, $comparison = null)
    {
        if (is_array($oldIdServiceSource)) {
            $useMinMax = false;
            if (isset($oldIdServiceSource['min'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE, $oldIdServiceSource['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdServiceSource['max'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE, $oldIdServiceSource['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE, $oldIdServiceSource, $comparison);
    }

    /**
     * Filter the query on the old_id_service_cible column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdServiceCible(1234); // WHERE old_id_service_cible = 1234
     * $query->filterByOldIdServiceCible(array(12, 34)); // WHERE old_id_service_cible IN (12, 34)
     * $query->filterByOldIdServiceCible(array('min' => 12)); // WHERE old_id_service_cible >= 12
     * $query->filterByOldIdServiceCible(array('max' => 12)); // WHERE old_id_service_cible <= 12
     * </code>
     *
     * @param     mixed $oldIdServiceCible The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByOldIdServiceCible($oldIdServiceCible = null, $comparison = null)
    {
        if (is_array($oldIdServiceCible)) {
            $useMinMax = false;
            if (isset($oldIdServiceCible['min'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE, $oldIdServiceCible['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdServiceCible['max'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE, $oldIdServiceCible['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE, $oldIdServiceCible, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_fusion column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFusion('2011-03-14'); // WHERE date_fusion = '2011-03-14'
     * $query->filterByDateFusion('now'); // WHERE date_fusion = '2011-03-14'
     * $query->filterByDateFusion(array('max' => 'yesterday')); // WHERE date_fusion > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFusion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByDateFusion($dateFusion = null, $comparison = null)
    {
        if (is_array($dateFusion)) {
            $useMinMax = false;
            if (isset($dateFusion['min'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::DATE_FUSION, $dateFusion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFusion['max'])) {
                $this->addUsingAlias(CommonTFusionnerServicesPeer::DATE_FUSION, $dateFusion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::DATE_FUSION, $dateFusion, $comparison);
    }

    /**
     * Filter the query on the donnees_fusionnees column
     *
     * Example usage:
     * <code>
     * $query->filterByDonneesFusionnees('fooValue');   // WHERE donnees_fusionnees = 'fooValue'
     * $query->filterByDonneesFusionnees('%fooValue%'); // WHERE donnees_fusionnees LIKE '%fooValue%'
     * </code>
     *
     * @param     string $donneesFusionnees The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function filterByDonneesFusionnees($donneesFusionnees = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($donneesFusionnees)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $donneesFusionnees)) {
                $donneesFusionnees = str_replace('*', '%', $donneesFusionnees);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES, $donneesFusionnees, $comparison);
    }

    /**
     * Filter the query by a related CommonService object
     *
     * @param   CommonService|PropelObjectCollection $commonService The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTFusionnerServicesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonServiceRelatedByIdServiceCible($commonService, $comparison = null)
    {
        if ($commonService instanceof CommonService) {
            return $this
                ->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, $commonService->getId(), $comparison);
        } elseif ($commonService instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, $commonService->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonServiceRelatedByIdServiceCible() only accepts arguments of type CommonService or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonServiceRelatedByIdServiceCible relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function joinCommonServiceRelatedByIdServiceCible($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonServiceRelatedByIdServiceCible');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonServiceRelatedByIdServiceCible');
        }

        return $this;
    }

    /**
     * Use the CommonServiceRelatedByIdServiceCible relation CommonService object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonServiceQuery A secondary query class using the current class as primary query
     */
    public function useCommonServiceRelatedByIdServiceCibleQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonServiceRelatedByIdServiceCible($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonServiceRelatedByIdServiceCible', '\Application\Propel\Mpe\CommonServiceQuery');
    }

    /**
     * Filter the query by a related CommonService object
     *
     * @param   CommonService|PropelObjectCollection $commonService The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTFusionnerServicesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonServiceRelatedByIdServiceSource($commonService, $comparison = null)
    {
        if ($commonService instanceof CommonService) {
            return $this
                ->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, $commonService->getId(), $comparison);
        } elseif ($commonService instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, $commonService->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonServiceRelatedByIdServiceSource() only accepts arguments of type CommonService or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonServiceRelatedByIdServiceSource relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function joinCommonServiceRelatedByIdServiceSource($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonServiceRelatedByIdServiceSource');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonServiceRelatedByIdServiceSource');
        }

        return $this;
    }

    /**
     * Use the CommonServiceRelatedByIdServiceSource relation CommonService object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonServiceQuery A secondary query class using the current class as primary query
     */
    public function useCommonServiceRelatedByIdServiceSourceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonServiceRelatedByIdServiceSource($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonServiceRelatedByIdServiceSource', '\Application\Propel\Mpe\CommonServiceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTFusionnerServices $commonTFusionnerServices Object to remove from the list of results
     *
     * @return CommonTFusionnerServicesQuery The current query, for fluid interface
     */
    public function prune($commonTFusionnerServices = null)
    {
        if ($commonTFusionnerServices) {
            $this->addUsingAlias(CommonTFusionnerServicesPeer::ID, $commonTFusionnerServices->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
