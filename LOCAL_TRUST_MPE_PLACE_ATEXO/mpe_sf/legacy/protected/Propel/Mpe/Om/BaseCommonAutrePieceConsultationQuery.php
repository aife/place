<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAutrePieceConsultation;
use Application\Propel\Mpe\CommonAutrePieceConsultationPeer;
use Application\Propel\Mpe\CommonAutrePieceConsultationQuery;
use Application\Propel\Mpe\CommonBlobOrganismeFile;
use Application\Propel\Mpe\CommonConsultation;

/**
 * Base class that represents a query for the 'autre_piece_consultation' table.
 *
 *
 *
 * @method CommonAutrePieceConsultationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonAutrePieceConsultationQuery orderByBlobId($order = Criteria::ASC) Order by the blob_id column
 * @method CommonAutrePieceConsultationQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 * @method CommonAutrePieceConsultationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method CommonAutrePieceConsultationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method CommonAutrePieceConsultationQuery groupById() Group by the id column
 * @method CommonAutrePieceConsultationQuery groupByBlobId() Group by the blob_id column
 * @method CommonAutrePieceConsultationQuery groupByConsultationId() Group by the consultation_id column
 * @method CommonAutrePieceConsultationQuery groupByCreatedAt() Group by the created_at column
 * @method CommonAutrePieceConsultationQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method CommonAutrePieceConsultationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonAutrePieceConsultationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonAutrePieceConsultationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonAutrePieceConsultationQuery leftJoinCommonBlobOrganismeFile($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonBlobOrganismeFile relation
 * @method CommonAutrePieceConsultationQuery rightJoinCommonBlobOrganismeFile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonBlobOrganismeFile relation
 * @method CommonAutrePieceConsultationQuery innerJoinCommonBlobOrganismeFile($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonBlobOrganismeFile relation
 *
 * @method CommonAutrePieceConsultationQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonAutrePieceConsultationQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonAutrePieceConsultationQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonAutrePieceConsultation findOne(PropelPDO $con = null) Return the first CommonAutrePieceConsultation matching the query
 * @method CommonAutrePieceConsultation findOneOrCreate(PropelPDO $con = null) Return the first CommonAutrePieceConsultation matching the query, or a new CommonAutrePieceConsultation object populated from the query conditions when no match is found
 *
 * @method CommonAutrePieceConsultation findOneByBlobId(int $blob_id) Return the first CommonAutrePieceConsultation filtered by the blob_id column
 * @method CommonAutrePieceConsultation findOneByConsultationId(int $consultation_id) Return the first CommonAutrePieceConsultation filtered by the consultation_id column
 * @method CommonAutrePieceConsultation findOneByCreatedAt(string $created_at) Return the first CommonAutrePieceConsultation filtered by the created_at column
 * @method CommonAutrePieceConsultation findOneByUpdatedAt(string $updated_at) Return the first CommonAutrePieceConsultation filtered by the updated_at column
 *
 * @method array findById(int $id) Return CommonAutrePieceConsultation objects filtered by the id column
 * @method array findByBlobId(int $blob_id) Return CommonAutrePieceConsultation objects filtered by the blob_id column
 * @method array findByConsultationId(int $consultation_id) Return CommonAutrePieceConsultation objects filtered by the consultation_id column
 * @method array findByCreatedAt(string $created_at) Return CommonAutrePieceConsultation objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return CommonAutrePieceConsultation objects filtered by the updated_at column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonAutrePieceConsultationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonAutrePieceConsultationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonAutrePieceConsultation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonAutrePieceConsultationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonAutrePieceConsultationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonAutrePieceConsultationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonAutrePieceConsultationQuery) {
            return $criteria;
        }
        $query = new CommonAutrePieceConsultationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonAutrePieceConsultation|CommonAutrePieceConsultation[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonAutrePieceConsultationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonAutrePieceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonAutrePieceConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonAutrePieceConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `blob_id`, `consultation_id`, `created_at`, `updated_at` FROM `autre_piece_consultation` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonAutrePieceConsultation();
            $obj->hydrate($row);
            CommonAutrePieceConsultationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonAutrePieceConsultation|CommonAutrePieceConsultation[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonAutrePieceConsultation[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonAutrePieceConsultationPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonAutrePieceConsultationPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAutrePieceConsultationPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the blob_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBlobId(1234); // WHERE blob_id = 1234
     * $query->filterByBlobId(array(12, 34)); // WHERE blob_id IN (12, 34)
     * $query->filterByBlobId(array('min' => 12)); // WHERE blob_id >= 12
     * $query->filterByBlobId(array('max' => 12)); // WHERE blob_id <= 12
     * </code>
     *
     * @see       filterByCommonBlobOrganismeFile()
     *
     * @param     mixed $blobId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function filterByBlobId($blobId = null, $comparison = null)
    {
        if (is_array($blobId)) {
            $useMinMax = false;
            if (isset($blobId['min'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::BLOB_ID, $blobId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($blobId['max'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::BLOB_ID, $blobId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAutrePieceConsultationPeer::BLOB_ID, $blobId, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultation()
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAutrePieceConsultationPeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAutrePieceConsultationPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CommonAutrePieceConsultationPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAutrePieceConsultationPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related CommonBlobOrganismeFile object
     *
     * @param   CommonBlobOrganismeFile|PropelObjectCollection $commonBlobOrganismeFile The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAutrePieceConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonBlobOrganismeFile($commonBlobOrganismeFile, $comparison = null)
    {
        if ($commonBlobOrganismeFile instanceof CommonBlobOrganismeFile) {
            return $this
                ->addUsingAlias(CommonAutrePieceConsultationPeer::BLOB_ID, $commonBlobOrganismeFile->getId(), $comparison);
        } elseif ($commonBlobOrganismeFile instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonAutrePieceConsultationPeer::BLOB_ID, $commonBlobOrganismeFile->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonBlobOrganismeFile() only accepts arguments of type CommonBlobOrganismeFile or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonBlobOrganismeFile relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function joinCommonBlobOrganismeFile($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonBlobOrganismeFile');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonBlobOrganismeFile');
        }

        return $this;
    }

    /**
     * Use the CommonBlobOrganismeFile relation CommonBlobOrganismeFile object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonBlobOrganismeFileQuery A secondary query class using the current class as primary query
     */
    public function useCommonBlobOrganismeFileQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonBlobOrganismeFile($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonBlobOrganismeFile', '\Application\Propel\Mpe\CommonBlobOrganismeFileQuery');
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAutrePieceConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonAutrePieceConsultationPeer::CONSULTATION_ID, $commonConsultation->getId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonAutrePieceConsultationPeer::CONSULTATION_ID, $commonConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonAutrePieceConsultation $commonAutrePieceConsultation Object to remove from the list of results
     *
     * @return CommonAutrePieceConsultationQuery The current query, for fluid interface
     */
    public function prune($commonAutrePieceConsultation = null)
    {
        if ($commonAutrePieceConsultation) {
            $this->addUsingAlias(CommonAutrePieceConsultationPeer::ID, $commonAutrePieceConsultation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
