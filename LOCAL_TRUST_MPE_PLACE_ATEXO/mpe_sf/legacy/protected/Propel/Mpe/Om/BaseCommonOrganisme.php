<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAdministrateur;
use Application\Propel\Mpe\CommonAdministrateurQuery;
use Application\Propel\Mpe\CommonAffiliationService;
use Application\Propel\Mpe\CommonAffiliationServiceQuery;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonCategorieINSEE;
use Application\Propel\Mpe\CommonCategorieINSEEQuery;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConfigurationOrganismeQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonDestinataireMiseDisposition;
use Application\Propel\Mpe\CommonDestinataireMiseDispositionQuery;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganisme;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganismeQuery;
use Application\Propel\Mpe\CommonEchangeQuery;
use Application\Propel\Mpe\CommonInvitePermanentTransverse;
use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonOrganismeServiceMetier;
use Application\Propel\Mpe\CommonOrganismeServiceMetierQuery;
use Application\Propel\Mpe\CommonParametrageEnchere;
use Application\Propel\Mpe\CommonParametrageEnchereQuery;
use Application\Propel\Mpe\CommonPlateformeVirtuelleOrganisme;
use Application\Propel\Mpe\CommonPlateformeVirtuelleOrganismeQuery;
use Application\Propel\Mpe\CommonRPA;
use Application\Propel\Mpe\CommonRPAQuery;
use Application\Propel\Mpe\CommonTCAOCommission;
use Application\Propel\Mpe\CommonTCAOCommissionAgent;
use Application\Propel\Mpe\CommonTCAOCommissionAgentQuery;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExterneQuery;
use Application\Propel\Mpe\CommonTCAOCommissionQuery;
use Application\Propel\Mpe\CommonTCAOIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOIntervenantExterneQuery;
use Application\Propel\Mpe\CommonTCAOOrdreDuJour;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenant;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenantQuery;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourQuery;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeanceAgent;
use Application\Propel\Mpe\CommonTCAOSeanceAgentQuery;
use Application\Propel\Mpe\CommonTCAOSeanceIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOSeanceIntervenantExterneQuery;
use Application\Propel\Mpe\CommonTCAOSeanceQuery;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidatureQuery;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTDumeContexteQuery;
use Application\Propel\Mpe\CommonTListeLotsCandidature;
use Application\Propel\Mpe\CommonTListeLotsCandidatureQuery;
use Application\Propel\Mpe\CommonTVisionRmaAgentOrganisme;
use Application\Propel\Mpe\CommonTVisionRmaAgentOrganismeQuery;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\CommonTelechargementQuery;
use Application\Propel\Mpe\CommonTiers;
use Application\Propel\Mpe\CommonTiersQuery;

/**
 * Base class that represents a row from the 'Organisme' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonOrganisme extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonOrganismePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonOrganismePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the acronyme field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $acronyme;

    /**
     * The value for the type_article_org field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $type_article_org;

    /**
     * The value for the denomination_org field.
     * @var        string
     */
    protected $denomination_org;

    /**
     * The value for the categorie_insee field.
     * @var        string
     */
    protected $categorie_insee;

    /**
     * The value for the description_org field.
     * @var        string
     */
    protected $description_org;

    /**
     * The value for the adresse field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse;

    /**
     * The value for the cp field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $cp;

    /**
     * The value for the ville field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville;

    /**
     * The value for the email field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $email;

    /**
     * The value for the url field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $url;

    /**
     * The value for the id_attrib_file field.
     * @var        string
     */
    protected $id_attrib_file;

    /**
     * The value for the attrib_file field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $attrib_file;

    /**
     * The value for the date_creation field.
     * Note: this column has a database default value of: '0000-00-00 00:00:00'
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the active field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $active;

    /**
     * The value for the id_client_anm field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $id_client_anm;

    /**
     * The value for the status field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $status;

    /**
     * The value for the signataire_cao field.
     * @var        string
     */
    protected $signataire_cao;

    /**
     * The value for the sigle field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $sigle;

    /**
     * The value for the adresse2 field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse2;

    /**
     * The value for the tel field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $tel;

    /**
     * The value for the telecopie field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $telecopie;

    /**
     * The value for the pays field.
     * @var        string
     */
    protected $pays;

    /**
     * The value for the affichage_entite field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $affichage_entite;

    /**
     * The value for the id_initial field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $id_initial;

    /**
     * The value for the denomination_org_ar field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination_org_ar;

    /**
     * The value for the description_org_ar field.
     * @var        string
     */
    protected $description_org_ar;

    /**
     * The value for the adresse_ar field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_ar;

    /**
     * The value for the ville_ar field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_ar;

    /**
     * The value for the adresse2_ar field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse2_ar;

    /**
     * The value for the pays_ar field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $pays_ar;

    /**
     * The value for the denomination_org_fr field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination_org_fr;

    /**
     * The value for the description_org_fr field.
     * @var        string
     */
    protected $description_org_fr;

    /**
     * The value for the adresse_fr field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_fr;

    /**
     * The value for the ville_fr field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_fr;

    /**
     * The value for the adresse2_fr field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse2_fr;

    /**
     * The value for the pays_fr field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $pays_fr;

    /**
     * The value for the denomination_org_es field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination_org_es;

    /**
     * The value for the description_org_es field.
     * @var        string
     */
    protected $description_org_es;

    /**
     * The value for the adresse_es field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_es;

    /**
     * The value for the ville_es field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_es;

    /**
     * The value for the adresse2_es field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse2_es;

    /**
     * The value for the pays_es field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $pays_es;

    /**
     * The value for the denomination_org_en field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination_org_en;

    /**
     * The value for the description_org_en field.
     * @var        string
     */
    protected $description_org_en;

    /**
     * The value for the adresse_en field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_en;

    /**
     * The value for the ville_en field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_en;

    /**
     * The value for the adresse2_en field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse2_en;

    /**
     * The value for the pays_en field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $pays_en;

    /**
     * The value for the denomination_org_su field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination_org_su;

    /**
     * The value for the description_org_su field.
     * @var        string
     */
    protected $description_org_su;

    /**
     * The value for the adresse_su field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_su;

    /**
     * The value for the ville_su field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_su;

    /**
     * The value for the adresse2_su field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse2_su;

    /**
     * The value for the pays_su field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $pays_su;

    /**
     * The value for the denomination_org_du field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination_org_du;

    /**
     * The value for the description_org_du field.
     * @var        string
     */
    protected $description_org_du;

    /**
     * The value for the adresse_du field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_du;

    /**
     * The value for the ville_du field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_du;

    /**
     * The value for the adresse2_du field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse2_du;

    /**
     * The value for the pays_du field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $pays_du;

    /**
     * The value for the denomination_org_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination_org_cz;

    /**
     * The value for the description_org_cz field.
     * @var        string
     */
    protected $description_org_cz;

    /**
     * The value for the adresse_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_cz;

    /**
     * The value for the ville_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_cz;

    /**
     * The value for the adresse2_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse2_cz;

    /**
     * The value for the pays_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $pays_cz;

    /**
     * The value for the denomination_org_it field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination_org_it;

    /**
     * The value for the description_org_it field.
     * @var        string
     */
    protected $description_org_it;

    /**
     * The value for the adresse_it field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_it;

    /**
     * The value for the ville_it field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_it;

    /**
     * The value for the adresse2_it field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse2_it;

    /**
     * The value for the pays_it field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $pays_it;

    /**
     * The value for the siren field.
     * @var        string
     */
    protected $siren;

    /**
     * The value for the complement field.
     * @var        string
     */
    protected $complement;

    /**
     * The value for the moniteur_provenance field.
     * @var        int
     */
    protected $moniteur_provenance;

    /**
     * The value for the code_acces_logiciel field.
     * @var        string
     */
    protected $code_acces_logiciel;

    /**
     * The value for the decalage_horaire field.
     * @var        string
     */
    protected $decalage_horaire;

    /**
     * The value for the lieu_residence field.
     * @var        string
     */
    protected $lieu_residence;

    /**
     * The value for the activation_fuseau_horaire field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $activation_fuseau_horaire;

    /**
     * The value for the alerte field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte;

    /**
     * The value for the ordre field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ordre;

    /**
     * The value for the url_interface_anm field.
     * @var        string
     */
    protected $url_interface_anm;

    /**
     * The value for the sous_type_organisme field.
     * Note: this column has a database default value of: 2
     * @var        int
     */
    protected $sous_type_organisme;

    /**
     * The value for the pf_url field.
     * @var        string
     */
    protected $pf_url;

    /**
     * The value for the tag_purge field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $tag_purge;

    /**
     * The value for the id_externe field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $id_externe;

    /**
     * The value for the id_entite field.
     * @var        int
     */
    protected $id_entite;

    /**
     * @var        CommonCategorieINSEE
     */
    protected $aCommonCategorieINSEE;

    /**
     * @var        PropelObjectCollection|CommonAdministrateur[] Collection to store aggregation of CommonAdministrateur objects.
     */
    protected $collCommonAdministrateurs;
    protected $collCommonAdministrateursPartial;

    /**
     * @var        PropelObjectCollection|CommonAffiliationService[] Collection to store aggregation of CommonAffiliationService objects.
     */
    protected $collCommonAffiliationServices;
    protected $collCommonAffiliationServicesPartial;

    /**
     * @var        PropelObjectCollection|CommonAgent[] Collection to store aggregation of CommonAgent objects.
     */
    protected $collCommonAgents;
    protected $collCommonAgentsPartial;

    /**
     * @var        PropelObjectCollection|CommonEchange[] Collection to store aggregation of CommonEchange objects.
     */
    protected $collCommonEchanges;
    protected $collCommonEchangesPartial;

    /**
     * @var        PropelObjectCollection|CommonOrganismeServiceMetier[] Collection to store aggregation of CommonOrganismeServiceMetier objects.
     */
    protected $collCommonOrganismeServiceMetiers;
    protected $collCommonOrganismeServiceMetiersPartial;

    /**
     * @var        PropelObjectCollection|CommonParametrageEnchere[] Collection to store aggregation of CommonParametrageEnchere objects.
     */
    protected $collCommonParametrageEncheres;
    protected $collCommonParametrageEncheresPartial;

    /**
     * @var        PropelObjectCollection|CommonRPA[] Collection to store aggregation of CommonRPA objects.
     */
    protected $collCommonRPAs;
    protected $collCommonRPAsPartial;

    /**
     * @var        PropelObjectCollection|CommonTelechargement[] Collection to store aggregation of CommonTelechargement objects.
     */
    protected $collCommonTelechargements;
    protected $collCommonTelechargementsPartial;

    /**
     * @var        PropelObjectCollection|CommonTiers[] Collection to store aggregation of CommonTiers objects.
     */
    protected $collCommonTierss;
    protected $collCommonTierssPartial;

    /**
     * @var        CommonConfigurationOrganisme one-to-one related CommonConfigurationOrganisme object
     */
    protected $singleCommonConfigurationOrganisme;

    /**
     * @var        PropelObjectCollection|CommonConsultation[] Collection to store aggregation of CommonConsultation objects.
     */
    protected $collCommonConsultations;
    protected $collCommonConsultationsPartial;

    /**
     * @var        PropelObjectCollection|CommonDestinataireMiseDisposition[] Collection to store aggregation of CommonDestinataireMiseDisposition objects.
     */
    protected $collCommonDestinataireMiseDispositions;
    protected $collCommonDestinataireMiseDispositionsPartial;

    /**
     * @var        PropelObjectCollection|CommonEchangeDocApplicationClientOrganisme[] Collection to store aggregation of CommonEchangeDocApplicationClientOrganisme objects.
     */
    protected $collCommonEchangeDocApplicationClientOrganismes;
    protected $collCommonEchangeDocApplicationClientOrganismesPartial;

    /**
     * @var        PropelObjectCollection|CommonInvitePermanentTransverse[] Collection to store aggregation of CommonInvitePermanentTransverse objects.
     */
    protected $collCommonInvitePermanentTransverses;
    protected $collCommonInvitePermanentTransversesPartial;

    /**
     * @var        PropelObjectCollection|CommonPlateformeVirtuelleOrganisme[] Collection to store aggregation of CommonPlateformeVirtuelleOrganisme objects.
     */
    protected $collCommonPlateformeVirtuelleOrganismes;
    protected $collCommonPlateformeVirtuelleOrganismesPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOCommission[] Collection to store aggregation of CommonTCAOCommission objects.
     */
    protected $collCommonTCAOCommissions;
    protected $collCommonTCAOCommissionsPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOCommissionAgent[] Collection to store aggregation of CommonTCAOCommissionAgent objects.
     */
    protected $collCommonTCAOCommissionAgents;
    protected $collCommonTCAOCommissionAgentsPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOCommissionIntervenantExterne[] Collection to store aggregation of CommonTCAOCommissionIntervenantExterne objects.
     */
    protected $collCommonTCAOCommissionIntervenantExternes;
    protected $collCommonTCAOCommissionIntervenantExternesPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOIntervenantExterne[] Collection to store aggregation of CommonTCAOIntervenantExterne objects.
     */
    protected $collCommonTCAOIntervenantExternes;
    protected $collCommonTCAOIntervenantExternesPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOOrdreDuJour[] Collection to store aggregation of CommonTCAOOrdreDuJour objects.
     */
    protected $collCommonTCAOOrdreDuJours;
    protected $collCommonTCAOOrdreDuJoursPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOOrdreDuJourIntervenant[] Collection to store aggregation of CommonTCAOOrdreDuJourIntervenant objects.
     */
    protected $collCommonTCAOOrdreDuJourIntervenants;
    protected $collCommonTCAOOrdreDuJourIntervenantsPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOSeance[] Collection to store aggregation of CommonTCAOSeance objects.
     */
    protected $collCommonTCAOSeances;
    protected $collCommonTCAOSeancesPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOSeanceAgent[] Collection to store aggregation of CommonTCAOSeanceAgent objects.
     */
    protected $collCommonTCAOSeanceAgents;
    protected $collCommonTCAOSeanceAgentsPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOSeanceIntervenantExterne[] Collection to store aggregation of CommonTCAOSeanceIntervenantExterne objects.
     */
    protected $collCommonTCAOSeanceIntervenantExternes;
    protected $collCommonTCAOSeanceIntervenantExternesPartial;

    /**
     * @var        PropelObjectCollection|CommonTCandidature[] Collection to store aggregation of CommonTCandidature objects.
     */
    protected $collCommonTCandidatures;
    protected $collCommonTCandidaturesPartial;

    /**
     * @var        PropelObjectCollection|CommonTDumeContexte[] Collection to store aggregation of CommonTDumeContexte objects.
     */
    protected $collCommonTDumeContextes;
    protected $collCommonTDumeContextesPartial;

    /**
     * @var        PropelObjectCollection|CommonTListeLotsCandidature[] Collection to store aggregation of CommonTListeLotsCandidature objects.
     */
    protected $collCommonTListeLotsCandidatures;
    protected $collCommonTListeLotsCandidaturesPartial;

    /**
     * @var        PropelObjectCollection|CommonTVisionRmaAgentOrganisme[] Collection to store aggregation of CommonTVisionRmaAgentOrganisme objects.
     */
    protected $collCommonTVisionRmaAgentOrganismes;
    protected $collCommonTVisionRmaAgentOrganismesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAdministrateursScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAffiliationServicesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAgentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonOrganismeServiceMetiersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonParametrageEncheresScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonRPAsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTelechargementsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTierssScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConfigurationOrganismesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonDestinataireMiseDispositionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocApplicationClientOrganismesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonInvitePermanentTransversesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonPlateformeVirtuelleOrganismesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOCommissionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOCommissionAgentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOCommissionIntervenantExternesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOIntervenantExternesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOOrdreDuJoursScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOOrdreDuJourIntervenantsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOSeancesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOSeanceAgentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOSeanceIntervenantExternesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCandidaturesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTDumeContextesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTListeLotsCandidaturesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTVisionRmaAgentOrganismesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->acronyme = '';
        $this->type_article_org = 0;
        $this->adresse = '';
        $this->cp = '';
        $this->ville = '';
        $this->email = '';
        $this->url = '';
        $this->attrib_file = '';
        $this->date_creation = '0000-00-00 00:00:00';
        $this->active = '1';
        $this->id_client_anm = '0';
        $this->status = '0';
        $this->sigle = '';
        $this->adresse2 = '';
        $this->tel = '';
        $this->telecopie = '';
        $this->affichage_entite = '';
        $this->id_initial = 0;
        $this->denomination_org_ar = '';
        $this->adresse_ar = '';
        $this->ville_ar = '';
        $this->adresse2_ar = '';
        $this->pays_ar = '';
        $this->denomination_org_fr = '';
        $this->adresse_fr = '';
        $this->ville_fr = '';
        $this->adresse2_fr = '';
        $this->pays_fr = '';
        $this->denomination_org_es = '';
        $this->adresse_es = '';
        $this->ville_es = '';
        $this->adresse2_es = '';
        $this->pays_es = '';
        $this->denomination_org_en = '';
        $this->adresse_en = '';
        $this->ville_en = '';
        $this->adresse2_en = '';
        $this->pays_en = '';
        $this->denomination_org_su = '';
        $this->adresse_su = '';
        $this->ville_su = '';
        $this->adresse2_su = '';
        $this->pays_su = '';
        $this->denomination_org_du = '';
        $this->adresse_du = '';
        $this->ville_du = '';
        $this->adresse2_du = '';
        $this->pays_du = '';
        $this->denomination_org_cz = '';
        $this->adresse_cz = '';
        $this->ville_cz = '';
        $this->adresse2_cz = '';
        $this->pays_cz = '';
        $this->denomination_org_it = '';
        $this->adresse_it = '';
        $this->ville_it = '';
        $this->adresse2_it = '';
        $this->pays_it = '';
        $this->activation_fuseau_horaire = '0';
        $this->alerte = '0';
        $this->ordre = 0;
        $this->sous_type_organisme = 2;
        $this->tag_purge = false;
        $this->id_externe = '0';
    }

    /**
     * Initializes internal state of BaseCommonOrganisme object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [acronyme] column value.
     *
     * @return string
     */
    public function getAcronyme()
    {

        return $this->acronyme;
    }

    /**
     * Get the [type_article_org] column value.
     *
     * @return int
     */
    public function getTypeArticleOrg()
    {

        return $this->type_article_org;
    }

    /**
     * Get the [denomination_org] column value.
     *
     * @return string
     */
    public function getDenominationOrg()
    {

        return $this->denomination_org;
    }

    /**
     * Get the [categorie_insee] column value.
     *
     * @return string
     */
    public function getCategorieInsee()
    {

        return $this->categorie_insee;
    }

    /**
     * Get the [description_org] column value.
     *
     * @return string
     */
    public function getDescriptionOrg()
    {

        return $this->description_org;
    }

    /**
     * Get the [adresse] column value.
     *
     * @return string
     */
    public function getAdresse()
    {

        return $this->adresse;
    }

    /**
     * Get the [cp] column value.
     *
     * @return string
     */
    public function getCp()
    {

        return $this->cp;
    }

    /**
     * Get the [ville] column value.
     *
     * @return string
     */
    public function getVille()
    {

        return $this->ville;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {

        return $this->url;
    }

    /**
     * Get the [id_attrib_file] column value.
     *
     * @return string
     */
    public function getIdAttribFile()
    {

        return $this->id_attrib_file;
    }

    /**
     * Get the [attrib_file] column value.
     *
     * @return string
     */
    public function getAttribFile()
    {

        return $this->attrib_file;
    }

    /**
     * Get the [date_creation] column value.
     *
     * @return string
     */
    public function getDateCreation()
    {

        return $this->date_creation;
    }

    /**
     * Get the [active] column value.
     *
     * @return string
     */
    public function getActive()
    {

        return $this->active;
    }

    /**
     * Get the [id_client_anm] column value.
     *
     * @return string
     */
    public function getIdClientAnm()
    {

        return $this->id_client_anm;
    }

    /**
     * Get the [status] column value.
     *
     * @return string
     */
    public function getStatus()
    {

        return $this->status;
    }

    /**
     * Get the [signataire_cao] column value.
     *
     * @return string
     */
    public function getSignataireCao()
    {

        return $this->signataire_cao;
    }

    /**
     * Get the [sigle] column value.
     *
     * @return string
     */
    public function getSigle()
    {

        return $this->sigle;
    }

    /**
     * Get the [adresse2] column value.
     *
     * @return string
     */
    public function getAdresse2()
    {

        return $this->adresse2;
    }

    /**
     * Get the [tel] column value.
     *
     * @return string
     */
    public function getTel()
    {

        return $this->tel;
    }

    /**
     * Get the [telecopie] column value.
     *
     * @return string
     */
    public function getTelecopie()
    {

        return $this->telecopie;
    }

    /**
     * Get the [pays] column value.
     *
     * @return string
     */
    public function getPays()
    {

        return $this->pays;
    }

    /**
     * Get the [affichage_entite] column value.
     *
     * @return string
     */
    public function getAffichageEntite()
    {

        return $this->affichage_entite;
    }

    /**
     * Get the [id_initial] column value.
     *
     * @return int
     */
    public function getIdInitial()
    {

        return $this->id_initial;
    }

    /**
     * Get the [denomination_org_ar] column value.
     *
     * @return string
     */
    public function getDenominationOrgAr()
    {

        return $this->denomination_org_ar;
    }

    /**
     * Get the [description_org_ar] column value.
     *
     * @return string
     */
    public function getDescriptionOrgAr()
    {

        return $this->description_org_ar;
    }

    /**
     * Get the [adresse_ar] column value.
     *
     * @return string
     */
    public function getAdresseAr()
    {

        return $this->adresse_ar;
    }

    /**
     * Get the [ville_ar] column value.
     *
     * @return string
     */
    public function getVilleAr()
    {

        return $this->ville_ar;
    }

    /**
     * Get the [adresse2_ar] column value.
     *
     * @return string
     */
    public function getAdresse2Ar()
    {

        return $this->adresse2_ar;
    }

    /**
     * Get the [pays_ar] column value.
     *
     * @return string
     */
    public function getPaysAr()
    {

        return $this->pays_ar;
    }

    /**
     * Get the [denomination_org_fr] column value.
     *
     * @return string
     */
    public function getDenominationOrgFr()
    {

        return $this->denomination_org_fr;
    }

    /**
     * Get the [description_org_fr] column value.
     *
     * @return string
     */
    public function getDescriptionOrgFr()
    {

        return $this->description_org_fr;
    }

    /**
     * Get the [adresse_fr] column value.
     *
     * @return string
     */
    public function getAdresseFr()
    {

        return $this->adresse_fr;
    }

    /**
     * Get the [ville_fr] column value.
     *
     * @return string
     */
    public function getVilleFr()
    {

        return $this->ville_fr;
    }

    /**
     * Get the [adresse2_fr] column value.
     *
     * @return string
     */
    public function getAdresse2Fr()
    {

        return $this->adresse2_fr;
    }

    /**
     * Get the [pays_fr] column value.
     *
     * @return string
     */
    public function getPaysFr()
    {

        return $this->pays_fr;
    }

    /**
     * Get the [denomination_org_es] column value.
     *
     * @return string
     */
    public function getDenominationOrgEs()
    {

        return $this->denomination_org_es;
    }

    /**
     * Get the [description_org_es] column value.
     *
     * @return string
     */
    public function getDescriptionOrgEs()
    {

        return $this->description_org_es;
    }

    /**
     * Get the [adresse_es] column value.
     *
     * @return string
     */
    public function getAdresseEs()
    {

        return $this->adresse_es;
    }

    /**
     * Get the [ville_es] column value.
     *
     * @return string
     */
    public function getVilleEs()
    {

        return $this->ville_es;
    }

    /**
     * Get the [adresse2_es] column value.
     *
     * @return string
     */
    public function getAdresse2Es()
    {

        return $this->adresse2_es;
    }

    /**
     * Get the [pays_es] column value.
     *
     * @return string
     */
    public function getPaysEs()
    {

        return $this->pays_es;
    }

    /**
     * Get the [denomination_org_en] column value.
     *
     * @return string
     */
    public function getDenominationOrgEn()
    {

        return $this->denomination_org_en;
    }

    /**
     * Get the [description_org_en] column value.
     *
     * @return string
     */
    public function getDescriptionOrgEn()
    {

        return $this->description_org_en;
    }

    /**
     * Get the [adresse_en] column value.
     *
     * @return string
     */
    public function getAdresseEn()
    {

        return $this->adresse_en;
    }

    /**
     * Get the [ville_en] column value.
     *
     * @return string
     */
    public function getVilleEn()
    {

        return $this->ville_en;
    }

    /**
     * Get the [adresse2_en] column value.
     *
     * @return string
     */
    public function getAdresse2En()
    {

        return $this->adresse2_en;
    }

    /**
     * Get the [pays_en] column value.
     *
     * @return string
     */
    public function getPaysEn()
    {

        return $this->pays_en;
    }

    /**
     * Get the [denomination_org_su] column value.
     *
     * @return string
     */
    public function getDenominationOrgSu()
    {

        return $this->denomination_org_su;
    }

    /**
     * Get the [description_org_su] column value.
     *
     * @return string
     */
    public function getDescriptionOrgSu()
    {

        return $this->description_org_su;
    }

    /**
     * Get the [adresse_su] column value.
     *
     * @return string
     */
    public function getAdresseSu()
    {

        return $this->adresse_su;
    }

    /**
     * Get the [ville_su] column value.
     *
     * @return string
     */
    public function getVilleSu()
    {

        return $this->ville_su;
    }

    /**
     * Get the [adresse2_su] column value.
     *
     * @return string
     */
    public function getAdresse2Su()
    {

        return $this->adresse2_su;
    }

    /**
     * Get the [pays_su] column value.
     *
     * @return string
     */
    public function getPaysSu()
    {

        return $this->pays_su;
    }

    /**
     * Get the [denomination_org_du] column value.
     *
     * @return string
     */
    public function getDenominationOrgDu()
    {

        return $this->denomination_org_du;
    }

    /**
     * Get the [description_org_du] column value.
     *
     * @return string
     */
    public function getDescriptionOrgDu()
    {

        return $this->description_org_du;
    }

    /**
     * Get the [adresse_du] column value.
     *
     * @return string
     */
    public function getAdresseDu()
    {

        return $this->adresse_du;
    }

    /**
     * Get the [ville_du] column value.
     *
     * @return string
     */
    public function getVilleDu()
    {

        return $this->ville_du;
    }

    /**
     * Get the [adresse2_du] column value.
     *
     * @return string
     */
    public function getAdresse2Du()
    {

        return $this->adresse2_du;
    }

    /**
     * Get the [pays_du] column value.
     *
     * @return string
     */
    public function getPaysDu()
    {

        return $this->pays_du;
    }

    /**
     * Get the [denomination_org_cz] column value.
     *
     * @return string
     */
    public function getDenominationOrgCz()
    {

        return $this->denomination_org_cz;
    }

    /**
     * Get the [description_org_cz] column value.
     *
     * @return string
     */
    public function getDescriptionOrgCz()
    {

        return $this->description_org_cz;
    }

    /**
     * Get the [adresse_cz] column value.
     *
     * @return string
     */
    public function getAdresseCz()
    {

        return $this->adresse_cz;
    }

    /**
     * Get the [ville_cz] column value.
     *
     * @return string
     */
    public function getVilleCz()
    {

        return $this->ville_cz;
    }

    /**
     * Get the [adresse2_cz] column value.
     *
     * @return string
     */
    public function getAdresse2Cz()
    {

        return $this->adresse2_cz;
    }

    /**
     * Get the [pays_cz] column value.
     *
     * @return string
     */
    public function getPaysCz()
    {

        return $this->pays_cz;
    }

    /**
     * Get the [denomination_org_it] column value.
     *
     * @return string
     */
    public function getDenominationOrgIt()
    {

        return $this->denomination_org_it;
    }

    /**
     * Get the [description_org_it] column value.
     *
     * @return string
     */
    public function getDescriptionOrgIt()
    {

        return $this->description_org_it;
    }

    /**
     * Get the [adresse_it] column value.
     *
     * @return string
     */
    public function getAdresseIt()
    {

        return $this->adresse_it;
    }

    /**
     * Get the [ville_it] column value.
     *
     * @return string
     */
    public function getVilleIt()
    {

        return $this->ville_it;
    }

    /**
     * Get the [adresse2_it] column value.
     *
     * @return string
     */
    public function getAdresse2It()
    {

        return $this->adresse2_it;
    }

    /**
     * Get the [pays_it] column value.
     *
     * @return string
     */
    public function getPaysIt()
    {

        return $this->pays_it;
    }

    /**
     * Get the [siren] column value.
     *
     * @return string
     */
    public function getSiren()
    {

        return $this->siren;
    }

    /**
     * Get the [complement] column value.
     *
     * @return string
     */
    public function getComplement()
    {

        return $this->complement;
    }

    /**
     * Get the [moniteur_provenance] column value.
     *
     * @return int
     */
    public function getMoniteurProvenance()
    {

        return $this->moniteur_provenance;
    }

    /**
     * Get the [code_acces_logiciel] column value.
     *
     * @return string
     */
    public function getCodeAccesLogiciel()
    {

        return $this->code_acces_logiciel;
    }

    /**
     * Get the [decalage_horaire] column value.
     *
     * @return string
     */
    public function getDecalageHoraire()
    {

        return $this->decalage_horaire;
    }

    /**
     * Get the [lieu_residence] column value.
     *
     * @return string
     */
    public function getLieuResidence()
    {

        return $this->lieu_residence;
    }

    /**
     * Get the [activation_fuseau_horaire] column value.
     *
     * @return string
     */
    public function getActivationFuseauHoraire()
    {

        return $this->activation_fuseau_horaire;
    }

    /**
     * Get the [alerte] column value.
     *
     * @return string
     */
    public function getAlerte()
    {

        return $this->alerte;
    }

    /**
     * Get the [ordre] column value.
     *
     * @return int
     */
    public function getOrdre()
    {

        return $this->ordre;
    }

    /**
     * Get the [url_interface_anm] column value.
     *
     * @return string
     */
    public function getUrlInterfaceAnm()
    {

        return $this->url_interface_anm;
    }

    /**
     * Get the [sous_type_organisme] column value.
     *
     * @return int
     */
    public function getSousTypeOrganisme()
    {

        return $this->sous_type_organisme;
    }

    /**
     * Get the [pf_url] column value.
     *
     * @return string
     */
    public function getPfUrl()
    {

        return $this->pf_url;
    }

    /**
     * Get the [tag_purge] column value.
     *
     * @return boolean
     */
    public function getTagPurge()
    {

        return $this->tag_purge;
    }

    /**
     * Get the [id_externe] column value.
     *
     * @return string
     */
    public function getIdExterne()
    {

        return $this->id_externe;
    }

    /**
     * Get the [id_entite] column value.
     *
     * @return int
     */
    public function getIdEntite()
    {

        return $this->id_entite;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [acronyme] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAcronyme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acronyme !== $v) {
            $this->acronyme = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ACRONYME;
        }


        return $this;
    } // setAcronyme()

    /**
     * Set the value of [type_article_org] column.
     *
     * @param int $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setTypeArticleOrg($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->type_article_org !== $v) {
            $this->type_article_org = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::TYPE_ARTICLE_ORG;
        }


        return $this;
    } // setTypeArticleOrg()

    /**
     * Set the value of [denomination_org] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDenominationOrg($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination_org !== $v) {
            $this->denomination_org = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DENOMINATION_ORG;
        }


        return $this;
    } // setDenominationOrg()

    /**
     * Set the value of [categorie_insee] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCategorieInsee($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->categorie_insee !== $v) {
            $this->categorie_insee = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::CATEGORIE_INSEE;
        }

        if ($this->aCommonCategorieINSEE !== null && $this->aCommonCategorieINSEE->getId() !== $v) {
            $this->aCommonCategorieINSEE = null;
        }


        return $this;
    } // setCategorieInsee()

    /**
     * Set the value of [description_org] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDescriptionOrg($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description_org !== $v) {
            $this->description_org = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DESCRIPTION_ORG;
        }


        return $this;
    } // setDescriptionOrg()

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse !== $v) {
            $this->adresse = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE;
        }


        return $this;
    } // setAdresse()

    /**
     * Set the value of [cp] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->cp !== $v) {
            $this->cp = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::CP;
        }


        return $this;
    } // setCp()

    /**
     * Set the value of [ville] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville !== $v) {
            $this->ville = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::VILLE;
        }


        return $this;
    } // setVille()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::URL;
        }


        return $this;
    } // setUrl()

    /**
     * Set the value of [id_attrib_file] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setIdAttribFile($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_attrib_file !== $v) {
            $this->id_attrib_file = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ID_ATTRIB_FILE;
        }


        return $this;
    } // setIdAttribFile()

    /**
     * Set the value of [attrib_file] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAttribFile($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->attrib_file !== $v) {
            $this->attrib_file = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ATTRIB_FILE;
        }


        return $this;
    } // setAttribFile()

    /**
     * Set the value of [date_creation] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_creation !== $v) {
            $this->date_creation = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DATE_CREATION;
        }


        return $this;
    } // setDateCreation()

    /**
     * Set the value of [active] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ACTIVE;
        }


        return $this;
    } // setActive()

    /**
     * Set the value of [id_client_anm] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setIdClientAnm($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_client_anm !== $v) {
            $this->id_client_anm = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ID_CLIENT_ANM;
        }


        return $this;
    } // setIdClientAnm()

    /**
     * Set the value of [status] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Set the value of [signataire_cao] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setSignataireCao($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->signataire_cao !== $v) {
            $this->signataire_cao = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::SIGNATAIRE_CAO;
        }


        return $this;
    } // setSignataireCao()

    /**
     * Set the value of [sigle] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setSigle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sigle !== $v) {
            $this->sigle = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::SIGLE;
        }


        return $this;
    } // setSigle()

    /**
     * Set the value of [adresse2] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2 !== $v) {
            $this->adresse2 = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE2;
        }


        return $this;
    } // setAdresse2()

    /**
     * Set the value of [tel] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setTel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tel !== $v) {
            $this->tel = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::TEL;
        }


        return $this;
    } // setTel()

    /**
     * Set the value of [telecopie] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setTelecopie($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telecopie !== $v) {
            $this->telecopie = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::TELECOPIE;
        }


        return $this;
    } // setTelecopie()

    /**
     * Set the value of [pays] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPays($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays !== $v) {
            $this->pays = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PAYS;
        }


        return $this;
    } // setPays()

    /**
     * Set the value of [affichage_entite] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAffichageEntite($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->affichage_entite !== $v) {
            $this->affichage_entite = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::AFFICHAGE_ENTITE;
        }


        return $this;
    } // setAffichageEntite()

    /**
     * Set the value of [id_initial] column.
     *
     * @param int $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setIdInitial($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_initial !== $v) {
            $this->id_initial = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ID_INITIAL;
        }


        return $this;
    } // setIdInitial()

    /**
     * Set the value of [denomination_org_ar] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDenominationOrgAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination_org_ar !== $v) {
            $this->denomination_org_ar = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DENOMINATION_ORG_AR;
        }


        return $this;
    } // setDenominationOrgAr()

    /**
     * Set the value of [description_org_ar] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDescriptionOrgAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description_org_ar !== $v) {
            $this->description_org_ar = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DESCRIPTION_ORG_AR;
        }


        return $this;
    } // setDescriptionOrgAr()

    /**
     * Set the value of [adresse_ar] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresseAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_ar !== $v) {
            $this->adresse_ar = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE_AR;
        }


        return $this;
    } // setAdresseAr()

    /**
     * Set the value of [ville_ar] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setVilleAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_ar !== $v) {
            $this->ville_ar = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::VILLE_AR;
        }


        return $this;
    } // setVilleAr()

    /**
     * Set the value of [adresse2_ar] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse2Ar($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_ar !== $v) {
            $this->adresse2_ar = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE2_AR;
        }


        return $this;
    } // setAdresse2Ar()

    /**
     * Set the value of [pays_ar] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPaysAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_ar !== $v) {
            $this->pays_ar = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PAYS_AR;
        }


        return $this;
    } // setPaysAr()

    /**
     * Set the value of [denomination_org_fr] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDenominationOrgFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination_org_fr !== $v) {
            $this->denomination_org_fr = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DENOMINATION_ORG_FR;
        }


        return $this;
    } // setDenominationOrgFr()

    /**
     * Set the value of [description_org_fr] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDescriptionOrgFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description_org_fr !== $v) {
            $this->description_org_fr = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DESCRIPTION_ORG_FR;
        }


        return $this;
    } // setDescriptionOrgFr()

    /**
     * Set the value of [adresse_fr] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresseFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_fr !== $v) {
            $this->adresse_fr = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE_FR;
        }


        return $this;
    } // setAdresseFr()

    /**
     * Set the value of [ville_fr] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setVilleFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_fr !== $v) {
            $this->ville_fr = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::VILLE_FR;
        }


        return $this;
    } // setVilleFr()

    /**
     * Set the value of [adresse2_fr] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse2Fr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_fr !== $v) {
            $this->adresse2_fr = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE2_FR;
        }


        return $this;
    } // setAdresse2Fr()

    /**
     * Set the value of [pays_fr] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPaysFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_fr !== $v) {
            $this->pays_fr = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PAYS_FR;
        }


        return $this;
    } // setPaysFr()

    /**
     * Set the value of [denomination_org_es] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDenominationOrgEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination_org_es !== $v) {
            $this->denomination_org_es = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DENOMINATION_ORG_ES;
        }


        return $this;
    } // setDenominationOrgEs()

    /**
     * Set the value of [description_org_es] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDescriptionOrgEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description_org_es !== $v) {
            $this->description_org_es = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DESCRIPTION_ORG_ES;
        }


        return $this;
    } // setDescriptionOrgEs()

    /**
     * Set the value of [adresse_es] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresseEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_es !== $v) {
            $this->adresse_es = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE_ES;
        }


        return $this;
    } // setAdresseEs()

    /**
     * Set the value of [ville_es] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setVilleEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_es !== $v) {
            $this->ville_es = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::VILLE_ES;
        }


        return $this;
    } // setVilleEs()

    /**
     * Set the value of [adresse2_es] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse2Es($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_es !== $v) {
            $this->adresse2_es = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE2_ES;
        }


        return $this;
    } // setAdresse2Es()

    /**
     * Set the value of [pays_es] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPaysEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_es !== $v) {
            $this->pays_es = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PAYS_ES;
        }


        return $this;
    } // setPaysEs()

    /**
     * Set the value of [denomination_org_en] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDenominationOrgEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination_org_en !== $v) {
            $this->denomination_org_en = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DENOMINATION_ORG_EN;
        }


        return $this;
    } // setDenominationOrgEn()

    /**
     * Set the value of [description_org_en] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDescriptionOrgEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description_org_en !== $v) {
            $this->description_org_en = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DESCRIPTION_ORG_EN;
        }


        return $this;
    } // setDescriptionOrgEn()

    /**
     * Set the value of [adresse_en] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresseEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_en !== $v) {
            $this->adresse_en = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE_EN;
        }


        return $this;
    } // setAdresseEn()

    /**
     * Set the value of [ville_en] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setVilleEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_en !== $v) {
            $this->ville_en = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::VILLE_EN;
        }


        return $this;
    } // setVilleEn()

    /**
     * Set the value of [adresse2_en] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse2En($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_en !== $v) {
            $this->adresse2_en = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE2_EN;
        }


        return $this;
    } // setAdresse2En()

    /**
     * Set the value of [pays_en] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPaysEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_en !== $v) {
            $this->pays_en = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PAYS_EN;
        }


        return $this;
    } // setPaysEn()

    /**
     * Set the value of [denomination_org_su] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDenominationOrgSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination_org_su !== $v) {
            $this->denomination_org_su = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DENOMINATION_ORG_SU;
        }


        return $this;
    } // setDenominationOrgSu()

    /**
     * Set the value of [description_org_su] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDescriptionOrgSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description_org_su !== $v) {
            $this->description_org_su = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DESCRIPTION_ORG_SU;
        }


        return $this;
    } // setDescriptionOrgSu()

    /**
     * Set the value of [adresse_su] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresseSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_su !== $v) {
            $this->adresse_su = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE_SU;
        }


        return $this;
    } // setAdresseSu()

    /**
     * Set the value of [ville_su] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setVilleSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_su !== $v) {
            $this->ville_su = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::VILLE_SU;
        }


        return $this;
    } // setVilleSu()

    /**
     * Set the value of [adresse2_su] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse2Su($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_su !== $v) {
            $this->adresse2_su = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE2_SU;
        }


        return $this;
    } // setAdresse2Su()

    /**
     * Set the value of [pays_su] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPaysSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_su !== $v) {
            $this->pays_su = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PAYS_SU;
        }


        return $this;
    } // setPaysSu()

    /**
     * Set the value of [denomination_org_du] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDenominationOrgDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination_org_du !== $v) {
            $this->denomination_org_du = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DENOMINATION_ORG_DU;
        }


        return $this;
    } // setDenominationOrgDu()

    /**
     * Set the value of [description_org_du] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDescriptionOrgDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description_org_du !== $v) {
            $this->description_org_du = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DESCRIPTION_ORG_DU;
        }


        return $this;
    } // setDescriptionOrgDu()

    /**
     * Set the value of [adresse_du] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresseDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_du !== $v) {
            $this->adresse_du = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE_DU;
        }


        return $this;
    } // setAdresseDu()

    /**
     * Set the value of [ville_du] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setVilleDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_du !== $v) {
            $this->ville_du = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::VILLE_DU;
        }


        return $this;
    } // setVilleDu()

    /**
     * Set the value of [adresse2_du] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse2Du($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_du !== $v) {
            $this->adresse2_du = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE2_DU;
        }


        return $this;
    } // setAdresse2Du()

    /**
     * Set the value of [pays_du] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPaysDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_du !== $v) {
            $this->pays_du = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PAYS_DU;
        }


        return $this;
    } // setPaysDu()

    /**
     * Set the value of [denomination_org_cz] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDenominationOrgCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination_org_cz !== $v) {
            $this->denomination_org_cz = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DENOMINATION_ORG_CZ;
        }


        return $this;
    } // setDenominationOrgCz()

    /**
     * Set the value of [description_org_cz] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDescriptionOrgCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description_org_cz !== $v) {
            $this->description_org_cz = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DESCRIPTION_ORG_CZ;
        }


        return $this;
    } // setDescriptionOrgCz()

    /**
     * Set the value of [adresse_cz] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresseCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_cz !== $v) {
            $this->adresse_cz = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE_CZ;
        }


        return $this;
    } // setAdresseCz()

    /**
     * Set the value of [ville_cz] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setVilleCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_cz !== $v) {
            $this->ville_cz = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::VILLE_CZ;
        }


        return $this;
    } // setVilleCz()

    /**
     * Set the value of [adresse2_cz] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse2Cz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_cz !== $v) {
            $this->adresse2_cz = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE2_CZ;
        }


        return $this;
    } // setAdresse2Cz()

    /**
     * Set the value of [pays_cz] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPaysCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_cz !== $v) {
            $this->pays_cz = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PAYS_CZ;
        }


        return $this;
    } // setPaysCz()

    /**
     * Set the value of [denomination_org_it] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDenominationOrgIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination_org_it !== $v) {
            $this->denomination_org_it = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DENOMINATION_ORG_IT;
        }


        return $this;
    } // setDenominationOrgIt()

    /**
     * Set the value of [description_org_it] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDescriptionOrgIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description_org_it !== $v) {
            $this->description_org_it = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DESCRIPTION_ORG_IT;
        }


        return $this;
    } // setDescriptionOrgIt()

    /**
     * Set the value of [adresse_it] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresseIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_it !== $v) {
            $this->adresse_it = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE_IT;
        }


        return $this;
    } // setAdresseIt()

    /**
     * Set the value of [ville_it] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setVilleIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_it !== $v) {
            $this->ville_it = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::VILLE_IT;
        }


        return $this;
    } // setVilleIt()

    /**
     * Set the value of [adresse2_it] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAdresse2It($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_it !== $v) {
            $this->adresse2_it = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ADRESSE2_IT;
        }


        return $this;
    } // setAdresse2It()

    /**
     * Set the value of [pays_it] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPaysIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_it !== $v) {
            $this->pays_it = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PAYS_IT;
        }


        return $this;
    } // setPaysIt()

    /**
     * Set the value of [siren] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setSiren($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siren !== $v) {
            $this->siren = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::SIREN;
        }


        return $this;
    } // setSiren()

    /**
     * Set the value of [complement] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setComplement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->complement !== $v) {
            $this->complement = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::COMPLEMENT;
        }


        return $this;
    } // setComplement()

    /**
     * Set the value of [moniteur_provenance] column.
     *
     * @param int $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setMoniteurProvenance($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->moniteur_provenance !== $v) {
            $this->moniteur_provenance = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::MONITEUR_PROVENANCE;
        }


        return $this;
    } // setMoniteurProvenance()

    /**
     * Set the value of [code_acces_logiciel] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCodeAccesLogiciel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_acces_logiciel !== $v) {
            $this->code_acces_logiciel = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::CODE_ACCES_LOGICIEL;
        }


        return $this;
    } // setCodeAccesLogiciel()

    /**
     * Set the value of [decalage_horaire] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setDecalageHoraire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->decalage_horaire !== $v) {
            $this->decalage_horaire = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::DECALAGE_HORAIRE;
        }


        return $this;
    } // setDecalageHoraire()

    /**
     * Set the value of [lieu_residence] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setLieuResidence($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lieu_residence !== $v) {
            $this->lieu_residence = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::LIEU_RESIDENCE;
        }


        return $this;
    } // setLieuResidence()

    /**
     * Set the value of [activation_fuseau_horaire] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setActivationFuseauHoraire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->activation_fuseau_horaire !== $v) {
            $this->activation_fuseau_horaire = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ACTIVATION_FUSEAU_HORAIRE;
        }


        return $this;
    } // setActivationFuseauHoraire()

    /**
     * Set the value of [alerte] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setAlerte($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte !== $v) {
            $this->alerte = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ALERTE;
        }


        return $this;
    } // setAlerte()

    /**
     * Set the value of [ordre] column.
     *
     * @param int $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setOrdre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ordre !== $v) {
            $this->ordre = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ORDRE;
        }


        return $this;
    } // setOrdre()

    /**
     * Set the value of [url_interface_anm] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setUrlInterfaceAnm($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->url_interface_anm !== $v) {
            $this->url_interface_anm = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::URL_INTERFACE_ANM;
        }


        return $this;
    } // setUrlInterfaceAnm()

    /**
     * Set the value of [sous_type_organisme] column.
     *
     * @param int $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setSousTypeOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->sous_type_organisme !== $v) {
            $this->sous_type_organisme = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::SOUS_TYPE_ORGANISME;
        }


        return $this;
    } // setSousTypeOrganisme()

    /**
     * Set the value of [pf_url] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setPfUrl($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pf_url !== $v) {
            $this->pf_url = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::PF_URL;
        }


        return $this;
    } // setPfUrl()

    /**
     * Sets the value of the [tag_purge] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setTagPurge($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->tag_purge !== $v) {
            $this->tag_purge = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::TAG_PURGE;
        }


        return $this;
    } // setTagPurge()

    /**
     * Set the value of [id_externe] column.
     *
     * @param string $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setIdExterne($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_externe !== $v) {
            $this->id_externe = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ID_EXTERNE;
        }


        return $this;
    } // setIdExterne()

    /**
     * Set the value of [id_entite] column.
     *
     * @param int $v new value
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[] = CommonOrganismePeer::ID_ENTITE;
        }


        return $this;
    } // setIdEntite()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->acronyme !== '') {
                return false;
            }

            if ($this->type_article_org !== 0) {
                return false;
            }

            if ($this->adresse !== '') {
                return false;
            }

            if ($this->cp !== '') {
                return false;
            }

            if ($this->ville !== '') {
                return false;
            }

            if ($this->email !== '') {
                return false;
            }

            if ($this->url !== '') {
                return false;
            }

            if ($this->attrib_file !== '') {
                return false;
            }

            if ($this->date_creation !== '0000-00-00 00:00:00') {
                return false;
            }

            if ($this->active !== '1') {
                return false;
            }

            if ($this->id_client_anm !== '0') {
                return false;
            }

            if ($this->status !== '0') {
                return false;
            }

            if ($this->sigle !== '') {
                return false;
            }

            if ($this->adresse2 !== '') {
                return false;
            }

            if ($this->tel !== '') {
                return false;
            }

            if ($this->telecopie !== '') {
                return false;
            }

            if ($this->affichage_entite !== '') {
                return false;
            }

            if ($this->id_initial !== 0) {
                return false;
            }

            if ($this->denomination_org_ar !== '') {
                return false;
            }

            if ($this->adresse_ar !== '') {
                return false;
            }

            if ($this->ville_ar !== '') {
                return false;
            }

            if ($this->adresse2_ar !== '') {
                return false;
            }

            if ($this->pays_ar !== '') {
                return false;
            }

            if ($this->denomination_org_fr !== '') {
                return false;
            }

            if ($this->adresse_fr !== '') {
                return false;
            }

            if ($this->ville_fr !== '') {
                return false;
            }

            if ($this->adresse2_fr !== '') {
                return false;
            }

            if ($this->pays_fr !== '') {
                return false;
            }

            if ($this->denomination_org_es !== '') {
                return false;
            }

            if ($this->adresse_es !== '') {
                return false;
            }

            if ($this->ville_es !== '') {
                return false;
            }

            if ($this->adresse2_es !== '') {
                return false;
            }

            if ($this->pays_es !== '') {
                return false;
            }

            if ($this->denomination_org_en !== '') {
                return false;
            }

            if ($this->adresse_en !== '') {
                return false;
            }

            if ($this->ville_en !== '') {
                return false;
            }

            if ($this->adresse2_en !== '') {
                return false;
            }

            if ($this->pays_en !== '') {
                return false;
            }

            if ($this->denomination_org_su !== '') {
                return false;
            }

            if ($this->adresse_su !== '') {
                return false;
            }

            if ($this->ville_su !== '') {
                return false;
            }

            if ($this->adresse2_su !== '') {
                return false;
            }

            if ($this->pays_su !== '') {
                return false;
            }

            if ($this->denomination_org_du !== '') {
                return false;
            }

            if ($this->adresse_du !== '') {
                return false;
            }

            if ($this->ville_du !== '') {
                return false;
            }

            if ($this->adresse2_du !== '') {
                return false;
            }

            if ($this->pays_du !== '') {
                return false;
            }

            if ($this->denomination_org_cz !== '') {
                return false;
            }

            if ($this->adresse_cz !== '') {
                return false;
            }

            if ($this->ville_cz !== '') {
                return false;
            }

            if ($this->adresse2_cz !== '') {
                return false;
            }

            if ($this->pays_cz !== '') {
                return false;
            }

            if ($this->denomination_org_it !== '') {
                return false;
            }

            if ($this->adresse_it !== '') {
                return false;
            }

            if ($this->ville_it !== '') {
                return false;
            }

            if ($this->adresse2_it !== '') {
                return false;
            }

            if ($this->pays_it !== '') {
                return false;
            }

            if ($this->activation_fuseau_horaire !== '0') {
                return false;
            }

            if ($this->alerte !== '0') {
                return false;
            }

            if ($this->ordre !== 0) {
                return false;
            }

            if ($this->sous_type_organisme !== 2) {
                return false;
            }

            if ($this->tag_purge !== false) {
                return false;
            }

            if ($this->id_externe !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->acronyme = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->type_article_org = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->denomination_org = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->categorie_insee = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->description_org = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->adresse = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->cp = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->ville = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->email = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->url = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->id_attrib_file = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->attrib_file = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->date_creation = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->active = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->id_client_anm = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->status = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->signataire_cao = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->sigle = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->adresse2 = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->tel = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->telecopie = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->pays = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->affichage_entite = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->id_initial = ($row[$startcol + 24] !== null) ? (int) $row[$startcol + 24] : null;
            $this->denomination_org_ar = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->description_org_ar = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->adresse_ar = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->ville_ar = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->adresse2_ar = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->pays_ar = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->denomination_org_fr = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->description_org_fr = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->adresse_fr = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->ville_fr = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->adresse2_fr = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->pays_fr = ($row[$startcol + 36] !== null) ? (string) $row[$startcol + 36] : null;
            $this->denomination_org_es = ($row[$startcol + 37] !== null) ? (string) $row[$startcol + 37] : null;
            $this->description_org_es = ($row[$startcol + 38] !== null) ? (string) $row[$startcol + 38] : null;
            $this->adresse_es = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->ville_es = ($row[$startcol + 40] !== null) ? (string) $row[$startcol + 40] : null;
            $this->adresse2_es = ($row[$startcol + 41] !== null) ? (string) $row[$startcol + 41] : null;
            $this->pays_es = ($row[$startcol + 42] !== null) ? (string) $row[$startcol + 42] : null;
            $this->denomination_org_en = ($row[$startcol + 43] !== null) ? (string) $row[$startcol + 43] : null;
            $this->description_org_en = ($row[$startcol + 44] !== null) ? (string) $row[$startcol + 44] : null;
            $this->adresse_en = ($row[$startcol + 45] !== null) ? (string) $row[$startcol + 45] : null;
            $this->ville_en = ($row[$startcol + 46] !== null) ? (string) $row[$startcol + 46] : null;
            $this->adresse2_en = ($row[$startcol + 47] !== null) ? (string) $row[$startcol + 47] : null;
            $this->pays_en = ($row[$startcol + 48] !== null) ? (string) $row[$startcol + 48] : null;
            $this->denomination_org_su = ($row[$startcol + 49] !== null) ? (string) $row[$startcol + 49] : null;
            $this->description_org_su = ($row[$startcol + 50] !== null) ? (string) $row[$startcol + 50] : null;
            $this->adresse_su = ($row[$startcol + 51] !== null) ? (string) $row[$startcol + 51] : null;
            $this->ville_su = ($row[$startcol + 52] !== null) ? (string) $row[$startcol + 52] : null;
            $this->adresse2_su = ($row[$startcol + 53] !== null) ? (string) $row[$startcol + 53] : null;
            $this->pays_su = ($row[$startcol + 54] !== null) ? (string) $row[$startcol + 54] : null;
            $this->denomination_org_du = ($row[$startcol + 55] !== null) ? (string) $row[$startcol + 55] : null;
            $this->description_org_du = ($row[$startcol + 56] !== null) ? (string) $row[$startcol + 56] : null;
            $this->adresse_du = ($row[$startcol + 57] !== null) ? (string) $row[$startcol + 57] : null;
            $this->ville_du = ($row[$startcol + 58] !== null) ? (string) $row[$startcol + 58] : null;
            $this->adresse2_du = ($row[$startcol + 59] !== null) ? (string) $row[$startcol + 59] : null;
            $this->pays_du = ($row[$startcol + 60] !== null) ? (string) $row[$startcol + 60] : null;
            $this->denomination_org_cz = ($row[$startcol + 61] !== null) ? (string) $row[$startcol + 61] : null;
            $this->description_org_cz = ($row[$startcol + 62] !== null) ? (string) $row[$startcol + 62] : null;
            $this->adresse_cz = ($row[$startcol + 63] !== null) ? (string) $row[$startcol + 63] : null;
            $this->ville_cz = ($row[$startcol + 64] !== null) ? (string) $row[$startcol + 64] : null;
            $this->adresse2_cz = ($row[$startcol + 65] !== null) ? (string) $row[$startcol + 65] : null;
            $this->pays_cz = ($row[$startcol + 66] !== null) ? (string) $row[$startcol + 66] : null;
            $this->denomination_org_it = ($row[$startcol + 67] !== null) ? (string) $row[$startcol + 67] : null;
            $this->description_org_it = ($row[$startcol + 68] !== null) ? (string) $row[$startcol + 68] : null;
            $this->adresse_it = ($row[$startcol + 69] !== null) ? (string) $row[$startcol + 69] : null;
            $this->ville_it = ($row[$startcol + 70] !== null) ? (string) $row[$startcol + 70] : null;
            $this->adresse2_it = ($row[$startcol + 71] !== null) ? (string) $row[$startcol + 71] : null;
            $this->pays_it = ($row[$startcol + 72] !== null) ? (string) $row[$startcol + 72] : null;
            $this->siren = ($row[$startcol + 73] !== null) ? (string) $row[$startcol + 73] : null;
            $this->complement = ($row[$startcol + 74] !== null) ? (string) $row[$startcol + 74] : null;
            $this->moniteur_provenance = ($row[$startcol + 75] !== null) ? (int) $row[$startcol + 75] : null;
            $this->code_acces_logiciel = ($row[$startcol + 76] !== null) ? (string) $row[$startcol + 76] : null;
            $this->decalage_horaire = ($row[$startcol + 77] !== null) ? (string) $row[$startcol + 77] : null;
            $this->lieu_residence = ($row[$startcol + 78] !== null) ? (string) $row[$startcol + 78] : null;
            $this->activation_fuseau_horaire = ($row[$startcol + 79] !== null) ? (string) $row[$startcol + 79] : null;
            $this->alerte = ($row[$startcol + 80] !== null) ? (string) $row[$startcol + 80] : null;
            $this->ordre = ($row[$startcol + 81] !== null) ? (int) $row[$startcol + 81] : null;
            $this->url_interface_anm = ($row[$startcol + 82] !== null) ? (string) $row[$startcol + 82] : null;
            $this->sous_type_organisme = ($row[$startcol + 83] !== null) ? (int) $row[$startcol + 83] : null;
            $this->pf_url = ($row[$startcol + 84] !== null) ? (string) $row[$startcol + 84] : null;
            $this->tag_purge = ($row[$startcol + 85] !== null) ? (boolean) $row[$startcol + 85] : null;
            $this->id_externe = ($row[$startcol + 86] !== null) ? (string) $row[$startcol + 86] : null;
            $this->id_entite = ($row[$startcol + 87] !== null) ? (int) $row[$startcol + 87] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 88; // 88 = CommonOrganismePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonOrganisme object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonCategorieINSEE !== null && $this->categorie_insee !== $this->aCommonCategorieINSEE->getId()) {
            $this->aCommonCategorieINSEE = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonOrganismePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonCategorieINSEE = null;
            $this->collCommonAdministrateurs = null;

            $this->collCommonAffiliationServices = null;

            $this->collCommonAgents = null;

            $this->collCommonEchanges = null;

            $this->collCommonOrganismeServiceMetiers = null;

            $this->collCommonParametrageEncheres = null;

            $this->collCommonRPAs = null;

            $this->collCommonTelechargements = null;

            $this->collCommonTierss = null;

            $this->singleCommonConfigurationOrganisme = null;

            $this->collCommonConsultations = null;

            $this->collCommonDestinataireMiseDispositions = null;

            $this->collCommonEchangeDocApplicationClientOrganismes = null;

            $this->collCommonInvitePermanentTransverses = null;

            $this->collCommonPlateformeVirtuelleOrganismes = null;

            $this->collCommonTCAOCommissions = null;

            $this->collCommonTCAOCommissionAgents = null;

            $this->collCommonTCAOCommissionIntervenantExternes = null;

            $this->collCommonTCAOIntervenantExternes = null;

            $this->collCommonTCAOOrdreDuJours = null;

            $this->collCommonTCAOOrdreDuJourIntervenants = null;

            $this->collCommonTCAOSeances = null;

            $this->collCommonTCAOSeanceAgents = null;

            $this->collCommonTCAOSeanceIntervenantExternes = null;

            $this->collCommonTCandidatures = null;

            $this->collCommonTDumeContextes = null;

            $this->collCommonTListeLotsCandidatures = null;

            $this->collCommonTVisionRmaAgentOrganismes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonOrganismePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonOrganismeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonOrganismePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonOrganismePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonCategorieINSEE !== null) {
                if ($this->aCommonCategorieINSEE->isModified() || $this->aCommonCategorieINSEE->isNew()) {
                    $affectedRows += $this->aCommonCategorieINSEE->save($con);
                }
                $this->setCommonCategorieINSEE($this->aCommonCategorieINSEE);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonAdministrateursScheduledForDeletion !== null) {
                if (!$this->commonAdministrateursScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonAdministrateursScheduledForDeletion as $commonAdministrateur) {
                        // need to save related object because we set the relation to null
                        $commonAdministrateur->save($con);
                    }
                    $this->commonAdministrateursScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAdministrateurs !== null) {
                foreach ($this->collCommonAdministrateurs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonAffiliationServicesScheduledForDeletion !== null) {
                if (!$this->commonAffiliationServicesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonAffiliationServicesScheduledForDeletion as $commonAffiliationService) {
                        // need to save related object because we set the relation to null
                        $commonAffiliationService->save($con);
                    }
                    $this->commonAffiliationServicesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAffiliationServices !== null) {
                foreach ($this->collCommonAffiliationServices as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonAgentsScheduledForDeletion !== null) {
                if (!$this->commonAgentsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonAgentsScheduledForDeletion as $commonAgent) {
                        // need to save related object because we set the relation to null
                        $commonAgent->save($con);
                    }
                    $this->commonAgentsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAgents !== null) {
                foreach ($this->collCommonAgents as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEchangesScheduledForDeletion !== null) {
                if (!$this->commonEchangesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangesScheduledForDeletion as $commonEchange) {
                        // need to save related object because we set the relation to null
                        $commonEchange->save($con);
                    }
                    $this->commonEchangesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchanges !== null) {
                foreach ($this->collCommonEchanges as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonOrganismeServiceMetiersScheduledForDeletion !== null) {
                if (!$this->commonOrganismeServiceMetiersScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonOrganismeServiceMetiersScheduledForDeletion as $commonOrganismeServiceMetier) {
                        // need to save related object because we set the relation to null
                        $commonOrganismeServiceMetier->save($con);
                    }
                    $this->commonOrganismeServiceMetiersScheduledForDeletion = null;
                }
            }

            if ($this->collCommonOrganismeServiceMetiers !== null) {
                foreach ($this->collCommonOrganismeServiceMetiers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonParametrageEncheresScheduledForDeletion !== null) {
                if (!$this->commonParametrageEncheresScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonParametrageEncheresScheduledForDeletion as $commonParametrageEnchere) {
                        // need to save related object because we set the relation to null
                        $commonParametrageEnchere->save($con);
                    }
                    $this->commonParametrageEncheresScheduledForDeletion = null;
                }
            }

            if ($this->collCommonParametrageEncheres !== null) {
                foreach ($this->collCommonParametrageEncheres as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonRPAsScheduledForDeletion !== null) {
                if (!$this->commonRPAsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonRPAsScheduledForDeletion as $commonRPA) {
                        // need to save related object because we set the relation to null
                        $commonRPA->save($con);
                    }
                    $this->commonRPAsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonRPAs !== null) {
                foreach ($this->collCommonRPAs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTelechargementsScheduledForDeletion !== null) {
                if (!$this->commonTelechargementsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTelechargementsScheduledForDeletion as $commonTelechargement) {
                        // need to save related object because we set the relation to null
                        $commonTelechargement->save($con);
                    }
                    $this->commonTelechargementsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTelechargements !== null) {
                foreach ($this->collCommonTelechargements as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTierssScheduledForDeletion !== null) {
                if (!$this->commonTierssScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTierssScheduledForDeletion as $commonTiers) {
                        // need to save related object because we set the relation to null
                        $commonTiers->save($con);
                    }
                    $this->commonTierssScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTierss !== null) {
                foreach ($this->collCommonTierss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonConfigurationOrganismesScheduledForDeletion !== null) {
                if (!$this->commonConfigurationOrganismesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConfigurationOrganismesScheduledForDeletion as $commonConfigurationOrganisme) {
                        // need to save related object because we set the relation to null
                        $commonConfigurationOrganisme->save($con);
                    }
                    $this->commonConfigurationOrganismesScheduledForDeletion = null;
                }
            }

            if ($this->singleCommonConfigurationOrganisme !== null) {
                if (!$this->singleCommonConfigurationOrganisme->isDeleted() && ($this->singleCommonConfigurationOrganisme->isNew() || $this->singleCommonConfigurationOrganisme->isModified())) {
                        $affectedRows += $this->singleCommonConfigurationOrganisme->save($con);
                }
            }

            if ($this->commonConsultationsScheduledForDeletion !== null) {
                if (!$this->commonConsultationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationsScheduledForDeletion as $commonConsultation) {
                        // need to save related object because we set the relation to null
                        $commonConsultation->save($con);
                    }
                    $this->commonConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultations !== null) {
                foreach ($this->collCommonConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonDestinataireMiseDispositionsScheduledForDeletion !== null) {
                if (!$this->commonDestinataireMiseDispositionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonDestinataireMiseDispositionsScheduledForDeletion as $commonDestinataireMiseDisposition) {
                        // need to save related object because we set the relation to null
                        $commonDestinataireMiseDisposition->save($con);
                    }
                    $this->commonDestinataireMiseDispositionsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonDestinataireMiseDispositions !== null) {
                foreach ($this->collCommonDestinataireMiseDispositions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion as $commonEchangeDocApplicationClientOrganisme) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDocApplicationClientOrganisme->save($con);
                    }
                    $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocApplicationClientOrganismes !== null) {
                foreach ($this->collCommonEchangeDocApplicationClientOrganismes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonInvitePermanentTransversesScheduledForDeletion !== null) {
                if (!$this->commonInvitePermanentTransversesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonInvitePermanentTransversesScheduledForDeletion as $commonInvitePermanentTransverse) {
                        // need to save related object because we set the relation to null
                        $commonInvitePermanentTransverse->save($con);
                    }
                    $this->commonInvitePermanentTransversesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonInvitePermanentTransverses !== null) {
                foreach ($this->collCommonInvitePermanentTransverses as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonPlateformeVirtuelleOrganismesScheduledForDeletion !== null) {
                if (!$this->commonPlateformeVirtuelleOrganismesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonPlateformeVirtuelleOrganismesScheduledForDeletion as $commonPlateformeVirtuelleOrganisme) {
                        // need to save related object because we set the relation to null
                        $commonPlateformeVirtuelleOrganisme->save($con);
                    }
                    $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonPlateformeVirtuelleOrganismes !== null) {
                foreach ($this->collCommonPlateformeVirtuelleOrganismes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOCommissionsScheduledForDeletion !== null) {
                if (!$this->commonTCAOCommissionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOCommissionsScheduledForDeletion as $commonTCAOCommission) {
                        // need to save related object because we set the relation to null
                        $commonTCAOCommission->save($con);
                    }
                    $this->commonTCAOCommissionsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOCommissions !== null) {
                foreach ($this->collCommonTCAOCommissions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOCommissionAgentsScheduledForDeletion !== null) {
                if (!$this->commonTCAOCommissionAgentsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOCommissionAgentsScheduledForDeletion as $commonTCAOCommissionAgent) {
                        // need to save related object because we set the relation to null
                        $commonTCAOCommissionAgent->save($con);
                    }
                    $this->commonTCAOCommissionAgentsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOCommissionAgents !== null) {
                foreach ($this->collCommonTCAOCommissionAgents as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOCommissionIntervenantExternesScheduledForDeletion !== null) {
                if (!$this->commonTCAOCommissionIntervenantExternesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOCommissionIntervenantExternesScheduledForDeletion as $commonTCAOCommissionIntervenantExterne) {
                        // need to save related object because we set the relation to null
                        $commonTCAOCommissionIntervenantExterne->save($con);
                    }
                    $this->commonTCAOCommissionIntervenantExternesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOCommissionIntervenantExternes !== null) {
                foreach ($this->collCommonTCAOCommissionIntervenantExternes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOIntervenantExternesScheduledForDeletion !== null) {
                if (!$this->commonTCAOIntervenantExternesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOIntervenantExternesScheduledForDeletion as $commonTCAOIntervenantExterne) {
                        // need to save related object because we set the relation to null
                        $commonTCAOIntervenantExterne->save($con);
                    }
                    $this->commonTCAOIntervenantExternesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOIntervenantExternes !== null) {
                foreach ($this->collCommonTCAOIntervenantExternes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOOrdreDuJoursScheduledForDeletion !== null) {
                if (!$this->commonTCAOOrdreDuJoursScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOOrdreDuJoursScheduledForDeletion as $commonTCAOOrdreDuJour) {
                        // need to save related object because we set the relation to null
                        $commonTCAOOrdreDuJour->save($con);
                    }
                    $this->commonTCAOOrdreDuJoursScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOOrdreDuJours !== null) {
                foreach ($this->collCommonTCAOOrdreDuJours as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion !== null) {
                if (!$this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion as $commonTCAOOrdreDuJourIntervenant) {
                        // need to save related object because we set the relation to null
                        $commonTCAOOrdreDuJourIntervenant->save($con);
                    }
                    $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOOrdreDuJourIntervenants !== null) {
                foreach ($this->collCommonTCAOOrdreDuJourIntervenants as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOSeancesScheduledForDeletion !== null) {
                if (!$this->commonTCAOSeancesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOSeancesScheduledForDeletion as $commonTCAOSeance) {
                        // need to save related object because we set the relation to null
                        $commonTCAOSeance->save($con);
                    }
                    $this->commonTCAOSeancesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOSeances !== null) {
                foreach ($this->collCommonTCAOSeances as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOSeanceAgentsScheduledForDeletion !== null) {
                if (!$this->commonTCAOSeanceAgentsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOSeanceAgentsScheduledForDeletion as $commonTCAOSeanceAgent) {
                        // need to save related object because we set the relation to null
                        $commonTCAOSeanceAgent->save($con);
                    }
                    $this->commonTCAOSeanceAgentsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOSeanceAgents !== null) {
                foreach ($this->collCommonTCAOSeanceAgents as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOSeanceIntervenantExternesScheduledForDeletion !== null) {
                if (!$this->commonTCAOSeanceIntervenantExternesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOSeanceIntervenantExternesScheduledForDeletion as $commonTCAOSeanceIntervenantExterne) {
                        // need to save related object because we set the relation to null
                        $commonTCAOSeanceIntervenantExterne->save($con);
                    }
                    $this->commonTCAOSeanceIntervenantExternesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOSeanceIntervenantExternes !== null) {
                foreach ($this->collCommonTCAOSeanceIntervenantExternes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCandidaturesScheduledForDeletion !== null) {
                if (!$this->commonTCandidaturesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCandidaturesScheduledForDeletion as $commonTCandidature) {
                        // need to save related object because we set the relation to null
                        $commonTCandidature->save($con);
                    }
                    $this->commonTCandidaturesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCandidatures !== null) {
                foreach ($this->collCommonTCandidatures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTDumeContextesScheduledForDeletion !== null) {
                if (!$this->commonTDumeContextesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTDumeContextesScheduledForDeletion as $commonTDumeContexte) {
                        // need to save related object because we set the relation to null
                        $commonTDumeContexte->save($con);
                    }
                    $this->commonTDumeContextesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTDumeContextes !== null) {
                foreach ($this->collCommonTDumeContextes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTListeLotsCandidaturesScheduledForDeletion !== null) {
                if (!$this->commonTListeLotsCandidaturesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTListeLotsCandidaturesScheduledForDeletion as $commonTListeLotsCandidature) {
                        // need to save related object because we set the relation to null
                        $commonTListeLotsCandidature->save($con);
                    }
                    $this->commonTListeLotsCandidaturesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTListeLotsCandidatures !== null) {
                foreach ($this->collCommonTListeLotsCandidatures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTVisionRmaAgentOrganismesScheduledForDeletion !== null) {
                if (!$this->commonTVisionRmaAgentOrganismesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTVisionRmaAgentOrganismesScheduledForDeletion as $commonTVisionRmaAgentOrganisme) {
                        // need to save related object because we set the relation to null
                        $commonTVisionRmaAgentOrganisme->save($con);
                    }
                    $this->commonTVisionRmaAgentOrganismesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTVisionRmaAgentOrganismes !== null) {
                foreach ($this->collCommonTVisionRmaAgentOrganismes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonOrganismePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonOrganismePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonOrganismePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ACRONYME)) {
            $modifiedColumns[':p' . $index++]  = '`acronyme`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::TYPE_ARTICLE_ORG)) {
            $modifiedColumns[':p' . $index++]  = '`type_article_org`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG)) {
            $modifiedColumns[':p' . $index++]  = '`denomination_org`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::CATEGORIE_INSEE)) {
            $modifiedColumns[':p' . $index++]  = '`categorie_insee`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG)) {
            $modifiedColumns[':p' . $index++]  = '`description_org`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`adresse`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::CP)) {
            $modifiedColumns[':p' . $index++]  = '`cp`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`ville`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::URL)) {
            $modifiedColumns[':p' . $index++]  = '`url`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ID_ATTRIB_FILE)) {
            $modifiedColumns[':p' . $index++]  = '`id_attrib_file`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ATTRIB_FILE)) {
            $modifiedColumns[':p' . $index++]  = '`attrib_file`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = '`active`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ID_CLIENT_ANM)) {
            $modifiedColumns[':p' . $index++]  = '`id_client_ANM`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`status`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::SIGNATAIRE_CAO)) {
            $modifiedColumns[':p' . $index++]  = '`signataire_cao`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::SIGLE)) {
            $modifiedColumns[':p' . $index++]  = '`sigle`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::TEL)) {
            $modifiedColumns[':p' . $index++]  = '`tel`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::TELECOPIE)) {
            $modifiedColumns[':p' . $index++]  = '`telecopie`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PAYS)) {
            $modifiedColumns[':p' . $index++]  = '`pays`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::AFFICHAGE_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`affichage_entite`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ID_INITIAL)) {
            $modifiedColumns[':p' . $index++]  = '`id_initial`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_AR)) {
            $modifiedColumns[':p' . $index++]  = '`denomination_org_ar`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_AR)) {
            $modifiedColumns[':p' . $index++]  = '`description_org_ar`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_AR)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_ar`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_AR)) {
            $modifiedColumns[':p' . $index++]  = '`ville_ar`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_AR)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_ar`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_AR)) {
            $modifiedColumns[':p' . $index++]  = '`pays_ar`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_FR)) {
            $modifiedColumns[':p' . $index++]  = '`denomination_org_fr`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_FR)) {
            $modifiedColumns[':p' . $index++]  = '`description_org_fr`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_FR)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_fr`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_FR)) {
            $modifiedColumns[':p' . $index++]  = '`ville_fr`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_FR)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_fr`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_FR)) {
            $modifiedColumns[':p' . $index++]  = '`pays_fr`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_ES)) {
            $modifiedColumns[':p' . $index++]  = '`denomination_org_es`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_ES)) {
            $modifiedColumns[':p' . $index++]  = '`description_org_es`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_ES)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_es`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_ES)) {
            $modifiedColumns[':p' . $index++]  = '`ville_es`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_ES)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_es`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_ES)) {
            $modifiedColumns[':p' . $index++]  = '`pays_es`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_EN)) {
            $modifiedColumns[':p' . $index++]  = '`denomination_org_en`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_EN)) {
            $modifiedColumns[':p' . $index++]  = '`description_org_en`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_EN)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_en`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_EN)) {
            $modifiedColumns[':p' . $index++]  = '`ville_en`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_EN)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_en`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_EN)) {
            $modifiedColumns[':p' . $index++]  = '`pays_en`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_SU)) {
            $modifiedColumns[':p' . $index++]  = '`denomination_org_su`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_SU)) {
            $modifiedColumns[':p' . $index++]  = '`description_org_su`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_SU)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_su`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_SU)) {
            $modifiedColumns[':p' . $index++]  = '`ville_su`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_SU)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_su`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_SU)) {
            $modifiedColumns[':p' . $index++]  = '`pays_su`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_DU)) {
            $modifiedColumns[':p' . $index++]  = '`denomination_org_du`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_DU)) {
            $modifiedColumns[':p' . $index++]  = '`description_org_du`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_DU)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_du`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_DU)) {
            $modifiedColumns[':p' . $index++]  = '`ville_du`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_DU)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_du`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_DU)) {
            $modifiedColumns[':p' . $index++]  = '`pays_du`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`denomination_org_cz`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`description_org_cz`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_cz`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`ville_cz`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_cz`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`pays_cz`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_IT)) {
            $modifiedColumns[':p' . $index++]  = '`denomination_org_it`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_IT)) {
            $modifiedColumns[':p' . $index++]  = '`description_org_it`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_IT)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_it`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_IT)) {
            $modifiedColumns[':p' . $index++]  = '`ville_it`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_IT)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_it`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_IT)) {
            $modifiedColumns[':p' . $index++]  = '`pays_it`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::SIREN)) {
            $modifiedColumns[':p' . $index++]  = '`siren`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::COMPLEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`complement`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::MONITEUR_PROVENANCE)) {
            $modifiedColumns[':p' . $index++]  = '`moniteur_provenance`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::CODE_ACCES_LOGICIEL)) {
            $modifiedColumns[':p' . $index++]  = '`code_acces_logiciel`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::DECALAGE_HORAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`decalage_horaire`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::LIEU_RESIDENCE)) {
            $modifiedColumns[':p' . $index++]  = '`lieu_residence`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ACTIVATION_FUSEAU_HORAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`activation_fuseau_horaire`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ALERTE)) {
            $modifiedColumns[':p' . $index++]  = '`alerte`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ORDRE)) {
            $modifiedColumns[':p' . $index++]  = '`ordre`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::URL_INTERFACE_ANM)) {
            $modifiedColumns[':p' . $index++]  = '`URL_INTERFACE_ANM`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::SOUS_TYPE_ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`sous_type_organisme`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::PF_URL)) {
            $modifiedColumns[':p' . $index++]  = '`pf_url`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::TAG_PURGE)) {
            $modifiedColumns[':p' . $index++]  = '`tag_purge`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ID_EXTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`id_externe`';
        }
        if ($this->isColumnModified(CommonOrganismePeer::ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }

        $sql = sprintf(
            'INSERT INTO `Organisme` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`acronyme`':
                        $stmt->bindValue($identifier, $this->acronyme, PDO::PARAM_STR);
                        break;
                    case '`type_article_org`':
                        $stmt->bindValue($identifier, $this->type_article_org, PDO::PARAM_INT);
                        break;
                    case '`denomination_org`':
                        $stmt->bindValue($identifier, $this->denomination_org, PDO::PARAM_STR);
                        break;
                    case '`categorie_insee`':
                        $stmt->bindValue($identifier, $this->categorie_insee, PDO::PARAM_STR);
                        break;
                    case '`description_org`':
                        $stmt->bindValue($identifier, $this->description_org, PDO::PARAM_STR);
                        break;
                    case '`adresse`':
                        $stmt->bindValue($identifier, $this->adresse, PDO::PARAM_STR);
                        break;
                    case '`cp`':
                        $stmt->bindValue($identifier, $this->cp, PDO::PARAM_STR);
                        break;
                    case '`ville`':
                        $stmt->bindValue($identifier, $this->ville, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`url`':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case '`id_attrib_file`':
                        $stmt->bindValue($identifier, $this->id_attrib_file, PDO::PARAM_STR);
                        break;
                    case '`attrib_file`':
                        $stmt->bindValue($identifier, $this->attrib_file, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`active`':
                        $stmt->bindValue($identifier, $this->active, PDO::PARAM_STR);
                        break;
                    case '`id_client_ANM`':
                        $stmt->bindValue($identifier, $this->id_client_anm, PDO::PARAM_STR);
                        break;
                    case '`status`':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case '`signataire_cao`':
                        $stmt->bindValue($identifier, $this->signataire_cao, PDO::PARAM_STR);
                        break;
                    case '`sigle`':
                        $stmt->bindValue($identifier, $this->sigle, PDO::PARAM_STR);
                        break;
                    case '`adresse2`':
                        $stmt->bindValue($identifier, $this->adresse2, PDO::PARAM_STR);
                        break;
                    case '`tel`':
                        $stmt->bindValue($identifier, $this->tel, PDO::PARAM_STR);
                        break;
                    case '`telecopie`':
                        $stmt->bindValue($identifier, $this->telecopie, PDO::PARAM_STR);
                        break;
                    case '`pays`':
                        $stmt->bindValue($identifier, $this->pays, PDO::PARAM_STR);
                        break;
                    case '`affichage_entite`':
                        $stmt->bindValue($identifier, $this->affichage_entite, PDO::PARAM_STR);
                        break;
                    case '`id_initial`':
                        $stmt->bindValue($identifier, $this->id_initial, PDO::PARAM_INT);
                        break;
                    case '`denomination_org_ar`':
                        $stmt->bindValue($identifier, $this->denomination_org_ar, PDO::PARAM_STR);
                        break;
                    case '`description_org_ar`':
                        $stmt->bindValue($identifier, $this->description_org_ar, PDO::PARAM_STR);
                        break;
                    case '`adresse_ar`':
                        $stmt->bindValue($identifier, $this->adresse_ar, PDO::PARAM_STR);
                        break;
                    case '`ville_ar`':
                        $stmt->bindValue($identifier, $this->ville_ar, PDO::PARAM_STR);
                        break;
                    case '`adresse2_ar`':
                        $stmt->bindValue($identifier, $this->adresse2_ar, PDO::PARAM_STR);
                        break;
                    case '`pays_ar`':
                        $stmt->bindValue($identifier, $this->pays_ar, PDO::PARAM_STR);
                        break;
                    case '`denomination_org_fr`':
                        $stmt->bindValue($identifier, $this->denomination_org_fr, PDO::PARAM_STR);
                        break;
                    case '`description_org_fr`':
                        $stmt->bindValue($identifier, $this->description_org_fr, PDO::PARAM_STR);
                        break;
                    case '`adresse_fr`':
                        $stmt->bindValue($identifier, $this->adresse_fr, PDO::PARAM_STR);
                        break;
                    case '`ville_fr`':
                        $stmt->bindValue($identifier, $this->ville_fr, PDO::PARAM_STR);
                        break;
                    case '`adresse2_fr`':
                        $stmt->bindValue($identifier, $this->adresse2_fr, PDO::PARAM_STR);
                        break;
                    case '`pays_fr`':
                        $stmt->bindValue($identifier, $this->pays_fr, PDO::PARAM_STR);
                        break;
                    case '`denomination_org_es`':
                        $stmt->bindValue($identifier, $this->denomination_org_es, PDO::PARAM_STR);
                        break;
                    case '`description_org_es`':
                        $stmt->bindValue($identifier, $this->description_org_es, PDO::PARAM_STR);
                        break;
                    case '`adresse_es`':
                        $stmt->bindValue($identifier, $this->adresse_es, PDO::PARAM_STR);
                        break;
                    case '`ville_es`':
                        $stmt->bindValue($identifier, $this->ville_es, PDO::PARAM_STR);
                        break;
                    case '`adresse2_es`':
                        $stmt->bindValue($identifier, $this->adresse2_es, PDO::PARAM_STR);
                        break;
                    case '`pays_es`':
                        $stmt->bindValue($identifier, $this->pays_es, PDO::PARAM_STR);
                        break;
                    case '`denomination_org_en`':
                        $stmt->bindValue($identifier, $this->denomination_org_en, PDO::PARAM_STR);
                        break;
                    case '`description_org_en`':
                        $stmt->bindValue($identifier, $this->description_org_en, PDO::PARAM_STR);
                        break;
                    case '`adresse_en`':
                        $stmt->bindValue($identifier, $this->adresse_en, PDO::PARAM_STR);
                        break;
                    case '`ville_en`':
                        $stmt->bindValue($identifier, $this->ville_en, PDO::PARAM_STR);
                        break;
                    case '`adresse2_en`':
                        $stmt->bindValue($identifier, $this->adresse2_en, PDO::PARAM_STR);
                        break;
                    case '`pays_en`':
                        $stmt->bindValue($identifier, $this->pays_en, PDO::PARAM_STR);
                        break;
                    case '`denomination_org_su`':
                        $stmt->bindValue($identifier, $this->denomination_org_su, PDO::PARAM_STR);
                        break;
                    case '`description_org_su`':
                        $stmt->bindValue($identifier, $this->description_org_su, PDO::PARAM_STR);
                        break;
                    case '`adresse_su`':
                        $stmt->bindValue($identifier, $this->adresse_su, PDO::PARAM_STR);
                        break;
                    case '`ville_su`':
                        $stmt->bindValue($identifier, $this->ville_su, PDO::PARAM_STR);
                        break;
                    case '`adresse2_su`':
                        $stmt->bindValue($identifier, $this->adresse2_su, PDO::PARAM_STR);
                        break;
                    case '`pays_su`':
                        $stmt->bindValue($identifier, $this->pays_su, PDO::PARAM_STR);
                        break;
                    case '`denomination_org_du`':
                        $stmt->bindValue($identifier, $this->denomination_org_du, PDO::PARAM_STR);
                        break;
                    case '`description_org_du`':
                        $stmt->bindValue($identifier, $this->description_org_du, PDO::PARAM_STR);
                        break;
                    case '`adresse_du`':
                        $stmt->bindValue($identifier, $this->adresse_du, PDO::PARAM_STR);
                        break;
                    case '`ville_du`':
                        $stmt->bindValue($identifier, $this->ville_du, PDO::PARAM_STR);
                        break;
                    case '`adresse2_du`':
                        $stmt->bindValue($identifier, $this->adresse2_du, PDO::PARAM_STR);
                        break;
                    case '`pays_du`':
                        $stmt->bindValue($identifier, $this->pays_du, PDO::PARAM_STR);
                        break;
                    case '`denomination_org_cz`':
                        $stmt->bindValue($identifier, $this->denomination_org_cz, PDO::PARAM_STR);
                        break;
                    case '`description_org_cz`':
                        $stmt->bindValue($identifier, $this->description_org_cz, PDO::PARAM_STR);
                        break;
                    case '`adresse_cz`':
                        $stmt->bindValue($identifier, $this->adresse_cz, PDO::PARAM_STR);
                        break;
                    case '`ville_cz`':
                        $stmt->bindValue($identifier, $this->ville_cz, PDO::PARAM_STR);
                        break;
                    case '`adresse2_cz`':
                        $stmt->bindValue($identifier, $this->adresse2_cz, PDO::PARAM_STR);
                        break;
                    case '`pays_cz`':
                        $stmt->bindValue($identifier, $this->pays_cz, PDO::PARAM_STR);
                        break;
                    case '`denomination_org_it`':
                        $stmt->bindValue($identifier, $this->denomination_org_it, PDO::PARAM_STR);
                        break;
                    case '`description_org_it`':
                        $stmt->bindValue($identifier, $this->description_org_it, PDO::PARAM_STR);
                        break;
                    case '`adresse_it`':
                        $stmt->bindValue($identifier, $this->adresse_it, PDO::PARAM_STR);
                        break;
                    case '`ville_it`':
                        $stmt->bindValue($identifier, $this->ville_it, PDO::PARAM_STR);
                        break;
                    case '`adresse2_it`':
                        $stmt->bindValue($identifier, $this->adresse2_it, PDO::PARAM_STR);
                        break;
                    case '`pays_it`':
                        $stmt->bindValue($identifier, $this->pays_it, PDO::PARAM_STR);
                        break;
                    case '`siren`':
                        $stmt->bindValue($identifier, $this->siren, PDO::PARAM_STR);
                        break;
                    case '`complement`':
                        $stmt->bindValue($identifier, $this->complement, PDO::PARAM_STR);
                        break;
                    case '`moniteur_provenance`':
                        $stmt->bindValue($identifier, $this->moniteur_provenance, PDO::PARAM_INT);
                        break;
                    case '`code_acces_logiciel`':
                        $stmt->bindValue($identifier, $this->code_acces_logiciel, PDO::PARAM_STR);
                        break;
                    case '`decalage_horaire`':
                        $stmt->bindValue($identifier, $this->decalage_horaire, PDO::PARAM_STR);
                        break;
                    case '`lieu_residence`':
                        $stmt->bindValue($identifier, $this->lieu_residence, PDO::PARAM_STR);
                        break;
                    case '`activation_fuseau_horaire`':
                        $stmt->bindValue($identifier, $this->activation_fuseau_horaire, PDO::PARAM_STR);
                        break;
                    case '`alerte`':
                        $stmt->bindValue($identifier, $this->alerte, PDO::PARAM_STR);
                        break;
                    case '`ordre`':
                        $stmt->bindValue($identifier, $this->ordre, PDO::PARAM_INT);
                        break;
                    case '`URL_INTERFACE_ANM`':
                        $stmt->bindValue($identifier, $this->url_interface_anm, PDO::PARAM_STR);
                        break;
                    case '`sous_type_organisme`':
                        $stmt->bindValue($identifier, $this->sous_type_organisme, PDO::PARAM_INT);
                        break;
                    case '`pf_url`':
                        $stmt->bindValue($identifier, $this->pf_url, PDO::PARAM_STR);
                        break;
                    case '`tag_purge`':
                        $stmt->bindValue($identifier, (int) $this->tag_purge, PDO::PARAM_INT);
                        break;
                    case '`id_externe`':
                        $stmt->bindValue($identifier, $this->id_externe, PDO::PARAM_STR);
                        break;
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonCategorieINSEE !== null) {
                if (!$this->aCommonCategorieINSEE->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonCategorieINSEE->getValidationFailures());
                }
            }


            if (($retval = CommonOrganismePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonAdministrateurs !== null) {
                    foreach ($this->collCommonAdministrateurs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonAffiliationServices !== null) {
                    foreach ($this->collCommonAffiliationServices as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonAgents !== null) {
                    foreach ($this->collCommonAgents as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEchanges !== null) {
                    foreach ($this->collCommonEchanges as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonOrganismeServiceMetiers !== null) {
                    foreach ($this->collCommonOrganismeServiceMetiers as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonParametrageEncheres !== null) {
                    foreach ($this->collCommonParametrageEncheres as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonRPAs !== null) {
                    foreach ($this->collCommonRPAs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTelechargements !== null) {
                    foreach ($this->collCommonTelechargements as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTierss !== null) {
                    foreach ($this->collCommonTierss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->singleCommonConfigurationOrganisme !== null) {
                    if (!$this->singleCommonConfigurationOrganisme->validate($columns)) {
                        $failureMap = array_merge($failureMap, $this->singleCommonConfigurationOrganisme->getValidationFailures());
                    }
                }

                if ($this->collCommonConsultations !== null) {
                    foreach ($this->collCommonConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonDestinataireMiseDispositions !== null) {
                    foreach ($this->collCommonDestinataireMiseDispositions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEchangeDocApplicationClientOrganismes !== null) {
                    foreach ($this->collCommonEchangeDocApplicationClientOrganismes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonInvitePermanentTransverses !== null) {
                    foreach ($this->collCommonInvitePermanentTransverses as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonPlateformeVirtuelleOrganismes !== null) {
                    foreach ($this->collCommonPlateformeVirtuelleOrganismes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOCommissions !== null) {
                    foreach ($this->collCommonTCAOCommissions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOCommissionAgents !== null) {
                    foreach ($this->collCommonTCAOCommissionAgents as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOCommissionIntervenantExternes !== null) {
                    foreach ($this->collCommonTCAOCommissionIntervenantExternes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOIntervenantExternes !== null) {
                    foreach ($this->collCommonTCAOIntervenantExternes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOOrdreDuJours !== null) {
                    foreach ($this->collCommonTCAOOrdreDuJours as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOOrdreDuJourIntervenants !== null) {
                    foreach ($this->collCommonTCAOOrdreDuJourIntervenants as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOSeances !== null) {
                    foreach ($this->collCommonTCAOSeances as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOSeanceAgents !== null) {
                    foreach ($this->collCommonTCAOSeanceAgents as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOSeanceIntervenantExternes !== null) {
                    foreach ($this->collCommonTCAOSeanceIntervenantExternes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCandidatures !== null) {
                    foreach ($this->collCommonTCandidatures as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTDumeContextes !== null) {
                    foreach ($this->collCommonTDumeContextes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTListeLotsCandidatures !== null) {
                    foreach ($this->collCommonTListeLotsCandidatures as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTVisionRmaAgentOrganismes !== null) {
                    foreach ($this->collCommonTVisionRmaAgentOrganismes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonOrganismePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getAcronyme();
                break;
            case 2:
                return $this->getTypeArticleOrg();
                break;
            case 3:
                return $this->getDenominationOrg();
                break;
            case 4:
                return $this->getCategorieInsee();
                break;
            case 5:
                return $this->getDescriptionOrg();
                break;
            case 6:
                return $this->getAdresse();
                break;
            case 7:
                return $this->getCp();
                break;
            case 8:
                return $this->getVille();
                break;
            case 9:
                return $this->getEmail();
                break;
            case 10:
                return $this->getUrl();
                break;
            case 11:
                return $this->getIdAttribFile();
                break;
            case 12:
                return $this->getAttribFile();
                break;
            case 13:
                return $this->getDateCreation();
                break;
            case 14:
                return $this->getActive();
                break;
            case 15:
                return $this->getIdClientAnm();
                break;
            case 16:
                return $this->getStatus();
                break;
            case 17:
                return $this->getSignataireCao();
                break;
            case 18:
                return $this->getSigle();
                break;
            case 19:
                return $this->getAdresse2();
                break;
            case 20:
                return $this->getTel();
                break;
            case 21:
                return $this->getTelecopie();
                break;
            case 22:
                return $this->getPays();
                break;
            case 23:
                return $this->getAffichageEntite();
                break;
            case 24:
                return $this->getIdInitial();
                break;
            case 25:
                return $this->getDenominationOrgAr();
                break;
            case 26:
                return $this->getDescriptionOrgAr();
                break;
            case 27:
                return $this->getAdresseAr();
                break;
            case 28:
                return $this->getVilleAr();
                break;
            case 29:
                return $this->getAdresse2Ar();
                break;
            case 30:
                return $this->getPaysAr();
                break;
            case 31:
                return $this->getDenominationOrgFr();
                break;
            case 32:
                return $this->getDescriptionOrgFr();
                break;
            case 33:
                return $this->getAdresseFr();
                break;
            case 34:
                return $this->getVilleFr();
                break;
            case 35:
                return $this->getAdresse2Fr();
                break;
            case 36:
                return $this->getPaysFr();
                break;
            case 37:
                return $this->getDenominationOrgEs();
                break;
            case 38:
                return $this->getDescriptionOrgEs();
                break;
            case 39:
                return $this->getAdresseEs();
                break;
            case 40:
                return $this->getVilleEs();
                break;
            case 41:
                return $this->getAdresse2Es();
                break;
            case 42:
                return $this->getPaysEs();
                break;
            case 43:
                return $this->getDenominationOrgEn();
                break;
            case 44:
                return $this->getDescriptionOrgEn();
                break;
            case 45:
                return $this->getAdresseEn();
                break;
            case 46:
                return $this->getVilleEn();
                break;
            case 47:
                return $this->getAdresse2En();
                break;
            case 48:
                return $this->getPaysEn();
                break;
            case 49:
                return $this->getDenominationOrgSu();
                break;
            case 50:
                return $this->getDescriptionOrgSu();
                break;
            case 51:
                return $this->getAdresseSu();
                break;
            case 52:
                return $this->getVilleSu();
                break;
            case 53:
                return $this->getAdresse2Su();
                break;
            case 54:
                return $this->getPaysSu();
                break;
            case 55:
                return $this->getDenominationOrgDu();
                break;
            case 56:
                return $this->getDescriptionOrgDu();
                break;
            case 57:
                return $this->getAdresseDu();
                break;
            case 58:
                return $this->getVilleDu();
                break;
            case 59:
                return $this->getAdresse2Du();
                break;
            case 60:
                return $this->getPaysDu();
                break;
            case 61:
                return $this->getDenominationOrgCz();
                break;
            case 62:
                return $this->getDescriptionOrgCz();
                break;
            case 63:
                return $this->getAdresseCz();
                break;
            case 64:
                return $this->getVilleCz();
                break;
            case 65:
                return $this->getAdresse2Cz();
                break;
            case 66:
                return $this->getPaysCz();
                break;
            case 67:
                return $this->getDenominationOrgIt();
                break;
            case 68:
                return $this->getDescriptionOrgIt();
                break;
            case 69:
                return $this->getAdresseIt();
                break;
            case 70:
                return $this->getVilleIt();
                break;
            case 71:
                return $this->getAdresse2It();
                break;
            case 72:
                return $this->getPaysIt();
                break;
            case 73:
                return $this->getSiren();
                break;
            case 74:
                return $this->getComplement();
                break;
            case 75:
                return $this->getMoniteurProvenance();
                break;
            case 76:
                return $this->getCodeAccesLogiciel();
                break;
            case 77:
                return $this->getDecalageHoraire();
                break;
            case 78:
                return $this->getLieuResidence();
                break;
            case 79:
                return $this->getActivationFuseauHoraire();
                break;
            case 80:
                return $this->getAlerte();
                break;
            case 81:
                return $this->getOrdre();
                break;
            case 82:
                return $this->getUrlInterfaceAnm();
                break;
            case 83:
                return $this->getSousTypeOrganisme();
                break;
            case 84:
                return $this->getPfUrl();
                break;
            case 85:
                return $this->getTagPurge();
                break;
            case 86:
                return $this->getIdExterne();
                break;
            case 87:
                return $this->getIdEntite();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonOrganisme'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonOrganisme'][$this->getPrimaryKey()] = true;
        $keys = CommonOrganismePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getAcronyme(),
            $keys[2] => $this->getTypeArticleOrg(),
            $keys[3] => $this->getDenominationOrg(),
            $keys[4] => $this->getCategorieInsee(),
            $keys[5] => $this->getDescriptionOrg(),
            $keys[6] => $this->getAdresse(),
            $keys[7] => $this->getCp(),
            $keys[8] => $this->getVille(),
            $keys[9] => $this->getEmail(),
            $keys[10] => $this->getUrl(),
            $keys[11] => $this->getIdAttribFile(),
            $keys[12] => $this->getAttribFile(),
            $keys[13] => $this->getDateCreation(),
            $keys[14] => $this->getActive(),
            $keys[15] => $this->getIdClientAnm(),
            $keys[16] => $this->getStatus(),
            $keys[17] => $this->getSignataireCao(),
            $keys[18] => $this->getSigle(),
            $keys[19] => $this->getAdresse2(),
            $keys[20] => $this->getTel(),
            $keys[21] => $this->getTelecopie(),
            $keys[22] => $this->getPays(),
            $keys[23] => $this->getAffichageEntite(),
            $keys[24] => $this->getIdInitial(),
            $keys[25] => $this->getDenominationOrgAr(),
            $keys[26] => $this->getDescriptionOrgAr(),
            $keys[27] => $this->getAdresseAr(),
            $keys[28] => $this->getVilleAr(),
            $keys[29] => $this->getAdresse2Ar(),
            $keys[30] => $this->getPaysAr(),
            $keys[31] => $this->getDenominationOrgFr(),
            $keys[32] => $this->getDescriptionOrgFr(),
            $keys[33] => $this->getAdresseFr(),
            $keys[34] => $this->getVilleFr(),
            $keys[35] => $this->getAdresse2Fr(),
            $keys[36] => $this->getPaysFr(),
            $keys[37] => $this->getDenominationOrgEs(),
            $keys[38] => $this->getDescriptionOrgEs(),
            $keys[39] => $this->getAdresseEs(),
            $keys[40] => $this->getVilleEs(),
            $keys[41] => $this->getAdresse2Es(),
            $keys[42] => $this->getPaysEs(),
            $keys[43] => $this->getDenominationOrgEn(),
            $keys[44] => $this->getDescriptionOrgEn(),
            $keys[45] => $this->getAdresseEn(),
            $keys[46] => $this->getVilleEn(),
            $keys[47] => $this->getAdresse2En(),
            $keys[48] => $this->getPaysEn(),
            $keys[49] => $this->getDenominationOrgSu(),
            $keys[50] => $this->getDescriptionOrgSu(),
            $keys[51] => $this->getAdresseSu(),
            $keys[52] => $this->getVilleSu(),
            $keys[53] => $this->getAdresse2Su(),
            $keys[54] => $this->getPaysSu(),
            $keys[55] => $this->getDenominationOrgDu(),
            $keys[56] => $this->getDescriptionOrgDu(),
            $keys[57] => $this->getAdresseDu(),
            $keys[58] => $this->getVilleDu(),
            $keys[59] => $this->getAdresse2Du(),
            $keys[60] => $this->getPaysDu(),
            $keys[61] => $this->getDenominationOrgCz(),
            $keys[62] => $this->getDescriptionOrgCz(),
            $keys[63] => $this->getAdresseCz(),
            $keys[64] => $this->getVilleCz(),
            $keys[65] => $this->getAdresse2Cz(),
            $keys[66] => $this->getPaysCz(),
            $keys[67] => $this->getDenominationOrgIt(),
            $keys[68] => $this->getDescriptionOrgIt(),
            $keys[69] => $this->getAdresseIt(),
            $keys[70] => $this->getVilleIt(),
            $keys[71] => $this->getAdresse2It(),
            $keys[72] => $this->getPaysIt(),
            $keys[73] => $this->getSiren(),
            $keys[74] => $this->getComplement(),
            $keys[75] => $this->getMoniteurProvenance(),
            $keys[76] => $this->getCodeAccesLogiciel(),
            $keys[77] => $this->getDecalageHoraire(),
            $keys[78] => $this->getLieuResidence(),
            $keys[79] => $this->getActivationFuseauHoraire(),
            $keys[80] => $this->getAlerte(),
            $keys[81] => $this->getOrdre(),
            $keys[82] => $this->getUrlInterfaceAnm(),
            $keys[83] => $this->getSousTypeOrganisme(),
            $keys[84] => $this->getPfUrl(),
            $keys[85] => $this->getTagPurge(),
            $keys[86] => $this->getIdExterne(),
            $keys[87] => $this->getIdEntite(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonCategorieINSEE) {
                $result['CommonCategorieINSEE'] = $this->aCommonCategorieINSEE->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonAdministrateurs) {
                $result['CommonAdministrateurs'] = $this->collCommonAdministrateurs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonAffiliationServices) {
                $result['CommonAffiliationServices'] = $this->collCommonAffiliationServices->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonAgents) {
                $result['CommonAgents'] = $this->collCommonAgents->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEchanges) {
                $result['CommonEchanges'] = $this->collCommonEchanges->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonOrganismeServiceMetiers) {
                $result['CommonOrganismeServiceMetiers'] = $this->collCommonOrganismeServiceMetiers->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonParametrageEncheres) {
                $result['CommonParametrageEncheres'] = $this->collCommonParametrageEncheres->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonRPAs) {
                $result['CommonRPAs'] = $this->collCommonRPAs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTelechargements) {
                $result['CommonTelechargements'] = $this->collCommonTelechargements->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTierss) {
                $result['CommonTierss'] = $this->collCommonTierss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->singleCommonConfigurationOrganisme) {
                $result['CommonConfigurationOrganisme'] = $this->singleCommonConfigurationOrganisme->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonConsultations) {
                $result['CommonConsultations'] = $this->collCommonConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonDestinataireMiseDispositions) {
                $result['CommonDestinataireMiseDispositions'] = $this->collCommonDestinataireMiseDispositions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEchangeDocApplicationClientOrganismes) {
                $result['CommonEchangeDocApplicationClientOrganismes'] = $this->collCommonEchangeDocApplicationClientOrganismes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonInvitePermanentTransverses) {
                $result['CommonInvitePermanentTransverses'] = $this->collCommonInvitePermanentTransverses->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonPlateformeVirtuelleOrganismes) {
                $result['CommonPlateformeVirtuelleOrganismes'] = $this->collCommonPlateformeVirtuelleOrganismes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOCommissions) {
                $result['CommonTCAOCommissions'] = $this->collCommonTCAOCommissions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOCommissionAgents) {
                $result['CommonTCAOCommissionAgents'] = $this->collCommonTCAOCommissionAgents->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOCommissionIntervenantExternes) {
                $result['CommonTCAOCommissionIntervenantExternes'] = $this->collCommonTCAOCommissionIntervenantExternes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOIntervenantExternes) {
                $result['CommonTCAOIntervenantExternes'] = $this->collCommonTCAOIntervenantExternes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOOrdreDuJours) {
                $result['CommonTCAOOrdreDuJours'] = $this->collCommonTCAOOrdreDuJours->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOOrdreDuJourIntervenants) {
                $result['CommonTCAOOrdreDuJourIntervenants'] = $this->collCommonTCAOOrdreDuJourIntervenants->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOSeances) {
                $result['CommonTCAOSeances'] = $this->collCommonTCAOSeances->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOSeanceAgents) {
                $result['CommonTCAOSeanceAgents'] = $this->collCommonTCAOSeanceAgents->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOSeanceIntervenantExternes) {
                $result['CommonTCAOSeanceIntervenantExternes'] = $this->collCommonTCAOSeanceIntervenantExternes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCandidatures) {
                $result['CommonTCandidatures'] = $this->collCommonTCandidatures->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTDumeContextes) {
                $result['CommonTDumeContextes'] = $this->collCommonTDumeContextes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTListeLotsCandidatures) {
                $result['CommonTListeLotsCandidatures'] = $this->collCommonTListeLotsCandidatures->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTVisionRmaAgentOrganismes) {
                $result['CommonTVisionRmaAgentOrganismes'] = $this->collCommonTVisionRmaAgentOrganismes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonOrganismePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setAcronyme($value);
                break;
            case 2:
                $this->setTypeArticleOrg($value);
                break;
            case 3:
                $this->setDenominationOrg($value);
                break;
            case 4:
                $this->setCategorieInsee($value);
                break;
            case 5:
                $this->setDescriptionOrg($value);
                break;
            case 6:
                $this->setAdresse($value);
                break;
            case 7:
                $this->setCp($value);
                break;
            case 8:
                $this->setVille($value);
                break;
            case 9:
                $this->setEmail($value);
                break;
            case 10:
                $this->setUrl($value);
                break;
            case 11:
                $this->setIdAttribFile($value);
                break;
            case 12:
                $this->setAttribFile($value);
                break;
            case 13:
                $this->setDateCreation($value);
                break;
            case 14:
                $this->setActive($value);
                break;
            case 15:
                $this->setIdClientAnm($value);
                break;
            case 16:
                $this->setStatus($value);
                break;
            case 17:
                $this->setSignataireCao($value);
                break;
            case 18:
                $this->setSigle($value);
                break;
            case 19:
                $this->setAdresse2($value);
                break;
            case 20:
                $this->setTel($value);
                break;
            case 21:
                $this->setTelecopie($value);
                break;
            case 22:
                $this->setPays($value);
                break;
            case 23:
                $this->setAffichageEntite($value);
                break;
            case 24:
                $this->setIdInitial($value);
                break;
            case 25:
                $this->setDenominationOrgAr($value);
                break;
            case 26:
                $this->setDescriptionOrgAr($value);
                break;
            case 27:
                $this->setAdresseAr($value);
                break;
            case 28:
                $this->setVilleAr($value);
                break;
            case 29:
                $this->setAdresse2Ar($value);
                break;
            case 30:
                $this->setPaysAr($value);
                break;
            case 31:
                $this->setDenominationOrgFr($value);
                break;
            case 32:
                $this->setDescriptionOrgFr($value);
                break;
            case 33:
                $this->setAdresseFr($value);
                break;
            case 34:
                $this->setVilleFr($value);
                break;
            case 35:
                $this->setAdresse2Fr($value);
                break;
            case 36:
                $this->setPaysFr($value);
                break;
            case 37:
                $this->setDenominationOrgEs($value);
                break;
            case 38:
                $this->setDescriptionOrgEs($value);
                break;
            case 39:
                $this->setAdresseEs($value);
                break;
            case 40:
                $this->setVilleEs($value);
                break;
            case 41:
                $this->setAdresse2Es($value);
                break;
            case 42:
                $this->setPaysEs($value);
                break;
            case 43:
                $this->setDenominationOrgEn($value);
                break;
            case 44:
                $this->setDescriptionOrgEn($value);
                break;
            case 45:
                $this->setAdresseEn($value);
                break;
            case 46:
                $this->setVilleEn($value);
                break;
            case 47:
                $this->setAdresse2En($value);
                break;
            case 48:
                $this->setPaysEn($value);
                break;
            case 49:
                $this->setDenominationOrgSu($value);
                break;
            case 50:
                $this->setDescriptionOrgSu($value);
                break;
            case 51:
                $this->setAdresseSu($value);
                break;
            case 52:
                $this->setVilleSu($value);
                break;
            case 53:
                $this->setAdresse2Su($value);
                break;
            case 54:
                $this->setPaysSu($value);
                break;
            case 55:
                $this->setDenominationOrgDu($value);
                break;
            case 56:
                $this->setDescriptionOrgDu($value);
                break;
            case 57:
                $this->setAdresseDu($value);
                break;
            case 58:
                $this->setVilleDu($value);
                break;
            case 59:
                $this->setAdresse2Du($value);
                break;
            case 60:
                $this->setPaysDu($value);
                break;
            case 61:
                $this->setDenominationOrgCz($value);
                break;
            case 62:
                $this->setDescriptionOrgCz($value);
                break;
            case 63:
                $this->setAdresseCz($value);
                break;
            case 64:
                $this->setVilleCz($value);
                break;
            case 65:
                $this->setAdresse2Cz($value);
                break;
            case 66:
                $this->setPaysCz($value);
                break;
            case 67:
                $this->setDenominationOrgIt($value);
                break;
            case 68:
                $this->setDescriptionOrgIt($value);
                break;
            case 69:
                $this->setAdresseIt($value);
                break;
            case 70:
                $this->setVilleIt($value);
                break;
            case 71:
                $this->setAdresse2It($value);
                break;
            case 72:
                $this->setPaysIt($value);
                break;
            case 73:
                $this->setSiren($value);
                break;
            case 74:
                $this->setComplement($value);
                break;
            case 75:
                $this->setMoniteurProvenance($value);
                break;
            case 76:
                $this->setCodeAccesLogiciel($value);
                break;
            case 77:
                $this->setDecalageHoraire($value);
                break;
            case 78:
                $this->setLieuResidence($value);
                break;
            case 79:
                $this->setActivationFuseauHoraire($value);
                break;
            case 80:
                $this->setAlerte($value);
                break;
            case 81:
                $this->setOrdre($value);
                break;
            case 82:
                $this->setUrlInterfaceAnm($value);
                break;
            case 83:
                $this->setSousTypeOrganisme($value);
                break;
            case 84:
                $this->setPfUrl($value);
                break;
            case 85:
                $this->setTagPurge($value);
                break;
            case 86:
                $this->setIdExterne($value);
                break;
            case 87:
                $this->setIdEntite($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonOrganismePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setAcronyme($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTypeArticleOrg($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDenominationOrg($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setCategorieInsee($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDescriptionOrg($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAdresse($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setCp($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setVille($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setEmail($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUrl($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setIdAttribFile($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setAttribFile($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setDateCreation($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setActive($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setIdClientAnm($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setStatus($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setSignataireCao($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setSigle($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setAdresse2($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setTel($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setTelecopie($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setPays($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setAffichageEntite($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setIdInitial($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setDenominationOrgAr($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setDescriptionOrgAr($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setAdresseAr($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setVilleAr($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setAdresse2Ar($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setPaysAr($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setDenominationOrgFr($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setDescriptionOrgFr($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setAdresseFr($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setVilleFr($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setAdresse2Fr($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setPaysFr($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setDenominationOrgEs($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setDescriptionOrgEs($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setAdresseEs($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setVilleEs($arr[$keys[40]]);
        if (array_key_exists($keys[41], $arr)) $this->setAdresse2Es($arr[$keys[41]]);
        if (array_key_exists($keys[42], $arr)) $this->setPaysEs($arr[$keys[42]]);
        if (array_key_exists($keys[43], $arr)) $this->setDenominationOrgEn($arr[$keys[43]]);
        if (array_key_exists($keys[44], $arr)) $this->setDescriptionOrgEn($arr[$keys[44]]);
        if (array_key_exists($keys[45], $arr)) $this->setAdresseEn($arr[$keys[45]]);
        if (array_key_exists($keys[46], $arr)) $this->setVilleEn($arr[$keys[46]]);
        if (array_key_exists($keys[47], $arr)) $this->setAdresse2En($arr[$keys[47]]);
        if (array_key_exists($keys[48], $arr)) $this->setPaysEn($arr[$keys[48]]);
        if (array_key_exists($keys[49], $arr)) $this->setDenominationOrgSu($arr[$keys[49]]);
        if (array_key_exists($keys[50], $arr)) $this->setDescriptionOrgSu($arr[$keys[50]]);
        if (array_key_exists($keys[51], $arr)) $this->setAdresseSu($arr[$keys[51]]);
        if (array_key_exists($keys[52], $arr)) $this->setVilleSu($arr[$keys[52]]);
        if (array_key_exists($keys[53], $arr)) $this->setAdresse2Su($arr[$keys[53]]);
        if (array_key_exists($keys[54], $arr)) $this->setPaysSu($arr[$keys[54]]);
        if (array_key_exists($keys[55], $arr)) $this->setDenominationOrgDu($arr[$keys[55]]);
        if (array_key_exists($keys[56], $arr)) $this->setDescriptionOrgDu($arr[$keys[56]]);
        if (array_key_exists($keys[57], $arr)) $this->setAdresseDu($arr[$keys[57]]);
        if (array_key_exists($keys[58], $arr)) $this->setVilleDu($arr[$keys[58]]);
        if (array_key_exists($keys[59], $arr)) $this->setAdresse2Du($arr[$keys[59]]);
        if (array_key_exists($keys[60], $arr)) $this->setPaysDu($arr[$keys[60]]);
        if (array_key_exists($keys[61], $arr)) $this->setDenominationOrgCz($arr[$keys[61]]);
        if (array_key_exists($keys[62], $arr)) $this->setDescriptionOrgCz($arr[$keys[62]]);
        if (array_key_exists($keys[63], $arr)) $this->setAdresseCz($arr[$keys[63]]);
        if (array_key_exists($keys[64], $arr)) $this->setVilleCz($arr[$keys[64]]);
        if (array_key_exists($keys[65], $arr)) $this->setAdresse2Cz($arr[$keys[65]]);
        if (array_key_exists($keys[66], $arr)) $this->setPaysCz($arr[$keys[66]]);
        if (array_key_exists($keys[67], $arr)) $this->setDenominationOrgIt($arr[$keys[67]]);
        if (array_key_exists($keys[68], $arr)) $this->setDescriptionOrgIt($arr[$keys[68]]);
        if (array_key_exists($keys[69], $arr)) $this->setAdresseIt($arr[$keys[69]]);
        if (array_key_exists($keys[70], $arr)) $this->setVilleIt($arr[$keys[70]]);
        if (array_key_exists($keys[71], $arr)) $this->setAdresse2It($arr[$keys[71]]);
        if (array_key_exists($keys[72], $arr)) $this->setPaysIt($arr[$keys[72]]);
        if (array_key_exists($keys[73], $arr)) $this->setSiren($arr[$keys[73]]);
        if (array_key_exists($keys[74], $arr)) $this->setComplement($arr[$keys[74]]);
        if (array_key_exists($keys[75], $arr)) $this->setMoniteurProvenance($arr[$keys[75]]);
        if (array_key_exists($keys[76], $arr)) $this->setCodeAccesLogiciel($arr[$keys[76]]);
        if (array_key_exists($keys[77], $arr)) $this->setDecalageHoraire($arr[$keys[77]]);
        if (array_key_exists($keys[78], $arr)) $this->setLieuResidence($arr[$keys[78]]);
        if (array_key_exists($keys[79], $arr)) $this->setActivationFuseauHoraire($arr[$keys[79]]);
        if (array_key_exists($keys[80], $arr)) $this->setAlerte($arr[$keys[80]]);
        if (array_key_exists($keys[81], $arr)) $this->setOrdre($arr[$keys[81]]);
        if (array_key_exists($keys[82], $arr)) $this->setUrlInterfaceAnm($arr[$keys[82]]);
        if (array_key_exists($keys[83], $arr)) $this->setSousTypeOrganisme($arr[$keys[83]]);
        if (array_key_exists($keys[84], $arr)) $this->setPfUrl($arr[$keys[84]]);
        if (array_key_exists($keys[85], $arr)) $this->setTagPurge($arr[$keys[85]]);
        if (array_key_exists($keys[86], $arr)) $this->setIdExterne($arr[$keys[86]]);
        if (array_key_exists($keys[87], $arr)) $this->setIdEntite($arr[$keys[87]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonOrganismePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonOrganismePeer::ID)) $criteria->add(CommonOrganismePeer::ID, $this->id);
        if ($this->isColumnModified(CommonOrganismePeer::ACRONYME)) $criteria->add(CommonOrganismePeer::ACRONYME, $this->acronyme);
        if ($this->isColumnModified(CommonOrganismePeer::TYPE_ARTICLE_ORG)) $criteria->add(CommonOrganismePeer::TYPE_ARTICLE_ORG, $this->type_article_org);
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG)) $criteria->add(CommonOrganismePeer::DENOMINATION_ORG, $this->denomination_org);
        if ($this->isColumnModified(CommonOrganismePeer::CATEGORIE_INSEE)) $criteria->add(CommonOrganismePeer::CATEGORIE_INSEE, $this->categorie_insee);
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG)) $criteria->add(CommonOrganismePeer::DESCRIPTION_ORG, $this->description_org);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE)) $criteria->add(CommonOrganismePeer::ADRESSE, $this->adresse);
        if ($this->isColumnModified(CommonOrganismePeer::CP)) $criteria->add(CommonOrganismePeer::CP, $this->cp);
        if ($this->isColumnModified(CommonOrganismePeer::VILLE)) $criteria->add(CommonOrganismePeer::VILLE, $this->ville);
        if ($this->isColumnModified(CommonOrganismePeer::EMAIL)) $criteria->add(CommonOrganismePeer::EMAIL, $this->email);
        if ($this->isColumnModified(CommonOrganismePeer::URL)) $criteria->add(CommonOrganismePeer::URL, $this->url);
        if ($this->isColumnModified(CommonOrganismePeer::ID_ATTRIB_FILE)) $criteria->add(CommonOrganismePeer::ID_ATTRIB_FILE, $this->id_attrib_file);
        if ($this->isColumnModified(CommonOrganismePeer::ATTRIB_FILE)) $criteria->add(CommonOrganismePeer::ATTRIB_FILE, $this->attrib_file);
        if ($this->isColumnModified(CommonOrganismePeer::DATE_CREATION)) $criteria->add(CommonOrganismePeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonOrganismePeer::ACTIVE)) $criteria->add(CommonOrganismePeer::ACTIVE, $this->active);
        if ($this->isColumnModified(CommonOrganismePeer::ID_CLIENT_ANM)) $criteria->add(CommonOrganismePeer::ID_CLIENT_ANM, $this->id_client_anm);
        if ($this->isColumnModified(CommonOrganismePeer::STATUS)) $criteria->add(CommonOrganismePeer::STATUS, $this->status);
        if ($this->isColumnModified(CommonOrganismePeer::SIGNATAIRE_CAO)) $criteria->add(CommonOrganismePeer::SIGNATAIRE_CAO, $this->signataire_cao);
        if ($this->isColumnModified(CommonOrganismePeer::SIGLE)) $criteria->add(CommonOrganismePeer::SIGLE, $this->sigle);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2)) $criteria->add(CommonOrganismePeer::ADRESSE2, $this->adresse2);
        if ($this->isColumnModified(CommonOrganismePeer::TEL)) $criteria->add(CommonOrganismePeer::TEL, $this->tel);
        if ($this->isColumnModified(CommonOrganismePeer::TELECOPIE)) $criteria->add(CommonOrganismePeer::TELECOPIE, $this->telecopie);
        if ($this->isColumnModified(CommonOrganismePeer::PAYS)) $criteria->add(CommonOrganismePeer::PAYS, $this->pays);
        if ($this->isColumnModified(CommonOrganismePeer::AFFICHAGE_ENTITE)) $criteria->add(CommonOrganismePeer::AFFICHAGE_ENTITE, $this->affichage_entite);
        if ($this->isColumnModified(CommonOrganismePeer::ID_INITIAL)) $criteria->add(CommonOrganismePeer::ID_INITIAL, $this->id_initial);
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_AR)) $criteria->add(CommonOrganismePeer::DENOMINATION_ORG_AR, $this->denomination_org_ar);
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_AR)) $criteria->add(CommonOrganismePeer::DESCRIPTION_ORG_AR, $this->description_org_ar);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_AR)) $criteria->add(CommonOrganismePeer::ADRESSE_AR, $this->adresse_ar);
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_AR)) $criteria->add(CommonOrganismePeer::VILLE_AR, $this->ville_ar);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_AR)) $criteria->add(CommonOrganismePeer::ADRESSE2_AR, $this->adresse2_ar);
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_AR)) $criteria->add(CommonOrganismePeer::PAYS_AR, $this->pays_ar);
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_FR)) $criteria->add(CommonOrganismePeer::DENOMINATION_ORG_FR, $this->denomination_org_fr);
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_FR)) $criteria->add(CommonOrganismePeer::DESCRIPTION_ORG_FR, $this->description_org_fr);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_FR)) $criteria->add(CommonOrganismePeer::ADRESSE_FR, $this->adresse_fr);
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_FR)) $criteria->add(CommonOrganismePeer::VILLE_FR, $this->ville_fr);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_FR)) $criteria->add(CommonOrganismePeer::ADRESSE2_FR, $this->adresse2_fr);
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_FR)) $criteria->add(CommonOrganismePeer::PAYS_FR, $this->pays_fr);
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_ES)) $criteria->add(CommonOrganismePeer::DENOMINATION_ORG_ES, $this->denomination_org_es);
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_ES)) $criteria->add(CommonOrganismePeer::DESCRIPTION_ORG_ES, $this->description_org_es);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_ES)) $criteria->add(CommonOrganismePeer::ADRESSE_ES, $this->adresse_es);
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_ES)) $criteria->add(CommonOrganismePeer::VILLE_ES, $this->ville_es);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_ES)) $criteria->add(CommonOrganismePeer::ADRESSE2_ES, $this->adresse2_es);
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_ES)) $criteria->add(CommonOrganismePeer::PAYS_ES, $this->pays_es);
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_EN)) $criteria->add(CommonOrganismePeer::DENOMINATION_ORG_EN, $this->denomination_org_en);
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_EN)) $criteria->add(CommonOrganismePeer::DESCRIPTION_ORG_EN, $this->description_org_en);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_EN)) $criteria->add(CommonOrganismePeer::ADRESSE_EN, $this->adresse_en);
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_EN)) $criteria->add(CommonOrganismePeer::VILLE_EN, $this->ville_en);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_EN)) $criteria->add(CommonOrganismePeer::ADRESSE2_EN, $this->adresse2_en);
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_EN)) $criteria->add(CommonOrganismePeer::PAYS_EN, $this->pays_en);
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_SU)) $criteria->add(CommonOrganismePeer::DENOMINATION_ORG_SU, $this->denomination_org_su);
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_SU)) $criteria->add(CommonOrganismePeer::DESCRIPTION_ORG_SU, $this->description_org_su);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_SU)) $criteria->add(CommonOrganismePeer::ADRESSE_SU, $this->adresse_su);
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_SU)) $criteria->add(CommonOrganismePeer::VILLE_SU, $this->ville_su);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_SU)) $criteria->add(CommonOrganismePeer::ADRESSE2_SU, $this->adresse2_su);
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_SU)) $criteria->add(CommonOrganismePeer::PAYS_SU, $this->pays_su);
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_DU)) $criteria->add(CommonOrganismePeer::DENOMINATION_ORG_DU, $this->denomination_org_du);
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_DU)) $criteria->add(CommonOrganismePeer::DESCRIPTION_ORG_DU, $this->description_org_du);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_DU)) $criteria->add(CommonOrganismePeer::ADRESSE_DU, $this->adresse_du);
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_DU)) $criteria->add(CommonOrganismePeer::VILLE_DU, $this->ville_du);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_DU)) $criteria->add(CommonOrganismePeer::ADRESSE2_DU, $this->adresse2_du);
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_DU)) $criteria->add(CommonOrganismePeer::PAYS_DU, $this->pays_du);
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_CZ)) $criteria->add(CommonOrganismePeer::DENOMINATION_ORG_CZ, $this->denomination_org_cz);
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_CZ)) $criteria->add(CommonOrganismePeer::DESCRIPTION_ORG_CZ, $this->description_org_cz);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_CZ)) $criteria->add(CommonOrganismePeer::ADRESSE_CZ, $this->adresse_cz);
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_CZ)) $criteria->add(CommonOrganismePeer::VILLE_CZ, $this->ville_cz);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_CZ)) $criteria->add(CommonOrganismePeer::ADRESSE2_CZ, $this->adresse2_cz);
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_CZ)) $criteria->add(CommonOrganismePeer::PAYS_CZ, $this->pays_cz);
        if ($this->isColumnModified(CommonOrganismePeer::DENOMINATION_ORG_IT)) $criteria->add(CommonOrganismePeer::DENOMINATION_ORG_IT, $this->denomination_org_it);
        if ($this->isColumnModified(CommonOrganismePeer::DESCRIPTION_ORG_IT)) $criteria->add(CommonOrganismePeer::DESCRIPTION_ORG_IT, $this->description_org_it);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE_IT)) $criteria->add(CommonOrganismePeer::ADRESSE_IT, $this->adresse_it);
        if ($this->isColumnModified(CommonOrganismePeer::VILLE_IT)) $criteria->add(CommonOrganismePeer::VILLE_IT, $this->ville_it);
        if ($this->isColumnModified(CommonOrganismePeer::ADRESSE2_IT)) $criteria->add(CommonOrganismePeer::ADRESSE2_IT, $this->adresse2_it);
        if ($this->isColumnModified(CommonOrganismePeer::PAYS_IT)) $criteria->add(CommonOrganismePeer::PAYS_IT, $this->pays_it);
        if ($this->isColumnModified(CommonOrganismePeer::SIREN)) $criteria->add(CommonOrganismePeer::SIREN, $this->siren);
        if ($this->isColumnModified(CommonOrganismePeer::COMPLEMENT)) $criteria->add(CommonOrganismePeer::COMPLEMENT, $this->complement);
        if ($this->isColumnModified(CommonOrganismePeer::MONITEUR_PROVENANCE)) $criteria->add(CommonOrganismePeer::MONITEUR_PROVENANCE, $this->moniteur_provenance);
        if ($this->isColumnModified(CommonOrganismePeer::CODE_ACCES_LOGICIEL)) $criteria->add(CommonOrganismePeer::CODE_ACCES_LOGICIEL, $this->code_acces_logiciel);
        if ($this->isColumnModified(CommonOrganismePeer::DECALAGE_HORAIRE)) $criteria->add(CommonOrganismePeer::DECALAGE_HORAIRE, $this->decalage_horaire);
        if ($this->isColumnModified(CommonOrganismePeer::LIEU_RESIDENCE)) $criteria->add(CommonOrganismePeer::LIEU_RESIDENCE, $this->lieu_residence);
        if ($this->isColumnModified(CommonOrganismePeer::ACTIVATION_FUSEAU_HORAIRE)) $criteria->add(CommonOrganismePeer::ACTIVATION_FUSEAU_HORAIRE, $this->activation_fuseau_horaire);
        if ($this->isColumnModified(CommonOrganismePeer::ALERTE)) $criteria->add(CommonOrganismePeer::ALERTE, $this->alerte);
        if ($this->isColumnModified(CommonOrganismePeer::ORDRE)) $criteria->add(CommonOrganismePeer::ORDRE, $this->ordre);
        if ($this->isColumnModified(CommonOrganismePeer::URL_INTERFACE_ANM)) $criteria->add(CommonOrganismePeer::URL_INTERFACE_ANM, $this->url_interface_anm);
        if ($this->isColumnModified(CommonOrganismePeer::SOUS_TYPE_ORGANISME)) $criteria->add(CommonOrganismePeer::SOUS_TYPE_ORGANISME, $this->sous_type_organisme);
        if ($this->isColumnModified(CommonOrganismePeer::PF_URL)) $criteria->add(CommonOrganismePeer::PF_URL, $this->pf_url);
        if ($this->isColumnModified(CommonOrganismePeer::TAG_PURGE)) $criteria->add(CommonOrganismePeer::TAG_PURGE, $this->tag_purge);
        if ($this->isColumnModified(CommonOrganismePeer::ID_EXTERNE)) $criteria->add(CommonOrganismePeer::ID_EXTERNE, $this->id_externe);
        if ($this->isColumnModified(CommonOrganismePeer::ID_ENTITE)) $criteria->add(CommonOrganismePeer::ID_ENTITE, $this->id_entite);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonOrganismePeer::DATABASE_NAME);
        $criteria->add(CommonOrganismePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonOrganisme (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setAcronyme($this->getAcronyme());
        $copyObj->setTypeArticleOrg($this->getTypeArticleOrg());
        $copyObj->setDenominationOrg($this->getDenominationOrg());
        $copyObj->setCategorieInsee($this->getCategorieInsee());
        $copyObj->setDescriptionOrg($this->getDescriptionOrg());
        $copyObj->setAdresse($this->getAdresse());
        $copyObj->setCp($this->getCp());
        $copyObj->setVille($this->getVille());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setIdAttribFile($this->getIdAttribFile());
        $copyObj->setAttribFile($this->getAttribFile());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setActive($this->getActive());
        $copyObj->setIdClientAnm($this->getIdClientAnm());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setSignataireCao($this->getSignataireCao());
        $copyObj->setSigle($this->getSigle());
        $copyObj->setAdresse2($this->getAdresse2());
        $copyObj->setTel($this->getTel());
        $copyObj->setTelecopie($this->getTelecopie());
        $copyObj->setPays($this->getPays());
        $copyObj->setAffichageEntite($this->getAffichageEntite());
        $copyObj->setIdInitial($this->getIdInitial());
        $copyObj->setDenominationOrgAr($this->getDenominationOrgAr());
        $copyObj->setDescriptionOrgAr($this->getDescriptionOrgAr());
        $copyObj->setAdresseAr($this->getAdresseAr());
        $copyObj->setVilleAr($this->getVilleAr());
        $copyObj->setAdresse2Ar($this->getAdresse2Ar());
        $copyObj->setPaysAr($this->getPaysAr());
        $copyObj->setDenominationOrgFr($this->getDenominationOrgFr());
        $copyObj->setDescriptionOrgFr($this->getDescriptionOrgFr());
        $copyObj->setAdresseFr($this->getAdresseFr());
        $copyObj->setVilleFr($this->getVilleFr());
        $copyObj->setAdresse2Fr($this->getAdresse2Fr());
        $copyObj->setPaysFr($this->getPaysFr());
        $copyObj->setDenominationOrgEs($this->getDenominationOrgEs());
        $copyObj->setDescriptionOrgEs($this->getDescriptionOrgEs());
        $copyObj->setAdresseEs($this->getAdresseEs());
        $copyObj->setVilleEs($this->getVilleEs());
        $copyObj->setAdresse2Es($this->getAdresse2Es());
        $copyObj->setPaysEs($this->getPaysEs());
        $copyObj->setDenominationOrgEn($this->getDenominationOrgEn());
        $copyObj->setDescriptionOrgEn($this->getDescriptionOrgEn());
        $copyObj->setAdresseEn($this->getAdresseEn());
        $copyObj->setVilleEn($this->getVilleEn());
        $copyObj->setAdresse2En($this->getAdresse2En());
        $copyObj->setPaysEn($this->getPaysEn());
        $copyObj->setDenominationOrgSu($this->getDenominationOrgSu());
        $copyObj->setDescriptionOrgSu($this->getDescriptionOrgSu());
        $copyObj->setAdresseSu($this->getAdresseSu());
        $copyObj->setVilleSu($this->getVilleSu());
        $copyObj->setAdresse2Su($this->getAdresse2Su());
        $copyObj->setPaysSu($this->getPaysSu());
        $copyObj->setDenominationOrgDu($this->getDenominationOrgDu());
        $copyObj->setDescriptionOrgDu($this->getDescriptionOrgDu());
        $copyObj->setAdresseDu($this->getAdresseDu());
        $copyObj->setVilleDu($this->getVilleDu());
        $copyObj->setAdresse2Du($this->getAdresse2Du());
        $copyObj->setPaysDu($this->getPaysDu());
        $copyObj->setDenominationOrgCz($this->getDenominationOrgCz());
        $copyObj->setDescriptionOrgCz($this->getDescriptionOrgCz());
        $copyObj->setAdresseCz($this->getAdresseCz());
        $copyObj->setVilleCz($this->getVilleCz());
        $copyObj->setAdresse2Cz($this->getAdresse2Cz());
        $copyObj->setPaysCz($this->getPaysCz());
        $copyObj->setDenominationOrgIt($this->getDenominationOrgIt());
        $copyObj->setDescriptionOrgIt($this->getDescriptionOrgIt());
        $copyObj->setAdresseIt($this->getAdresseIt());
        $copyObj->setVilleIt($this->getVilleIt());
        $copyObj->setAdresse2It($this->getAdresse2It());
        $copyObj->setPaysIt($this->getPaysIt());
        $copyObj->setSiren($this->getSiren());
        $copyObj->setComplement($this->getComplement());
        $copyObj->setMoniteurProvenance($this->getMoniteurProvenance());
        $copyObj->setCodeAccesLogiciel($this->getCodeAccesLogiciel());
        $copyObj->setDecalageHoraire($this->getDecalageHoraire());
        $copyObj->setLieuResidence($this->getLieuResidence());
        $copyObj->setActivationFuseauHoraire($this->getActivationFuseauHoraire());
        $copyObj->setAlerte($this->getAlerte());
        $copyObj->setOrdre($this->getOrdre());
        $copyObj->setUrlInterfaceAnm($this->getUrlInterfaceAnm());
        $copyObj->setSousTypeOrganisme($this->getSousTypeOrganisme());
        $copyObj->setPfUrl($this->getPfUrl());
        $copyObj->setTagPurge($this->getTagPurge());
        $copyObj->setIdExterne($this->getIdExterne());
        $copyObj->setIdEntite($this->getIdEntite());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonAdministrateurs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAdministrateur($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonAffiliationServices() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAffiliationService($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonAgents() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAgent($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEchanges() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchange($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonOrganismeServiceMetiers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonOrganismeServiceMetier($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonParametrageEncheres() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonParametrageEnchere($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonRPAs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonRPA($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTelechargements() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTelechargement($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTierss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTiers($relObj->copy($deepCopy));
                }
            }

            $relObj = $this->getCommonConfigurationOrganisme();
            if ($relObj) {
                $copyObj->setCommonConfigurationOrganisme($relObj->copy($deepCopy));
            }

            foreach ($this->getCommonConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonDestinataireMiseDispositions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonDestinataireMiseDisposition($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEchangeDocApplicationClientOrganismes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDocApplicationClientOrganisme($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonInvitePermanentTransverses() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonInvitePermanentTransverse($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonPlateformeVirtuelleOrganismes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonPlateformeVirtuelleOrganisme($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOCommissions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOCommission($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOCommissionAgents() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOCommissionAgent($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOCommissionIntervenantExternes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOCommissionIntervenantExterne($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOIntervenantExternes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOIntervenantExterne($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOOrdreDuJours() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOOrdreDuJour($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOOrdreDuJourIntervenants() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOOrdreDuJourIntervenant($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOSeances() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOSeance($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOSeanceAgents() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOSeanceAgent($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOSeanceIntervenantExternes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOSeanceIntervenantExterne($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCandidatures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCandidature($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTDumeContextes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTDumeContexte($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTListeLotsCandidatures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTListeLotsCandidature($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTVisionRmaAgentOrganismes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTVisionRmaAgentOrganisme($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonOrganisme Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonOrganismePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonOrganismePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonCategorieINSEE object.
     *
     * @param   CommonCategorieINSEE $v
     * @return CommonOrganisme The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonCategorieINSEE(CommonCategorieINSEE $v = null)
    {
        if ($v === null) {
            $this->setCategorieInsee(NULL);
        } else {
            $this->setCategorieInsee($v->getId());
        }

        $this->aCommonCategorieINSEE = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonCategorieINSEE object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonOrganisme($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonCategorieINSEE object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonCategorieINSEE The associated CommonCategorieINSEE object.
     * @throws PropelException
     */
    public function getCommonCategorieINSEE(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonCategorieINSEE === null && (($this->categorie_insee !== "" && $this->categorie_insee !== null)) && $doQuery) {
            $this->aCommonCategorieINSEE = CommonCategorieINSEEQuery::create()->findPk($this->categorie_insee, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonCategorieINSEE->addCommonOrganismes($this);
             */
        }

        return $this->aCommonCategorieINSEE;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonAdministrateur' == $relationName) {
            $this->initCommonAdministrateurs();
        }
        if ('CommonAffiliationService' == $relationName) {
            $this->initCommonAffiliationServices();
        }
        if ('CommonAgent' == $relationName) {
            $this->initCommonAgents();
        }
        if ('CommonEchange' == $relationName) {
            $this->initCommonEchanges();
        }
        if ('CommonOrganismeServiceMetier' == $relationName) {
            $this->initCommonOrganismeServiceMetiers();
        }
        if ('CommonParametrageEnchere' == $relationName) {
            $this->initCommonParametrageEncheres();
        }
        if ('CommonRPA' == $relationName) {
            $this->initCommonRPAs();
        }
        if ('CommonTelechargement' == $relationName) {
            $this->initCommonTelechargements();
        }
        if ('CommonTiers' == $relationName) {
            $this->initCommonTierss();
        }
        if ('CommonConsultation' == $relationName) {
            $this->initCommonConsultations();
        }
        if ('CommonDestinataireMiseDisposition' == $relationName) {
            $this->initCommonDestinataireMiseDispositions();
        }
        if ('CommonEchangeDocApplicationClientOrganisme' == $relationName) {
            $this->initCommonEchangeDocApplicationClientOrganismes();
        }
        if ('CommonInvitePermanentTransverse' == $relationName) {
            $this->initCommonInvitePermanentTransverses();
        }
        if ('CommonPlateformeVirtuelleOrganisme' == $relationName) {
            $this->initCommonPlateformeVirtuelleOrganismes();
        }
        if ('CommonTCAOCommission' == $relationName) {
            $this->initCommonTCAOCommissions();
        }
        if ('CommonTCAOCommissionAgent' == $relationName) {
            $this->initCommonTCAOCommissionAgents();
        }
        if ('CommonTCAOCommissionIntervenantExterne' == $relationName) {
            $this->initCommonTCAOCommissionIntervenantExternes();
        }
        if ('CommonTCAOIntervenantExterne' == $relationName) {
            $this->initCommonTCAOIntervenantExternes();
        }
        if ('CommonTCAOOrdreDuJour' == $relationName) {
            $this->initCommonTCAOOrdreDuJours();
        }
        if ('CommonTCAOOrdreDuJourIntervenant' == $relationName) {
            $this->initCommonTCAOOrdreDuJourIntervenants();
        }
        if ('CommonTCAOSeance' == $relationName) {
            $this->initCommonTCAOSeances();
        }
        if ('CommonTCAOSeanceAgent' == $relationName) {
            $this->initCommonTCAOSeanceAgents();
        }
        if ('CommonTCAOSeanceIntervenantExterne' == $relationName) {
            $this->initCommonTCAOSeanceIntervenantExternes();
        }
        if ('CommonTCandidature' == $relationName) {
            $this->initCommonTCandidatures();
        }
        if ('CommonTDumeContexte' == $relationName) {
            $this->initCommonTDumeContextes();
        }
        if ('CommonTListeLotsCandidature' == $relationName) {
            $this->initCommonTListeLotsCandidatures();
        }
        if ('CommonTVisionRmaAgentOrganisme' == $relationName) {
            $this->initCommonTVisionRmaAgentOrganismes();
        }
    }

    /**
     * Clears out the collCommonAdministrateurs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonAdministrateurs()
     */
    public function clearCommonAdministrateurs()
    {
        $this->collCommonAdministrateurs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAdministrateursPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAdministrateurs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAdministrateurs($v = true)
    {
        $this->collCommonAdministrateursPartial = $v;
    }

    /**
     * Initializes the collCommonAdministrateurs collection.
     *
     * By default this just sets the collCommonAdministrateurs collection to an empty array (like clearcollCommonAdministrateurs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAdministrateurs($overrideExisting = true)
    {
        if (null !== $this->collCommonAdministrateurs && !$overrideExisting) {
            return;
        }
        $this->collCommonAdministrateurs = new PropelObjectCollection();
        $this->collCommonAdministrateurs->setModel('CommonAdministrateur');
    }

    /**
     * Gets an array of CommonAdministrateur objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAdministrateur[] List of CommonAdministrateur objects
     * @throws PropelException
     */
    public function getCommonAdministrateurs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAdministrateursPartial && !$this->isNew();
        if (null === $this->collCommonAdministrateurs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAdministrateurs) {
                // return empty collection
                $this->initCommonAdministrateurs();
            } else {
                $collCommonAdministrateurs = CommonAdministrateurQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAdministrateursPartial && count($collCommonAdministrateurs)) {
                      $this->initCommonAdministrateurs(false);

                      foreach ($collCommonAdministrateurs as $obj) {
                        if (false == $this->collCommonAdministrateurs->contains($obj)) {
                          $this->collCommonAdministrateurs->append($obj);
                        }
                      }

                      $this->collCommonAdministrateursPartial = true;
                    }

                    $collCommonAdministrateurs->getInternalIterator()->rewind();

                    return $collCommonAdministrateurs;
                }

                if ($partial && $this->collCommonAdministrateurs) {
                    foreach ($this->collCommonAdministrateurs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAdministrateurs[] = $obj;
                        }
                    }
                }

                $this->collCommonAdministrateurs = $collCommonAdministrateurs;
                $this->collCommonAdministrateursPartial = false;
            }
        }

        return $this->collCommonAdministrateurs;
    }

    /**
     * Sets a collection of CommonAdministrateur objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAdministrateurs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonAdministrateurs(PropelCollection $commonAdministrateurs, PropelPDO $con = null)
    {
        $commonAdministrateursToDelete = $this->getCommonAdministrateurs(new Criteria(), $con)->diff($commonAdministrateurs);


        $this->commonAdministrateursScheduledForDeletion = $commonAdministrateursToDelete;

        foreach ($commonAdministrateursToDelete as $commonAdministrateurRemoved) {
            $commonAdministrateurRemoved->setCommonOrganisme(null);
        }

        $this->collCommonAdministrateurs = null;
        foreach ($commonAdministrateurs as $commonAdministrateur) {
            $this->addCommonAdministrateur($commonAdministrateur);
        }

        $this->collCommonAdministrateurs = $commonAdministrateurs;
        $this->collCommonAdministrateursPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAdministrateur objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAdministrateur objects.
     * @throws PropelException
     */
    public function countCommonAdministrateurs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAdministrateursPartial && !$this->isNew();
        if (null === $this->collCommonAdministrateurs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAdministrateurs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAdministrateurs());
            }
            $query = CommonAdministrateurQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonAdministrateurs);
    }

    /**
     * Method called to associate a CommonAdministrateur object to this object
     * through the CommonAdministrateur foreign key attribute.
     *
     * @param   CommonAdministrateur $l CommonAdministrateur
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonAdministrateur(CommonAdministrateur $l)
    {
        if ($this->collCommonAdministrateurs === null) {
            $this->initCommonAdministrateurs();
            $this->collCommonAdministrateursPartial = true;
        }
        if (!in_array($l, $this->collCommonAdministrateurs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAdministrateur($l);
        }

        return $this;
    }

    /**
     * @param	CommonAdministrateur $commonAdministrateur The commonAdministrateur object to add.
     */
    protected function doAddCommonAdministrateur($commonAdministrateur)
    {
        $this->collCommonAdministrateurs[]= $commonAdministrateur;
        $commonAdministrateur->setCommonOrganisme($this);
    }

    /**
     * @param	CommonAdministrateur $commonAdministrateur The commonAdministrateur object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonAdministrateur($commonAdministrateur)
    {
        if ($this->getCommonAdministrateurs()->contains($commonAdministrateur)) {
            $this->collCommonAdministrateurs->remove($this->collCommonAdministrateurs->search($commonAdministrateur));
            if (null === $this->commonAdministrateursScheduledForDeletion) {
                $this->commonAdministrateursScheduledForDeletion = clone $this->collCommonAdministrateurs;
                $this->commonAdministrateursScheduledForDeletion->clear();
            }
            $this->commonAdministrateursScheduledForDeletion[]= $commonAdministrateur;
            $commonAdministrateur->setCommonOrganisme(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonAffiliationServices collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonAffiliationServices()
     */
    public function clearCommonAffiliationServices()
    {
        $this->collCommonAffiliationServices = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAffiliationServicesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAffiliationServices collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAffiliationServices($v = true)
    {
        $this->collCommonAffiliationServicesPartial = $v;
    }

    /**
     * Initializes the collCommonAffiliationServices collection.
     *
     * By default this just sets the collCommonAffiliationServices collection to an empty array (like clearcollCommonAffiliationServices());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAffiliationServices($overrideExisting = true)
    {
        if (null !== $this->collCommonAffiliationServices && !$overrideExisting) {
            return;
        }
        $this->collCommonAffiliationServices = new PropelObjectCollection();
        $this->collCommonAffiliationServices->setModel('CommonAffiliationService');
    }

    /**
     * Gets an array of CommonAffiliationService objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAffiliationService[] List of CommonAffiliationService objects
     * @throws PropelException
     */
    public function getCommonAffiliationServices($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAffiliationServicesPartial && !$this->isNew();
        if (null === $this->collCommonAffiliationServices || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAffiliationServices) {
                // return empty collection
                $this->initCommonAffiliationServices();
            } else {
                $collCommonAffiliationServices = CommonAffiliationServiceQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAffiliationServicesPartial && count($collCommonAffiliationServices)) {
                      $this->initCommonAffiliationServices(false);

                      foreach ($collCommonAffiliationServices as $obj) {
                        if (false == $this->collCommonAffiliationServices->contains($obj)) {
                          $this->collCommonAffiliationServices->append($obj);
                        }
                      }

                      $this->collCommonAffiliationServicesPartial = true;
                    }

                    $collCommonAffiliationServices->getInternalIterator()->rewind();

                    return $collCommonAffiliationServices;
                }

                if ($partial && $this->collCommonAffiliationServices) {
                    foreach ($this->collCommonAffiliationServices as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAffiliationServices[] = $obj;
                        }
                    }
                }

                $this->collCommonAffiliationServices = $collCommonAffiliationServices;
                $this->collCommonAffiliationServicesPartial = false;
            }
        }

        return $this->collCommonAffiliationServices;
    }

    /**
     * Sets a collection of CommonAffiliationService objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAffiliationServices A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonAffiliationServices(PropelCollection $commonAffiliationServices, PropelPDO $con = null)
    {
        $commonAffiliationServicesToDelete = $this->getCommonAffiliationServices(new Criteria(), $con)->diff($commonAffiliationServices);


        $this->commonAffiliationServicesScheduledForDeletion = $commonAffiliationServicesToDelete;

        foreach ($commonAffiliationServicesToDelete as $commonAffiliationServiceRemoved) {
            $commonAffiliationServiceRemoved->setCommonOrganisme(null);
        }

        $this->collCommonAffiliationServices = null;
        foreach ($commonAffiliationServices as $commonAffiliationService) {
            $this->addCommonAffiliationService($commonAffiliationService);
        }

        $this->collCommonAffiliationServices = $commonAffiliationServices;
        $this->collCommonAffiliationServicesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAffiliationService objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAffiliationService objects.
     * @throws PropelException
     */
    public function countCommonAffiliationServices(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAffiliationServicesPartial && !$this->isNew();
        if (null === $this->collCommonAffiliationServices || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAffiliationServices) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAffiliationServices());
            }
            $query = CommonAffiliationServiceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonAffiliationServices);
    }

    /**
     * Method called to associate a CommonAffiliationService object to this object
     * through the CommonAffiliationService foreign key attribute.
     *
     * @param   CommonAffiliationService $l CommonAffiliationService
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonAffiliationService(CommonAffiliationService $l)
    {
        if ($this->collCommonAffiliationServices === null) {
            $this->initCommonAffiliationServices();
            $this->collCommonAffiliationServicesPartial = true;
        }
        if (!in_array($l, $this->collCommonAffiliationServices->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAffiliationService($l);
        }

        return $this;
    }

    /**
     * @param	CommonAffiliationService $commonAffiliationService The commonAffiliationService object to add.
     */
    protected function doAddCommonAffiliationService($commonAffiliationService)
    {
        $this->collCommonAffiliationServices[]= $commonAffiliationService;
        $commonAffiliationService->setCommonOrganisme($this);
    }

    /**
     * @param	CommonAffiliationService $commonAffiliationService The commonAffiliationService object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonAffiliationService($commonAffiliationService)
    {
        if ($this->getCommonAffiliationServices()->contains($commonAffiliationService)) {
            $this->collCommonAffiliationServices->remove($this->collCommonAffiliationServices->search($commonAffiliationService));
            if (null === $this->commonAffiliationServicesScheduledForDeletion) {
                $this->commonAffiliationServicesScheduledForDeletion = clone $this->collCommonAffiliationServices;
                $this->commonAffiliationServicesScheduledForDeletion->clear();
            }
            $this->commonAffiliationServicesScheduledForDeletion[]= clone $commonAffiliationService;
            $commonAffiliationService->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonAffiliationServices from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAffiliationService[] List of CommonAffiliationService objects
     */
    public function getCommonAffiliationServicesJoinCommonServiceRelatedByServiceId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAffiliationServiceQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceId', $join_behavior);

        return $this->getCommonAffiliationServices($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonAffiliationServices from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAffiliationService[] List of CommonAffiliationService objects
     */
    public function getCommonAffiliationServicesJoinCommonServiceRelatedByServiceParentId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAffiliationServiceQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceParentId', $join_behavior);

        return $this->getCommonAffiliationServices($query, $con);
    }

    /**
     * Clears out the collCommonAgents collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonAgents()
     */
    public function clearCommonAgents()
    {
        $this->collCommonAgents = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAgentsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAgents collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAgents($v = true)
    {
        $this->collCommonAgentsPartial = $v;
    }

    /**
     * Initializes the collCommonAgents collection.
     *
     * By default this just sets the collCommonAgents collection to an empty array (like clearcollCommonAgents());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAgents($overrideExisting = true)
    {
        if (null !== $this->collCommonAgents && !$overrideExisting) {
            return;
        }
        $this->collCommonAgents = new PropelObjectCollection();
        $this->collCommonAgents->setModel('CommonAgent');
    }

    /**
     * Gets an array of CommonAgent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAgent[] List of CommonAgent objects
     * @throws PropelException
     */
    public function getCommonAgents($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentsPartial && !$this->isNew();
        if (null === $this->collCommonAgents || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAgents) {
                // return empty collection
                $this->initCommonAgents();
            } else {
                $collCommonAgents = CommonAgentQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAgentsPartial && count($collCommonAgents)) {
                      $this->initCommonAgents(false);

                      foreach ($collCommonAgents as $obj) {
                        if (false == $this->collCommonAgents->contains($obj)) {
                          $this->collCommonAgents->append($obj);
                        }
                      }

                      $this->collCommonAgentsPartial = true;
                    }

                    $collCommonAgents->getInternalIterator()->rewind();

                    return $collCommonAgents;
                }

                if ($partial && $this->collCommonAgents) {
                    foreach ($this->collCommonAgents as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAgents[] = $obj;
                        }
                    }
                }

                $this->collCommonAgents = $collCommonAgents;
                $this->collCommonAgentsPartial = false;
            }
        }

        return $this->collCommonAgents;
    }

    /**
     * Sets a collection of CommonAgent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAgents A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonAgents(PropelCollection $commonAgents, PropelPDO $con = null)
    {
        $commonAgentsToDelete = $this->getCommonAgents(new Criteria(), $con)->diff($commonAgents);


        $this->commonAgentsScheduledForDeletion = $commonAgentsToDelete;

        foreach ($commonAgentsToDelete as $commonAgentRemoved) {
            $commonAgentRemoved->setCommonOrganisme(null);
        }

        $this->collCommonAgents = null;
        foreach ($commonAgents as $commonAgent) {
            $this->addCommonAgent($commonAgent);
        }

        $this->collCommonAgents = $commonAgents;
        $this->collCommonAgentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAgent objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAgent objects.
     * @throws PropelException
     */
    public function countCommonAgents(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentsPartial && !$this->isNew();
        if (null === $this->collCommonAgents || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAgents) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAgents());
            }
            $query = CommonAgentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonAgents);
    }

    /**
     * Method called to associate a CommonAgent object to this object
     * through the CommonAgent foreign key attribute.
     *
     * @param   CommonAgent $l CommonAgent
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonAgent(CommonAgent $l)
    {
        if ($this->collCommonAgents === null) {
            $this->initCommonAgents();
            $this->collCommonAgentsPartial = true;
        }
        if (!in_array($l, $this->collCommonAgents->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAgent($l);
        }

        return $this;
    }

    /**
     * @param	CommonAgent $commonAgent The commonAgent object to add.
     */
    protected function doAddCommonAgent($commonAgent)
    {
        $this->collCommonAgents[]= $commonAgent;
        $commonAgent->setCommonOrganisme($this);
    }

    /**
     * @param	CommonAgent $commonAgent The commonAgent object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonAgent($commonAgent)
    {
        if ($this->getCommonAgents()->contains($commonAgent)) {
            $this->collCommonAgents->remove($this->collCommonAgents->search($commonAgent));
            if (null === $this->commonAgentsScheduledForDeletion) {
                $this->commonAgentsScheduledForDeletion = clone $this->collCommonAgents;
                $this->commonAgentsScheduledForDeletion->clear();
            }
            $this->commonAgentsScheduledForDeletion[]= $commonAgent;
            $commonAgent->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAgent[] List of CommonAgent objects
     */
    public function getCommonAgentsJoinCommonService($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAgentQuery::create(null, $criteria);
        $query->joinWith('CommonService', $join_behavior);

        return $this->getCommonAgents($query, $con);
    }

    /**
     * Clears out the collCommonEchanges collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonEchanges()
     */
    public function clearCommonEchanges()
    {
        $this->collCommonEchanges = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchanges collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchanges($v = true)
    {
        $this->collCommonEchangesPartial = $v;
    }

    /**
     * Initializes the collCommonEchanges collection.
     *
     * By default this just sets the collCommonEchanges collection to an empty array (like clearcollCommonEchanges());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchanges($overrideExisting = true)
    {
        if (null !== $this->collCommonEchanges && !$overrideExisting) {
            return;
        }
        $this->collCommonEchanges = new PropelObjectCollection();
        $this->collCommonEchanges->setModel('CommonEchange');
    }

    /**
     * Gets an array of CommonEchange objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchange[] List of CommonEchange objects
     * @throws PropelException
     */
    public function getCommonEchanges($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangesPartial && !$this->isNew();
        if (null === $this->collCommonEchanges || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchanges) {
                // return empty collection
                $this->initCommonEchanges();
            } else {
                $collCommonEchanges = CommonEchangeQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangesPartial && count($collCommonEchanges)) {
                      $this->initCommonEchanges(false);

                      foreach ($collCommonEchanges as $obj) {
                        if (false == $this->collCommonEchanges->contains($obj)) {
                          $this->collCommonEchanges->append($obj);
                        }
                      }

                      $this->collCommonEchangesPartial = true;
                    }

                    $collCommonEchanges->getInternalIterator()->rewind();

                    return $collCommonEchanges;
                }

                if ($partial && $this->collCommonEchanges) {
                    foreach ($this->collCommonEchanges as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchanges[] = $obj;
                        }
                    }
                }

                $this->collCommonEchanges = $collCommonEchanges;
                $this->collCommonEchangesPartial = false;
            }
        }

        return $this->collCommonEchanges;
    }

    /**
     * Sets a collection of CommonEchange objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchanges A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonEchanges(PropelCollection $commonEchanges, PropelPDO $con = null)
    {
        $commonEchangesToDelete = $this->getCommonEchanges(new Criteria(), $con)->diff($commonEchanges);


        $this->commonEchangesScheduledForDeletion = $commonEchangesToDelete;

        foreach ($commonEchangesToDelete as $commonEchangeRemoved) {
            $commonEchangeRemoved->setCommonOrganisme(null);
        }

        $this->collCommonEchanges = null;
        foreach ($commonEchanges as $commonEchange) {
            $this->addCommonEchange($commonEchange);
        }

        $this->collCommonEchanges = $commonEchanges;
        $this->collCommonEchangesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchange objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchange objects.
     * @throws PropelException
     */
    public function countCommonEchanges(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangesPartial && !$this->isNew();
        if (null === $this->collCommonEchanges || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchanges) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchanges());
            }
            $query = CommonEchangeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonEchanges);
    }

    /**
     * Method called to associate a CommonEchange object to this object
     * through the CommonEchange foreign key attribute.
     *
     * @param   CommonEchange $l CommonEchange
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonEchange(CommonEchange $l)
    {
        if ($this->collCommonEchanges === null) {
            $this->initCommonEchanges();
            $this->collCommonEchangesPartial = true;
        }
        if (!in_array($l, $this->collCommonEchanges->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchange($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchange $commonEchange The commonEchange object to add.
     */
    protected function doAddCommonEchange($commonEchange)
    {
        $this->collCommonEchanges[]= $commonEchange;
        $commonEchange->setCommonOrganisme($this);
    }

    /**
     * @param	CommonEchange $commonEchange The commonEchange object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonEchange($commonEchange)
    {
        if ($this->getCommonEchanges()->contains($commonEchange)) {
            $this->collCommonEchanges->remove($this->collCommonEchanges->search($commonEchange));
            if (null === $this->commonEchangesScheduledForDeletion) {
                $this->commonEchangesScheduledForDeletion = clone $this->collCommonEchanges;
                $this->commonEchangesScheduledForDeletion->clear();
            }
            $this->commonEchangesScheduledForDeletion[]= $commonEchange;
            $commonEchange->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonEchanges from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchange[] List of CommonEchange objects
     */
    public function getCommonEchangesJoinCommonDossierVolumineux($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeQuery::create(null, $criteria);
        $query->joinWith('CommonDossierVolumineux', $join_behavior);

        return $this->getCommonEchanges($query, $con);
    }

    /**
     * Clears out the collCommonOrganismeServiceMetiers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonOrganismeServiceMetiers()
     */
    public function clearCommonOrganismeServiceMetiers()
    {
        $this->collCommonOrganismeServiceMetiers = null; // important to set this to null since that means it is uninitialized
        $this->collCommonOrganismeServiceMetiersPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonOrganismeServiceMetiers collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonOrganismeServiceMetiers($v = true)
    {
        $this->collCommonOrganismeServiceMetiersPartial = $v;
    }

    /**
     * Initializes the collCommonOrganismeServiceMetiers collection.
     *
     * By default this just sets the collCommonOrganismeServiceMetiers collection to an empty array (like clearcollCommonOrganismeServiceMetiers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonOrganismeServiceMetiers($overrideExisting = true)
    {
        if (null !== $this->collCommonOrganismeServiceMetiers && !$overrideExisting) {
            return;
        }
        $this->collCommonOrganismeServiceMetiers = new PropelObjectCollection();
        $this->collCommonOrganismeServiceMetiers->setModel('CommonOrganismeServiceMetier');
    }

    /**
     * Gets an array of CommonOrganismeServiceMetier objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonOrganismeServiceMetier[] List of CommonOrganismeServiceMetier objects
     * @throws PropelException
     */
    public function getCommonOrganismeServiceMetiers($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonOrganismeServiceMetiersPartial && !$this->isNew();
        if (null === $this->collCommonOrganismeServiceMetiers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonOrganismeServiceMetiers) {
                // return empty collection
                $this->initCommonOrganismeServiceMetiers();
            } else {
                $collCommonOrganismeServiceMetiers = CommonOrganismeServiceMetierQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonOrganismeServiceMetiersPartial && count($collCommonOrganismeServiceMetiers)) {
                      $this->initCommonOrganismeServiceMetiers(false);

                      foreach ($collCommonOrganismeServiceMetiers as $obj) {
                        if (false == $this->collCommonOrganismeServiceMetiers->contains($obj)) {
                          $this->collCommonOrganismeServiceMetiers->append($obj);
                        }
                      }

                      $this->collCommonOrganismeServiceMetiersPartial = true;
                    }

                    $collCommonOrganismeServiceMetiers->getInternalIterator()->rewind();

                    return $collCommonOrganismeServiceMetiers;
                }

                if ($partial && $this->collCommonOrganismeServiceMetiers) {
                    foreach ($this->collCommonOrganismeServiceMetiers as $obj) {
                        if ($obj->isNew()) {
                            $collCommonOrganismeServiceMetiers[] = $obj;
                        }
                    }
                }

                $this->collCommonOrganismeServiceMetiers = $collCommonOrganismeServiceMetiers;
                $this->collCommonOrganismeServiceMetiersPartial = false;
            }
        }

        return $this->collCommonOrganismeServiceMetiers;
    }

    /**
     * Sets a collection of CommonOrganismeServiceMetier objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonOrganismeServiceMetiers A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonOrganismeServiceMetiers(PropelCollection $commonOrganismeServiceMetiers, PropelPDO $con = null)
    {
        $commonOrganismeServiceMetiersToDelete = $this->getCommonOrganismeServiceMetiers(new Criteria(), $con)->diff($commonOrganismeServiceMetiers);


        $this->commonOrganismeServiceMetiersScheduledForDeletion = $commonOrganismeServiceMetiersToDelete;

        foreach ($commonOrganismeServiceMetiersToDelete as $commonOrganismeServiceMetierRemoved) {
            $commonOrganismeServiceMetierRemoved->setCommonOrganisme(null);
        }

        $this->collCommonOrganismeServiceMetiers = null;
        foreach ($commonOrganismeServiceMetiers as $commonOrganismeServiceMetier) {
            $this->addCommonOrganismeServiceMetier($commonOrganismeServiceMetier);
        }

        $this->collCommonOrganismeServiceMetiers = $commonOrganismeServiceMetiers;
        $this->collCommonOrganismeServiceMetiersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonOrganismeServiceMetier objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonOrganismeServiceMetier objects.
     * @throws PropelException
     */
    public function countCommonOrganismeServiceMetiers(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonOrganismeServiceMetiersPartial && !$this->isNew();
        if (null === $this->collCommonOrganismeServiceMetiers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonOrganismeServiceMetiers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonOrganismeServiceMetiers());
            }
            $query = CommonOrganismeServiceMetierQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonOrganismeServiceMetiers);
    }

    /**
     * Method called to associate a CommonOrganismeServiceMetier object to this object
     * through the CommonOrganismeServiceMetier foreign key attribute.
     *
     * @param   CommonOrganismeServiceMetier $l CommonOrganismeServiceMetier
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonOrganismeServiceMetier(CommonOrganismeServiceMetier $l)
    {
        if ($this->collCommonOrganismeServiceMetiers === null) {
            $this->initCommonOrganismeServiceMetiers();
            $this->collCommonOrganismeServiceMetiersPartial = true;
        }
        if (!in_array($l, $this->collCommonOrganismeServiceMetiers->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonOrganismeServiceMetier($l);
        }

        return $this;
    }

    /**
     * @param	CommonOrganismeServiceMetier $commonOrganismeServiceMetier The commonOrganismeServiceMetier object to add.
     */
    protected function doAddCommonOrganismeServiceMetier($commonOrganismeServiceMetier)
    {
        $this->collCommonOrganismeServiceMetiers[]= $commonOrganismeServiceMetier;
        $commonOrganismeServiceMetier->setCommonOrganisme($this);
    }

    /**
     * @param	CommonOrganismeServiceMetier $commonOrganismeServiceMetier The commonOrganismeServiceMetier object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonOrganismeServiceMetier($commonOrganismeServiceMetier)
    {
        if ($this->getCommonOrganismeServiceMetiers()->contains($commonOrganismeServiceMetier)) {
            $this->collCommonOrganismeServiceMetiers->remove($this->collCommonOrganismeServiceMetiers->search($commonOrganismeServiceMetier));
            if (null === $this->commonOrganismeServiceMetiersScheduledForDeletion) {
                $this->commonOrganismeServiceMetiersScheduledForDeletion = clone $this->collCommonOrganismeServiceMetiers;
                $this->commonOrganismeServiceMetiersScheduledForDeletion->clear();
            }
            $this->commonOrganismeServiceMetiersScheduledForDeletion[]= clone $commonOrganismeServiceMetier;
            $commonOrganismeServiceMetier->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonOrganismeServiceMetiers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOrganismeServiceMetier[] List of CommonOrganismeServiceMetier objects
     */
    public function getCommonOrganismeServiceMetiersJoinCommonServiceMertier($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOrganismeServiceMetierQuery::create(null, $criteria);
        $query->joinWith('CommonServiceMertier', $join_behavior);

        return $this->getCommonOrganismeServiceMetiers($query, $con);
    }

    /**
     * Clears out the collCommonParametrageEncheres collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonParametrageEncheres()
     */
    public function clearCommonParametrageEncheres()
    {
        $this->collCommonParametrageEncheres = null; // important to set this to null since that means it is uninitialized
        $this->collCommonParametrageEncheresPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonParametrageEncheres collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonParametrageEncheres($v = true)
    {
        $this->collCommonParametrageEncheresPartial = $v;
    }

    /**
     * Initializes the collCommonParametrageEncheres collection.
     *
     * By default this just sets the collCommonParametrageEncheres collection to an empty array (like clearcollCommonParametrageEncheres());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonParametrageEncheres($overrideExisting = true)
    {
        if (null !== $this->collCommonParametrageEncheres && !$overrideExisting) {
            return;
        }
        $this->collCommonParametrageEncheres = new PropelObjectCollection();
        $this->collCommonParametrageEncheres->setModel('CommonParametrageEnchere');
    }

    /**
     * Gets an array of CommonParametrageEnchere objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonParametrageEnchere[] List of CommonParametrageEnchere objects
     * @throws PropelException
     */
    public function getCommonParametrageEncheres($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonParametrageEncheresPartial && !$this->isNew();
        if (null === $this->collCommonParametrageEncheres || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonParametrageEncheres) {
                // return empty collection
                $this->initCommonParametrageEncheres();
            } else {
                $collCommonParametrageEncheres = CommonParametrageEnchereQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonParametrageEncheresPartial && count($collCommonParametrageEncheres)) {
                      $this->initCommonParametrageEncheres(false);

                      foreach ($collCommonParametrageEncheres as $obj) {
                        if (false == $this->collCommonParametrageEncheres->contains($obj)) {
                          $this->collCommonParametrageEncheres->append($obj);
                        }
                      }

                      $this->collCommonParametrageEncheresPartial = true;
                    }

                    $collCommonParametrageEncheres->getInternalIterator()->rewind();

                    return $collCommonParametrageEncheres;
                }

                if ($partial && $this->collCommonParametrageEncheres) {
                    foreach ($this->collCommonParametrageEncheres as $obj) {
                        if ($obj->isNew()) {
                            $collCommonParametrageEncheres[] = $obj;
                        }
                    }
                }

                $this->collCommonParametrageEncheres = $collCommonParametrageEncheres;
                $this->collCommonParametrageEncheresPartial = false;
            }
        }

        return $this->collCommonParametrageEncheres;
    }

    /**
     * Sets a collection of CommonParametrageEnchere objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonParametrageEncheres A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonParametrageEncheres(PropelCollection $commonParametrageEncheres, PropelPDO $con = null)
    {
        $commonParametrageEncheresToDelete = $this->getCommonParametrageEncheres(new Criteria(), $con)->diff($commonParametrageEncheres);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonParametrageEncheresScheduledForDeletion = clone $commonParametrageEncheresToDelete;

        foreach ($commonParametrageEncheresToDelete as $commonParametrageEnchereRemoved) {
            $commonParametrageEnchereRemoved->setCommonOrganisme(null);
        }

        $this->collCommonParametrageEncheres = null;
        foreach ($commonParametrageEncheres as $commonParametrageEnchere) {
            $this->addCommonParametrageEnchere($commonParametrageEnchere);
        }

        $this->collCommonParametrageEncheres = $commonParametrageEncheres;
        $this->collCommonParametrageEncheresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonParametrageEnchere objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonParametrageEnchere objects.
     * @throws PropelException
     */
    public function countCommonParametrageEncheres(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonParametrageEncheresPartial && !$this->isNew();
        if (null === $this->collCommonParametrageEncheres || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonParametrageEncheres) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonParametrageEncheres());
            }
            $query = CommonParametrageEnchereQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonParametrageEncheres);
    }

    /**
     * Method called to associate a CommonParametrageEnchere object to this object
     * through the CommonParametrageEnchere foreign key attribute.
     *
     * @param   CommonParametrageEnchere $l CommonParametrageEnchere
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonParametrageEnchere(CommonParametrageEnchere $l)
    {
        if ($this->collCommonParametrageEncheres === null) {
            $this->initCommonParametrageEncheres();
            $this->collCommonParametrageEncheresPartial = true;
        }
        if (!in_array($l, $this->collCommonParametrageEncheres->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonParametrageEnchere($l);
        }

        return $this;
    }

    /**
     * @param	CommonParametrageEnchere $commonParametrageEnchere The commonParametrageEnchere object to add.
     */
    protected function doAddCommonParametrageEnchere($commonParametrageEnchere)
    {
        $this->collCommonParametrageEncheres[]= $commonParametrageEnchere;
        $commonParametrageEnchere->setCommonOrganisme($this);
    }

    /**
     * @param	CommonParametrageEnchere $commonParametrageEnchere The commonParametrageEnchere object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonParametrageEnchere($commonParametrageEnchere)
    {
        if ($this->getCommonParametrageEncheres()->contains($commonParametrageEnchere)) {
            $this->collCommonParametrageEncheres->remove($this->collCommonParametrageEncheres->search($commonParametrageEnchere));
            if (null === $this->commonParametrageEncheresScheduledForDeletion) {
                $this->commonParametrageEncheresScheduledForDeletion = clone $this->collCommonParametrageEncheres;
                $this->commonParametrageEncheresScheduledForDeletion->clear();
            }
            $this->commonParametrageEncheresScheduledForDeletion[]= clone $commonParametrageEnchere;
            $commonParametrageEnchere->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonParametrageEncheres from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonParametrageEnchere[] List of CommonParametrageEnchere objects
     */
    public function getCommonParametrageEncheresJoinCommonService($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonParametrageEnchereQuery::create(null, $criteria);
        $query->joinWith('CommonService', $join_behavior);

        return $this->getCommonParametrageEncheres($query, $con);
    }

    /**
     * Clears out the collCommonRPAs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonRPAs()
     */
    public function clearCommonRPAs()
    {
        $this->collCommonRPAs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonRPAsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonRPAs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonRPAs($v = true)
    {
        $this->collCommonRPAsPartial = $v;
    }

    /**
     * Initializes the collCommonRPAs collection.
     *
     * By default this just sets the collCommonRPAs collection to an empty array (like clearcollCommonRPAs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonRPAs($overrideExisting = true)
    {
        if (null !== $this->collCommonRPAs && !$overrideExisting) {
            return;
        }
        $this->collCommonRPAs = new PropelObjectCollection();
        $this->collCommonRPAs->setModel('CommonRPA');
    }

    /**
     * Gets an array of CommonRPA objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonRPA[] List of CommonRPA objects
     * @throws PropelException
     */
    public function getCommonRPAs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonRPAsPartial && !$this->isNew();
        if (null === $this->collCommonRPAs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonRPAs) {
                // return empty collection
                $this->initCommonRPAs();
            } else {
                $collCommonRPAs = CommonRPAQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonRPAsPartial && count($collCommonRPAs)) {
                      $this->initCommonRPAs(false);

                      foreach ($collCommonRPAs as $obj) {
                        if (false == $this->collCommonRPAs->contains($obj)) {
                          $this->collCommonRPAs->append($obj);
                        }
                      }

                      $this->collCommonRPAsPartial = true;
                    }

                    $collCommonRPAs->getInternalIterator()->rewind();

                    return $collCommonRPAs;
                }

                if ($partial && $this->collCommonRPAs) {
                    foreach ($this->collCommonRPAs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonRPAs[] = $obj;
                        }
                    }
                }

                $this->collCommonRPAs = $collCommonRPAs;
                $this->collCommonRPAsPartial = false;
            }
        }

        return $this->collCommonRPAs;
    }

    /**
     * Sets a collection of CommonRPA objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonRPAs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonRPAs(PropelCollection $commonRPAs, PropelPDO $con = null)
    {
        $commonRPAsToDelete = $this->getCommonRPAs(new Criteria(), $con)->diff($commonRPAs);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonRPAsScheduledForDeletion = clone $commonRPAsToDelete;

        foreach ($commonRPAsToDelete as $commonRPARemoved) {
            $commonRPARemoved->setCommonOrganisme(null);
        }

        $this->collCommonRPAs = null;
        foreach ($commonRPAs as $commonRPA) {
            $this->addCommonRPA($commonRPA);
        }

        $this->collCommonRPAs = $commonRPAs;
        $this->collCommonRPAsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonRPA objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonRPA objects.
     * @throws PropelException
     */
    public function countCommonRPAs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonRPAsPartial && !$this->isNew();
        if (null === $this->collCommonRPAs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonRPAs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonRPAs());
            }
            $query = CommonRPAQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonRPAs);
    }

    /**
     * Method called to associate a CommonRPA object to this object
     * through the CommonRPA foreign key attribute.
     *
     * @param   CommonRPA $l CommonRPA
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonRPA(CommonRPA $l)
    {
        if ($this->collCommonRPAs === null) {
            $this->initCommonRPAs();
            $this->collCommonRPAsPartial = true;
        }
        if (!in_array($l, $this->collCommonRPAs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonRPA($l);
        }

        return $this;
    }

    /**
     * @param	CommonRPA $commonRPA The commonRPA object to add.
     */
    protected function doAddCommonRPA($commonRPA)
    {
        $this->collCommonRPAs[]= $commonRPA;
        $commonRPA->setCommonOrganisme($this);
    }

    /**
     * @param	CommonRPA $commonRPA The commonRPA object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonRPA($commonRPA)
    {
        if ($this->getCommonRPAs()->contains($commonRPA)) {
            $this->collCommonRPAs->remove($this->collCommonRPAs->search($commonRPA));
            if (null === $this->commonRPAsScheduledForDeletion) {
                $this->commonRPAsScheduledForDeletion = clone $this->collCommonRPAs;
                $this->commonRPAsScheduledForDeletion->clear();
            }
            $this->commonRPAsScheduledForDeletion[]= clone $commonRPA;
            $commonRPA->setCommonOrganisme(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTelechargements collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTelechargements()
     */
    public function clearCommonTelechargements()
    {
        $this->collCommonTelechargements = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTelechargementsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTelechargements collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTelechargements($v = true)
    {
        $this->collCommonTelechargementsPartial = $v;
    }

    /**
     * Initializes the collCommonTelechargements collection.
     *
     * By default this just sets the collCommonTelechargements collection to an empty array (like clearcollCommonTelechargements());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTelechargements($overrideExisting = true)
    {
        if (null !== $this->collCommonTelechargements && !$overrideExisting) {
            return;
        }
        $this->collCommonTelechargements = new PropelObjectCollection();
        $this->collCommonTelechargements->setModel('CommonTelechargement');
    }

    /**
     * Gets an array of CommonTelechargement objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     * @throws PropelException
     */
    public function getCommonTelechargements($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTelechargementsPartial && !$this->isNew();
        if (null === $this->collCommonTelechargements || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTelechargements) {
                // return empty collection
                $this->initCommonTelechargements();
            } else {
                $collCommonTelechargements = CommonTelechargementQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTelechargementsPartial && count($collCommonTelechargements)) {
                      $this->initCommonTelechargements(false);

                      foreach ($collCommonTelechargements as $obj) {
                        if (false == $this->collCommonTelechargements->contains($obj)) {
                          $this->collCommonTelechargements->append($obj);
                        }
                      }

                      $this->collCommonTelechargementsPartial = true;
                    }

                    $collCommonTelechargements->getInternalIterator()->rewind();

                    return $collCommonTelechargements;
                }

                if ($partial && $this->collCommonTelechargements) {
                    foreach ($this->collCommonTelechargements as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTelechargements[] = $obj;
                        }
                    }
                }

                $this->collCommonTelechargements = $collCommonTelechargements;
                $this->collCommonTelechargementsPartial = false;
            }
        }

        return $this->collCommonTelechargements;
    }

    /**
     * Sets a collection of CommonTelechargement objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTelechargements A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTelechargements(PropelCollection $commonTelechargements, PropelPDO $con = null)
    {
        $commonTelechargementsToDelete = $this->getCommonTelechargements(new Criteria(), $con)->diff($commonTelechargements);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTelechargementsScheduledForDeletion = clone $commonTelechargementsToDelete;

        foreach ($commonTelechargementsToDelete as $commonTelechargementRemoved) {
            $commonTelechargementRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTelechargements = null;
        foreach ($commonTelechargements as $commonTelechargement) {
            $this->addCommonTelechargement($commonTelechargement);
        }

        $this->collCommonTelechargements = $commonTelechargements;
        $this->collCommonTelechargementsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTelechargement objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTelechargement objects.
     * @throws PropelException
     */
    public function countCommonTelechargements(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTelechargementsPartial && !$this->isNew();
        if (null === $this->collCommonTelechargements || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTelechargements) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTelechargements());
            }
            $query = CommonTelechargementQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTelechargements);
    }

    /**
     * Method called to associate a CommonTelechargement object to this object
     * through the CommonTelechargement foreign key attribute.
     *
     * @param   CommonTelechargement $l CommonTelechargement
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTelechargement(CommonTelechargement $l)
    {
        if ($this->collCommonTelechargements === null) {
            $this->initCommonTelechargements();
            $this->collCommonTelechargementsPartial = true;
        }
        if (!in_array($l, $this->collCommonTelechargements->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTelechargement($l);
        }

        return $this;
    }

    /**
     * @param	CommonTelechargement $commonTelechargement The commonTelechargement object to add.
     */
    protected function doAddCommonTelechargement($commonTelechargement)
    {
        $this->collCommonTelechargements[]= $commonTelechargement;
        $commonTelechargement->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTelechargement $commonTelechargement The commonTelechargement object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTelechargement($commonTelechargement)
    {
        if ($this->getCommonTelechargements()->contains($commonTelechargement)) {
            $this->collCommonTelechargements->remove($this->collCommonTelechargements->search($commonTelechargement));
            if (null === $this->commonTelechargementsScheduledForDeletion) {
                $this->commonTelechargementsScheduledForDeletion = clone $this->collCommonTelechargements;
                $this->commonTelechargementsScheduledForDeletion->clear();
            }
            $this->commonTelechargementsScheduledForDeletion[]= clone $commonTelechargement;
            $commonTelechargement->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTelechargements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     */
    public function getCommonTelechargementsJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTelechargementQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTelechargements($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTelechargements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     */
    public function getCommonTelechargementsJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTelechargementQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonTelechargements($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTelechargements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     */
    public function getCommonTelechargementsJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTelechargementQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonTelechargements($query, $con);
    }

    /**
     * Clears out the collCommonTierss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTierss()
     */
    public function clearCommonTierss()
    {
        $this->collCommonTierss = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTierssPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTierss collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTierss($v = true)
    {
        $this->collCommonTierssPartial = $v;
    }

    /**
     * Initializes the collCommonTierss collection.
     *
     * By default this just sets the collCommonTierss collection to an empty array (like clearcollCommonTierss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTierss($overrideExisting = true)
    {
        if (null !== $this->collCommonTierss && !$overrideExisting) {
            return;
        }
        $this->collCommonTierss = new PropelObjectCollection();
        $this->collCommonTierss->setModel('CommonTiers');
    }

    /**
     * Gets an array of CommonTiers objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTiers[] List of CommonTiers objects
     * @throws PropelException
     */
    public function getCommonTierss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTierssPartial && !$this->isNew();
        if (null === $this->collCommonTierss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTierss) {
                // return empty collection
                $this->initCommonTierss();
            } else {
                $collCommonTierss = CommonTiersQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTierssPartial && count($collCommonTierss)) {
                      $this->initCommonTierss(false);

                      foreach ($collCommonTierss as $obj) {
                        if (false == $this->collCommonTierss->contains($obj)) {
                          $this->collCommonTierss->append($obj);
                        }
                      }

                      $this->collCommonTierssPartial = true;
                    }

                    $collCommonTierss->getInternalIterator()->rewind();

                    return $collCommonTierss;
                }

                if ($partial && $this->collCommonTierss) {
                    foreach ($this->collCommonTierss as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTierss[] = $obj;
                        }
                    }
                }

                $this->collCommonTierss = $collCommonTierss;
                $this->collCommonTierssPartial = false;
            }
        }

        return $this->collCommonTierss;
    }

    /**
     * Sets a collection of CommonTiers objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTierss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTierss(PropelCollection $commonTierss, PropelPDO $con = null)
    {
        $commonTierssToDelete = $this->getCommonTierss(new Criteria(), $con)->diff($commonTierss);


        $this->commonTierssScheduledForDeletion = $commonTierssToDelete;

        foreach ($commonTierssToDelete as $commonTiersRemoved) {
            $commonTiersRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTierss = null;
        foreach ($commonTierss as $commonTiers) {
            $this->addCommonTiers($commonTiers);
        }

        $this->collCommonTierss = $commonTierss;
        $this->collCommonTierssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTiers objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTiers objects.
     * @throws PropelException
     */
    public function countCommonTierss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTierssPartial && !$this->isNew();
        if (null === $this->collCommonTierss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTierss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTierss());
            }
            $query = CommonTiersQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTierss);
    }

    /**
     * Method called to associate a CommonTiers object to this object
     * through the CommonTiers foreign key attribute.
     *
     * @param   CommonTiers $l CommonTiers
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTiers(CommonTiers $l)
    {
        if ($this->collCommonTierss === null) {
            $this->initCommonTierss();
            $this->collCommonTierssPartial = true;
        }
        if (!in_array($l, $this->collCommonTierss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTiers($l);
        }

        return $this;
    }

    /**
     * @param	CommonTiers $commonTiers The commonTiers object to add.
     */
    protected function doAddCommonTiers($commonTiers)
    {
        $this->collCommonTierss[]= $commonTiers;
        $commonTiers->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTiers $commonTiers The commonTiers object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTiers($commonTiers)
    {
        if ($this->getCommonTierss()->contains($commonTiers)) {
            $this->collCommonTierss->remove($this->collCommonTierss->search($commonTiers));
            if (null === $this->commonTierssScheduledForDeletion) {
                $this->commonTierssScheduledForDeletion = clone $this->collCommonTierss;
                $this->commonTierssScheduledForDeletion->clear();
            }
            $this->commonTierssScheduledForDeletion[]= $commonTiers;
            $commonTiers->setCommonOrganisme(null);
        }

        return $this;
    }

    /**
     * Gets a single CommonConfigurationOrganisme object, which is related to this object by a one-to-one relationship.
     *
     * @param PropelPDO $con optional connection object
     * @return CommonConfigurationOrganisme
     * @throws PropelException
     */
    public function getCommonConfigurationOrganisme(PropelPDO $con = null)
    {

        if ($this->singleCommonConfigurationOrganisme === null && !$this->isNew()) {
            $this->singleCommonConfigurationOrganisme = CommonConfigurationOrganismeQuery::create()->findPk($this->getPrimaryKey(), $con);
        }

        return $this->singleCommonConfigurationOrganisme;
    }

    /**
     * Sets a single CommonConfigurationOrganisme object as related to this object by a one-to-one relationship.
     *
     * @param   CommonConfigurationOrganisme $v CommonConfigurationOrganisme
     * @return CommonOrganisme The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonConfigurationOrganisme(CommonConfigurationOrganisme $v = null)
    {
        $this->singleCommonConfigurationOrganisme = $v;

        // Make sure that that the passed-in CommonConfigurationOrganisme isn't already associated with this object
        if ($v !== null && $v->getCommonOrganisme(null, false) === null) {
            $v->setCommonOrganisme($this);
        }

        return $this;
    }

    /**
     * Clears out the collCommonConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonConsultations()
     */
    public function clearCommonConsultations()
    {
        $this->collCommonConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultations($v = true)
    {
        $this->collCommonConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonConsultations collection.
     *
     * By default this just sets the collCommonConsultations collection to an empty array (like clearcollCommonConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultations = new PropelObjectCollection();
        $this->collCommonConsultations->setModel('CommonConsultation');
    }

    /**
     * Gets an array of CommonConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     * @throws PropelException
     */
    public function getCommonConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultations) {
                // return empty collection
                $this->initCommonConsultations();
            } else {
                $collCommonConsultations = CommonConsultationQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationsPartial && count($collCommonConsultations)) {
                      $this->initCommonConsultations(false);

                      foreach ($collCommonConsultations as $obj) {
                        if (false == $this->collCommonConsultations->contains($obj)) {
                          $this->collCommonConsultations->append($obj);
                        }
                      }

                      $this->collCommonConsultationsPartial = true;
                    }

                    $collCommonConsultations->getInternalIterator()->rewind();

                    return $collCommonConsultations;
                }

                if ($partial && $this->collCommonConsultations) {
                    foreach ($this->collCommonConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultations = $collCommonConsultations;
                $this->collCommonConsultationsPartial = false;
            }
        }

        return $this->collCommonConsultations;
    }

    /**
     * Sets a collection of CommonConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonConsultations(PropelCollection $commonConsultations, PropelPDO $con = null)
    {
        $commonConsultationsToDelete = $this->getCommonConsultations(new Criteria(), $con)->diff($commonConsultations);


        $this->commonConsultationsScheduledForDeletion = $commonConsultationsToDelete;

        foreach ($commonConsultationsToDelete as $commonConsultationRemoved) {
            $commonConsultationRemoved->setCommonOrganisme(null);
        }

        $this->collCommonConsultations = null;
        foreach ($commonConsultations as $commonConsultation) {
            $this->addCommonConsultation($commonConsultation);
        }

        $this->collCommonConsultations = $commonConsultations;
        $this->collCommonConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultation objects.
     * @throws PropelException
     */
    public function countCommonConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultations());
            }
            $query = CommonConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonConsultations);
    }

    /**
     * Method called to associate a CommonConsultation object to this object
     * through the CommonConsultation foreign key attribute.
     *
     * @param   CommonConsultation $l CommonConsultation
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonConsultation(CommonConsultation $l)
    {
        if ($this->collCommonConsultations === null) {
            $this->initCommonConsultations();
            $this->collCommonConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultation $commonConsultation The commonConsultation object to add.
     */
    protected function doAddCommonConsultation($commonConsultation)
    {
        $this->collCommonConsultations[]= $commonConsultation;
        $commonConsultation->setCommonOrganisme($this);
    }

    /**
     * @param	CommonConsultation $commonConsultation The commonConsultation object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonConsultation($commonConsultation)
    {
        if ($this->getCommonConsultations()->contains($commonConsultation)) {
            $this->collCommonConsultations->remove($this->collCommonConsultations->search($commonConsultation));
            if (null === $this->commonConsultationsScheduledForDeletion) {
                $this->commonConsultationsScheduledForDeletion = clone $this->collCommonConsultations;
                $this->commonConsultationsScheduledForDeletion->clear();
            }
            $this->commonConsultationsScheduledForDeletion[]= clone $commonConsultation;
            $commonConsultation->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonReferentielAchat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonReferentielAchat', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceValidation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceValidation', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonTTypeContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeContrat', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceId', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonConsultationRelatedByReferenceConsultationInit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonConsultationRelatedByReferenceConsultationInit', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceValidationIntermediaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceValidationIntermediaire', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonOperations($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOperations', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonDossierVolumineux($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonDossierVolumineux', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }

    /**
     * Clears out the collCommonDestinataireMiseDispositions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonDestinataireMiseDispositions()
     */
    public function clearCommonDestinataireMiseDispositions()
    {
        $this->collCommonDestinataireMiseDispositions = null; // important to set this to null since that means it is uninitialized
        $this->collCommonDestinataireMiseDispositionsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonDestinataireMiseDispositions collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonDestinataireMiseDispositions($v = true)
    {
        $this->collCommonDestinataireMiseDispositionsPartial = $v;
    }

    /**
     * Initializes the collCommonDestinataireMiseDispositions collection.
     *
     * By default this just sets the collCommonDestinataireMiseDispositions collection to an empty array (like clearcollCommonDestinataireMiseDispositions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonDestinataireMiseDispositions($overrideExisting = true)
    {
        if (null !== $this->collCommonDestinataireMiseDispositions && !$overrideExisting) {
            return;
        }
        $this->collCommonDestinataireMiseDispositions = new PropelObjectCollection();
        $this->collCommonDestinataireMiseDispositions->setModel('CommonDestinataireMiseDisposition');
    }

    /**
     * Gets an array of CommonDestinataireMiseDisposition objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonDestinataireMiseDisposition[] List of CommonDestinataireMiseDisposition objects
     * @throws PropelException
     */
    public function getCommonDestinataireMiseDispositions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonDestinataireMiseDispositionsPartial && !$this->isNew();
        if (null === $this->collCommonDestinataireMiseDispositions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonDestinataireMiseDispositions) {
                // return empty collection
                $this->initCommonDestinataireMiseDispositions();
            } else {
                $collCommonDestinataireMiseDispositions = CommonDestinataireMiseDispositionQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonDestinataireMiseDispositionsPartial && count($collCommonDestinataireMiseDispositions)) {
                      $this->initCommonDestinataireMiseDispositions(false);

                      foreach ($collCommonDestinataireMiseDispositions as $obj) {
                        if (false == $this->collCommonDestinataireMiseDispositions->contains($obj)) {
                          $this->collCommonDestinataireMiseDispositions->append($obj);
                        }
                      }

                      $this->collCommonDestinataireMiseDispositionsPartial = true;
                    }

                    $collCommonDestinataireMiseDispositions->getInternalIterator()->rewind();

                    return $collCommonDestinataireMiseDispositions;
                }

                if ($partial && $this->collCommonDestinataireMiseDispositions) {
                    foreach ($this->collCommonDestinataireMiseDispositions as $obj) {
                        if ($obj->isNew()) {
                            $collCommonDestinataireMiseDispositions[] = $obj;
                        }
                    }
                }

                $this->collCommonDestinataireMiseDispositions = $collCommonDestinataireMiseDispositions;
                $this->collCommonDestinataireMiseDispositionsPartial = false;
            }
        }

        return $this->collCommonDestinataireMiseDispositions;
    }

    /**
     * Sets a collection of CommonDestinataireMiseDisposition objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonDestinataireMiseDispositions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonDestinataireMiseDispositions(PropelCollection $commonDestinataireMiseDispositions, PropelPDO $con = null)
    {
        $commonDestinataireMiseDispositionsToDelete = $this->getCommonDestinataireMiseDispositions(new Criteria(), $con)->diff($commonDestinataireMiseDispositions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonDestinataireMiseDispositionsScheduledForDeletion = clone $commonDestinataireMiseDispositionsToDelete;

        foreach ($commonDestinataireMiseDispositionsToDelete as $commonDestinataireMiseDispositionRemoved) {
            $commonDestinataireMiseDispositionRemoved->setCommonOrganisme(null);
        }

        $this->collCommonDestinataireMiseDispositions = null;
        foreach ($commonDestinataireMiseDispositions as $commonDestinataireMiseDisposition) {
            $this->addCommonDestinataireMiseDisposition($commonDestinataireMiseDisposition);
        }

        $this->collCommonDestinataireMiseDispositions = $commonDestinataireMiseDispositions;
        $this->collCommonDestinataireMiseDispositionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonDestinataireMiseDisposition objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonDestinataireMiseDisposition objects.
     * @throws PropelException
     */
    public function countCommonDestinataireMiseDispositions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonDestinataireMiseDispositionsPartial && !$this->isNew();
        if (null === $this->collCommonDestinataireMiseDispositions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonDestinataireMiseDispositions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonDestinataireMiseDispositions());
            }
            $query = CommonDestinataireMiseDispositionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonDestinataireMiseDispositions);
    }

    /**
     * Method called to associate a CommonDestinataireMiseDisposition object to this object
     * through the CommonDestinataireMiseDisposition foreign key attribute.
     *
     * @param   CommonDestinataireMiseDisposition $l CommonDestinataireMiseDisposition
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonDestinataireMiseDisposition(CommonDestinataireMiseDisposition $l)
    {
        if ($this->collCommonDestinataireMiseDispositions === null) {
            $this->initCommonDestinataireMiseDispositions();
            $this->collCommonDestinataireMiseDispositionsPartial = true;
        }
        if (!in_array($l, $this->collCommonDestinataireMiseDispositions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonDestinataireMiseDisposition($l);
        }

        return $this;
    }

    /**
     * @param	CommonDestinataireMiseDisposition $commonDestinataireMiseDisposition The commonDestinataireMiseDisposition object to add.
     */
    protected function doAddCommonDestinataireMiseDisposition($commonDestinataireMiseDisposition)
    {
        $this->collCommonDestinataireMiseDispositions[]= $commonDestinataireMiseDisposition;
        $commonDestinataireMiseDisposition->setCommonOrganisme($this);
    }

    /**
     * @param	CommonDestinataireMiseDisposition $commonDestinataireMiseDisposition The commonDestinataireMiseDisposition object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonDestinataireMiseDisposition($commonDestinataireMiseDisposition)
    {
        if ($this->getCommonDestinataireMiseDispositions()->contains($commonDestinataireMiseDisposition)) {
            $this->collCommonDestinataireMiseDispositions->remove($this->collCommonDestinataireMiseDispositions->search($commonDestinataireMiseDisposition));
            if (null === $this->commonDestinataireMiseDispositionsScheduledForDeletion) {
                $this->commonDestinataireMiseDispositionsScheduledForDeletion = clone $this->collCommonDestinataireMiseDispositions;
                $this->commonDestinataireMiseDispositionsScheduledForDeletion->clear();
            }
            $this->commonDestinataireMiseDispositionsScheduledForDeletion[]= clone $commonDestinataireMiseDisposition;
            $commonDestinataireMiseDisposition->setCommonOrganisme(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonEchangeDocApplicationClientOrganismes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonEchangeDocApplicationClientOrganismes()
     */
    public function clearCommonEchangeDocApplicationClientOrganismes()
    {
        $this->collCommonEchangeDocApplicationClientOrganismes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocApplicationClientOrganismesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocApplicationClientOrganismes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocApplicationClientOrganismes($v = true)
    {
        $this->collCommonEchangeDocApplicationClientOrganismesPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocApplicationClientOrganismes collection.
     *
     * By default this just sets the collCommonEchangeDocApplicationClientOrganismes collection to an empty array (like clearcollCommonEchangeDocApplicationClientOrganismes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocApplicationClientOrganismes($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocApplicationClientOrganismes && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocApplicationClientOrganismes = new PropelObjectCollection();
        $this->collCommonEchangeDocApplicationClientOrganismes->setModel('CommonEchangeDocApplicationClientOrganisme');
    }

    /**
     * Gets an array of CommonEchangeDocApplicationClientOrganisme objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDocApplicationClientOrganisme[] List of CommonEchangeDocApplicationClientOrganisme objects
     * @throws PropelException
     */
    public function getCommonEchangeDocApplicationClientOrganismes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocApplicationClientOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocApplicationClientOrganismes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocApplicationClientOrganismes) {
                // return empty collection
                $this->initCommonEchangeDocApplicationClientOrganismes();
            } else {
                $collCommonEchangeDocApplicationClientOrganismes = CommonEchangeDocApplicationClientOrganismeQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocApplicationClientOrganismesPartial && count($collCommonEchangeDocApplicationClientOrganismes)) {
                      $this->initCommonEchangeDocApplicationClientOrganismes(false);

                      foreach ($collCommonEchangeDocApplicationClientOrganismes as $obj) {
                        if (false == $this->collCommonEchangeDocApplicationClientOrganismes->contains($obj)) {
                          $this->collCommonEchangeDocApplicationClientOrganismes->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocApplicationClientOrganismesPartial = true;
                    }

                    $collCommonEchangeDocApplicationClientOrganismes->getInternalIterator()->rewind();

                    return $collCommonEchangeDocApplicationClientOrganismes;
                }

                if ($partial && $this->collCommonEchangeDocApplicationClientOrganismes) {
                    foreach ($this->collCommonEchangeDocApplicationClientOrganismes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocApplicationClientOrganismes[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocApplicationClientOrganismes = $collCommonEchangeDocApplicationClientOrganismes;
                $this->collCommonEchangeDocApplicationClientOrganismesPartial = false;
            }
        }

        return $this->collCommonEchangeDocApplicationClientOrganismes;
    }

    /**
     * Sets a collection of CommonEchangeDocApplicationClientOrganisme objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocApplicationClientOrganismes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonEchangeDocApplicationClientOrganismes(PropelCollection $commonEchangeDocApplicationClientOrganismes, PropelPDO $con = null)
    {
        $commonEchangeDocApplicationClientOrganismesToDelete = $this->getCommonEchangeDocApplicationClientOrganismes(new Criteria(), $con)->diff($commonEchangeDocApplicationClientOrganismes);


        $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion = $commonEchangeDocApplicationClientOrganismesToDelete;

        foreach ($commonEchangeDocApplicationClientOrganismesToDelete as $commonEchangeDocApplicationClientOrganismeRemoved) {
            $commonEchangeDocApplicationClientOrganismeRemoved->setCommonOrganisme(null);
        }

        $this->collCommonEchangeDocApplicationClientOrganismes = null;
        foreach ($commonEchangeDocApplicationClientOrganismes as $commonEchangeDocApplicationClientOrganisme) {
            $this->addCommonEchangeDocApplicationClientOrganisme($commonEchangeDocApplicationClientOrganisme);
        }

        $this->collCommonEchangeDocApplicationClientOrganismes = $commonEchangeDocApplicationClientOrganismes;
        $this->collCommonEchangeDocApplicationClientOrganismesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDocApplicationClientOrganisme objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDocApplicationClientOrganisme objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocApplicationClientOrganismes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocApplicationClientOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocApplicationClientOrganismes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocApplicationClientOrganismes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocApplicationClientOrganismes());
            }
            $query = CommonEchangeDocApplicationClientOrganismeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocApplicationClientOrganismes);
    }

    /**
     * Method called to associate a CommonEchangeDocApplicationClientOrganisme object to this object
     * through the CommonEchangeDocApplicationClientOrganisme foreign key attribute.
     *
     * @param   CommonEchangeDocApplicationClientOrganisme $l CommonEchangeDocApplicationClientOrganisme
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonEchangeDocApplicationClientOrganisme(CommonEchangeDocApplicationClientOrganisme $l)
    {
        if ($this->collCommonEchangeDocApplicationClientOrganismes === null) {
            $this->initCommonEchangeDocApplicationClientOrganismes();
            $this->collCommonEchangeDocApplicationClientOrganismesPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocApplicationClientOrganismes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDocApplicationClientOrganisme($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDocApplicationClientOrganisme $commonEchangeDocApplicationClientOrganisme The commonEchangeDocApplicationClientOrganisme object to add.
     */
    protected function doAddCommonEchangeDocApplicationClientOrganisme($commonEchangeDocApplicationClientOrganisme)
    {
        $this->collCommonEchangeDocApplicationClientOrganismes[]= $commonEchangeDocApplicationClientOrganisme;
        $commonEchangeDocApplicationClientOrganisme->setCommonOrganisme($this);
    }

    /**
     * @param	CommonEchangeDocApplicationClientOrganisme $commonEchangeDocApplicationClientOrganisme The commonEchangeDocApplicationClientOrganisme object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonEchangeDocApplicationClientOrganisme($commonEchangeDocApplicationClientOrganisme)
    {
        if ($this->getCommonEchangeDocApplicationClientOrganismes()->contains($commonEchangeDocApplicationClientOrganisme)) {
            $this->collCommonEchangeDocApplicationClientOrganismes->remove($this->collCommonEchangeDocApplicationClientOrganismes->search($commonEchangeDocApplicationClientOrganisme));
            if (null === $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion) {
                $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion = clone $this->collCommonEchangeDocApplicationClientOrganismes;
                $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion->clear();
            }
            $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion[]= $commonEchangeDocApplicationClientOrganisme;
            $commonEchangeDocApplicationClientOrganisme->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonEchangeDocApplicationClientOrganismes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocApplicationClientOrganisme[] List of CommonEchangeDocApplicationClientOrganisme objects
     */
    public function getCommonEchangeDocApplicationClientOrganismesJoinCommonEchangeDocApplicationClient($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocApplicationClientOrganismeQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDocApplicationClient', $join_behavior);

        return $this->getCommonEchangeDocApplicationClientOrganismes($query, $con);
    }

    /**
     * Clears out the collCommonInvitePermanentTransverses collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonInvitePermanentTransverses()
     */
    public function clearCommonInvitePermanentTransverses()
    {
        $this->collCommonInvitePermanentTransverses = null; // important to set this to null since that means it is uninitialized
        $this->collCommonInvitePermanentTransversesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonInvitePermanentTransverses collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonInvitePermanentTransverses($v = true)
    {
        $this->collCommonInvitePermanentTransversesPartial = $v;
    }

    /**
     * Initializes the collCommonInvitePermanentTransverses collection.
     *
     * By default this just sets the collCommonInvitePermanentTransverses collection to an empty array (like clearcollCommonInvitePermanentTransverses());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonInvitePermanentTransverses($overrideExisting = true)
    {
        if (null !== $this->collCommonInvitePermanentTransverses && !$overrideExisting) {
            return;
        }
        $this->collCommonInvitePermanentTransverses = new PropelObjectCollection();
        $this->collCommonInvitePermanentTransverses->setModel('CommonInvitePermanentTransverse');
    }

    /**
     * Gets an array of CommonInvitePermanentTransverse objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[] List of CommonInvitePermanentTransverse objects
     * @throws PropelException
     */
    public function getCommonInvitePermanentTransverses($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonInvitePermanentTransversesPartial && !$this->isNew();
        if (null === $this->collCommonInvitePermanentTransverses || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonInvitePermanentTransverses) {
                // return empty collection
                $this->initCommonInvitePermanentTransverses();
            } else {
                $collCommonInvitePermanentTransverses = CommonInvitePermanentTransverseQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonInvitePermanentTransversesPartial && count($collCommonInvitePermanentTransverses)) {
                      $this->initCommonInvitePermanentTransverses(false);

                      foreach ($collCommonInvitePermanentTransverses as $obj) {
                        if (false == $this->collCommonInvitePermanentTransverses->contains($obj)) {
                          $this->collCommonInvitePermanentTransverses->append($obj);
                        }
                      }

                      $this->collCommonInvitePermanentTransversesPartial = true;
                    }

                    $collCommonInvitePermanentTransverses->getInternalIterator()->rewind();

                    return $collCommonInvitePermanentTransverses;
                }

                if ($partial && $this->collCommonInvitePermanentTransverses) {
                    foreach ($this->collCommonInvitePermanentTransverses as $obj) {
                        if ($obj->isNew()) {
                            $collCommonInvitePermanentTransverses[] = $obj;
                        }
                    }
                }

                $this->collCommonInvitePermanentTransverses = $collCommonInvitePermanentTransverses;
                $this->collCommonInvitePermanentTransversesPartial = false;
            }
        }

        return $this->collCommonInvitePermanentTransverses;
    }

    /**
     * Sets a collection of CommonInvitePermanentTransverse objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonInvitePermanentTransverses A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonInvitePermanentTransverses(PropelCollection $commonInvitePermanentTransverses, PropelPDO $con = null)
    {
        $commonInvitePermanentTransversesToDelete = $this->getCommonInvitePermanentTransverses(new Criteria(), $con)->diff($commonInvitePermanentTransverses);


        $this->commonInvitePermanentTransversesScheduledForDeletion = $commonInvitePermanentTransversesToDelete;

        foreach ($commonInvitePermanentTransversesToDelete as $commonInvitePermanentTransverseRemoved) {
            $commonInvitePermanentTransverseRemoved->setCommonOrganisme(null);
        }

        $this->collCommonInvitePermanentTransverses = null;
        foreach ($commonInvitePermanentTransverses as $commonInvitePermanentTransverse) {
            $this->addCommonInvitePermanentTransverse($commonInvitePermanentTransverse);
        }

        $this->collCommonInvitePermanentTransverses = $commonInvitePermanentTransverses;
        $this->collCommonInvitePermanentTransversesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonInvitePermanentTransverse objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonInvitePermanentTransverse objects.
     * @throws PropelException
     */
    public function countCommonInvitePermanentTransverses(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonInvitePermanentTransversesPartial && !$this->isNew();
        if (null === $this->collCommonInvitePermanentTransverses || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonInvitePermanentTransverses) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonInvitePermanentTransverses());
            }
            $query = CommonInvitePermanentTransverseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonInvitePermanentTransverses);
    }

    /**
     * Method called to associate a CommonInvitePermanentTransverse object to this object
     * through the CommonInvitePermanentTransverse foreign key attribute.
     *
     * @param   CommonInvitePermanentTransverse $l CommonInvitePermanentTransverse
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonInvitePermanentTransverse(CommonInvitePermanentTransverse $l)
    {
        if ($this->collCommonInvitePermanentTransverses === null) {
            $this->initCommonInvitePermanentTransverses();
            $this->collCommonInvitePermanentTransversesPartial = true;
        }
        if (!in_array($l, $this->collCommonInvitePermanentTransverses->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonInvitePermanentTransverse($l);
        }

        return $this;
    }

    /**
     * @param	CommonInvitePermanentTransverse $commonInvitePermanentTransverse The commonInvitePermanentTransverse object to add.
     */
    protected function doAddCommonInvitePermanentTransverse($commonInvitePermanentTransverse)
    {
        $this->collCommonInvitePermanentTransverses[]= $commonInvitePermanentTransverse;
        $commonInvitePermanentTransverse->setCommonOrganisme($this);
    }

    /**
     * @param	CommonInvitePermanentTransverse $commonInvitePermanentTransverse The commonInvitePermanentTransverse object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonInvitePermanentTransverse($commonInvitePermanentTransverse)
    {
        if ($this->getCommonInvitePermanentTransverses()->contains($commonInvitePermanentTransverse)) {
            $this->collCommonInvitePermanentTransverses->remove($this->collCommonInvitePermanentTransverses->search($commonInvitePermanentTransverse));
            if (null === $this->commonInvitePermanentTransversesScheduledForDeletion) {
                $this->commonInvitePermanentTransversesScheduledForDeletion = clone $this->collCommonInvitePermanentTransverses;
                $this->commonInvitePermanentTransversesScheduledForDeletion->clear();
            }
            $this->commonInvitePermanentTransversesScheduledForDeletion[]= clone $commonInvitePermanentTransverse;
            $commonInvitePermanentTransverse->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonInvitePermanentTransverses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[] List of CommonInvitePermanentTransverse objects
     */
    public function getCommonInvitePermanentTransversesJoinCommonServiceRelatedByServiceId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonInvitePermanentTransverseQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceId', $join_behavior);

        return $this->getCommonInvitePermanentTransverses($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonInvitePermanentTransverses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[] List of CommonInvitePermanentTransverse objects
     */
    public function getCommonInvitePermanentTransversesJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonInvitePermanentTransverseQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonInvitePermanentTransverses($query, $con);
    }

    /**
     * Clears out the collCommonPlateformeVirtuelleOrganismes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonPlateformeVirtuelleOrganismes()
     */
    public function clearCommonPlateformeVirtuelleOrganismes()
    {
        $this->collCommonPlateformeVirtuelleOrganismes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonPlateformeVirtuelleOrganismesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonPlateformeVirtuelleOrganismes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonPlateformeVirtuelleOrganismes($v = true)
    {
        $this->collCommonPlateformeVirtuelleOrganismesPartial = $v;
    }

    /**
     * Initializes the collCommonPlateformeVirtuelleOrganismes collection.
     *
     * By default this just sets the collCommonPlateformeVirtuelleOrganismes collection to an empty array (like clearcollCommonPlateformeVirtuelleOrganismes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonPlateformeVirtuelleOrganismes($overrideExisting = true)
    {
        if (null !== $this->collCommonPlateformeVirtuelleOrganismes && !$overrideExisting) {
            return;
        }
        $this->collCommonPlateformeVirtuelleOrganismes = new PropelObjectCollection();
        $this->collCommonPlateformeVirtuelleOrganismes->setModel('CommonPlateformeVirtuelleOrganisme');
    }

    /**
     * Gets an array of CommonPlateformeVirtuelleOrganisme objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonPlateformeVirtuelleOrganisme[] List of CommonPlateformeVirtuelleOrganisme objects
     * @throws PropelException
     */
    public function getCommonPlateformeVirtuelleOrganismes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonPlateformeVirtuelleOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonPlateformeVirtuelleOrganismes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonPlateformeVirtuelleOrganismes) {
                // return empty collection
                $this->initCommonPlateformeVirtuelleOrganismes();
            } else {
                $collCommonPlateformeVirtuelleOrganismes = CommonPlateformeVirtuelleOrganismeQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonPlateformeVirtuelleOrganismesPartial && count($collCommonPlateformeVirtuelleOrganismes)) {
                      $this->initCommonPlateformeVirtuelleOrganismes(false);

                      foreach ($collCommonPlateformeVirtuelleOrganismes as $obj) {
                        if (false == $this->collCommonPlateformeVirtuelleOrganismes->contains($obj)) {
                          $this->collCommonPlateformeVirtuelleOrganismes->append($obj);
                        }
                      }

                      $this->collCommonPlateformeVirtuelleOrganismesPartial = true;
                    }

                    $collCommonPlateformeVirtuelleOrganismes->getInternalIterator()->rewind();

                    return $collCommonPlateformeVirtuelleOrganismes;
                }

                if ($partial && $this->collCommonPlateformeVirtuelleOrganismes) {
                    foreach ($this->collCommonPlateformeVirtuelleOrganismes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonPlateformeVirtuelleOrganismes[] = $obj;
                        }
                    }
                }

                $this->collCommonPlateformeVirtuelleOrganismes = $collCommonPlateformeVirtuelleOrganismes;
                $this->collCommonPlateformeVirtuelleOrganismesPartial = false;
            }
        }

        return $this->collCommonPlateformeVirtuelleOrganismes;
    }

    /**
     * Sets a collection of CommonPlateformeVirtuelleOrganisme objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonPlateformeVirtuelleOrganismes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonPlateformeVirtuelleOrganismes(PropelCollection $commonPlateformeVirtuelleOrganismes, PropelPDO $con = null)
    {
        $commonPlateformeVirtuelleOrganismesToDelete = $this->getCommonPlateformeVirtuelleOrganismes(new Criteria(), $con)->diff($commonPlateformeVirtuelleOrganismes);


        $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion = $commonPlateformeVirtuelleOrganismesToDelete;

        foreach ($commonPlateformeVirtuelleOrganismesToDelete as $commonPlateformeVirtuelleOrganismeRemoved) {
            $commonPlateformeVirtuelleOrganismeRemoved->setCommonOrganisme(null);
        }

        $this->collCommonPlateformeVirtuelleOrganismes = null;
        foreach ($commonPlateformeVirtuelleOrganismes as $commonPlateformeVirtuelleOrganisme) {
            $this->addCommonPlateformeVirtuelleOrganisme($commonPlateformeVirtuelleOrganisme);
        }

        $this->collCommonPlateformeVirtuelleOrganismes = $commonPlateformeVirtuelleOrganismes;
        $this->collCommonPlateformeVirtuelleOrganismesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonPlateformeVirtuelleOrganisme objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonPlateformeVirtuelleOrganisme objects.
     * @throws PropelException
     */
    public function countCommonPlateformeVirtuelleOrganismes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonPlateformeVirtuelleOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonPlateformeVirtuelleOrganismes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonPlateformeVirtuelleOrganismes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonPlateformeVirtuelleOrganismes());
            }
            $query = CommonPlateformeVirtuelleOrganismeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonPlateformeVirtuelleOrganismes);
    }

    /**
     * Method called to associate a CommonPlateformeVirtuelleOrganisme object to this object
     * through the CommonPlateformeVirtuelleOrganisme foreign key attribute.
     *
     * @param   CommonPlateformeVirtuelleOrganisme $l CommonPlateformeVirtuelleOrganisme
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonPlateformeVirtuelleOrganisme(CommonPlateformeVirtuelleOrganisme $l)
    {
        if ($this->collCommonPlateformeVirtuelleOrganismes === null) {
            $this->initCommonPlateformeVirtuelleOrganismes();
            $this->collCommonPlateformeVirtuelleOrganismesPartial = true;
        }
        if (!in_array($l, $this->collCommonPlateformeVirtuelleOrganismes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonPlateformeVirtuelleOrganisme($l);
        }

        return $this;
    }

    /**
     * @param	CommonPlateformeVirtuelleOrganisme $commonPlateformeVirtuelleOrganisme The commonPlateformeVirtuelleOrganisme object to add.
     */
    protected function doAddCommonPlateformeVirtuelleOrganisme($commonPlateformeVirtuelleOrganisme)
    {
        $this->collCommonPlateformeVirtuelleOrganismes[]= $commonPlateformeVirtuelleOrganisme;
        $commonPlateformeVirtuelleOrganisme->setCommonOrganisme($this);
    }

    /**
     * @param	CommonPlateformeVirtuelleOrganisme $commonPlateformeVirtuelleOrganisme The commonPlateformeVirtuelleOrganisme object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonPlateformeVirtuelleOrganisme($commonPlateformeVirtuelleOrganisme)
    {
        if ($this->getCommonPlateformeVirtuelleOrganismes()->contains($commonPlateformeVirtuelleOrganisme)) {
            $this->collCommonPlateformeVirtuelleOrganismes->remove($this->collCommonPlateformeVirtuelleOrganismes->search($commonPlateformeVirtuelleOrganisme));
            if (null === $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion) {
                $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion = clone $this->collCommonPlateformeVirtuelleOrganismes;
                $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion->clear();
            }
            $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion[]= clone $commonPlateformeVirtuelleOrganisme;
            $commonPlateformeVirtuelleOrganisme->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonPlateformeVirtuelleOrganismes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonPlateformeVirtuelleOrganisme[] List of CommonPlateformeVirtuelleOrganisme objects
     */
    public function getCommonPlateformeVirtuelleOrganismesJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonPlateformeVirtuelleOrganismeQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonPlateformeVirtuelleOrganismes($query, $con);
    }

    /**
     * Clears out the collCommonTCAOCommissions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCAOCommissions()
     */
    public function clearCommonTCAOCommissions()
    {
        $this->collCommonTCAOCommissions = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOCommissionsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOCommissions collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOCommissions($v = true)
    {
        $this->collCommonTCAOCommissionsPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOCommissions collection.
     *
     * By default this just sets the collCommonTCAOCommissions collection to an empty array (like clearcollCommonTCAOCommissions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOCommissions($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOCommissions && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOCommissions = new PropelObjectCollection();
        $this->collCommonTCAOCommissions->setModel('CommonTCAOCommission');
    }

    /**
     * Gets an array of CommonTCAOCommission objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOCommission[] List of CommonTCAOCommission objects
     * @throws PropelException
     */
    public function getCommonTCAOCommissions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOCommissionsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOCommissions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOCommissions) {
                // return empty collection
                $this->initCommonTCAOCommissions();
            } else {
                $collCommonTCAOCommissions = CommonTCAOCommissionQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOCommissionsPartial && count($collCommonTCAOCommissions)) {
                      $this->initCommonTCAOCommissions(false);

                      foreach ($collCommonTCAOCommissions as $obj) {
                        if (false == $this->collCommonTCAOCommissions->contains($obj)) {
                          $this->collCommonTCAOCommissions->append($obj);
                        }
                      }

                      $this->collCommonTCAOCommissionsPartial = true;
                    }

                    $collCommonTCAOCommissions->getInternalIterator()->rewind();

                    return $collCommonTCAOCommissions;
                }

                if ($partial && $this->collCommonTCAOCommissions) {
                    foreach ($this->collCommonTCAOCommissions as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOCommissions[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOCommissions = $collCommonTCAOCommissions;
                $this->collCommonTCAOCommissionsPartial = false;
            }
        }

        return $this->collCommonTCAOCommissions;
    }

    /**
     * Sets a collection of CommonTCAOCommission objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOCommissions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCAOCommissions(PropelCollection $commonTCAOCommissions, PropelPDO $con = null)
    {
        $commonTCAOCommissionsToDelete = $this->getCommonTCAOCommissions(new Criteria(), $con)->diff($commonTCAOCommissions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTCAOCommissionsScheduledForDeletion = clone $commonTCAOCommissionsToDelete;

        foreach ($commonTCAOCommissionsToDelete as $commonTCAOCommissionRemoved) {
            $commonTCAOCommissionRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCAOCommissions = null;
        foreach ($commonTCAOCommissions as $commonTCAOCommission) {
            $this->addCommonTCAOCommission($commonTCAOCommission);
        }

        $this->collCommonTCAOCommissions = $commonTCAOCommissions;
        $this->collCommonTCAOCommissionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOCommission objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOCommission objects.
     * @throws PropelException
     */
    public function countCommonTCAOCommissions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOCommissionsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOCommissions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOCommissions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOCommissions());
            }
            $query = CommonTCAOCommissionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCAOCommissions);
    }

    /**
     * Method called to associate a CommonTCAOCommission object to this object
     * through the CommonTCAOCommission foreign key attribute.
     *
     * @param   CommonTCAOCommission $l CommonTCAOCommission
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCAOCommission(CommonTCAOCommission $l)
    {
        if ($this->collCommonTCAOCommissions === null) {
            $this->initCommonTCAOCommissions();
            $this->collCommonTCAOCommissionsPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOCommissions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOCommission($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOCommission $commonTCAOCommission The commonTCAOCommission object to add.
     */
    protected function doAddCommonTCAOCommission($commonTCAOCommission)
    {
        $this->collCommonTCAOCommissions[]= $commonTCAOCommission;
        $commonTCAOCommission->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCAOCommission $commonTCAOCommission The commonTCAOCommission object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCAOCommission($commonTCAOCommission)
    {
        if ($this->getCommonTCAOCommissions()->contains($commonTCAOCommission)) {
            $this->collCommonTCAOCommissions->remove($this->collCommonTCAOCommissions->search($commonTCAOCommission));
            if (null === $this->commonTCAOCommissionsScheduledForDeletion) {
                $this->commonTCAOCommissionsScheduledForDeletion = clone $this->collCommonTCAOCommissions;
                $this->commonTCAOCommissionsScheduledForDeletion->clear();
            }
            $this->commonTCAOCommissionsScheduledForDeletion[]= clone $commonTCAOCommission;
            $commonTCAOCommission->setCommonOrganisme(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTCAOCommissionAgents collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCAOCommissionAgents()
     */
    public function clearCommonTCAOCommissionAgents()
    {
        $this->collCommonTCAOCommissionAgents = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOCommissionAgentsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOCommissionAgents collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOCommissionAgents($v = true)
    {
        $this->collCommonTCAOCommissionAgentsPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOCommissionAgents collection.
     *
     * By default this just sets the collCommonTCAOCommissionAgents collection to an empty array (like clearcollCommonTCAOCommissionAgents());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOCommissionAgents($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOCommissionAgents && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOCommissionAgents = new PropelObjectCollection();
        $this->collCommonTCAOCommissionAgents->setModel('CommonTCAOCommissionAgent');
    }

    /**
     * Gets an array of CommonTCAOCommissionAgent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOCommissionAgent[] List of CommonTCAOCommissionAgent objects
     * @throws PropelException
     */
    public function getCommonTCAOCommissionAgents($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOCommissionAgentsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOCommissionAgents || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOCommissionAgents) {
                // return empty collection
                $this->initCommonTCAOCommissionAgents();
            } else {
                $collCommonTCAOCommissionAgents = CommonTCAOCommissionAgentQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOCommissionAgentsPartial && count($collCommonTCAOCommissionAgents)) {
                      $this->initCommonTCAOCommissionAgents(false);

                      foreach ($collCommonTCAOCommissionAgents as $obj) {
                        if (false == $this->collCommonTCAOCommissionAgents->contains($obj)) {
                          $this->collCommonTCAOCommissionAgents->append($obj);
                        }
                      }

                      $this->collCommonTCAOCommissionAgentsPartial = true;
                    }

                    $collCommonTCAOCommissionAgents->getInternalIterator()->rewind();

                    return $collCommonTCAOCommissionAgents;
                }

                if ($partial && $this->collCommonTCAOCommissionAgents) {
                    foreach ($this->collCommonTCAOCommissionAgents as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOCommissionAgents[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOCommissionAgents = $collCommonTCAOCommissionAgents;
                $this->collCommonTCAOCommissionAgentsPartial = false;
            }
        }

        return $this->collCommonTCAOCommissionAgents;
    }

    /**
     * Sets a collection of CommonTCAOCommissionAgent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOCommissionAgents A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCAOCommissionAgents(PropelCollection $commonTCAOCommissionAgents, PropelPDO $con = null)
    {
        $commonTCAOCommissionAgentsToDelete = $this->getCommonTCAOCommissionAgents(new Criteria(), $con)->diff($commonTCAOCommissionAgents);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTCAOCommissionAgentsScheduledForDeletion = clone $commonTCAOCommissionAgentsToDelete;

        foreach ($commonTCAOCommissionAgentsToDelete as $commonTCAOCommissionAgentRemoved) {
            $commonTCAOCommissionAgentRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCAOCommissionAgents = null;
        foreach ($commonTCAOCommissionAgents as $commonTCAOCommissionAgent) {
            $this->addCommonTCAOCommissionAgent($commonTCAOCommissionAgent);
        }

        $this->collCommonTCAOCommissionAgents = $commonTCAOCommissionAgents;
        $this->collCommonTCAOCommissionAgentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOCommissionAgent objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOCommissionAgent objects.
     * @throws PropelException
     */
    public function countCommonTCAOCommissionAgents(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOCommissionAgentsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOCommissionAgents || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOCommissionAgents) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOCommissionAgents());
            }
            $query = CommonTCAOCommissionAgentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCAOCommissionAgents);
    }

    /**
     * Method called to associate a CommonTCAOCommissionAgent object to this object
     * through the CommonTCAOCommissionAgent foreign key attribute.
     *
     * @param   CommonTCAOCommissionAgent $l CommonTCAOCommissionAgent
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCAOCommissionAgent(CommonTCAOCommissionAgent $l)
    {
        if ($this->collCommonTCAOCommissionAgents === null) {
            $this->initCommonTCAOCommissionAgents();
            $this->collCommonTCAOCommissionAgentsPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOCommissionAgents->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOCommissionAgent($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOCommissionAgent $commonTCAOCommissionAgent The commonTCAOCommissionAgent object to add.
     */
    protected function doAddCommonTCAOCommissionAgent($commonTCAOCommissionAgent)
    {
        $this->collCommonTCAOCommissionAgents[]= $commonTCAOCommissionAgent;
        $commonTCAOCommissionAgent->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCAOCommissionAgent $commonTCAOCommissionAgent The commonTCAOCommissionAgent object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCAOCommissionAgent($commonTCAOCommissionAgent)
    {
        if ($this->getCommonTCAOCommissionAgents()->contains($commonTCAOCommissionAgent)) {
            $this->collCommonTCAOCommissionAgents->remove($this->collCommonTCAOCommissionAgents->search($commonTCAOCommissionAgent));
            if (null === $this->commonTCAOCommissionAgentsScheduledForDeletion) {
                $this->commonTCAOCommissionAgentsScheduledForDeletion = clone $this->collCommonTCAOCommissionAgents;
                $this->commonTCAOCommissionAgentsScheduledForDeletion->clear();
            }
            $this->commonTCAOCommissionAgentsScheduledForDeletion[]= clone $commonTCAOCommissionAgent;
            $commonTCAOCommissionAgent->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOCommissionAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOCommissionAgent[] List of CommonTCAOCommissionAgent objects
     */
    public function getCommonTCAOCommissionAgentsJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOCommissionAgentQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonTCAOCommissionAgents($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOCommissionAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOCommissionAgent[] List of CommonTCAOCommissionAgent objects
     */
    public function getCommonTCAOCommissionAgentsJoinCommonTCAOCommission($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOCommissionAgentQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOCommission', $join_behavior);

        return $this->getCommonTCAOCommissionAgents($query, $con);
    }

    /**
     * Clears out the collCommonTCAOCommissionIntervenantExternes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCAOCommissionIntervenantExternes()
     */
    public function clearCommonTCAOCommissionIntervenantExternes()
    {
        $this->collCommonTCAOCommissionIntervenantExternes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOCommissionIntervenantExternesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOCommissionIntervenantExternes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOCommissionIntervenantExternes($v = true)
    {
        $this->collCommonTCAOCommissionIntervenantExternesPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOCommissionIntervenantExternes collection.
     *
     * By default this just sets the collCommonTCAOCommissionIntervenantExternes collection to an empty array (like clearcollCommonTCAOCommissionIntervenantExternes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOCommissionIntervenantExternes($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOCommissionIntervenantExternes && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOCommissionIntervenantExternes = new PropelObjectCollection();
        $this->collCommonTCAOCommissionIntervenantExternes->setModel('CommonTCAOCommissionIntervenantExterne');
    }

    /**
     * Gets an array of CommonTCAOCommissionIntervenantExterne objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOCommissionIntervenantExterne[] List of CommonTCAOCommissionIntervenantExterne objects
     * @throws PropelException
     */
    public function getCommonTCAOCommissionIntervenantExternes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOCommissionIntervenantExternesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOCommissionIntervenantExternes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOCommissionIntervenantExternes) {
                // return empty collection
                $this->initCommonTCAOCommissionIntervenantExternes();
            } else {
                $collCommonTCAOCommissionIntervenantExternes = CommonTCAOCommissionIntervenantExterneQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOCommissionIntervenantExternesPartial && count($collCommonTCAOCommissionIntervenantExternes)) {
                      $this->initCommonTCAOCommissionIntervenantExternes(false);

                      foreach ($collCommonTCAOCommissionIntervenantExternes as $obj) {
                        if (false == $this->collCommonTCAOCommissionIntervenantExternes->contains($obj)) {
                          $this->collCommonTCAOCommissionIntervenantExternes->append($obj);
                        }
                      }

                      $this->collCommonTCAOCommissionIntervenantExternesPartial = true;
                    }

                    $collCommonTCAOCommissionIntervenantExternes->getInternalIterator()->rewind();

                    return $collCommonTCAOCommissionIntervenantExternes;
                }

                if ($partial && $this->collCommonTCAOCommissionIntervenantExternes) {
                    foreach ($this->collCommonTCAOCommissionIntervenantExternes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOCommissionIntervenantExternes[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOCommissionIntervenantExternes = $collCommonTCAOCommissionIntervenantExternes;
                $this->collCommonTCAOCommissionIntervenantExternesPartial = false;
            }
        }

        return $this->collCommonTCAOCommissionIntervenantExternes;
    }

    /**
     * Sets a collection of CommonTCAOCommissionIntervenantExterne objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOCommissionIntervenantExternes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCAOCommissionIntervenantExternes(PropelCollection $commonTCAOCommissionIntervenantExternes, PropelPDO $con = null)
    {
        $commonTCAOCommissionIntervenantExternesToDelete = $this->getCommonTCAOCommissionIntervenantExternes(new Criteria(), $con)->diff($commonTCAOCommissionIntervenantExternes);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTCAOCommissionIntervenantExternesScheduledForDeletion = clone $commonTCAOCommissionIntervenantExternesToDelete;

        foreach ($commonTCAOCommissionIntervenantExternesToDelete as $commonTCAOCommissionIntervenantExterneRemoved) {
            $commonTCAOCommissionIntervenantExterneRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCAOCommissionIntervenantExternes = null;
        foreach ($commonTCAOCommissionIntervenantExternes as $commonTCAOCommissionIntervenantExterne) {
            $this->addCommonTCAOCommissionIntervenantExterne($commonTCAOCommissionIntervenantExterne);
        }

        $this->collCommonTCAOCommissionIntervenantExternes = $commonTCAOCommissionIntervenantExternes;
        $this->collCommonTCAOCommissionIntervenantExternesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOCommissionIntervenantExterne objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOCommissionIntervenantExterne objects.
     * @throws PropelException
     */
    public function countCommonTCAOCommissionIntervenantExternes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOCommissionIntervenantExternesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOCommissionIntervenantExternes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOCommissionIntervenantExternes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOCommissionIntervenantExternes());
            }
            $query = CommonTCAOCommissionIntervenantExterneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCAOCommissionIntervenantExternes);
    }

    /**
     * Method called to associate a CommonTCAOCommissionIntervenantExterne object to this object
     * through the CommonTCAOCommissionIntervenantExterne foreign key attribute.
     *
     * @param   CommonTCAOCommissionIntervenantExterne $l CommonTCAOCommissionIntervenantExterne
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCAOCommissionIntervenantExterne(CommonTCAOCommissionIntervenantExterne $l)
    {
        if ($this->collCommonTCAOCommissionIntervenantExternes === null) {
            $this->initCommonTCAOCommissionIntervenantExternes();
            $this->collCommonTCAOCommissionIntervenantExternesPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOCommissionIntervenantExternes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOCommissionIntervenantExterne($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOCommissionIntervenantExterne $commonTCAOCommissionIntervenantExterne The commonTCAOCommissionIntervenantExterne object to add.
     */
    protected function doAddCommonTCAOCommissionIntervenantExterne($commonTCAOCommissionIntervenantExterne)
    {
        $this->collCommonTCAOCommissionIntervenantExternes[]= $commonTCAOCommissionIntervenantExterne;
        $commonTCAOCommissionIntervenantExterne->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCAOCommissionIntervenantExterne $commonTCAOCommissionIntervenantExterne The commonTCAOCommissionIntervenantExterne object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCAOCommissionIntervenantExterne($commonTCAOCommissionIntervenantExterne)
    {
        if ($this->getCommonTCAOCommissionIntervenantExternes()->contains($commonTCAOCommissionIntervenantExterne)) {
            $this->collCommonTCAOCommissionIntervenantExternes->remove($this->collCommonTCAOCommissionIntervenantExternes->search($commonTCAOCommissionIntervenantExterne));
            if (null === $this->commonTCAOCommissionIntervenantExternesScheduledForDeletion) {
                $this->commonTCAOCommissionIntervenantExternesScheduledForDeletion = clone $this->collCommonTCAOCommissionIntervenantExternes;
                $this->commonTCAOCommissionIntervenantExternesScheduledForDeletion->clear();
            }
            $this->commonTCAOCommissionIntervenantExternesScheduledForDeletion[]= clone $commonTCAOCommissionIntervenantExterne;
            $commonTCAOCommissionIntervenantExterne->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOCommissionIntervenantExternes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOCommissionIntervenantExterne[] List of CommonTCAOCommissionIntervenantExterne objects
     */
    public function getCommonTCAOCommissionIntervenantExternesJoinCommonTCAOCommission($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOCommissionIntervenantExterneQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOCommission', $join_behavior);

        return $this->getCommonTCAOCommissionIntervenantExternes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOCommissionIntervenantExternes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOCommissionIntervenantExterne[] List of CommonTCAOCommissionIntervenantExterne objects
     */
    public function getCommonTCAOCommissionIntervenantExternesJoinCommonTCAOIntervenantExterne($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOCommissionIntervenantExterneQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOIntervenantExterne', $join_behavior);

        return $this->getCommonTCAOCommissionIntervenantExternes($query, $con);
    }

    /**
     * Clears out the collCommonTCAOIntervenantExternes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCAOIntervenantExternes()
     */
    public function clearCommonTCAOIntervenantExternes()
    {
        $this->collCommonTCAOIntervenantExternes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOIntervenantExternesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOIntervenantExternes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOIntervenantExternes($v = true)
    {
        $this->collCommonTCAOIntervenantExternesPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOIntervenantExternes collection.
     *
     * By default this just sets the collCommonTCAOIntervenantExternes collection to an empty array (like clearcollCommonTCAOIntervenantExternes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOIntervenantExternes($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOIntervenantExternes && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOIntervenantExternes = new PropelObjectCollection();
        $this->collCommonTCAOIntervenantExternes->setModel('CommonTCAOIntervenantExterne');
    }

    /**
     * Gets an array of CommonTCAOIntervenantExterne objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOIntervenantExterne[] List of CommonTCAOIntervenantExterne objects
     * @throws PropelException
     */
    public function getCommonTCAOIntervenantExternes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOIntervenantExternesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOIntervenantExternes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOIntervenantExternes) {
                // return empty collection
                $this->initCommonTCAOIntervenantExternes();
            } else {
                $collCommonTCAOIntervenantExternes = CommonTCAOIntervenantExterneQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOIntervenantExternesPartial && count($collCommonTCAOIntervenantExternes)) {
                      $this->initCommonTCAOIntervenantExternes(false);

                      foreach ($collCommonTCAOIntervenantExternes as $obj) {
                        if (false == $this->collCommonTCAOIntervenantExternes->contains($obj)) {
                          $this->collCommonTCAOIntervenantExternes->append($obj);
                        }
                      }

                      $this->collCommonTCAOIntervenantExternesPartial = true;
                    }

                    $collCommonTCAOIntervenantExternes->getInternalIterator()->rewind();

                    return $collCommonTCAOIntervenantExternes;
                }

                if ($partial && $this->collCommonTCAOIntervenantExternes) {
                    foreach ($this->collCommonTCAOIntervenantExternes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOIntervenantExternes[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOIntervenantExternes = $collCommonTCAOIntervenantExternes;
                $this->collCommonTCAOIntervenantExternesPartial = false;
            }
        }

        return $this->collCommonTCAOIntervenantExternes;
    }

    /**
     * Sets a collection of CommonTCAOIntervenantExterne objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOIntervenantExternes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCAOIntervenantExternes(PropelCollection $commonTCAOIntervenantExternes, PropelPDO $con = null)
    {
        $commonTCAOIntervenantExternesToDelete = $this->getCommonTCAOIntervenantExternes(new Criteria(), $con)->diff($commonTCAOIntervenantExternes);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTCAOIntervenantExternesScheduledForDeletion = clone $commonTCAOIntervenantExternesToDelete;

        foreach ($commonTCAOIntervenantExternesToDelete as $commonTCAOIntervenantExterneRemoved) {
            $commonTCAOIntervenantExterneRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCAOIntervenantExternes = null;
        foreach ($commonTCAOIntervenantExternes as $commonTCAOIntervenantExterne) {
            $this->addCommonTCAOIntervenantExterne($commonTCAOIntervenantExterne);
        }

        $this->collCommonTCAOIntervenantExternes = $commonTCAOIntervenantExternes;
        $this->collCommonTCAOIntervenantExternesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOIntervenantExterne objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOIntervenantExterne objects.
     * @throws PropelException
     */
    public function countCommonTCAOIntervenantExternes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOIntervenantExternesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOIntervenantExternes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOIntervenantExternes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOIntervenantExternes());
            }
            $query = CommonTCAOIntervenantExterneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCAOIntervenantExternes);
    }

    /**
     * Method called to associate a CommonTCAOIntervenantExterne object to this object
     * through the CommonTCAOIntervenantExterne foreign key attribute.
     *
     * @param   CommonTCAOIntervenantExterne $l CommonTCAOIntervenantExterne
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCAOIntervenantExterne(CommonTCAOIntervenantExterne $l)
    {
        if ($this->collCommonTCAOIntervenantExternes === null) {
            $this->initCommonTCAOIntervenantExternes();
            $this->collCommonTCAOIntervenantExternesPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOIntervenantExternes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOIntervenantExterne($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOIntervenantExterne $commonTCAOIntervenantExterne The commonTCAOIntervenantExterne object to add.
     */
    protected function doAddCommonTCAOIntervenantExterne($commonTCAOIntervenantExterne)
    {
        $this->collCommonTCAOIntervenantExternes[]= $commonTCAOIntervenantExterne;
        $commonTCAOIntervenantExterne->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCAOIntervenantExterne $commonTCAOIntervenantExterne The commonTCAOIntervenantExterne object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCAOIntervenantExterne($commonTCAOIntervenantExterne)
    {
        if ($this->getCommonTCAOIntervenantExternes()->contains($commonTCAOIntervenantExterne)) {
            $this->collCommonTCAOIntervenantExternes->remove($this->collCommonTCAOIntervenantExternes->search($commonTCAOIntervenantExterne));
            if (null === $this->commonTCAOIntervenantExternesScheduledForDeletion) {
                $this->commonTCAOIntervenantExternesScheduledForDeletion = clone $this->collCommonTCAOIntervenantExternes;
                $this->commonTCAOIntervenantExternesScheduledForDeletion->clear();
            }
            $this->commonTCAOIntervenantExternesScheduledForDeletion[]= clone $commonTCAOIntervenantExterne;
            $commonTCAOIntervenantExterne->setCommonOrganisme(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTCAOOrdreDuJours collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCAOOrdreDuJours()
     */
    public function clearCommonTCAOOrdreDuJours()
    {
        $this->collCommonTCAOOrdreDuJours = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOOrdreDuJoursPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOOrdreDuJours collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOOrdreDuJours($v = true)
    {
        $this->collCommonTCAOOrdreDuJoursPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOOrdreDuJours collection.
     *
     * By default this just sets the collCommonTCAOOrdreDuJours collection to an empty array (like clearcollCommonTCAOOrdreDuJours());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOOrdreDuJours($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOOrdreDuJours && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOOrdreDuJours = new PropelObjectCollection();
        $this->collCommonTCAOOrdreDuJours->setModel('CommonTCAOOrdreDuJour');
    }

    /**
     * Gets an array of CommonTCAOOrdreDuJour objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOOrdreDuJour[] List of CommonTCAOOrdreDuJour objects
     * @throws PropelException
     */
    public function getCommonTCAOOrdreDuJours($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOOrdreDuJoursPartial && !$this->isNew();
        if (null === $this->collCommonTCAOOrdreDuJours || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOOrdreDuJours) {
                // return empty collection
                $this->initCommonTCAOOrdreDuJours();
            } else {
                $collCommonTCAOOrdreDuJours = CommonTCAOOrdreDuJourQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOOrdreDuJoursPartial && count($collCommonTCAOOrdreDuJours)) {
                      $this->initCommonTCAOOrdreDuJours(false);

                      foreach ($collCommonTCAOOrdreDuJours as $obj) {
                        if (false == $this->collCommonTCAOOrdreDuJours->contains($obj)) {
                          $this->collCommonTCAOOrdreDuJours->append($obj);
                        }
                      }

                      $this->collCommonTCAOOrdreDuJoursPartial = true;
                    }

                    $collCommonTCAOOrdreDuJours->getInternalIterator()->rewind();

                    return $collCommonTCAOOrdreDuJours;
                }

                if ($partial && $this->collCommonTCAOOrdreDuJours) {
                    foreach ($this->collCommonTCAOOrdreDuJours as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOOrdreDuJours[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOOrdreDuJours = $collCommonTCAOOrdreDuJours;
                $this->collCommonTCAOOrdreDuJoursPartial = false;
            }
        }

        return $this->collCommonTCAOOrdreDuJours;
    }

    /**
     * Sets a collection of CommonTCAOOrdreDuJour objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOOrdreDuJours A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCAOOrdreDuJours(PropelCollection $commonTCAOOrdreDuJours, PropelPDO $con = null)
    {
        $commonTCAOOrdreDuJoursToDelete = $this->getCommonTCAOOrdreDuJours(new Criteria(), $con)->diff($commonTCAOOrdreDuJours);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTCAOOrdreDuJoursScheduledForDeletion = clone $commonTCAOOrdreDuJoursToDelete;

        foreach ($commonTCAOOrdreDuJoursToDelete as $commonTCAOOrdreDuJourRemoved) {
            $commonTCAOOrdreDuJourRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCAOOrdreDuJours = null;
        foreach ($commonTCAOOrdreDuJours as $commonTCAOOrdreDuJour) {
            $this->addCommonTCAOOrdreDuJour($commonTCAOOrdreDuJour);
        }

        $this->collCommonTCAOOrdreDuJours = $commonTCAOOrdreDuJours;
        $this->collCommonTCAOOrdreDuJoursPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOOrdreDuJour objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOOrdreDuJour objects.
     * @throws PropelException
     */
    public function countCommonTCAOOrdreDuJours(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOOrdreDuJoursPartial && !$this->isNew();
        if (null === $this->collCommonTCAOOrdreDuJours || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOOrdreDuJours) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOOrdreDuJours());
            }
            $query = CommonTCAOOrdreDuJourQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCAOOrdreDuJours);
    }

    /**
     * Method called to associate a CommonTCAOOrdreDuJour object to this object
     * through the CommonTCAOOrdreDuJour foreign key attribute.
     *
     * @param   CommonTCAOOrdreDuJour $l CommonTCAOOrdreDuJour
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCAOOrdreDuJour(CommonTCAOOrdreDuJour $l)
    {
        if ($this->collCommonTCAOOrdreDuJours === null) {
            $this->initCommonTCAOOrdreDuJours();
            $this->collCommonTCAOOrdreDuJoursPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOOrdreDuJours->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOOrdreDuJour($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOOrdreDuJour $commonTCAOOrdreDuJour The commonTCAOOrdreDuJour object to add.
     */
    protected function doAddCommonTCAOOrdreDuJour($commonTCAOOrdreDuJour)
    {
        $this->collCommonTCAOOrdreDuJours[]= $commonTCAOOrdreDuJour;
        $commonTCAOOrdreDuJour->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCAOOrdreDuJour $commonTCAOOrdreDuJour The commonTCAOOrdreDuJour object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCAOOrdreDuJour($commonTCAOOrdreDuJour)
    {
        if ($this->getCommonTCAOOrdreDuJours()->contains($commonTCAOOrdreDuJour)) {
            $this->collCommonTCAOOrdreDuJours->remove($this->collCommonTCAOOrdreDuJours->search($commonTCAOOrdreDuJour));
            if (null === $this->commonTCAOOrdreDuJoursScheduledForDeletion) {
                $this->commonTCAOOrdreDuJoursScheduledForDeletion = clone $this->collCommonTCAOOrdreDuJours;
                $this->commonTCAOOrdreDuJoursScheduledForDeletion->clear();
            }
            $this->commonTCAOOrdreDuJoursScheduledForDeletion[]= clone $commonTCAOOrdreDuJour;
            $commonTCAOOrdreDuJour->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOOrdreDuJours from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOOrdreDuJour[] List of CommonTCAOOrdreDuJour objects
     */
    public function getCommonTCAOOrdreDuJoursJoinCommonTCAOSeanceRelatedByDateSeance($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOOrdreDuJourQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOSeanceRelatedByDateSeance', $join_behavior);

        return $this->getCommonTCAOOrdreDuJours($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOOrdreDuJours from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOOrdreDuJour[] List of CommonTCAOOrdreDuJour objects
     */
    public function getCommonTCAOOrdreDuJoursJoinCommonTCAOCommissionConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOOrdreDuJourQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOCommissionConsultation', $join_behavior);

        return $this->getCommonTCAOOrdreDuJours($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOOrdreDuJours from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOOrdreDuJour[] List of CommonTCAOOrdreDuJour objects
     */
    public function getCommonTCAOOrdreDuJoursJoinCommonTCAOCommission($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOOrdreDuJourQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOCommission', $join_behavior);

        return $this->getCommonTCAOOrdreDuJours($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOOrdreDuJours from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOOrdreDuJour[] List of CommonTCAOOrdreDuJour objects
     */
    public function getCommonTCAOOrdreDuJoursJoinCommonTCAOSeanceRelatedByIdSeance($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOOrdreDuJourQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOSeanceRelatedByIdSeance', $join_behavior);

        return $this->getCommonTCAOOrdreDuJours($query, $con);
    }

    /**
     * Clears out the collCommonTCAOOrdreDuJourIntervenants collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCAOOrdreDuJourIntervenants()
     */
    public function clearCommonTCAOOrdreDuJourIntervenants()
    {
        $this->collCommonTCAOOrdreDuJourIntervenants = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOOrdreDuJourIntervenantsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOOrdreDuJourIntervenants collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOOrdreDuJourIntervenants($v = true)
    {
        $this->collCommonTCAOOrdreDuJourIntervenantsPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOOrdreDuJourIntervenants collection.
     *
     * By default this just sets the collCommonTCAOOrdreDuJourIntervenants collection to an empty array (like clearcollCommonTCAOOrdreDuJourIntervenants());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOOrdreDuJourIntervenants($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOOrdreDuJourIntervenants && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOOrdreDuJourIntervenants = new PropelObjectCollection();
        $this->collCommonTCAOOrdreDuJourIntervenants->setModel('CommonTCAOOrdreDuJourIntervenant');
    }

    /**
     * Gets an array of CommonTCAOOrdreDuJourIntervenant objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOOrdreDuJourIntervenant[] List of CommonTCAOOrdreDuJourIntervenant objects
     * @throws PropelException
     */
    public function getCommonTCAOOrdreDuJourIntervenants($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOOrdreDuJourIntervenantsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOOrdreDuJourIntervenants || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOOrdreDuJourIntervenants) {
                // return empty collection
                $this->initCommonTCAOOrdreDuJourIntervenants();
            } else {
                $collCommonTCAOOrdreDuJourIntervenants = CommonTCAOOrdreDuJourIntervenantQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOOrdreDuJourIntervenantsPartial && count($collCommonTCAOOrdreDuJourIntervenants)) {
                      $this->initCommonTCAOOrdreDuJourIntervenants(false);

                      foreach ($collCommonTCAOOrdreDuJourIntervenants as $obj) {
                        if (false == $this->collCommonTCAOOrdreDuJourIntervenants->contains($obj)) {
                          $this->collCommonTCAOOrdreDuJourIntervenants->append($obj);
                        }
                      }

                      $this->collCommonTCAOOrdreDuJourIntervenantsPartial = true;
                    }

                    $collCommonTCAOOrdreDuJourIntervenants->getInternalIterator()->rewind();

                    return $collCommonTCAOOrdreDuJourIntervenants;
                }

                if ($partial && $this->collCommonTCAOOrdreDuJourIntervenants) {
                    foreach ($this->collCommonTCAOOrdreDuJourIntervenants as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOOrdreDuJourIntervenants[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOOrdreDuJourIntervenants = $collCommonTCAOOrdreDuJourIntervenants;
                $this->collCommonTCAOOrdreDuJourIntervenantsPartial = false;
            }
        }

        return $this->collCommonTCAOOrdreDuJourIntervenants;
    }

    /**
     * Sets a collection of CommonTCAOOrdreDuJourIntervenant objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOOrdreDuJourIntervenants A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCAOOrdreDuJourIntervenants(PropelCollection $commonTCAOOrdreDuJourIntervenants, PropelPDO $con = null)
    {
        $commonTCAOOrdreDuJourIntervenantsToDelete = $this->getCommonTCAOOrdreDuJourIntervenants(new Criteria(), $con)->diff($commonTCAOOrdreDuJourIntervenants);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion = clone $commonTCAOOrdreDuJourIntervenantsToDelete;

        foreach ($commonTCAOOrdreDuJourIntervenantsToDelete as $commonTCAOOrdreDuJourIntervenantRemoved) {
            $commonTCAOOrdreDuJourIntervenantRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCAOOrdreDuJourIntervenants = null;
        foreach ($commonTCAOOrdreDuJourIntervenants as $commonTCAOOrdreDuJourIntervenant) {
            $this->addCommonTCAOOrdreDuJourIntervenant($commonTCAOOrdreDuJourIntervenant);
        }

        $this->collCommonTCAOOrdreDuJourIntervenants = $commonTCAOOrdreDuJourIntervenants;
        $this->collCommonTCAOOrdreDuJourIntervenantsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOOrdreDuJourIntervenant objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOOrdreDuJourIntervenant objects.
     * @throws PropelException
     */
    public function countCommonTCAOOrdreDuJourIntervenants(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOOrdreDuJourIntervenantsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOOrdreDuJourIntervenants || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOOrdreDuJourIntervenants) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOOrdreDuJourIntervenants());
            }
            $query = CommonTCAOOrdreDuJourIntervenantQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCAOOrdreDuJourIntervenants);
    }

    /**
     * Method called to associate a CommonTCAOOrdreDuJourIntervenant object to this object
     * through the CommonTCAOOrdreDuJourIntervenant foreign key attribute.
     *
     * @param   CommonTCAOOrdreDuJourIntervenant $l CommonTCAOOrdreDuJourIntervenant
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCAOOrdreDuJourIntervenant(CommonTCAOOrdreDuJourIntervenant $l)
    {
        if ($this->collCommonTCAOOrdreDuJourIntervenants === null) {
            $this->initCommonTCAOOrdreDuJourIntervenants();
            $this->collCommonTCAOOrdreDuJourIntervenantsPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOOrdreDuJourIntervenants->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOOrdreDuJourIntervenant($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOOrdreDuJourIntervenant $commonTCAOOrdreDuJourIntervenant The commonTCAOOrdreDuJourIntervenant object to add.
     */
    protected function doAddCommonTCAOOrdreDuJourIntervenant($commonTCAOOrdreDuJourIntervenant)
    {
        $this->collCommonTCAOOrdreDuJourIntervenants[]= $commonTCAOOrdreDuJourIntervenant;
        $commonTCAOOrdreDuJourIntervenant->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCAOOrdreDuJourIntervenant $commonTCAOOrdreDuJourIntervenant The commonTCAOOrdreDuJourIntervenant object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCAOOrdreDuJourIntervenant($commonTCAOOrdreDuJourIntervenant)
    {
        if ($this->getCommonTCAOOrdreDuJourIntervenants()->contains($commonTCAOOrdreDuJourIntervenant)) {
            $this->collCommonTCAOOrdreDuJourIntervenants->remove($this->collCommonTCAOOrdreDuJourIntervenants->search($commonTCAOOrdreDuJourIntervenant));
            if (null === $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion) {
                $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion = clone $this->collCommonTCAOOrdreDuJourIntervenants;
                $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion->clear();
            }
            $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion[]= clone $commonTCAOOrdreDuJourIntervenant;
            $commonTCAOOrdreDuJourIntervenant->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOOrdreDuJourIntervenants from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOOrdreDuJourIntervenant[] List of CommonTCAOOrdreDuJourIntervenant objects
     */
    public function getCommonTCAOOrdreDuJourIntervenantsJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOOrdreDuJourIntervenantQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonTCAOOrdreDuJourIntervenants($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOOrdreDuJourIntervenants from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOOrdreDuJourIntervenant[] List of CommonTCAOOrdreDuJourIntervenant objects
     */
    public function getCommonTCAOOrdreDuJourIntervenantsJoinCommonTCAOIntervenantExterne($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOOrdreDuJourIntervenantQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOIntervenantExterne', $join_behavior);

        return $this->getCommonTCAOOrdreDuJourIntervenants($query, $con);
    }

    /**
     * Clears out the collCommonTCAOSeances collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCAOSeances()
     */
    public function clearCommonTCAOSeances()
    {
        $this->collCommonTCAOSeances = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOSeancesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOSeances collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOSeances($v = true)
    {
        $this->collCommonTCAOSeancesPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOSeances collection.
     *
     * By default this just sets the collCommonTCAOSeances collection to an empty array (like clearcollCommonTCAOSeances());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOSeances($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOSeances && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOSeances = new PropelObjectCollection();
        $this->collCommonTCAOSeances->setModel('CommonTCAOSeance');
    }

    /**
     * Gets an array of CommonTCAOSeance objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOSeance[] List of CommonTCAOSeance objects
     * @throws PropelException
     */
    public function getCommonTCAOSeances($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeancesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeances || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeances) {
                // return empty collection
                $this->initCommonTCAOSeances();
            } else {
                $collCommonTCAOSeances = CommonTCAOSeanceQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOSeancesPartial && count($collCommonTCAOSeances)) {
                      $this->initCommonTCAOSeances(false);

                      foreach ($collCommonTCAOSeances as $obj) {
                        if (false == $this->collCommonTCAOSeances->contains($obj)) {
                          $this->collCommonTCAOSeances->append($obj);
                        }
                      }

                      $this->collCommonTCAOSeancesPartial = true;
                    }

                    $collCommonTCAOSeances->getInternalIterator()->rewind();

                    return $collCommonTCAOSeances;
                }

                if ($partial && $this->collCommonTCAOSeances) {
                    foreach ($this->collCommonTCAOSeances as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOSeances[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOSeances = $collCommonTCAOSeances;
                $this->collCommonTCAOSeancesPartial = false;
            }
        }

        return $this->collCommonTCAOSeances;
    }

    /**
     * Sets a collection of CommonTCAOSeance objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOSeances A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCAOSeances(PropelCollection $commonTCAOSeances, PropelPDO $con = null)
    {
        $commonTCAOSeancesToDelete = $this->getCommonTCAOSeances(new Criteria(), $con)->diff($commonTCAOSeances);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTCAOSeancesScheduledForDeletion = clone $commonTCAOSeancesToDelete;

        foreach ($commonTCAOSeancesToDelete as $commonTCAOSeanceRemoved) {
            $commonTCAOSeanceRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCAOSeances = null;
        foreach ($commonTCAOSeances as $commonTCAOSeance) {
            $this->addCommonTCAOSeance($commonTCAOSeance);
        }

        $this->collCommonTCAOSeances = $commonTCAOSeances;
        $this->collCommonTCAOSeancesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOSeance objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOSeance objects.
     * @throws PropelException
     */
    public function countCommonTCAOSeances(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeancesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeances || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeances) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOSeances());
            }
            $query = CommonTCAOSeanceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCAOSeances);
    }

    /**
     * Method called to associate a CommonTCAOSeance object to this object
     * through the CommonTCAOSeance foreign key attribute.
     *
     * @param   CommonTCAOSeance $l CommonTCAOSeance
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCAOSeance(CommonTCAOSeance $l)
    {
        if ($this->collCommonTCAOSeances === null) {
            $this->initCommonTCAOSeances();
            $this->collCommonTCAOSeancesPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOSeances->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOSeance($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOSeance $commonTCAOSeance The commonTCAOSeance object to add.
     */
    protected function doAddCommonTCAOSeance($commonTCAOSeance)
    {
        $this->collCommonTCAOSeances[]= $commonTCAOSeance;
        $commonTCAOSeance->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCAOSeance $commonTCAOSeance The commonTCAOSeance object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCAOSeance($commonTCAOSeance)
    {
        if ($this->getCommonTCAOSeances()->contains($commonTCAOSeance)) {
            $this->collCommonTCAOSeances->remove($this->collCommonTCAOSeances->search($commonTCAOSeance));
            if (null === $this->commonTCAOSeancesScheduledForDeletion) {
                $this->commonTCAOSeancesScheduledForDeletion = clone $this->collCommonTCAOSeances;
                $this->commonTCAOSeancesScheduledForDeletion->clear();
            }
            $this->commonTCAOSeancesScheduledForDeletion[]= clone $commonTCAOSeance;
            $commonTCAOSeance->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOSeances from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOSeance[] List of CommonTCAOSeance objects
     */
    public function getCommonTCAOSeancesJoinCommonTCAOCommission($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOSeanceQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOCommission', $join_behavior);

        return $this->getCommonTCAOSeances($query, $con);
    }

    /**
     * Clears out the collCommonTCAOSeanceAgents collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCAOSeanceAgents()
     */
    public function clearCommonTCAOSeanceAgents()
    {
        $this->collCommonTCAOSeanceAgents = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOSeanceAgentsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOSeanceAgents collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOSeanceAgents($v = true)
    {
        $this->collCommonTCAOSeanceAgentsPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOSeanceAgents collection.
     *
     * By default this just sets the collCommonTCAOSeanceAgents collection to an empty array (like clearcollCommonTCAOSeanceAgents());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOSeanceAgents($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOSeanceAgents && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOSeanceAgents = new PropelObjectCollection();
        $this->collCommonTCAOSeanceAgents->setModel('CommonTCAOSeanceAgent');
    }

    /**
     * Gets an array of CommonTCAOSeanceAgent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOSeanceAgent[] List of CommonTCAOSeanceAgent objects
     * @throws PropelException
     */
    public function getCommonTCAOSeanceAgents($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeanceAgentsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeanceAgents || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeanceAgents) {
                // return empty collection
                $this->initCommonTCAOSeanceAgents();
            } else {
                $collCommonTCAOSeanceAgents = CommonTCAOSeanceAgentQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOSeanceAgentsPartial && count($collCommonTCAOSeanceAgents)) {
                      $this->initCommonTCAOSeanceAgents(false);

                      foreach ($collCommonTCAOSeanceAgents as $obj) {
                        if (false == $this->collCommonTCAOSeanceAgents->contains($obj)) {
                          $this->collCommonTCAOSeanceAgents->append($obj);
                        }
                      }

                      $this->collCommonTCAOSeanceAgentsPartial = true;
                    }

                    $collCommonTCAOSeanceAgents->getInternalIterator()->rewind();

                    return $collCommonTCAOSeanceAgents;
                }

                if ($partial && $this->collCommonTCAOSeanceAgents) {
                    foreach ($this->collCommonTCAOSeanceAgents as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOSeanceAgents[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOSeanceAgents = $collCommonTCAOSeanceAgents;
                $this->collCommonTCAOSeanceAgentsPartial = false;
            }
        }

        return $this->collCommonTCAOSeanceAgents;
    }

    /**
     * Sets a collection of CommonTCAOSeanceAgent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOSeanceAgents A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCAOSeanceAgents(PropelCollection $commonTCAOSeanceAgents, PropelPDO $con = null)
    {
        $commonTCAOSeanceAgentsToDelete = $this->getCommonTCAOSeanceAgents(new Criteria(), $con)->diff($commonTCAOSeanceAgents);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTCAOSeanceAgentsScheduledForDeletion = clone $commonTCAOSeanceAgentsToDelete;

        foreach ($commonTCAOSeanceAgentsToDelete as $commonTCAOSeanceAgentRemoved) {
            $commonTCAOSeanceAgentRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCAOSeanceAgents = null;
        foreach ($commonTCAOSeanceAgents as $commonTCAOSeanceAgent) {
            $this->addCommonTCAOSeanceAgent($commonTCAOSeanceAgent);
        }

        $this->collCommonTCAOSeanceAgents = $commonTCAOSeanceAgents;
        $this->collCommonTCAOSeanceAgentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOSeanceAgent objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOSeanceAgent objects.
     * @throws PropelException
     */
    public function countCommonTCAOSeanceAgents(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeanceAgentsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeanceAgents || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeanceAgents) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOSeanceAgents());
            }
            $query = CommonTCAOSeanceAgentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCAOSeanceAgents);
    }

    /**
     * Method called to associate a CommonTCAOSeanceAgent object to this object
     * through the CommonTCAOSeanceAgent foreign key attribute.
     *
     * @param   CommonTCAOSeanceAgent $l CommonTCAOSeanceAgent
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCAOSeanceAgent(CommonTCAOSeanceAgent $l)
    {
        if ($this->collCommonTCAOSeanceAgents === null) {
            $this->initCommonTCAOSeanceAgents();
            $this->collCommonTCAOSeanceAgentsPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOSeanceAgents->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOSeanceAgent($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOSeanceAgent $commonTCAOSeanceAgent The commonTCAOSeanceAgent object to add.
     */
    protected function doAddCommonTCAOSeanceAgent($commonTCAOSeanceAgent)
    {
        $this->collCommonTCAOSeanceAgents[]= $commonTCAOSeanceAgent;
        $commonTCAOSeanceAgent->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCAOSeanceAgent $commonTCAOSeanceAgent The commonTCAOSeanceAgent object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCAOSeanceAgent($commonTCAOSeanceAgent)
    {
        if ($this->getCommonTCAOSeanceAgents()->contains($commonTCAOSeanceAgent)) {
            $this->collCommonTCAOSeanceAgents->remove($this->collCommonTCAOSeanceAgents->search($commonTCAOSeanceAgent));
            if (null === $this->commonTCAOSeanceAgentsScheduledForDeletion) {
                $this->commonTCAOSeanceAgentsScheduledForDeletion = clone $this->collCommonTCAOSeanceAgents;
                $this->commonTCAOSeanceAgentsScheduledForDeletion->clear();
            }
            $this->commonTCAOSeanceAgentsScheduledForDeletion[]= clone $commonTCAOSeanceAgent;
            $commonTCAOSeanceAgent->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOSeanceAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOSeanceAgent[] List of CommonTCAOSeanceAgent objects
     */
    public function getCommonTCAOSeanceAgentsJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOSeanceAgentQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonTCAOSeanceAgents($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOSeanceAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOSeanceAgent[] List of CommonTCAOSeanceAgent objects
     */
    public function getCommonTCAOSeanceAgentsJoinCommonTCAOSeance($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOSeanceAgentQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOSeance', $join_behavior);

        return $this->getCommonTCAOSeanceAgents($query, $con);
    }

    /**
     * Clears out the collCommonTCAOSeanceIntervenantExternes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCAOSeanceIntervenantExternes()
     */
    public function clearCommonTCAOSeanceIntervenantExternes()
    {
        $this->collCommonTCAOSeanceIntervenantExternes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOSeanceIntervenantExternesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOSeanceIntervenantExternes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOSeanceIntervenantExternes($v = true)
    {
        $this->collCommonTCAOSeanceIntervenantExternesPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOSeanceIntervenantExternes collection.
     *
     * By default this just sets the collCommonTCAOSeanceIntervenantExternes collection to an empty array (like clearcollCommonTCAOSeanceIntervenantExternes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOSeanceIntervenantExternes($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOSeanceIntervenantExternes && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOSeanceIntervenantExternes = new PropelObjectCollection();
        $this->collCommonTCAOSeanceIntervenantExternes->setModel('CommonTCAOSeanceIntervenantExterne');
    }

    /**
     * Gets an array of CommonTCAOSeanceIntervenantExterne objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOSeanceIntervenantExterne[] List of CommonTCAOSeanceIntervenantExterne objects
     * @throws PropelException
     */
    public function getCommonTCAOSeanceIntervenantExternes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeanceIntervenantExternesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeanceIntervenantExternes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeanceIntervenantExternes) {
                // return empty collection
                $this->initCommonTCAOSeanceIntervenantExternes();
            } else {
                $collCommonTCAOSeanceIntervenantExternes = CommonTCAOSeanceIntervenantExterneQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOSeanceIntervenantExternesPartial && count($collCommonTCAOSeanceIntervenantExternes)) {
                      $this->initCommonTCAOSeanceIntervenantExternes(false);

                      foreach ($collCommonTCAOSeanceIntervenantExternes as $obj) {
                        if (false == $this->collCommonTCAOSeanceIntervenantExternes->contains($obj)) {
                          $this->collCommonTCAOSeanceIntervenantExternes->append($obj);
                        }
                      }

                      $this->collCommonTCAOSeanceIntervenantExternesPartial = true;
                    }

                    $collCommonTCAOSeanceIntervenantExternes->getInternalIterator()->rewind();

                    return $collCommonTCAOSeanceIntervenantExternes;
                }

                if ($partial && $this->collCommonTCAOSeanceIntervenantExternes) {
                    foreach ($this->collCommonTCAOSeanceIntervenantExternes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOSeanceIntervenantExternes[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOSeanceIntervenantExternes = $collCommonTCAOSeanceIntervenantExternes;
                $this->collCommonTCAOSeanceIntervenantExternesPartial = false;
            }
        }

        return $this->collCommonTCAOSeanceIntervenantExternes;
    }

    /**
     * Sets a collection of CommonTCAOSeanceIntervenantExterne objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOSeanceIntervenantExternes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCAOSeanceIntervenantExternes(PropelCollection $commonTCAOSeanceIntervenantExternes, PropelPDO $con = null)
    {
        $commonTCAOSeanceIntervenantExternesToDelete = $this->getCommonTCAOSeanceIntervenantExternes(new Criteria(), $con)->diff($commonTCAOSeanceIntervenantExternes);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonTCAOSeanceIntervenantExternesScheduledForDeletion = clone $commonTCAOSeanceIntervenantExternesToDelete;

        foreach ($commonTCAOSeanceIntervenantExternesToDelete as $commonTCAOSeanceIntervenantExterneRemoved) {
            $commonTCAOSeanceIntervenantExterneRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCAOSeanceIntervenantExternes = null;
        foreach ($commonTCAOSeanceIntervenantExternes as $commonTCAOSeanceIntervenantExterne) {
            $this->addCommonTCAOSeanceIntervenantExterne($commonTCAOSeanceIntervenantExterne);
        }

        $this->collCommonTCAOSeanceIntervenantExternes = $commonTCAOSeanceIntervenantExternes;
        $this->collCommonTCAOSeanceIntervenantExternesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOSeanceIntervenantExterne objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOSeanceIntervenantExterne objects.
     * @throws PropelException
     */
    public function countCommonTCAOSeanceIntervenantExternes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeanceIntervenantExternesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeanceIntervenantExternes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeanceIntervenantExternes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOSeanceIntervenantExternes());
            }
            $query = CommonTCAOSeanceIntervenantExterneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCAOSeanceIntervenantExternes);
    }

    /**
     * Method called to associate a CommonTCAOSeanceIntervenantExterne object to this object
     * through the CommonTCAOSeanceIntervenantExterne foreign key attribute.
     *
     * @param   CommonTCAOSeanceIntervenantExterne $l CommonTCAOSeanceIntervenantExterne
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCAOSeanceIntervenantExterne(CommonTCAOSeanceIntervenantExterne $l)
    {
        if ($this->collCommonTCAOSeanceIntervenantExternes === null) {
            $this->initCommonTCAOSeanceIntervenantExternes();
            $this->collCommonTCAOSeanceIntervenantExternesPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOSeanceIntervenantExternes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOSeanceIntervenantExterne($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOSeanceIntervenantExterne $commonTCAOSeanceIntervenantExterne The commonTCAOSeanceIntervenantExterne object to add.
     */
    protected function doAddCommonTCAOSeanceIntervenantExterne($commonTCAOSeanceIntervenantExterne)
    {
        $this->collCommonTCAOSeanceIntervenantExternes[]= $commonTCAOSeanceIntervenantExterne;
        $commonTCAOSeanceIntervenantExterne->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCAOSeanceIntervenantExterne $commonTCAOSeanceIntervenantExterne The commonTCAOSeanceIntervenantExterne object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCAOSeanceIntervenantExterne($commonTCAOSeanceIntervenantExterne)
    {
        if ($this->getCommonTCAOSeanceIntervenantExternes()->contains($commonTCAOSeanceIntervenantExterne)) {
            $this->collCommonTCAOSeanceIntervenantExternes->remove($this->collCommonTCAOSeanceIntervenantExternes->search($commonTCAOSeanceIntervenantExterne));
            if (null === $this->commonTCAOSeanceIntervenantExternesScheduledForDeletion) {
                $this->commonTCAOSeanceIntervenantExternesScheduledForDeletion = clone $this->collCommonTCAOSeanceIntervenantExternes;
                $this->commonTCAOSeanceIntervenantExternesScheduledForDeletion->clear();
            }
            $this->commonTCAOSeanceIntervenantExternesScheduledForDeletion[]= clone $commonTCAOSeanceIntervenantExterne;
            $commonTCAOSeanceIntervenantExterne->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOSeanceIntervenantExternes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOSeanceIntervenantExterne[] List of CommonTCAOSeanceIntervenantExterne objects
     */
    public function getCommonTCAOSeanceIntervenantExternesJoinCommonTCAOIntervenantExterne($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOSeanceIntervenantExterneQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOIntervenantExterne', $join_behavior);

        return $this->getCommonTCAOSeanceIntervenantExternes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCAOSeanceIntervenantExternes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOSeanceIntervenantExterne[] List of CommonTCAOSeanceIntervenantExterne objects
     */
    public function getCommonTCAOSeanceIntervenantExternesJoinCommonTCAOSeance($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOSeanceIntervenantExterneQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOSeance', $join_behavior);

        return $this->getCommonTCAOSeanceIntervenantExternes($query, $con);
    }

    /**
     * Clears out the collCommonTCandidatures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTCandidatures()
     */
    public function clearCommonTCandidatures()
    {
        $this->collCommonTCandidatures = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCandidaturesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCandidatures collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCandidatures($v = true)
    {
        $this->collCommonTCandidaturesPartial = $v;
    }

    /**
     * Initializes the collCommonTCandidatures collection.
     *
     * By default this just sets the collCommonTCandidatures collection to an empty array (like clearcollCommonTCandidatures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCandidatures($overrideExisting = true)
    {
        if (null !== $this->collCommonTCandidatures && !$overrideExisting) {
            return;
        }
        $this->collCommonTCandidatures = new PropelObjectCollection();
        $this->collCommonTCandidatures->setModel('CommonTCandidature');
    }

    /**
     * Gets an array of CommonTCandidature objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     * @throws PropelException
     */
    public function getCommonTCandidatures($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatures || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatures) {
                // return empty collection
                $this->initCommonTCandidatures();
            } else {
                $collCommonTCandidatures = CommonTCandidatureQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCandidaturesPartial && count($collCommonTCandidatures)) {
                      $this->initCommonTCandidatures(false);

                      foreach ($collCommonTCandidatures as $obj) {
                        if (false == $this->collCommonTCandidatures->contains($obj)) {
                          $this->collCommonTCandidatures->append($obj);
                        }
                      }

                      $this->collCommonTCandidaturesPartial = true;
                    }

                    $collCommonTCandidatures->getInternalIterator()->rewind();

                    return $collCommonTCandidatures;
                }

                if ($partial && $this->collCommonTCandidatures) {
                    foreach ($this->collCommonTCandidatures as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCandidatures[] = $obj;
                        }
                    }
                }

                $this->collCommonTCandidatures = $collCommonTCandidatures;
                $this->collCommonTCandidaturesPartial = false;
            }
        }

        return $this->collCommonTCandidatures;
    }

    /**
     * Sets a collection of CommonTCandidature objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCandidatures A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTCandidatures(PropelCollection $commonTCandidatures, PropelPDO $con = null)
    {
        $commonTCandidaturesToDelete = $this->getCommonTCandidatures(new Criteria(), $con)->diff($commonTCandidatures);


        $this->commonTCandidaturesScheduledForDeletion = $commonTCandidaturesToDelete;

        foreach ($commonTCandidaturesToDelete as $commonTCandidatureRemoved) {
            $commonTCandidatureRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTCandidatures = null;
        foreach ($commonTCandidatures as $commonTCandidature) {
            $this->addCommonTCandidature($commonTCandidature);
        }

        $this->collCommonTCandidatures = $commonTCandidatures;
        $this->collCommonTCandidaturesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCandidature objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCandidature objects.
     * @throws PropelException
     */
    public function countCommonTCandidatures(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCandidatures());
            }
            $query = CommonTCandidatureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTCandidatures);
    }

    /**
     * Method called to associate a CommonTCandidature object to this object
     * through the CommonTCandidature foreign key attribute.
     *
     * @param   CommonTCandidature $l CommonTCandidature
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTCandidature(CommonTCandidature $l)
    {
        if ($this->collCommonTCandidatures === null) {
            $this->initCommonTCandidatures();
            $this->collCommonTCandidaturesPartial = true;
        }
        if (!in_array($l, $this->collCommonTCandidatures->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCandidature($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCandidature $commonTCandidature The commonTCandidature object to add.
     */
    protected function doAddCommonTCandidature($commonTCandidature)
    {
        $this->collCommonTCandidatures[]= $commonTCandidature;
        $commonTCandidature->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTCandidature $commonTCandidature The commonTCandidature object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTCandidature($commonTCandidature)
    {
        if ($this->getCommonTCandidatures()->contains($commonTCandidature)) {
            $this->collCommonTCandidatures->remove($this->collCommonTCandidatures->search($commonTCandidature));
            if (null === $this->commonTCandidaturesScheduledForDeletion) {
                $this->commonTCandidaturesScheduledForDeletion = clone $this->collCommonTCandidatures;
                $this->commonTCandidaturesScheduledForDeletion->clear();
            }
            $this->commonTCandidaturesScheduledForDeletion[]= clone $commonTCandidature;
            $commonTCandidature->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonTDumeContexte($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonTDumeContexte', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonOffres($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonOffres', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }

    /**
     * Clears out the collCommonTDumeContextes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTDumeContextes()
     */
    public function clearCommonTDumeContextes()
    {
        $this->collCommonTDumeContextes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTDumeContextesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTDumeContextes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTDumeContextes($v = true)
    {
        $this->collCommonTDumeContextesPartial = $v;
    }

    /**
     * Initializes the collCommonTDumeContextes collection.
     *
     * By default this just sets the collCommonTDumeContextes collection to an empty array (like clearcollCommonTDumeContextes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTDumeContextes($overrideExisting = true)
    {
        if (null !== $this->collCommonTDumeContextes && !$overrideExisting) {
            return;
        }
        $this->collCommonTDumeContextes = new PropelObjectCollection();
        $this->collCommonTDumeContextes->setModel('CommonTDumeContexte');
    }

    /**
     * Gets an array of CommonTDumeContexte objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTDumeContexte[] List of CommonTDumeContexte objects
     * @throws PropelException
     */
    public function getCommonTDumeContextes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDumeContextesPartial && !$this->isNew();
        if (null === $this->collCommonTDumeContextes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTDumeContextes) {
                // return empty collection
                $this->initCommonTDumeContextes();
            } else {
                $collCommonTDumeContextes = CommonTDumeContexteQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTDumeContextesPartial && count($collCommonTDumeContextes)) {
                      $this->initCommonTDumeContextes(false);

                      foreach ($collCommonTDumeContextes as $obj) {
                        if (false == $this->collCommonTDumeContextes->contains($obj)) {
                          $this->collCommonTDumeContextes->append($obj);
                        }
                      }

                      $this->collCommonTDumeContextesPartial = true;
                    }

                    $collCommonTDumeContextes->getInternalIterator()->rewind();

                    return $collCommonTDumeContextes;
                }

                if ($partial && $this->collCommonTDumeContextes) {
                    foreach ($this->collCommonTDumeContextes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTDumeContextes[] = $obj;
                        }
                    }
                }

                $this->collCommonTDumeContextes = $collCommonTDumeContextes;
                $this->collCommonTDumeContextesPartial = false;
            }
        }

        return $this->collCommonTDumeContextes;
    }

    /**
     * Sets a collection of CommonTDumeContexte objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTDumeContextes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTDumeContextes(PropelCollection $commonTDumeContextes, PropelPDO $con = null)
    {
        $commonTDumeContextesToDelete = $this->getCommonTDumeContextes(new Criteria(), $con)->diff($commonTDumeContextes);


        $this->commonTDumeContextesScheduledForDeletion = $commonTDumeContextesToDelete;

        foreach ($commonTDumeContextesToDelete as $commonTDumeContexteRemoved) {
            $commonTDumeContexteRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTDumeContextes = null;
        foreach ($commonTDumeContextes as $commonTDumeContexte) {
            $this->addCommonTDumeContexte($commonTDumeContexte);
        }

        $this->collCommonTDumeContextes = $commonTDumeContextes;
        $this->collCommonTDumeContextesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTDumeContexte objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTDumeContexte objects.
     * @throws PropelException
     */
    public function countCommonTDumeContextes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDumeContextesPartial && !$this->isNew();
        if (null === $this->collCommonTDumeContextes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTDumeContextes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTDumeContextes());
            }
            $query = CommonTDumeContexteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTDumeContextes);
    }

    /**
     * Method called to associate a CommonTDumeContexte object to this object
     * through the CommonTDumeContexte foreign key attribute.
     *
     * @param   CommonTDumeContexte $l CommonTDumeContexte
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTDumeContexte(CommonTDumeContexte $l)
    {
        if ($this->collCommonTDumeContextes === null) {
            $this->initCommonTDumeContextes();
            $this->collCommonTDumeContextesPartial = true;
        }
        if (!in_array($l, $this->collCommonTDumeContextes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTDumeContexte($l);
        }

        return $this;
    }

    /**
     * @param	CommonTDumeContexte $commonTDumeContexte The commonTDumeContexte object to add.
     */
    protected function doAddCommonTDumeContexte($commonTDumeContexte)
    {
        $this->collCommonTDumeContextes[]= $commonTDumeContexte;
        $commonTDumeContexte->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTDumeContexte $commonTDumeContexte The commonTDumeContexte object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTDumeContexte($commonTDumeContexte)
    {
        if ($this->getCommonTDumeContextes()->contains($commonTDumeContexte)) {
            $this->collCommonTDumeContextes->remove($this->collCommonTDumeContextes->search($commonTDumeContexte));
            if (null === $this->commonTDumeContextesScheduledForDeletion) {
                $this->commonTDumeContextesScheduledForDeletion = clone $this->collCommonTDumeContextes;
                $this->commonTDumeContextesScheduledForDeletion->clear();
            }
            $this->commonTDumeContextesScheduledForDeletion[]= clone $commonTDumeContexte;
            $commonTDumeContexte->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTDumeContextes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTDumeContexte[] List of CommonTDumeContexte objects
     */
    public function getCommonTDumeContextesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTDumeContexteQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTDumeContextes($query, $con);
    }

    /**
     * Clears out the collCommonTListeLotsCandidatures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTListeLotsCandidatures()
     */
    public function clearCommonTListeLotsCandidatures()
    {
        $this->collCommonTListeLotsCandidatures = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTListeLotsCandidaturesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTListeLotsCandidatures collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTListeLotsCandidatures($v = true)
    {
        $this->collCommonTListeLotsCandidaturesPartial = $v;
    }

    /**
     * Initializes the collCommonTListeLotsCandidatures collection.
     *
     * By default this just sets the collCommonTListeLotsCandidatures collection to an empty array (like clearcollCommonTListeLotsCandidatures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTListeLotsCandidatures($overrideExisting = true)
    {
        if (null !== $this->collCommonTListeLotsCandidatures && !$overrideExisting) {
            return;
        }
        $this->collCommonTListeLotsCandidatures = new PropelObjectCollection();
        $this->collCommonTListeLotsCandidatures->setModel('CommonTListeLotsCandidature');
    }

    /**
     * Gets an array of CommonTListeLotsCandidature objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     * @throws PropelException
     */
    public function getCommonTListeLotsCandidatures($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTListeLotsCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTListeLotsCandidatures || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTListeLotsCandidatures) {
                // return empty collection
                $this->initCommonTListeLotsCandidatures();
            } else {
                $collCommonTListeLotsCandidatures = CommonTListeLotsCandidatureQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTListeLotsCandidaturesPartial && count($collCommonTListeLotsCandidatures)) {
                      $this->initCommonTListeLotsCandidatures(false);

                      foreach ($collCommonTListeLotsCandidatures as $obj) {
                        if (false == $this->collCommonTListeLotsCandidatures->contains($obj)) {
                          $this->collCommonTListeLotsCandidatures->append($obj);
                        }
                      }

                      $this->collCommonTListeLotsCandidaturesPartial = true;
                    }

                    $collCommonTListeLotsCandidatures->getInternalIterator()->rewind();

                    return $collCommonTListeLotsCandidatures;
                }

                if ($partial && $this->collCommonTListeLotsCandidatures) {
                    foreach ($this->collCommonTListeLotsCandidatures as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTListeLotsCandidatures[] = $obj;
                        }
                    }
                }

                $this->collCommonTListeLotsCandidatures = $collCommonTListeLotsCandidatures;
                $this->collCommonTListeLotsCandidaturesPartial = false;
            }
        }

        return $this->collCommonTListeLotsCandidatures;
    }

    /**
     * Sets a collection of CommonTListeLotsCandidature objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTListeLotsCandidatures A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTListeLotsCandidatures(PropelCollection $commonTListeLotsCandidatures, PropelPDO $con = null)
    {
        $commonTListeLotsCandidaturesToDelete = $this->getCommonTListeLotsCandidatures(new Criteria(), $con)->diff($commonTListeLotsCandidatures);


        $this->commonTListeLotsCandidaturesScheduledForDeletion = $commonTListeLotsCandidaturesToDelete;

        foreach ($commonTListeLotsCandidaturesToDelete as $commonTListeLotsCandidatureRemoved) {
            $commonTListeLotsCandidatureRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTListeLotsCandidatures = null;
        foreach ($commonTListeLotsCandidatures as $commonTListeLotsCandidature) {
            $this->addCommonTListeLotsCandidature($commonTListeLotsCandidature);
        }

        $this->collCommonTListeLotsCandidatures = $commonTListeLotsCandidatures;
        $this->collCommonTListeLotsCandidaturesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTListeLotsCandidature objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTListeLotsCandidature objects.
     * @throws PropelException
     */
    public function countCommonTListeLotsCandidatures(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTListeLotsCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTListeLotsCandidatures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTListeLotsCandidatures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTListeLotsCandidatures());
            }
            $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTListeLotsCandidatures);
    }

    /**
     * Method called to associate a CommonTListeLotsCandidature object to this object
     * through the CommonTListeLotsCandidature foreign key attribute.
     *
     * @param   CommonTListeLotsCandidature $l CommonTListeLotsCandidature
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTListeLotsCandidature(CommonTListeLotsCandidature $l)
    {
        if ($this->collCommonTListeLotsCandidatures === null) {
            $this->initCommonTListeLotsCandidatures();
            $this->collCommonTListeLotsCandidaturesPartial = true;
        }
        if (!in_array($l, $this->collCommonTListeLotsCandidatures->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTListeLotsCandidature($l);
        }

        return $this;
    }

    /**
     * @param	CommonTListeLotsCandidature $commonTListeLotsCandidature The commonTListeLotsCandidature object to add.
     */
    protected function doAddCommonTListeLotsCandidature($commonTListeLotsCandidature)
    {
        $this->collCommonTListeLotsCandidatures[]= $commonTListeLotsCandidature;
        $commonTListeLotsCandidature->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTListeLotsCandidature $commonTListeLotsCandidature The commonTListeLotsCandidature object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTListeLotsCandidature($commonTListeLotsCandidature)
    {
        if ($this->getCommonTListeLotsCandidatures()->contains($commonTListeLotsCandidature)) {
            $this->collCommonTListeLotsCandidatures->remove($this->collCommonTListeLotsCandidatures->search($commonTListeLotsCandidature));
            if (null === $this->commonTListeLotsCandidaturesScheduledForDeletion) {
                $this->commonTListeLotsCandidaturesScheduledForDeletion = clone $this->collCommonTListeLotsCandidatures;
                $this->commonTListeLotsCandidaturesScheduledForDeletion->clear();
            }
            $this->commonTListeLotsCandidaturesScheduledForDeletion[]= clone $commonTListeLotsCandidature;
            $commonTListeLotsCandidature->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTListeLotsCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     */
    public function getCommonTListeLotsCandidaturesJoinCommonTCandidature($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonTCandidature', $join_behavior);

        return $this->getCommonTListeLotsCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTListeLotsCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     */
    public function getCommonTListeLotsCandidaturesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTListeLotsCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTListeLotsCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     */
    public function getCommonTListeLotsCandidaturesJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonTListeLotsCandidatures($query, $con);
    }

    /**
     * Clears out the collCommonTVisionRmaAgentOrganismes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOrganisme The current object (for fluent API support)
     * @see        addCommonTVisionRmaAgentOrganismes()
     */
    public function clearCommonTVisionRmaAgentOrganismes()
    {
        $this->collCommonTVisionRmaAgentOrganismes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTVisionRmaAgentOrganismesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTVisionRmaAgentOrganismes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTVisionRmaAgentOrganismes($v = true)
    {
        $this->collCommonTVisionRmaAgentOrganismesPartial = $v;
    }

    /**
     * Initializes the collCommonTVisionRmaAgentOrganismes collection.
     *
     * By default this just sets the collCommonTVisionRmaAgentOrganismes collection to an empty array (like clearcollCommonTVisionRmaAgentOrganismes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTVisionRmaAgentOrganismes($overrideExisting = true)
    {
        if (null !== $this->collCommonTVisionRmaAgentOrganismes && !$overrideExisting) {
            return;
        }
        $this->collCommonTVisionRmaAgentOrganismes = new PropelObjectCollection();
        $this->collCommonTVisionRmaAgentOrganismes->setModel('CommonTVisionRmaAgentOrganisme');
    }

    /**
     * Gets an array of CommonTVisionRmaAgentOrganisme objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOrganisme is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTVisionRmaAgentOrganisme[] List of CommonTVisionRmaAgentOrganisme objects
     * @throws PropelException
     */
    public function getCommonTVisionRmaAgentOrganismes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTVisionRmaAgentOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonTVisionRmaAgentOrganismes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTVisionRmaAgentOrganismes) {
                // return empty collection
                $this->initCommonTVisionRmaAgentOrganismes();
            } else {
                $collCommonTVisionRmaAgentOrganismes = CommonTVisionRmaAgentOrganismeQuery::create(null, $criteria)
                    ->filterByCommonOrganisme($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTVisionRmaAgentOrganismesPartial && count($collCommonTVisionRmaAgentOrganismes)) {
                      $this->initCommonTVisionRmaAgentOrganismes(false);

                      foreach ($collCommonTVisionRmaAgentOrganismes as $obj) {
                        if (false == $this->collCommonTVisionRmaAgentOrganismes->contains($obj)) {
                          $this->collCommonTVisionRmaAgentOrganismes->append($obj);
                        }
                      }

                      $this->collCommonTVisionRmaAgentOrganismesPartial = true;
                    }

                    $collCommonTVisionRmaAgentOrganismes->getInternalIterator()->rewind();

                    return $collCommonTVisionRmaAgentOrganismes;
                }

                if ($partial && $this->collCommonTVisionRmaAgentOrganismes) {
                    foreach ($this->collCommonTVisionRmaAgentOrganismes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTVisionRmaAgentOrganismes[] = $obj;
                        }
                    }
                }

                $this->collCommonTVisionRmaAgentOrganismes = $collCommonTVisionRmaAgentOrganismes;
                $this->collCommonTVisionRmaAgentOrganismesPartial = false;
            }
        }

        return $this->collCommonTVisionRmaAgentOrganismes;
    }

    /**
     * Sets a collection of CommonTVisionRmaAgentOrganisme objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTVisionRmaAgentOrganismes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function setCommonTVisionRmaAgentOrganismes(PropelCollection $commonTVisionRmaAgentOrganismes, PropelPDO $con = null)
    {
        $commonTVisionRmaAgentOrganismesToDelete = $this->getCommonTVisionRmaAgentOrganismes(new Criteria(), $con)->diff($commonTVisionRmaAgentOrganismes);


        $this->commonTVisionRmaAgentOrganismesScheduledForDeletion = $commonTVisionRmaAgentOrganismesToDelete;

        foreach ($commonTVisionRmaAgentOrganismesToDelete as $commonTVisionRmaAgentOrganismeRemoved) {
            $commonTVisionRmaAgentOrganismeRemoved->setCommonOrganisme(null);
        }

        $this->collCommonTVisionRmaAgentOrganismes = null;
        foreach ($commonTVisionRmaAgentOrganismes as $commonTVisionRmaAgentOrganisme) {
            $this->addCommonTVisionRmaAgentOrganisme($commonTVisionRmaAgentOrganisme);
        }

        $this->collCommonTVisionRmaAgentOrganismes = $commonTVisionRmaAgentOrganismes;
        $this->collCommonTVisionRmaAgentOrganismesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTVisionRmaAgentOrganisme objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTVisionRmaAgentOrganisme objects.
     * @throws PropelException
     */
    public function countCommonTVisionRmaAgentOrganismes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTVisionRmaAgentOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonTVisionRmaAgentOrganismes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTVisionRmaAgentOrganismes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTVisionRmaAgentOrganismes());
            }
            $query = CommonTVisionRmaAgentOrganismeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOrganisme($this)
                ->count($con);
        }

        return count($this->collCommonTVisionRmaAgentOrganismes);
    }

    /**
     * Method called to associate a CommonTVisionRmaAgentOrganisme object to this object
     * through the CommonTVisionRmaAgentOrganisme foreign key attribute.
     *
     * @param   CommonTVisionRmaAgentOrganisme $l CommonTVisionRmaAgentOrganisme
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function addCommonTVisionRmaAgentOrganisme(CommonTVisionRmaAgentOrganisme $l)
    {
        if ($this->collCommonTVisionRmaAgentOrganismes === null) {
            $this->initCommonTVisionRmaAgentOrganismes();
            $this->collCommonTVisionRmaAgentOrganismesPartial = true;
        }
        if (!in_array($l, $this->collCommonTVisionRmaAgentOrganismes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTVisionRmaAgentOrganisme($l);
        }

        return $this;
    }

    /**
     * @param	CommonTVisionRmaAgentOrganisme $commonTVisionRmaAgentOrganisme The commonTVisionRmaAgentOrganisme object to add.
     */
    protected function doAddCommonTVisionRmaAgentOrganisme($commonTVisionRmaAgentOrganisme)
    {
        $this->collCommonTVisionRmaAgentOrganismes[]= $commonTVisionRmaAgentOrganisme;
        $commonTVisionRmaAgentOrganisme->setCommonOrganisme($this);
    }

    /**
     * @param	CommonTVisionRmaAgentOrganisme $commonTVisionRmaAgentOrganisme The commonTVisionRmaAgentOrganisme object to remove.
     * @return CommonOrganisme The current object (for fluent API support)
     */
    public function removeCommonTVisionRmaAgentOrganisme($commonTVisionRmaAgentOrganisme)
    {
        if ($this->getCommonTVisionRmaAgentOrganismes()->contains($commonTVisionRmaAgentOrganisme)) {
            $this->collCommonTVisionRmaAgentOrganismes->remove($this->collCommonTVisionRmaAgentOrganismes->search($commonTVisionRmaAgentOrganisme));
            if (null === $this->commonTVisionRmaAgentOrganismesScheduledForDeletion) {
                $this->commonTVisionRmaAgentOrganismesScheduledForDeletion = clone $this->collCommonTVisionRmaAgentOrganismes;
                $this->commonTVisionRmaAgentOrganismesScheduledForDeletion->clear();
            }
            $this->commonTVisionRmaAgentOrganismesScheduledForDeletion[]= clone $commonTVisionRmaAgentOrganisme;
            $commonTVisionRmaAgentOrganisme->setCommonOrganisme(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOrganisme is new, it will return
     * an empty collection; or if this CommonOrganisme has previously
     * been saved, it will retrieve related CommonTVisionRmaAgentOrganismes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOrganisme.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTVisionRmaAgentOrganisme[] List of CommonTVisionRmaAgentOrganisme objects
     */
    public function getCommonTVisionRmaAgentOrganismesJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTVisionRmaAgentOrganismeQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonTVisionRmaAgentOrganismes($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->acronyme = null;
        $this->type_article_org = null;
        $this->denomination_org = null;
        $this->categorie_insee = null;
        $this->description_org = null;
        $this->adresse = null;
        $this->cp = null;
        $this->ville = null;
        $this->email = null;
        $this->url = null;
        $this->id_attrib_file = null;
        $this->attrib_file = null;
        $this->date_creation = null;
        $this->active = null;
        $this->id_client_anm = null;
        $this->status = null;
        $this->signataire_cao = null;
        $this->sigle = null;
        $this->adresse2 = null;
        $this->tel = null;
        $this->telecopie = null;
        $this->pays = null;
        $this->affichage_entite = null;
        $this->id_initial = null;
        $this->denomination_org_ar = null;
        $this->description_org_ar = null;
        $this->adresse_ar = null;
        $this->ville_ar = null;
        $this->adresse2_ar = null;
        $this->pays_ar = null;
        $this->denomination_org_fr = null;
        $this->description_org_fr = null;
        $this->adresse_fr = null;
        $this->ville_fr = null;
        $this->adresse2_fr = null;
        $this->pays_fr = null;
        $this->denomination_org_es = null;
        $this->description_org_es = null;
        $this->adresse_es = null;
        $this->ville_es = null;
        $this->adresse2_es = null;
        $this->pays_es = null;
        $this->denomination_org_en = null;
        $this->description_org_en = null;
        $this->adresse_en = null;
        $this->ville_en = null;
        $this->adresse2_en = null;
        $this->pays_en = null;
        $this->denomination_org_su = null;
        $this->description_org_su = null;
        $this->adresse_su = null;
        $this->ville_su = null;
        $this->adresse2_su = null;
        $this->pays_su = null;
        $this->denomination_org_du = null;
        $this->description_org_du = null;
        $this->adresse_du = null;
        $this->ville_du = null;
        $this->adresse2_du = null;
        $this->pays_du = null;
        $this->denomination_org_cz = null;
        $this->description_org_cz = null;
        $this->adresse_cz = null;
        $this->ville_cz = null;
        $this->adresse2_cz = null;
        $this->pays_cz = null;
        $this->denomination_org_it = null;
        $this->description_org_it = null;
        $this->adresse_it = null;
        $this->ville_it = null;
        $this->adresse2_it = null;
        $this->pays_it = null;
        $this->siren = null;
        $this->complement = null;
        $this->moniteur_provenance = null;
        $this->code_acces_logiciel = null;
        $this->decalage_horaire = null;
        $this->lieu_residence = null;
        $this->activation_fuseau_horaire = null;
        $this->alerte = null;
        $this->ordre = null;
        $this->url_interface_anm = null;
        $this->sous_type_organisme = null;
        $this->pf_url = null;
        $this->tag_purge = null;
        $this->id_externe = null;
        $this->id_entite = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonAdministrateurs) {
                foreach ($this->collCommonAdministrateurs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonAffiliationServices) {
                foreach ($this->collCommonAffiliationServices as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonAgents) {
                foreach ($this->collCommonAgents as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEchanges) {
                foreach ($this->collCommonEchanges as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonOrganismeServiceMetiers) {
                foreach ($this->collCommonOrganismeServiceMetiers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonParametrageEncheres) {
                foreach ($this->collCommonParametrageEncheres as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonRPAs) {
                foreach ($this->collCommonRPAs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTelechargements) {
                foreach ($this->collCommonTelechargements as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTierss) {
                foreach ($this->collCommonTierss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->singleCommonConfigurationOrganisme) {
                $this->singleCommonConfigurationOrganisme->clearAllReferences($deep);
            }
            if ($this->collCommonConsultations) {
                foreach ($this->collCommonConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonDestinataireMiseDispositions) {
                foreach ($this->collCommonDestinataireMiseDispositions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEchangeDocApplicationClientOrganismes) {
                foreach ($this->collCommonEchangeDocApplicationClientOrganismes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonInvitePermanentTransverses) {
                foreach ($this->collCommonInvitePermanentTransverses as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonPlateformeVirtuelleOrganismes) {
                foreach ($this->collCommonPlateformeVirtuelleOrganismes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOCommissions) {
                foreach ($this->collCommonTCAOCommissions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOCommissionAgents) {
                foreach ($this->collCommonTCAOCommissionAgents as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOCommissionIntervenantExternes) {
                foreach ($this->collCommonTCAOCommissionIntervenantExternes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOIntervenantExternes) {
                foreach ($this->collCommonTCAOIntervenantExternes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOOrdreDuJours) {
                foreach ($this->collCommonTCAOOrdreDuJours as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOOrdreDuJourIntervenants) {
                foreach ($this->collCommonTCAOOrdreDuJourIntervenants as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOSeances) {
                foreach ($this->collCommonTCAOSeances as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOSeanceAgents) {
                foreach ($this->collCommonTCAOSeanceAgents as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOSeanceIntervenantExternes) {
                foreach ($this->collCommonTCAOSeanceIntervenantExternes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCandidatures) {
                foreach ($this->collCommonTCandidatures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTDumeContextes) {
                foreach ($this->collCommonTDumeContextes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTListeLotsCandidatures) {
                foreach ($this->collCommonTListeLotsCandidatures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTVisionRmaAgentOrganismes) {
                foreach ($this->collCommonTVisionRmaAgentOrganismes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonCategorieINSEE instanceof Persistent) {
              $this->aCommonCategorieINSEE->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonAdministrateurs instanceof PropelCollection) {
            $this->collCommonAdministrateurs->clearIterator();
        }
        $this->collCommonAdministrateurs = null;
        if ($this->collCommonAffiliationServices instanceof PropelCollection) {
            $this->collCommonAffiliationServices->clearIterator();
        }
        $this->collCommonAffiliationServices = null;
        if ($this->collCommonAgents instanceof PropelCollection) {
            $this->collCommonAgents->clearIterator();
        }
        $this->collCommonAgents = null;
        if ($this->collCommonEchanges instanceof PropelCollection) {
            $this->collCommonEchanges->clearIterator();
        }
        $this->collCommonEchanges = null;
        if ($this->collCommonOrganismeServiceMetiers instanceof PropelCollection) {
            $this->collCommonOrganismeServiceMetiers->clearIterator();
        }
        $this->collCommonOrganismeServiceMetiers = null;
        if ($this->collCommonParametrageEncheres instanceof PropelCollection) {
            $this->collCommonParametrageEncheres->clearIterator();
        }
        $this->collCommonParametrageEncheres = null;
        if ($this->collCommonRPAs instanceof PropelCollection) {
            $this->collCommonRPAs->clearIterator();
        }
        $this->collCommonRPAs = null;
        if ($this->collCommonTelechargements instanceof PropelCollection) {
            $this->collCommonTelechargements->clearIterator();
        }
        $this->collCommonTelechargements = null;
        if ($this->collCommonTierss instanceof PropelCollection) {
            $this->collCommonTierss->clearIterator();
        }
        $this->collCommonTierss = null;
        if ($this->singleCommonConfigurationOrganisme instanceof PropelCollection) {
            $this->singleCommonConfigurationOrganisme->clearIterator();
        }
        $this->singleCommonConfigurationOrganisme = null;
        if ($this->collCommonConsultations instanceof PropelCollection) {
            $this->collCommonConsultations->clearIterator();
        }
        $this->collCommonConsultations = null;
        if ($this->collCommonDestinataireMiseDispositions instanceof PropelCollection) {
            $this->collCommonDestinataireMiseDispositions->clearIterator();
        }
        $this->collCommonDestinataireMiseDispositions = null;
        if ($this->collCommonEchangeDocApplicationClientOrganismes instanceof PropelCollection) {
            $this->collCommonEchangeDocApplicationClientOrganismes->clearIterator();
        }
        $this->collCommonEchangeDocApplicationClientOrganismes = null;
        if ($this->collCommonInvitePermanentTransverses instanceof PropelCollection) {
            $this->collCommonInvitePermanentTransverses->clearIterator();
        }
        $this->collCommonInvitePermanentTransverses = null;
        if ($this->collCommonPlateformeVirtuelleOrganismes instanceof PropelCollection) {
            $this->collCommonPlateformeVirtuelleOrganismes->clearIterator();
        }
        $this->collCommonPlateformeVirtuelleOrganismes = null;
        if ($this->collCommonTCAOCommissions instanceof PropelCollection) {
            $this->collCommonTCAOCommissions->clearIterator();
        }
        $this->collCommonTCAOCommissions = null;
        if ($this->collCommonTCAOCommissionAgents instanceof PropelCollection) {
            $this->collCommonTCAOCommissionAgents->clearIterator();
        }
        $this->collCommonTCAOCommissionAgents = null;
        if ($this->collCommonTCAOCommissionIntervenantExternes instanceof PropelCollection) {
            $this->collCommonTCAOCommissionIntervenantExternes->clearIterator();
        }
        $this->collCommonTCAOCommissionIntervenantExternes = null;
        if ($this->collCommonTCAOIntervenantExternes instanceof PropelCollection) {
            $this->collCommonTCAOIntervenantExternes->clearIterator();
        }
        $this->collCommonTCAOIntervenantExternes = null;
        if ($this->collCommonTCAOOrdreDuJours instanceof PropelCollection) {
            $this->collCommonTCAOOrdreDuJours->clearIterator();
        }
        $this->collCommonTCAOOrdreDuJours = null;
        if ($this->collCommonTCAOOrdreDuJourIntervenants instanceof PropelCollection) {
            $this->collCommonTCAOOrdreDuJourIntervenants->clearIterator();
        }
        $this->collCommonTCAOOrdreDuJourIntervenants = null;
        if ($this->collCommonTCAOSeances instanceof PropelCollection) {
            $this->collCommonTCAOSeances->clearIterator();
        }
        $this->collCommonTCAOSeances = null;
        if ($this->collCommonTCAOSeanceAgents instanceof PropelCollection) {
            $this->collCommonTCAOSeanceAgents->clearIterator();
        }
        $this->collCommonTCAOSeanceAgents = null;
        if ($this->collCommonTCAOSeanceIntervenantExternes instanceof PropelCollection) {
            $this->collCommonTCAOSeanceIntervenantExternes->clearIterator();
        }
        $this->collCommonTCAOSeanceIntervenantExternes = null;
        if ($this->collCommonTCandidatures instanceof PropelCollection) {
            $this->collCommonTCandidatures->clearIterator();
        }
        $this->collCommonTCandidatures = null;
        if ($this->collCommonTDumeContextes instanceof PropelCollection) {
            $this->collCommonTDumeContextes->clearIterator();
        }
        $this->collCommonTDumeContextes = null;
        if ($this->collCommonTListeLotsCandidatures instanceof PropelCollection) {
            $this->collCommonTListeLotsCandidatures->clearIterator();
        }
        $this->collCommonTListeLotsCandidatures = null;
        if ($this->collCommonTVisionRmaAgentOrganismes instanceof PropelCollection) {
            $this->collCommonTVisionRmaAgentOrganismes->clearIterator();
        }
        $this->collCommonTVisionRmaAgentOrganismes = null;
        $this->aCommonCategorieINSEE = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonOrganismePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
