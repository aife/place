<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTSupportPublicationPeer;
use Application\Propel\Mpe\Map\CommonTSupportAnnonceConsultationTableMap;

/**
 * Base static class for performing query and update operations on the 't_support_annonce_consultation' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTSupportAnnonceConsultationPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_support_annonce_consultation';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTSupportAnnonceConsultation';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTSupportAnnonceConsultationTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 17;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 17;

    /** the column name for the id field */
    const ID = 't_support_annonce_consultation.id';

    /** the column name for the id_support field */
    const ID_SUPPORT = 't_support_annonce_consultation.id_support';

    /** the column name for the id_annonce_cons field */
    const ID_ANNONCE_CONS = 't_support_annonce_consultation.id_annonce_cons';

    /** the column name for the prenom_nom_agent_createur field */
    const PRENOM_NOM_AGENT_CREATEUR = 't_support_annonce_consultation.prenom_nom_agent_createur';

    /** the column name for the id_agent field */
    const ID_AGENT = 't_support_annonce_consultation.id_agent';

    /** the column name for the date_creation field */
    const DATE_CREATION = 't_support_annonce_consultation.date_creation';

    /** the column name for the statut field */
    const STATUT = 't_support_annonce_consultation.statut';

    /** the column name for the date_statut field */
    const DATE_STATUT = 't_support_annonce_consultation.date_statut';

    /** the column name for the numero_avis field */
    const NUMERO_AVIS = 't_support_annonce_consultation.numero_avis';

    /** the column name for the message_statut field */
    const MESSAGE_STATUT = 't_support_annonce_consultation.message_statut';

    /** the column name for the lien_publication field */
    const LIEN_PUBLICATION = 't_support_annonce_consultation.lien_publication';

    /** the column name for the date_envoi_support field */
    const DATE_ENVOI_SUPPORT = 't_support_annonce_consultation.date_envoi_support';

    /** the column name for the date_publication_support field */
    const DATE_PUBLICATION_SUPPORT = 't_support_annonce_consultation.date_publication_support';

    /** the column name for the numero_avis_parent field */
    const NUMERO_AVIS_PARENT = 't_support_annonce_consultation.numero_avis_parent';

    /** the column name for the id_offre field */
    const ID_OFFRE = 't_support_annonce_consultation.id_offre';

    /** the column name for the message_acheteur field */
    const MESSAGE_ACHETEUR = 't_support_annonce_consultation.message_acheteur';

    /** the column name for the departements_parution_annonce field */
    const DEPARTEMENTS_PARUTION_ANNONCE = 't_support_annonce_consultation.departements_parution_annonce';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTSupportAnnonceConsultation objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTSupportAnnonceConsultation[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTSupportAnnonceConsultationPeer::$fieldNames[CommonTSupportAnnonceConsultationPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'IdSupport', 'IdAnnonceCons', 'PrenomNomAgentCreateur', 'IdAgent', 'DateCreation', 'Statut', 'DateStatut', 'NumeroAvis', 'MessageStatut', 'LienPublication', 'DateEnvoiSupport', 'DatePublicationSupport', 'NumeroAvisParent', 'IdOffre', 'MessageAcheteur', 'DepartementsParutionAnnonce', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'idSupport', 'idAnnonceCons', 'prenomNomAgentCreateur', 'idAgent', 'dateCreation', 'statut', 'dateStatut', 'numeroAvis', 'messageStatut', 'lienPublication', 'dateEnvoiSupport', 'datePublicationSupport', 'numeroAvisParent', 'idOffre', 'messageAcheteur', 'departementsParutionAnnonce', ),
        BasePeer::TYPE_COLNAME => array (CommonTSupportAnnonceConsultationPeer::ID, CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, CommonTSupportAnnonceConsultationPeer::PRENOM_NOM_AGENT_CREATEUR, CommonTSupportAnnonceConsultationPeer::ID_AGENT, CommonTSupportAnnonceConsultationPeer::DATE_CREATION, CommonTSupportAnnonceConsultationPeer::STATUT, CommonTSupportAnnonceConsultationPeer::DATE_STATUT, CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS, CommonTSupportAnnonceConsultationPeer::MESSAGE_STATUT, CommonTSupportAnnonceConsultationPeer::LIEN_PUBLICATION, CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT, CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT, CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS_PARENT, CommonTSupportAnnonceConsultationPeer::ID_OFFRE, CommonTSupportAnnonceConsultationPeer::MESSAGE_ACHETEUR, CommonTSupportAnnonceConsultationPeer::DEPARTEMENTS_PARUTION_ANNONCE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ID_SUPPORT', 'ID_ANNONCE_CONS', 'PRENOM_NOM_AGENT_CREATEUR', 'ID_AGENT', 'DATE_CREATION', 'STATUT', 'DATE_STATUT', 'NUMERO_AVIS', 'MESSAGE_STATUT', 'LIEN_PUBLICATION', 'DATE_ENVOI_SUPPORT', 'DATE_PUBLICATION_SUPPORT', 'NUMERO_AVIS_PARENT', 'ID_OFFRE', 'MESSAGE_ACHETEUR', 'DEPARTEMENTS_PARUTION_ANNONCE', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'id_support', 'id_annonce_cons', 'prenom_nom_agent_createur', 'id_agent', 'date_creation', 'statut', 'date_statut', 'numero_avis', 'message_statut', 'lien_publication', 'date_envoi_support', 'date_publication_support', 'numero_avis_parent', 'id_offre', 'message_acheteur', 'departements_parution_annonce', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTSupportAnnonceConsultationPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdSupport' => 1, 'IdAnnonceCons' => 2, 'PrenomNomAgentCreateur' => 3, 'IdAgent' => 4, 'DateCreation' => 5, 'Statut' => 6, 'DateStatut' => 7, 'NumeroAvis' => 8, 'MessageStatut' => 9, 'LienPublication' => 10, 'DateEnvoiSupport' => 11, 'DatePublicationSupport' => 12, 'NumeroAvisParent' => 13, 'IdOffre' => 14, 'MessageAcheteur' => 15, 'DepartementsParutionAnnonce' => 16, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'idSupport' => 1, 'idAnnonceCons' => 2, 'prenomNomAgentCreateur' => 3, 'idAgent' => 4, 'dateCreation' => 5, 'statut' => 6, 'dateStatut' => 7, 'numeroAvis' => 8, 'messageStatut' => 9, 'lienPublication' => 10, 'dateEnvoiSupport' => 11, 'datePublicationSupport' => 12, 'numeroAvisParent' => 13, 'idOffre' => 14, 'messageAcheteur' => 15, 'departementsParutionAnnonce' => 16, ),
        BasePeer::TYPE_COLNAME => array (CommonTSupportAnnonceConsultationPeer::ID => 0, CommonTSupportAnnonceConsultationPeer::ID_SUPPORT => 1, CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS => 2, CommonTSupportAnnonceConsultationPeer::PRENOM_NOM_AGENT_CREATEUR => 3, CommonTSupportAnnonceConsultationPeer::ID_AGENT => 4, CommonTSupportAnnonceConsultationPeer::DATE_CREATION => 5, CommonTSupportAnnonceConsultationPeer::STATUT => 6, CommonTSupportAnnonceConsultationPeer::DATE_STATUT => 7, CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS => 8, CommonTSupportAnnonceConsultationPeer::MESSAGE_STATUT => 9, CommonTSupportAnnonceConsultationPeer::LIEN_PUBLICATION => 10, CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT => 11, CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT => 12, CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS_PARENT => 13, CommonTSupportAnnonceConsultationPeer::ID_OFFRE => 14, CommonTSupportAnnonceConsultationPeer::MESSAGE_ACHETEUR => 15, CommonTSupportAnnonceConsultationPeer::DEPARTEMENTS_PARUTION_ANNONCE => 16, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ID_SUPPORT' => 1, 'ID_ANNONCE_CONS' => 2, 'PRENOM_NOM_AGENT_CREATEUR' => 3, 'ID_AGENT' => 4, 'DATE_CREATION' => 5, 'STATUT' => 6, 'DATE_STATUT' => 7, 'NUMERO_AVIS' => 8, 'MESSAGE_STATUT' => 9, 'LIEN_PUBLICATION' => 10, 'DATE_ENVOI_SUPPORT' => 11, 'DATE_PUBLICATION_SUPPORT' => 12, 'NUMERO_AVIS_PARENT' => 13, 'ID_OFFRE' => 14, 'MESSAGE_ACHETEUR' => 15, 'DEPARTEMENTS_PARUTION_ANNONCE' => 16, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_support' => 1, 'id_annonce_cons' => 2, 'prenom_nom_agent_createur' => 3, 'id_agent' => 4, 'date_creation' => 5, 'statut' => 6, 'date_statut' => 7, 'numero_avis' => 8, 'message_statut' => 9, 'lien_publication' => 10, 'date_envoi_support' => 11, 'date_publication_support' => 12, 'numero_avis_parent' => 13, 'id_offre' => 14, 'message_acheteur' => 15, 'departements_parution_annonce' => 16, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTSupportAnnonceConsultationPeer::getFieldNames($toType);
        $key = isset(CommonTSupportAnnonceConsultationPeer::$fieldKeys[$fromType][$name]) ? CommonTSupportAnnonceConsultationPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTSupportAnnonceConsultationPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTSupportAnnonceConsultationPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTSupportAnnonceConsultationPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTSupportAnnonceConsultationPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTSupportAnnonceConsultationPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::ID);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::PRENOM_NOM_AGENT_CREATEUR);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::ID_AGENT);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::STATUT);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::DATE_STATUT);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::MESSAGE_STATUT);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::LIEN_PUBLICATION);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS_PARENT);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::ID_OFFRE);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::MESSAGE_ACHETEUR);
            $criteria->addSelectColumn(CommonTSupportAnnonceConsultationPeer::DEPARTEMENTS_PARUTION_ANNONCE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.id_support');
            $criteria->addSelectColumn($alias . '.id_annonce_cons');
            $criteria->addSelectColumn($alias . '.prenom_nom_agent_createur');
            $criteria->addSelectColumn($alias . '.id_agent');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.statut');
            $criteria->addSelectColumn($alias . '.date_statut');
            $criteria->addSelectColumn($alias . '.numero_avis');
            $criteria->addSelectColumn($alias . '.message_statut');
            $criteria->addSelectColumn($alias . '.lien_publication');
            $criteria->addSelectColumn($alias . '.date_envoi_support');
            $criteria->addSelectColumn($alias . '.date_publication_support');
            $criteria->addSelectColumn($alias . '.numero_avis_parent');
            $criteria->addSelectColumn($alias . '.id_offre');
            $criteria->addSelectColumn($alias . '.message_acheteur');
            $criteria->addSelectColumn($alias . '.departements_parution_annonce');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTSupportAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTSupportAnnonceConsultation
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTSupportAnnonceConsultationPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTSupportAnnonceConsultationPeer::populateObjects(CommonTSupportAnnonceConsultationPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTSupportAnnonceConsultation $obj A CommonTSupportAnnonceConsultation object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTSupportAnnonceConsultationPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTSupportAnnonceConsultation object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTSupportAnnonceConsultation) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTSupportAnnonceConsultation object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTSupportAnnonceConsultationPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTSupportAnnonceConsultation Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTSupportAnnonceConsultationPeer::$instances[$key])) {
                return CommonTSupportAnnonceConsultationPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTSupportAnnonceConsultationPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTSupportAnnonceConsultationPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_support_annonce_consultation
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTSupportAnnonceConsultationPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTSupportAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTSupportAnnonceConsultationPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTSupportAnnonceConsultationPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTSupportAnnonceConsultation object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTSupportAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTSupportAnnonceConsultationPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTSupportAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTSupportAnnonceConsultationPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTSupportAnnonceConsultationPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTAnnonceConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTAnnonceConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTSupportAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, CommonTAnnonceConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTSupportPublication table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTSupportPublication(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTSupportAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, CommonTSupportPublicationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTSupportAnnonceConsultation objects pre-filled with their CommonTAnnonceConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTSupportAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTAnnonceConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonTSupportAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonTAnnonceConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, CommonTAnnonceConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTSupportAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTSupportAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTSupportAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTSupportAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTAnnonceConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTAnnonceConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTAnnonceConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTSupportAnnonceConsultation) to $obj2 (CommonTAnnonceConsultation)
                $obj2->addCommonTSupportAnnonceConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTSupportAnnonceConsultation objects pre-filled with their CommonTSupportPublication objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTSupportAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTSupportPublication(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonTSupportAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonTSupportPublicationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, CommonTSupportPublicationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTSupportAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTSupportAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTSupportAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTSupportAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTSupportPublicationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTSupportPublicationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTSupportPublicationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTSupportPublicationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTSupportAnnonceConsultation) to $obj2 (CommonTSupportPublication)
                $obj2->addCommonTSupportAnnonceConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTSupportAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, CommonTAnnonceConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, CommonTSupportPublicationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTSupportAnnonceConsultation objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTSupportAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonTSupportAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTSupportPublicationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTSupportPublicationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, CommonTAnnonceConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, CommonTSupportPublicationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTSupportAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTSupportAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTSupportAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTSupportAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonTAnnonceConsultation rows

            $key2 = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonTAnnonceConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTAnnonceConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTAnnonceConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTSupportAnnonceConsultation) to the collection in $obj2 (CommonTAnnonceConsultation)
                $obj2->addCommonTSupportAnnonceConsultation($obj1);
            } // if joined row not null

            // Add objects for joined CommonTSupportPublication rows

            $key3 = CommonTSupportPublicationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonTSupportPublicationPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonTSupportPublicationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTSupportPublicationPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonTSupportAnnonceConsultation) to the collection in $obj3 (CommonTSupportPublication)
                $obj3->addCommonTSupportAnnonceConsultation($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTAnnonceConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTAnnonceConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTSupportAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, CommonTSupportPublicationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTSupportPublication table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTSupportPublication(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTSupportAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, CommonTAnnonceConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTSupportAnnonceConsultation objects pre-filled with all related objects except CommonTAnnonceConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTSupportAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTAnnonceConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonTSupportAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTSupportPublicationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTSupportPublicationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, CommonTSupportPublicationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTSupportAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTSupportAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTSupportAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTSupportAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTSupportPublication rows

                $key2 = CommonTSupportPublicationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTSupportPublicationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTSupportPublicationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTSupportPublicationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTSupportAnnonceConsultation) to the collection in $obj2 (CommonTSupportPublication)
                $obj2->addCommonTSupportAnnonceConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTSupportAnnonceConsultation objects pre-filled with all related objects except CommonTSupportPublication.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTSupportAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTSupportPublication(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTSupportAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonTSupportAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, CommonTAnnonceConsultationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTSupportAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTSupportAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTSupportAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTSupportAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTAnnonceConsultation rows

                $key2 = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTAnnonceConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTAnnonceConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTAnnonceConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTSupportAnnonceConsultation) to the collection in $obj2 (CommonTAnnonceConsultation)
                $obj2->addCommonTSupportAnnonceConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME)->getTable(CommonTSupportAnnonceConsultationPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTSupportAnnonceConsultationPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTSupportAnnonceConsultationTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTSupportAnnonceConsultationPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTSupportAnnonceConsultation or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTSupportAnnonceConsultation object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTSupportAnnonceConsultation object
        }

        if ($criteria->containsKey(CommonTSupportAnnonceConsultationPeer::ID) && $criteria->keyContainsValue(CommonTSupportAnnonceConsultationPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTSupportAnnonceConsultationPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTSupportAnnonceConsultation or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTSupportAnnonceConsultation object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTSupportAnnonceConsultationPeer::ID);
            $value = $criteria->remove(CommonTSupportAnnonceConsultationPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTSupportAnnonceConsultationPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTSupportAnnonceConsultationPeer::TABLE_NAME);
            }

        } else { // $values is CommonTSupportAnnonceConsultation object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_support_annonce_consultation table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTSupportAnnonceConsultationPeer::TABLE_NAME, $con, CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTSupportAnnonceConsultationPeer::clearInstancePool();
            CommonTSupportAnnonceConsultationPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTSupportAnnonceConsultation or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTSupportAnnonceConsultation object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTSupportAnnonceConsultationPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTSupportAnnonceConsultation) { // it's a model object
            // invalidate the cache for this single object
            CommonTSupportAnnonceConsultationPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
            $criteria->add(CommonTSupportAnnonceConsultationPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTSupportAnnonceConsultationPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTSupportAnnonceConsultationPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTSupportAnnonceConsultation object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTSupportAnnonceConsultation $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTSupportAnnonceConsultationPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, CommonTSupportAnnonceConsultationPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTSupportAnnonceConsultation
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTSupportAnnonceConsultationPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
        $criteria->add(CommonTSupportAnnonceConsultationPeer::ID, $pk);

        $v = CommonTSupportAnnonceConsultationPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTSupportAnnonceConsultation[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
            $criteria->add(CommonTSupportAnnonceConsultationPeer::ID, $pks, Criteria::IN);
            $objs = CommonTSupportAnnonceConsultationPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTSupportAnnonceConsultationPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTSupportAnnonceConsultationPeer::buildTableMap();

