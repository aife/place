<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonMailType;
use Application\Propel\Mpe\CommonMailTypeGroup;
use Application\Propel\Mpe\CommonMailTypeGroupPeer;
use Application\Propel\Mpe\CommonMailTypeGroupQuery;

/**
 * Base class that represents a query for the 'mail_type_group' table.
 *
 *
 *
 * @method CommonMailTypeGroupQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonMailTypeGroupQuery orderByLabel($order = Criteria::ASC) Order by the label column
 *
 * @method CommonMailTypeGroupQuery groupById() Group by the id column
 * @method CommonMailTypeGroupQuery groupByLabel() Group by the label column
 *
 * @method CommonMailTypeGroupQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonMailTypeGroupQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonMailTypeGroupQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonMailTypeGroupQuery leftJoinCommonMailType($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonMailType relation
 * @method CommonMailTypeGroupQuery rightJoinCommonMailType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonMailType relation
 * @method CommonMailTypeGroupQuery innerJoinCommonMailType($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonMailType relation
 *
 * @method CommonMailTypeGroup findOne(PropelPDO $con = null) Return the first CommonMailTypeGroup matching the query
 * @method CommonMailTypeGroup findOneOrCreate(PropelPDO $con = null) Return the first CommonMailTypeGroup matching the query, or a new CommonMailTypeGroup object populated from the query conditions when no match is found
 *
 * @method CommonMailTypeGroup findOneByLabel(string $label) Return the first CommonMailTypeGroup filtered by the label column
 *
 * @method array findById(int $id) Return CommonMailTypeGroup objects filtered by the id column
 * @method array findByLabel(string $label) Return CommonMailTypeGroup objects filtered by the label column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonMailTypeGroupQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonMailTypeGroupQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonMailTypeGroup', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonMailTypeGroupQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonMailTypeGroupQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonMailTypeGroupQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonMailTypeGroupQuery) {
            return $criteria;
        }
        $query = new CommonMailTypeGroupQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonMailTypeGroup|CommonMailTypeGroup[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonMailTypeGroupPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonMailTypeGroupPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonMailTypeGroup A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonMailTypeGroup A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `label` FROM `mail_type_group` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonMailTypeGroup();
            $obj->hydrate($row);
            CommonMailTypeGroupPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonMailTypeGroup|CommonMailTypeGroup[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonMailTypeGroup[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonMailTypeGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonMailTypeGroupPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonMailTypeGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonMailTypeGroupPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTypeGroupQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonMailTypeGroupPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonMailTypeGroupPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonMailTypeGroupPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE label = 'fooValue'
     * $query->filterByLabel('%fooValue%'); // WHERE label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTypeGroupQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $label)) {
                $label = str_replace('*', '%', $label);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonMailTypeGroupPeer::LABEL, $label, $comparison);
    }

    /**
     * Filter the query by a related CommonMailType object
     *
     * @param   CommonMailType|PropelObjectCollection $commonMailType  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonMailTypeGroupQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonMailType($commonMailType, $comparison = null)
    {
        if ($commonMailType instanceof CommonMailType) {
            return $this
                ->addUsingAlias(CommonMailTypeGroupPeer::ID, $commonMailType->getMailTypeGroupId(), $comparison);
        } elseif ($commonMailType instanceof PropelObjectCollection) {
            return $this
                ->useCommonMailTypeQuery()
                ->filterByPrimaryKeys($commonMailType->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonMailType() only accepts arguments of type CommonMailType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonMailType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonMailTypeGroupQuery The current query, for fluid interface
     */
    public function joinCommonMailType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonMailType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonMailType');
        }

        return $this;
    }

    /**
     * Use the CommonMailType relation CommonMailType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonMailTypeQuery A secondary query class using the current class as primary query
     */
    public function useCommonMailTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonMailType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonMailType', '\Application\Propel\Mpe\CommonMailTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonMailTypeGroup $commonMailTypeGroup Object to remove from the list of results
     *
     * @return CommonMailTypeGroupQuery The current query, for fluid interface
     */
    public function prune($commonMailTypeGroup = null)
    {
        if ($commonMailTypeGroup) {
            $this->addUsingAlias(CommonMailTypeGroupPeer::ID, $commonMailTypeGroup->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
