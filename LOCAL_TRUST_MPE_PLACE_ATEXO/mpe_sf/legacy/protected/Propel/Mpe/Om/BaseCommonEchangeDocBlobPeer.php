<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonBlobOrganismeFilePeer;
use Application\Propel\Mpe\CommonEchangeDocBlob;
use Application\Propel\Mpe\CommonEchangeDocBlobPeer;
use Application\Propel\Mpe\CommonEchangeDocPeer;
use Application\Propel\Mpe\CommonEchangeDocTypePieceStandardPeer;
use Application\Propel\Mpe\Map\CommonEchangeDocBlobTableMap;

/**
 * Base static class for performing query and update operations on the 'echange_doc_blob' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocBlobPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'echange_doc_blob';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonEchangeDocBlob';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonEchangeDocBlobTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 10;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 10;

    /** the column name for the id field */
    const ID = 'echange_doc_blob.id';

    /** the column name for the echange_doc_id field */
    const ECHANGE_DOC_ID = 'echange_doc_blob.echange_doc_id';

    /** the column name for the blob_organisme_id field */
    const BLOB_ORGANISME_ID = 'echange_doc_blob.blob_organisme_id';

    /** the column name for the chemin field */
    const CHEMIN = 'echange_doc_blob.chemin';

    /** the column name for the poids field */
    const POIDS = 'echange_doc_blob.poids';

    /** the column name for the checksum field */
    const CHECKSUM = 'echange_doc_blob.checksum';

    /** the column name for the echange_type_piece_actes_id field */
    const ECHANGE_TYPE_PIECE_ACTES_ID = 'echange_doc_blob.echange_type_piece_actes_id';

    /** the column name for the echange_type_piece_standard_id field */
    const ECHANGE_TYPE_PIECE_STANDARD_ID = 'echange_doc_blob.echange_type_piece_standard_id';

    /** the column name for the categorie_piece field */
    const CATEGORIE_PIECE = 'echange_doc_blob.categorie_piece';

    /** the column name for the doc_blob_principal_id field */
    const DOC_BLOB_PRINCIPAL_ID = 'echange_doc_blob.doc_blob_principal_id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonEchangeDocBlob objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonEchangeDocBlob[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonEchangeDocBlobPeer::$fieldNames[CommonEchangeDocBlobPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'EchangeDocId', 'BlobOrganismeId', 'Chemin', 'Poids', 'Checksum', 'EchangeTypePieceActesId', 'EchangeTypePieceStandardId', 'CategoriePiece', 'DocBlobPrincipalId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'echangeDocId', 'blobOrganismeId', 'chemin', 'poids', 'checksum', 'echangeTypePieceActesId', 'echangeTypePieceStandardId', 'categoriePiece', 'docBlobPrincipalId', ),
        BasePeer::TYPE_COLNAME => array (CommonEchangeDocBlobPeer::ID, CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonEchangeDocBlobPeer::CHEMIN, CommonEchangeDocBlobPeer::POIDS, CommonEchangeDocBlobPeer::CHECKSUM, CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID, CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocBlobPeer::CATEGORIE_PIECE, CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ECHANGE_DOC_ID', 'BLOB_ORGANISME_ID', 'CHEMIN', 'POIDS', 'CHECKSUM', 'ECHANGE_TYPE_PIECE_ACTES_ID', 'ECHANGE_TYPE_PIECE_STANDARD_ID', 'CATEGORIE_PIECE', 'DOC_BLOB_PRINCIPAL_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'echange_doc_id', 'blob_organisme_id', 'chemin', 'poids', 'checksum', 'echange_type_piece_actes_id', 'echange_type_piece_standard_id', 'categorie_piece', 'doc_blob_principal_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonEchangeDocBlobPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'EchangeDocId' => 1, 'BlobOrganismeId' => 2, 'Chemin' => 3, 'Poids' => 4, 'Checksum' => 5, 'EchangeTypePieceActesId' => 6, 'EchangeTypePieceStandardId' => 7, 'CategoriePiece' => 8, 'DocBlobPrincipalId' => 9, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'echangeDocId' => 1, 'blobOrganismeId' => 2, 'chemin' => 3, 'poids' => 4, 'checksum' => 5, 'echangeTypePieceActesId' => 6, 'echangeTypePieceStandardId' => 7, 'categoriePiece' => 8, 'docBlobPrincipalId' => 9, ),
        BasePeer::TYPE_COLNAME => array (CommonEchangeDocBlobPeer::ID => 0, CommonEchangeDocBlobPeer::ECHANGE_DOC_ID => 1, CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID => 2, CommonEchangeDocBlobPeer::CHEMIN => 3, CommonEchangeDocBlobPeer::POIDS => 4, CommonEchangeDocBlobPeer::CHECKSUM => 5, CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID => 6, CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID => 7, CommonEchangeDocBlobPeer::CATEGORIE_PIECE => 8, CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID => 9, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ECHANGE_DOC_ID' => 1, 'BLOB_ORGANISME_ID' => 2, 'CHEMIN' => 3, 'POIDS' => 4, 'CHECKSUM' => 5, 'ECHANGE_TYPE_PIECE_ACTES_ID' => 6, 'ECHANGE_TYPE_PIECE_STANDARD_ID' => 7, 'CATEGORIE_PIECE' => 8, 'DOC_BLOB_PRINCIPAL_ID' => 9, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'echange_doc_id' => 1, 'blob_organisme_id' => 2, 'chemin' => 3, 'poids' => 4, 'checksum' => 5, 'echange_type_piece_actes_id' => 6, 'echange_type_piece_standard_id' => 7, 'categorie_piece' => 8, 'doc_blob_principal_id' => 9, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonEchangeDocBlobPeer::getFieldNames($toType);
        $key = isset(CommonEchangeDocBlobPeer::$fieldKeys[$fromType][$name]) ? CommonEchangeDocBlobPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonEchangeDocBlobPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonEchangeDocBlobPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonEchangeDocBlobPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonEchangeDocBlobPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonEchangeDocBlobPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::ID);
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID);
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID);
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::CHEMIN);
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::POIDS);
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::CHECKSUM);
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID);
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID);
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::CATEGORIE_PIECE);
            $criteria->addSelectColumn(CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.echange_doc_id');
            $criteria->addSelectColumn($alias . '.blob_organisme_id');
            $criteria->addSelectColumn($alias . '.chemin');
            $criteria->addSelectColumn($alias . '.poids');
            $criteria->addSelectColumn($alias . '.checksum');
            $criteria->addSelectColumn($alias . '.echange_type_piece_actes_id');
            $criteria->addSelectColumn($alias . '.echange_type_piece_standard_id');
            $criteria->addSelectColumn($alias . '.categorie_piece');
            $criteria->addSelectColumn($alias . '.doc_blob_principal_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonEchangeDocBlob
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonEchangeDocBlobPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonEchangeDocBlobPeer::populateObjects(CommonEchangeDocBlobPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonEchangeDocBlob $obj A CommonEchangeDocBlob object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonEchangeDocBlobPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonEchangeDocBlob object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonEchangeDocBlob) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonEchangeDocBlob object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonEchangeDocBlobPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonEchangeDocBlob Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonEchangeDocBlobPeer::$instances[$key])) {
                return CommonEchangeDocBlobPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonEchangeDocBlobPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonEchangeDocBlobPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to echange_doc_blob
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonEchangeDocBlobPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonEchangeDocBlobPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonEchangeDocBlobPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonEchangeDocBlob object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonEchangeDocBlobPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonEchangeDocBlobPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonEchangeDocBlobPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonEchangeDoc table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonEchangeDoc(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonBlobOrganismeFile table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonBlobOrganismeFile(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonEchangeDocTypePieceStandard table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonEchangeDocTypePieceStandard(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonEchangeDocBlob objects pre-filled with their CommonEchangeDoc objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocBlob objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonEchangeDoc(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);
        }

        CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        $startcol = CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS;
        CommonEchangeDocPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocBlobPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangeDocBlobPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocBlobPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonEchangeDocPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonEchangeDocPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonEchangeDocPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to $obj2 (CommonEchangeDoc)
                $obj2->addCommonEchangeDocBlob($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDocBlob objects pre-filled with their CommonBlobOrganismeFile objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocBlob objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonBlobOrganismeFile(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);
        }

        CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        $startcol = CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS;
        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocBlobPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangeDocBlobPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocBlobPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to $obj2 (CommonBlobOrganismeFile)
                $obj2->addCommonEchangeDocBlob($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDocBlob objects pre-filled with their CommonEchangeDocTypePieceStandard objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocBlob objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonEchangeDocTypePieceStandard(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);
        }

        CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        $startcol = CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS;
        CommonEchangeDocTypePieceStandardPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocBlobPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangeDocBlobPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocBlobPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonEchangeDocTypePieceStandardPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonEchangeDocTypePieceStandardPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonEchangeDocTypePieceStandardPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonEchangeDocTypePieceStandardPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to $obj2 (CommonEchangeDocTypePieceStandard)
                $obj2->addCommonEchangeDocBlob($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonEchangeDocBlob objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocBlob objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);
        }

        CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocTypePieceStandardPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonEchangeDocTypePieceStandardPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocBlobPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocBlobPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocBlobPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonEchangeDoc rows

            $key2 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonEchangeDocPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonEchangeDocPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonEchangeDocPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj2 (CommonEchangeDoc)
                $obj2->addCommonEchangeDocBlob($obj1);
            } // if joined row not null

            // Add objects for joined CommonBlobOrganismeFile rows

            $key3 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj3 (CommonBlobOrganismeFile)
                $obj3->addCommonEchangeDocBlob($obj1);
            } // if joined row not null

            // Add objects for joined CommonEchangeDocTypePieceStandard rows

            $key4 = CommonEchangeDocTypePieceStandardPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonEchangeDocTypePieceStandardPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonEchangeDocTypePieceStandardPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonEchangeDocTypePieceStandardPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj4 (CommonEchangeDocTypePieceStandard)
                $obj4->addCommonEchangeDocBlob($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonEchangeDoc table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonEchangeDoc(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonBlobOrganismeFile table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonBlobOrganismeFile(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonEchangeDocBlobRelatedByDocBlobPrincipalId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonEchangeDocBlobRelatedByDocBlobPrincipalId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonEchangeDocTypePieceStandard table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonEchangeDocTypePieceStandard(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonEchangeDocBlob objects pre-filled with all related objects except CommonEchangeDoc.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocBlob objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonEchangeDoc(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);
        }

        CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocTypePieceStandardPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonEchangeDocTypePieceStandardPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocBlobPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocBlobPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocBlobPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonBlobOrganismeFile rows

                $key2 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj2 (CommonBlobOrganismeFile)
                $obj2->addCommonEchangeDocBlob($obj1);

            } // if joined row is not null

                // Add objects for joined CommonEchangeDocTypePieceStandard rows

                $key3 = CommonEchangeDocTypePieceStandardPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonEchangeDocTypePieceStandardPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonEchangeDocTypePieceStandardPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonEchangeDocTypePieceStandardPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj3 (CommonEchangeDocTypePieceStandard)
                $obj3->addCommonEchangeDocBlob($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDocBlob objects pre-filled with all related objects except CommonBlobOrganismeFile.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocBlob objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonBlobOrganismeFile(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);
        }

        CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocTypePieceStandardPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonEchangeDocTypePieceStandardPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocBlobPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocBlobPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocBlobPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonEchangeDoc rows

                $key2 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonEchangeDocPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonEchangeDocPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonEchangeDocPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj2 (CommonEchangeDoc)
                $obj2->addCommonEchangeDocBlob($obj1);

            } // if joined row is not null

                // Add objects for joined CommonEchangeDocTypePieceStandard rows

                $key3 = CommonEchangeDocTypePieceStandardPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonEchangeDocTypePieceStandardPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonEchangeDocTypePieceStandardPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonEchangeDocTypePieceStandardPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj3 (CommonEchangeDocTypePieceStandard)
                $obj3->addCommonEchangeDocBlob($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDocBlob objects pre-filled with all related objects except CommonEchangeDocBlobRelatedByDocBlobPrincipalId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocBlob objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonEchangeDocBlobRelatedByDocBlobPrincipalId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);
        }

        CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocTypePieceStandardPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonEchangeDocTypePieceStandardPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, CommonEchangeDocTypePieceStandardPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocBlobPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocBlobPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocBlobPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonEchangeDoc rows

                $key2 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonEchangeDocPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonEchangeDocPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonEchangeDocPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj2 (CommonEchangeDoc)
                $obj2->addCommonEchangeDocBlob($obj1);

            } // if joined row is not null

                // Add objects for joined CommonBlobOrganismeFile rows

                $key3 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj3 (CommonBlobOrganismeFile)
                $obj3->addCommonEchangeDocBlob($obj1);

            } // if joined row is not null

                // Add objects for joined CommonEchangeDocTypePieceStandard rows

                $key4 = CommonEchangeDocTypePieceStandardPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonEchangeDocTypePieceStandardPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonEchangeDocTypePieceStandardPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonEchangeDocTypePieceStandardPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj4 (CommonEchangeDocTypePieceStandard)
                $obj4->addCommonEchangeDocBlob($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDocBlob objects pre-filled with all related objects except CommonEchangeDocTypePieceStandard.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocBlob objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonEchangeDocTypePieceStandard(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);
        }

        CommonEchangeDocBlobPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonBlobOrganismeFilePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, CommonEchangeDocPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, CommonBlobOrganismeFilePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocBlobPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocBlobPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocBlobPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocBlobPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonEchangeDoc rows

                $key2 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonEchangeDocPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonEchangeDocPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonEchangeDocPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj2 (CommonEchangeDoc)
                $obj2->addCommonEchangeDocBlob($obj1);

            } // if joined row is not null

                // Add objects for joined CommonBlobOrganismeFile rows

                $key3 = CommonBlobOrganismeFilePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonBlobOrganismeFilePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonBlobOrganismeFilePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonBlobOrganismeFilePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonEchangeDocBlob) to the collection in $obj3 (CommonBlobOrganismeFile)
                $obj3->addCommonEchangeDocBlob($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonEchangeDocBlobPeer::DATABASE_NAME)->getTable(CommonEchangeDocBlobPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonEchangeDocBlobPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonEchangeDocBlobPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonEchangeDocBlobTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonEchangeDocBlobPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonEchangeDocBlob or Criteria object.
     *
     * @param      mixed $values Criteria or CommonEchangeDocBlob object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonEchangeDocBlob object
        }

        if ($criteria->containsKey(CommonEchangeDocBlobPeer::ID) && $criteria->keyContainsValue(CommonEchangeDocBlobPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonEchangeDocBlobPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonEchangeDocBlob or Criteria object.
     *
     * @param      mixed $values Criteria or CommonEchangeDocBlob object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonEchangeDocBlobPeer::ID);
            $value = $criteria->remove(CommonEchangeDocBlobPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonEchangeDocBlobPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonEchangeDocBlobPeer::TABLE_NAME);
            }

        } else { // $values is CommonEchangeDocBlob object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the echange_doc_blob table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonEchangeDocBlobPeer::TABLE_NAME, $con, CommonEchangeDocBlobPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonEchangeDocBlobPeer::clearInstancePool();
            CommonEchangeDocBlobPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonEchangeDocBlob or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonEchangeDocBlob object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonEchangeDocBlobPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonEchangeDocBlob) { // it's a model object
            // invalidate the cache for this single object
            CommonEchangeDocBlobPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonEchangeDocBlobPeer::DATABASE_NAME);
            $criteria->add(CommonEchangeDocBlobPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonEchangeDocBlobPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocBlobPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonEchangeDocBlobPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonEchangeDocBlob object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonEchangeDocBlob $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonEchangeDocBlobPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonEchangeDocBlobPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonEchangeDocBlobPeer::DATABASE_NAME, CommonEchangeDocBlobPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonEchangeDocBlob
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonEchangeDocBlobPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonEchangeDocBlobPeer::DATABASE_NAME);
        $criteria->add(CommonEchangeDocBlobPeer::ID, $pk);

        $v = CommonEchangeDocBlobPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonEchangeDocBlob[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonEchangeDocBlobPeer::DATABASE_NAME);
            $criteria->add(CommonEchangeDocBlobPeer::ID, $pks, Criteria::IN);
            $objs = CommonEchangeDocBlobPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonEchangeDocBlobPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonEchangeDocBlobPeer::buildTableMap();

