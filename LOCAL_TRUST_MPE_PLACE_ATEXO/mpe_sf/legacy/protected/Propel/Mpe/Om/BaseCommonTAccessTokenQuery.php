<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTAccessToken;
use Application\Propel\Mpe\CommonTAccessTokenPeer;
use Application\Propel\Mpe\CommonTAccessTokenQuery;

/**
 * Base class that represents a query for the 't_access_token' table.
 *
 *
 *
 * @method CommonTAccessTokenQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTAccessTokenQuery orderByData($order = Criteria::ASC) Order by the data column
 * @method CommonTAccessTokenQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method CommonTAccessTokenQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method CommonTAccessTokenQuery orderByUserType($order = Criteria::ASC) Order by the user_type column
 * @method CommonTAccessTokenQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 *
 * @method CommonTAccessTokenQuery groupById() Group by the id column
 * @method CommonTAccessTokenQuery groupByData() Group by the data column
 * @method CommonTAccessTokenQuery groupByIp() Group by the ip column
 * @method CommonTAccessTokenQuery groupByUserId() Group by the user_id column
 * @method CommonTAccessTokenQuery groupByUserType() Group by the user_type column
 * @method CommonTAccessTokenQuery groupByCreatedAt() Group by the created_at column
 *
 * @method CommonTAccessTokenQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTAccessTokenQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTAccessTokenQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTAccessToken findOne(PropelPDO $con = null) Return the first CommonTAccessToken matching the query
 * @method CommonTAccessToken findOneOrCreate(PropelPDO $con = null) Return the first CommonTAccessToken matching the query, or a new CommonTAccessToken object populated from the query conditions when no match is found
 *
 * @method CommonTAccessToken findOneByData(string $data) Return the first CommonTAccessToken filtered by the data column
 * @method CommonTAccessToken findOneByIp(string $ip) Return the first CommonTAccessToken filtered by the ip column
 * @method CommonTAccessToken findOneByUserId(int $user_id) Return the first CommonTAccessToken filtered by the user_id column
 * @method CommonTAccessToken findOneByUserType(int $user_type) Return the first CommonTAccessToken filtered by the user_type column
 * @method CommonTAccessToken findOneByCreatedAt(string $created_at) Return the first CommonTAccessToken filtered by the created_at column
 *
 * @method array findById(string $id) Return CommonTAccessToken objects filtered by the id column
 * @method array findByData(string $data) Return CommonTAccessToken objects filtered by the data column
 * @method array findByIp(string $ip) Return CommonTAccessToken objects filtered by the ip column
 * @method array findByUserId(int $user_id) Return CommonTAccessToken objects filtered by the user_id column
 * @method array findByUserType(int $user_type) Return CommonTAccessToken objects filtered by the user_type column
 * @method array findByCreatedAt(string $created_at) Return CommonTAccessToken objects filtered by the created_at column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTAccessTokenQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTAccessTokenQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTAccessToken', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTAccessTokenQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTAccessTokenQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTAccessTokenQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTAccessTokenQuery) {
            return $criteria;
        }
        $query = new CommonTAccessTokenQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTAccessToken|CommonTAccessToken[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTAccessTokenPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTAccessTokenPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTAccessToken A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTAccessToken A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `data`, `ip`, `user_id`, `user_type`, `created_at` FROM `t_access_token` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTAccessToken();
            $obj->hydrate($row);
            CommonTAccessTokenPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTAccessToken|CommonTAccessToken[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTAccessToken[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTAccessTokenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTAccessTokenPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTAccessTokenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTAccessTokenPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAccessTokenQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTAccessTokenPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the data column
     *
     * Example usage:
     * <code>
     * $query->filterByData('fooValue');   // WHERE data = 'fooValue'
     * $query->filterByData('%fooValue%'); // WHERE data LIKE '%fooValue%'
     * </code>
     *
     * @param     string $data The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAccessTokenQuery The current query, for fluid interface
     */
    public function filterByData($data = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($data)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $data)) {
                $data = str_replace('*', '%', $data);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTAccessTokenPeer::DATA, $data, $comparison);
    }

    /**
     * Filter the query on the ip column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
     * $query->filterByIp('%fooValue%'); // WHERE ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAccessTokenQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ip)) {
                $ip = str_replace('*', '%', $ip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTAccessTokenPeer::IP, $ip, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id >= 12
     * $query->filterByUserId(array('max' => 12)); // WHERE user_id <= 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAccessTokenQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(CommonTAccessTokenPeer::USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(CommonTAccessTokenPeer::USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAccessTokenPeer::USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the user_type column
     *
     * Example usage:
     * <code>
     * $query->filterByUserType(1234); // WHERE user_type = 1234
     * $query->filterByUserType(array(12, 34)); // WHERE user_type IN (12, 34)
     * $query->filterByUserType(array('min' => 12)); // WHERE user_type >= 12
     * $query->filterByUserType(array('max' => 12)); // WHERE user_type <= 12
     * </code>
     *
     * @param     mixed $userType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAccessTokenQuery The current query, for fluid interface
     */
    public function filterByUserType($userType = null, $comparison = null)
    {
        if (is_array($userType)) {
            $useMinMax = false;
            if (isset($userType['min'])) {
                $this->addUsingAlias(CommonTAccessTokenPeer::USER_TYPE, $userType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userType['max'])) {
                $this->addUsingAlias(CommonTAccessTokenPeer::USER_TYPE, $userType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAccessTokenPeer::USER_TYPE, $userType, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAccessTokenQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CommonTAccessTokenPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CommonTAccessTokenPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAccessTokenPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTAccessToken $commonTAccessToken Object to remove from the list of results
     *
     * @return CommonTAccessTokenQuery The current query, for fluid interface
     */
    public function prune($commonTAccessToken = null)
    {
        if ($commonTAccessToken) {
            $this->addUsingAlias(CommonTAccessTokenPeer::ID, $commonTAccessToken->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
