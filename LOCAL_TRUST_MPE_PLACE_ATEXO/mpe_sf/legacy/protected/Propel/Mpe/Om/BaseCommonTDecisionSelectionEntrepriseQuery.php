<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTDecisionSelectionEntreprise;
use Application\Propel\Mpe\CommonTDecisionSelectionEntreprisePeer;
use Application\Propel\Mpe\CommonTDecisionSelectionEntrepriseQuery;

/**
 * Base class that represents a query for the 't_decision_selection_entreprise' table.
 *
 *
 *
 * @method CommonTDecisionSelectionEntrepriseQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByConsultationRef($order = Criteria::ASC) Order by the consultation_ref column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByOldServiceId($order = Criteria::ASC) Order by the old_service_id column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByLot($order = Criteria::ASC) Order by the lot column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByIdEntreprise($order = Criteria::ASC) Order by the id_entreprise column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByIdEtablissement($order = Criteria::ASC) Order by the id_etablissement column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByIdOffre($order = Criteria::ASC) Order by the id_offre column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByTypeDepotReponse($order = Criteria::ASC) Order by the type_depot_reponse column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByIdContactContrat($order = Criteria::ASC) Order by the id_contact_contrat column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 * @method CommonTDecisionSelectionEntrepriseQuery orderByServiceId($order = Criteria::ASC) Order by the service_id column
 *
 * @method CommonTDecisionSelectionEntrepriseQuery groupById() Group by the id column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByConsultationRef() Group by the consultation_ref column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByOrganisme() Group by the organisme column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByOldServiceId() Group by the old_service_id column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByLot() Group by the lot column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByIdEntreprise() Group by the id_entreprise column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByIdEtablissement() Group by the id_etablissement column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByIdOffre() Group by the id_offre column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByTypeDepotReponse() Group by the type_depot_reponse column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByDateModification() Group by the date_modification column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByIdContactContrat() Group by the id_contact_contrat column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByConsultationId() Group by the consultation_id column
 * @method CommonTDecisionSelectionEntrepriseQuery groupByServiceId() Group by the service_id column
 *
 * @method CommonTDecisionSelectionEntrepriseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTDecisionSelectionEntrepriseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTDecisionSelectionEntrepriseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTDecisionSelectionEntrepriseQuery leftJoinCommonTContactContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContactContrat relation
 * @method CommonTDecisionSelectionEntrepriseQuery rightJoinCommonTContactContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContactContrat relation
 * @method CommonTDecisionSelectionEntrepriseQuery innerJoinCommonTContactContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContactContrat relation
 *
 * @method CommonTDecisionSelectionEntreprise findOne(PropelPDO $con = null) Return the first CommonTDecisionSelectionEntreprise matching the query
 * @method CommonTDecisionSelectionEntreprise findOneOrCreate(PropelPDO $con = null) Return the first CommonTDecisionSelectionEntreprise matching the query, or a new CommonTDecisionSelectionEntreprise object populated from the query conditions when no match is found
 *
 * @method CommonTDecisionSelectionEntreprise findOneByConsultationRef(int $consultation_ref) Return the first CommonTDecisionSelectionEntreprise filtered by the consultation_ref column
 * @method CommonTDecisionSelectionEntreprise findOneByOrganisme(string $organisme) Return the first CommonTDecisionSelectionEntreprise filtered by the organisme column
 * @method CommonTDecisionSelectionEntreprise findOneByOldServiceId(int $old_service_id) Return the first CommonTDecisionSelectionEntreprise filtered by the old_service_id column
 * @method CommonTDecisionSelectionEntreprise findOneByLot(int $lot) Return the first CommonTDecisionSelectionEntreprise filtered by the lot column
 * @method CommonTDecisionSelectionEntreprise findOneByIdEntreprise(int $id_entreprise) Return the first CommonTDecisionSelectionEntreprise filtered by the id_entreprise column
 * @method CommonTDecisionSelectionEntreprise findOneByIdEtablissement(int $id_etablissement) Return the first CommonTDecisionSelectionEntreprise filtered by the id_etablissement column
 * @method CommonTDecisionSelectionEntreprise findOneByIdOffre(int $id_offre) Return the first CommonTDecisionSelectionEntreprise filtered by the id_offre column
 * @method CommonTDecisionSelectionEntreprise findOneByTypeDepotReponse(string $type_depot_reponse) Return the first CommonTDecisionSelectionEntreprise filtered by the type_depot_reponse column
 * @method CommonTDecisionSelectionEntreprise findOneByDateCreation(string $date_creation) Return the first CommonTDecisionSelectionEntreprise filtered by the date_creation column
 * @method CommonTDecisionSelectionEntreprise findOneByDateModification(string $date_modification) Return the first CommonTDecisionSelectionEntreprise filtered by the date_modification column
 * @method CommonTDecisionSelectionEntreprise findOneByIdContactContrat(int $id_contact_contrat) Return the first CommonTDecisionSelectionEntreprise filtered by the id_contact_contrat column
 * @method CommonTDecisionSelectionEntreprise findOneByConsultationId(int $consultation_id) Return the first CommonTDecisionSelectionEntreprise filtered by the consultation_id column
 * @method CommonTDecisionSelectionEntreprise findOneByServiceId(string $service_id) Return the first CommonTDecisionSelectionEntreprise filtered by the service_id column
 *
 * @method array findById(int $id) Return CommonTDecisionSelectionEntreprise objects filtered by the id column
 * @method array findByConsultationRef(int $consultation_ref) Return CommonTDecisionSelectionEntreprise objects filtered by the consultation_ref column
 * @method array findByOrganisme(string $organisme) Return CommonTDecisionSelectionEntreprise objects filtered by the organisme column
 * @method array findByOldServiceId(int $old_service_id) Return CommonTDecisionSelectionEntreprise objects filtered by the old_service_id column
 * @method array findByLot(int $lot) Return CommonTDecisionSelectionEntreprise objects filtered by the lot column
 * @method array findByIdEntreprise(int $id_entreprise) Return CommonTDecisionSelectionEntreprise objects filtered by the id_entreprise column
 * @method array findByIdEtablissement(int $id_etablissement) Return CommonTDecisionSelectionEntreprise objects filtered by the id_etablissement column
 * @method array findByIdOffre(int $id_offre) Return CommonTDecisionSelectionEntreprise objects filtered by the id_offre column
 * @method array findByTypeDepotReponse(string $type_depot_reponse) Return CommonTDecisionSelectionEntreprise objects filtered by the type_depot_reponse column
 * @method array findByDateCreation(string $date_creation) Return CommonTDecisionSelectionEntreprise objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonTDecisionSelectionEntreprise objects filtered by the date_modification column
 * @method array findByIdContactContrat(int $id_contact_contrat) Return CommonTDecisionSelectionEntreprise objects filtered by the id_contact_contrat column
 * @method array findByConsultationId(int $consultation_id) Return CommonTDecisionSelectionEntreprise objects filtered by the consultation_id column
 * @method array findByServiceId(string $service_id) Return CommonTDecisionSelectionEntreprise objects filtered by the service_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDecisionSelectionEntrepriseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTDecisionSelectionEntrepriseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTDecisionSelectionEntreprise', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTDecisionSelectionEntrepriseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTDecisionSelectionEntrepriseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTDecisionSelectionEntrepriseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTDecisionSelectionEntrepriseQuery) {
            return $criteria;
        }
        $query = new CommonTDecisionSelectionEntrepriseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTDecisionSelectionEntreprise|CommonTDecisionSelectionEntreprise[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTDecisionSelectionEntreprisePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTDecisionSelectionEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDecisionSelectionEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDecisionSelectionEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `consultation_ref`, `organisme`, `old_service_id`, `lot`, `id_entreprise`, `id_etablissement`, `id_offre`, `type_depot_reponse`, `date_creation`, `date_modification`, `id_contact_contrat`, `consultation_id`, `service_id` FROM `t_decision_selection_entreprise` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTDecisionSelectionEntreprise();
            $obj->hydrate($row);
            CommonTDecisionSelectionEntreprisePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTDecisionSelectionEntreprise|CommonTDecisionSelectionEntreprise[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTDecisionSelectionEntreprise[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the consultation_ref column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationRef(1234); // WHERE consultation_ref = 1234
     * $query->filterByConsultationRef(array(12, 34)); // WHERE consultation_ref IN (12, 34)
     * $query->filterByConsultationRef(array('min' => 12)); // WHERE consultation_ref >= 12
     * $query->filterByConsultationRef(array('max' => 12)); // WHERE consultation_ref <= 12
     * </code>
     *
     * @param     mixed $consultationRef The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByConsultationRef($consultationRef = null, $comparison = null)
    {
        if (is_array($consultationRef)) {
            $useMinMax = false;
            if (isset($consultationRef['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::CONSULTATION_REF, $consultationRef['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationRef['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::CONSULTATION_REF, $consultationRef['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::CONSULTATION_REF, $consultationRef, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldServiceId(1234); // WHERE old_service_id = 1234
     * $query->filterByOldServiceId(array(12, 34)); // WHERE old_service_id IN (12, 34)
     * $query->filterByOldServiceId(array('min' => 12)); // WHERE old_service_id >= 12
     * $query->filterByOldServiceId(array('max' => 12)); // WHERE old_service_id <= 12
     * </code>
     *
     * @param     mixed $oldServiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByOldServiceId($oldServiceId = null, $comparison = null)
    {
        if (is_array($oldServiceId)) {
            $useMinMax = false;
            if (isset($oldServiceId['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::OLD_SERVICE_ID, $oldServiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldServiceId['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::OLD_SERVICE_ID, $oldServiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::OLD_SERVICE_ID, $oldServiceId, $comparison);
    }

    /**
     * Filter the query on the lot column
     *
     * Example usage:
     * <code>
     * $query->filterByLot(1234); // WHERE lot = 1234
     * $query->filterByLot(array(12, 34)); // WHERE lot IN (12, 34)
     * $query->filterByLot(array('min' => 12)); // WHERE lot >= 12
     * $query->filterByLot(array('max' => 12)); // WHERE lot <= 12
     * </code>
     *
     * @param     mixed $lot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByLot($lot = null, $comparison = null)
    {
        if (is_array($lot)) {
            $useMinMax = false;
            if (isset($lot['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::LOT, $lot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lot['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::LOT, $lot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::LOT, $lot, $comparison);
    }

    /**
     * Filter the query on the id_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntreprise(1234); // WHERE id_entreprise = 1234
     * $query->filterByIdEntreprise(array(12, 34)); // WHERE id_entreprise IN (12, 34)
     * $query->filterByIdEntreprise(array('min' => 12)); // WHERE id_entreprise >= 12
     * $query->filterByIdEntreprise(array('max' => 12)); // WHERE id_entreprise <= 12
     * </code>
     *
     * @param     mixed $idEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdEntreprise($idEntreprise = null, $comparison = null)
    {
        if (is_array($idEntreprise)) {
            $useMinMax = false;
            if (isset($idEntreprise['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_ENTREPRISE, $idEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntreprise['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_ENTREPRISE, $idEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_ENTREPRISE, $idEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtablissement(1234); // WHERE id_etablissement = 1234
     * $query->filterByIdEtablissement(array(12, 34)); // WHERE id_etablissement IN (12, 34)
     * $query->filterByIdEtablissement(array('min' => 12)); // WHERE id_etablissement >= 12
     * $query->filterByIdEtablissement(array('max' => 12)); // WHERE id_etablissement <= 12
     * </code>
     *
     * @param     mixed $idEtablissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdEtablissement($idEtablissement = null, $comparison = null)
    {
        if (is_array($idEtablissement)) {
            $useMinMax = false;
            if (isset($idEtablissement['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_ETABLISSEMENT, $idEtablissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtablissement['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_ETABLISSEMENT, $idEtablissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_ETABLISSEMENT, $idEtablissement, $comparison);
    }

    /**
     * Filter the query on the id_offre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOffre(1234); // WHERE id_offre = 1234
     * $query->filterByIdOffre(array(12, 34)); // WHERE id_offre IN (12, 34)
     * $query->filterByIdOffre(array('min' => 12)); // WHERE id_offre >= 12
     * $query->filterByIdOffre(array('max' => 12)); // WHERE id_offre <= 12
     * </code>
     *
     * @param     mixed $idOffre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdOffre($idOffre = null, $comparison = null)
    {
        if (is_array($idOffre)) {
            $useMinMax = false;
            if (isset($idOffre['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_OFFRE, $idOffre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOffre['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_OFFRE, $idOffre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_OFFRE, $idOffre, $comparison);
    }

    /**
     * Filter the query on the type_depot_reponse column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeDepotReponse('fooValue');   // WHERE type_depot_reponse = 'fooValue'
     * $query->filterByTypeDepotReponse('%fooValue%'); // WHERE type_depot_reponse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeDepotReponse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByTypeDepotReponse($typeDepotReponse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeDepotReponse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeDepotReponse)) {
                $typeDepotReponse = str_replace('*', '%', $typeDepotReponse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::TYPE_DEPOT_REPONSE, $typeDepotReponse, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the id_contact_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContactContrat(1234); // WHERE id_contact_contrat = 1234
     * $query->filterByIdContactContrat(array(12, 34)); // WHERE id_contact_contrat IN (12, 34)
     * $query->filterByIdContactContrat(array('min' => 12)); // WHERE id_contact_contrat >= 12
     * $query->filterByIdContactContrat(array('max' => 12)); // WHERE id_contact_contrat <= 12
     * </code>
     *
     * @see       filterByCommonTContactContrat()
     *
     * @param     mixed $idContactContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdContactContrat($idContactContrat = null, $comparison = null)
    {
        if (is_array($idContactContrat)) {
            $useMinMax = false;
            if (isset($idContactContrat['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_CONTACT_CONTRAT, $idContactContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContactContrat['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_CONTACT_CONTRAT, $idContactContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_CONTACT_CONTRAT, $idContactContrat, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query on the service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceId(1234); // WHERE service_id = 1234
     * $query->filterByServiceId(array(12, 34)); // WHERE service_id IN (12, 34)
     * $query->filterByServiceId(array('min' => 12)); // WHERE service_id >= 12
     * $query->filterByServiceId(array('max' => 12)); // WHERE service_id <= 12
     * </code>
     *
     * @param     mixed $serviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function filterByServiceId($serviceId = null, $comparison = null)
    {
        if (is_array($serviceId)) {
            $useMinMax = false;
            if (isset($serviceId['min'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::SERVICE_ID, $serviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceId['max'])) {
                $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::SERVICE_ID, $serviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::SERVICE_ID, $serviceId, $comparison);
    }

    /**
     * Filter the query by a related CommonTContactContrat object
     *
     * @param   CommonTContactContrat|PropelObjectCollection $commonTContactContrat The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContactContrat($commonTContactContrat, $comparison = null)
    {
        if ($commonTContactContrat instanceof CommonTContactContrat) {
            return $this
                ->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_CONTACT_CONTRAT, $commonTContactContrat->getIdContactContrat(), $comparison);
        } elseif ($commonTContactContrat instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID_CONTACT_CONTRAT, $commonTContactContrat->toKeyValue('PrimaryKey', 'IdContactContrat'), $comparison);
        } else {
            throw new PropelException('filterByCommonTContactContrat() only accepts arguments of type CommonTContactContrat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContactContrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTContactContrat($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContactContrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContactContrat');
        }

        return $this;
    }

    /**
     * Use the CommonTContactContrat relation CommonTContactContrat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContactContratQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContactContratQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContactContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContactContrat', '\Application\Propel\Mpe\CommonTContactContratQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTDecisionSelectionEntreprise $commonTDecisionSelectionEntreprise Object to remove from the list of results
     *
     * @return CommonTDecisionSelectionEntrepriseQuery The current query, for fluid interface
     */
    public function prune($commonTDecisionSelectionEntreprise = null)
    {
        if ($commonTDecisionSelectionEntreprise) {
            $this->addUsingAlias(CommonTDecisionSelectionEntreprisePeer::ID, $commonTDecisionSelectionEntreprise->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
