<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativePeer;
use Application\Propel\Mpe\CommonTChorusFicheModificativePjPeer;
use Application\Propel\Mpe\Map\CommonTChorusFicheModificativeTableMap;

/**
 * Base static class for performing query and update operations on the 't_chorus_fiche_modificative' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTChorusFicheModificativePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_chorus_fiche_modificative';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTChorusFicheModificative';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTChorusFicheModificativeTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 23;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 23;

    /** the column name for the id_fiche_modificative field */
    const ID_FICHE_MODIFICATIVE = 't_chorus_fiche_modificative.id_fiche_modificative';

    /** the column name for the id_echange field */
    const ID_ECHANGE = 't_chorus_fiche_modificative.id_echange';

    /** the column name for the organisme field */
    const ORGANISME = 't_chorus_fiche_modificative.organisme';

    /** the column name for the type_modification field */
    const TYPE_MODIFICATION = 't_chorus_fiche_modificative.type_modification';

    /** the column name for the date_prevue_notification field */
    const DATE_PREVUE_NOTIFICATION = 't_chorus_fiche_modificative.date_prevue_notification';

    /** the column name for the date_fin_marche field */
    const DATE_FIN_MARCHE = 't_chorus_fiche_modificative.date_fin_marche';

    /** the column name for the date_fin_marche_modifie field */
    const DATE_FIN_MARCHE_MODIFIE = 't_chorus_fiche_modificative.date_fin_marche_modifie';

    /** the column name for the montant_marche field */
    const MONTANT_MARCHE = 't_chorus_fiche_modificative.montant_marche';

    /** the column name for the montant_acte field */
    const MONTANT_ACTE = 't_chorus_fiche_modificative.montant_acte';

    /** the column name for the taux_tva field */
    const TAUX_TVA = 't_chorus_fiche_modificative.taux_tva';

    /** the column name for the nombre_fournisseur_cotraitant field */
    const NOMBRE_FOURNISSEUR_COTRAITANT = 't_chorus_fiche_modificative.nombre_fournisseur_cotraitant';

    /** the column name for the localites_fournisseurs field */
    const LOCALITES_FOURNISSEURS = 't_chorus_fiche_modificative.localites_fournisseurs';

    /** the column name for the siren_fournisseur field */
    const SIREN_FOURNISSEUR = 't_chorus_fiche_modificative.siren_fournisseur';

    /** the column name for the siret_fournisseur field */
    const SIRET_FOURNISSEUR = 't_chorus_fiche_modificative.siret_fournisseur';

    /** the column name for the nom_fournisseur field */
    const NOM_FOURNISSEUR = 't_chorus_fiche_modificative.nom_fournisseur';

    /** the column name for the type_fournisseur field */
    const TYPE_FOURNISSEUR = 't_chorus_fiche_modificative.type_fournisseur';

    /** the column name for the visa_accf field */
    const VISA_ACCF = 't_chorus_fiche_modificative.visa_accf';

    /** the column name for the visa_prefet field */
    const VISA_PREFET = 't_chorus_fiche_modificative.visa_prefet';

    /** the column name for the remarque field */
    const REMARQUE = 't_chorus_fiche_modificative.remarque';

    /** the column name for the id_blob_piece_justificatives field */
    const ID_BLOB_PIECE_JUSTIFICATIVES = 't_chorus_fiche_modificative.id_blob_piece_justificatives';

    /** the column name for the id_blob_fiche_modificative field */
    const ID_BLOB_FICHE_MODIFICATIVE = 't_chorus_fiche_modificative.id_blob_fiche_modificative';

    /** the column name for the date_creation field */
    const DATE_CREATION = 't_chorus_fiche_modificative.date_creation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 't_chorus_fiche_modificative.date_modification';

    /** The enumerated values for the visa_accf field */
    const VISA_ACCF_0 = '0';
    const VISA_ACCF_1 = '1';
    const VISA_ACCF_2 = '2';

    /** The enumerated values for the visa_prefet field */
    const VISA_PREFET_0 = '0';
    const VISA_PREFET_1 = '1';
    const VISA_PREFET_2 = '2';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTChorusFicheModificative objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTChorusFicheModificative[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTChorusFicheModificativePeer::$fieldNames[CommonTChorusFicheModificativePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdFicheModificative', 'IdEchange', 'Organisme', 'TypeModification', 'DatePrevueNotification', 'DateFinMarche', 'DateFinMarcheModifie', 'MontantMarche', 'MontantActe', 'TauxTva', 'NombreFournisseurCotraitant', 'LocalitesFournisseurs', 'SirenFournisseur', 'SiretFournisseur', 'NomFournisseur', 'TypeFournisseur', 'VisaAccf', 'VisaPrefet', 'Remarque', 'IdBlobPieceJustificatives', 'IdBlobFicheModificative', 'DateCreation', 'DateModification', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idFicheModificative', 'idEchange', 'organisme', 'typeModification', 'datePrevueNotification', 'dateFinMarche', 'dateFinMarcheModifie', 'montantMarche', 'montantActe', 'tauxTva', 'nombreFournisseurCotraitant', 'localitesFournisseurs', 'sirenFournisseur', 'siretFournisseur', 'nomFournisseur', 'typeFournisseur', 'visaAccf', 'visaPrefet', 'remarque', 'idBlobPieceJustificatives', 'idBlobFicheModificative', 'dateCreation', 'dateModification', ),
        BasePeer::TYPE_COLNAME => array (CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, CommonTChorusFicheModificativePeer::ID_ECHANGE, CommonTChorusFicheModificativePeer::ORGANISME, CommonTChorusFicheModificativePeer::TYPE_MODIFICATION, CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION, CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE, CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE, CommonTChorusFicheModificativePeer::MONTANT_MARCHE, CommonTChorusFicheModificativePeer::MONTANT_ACTE, CommonTChorusFicheModificativePeer::TAUX_TVA, CommonTChorusFicheModificativePeer::NOMBRE_FOURNISSEUR_COTRAITANT, CommonTChorusFicheModificativePeer::LOCALITES_FOURNISSEURS, CommonTChorusFicheModificativePeer::SIREN_FOURNISSEUR, CommonTChorusFicheModificativePeer::SIRET_FOURNISSEUR, CommonTChorusFicheModificativePeer::NOM_FOURNISSEUR, CommonTChorusFicheModificativePeer::TYPE_FOURNISSEUR, CommonTChorusFicheModificativePeer::VISA_ACCF, CommonTChorusFicheModificativePeer::VISA_PREFET, CommonTChorusFicheModificativePeer::REMARQUE, CommonTChorusFicheModificativePeer::ID_BLOB_PIECE_JUSTIFICATIVES, CommonTChorusFicheModificativePeer::ID_BLOB_FICHE_MODIFICATIVE, CommonTChorusFicheModificativePeer::DATE_CREATION, CommonTChorusFicheModificativePeer::DATE_MODIFICATION, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_FICHE_MODIFICATIVE', 'ID_ECHANGE', 'ORGANISME', 'TYPE_MODIFICATION', 'DATE_PREVUE_NOTIFICATION', 'DATE_FIN_MARCHE', 'DATE_FIN_MARCHE_MODIFIE', 'MONTANT_MARCHE', 'MONTANT_ACTE', 'TAUX_TVA', 'NOMBRE_FOURNISSEUR_COTRAITANT', 'LOCALITES_FOURNISSEURS', 'SIREN_FOURNISSEUR', 'SIRET_FOURNISSEUR', 'NOM_FOURNISSEUR', 'TYPE_FOURNISSEUR', 'VISA_ACCF', 'VISA_PREFET', 'REMARQUE', 'ID_BLOB_PIECE_JUSTIFICATIVES', 'ID_BLOB_FICHE_MODIFICATIVE', 'DATE_CREATION', 'DATE_MODIFICATION', ),
        BasePeer::TYPE_FIELDNAME => array ('id_fiche_modificative', 'id_echange', 'organisme', 'type_modification', 'date_prevue_notification', 'date_fin_marche', 'date_fin_marche_modifie', 'montant_marche', 'montant_acte', 'taux_tva', 'nombre_fournisseur_cotraitant', 'localites_fournisseurs', 'siren_fournisseur', 'siret_fournisseur', 'nom_fournisseur', 'type_fournisseur', 'visa_accf', 'visa_prefet', 'remarque', 'id_blob_piece_justificatives', 'id_blob_fiche_modificative', 'date_creation', 'date_modification', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTChorusFicheModificativePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdFicheModificative' => 0, 'IdEchange' => 1, 'Organisme' => 2, 'TypeModification' => 3, 'DatePrevueNotification' => 4, 'DateFinMarche' => 5, 'DateFinMarcheModifie' => 6, 'MontantMarche' => 7, 'MontantActe' => 8, 'TauxTva' => 9, 'NombreFournisseurCotraitant' => 10, 'LocalitesFournisseurs' => 11, 'SirenFournisseur' => 12, 'SiretFournisseur' => 13, 'NomFournisseur' => 14, 'TypeFournisseur' => 15, 'VisaAccf' => 16, 'VisaPrefet' => 17, 'Remarque' => 18, 'IdBlobPieceJustificatives' => 19, 'IdBlobFicheModificative' => 20, 'DateCreation' => 21, 'DateModification' => 22, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idFicheModificative' => 0, 'idEchange' => 1, 'organisme' => 2, 'typeModification' => 3, 'datePrevueNotification' => 4, 'dateFinMarche' => 5, 'dateFinMarcheModifie' => 6, 'montantMarche' => 7, 'montantActe' => 8, 'tauxTva' => 9, 'nombreFournisseurCotraitant' => 10, 'localitesFournisseurs' => 11, 'sirenFournisseur' => 12, 'siretFournisseur' => 13, 'nomFournisseur' => 14, 'typeFournisseur' => 15, 'visaAccf' => 16, 'visaPrefet' => 17, 'remarque' => 18, 'idBlobPieceJustificatives' => 19, 'idBlobFicheModificative' => 20, 'dateCreation' => 21, 'dateModification' => 22, ),
        BasePeer::TYPE_COLNAME => array (CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE => 0, CommonTChorusFicheModificativePeer::ID_ECHANGE => 1, CommonTChorusFicheModificativePeer::ORGANISME => 2, CommonTChorusFicheModificativePeer::TYPE_MODIFICATION => 3, CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION => 4, CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE => 5, CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE => 6, CommonTChorusFicheModificativePeer::MONTANT_MARCHE => 7, CommonTChorusFicheModificativePeer::MONTANT_ACTE => 8, CommonTChorusFicheModificativePeer::TAUX_TVA => 9, CommonTChorusFicheModificativePeer::NOMBRE_FOURNISSEUR_COTRAITANT => 10, CommonTChorusFicheModificativePeer::LOCALITES_FOURNISSEURS => 11, CommonTChorusFicheModificativePeer::SIREN_FOURNISSEUR => 12, CommonTChorusFicheModificativePeer::SIRET_FOURNISSEUR => 13, CommonTChorusFicheModificativePeer::NOM_FOURNISSEUR => 14, CommonTChorusFicheModificativePeer::TYPE_FOURNISSEUR => 15, CommonTChorusFicheModificativePeer::VISA_ACCF => 16, CommonTChorusFicheModificativePeer::VISA_PREFET => 17, CommonTChorusFicheModificativePeer::REMARQUE => 18, CommonTChorusFicheModificativePeer::ID_BLOB_PIECE_JUSTIFICATIVES => 19, CommonTChorusFicheModificativePeer::ID_BLOB_FICHE_MODIFICATIVE => 20, CommonTChorusFicheModificativePeer::DATE_CREATION => 21, CommonTChorusFicheModificativePeer::DATE_MODIFICATION => 22, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_FICHE_MODIFICATIVE' => 0, 'ID_ECHANGE' => 1, 'ORGANISME' => 2, 'TYPE_MODIFICATION' => 3, 'DATE_PREVUE_NOTIFICATION' => 4, 'DATE_FIN_MARCHE' => 5, 'DATE_FIN_MARCHE_MODIFIE' => 6, 'MONTANT_MARCHE' => 7, 'MONTANT_ACTE' => 8, 'TAUX_TVA' => 9, 'NOMBRE_FOURNISSEUR_COTRAITANT' => 10, 'LOCALITES_FOURNISSEURS' => 11, 'SIREN_FOURNISSEUR' => 12, 'SIRET_FOURNISSEUR' => 13, 'NOM_FOURNISSEUR' => 14, 'TYPE_FOURNISSEUR' => 15, 'VISA_ACCF' => 16, 'VISA_PREFET' => 17, 'REMARQUE' => 18, 'ID_BLOB_PIECE_JUSTIFICATIVES' => 19, 'ID_BLOB_FICHE_MODIFICATIVE' => 20, 'DATE_CREATION' => 21, 'DATE_MODIFICATION' => 22, ),
        BasePeer::TYPE_FIELDNAME => array ('id_fiche_modificative' => 0, 'id_echange' => 1, 'organisme' => 2, 'type_modification' => 3, 'date_prevue_notification' => 4, 'date_fin_marche' => 5, 'date_fin_marche_modifie' => 6, 'montant_marche' => 7, 'montant_acte' => 8, 'taux_tva' => 9, 'nombre_fournisseur_cotraitant' => 10, 'localites_fournisseurs' => 11, 'siren_fournisseur' => 12, 'siret_fournisseur' => 13, 'nom_fournisseur' => 14, 'type_fournisseur' => 15, 'visa_accf' => 16, 'visa_prefet' => 17, 'remarque' => 18, 'id_blob_piece_justificatives' => 19, 'id_blob_fiche_modificative' => 20, 'date_creation' => 21, 'date_modification' => 22, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTChorusFicheModificativePeer::VISA_ACCF => array(
            CommonTChorusFicheModificativePeer::VISA_ACCF_0,
            CommonTChorusFicheModificativePeer::VISA_ACCF_1,
            CommonTChorusFicheModificativePeer::VISA_ACCF_2,
        ),
        CommonTChorusFicheModificativePeer::VISA_PREFET => array(
            CommonTChorusFicheModificativePeer::VISA_PREFET_0,
            CommonTChorusFicheModificativePeer::VISA_PREFET_1,
            CommonTChorusFicheModificativePeer::VISA_PREFET_2,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTChorusFicheModificativePeer::getFieldNames($toType);
        $key = isset(CommonTChorusFicheModificativePeer::$fieldKeys[$fromType][$name]) ? CommonTChorusFicheModificativePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTChorusFicheModificativePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTChorusFicheModificativePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTChorusFicheModificativePeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTChorusFicheModificativePeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTChorusFicheModificativePeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTChorusFicheModificativePeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTChorusFicheModificativePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTChorusFicheModificativePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::ID_ECHANGE);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::ORGANISME);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::TYPE_MODIFICATION);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::MONTANT_MARCHE);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::MONTANT_ACTE);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::TAUX_TVA);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::NOMBRE_FOURNISSEUR_COTRAITANT);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::LOCALITES_FOURNISSEURS);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::SIREN_FOURNISSEUR);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::SIRET_FOURNISSEUR);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::NOM_FOURNISSEUR);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::TYPE_FOURNISSEUR);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::VISA_ACCF);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::VISA_PREFET);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::REMARQUE);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::ID_BLOB_PIECE_JUSTIFICATIVES);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::ID_BLOB_FICHE_MODIFICATIVE);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonTChorusFicheModificativePeer::DATE_MODIFICATION);
        } else {
            $criteria->addSelectColumn($alias . '.id_fiche_modificative');
            $criteria->addSelectColumn($alias . '.id_echange');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.type_modification');
            $criteria->addSelectColumn($alias . '.date_prevue_notification');
            $criteria->addSelectColumn($alias . '.date_fin_marche');
            $criteria->addSelectColumn($alias . '.date_fin_marche_modifie');
            $criteria->addSelectColumn($alias . '.montant_marche');
            $criteria->addSelectColumn($alias . '.montant_acte');
            $criteria->addSelectColumn($alias . '.taux_tva');
            $criteria->addSelectColumn($alias . '.nombre_fournisseur_cotraitant');
            $criteria->addSelectColumn($alias . '.localites_fournisseurs');
            $criteria->addSelectColumn($alias . '.siren_fournisseur');
            $criteria->addSelectColumn($alias . '.siret_fournisseur');
            $criteria->addSelectColumn($alias . '.nom_fournisseur');
            $criteria->addSelectColumn($alias . '.type_fournisseur');
            $criteria->addSelectColumn($alias . '.visa_accf');
            $criteria->addSelectColumn($alias . '.visa_prefet');
            $criteria->addSelectColumn($alias . '.remarque');
            $criteria->addSelectColumn($alias . '.id_blob_piece_justificatives');
            $criteria->addSelectColumn($alias . '.id_blob_fiche_modificative');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_modification');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTChorusFicheModificativePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTChorusFicheModificativePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTChorusFicheModificativePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTChorusFicheModificative
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTChorusFicheModificativePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTChorusFicheModificativePeer::populateObjects(CommonTChorusFicheModificativePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTChorusFicheModificativePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTChorusFicheModificativePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTChorusFicheModificative $obj A CommonTChorusFicheModificative object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getIdFicheModificative();
            } // if key === null
            CommonTChorusFicheModificativePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTChorusFicheModificative object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTChorusFicheModificative) {
                $key = (string) $value->getIdFicheModificative();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTChorusFicheModificative object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTChorusFicheModificativePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTChorusFicheModificative Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTChorusFicheModificativePeer::$instances[$key])) {
                return CommonTChorusFicheModificativePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTChorusFicheModificativePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTChorusFicheModificativePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_chorus_fiche_modificative
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in CommonTChorusFicheModificativePjPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonTChorusFicheModificativePjPeer::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTChorusFicheModificativePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTChorusFicheModificativePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTChorusFicheModificativePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTChorusFicheModificativePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTChorusFicheModificative object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTChorusFicheModificativePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTChorusFicheModificativePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTChorusFicheModificativePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTChorusFicheModificativePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTChorusFicheModificativePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonChorusEchange table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonChorusEchange(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTChorusFicheModificativePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTChorusFicheModificativePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTChorusFicheModificativePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addMultipleJoin(array(
        array(CommonTChorusFicheModificativePeer::ID_ECHANGE, CommonChorusEchangePeer::ID),
        array(CommonTChorusFicheModificativePeer::ORGANISME, CommonChorusEchangePeer::ORGANISME),
      ), $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTChorusFicheModificative objects pre-filled with their CommonChorusEchange objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTChorusFicheModificative objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonChorusEchange(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTChorusFicheModificativePeer::DATABASE_NAME);
        }

        CommonTChorusFicheModificativePeer::addSelectColumns($criteria);
        $startcol = CommonTChorusFicheModificativePeer::NUM_HYDRATE_COLUMNS;
        CommonChorusEchangePeer::addSelectColumns($criteria);

        $criteria->addMultipleJoin(array(
        array(CommonTChorusFicheModificativePeer::ID_ECHANGE, CommonChorusEchangePeer::ID),
        array(CommonTChorusFicheModificativePeer::ORGANISME, CommonChorusEchangePeer::ORGANISME),
      ), $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTChorusFicheModificativePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTChorusFicheModificativePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTChorusFicheModificativePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTChorusFicheModificativePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonChorusEchangePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonChorusEchangePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonChorusEchangePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonChorusEchangePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTChorusFicheModificative) to $obj2 (CommonChorusEchange)
                $obj2->addCommonTChorusFicheModificative($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTChorusFicheModificativePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTChorusFicheModificativePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTChorusFicheModificativePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addMultipleJoin(array(
        array(CommonTChorusFicheModificativePeer::ID_ECHANGE, CommonChorusEchangePeer::ID),
        array(CommonTChorusFicheModificativePeer::ORGANISME, CommonChorusEchangePeer::ORGANISME),
      ), $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTChorusFicheModificative objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTChorusFicheModificative objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTChorusFicheModificativePeer::DATABASE_NAME);
        }

        CommonTChorusFicheModificativePeer::addSelectColumns($criteria);
        $startcol2 = CommonTChorusFicheModificativePeer::NUM_HYDRATE_COLUMNS;

        CommonChorusEchangePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonChorusEchangePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addMultipleJoin(array(
        array(CommonTChorusFicheModificativePeer::ID_ECHANGE, CommonChorusEchangePeer::ID),
        array(CommonTChorusFicheModificativePeer::ORGANISME, CommonChorusEchangePeer::ORGANISME),
      ), $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTChorusFicheModificativePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTChorusFicheModificativePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTChorusFicheModificativePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTChorusFicheModificativePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonChorusEchange rows

            $key2 = CommonChorusEchangePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonChorusEchangePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonChorusEchangePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonChorusEchangePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTChorusFicheModificative) to the collection in $obj2 (CommonChorusEchange)
                $obj2->addCommonTChorusFicheModificative($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTChorusFicheModificativePeer::DATABASE_NAME)->getTable(CommonTChorusFicheModificativePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTChorusFicheModificativePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTChorusFicheModificativePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTChorusFicheModificativeTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTChorusFicheModificativePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTChorusFicheModificative or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTChorusFicheModificative object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTChorusFicheModificative object
        }

        if ($criteria->containsKey(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE) && $criteria->keyContainsValue(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTChorusFicheModificativePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTChorusFicheModificative or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTChorusFicheModificative object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTChorusFicheModificativePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE);
            $value = $criteria->remove(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE);
            if ($value) {
                $selectCriteria->add(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTChorusFicheModificativePeer::TABLE_NAME);
            }

        } else { // $values is CommonTChorusFicheModificative object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTChorusFicheModificativePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_chorus_fiche_modificative table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += CommonTChorusFicheModificativePeer::doOnDeleteCascade(new Criteria(CommonTChorusFicheModificativePeer::DATABASE_NAME), $con);
            $affectedRows += BasePeer::doDeleteAll(CommonTChorusFicheModificativePeer::TABLE_NAME, $con, CommonTChorusFicheModificativePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTChorusFicheModificativePeer::clearInstancePool();
            CommonTChorusFicheModificativePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTChorusFicheModificative or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTChorusFicheModificative object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTChorusFicheModificative) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTChorusFicheModificativePeer::DATABASE_NAME);
            $criteria->add(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, (array) $values, Criteria::IN);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTChorusFicheModificativePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += CommonTChorusFicheModificativePeer::doOnDeleteCascade($c, $con);

            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            if ($values instanceof Criteria) {
                CommonTChorusFicheModificativePeer::clearInstancePool();
            } elseif ($values instanceof CommonTChorusFicheModificative) { // it's a model object
                CommonTChorusFicheModificativePeer::removeInstanceFromPool($values);
            } else { // it's a primary key, or an array of pks
                foreach ((array) $values as $singleval) {
                    CommonTChorusFicheModificativePeer::removeInstanceFromPool($singleval);
                }
            }

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTChorusFicheModificativePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected static function doOnDeleteCascade(Criteria $criteria, PropelPDO $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $criteria
        $objects = CommonTChorusFicheModificativePeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {


            // delete related CommonTChorusFicheModificativePj objects
            $criteria = new Criteria(CommonTChorusFicheModificativePjPeer::DATABASE_NAME);

            $criteria->add(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE, $obj->getIdFicheModificative());
            $affectedRows += CommonTChorusFicheModificativePjPeer::doDelete($criteria, $con);
        }

        return $affectedRows;
    }

    /**
     * Validates all modified columns of given CommonTChorusFicheModificative object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTChorusFicheModificative $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTChorusFicheModificativePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTChorusFicheModificativePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTChorusFicheModificativePeer::DATABASE_NAME, CommonTChorusFicheModificativePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTChorusFicheModificative
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTChorusFicheModificativePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTChorusFicheModificativePeer::DATABASE_NAME);
        $criteria->add(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $pk);

        $v = CommonTChorusFicheModificativePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTChorusFicheModificative[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTChorusFicheModificativePeer::DATABASE_NAME);
            $criteria->add(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $pks, Criteria::IN);
            $objs = CommonTChorusFicheModificativePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTChorusFicheModificativePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTChorusFicheModificativePeer::buildTableMap();

