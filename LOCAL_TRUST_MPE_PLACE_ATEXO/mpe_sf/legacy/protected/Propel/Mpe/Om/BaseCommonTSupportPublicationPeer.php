<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTOffreSupportPublicitePeer;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\CommonTSupportPublicationPeer;
use Application\Propel\Mpe\Map\CommonTSupportPublicationTableMap;

/**
 * Base static class for performing query and update operations on the 't_support_publication' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTSupportPublicationPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_support_publication';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTSupportPublication';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTSupportPublicationTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 20;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 20;

    /** the column name for the id field */
    const ID = 't_support_publication.id';

    /** the column name for the image_logo field */
    const IMAGE_LOGO = 't_support_publication.image_logo';

    /** the column name for the nom field */
    const NOM = 't_support_publication.nom';

    /** the column name for the visible field */
    const VISIBLE = 't_support_publication.visible';

    /** the column name for the ordre field */
    const ORDRE = 't_support_publication.ordre';

    /** the column name for the default_value field */
    const DEFAULT_VALUE = 't_support_publication.default_value';

    /** the column name for the code field */
    const CODE = 't_support_publication.code';

    /** the column name for the actif field */
    const ACTIF = 't_support_publication.actif';

    /** the column name for the url field */
    const URL = 't_support_publication.url';

    /** the column name for the groupe field */
    const GROUPE = 't_support_publication.groupe';

    /** the column name for the tag_debut_fin_groupe field */
    const TAG_DEBUT_FIN_GROUPE = 't_support_publication.tag_debut_fin_groupe';

    /** the column name for the description field */
    const DESCRIPTION = 't_support_publication.description';

    /** the column name for the nbre_total_groupe field */
    const NBRE_TOTAL_GROUPE = 't_support_publication.nbre_total_groupe';

    /** the column name for the lieux_execution field */
    const LIEUX_EXECUTION = 't_support_publication.lieux_execution';

    /** the column name for the detail_info field */
    const DETAIL_INFO = 't_support_publication.detail_info';

    /** the column name for the type_info field */
    const TYPE_INFO = 't_support_publication.type_info';

    /** the column name for the affichage_infos field */
    const AFFICHAGE_INFOS = 't_support_publication.affichage_infos';

    /** the column name for the affichage_message_support field */
    const AFFICHAGE_MESSAGE_SUPPORT = 't_support_publication.affichage_message_support';

    /** the column name for the selection_departements_parution field */
    const SELECTION_DEPARTEMENTS_PARUTION = 't_support_publication.selection_departements_parution';

    /** the column name for the departements_parution field */
    const DEPARTEMENTS_PARUTION = 't_support_publication.departements_parution';

    /** The enumerated values for the visible field */
    const VISIBLE_0 = '0';
    const VISIBLE_1 = '1';

    /** The enumerated values for the default_value field */
    const DEFAULT_VALUE_0 = '0';
    const DEFAULT_VALUE_1 = '1';

    /** The enumerated values for the actif field */
    const ACTIF_0 = '0';
    const ACTIF_1 = '1';

    /** The enumerated values for the affichage_infos field */
    const AFFICHAGE_INFOS_0 = '0';
    const AFFICHAGE_INFOS_1 = '1';

    /** The enumerated values for the affichage_message_support field */
    const AFFICHAGE_MESSAGE_SUPPORT_0 = '0';
    const AFFICHAGE_MESSAGE_SUPPORT_1 = '1';

    /** The enumerated values for the selection_departements_parution field */
    const SELECTION_DEPARTEMENTS_PARUTION_0 = '0';
    const SELECTION_DEPARTEMENTS_PARUTION_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTSupportPublication objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTSupportPublication[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTSupportPublicationPeer::$fieldNames[CommonTSupportPublicationPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'ImageLogo', 'Nom', 'Visible', 'Ordre', 'DefaultValue', 'Code', 'Actif', 'Url', 'Groupe', 'TagDebutFinGroupe', 'Description', 'NbreTotalGroupe', 'LieuxExecution', 'DetailInfo', 'TypeInfo', 'AffichageInfos', 'AffichageMessageSupport', 'SelectionDepartementsParution', 'DepartementsParution', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'imageLogo', 'nom', 'visible', 'ordre', 'defaultValue', 'code', 'actif', 'url', 'groupe', 'tagDebutFinGroupe', 'description', 'nbreTotalGroupe', 'lieuxExecution', 'detailInfo', 'typeInfo', 'affichageInfos', 'affichageMessageSupport', 'selectionDepartementsParution', 'departementsParution', ),
        BasePeer::TYPE_COLNAME => array (CommonTSupportPublicationPeer::ID, CommonTSupportPublicationPeer::IMAGE_LOGO, CommonTSupportPublicationPeer::NOM, CommonTSupportPublicationPeer::VISIBLE, CommonTSupportPublicationPeer::ORDRE, CommonTSupportPublicationPeer::DEFAULT_VALUE, CommonTSupportPublicationPeer::CODE, CommonTSupportPublicationPeer::ACTIF, CommonTSupportPublicationPeer::URL, CommonTSupportPublicationPeer::GROUPE, CommonTSupportPublicationPeer::TAG_DEBUT_FIN_GROUPE, CommonTSupportPublicationPeer::DESCRIPTION, CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE, CommonTSupportPublicationPeer::LIEUX_EXECUTION, CommonTSupportPublicationPeer::DETAIL_INFO, CommonTSupportPublicationPeer::TYPE_INFO, CommonTSupportPublicationPeer::AFFICHAGE_INFOS, CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT, CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION, CommonTSupportPublicationPeer::DEPARTEMENTS_PARUTION, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'IMAGE_LOGO', 'NOM', 'VISIBLE', 'ORDRE', 'DEFAULT_VALUE', 'CODE', 'ACTIF', 'URL', 'GROUPE', 'TAG_DEBUT_FIN_GROUPE', 'DESCRIPTION', 'NBRE_TOTAL_GROUPE', 'LIEUX_EXECUTION', 'DETAIL_INFO', 'TYPE_INFO', 'AFFICHAGE_INFOS', 'AFFICHAGE_MESSAGE_SUPPORT', 'SELECTION_DEPARTEMENTS_PARUTION', 'DEPARTEMENTS_PARUTION', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'image_logo', 'nom', 'visible', 'ordre', 'default_value', 'code', 'actif', 'url', 'groupe', 'tag_debut_fin_groupe', 'description', 'nbre_total_groupe', 'lieux_execution', 'detail_info', 'type_info', 'affichage_infos', 'affichage_message_support', 'selection_departements_parution', 'departements_parution', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTSupportPublicationPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ImageLogo' => 1, 'Nom' => 2, 'Visible' => 3, 'Ordre' => 4, 'DefaultValue' => 5, 'Code' => 6, 'Actif' => 7, 'Url' => 8, 'Groupe' => 9, 'TagDebutFinGroupe' => 10, 'Description' => 11, 'NbreTotalGroupe' => 12, 'LieuxExecution' => 13, 'DetailInfo' => 14, 'TypeInfo' => 15, 'AffichageInfos' => 16, 'AffichageMessageSupport' => 17, 'SelectionDepartementsParution' => 18, 'DepartementsParution' => 19, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'imageLogo' => 1, 'nom' => 2, 'visible' => 3, 'ordre' => 4, 'defaultValue' => 5, 'code' => 6, 'actif' => 7, 'url' => 8, 'groupe' => 9, 'tagDebutFinGroupe' => 10, 'description' => 11, 'nbreTotalGroupe' => 12, 'lieuxExecution' => 13, 'detailInfo' => 14, 'typeInfo' => 15, 'affichageInfos' => 16, 'affichageMessageSupport' => 17, 'selectionDepartementsParution' => 18, 'departementsParution' => 19, ),
        BasePeer::TYPE_COLNAME => array (CommonTSupportPublicationPeer::ID => 0, CommonTSupportPublicationPeer::IMAGE_LOGO => 1, CommonTSupportPublicationPeer::NOM => 2, CommonTSupportPublicationPeer::VISIBLE => 3, CommonTSupportPublicationPeer::ORDRE => 4, CommonTSupportPublicationPeer::DEFAULT_VALUE => 5, CommonTSupportPublicationPeer::CODE => 6, CommonTSupportPublicationPeer::ACTIF => 7, CommonTSupportPublicationPeer::URL => 8, CommonTSupportPublicationPeer::GROUPE => 9, CommonTSupportPublicationPeer::TAG_DEBUT_FIN_GROUPE => 10, CommonTSupportPublicationPeer::DESCRIPTION => 11, CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE => 12, CommonTSupportPublicationPeer::LIEUX_EXECUTION => 13, CommonTSupportPublicationPeer::DETAIL_INFO => 14, CommonTSupportPublicationPeer::TYPE_INFO => 15, CommonTSupportPublicationPeer::AFFICHAGE_INFOS => 16, CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT => 17, CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION => 18, CommonTSupportPublicationPeer::DEPARTEMENTS_PARUTION => 19, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'IMAGE_LOGO' => 1, 'NOM' => 2, 'VISIBLE' => 3, 'ORDRE' => 4, 'DEFAULT_VALUE' => 5, 'CODE' => 6, 'ACTIF' => 7, 'URL' => 8, 'GROUPE' => 9, 'TAG_DEBUT_FIN_GROUPE' => 10, 'DESCRIPTION' => 11, 'NBRE_TOTAL_GROUPE' => 12, 'LIEUX_EXECUTION' => 13, 'DETAIL_INFO' => 14, 'TYPE_INFO' => 15, 'AFFICHAGE_INFOS' => 16, 'AFFICHAGE_MESSAGE_SUPPORT' => 17, 'SELECTION_DEPARTEMENTS_PARUTION' => 18, 'DEPARTEMENTS_PARUTION' => 19, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'image_logo' => 1, 'nom' => 2, 'visible' => 3, 'ordre' => 4, 'default_value' => 5, 'code' => 6, 'actif' => 7, 'url' => 8, 'groupe' => 9, 'tag_debut_fin_groupe' => 10, 'description' => 11, 'nbre_total_groupe' => 12, 'lieux_execution' => 13, 'detail_info' => 14, 'type_info' => 15, 'affichage_infos' => 16, 'affichage_message_support' => 17, 'selection_departements_parution' => 18, 'departements_parution' => 19, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTSupportPublicationPeer::VISIBLE => array(
            CommonTSupportPublicationPeer::VISIBLE_0,
            CommonTSupportPublicationPeer::VISIBLE_1,
        ),
        CommonTSupportPublicationPeer::DEFAULT_VALUE => array(
            CommonTSupportPublicationPeer::DEFAULT_VALUE_0,
            CommonTSupportPublicationPeer::DEFAULT_VALUE_1,
        ),
        CommonTSupportPublicationPeer::ACTIF => array(
            CommonTSupportPublicationPeer::ACTIF_0,
            CommonTSupportPublicationPeer::ACTIF_1,
        ),
        CommonTSupportPublicationPeer::AFFICHAGE_INFOS => array(
            CommonTSupportPublicationPeer::AFFICHAGE_INFOS_0,
            CommonTSupportPublicationPeer::AFFICHAGE_INFOS_1,
        ),
        CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT => array(
            CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT_0,
            CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT_1,
        ),
        CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION => array(
            CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION_0,
            CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTSupportPublicationPeer::getFieldNames($toType);
        $key = isset(CommonTSupportPublicationPeer::$fieldKeys[$fromType][$name]) ? CommonTSupportPublicationPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTSupportPublicationPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTSupportPublicationPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTSupportPublicationPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTSupportPublicationPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTSupportPublicationPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTSupportPublicationPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTSupportPublicationPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTSupportPublicationPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::ID);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::IMAGE_LOGO);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::NOM);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::VISIBLE);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::ORDRE);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::DEFAULT_VALUE);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::CODE);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::ACTIF);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::URL);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::GROUPE);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::TAG_DEBUT_FIN_GROUPE);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::DESCRIPTION);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::LIEUX_EXECUTION);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::DETAIL_INFO);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::TYPE_INFO);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::AFFICHAGE_INFOS);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION);
            $criteria->addSelectColumn(CommonTSupportPublicationPeer::DEPARTEMENTS_PARUTION);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.image_logo');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.visible');
            $criteria->addSelectColumn($alias . '.ordre');
            $criteria->addSelectColumn($alias . '.default_value');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.groupe');
            $criteria->addSelectColumn($alias . '.tag_debut_fin_groupe');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.nbre_total_groupe');
            $criteria->addSelectColumn($alias . '.lieux_execution');
            $criteria->addSelectColumn($alias . '.detail_info');
            $criteria->addSelectColumn($alias . '.type_info');
            $criteria->addSelectColumn($alias . '.affichage_infos');
            $criteria->addSelectColumn($alias . '.affichage_message_support');
            $criteria->addSelectColumn($alias . '.selection_departements_parution');
            $criteria->addSelectColumn($alias . '.departements_parution');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTSupportPublicationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTSupportPublicationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTSupportPublicationPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTSupportPublication
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTSupportPublicationPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTSupportPublicationPeer::populateObjects(CommonTSupportPublicationPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTSupportPublicationPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTSupportPublicationPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTSupportPublication $obj A CommonTSupportPublication object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTSupportPublicationPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTSupportPublication object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTSupportPublication) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTSupportPublication object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTSupportPublicationPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTSupportPublication Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTSupportPublicationPeer::$instances[$key])) {
                return CommonTSupportPublicationPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTSupportPublicationPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTSupportPublicationPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_support_publication
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in CommonTOffreSupportPublicitePeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonTOffreSupportPublicitePeer::clearInstancePool();
        // Invalidate objects in CommonTSupportAnnonceConsultationPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonTSupportAnnonceConsultationPeer::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTSupportPublicationPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTSupportPublicationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTSupportPublicationPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTSupportPublicationPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTSupportPublication object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTSupportPublicationPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTSupportPublicationPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTSupportPublicationPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTSupportPublicationPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTSupportPublicationPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTSupportPublicationPeer::DATABASE_NAME)->getTable(CommonTSupportPublicationPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTSupportPublicationPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTSupportPublicationPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTSupportPublicationTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTSupportPublicationPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTSupportPublication or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTSupportPublication object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTSupportPublication object
        }

        if ($criteria->containsKey(CommonTSupportPublicationPeer::ID) && $criteria->keyContainsValue(CommonTSupportPublicationPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTSupportPublicationPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTSupportPublicationPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTSupportPublication or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTSupportPublication object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTSupportPublicationPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTSupportPublicationPeer::ID);
            $value = $criteria->remove(CommonTSupportPublicationPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTSupportPublicationPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTSupportPublicationPeer::TABLE_NAME);
            }

        } else { // $values is CommonTSupportPublication object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTSupportPublicationPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_support_publication table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += CommonTSupportPublicationPeer::doOnDeleteCascade(new Criteria(CommonTSupportPublicationPeer::DATABASE_NAME), $con);
            $affectedRows += BasePeer::doDeleteAll(CommonTSupportPublicationPeer::TABLE_NAME, $con, CommonTSupportPublicationPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTSupportPublicationPeer::clearInstancePool();
            CommonTSupportPublicationPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTSupportPublication or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTSupportPublication object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTSupportPublication) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTSupportPublicationPeer::DATABASE_NAME);
            $criteria->add(CommonTSupportPublicationPeer::ID, (array) $values, Criteria::IN);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTSupportPublicationPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += CommonTSupportPublicationPeer::doOnDeleteCascade($c, $con);

            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            if ($values instanceof Criteria) {
                CommonTSupportPublicationPeer::clearInstancePool();
            } elseif ($values instanceof CommonTSupportPublication) { // it's a model object
                CommonTSupportPublicationPeer::removeInstanceFromPool($values);
            } else { // it's a primary key, or an array of pks
                foreach ((array) $values as $singleval) {
                    CommonTSupportPublicationPeer::removeInstanceFromPool($singleval);
                }
            }

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTSupportPublicationPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected static function doOnDeleteCascade(Criteria $criteria, PropelPDO $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $criteria
        $objects = CommonTSupportPublicationPeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {


            // delete related CommonTOffreSupportPublicite objects
            $criteria = new Criteria(CommonTOffreSupportPublicitePeer::DATABASE_NAME);

            $criteria->add(CommonTOffreSupportPublicitePeer::ID_SUPPORT, $obj->getId());
            $affectedRows += CommonTOffreSupportPublicitePeer::doDelete($criteria, $con);

            // delete related CommonTSupportAnnonceConsultation objects
            $criteria = new Criteria(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

            $criteria->add(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, $obj->getId());
            $affectedRows += CommonTSupportAnnonceConsultationPeer::doDelete($criteria, $con);
        }

        return $affectedRows;
    }

    /**
     * Validates all modified columns of given CommonTSupportPublication object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTSupportPublication $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTSupportPublicationPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTSupportPublicationPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTSupportPublicationPeer::DATABASE_NAME, CommonTSupportPublicationPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTSupportPublication
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTSupportPublicationPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTSupportPublicationPeer::DATABASE_NAME);
        $criteria->add(CommonTSupportPublicationPeer::ID, $pk);

        $v = CommonTSupportPublicationPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTSupportPublication[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTSupportPublicationPeer::DATABASE_NAME);
            $criteria->add(CommonTSupportPublicationPeer::ID, $pks, Criteria::IN);
            $objs = CommonTSupportPublicationPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTSupportPublicationPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTSupportPublicationPeer::buildTableMap();

