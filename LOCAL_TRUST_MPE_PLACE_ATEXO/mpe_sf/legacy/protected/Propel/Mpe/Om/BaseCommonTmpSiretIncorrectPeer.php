<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonTmpSiretIncorrect;
use Application\Propel\Mpe\CommonTmpSiretIncorrectPeer;
use Application\Propel\Mpe\Map\CommonTmpSiretIncorrectTableMap;

/**
 * Base static class for performing query and update operations on the 'tmp_siret_incorrect' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTmpSiretIncorrectPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'tmp_siret_incorrect';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTmpSiretIncorrect';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTmpSiretIncorrectTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 8;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 8;

    /** the column name for the id field */
    const ID = 'tmp_siret_incorrect.id';

    /** the column name for the old_id_inscrit field */
    const OLD_ID_INSCRIT = 'tmp_siret_incorrect.old_id_inscrit';

    /** the column name for the email field */
    const EMAIL = 'tmp_siret_incorrect.email';

    /** the column name for the siret field */
    const SIRET = 'tmp_siret_incorrect.siret';

    /** the column name for the is_send_message field */
    const IS_SEND_MESSAGE = 'tmp_siret_incorrect.is_send_message';

    /** the column name for the send_date field */
    const SEND_DATE = 'tmp_siret_incorrect.send_date';

    /** the column name for the create_date field */
    const CREATE_DATE = 'tmp_siret_incorrect.create_date';

    /** the column name for the id_inscrit field */
    const ID_INSCRIT = 'tmp_siret_incorrect.id_inscrit';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTmpSiretIncorrect objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTmpSiretIncorrect[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTmpSiretIncorrectPeer::$fieldNames[CommonTmpSiretIncorrectPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'OldIdInscrit', 'Email', 'Siret', 'IsSendMessage', 'SendDate', 'CreateDate', 'IdInscrit', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'oldIdInscrit', 'email', 'siret', 'isSendMessage', 'sendDate', 'createDate', 'idInscrit', ),
        BasePeer::TYPE_COLNAME => array (CommonTmpSiretIncorrectPeer::ID, CommonTmpSiretIncorrectPeer::OLD_ID_INSCRIT, CommonTmpSiretIncorrectPeer::EMAIL, CommonTmpSiretIncorrectPeer::SIRET, CommonTmpSiretIncorrectPeer::IS_SEND_MESSAGE, CommonTmpSiretIncorrectPeer::SEND_DATE, CommonTmpSiretIncorrectPeer::CREATE_DATE, CommonTmpSiretIncorrectPeer::ID_INSCRIT, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'OLD_ID_INSCRIT', 'EMAIL', 'SIRET', 'IS_SEND_MESSAGE', 'SEND_DATE', 'CREATE_DATE', 'ID_INSCRIT', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'old_id_inscrit', 'email', 'siret', 'is_send_message', 'send_date', 'create_date', 'id_inscrit', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTmpSiretIncorrectPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'OldIdInscrit' => 1, 'Email' => 2, 'Siret' => 3, 'IsSendMessage' => 4, 'SendDate' => 5, 'CreateDate' => 6, 'IdInscrit' => 7, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'oldIdInscrit' => 1, 'email' => 2, 'siret' => 3, 'isSendMessage' => 4, 'sendDate' => 5, 'createDate' => 6, 'idInscrit' => 7, ),
        BasePeer::TYPE_COLNAME => array (CommonTmpSiretIncorrectPeer::ID => 0, CommonTmpSiretIncorrectPeer::OLD_ID_INSCRIT => 1, CommonTmpSiretIncorrectPeer::EMAIL => 2, CommonTmpSiretIncorrectPeer::SIRET => 3, CommonTmpSiretIncorrectPeer::IS_SEND_MESSAGE => 4, CommonTmpSiretIncorrectPeer::SEND_DATE => 5, CommonTmpSiretIncorrectPeer::CREATE_DATE => 6, CommonTmpSiretIncorrectPeer::ID_INSCRIT => 7, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'OLD_ID_INSCRIT' => 1, 'EMAIL' => 2, 'SIRET' => 3, 'IS_SEND_MESSAGE' => 4, 'SEND_DATE' => 5, 'CREATE_DATE' => 6, 'ID_INSCRIT' => 7, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'old_id_inscrit' => 1, 'email' => 2, 'siret' => 3, 'is_send_message' => 4, 'send_date' => 5, 'create_date' => 6, 'id_inscrit' => 7, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTmpSiretIncorrectPeer::getFieldNames($toType);
        $key = isset(CommonTmpSiretIncorrectPeer::$fieldKeys[$fromType][$name]) ? CommonTmpSiretIncorrectPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTmpSiretIncorrectPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTmpSiretIncorrectPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTmpSiretIncorrectPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTmpSiretIncorrectPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTmpSiretIncorrectPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTmpSiretIncorrectPeer::ID);
            $criteria->addSelectColumn(CommonTmpSiretIncorrectPeer::OLD_ID_INSCRIT);
            $criteria->addSelectColumn(CommonTmpSiretIncorrectPeer::EMAIL);
            $criteria->addSelectColumn(CommonTmpSiretIncorrectPeer::SIRET);
            $criteria->addSelectColumn(CommonTmpSiretIncorrectPeer::IS_SEND_MESSAGE);
            $criteria->addSelectColumn(CommonTmpSiretIncorrectPeer::SEND_DATE);
            $criteria->addSelectColumn(CommonTmpSiretIncorrectPeer::CREATE_DATE);
            $criteria->addSelectColumn(CommonTmpSiretIncorrectPeer::ID_INSCRIT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.old_id_inscrit');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.siret');
            $criteria->addSelectColumn($alias . '.is_send_message');
            $criteria->addSelectColumn($alias . '.send_date');
            $criteria->addSelectColumn($alias . '.create_date');
            $criteria->addSelectColumn($alias . '.id_inscrit');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTmpSiretIncorrectPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTmpSiretIncorrectPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTmpSiretIncorrectPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTmpSiretIncorrect
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTmpSiretIncorrectPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTmpSiretIncorrectPeer::populateObjects(CommonTmpSiretIncorrectPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTmpSiretIncorrectPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTmpSiretIncorrectPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTmpSiretIncorrect $obj A CommonTmpSiretIncorrect object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTmpSiretIncorrectPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTmpSiretIncorrect object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTmpSiretIncorrect) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTmpSiretIncorrect object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTmpSiretIncorrectPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTmpSiretIncorrect Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTmpSiretIncorrectPeer::$instances[$key])) {
                return CommonTmpSiretIncorrectPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTmpSiretIncorrectPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTmpSiretIncorrectPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to tmp_siret_incorrect
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTmpSiretIncorrectPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTmpSiretIncorrectPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTmpSiretIncorrectPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTmpSiretIncorrectPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTmpSiretIncorrect object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTmpSiretIncorrectPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTmpSiretIncorrectPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTmpSiretIncorrectPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTmpSiretIncorrectPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTmpSiretIncorrectPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonInscrit table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonInscrit(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTmpSiretIncorrectPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTmpSiretIncorrectPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTmpSiretIncorrectPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTmpSiretIncorrectPeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTmpSiretIncorrect objects pre-filled with their CommonInscrit objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTmpSiretIncorrect objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonInscrit(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTmpSiretIncorrectPeer::DATABASE_NAME);
        }

        CommonTmpSiretIncorrectPeer::addSelectColumns($criteria);
        $startcol = CommonTmpSiretIncorrectPeer::NUM_HYDRATE_COLUMNS;
        CommonInscritPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTmpSiretIncorrectPeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTmpSiretIncorrectPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTmpSiretIncorrectPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTmpSiretIncorrectPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTmpSiretIncorrectPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonInscritPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonInscritPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonInscritPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTmpSiretIncorrect) to $obj2 (CommonInscrit)
                $obj2->addCommonTmpSiretIncorrect($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTmpSiretIncorrectPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTmpSiretIncorrectPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTmpSiretIncorrectPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTmpSiretIncorrectPeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTmpSiretIncorrect objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTmpSiretIncorrect objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTmpSiretIncorrectPeer::DATABASE_NAME);
        }

        CommonTmpSiretIncorrectPeer::addSelectColumns($criteria);
        $startcol2 = CommonTmpSiretIncorrectPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTmpSiretIncorrectPeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTmpSiretIncorrectPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTmpSiretIncorrectPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTmpSiretIncorrectPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTmpSiretIncorrectPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonInscrit rows

            $key2 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonInscritPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonInscritPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonInscritPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTmpSiretIncorrect) to the collection in $obj2 (CommonInscrit)
                $obj2->addCommonTmpSiretIncorrect($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTmpSiretIncorrectPeer::DATABASE_NAME)->getTable(CommonTmpSiretIncorrectPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTmpSiretIncorrectPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTmpSiretIncorrectPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTmpSiretIncorrectTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTmpSiretIncorrectPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTmpSiretIncorrect or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTmpSiretIncorrect object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTmpSiretIncorrect object
        }

        if ($criteria->containsKey(CommonTmpSiretIncorrectPeer::ID) && $criteria->keyContainsValue(CommonTmpSiretIncorrectPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTmpSiretIncorrectPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTmpSiretIncorrectPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTmpSiretIncorrect or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTmpSiretIncorrect object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTmpSiretIncorrectPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTmpSiretIncorrectPeer::ID);
            $value = $criteria->remove(CommonTmpSiretIncorrectPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTmpSiretIncorrectPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTmpSiretIncorrectPeer::TABLE_NAME);
            }

        } else { // $values is CommonTmpSiretIncorrect object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTmpSiretIncorrectPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the tmp_siret_incorrect table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTmpSiretIncorrectPeer::TABLE_NAME, $con, CommonTmpSiretIncorrectPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTmpSiretIncorrectPeer::clearInstancePool();
            CommonTmpSiretIncorrectPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTmpSiretIncorrect or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTmpSiretIncorrect object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTmpSiretIncorrectPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTmpSiretIncorrect) { // it's a model object
            // invalidate the cache for this single object
            CommonTmpSiretIncorrectPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTmpSiretIncorrectPeer::DATABASE_NAME);
            $criteria->add(CommonTmpSiretIncorrectPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTmpSiretIncorrectPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTmpSiretIncorrectPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTmpSiretIncorrectPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTmpSiretIncorrect object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTmpSiretIncorrect $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTmpSiretIncorrectPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTmpSiretIncorrectPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTmpSiretIncorrectPeer::DATABASE_NAME, CommonTmpSiretIncorrectPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTmpSiretIncorrect
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTmpSiretIncorrectPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTmpSiretIncorrectPeer::DATABASE_NAME);
        $criteria->add(CommonTmpSiretIncorrectPeer::ID, $pk);

        $v = CommonTmpSiretIncorrectPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTmpSiretIncorrect[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTmpSiretIncorrectPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTmpSiretIncorrectPeer::DATABASE_NAME);
            $criteria->add(CommonTmpSiretIncorrectPeer::ID, $pks, Criteria::IN);
            $objs = CommonTmpSiretIncorrectPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTmpSiretIncorrectPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTmpSiretIncorrectPeer::buildTableMap();

