<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Propel\Mpe\Map\CommonTEtablissementTableMap;

/**
 * Base static class for performing query and update operations on the 't_etablissement' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTEtablissementPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_etablissement';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTEtablissement';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTEtablissementTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 23;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 23;

    /** the column name for the id_etablissement field */
    const ID_ETABLISSEMENT = 't_etablissement.id_etablissement';

    /** the column name for the id_entreprise field */
    const ID_ENTREPRISE = 't_etablissement.id_entreprise';

    /** the column name for the code_etablissement field */
    const CODE_ETABLISSEMENT = 't_etablissement.code_etablissement';

    /** the column name for the est_siege field */
    const EST_SIEGE = 't_etablissement.est_siege';

    /** the column name for the adresse field */
    const ADRESSE = 't_etablissement.adresse';

    /** the column name for the adresse2 field */
    const ADRESSE2 = 't_etablissement.adresse2';

    /** the column name for the code_postal field */
    const CODE_POSTAL = 't_etablissement.code_postal';

    /** the column name for the ville field */
    const VILLE = 't_etablissement.ville';

    /** the column name for the pays field */
    const PAYS = 't_etablissement.pays';

    /** the column name for the saisie_manuelle field */
    const SAISIE_MANUELLE = 't_etablissement.saisie_manuelle';

    /** the column name for the id_initial field */
    const ID_INITIAL = 't_etablissement.id_initial';

    /** the column name for the date_creation field */
    const DATE_CREATION = 't_etablissement.date_creation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 't_etablissement.date_modification';

    /** the column name for the date_suppression field */
    const DATE_SUPPRESSION = 't_etablissement.date_suppression';

    /** the column name for the statut_actif field */
    const STATUT_ACTIF = 't_etablissement.statut_actif';

    /** the column name for the inscrit_annuaire_defense field */
    const INSCRIT_ANNUAIRE_DEFENSE = 't_etablissement.inscrit_annuaire_defense';

    /** the column name for the long field */
    const LONG = 't_etablissement.long';

    /** the column name for the lat field */
    const LAT = 't_etablissement.lat';

    /** the column name for the maj_long_lat field */
    const MAJ_LONG_LAT = 't_etablissement.maj_long_lat';

    /** the column name for the tva_intracommunautaire field */
    const TVA_INTRACOMMUNAUTAIRE = 't_etablissement.tva_intracommunautaire';

    /** the column name for the etat_administratif field */
    const ETAT_ADMINISTRATIF = 't_etablissement.etat_administratif';

    /** the column name for the date_fermeture field */
    const DATE_FERMETURE = 't_etablissement.date_fermeture';

    /** the column name for the id_externe field */
    const ID_EXTERNE = 't_etablissement.id_externe';

    /** The enumerated values for the est_siege field */
    const EST_SIEGE_0 = '0';
    const EST_SIEGE_1 = '1';

    /** The enumerated values for the saisie_manuelle field */
    const SAISIE_MANUELLE_0 = '0';
    const SAISIE_MANUELLE_1 = '1';

    /** The enumerated values for the statut_actif field */
    const STATUT_ACTIF_0 = '0';
    const STATUT_ACTIF_1 = '1';

    /** The enumerated values for the inscrit_annuaire_defense field */
    const INSCRIT_ANNUAIRE_DEFENSE_0 = '0';
    const INSCRIT_ANNUAIRE_DEFENSE_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTEtablissement objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTEtablissement[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTEtablissementPeer::$fieldNames[CommonTEtablissementPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdEtablissement', 'IdEntreprise', 'CodeEtablissement', 'EstSiege', 'Adresse', 'Adresse2', 'CodePostal', 'Ville', 'Pays', 'SaisieManuelle', 'IdInitial', 'DateCreation', 'DateModification', 'DateSuppression', 'StatutActif', 'InscritAnnuaireDefense', 'Long', 'Lat', 'MajLongLat', 'TvaIntracommunautaire', 'EtatAdministratif', 'DateFermeture', 'IdExterne', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idEtablissement', 'idEntreprise', 'codeEtablissement', 'estSiege', 'adresse', 'adresse2', 'codePostal', 'ville', 'pays', 'saisieManuelle', 'idInitial', 'dateCreation', 'dateModification', 'dateSuppression', 'statutActif', 'inscritAnnuaireDefense', 'long', 'lat', 'majLongLat', 'tvaIntracommunautaire', 'etatAdministratif', 'dateFermeture', 'idExterne', ),
        BasePeer::TYPE_COLNAME => array (CommonTEtablissementPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ENTREPRISE, CommonTEtablissementPeer::CODE_ETABLISSEMENT, CommonTEtablissementPeer::EST_SIEGE, CommonTEtablissementPeer::ADRESSE, CommonTEtablissementPeer::ADRESSE2, CommonTEtablissementPeer::CODE_POSTAL, CommonTEtablissementPeer::VILLE, CommonTEtablissementPeer::PAYS, CommonTEtablissementPeer::SAISIE_MANUELLE, CommonTEtablissementPeer::ID_INITIAL, CommonTEtablissementPeer::DATE_CREATION, CommonTEtablissementPeer::DATE_MODIFICATION, CommonTEtablissementPeer::DATE_SUPPRESSION, CommonTEtablissementPeer::STATUT_ACTIF, CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE, CommonTEtablissementPeer::LONG, CommonTEtablissementPeer::LAT, CommonTEtablissementPeer::MAJ_LONG_LAT, CommonTEtablissementPeer::TVA_INTRACOMMUNAUTAIRE, CommonTEtablissementPeer::ETAT_ADMINISTRATIF, CommonTEtablissementPeer::DATE_FERMETURE, CommonTEtablissementPeer::ID_EXTERNE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_ETABLISSEMENT', 'ID_ENTREPRISE', 'CODE_ETABLISSEMENT', 'EST_SIEGE', 'ADRESSE', 'ADRESSE2', 'CODE_POSTAL', 'VILLE', 'PAYS', 'SAISIE_MANUELLE', 'ID_INITIAL', 'DATE_CREATION', 'DATE_MODIFICATION', 'DATE_SUPPRESSION', 'STATUT_ACTIF', 'INSCRIT_ANNUAIRE_DEFENSE', 'LONG', 'LAT', 'MAJ_LONG_LAT', 'TVA_INTRACOMMUNAUTAIRE', 'ETAT_ADMINISTRATIF', 'DATE_FERMETURE', 'ID_EXTERNE', ),
        BasePeer::TYPE_FIELDNAME => array ('id_etablissement', 'id_entreprise', 'code_etablissement', 'est_siege', 'adresse', 'adresse2', 'code_postal', 'ville', 'pays', 'saisie_manuelle', 'id_initial', 'date_creation', 'date_modification', 'date_suppression', 'statut_actif', 'inscrit_annuaire_defense', 'long', 'lat', 'maj_long_lat', 'tva_intracommunautaire', 'etat_administratif', 'date_fermeture', 'id_externe', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTEtablissementPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdEtablissement' => 0, 'IdEntreprise' => 1, 'CodeEtablissement' => 2, 'EstSiege' => 3, 'Adresse' => 4, 'Adresse2' => 5, 'CodePostal' => 6, 'Ville' => 7, 'Pays' => 8, 'SaisieManuelle' => 9, 'IdInitial' => 10, 'DateCreation' => 11, 'DateModification' => 12, 'DateSuppression' => 13, 'StatutActif' => 14, 'InscritAnnuaireDefense' => 15, 'Long' => 16, 'Lat' => 17, 'MajLongLat' => 18, 'TvaIntracommunautaire' => 19, 'EtatAdministratif' => 20, 'DateFermeture' => 21, 'IdExterne' => 22, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idEtablissement' => 0, 'idEntreprise' => 1, 'codeEtablissement' => 2, 'estSiege' => 3, 'adresse' => 4, 'adresse2' => 5, 'codePostal' => 6, 'ville' => 7, 'pays' => 8, 'saisieManuelle' => 9, 'idInitial' => 10, 'dateCreation' => 11, 'dateModification' => 12, 'dateSuppression' => 13, 'statutActif' => 14, 'inscritAnnuaireDefense' => 15, 'long' => 16, 'lat' => 17, 'majLongLat' => 18, 'tvaIntracommunautaire' => 19, 'etatAdministratif' => 20, 'dateFermeture' => 21, 'idExterne' => 22, ),
        BasePeer::TYPE_COLNAME => array (CommonTEtablissementPeer::ID_ETABLISSEMENT => 0, CommonTEtablissementPeer::ID_ENTREPRISE => 1, CommonTEtablissementPeer::CODE_ETABLISSEMENT => 2, CommonTEtablissementPeer::EST_SIEGE => 3, CommonTEtablissementPeer::ADRESSE => 4, CommonTEtablissementPeer::ADRESSE2 => 5, CommonTEtablissementPeer::CODE_POSTAL => 6, CommonTEtablissementPeer::VILLE => 7, CommonTEtablissementPeer::PAYS => 8, CommonTEtablissementPeer::SAISIE_MANUELLE => 9, CommonTEtablissementPeer::ID_INITIAL => 10, CommonTEtablissementPeer::DATE_CREATION => 11, CommonTEtablissementPeer::DATE_MODIFICATION => 12, CommonTEtablissementPeer::DATE_SUPPRESSION => 13, CommonTEtablissementPeer::STATUT_ACTIF => 14, CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE => 15, CommonTEtablissementPeer::LONG => 16, CommonTEtablissementPeer::LAT => 17, CommonTEtablissementPeer::MAJ_LONG_LAT => 18, CommonTEtablissementPeer::TVA_INTRACOMMUNAUTAIRE => 19, CommonTEtablissementPeer::ETAT_ADMINISTRATIF => 20, CommonTEtablissementPeer::DATE_FERMETURE => 21, CommonTEtablissementPeer::ID_EXTERNE => 22, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_ETABLISSEMENT' => 0, 'ID_ENTREPRISE' => 1, 'CODE_ETABLISSEMENT' => 2, 'EST_SIEGE' => 3, 'ADRESSE' => 4, 'ADRESSE2' => 5, 'CODE_POSTAL' => 6, 'VILLE' => 7, 'PAYS' => 8, 'SAISIE_MANUELLE' => 9, 'ID_INITIAL' => 10, 'DATE_CREATION' => 11, 'DATE_MODIFICATION' => 12, 'DATE_SUPPRESSION' => 13, 'STATUT_ACTIF' => 14, 'INSCRIT_ANNUAIRE_DEFENSE' => 15, 'LONG' => 16, 'LAT' => 17, 'MAJ_LONG_LAT' => 18, 'TVA_INTRACOMMUNAUTAIRE' => 19, 'ETAT_ADMINISTRATIF' => 20, 'DATE_FERMETURE' => 21, 'ID_EXTERNE' => 22, ),
        BasePeer::TYPE_FIELDNAME => array ('id_etablissement' => 0, 'id_entreprise' => 1, 'code_etablissement' => 2, 'est_siege' => 3, 'adresse' => 4, 'adresse2' => 5, 'code_postal' => 6, 'ville' => 7, 'pays' => 8, 'saisie_manuelle' => 9, 'id_initial' => 10, 'date_creation' => 11, 'date_modification' => 12, 'date_suppression' => 13, 'statut_actif' => 14, 'inscrit_annuaire_defense' => 15, 'long' => 16, 'lat' => 17, 'maj_long_lat' => 18, 'tva_intracommunautaire' => 19, 'etat_administratif' => 20, 'date_fermeture' => 21, 'id_externe' => 22, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTEtablissementPeer::EST_SIEGE => array(
            CommonTEtablissementPeer::EST_SIEGE_0,
            CommonTEtablissementPeer::EST_SIEGE_1,
        ),
        CommonTEtablissementPeer::SAISIE_MANUELLE => array(
            CommonTEtablissementPeer::SAISIE_MANUELLE_0,
            CommonTEtablissementPeer::SAISIE_MANUELLE_1,
        ),
        CommonTEtablissementPeer::STATUT_ACTIF => array(
            CommonTEtablissementPeer::STATUT_ACTIF_0,
            CommonTEtablissementPeer::STATUT_ACTIF_1,
        ),
        CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE => array(
            CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE_0,
            CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTEtablissementPeer::getFieldNames($toType);
        $key = isset(CommonTEtablissementPeer::$fieldKeys[$fromType][$name]) ? CommonTEtablissementPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTEtablissementPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTEtablissementPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTEtablissementPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTEtablissementPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTEtablissementPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTEtablissementPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTEtablissementPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTEtablissementPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTEtablissementPeer::ID_ETABLISSEMENT);
            $criteria->addSelectColumn(CommonTEtablissementPeer::ID_ENTREPRISE);
            $criteria->addSelectColumn(CommonTEtablissementPeer::CODE_ETABLISSEMENT);
            $criteria->addSelectColumn(CommonTEtablissementPeer::EST_SIEGE);
            $criteria->addSelectColumn(CommonTEtablissementPeer::ADRESSE);
            $criteria->addSelectColumn(CommonTEtablissementPeer::ADRESSE2);
            $criteria->addSelectColumn(CommonTEtablissementPeer::CODE_POSTAL);
            $criteria->addSelectColumn(CommonTEtablissementPeer::VILLE);
            $criteria->addSelectColumn(CommonTEtablissementPeer::PAYS);
            $criteria->addSelectColumn(CommonTEtablissementPeer::SAISIE_MANUELLE);
            $criteria->addSelectColumn(CommonTEtablissementPeer::ID_INITIAL);
            $criteria->addSelectColumn(CommonTEtablissementPeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonTEtablissementPeer::DATE_MODIFICATION);
            $criteria->addSelectColumn(CommonTEtablissementPeer::DATE_SUPPRESSION);
            $criteria->addSelectColumn(CommonTEtablissementPeer::STATUT_ACTIF);
            $criteria->addSelectColumn(CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE);
            $criteria->addSelectColumn(CommonTEtablissementPeer::LONG);
            $criteria->addSelectColumn(CommonTEtablissementPeer::LAT);
            $criteria->addSelectColumn(CommonTEtablissementPeer::MAJ_LONG_LAT);
            $criteria->addSelectColumn(CommonTEtablissementPeer::TVA_INTRACOMMUNAUTAIRE);
            $criteria->addSelectColumn(CommonTEtablissementPeer::ETAT_ADMINISTRATIF);
            $criteria->addSelectColumn(CommonTEtablissementPeer::DATE_FERMETURE);
            $criteria->addSelectColumn(CommonTEtablissementPeer::ID_EXTERNE);
        } else {
            $criteria->addSelectColumn($alias . '.id_etablissement');
            $criteria->addSelectColumn($alias . '.id_entreprise');
            $criteria->addSelectColumn($alias . '.code_etablissement');
            $criteria->addSelectColumn($alias . '.est_siege');
            $criteria->addSelectColumn($alias . '.adresse');
            $criteria->addSelectColumn($alias . '.adresse2');
            $criteria->addSelectColumn($alias . '.code_postal');
            $criteria->addSelectColumn($alias . '.ville');
            $criteria->addSelectColumn($alias . '.pays');
            $criteria->addSelectColumn($alias . '.saisie_manuelle');
            $criteria->addSelectColumn($alias . '.id_initial');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_modification');
            $criteria->addSelectColumn($alias . '.date_suppression');
            $criteria->addSelectColumn($alias . '.statut_actif');
            $criteria->addSelectColumn($alias . '.inscrit_annuaire_defense');
            $criteria->addSelectColumn($alias . '.long');
            $criteria->addSelectColumn($alias . '.lat');
            $criteria->addSelectColumn($alias . '.maj_long_lat');
            $criteria->addSelectColumn($alias . '.tva_intracommunautaire');
            $criteria->addSelectColumn($alias . '.etat_administratif');
            $criteria->addSelectColumn($alias . '.date_fermeture');
            $criteria->addSelectColumn($alias . '.id_externe');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTEtablissementPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTEtablissementPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTEtablissementPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTEtablissement
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTEtablissementPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTEtablissementPeer::populateObjects(CommonTEtablissementPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTEtablissementPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTEtablissementPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTEtablissement $obj A CommonTEtablissement object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getIdEtablissement();
            } // if key === null
            CommonTEtablissementPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTEtablissement object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTEtablissement) {
                $key = (string) $value->getIdEtablissement();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTEtablissement object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTEtablissementPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTEtablissement Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTEtablissementPeer::$instances[$key])) {
                return CommonTEtablissementPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTEtablissementPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTEtablissementPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_etablissement
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTEtablissementPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTEtablissementPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTEtablissementPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTEtablissement object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTEtablissementPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTEtablissementPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTEtablissementPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related Entreprise table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinEntreprise(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTEtablissementPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTEtablissementPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTEtablissementPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTEtablissementPeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTEtablissement objects pre-filled with their Entreprise objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTEtablissement objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTEtablissementPeer::DATABASE_NAME);
        }

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol = CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;
        EntreprisePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTEtablissementPeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTEtablissementPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTEtablissementPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTEtablissementPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTEtablissement) to $obj2 (Entreprise)
                $obj2->addCommonTEtablissement($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTEtablissementPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTEtablissementPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTEtablissementPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTEtablissementPeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTEtablissement objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTEtablissement objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTEtablissementPeer::DATABASE_NAME);
        }

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol2 = CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTEtablissementPeer::ID_ENTREPRISE, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTEtablissementPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTEtablissementPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTEtablissementPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Entreprise rows

            $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTEtablissement) to the collection in $obj2 (Entreprise)
                $obj2->addCommonTEtablissement($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTEtablissementPeer::DATABASE_NAME)->getTable(CommonTEtablissementPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTEtablissementPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTEtablissementPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTEtablissementTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTEtablissementPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTEtablissement or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTEtablissement object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTEtablissement object
        }

        if ($criteria->containsKey(CommonTEtablissementPeer::ID_ETABLISSEMENT) && $criteria->keyContainsValue(CommonTEtablissementPeer::ID_ETABLISSEMENT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTEtablissementPeer::ID_ETABLISSEMENT.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTEtablissementPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTEtablissement or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTEtablissement object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTEtablissementPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTEtablissementPeer::ID_ETABLISSEMENT);
            $value = $criteria->remove(CommonTEtablissementPeer::ID_ETABLISSEMENT);
            if ($value) {
                $selectCriteria->add(CommonTEtablissementPeer::ID_ETABLISSEMENT, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTEtablissementPeer::TABLE_NAME);
            }

        } else { // $values is CommonTEtablissement object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTEtablissementPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_etablissement table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTEtablissementPeer::TABLE_NAME, $con, CommonTEtablissementPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTEtablissementPeer::clearInstancePool();
            CommonTEtablissementPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTEtablissement or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTEtablissement object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTEtablissementPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTEtablissement) { // it's a model object
            // invalidate the cache for this single object
            CommonTEtablissementPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTEtablissementPeer::DATABASE_NAME);
            $criteria->add(CommonTEtablissementPeer::ID_ETABLISSEMENT, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTEtablissementPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTEtablissementPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTEtablissementPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTEtablissement object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTEtablissement $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTEtablissementPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTEtablissementPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTEtablissementPeer::DATABASE_NAME, CommonTEtablissementPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTEtablissement
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTEtablissementPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTEtablissementPeer::DATABASE_NAME);
        $criteria->add(CommonTEtablissementPeer::ID_ETABLISSEMENT, $pk);

        $v = CommonTEtablissementPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTEtablissement[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTEtablissementPeer::DATABASE_NAME);
            $criteria->add(CommonTEtablissementPeer::ID_ETABLISSEMENT, $pks, Criteria::IN);
            $objs = CommonTEtablissementPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTEtablissementPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTEtablissementPeer::buildTableMap();

