<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDispositifAnnonce;
use Application\Propel\Mpe\CommonTDispositifAnnoncePeer;
use Application\Propel\Mpe\CommonTDispositifAnnonceQuery;

/**
 * Base class that represents a query for the 't_dispositif_annonce' table.
 *
 *
 *
 * @method CommonTDispositifAnnonceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTDispositifAnnonceQuery orderByLibelle($order = Criteria::ASC) Order by the libelle column
 * @method CommonTDispositifAnnonceQuery orderByIdExterne($order = Criteria::ASC) Order by the id_externe column
 * @method CommonTDispositifAnnonceQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method CommonTDispositifAnnonceQuery orderByVisible($order = Criteria::ASC) Order by the visible column
 * @method CommonTDispositifAnnonceQuery orderByIdDispositifRectificatif($order = Criteria::ASC) Order by the id_dispositif_rectificatif column
 * @method CommonTDispositifAnnonceQuery orderByIdDispositifAnnulation($order = Criteria::ASC) Order by the id_dispositif_annulation column
 * @method CommonTDispositifAnnonceQuery orderBySigle($order = Criteria::ASC) Order by the sigle column
 * @method CommonTDispositifAnnonceQuery orderByIdTypeAvisRectificatif($order = Criteria::ASC) Order by the id_type_avis_rectificatif column
 * @method CommonTDispositifAnnonceQuery orderByIdTypeAvisAnnulation($order = Criteria::ASC) Order by the id_type_avis_annulation column
 * @method CommonTDispositifAnnonceQuery orderByLibelleFormulairePropose($order = Criteria::ASC) Order by the libelle_formulaire_propose column
 *
 * @method CommonTDispositifAnnonceQuery groupById() Group by the id column
 * @method CommonTDispositifAnnonceQuery groupByLibelle() Group by the libelle column
 * @method CommonTDispositifAnnonceQuery groupByIdExterne() Group by the id_externe column
 * @method CommonTDispositifAnnonceQuery groupByType() Group by the type column
 * @method CommonTDispositifAnnonceQuery groupByVisible() Group by the visible column
 * @method CommonTDispositifAnnonceQuery groupByIdDispositifRectificatif() Group by the id_dispositif_rectificatif column
 * @method CommonTDispositifAnnonceQuery groupByIdDispositifAnnulation() Group by the id_dispositif_annulation column
 * @method CommonTDispositifAnnonceQuery groupBySigle() Group by the sigle column
 * @method CommonTDispositifAnnonceQuery groupByIdTypeAvisRectificatif() Group by the id_type_avis_rectificatif column
 * @method CommonTDispositifAnnonceQuery groupByIdTypeAvisAnnulation() Group by the id_type_avis_annulation column
 * @method CommonTDispositifAnnonceQuery groupByLibelleFormulairePropose() Group by the libelle_formulaire_propose column
 *
 * @method CommonTDispositifAnnonceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTDispositifAnnonceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTDispositifAnnonceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTDispositifAnnonce findOne(PropelPDO $con = null) Return the first CommonTDispositifAnnonce matching the query
 * @method CommonTDispositifAnnonce findOneOrCreate(PropelPDO $con = null) Return the first CommonTDispositifAnnonce matching the query, or a new CommonTDispositifAnnonce object populated from the query conditions when no match is found
 *
 * @method CommonTDispositifAnnonce findOneByLibelle(string $libelle) Return the first CommonTDispositifAnnonce filtered by the libelle column
 * @method CommonTDispositifAnnonce findOneByIdExterne(int $id_externe) Return the first CommonTDispositifAnnonce filtered by the id_externe column
 * @method CommonTDispositifAnnonce findOneByType(int $type) Return the first CommonTDispositifAnnonce filtered by the type column
 * @method CommonTDispositifAnnonce findOneByVisible(string $visible) Return the first CommonTDispositifAnnonce filtered by the visible column
 * @method CommonTDispositifAnnonce findOneByIdDispositifRectificatif(int $id_dispositif_rectificatif) Return the first CommonTDispositifAnnonce filtered by the id_dispositif_rectificatif column
 * @method CommonTDispositifAnnonce findOneByIdDispositifAnnulation(int $id_dispositif_annulation) Return the first CommonTDispositifAnnonce filtered by the id_dispositif_annulation column
 * @method CommonTDispositifAnnonce findOneBySigle(string $sigle) Return the first CommonTDispositifAnnonce filtered by the sigle column
 * @method CommonTDispositifAnnonce findOneByIdTypeAvisRectificatif(int $id_type_avis_rectificatif) Return the first CommonTDispositifAnnonce filtered by the id_type_avis_rectificatif column
 * @method CommonTDispositifAnnonce findOneByIdTypeAvisAnnulation(int $id_type_avis_annulation) Return the first CommonTDispositifAnnonce filtered by the id_type_avis_annulation column
 * @method CommonTDispositifAnnonce findOneByLibelleFormulairePropose(string $libelle_formulaire_propose) Return the first CommonTDispositifAnnonce filtered by the libelle_formulaire_propose column
 *
 * @method array findById(int $id) Return CommonTDispositifAnnonce objects filtered by the id column
 * @method array findByLibelle(string $libelle) Return CommonTDispositifAnnonce objects filtered by the libelle column
 * @method array findByIdExterne(int $id_externe) Return CommonTDispositifAnnonce objects filtered by the id_externe column
 * @method array findByType(int $type) Return CommonTDispositifAnnonce objects filtered by the type column
 * @method array findByVisible(string $visible) Return CommonTDispositifAnnonce objects filtered by the visible column
 * @method array findByIdDispositifRectificatif(int $id_dispositif_rectificatif) Return CommonTDispositifAnnonce objects filtered by the id_dispositif_rectificatif column
 * @method array findByIdDispositifAnnulation(int $id_dispositif_annulation) Return CommonTDispositifAnnonce objects filtered by the id_dispositif_annulation column
 * @method array findBySigle(string $sigle) Return CommonTDispositifAnnonce objects filtered by the sigle column
 * @method array findByIdTypeAvisRectificatif(int $id_type_avis_rectificatif) Return CommonTDispositifAnnonce objects filtered by the id_type_avis_rectificatif column
 * @method array findByIdTypeAvisAnnulation(int $id_type_avis_annulation) Return CommonTDispositifAnnonce objects filtered by the id_type_avis_annulation column
 * @method array findByLibelleFormulairePropose(string $libelle_formulaire_propose) Return CommonTDispositifAnnonce objects filtered by the libelle_formulaire_propose column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDispositifAnnonceQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTDispositifAnnonceQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTDispositifAnnonce', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTDispositifAnnonceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTDispositifAnnonceQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTDispositifAnnonceQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTDispositifAnnonceQuery) {
            return $criteria;
        }
        $query = new CommonTDispositifAnnonceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTDispositifAnnonce|CommonTDispositifAnnonce[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTDispositifAnnoncePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTDispositifAnnoncePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDispositifAnnonce A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDispositifAnnonce A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `libelle`, `id_externe`, `type`, `visible`, `id_dispositif_rectificatif`, `id_dispositif_annulation`, `sigle`, `id_type_avis_rectificatif`, `id_type_avis_annulation`, `libelle_formulaire_propose` FROM `t_dispositif_annonce` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTDispositifAnnonce();
            $obj->hydrate($row);
            CommonTDispositifAnnoncePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTDispositifAnnonce|CommonTDispositifAnnonce[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTDispositifAnnonce[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the libelle column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelle('fooValue');   // WHERE libelle = 'fooValue'
     * $query->filterByLibelle('%fooValue%'); // WHERE libelle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByLibelle($libelle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelle)) {
                $libelle = str_replace('*', '%', $libelle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::LIBELLE, $libelle, $comparison);
    }

    /**
     * Filter the query on the id_externe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdExterne(1234); // WHERE id_externe = 1234
     * $query->filterByIdExterne(array(12, 34)); // WHERE id_externe IN (12, 34)
     * $query->filterByIdExterne(array('min' => 12)); // WHERE id_externe >= 12
     * $query->filterByIdExterne(array('max' => 12)); // WHERE id_externe <= 12
     * </code>
     *
     * @param     mixed $idExterne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByIdExterne($idExterne = null, $comparison = null)
    {
        if (is_array($idExterne)) {
            $useMinMax = false;
            if (isset($idExterne['min'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_EXTERNE, $idExterne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idExterne['max'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_EXTERNE, $idExterne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_EXTERNE, $idExterne, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType(1234); // WHERE type = 1234
     * $query->filterByType(array(12, 34)); // WHERE type IN (12, 34)
     * $query->filterByType(array('min' => 12)); // WHERE type >= 12
     * $query->filterByType(array('max' => 12)); // WHERE type <= 12
     * </code>
     *
     * @param     mixed $type The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (is_array($type)) {
            $useMinMax = false;
            if (isset($type['min'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::TYPE, $type['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($type['max'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::TYPE, $type['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the visible column
     *
     * Example usage:
     * <code>
     * $query->filterByVisible('fooValue');   // WHERE visible = 'fooValue'
     * $query->filterByVisible('%fooValue%'); // WHERE visible LIKE '%fooValue%'
     * </code>
     *
     * @param     string $visible The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByVisible($visible = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($visible)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $visible)) {
                $visible = str_replace('*', '%', $visible);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::VISIBLE, $visible, $comparison);
    }

    /**
     * Filter the query on the id_dispositif_rectificatif column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDispositifRectificatif(1234); // WHERE id_dispositif_rectificatif = 1234
     * $query->filterByIdDispositifRectificatif(array(12, 34)); // WHERE id_dispositif_rectificatif IN (12, 34)
     * $query->filterByIdDispositifRectificatif(array('min' => 12)); // WHERE id_dispositif_rectificatif >= 12
     * $query->filterByIdDispositifRectificatif(array('max' => 12)); // WHERE id_dispositif_rectificatif <= 12
     * </code>
     *
     * @param     mixed $idDispositifRectificatif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByIdDispositifRectificatif($idDispositifRectificatif = null, $comparison = null)
    {
        if (is_array($idDispositifRectificatif)) {
            $useMinMax = false;
            if (isset($idDispositifRectificatif['min'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_RECTIFICATIF, $idDispositifRectificatif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDispositifRectificatif['max'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_RECTIFICATIF, $idDispositifRectificatif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_RECTIFICATIF, $idDispositifRectificatif, $comparison);
    }

    /**
     * Filter the query on the id_dispositif_annulation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDispositifAnnulation(1234); // WHERE id_dispositif_annulation = 1234
     * $query->filterByIdDispositifAnnulation(array(12, 34)); // WHERE id_dispositif_annulation IN (12, 34)
     * $query->filterByIdDispositifAnnulation(array('min' => 12)); // WHERE id_dispositif_annulation >= 12
     * $query->filterByIdDispositifAnnulation(array('max' => 12)); // WHERE id_dispositif_annulation <= 12
     * </code>
     *
     * @param     mixed $idDispositifAnnulation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByIdDispositifAnnulation($idDispositifAnnulation = null, $comparison = null)
    {
        if (is_array($idDispositifAnnulation)) {
            $useMinMax = false;
            if (isset($idDispositifAnnulation['min'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_ANNULATION, $idDispositifAnnulation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDispositifAnnulation['max'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_ANNULATION, $idDispositifAnnulation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_DISPOSITIF_ANNULATION, $idDispositifAnnulation, $comparison);
    }

    /**
     * Filter the query on the sigle column
     *
     * Example usage:
     * <code>
     * $query->filterBySigle('fooValue');   // WHERE sigle = 'fooValue'
     * $query->filterBySigle('%fooValue%'); // WHERE sigle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sigle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterBySigle($sigle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sigle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sigle)) {
                $sigle = str_replace('*', '%', $sigle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::SIGLE, $sigle, $comparison);
    }

    /**
     * Filter the query on the id_type_avis_rectificatif column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeAvisRectificatif(1234); // WHERE id_type_avis_rectificatif = 1234
     * $query->filterByIdTypeAvisRectificatif(array(12, 34)); // WHERE id_type_avis_rectificatif IN (12, 34)
     * $query->filterByIdTypeAvisRectificatif(array('min' => 12)); // WHERE id_type_avis_rectificatif >= 12
     * $query->filterByIdTypeAvisRectificatif(array('max' => 12)); // WHERE id_type_avis_rectificatif <= 12
     * </code>
     *
     * @param     mixed $idTypeAvisRectificatif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByIdTypeAvisRectificatif($idTypeAvisRectificatif = null, $comparison = null)
    {
        if (is_array($idTypeAvisRectificatif)) {
            $useMinMax = false;
            if (isset($idTypeAvisRectificatif['min'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_RECTIFICATIF, $idTypeAvisRectificatif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeAvisRectificatif['max'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_RECTIFICATIF, $idTypeAvisRectificatif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_RECTIFICATIF, $idTypeAvisRectificatif, $comparison);
    }

    /**
     * Filter the query on the id_type_avis_annulation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeAvisAnnulation(1234); // WHERE id_type_avis_annulation = 1234
     * $query->filterByIdTypeAvisAnnulation(array(12, 34)); // WHERE id_type_avis_annulation IN (12, 34)
     * $query->filterByIdTypeAvisAnnulation(array('min' => 12)); // WHERE id_type_avis_annulation >= 12
     * $query->filterByIdTypeAvisAnnulation(array('max' => 12)); // WHERE id_type_avis_annulation <= 12
     * </code>
     *
     * @param     mixed $idTypeAvisAnnulation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByIdTypeAvisAnnulation($idTypeAvisAnnulation = null, $comparison = null)
    {
        if (is_array($idTypeAvisAnnulation)) {
            $useMinMax = false;
            if (isset($idTypeAvisAnnulation['min'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_ANNULATION, $idTypeAvisAnnulation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeAvisAnnulation['max'])) {
                $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_ANNULATION, $idTypeAvisAnnulation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID_TYPE_AVIS_ANNULATION, $idTypeAvisAnnulation, $comparison);
    }

    /**
     * Filter the query on the libelle_formulaire_propose column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleFormulairePropose('fooValue');   // WHERE libelle_formulaire_propose = 'fooValue'
     * $query->filterByLibelleFormulairePropose('%fooValue%'); // WHERE libelle_formulaire_propose LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleFormulairePropose The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function filterByLibelleFormulairePropose($libelleFormulairePropose = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleFormulairePropose)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleFormulairePropose)) {
                $libelleFormulairePropose = str_replace('*', '%', $libelleFormulairePropose);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDispositifAnnoncePeer::LIBELLE_FORMULAIRE_PROPOSE, $libelleFormulairePropose, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTDispositifAnnonce $commonTDispositifAnnonce Object to remove from the list of results
     *
     * @return CommonTDispositifAnnonceQuery The current query, for fluid interface
     */
    public function prune($commonTDispositifAnnonce = null)
    {
        if ($commonTDispositifAnnonce) {
            $this->addUsingAlias(CommonTDispositifAnnoncePeer::ID, $commonTDispositifAnnonce->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
