<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonPanierEntreprisePeer;
use Application\Propel\Mpe\CommonPrestationPeer;
use Application\Propel\Mpe\CommonResponsableengagementPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Propel\Mpe\Map\EntrepriseTableMap;

/**
 * Base static class for performing query and update operations on the 'Entreprise' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseEntreprisePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'Entreprise';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\Entreprise';

    /** the related TableMap class for this table */
    const TM_CLASS = 'EntrepriseTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 104;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 104;

    /** the column name for the id field */
    const ID = 'Entreprise.id';

    /** the column name for the admin_id field */
    const ADMIN_ID = 'Entreprise.admin_id';

    /** the column name for the siren field */
    const SIREN = 'Entreprise.siren';

    /** the column name for the repmetiers field */
    const REPMETIERS = 'Entreprise.repmetiers';

    /** the column name for the nom field */
    const NOM = 'Entreprise.nom';

    /** the column name for the adresse field */
    const ADRESSE = 'Entreprise.adresse';

    /** the column name for the codepostal field */
    const CODEPOSTAL = 'Entreprise.codepostal';

    /** the column name for the villeadresse field */
    const VILLEADRESSE = 'Entreprise.villeadresse';

    /** the column name for the paysadresse field */
    const PAYSADRESSE = 'Entreprise.paysadresse';

    /** the column name for the email field */
    const EMAIL = 'Entreprise.email';

    /** the column name for the taille field */
    const TAILLE = 'Entreprise.taille';

    /** the column name for the formejuridique field */
    const FORMEJURIDIQUE = 'Entreprise.formejuridique';

    /** the column name for the villeenregistrement field */
    const VILLEENREGISTREMENT = 'Entreprise.villeenregistrement';

    /** the column name for the motifNonIndNum field */
    const MOTIFNONINDNUM = 'Entreprise.motifNonIndNum';

    /** the column name for the ordreProfOuAgrement field */
    const ORDREPROFOUAGREMENT = 'Entreprise.ordreProfOuAgrement';

    /** the column name for the dateConstSociete field */
    const DATECONSTSOCIETE = 'Entreprise.dateConstSociete';

    /** the column name for the nomOrgInscription field */
    const NOMORGINSCRIPTION = 'Entreprise.nomOrgInscription';

    /** the column name for the adrOrgInscription field */
    const ADRORGINSCRIPTION = 'Entreprise.adrOrgInscription';

    /** the column name for the dateConstAssoc field */
    const DATECONSTASSOC = 'Entreprise.dateConstAssoc';

    /** the column name for the dateConstAssocEtrangere field */
    const DATECONSTASSOCETRANGERE = 'Entreprise.dateConstAssocEtrangere';

    /** the column name for the nomPersonnePublique field */
    const NOMPERSONNEPUBLIQUE = 'Entreprise.nomPersonnePublique';

    /** the column name for the nationalite field */
    const NATIONALITE = 'Entreprise.nationalite';

    /** the column name for the redressement field */
    const REDRESSEMENT = 'Entreprise.redressement';

    /** the column name for the paysenregistrement field */
    const PAYSENREGISTREMENT = 'Entreprise.paysenregistrement';

    /** the column name for the sirenEtranger field */
    const SIRENETRANGER = 'Entreprise.sirenEtranger';

    /** the column name for the numAssoEtrangere field */
    const NUMASSOETRANGERE = 'Entreprise.numAssoEtrangere';

    /** the column name for the debutExerciceGlob1 field */
    const DEBUTEXERCICEGLOB1 = 'Entreprise.debutExerciceGlob1';

    /** the column name for the finExerciceGlob1 field */
    const FINEXERCICEGLOB1 = 'Entreprise.finExerciceGlob1';

    /** the column name for the debutExerciceGlob2 field */
    const DEBUTEXERCICEGLOB2 = 'Entreprise.debutExerciceGlob2';

    /** the column name for the finExerciceGlob2 field */
    const FINEXERCICEGLOB2 = 'Entreprise.finExerciceGlob2';

    /** the column name for the debutExerciceGlob3 field */
    const DEBUTEXERCICEGLOB3 = 'Entreprise.debutExerciceGlob3';

    /** the column name for the finExerciceGlob3 field */
    const FINEXERCICEGLOB3 = 'Entreprise.finExerciceGlob3';

    /** the column name for the ventesGlob1 field */
    const VENTESGLOB1 = 'Entreprise.ventesGlob1';

    /** the column name for the ventesGlob2 field */
    const VENTESGLOB2 = 'Entreprise.ventesGlob2';

    /** the column name for the ventesGlob3 field */
    const VENTESGLOB3 = 'Entreprise.ventesGlob3';

    /** the column name for the biensGlob1 field */
    const BIENSGLOB1 = 'Entreprise.biensGlob1';

    /** the column name for the biensGlob2 field */
    const BIENSGLOB2 = 'Entreprise.biensGlob2';

    /** the column name for the biensGlob3 field */
    const BIENSGLOB3 = 'Entreprise.biensGlob3';

    /** the column name for the servicesGlob1 field */
    const SERVICESGLOB1 = 'Entreprise.servicesGlob1';

    /** the column name for the servicesGlob2 field */
    const SERVICESGLOB2 = 'Entreprise.servicesGlob2';

    /** the column name for the servicesGlob3 field */
    const SERVICESGLOB3 = 'Entreprise.servicesGlob3';

    /** the column name for the totalGlob1 field */
    const TOTALGLOB1 = 'Entreprise.totalGlob1';

    /** the column name for the totalGlob2 field */
    const TOTALGLOB2 = 'Entreprise.totalGlob2';

    /** the column name for the totalGlob3 field */
    const TOTALGLOB3 = 'Entreprise.totalGlob3';

    /** the column name for the codeape field */
    const CODEAPE = 'Entreprise.codeape';

    /** the column name for the libelle_ape field */
    const LIBELLE_APE = 'Entreprise.libelle_ape';

    /** the column name for the origine_compte field */
    const ORIGINE_COMPTE = 'Entreprise.origine_compte';

    /** the column name for the telephone field */
    const TELEPHONE = 'Entreprise.telephone';

    /** the column name for the fax field */
    const FAX = 'Entreprise.fax';

    /** the column name for the site_internet field */
    const SITE_INTERNET = 'Entreprise.site_internet';

    /** the column name for the description_activite field */
    const DESCRIPTION_ACTIVITE = 'Entreprise.description_activite';

    /** the column name for the activite_domaine_defense field */
    const ACTIVITE_DOMAINE_DEFENSE = 'Entreprise.activite_domaine_defense';

    /** the column name for the annee_cloture_exercice1 field */
    const ANNEE_CLOTURE_EXERCICE1 = 'Entreprise.annee_cloture_exercice1';

    /** the column name for the annee_cloture_exercice2 field */
    const ANNEE_CLOTURE_EXERCICE2 = 'Entreprise.annee_cloture_exercice2';

    /** the column name for the annee_cloture_exercice3 field */
    const ANNEE_CLOTURE_EXERCICE3 = 'Entreprise.annee_cloture_exercice3';

    /** the column name for the effectif_moyen1 field */
    const EFFECTIF_MOYEN1 = 'Entreprise.effectif_moyen1';

    /** the column name for the effectif_moyen2 field */
    const EFFECTIF_MOYEN2 = 'Entreprise.effectif_moyen2';

    /** the column name for the effectif_moyen3 field */
    const EFFECTIF_MOYEN3 = 'Entreprise.effectif_moyen3';

    /** the column name for the effectif_encadrement1 field */
    const EFFECTIF_ENCADREMENT1 = 'Entreprise.effectif_encadrement1';

    /** the column name for the effectif_encadrement2 field */
    const EFFECTIF_ENCADREMENT2 = 'Entreprise.effectif_encadrement2';

    /** the column name for the effectif_encadrement3 field */
    const EFFECTIF_ENCADREMENT3 = 'Entreprise.effectif_encadrement3';

    /** the column name for the pme1 field */
    const PME1 = 'Entreprise.pme1';

    /** the column name for the pme2 field */
    const PME2 = 'Entreprise.pme2';

    /** the column name for the pme3 field */
    const PME3 = 'Entreprise.pme3';

    /** the column name for the adresse2 field */
    const ADRESSE2 = 'Entreprise.adresse2';

    /** the column name for the nicSiege field */
    const NICSIEGE = 'Entreprise.nicSiege';

    /** the column name for the acronyme_pays field */
    const ACRONYME_PAYS = 'Entreprise.acronyme_pays';

    /** the column name for the date_creation field */
    const DATE_CREATION = 'Entreprise.date_creation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 'Entreprise.date_modification';

    /** the column name for the id_initial field */
    const ID_INITIAL = 'Entreprise.id_initial';

    /** the column name for the region field */
    const REGION = 'Entreprise.region';

    /** the column name for the province field */
    const PROVINCE = 'Entreprise.province';

    /** the column name for the telephone2 field */
    const TELEPHONE2 = 'Entreprise.telephone2';

    /** the column name for the telephone3 field */
    const TELEPHONE3 = 'Entreprise.telephone3';

    /** the column name for the cnss field */
    const CNSS = 'Entreprise.cnss';

    /** the column name for the rc_num field */
    const RC_NUM = 'Entreprise.rc_num';

    /** the column name for the rc_ville field */
    const RC_VILLE = 'Entreprise.rc_ville';

    /** the column name for the domaines_activites field */
    const DOMAINES_ACTIVITES = 'Entreprise.domaines_activites';

    /** the column name for the num_tax field */
    const NUM_TAX = 'Entreprise.num_tax';

    /** the column name for the documents_commerciaux field */
    const DOCUMENTS_COMMERCIAUX = 'Entreprise.documents_commerciaux';

    /** the column name for the intitule_documents_commerciaux field */
    const INTITULE_DOCUMENTS_COMMERCIAUX = 'Entreprise.intitule_documents_commerciaux';

    /** the column name for the taille_documents_commerciaux field */
    const TAILLE_DOCUMENTS_COMMERCIAUX = 'Entreprise.taille_documents_commerciaux';

    /** the column name for the qualification field */
    const QUALIFICATION = 'Entreprise.qualification';

    /** the column name for the agrement field */
    const AGREMENT = 'Entreprise.agrement';

    /** the column name for the moyens_technique field */
    const MOYENS_TECHNIQUE = 'Entreprise.moyens_technique';

    /** the column name for the moyens_humains field */
    const MOYENS_HUMAINS = 'Entreprise.moyens_humains';

    /** the column name for the compte_actif field */
    const COMPTE_ACTIF = 'Entreprise.compte_actif';

    /** the column name for the capital_social field */
    const CAPITAL_SOCIAL = 'Entreprise.capital_social';

    /** the column name for the ifu field */
    const IFU = 'Entreprise.ifu';

    /** the column name for the id_agent_createur field */
    const ID_AGENT_CREATEUR = 'Entreprise.id_agent_createur';

    /** the column name for the nom_agent field */
    const NOM_AGENT = 'Entreprise.nom_agent';

    /** the column name for the prenom_agent field */
    const PRENOM_AGENT = 'Entreprise.prenom_agent';

    /** the column name for the adresses_electroniques field */
    const ADRESSES_ELECTRONIQUES = 'Entreprise.adresses_electroniques';

    /** the column name for the visible_bourse field */
    const VISIBLE_BOURSE = 'Entreprise.visible_bourse';

    /** the column name for the type_collaboration field */
    const TYPE_COLLABORATION = 'Entreprise.type_collaboration';

    /** the column name for the entreprise_EA field */
    const ENTREPRISE_EA = 'Entreprise.entreprise_EA';

    /** the column name for the entreprise_SIAE field */
    const ENTREPRISE_SIAE = 'Entreprise.entreprise_SIAE';

    /** the column name for the saisie_manuelle field */
    const SAISIE_MANUELLE = 'Entreprise.saisie_manuelle';

    /** the column name for the created_from_decision field */
    const CREATED_FROM_DECISION = 'Entreprise.created_from_decision';

    /** the column name for the id_code_effectif field */
    const ID_CODE_EFFECTIF = 'Entreprise.id_code_effectif';

    /** the column name for the categorie_entreprise field */
    const CATEGORIE_ENTREPRISE = 'Entreprise.categorie_entreprise';

    /** the column name for the etat_administratif field */
    const ETAT_ADMINISTRATIF = 'Entreprise.etat_administratif';

    /** the column name for the date_cessation field */
    const DATE_CESSATION = 'Entreprise.date_cessation';

    /** the column name for the id_externe field */
    const ID_EXTERNE = 'Entreprise.id_externe';

    /** The enumerated values for the pme1 field */
    const PME1_1 = '1';
    const PME1_0 = '0';

    /** The enumerated values for the pme2 field */
    const PME2_1 = '1';
    const PME2_0 = '0';

    /** The enumerated values for the pme3 field */
    const PME3_1 = '1';
    const PME3_0 = '0';

    /** The enumerated values for the entreprise_EA field */
    const ENTREPRISE_EA_0 = '0';
    const ENTREPRISE_EA_1 = '1';

    /** The enumerated values for the entreprise_SIAE field */
    const ENTREPRISE_SIAE_0 = '0';
    const ENTREPRISE_SIAE_1 = '1';

    /** The enumerated values for the saisie_manuelle field */
    const SAISIE_MANUELLE_0 = '0';
    const SAISIE_MANUELLE_1 = '1';

    /** The enumerated values for the created_from_decision field */
    const CREATED_FROM_DECISION_0 = '0';
    const CREATED_FROM_DECISION_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of Entreprise objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Entreprise[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. EntreprisePeer::$fieldNames[EntreprisePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'AdminId', 'Siren', 'Repmetiers', 'Nom', 'Adresse', 'Codepostal', 'Villeadresse', 'Paysadresse', 'Email', 'Taille', 'Formejuridique', 'Villeenregistrement', 'Motifnonindnum', 'Ordreprofouagrement', 'Dateconstsociete', 'Nomorginscription', 'Adrorginscription', 'Dateconstassoc', 'Dateconstassocetrangere', 'Nompersonnepublique', 'Nationalite', 'Redressement', 'Paysenregistrement', 'Sirenetranger', 'Numassoetrangere', 'Debutexerciceglob1', 'Finexerciceglob1', 'Debutexerciceglob2', 'Finexerciceglob2', 'Debutexerciceglob3', 'Finexerciceglob3', 'Ventesglob1', 'Ventesglob2', 'Ventesglob3', 'Biensglob1', 'Biensglob2', 'Biensglob3', 'Servicesglob1', 'Servicesglob2', 'Servicesglob3', 'Totalglob1', 'Totalglob2', 'Totalglob3', 'Codeape', 'LibelleApe', 'OrigineCompte', 'Telephone', 'Fax', 'SiteInternet', 'DescriptionActivite', 'ActiviteDomaineDefense', 'AnneeClotureExercice1', 'AnneeClotureExercice2', 'AnneeClotureExercice3', 'EffectifMoyen1', 'EffectifMoyen2', 'EffectifMoyen3', 'EffectifEncadrement1', 'EffectifEncadrement2', 'EffectifEncadrement3', 'Pme1', 'Pme2', 'Pme3', 'Adresse2', 'Nicsiege', 'AcronymePays', 'DateCreation', 'DateModification', 'IdInitial', 'Region', 'Province', 'Telephone2', 'Telephone3', 'Cnss', 'RcNum', 'RcVille', 'DomainesActivites', 'NumTax', 'DocumentsCommerciaux', 'IntituleDocumentsCommerciaux', 'TailleDocumentsCommerciaux', 'Qualification', 'Agrement', 'MoyensTechnique', 'MoyensHumains', 'CompteActif', 'CapitalSocial', 'Ifu', 'IdAgentCreateur', 'NomAgent', 'PrenomAgent', 'AdressesElectroniques', 'VisibleBourse', 'TypeCollaboration', 'EntrepriseEa', 'EntrepriseSiae', 'SaisieManuelle', 'CreatedFromDecision', 'IdCodeEffectif', 'CategorieEntreprise', 'EtatAdministratif', 'DateCessation', 'IdExterne', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'adminId', 'siren', 'repmetiers', 'nom', 'adresse', 'codepostal', 'villeadresse', 'paysadresse', 'email', 'taille', 'formejuridique', 'villeenregistrement', 'motifnonindnum', 'ordreprofouagrement', 'dateconstsociete', 'nomorginscription', 'adrorginscription', 'dateconstassoc', 'dateconstassocetrangere', 'nompersonnepublique', 'nationalite', 'redressement', 'paysenregistrement', 'sirenetranger', 'numassoetrangere', 'debutexerciceglob1', 'finexerciceglob1', 'debutexerciceglob2', 'finexerciceglob2', 'debutexerciceglob3', 'finexerciceglob3', 'ventesglob1', 'ventesglob2', 'ventesglob3', 'biensglob1', 'biensglob2', 'biensglob3', 'servicesglob1', 'servicesglob2', 'servicesglob3', 'totalglob1', 'totalglob2', 'totalglob3', 'codeape', 'libelleApe', 'origineCompte', 'telephone', 'fax', 'siteInternet', 'descriptionActivite', 'activiteDomaineDefense', 'anneeClotureExercice1', 'anneeClotureExercice2', 'anneeClotureExercice3', 'effectifMoyen1', 'effectifMoyen2', 'effectifMoyen3', 'effectifEncadrement1', 'effectifEncadrement2', 'effectifEncadrement3', 'pme1', 'pme2', 'pme3', 'adresse2', 'nicsiege', 'acronymePays', 'dateCreation', 'dateModification', 'idInitial', 'region', 'province', 'telephone2', 'telephone3', 'cnss', 'rcNum', 'rcVille', 'domainesActivites', 'numTax', 'documentsCommerciaux', 'intituleDocumentsCommerciaux', 'tailleDocumentsCommerciaux', 'qualification', 'agrement', 'moyensTechnique', 'moyensHumains', 'compteActif', 'capitalSocial', 'ifu', 'idAgentCreateur', 'nomAgent', 'prenomAgent', 'adressesElectroniques', 'visibleBourse', 'typeCollaboration', 'entrepriseEa', 'entrepriseSiae', 'saisieManuelle', 'createdFromDecision', 'idCodeEffectif', 'categorieEntreprise', 'etatAdministratif', 'dateCessation', 'idExterne', ),
        BasePeer::TYPE_COLNAME => array (EntreprisePeer::ID, EntreprisePeer::ADMIN_ID, EntreprisePeer::SIREN, EntreprisePeer::REPMETIERS, EntreprisePeer::NOM, EntreprisePeer::ADRESSE, EntreprisePeer::CODEPOSTAL, EntreprisePeer::VILLEADRESSE, EntreprisePeer::PAYSADRESSE, EntreprisePeer::EMAIL, EntreprisePeer::TAILLE, EntreprisePeer::FORMEJURIDIQUE, EntreprisePeer::VILLEENREGISTREMENT, EntreprisePeer::MOTIFNONINDNUM, EntreprisePeer::ORDREPROFOUAGREMENT, EntreprisePeer::DATECONSTSOCIETE, EntreprisePeer::NOMORGINSCRIPTION, EntreprisePeer::ADRORGINSCRIPTION, EntreprisePeer::DATECONSTASSOC, EntreprisePeer::DATECONSTASSOCETRANGERE, EntreprisePeer::NOMPERSONNEPUBLIQUE, EntreprisePeer::NATIONALITE, EntreprisePeer::REDRESSEMENT, EntreprisePeer::PAYSENREGISTREMENT, EntreprisePeer::SIRENETRANGER, EntreprisePeer::NUMASSOETRANGERE, EntreprisePeer::DEBUTEXERCICEGLOB1, EntreprisePeer::FINEXERCICEGLOB1, EntreprisePeer::DEBUTEXERCICEGLOB2, EntreprisePeer::FINEXERCICEGLOB2, EntreprisePeer::DEBUTEXERCICEGLOB3, EntreprisePeer::FINEXERCICEGLOB3, EntreprisePeer::VENTESGLOB1, EntreprisePeer::VENTESGLOB2, EntreprisePeer::VENTESGLOB3, EntreprisePeer::BIENSGLOB1, EntreprisePeer::BIENSGLOB2, EntreprisePeer::BIENSGLOB3, EntreprisePeer::SERVICESGLOB1, EntreprisePeer::SERVICESGLOB2, EntreprisePeer::SERVICESGLOB3, EntreprisePeer::TOTALGLOB1, EntreprisePeer::TOTALGLOB2, EntreprisePeer::TOTALGLOB3, EntreprisePeer::CODEAPE, EntreprisePeer::LIBELLE_APE, EntreprisePeer::ORIGINE_COMPTE, EntreprisePeer::TELEPHONE, EntreprisePeer::FAX, EntreprisePeer::SITE_INTERNET, EntreprisePeer::DESCRIPTION_ACTIVITE, EntreprisePeer::ACTIVITE_DOMAINE_DEFENSE, EntreprisePeer::ANNEE_CLOTURE_EXERCICE1, EntreprisePeer::ANNEE_CLOTURE_EXERCICE2, EntreprisePeer::ANNEE_CLOTURE_EXERCICE3, EntreprisePeer::EFFECTIF_MOYEN1, EntreprisePeer::EFFECTIF_MOYEN2, EntreprisePeer::EFFECTIF_MOYEN3, EntreprisePeer::EFFECTIF_ENCADREMENT1, EntreprisePeer::EFFECTIF_ENCADREMENT2, EntreprisePeer::EFFECTIF_ENCADREMENT3, EntreprisePeer::PME1, EntreprisePeer::PME2, EntreprisePeer::PME3, EntreprisePeer::ADRESSE2, EntreprisePeer::NICSIEGE, EntreprisePeer::ACRONYME_PAYS, EntreprisePeer::DATE_CREATION, EntreprisePeer::DATE_MODIFICATION, EntreprisePeer::ID_INITIAL, EntreprisePeer::REGION, EntreprisePeer::PROVINCE, EntreprisePeer::TELEPHONE2, EntreprisePeer::TELEPHONE3, EntreprisePeer::CNSS, EntreprisePeer::RC_NUM, EntreprisePeer::RC_VILLE, EntreprisePeer::DOMAINES_ACTIVITES, EntreprisePeer::NUM_TAX, EntreprisePeer::DOCUMENTS_COMMERCIAUX, EntreprisePeer::INTITULE_DOCUMENTS_COMMERCIAUX, EntreprisePeer::TAILLE_DOCUMENTS_COMMERCIAUX, EntreprisePeer::QUALIFICATION, EntreprisePeer::AGREMENT, EntreprisePeer::MOYENS_TECHNIQUE, EntreprisePeer::MOYENS_HUMAINS, EntreprisePeer::COMPTE_ACTIF, EntreprisePeer::CAPITAL_SOCIAL, EntreprisePeer::IFU, EntreprisePeer::ID_AGENT_CREATEUR, EntreprisePeer::NOM_AGENT, EntreprisePeer::PRENOM_AGENT, EntreprisePeer::ADRESSES_ELECTRONIQUES, EntreprisePeer::VISIBLE_BOURSE, EntreprisePeer::TYPE_COLLABORATION, EntreprisePeer::ENTREPRISE_EA, EntreprisePeer::ENTREPRISE_SIAE, EntreprisePeer::SAISIE_MANUELLE, EntreprisePeer::CREATED_FROM_DECISION, EntreprisePeer::ID_CODE_EFFECTIF, EntreprisePeer::CATEGORIE_ENTREPRISE, EntreprisePeer::ETAT_ADMINISTRATIF, EntreprisePeer::DATE_CESSATION, EntreprisePeer::ID_EXTERNE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ADMIN_ID', 'SIREN', 'REPMETIERS', 'NOM', 'ADRESSE', 'CODEPOSTAL', 'VILLEADRESSE', 'PAYSADRESSE', 'EMAIL', 'TAILLE', 'FORMEJURIDIQUE', 'VILLEENREGISTREMENT', 'MOTIFNONINDNUM', 'ORDREPROFOUAGREMENT', 'DATECONSTSOCIETE', 'NOMORGINSCRIPTION', 'ADRORGINSCRIPTION', 'DATECONSTASSOC', 'DATECONSTASSOCETRANGERE', 'NOMPERSONNEPUBLIQUE', 'NATIONALITE', 'REDRESSEMENT', 'PAYSENREGISTREMENT', 'SIRENETRANGER', 'NUMASSOETRANGERE', 'DEBUTEXERCICEGLOB1', 'FINEXERCICEGLOB1', 'DEBUTEXERCICEGLOB2', 'FINEXERCICEGLOB2', 'DEBUTEXERCICEGLOB3', 'FINEXERCICEGLOB3', 'VENTESGLOB1', 'VENTESGLOB2', 'VENTESGLOB3', 'BIENSGLOB1', 'BIENSGLOB2', 'BIENSGLOB3', 'SERVICESGLOB1', 'SERVICESGLOB2', 'SERVICESGLOB3', 'TOTALGLOB1', 'TOTALGLOB2', 'TOTALGLOB3', 'CODEAPE', 'LIBELLE_APE', 'ORIGINE_COMPTE', 'TELEPHONE', 'FAX', 'SITE_INTERNET', 'DESCRIPTION_ACTIVITE', 'ACTIVITE_DOMAINE_DEFENSE', 'ANNEE_CLOTURE_EXERCICE1', 'ANNEE_CLOTURE_EXERCICE2', 'ANNEE_CLOTURE_EXERCICE3', 'EFFECTIF_MOYEN1', 'EFFECTIF_MOYEN2', 'EFFECTIF_MOYEN3', 'EFFECTIF_ENCADREMENT1', 'EFFECTIF_ENCADREMENT2', 'EFFECTIF_ENCADREMENT3', 'PME1', 'PME2', 'PME3', 'ADRESSE2', 'NICSIEGE', 'ACRONYME_PAYS', 'DATE_CREATION', 'DATE_MODIFICATION', 'ID_INITIAL', 'REGION', 'PROVINCE', 'TELEPHONE2', 'TELEPHONE3', 'CNSS', 'RC_NUM', 'RC_VILLE', 'DOMAINES_ACTIVITES', 'NUM_TAX', 'DOCUMENTS_COMMERCIAUX', 'INTITULE_DOCUMENTS_COMMERCIAUX', 'TAILLE_DOCUMENTS_COMMERCIAUX', 'QUALIFICATION', 'AGREMENT', 'MOYENS_TECHNIQUE', 'MOYENS_HUMAINS', 'COMPTE_ACTIF', 'CAPITAL_SOCIAL', 'IFU', 'ID_AGENT_CREATEUR', 'NOM_AGENT', 'PRENOM_AGENT', 'ADRESSES_ELECTRONIQUES', 'VISIBLE_BOURSE', 'TYPE_COLLABORATION', 'ENTREPRISE_EA', 'ENTREPRISE_SIAE', 'SAISIE_MANUELLE', 'CREATED_FROM_DECISION', 'ID_CODE_EFFECTIF', 'CATEGORIE_ENTREPRISE', 'ETAT_ADMINISTRATIF', 'DATE_CESSATION', 'ID_EXTERNE', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'admin_id', 'siren', 'repmetiers', 'nom', 'adresse', 'codepostal', 'villeadresse', 'paysadresse', 'email', 'taille', 'formejuridique', 'villeenregistrement', 'motifNonIndNum', 'ordreProfOuAgrement', 'dateConstSociete', 'nomOrgInscription', 'adrOrgInscription', 'dateConstAssoc', 'dateConstAssocEtrangere', 'nomPersonnePublique', 'nationalite', 'redressement', 'paysenregistrement', 'sirenEtranger', 'numAssoEtrangere', 'debutExerciceGlob1', 'finExerciceGlob1', 'debutExerciceGlob2', 'finExerciceGlob2', 'debutExerciceGlob3', 'finExerciceGlob3', 'ventesGlob1', 'ventesGlob2', 'ventesGlob3', 'biensGlob1', 'biensGlob2', 'biensGlob3', 'servicesGlob1', 'servicesGlob2', 'servicesGlob3', 'totalGlob1', 'totalGlob2', 'totalGlob3', 'codeape', 'libelle_ape', 'origine_compte', 'telephone', 'fax', 'site_internet', 'description_activite', 'activite_domaine_defense', 'annee_cloture_exercice1', 'annee_cloture_exercice2', 'annee_cloture_exercice3', 'effectif_moyen1', 'effectif_moyen2', 'effectif_moyen3', 'effectif_encadrement1', 'effectif_encadrement2', 'effectif_encadrement3', 'pme1', 'pme2', 'pme3', 'adresse2', 'nicSiege', 'acronyme_pays', 'date_creation', 'date_modification', 'id_initial', 'region', 'province', 'telephone2', 'telephone3', 'cnss', 'rc_num', 'rc_ville', 'domaines_activites', 'num_tax', 'documents_commerciaux', 'intitule_documents_commerciaux', 'taille_documents_commerciaux', 'qualification', 'agrement', 'moyens_technique', 'moyens_humains', 'compte_actif', 'capital_social', 'ifu', 'id_agent_createur', 'nom_agent', 'prenom_agent', 'adresses_electroniques', 'visible_bourse', 'type_collaboration', 'entreprise_EA', 'entreprise_SIAE', 'saisie_manuelle', 'created_from_decision', 'id_code_effectif', 'categorie_entreprise', 'etat_administratif', 'date_cessation', 'id_externe', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. EntreprisePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'AdminId' => 1, 'Siren' => 2, 'Repmetiers' => 3, 'Nom' => 4, 'Adresse' => 5, 'Codepostal' => 6, 'Villeadresse' => 7, 'Paysadresse' => 8, 'Email' => 9, 'Taille' => 10, 'Formejuridique' => 11, 'Villeenregistrement' => 12, 'Motifnonindnum' => 13, 'Ordreprofouagrement' => 14, 'Dateconstsociete' => 15, 'Nomorginscription' => 16, 'Adrorginscription' => 17, 'Dateconstassoc' => 18, 'Dateconstassocetrangere' => 19, 'Nompersonnepublique' => 20, 'Nationalite' => 21, 'Redressement' => 22, 'Paysenregistrement' => 23, 'Sirenetranger' => 24, 'Numassoetrangere' => 25, 'Debutexerciceglob1' => 26, 'Finexerciceglob1' => 27, 'Debutexerciceglob2' => 28, 'Finexerciceglob2' => 29, 'Debutexerciceglob3' => 30, 'Finexerciceglob3' => 31, 'Ventesglob1' => 32, 'Ventesglob2' => 33, 'Ventesglob3' => 34, 'Biensglob1' => 35, 'Biensglob2' => 36, 'Biensglob3' => 37, 'Servicesglob1' => 38, 'Servicesglob2' => 39, 'Servicesglob3' => 40, 'Totalglob1' => 41, 'Totalglob2' => 42, 'Totalglob3' => 43, 'Codeape' => 44, 'LibelleApe' => 45, 'OrigineCompte' => 46, 'Telephone' => 47, 'Fax' => 48, 'SiteInternet' => 49, 'DescriptionActivite' => 50, 'ActiviteDomaineDefense' => 51, 'AnneeClotureExercice1' => 52, 'AnneeClotureExercice2' => 53, 'AnneeClotureExercice3' => 54, 'EffectifMoyen1' => 55, 'EffectifMoyen2' => 56, 'EffectifMoyen3' => 57, 'EffectifEncadrement1' => 58, 'EffectifEncadrement2' => 59, 'EffectifEncadrement3' => 60, 'Pme1' => 61, 'Pme2' => 62, 'Pme3' => 63, 'Adresse2' => 64, 'Nicsiege' => 65, 'AcronymePays' => 66, 'DateCreation' => 67, 'DateModification' => 68, 'IdInitial' => 69, 'Region' => 70, 'Province' => 71, 'Telephone2' => 72, 'Telephone3' => 73, 'Cnss' => 74, 'RcNum' => 75, 'RcVille' => 76, 'DomainesActivites' => 77, 'NumTax' => 78, 'DocumentsCommerciaux' => 79, 'IntituleDocumentsCommerciaux' => 80, 'TailleDocumentsCommerciaux' => 81, 'Qualification' => 82, 'Agrement' => 83, 'MoyensTechnique' => 84, 'MoyensHumains' => 85, 'CompteActif' => 86, 'CapitalSocial' => 87, 'Ifu' => 88, 'IdAgentCreateur' => 89, 'NomAgent' => 90, 'PrenomAgent' => 91, 'AdressesElectroniques' => 92, 'VisibleBourse' => 93, 'TypeCollaboration' => 94, 'EntrepriseEa' => 95, 'EntrepriseSiae' => 96, 'SaisieManuelle' => 97, 'CreatedFromDecision' => 98, 'IdCodeEffectif' => 99, 'CategorieEntreprise' => 100, 'EtatAdministratif' => 101, 'DateCessation' => 102, 'IdExterne' => 103, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'adminId' => 1, 'siren' => 2, 'repmetiers' => 3, 'nom' => 4, 'adresse' => 5, 'codepostal' => 6, 'villeadresse' => 7, 'paysadresse' => 8, 'email' => 9, 'taille' => 10, 'formejuridique' => 11, 'villeenregistrement' => 12, 'motifnonindnum' => 13, 'ordreprofouagrement' => 14, 'dateconstsociete' => 15, 'nomorginscription' => 16, 'adrorginscription' => 17, 'dateconstassoc' => 18, 'dateconstassocetrangere' => 19, 'nompersonnepublique' => 20, 'nationalite' => 21, 'redressement' => 22, 'paysenregistrement' => 23, 'sirenetranger' => 24, 'numassoetrangere' => 25, 'debutexerciceglob1' => 26, 'finexerciceglob1' => 27, 'debutexerciceglob2' => 28, 'finexerciceglob2' => 29, 'debutexerciceglob3' => 30, 'finexerciceglob3' => 31, 'ventesglob1' => 32, 'ventesglob2' => 33, 'ventesglob3' => 34, 'biensglob1' => 35, 'biensglob2' => 36, 'biensglob3' => 37, 'servicesglob1' => 38, 'servicesglob2' => 39, 'servicesglob3' => 40, 'totalglob1' => 41, 'totalglob2' => 42, 'totalglob3' => 43, 'codeape' => 44, 'libelleApe' => 45, 'origineCompte' => 46, 'telephone' => 47, 'fax' => 48, 'siteInternet' => 49, 'descriptionActivite' => 50, 'activiteDomaineDefense' => 51, 'anneeClotureExercice1' => 52, 'anneeClotureExercice2' => 53, 'anneeClotureExercice3' => 54, 'effectifMoyen1' => 55, 'effectifMoyen2' => 56, 'effectifMoyen3' => 57, 'effectifEncadrement1' => 58, 'effectifEncadrement2' => 59, 'effectifEncadrement3' => 60, 'pme1' => 61, 'pme2' => 62, 'pme3' => 63, 'adresse2' => 64, 'nicsiege' => 65, 'acronymePays' => 66, 'dateCreation' => 67, 'dateModification' => 68, 'idInitial' => 69, 'region' => 70, 'province' => 71, 'telephone2' => 72, 'telephone3' => 73, 'cnss' => 74, 'rcNum' => 75, 'rcVille' => 76, 'domainesActivites' => 77, 'numTax' => 78, 'documentsCommerciaux' => 79, 'intituleDocumentsCommerciaux' => 80, 'tailleDocumentsCommerciaux' => 81, 'qualification' => 82, 'agrement' => 83, 'moyensTechnique' => 84, 'moyensHumains' => 85, 'compteActif' => 86, 'capitalSocial' => 87, 'ifu' => 88, 'idAgentCreateur' => 89, 'nomAgent' => 90, 'prenomAgent' => 91, 'adressesElectroniques' => 92, 'visibleBourse' => 93, 'typeCollaboration' => 94, 'entrepriseEa' => 95, 'entrepriseSiae' => 96, 'saisieManuelle' => 97, 'createdFromDecision' => 98, 'idCodeEffectif' => 99, 'categorieEntreprise' => 100, 'etatAdministratif' => 101, 'dateCessation' => 102, 'idExterne' => 103, ),
        BasePeer::TYPE_COLNAME => array (EntreprisePeer::ID => 0, EntreprisePeer::ADMIN_ID => 1, EntreprisePeer::SIREN => 2, EntreprisePeer::REPMETIERS => 3, EntreprisePeer::NOM => 4, EntreprisePeer::ADRESSE => 5, EntreprisePeer::CODEPOSTAL => 6, EntreprisePeer::VILLEADRESSE => 7, EntreprisePeer::PAYSADRESSE => 8, EntreprisePeer::EMAIL => 9, EntreprisePeer::TAILLE => 10, EntreprisePeer::FORMEJURIDIQUE => 11, EntreprisePeer::VILLEENREGISTREMENT => 12, EntreprisePeer::MOTIFNONINDNUM => 13, EntreprisePeer::ORDREPROFOUAGREMENT => 14, EntreprisePeer::DATECONSTSOCIETE => 15, EntreprisePeer::NOMORGINSCRIPTION => 16, EntreprisePeer::ADRORGINSCRIPTION => 17, EntreprisePeer::DATECONSTASSOC => 18, EntreprisePeer::DATECONSTASSOCETRANGERE => 19, EntreprisePeer::NOMPERSONNEPUBLIQUE => 20, EntreprisePeer::NATIONALITE => 21, EntreprisePeer::REDRESSEMENT => 22, EntreprisePeer::PAYSENREGISTREMENT => 23, EntreprisePeer::SIRENETRANGER => 24, EntreprisePeer::NUMASSOETRANGERE => 25, EntreprisePeer::DEBUTEXERCICEGLOB1 => 26, EntreprisePeer::FINEXERCICEGLOB1 => 27, EntreprisePeer::DEBUTEXERCICEGLOB2 => 28, EntreprisePeer::FINEXERCICEGLOB2 => 29, EntreprisePeer::DEBUTEXERCICEGLOB3 => 30, EntreprisePeer::FINEXERCICEGLOB3 => 31, EntreprisePeer::VENTESGLOB1 => 32, EntreprisePeer::VENTESGLOB2 => 33, EntreprisePeer::VENTESGLOB3 => 34, EntreprisePeer::BIENSGLOB1 => 35, EntreprisePeer::BIENSGLOB2 => 36, EntreprisePeer::BIENSGLOB3 => 37, EntreprisePeer::SERVICESGLOB1 => 38, EntreprisePeer::SERVICESGLOB2 => 39, EntreprisePeer::SERVICESGLOB3 => 40, EntreprisePeer::TOTALGLOB1 => 41, EntreprisePeer::TOTALGLOB2 => 42, EntreprisePeer::TOTALGLOB3 => 43, EntreprisePeer::CODEAPE => 44, EntreprisePeer::LIBELLE_APE => 45, EntreprisePeer::ORIGINE_COMPTE => 46, EntreprisePeer::TELEPHONE => 47, EntreprisePeer::FAX => 48, EntreprisePeer::SITE_INTERNET => 49, EntreprisePeer::DESCRIPTION_ACTIVITE => 50, EntreprisePeer::ACTIVITE_DOMAINE_DEFENSE => 51, EntreprisePeer::ANNEE_CLOTURE_EXERCICE1 => 52, EntreprisePeer::ANNEE_CLOTURE_EXERCICE2 => 53, EntreprisePeer::ANNEE_CLOTURE_EXERCICE3 => 54, EntreprisePeer::EFFECTIF_MOYEN1 => 55, EntreprisePeer::EFFECTIF_MOYEN2 => 56, EntreprisePeer::EFFECTIF_MOYEN3 => 57, EntreprisePeer::EFFECTIF_ENCADREMENT1 => 58, EntreprisePeer::EFFECTIF_ENCADREMENT2 => 59, EntreprisePeer::EFFECTIF_ENCADREMENT3 => 60, EntreprisePeer::PME1 => 61, EntreprisePeer::PME2 => 62, EntreprisePeer::PME3 => 63, EntreprisePeer::ADRESSE2 => 64, EntreprisePeer::NICSIEGE => 65, EntreprisePeer::ACRONYME_PAYS => 66, EntreprisePeer::DATE_CREATION => 67, EntreprisePeer::DATE_MODIFICATION => 68, EntreprisePeer::ID_INITIAL => 69, EntreprisePeer::REGION => 70, EntreprisePeer::PROVINCE => 71, EntreprisePeer::TELEPHONE2 => 72, EntreprisePeer::TELEPHONE3 => 73, EntreprisePeer::CNSS => 74, EntreprisePeer::RC_NUM => 75, EntreprisePeer::RC_VILLE => 76, EntreprisePeer::DOMAINES_ACTIVITES => 77, EntreprisePeer::NUM_TAX => 78, EntreprisePeer::DOCUMENTS_COMMERCIAUX => 79, EntreprisePeer::INTITULE_DOCUMENTS_COMMERCIAUX => 80, EntreprisePeer::TAILLE_DOCUMENTS_COMMERCIAUX => 81, EntreprisePeer::QUALIFICATION => 82, EntreprisePeer::AGREMENT => 83, EntreprisePeer::MOYENS_TECHNIQUE => 84, EntreprisePeer::MOYENS_HUMAINS => 85, EntreprisePeer::COMPTE_ACTIF => 86, EntreprisePeer::CAPITAL_SOCIAL => 87, EntreprisePeer::IFU => 88, EntreprisePeer::ID_AGENT_CREATEUR => 89, EntreprisePeer::NOM_AGENT => 90, EntreprisePeer::PRENOM_AGENT => 91, EntreprisePeer::ADRESSES_ELECTRONIQUES => 92, EntreprisePeer::VISIBLE_BOURSE => 93, EntreprisePeer::TYPE_COLLABORATION => 94, EntreprisePeer::ENTREPRISE_EA => 95, EntreprisePeer::ENTREPRISE_SIAE => 96, EntreprisePeer::SAISIE_MANUELLE => 97, EntreprisePeer::CREATED_FROM_DECISION => 98, EntreprisePeer::ID_CODE_EFFECTIF => 99, EntreprisePeer::CATEGORIE_ENTREPRISE => 100, EntreprisePeer::ETAT_ADMINISTRATIF => 101, EntreprisePeer::DATE_CESSATION => 102, EntreprisePeer::ID_EXTERNE => 103, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ADMIN_ID' => 1, 'SIREN' => 2, 'REPMETIERS' => 3, 'NOM' => 4, 'ADRESSE' => 5, 'CODEPOSTAL' => 6, 'VILLEADRESSE' => 7, 'PAYSADRESSE' => 8, 'EMAIL' => 9, 'TAILLE' => 10, 'FORMEJURIDIQUE' => 11, 'VILLEENREGISTREMENT' => 12, 'MOTIFNONINDNUM' => 13, 'ORDREPROFOUAGREMENT' => 14, 'DATECONSTSOCIETE' => 15, 'NOMORGINSCRIPTION' => 16, 'ADRORGINSCRIPTION' => 17, 'DATECONSTASSOC' => 18, 'DATECONSTASSOCETRANGERE' => 19, 'NOMPERSONNEPUBLIQUE' => 20, 'NATIONALITE' => 21, 'REDRESSEMENT' => 22, 'PAYSENREGISTREMENT' => 23, 'SIRENETRANGER' => 24, 'NUMASSOETRANGERE' => 25, 'DEBUTEXERCICEGLOB1' => 26, 'FINEXERCICEGLOB1' => 27, 'DEBUTEXERCICEGLOB2' => 28, 'FINEXERCICEGLOB2' => 29, 'DEBUTEXERCICEGLOB3' => 30, 'FINEXERCICEGLOB3' => 31, 'VENTESGLOB1' => 32, 'VENTESGLOB2' => 33, 'VENTESGLOB3' => 34, 'BIENSGLOB1' => 35, 'BIENSGLOB2' => 36, 'BIENSGLOB3' => 37, 'SERVICESGLOB1' => 38, 'SERVICESGLOB2' => 39, 'SERVICESGLOB3' => 40, 'TOTALGLOB1' => 41, 'TOTALGLOB2' => 42, 'TOTALGLOB3' => 43, 'CODEAPE' => 44, 'LIBELLE_APE' => 45, 'ORIGINE_COMPTE' => 46, 'TELEPHONE' => 47, 'FAX' => 48, 'SITE_INTERNET' => 49, 'DESCRIPTION_ACTIVITE' => 50, 'ACTIVITE_DOMAINE_DEFENSE' => 51, 'ANNEE_CLOTURE_EXERCICE1' => 52, 'ANNEE_CLOTURE_EXERCICE2' => 53, 'ANNEE_CLOTURE_EXERCICE3' => 54, 'EFFECTIF_MOYEN1' => 55, 'EFFECTIF_MOYEN2' => 56, 'EFFECTIF_MOYEN3' => 57, 'EFFECTIF_ENCADREMENT1' => 58, 'EFFECTIF_ENCADREMENT2' => 59, 'EFFECTIF_ENCADREMENT3' => 60, 'PME1' => 61, 'PME2' => 62, 'PME3' => 63, 'ADRESSE2' => 64, 'NICSIEGE' => 65, 'ACRONYME_PAYS' => 66, 'DATE_CREATION' => 67, 'DATE_MODIFICATION' => 68, 'ID_INITIAL' => 69, 'REGION' => 70, 'PROVINCE' => 71, 'TELEPHONE2' => 72, 'TELEPHONE3' => 73, 'CNSS' => 74, 'RC_NUM' => 75, 'RC_VILLE' => 76, 'DOMAINES_ACTIVITES' => 77, 'NUM_TAX' => 78, 'DOCUMENTS_COMMERCIAUX' => 79, 'INTITULE_DOCUMENTS_COMMERCIAUX' => 80, 'TAILLE_DOCUMENTS_COMMERCIAUX' => 81, 'QUALIFICATION' => 82, 'AGREMENT' => 83, 'MOYENS_TECHNIQUE' => 84, 'MOYENS_HUMAINS' => 85, 'COMPTE_ACTIF' => 86, 'CAPITAL_SOCIAL' => 87, 'IFU' => 88, 'ID_AGENT_CREATEUR' => 89, 'NOM_AGENT' => 90, 'PRENOM_AGENT' => 91, 'ADRESSES_ELECTRONIQUES' => 92, 'VISIBLE_BOURSE' => 93, 'TYPE_COLLABORATION' => 94, 'ENTREPRISE_EA' => 95, 'ENTREPRISE_SIAE' => 96, 'SAISIE_MANUELLE' => 97, 'CREATED_FROM_DECISION' => 98, 'ID_CODE_EFFECTIF' => 99, 'CATEGORIE_ENTREPRISE' => 100, 'ETAT_ADMINISTRATIF' => 101, 'DATE_CESSATION' => 102, 'ID_EXTERNE' => 103, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'admin_id' => 1, 'siren' => 2, 'repmetiers' => 3, 'nom' => 4, 'adresse' => 5, 'codepostal' => 6, 'villeadresse' => 7, 'paysadresse' => 8, 'email' => 9, 'taille' => 10, 'formejuridique' => 11, 'villeenregistrement' => 12, 'motifNonIndNum' => 13, 'ordreProfOuAgrement' => 14, 'dateConstSociete' => 15, 'nomOrgInscription' => 16, 'adrOrgInscription' => 17, 'dateConstAssoc' => 18, 'dateConstAssocEtrangere' => 19, 'nomPersonnePublique' => 20, 'nationalite' => 21, 'redressement' => 22, 'paysenregistrement' => 23, 'sirenEtranger' => 24, 'numAssoEtrangere' => 25, 'debutExerciceGlob1' => 26, 'finExerciceGlob1' => 27, 'debutExerciceGlob2' => 28, 'finExerciceGlob2' => 29, 'debutExerciceGlob3' => 30, 'finExerciceGlob3' => 31, 'ventesGlob1' => 32, 'ventesGlob2' => 33, 'ventesGlob3' => 34, 'biensGlob1' => 35, 'biensGlob2' => 36, 'biensGlob3' => 37, 'servicesGlob1' => 38, 'servicesGlob2' => 39, 'servicesGlob3' => 40, 'totalGlob1' => 41, 'totalGlob2' => 42, 'totalGlob3' => 43, 'codeape' => 44, 'libelle_ape' => 45, 'origine_compte' => 46, 'telephone' => 47, 'fax' => 48, 'site_internet' => 49, 'description_activite' => 50, 'activite_domaine_defense' => 51, 'annee_cloture_exercice1' => 52, 'annee_cloture_exercice2' => 53, 'annee_cloture_exercice3' => 54, 'effectif_moyen1' => 55, 'effectif_moyen2' => 56, 'effectif_moyen3' => 57, 'effectif_encadrement1' => 58, 'effectif_encadrement2' => 59, 'effectif_encadrement3' => 60, 'pme1' => 61, 'pme2' => 62, 'pme3' => 63, 'adresse2' => 64, 'nicSiege' => 65, 'acronyme_pays' => 66, 'date_creation' => 67, 'date_modification' => 68, 'id_initial' => 69, 'region' => 70, 'province' => 71, 'telephone2' => 72, 'telephone3' => 73, 'cnss' => 74, 'rc_num' => 75, 'rc_ville' => 76, 'domaines_activites' => 77, 'num_tax' => 78, 'documents_commerciaux' => 79, 'intitule_documents_commerciaux' => 80, 'taille_documents_commerciaux' => 81, 'qualification' => 82, 'agrement' => 83, 'moyens_technique' => 84, 'moyens_humains' => 85, 'compte_actif' => 86, 'capital_social' => 87, 'ifu' => 88, 'id_agent_createur' => 89, 'nom_agent' => 90, 'prenom_agent' => 91, 'adresses_electroniques' => 92, 'visible_bourse' => 93, 'type_collaboration' => 94, 'entreprise_EA' => 95, 'entreprise_SIAE' => 96, 'saisie_manuelle' => 97, 'created_from_decision' => 98, 'id_code_effectif' => 99, 'categorie_entreprise' => 100, 'etat_administratif' => 101, 'date_cessation' => 102, 'id_externe' => 103, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        EntreprisePeer::PME1 => array(
            EntreprisePeer::PME1_1,
            EntreprisePeer::PME1_0,
        ),
        EntreprisePeer::PME2 => array(
            EntreprisePeer::PME2_1,
            EntreprisePeer::PME2_0,
        ),
        EntreprisePeer::PME3 => array(
            EntreprisePeer::PME3_1,
            EntreprisePeer::PME3_0,
        ),
        EntreprisePeer::ENTREPRISE_EA => array(
            EntreprisePeer::ENTREPRISE_EA_0,
            EntreprisePeer::ENTREPRISE_EA_1,
        ),
        EntreprisePeer::ENTREPRISE_SIAE => array(
            EntreprisePeer::ENTREPRISE_SIAE_0,
            EntreprisePeer::ENTREPRISE_SIAE_1,
        ),
        EntreprisePeer::SAISIE_MANUELLE => array(
            EntreprisePeer::SAISIE_MANUELLE_0,
            EntreprisePeer::SAISIE_MANUELLE_1,
        ),
        EntreprisePeer::CREATED_FROM_DECISION => array(
            EntreprisePeer::CREATED_FROM_DECISION_0,
            EntreprisePeer::CREATED_FROM_DECISION_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = EntreprisePeer::getFieldNames($toType);
        $key = isset(EntreprisePeer::$fieldKeys[$fromType][$name]) ? EntreprisePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(EntreprisePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, EntreprisePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return EntreprisePeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return EntreprisePeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = EntreprisePeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = EntreprisePeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. EntreprisePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(EntreprisePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EntreprisePeer::ID);
            $criteria->addSelectColumn(EntreprisePeer::ADMIN_ID);
            $criteria->addSelectColumn(EntreprisePeer::SIREN);
            $criteria->addSelectColumn(EntreprisePeer::REPMETIERS);
            $criteria->addSelectColumn(EntreprisePeer::NOM);
            $criteria->addSelectColumn(EntreprisePeer::ADRESSE);
            $criteria->addSelectColumn(EntreprisePeer::CODEPOSTAL);
            $criteria->addSelectColumn(EntreprisePeer::VILLEADRESSE);
            $criteria->addSelectColumn(EntreprisePeer::PAYSADRESSE);
            $criteria->addSelectColumn(EntreprisePeer::EMAIL);
            $criteria->addSelectColumn(EntreprisePeer::TAILLE);
            $criteria->addSelectColumn(EntreprisePeer::FORMEJURIDIQUE);
            $criteria->addSelectColumn(EntreprisePeer::VILLEENREGISTREMENT);
            $criteria->addSelectColumn(EntreprisePeer::MOTIFNONINDNUM);
            $criteria->addSelectColumn(EntreprisePeer::ORDREPROFOUAGREMENT);
            $criteria->addSelectColumn(EntreprisePeer::DATECONSTSOCIETE);
            $criteria->addSelectColumn(EntreprisePeer::NOMORGINSCRIPTION);
            $criteria->addSelectColumn(EntreprisePeer::ADRORGINSCRIPTION);
            $criteria->addSelectColumn(EntreprisePeer::DATECONSTASSOC);
            $criteria->addSelectColumn(EntreprisePeer::DATECONSTASSOCETRANGERE);
            $criteria->addSelectColumn(EntreprisePeer::NOMPERSONNEPUBLIQUE);
            $criteria->addSelectColumn(EntreprisePeer::NATIONALITE);
            $criteria->addSelectColumn(EntreprisePeer::REDRESSEMENT);
            $criteria->addSelectColumn(EntreprisePeer::PAYSENREGISTREMENT);
            $criteria->addSelectColumn(EntreprisePeer::SIRENETRANGER);
            $criteria->addSelectColumn(EntreprisePeer::NUMASSOETRANGERE);
            $criteria->addSelectColumn(EntreprisePeer::DEBUTEXERCICEGLOB1);
            $criteria->addSelectColumn(EntreprisePeer::FINEXERCICEGLOB1);
            $criteria->addSelectColumn(EntreprisePeer::DEBUTEXERCICEGLOB2);
            $criteria->addSelectColumn(EntreprisePeer::FINEXERCICEGLOB2);
            $criteria->addSelectColumn(EntreprisePeer::DEBUTEXERCICEGLOB3);
            $criteria->addSelectColumn(EntreprisePeer::FINEXERCICEGLOB3);
            $criteria->addSelectColumn(EntreprisePeer::VENTESGLOB1);
            $criteria->addSelectColumn(EntreprisePeer::VENTESGLOB2);
            $criteria->addSelectColumn(EntreprisePeer::VENTESGLOB3);
            $criteria->addSelectColumn(EntreprisePeer::BIENSGLOB1);
            $criteria->addSelectColumn(EntreprisePeer::BIENSGLOB2);
            $criteria->addSelectColumn(EntreprisePeer::BIENSGLOB3);
            $criteria->addSelectColumn(EntreprisePeer::SERVICESGLOB1);
            $criteria->addSelectColumn(EntreprisePeer::SERVICESGLOB2);
            $criteria->addSelectColumn(EntreprisePeer::SERVICESGLOB3);
            $criteria->addSelectColumn(EntreprisePeer::TOTALGLOB1);
            $criteria->addSelectColumn(EntreprisePeer::TOTALGLOB2);
            $criteria->addSelectColumn(EntreprisePeer::TOTALGLOB3);
            $criteria->addSelectColumn(EntreprisePeer::CODEAPE);
            $criteria->addSelectColumn(EntreprisePeer::LIBELLE_APE);
            $criteria->addSelectColumn(EntreprisePeer::ORIGINE_COMPTE);
            $criteria->addSelectColumn(EntreprisePeer::TELEPHONE);
            $criteria->addSelectColumn(EntreprisePeer::FAX);
            $criteria->addSelectColumn(EntreprisePeer::SITE_INTERNET);
            $criteria->addSelectColumn(EntreprisePeer::DESCRIPTION_ACTIVITE);
            $criteria->addSelectColumn(EntreprisePeer::ACTIVITE_DOMAINE_DEFENSE);
            $criteria->addSelectColumn(EntreprisePeer::ANNEE_CLOTURE_EXERCICE1);
            $criteria->addSelectColumn(EntreprisePeer::ANNEE_CLOTURE_EXERCICE2);
            $criteria->addSelectColumn(EntreprisePeer::ANNEE_CLOTURE_EXERCICE3);
            $criteria->addSelectColumn(EntreprisePeer::EFFECTIF_MOYEN1);
            $criteria->addSelectColumn(EntreprisePeer::EFFECTIF_MOYEN2);
            $criteria->addSelectColumn(EntreprisePeer::EFFECTIF_MOYEN3);
            $criteria->addSelectColumn(EntreprisePeer::EFFECTIF_ENCADREMENT1);
            $criteria->addSelectColumn(EntreprisePeer::EFFECTIF_ENCADREMENT2);
            $criteria->addSelectColumn(EntreprisePeer::EFFECTIF_ENCADREMENT3);
            $criteria->addSelectColumn(EntreprisePeer::PME1);
            $criteria->addSelectColumn(EntreprisePeer::PME2);
            $criteria->addSelectColumn(EntreprisePeer::PME3);
            $criteria->addSelectColumn(EntreprisePeer::ADRESSE2);
            $criteria->addSelectColumn(EntreprisePeer::NICSIEGE);
            $criteria->addSelectColumn(EntreprisePeer::ACRONYME_PAYS);
            $criteria->addSelectColumn(EntreprisePeer::DATE_CREATION);
            $criteria->addSelectColumn(EntreprisePeer::DATE_MODIFICATION);
            $criteria->addSelectColumn(EntreprisePeer::ID_INITIAL);
            $criteria->addSelectColumn(EntreprisePeer::REGION);
            $criteria->addSelectColumn(EntreprisePeer::PROVINCE);
            $criteria->addSelectColumn(EntreprisePeer::TELEPHONE2);
            $criteria->addSelectColumn(EntreprisePeer::TELEPHONE3);
            $criteria->addSelectColumn(EntreprisePeer::CNSS);
            $criteria->addSelectColumn(EntreprisePeer::RC_NUM);
            $criteria->addSelectColumn(EntreprisePeer::RC_VILLE);
            $criteria->addSelectColumn(EntreprisePeer::DOMAINES_ACTIVITES);
            $criteria->addSelectColumn(EntreprisePeer::NUM_TAX);
            $criteria->addSelectColumn(EntreprisePeer::DOCUMENTS_COMMERCIAUX);
            $criteria->addSelectColumn(EntreprisePeer::INTITULE_DOCUMENTS_COMMERCIAUX);
            $criteria->addSelectColumn(EntreprisePeer::TAILLE_DOCUMENTS_COMMERCIAUX);
            $criteria->addSelectColumn(EntreprisePeer::QUALIFICATION);
            $criteria->addSelectColumn(EntreprisePeer::AGREMENT);
            $criteria->addSelectColumn(EntreprisePeer::MOYENS_TECHNIQUE);
            $criteria->addSelectColumn(EntreprisePeer::MOYENS_HUMAINS);
            $criteria->addSelectColumn(EntreprisePeer::COMPTE_ACTIF);
            $criteria->addSelectColumn(EntreprisePeer::CAPITAL_SOCIAL);
            $criteria->addSelectColumn(EntreprisePeer::IFU);
            $criteria->addSelectColumn(EntreprisePeer::ID_AGENT_CREATEUR);
            $criteria->addSelectColumn(EntreprisePeer::NOM_AGENT);
            $criteria->addSelectColumn(EntreprisePeer::PRENOM_AGENT);
            $criteria->addSelectColumn(EntreprisePeer::ADRESSES_ELECTRONIQUES);
            $criteria->addSelectColumn(EntreprisePeer::VISIBLE_BOURSE);
            $criteria->addSelectColumn(EntreprisePeer::TYPE_COLLABORATION);
            $criteria->addSelectColumn(EntreprisePeer::ENTREPRISE_EA);
            $criteria->addSelectColumn(EntreprisePeer::ENTREPRISE_SIAE);
            $criteria->addSelectColumn(EntreprisePeer::SAISIE_MANUELLE);
            $criteria->addSelectColumn(EntreprisePeer::CREATED_FROM_DECISION);
            $criteria->addSelectColumn(EntreprisePeer::ID_CODE_EFFECTIF);
            $criteria->addSelectColumn(EntreprisePeer::CATEGORIE_ENTREPRISE);
            $criteria->addSelectColumn(EntreprisePeer::ETAT_ADMINISTRATIF);
            $criteria->addSelectColumn(EntreprisePeer::DATE_CESSATION);
            $criteria->addSelectColumn(EntreprisePeer::ID_EXTERNE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.admin_id');
            $criteria->addSelectColumn($alias . '.siren');
            $criteria->addSelectColumn($alias . '.repmetiers');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.adresse');
            $criteria->addSelectColumn($alias . '.codepostal');
            $criteria->addSelectColumn($alias . '.villeadresse');
            $criteria->addSelectColumn($alias . '.paysadresse');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.taille');
            $criteria->addSelectColumn($alias . '.formejuridique');
            $criteria->addSelectColumn($alias . '.villeenregistrement');
            $criteria->addSelectColumn($alias . '.motifNonIndNum');
            $criteria->addSelectColumn($alias . '.ordreProfOuAgrement');
            $criteria->addSelectColumn($alias . '.dateConstSociete');
            $criteria->addSelectColumn($alias . '.nomOrgInscription');
            $criteria->addSelectColumn($alias . '.adrOrgInscription');
            $criteria->addSelectColumn($alias . '.dateConstAssoc');
            $criteria->addSelectColumn($alias . '.dateConstAssocEtrangere');
            $criteria->addSelectColumn($alias . '.nomPersonnePublique');
            $criteria->addSelectColumn($alias . '.nationalite');
            $criteria->addSelectColumn($alias . '.redressement');
            $criteria->addSelectColumn($alias . '.paysenregistrement');
            $criteria->addSelectColumn($alias . '.sirenEtranger');
            $criteria->addSelectColumn($alias . '.numAssoEtrangere');
            $criteria->addSelectColumn($alias . '.debutExerciceGlob1');
            $criteria->addSelectColumn($alias . '.finExerciceGlob1');
            $criteria->addSelectColumn($alias . '.debutExerciceGlob2');
            $criteria->addSelectColumn($alias . '.finExerciceGlob2');
            $criteria->addSelectColumn($alias . '.debutExerciceGlob3');
            $criteria->addSelectColumn($alias . '.finExerciceGlob3');
            $criteria->addSelectColumn($alias . '.ventesGlob1');
            $criteria->addSelectColumn($alias . '.ventesGlob2');
            $criteria->addSelectColumn($alias . '.ventesGlob3');
            $criteria->addSelectColumn($alias . '.biensGlob1');
            $criteria->addSelectColumn($alias . '.biensGlob2');
            $criteria->addSelectColumn($alias . '.biensGlob3');
            $criteria->addSelectColumn($alias . '.servicesGlob1');
            $criteria->addSelectColumn($alias . '.servicesGlob2');
            $criteria->addSelectColumn($alias . '.servicesGlob3');
            $criteria->addSelectColumn($alias . '.totalGlob1');
            $criteria->addSelectColumn($alias . '.totalGlob2');
            $criteria->addSelectColumn($alias . '.totalGlob3');
            $criteria->addSelectColumn($alias . '.codeape');
            $criteria->addSelectColumn($alias . '.libelle_ape');
            $criteria->addSelectColumn($alias . '.origine_compte');
            $criteria->addSelectColumn($alias . '.telephone');
            $criteria->addSelectColumn($alias . '.fax');
            $criteria->addSelectColumn($alias . '.site_internet');
            $criteria->addSelectColumn($alias . '.description_activite');
            $criteria->addSelectColumn($alias . '.activite_domaine_defense');
            $criteria->addSelectColumn($alias . '.annee_cloture_exercice1');
            $criteria->addSelectColumn($alias . '.annee_cloture_exercice2');
            $criteria->addSelectColumn($alias . '.annee_cloture_exercice3');
            $criteria->addSelectColumn($alias . '.effectif_moyen1');
            $criteria->addSelectColumn($alias . '.effectif_moyen2');
            $criteria->addSelectColumn($alias . '.effectif_moyen3');
            $criteria->addSelectColumn($alias . '.effectif_encadrement1');
            $criteria->addSelectColumn($alias . '.effectif_encadrement2');
            $criteria->addSelectColumn($alias . '.effectif_encadrement3');
            $criteria->addSelectColumn($alias . '.pme1');
            $criteria->addSelectColumn($alias . '.pme2');
            $criteria->addSelectColumn($alias . '.pme3');
            $criteria->addSelectColumn($alias . '.adresse2');
            $criteria->addSelectColumn($alias . '.nicSiege');
            $criteria->addSelectColumn($alias . '.acronyme_pays');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_modification');
            $criteria->addSelectColumn($alias . '.id_initial');
            $criteria->addSelectColumn($alias . '.region');
            $criteria->addSelectColumn($alias . '.province');
            $criteria->addSelectColumn($alias . '.telephone2');
            $criteria->addSelectColumn($alias . '.telephone3');
            $criteria->addSelectColumn($alias . '.cnss');
            $criteria->addSelectColumn($alias . '.rc_num');
            $criteria->addSelectColumn($alias . '.rc_ville');
            $criteria->addSelectColumn($alias . '.domaines_activites');
            $criteria->addSelectColumn($alias . '.num_tax');
            $criteria->addSelectColumn($alias . '.documents_commerciaux');
            $criteria->addSelectColumn($alias . '.intitule_documents_commerciaux');
            $criteria->addSelectColumn($alias . '.taille_documents_commerciaux');
            $criteria->addSelectColumn($alias . '.qualification');
            $criteria->addSelectColumn($alias . '.agrement');
            $criteria->addSelectColumn($alias . '.moyens_technique');
            $criteria->addSelectColumn($alias . '.moyens_humains');
            $criteria->addSelectColumn($alias . '.compte_actif');
            $criteria->addSelectColumn($alias . '.capital_social');
            $criteria->addSelectColumn($alias . '.ifu');
            $criteria->addSelectColumn($alias . '.id_agent_createur');
            $criteria->addSelectColumn($alias . '.nom_agent');
            $criteria->addSelectColumn($alias . '.prenom_agent');
            $criteria->addSelectColumn($alias . '.adresses_electroniques');
            $criteria->addSelectColumn($alias . '.visible_bourse');
            $criteria->addSelectColumn($alias . '.type_collaboration');
            $criteria->addSelectColumn($alias . '.entreprise_EA');
            $criteria->addSelectColumn($alias . '.entreprise_SIAE');
            $criteria->addSelectColumn($alias . '.saisie_manuelle');
            $criteria->addSelectColumn($alias . '.created_from_decision');
            $criteria->addSelectColumn($alias . '.id_code_effectif');
            $criteria->addSelectColumn($alias . '.categorie_entreprise');
            $criteria->addSelectColumn($alias . '.etat_administratif');
            $criteria->addSelectColumn($alias . '.date_cessation');
            $criteria->addSelectColumn($alias . '.id_externe');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(EntreprisePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            EntreprisePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(EntreprisePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(EntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 Entreprise
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = EntreprisePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return EntreprisePeer::populateObjects(EntreprisePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(EntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            EntreprisePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(EntreprisePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      Entreprise $obj A Entreprise object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            EntreprisePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Entreprise object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Entreprise) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Entreprise object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(EntreprisePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   Entreprise Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(EntreprisePeer::$instances[$key])) {
                return EntreprisePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (EntreprisePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        EntreprisePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to Entreprise
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in CommonInscritPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonInscritPeer::clearInstancePool();
        // Invalidate objects in CommonPanierEntreprisePeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonPanierEntreprisePeer::clearInstancePool();
        // Invalidate objects in CommonPrestationPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonPrestationPeer::clearInstancePool();
        // Invalidate objects in CommonResponsableengagementPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonResponsableengagementPeer::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = EntreprisePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = EntreprisePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = EntreprisePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EntreprisePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Entreprise object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = EntreprisePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + EntreprisePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EntreprisePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            EntreprisePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(EntreprisePeer::DATABASE_NAME)->getTable(EntreprisePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseEntreprisePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseEntreprisePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new EntrepriseTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return EntreprisePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Entreprise or Criteria object.
     *
     * @param      mixed $values Criteria or Entreprise object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(EntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Entreprise object
        }

        if ($criteria->containsKey(EntreprisePeer::ID) && $criteria->keyContainsValue(EntreprisePeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.EntreprisePeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(EntreprisePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Entreprise or Criteria object.
     *
     * @param      mixed $values Criteria or Entreprise object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(EntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(EntreprisePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(EntreprisePeer::ID);
            $value = $criteria->remove(EntreprisePeer::ID);
            if ($value) {
                $selectCriteria->add(EntreprisePeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(EntreprisePeer::TABLE_NAME);
            }

        } else { // $values is Entreprise object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(EntreprisePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the Entreprise table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(EntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += EntreprisePeer::doOnDeleteCascade(new Criteria(EntreprisePeer::DATABASE_NAME), $con);
            $affectedRows += BasePeer::doDeleteAll(EntreprisePeer::TABLE_NAME, $con, EntreprisePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EntreprisePeer::clearInstancePool();
            EntreprisePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Entreprise or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Entreprise object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(EntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Entreprise) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EntreprisePeer::DATABASE_NAME);
            $criteria->add(EntreprisePeer::ID, (array) $values, Criteria::IN);
        }

        // Set the correct dbName
        $criteria->setDbName(EntreprisePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += EntreprisePeer::doOnDeleteCascade($c, $con);

            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            if ($values instanceof Criteria) {
                EntreprisePeer::clearInstancePool();
            } elseif ($values instanceof Entreprise) { // it's a model object
                EntreprisePeer::removeInstanceFromPool($values);
            } else { // it's a primary key, or an array of pks
                foreach ((array) $values as $singleval) {
                    EntreprisePeer::removeInstanceFromPool($singleval);
                }
            }

            $affectedRows += BasePeer::doDelete($criteria, $con);
            EntreprisePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected static function doOnDeleteCascade(Criteria $criteria, PropelPDO $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $criteria
        $objects = EntreprisePeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {


            // delete related CommonInscrit objects
            $criteria = new Criteria(CommonInscritPeer::DATABASE_NAME);

            $criteria->add(CommonInscritPeer::ENTREPRISE_ID, $obj->getId());
            $affectedRows += CommonInscritPeer::doDelete($criteria, $con);

            // delete related CommonPanierEntreprise objects
            $criteria = new Criteria(CommonPanierEntreprisePeer::DATABASE_NAME);

            $criteria->add(CommonPanierEntreprisePeer::ID_ENTREPRISE, $obj->getId());
            $affectedRows += CommonPanierEntreprisePeer::doDelete($criteria, $con);

            // delete related CommonPrestation objects
            $criteria = new Criteria(CommonPrestationPeer::DATABASE_NAME);

            $criteria->add(CommonPrestationPeer::ID_ENTREPRISE, $obj->getId());
            $affectedRows += CommonPrestationPeer::doDelete($criteria, $con);

            // delete related CommonResponsableengagement objects
            $criteria = new Criteria(CommonResponsableengagementPeer::DATABASE_NAME);

            $criteria->add(CommonResponsableengagementPeer::ENTREPRISE_ID, $obj->getId());
            $affectedRows += CommonResponsableengagementPeer::doDelete($criteria, $con);
        }

        return $affectedRows;
    }

    /**
     * Validates all modified columns of given Entreprise object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      Entreprise $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(EntreprisePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(EntreprisePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(EntreprisePeer::DATABASE_NAME, EntreprisePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Entreprise
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = EntreprisePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(EntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(EntreprisePeer::DATABASE_NAME);
        $criteria->add(EntreprisePeer::ID, $pk);

        $v = EntreprisePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Entreprise[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(EntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(EntreprisePeer::DATABASE_NAME);
            $criteria->add(EntreprisePeer::ID, $pks, Criteria::IN);
            $objs = EntreprisePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseEntreprisePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseEntreprisePeer::buildTableMap();

