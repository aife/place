<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTHistoriqueAnnoncePeer;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTTypeAnnonceConsultationPeer;
use Application\Propel\Mpe\Map\CommonTAnnonceConsultationTableMap;

/**
 * Base static class for performing query and update operations on the 't_annonce_consultation' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTAnnonceConsultationPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_annonce_consultation';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTAnnonceConsultation';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTAnnonceConsultationTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 18;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 18;

    /** the column name for the id field */
    const ID = 't_annonce_consultation.id';

    /** the column name for the ref_consultation field */
    const REF_CONSULTATION = 't_annonce_consultation.ref_consultation';

    /** the column name for the organisme field */
    const ORGANISME = 't_annonce_consultation.organisme';

    /** the column name for the id_dossier_sub field */
    const ID_DOSSIER_SUB = 't_annonce_consultation.id_dossier_sub';

    /** the column name for the id_dispositif field */
    const ID_DISPOSITIF = 't_annonce_consultation.id_dispositif';

    /** the column name for the id_compte_boamp field */
    const ID_COMPTE_BOAMP = 't_annonce_consultation.id_compte_boamp';

    /** the column name for the statut field */
    const STATUT = 't_annonce_consultation.statut';

    /** the column name for the id_type_annonce field */
    const ID_TYPE_ANNONCE = 't_annonce_consultation.id_type_annonce';

    /** the column name for the id_agent field */
    const ID_AGENT = 't_annonce_consultation.id_agent';

    /** the column name for the prenom_nom_agent field */
    const PRENOM_NOM_AGENT = 't_annonce_consultation.prenom_nom_agent';

    /** the column name for the date_creation field */
    const DATE_CREATION = 't_annonce_consultation.date_creation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 't_annonce_consultation.date_modification';

    /** the column name for the date_statut field */
    const DATE_STATUT = 't_annonce_consultation.date_statut';

    /** the column name for the motif_statut field */
    const MOTIF_STATUT = 't_annonce_consultation.motif_statut';

    /** the column name for the id_dossier_parent field */
    const ID_DOSSIER_PARENT = 't_annonce_consultation.id_dossier_parent';

    /** the column name for the id_dispositif_parent field */
    const ID_DISPOSITIF_PARENT = 't_annonce_consultation.id_dispositif_parent';

    /** the column name for the publicite_simplifie field */
    const PUBLICITE_SIMPLIFIE = 't_annonce_consultation.publicite_simplifie';

    /** the column name for the consultation_id field */
    const CONSULTATION_ID = 't_annonce_consultation.consultation_id';

    /** The enumerated values for the publicite_simplifie field */
    const PUBLICITE_SIMPLIFIE_0 = '0';
    const PUBLICITE_SIMPLIFIE_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTAnnonceConsultation objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTAnnonceConsultation[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTAnnonceConsultationPeer::$fieldNames[CommonTAnnonceConsultationPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'RefConsultation', 'Organisme', 'IdDossierSub', 'IdDispositif', 'IdCompteBoamp', 'Statut', 'IdTypeAnnonce', 'IdAgent', 'PrenomNomAgent', 'DateCreation', 'DateModification', 'DateStatut', 'MotifStatut', 'IdDossierParent', 'IdDispositifParent', 'PubliciteSimplifie', 'ConsultationId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'refConsultation', 'organisme', 'idDossierSub', 'idDispositif', 'idCompteBoamp', 'statut', 'idTypeAnnonce', 'idAgent', 'prenomNomAgent', 'dateCreation', 'dateModification', 'dateStatut', 'motifStatut', 'idDossierParent', 'idDispositifParent', 'publiciteSimplifie', 'consultationId', ),
        BasePeer::TYPE_COLNAME => array (CommonTAnnonceConsultationPeer::ID, CommonTAnnonceConsultationPeer::REF_CONSULTATION, CommonTAnnonceConsultationPeer::ORGANISME, CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB, CommonTAnnonceConsultationPeer::ID_DISPOSITIF, CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP, CommonTAnnonceConsultationPeer::STATUT, CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, CommonTAnnonceConsultationPeer::ID_AGENT, CommonTAnnonceConsultationPeer::PRENOM_NOM_AGENT, CommonTAnnonceConsultationPeer::DATE_CREATION, CommonTAnnonceConsultationPeer::DATE_MODIFICATION, CommonTAnnonceConsultationPeer::DATE_STATUT, CommonTAnnonceConsultationPeer::MOTIF_STATUT, CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT, CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT, CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE, CommonTAnnonceConsultationPeer::CONSULTATION_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'REF_CONSULTATION', 'ORGANISME', 'ID_DOSSIER_SUB', 'ID_DISPOSITIF', 'ID_COMPTE_BOAMP', 'STATUT', 'ID_TYPE_ANNONCE', 'ID_AGENT', 'PRENOM_NOM_AGENT', 'DATE_CREATION', 'DATE_MODIFICATION', 'DATE_STATUT', 'MOTIF_STATUT', 'ID_DOSSIER_PARENT', 'ID_DISPOSITIF_PARENT', 'PUBLICITE_SIMPLIFIE', 'CONSULTATION_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'ref_consultation', 'organisme', 'id_dossier_sub', 'id_dispositif', 'id_compte_boamp', 'statut', 'id_type_annonce', 'id_agent', 'prenom_nom_agent', 'date_creation', 'date_modification', 'date_statut', 'motif_statut', 'id_dossier_parent', 'id_dispositif_parent', 'publicite_simplifie', 'consultation_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTAnnonceConsultationPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'RefConsultation' => 1, 'Organisme' => 2, 'IdDossierSub' => 3, 'IdDispositif' => 4, 'IdCompteBoamp' => 5, 'Statut' => 6, 'IdTypeAnnonce' => 7, 'IdAgent' => 8, 'PrenomNomAgent' => 9, 'DateCreation' => 10, 'DateModification' => 11, 'DateStatut' => 12, 'MotifStatut' => 13, 'IdDossierParent' => 14, 'IdDispositifParent' => 15, 'PubliciteSimplifie' => 16, 'ConsultationId' => 17, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'refConsultation' => 1, 'organisme' => 2, 'idDossierSub' => 3, 'idDispositif' => 4, 'idCompteBoamp' => 5, 'statut' => 6, 'idTypeAnnonce' => 7, 'idAgent' => 8, 'prenomNomAgent' => 9, 'dateCreation' => 10, 'dateModification' => 11, 'dateStatut' => 12, 'motifStatut' => 13, 'idDossierParent' => 14, 'idDispositifParent' => 15, 'publiciteSimplifie' => 16, 'consultationId' => 17, ),
        BasePeer::TYPE_COLNAME => array (CommonTAnnonceConsultationPeer::ID => 0, CommonTAnnonceConsultationPeer::REF_CONSULTATION => 1, CommonTAnnonceConsultationPeer::ORGANISME => 2, CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB => 3, CommonTAnnonceConsultationPeer::ID_DISPOSITIF => 4, CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP => 5, CommonTAnnonceConsultationPeer::STATUT => 6, CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE => 7, CommonTAnnonceConsultationPeer::ID_AGENT => 8, CommonTAnnonceConsultationPeer::PRENOM_NOM_AGENT => 9, CommonTAnnonceConsultationPeer::DATE_CREATION => 10, CommonTAnnonceConsultationPeer::DATE_MODIFICATION => 11, CommonTAnnonceConsultationPeer::DATE_STATUT => 12, CommonTAnnonceConsultationPeer::MOTIF_STATUT => 13, CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT => 14, CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT => 15, CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE => 16, CommonTAnnonceConsultationPeer::CONSULTATION_ID => 17, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'REF_CONSULTATION' => 1, 'ORGANISME' => 2, 'ID_DOSSIER_SUB' => 3, 'ID_DISPOSITIF' => 4, 'ID_COMPTE_BOAMP' => 5, 'STATUT' => 6, 'ID_TYPE_ANNONCE' => 7, 'ID_AGENT' => 8, 'PRENOM_NOM_AGENT' => 9, 'DATE_CREATION' => 10, 'DATE_MODIFICATION' => 11, 'DATE_STATUT' => 12, 'MOTIF_STATUT' => 13, 'ID_DOSSIER_PARENT' => 14, 'ID_DISPOSITIF_PARENT' => 15, 'PUBLICITE_SIMPLIFIE' => 16, 'CONSULTATION_ID' => 17, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'ref_consultation' => 1, 'organisme' => 2, 'id_dossier_sub' => 3, 'id_dispositif' => 4, 'id_compte_boamp' => 5, 'statut' => 6, 'id_type_annonce' => 7, 'id_agent' => 8, 'prenom_nom_agent' => 9, 'date_creation' => 10, 'date_modification' => 11, 'date_statut' => 12, 'motif_statut' => 13, 'id_dossier_parent' => 14, 'id_dispositif_parent' => 15, 'publicite_simplifie' => 16, 'consultation_id' => 17, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE => array(
            CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE_0,
            CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTAnnonceConsultationPeer::getFieldNames($toType);
        $key = isset(CommonTAnnonceConsultationPeer::$fieldKeys[$fromType][$name]) ? CommonTAnnonceConsultationPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTAnnonceConsultationPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTAnnonceConsultationPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTAnnonceConsultationPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTAnnonceConsultationPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTAnnonceConsultationPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTAnnonceConsultationPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTAnnonceConsultationPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTAnnonceConsultationPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::ID);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::REF_CONSULTATION);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::ORGANISME);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::ID_DISPOSITIF);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::STATUT);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::ID_AGENT);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::PRENOM_NOM_AGENT);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::DATE_MODIFICATION);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::DATE_STATUT);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::MOTIF_STATUT);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE);
            $criteria->addSelectColumn(CommonTAnnonceConsultationPeer::CONSULTATION_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.ref_consultation');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.id_dossier_sub');
            $criteria->addSelectColumn($alias . '.id_dispositif');
            $criteria->addSelectColumn($alias . '.id_compte_boamp');
            $criteria->addSelectColumn($alias . '.statut');
            $criteria->addSelectColumn($alias . '.id_type_annonce');
            $criteria->addSelectColumn($alias . '.id_agent');
            $criteria->addSelectColumn($alias . '.prenom_nom_agent');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_modification');
            $criteria->addSelectColumn($alias . '.date_statut');
            $criteria->addSelectColumn($alias . '.motif_statut');
            $criteria->addSelectColumn($alias . '.id_dossier_parent');
            $criteria->addSelectColumn($alias . '.id_dispositif_parent');
            $criteria->addSelectColumn($alias . '.publicite_simplifie');
            $criteria->addSelectColumn($alias . '.consultation_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTAnnonceConsultation
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTAnnonceConsultationPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTAnnonceConsultationPeer::populateObjects(CommonTAnnonceConsultationPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTAnnonceConsultation $obj A CommonTAnnonceConsultation object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTAnnonceConsultationPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTAnnonceConsultation object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTAnnonceConsultation) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTAnnonceConsultation object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTAnnonceConsultationPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTAnnonceConsultation Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTAnnonceConsultationPeer::$instances[$key])) {
                return CommonTAnnonceConsultationPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTAnnonceConsultationPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTAnnonceConsultationPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_annonce_consultation
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in CommonTHistoriqueAnnoncePeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonTHistoriqueAnnoncePeer::clearInstancePool();
        // Invalidate objects in CommonTSupportAnnonceConsultationPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonTSupportAnnonceConsultationPeer::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTAnnonceConsultationPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTAnnonceConsultationPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTAnnonceConsultationPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTAnnonceConsultation object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTAnnonceConsultationPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTAnnonceConsultationPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTAnnonceConsultationPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTAnnonceConsultationPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTTypeAnnonceConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTTypeAnnonceConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, CommonTTypeAnnonceConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTAnnonceConsultation objects pre-filled with their CommonConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonTAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTAnnonceConsultationPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTAnnonceConsultation) to $obj2 (CommonConsultation)
                $obj2->addCommonTAnnonceConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTAnnonceConsultation objects pre-filled with their CommonTTypeAnnonceConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTTypeAnnonceConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonTAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonTTypeAnnonceConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, CommonTTypeAnnonceConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTTypeAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTTypeAnnonceConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTTypeAnnonceConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTTypeAnnonceConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTAnnonceConsultation) to $obj2 (CommonTTypeAnnonceConsultation)
                $obj2->addCommonTAnnonceConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTAnnonceConsultationPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, CommonTTypeAnnonceConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTAnnonceConsultation objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonTAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTTypeAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTAnnonceConsultationPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, CommonTTypeAnnonceConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonConsultation rows

            $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTAnnonceConsultation) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonTAnnonceConsultation($obj1);
            } // if joined row not null

            // Add objects for joined CommonTTypeAnnonceConsultation rows

            $key3 = CommonTTypeAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonTTypeAnnonceConsultationPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonTTypeAnnonceConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTTypeAnnonceConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonTAnnonceConsultation) to the collection in $obj3 (CommonTTypeAnnonceConsultation)
                $obj3->addCommonTAnnonceConsultation($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, CommonTTypeAnnonceConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTTypeAnnonceConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTTypeAnnonceConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTAnnonceConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTAnnonceConsultationPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTAnnonceConsultation objects pre-filled with all related objects except CommonConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonTAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTTypeAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, CommonTTypeAnnonceConsultationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTTypeAnnonceConsultation rows

                $key2 = CommonTTypeAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTTypeAnnonceConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTTypeAnnonceConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTTypeAnnonceConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTAnnonceConsultation) to the collection in $obj2 (CommonTTypeAnnonceConsultation)
                $obj2->addCommonTAnnonceConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTAnnonceConsultation objects pre-filled with all related objects except CommonTTypeAnnonceConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTAnnonceConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTTypeAnnonceConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);
        }

        CommonTAnnonceConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonTAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTAnnonceConsultationPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTAnnonceConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTAnnonceConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTAnnonceConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTAnnonceConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonConsultation rows

                $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTAnnonceConsultation) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonTAnnonceConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTAnnonceConsultationPeer::DATABASE_NAME)->getTable(CommonTAnnonceConsultationPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTAnnonceConsultationPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTAnnonceConsultationPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTAnnonceConsultationTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTAnnonceConsultationPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTAnnonceConsultation or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTAnnonceConsultation object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTAnnonceConsultation object
        }

        if ($criteria->containsKey(CommonTAnnonceConsultationPeer::ID) && $criteria->keyContainsValue(CommonTAnnonceConsultationPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTAnnonceConsultationPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTAnnonceConsultation or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTAnnonceConsultation object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTAnnonceConsultationPeer::ID);
            $value = $criteria->remove(CommonTAnnonceConsultationPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTAnnonceConsultationPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTAnnonceConsultationPeer::TABLE_NAME);
            }

        } else { // $values is CommonTAnnonceConsultation object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_annonce_consultation table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += CommonTAnnonceConsultationPeer::doOnDeleteCascade(new Criteria(CommonTAnnonceConsultationPeer::DATABASE_NAME), $con);
            $affectedRows += BasePeer::doDeleteAll(CommonTAnnonceConsultationPeer::TABLE_NAME, $con, CommonTAnnonceConsultationPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTAnnonceConsultationPeer::clearInstancePool();
            CommonTAnnonceConsultationPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTAnnonceConsultation or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTAnnonceConsultation object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTAnnonceConsultation) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTAnnonceConsultationPeer::DATABASE_NAME);
            $criteria->add(CommonTAnnonceConsultationPeer::ID, (array) $values, Criteria::IN);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += CommonTAnnonceConsultationPeer::doOnDeleteCascade($c, $con);

            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            if ($values instanceof Criteria) {
                CommonTAnnonceConsultationPeer::clearInstancePool();
            } elseif ($values instanceof CommonTAnnonceConsultation) { // it's a model object
                CommonTAnnonceConsultationPeer::removeInstanceFromPool($values);
            } else { // it's a primary key, or an array of pks
                foreach ((array) $values as $singleval) {
                    CommonTAnnonceConsultationPeer::removeInstanceFromPool($singleval);
                }
            }

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTAnnonceConsultationPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected static function doOnDeleteCascade(Criteria $criteria, PropelPDO $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $criteria
        $objects = CommonTAnnonceConsultationPeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {


            // delete related CommonTHistoriqueAnnonce objects
            $criteria = new Criteria(CommonTHistoriqueAnnoncePeer::DATABASE_NAME);

            $criteria->add(CommonTHistoriqueAnnoncePeer::ID_ANNONCE, $obj->getId());
            $affectedRows += CommonTHistoriqueAnnoncePeer::doDelete($criteria, $con);

            // delete related CommonTSupportAnnonceConsultation objects
            $criteria = new Criteria(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

            $criteria->add(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, $obj->getId());
            $affectedRows += CommonTSupportAnnonceConsultationPeer::doDelete($criteria, $con);
        }

        return $affectedRows;
    }

    /**
     * Validates all modified columns of given CommonTAnnonceConsultation object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTAnnonceConsultation $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTAnnonceConsultationPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTAnnonceConsultationPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTAnnonceConsultationPeer::DATABASE_NAME, CommonTAnnonceConsultationPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTAnnonceConsultation
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTAnnonceConsultationPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTAnnonceConsultationPeer::DATABASE_NAME);
        $criteria->add(CommonTAnnonceConsultationPeer::ID, $pk);

        $v = CommonTAnnonceConsultationPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTAnnonceConsultation[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTAnnonceConsultationPeer::DATABASE_NAME);
            $criteria->add(CommonTAnnonceConsultationPeer::ID, $pks, Criteria::IN);
            $objs = CommonTAnnonceConsultationPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTAnnonceConsultationPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTAnnonceConsultationPeer::buildTableMap();

