<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTTypeContratEtProcedure;
use Application\Propel\Mpe\CommonTTypeContratEtProcedurePeer;
use Application\Propel\Mpe\CommonTTypeContratEtProcedureQuery;

/**
 * Base class that represents a query for the 't_type_contrat_et_procedure' table.
 *
 *
 *
 * @method CommonTTypeContratEtProcedureQuery orderByIdTypeContratEtProcedure($order = Criteria::ASC) Order by the id_type_contrat_et_procedure column
 * @method CommonTTypeContratEtProcedureQuery orderByIdTypeContrat($order = Criteria::ASC) Order by the id_type_contrat column
 * @method CommonTTypeContratEtProcedureQuery orderByIdTypeProcedure($order = Criteria::ASC) Order by the id_type_procedure column
 * @method CommonTTypeContratEtProcedureQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 *
 * @method CommonTTypeContratEtProcedureQuery groupByIdTypeContratEtProcedure() Group by the id_type_contrat_et_procedure column
 * @method CommonTTypeContratEtProcedureQuery groupByIdTypeContrat() Group by the id_type_contrat column
 * @method CommonTTypeContratEtProcedureQuery groupByIdTypeProcedure() Group by the id_type_procedure column
 * @method CommonTTypeContratEtProcedureQuery groupByOrganisme() Group by the organisme column
 *
 * @method CommonTTypeContratEtProcedureQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTTypeContratEtProcedureQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTTypeContratEtProcedureQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTTypeContratEtProcedure findOne(PropelPDO $con = null) Return the first CommonTTypeContratEtProcedure matching the query
 * @method CommonTTypeContratEtProcedure findOneOrCreate(PropelPDO $con = null) Return the first CommonTTypeContratEtProcedure matching the query, or a new CommonTTypeContratEtProcedure object populated from the query conditions when no match is found
 *
 * @method CommonTTypeContratEtProcedure findOneByIdTypeContrat(int $id_type_contrat) Return the first CommonTTypeContratEtProcedure filtered by the id_type_contrat column
 * @method CommonTTypeContratEtProcedure findOneByIdTypeProcedure(int $id_type_procedure) Return the first CommonTTypeContratEtProcedure filtered by the id_type_procedure column
 * @method CommonTTypeContratEtProcedure findOneByOrganisme(string $organisme) Return the first CommonTTypeContratEtProcedure filtered by the organisme column
 *
 * @method array findByIdTypeContratEtProcedure(int $id_type_contrat_et_procedure) Return CommonTTypeContratEtProcedure objects filtered by the id_type_contrat_et_procedure column
 * @method array findByIdTypeContrat(int $id_type_contrat) Return CommonTTypeContratEtProcedure objects filtered by the id_type_contrat column
 * @method array findByIdTypeProcedure(int $id_type_procedure) Return CommonTTypeContratEtProcedure objects filtered by the id_type_procedure column
 * @method array findByOrganisme(string $organisme) Return CommonTTypeContratEtProcedure objects filtered by the organisme column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTypeContratEtProcedureQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTTypeContratEtProcedureQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTTypeContratEtProcedure', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTTypeContratEtProcedureQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTTypeContratEtProcedureQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTTypeContratEtProcedureQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTTypeContratEtProcedureQuery) {
            return $criteria;
        }
        $query = new CommonTTypeContratEtProcedureQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTTypeContratEtProcedure|CommonTTypeContratEtProcedure[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTTypeContratEtProcedurePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratEtProcedurePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeContratEtProcedure A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdTypeContratEtProcedure($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeContratEtProcedure A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_type_contrat_et_procedure`, `id_type_contrat`, `id_type_procedure`, `organisme` FROM `t_type_contrat_et_procedure` WHERE `id_type_contrat_et_procedure` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTTypeContratEtProcedure();
            $obj->hydrate($row);
            CommonTTypeContratEtProcedurePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTTypeContratEtProcedure|CommonTTypeContratEtProcedure[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTTypeContratEtProcedure[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTTypeContratEtProcedureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_CONTRAT_ET_PROCEDURE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTTypeContratEtProcedureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_CONTRAT_ET_PROCEDURE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_type_contrat_et_procedure column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeContratEtProcedure(1234); // WHERE id_type_contrat_et_procedure = 1234
     * $query->filterByIdTypeContratEtProcedure(array(12, 34)); // WHERE id_type_contrat_et_procedure IN (12, 34)
     * $query->filterByIdTypeContratEtProcedure(array('min' => 12)); // WHERE id_type_contrat_et_procedure >= 12
     * $query->filterByIdTypeContratEtProcedure(array('max' => 12)); // WHERE id_type_contrat_et_procedure <= 12
     * </code>
     *
     * @param     mixed $idTypeContratEtProcedure The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratEtProcedureQuery The current query, for fluid interface
     */
    public function filterByIdTypeContratEtProcedure($idTypeContratEtProcedure = null, $comparison = null)
    {
        if (is_array($idTypeContratEtProcedure)) {
            $useMinMax = false;
            if (isset($idTypeContratEtProcedure['min'])) {
                $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_CONTRAT_ET_PROCEDURE, $idTypeContratEtProcedure['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeContratEtProcedure['max'])) {
                $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_CONTRAT_ET_PROCEDURE, $idTypeContratEtProcedure['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_CONTRAT_ET_PROCEDURE, $idTypeContratEtProcedure, $comparison);
    }

    /**
     * Filter the query on the id_type_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeContrat(1234); // WHERE id_type_contrat = 1234
     * $query->filterByIdTypeContrat(array(12, 34)); // WHERE id_type_contrat IN (12, 34)
     * $query->filterByIdTypeContrat(array('min' => 12)); // WHERE id_type_contrat >= 12
     * $query->filterByIdTypeContrat(array('max' => 12)); // WHERE id_type_contrat <= 12
     * </code>
     *
     * @param     mixed $idTypeContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratEtProcedureQuery The current query, for fluid interface
     */
    public function filterByIdTypeContrat($idTypeContrat = null, $comparison = null)
    {
        if (is_array($idTypeContrat)) {
            $useMinMax = false;
            if (isset($idTypeContrat['min'])) {
                $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_CONTRAT, $idTypeContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeContrat['max'])) {
                $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_CONTRAT, $idTypeContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_CONTRAT, $idTypeContrat, $comparison);
    }

    /**
     * Filter the query on the id_type_procedure column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeProcedure(1234); // WHERE id_type_procedure = 1234
     * $query->filterByIdTypeProcedure(array(12, 34)); // WHERE id_type_procedure IN (12, 34)
     * $query->filterByIdTypeProcedure(array('min' => 12)); // WHERE id_type_procedure >= 12
     * $query->filterByIdTypeProcedure(array('max' => 12)); // WHERE id_type_procedure <= 12
     * </code>
     *
     * @param     mixed $idTypeProcedure The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratEtProcedureQuery The current query, for fluid interface
     */
    public function filterByIdTypeProcedure($idTypeProcedure = null, $comparison = null)
    {
        if (is_array($idTypeProcedure)) {
            $useMinMax = false;
            if (isset($idTypeProcedure['min'])) {
                $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_PROCEDURE, $idTypeProcedure['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeProcedure['max'])) {
                $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_PROCEDURE, $idTypeProcedure['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_PROCEDURE, $idTypeProcedure, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratEtProcedureQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTTypeContratEtProcedure $commonTTypeContratEtProcedure Object to remove from the list of results
     *
     * @return CommonTTypeContratEtProcedureQuery The current query, for fluid interface
     */
    public function prune($commonTTypeContratEtProcedure = null)
    {
        if ($commonTTypeContratEtProcedure) {
            $this->addUsingAlias(CommonTTypeContratEtProcedurePeer::ID_TYPE_CONTRAT_ET_PROCEDURE, $commonTTypeContratEtProcedure->getIdTypeContratEtProcedure(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
