<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTGroupementEntrepriseQuery;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprisePeer;
use Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery;
use Application\Propel\Mpe\CommonTRoleJuridique;
use Application\Propel\Mpe\CommonTRoleJuridiqueQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntrepriseQuery;

/**
 * Base class that represents a row from the 't_membre_groupement_entreprise' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTMembreGroupementEntreprise extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTMembreGroupementEntreprisePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTMembreGroupementEntreprisePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_membre_groupement_entreprise field.
     * @var        int
     */
    protected $id_membre_groupement_entreprise;

    /**
     * The value for the id_role_juridique field.
     * @var        int
     */
    protected $id_role_juridique;

    /**
     * The value for the id_groupement_entreprise field.
     * @var        int
     */
    protected $id_groupement_entreprise;

    /**
     * The value for the id_entreprise field.
     * @var        int
     */
    protected $id_entreprise;

    /**
     * The value for the id_etablissement field.
     * @var        int
     */
    protected $id_etablissement;

    /**
     * The value for the id_membre_parent field.
     * @var        int
     */
    protected $id_membre_parent;

    /**
     * The value for the numerosn field.
     * @var        string
     */
    protected $numerosn;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * @var        Entreprise
     */
    protected $aEntreprise;

    /**
     * @var        CommonTEtablissement
     */
    protected $aCommonTEtablissement;

    /**
     * @var        CommonTGroupementEntreprise
     */
    protected $aCommonTGroupementEntreprise;

    /**
     * @var        CommonTMembreGroupementEntreprise
     */
    protected $aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent;

    /**
     * @var        CommonTRoleJuridique
     */
    protected $aCommonTRoleJuridique;

    /**
     * @var        PropelObjectCollection|CommonTMembreGroupementEntreprise[] Collection to store aggregation of CommonTMembreGroupementEntreprise objects.
     */
    protected $collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise;
    protected $collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion = null;

    /**
     * Get the [id_membre_groupement_entreprise] column value.
     *
     * @return int
     */
    public function getIdMembreGroupementEntreprise()
    {

        return $this->id_membre_groupement_entreprise;
    }

    /**
     * Get the [id_role_juridique] column value.
     *
     * @return int
     */
    public function getIdRoleJuridique()
    {

        return $this->id_role_juridique;
    }

    /**
     * Get the [id_groupement_entreprise] column value.
     *
     * @return int
     */
    public function getIdGroupementEntreprise()
    {

        return $this->id_groupement_entreprise;
    }

    /**
     * Get the [id_entreprise] column value.
     *
     * @return int
     */
    public function getIdEntreprise()
    {

        return $this->id_entreprise;
    }

    /**
     * Get the [id_etablissement] column value.
     *
     * @return int
     */
    public function getIdEtablissement()
    {

        return $this->id_etablissement;
    }

    /**
     * Get the [id_membre_parent] column value.
     *
     * @return int
     */
    public function getIdMembreParent()
    {

        return $this->id_membre_parent;
    }

    /**
     * Get the [numerosn] column value.
     *
     * @return string
     */
    public function getNumerosn()
    {

        return $this->numerosn;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Set the value of [id_membre_groupement_entreprise] column.
     *
     * @param int $v new value
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdMembreGroupementEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_membre_groupement_entreprise !== $v) {
            $this->id_membre_groupement_entreprise = $v;
            $this->modifiedColumns[] = CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE;
        }


        return $this;
    } // setIdMembreGroupementEntreprise()

    /**
     * Set the value of [id_role_juridique] column.
     *
     * @param int $v new value
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdRoleJuridique($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_role_juridique !== $v) {
            $this->id_role_juridique = $v;
            $this->modifiedColumns[] = CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE;
        }

        if ($this->aCommonTRoleJuridique !== null && $this->aCommonTRoleJuridique->getIdRoleJuridique() !== $v) {
            $this->aCommonTRoleJuridique = null;
        }


        return $this;
    } // setIdRoleJuridique()

    /**
     * Set the value of [id_groupement_entreprise] column.
     *
     * @param int $v new value
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdGroupementEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_groupement_entreprise !== $v) {
            $this->id_groupement_entreprise = $v;
            $this->modifiedColumns[] = CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE;
        }

        if ($this->aCommonTGroupementEntreprise !== null && $this->aCommonTGroupementEntreprise->getIdGroupementEntreprise() !== $v) {
            $this->aCommonTGroupementEntreprise = null;
        }


        return $this;
    } // setIdGroupementEntreprise()

    /**
     * Set the value of [id_entreprise] column.
     *
     * @param int $v new value
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_entreprise !== $v) {
            $this->id_entreprise = $v;
            $this->modifiedColumns[] = CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE;
        }

        if ($this->aEntreprise !== null && $this->aEntreprise->getId() !== $v) {
            $this->aEntreprise = null;
        }


        return $this;
    } // setIdEntreprise()

    /**
     * Set the value of [id_etablissement] column.
     *
     * @param int $v new value
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_etablissement !== $v) {
            $this->id_etablissement = $v;
            $this->modifiedColumns[] = CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT;
        }

        if ($this->aCommonTEtablissement !== null && $this->aCommonTEtablissement->getIdEtablissement() !== $v) {
            $this->aCommonTEtablissement = null;
        }


        return $this;
    } // setIdEtablissement()

    /**
     * Set the value of [id_membre_parent] column.
     *
     * @param int $v new value
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function setIdMembreParent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_membre_parent !== $v) {
            $this->id_membre_parent = $v;
            $this->modifiedColumns[] = CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT;
        }

        if ($this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent !== null && $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->getIdMembreGroupementEntreprise() !== $v) {
            $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent = null;
        }


        return $this;
    } // setIdMembreParent()

    /**
     * Set the value of [numerosn] column.
     *
     * @param string $v new value
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function setNumerosn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->numerosn !== $v) {
            $this->numerosn = $v;
            $this->modifiedColumns[] = CommonTMembreGroupementEntreprisePeer::NUMEROSN;
        }


        return $this;
    } // setNumerosn()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = CommonTMembreGroupementEntreprisePeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_membre_groupement_entreprise = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_role_juridique = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_groupement_entreprise = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->id_entreprise = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->id_etablissement = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->id_membre_parent = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->numerosn = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->email = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 8; // 8 = CommonTMembreGroupementEntreprisePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTMembreGroupementEntreprise object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTRoleJuridique !== null && $this->id_role_juridique !== $this->aCommonTRoleJuridique->getIdRoleJuridique()) {
            $this->aCommonTRoleJuridique = null;
        }
        if ($this->aCommonTGroupementEntreprise !== null && $this->id_groupement_entreprise !== $this->aCommonTGroupementEntreprise->getIdGroupementEntreprise()) {
            $this->aCommonTGroupementEntreprise = null;
        }
        if ($this->aEntreprise !== null && $this->id_entreprise !== $this->aEntreprise->getId()) {
            $this->aEntreprise = null;
        }
        if ($this->aCommonTEtablissement !== null && $this->id_etablissement !== $this->aCommonTEtablissement->getIdEtablissement()) {
            $this->aCommonTEtablissement = null;
        }
        if ($this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent !== null && $this->id_membre_parent !== $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->getIdMembreGroupementEntreprise()) {
            $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTMembreGroupementEntreprisePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEntreprise = null;
            $this->aCommonTEtablissement = null;
            $this->aCommonTGroupementEntreprise = null;
            $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent = null;
            $this->aCommonTRoleJuridique = null;
            $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTMembreGroupementEntrepriseQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTMembreGroupementEntreprisePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntreprise !== null) {
                if ($this->aEntreprise->isModified() || $this->aEntreprise->isNew()) {
                    $affectedRows += $this->aEntreprise->save($con);
                }
                $this->setEntreprise($this->aEntreprise);
            }

            if ($this->aCommonTEtablissement !== null) {
                if ($this->aCommonTEtablissement->isModified() || $this->aCommonTEtablissement->isNew()) {
                    $affectedRows += $this->aCommonTEtablissement->save($con);
                }
                $this->setCommonTEtablissement($this->aCommonTEtablissement);
            }

            if ($this->aCommonTGroupementEntreprise !== null) {
                if ($this->aCommonTGroupementEntreprise->isModified() || $this->aCommonTGroupementEntreprise->isNew()) {
                    $affectedRows += $this->aCommonTGroupementEntreprise->save($con);
                }
                $this->setCommonTGroupementEntreprise($this->aCommonTGroupementEntreprise);
            }

            if ($this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent !== null) {
                if ($this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->isModified() || $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->isNew()) {
                    $affectedRows += $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->save($con);
                }
                $this->setCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent);
            }

            if ($this->aCommonTRoleJuridique !== null) {
                if ($this->aCommonTRoleJuridique->isModified() || $this->aCommonTRoleJuridique->isNew()) {
                    $affectedRows += $this->aCommonTRoleJuridique->save($con);
                }
                $this->setCommonTRoleJuridique($this->aCommonTRoleJuridique);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion !== null) {
                if (!$this->commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion as $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise) {
                        // need to save related object because we set the relation to null
                        $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise->save($con);
                    }
                    $this->commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise !== null) {
                foreach ($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE;
        if (null !== $this->id_membre_groupement_entreprise) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_membre_groupement_entreprise`';
        }
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`id_role_juridique`';
        }
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_groupement_entreprise`';
        }
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entreprise`';
        }
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_etablissement`';
        }
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_membre_parent`';
        }
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::NUMEROSN)) {
            $modifiedColumns[':p' . $index++]  = '`numeroSN`';
        }
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }

        $sql = sprintf(
            'INSERT INTO `t_membre_groupement_entreprise` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_membre_groupement_entreprise`':
                        $stmt->bindValue($identifier, $this->id_membre_groupement_entreprise, PDO::PARAM_INT);
                        break;
                    case '`id_role_juridique`':
                        $stmt->bindValue($identifier, $this->id_role_juridique, PDO::PARAM_INT);
                        break;
                    case '`id_groupement_entreprise`':
                        $stmt->bindValue($identifier, $this->id_groupement_entreprise, PDO::PARAM_INT);
                        break;
                    case '`id_entreprise`':
                        $stmt->bindValue($identifier, $this->id_entreprise, PDO::PARAM_INT);
                        break;
                    case '`id_etablissement`':
                        $stmt->bindValue($identifier, $this->id_etablissement, PDO::PARAM_INT);
                        break;
                    case '`id_membre_parent`':
                        $stmt->bindValue($identifier, $this->id_membre_parent, PDO::PARAM_INT);
                        break;
                    case '`numeroSN`':
                        $stmt->bindValue($identifier, $this->numerosn, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdMembreGroupementEntreprise($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntreprise !== null) {
                if (!$this->aEntreprise->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aEntreprise->getValidationFailures());
                }
            }

            if ($this->aCommonTEtablissement !== null) {
                if (!$this->aCommonTEtablissement->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTEtablissement->getValidationFailures());
                }
            }

            if ($this->aCommonTGroupementEntreprise !== null) {
                if (!$this->aCommonTGroupementEntreprise->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTGroupementEntreprise->getValidationFailures());
                }
            }

            if ($this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent !== null) {
                if (!$this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->getValidationFailures());
                }
            }

            if ($this->aCommonTRoleJuridique !== null) {
                if (!$this->aCommonTRoleJuridique->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTRoleJuridique->getValidationFailures());
                }
            }


            if (($retval = CommonTMembreGroupementEntreprisePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise !== null) {
                    foreach ($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTMembreGroupementEntreprisePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdMembreGroupementEntreprise();
                break;
            case 1:
                return $this->getIdRoleJuridique();
                break;
            case 2:
                return $this->getIdGroupementEntreprise();
                break;
            case 3:
                return $this->getIdEntreprise();
                break;
            case 4:
                return $this->getIdEtablissement();
                break;
            case 5:
                return $this->getIdMembreParent();
                break;
            case 6:
                return $this->getNumerosn();
                break;
            case 7:
                return $this->getEmail();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTMembreGroupementEntreprise'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTMembreGroupementEntreprise'][$this->getPrimaryKey()] = true;
        $keys = CommonTMembreGroupementEntreprisePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdMembreGroupementEntreprise(),
            $keys[1] => $this->getIdRoleJuridique(),
            $keys[2] => $this->getIdGroupementEntreprise(),
            $keys[3] => $this->getIdEntreprise(),
            $keys[4] => $this->getIdEtablissement(),
            $keys[5] => $this->getIdMembreParent(),
            $keys[6] => $this->getNumerosn(),
            $keys[7] => $this->getEmail(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aEntreprise) {
                $result['Entreprise'] = $this->aEntreprise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTEtablissement) {
                $result['CommonTEtablissement'] = $this->aCommonTEtablissement->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTGroupementEntreprise) {
                $result['CommonTGroupementEntreprise'] = $this->aCommonTGroupementEntreprise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent) {
                $result['CommonTMembreGroupementEntrepriseRelatedByIdMembreParent'] = $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTRoleJuridique) {
                $result['CommonTRoleJuridique'] = $this->aCommonTRoleJuridique->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise) {
                $result['CommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise'] = $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTMembreGroupementEntreprisePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdMembreGroupementEntreprise($value);
                break;
            case 1:
                $this->setIdRoleJuridique($value);
                break;
            case 2:
                $this->setIdGroupementEntreprise($value);
                break;
            case 3:
                $this->setIdEntreprise($value);
                break;
            case 4:
                $this->setIdEtablissement($value);
                break;
            case 5:
                $this->setIdMembreParent($value);
                break;
            case 6:
                $this->setNumerosn($value);
                break;
            case 7:
                $this->setEmail($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTMembreGroupementEntreprisePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdMembreGroupementEntreprise($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdRoleJuridique($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdGroupementEntreprise($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIdEntreprise($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdEtablissement($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setIdMembreParent($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setNumerosn($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setEmail($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE)) $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $this->id_membre_groupement_entreprise);
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE)) $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, $this->id_role_juridique);
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE)) $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $this->id_groupement_entreprise);
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE)) $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, $this->id_entreprise);
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT)) $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, $this->id_etablissement);
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT)) $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT, $this->id_membre_parent);
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::NUMEROSN)) $criteria->add(CommonTMembreGroupementEntreprisePeer::NUMEROSN, $this->numerosn);
        if ($this->isColumnModified(CommonTMembreGroupementEntreprisePeer::EMAIL)) $criteria->add(CommonTMembreGroupementEntreprisePeer::EMAIL, $this->email);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME);
        $criteria->add(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $this->id_membre_groupement_entreprise);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdMembreGroupementEntreprise();
    }

    /**
     * Generic method to set the primary key (id_membre_groupement_entreprise column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdMembreGroupementEntreprise($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdMembreGroupementEntreprise();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTMembreGroupementEntreprise (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdRoleJuridique($this->getIdRoleJuridique());
        $copyObj->setIdGroupementEntreprise($this->getIdGroupementEntreprise());
        $copyObj->setIdEntreprise($this->getIdEntreprise());
        $copyObj->setIdEtablissement($this->getIdEtablissement());
        $copyObj->setIdMembreParent($this->getIdMembreParent());
        $copyObj->setNumerosn($this->getNumerosn());
        $copyObj->setEmail($this->getEmail());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdMembreGroupementEntreprise(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTMembreGroupementEntreprise Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTMembreGroupementEntreprisePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTMembreGroupementEntreprisePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Entreprise object.
     *
     * @param   Entreprise $v
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntreprise(Entreprise $v = null)
    {
        if ($v === null) {
            $this->setIdEntreprise(NULL);
        } else {
            $this->setIdEntreprise($v->getId());
        }

        $this->aEntreprise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Entreprise object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTMembreGroupementEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated Entreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Entreprise The associated Entreprise object.
     * @throws PropelException
     */
    public function getEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aEntreprise === null && ($this->id_entreprise !== null) && $doQuery) {
            $this->aEntreprise = EntrepriseQuery::create()->findPk($this->id_entreprise, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntreprise->addCommonTMembreGroupementEntreprises($this);
             */
        }

        return $this->aEntreprise;
    }

    /**
     * Declares an association between this object and a CommonTEtablissement object.
     *
     * @param   CommonTEtablissement $v
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTEtablissement(CommonTEtablissement $v = null)
    {
        if ($v === null) {
            $this->setIdEtablissement(NULL);
        } else {
            $this->setIdEtablissement($v->getIdEtablissement());
        }

        $this->aCommonTEtablissement = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTEtablissement object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTMembreGroupementEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTEtablissement object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTEtablissement The associated CommonTEtablissement object.
     * @throws PropelException
     */
    public function getCommonTEtablissement(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTEtablissement === null && ($this->id_etablissement !== null) && $doQuery) {
            $this->aCommonTEtablissement = CommonTEtablissementQuery::create()->findPk($this->id_etablissement, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTEtablissement->addCommonTMembreGroupementEntreprises($this);
             */
        }

        return $this->aCommonTEtablissement;
    }

    /**
     * Declares an association between this object and a CommonTGroupementEntreprise object.
     *
     * @param   CommonTGroupementEntreprise $v
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTGroupementEntreprise(CommonTGroupementEntreprise $v = null)
    {
        if ($v === null) {
            $this->setIdGroupementEntreprise(NULL);
        } else {
            $this->setIdGroupementEntreprise($v->getIdGroupementEntreprise());
        }

        $this->aCommonTGroupementEntreprise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTGroupementEntreprise object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTMembreGroupementEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTGroupementEntreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTGroupementEntreprise The associated CommonTGroupementEntreprise object.
     * @throws PropelException
     */
    public function getCommonTGroupementEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTGroupementEntreprise === null && ($this->id_groupement_entreprise !== null) && $doQuery) {
            $this->aCommonTGroupementEntreprise = CommonTGroupementEntrepriseQuery::create()->findPk($this->id_groupement_entreprise, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTGroupementEntreprise->addCommonTMembreGroupementEntreprises($this);
             */
        }

        return $this->aCommonTGroupementEntreprise;
    }

    /**
     * Declares an association between this object and a CommonTMembreGroupementEntreprise object.
     *
     * @param   CommonTMembreGroupementEntreprise $v
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTMembreGroupementEntrepriseRelatedByIdMembreParent(CommonTMembreGroupementEntreprise $v = null)
    {
        if ($v === null) {
            $this->setIdMembreParent(NULL);
        } else {
            $this->setIdMembreParent($v->getIdMembreGroupementEntreprise());
        }

        $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTMembreGroupementEntreprise object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTMembreGroupementEntreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTMembreGroupementEntreprise The associated CommonTMembreGroupementEntreprise object.
     * @throws PropelException
     */
    public function getCommonTMembreGroupementEntrepriseRelatedByIdMembreParent(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent === null && ($this->id_membre_parent !== null) && $doQuery) {
            $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent = CommonTMembreGroupementEntrepriseQuery::create()->findPk($this->id_membre_parent, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->addCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise($this);
             */
        }

        return $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent;
    }

    /**
     * Declares an association between this object and a CommonTRoleJuridique object.
     *
     * @param   CommonTRoleJuridique $v
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTRoleJuridique(CommonTRoleJuridique $v = null)
    {
        if ($v === null) {
            $this->setIdRoleJuridique(NULL);
        } else {
            $this->setIdRoleJuridique($v->getIdRoleJuridique());
        }

        $this->aCommonTRoleJuridique = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTRoleJuridique object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTMembreGroupementEntreprise($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTRoleJuridique object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTRoleJuridique The associated CommonTRoleJuridique object.
     * @throws PropelException
     */
    public function getCommonTRoleJuridique(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTRoleJuridique === null && ($this->id_role_juridique !== null) && $doQuery) {
            $this->aCommonTRoleJuridique = CommonTRoleJuridiqueQuery::create()->findPk($this->id_role_juridique, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTRoleJuridique->addCommonTMembreGroupementEntreprises($this);
             */
        }

        return $this->aCommonTRoleJuridique;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise' == $relationName) {
            $this->initCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise();
        }
    }

    /**
     * Clears out the collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     * @see        addCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise()
     */
    public function clearCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise()
    {
        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise($v = true)
    {
        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial = $v;
    }

    /**
     * Initializes the collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise collection.
     *
     * By default this just sets the collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise collection to an empty array (like clearcollCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise($overrideExisting = true)
    {
        if (null !== $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise && !$overrideExisting) {
            return;
        }
        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise = new PropelObjectCollection();
        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise->setModel('CommonTMembreGroupementEntreprise');
    }

    /**
     * Gets an array of CommonTMembreGroupementEntreprise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTMembreGroupementEntreprise is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     * @throws PropelException
     */
    public function getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial && !$this->isNew();
        if (null === $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise) {
                // return empty collection
                $this->initCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise();
            } else {
                $collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria)
                    ->filterByCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial && count($collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise)) {
                      $this->initCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise(false);

                      foreach ($collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise as $obj) {
                        if (false == $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise->contains($obj)) {
                          $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise->append($obj);
                        }
                      }

                      $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial = true;
                    }

                    $collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise->getInternalIterator()->rewind();

                    return $collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise;
                }

                if ($partial && $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise) {
                    foreach ($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise[] = $obj;
                        }
                    }
                }

                $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise = $collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise;
                $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial = false;
            }
        }

        return $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise;
    }

    /**
     * Sets a collection of CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function setCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise(PropelCollection $commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise, PropelPDO $con = null)
    {
        $commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseToDelete = $this->getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise(new Criteria(), $con)->diff($commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise);


        $this->commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion = $commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseToDelete;

        foreach ($commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseToDelete as $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntrepriseRemoved) {
            $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntrepriseRemoved->setCommonTMembreGroupementEntrepriseRelatedByIdMembreParent(null);
        }

        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise = null;
        foreach ($commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise as $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise) {
            $this->addCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise);
        }

        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise = $commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise;
        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTMembreGroupementEntreprise objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTMembreGroupementEntreprise objects.
     * @throws PropelException
     */
    public function countCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial && !$this->isNew();
        if (null === $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise());
            }
            $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($this)
                ->count($con);
        }

        return count($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise);
    }

    /**
     * Method called to associate a CommonTMembreGroupementEntreprise object to this object
     * through the CommonTMembreGroupementEntreprise foreign key attribute.
     *
     * @param   CommonTMembreGroupementEntreprise $l CommonTMembreGroupementEntreprise
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function addCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise(CommonTMembreGroupementEntreprise $l)
    {
        if ($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise === null) {
            $this->initCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise();
            $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprisePartial = true;
        }
        if (!in_array($l, $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($l);
        }

        return $this;
    }

    /**
     * @param	CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise The commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise object to add.
     */
    protected function doAddCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise)
    {
        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise[]= $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise;
        $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise->setCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($this);
    }

    /**
     * @param	CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise The commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise object to remove.
     * @return CommonTMembreGroupementEntreprise The current object (for fluent API support)
     */
    public function removeCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise)
    {
        if ($this->getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise()->contains($commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise)) {
            $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise->remove($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise->search($commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise));
            if (null === $this->commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion) {
                $this->commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion = clone $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise;
                $this->commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion->clear();
            }
            $this->commonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseScheduledForDeletion[]= $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise;
            $commonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise->setCommonTMembreGroupementEntrepriseRelatedByIdMembreParent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTMembreGroupementEntreprise is new, it will return
     * an empty collection; or if this CommonTMembreGroupementEntreprise has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTMembreGroupementEntreprise.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTMembreGroupementEntreprise is new, it will return
     * an empty collection; or if this CommonTMembreGroupementEntreprise has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTMembreGroupementEntreprise.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseJoinCommonTEtablissement($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTEtablissement', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTMembreGroupementEntreprise is new, it will return
     * an empty collection; or if this CommonTMembreGroupementEntreprise has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTMembreGroupementEntreprise.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseJoinCommonTGroupementEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTGroupementEntreprise', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTMembreGroupementEntreprise is new, it will return
     * an empty collection; or if this CommonTMembreGroupementEntreprise has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTMembreGroupementEntreprise.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntrepriseJoinCommonTRoleJuridique($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTRoleJuridique', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_membre_groupement_entreprise = null;
        $this->id_role_juridique = null;
        $this->id_groupement_entreprise = null;
        $this->id_entreprise = null;
        $this->id_etablissement = null;
        $this->id_membre_parent = null;
        $this->numerosn = null;
        $this->email = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise) {
                foreach ($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aEntreprise instanceof Persistent) {
              $this->aEntreprise->clearAllReferences($deep);
            }
            if ($this->aCommonTEtablissement instanceof Persistent) {
              $this->aCommonTEtablissement->clearAllReferences($deep);
            }
            if ($this->aCommonTGroupementEntreprise instanceof Persistent) {
              $this->aCommonTGroupementEntreprise->clearAllReferences($deep);
            }
            if ($this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent instanceof Persistent) {
              $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent->clearAllReferences($deep);
            }
            if ($this->aCommonTRoleJuridique instanceof Persistent) {
              $this->aCommonTRoleJuridique->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise instanceof PropelCollection) {
            $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise->clearIterator();
        }
        $this->collCommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise = null;
        $this->aEntreprise = null;
        $this->aCommonTEtablissement = null;
        $this->aCommonTGroupementEntreprise = null;
        $this->aCommonTMembreGroupementEntrepriseRelatedByIdMembreParent = null;
        $this->aCommonTRoleJuridique = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTMembreGroupementEntreprisePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
