<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTBourseCotraitance;
use Application\Propel\Mpe\CommonTBourseCotraitancePeer;
use Application\Propel\Mpe\CommonTBourseCotraitanceQuery;

/**
 * Base class that represents a row from the 't_bourse_cotraitance' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTBourseCotraitance extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTBourseCotraitancePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTBourseCotraitancePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_auto_bc field.
     * @var        int
     */
    protected $id_auto_bc;

    /**
     * The value for the reference_consultation field.
     * @var        int
     */
    protected $reference_consultation;

    /**
     * The value for the id_entreprise field.
     * @var        int
     */
    protected $id_entreprise;

    /**
     * The value for the id_etablissement_inscrite field.
     * @var        int
     */
    protected $id_etablissement_inscrite;

    /**
     * The value for the nom_inscrit field.
     * @var        string
     */
    protected $nom_inscrit;

    /**
     * The value for the prenom_inscrit field.
     * @var        string
     */
    protected $prenom_inscrit;

    /**
     * The value for the adresse_inscrit field.
     * @var        string
     */
    protected $adresse_inscrit;

    /**
     * The value for the adresse2_incsrit field.
     * @var        string
     */
    protected $adresse2_incsrit;

    /**
     * The value for the cp_inscrit field.
     * @var        string
     */
    protected $cp_inscrit;

    /**
     * The value for the ville_inscrit field.
     * @var        string
     */
    protected $ville_inscrit;

    /**
     * The value for the pays_inscrit field.
     * @var        string
     */
    protected $pays_inscrit;

    /**
     * The value for the fonction_inscrit field.
     * @var        string
     */
    protected $fonction_inscrit;

    /**
     * The value for the email_inscrit field.
     * @var        string
     */
    protected $email_inscrit;

    /**
     * The value for the tel_fixe_inscrit field.
     * @var        string
     */
    protected $tel_fixe_inscrit;

    /**
     * The value for the tel_mobile_inscrit field.
     * @var        string
     */
    protected $tel_mobile_inscrit;

    /**
     * The value for the mandataire_groupement field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $mandataire_groupement;

    /**
     * The value for the cotraitant_solidaire field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $cotraitant_solidaire;

    /**
     * The value for the cotraitant_conjoint field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $cotraitant_conjoint;

    /**
     * The value for the desc_mon_apport_marche field.
     * @var        string
     */
    protected $desc_mon_apport_marche;

    /**
     * The value for the desc_type_cotraitance_recherche field.
     * @var        string
     */
    protected $desc_type_cotraitance_recherche;

    /**
     * The value for the clause_social field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $clause_social;

    /**
     * The value for the entreprise_adapte field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $entreprise_adapte;

    /**
     * The value for the long field.
     * @var        double
     */
    protected $long;

    /**
     * The value for the lat field.
     * @var        double
     */
    protected $lat;

    /**
     * The value for the maj_long_lat field.
     * @var        string
     */
    protected $maj_long_lat;

    /**
     * The value for the sous_traitant field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $sous_traitant;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->mandataire_groupement = '0';
        $this->cotraitant_solidaire = '0';
        $this->cotraitant_conjoint = '0';
        $this->clause_social = '0';
        $this->entreprise_adapte = '0';
        $this->sous_traitant = '0';
    }

    /**
     * Initializes internal state of BaseCommonTBourseCotraitance object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_auto_bc] column value.
     *
     * @return int
     */
    public function getIdAutoBc()
    {

        return $this->id_auto_bc;
    }

    /**
     * Get the [reference_consultation] column value.
     *
     * @return int
     */
    public function getReferenceConsultation()
    {

        return $this->reference_consultation;
    }

    /**
     * Get the [id_entreprise] column value.
     *
     * @return int
     */
    public function getIdEntreprise()
    {

        return $this->id_entreprise;
    }

    /**
     * Get the [id_etablissement_inscrite] column value.
     *
     * @return int
     */
    public function getIdEtablissementInscrite()
    {

        return $this->id_etablissement_inscrite;
    }

    /**
     * Get the [nom_inscrit] column value.
     *
     * @return string
     */
    public function getNomInscrit()
    {

        return $this->nom_inscrit;
    }

    /**
     * Get the [prenom_inscrit] column value.
     *
     * @return string
     */
    public function getPrenomInscrit()
    {

        return $this->prenom_inscrit;
    }

    /**
     * Get the [adresse_inscrit] column value.
     *
     * @return string
     */
    public function getAdresseInscrit()
    {

        return $this->adresse_inscrit;
    }

    /**
     * Get the [adresse2_incsrit] column value.
     *
     * @return string
     */
    public function getAdresse2Incsrit()
    {

        return $this->adresse2_incsrit;
    }

    /**
     * Get the [cp_inscrit] column value.
     *
     * @return string
     */
    public function getCpInscrit()
    {

        return $this->cp_inscrit;
    }

    /**
     * Get the [ville_inscrit] column value.
     *
     * @return string
     */
    public function getVilleInscrit()
    {

        return $this->ville_inscrit;
    }

    /**
     * Get the [pays_inscrit] column value.
     *
     * @return string
     */
    public function getPaysInscrit()
    {

        return $this->pays_inscrit;
    }

    /**
     * Get the [fonction_inscrit] column value.
     *
     * @return string
     */
    public function getFonctionInscrit()
    {

        return $this->fonction_inscrit;
    }

    /**
     * Get the [email_inscrit] column value.
     *
     * @return string
     */
    public function getEmailInscrit()
    {

        return $this->email_inscrit;
    }

    /**
     * Get the [tel_fixe_inscrit] column value.
     *
     * @return string
     */
    public function getTelFixeInscrit()
    {

        return $this->tel_fixe_inscrit;
    }

    /**
     * Get the [tel_mobile_inscrit] column value.
     *
     * @return string
     */
    public function getTelMobileInscrit()
    {

        return $this->tel_mobile_inscrit;
    }

    /**
     * Get the [mandataire_groupement] column value.
     *
     * @return string
     */
    public function getMandataireGroupement()
    {

        return $this->mandataire_groupement;
    }

    /**
     * Get the [cotraitant_solidaire] column value.
     *
     * @return string
     */
    public function getCotraitantSolidaire()
    {

        return $this->cotraitant_solidaire;
    }

    /**
     * Get the [cotraitant_conjoint] column value.
     *
     * @return string
     */
    public function getCotraitantConjoint()
    {

        return $this->cotraitant_conjoint;
    }

    /**
     * Get the [desc_mon_apport_marche] column value.
     *
     * @return string
     */
    public function getDescMonApportMarche()
    {

        return $this->desc_mon_apport_marche;
    }

    /**
     * Get the [desc_type_cotraitance_recherche] column value.
     *
     * @return string
     */
    public function getDescTypeCotraitanceRecherche()
    {

        return $this->desc_type_cotraitance_recherche;
    }

    /**
     * Get the [clause_social] column value.
     *
     * @return string
     */
    public function getClauseSocial()
    {

        return $this->clause_social;
    }

    /**
     * Get the [entreprise_adapte] column value.
     *
     * @return string
     */
    public function getEntrepriseAdapte()
    {

        return $this->entreprise_adapte;
    }

    /**
     * Get the [long] column value.
     *
     * @return double
     */
    public function getLong()
    {

        return $this->long;
    }

    /**
     * Get the [lat] column value.
     *
     * @return double
     */
    public function getLat()
    {

        return $this->lat;
    }

    /**
     * Get the [optionally formatted] temporal [maj_long_lat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getMajLongLat($format = 'Y-m-d H:i:s')
    {
        if ($this->maj_long_lat === null) {
            return null;
        }

        if ($this->maj_long_lat === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->maj_long_lat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->maj_long_lat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [sous_traitant] column value.
     *
     * @return string
     */
    public function getSousTraitant()
    {

        return $this->sous_traitant;
    }

    /**
     * Set the value of [id_auto_bc] column.
     *
     * @param int $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setIdAutoBc($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_auto_bc !== $v) {
            $this->id_auto_bc = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::ID_AUTO_BC;
        }


        return $this;
    } // setIdAutoBc()

    /**
     * Set the value of [reference_consultation] column.
     *
     * @param int $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setReferenceConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->reference_consultation !== $v) {
            $this->reference_consultation = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION;
        }


        return $this;
    } // setReferenceConsultation()

    /**
     * Set the value of [id_entreprise] column.
     *
     * @param int $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setIdEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_entreprise !== $v) {
            $this->id_entreprise = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::ID_ENTREPRISE;
        }


        return $this;
    } // setIdEntreprise()

    /**
     * Set the value of [id_etablissement_inscrite] column.
     *
     * @param int $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setIdEtablissementInscrite($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_etablissement_inscrite !== $v) {
            $this->id_etablissement_inscrite = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE;
        }


        return $this;
    } // setIdEtablissementInscrite()

    /**
     * Set the value of [nom_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setNomInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_inscrit !== $v) {
            $this->nom_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::NOM_INSCRIT;
        }


        return $this;
    } // setNomInscrit()

    /**
     * Set the value of [prenom_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setPrenomInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prenom_inscrit !== $v) {
            $this->prenom_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::PRENOM_INSCRIT;
        }


        return $this;
    } // setPrenomInscrit()

    /**
     * Set the value of [adresse_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setAdresseInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_inscrit !== $v) {
            $this->adresse_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::ADRESSE_INSCRIT;
        }


        return $this;
    } // setAdresseInscrit()

    /**
     * Set the value of [adresse2_incsrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setAdresse2Incsrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_incsrit !== $v) {
            $this->adresse2_incsrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::ADRESSE2_INCSRIT;
        }


        return $this;
    } // setAdresse2Incsrit()

    /**
     * Set the value of [cp_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setCpInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->cp_inscrit !== $v) {
            $this->cp_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::CP_INSCRIT;
        }


        return $this;
    } // setCpInscrit()

    /**
     * Set the value of [ville_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setVilleInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_inscrit !== $v) {
            $this->ville_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::VILLE_INSCRIT;
        }


        return $this;
    } // setVilleInscrit()

    /**
     * Set the value of [pays_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setPaysInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_inscrit !== $v) {
            $this->pays_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::PAYS_INSCRIT;
        }


        return $this;
    } // setPaysInscrit()

    /**
     * Set the value of [fonction_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setFonctionInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->fonction_inscrit !== $v) {
            $this->fonction_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::FONCTION_INSCRIT;
        }


        return $this;
    } // setFonctionInscrit()

    /**
     * Set the value of [email_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setEmailInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email_inscrit !== $v) {
            $this->email_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::EMAIL_INSCRIT;
        }


        return $this;
    } // setEmailInscrit()

    /**
     * Set the value of [tel_fixe_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setTelFixeInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tel_fixe_inscrit !== $v) {
            $this->tel_fixe_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::TEL_FIXE_INSCRIT;
        }


        return $this;
    } // setTelFixeInscrit()

    /**
     * Set the value of [tel_mobile_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setTelMobileInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tel_mobile_inscrit !== $v) {
            $this->tel_mobile_inscrit = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::TEL_MOBILE_INSCRIT;
        }


        return $this;
    } // setTelMobileInscrit()

    /**
     * Set the value of [mandataire_groupement] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setMandataireGroupement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mandataire_groupement !== $v) {
            $this->mandataire_groupement = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT;
        }


        return $this;
    } // setMandataireGroupement()

    /**
     * Set the value of [cotraitant_solidaire] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setCotraitantSolidaire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->cotraitant_solidaire !== $v) {
            $this->cotraitant_solidaire = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE;
        }


        return $this;
    } // setCotraitantSolidaire()

    /**
     * Set the value of [cotraitant_conjoint] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setCotraitantConjoint($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->cotraitant_conjoint !== $v) {
            $this->cotraitant_conjoint = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT;
        }


        return $this;
    } // setCotraitantConjoint()

    /**
     * Set the value of [desc_mon_apport_marche] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setDescMonApportMarche($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->desc_mon_apport_marche !== $v) {
            $this->desc_mon_apport_marche = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::DESC_MON_APPORT_MARCHE;
        }


        return $this;
    } // setDescMonApportMarche()

    /**
     * Set the value of [desc_type_cotraitance_recherche] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setDescTypeCotraitanceRecherche($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->desc_type_cotraitance_recherche !== $v) {
            $this->desc_type_cotraitance_recherche = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::DESC_TYPE_COTRAITANCE_RECHERCHE;
        }


        return $this;
    } // setDescTypeCotraitanceRecherche()

    /**
     * Set the value of [clause_social] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setClauseSocial($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_social !== $v) {
            $this->clause_social = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::CLAUSE_SOCIAL;
        }


        return $this;
    } // setClauseSocial()

    /**
     * Set the value of [entreprise_adapte] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setEntrepriseAdapte($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->entreprise_adapte !== $v) {
            $this->entreprise_adapte = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE;
        }


        return $this;
    } // setEntrepriseAdapte()

    /**
     * Set the value of [long] column.
     *
     * @param double $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setLong($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->long !== $v) {
            $this->long = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::LONG;
        }


        return $this;
    } // setLong()

    /**
     * Set the value of [lat] column.
     *
     * @param double $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setLat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->lat !== $v) {
            $this->lat = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::LAT;
        }


        return $this;
    } // setLat()

    /**
     * Sets the value of [maj_long_lat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setMajLongLat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->maj_long_lat !== null || $dt !== null) {
            $currentDateAsString = ($this->maj_long_lat !== null && $tmpDt = new DateTime($this->maj_long_lat)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->maj_long_lat = $newDateAsString;
                $this->modifiedColumns[] = CommonTBourseCotraitancePeer::MAJ_LONG_LAT;
            }
        } // if either are not null


        return $this;
    } // setMajLongLat()

    /**
     * Set the value of [sous_traitant] column.
     *
     * @param string $v new value
     * @return CommonTBourseCotraitance The current object (for fluent API support)
     */
    public function setSousTraitant($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sous_traitant !== $v) {
            $this->sous_traitant = $v;
            $this->modifiedColumns[] = CommonTBourseCotraitancePeer::SOUS_TRAITANT;
        }


        return $this;
    } // setSousTraitant()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->mandataire_groupement !== '0') {
                return false;
            }

            if ($this->cotraitant_solidaire !== '0') {
                return false;
            }

            if ($this->cotraitant_conjoint !== '0') {
                return false;
            }

            if ($this->clause_social !== '0') {
                return false;
            }

            if ($this->entreprise_adapte !== '0') {
                return false;
            }

            if ($this->sous_traitant !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_auto_bc = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->reference_consultation = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_entreprise = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->id_etablissement_inscrite = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->nom_inscrit = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->prenom_inscrit = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->adresse_inscrit = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->adresse2_incsrit = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->cp_inscrit = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->ville_inscrit = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->pays_inscrit = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->fonction_inscrit = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->email_inscrit = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->tel_fixe_inscrit = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->tel_mobile_inscrit = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->mandataire_groupement = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->cotraitant_solidaire = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->cotraitant_conjoint = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->desc_mon_apport_marche = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->desc_type_cotraitance_recherche = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->clause_social = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->entreprise_adapte = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->long = ($row[$startcol + 22] !== null) ? (double) $row[$startcol + 22] : null;
            $this->lat = ($row[$startcol + 23] !== null) ? (double) $row[$startcol + 23] : null;
            $this->maj_long_lat = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->sous_traitant = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 26; // 26 = CommonTBourseCotraitancePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTBourseCotraitance object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTBourseCotraitancePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTBourseCotraitanceQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTBourseCotraitancePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTBourseCotraitancePeer::ID_AUTO_BC;
        if (null !== $this->id_auto_bc) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTBourseCotraitancePeer::ID_AUTO_BC . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ID_AUTO_BC)) {
            $modifiedColumns[':p' . $index++]  = '`id_auto_BC`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`reference_consultation`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ID_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entreprise`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_etablissement_inscrite`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::NOM_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`nom_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::PRENOM_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`prenom_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ADRESSE_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ADRESSE2_INCSRIT)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_incsrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::CP_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`cp_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::VILLE_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`ville_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::PAYS_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`pays_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::FONCTION_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`fonction_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::EMAIL_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`email_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::TEL_FIXE_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`tel_fixe_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::TEL_MOBILE_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`tel_mobile_inscrit`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`mandataire_groupement`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`cotraitant_solidaire`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT)) {
            $modifiedColumns[':p' . $index++]  = '`cotraitant_conjoint`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::DESC_MON_APPORT_MARCHE)) {
            $modifiedColumns[':p' . $index++]  = '`desc_mon_apport_marche`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::DESC_TYPE_COTRAITANCE_RECHERCHE)) {
            $modifiedColumns[':p' . $index++]  = '`desc_type_cotraitance_recherche`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::CLAUSE_SOCIAL)) {
            $modifiedColumns[':p' . $index++]  = '`clause_social`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE)) {
            $modifiedColumns[':p' . $index++]  = '`entreprise_adapte`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::LONG)) {
            $modifiedColumns[':p' . $index++]  = '`long`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::LAT)) {
            $modifiedColumns[':p' . $index++]  = '`lat`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::MAJ_LONG_LAT)) {
            $modifiedColumns[':p' . $index++]  = '`maj_long_lat`';
        }
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::SOUS_TRAITANT)) {
            $modifiedColumns[':p' . $index++]  = '`sous_traitant`';
        }

        $sql = sprintf(
            'INSERT INTO `t_bourse_cotraitance` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_auto_BC`':
                        $stmt->bindValue($identifier, $this->id_auto_bc, PDO::PARAM_INT);
                        break;
                    case '`reference_consultation`':
                        $stmt->bindValue($identifier, $this->reference_consultation, PDO::PARAM_INT);
                        break;
                    case '`id_entreprise`':
                        $stmt->bindValue($identifier, $this->id_entreprise, PDO::PARAM_INT);
                        break;
                    case '`id_etablissement_inscrite`':
                        $stmt->bindValue($identifier, $this->id_etablissement_inscrite, PDO::PARAM_INT);
                        break;
                    case '`nom_inscrit`':
                        $stmt->bindValue($identifier, $this->nom_inscrit, PDO::PARAM_STR);
                        break;
                    case '`prenom_inscrit`':
                        $stmt->bindValue($identifier, $this->prenom_inscrit, PDO::PARAM_STR);
                        break;
                    case '`adresse_inscrit`':
                        $stmt->bindValue($identifier, $this->adresse_inscrit, PDO::PARAM_STR);
                        break;
                    case '`adresse2_incsrit`':
                        $stmt->bindValue($identifier, $this->adresse2_incsrit, PDO::PARAM_STR);
                        break;
                    case '`cp_inscrit`':
                        $stmt->bindValue($identifier, $this->cp_inscrit, PDO::PARAM_STR);
                        break;
                    case '`ville_inscrit`':
                        $stmt->bindValue($identifier, $this->ville_inscrit, PDO::PARAM_STR);
                        break;
                    case '`pays_inscrit`':
                        $stmt->bindValue($identifier, $this->pays_inscrit, PDO::PARAM_STR);
                        break;
                    case '`fonction_inscrit`':
                        $stmt->bindValue($identifier, $this->fonction_inscrit, PDO::PARAM_STR);
                        break;
                    case '`email_inscrit`':
                        $stmt->bindValue($identifier, $this->email_inscrit, PDO::PARAM_STR);
                        break;
                    case '`tel_fixe_inscrit`':
                        $stmt->bindValue($identifier, $this->tel_fixe_inscrit, PDO::PARAM_STR);
                        break;
                    case '`tel_mobile_inscrit`':
                        $stmt->bindValue($identifier, $this->tel_mobile_inscrit, PDO::PARAM_STR);
                        break;
                    case '`mandataire_groupement`':
                        $stmt->bindValue($identifier, $this->mandataire_groupement, PDO::PARAM_STR);
                        break;
                    case '`cotraitant_solidaire`':
                        $stmt->bindValue($identifier, $this->cotraitant_solidaire, PDO::PARAM_STR);
                        break;
                    case '`cotraitant_conjoint`':
                        $stmt->bindValue($identifier, $this->cotraitant_conjoint, PDO::PARAM_STR);
                        break;
                    case '`desc_mon_apport_marche`':
                        $stmt->bindValue($identifier, $this->desc_mon_apport_marche, PDO::PARAM_STR);
                        break;
                    case '`desc_type_cotraitance_recherche`':
                        $stmt->bindValue($identifier, $this->desc_type_cotraitance_recherche, PDO::PARAM_STR);
                        break;
                    case '`clause_social`':
                        $stmt->bindValue($identifier, $this->clause_social, PDO::PARAM_STR);
                        break;
                    case '`entreprise_adapte`':
                        $stmt->bindValue($identifier, $this->entreprise_adapte, PDO::PARAM_STR);
                        break;
                    case '`long`':
                        $stmt->bindValue($identifier, $this->long, PDO::PARAM_STR);
                        break;
                    case '`lat`':
                        $stmt->bindValue($identifier, $this->lat, PDO::PARAM_STR);
                        break;
                    case '`maj_long_lat`':
                        $stmt->bindValue($identifier, $this->maj_long_lat, PDO::PARAM_STR);
                        break;
                    case '`sous_traitant`':
                        $stmt->bindValue($identifier, $this->sous_traitant, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdAutoBc($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonTBourseCotraitancePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTBourseCotraitancePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdAutoBc();
                break;
            case 1:
                return $this->getReferenceConsultation();
                break;
            case 2:
                return $this->getIdEntreprise();
                break;
            case 3:
                return $this->getIdEtablissementInscrite();
                break;
            case 4:
                return $this->getNomInscrit();
                break;
            case 5:
                return $this->getPrenomInscrit();
                break;
            case 6:
                return $this->getAdresseInscrit();
                break;
            case 7:
                return $this->getAdresse2Incsrit();
                break;
            case 8:
                return $this->getCpInscrit();
                break;
            case 9:
                return $this->getVilleInscrit();
                break;
            case 10:
                return $this->getPaysInscrit();
                break;
            case 11:
                return $this->getFonctionInscrit();
                break;
            case 12:
                return $this->getEmailInscrit();
                break;
            case 13:
                return $this->getTelFixeInscrit();
                break;
            case 14:
                return $this->getTelMobileInscrit();
                break;
            case 15:
                return $this->getMandataireGroupement();
                break;
            case 16:
                return $this->getCotraitantSolidaire();
                break;
            case 17:
                return $this->getCotraitantConjoint();
                break;
            case 18:
                return $this->getDescMonApportMarche();
                break;
            case 19:
                return $this->getDescTypeCotraitanceRecherche();
                break;
            case 20:
                return $this->getClauseSocial();
                break;
            case 21:
                return $this->getEntrepriseAdapte();
                break;
            case 22:
                return $this->getLong();
                break;
            case 23:
                return $this->getLat();
                break;
            case 24:
                return $this->getMajLongLat();
                break;
            case 25:
                return $this->getSousTraitant();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['CommonTBourseCotraitance'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTBourseCotraitance'][$this->getPrimaryKey()] = true;
        $keys = CommonTBourseCotraitancePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdAutoBc(),
            $keys[1] => $this->getReferenceConsultation(),
            $keys[2] => $this->getIdEntreprise(),
            $keys[3] => $this->getIdEtablissementInscrite(),
            $keys[4] => $this->getNomInscrit(),
            $keys[5] => $this->getPrenomInscrit(),
            $keys[6] => $this->getAdresseInscrit(),
            $keys[7] => $this->getAdresse2Incsrit(),
            $keys[8] => $this->getCpInscrit(),
            $keys[9] => $this->getVilleInscrit(),
            $keys[10] => $this->getPaysInscrit(),
            $keys[11] => $this->getFonctionInscrit(),
            $keys[12] => $this->getEmailInscrit(),
            $keys[13] => $this->getTelFixeInscrit(),
            $keys[14] => $this->getTelMobileInscrit(),
            $keys[15] => $this->getMandataireGroupement(),
            $keys[16] => $this->getCotraitantSolidaire(),
            $keys[17] => $this->getCotraitantConjoint(),
            $keys[18] => $this->getDescMonApportMarche(),
            $keys[19] => $this->getDescTypeCotraitanceRecherche(),
            $keys[20] => $this->getClauseSocial(),
            $keys[21] => $this->getEntrepriseAdapte(),
            $keys[22] => $this->getLong(),
            $keys[23] => $this->getLat(),
            $keys[24] => $this->getMajLongLat(),
            $keys[25] => $this->getSousTraitant(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTBourseCotraitancePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdAutoBc($value);
                break;
            case 1:
                $this->setReferenceConsultation($value);
                break;
            case 2:
                $this->setIdEntreprise($value);
                break;
            case 3:
                $this->setIdEtablissementInscrite($value);
                break;
            case 4:
                $this->setNomInscrit($value);
                break;
            case 5:
                $this->setPrenomInscrit($value);
                break;
            case 6:
                $this->setAdresseInscrit($value);
                break;
            case 7:
                $this->setAdresse2Incsrit($value);
                break;
            case 8:
                $this->setCpInscrit($value);
                break;
            case 9:
                $this->setVilleInscrit($value);
                break;
            case 10:
                $this->setPaysInscrit($value);
                break;
            case 11:
                $this->setFonctionInscrit($value);
                break;
            case 12:
                $this->setEmailInscrit($value);
                break;
            case 13:
                $this->setTelFixeInscrit($value);
                break;
            case 14:
                $this->setTelMobileInscrit($value);
                break;
            case 15:
                $this->setMandataireGroupement($value);
                break;
            case 16:
                $this->setCotraitantSolidaire($value);
                break;
            case 17:
                $this->setCotraitantConjoint($value);
                break;
            case 18:
                $this->setDescMonApportMarche($value);
                break;
            case 19:
                $this->setDescTypeCotraitanceRecherche($value);
                break;
            case 20:
                $this->setClauseSocial($value);
                break;
            case 21:
                $this->setEntrepriseAdapte($value);
                break;
            case 22:
                $this->setLong($value);
                break;
            case 23:
                $this->setLat($value);
                break;
            case 24:
                $this->setMajLongLat($value);
                break;
            case 25:
                $this->setSousTraitant($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTBourseCotraitancePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdAutoBc($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setReferenceConsultation($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdEntreprise($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIdEtablissementInscrite($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNomInscrit($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPrenomInscrit($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAdresseInscrit($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setAdresse2Incsrit($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setCpInscrit($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setVilleInscrit($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setPaysInscrit($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setFonctionInscrit($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setEmailInscrit($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setTelFixeInscrit($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setTelMobileInscrit($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setMandataireGroupement($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setCotraitantSolidaire($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setCotraitantConjoint($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setDescMonApportMarche($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setDescTypeCotraitanceRecherche($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setClauseSocial($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setEntrepriseAdapte($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setLong($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setLat($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setMajLongLat($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setSousTraitant($arr[$keys[25]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTBourseCotraitancePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ID_AUTO_BC)) $criteria->add(CommonTBourseCotraitancePeer::ID_AUTO_BC, $this->id_auto_bc);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION)) $criteria->add(CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION, $this->reference_consultation);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ID_ENTREPRISE)) $criteria->add(CommonTBourseCotraitancePeer::ID_ENTREPRISE, $this->id_entreprise);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE)) $criteria->add(CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE, $this->id_etablissement_inscrite);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::NOM_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::NOM_INSCRIT, $this->nom_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::PRENOM_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::PRENOM_INSCRIT, $this->prenom_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ADRESSE_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::ADRESSE_INSCRIT, $this->adresse_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ADRESSE2_INCSRIT)) $criteria->add(CommonTBourseCotraitancePeer::ADRESSE2_INCSRIT, $this->adresse2_incsrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::CP_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::CP_INSCRIT, $this->cp_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::VILLE_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::VILLE_INSCRIT, $this->ville_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::PAYS_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::PAYS_INSCRIT, $this->pays_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::FONCTION_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::FONCTION_INSCRIT, $this->fonction_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::EMAIL_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::EMAIL_INSCRIT, $this->email_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::TEL_FIXE_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::TEL_FIXE_INSCRIT, $this->tel_fixe_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::TEL_MOBILE_INSCRIT)) $criteria->add(CommonTBourseCotraitancePeer::TEL_MOBILE_INSCRIT, $this->tel_mobile_inscrit);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT)) $criteria->add(CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT, $this->mandataire_groupement);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE)) $criteria->add(CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE, $this->cotraitant_solidaire);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT)) $criteria->add(CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT, $this->cotraitant_conjoint);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::DESC_MON_APPORT_MARCHE)) $criteria->add(CommonTBourseCotraitancePeer::DESC_MON_APPORT_MARCHE, $this->desc_mon_apport_marche);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::DESC_TYPE_COTRAITANCE_RECHERCHE)) $criteria->add(CommonTBourseCotraitancePeer::DESC_TYPE_COTRAITANCE_RECHERCHE, $this->desc_type_cotraitance_recherche);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::CLAUSE_SOCIAL)) $criteria->add(CommonTBourseCotraitancePeer::CLAUSE_SOCIAL, $this->clause_social);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE)) $criteria->add(CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE, $this->entreprise_adapte);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::LONG)) $criteria->add(CommonTBourseCotraitancePeer::LONG, $this->long);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::LAT)) $criteria->add(CommonTBourseCotraitancePeer::LAT, $this->lat);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::MAJ_LONG_LAT)) $criteria->add(CommonTBourseCotraitancePeer::MAJ_LONG_LAT, $this->maj_long_lat);
        if ($this->isColumnModified(CommonTBourseCotraitancePeer::SOUS_TRAITANT)) $criteria->add(CommonTBourseCotraitancePeer::SOUS_TRAITANT, $this->sous_traitant);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTBourseCotraitancePeer::DATABASE_NAME);
        $criteria->add(CommonTBourseCotraitancePeer::ID_AUTO_BC, $this->id_auto_bc);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdAutoBc();
    }

    /**
     * Generic method to set the primary key (id_auto_bc column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdAutoBc($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdAutoBc();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTBourseCotraitance (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setReferenceConsultation($this->getReferenceConsultation());
        $copyObj->setIdEntreprise($this->getIdEntreprise());
        $copyObj->setIdEtablissementInscrite($this->getIdEtablissementInscrite());
        $copyObj->setNomInscrit($this->getNomInscrit());
        $copyObj->setPrenomInscrit($this->getPrenomInscrit());
        $copyObj->setAdresseInscrit($this->getAdresseInscrit());
        $copyObj->setAdresse2Incsrit($this->getAdresse2Incsrit());
        $copyObj->setCpInscrit($this->getCpInscrit());
        $copyObj->setVilleInscrit($this->getVilleInscrit());
        $copyObj->setPaysInscrit($this->getPaysInscrit());
        $copyObj->setFonctionInscrit($this->getFonctionInscrit());
        $copyObj->setEmailInscrit($this->getEmailInscrit());
        $copyObj->setTelFixeInscrit($this->getTelFixeInscrit());
        $copyObj->setTelMobileInscrit($this->getTelMobileInscrit());
        $copyObj->setMandataireGroupement($this->getMandataireGroupement());
        $copyObj->setCotraitantSolidaire($this->getCotraitantSolidaire());
        $copyObj->setCotraitantConjoint($this->getCotraitantConjoint());
        $copyObj->setDescMonApportMarche($this->getDescMonApportMarche());
        $copyObj->setDescTypeCotraitanceRecherche($this->getDescTypeCotraitanceRecherche());
        $copyObj->setClauseSocial($this->getClauseSocial());
        $copyObj->setEntrepriseAdapte($this->getEntrepriseAdapte());
        $copyObj->setLong($this->getLong());
        $copyObj->setLat($this->getLat());
        $copyObj->setMajLongLat($this->getMajLongLat());
        $copyObj->setSousTraitant($this->getSousTraitant());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdAutoBc(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTBourseCotraitance Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTBourseCotraitancePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTBourseCotraitancePeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_auto_bc = null;
        $this->reference_consultation = null;
        $this->id_entreprise = null;
        $this->id_etablissement_inscrite = null;
        $this->nom_inscrit = null;
        $this->prenom_inscrit = null;
        $this->adresse_inscrit = null;
        $this->adresse2_incsrit = null;
        $this->cp_inscrit = null;
        $this->ville_inscrit = null;
        $this->pays_inscrit = null;
        $this->fonction_inscrit = null;
        $this->email_inscrit = null;
        $this->tel_fixe_inscrit = null;
        $this->tel_mobile_inscrit = null;
        $this->mandataire_groupement = null;
        $this->cotraitant_solidaire = null;
        $this->cotraitant_conjoint = null;
        $this->desc_mon_apport_marche = null;
        $this->desc_type_cotraitance_recherche = null;
        $this->clause_social = null;
        $this->entreprise_adapte = null;
        $this->long = null;
        $this->lat = null;
        $this->maj_long_lat = null;
        $this->sous_traitant = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTBourseCotraitancePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
