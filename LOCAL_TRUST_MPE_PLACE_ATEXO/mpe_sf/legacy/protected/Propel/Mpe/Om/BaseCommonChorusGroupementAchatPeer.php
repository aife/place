<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonChorusGroupementAchat;
use Application\Propel\Mpe\CommonChorusGroupementAchatPeer;
use Application\Propel\Mpe\Map\CommonChorusGroupementAchatTableMap;

/**
 * Base static class for performing query and update operations on the 'Chorus_groupement_achat' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonChorusGroupementAchatPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'Chorus_groupement_achat';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonChorusGroupementAchat';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonChorusGroupementAchatTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 8;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 8;

    /** the column name for the id field */
    const ID = 'Chorus_groupement_achat.id';

    /** the column name for the organisme field */
    const ORGANISME = 'Chorus_groupement_achat.organisme';

    /** the column name for the id_oa field */
    const ID_OA = 'Chorus_groupement_achat.id_oa';

    /** the column name for the libelle field */
    const LIBELLE = 'Chorus_groupement_achat.libelle';

    /** the column name for the code field */
    const CODE = 'Chorus_groupement_achat.code';

    /** the column name for the actif field */
    const ACTIF = 'Chorus_groupement_achat.actif';

    /** the column name for the old_service_id field */
    const OLD_SERVICE_ID = 'Chorus_groupement_achat.old_service_id';

    /** the column name for the service_id field */
    const SERVICE_ID = 'Chorus_groupement_achat.service_id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonChorusGroupementAchat objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonChorusGroupementAchat[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonChorusGroupementAchatPeer::$fieldNames[CommonChorusGroupementAchatPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'Organisme', 'IdOa', 'Libelle', 'Code', 'Actif', 'OldServiceId', 'ServiceId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'organisme', 'idOa', 'libelle', 'code', 'actif', 'oldServiceId', 'serviceId', ),
        BasePeer::TYPE_COLNAME => array (CommonChorusGroupementAchatPeer::ID, CommonChorusGroupementAchatPeer::ORGANISME, CommonChorusGroupementAchatPeer::ID_OA, CommonChorusGroupementAchatPeer::LIBELLE, CommonChorusGroupementAchatPeer::CODE, CommonChorusGroupementAchatPeer::ACTIF, CommonChorusGroupementAchatPeer::OLD_SERVICE_ID, CommonChorusGroupementAchatPeer::SERVICE_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ORGANISME', 'ID_OA', 'LIBELLE', 'CODE', 'ACTIF', 'OLD_SERVICE_ID', 'SERVICE_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'organisme', 'id_oa', 'libelle', 'code', 'actif', 'old_service_id', 'service_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonChorusGroupementAchatPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Organisme' => 1, 'IdOa' => 2, 'Libelle' => 3, 'Code' => 4, 'Actif' => 5, 'OldServiceId' => 6, 'ServiceId' => 7, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'organisme' => 1, 'idOa' => 2, 'libelle' => 3, 'code' => 4, 'actif' => 5, 'oldServiceId' => 6, 'serviceId' => 7, ),
        BasePeer::TYPE_COLNAME => array (CommonChorusGroupementAchatPeer::ID => 0, CommonChorusGroupementAchatPeer::ORGANISME => 1, CommonChorusGroupementAchatPeer::ID_OA => 2, CommonChorusGroupementAchatPeer::LIBELLE => 3, CommonChorusGroupementAchatPeer::CODE => 4, CommonChorusGroupementAchatPeer::ACTIF => 5, CommonChorusGroupementAchatPeer::OLD_SERVICE_ID => 6, CommonChorusGroupementAchatPeer::SERVICE_ID => 7, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ORGANISME' => 1, 'ID_OA' => 2, 'LIBELLE' => 3, 'CODE' => 4, 'ACTIF' => 5, 'OLD_SERVICE_ID' => 6, 'SERVICE_ID' => 7, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'organisme' => 1, 'id_oa' => 2, 'libelle' => 3, 'code' => 4, 'actif' => 5, 'old_service_id' => 6, 'service_id' => 7, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonChorusGroupementAchatPeer::getFieldNames($toType);
        $key = isset(CommonChorusGroupementAchatPeer::$fieldKeys[$fromType][$name]) ? CommonChorusGroupementAchatPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonChorusGroupementAchatPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonChorusGroupementAchatPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonChorusGroupementAchatPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonChorusGroupementAchatPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonChorusGroupementAchatPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonChorusGroupementAchatPeer::ID);
            $criteria->addSelectColumn(CommonChorusGroupementAchatPeer::ORGANISME);
            $criteria->addSelectColumn(CommonChorusGroupementAchatPeer::ID_OA);
            $criteria->addSelectColumn(CommonChorusGroupementAchatPeer::LIBELLE);
            $criteria->addSelectColumn(CommonChorusGroupementAchatPeer::CODE);
            $criteria->addSelectColumn(CommonChorusGroupementAchatPeer::ACTIF);
            $criteria->addSelectColumn(CommonChorusGroupementAchatPeer::OLD_SERVICE_ID);
            $criteria->addSelectColumn(CommonChorusGroupementAchatPeer::SERVICE_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.id_oa');
            $criteria->addSelectColumn($alias . '.libelle');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.old_service_id');
            $criteria->addSelectColumn($alias . '.service_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonChorusGroupementAchatPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonChorusGroupementAchatPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonChorusGroupementAchatPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonChorusGroupementAchatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonChorusGroupementAchat
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonChorusGroupementAchatPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonChorusGroupementAchatPeer::populateObjects(CommonChorusGroupementAchatPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonChorusGroupementAchatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonChorusGroupementAchatPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonChorusGroupementAchatPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonChorusGroupementAchat $obj A CommonChorusGroupementAchat object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getId(), (string) $obj->getOrganisme(), (string) $obj->getIdOa(), (string) $obj->getOldServiceId()));
            } // if key === null
            CommonChorusGroupementAchatPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonChorusGroupementAchat object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonChorusGroupementAchat) {
                $key = serialize(array((string) $value->getId(), (string) $value->getOrganisme(), (string) $value->getIdOa(), (string) $value->getOldServiceId()));
            } elseif (is_array($value) && count($value) === 4) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1], (string) $value[2], (string) $value[3]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonChorusGroupementAchat object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonChorusGroupementAchatPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonChorusGroupementAchat Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonChorusGroupementAchatPeer::$instances[$key])) {
                return CommonChorusGroupementAchatPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonChorusGroupementAchatPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonChorusGroupementAchatPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to Chorus_groupement_achat
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null && $row[$startcol + 2] === null && $row[$startcol + 6] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1], (string) $row[$startcol + 2], (string) $row[$startcol + 6]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((int) $row[$startcol], (string) $row[$startcol + 1], (int) $row[$startcol + 2], (int) $row[$startcol + 6]);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonChorusGroupementAchatPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonChorusGroupementAchatPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonChorusGroupementAchatPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonChorusGroupementAchatPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonChorusGroupementAchat object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonChorusGroupementAchatPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonChorusGroupementAchatPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonChorusGroupementAchatPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonChorusGroupementAchatPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonChorusGroupementAchatPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonChorusGroupementAchatPeer::DATABASE_NAME)->getTable(CommonChorusGroupementAchatPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonChorusGroupementAchatPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonChorusGroupementAchatPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonChorusGroupementAchatTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonChorusGroupementAchatPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonChorusGroupementAchat or Criteria object.
     *
     * @param      mixed $values Criteria or CommonChorusGroupementAchat object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonChorusGroupementAchatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonChorusGroupementAchat object
        }

        if ($criteria->containsKey(CommonChorusGroupementAchatPeer::ID) && $criteria->keyContainsValue(CommonChorusGroupementAchatPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonChorusGroupementAchatPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonChorusGroupementAchatPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonChorusGroupementAchat or Criteria object.
     *
     * @param      mixed $values Criteria or CommonChorusGroupementAchat object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonChorusGroupementAchatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonChorusGroupementAchatPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonChorusGroupementAchatPeer::ID);
            $value = $criteria->remove(CommonChorusGroupementAchatPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonChorusGroupementAchatPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonChorusGroupementAchatPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(CommonChorusGroupementAchatPeer::ORGANISME);
            $value = $criteria->remove(CommonChorusGroupementAchatPeer::ORGANISME);
            if ($value) {
                $selectCriteria->add(CommonChorusGroupementAchatPeer::ORGANISME, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonChorusGroupementAchatPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(CommonChorusGroupementAchatPeer::ID_OA);
            $value = $criteria->remove(CommonChorusGroupementAchatPeer::ID_OA);
            if ($value) {
                $selectCriteria->add(CommonChorusGroupementAchatPeer::ID_OA, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonChorusGroupementAchatPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(CommonChorusGroupementAchatPeer::OLD_SERVICE_ID);
            $value = $criteria->remove(CommonChorusGroupementAchatPeer::OLD_SERVICE_ID);
            if ($value) {
                $selectCriteria->add(CommonChorusGroupementAchatPeer::OLD_SERVICE_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonChorusGroupementAchatPeer::TABLE_NAME);
            }

        } else { // $values is CommonChorusGroupementAchat object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonChorusGroupementAchatPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the Chorus_groupement_achat table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonChorusGroupementAchatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonChorusGroupementAchatPeer::TABLE_NAME, $con, CommonChorusGroupementAchatPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonChorusGroupementAchatPeer::clearInstancePool();
            CommonChorusGroupementAchatPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonChorusGroupementAchat or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonChorusGroupementAchat object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonChorusGroupementAchatPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonChorusGroupementAchatPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonChorusGroupementAchat) { // it's a model object
            // invalidate the cache for this single object
            CommonChorusGroupementAchatPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonChorusGroupementAchatPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(CommonChorusGroupementAchatPeer::ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(CommonChorusGroupementAchatPeer::ORGANISME, $value[1]));
                $criterion->addAnd($criteria->getNewCriterion(CommonChorusGroupementAchatPeer::ID_OA, $value[2]));
                $criterion->addAnd($criteria->getNewCriterion(CommonChorusGroupementAchatPeer::OLD_SERVICE_ID, $value[3]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                CommonChorusGroupementAchatPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonChorusGroupementAchatPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonChorusGroupementAchatPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonChorusGroupementAchat object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonChorusGroupementAchat $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonChorusGroupementAchatPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonChorusGroupementAchatPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonChorusGroupementAchatPeer::DATABASE_NAME, CommonChorusGroupementAchatPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   int $id
     * @param   string $organisme
     * @param   int $id_oa
     * @param   int $old_service_id
     * @param      PropelPDO $con
     * @return   CommonChorusGroupementAchat
     */
    public static function retrieveByPK($id, $organisme, $id_oa, $old_service_id, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $id, (string) $organisme, (string) $id_oa, (string) $old_service_id));
         if (null !== ($obj = CommonChorusGroupementAchatPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonChorusGroupementAchatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(CommonChorusGroupementAchatPeer::DATABASE_NAME);
        $criteria->add(CommonChorusGroupementAchatPeer::ID, $id);
        $criteria->add(CommonChorusGroupementAchatPeer::ORGANISME, $organisme);
        $criteria->add(CommonChorusGroupementAchatPeer::ID_OA, $id_oa);
        $criteria->add(CommonChorusGroupementAchatPeer::OLD_SERVICE_ID, $old_service_id);
        $v = CommonChorusGroupementAchatPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BaseCommonChorusGroupementAchatPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonChorusGroupementAchatPeer::buildTableMap();

