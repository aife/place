<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDerniersAppelsValidesWsSgmapDocuments;
use Application\Propel\Mpe\CommonTDerniersAppelsValidesWsSgmapDocumentsPeer;
use Application\Propel\Mpe\CommonTDerniersAppelsValidesWsSgmapDocumentsQuery;

/**
 * Base class that represents a query for the 't_derniers_appels_valides_ws_sgmap_documents' table.
 *
 *
 *
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery orderByIdentifiant($order = Criteria::ASC) Order by the identifiant column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery orderByIdTypeDocument($order = Criteria::ASC) Order by the id_type_document column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery orderByIdEntreprise($order = Criteria::ASC) Order by the id_entreprise column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery orderByDateDernierAppelValide($order = Criteria::ASC) Order by the date_dernier_appel_valide column
 *
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery groupById() Group by the id column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery groupByIdentifiant() Group by the identifiant column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery groupByIdTypeDocument() Group by the id_type_document column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery groupByIdEntreprise() Group by the id_entreprise column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery groupByIdAgent() Group by the id_agent column
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery groupByDateDernierAppelValide() Group by the date_dernier_appel_valide column
 *
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTDerniersAppelsValidesWsSgmapDocumentsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTDerniersAppelsValidesWsSgmapDocuments findOne(PropelPDO $con = null) Return the first CommonTDerniersAppelsValidesWsSgmapDocuments matching the query
 * @method CommonTDerniersAppelsValidesWsSgmapDocuments findOneOrCreate(PropelPDO $con = null) Return the first CommonTDerniersAppelsValidesWsSgmapDocuments matching the query, or a new CommonTDerniersAppelsValidesWsSgmapDocuments object populated from the query conditions when no match is found
 *
 * @method CommonTDerniersAppelsValidesWsSgmapDocuments findOneByIdentifiant(string $identifiant) Return the first CommonTDerniersAppelsValidesWsSgmapDocuments filtered by the identifiant column
 * @method CommonTDerniersAppelsValidesWsSgmapDocuments findOneByIdTypeDocument(int $id_type_document) Return the first CommonTDerniersAppelsValidesWsSgmapDocuments filtered by the id_type_document column
 * @method CommonTDerniersAppelsValidesWsSgmapDocuments findOneByIdEntreprise(int $id_entreprise) Return the first CommonTDerniersAppelsValidesWsSgmapDocuments filtered by the id_entreprise column
 * @method CommonTDerniersAppelsValidesWsSgmapDocuments findOneByIdAgent(int $id_agent) Return the first CommonTDerniersAppelsValidesWsSgmapDocuments filtered by the id_agent column
 * @method CommonTDerniersAppelsValidesWsSgmapDocuments findOneByDateDernierAppelValide(string $date_dernier_appel_valide) Return the first CommonTDerniersAppelsValidesWsSgmapDocuments filtered by the date_dernier_appel_valide column
 *
 * @method array findById(int $id) Return CommonTDerniersAppelsValidesWsSgmapDocuments objects filtered by the id column
 * @method array findByIdentifiant(string $identifiant) Return CommonTDerniersAppelsValidesWsSgmapDocuments objects filtered by the identifiant column
 * @method array findByIdTypeDocument(int $id_type_document) Return CommonTDerniersAppelsValidesWsSgmapDocuments objects filtered by the id_type_document column
 * @method array findByIdEntreprise(int $id_entreprise) Return CommonTDerniersAppelsValidesWsSgmapDocuments objects filtered by the id_entreprise column
 * @method array findByIdAgent(int $id_agent) Return CommonTDerniersAppelsValidesWsSgmapDocuments objects filtered by the id_agent column
 * @method array findByDateDernierAppelValide(string $date_dernier_appel_valide) Return CommonTDerniersAppelsValidesWsSgmapDocuments objects filtered by the date_dernier_appel_valide column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDerniersAppelsValidesWsSgmapDocumentsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTDerniersAppelsValidesWsSgmapDocumentsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTDerniersAppelsValidesWsSgmapDocuments', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTDerniersAppelsValidesWsSgmapDocumentsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTDerniersAppelsValidesWsSgmapDocumentsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTDerniersAppelsValidesWsSgmapDocumentsQuery) {
            return $criteria;
        }
        $query = new CommonTDerniersAppelsValidesWsSgmapDocumentsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTDerniersAppelsValidesWsSgmapDocuments|CommonTDerniersAppelsValidesWsSgmapDocuments[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDerniersAppelsValidesWsSgmapDocuments A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDerniersAppelsValidesWsSgmapDocuments A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `identifiant`, `id_type_document`, `id_entreprise`, `id_agent`, `date_dernier_appel_valide` FROM `t_derniers_appels_valides_ws_sgmap_documents` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTDerniersAppelsValidesWsSgmapDocuments();
            $obj->hydrate($row);
            CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocuments|CommonTDerniersAppelsValidesWsSgmapDocuments[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTDerniersAppelsValidesWsSgmapDocuments[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the identifiant column
     *
     * Example usage:
     * <code>
     * $query->filterByIdentifiant(1234); // WHERE identifiant = 1234
     * $query->filterByIdentifiant(array(12, 34)); // WHERE identifiant IN (12, 34)
     * $query->filterByIdentifiant(array('min' => 12)); // WHERE identifiant >= 12
     * $query->filterByIdentifiant(array('max' => 12)); // WHERE identifiant <= 12
     * </code>
     *
     * @param     mixed $identifiant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery The current query, for fluid interface
     */
    public function filterByIdentifiant($identifiant = null, $comparison = null)
    {
        if (is_array($identifiant)) {
            $useMinMax = false;
            if (isset($identifiant['min'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::IDENTIFIANT, $identifiant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($identifiant['max'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::IDENTIFIANT, $identifiant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::IDENTIFIANT, $identifiant, $comparison);
    }

    /**
     * Filter the query on the id_type_document column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeDocument(1234); // WHERE id_type_document = 1234
     * $query->filterByIdTypeDocument(array(12, 34)); // WHERE id_type_document IN (12, 34)
     * $query->filterByIdTypeDocument(array('min' => 12)); // WHERE id_type_document >= 12
     * $query->filterByIdTypeDocument(array('max' => 12)); // WHERE id_type_document <= 12
     * </code>
     *
     * @param     mixed $idTypeDocument The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery The current query, for fluid interface
     */
    public function filterByIdTypeDocument($idTypeDocument = null, $comparison = null)
    {
        if (is_array($idTypeDocument)) {
            $useMinMax = false;
            if (isset($idTypeDocument['min'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID_TYPE_DOCUMENT, $idTypeDocument['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeDocument['max'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID_TYPE_DOCUMENT, $idTypeDocument['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID_TYPE_DOCUMENT, $idTypeDocument, $comparison);
    }

    /**
     * Filter the query on the id_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntreprise(1234); // WHERE id_entreprise = 1234
     * $query->filterByIdEntreprise(array(12, 34)); // WHERE id_entreprise IN (12, 34)
     * $query->filterByIdEntreprise(array('min' => 12)); // WHERE id_entreprise >= 12
     * $query->filterByIdEntreprise(array('max' => 12)); // WHERE id_entreprise <= 12
     * </code>
     *
     * @param     mixed $idEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery The current query, for fluid interface
     */
    public function filterByIdEntreprise($idEntreprise = null, $comparison = null)
    {
        if (is_array($idEntreprise)) {
            $useMinMax = false;
            if (isset($idEntreprise['min'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID_ENTREPRISE, $idEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntreprise['max'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID_ENTREPRISE, $idEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID_ENTREPRISE, $idEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the date_dernier_appel_valide column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDernierAppelValide('2011-03-14'); // WHERE date_dernier_appel_valide = '2011-03-14'
     * $query->filterByDateDernierAppelValide('now'); // WHERE date_dernier_appel_valide = '2011-03-14'
     * $query->filterByDateDernierAppelValide(array('max' => 'yesterday')); // WHERE date_dernier_appel_valide > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDernierAppelValide The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery The current query, for fluid interface
     */
    public function filterByDateDernierAppelValide($dateDernierAppelValide = null, $comparison = null)
    {
        if (is_array($dateDernierAppelValide)) {
            $useMinMax = false;
            if (isset($dateDernierAppelValide['min'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::DATE_DERNIER_APPEL_VALIDE, $dateDernierAppelValide['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDernierAppelValide['max'])) {
                $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::DATE_DERNIER_APPEL_VALIDE, $dateDernierAppelValide['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::DATE_DERNIER_APPEL_VALIDE, $dateDernierAppelValide, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTDerniersAppelsValidesWsSgmapDocuments $commonTDerniersAppelsValidesWsSgmapDocuments Object to remove from the list of results
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocumentsQuery The current query, for fluid interface
     */
    public function prune($commonTDerniersAppelsValidesWsSgmapDocuments = null)
    {
        if ($commonTDerniersAppelsValidesWsSgmapDocuments) {
            $this->addUsingAlias(CommonTDerniersAppelsValidesWsSgmapDocumentsPeer::ID, $commonTDerniersAppelsValidesWsSgmapDocuments->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
