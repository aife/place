<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonPlateformeVirtuelleOrganisme;
use Application\Propel\Mpe\CommonPlateformeVirtuelleOrganismeQuery;
use Application\Propel\Mpe\CommonPlateformeVirtuellePeer;
use Application\Propel\Mpe\CommonPlateformeVirtuelleQuery;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonQuestionsDceQuery;
use Application\Propel\Mpe\CommonTMesRecherches;
use Application\Propel\Mpe\CommonTMesRecherchesQuery;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\CommonTelechargementQuery;

/**
 * Base class that represents a row from the 'plateforme_virtuelle' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonPlateformeVirtuelle extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonPlateformeVirtuellePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonPlateformeVirtuellePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the domain field.
     * @var        string
     */
    protected $domain;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the code_design field.
     * @var        string
     */
    protected $code_design;

    /**
     * The value for the protocole field.
     * Note: this column has a database default value of: 'https'
     * @var        string
     */
    protected $protocole;

    /**
     * The value for the no_reply field.
     * @var        string
     */
    protected $no_reply;

    /**
     * The value for the footer_mail field.
     * @var        string
     */
    protected $footer_mail;

    /**
     * The value for the from_pf_name field.
     * @var        string
     */
    protected $from_pf_name;

    /**
     * @var        PropelObjectCollection|CommonOffres[] Collection to store aggregation of CommonOffres objects.
     */
    protected $collCommonOffress;
    protected $collCommonOffressPartial;

    /**
     * @var        PropelObjectCollection|CommonTMesRecherches[] Collection to store aggregation of CommonTMesRecherches objects.
     */
    protected $collCommonTMesRecherchess;
    protected $collCommonTMesRecherchessPartial;

    /**
     * @var        PropelObjectCollection|CommonTelechargement[] Collection to store aggregation of CommonTelechargement objects.
     */
    protected $collCommonTelechargements;
    protected $collCommonTelechargementsPartial;

    /**
     * @var        PropelObjectCollection|CommonConsultation[] Collection to store aggregation of CommonConsultation objects.
     */
    protected $collCommonConsultations;
    protected $collCommonConsultationsPartial;

    /**
     * @var        PropelObjectCollection|CommonPlateformeVirtuelleOrganisme[] Collection to store aggregation of CommonPlateformeVirtuelleOrganisme objects.
     */
    protected $collCommonPlateformeVirtuelleOrganismes;
    protected $collCommonPlateformeVirtuelleOrganismesPartial;

    /**
     * @var        PropelObjectCollection|CommonQuestionsDce[] Collection to store aggregation of CommonQuestionsDce objects.
     */
    protected $collCommonQuestionsDces;
    protected $collCommonQuestionsDcesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonOffressScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTMesRecherchessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTelechargementsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonPlateformeVirtuelleOrganismesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonQuestionsDcesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->protocole = 'https';
    }

    /**
     * Initializes internal state of BaseCommonPlateformeVirtuelle object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [domain] column value.
     *
     * @return string
     */
    public function getDomain()
    {

        return $this->domain;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [code_design] column value.
     *
     * @return string
     */
    public function getCodeDesign()
    {

        return $this->code_design;
    }

    /**
     * Get the [protocole] column value.
     *
     * @return string
     */
    public function getProtocole()
    {

        return $this->protocole;
    }

    /**
     * Get the [no_reply] column value.
     *
     * @return string
     */
    public function getNoReply()
    {

        return $this->no_reply;
    }

    /**
     * Get the [footer_mail] column value.
     *
     * @return string
     */
    public function getFooterMail()
    {

        return $this->footer_mail;
    }

    /**
     * Get the [from_pf_name] column value.
     *
     * @return string
     */
    public function getFromPfName()
    {

        return $this->from_pf_name;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonPlateformeVirtuellePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [domain] column.
     *
     * @param string $v new value
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setDomain($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->domain !== $v) {
            $this->domain = $v;
            $this->modifiedColumns[] = CommonPlateformeVirtuellePeer::DOMAIN;
        }


        return $this;
    } // setDomain()

    /**
     * Set the value of [name] column.
     *
     * @param string $v new value
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = CommonPlateformeVirtuellePeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [code_design] column.
     *
     * @param string $v new value
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setCodeDesign($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_design !== $v) {
            $this->code_design = $v;
            $this->modifiedColumns[] = CommonPlateformeVirtuellePeer::CODE_DESIGN;
        }


        return $this;
    } // setCodeDesign()

    /**
     * Set the value of [protocole] column.
     *
     * @param string $v new value
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setProtocole($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->protocole !== $v) {
            $this->protocole = $v;
            $this->modifiedColumns[] = CommonPlateformeVirtuellePeer::PROTOCOLE;
        }


        return $this;
    } // setProtocole()

    /**
     * Set the value of [no_reply] column.
     *
     * @param string $v new value
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setNoReply($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->no_reply !== $v) {
            $this->no_reply = $v;
            $this->modifiedColumns[] = CommonPlateformeVirtuellePeer::NO_REPLY;
        }


        return $this;
    } // setNoReply()

    /**
     * Set the value of [footer_mail] column.
     *
     * @param string $v new value
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setFooterMail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->footer_mail !== $v) {
            $this->footer_mail = $v;
            $this->modifiedColumns[] = CommonPlateformeVirtuellePeer::FOOTER_MAIL;
        }


        return $this;
    } // setFooterMail()

    /**
     * Set the value of [from_pf_name] column.
     *
     * @param string $v new value
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setFromPfName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->from_pf_name !== $v) {
            $this->from_pf_name = $v;
            $this->modifiedColumns[] = CommonPlateformeVirtuellePeer::FROM_PF_NAME;
        }


        return $this;
    } // setFromPfName()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->protocole !== 'https') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->domain = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->code_design = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->protocole = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->no_reply = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->footer_mail = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->from_pf_name = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 8; // 8 = CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonPlateformeVirtuelle object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonPlateformeVirtuellePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonPlateformeVirtuellePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonOffress = null;

            $this->collCommonTMesRecherchess = null;

            $this->collCommonTelechargements = null;

            $this->collCommonConsultations = null;

            $this->collCommonPlateformeVirtuelleOrganismes = null;

            $this->collCommonQuestionsDces = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonPlateformeVirtuellePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonPlateformeVirtuelleQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonPlateformeVirtuellePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonPlateformeVirtuellePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonOffressScheduledForDeletion !== null) {
                if (!$this->commonOffressScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonOffressScheduledForDeletion as $commonOffres) {
                        // need to save related object because we set the relation to null
                        $commonOffres->save($con);
                    }
                    $this->commonOffressScheduledForDeletion = null;
                }
            }

            if ($this->collCommonOffress !== null) {
                foreach ($this->collCommonOffress as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTMesRecherchessScheduledForDeletion !== null) {
                if (!$this->commonTMesRecherchessScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTMesRecherchessScheduledForDeletion as $commonTMesRecherches) {
                        // need to save related object because we set the relation to null
                        $commonTMesRecherches->save($con);
                    }
                    $this->commonTMesRecherchessScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTMesRecherchess !== null) {
                foreach ($this->collCommonTMesRecherchess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTelechargementsScheduledForDeletion !== null) {
                if (!$this->commonTelechargementsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTelechargementsScheduledForDeletion as $commonTelechargement) {
                        // need to save related object because we set the relation to null
                        $commonTelechargement->save($con);
                    }
                    $this->commonTelechargementsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTelechargements !== null) {
                foreach ($this->collCommonTelechargements as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonConsultationsScheduledForDeletion !== null) {
                if (!$this->commonConsultationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationsScheduledForDeletion as $commonConsultation) {
                        // need to save related object because we set the relation to null
                        $commonConsultation->save($con);
                    }
                    $this->commonConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultations !== null) {
                foreach ($this->collCommonConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonPlateformeVirtuelleOrganismesScheduledForDeletion !== null) {
                if (!$this->commonPlateformeVirtuelleOrganismesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonPlateformeVirtuelleOrganismeQuery::create()
                        ->filterByPrimaryKeys($this->commonPlateformeVirtuelleOrganismesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonPlateformeVirtuelleOrganismes !== null) {
                foreach ($this->collCommonPlateformeVirtuelleOrganismes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonQuestionsDcesScheduledForDeletion !== null) {
                if (!$this->commonQuestionsDcesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonQuestionsDcesScheduledForDeletion as $commonQuestionsDce) {
                        // need to save related object because we set the relation to null
                        $commonQuestionsDce->save($con);
                    }
                    $this->commonQuestionsDcesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonQuestionsDces !== null) {
                foreach ($this->collCommonQuestionsDces as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonPlateformeVirtuellePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonPlateformeVirtuellePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::DOMAIN)) {
            $modifiedColumns[':p' . $index++]  = '`domain`';
        }
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`name`';
        }
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::CODE_DESIGN)) {
            $modifiedColumns[':p' . $index++]  = '`code_design`';
        }
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::PROTOCOLE)) {
            $modifiedColumns[':p' . $index++]  = '`protocole`';
        }
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::NO_REPLY)) {
            $modifiedColumns[':p' . $index++]  = '`no_reply`';
        }
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::FOOTER_MAIL)) {
            $modifiedColumns[':p' . $index++]  = '`footer_mail`';
        }
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::FROM_PF_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`from_pf_name`';
        }

        $sql = sprintf(
            'INSERT INTO `plateforme_virtuelle` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`domain`':
                        $stmt->bindValue($identifier, $this->domain, PDO::PARAM_STR);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`code_design`':
                        $stmt->bindValue($identifier, $this->code_design, PDO::PARAM_STR);
                        break;
                    case '`protocole`':
                        $stmt->bindValue($identifier, $this->protocole, PDO::PARAM_STR);
                        break;
                    case '`no_reply`':
                        $stmt->bindValue($identifier, $this->no_reply, PDO::PARAM_STR);
                        break;
                    case '`footer_mail`':
                        $stmt->bindValue($identifier, $this->footer_mail, PDO::PARAM_STR);
                        break;
                    case '`from_pf_name`':
                        $stmt->bindValue($identifier, $this->from_pf_name, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonPlateformeVirtuellePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonOffress !== null) {
                    foreach ($this->collCommonOffress as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTMesRecherchess !== null) {
                    foreach ($this->collCommonTMesRecherchess as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTelechargements !== null) {
                    foreach ($this->collCommonTelechargements as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonConsultations !== null) {
                    foreach ($this->collCommonConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonPlateformeVirtuelleOrganismes !== null) {
                    foreach ($this->collCommonPlateformeVirtuelleOrganismes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonQuestionsDces !== null) {
                    foreach ($this->collCommonQuestionsDces as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonPlateformeVirtuellePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getDomain();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getCodeDesign();
                break;
            case 4:
                return $this->getProtocole();
                break;
            case 5:
                return $this->getNoReply();
                break;
            case 6:
                return $this->getFooterMail();
                break;
            case 7:
                return $this->getFromPfName();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonPlateformeVirtuelle'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonPlateformeVirtuelle'][$this->getPrimaryKey()] = true;
        $keys = CommonPlateformeVirtuellePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getDomain(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getCodeDesign(),
            $keys[4] => $this->getProtocole(),
            $keys[5] => $this->getNoReply(),
            $keys[6] => $this->getFooterMail(),
            $keys[7] => $this->getFromPfName(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonOffress) {
                $result['CommonOffress'] = $this->collCommonOffress->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTMesRecherchess) {
                $result['CommonTMesRecherchess'] = $this->collCommonTMesRecherchess->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTelechargements) {
                $result['CommonTelechargements'] = $this->collCommonTelechargements->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonConsultations) {
                $result['CommonConsultations'] = $this->collCommonConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonPlateformeVirtuelleOrganismes) {
                $result['CommonPlateformeVirtuelleOrganismes'] = $this->collCommonPlateformeVirtuelleOrganismes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonQuestionsDces) {
                $result['CommonQuestionsDces'] = $this->collCommonQuestionsDces->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonPlateformeVirtuellePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setDomain($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setCodeDesign($value);
                break;
            case 4:
                $this->setProtocole($value);
                break;
            case 5:
                $this->setNoReply($value);
                break;
            case 6:
                $this->setFooterMail($value);
                break;
            case 7:
                $this->setFromPfName($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonPlateformeVirtuellePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setDomain($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCodeDesign($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setProtocole($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setNoReply($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setFooterMail($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setFromPfName($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonPlateformeVirtuellePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::ID)) $criteria->add(CommonPlateformeVirtuellePeer::ID, $this->id);
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::DOMAIN)) $criteria->add(CommonPlateformeVirtuellePeer::DOMAIN, $this->domain);
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::NAME)) $criteria->add(CommonPlateformeVirtuellePeer::NAME, $this->name);
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::CODE_DESIGN)) $criteria->add(CommonPlateformeVirtuellePeer::CODE_DESIGN, $this->code_design);
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::PROTOCOLE)) $criteria->add(CommonPlateformeVirtuellePeer::PROTOCOLE, $this->protocole);
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::NO_REPLY)) $criteria->add(CommonPlateformeVirtuellePeer::NO_REPLY, $this->no_reply);
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::FOOTER_MAIL)) $criteria->add(CommonPlateformeVirtuellePeer::FOOTER_MAIL, $this->footer_mail);
        if ($this->isColumnModified(CommonPlateformeVirtuellePeer::FROM_PF_NAME)) $criteria->add(CommonPlateformeVirtuellePeer::FROM_PF_NAME, $this->from_pf_name);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonPlateformeVirtuellePeer::DATABASE_NAME);
        $criteria->add(CommonPlateformeVirtuellePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonPlateformeVirtuelle (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDomain($this->getDomain());
        $copyObj->setName($this->getName());
        $copyObj->setCodeDesign($this->getCodeDesign());
        $copyObj->setProtocole($this->getProtocole());
        $copyObj->setNoReply($this->getNoReply());
        $copyObj->setFooterMail($this->getFooterMail());
        $copyObj->setFromPfName($this->getFromPfName());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonOffress() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonOffres($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTMesRecherchess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTMesRecherches($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTelechargements() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTelechargement($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonPlateformeVirtuelleOrganismes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonPlateformeVirtuelleOrganisme($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonQuestionsDces() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonQuestionsDce($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonPlateformeVirtuelle Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonPlateformeVirtuellePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonPlateformeVirtuellePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonOffres' == $relationName) {
            $this->initCommonOffress();
        }
        if ('CommonTMesRecherches' == $relationName) {
            $this->initCommonTMesRecherchess();
        }
        if ('CommonTelechargement' == $relationName) {
            $this->initCommonTelechargements();
        }
        if ('CommonConsultation' == $relationName) {
            $this->initCommonConsultations();
        }
        if ('CommonPlateformeVirtuelleOrganisme' == $relationName) {
            $this->initCommonPlateformeVirtuelleOrganismes();
        }
        if ('CommonQuestionsDce' == $relationName) {
            $this->initCommonQuestionsDces();
        }
    }

    /**
     * Clears out the collCommonOffress collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     * @see        addCommonOffress()
     */
    public function clearCommonOffress()
    {
        $this->collCommonOffress = null; // important to set this to null since that means it is uninitialized
        $this->collCommonOffressPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonOffress collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonOffress($v = true)
    {
        $this->collCommonOffressPartial = $v;
    }

    /**
     * Initializes the collCommonOffress collection.
     *
     * By default this just sets the collCommonOffress collection to an empty array (like clearcollCommonOffress());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonOffress($overrideExisting = true)
    {
        if (null !== $this->collCommonOffress && !$overrideExisting) {
            return;
        }
        $this->collCommonOffress = new PropelObjectCollection();
        $this->collCommonOffress->setModel('CommonOffres');
    }

    /**
     * Gets an array of CommonOffres objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonPlateformeVirtuelle is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     * @throws PropelException
     */
    public function getCommonOffress($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonOffressPartial && !$this->isNew();
        if (null === $this->collCommonOffress || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonOffress) {
                // return empty collection
                $this->initCommonOffress();
            } else {
                $collCommonOffress = CommonOffresQuery::create(null, $criteria)
                    ->filterByCommonPlateformeVirtuelle($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonOffressPartial && count($collCommonOffress)) {
                      $this->initCommonOffress(false);

                      foreach ($collCommonOffress as $obj) {
                        if (false == $this->collCommonOffress->contains($obj)) {
                          $this->collCommonOffress->append($obj);
                        }
                      }

                      $this->collCommonOffressPartial = true;
                    }

                    $collCommonOffress->getInternalIterator()->rewind();

                    return $collCommonOffress;
                }

                if ($partial && $this->collCommonOffress) {
                    foreach ($this->collCommonOffress as $obj) {
                        if ($obj->isNew()) {
                            $collCommonOffress[] = $obj;
                        }
                    }
                }

                $this->collCommonOffress = $collCommonOffress;
                $this->collCommonOffressPartial = false;
            }
        }

        return $this->collCommonOffress;
    }

    /**
     * Sets a collection of CommonOffres objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonOffress A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setCommonOffress(PropelCollection $commonOffress, PropelPDO $con = null)
    {
        $commonOffressToDelete = $this->getCommonOffress(new Criteria(), $con)->diff($commonOffress);


        $this->commonOffressScheduledForDeletion = $commonOffressToDelete;

        foreach ($commonOffressToDelete as $commonOffresRemoved) {
            $commonOffresRemoved->setCommonPlateformeVirtuelle(null);
        }

        $this->collCommonOffress = null;
        foreach ($commonOffress as $commonOffres) {
            $this->addCommonOffres($commonOffres);
        }

        $this->collCommonOffress = $commonOffress;
        $this->collCommonOffressPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonOffres objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonOffres objects.
     * @throws PropelException
     */
    public function countCommonOffress(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonOffressPartial && !$this->isNew();
        if (null === $this->collCommonOffress || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonOffress) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonOffress());
            }
            $query = CommonOffresQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonPlateformeVirtuelle($this)
                ->count($con);
        }

        return count($this->collCommonOffress);
    }

    /**
     * Method called to associate a CommonOffres object to this object
     * through the CommonOffres foreign key attribute.
     *
     * @param   CommonOffres $l CommonOffres
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function addCommonOffres(CommonOffres $l)
    {
        if ($this->collCommonOffress === null) {
            $this->initCommonOffress();
            $this->collCommonOffressPartial = true;
        }
        if (!in_array($l, $this->collCommonOffress->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonOffres($l);
        }

        return $this;
    }

    /**
     * @param	CommonOffres $commonOffres The commonOffres object to add.
     */
    protected function doAddCommonOffres($commonOffres)
    {
        $this->collCommonOffress[]= $commonOffres;
        $commonOffres->setCommonPlateformeVirtuelle($this);
    }

    /**
     * @param	CommonOffres $commonOffres The commonOffres object to remove.
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function removeCommonOffres($commonOffres)
    {
        if ($this->getCommonOffress()->contains($commonOffres)) {
            $this->collCommonOffress->remove($this->collCommonOffress->search($commonOffres));
            if (null === $this->commonOffressScheduledForDeletion) {
                $this->commonOffressScheduledForDeletion = clone $this->collCommonOffress;
                $this->commonOffressScheduledForDeletion->clear();
            }
            $this->commonOffressScheduledForDeletion[]= $commonOffres;
            $commonOffres->setCommonPlateformeVirtuelle(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonBlobOrganismeFileRelatedByIdBlobHorodatageHash', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinCommonBlobOrganismeFileRelatedByIdBlobXmlReponse($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonBlobOrganismeFileRelatedByIdBlobXmlReponse', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }

    /**
     * Clears out the collCommonTMesRecherchess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     * @see        addCommonTMesRecherchess()
     */
    public function clearCommonTMesRecherchess()
    {
        $this->collCommonTMesRecherchess = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTMesRecherchessPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTMesRecherchess collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTMesRecherchess($v = true)
    {
        $this->collCommonTMesRecherchessPartial = $v;
    }

    /**
     * Initializes the collCommonTMesRecherchess collection.
     *
     * By default this just sets the collCommonTMesRecherchess collection to an empty array (like clearcollCommonTMesRecherchess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTMesRecherchess($overrideExisting = true)
    {
        if (null !== $this->collCommonTMesRecherchess && !$overrideExisting) {
            return;
        }
        $this->collCommonTMesRecherchess = new PropelObjectCollection();
        $this->collCommonTMesRecherchess->setModel('CommonTMesRecherches');
    }

    /**
     * Gets an array of CommonTMesRecherches objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonPlateformeVirtuelle is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTMesRecherches[] List of CommonTMesRecherches objects
     * @throws PropelException
     */
    public function getCommonTMesRecherchess($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTMesRecherchessPartial && !$this->isNew();
        if (null === $this->collCommonTMesRecherchess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTMesRecherchess) {
                // return empty collection
                $this->initCommonTMesRecherchess();
            } else {
                $collCommonTMesRecherchess = CommonTMesRecherchesQuery::create(null, $criteria)
                    ->filterByCommonPlateformeVirtuelle($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTMesRecherchessPartial && count($collCommonTMesRecherchess)) {
                      $this->initCommonTMesRecherchess(false);

                      foreach ($collCommonTMesRecherchess as $obj) {
                        if (false == $this->collCommonTMesRecherchess->contains($obj)) {
                          $this->collCommonTMesRecherchess->append($obj);
                        }
                      }

                      $this->collCommonTMesRecherchessPartial = true;
                    }

                    $collCommonTMesRecherchess->getInternalIterator()->rewind();

                    return $collCommonTMesRecherchess;
                }

                if ($partial && $this->collCommonTMesRecherchess) {
                    foreach ($this->collCommonTMesRecherchess as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTMesRecherchess[] = $obj;
                        }
                    }
                }

                $this->collCommonTMesRecherchess = $collCommonTMesRecherchess;
                $this->collCommonTMesRecherchessPartial = false;
            }
        }

        return $this->collCommonTMesRecherchess;
    }

    /**
     * Sets a collection of CommonTMesRecherches objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTMesRecherchess A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setCommonTMesRecherchess(PropelCollection $commonTMesRecherchess, PropelPDO $con = null)
    {
        $commonTMesRecherchessToDelete = $this->getCommonTMesRecherchess(new Criteria(), $con)->diff($commonTMesRecherchess);


        $this->commonTMesRecherchessScheduledForDeletion = $commonTMesRecherchessToDelete;

        foreach ($commonTMesRecherchessToDelete as $commonTMesRecherchesRemoved) {
            $commonTMesRecherchesRemoved->setCommonPlateformeVirtuelle(null);
        }

        $this->collCommonTMesRecherchess = null;
        foreach ($commonTMesRecherchess as $commonTMesRecherches) {
            $this->addCommonTMesRecherches($commonTMesRecherches);
        }

        $this->collCommonTMesRecherchess = $commonTMesRecherchess;
        $this->collCommonTMesRecherchessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTMesRecherches objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTMesRecherches objects.
     * @throws PropelException
     */
    public function countCommonTMesRecherchess(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTMesRecherchessPartial && !$this->isNew();
        if (null === $this->collCommonTMesRecherchess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTMesRecherchess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTMesRecherchess());
            }
            $query = CommonTMesRecherchesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonPlateformeVirtuelle($this)
                ->count($con);
        }

        return count($this->collCommonTMesRecherchess);
    }

    /**
     * Method called to associate a CommonTMesRecherches object to this object
     * through the CommonTMesRecherches foreign key attribute.
     *
     * @param   CommonTMesRecherches $l CommonTMesRecherches
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function addCommonTMesRecherches(CommonTMesRecherches $l)
    {
        if ($this->collCommonTMesRecherchess === null) {
            $this->initCommonTMesRecherchess();
            $this->collCommonTMesRecherchessPartial = true;
        }
        if (!in_array($l, $this->collCommonTMesRecherchess->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTMesRecherches($l);
        }

        return $this;
    }

    /**
     * @param	CommonTMesRecherches $commonTMesRecherches The commonTMesRecherches object to add.
     */
    protected function doAddCommonTMesRecherches($commonTMesRecherches)
    {
        $this->collCommonTMesRecherchess[]= $commonTMesRecherches;
        $commonTMesRecherches->setCommonPlateformeVirtuelle($this);
    }

    /**
     * @param	CommonTMesRecherches $commonTMesRecherches The commonTMesRecherches object to remove.
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function removeCommonTMesRecherches($commonTMesRecherches)
    {
        if ($this->getCommonTMesRecherchess()->contains($commonTMesRecherches)) {
            $this->collCommonTMesRecherchess->remove($this->collCommonTMesRecherchess->search($commonTMesRecherches));
            if (null === $this->commonTMesRecherchessScheduledForDeletion) {
                $this->commonTMesRecherchessScheduledForDeletion = clone $this->collCommonTMesRecherchess;
                $this->commonTMesRecherchessScheduledForDeletion->clear();
            }
            $this->commonTMesRecherchessScheduledForDeletion[]= $commonTMesRecherches;
            $commonTMesRecherches->setCommonPlateformeVirtuelle(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTelechargements collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     * @see        addCommonTelechargements()
     */
    public function clearCommonTelechargements()
    {
        $this->collCommonTelechargements = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTelechargementsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTelechargements collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTelechargements($v = true)
    {
        $this->collCommonTelechargementsPartial = $v;
    }

    /**
     * Initializes the collCommonTelechargements collection.
     *
     * By default this just sets the collCommonTelechargements collection to an empty array (like clearcollCommonTelechargements());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTelechargements($overrideExisting = true)
    {
        if (null !== $this->collCommonTelechargements && !$overrideExisting) {
            return;
        }
        $this->collCommonTelechargements = new PropelObjectCollection();
        $this->collCommonTelechargements->setModel('CommonTelechargement');
    }

    /**
     * Gets an array of CommonTelechargement objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonPlateformeVirtuelle is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     * @throws PropelException
     */
    public function getCommonTelechargements($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTelechargementsPartial && !$this->isNew();
        if (null === $this->collCommonTelechargements || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTelechargements) {
                // return empty collection
                $this->initCommonTelechargements();
            } else {
                $collCommonTelechargements = CommonTelechargementQuery::create(null, $criteria)
                    ->filterByCommonPlateformeVirtuelle($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTelechargementsPartial && count($collCommonTelechargements)) {
                      $this->initCommonTelechargements(false);

                      foreach ($collCommonTelechargements as $obj) {
                        if (false == $this->collCommonTelechargements->contains($obj)) {
                          $this->collCommonTelechargements->append($obj);
                        }
                      }

                      $this->collCommonTelechargementsPartial = true;
                    }

                    $collCommonTelechargements->getInternalIterator()->rewind();

                    return $collCommonTelechargements;
                }

                if ($partial && $this->collCommonTelechargements) {
                    foreach ($this->collCommonTelechargements as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTelechargements[] = $obj;
                        }
                    }
                }

                $this->collCommonTelechargements = $collCommonTelechargements;
                $this->collCommonTelechargementsPartial = false;
            }
        }

        return $this->collCommonTelechargements;
    }

    /**
     * Sets a collection of CommonTelechargement objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTelechargements A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setCommonTelechargements(PropelCollection $commonTelechargements, PropelPDO $con = null)
    {
        $commonTelechargementsToDelete = $this->getCommonTelechargements(new Criteria(), $con)->diff($commonTelechargements);


        $this->commonTelechargementsScheduledForDeletion = $commonTelechargementsToDelete;

        foreach ($commonTelechargementsToDelete as $commonTelechargementRemoved) {
            $commonTelechargementRemoved->setCommonPlateformeVirtuelle(null);
        }

        $this->collCommonTelechargements = null;
        foreach ($commonTelechargements as $commonTelechargement) {
            $this->addCommonTelechargement($commonTelechargement);
        }

        $this->collCommonTelechargements = $commonTelechargements;
        $this->collCommonTelechargementsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTelechargement objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTelechargement objects.
     * @throws PropelException
     */
    public function countCommonTelechargements(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTelechargementsPartial && !$this->isNew();
        if (null === $this->collCommonTelechargements || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTelechargements) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTelechargements());
            }
            $query = CommonTelechargementQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonPlateformeVirtuelle($this)
                ->count($con);
        }

        return count($this->collCommonTelechargements);
    }

    /**
     * Method called to associate a CommonTelechargement object to this object
     * through the CommonTelechargement foreign key attribute.
     *
     * @param   CommonTelechargement $l CommonTelechargement
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function addCommonTelechargement(CommonTelechargement $l)
    {
        if ($this->collCommonTelechargements === null) {
            $this->initCommonTelechargements();
            $this->collCommonTelechargementsPartial = true;
        }
        if (!in_array($l, $this->collCommonTelechargements->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTelechargement($l);
        }

        return $this;
    }

    /**
     * @param	CommonTelechargement $commonTelechargement The commonTelechargement object to add.
     */
    protected function doAddCommonTelechargement($commonTelechargement)
    {
        $this->collCommonTelechargements[]= $commonTelechargement;
        $commonTelechargement->setCommonPlateformeVirtuelle($this);
    }

    /**
     * @param	CommonTelechargement $commonTelechargement The commonTelechargement object to remove.
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function removeCommonTelechargement($commonTelechargement)
    {
        if ($this->getCommonTelechargements()->contains($commonTelechargement)) {
            $this->collCommonTelechargements->remove($this->collCommonTelechargements->search($commonTelechargement));
            if (null === $this->commonTelechargementsScheduledForDeletion) {
                $this->commonTelechargementsScheduledForDeletion = clone $this->collCommonTelechargements;
                $this->commonTelechargementsScheduledForDeletion->clear();
            }
            $this->commonTelechargementsScheduledForDeletion[]= $commonTelechargement;
            $commonTelechargement->setCommonPlateformeVirtuelle(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonTelechargements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     */
    public function getCommonTelechargementsJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTelechargementQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTelechargements($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonTelechargements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     */
    public function getCommonTelechargementsJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTelechargementQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonTelechargements($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonTelechargements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     */
    public function getCommonTelechargementsJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTelechargementQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTelechargements($query, $con);
    }

    /**
     * Clears out the collCommonConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     * @see        addCommonConsultations()
     */
    public function clearCommonConsultations()
    {
        $this->collCommonConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultations($v = true)
    {
        $this->collCommonConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonConsultations collection.
     *
     * By default this just sets the collCommonConsultations collection to an empty array (like clearcollCommonConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultations = new PropelObjectCollection();
        $this->collCommonConsultations->setModel('CommonConsultation');
    }

    /**
     * Gets an array of CommonConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonPlateformeVirtuelle is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     * @throws PropelException
     */
    public function getCommonConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultations) {
                // return empty collection
                $this->initCommonConsultations();
            } else {
                $collCommonConsultations = CommonConsultationQuery::create(null, $criteria)
                    ->filterByCommonPlateformeVirtuelle($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationsPartial && count($collCommonConsultations)) {
                      $this->initCommonConsultations(false);

                      foreach ($collCommonConsultations as $obj) {
                        if (false == $this->collCommonConsultations->contains($obj)) {
                          $this->collCommonConsultations->append($obj);
                        }
                      }

                      $this->collCommonConsultationsPartial = true;
                    }

                    $collCommonConsultations->getInternalIterator()->rewind();

                    return $collCommonConsultations;
                }

                if ($partial && $this->collCommonConsultations) {
                    foreach ($this->collCommonConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultations = $collCommonConsultations;
                $this->collCommonConsultationsPartial = false;
            }
        }

        return $this->collCommonConsultations;
    }

    /**
     * Sets a collection of CommonConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setCommonConsultations(PropelCollection $commonConsultations, PropelPDO $con = null)
    {
        $commonConsultationsToDelete = $this->getCommonConsultations(new Criteria(), $con)->diff($commonConsultations);


        $this->commonConsultationsScheduledForDeletion = $commonConsultationsToDelete;

        foreach ($commonConsultationsToDelete as $commonConsultationRemoved) {
            $commonConsultationRemoved->setCommonPlateformeVirtuelle(null);
        }

        $this->collCommonConsultations = null;
        foreach ($commonConsultations as $commonConsultation) {
            $this->addCommonConsultation($commonConsultation);
        }

        $this->collCommonConsultations = $commonConsultations;
        $this->collCommonConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultation objects.
     * @throws PropelException
     */
    public function countCommonConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultations());
            }
            $query = CommonConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonPlateformeVirtuelle($this)
                ->count($con);
        }

        return count($this->collCommonConsultations);
    }

    /**
     * Method called to associate a CommonConsultation object to this object
     * through the CommonConsultation foreign key attribute.
     *
     * @param   CommonConsultation $l CommonConsultation
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function addCommonConsultation(CommonConsultation $l)
    {
        if ($this->collCommonConsultations === null) {
            $this->initCommonConsultations();
            $this->collCommonConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultation $commonConsultation The commonConsultation object to add.
     */
    protected function doAddCommonConsultation($commonConsultation)
    {
        $this->collCommonConsultations[]= $commonConsultation;
        $commonConsultation->setCommonPlateformeVirtuelle($this);
    }

    /**
     * @param	CommonConsultation $commonConsultation The commonConsultation object to remove.
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function removeCommonConsultation($commonConsultation)
    {
        if ($this->getCommonConsultations()->contains($commonConsultation)) {
            $this->collCommonConsultations->remove($this->collCommonConsultations->search($commonConsultation));
            if (null === $this->commonConsultationsScheduledForDeletion) {
                $this->commonConsultationsScheduledForDeletion = clone $this->collCommonConsultations;
                $this->commonConsultationsScheduledForDeletion->clear();
            }
            $this->commonConsultationsScheduledForDeletion[]= $commonConsultation;
            $commonConsultation->setCommonPlateformeVirtuelle(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceValidation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceValidation', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonTTypeContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeContrat', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceId', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonConsultationRelatedByReferenceConsultationInit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonConsultationRelatedByReferenceConsultationInit', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceValidationIntermediaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceValidationIntermediaire', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonOperations($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOperations', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonDossierVolumineux($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonDossierVolumineux', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }

    /**
     * Clears out the collCommonPlateformeVirtuelleOrganismes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     * @see        addCommonPlateformeVirtuelleOrganismes()
     */
    public function clearCommonPlateformeVirtuelleOrganismes()
    {
        $this->collCommonPlateformeVirtuelleOrganismes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonPlateformeVirtuelleOrganismesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonPlateformeVirtuelleOrganismes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonPlateformeVirtuelleOrganismes($v = true)
    {
        $this->collCommonPlateformeVirtuelleOrganismesPartial = $v;
    }

    /**
     * Initializes the collCommonPlateformeVirtuelleOrganismes collection.
     *
     * By default this just sets the collCommonPlateformeVirtuelleOrganismes collection to an empty array (like clearcollCommonPlateformeVirtuelleOrganismes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonPlateformeVirtuelleOrganismes($overrideExisting = true)
    {
        if (null !== $this->collCommonPlateformeVirtuelleOrganismes && !$overrideExisting) {
            return;
        }
        $this->collCommonPlateformeVirtuelleOrganismes = new PropelObjectCollection();
        $this->collCommonPlateformeVirtuelleOrganismes->setModel('CommonPlateformeVirtuelleOrganisme');
    }

    /**
     * Gets an array of CommonPlateformeVirtuelleOrganisme objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonPlateformeVirtuelle is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonPlateformeVirtuelleOrganisme[] List of CommonPlateformeVirtuelleOrganisme objects
     * @throws PropelException
     */
    public function getCommonPlateformeVirtuelleOrganismes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonPlateformeVirtuelleOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonPlateformeVirtuelleOrganismes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonPlateformeVirtuelleOrganismes) {
                // return empty collection
                $this->initCommonPlateformeVirtuelleOrganismes();
            } else {
                $collCommonPlateformeVirtuelleOrganismes = CommonPlateformeVirtuelleOrganismeQuery::create(null, $criteria)
                    ->filterByCommonPlateformeVirtuelle($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonPlateformeVirtuelleOrganismesPartial && count($collCommonPlateformeVirtuelleOrganismes)) {
                      $this->initCommonPlateformeVirtuelleOrganismes(false);

                      foreach ($collCommonPlateformeVirtuelleOrganismes as $obj) {
                        if (false == $this->collCommonPlateformeVirtuelleOrganismes->contains($obj)) {
                          $this->collCommonPlateformeVirtuelleOrganismes->append($obj);
                        }
                      }

                      $this->collCommonPlateformeVirtuelleOrganismesPartial = true;
                    }

                    $collCommonPlateformeVirtuelleOrganismes->getInternalIterator()->rewind();

                    return $collCommonPlateformeVirtuelleOrganismes;
                }

                if ($partial && $this->collCommonPlateformeVirtuelleOrganismes) {
                    foreach ($this->collCommonPlateformeVirtuelleOrganismes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonPlateformeVirtuelleOrganismes[] = $obj;
                        }
                    }
                }

                $this->collCommonPlateformeVirtuelleOrganismes = $collCommonPlateformeVirtuelleOrganismes;
                $this->collCommonPlateformeVirtuelleOrganismesPartial = false;
            }
        }

        return $this->collCommonPlateformeVirtuelleOrganismes;
    }

    /**
     * Sets a collection of CommonPlateformeVirtuelleOrganisme objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonPlateformeVirtuelleOrganismes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setCommonPlateformeVirtuelleOrganismes(PropelCollection $commonPlateformeVirtuelleOrganismes, PropelPDO $con = null)
    {
        $commonPlateformeVirtuelleOrganismesToDelete = $this->getCommonPlateformeVirtuelleOrganismes(new Criteria(), $con)->diff($commonPlateformeVirtuelleOrganismes);


        $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion = $commonPlateformeVirtuelleOrganismesToDelete;

        foreach ($commonPlateformeVirtuelleOrganismesToDelete as $commonPlateformeVirtuelleOrganismeRemoved) {
            $commonPlateformeVirtuelleOrganismeRemoved->setCommonPlateformeVirtuelle(null);
        }

        $this->collCommonPlateformeVirtuelleOrganismes = null;
        foreach ($commonPlateformeVirtuelleOrganismes as $commonPlateformeVirtuelleOrganisme) {
            $this->addCommonPlateformeVirtuelleOrganisme($commonPlateformeVirtuelleOrganisme);
        }

        $this->collCommonPlateformeVirtuelleOrganismes = $commonPlateformeVirtuelleOrganismes;
        $this->collCommonPlateformeVirtuelleOrganismesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonPlateformeVirtuelleOrganisme objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonPlateformeVirtuelleOrganisme objects.
     * @throws PropelException
     */
    public function countCommonPlateformeVirtuelleOrganismes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonPlateformeVirtuelleOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonPlateformeVirtuelleOrganismes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonPlateformeVirtuelleOrganismes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonPlateformeVirtuelleOrganismes());
            }
            $query = CommonPlateformeVirtuelleOrganismeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonPlateformeVirtuelle($this)
                ->count($con);
        }

        return count($this->collCommonPlateformeVirtuelleOrganismes);
    }

    /**
     * Method called to associate a CommonPlateformeVirtuelleOrganisme object to this object
     * through the CommonPlateformeVirtuelleOrganisme foreign key attribute.
     *
     * @param   CommonPlateformeVirtuelleOrganisme $l CommonPlateformeVirtuelleOrganisme
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function addCommonPlateformeVirtuelleOrganisme(CommonPlateformeVirtuelleOrganisme $l)
    {
        if ($this->collCommonPlateformeVirtuelleOrganismes === null) {
            $this->initCommonPlateformeVirtuelleOrganismes();
            $this->collCommonPlateformeVirtuelleOrganismesPartial = true;
        }
        if (!in_array($l, $this->collCommonPlateformeVirtuelleOrganismes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonPlateformeVirtuelleOrganisme($l);
        }

        return $this;
    }

    /**
     * @param	CommonPlateformeVirtuelleOrganisme $commonPlateformeVirtuelleOrganisme The commonPlateformeVirtuelleOrganisme object to add.
     */
    protected function doAddCommonPlateformeVirtuelleOrganisme($commonPlateformeVirtuelleOrganisme)
    {
        $this->collCommonPlateformeVirtuelleOrganismes[]= $commonPlateformeVirtuelleOrganisme;
        $commonPlateformeVirtuelleOrganisme->setCommonPlateformeVirtuelle($this);
    }

    /**
     * @param	CommonPlateformeVirtuelleOrganisme $commonPlateformeVirtuelleOrganisme The commonPlateformeVirtuelleOrganisme object to remove.
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function removeCommonPlateformeVirtuelleOrganisme($commonPlateformeVirtuelleOrganisme)
    {
        if ($this->getCommonPlateformeVirtuelleOrganismes()->contains($commonPlateformeVirtuelleOrganisme)) {
            $this->collCommonPlateformeVirtuelleOrganismes->remove($this->collCommonPlateformeVirtuelleOrganismes->search($commonPlateformeVirtuelleOrganisme));
            if (null === $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion) {
                $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion = clone $this->collCommonPlateformeVirtuelleOrganismes;
                $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion->clear();
            }
            $this->commonPlateformeVirtuelleOrganismesScheduledForDeletion[]= clone $commonPlateformeVirtuelleOrganisme;
            $commonPlateformeVirtuelleOrganisme->setCommonPlateformeVirtuelle(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonPlateformeVirtuelleOrganismes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonPlateformeVirtuelleOrganisme[] List of CommonPlateformeVirtuelleOrganisme objects
     */
    public function getCommonPlateformeVirtuelleOrganismesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonPlateformeVirtuelleOrganismeQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonPlateformeVirtuelleOrganismes($query, $con);
    }

    /**
     * Clears out the collCommonQuestionsDces collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     * @see        addCommonQuestionsDces()
     */
    public function clearCommonQuestionsDces()
    {
        $this->collCommonQuestionsDces = null; // important to set this to null since that means it is uninitialized
        $this->collCommonQuestionsDcesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonQuestionsDces collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonQuestionsDces($v = true)
    {
        $this->collCommonQuestionsDcesPartial = $v;
    }

    /**
     * Initializes the collCommonQuestionsDces collection.
     *
     * By default this just sets the collCommonQuestionsDces collection to an empty array (like clearcollCommonQuestionsDces());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonQuestionsDces($overrideExisting = true)
    {
        if (null !== $this->collCommonQuestionsDces && !$overrideExisting) {
            return;
        }
        $this->collCommonQuestionsDces = new PropelObjectCollection();
        $this->collCommonQuestionsDces->setModel('CommonQuestionsDce');
    }

    /**
     * Gets an array of CommonQuestionsDce objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonPlateformeVirtuelle is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonQuestionsDce[] List of CommonQuestionsDce objects
     * @throws PropelException
     */
    public function getCommonQuestionsDces($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonQuestionsDcesPartial && !$this->isNew();
        if (null === $this->collCommonQuestionsDces || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonQuestionsDces) {
                // return empty collection
                $this->initCommonQuestionsDces();
            } else {
                $collCommonQuestionsDces = CommonQuestionsDceQuery::create(null, $criteria)
                    ->filterByCommonPlateformeVirtuelle($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonQuestionsDcesPartial && count($collCommonQuestionsDces)) {
                      $this->initCommonQuestionsDces(false);

                      foreach ($collCommonQuestionsDces as $obj) {
                        if (false == $this->collCommonQuestionsDces->contains($obj)) {
                          $this->collCommonQuestionsDces->append($obj);
                        }
                      }

                      $this->collCommonQuestionsDcesPartial = true;
                    }

                    $collCommonQuestionsDces->getInternalIterator()->rewind();

                    return $collCommonQuestionsDces;
                }

                if ($partial && $this->collCommonQuestionsDces) {
                    foreach ($this->collCommonQuestionsDces as $obj) {
                        if ($obj->isNew()) {
                            $collCommonQuestionsDces[] = $obj;
                        }
                    }
                }

                $this->collCommonQuestionsDces = $collCommonQuestionsDces;
                $this->collCommonQuestionsDcesPartial = false;
            }
        }

        return $this->collCommonQuestionsDces;
    }

    /**
     * Sets a collection of CommonQuestionsDce objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonQuestionsDces A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function setCommonQuestionsDces(PropelCollection $commonQuestionsDces, PropelPDO $con = null)
    {
        $commonQuestionsDcesToDelete = $this->getCommonQuestionsDces(new Criteria(), $con)->diff($commonQuestionsDces);


        $this->commonQuestionsDcesScheduledForDeletion = $commonQuestionsDcesToDelete;

        foreach ($commonQuestionsDcesToDelete as $commonQuestionsDceRemoved) {
            $commonQuestionsDceRemoved->setCommonPlateformeVirtuelle(null);
        }

        $this->collCommonQuestionsDces = null;
        foreach ($commonQuestionsDces as $commonQuestionsDce) {
            $this->addCommonQuestionsDce($commonQuestionsDce);
        }

        $this->collCommonQuestionsDces = $commonQuestionsDces;
        $this->collCommonQuestionsDcesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonQuestionsDce objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonQuestionsDce objects.
     * @throws PropelException
     */
    public function countCommonQuestionsDces(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonQuestionsDcesPartial && !$this->isNew();
        if (null === $this->collCommonQuestionsDces || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonQuestionsDces) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonQuestionsDces());
            }
            $query = CommonQuestionsDceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonPlateformeVirtuelle($this)
                ->count($con);
        }

        return count($this->collCommonQuestionsDces);
    }

    /**
     * Method called to associate a CommonQuestionsDce object to this object
     * through the CommonQuestionsDce foreign key attribute.
     *
     * @param   CommonQuestionsDce $l CommonQuestionsDce
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function addCommonQuestionsDce(CommonQuestionsDce $l)
    {
        if ($this->collCommonQuestionsDces === null) {
            $this->initCommonQuestionsDces();
            $this->collCommonQuestionsDcesPartial = true;
        }
        if (!in_array($l, $this->collCommonQuestionsDces->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonQuestionsDce($l);
        }

        return $this;
    }

    /**
     * @param	CommonQuestionsDce $commonQuestionsDce The commonQuestionsDce object to add.
     */
    protected function doAddCommonQuestionsDce($commonQuestionsDce)
    {
        $this->collCommonQuestionsDces[]= $commonQuestionsDce;
        $commonQuestionsDce->setCommonPlateformeVirtuelle($this);
    }

    /**
     * @param	CommonQuestionsDce $commonQuestionsDce The commonQuestionsDce object to remove.
     * @return CommonPlateformeVirtuelle The current object (for fluent API support)
     */
    public function removeCommonQuestionsDce($commonQuestionsDce)
    {
        if ($this->getCommonQuestionsDces()->contains($commonQuestionsDce)) {
            $this->collCommonQuestionsDces->remove($this->collCommonQuestionsDces->search($commonQuestionsDce));
            if (null === $this->commonQuestionsDcesScheduledForDeletion) {
                $this->commonQuestionsDcesScheduledForDeletion = clone $this->collCommonQuestionsDces;
                $this->commonQuestionsDcesScheduledForDeletion->clear();
            }
            $this->commonQuestionsDcesScheduledForDeletion[]= $commonQuestionsDce;
            $commonQuestionsDce->setCommonPlateformeVirtuelle(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonPlateformeVirtuelle is new, it will return
     * an empty collection; or if this CommonPlateformeVirtuelle has previously
     * been saved, it will retrieve related CommonQuestionsDces from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonPlateformeVirtuelle.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonQuestionsDce[] List of CommonQuestionsDce objects
     */
    public function getCommonQuestionsDcesJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonQuestionsDceQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonQuestionsDces($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->domain = null;
        $this->name = null;
        $this->code_design = null;
        $this->protocole = null;
        $this->no_reply = null;
        $this->footer_mail = null;
        $this->from_pf_name = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonOffress) {
                foreach ($this->collCommonOffress as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTMesRecherchess) {
                foreach ($this->collCommonTMesRecherchess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTelechargements) {
                foreach ($this->collCommonTelechargements as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonConsultations) {
                foreach ($this->collCommonConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonPlateformeVirtuelleOrganismes) {
                foreach ($this->collCommonPlateformeVirtuelleOrganismes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonQuestionsDces) {
                foreach ($this->collCommonQuestionsDces as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonOffress instanceof PropelCollection) {
            $this->collCommonOffress->clearIterator();
        }
        $this->collCommonOffress = null;
        if ($this->collCommonTMesRecherchess instanceof PropelCollection) {
            $this->collCommonTMesRecherchess->clearIterator();
        }
        $this->collCommonTMesRecherchess = null;
        if ($this->collCommonTelechargements instanceof PropelCollection) {
            $this->collCommonTelechargements->clearIterator();
        }
        $this->collCommonTelechargements = null;
        if ($this->collCommonConsultations instanceof PropelCollection) {
            $this->collCommonConsultations->clearIterator();
        }
        $this->collCommonConsultations = null;
        if ($this->collCommonPlateformeVirtuelleOrganismes instanceof PropelCollection) {
            $this->collCommonPlateformeVirtuelleOrganismes->clearIterator();
        }
        $this->collCommonPlateformeVirtuelleOrganismes = null;
        if ($this->collCommonQuestionsDces instanceof PropelCollection) {
            $this->collCommonQuestionsDces->clearIterator();
        }
        $this->collCommonQuestionsDces = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonPlateformeVirtuellePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
