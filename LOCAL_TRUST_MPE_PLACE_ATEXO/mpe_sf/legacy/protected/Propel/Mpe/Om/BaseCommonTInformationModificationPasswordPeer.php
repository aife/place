<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTInformationModificationPassword;
use Application\Propel\Mpe\CommonTInformationModificationPasswordPeer;
use Application\Propel\Mpe\Map\CommonTInformationModificationPasswordTableMap;

/**
 * Base static class for performing query and update operations on the 't_information_modification_password' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTInformationModificationPasswordPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_information_modification_password';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTInformationModificationPassword';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTInformationModificationPasswordTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 8;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 8;

    /** the column name for the id field */
    const ID = 't_information_modification_password.id';

    /** the column name for the id_user field */
    const ID_USER = 't_information_modification_password.id_user';

    /** the column name for the type_user field */
    const TYPE_USER = 't_information_modification_password.type_user';

    /** the column name for the email field */
    const EMAIL = 't_information_modification_password.email';

    /** the column name for the date_demande_modification field */
    const DATE_DEMANDE_MODIFICATION = 't_information_modification_password.date_demande_modification';

    /** the column name for the date_fin_validite field */
    const DATE_FIN_VALIDITE = 't_information_modification_password.date_fin_validite';

    /** the column name for the jeton field */
    const JETON = 't_information_modification_password.jeton';

    /** the column name for the modification_faite field */
    const MODIFICATION_FAITE = 't_information_modification_password.modification_faite';

    /** The enumerated values for the modification_faite field */
    const MODIFICATION_FAITE_0 = '0';
    const MODIFICATION_FAITE_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTInformationModificationPassword objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTInformationModificationPassword[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTInformationModificationPasswordPeer::$fieldNames[CommonTInformationModificationPasswordPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'IdUser', 'TypeUser', 'Email', 'DateDemandeModification', 'DateFinValidite', 'Jeton', 'ModificationFaite', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'idUser', 'typeUser', 'email', 'dateDemandeModification', 'dateFinValidite', 'jeton', 'modificationFaite', ),
        BasePeer::TYPE_COLNAME => array (CommonTInformationModificationPasswordPeer::ID, CommonTInformationModificationPasswordPeer::ID_USER, CommonTInformationModificationPasswordPeer::TYPE_USER, CommonTInformationModificationPasswordPeer::EMAIL, CommonTInformationModificationPasswordPeer::DATE_DEMANDE_MODIFICATION, CommonTInformationModificationPasswordPeer::DATE_FIN_VALIDITE, CommonTInformationModificationPasswordPeer::JETON, CommonTInformationModificationPasswordPeer::MODIFICATION_FAITE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ID_USER', 'TYPE_USER', 'EMAIL', 'DATE_DEMANDE_MODIFICATION', 'DATE_FIN_VALIDITE', 'JETON', 'MODIFICATION_FAITE', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'id_user', 'type_user', 'email', 'date_demande_modification', 'date_fin_validite', 'jeton', 'modification_faite', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTInformationModificationPasswordPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdUser' => 1, 'TypeUser' => 2, 'Email' => 3, 'DateDemandeModification' => 4, 'DateFinValidite' => 5, 'Jeton' => 6, 'ModificationFaite' => 7, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'idUser' => 1, 'typeUser' => 2, 'email' => 3, 'dateDemandeModification' => 4, 'dateFinValidite' => 5, 'jeton' => 6, 'modificationFaite' => 7, ),
        BasePeer::TYPE_COLNAME => array (CommonTInformationModificationPasswordPeer::ID => 0, CommonTInformationModificationPasswordPeer::ID_USER => 1, CommonTInformationModificationPasswordPeer::TYPE_USER => 2, CommonTInformationModificationPasswordPeer::EMAIL => 3, CommonTInformationModificationPasswordPeer::DATE_DEMANDE_MODIFICATION => 4, CommonTInformationModificationPasswordPeer::DATE_FIN_VALIDITE => 5, CommonTInformationModificationPasswordPeer::JETON => 6, CommonTInformationModificationPasswordPeer::MODIFICATION_FAITE => 7, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ID_USER' => 1, 'TYPE_USER' => 2, 'EMAIL' => 3, 'DATE_DEMANDE_MODIFICATION' => 4, 'DATE_FIN_VALIDITE' => 5, 'JETON' => 6, 'MODIFICATION_FAITE' => 7, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_user' => 1, 'type_user' => 2, 'email' => 3, 'date_demande_modification' => 4, 'date_fin_validite' => 5, 'jeton' => 6, 'modification_faite' => 7, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTInformationModificationPasswordPeer::MODIFICATION_FAITE => array(
            CommonTInformationModificationPasswordPeer::MODIFICATION_FAITE_0,
            CommonTInformationModificationPasswordPeer::MODIFICATION_FAITE_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTInformationModificationPasswordPeer::getFieldNames($toType);
        $key = isset(CommonTInformationModificationPasswordPeer::$fieldKeys[$fromType][$name]) ? CommonTInformationModificationPasswordPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTInformationModificationPasswordPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTInformationModificationPasswordPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTInformationModificationPasswordPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTInformationModificationPasswordPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTInformationModificationPasswordPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTInformationModificationPasswordPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTInformationModificationPasswordPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTInformationModificationPasswordPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTInformationModificationPasswordPeer::ID);
            $criteria->addSelectColumn(CommonTInformationModificationPasswordPeer::ID_USER);
            $criteria->addSelectColumn(CommonTInformationModificationPasswordPeer::TYPE_USER);
            $criteria->addSelectColumn(CommonTInformationModificationPasswordPeer::EMAIL);
            $criteria->addSelectColumn(CommonTInformationModificationPasswordPeer::DATE_DEMANDE_MODIFICATION);
            $criteria->addSelectColumn(CommonTInformationModificationPasswordPeer::DATE_FIN_VALIDITE);
            $criteria->addSelectColumn(CommonTInformationModificationPasswordPeer::JETON);
            $criteria->addSelectColumn(CommonTInformationModificationPasswordPeer::MODIFICATION_FAITE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.id_user');
            $criteria->addSelectColumn($alias . '.type_user');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.date_demande_modification');
            $criteria->addSelectColumn($alias . '.date_fin_validite');
            $criteria->addSelectColumn($alias . '.jeton');
            $criteria->addSelectColumn($alias . '.modification_faite');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTInformationModificationPasswordPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTInformationModificationPasswordPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTInformationModificationPasswordPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTInformationModificationPasswordPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTInformationModificationPassword
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTInformationModificationPasswordPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTInformationModificationPasswordPeer::populateObjects(CommonTInformationModificationPasswordPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTInformationModificationPasswordPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTInformationModificationPasswordPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTInformationModificationPasswordPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTInformationModificationPassword $obj A CommonTInformationModificationPassword object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTInformationModificationPasswordPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTInformationModificationPassword object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTInformationModificationPassword) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTInformationModificationPassword object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTInformationModificationPasswordPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTInformationModificationPassword Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTInformationModificationPasswordPeer::$instances[$key])) {
                return CommonTInformationModificationPasswordPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTInformationModificationPasswordPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTInformationModificationPasswordPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_information_modification_password
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTInformationModificationPasswordPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTInformationModificationPasswordPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTInformationModificationPasswordPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTInformationModificationPasswordPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTInformationModificationPassword object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTInformationModificationPasswordPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTInformationModificationPasswordPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTInformationModificationPasswordPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTInformationModificationPasswordPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTInformationModificationPasswordPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTInformationModificationPasswordPeer::DATABASE_NAME)->getTable(CommonTInformationModificationPasswordPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTInformationModificationPasswordPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTInformationModificationPasswordPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTInformationModificationPasswordTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTInformationModificationPasswordPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTInformationModificationPassword or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTInformationModificationPassword object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTInformationModificationPasswordPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTInformationModificationPassword object
        }

        if ($criteria->containsKey(CommonTInformationModificationPasswordPeer::ID) && $criteria->keyContainsValue(CommonTInformationModificationPasswordPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTInformationModificationPasswordPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTInformationModificationPasswordPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTInformationModificationPassword or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTInformationModificationPassword object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTInformationModificationPasswordPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTInformationModificationPasswordPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTInformationModificationPasswordPeer::ID);
            $value = $criteria->remove(CommonTInformationModificationPasswordPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTInformationModificationPasswordPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTInformationModificationPasswordPeer::TABLE_NAME);
            }

        } else { // $values is CommonTInformationModificationPassword object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTInformationModificationPasswordPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_information_modification_password table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTInformationModificationPasswordPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTInformationModificationPasswordPeer::TABLE_NAME, $con, CommonTInformationModificationPasswordPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTInformationModificationPasswordPeer::clearInstancePool();
            CommonTInformationModificationPasswordPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTInformationModificationPassword or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTInformationModificationPassword object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTInformationModificationPasswordPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTInformationModificationPasswordPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTInformationModificationPassword) { // it's a model object
            // invalidate the cache for this single object
            CommonTInformationModificationPasswordPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTInformationModificationPasswordPeer::DATABASE_NAME);
            $criteria->add(CommonTInformationModificationPasswordPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTInformationModificationPasswordPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTInformationModificationPasswordPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTInformationModificationPasswordPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTInformationModificationPassword object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTInformationModificationPassword $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTInformationModificationPasswordPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTInformationModificationPasswordPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTInformationModificationPasswordPeer::DATABASE_NAME, CommonTInformationModificationPasswordPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTInformationModificationPassword
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTInformationModificationPasswordPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTInformationModificationPasswordPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTInformationModificationPasswordPeer::DATABASE_NAME);
        $criteria->add(CommonTInformationModificationPasswordPeer::ID, $pk);

        $v = CommonTInformationModificationPasswordPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTInformationModificationPassword[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTInformationModificationPasswordPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTInformationModificationPasswordPeer::DATABASE_NAME);
            $criteria->add(CommonTInformationModificationPasswordPeer::ID, $pks, Criteria::IN);
            $objs = CommonTInformationModificationPasswordPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTInformationModificationPasswordPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTInformationModificationPasswordPeer::buildTableMap();

