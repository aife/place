<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTPubliciteFavoris;
use Application\Propel\Mpe\CommonTPubliciteFavorisPeer;
use Application\Propel\Mpe\CommonTPubliciteFavorisQuery;

/**
 * Base class that represents a query for the 't_publicite_favoris' table.
 *
 *
 *
 * @method CommonTPubliciteFavorisQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTPubliciteFavorisQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTPubliciteFavorisQuery orderByOldServiceId($order = Criteria::ASC) Order by the old_service_id column
 * @method CommonTPubliciteFavorisQuery orderByLibelle($order = Criteria::ASC) Order by the libelle column
 * @method CommonTPubliciteFavorisQuery orderByRegimeFinancierCautionnement($order = Criteria::ASC) Order by the regime_financier_cautionnement column
 * @method CommonTPubliciteFavorisQuery orderByRegimeFinancierModalitesFinancement($order = Criteria::ASC) Order by the regime_financier_modalites_financement column
 * @method CommonTPubliciteFavorisQuery orderByAcheteurCorrespondant($order = Criteria::ASC) Order by the acheteur_correspondant column
 * @method CommonTPubliciteFavorisQuery orderByAcheteurNomOrganisme($order = Criteria::ASC) Order by the acheteur_nom_organisme column
 * @method CommonTPubliciteFavorisQuery orderByAcheteurAdresse($order = Criteria::ASC) Order by the acheteur_adresse column
 * @method CommonTPubliciteFavorisQuery orderByAcheteurCp($order = Criteria::ASC) Order by the acheteur_cp column
 * @method CommonTPubliciteFavorisQuery orderByAcheteurVille($order = Criteria::ASC) Order by the acheteur_ville column
 * @method CommonTPubliciteFavorisQuery orderByAcheteurUrl($order = Criteria::ASC) Order by the acheteur_url column
 * @method CommonTPubliciteFavorisQuery orderByFactureDenomination($order = Criteria::ASC) Order by the facture_denomination column
 * @method CommonTPubliciteFavorisQuery orderByFactureAdresse($order = Criteria::ASC) Order by the facture_adresse column
 * @method CommonTPubliciteFavorisQuery orderByFactureCp($order = Criteria::ASC) Order by the facture_cp column
 * @method CommonTPubliciteFavorisQuery orderByFactureVille($order = Criteria::ASC) Order by the facture_ville column
 * @method CommonTPubliciteFavorisQuery orderByInstanceRecoursOrganisme($order = Criteria::ASC) Order by the instance_recours_organisme column
 * @method CommonTPubliciteFavorisQuery orderByInstanceRecoursAdresse($order = Criteria::ASC) Order by the instance_recours_adresse column
 * @method CommonTPubliciteFavorisQuery orderByInstanceRecoursCp($order = Criteria::ASC) Order by the instance_recours_cp column
 * @method CommonTPubliciteFavorisQuery orderByInstanceRecoursVille($order = Criteria::ASC) Order by the instance_recours_ville column
 * @method CommonTPubliciteFavorisQuery orderByInstanceRecoursUrl($order = Criteria::ASC) Order by the instance_recours_url column
 * @method CommonTPubliciteFavorisQuery orderByAcheteurTelephone($order = Criteria::ASC) Order by the acheteur_telephone column
 * @method CommonTPubliciteFavorisQuery orderByAcheteurEmail($order = Criteria::ASC) Order by the acheteur_email column
 * @method CommonTPubliciteFavorisQuery orderByServiceId($order = Criteria::ASC) Order by the service_id column
 *
 * @method CommonTPubliciteFavorisQuery groupById() Group by the id column
 * @method CommonTPubliciteFavorisQuery groupByOrganisme() Group by the organisme column
 * @method CommonTPubliciteFavorisQuery groupByOldServiceId() Group by the old_service_id column
 * @method CommonTPubliciteFavorisQuery groupByLibelle() Group by the libelle column
 * @method CommonTPubliciteFavorisQuery groupByRegimeFinancierCautionnement() Group by the regime_financier_cautionnement column
 * @method CommonTPubliciteFavorisQuery groupByRegimeFinancierModalitesFinancement() Group by the regime_financier_modalites_financement column
 * @method CommonTPubliciteFavorisQuery groupByAcheteurCorrespondant() Group by the acheteur_correspondant column
 * @method CommonTPubliciteFavorisQuery groupByAcheteurNomOrganisme() Group by the acheteur_nom_organisme column
 * @method CommonTPubliciteFavorisQuery groupByAcheteurAdresse() Group by the acheteur_adresse column
 * @method CommonTPubliciteFavorisQuery groupByAcheteurCp() Group by the acheteur_cp column
 * @method CommonTPubliciteFavorisQuery groupByAcheteurVille() Group by the acheteur_ville column
 * @method CommonTPubliciteFavorisQuery groupByAcheteurUrl() Group by the acheteur_url column
 * @method CommonTPubliciteFavorisQuery groupByFactureDenomination() Group by the facture_denomination column
 * @method CommonTPubliciteFavorisQuery groupByFactureAdresse() Group by the facture_adresse column
 * @method CommonTPubliciteFavorisQuery groupByFactureCp() Group by the facture_cp column
 * @method CommonTPubliciteFavorisQuery groupByFactureVille() Group by the facture_ville column
 * @method CommonTPubliciteFavorisQuery groupByInstanceRecoursOrganisme() Group by the instance_recours_organisme column
 * @method CommonTPubliciteFavorisQuery groupByInstanceRecoursAdresse() Group by the instance_recours_adresse column
 * @method CommonTPubliciteFavorisQuery groupByInstanceRecoursCp() Group by the instance_recours_cp column
 * @method CommonTPubliciteFavorisQuery groupByInstanceRecoursVille() Group by the instance_recours_ville column
 * @method CommonTPubliciteFavorisQuery groupByInstanceRecoursUrl() Group by the instance_recours_url column
 * @method CommonTPubliciteFavorisQuery groupByAcheteurTelephone() Group by the acheteur_telephone column
 * @method CommonTPubliciteFavorisQuery groupByAcheteurEmail() Group by the acheteur_email column
 * @method CommonTPubliciteFavorisQuery groupByServiceId() Group by the service_id column
 *
 * @method CommonTPubliciteFavorisQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTPubliciteFavorisQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTPubliciteFavorisQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTPubliciteFavoris findOne(PropelPDO $con = null) Return the first CommonTPubliciteFavoris matching the query
 * @method CommonTPubliciteFavoris findOneOrCreate(PropelPDO $con = null) Return the first CommonTPubliciteFavoris matching the query, or a new CommonTPubliciteFavoris object populated from the query conditions when no match is found
 *
 * @method CommonTPubliciteFavoris findOneByOrganisme(string $organisme) Return the first CommonTPubliciteFavoris filtered by the organisme column
 * @method CommonTPubliciteFavoris findOneByOldServiceId(int $old_service_id) Return the first CommonTPubliciteFavoris filtered by the old_service_id column
 * @method CommonTPubliciteFavoris findOneByLibelle(string $libelle) Return the first CommonTPubliciteFavoris filtered by the libelle column
 * @method CommonTPubliciteFavoris findOneByRegimeFinancierCautionnement(string $regime_financier_cautionnement) Return the first CommonTPubliciteFavoris filtered by the regime_financier_cautionnement column
 * @method CommonTPubliciteFavoris findOneByRegimeFinancierModalitesFinancement(string $regime_financier_modalites_financement) Return the first CommonTPubliciteFavoris filtered by the regime_financier_modalites_financement column
 * @method CommonTPubliciteFavoris findOneByAcheteurCorrespondant(string $acheteur_correspondant) Return the first CommonTPubliciteFavoris filtered by the acheteur_correspondant column
 * @method CommonTPubliciteFavoris findOneByAcheteurNomOrganisme(string $acheteur_nom_organisme) Return the first CommonTPubliciteFavoris filtered by the acheteur_nom_organisme column
 * @method CommonTPubliciteFavoris findOneByAcheteurAdresse(string $acheteur_adresse) Return the first CommonTPubliciteFavoris filtered by the acheteur_adresse column
 * @method CommonTPubliciteFavoris findOneByAcheteurCp(string $acheteur_cp) Return the first CommonTPubliciteFavoris filtered by the acheteur_cp column
 * @method CommonTPubliciteFavoris findOneByAcheteurVille(string $acheteur_ville) Return the first CommonTPubliciteFavoris filtered by the acheteur_ville column
 * @method CommonTPubliciteFavoris findOneByAcheteurUrl(string $acheteur_url) Return the first CommonTPubliciteFavoris filtered by the acheteur_url column
 * @method CommonTPubliciteFavoris findOneByFactureDenomination(string $facture_denomination) Return the first CommonTPubliciteFavoris filtered by the facture_denomination column
 * @method CommonTPubliciteFavoris findOneByFactureAdresse(string $facture_adresse) Return the first CommonTPubliciteFavoris filtered by the facture_adresse column
 * @method CommonTPubliciteFavoris findOneByFactureCp(string $facture_cp) Return the first CommonTPubliciteFavoris filtered by the facture_cp column
 * @method CommonTPubliciteFavoris findOneByFactureVille(string $facture_ville) Return the first CommonTPubliciteFavoris filtered by the facture_ville column
 * @method CommonTPubliciteFavoris findOneByInstanceRecoursOrganisme(string $instance_recours_organisme) Return the first CommonTPubliciteFavoris filtered by the instance_recours_organisme column
 * @method CommonTPubliciteFavoris findOneByInstanceRecoursAdresse(string $instance_recours_adresse) Return the first CommonTPubliciteFavoris filtered by the instance_recours_adresse column
 * @method CommonTPubliciteFavoris findOneByInstanceRecoursCp(string $instance_recours_cp) Return the first CommonTPubliciteFavoris filtered by the instance_recours_cp column
 * @method CommonTPubliciteFavoris findOneByInstanceRecoursVille(string $instance_recours_ville) Return the first CommonTPubliciteFavoris filtered by the instance_recours_ville column
 * @method CommonTPubliciteFavoris findOneByInstanceRecoursUrl(string $instance_recours_url) Return the first CommonTPubliciteFavoris filtered by the instance_recours_url column
 * @method CommonTPubliciteFavoris findOneByAcheteurTelephone(string $acheteur_telephone) Return the first CommonTPubliciteFavoris filtered by the acheteur_telephone column
 * @method CommonTPubliciteFavoris findOneByAcheteurEmail(string $acheteur_email) Return the first CommonTPubliciteFavoris filtered by the acheteur_email column
 * @method CommonTPubliciteFavoris findOneByServiceId(string $service_id) Return the first CommonTPubliciteFavoris filtered by the service_id column
 *
 * @method array findById(int $id) Return CommonTPubliciteFavoris objects filtered by the id column
 * @method array findByOrganisme(string $organisme) Return CommonTPubliciteFavoris objects filtered by the organisme column
 * @method array findByOldServiceId(int $old_service_id) Return CommonTPubliciteFavoris objects filtered by the old_service_id column
 * @method array findByLibelle(string $libelle) Return CommonTPubliciteFavoris objects filtered by the libelle column
 * @method array findByRegimeFinancierCautionnement(string $regime_financier_cautionnement) Return CommonTPubliciteFavoris objects filtered by the regime_financier_cautionnement column
 * @method array findByRegimeFinancierModalitesFinancement(string $regime_financier_modalites_financement) Return CommonTPubliciteFavoris objects filtered by the regime_financier_modalites_financement column
 * @method array findByAcheteurCorrespondant(string $acheteur_correspondant) Return CommonTPubliciteFavoris objects filtered by the acheteur_correspondant column
 * @method array findByAcheteurNomOrganisme(string $acheteur_nom_organisme) Return CommonTPubliciteFavoris objects filtered by the acheteur_nom_organisme column
 * @method array findByAcheteurAdresse(string $acheteur_adresse) Return CommonTPubliciteFavoris objects filtered by the acheteur_adresse column
 * @method array findByAcheteurCp(string $acheteur_cp) Return CommonTPubliciteFavoris objects filtered by the acheteur_cp column
 * @method array findByAcheteurVille(string $acheteur_ville) Return CommonTPubliciteFavoris objects filtered by the acheteur_ville column
 * @method array findByAcheteurUrl(string $acheteur_url) Return CommonTPubliciteFavoris objects filtered by the acheteur_url column
 * @method array findByFactureDenomination(string $facture_denomination) Return CommonTPubliciteFavoris objects filtered by the facture_denomination column
 * @method array findByFactureAdresse(string $facture_adresse) Return CommonTPubliciteFavoris objects filtered by the facture_adresse column
 * @method array findByFactureCp(string $facture_cp) Return CommonTPubliciteFavoris objects filtered by the facture_cp column
 * @method array findByFactureVille(string $facture_ville) Return CommonTPubliciteFavoris objects filtered by the facture_ville column
 * @method array findByInstanceRecoursOrganisme(string $instance_recours_organisme) Return CommonTPubliciteFavoris objects filtered by the instance_recours_organisme column
 * @method array findByInstanceRecoursAdresse(string $instance_recours_adresse) Return CommonTPubliciteFavoris objects filtered by the instance_recours_adresse column
 * @method array findByInstanceRecoursCp(string $instance_recours_cp) Return CommonTPubliciteFavoris objects filtered by the instance_recours_cp column
 * @method array findByInstanceRecoursVille(string $instance_recours_ville) Return CommonTPubliciteFavoris objects filtered by the instance_recours_ville column
 * @method array findByInstanceRecoursUrl(string $instance_recours_url) Return CommonTPubliciteFavoris objects filtered by the instance_recours_url column
 * @method array findByAcheteurTelephone(string $acheteur_telephone) Return CommonTPubliciteFavoris objects filtered by the acheteur_telephone column
 * @method array findByAcheteurEmail(string $acheteur_email) Return CommonTPubliciteFavoris objects filtered by the acheteur_email column
 * @method array findByServiceId(string $service_id) Return CommonTPubliciteFavoris objects filtered by the service_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTPubliciteFavorisQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTPubliciteFavorisQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTPubliciteFavoris', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTPubliciteFavorisQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTPubliciteFavorisQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTPubliciteFavorisQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTPubliciteFavorisQuery) {
            return $criteria;
        }
        $query = new CommonTPubliciteFavorisQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTPubliciteFavoris|CommonTPubliciteFavoris[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTPubliciteFavorisPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTPubliciteFavoris A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTPubliciteFavoris A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `organisme`, `old_service_id`, `libelle`, `regime_financier_cautionnement`, `regime_financier_modalites_financement`, `acheteur_correspondant`, `acheteur_nom_organisme`, `acheteur_adresse`, `acheteur_cp`, `acheteur_ville`, `acheteur_url`, `facture_denomination`, `facture_adresse`, `facture_cp`, `facture_ville`, `instance_recours_organisme`, `instance_recours_adresse`, `instance_recours_cp`, `instance_recours_ville`, `instance_recours_url`, `acheteur_telephone`, `acheteur_email`, `service_id` FROM `t_publicite_favoris` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTPubliciteFavoris();
            $obj->hydrate($row);
            CommonTPubliciteFavorisPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTPubliciteFavoris|CommonTPubliciteFavoris[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTPubliciteFavoris[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTPubliciteFavorisPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTPubliciteFavorisPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldServiceId(1234); // WHERE old_service_id = 1234
     * $query->filterByOldServiceId(array(12, 34)); // WHERE old_service_id IN (12, 34)
     * $query->filterByOldServiceId(array('min' => 12)); // WHERE old_service_id >= 12
     * $query->filterByOldServiceId(array('max' => 12)); // WHERE old_service_id <= 12
     * </code>
     *
     * @param     mixed $oldServiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByOldServiceId($oldServiceId = null, $comparison = null)
    {
        if (is_array($oldServiceId)) {
            $useMinMax = false;
            if (isset($oldServiceId['min'])) {
                $this->addUsingAlias(CommonTPubliciteFavorisPeer::OLD_SERVICE_ID, $oldServiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldServiceId['max'])) {
                $this->addUsingAlias(CommonTPubliciteFavorisPeer::OLD_SERVICE_ID, $oldServiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::OLD_SERVICE_ID, $oldServiceId, $comparison);
    }

    /**
     * Filter the query on the libelle column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelle('fooValue');   // WHERE libelle = 'fooValue'
     * $query->filterByLibelle('%fooValue%'); // WHERE libelle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByLibelle($libelle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelle)) {
                $libelle = str_replace('*', '%', $libelle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::LIBELLE, $libelle, $comparison);
    }

    /**
     * Filter the query on the regime_financier_cautionnement column
     *
     * Example usage:
     * <code>
     * $query->filterByRegimeFinancierCautionnement('fooValue');   // WHERE regime_financier_cautionnement = 'fooValue'
     * $query->filterByRegimeFinancierCautionnement('%fooValue%'); // WHERE regime_financier_cautionnement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $regimeFinancierCautionnement The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByRegimeFinancierCautionnement($regimeFinancierCautionnement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($regimeFinancierCautionnement)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $regimeFinancierCautionnement)) {
                $regimeFinancierCautionnement = str_replace('*', '%', $regimeFinancierCautionnement);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_CAUTIONNEMENT, $regimeFinancierCautionnement, $comparison);
    }

    /**
     * Filter the query on the regime_financier_modalites_financement column
     *
     * Example usage:
     * <code>
     * $query->filterByRegimeFinancierModalitesFinancement('fooValue');   // WHERE regime_financier_modalites_financement = 'fooValue'
     * $query->filterByRegimeFinancierModalitesFinancement('%fooValue%'); // WHERE regime_financier_modalites_financement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $regimeFinancierModalitesFinancement The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByRegimeFinancierModalitesFinancement($regimeFinancierModalitesFinancement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($regimeFinancierModalitesFinancement)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $regimeFinancierModalitesFinancement)) {
                $regimeFinancierModalitesFinancement = str_replace('*', '%', $regimeFinancierModalitesFinancement);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_MODALITES_FINANCEMENT, $regimeFinancierModalitesFinancement, $comparison);
    }

    /**
     * Filter the query on the acheteur_correspondant column
     *
     * Example usage:
     * <code>
     * $query->filterByAcheteurCorrespondant('fooValue');   // WHERE acheteur_correspondant = 'fooValue'
     * $query->filterByAcheteurCorrespondant('%fooValue%'); // WHERE acheteur_correspondant LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acheteurCorrespondant The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByAcheteurCorrespondant($acheteurCorrespondant = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acheteurCorrespondant)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acheteurCorrespondant)) {
                $acheteurCorrespondant = str_replace('*', '%', $acheteurCorrespondant);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ACHETEUR_CORRESPONDANT, $acheteurCorrespondant, $comparison);
    }

    /**
     * Filter the query on the acheteur_nom_organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByAcheteurNomOrganisme('fooValue');   // WHERE acheteur_nom_organisme = 'fooValue'
     * $query->filterByAcheteurNomOrganisme('%fooValue%'); // WHERE acheteur_nom_organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acheteurNomOrganisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByAcheteurNomOrganisme($acheteurNomOrganisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acheteurNomOrganisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acheteurNomOrganisme)) {
                $acheteurNomOrganisme = str_replace('*', '%', $acheteurNomOrganisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ACHETEUR_NOM_ORGANISME, $acheteurNomOrganisme, $comparison);
    }

    /**
     * Filter the query on the acheteur_adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByAcheteurAdresse('fooValue');   // WHERE acheteur_adresse = 'fooValue'
     * $query->filterByAcheteurAdresse('%fooValue%'); // WHERE acheteur_adresse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acheteurAdresse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByAcheteurAdresse($acheteurAdresse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acheteurAdresse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acheteurAdresse)) {
                $acheteurAdresse = str_replace('*', '%', $acheteurAdresse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ACHETEUR_ADRESSE, $acheteurAdresse, $comparison);
    }

    /**
     * Filter the query on the acheteur_cp column
     *
     * Example usage:
     * <code>
     * $query->filterByAcheteurCp('fooValue');   // WHERE acheteur_cp = 'fooValue'
     * $query->filterByAcheteurCp('%fooValue%'); // WHERE acheteur_cp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acheteurCp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByAcheteurCp($acheteurCp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acheteurCp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acheteurCp)) {
                $acheteurCp = str_replace('*', '%', $acheteurCp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ACHETEUR_CP, $acheteurCp, $comparison);
    }

    /**
     * Filter the query on the acheteur_ville column
     *
     * Example usage:
     * <code>
     * $query->filterByAcheteurVille('fooValue');   // WHERE acheteur_ville = 'fooValue'
     * $query->filterByAcheteurVille('%fooValue%'); // WHERE acheteur_ville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acheteurVille The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByAcheteurVille($acheteurVille = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acheteurVille)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acheteurVille)) {
                $acheteurVille = str_replace('*', '%', $acheteurVille);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ACHETEUR_VILLE, $acheteurVille, $comparison);
    }

    /**
     * Filter the query on the acheteur_url column
     *
     * Example usage:
     * <code>
     * $query->filterByAcheteurUrl('fooValue');   // WHERE acheteur_url = 'fooValue'
     * $query->filterByAcheteurUrl('%fooValue%'); // WHERE acheteur_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acheteurUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByAcheteurUrl($acheteurUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acheteurUrl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acheteurUrl)) {
                $acheteurUrl = str_replace('*', '%', $acheteurUrl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ACHETEUR_URL, $acheteurUrl, $comparison);
    }

    /**
     * Filter the query on the facture_denomination column
     *
     * Example usage:
     * <code>
     * $query->filterByFactureDenomination('fooValue');   // WHERE facture_denomination = 'fooValue'
     * $query->filterByFactureDenomination('%fooValue%'); // WHERE facture_denomination LIKE '%fooValue%'
     * </code>
     *
     * @param     string $factureDenomination The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByFactureDenomination($factureDenomination = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($factureDenomination)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $factureDenomination)) {
                $factureDenomination = str_replace('*', '%', $factureDenomination);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::FACTURE_DENOMINATION, $factureDenomination, $comparison);
    }

    /**
     * Filter the query on the facture_adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByFactureAdresse('fooValue');   // WHERE facture_adresse = 'fooValue'
     * $query->filterByFactureAdresse('%fooValue%'); // WHERE facture_adresse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $factureAdresse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByFactureAdresse($factureAdresse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($factureAdresse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $factureAdresse)) {
                $factureAdresse = str_replace('*', '%', $factureAdresse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::FACTURE_ADRESSE, $factureAdresse, $comparison);
    }

    /**
     * Filter the query on the facture_cp column
     *
     * Example usage:
     * <code>
     * $query->filterByFactureCp('fooValue');   // WHERE facture_cp = 'fooValue'
     * $query->filterByFactureCp('%fooValue%'); // WHERE facture_cp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $factureCp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByFactureCp($factureCp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($factureCp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $factureCp)) {
                $factureCp = str_replace('*', '%', $factureCp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::FACTURE_CP, $factureCp, $comparison);
    }

    /**
     * Filter the query on the facture_ville column
     *
     * Example usage:
     * <code>
     * $query->filterByFactureVille('fooValue');   // WHERE facture_ville = 'fooValue'
     * $query->filterByFactureVille('%fooValue%'); // WHERE facture_ville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $factureVille The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByFactureVille($factureVille = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($factureVille)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $factureVille)) {
                $factureVille = str_replace('*', '%', $factureVille);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::FACTURE_VILLE, $factureVille, $comparison);
    }

    /**
     * Filter the query on the instance_recours_organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByInstanceRecoursOrganisme('fooValue');   // WHERE instance_recours_organisme = 'fooValue'
     * $query->filterByInstanceRecoursOrganisme('%fooValue%'); // WHERE instance_recours_organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $instanceRecoursOrganisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByInstanceRecoursOrganisme($instanceRecoursOrganisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($instanceRecoursOrganisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $instanceRecoursOrganisme)) {
                $instanceRecoursOrganisme = str_replace('*', '%', $instanceRecoursOrganisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ORGANISME, $instanceRecoursOrganisme, $comparison);
    }

    /**
     * Filter the query on the instance_recours_adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByInstanceRecoursAdresse('fooValue');   // WHERE instance_recours_adresse = 'fooValue'
     * $query->filterByInstanceRecoursAdresse('%fooValue%'); // WHERE instance_recours_adresse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $instanceRecoursAdresse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByInstanceRecoursAdresse($instanceRecoursAdresse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($instanceRecoursAdresse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $instanceRecoursAdresse)) {
                $instanceRecoursAdresse = str_replace('*', '%', $instanceRecoursAdresse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ADRESSE, $instanceRecoursAdresse, $comparison);
    }

    /**
     * Filter the query on the instance_recours_cp column
     *
     * Example usage:
     * <code>
     * $query->filterByInstanceRecoursCp('fooValue');   // WHERE instance_recours_cp = 'fooValue'
     * $query->filterByInstanceRecoursCp('%fooValue%'); // WHERE instance_recours_cp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $instanceRecoursCp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByInstanceRecoursCp($instanceRecoursCp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($instanceRecoursCp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $instanceRecoursCp)) {
                $instanceRecoursCp = str_replace('*', '%', $instanceRecoursCp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_CP, $instanceRecoursCp, $comparison);
    }

    /**
     * Filter the query on the instance_recours_ville column
     *
     * Example usage:
     * <code>
     * $query->filterByInstanceRecoursVille('fooValue');   // WHERE instance_recours_ville = 'fooValue'
     * $query->filterByInstanceRecoursVille('%fooValue%'); // WHERE instance_recours_ville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $instanceRecoursVille The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByInstanceRecoursVille($instanceRecoursVille = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($instanceRecoursVille)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $instanceRecoursVille)) {
                $instanceRecoursVille = str_replace('*', '%', $instanceRecoursVille);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_VILLE, $instanceRecoursVille, $comparison);
    }

    /**
     * Filter the query on the instance_recours_url column
     *
     * Example usage:
     * <code>
     * $query->filterByInstanceRecoursUrl('fooValue');   // WHERE instance_recours_url = 'fooValue'
     * $query->filterByInstanceRecoursUrl('%fooValue%'); // WHERE instance_recours_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $instanceRecoursUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByInstanceRecoursUrl($instanceRecoursUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($instanceRecoursUrl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $instanceRecoursUrl)) {
                $instanceRecoursUrl = str_replace('*', '%', $instanceRecoursUrl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_URL, $instanceRecoursUrl, $comparison);
    }

    /**
     * Filter the query on the acheteur_telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByAcheteurTelephone('fooValue');   // WHERE acheteur_telephone = 'fooValue'
     * $query->filterByAcheteurTelephone('%fooValue%'); // WHERE acheteur_telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acheteurTelephone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByAcheteurTelephone($acheteurTelephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acheteurTelephone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acheteurTelephone)) {
                $acheteurTelephone = str_replace('*', '%', $acheteurTelephone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ACHETEUR_TELEPHONE, $acheteurTelephone, $comparison);
    }

    /**
     * Filter the query on the acheteur_email column
     *
     * Example usage:
     * <code>
     * $query->filterByAcheteurEmail('fooValue');   // WHERE acheteur_email = 'fooValue'
     * $query->filterByAcheteurEmail('%fooValue%'); // WHERE acheteur_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acheteurEmail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByAcheteurEmail($acheteurEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acheteurEmail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acheteurEmail)) {
                $acheteurEmail = str_replace('*', '%', $acheteurEmail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::ACHETEUR_EMAIL, $acheteurEmail, $comparison);
    }

    /**
     * Filter the query on the service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceId(1234); // WHERE service_id = 1234
     * $query->filterByServiceId(array(12, 34)); // WHERE service_id IN (12, 34)
     * $query->filterByServiceId(array('min' => 12)); // WHERE service_id >= 12
     * $query->filterByServiceId(array('max' => 12)); // WHERE service_id <= 12
     * </code>
     *
     * @param     mixed $serviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function filterByServiceId($serviceId = null, $comparison = null)
    {
        if (is_array($serviceId)) {
            $useMinMax = false;
            if (isset($serviceId['min'])) {
                $this->addUsingAlias(CommonTPubliciteFavorisPeer::SERVICE_ID, $serviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceId['max'])) {
                $this->addUsingAlias(CommonTPubliciteFavorisPeer::SERVICE_ID, $serviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTPubliciteFavorisPeer::SERVICE_ID, $serviceId, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTPubliciteFavoris $commonTPubliciteFavoris Object to remove from the list of results
     *
     * @return CommonTPubliciteFavorisQuery The current query, for fluid interface
     */
    public function prune($commonTPubliciteFavoris = null)
    {
        if ($commonTPubliciteFavoris) {
            $this->addUsingAlias(CommonTPubliciteFavorisPeer::ID, $commonTPubliciteFavoris->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
