<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientPeer;
use Application\Propel\Mpe\CommonEchangeDocPeer;
use Application\Propel\Mpe\CommonReferentielSousTypeParapheurPeer;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Propel\Mpe\Map\CommonEchangeDocTableMap;

/**
 * Base static class for performing query and update operations on the 'echange_doc' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'echange_doc';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonEchangeDoc';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonEchangeDocTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 15;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 15;

    /** the column name for the id field */
    const ID = 'echange_doc.id';

    /** the column name for the echange_doc_application_client_id field */
    const ECHANGE_DOC_APPLICATION_CLIENT_ID = 'echange_doc.echange_doc_application_client_id';

    /** the column name for the objet field */
    const OBJET = 'echange_doc.objet';

    /** the column name for the description field */
    const DESCRIPTION = 'echange_doc.description';

    /** the column name for the consultation_id field */
    const CONSULTATION_ID = 'echange_doc.consultation_id';

    /** the column name for the agent_id field */
    const AGENT_ID = 'echange_doc.agent_id';

    /** the column name for the statut field */
    const STATUT = 'echange_doc.statut';

    /** the column name for the created_at field */
    const CREATED_AT = 'echange_doc.created_at';

    /** the column name for the updated_at field */
    const UPDATED_AT = 'echange_doc.updated_at';

    /** the column name for the cheminement_signature field */
    const CHEMINEMENT_SIGNATURE = 'echange_doc.cheminement_signature';

    /** the column name for the cheminement_ged field */
    const CHEMINEMENT_GED = 'echange_doc.cheminement_ged';

    /** the column name for the cheminement_sae field */
    const CHEMINEMENT_SAE = 'echange_doc.cheminement_sae';

    /** the column name for the cheminement_tdt field */
    const CHEMINEMENT_TDT = 'echange_doc.cheminement_tdt';

    /** the column name for the id_contrat_titulaire field */
    const ID_CONTRAT_TITULAIRE = 'echange_doc.id_contrat_titulaire';

    /** the column name for the referentiel_sous_type_parapheur_id field */
    const REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID = 'echange_doc.referentiel_sous_type_parapheur_id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonEchangeDoc objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonEchangeDoc[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonEchangeDocPeer::$fieldNames[CommonEchangeDocPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'EchangeDocApplicationClientId', 'Objet', 'Description', 'ConsultationId', 'AgentId', 'Statut', 'CreatedAt', 'UpdatedAt', 'CheminementSignature', 'CheminementGed', 'CheminementSae', 'CheminementTdt', 'IdContratTitulaire', 'ReferentielSousTypeParapheurId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'echangeDocApplicationClientId', 'objet', 'description', 'consultationId', 'agentId', 'statut', 'createdAt', 'updatedAt', 'cheminementSignature', 'cheminementGed', 'cheminementSae', 'cheminementTdt', 'idContratTitulaire', 'referentielSousTypeParapheurId', ),
        BasePeer::TYPE_COLNAME => array (CommonEchangeDocPeer::ID, CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocPeer::OBJET, CommonEchangeDocPeer::DESCRIPTION, CommonEchangeDocPeer::CONSULTATION_ID, CommonEchangeDocPeer::AGENT_ID, CommonEchangeDocPeer::STATUT, CommonEchangeDocPeer::CREATED_AT, CommonEchangeDocPeer::UPDATED_AT, CommonEchangeDocPeer::CHEMINEMENT_SIGNATURE, CommonEchangeDocPeer::CHEMINEMENT_GED, CommonEchangeDocPeer::CHEMINEMENT_SAE, CommonEchangeDocPeer::CHEMINEMENT_TDT, CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ECHANGE_DOC_APPLICATION_CLIENT_ID', 'OBJET', 'DESCRIPTION', 'CONSULTATION_ID', 'AGENT_ID', 'STATUT', 'CREATED_AT', 'UPDATED_AT', 'CHEMINEMENT_SIGNATURE', 'CHEMINEMENT_GED', 'CHEMINEMENT_SAE', 'CHEMINEMENT_TDT', 'ID_CONTRAT_TITULAIRE', 'REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'echange_doc_application_client_id', 'objet', 'description', 'consultation_id', 'agent_id', 'statut', 'created_at', 'updated_at', 'cheminement_signature', 'cheminement_ged', 'cheminement_sae', 'cheminement_tdt', 'id_contrat_titulaire', 'referentiel_sous_type_parapheur_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonEchangeDocPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'EchangeDocApplicationClientId' => 1, 'Objet' => 2, 'Description' => 3, 'ConsultationId' => 4, 'AgentId' => 5, 'Statut' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, 'CheminementSignature' => 9, 'CheminementGed' => 10, 'CheminementSae' => 11, 'CheminementTdt' => 12, 'IdContratTitulaire' => 13, 'ReferentielSousTypeParapheurId' => 14, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'echangeDocApplicationClientId' => 1, 'objet' => 2, 'description' => 3, 'consultationId' => 4, 'agentId' => 5, 'statut' => 6, 'createdAt' => 7, 'updatedAt' => 8, 'cheminementSignature' => 9, 'cheminementGed' => 10, 'cheminementSae' => 11, 'cheminementTdt' => 12, 'idContratTitulaire' => 13, 'referentielSousTypeParapheurId' => 14, ),
        BasePeer::TYPE_COLNAME => array (CommonEchangeDocPeer::ID => 0, CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID => 1, CommonEchangeDocPeer::OBJET => 2, CommonEchangeDocPeer::DESCRIPTION => 3, CommonEchangeDocPeer::CONSULTATION_ID => 4, CommonEchangeDocPeer::AGENT_ID => 5, CommonEchangeDocPeer::STATUT => 6, CommonEchangeDocPeer::CREATED_AT => 7, CommonEchangeDocPeer::UPDATED_AT => 8, CommonEchangeDocPeer::CHEMINEMENT_SIGNATURE => 9, CommonEchangeDocPeer::CHEMINEMENT_GED => 10, CommonEchangeDocPeer::CHEMINEMENT_SAE => 11, CommonEchangeDocPeer::CHEMINEMENT_TDT => 12, CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE => 13, CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID => 14, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ECHANGE_DOC_APPLICATION_CLIENT_ID' => 1, 'OBJET' => 2, 'DESCRIPTION' => 3, 'CONSULTATION_ID' => 4, 'AGENT_ID' => 5, 'STATUT' => 6, 'CREATED_AT' => 7, 'UPDATED_AT' => 8, 'CHEMINEMENT_SIGNATURE' => 9, 'CHEMINEMENT_GED' => 10, 'CHEMINEMENT_SAE' => 11, 'CHEMINEMENT_TDT' => 12, 'ID_CONTRAT_TITULAIRE' => 13, 'REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID' => 14, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'echange_doc_application_client_id' => 1, 'objet' => 2, 'description' => 3, 'consultation_id' => 4, 'agent_id' => 5, 'statut' => 6, 'created_at' => 7, 'updated_at' => 8, 'cheminement_signature' => 9, 'cheminement_ged' => 10, 'cheminement_sae' => 11, 'cheminement_tdt' => 12, 'id_contrat_titulaire' => 13, 'referentiel_sous_type_parapheur_id' => 14, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonEchangeDocPeer::getFieldNames($toType);
        $key = isset(CommonEchangeDocPeer::$fieldKeys[$fromType][$name]) ? CommonEchangeDocPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonEchangeDocPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonEchangeDocPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonEchangeDocPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonEchangeDocPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonEchangeDocPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonEchangeDocPeer::ID);
            $criteria->addSelectColumn(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID);
            $criteria->addSelectColumn(CommonEchangeDocPeer::OBJET);
            $criteria->addSelectColumn(CommonEchangeDocPeer::DESCRIPTION);
            $criteria->addSelectColumn(CommonEchangeDocPeer::CONSULTATION_ID);
            $criteria->addSelectColumn(CommonEchangeDocPeer::AGENT_ID);
            $criteria->addSelectColumn(CommonEchangeDocPeer::STATUT);
            $criteria->addSelectColumn(CommonEchangeDocPeer::CREATED_AT);
            $criteria->addSelectColumn(CommonEchangeDocPeer::UPDATED_AT);
            $criteria->addSelectColumn(CommonEchangeDocPeer::CHEMINEMENT_SIGNATURE);
            $criteria->addSelectColumn(CommonEchangeDocPeer::CHEMINEMENT_GED);
            $criteria->addSelectColumn(CommonEchangeDocPeer::CHEMINEMENT_SAE);
            $criteria->addSelectColumn(CommonEchangeDocPeer::CHEMINEMENT_TDT);
            $criteria->addSelectColumn(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE);
            $criteria->addSelectColumn(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.echange_doc_application_client_id');
            $criteria->addSelectColumn($alias . '.objet');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.consultation_id');
            $criteria->addSelectColumn($alias . '.agent_id');
            $criteria->addSelectColumn($alias . '.statut');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.cheminement_signature');
            $criteria->addSelectColumn($alias . '.cheminement_ged');
            $criteria->addSelectColumn($alias . '.cheminement_sae');
            $criteria->addSelectColumn($alias . '.cheminement_tdt');
            $criteria->addSelectColumn($alias . '.id_contrat_titulaire');
            $criteria->addSelectColumn($alias . '.referentiel_sous_type_parapheur_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonEchangeDoc
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonEchangeDocPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonEchangeDocPeer::populateObjects(CommonEchangeDocPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonEchangeDoc $obj A CommonEchangeDoc object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonEchangeDocPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonEchangeDoc object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonEchangeDoc) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonEchangeDoc object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonEchangeDocPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonEchangeDoc Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonEchangeDocPeer::$instances[$key])) {
                return CommonEchangeDocPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonEchangeDocPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonEchangeDocPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to echange_doc
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonEchangeDocPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonEchangeDocPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonEchangeDocPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonEchangeDoc object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonEchangeDocPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonEchangeDocPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonEchangeDocPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonReferentielSousTypeParapheur table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonReferentielSousTypeParapheur(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTContratTitulaire table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTContratTitulaire(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonAgent table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonAgent(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonEchangeDocApplicationClient table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonEchangeDocApplicationClient(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with their CommonReferentielSousTypeParapheur objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonReferentielSousTypeParapheur(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;
        CommonReferentielSousTypeParapheurPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonReferentielSousTypeParapheurPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonReferentielSousTypeParapheurPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonReferentielSousTypeParapheurPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonReferentielSousTypeParapheurPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to $obj2 (CommonReferentielSousTypeParapheur)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with their CommonTContratTitulaire objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTContratTitulaire(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;
        CommonTContratTitulairePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTContratTitulairePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol);
                    $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTContratTitulairePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to $obj2 (CommonTContratTitulaire)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with their CommonAgent objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonAgent(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;
        CommonAgentPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonAgentPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonAgentPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonAgentPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to $obj2 (CommonAgent)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with their CommonEchangeDocApplicationClient objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonEchangeDocApplicationClient(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;
        CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonEchangeDocApplicationClientPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to $obj2 (CommonEchangeDocApplicationClient)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with their CommonConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;
        CommonConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to $obj2 (CommonConsultation)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielSousTypeParapheurPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielSousTypeParapheurPeer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonEchangeDocApplicationClientPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonReferentielSousTypeParapheur rows

            $key2 = CommonReferentielSousTypeParapheurPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonReferentielSousTypeParapheurPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonReferentielSousTypeParapheurPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielSousTypeParapheurPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj2 (CommonReferentielSousTypeParapheur)
                $obj2->addCommonEchangeDoc($obj1);
            } // if joined row not null

            // Add objects for joined CommonTContratTitulaire rows

            $key3 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonTContratTitulairePeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol3);
          $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTContratTitulairePeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj3 (CommonTContratTitulaire)
                $obj3->addCommonEchangeDoc($obj1);
            } // if joined row not null

            // Add objects for joined CommonAgent rows

            $key4 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonAgentPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonAgentPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonAgentPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj4 (CommonAgent)
                $obj4->addCommonEchangeDoc($obj1);
            } // if joined row not null

            // Add objects for joined CommonEchangeDocApplicationClient rows

            $key5 = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = CommonEchangeDocApplicationClientPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj5 (CommonEchangeDocApplicationClient)
                $obj5->addCommonEchangeDoc($obj1);
            } // if joined row not null

            // Add objects for joined CommonConsultation rows

            $key6 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = CommonConsultationPeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonConsultationPeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj6 (CommonConsultation)
                $obj6->addCommonEchangeDoc($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonReferentielSousTypeParapheur table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonReferentielSousTypeParapheur(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTContratTitulaire table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTContratTitulaire(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonAgent table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonAgent(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonEchangeDocApplicationClient table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonEchangeDocApplicationClient(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with all related objects except CommonReferentielSousTypeParapheur.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonReferentielSousTypeParapheur(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonEchangeDocApplicationClientPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTContratTitulaire rows

                $key2 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTContratTitulairePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol2);
            $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTContratTitulairePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj2 (CommonTContratTitulaire)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonEchangeDocApplicationClient rows

                $key4 = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonEchangeDocApplicationClientPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj4 (CommonEchangeDocApplicationClient)
                $obj4->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key5 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonConsultationPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonConsultationPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj5 (CommonConsultation)
                $obj5->addCommonEchangeDoc($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with all related objects except CommonTContratTitulaire.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTContratTitulaire(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielSousTypeParapheurPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielSousTypeParapheurPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonEchangeDocApplicationClientPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielSousTypeParapheur rows

                $key2 = CommonReferentielSousTypeParapheurPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielSousTypeParapheurPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielSousTypeParapheurPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielSousTypeParapheurPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj2 (CommonReferentielSousTypeParapheur)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonEchangeDocApplicationClient rows

                $key4 = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonEchangeDocApplicationClientPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj4 (CommonEchangeDocApplicationClient)
                $obj4->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key5 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonConsultationPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonConsultationPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj5 (CommonConsultation)
                $obj5->addCommonEchangeDoc($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with all related objects except CommonAgent.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonAgent(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielSousTypeParapheurPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielSousTypeParapheurPeer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonEchangeDocApplicationClientPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielSousTypeParapheur rows

                $key2 = CommonReferentielSousTypeParapheurPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielSousTypeParapheurPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielSousTypeParapheurPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielSousTypeParapheurPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj2 (CommonReferentielSousTypeParapheur)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContratTitulaire rows

                $key3 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTContratTitulairePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol3);
            $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTContratTitulairePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj3 (CommonTContratTitulaire)
                $obj3->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonEchangeDocApplicationClient rows

                $key4 = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonEchangeDocApplicationClientPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj4 (CommonEchangeDocApplicationClient)
                $obj4->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key5 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonConsultationPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonConsultationPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj5 (CommonConsultation)
                $obj5->addCommonEchangeDoc($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with all related objects except CommonEchangeDocApplicationClient.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonEchangeDocApplicationClient(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielSousTypeParapheurPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielSousTypeParapheurPeer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielSousTypeParapheur rows

                $key2 = CommonReferentielSousTypeParapheurPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielSousTypeParapheurPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielSousTypeParapheurPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielSousTypeParapheurPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj2 (CommonReferentielSousTypeParapheur)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContratTitulaire rows

                $key3 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTContratTitulairePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol3);
            $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTContratTitulairePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj3 (CommonTContratTitulaire)
                $obj3->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key4 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonAgentPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonAgentPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj4 (CommonAgent)
                $obj4->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key5 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonConsultationPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonConsultationPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj5 (CommonConsultation)
                $obj5->addCommonEchangeDoc($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangeDoc objects pre-filled with all related objects except CommonConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDoc objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);
        }

        CommonEchangeDocPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielSousTypeParapheurPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielSousTypeParapheurPeer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonEchangeDocApplicationClientPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, CommonReferentielSousTypeParapheurPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::AGENT_ID, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, CommonEchangeDocApplicationClientPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielSousTypeParapheur rows

                $key2 = CommonReferentielSousTypeParapheurPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielSousTypeParapheurPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielSousTypeParapheurPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielSousTypeParapheurPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj2 (CommonReferentielSousTypeParapheur)
                $obj2->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContratTitulaire rows

                $key3 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTContratTitulairePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol3);
            $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTContratTitulairePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj3 (CommonTContratTitulaire)
                $obj3->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key4 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonAgentPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonAgentPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj4 (CommonAgent)
                $obj4->addCommonEchangeDoc($obj1);

            } // if joined row is not null

                // Add objects for joined CommonEchangeDocApplicationClient rows

                $key5 = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonEchangeDocApplicationClientPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonEchangeDoc) to the collection in $obj5 (CommonEchangeDocApplicationClient)
                $obj5->addCommonEchangeDoc($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonEchangeDocPeer::DATABASE_NAME)->getTable(CommonEchangeDocPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonEchangeDocPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonEchangeDocPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonEchangeDocTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonEchangeDocPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonEchangeDoc or Criteria object.
     *
     * @param      mixed $values Criteria or CommonEchangeDoc object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonEchangeDoc object
        }

        if ($criteria->containsKey(CommonEchangeDocPeer::ID) && $criteria->keyContainsValue(CommonEchangeDocPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonEchangeDocPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonEchangeDoc or Criteria object.
     *
     * @param      mixed $values Criteria or CommonEchangeDoc object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonEchangeDocPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonEchangeDocPeer::ID);
            $value = $criteria->remove(CommonEchangeDocPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonEchangeDocPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonEchangeDocPeer::TABLE_NAME);
            }

        } else { // $values is CommonEchangeDoc object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the echange_doc table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonEchangeDocPeer::TABLE_NAME, $con, CommonEchangeDocPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonEchangeDocPeer::clearInstancePool();
            CommonEchangeDocPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonEchangeDoc or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonEchangeDoc object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonEchangeDocPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonEchangeDoc) { // it's a model object
            // invalidate the cache for this single object
            CommonEchangeDocPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonEchangeDocPeer::DATABASE_NAME);
            $criteria->add(CommonEchangeDocPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonEchangeDocPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonEchangeDocPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonEchangeDoc object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonEchangeDoc $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonEchangeDocPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonEchangeDocPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonEchangeDocPeer::DATABASE_NAME, CommonEchangeDocPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonEchangeDoc
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonEchangeDocPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonEchangeDocPeer::DATABASE_NAME);
        $criteria->add(CommonEchangeDocPeer::ID, $pk);

        $v = CommonEchangeDocPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonEchangeDoc[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonEchangeDocPeer::DATABASE_NAME);
            $criteria->add(CommonEchangeDocPeer::ID, $pks, Criteria::IN);
            $objs = CommonEchangeDocPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonEchangeDocPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonEchangeDocPeer::buildTableMap();

