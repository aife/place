<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonBlobOrganismeFile;
use Application\Propel\Mpe\CommonBlobOrganismeFileQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonPlateformeVirtuelleQuery;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidatureQuery;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTGroupementEntrepriseQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntrepriseQuery;

/**
 * Base class that represents a row from the 'Offres' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonOffres extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonOffresPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonOffresPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the organisme field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the consultation_ref field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $consultation_ref;

    /**
     * The value for the entreprise_id field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $entreprise_id;

    /**
     * The value for the id_etablissement field.
     * @var        int
     */
    protected $id_etablissement;

    /**
     * The value for the old_inscrit_id field.
     * @var        int
     */
    protected $old_inscrit_id;

    /**
     * The value for the signatureenvxml field.
     * @var        resource
     */
    protected $signatureenvxml;

    /**
     * The value for the horodatage field.
     * @var        resource
     */
    protected $horodatage;

    /**
     * The value for the mailsignataire field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $mailsignataire;

    /**
     * The value for the untrusteddate field.
     * Note: this column has a database default value of: '0000-00-00 00:00:00'
     * @var        string
     */
    protected $untrusteddate;

    /**
     * The value for the untrustedserial field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $untrustedserial;

    /**
     * The value for the envoi_complet field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $envoi_complet;

    /**
     * The value for the date_depot_differe field.
     * Note: this column has a database default value of: '0000-00-00 00:00:00'
     * @var        string
     */
    protected $date_depot_differe;

    /**
     * The value for the horodatage_envoi_differe field.
     * @var        resource
     */
    protected $horodatage_envoi_differe;

    /**
     * The value for the signatureenvxml_envoi_differe field.
     * @var        resource
     */
    protected $signatureenvxml_envoi_differe;

    /**
     * The value for the external_serial field.
     * @var        string
     */
    protected $external_serial;

    /**
     * The value for the internal_serial field.
     * @var        string
     */
    protected $internal_serial;

    /**
     * The value for the uid_offre field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $uid_offre;

    /**
     * The value for the offre_selectionnee field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $offre_selectionnee;

    /**
     * The value for the observation field.
     * @var        string
     */
    protected $observation;

    /**
     * The value for the xml_string field.
     * @var        string
     */
    protected $xml_string;

    /**
     * The value for the nom_entreprise_inscrit field.
     * @var        string
     */
    protected $nom_entreprise_inscrit;

    /**
     * The value for the nom_inscrit field.
     * @var        string
     */
    protected $nom_inscrit;

    /**
     * The value for the prenom_inscrit field.
     * @var        string
     */
    protected $prenom_inscrit;

    /**
     * The value for the adresse_inscrit field.
     * @var        string
     */
    protected $adresse_inscrit;

    /**
     * The value for the adresse2_inscrit field.
     * @var        string
     */
    protected $adresse2_inscrit;

    /**
     * The value for the telephone_inscrit field.
     * @var        string
     */
    protected $telephone_inscrit;

    /**
     * The value for the fax_inscrit field.
     * @var        string
     */
    protected $fax_inscrit;

    /**
     * The value for the code_postal_inscrit field.
     * @var        string
     */
    protected $code_postal_inscrit;

    /**
     * The value for the ville_inscrit field.
     * @var        string
     */
    protected $ville_inscrit;

    /**
     * The value for the pays_inscrit field.
     * @var        string
     */
    protected $pays_inscrit;

    /**
     * The value for the acronyme_pays field.
     * @var        string
     */
    protected $acronyme_pays;

    /**
     * The value for the siret_entreprise field.
     * @var        string
     */
    protected $siret_entreprise;

    /**
     * The value for the identifiant_national field.
     * @var        string
     */
    protected $identifiant_national;

    /**
     * The value for the email_inscrit field.
     * @var        string
     */
    protected $email_inscrit;

    /**
     * The value for the siret_inscrit field.
     * @var        string
     */
    protected $siret_inscrit;

    /**
     * The value for the nom_entreprise field.
     * @var        string
     */
    protected $nom_entreprise;

    /**
     * The value for the horodatage_annulation field.
     * @var        resource
     */
    protected $horodatage_annulation;

    /**
     * The value for the date_annulation field.
     * @var        string
     */
    protected $date_annulation;

    /**
     * The value for the signature_annulation field.
     * @var        string
     */
    protected $signature_annulation;

    /**
     * The value for the depot_annule field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $depot_annule;

    /**
     * The value for the string_annulation field.
     * @var        string
     */
    protected $string_annulation;

    /**
     * The value for the verification_certificat_annulation field.
     * @var        string
     */
    protected $verification_certificat_annulation;

    /**
     * The value for the offre_variante field.
     * @var        string
     */
    protected $offre_variante;

    /**
     * The value for the reponse_pas_a_pas field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $reponse_pas_a_pas;

    /**
     * The value for the numero_reponse field.
     * @var        int
     */
    protected $numero_reponse;

    /**
     * The value for the statut_offres field.
     * @var        int
     */
    protected $statut_offres;

    /**
     * The value for the date_heure_ouverture field.
     * Note: this column has a database default value of: '0000-00-00 00:00:00'
     * @var        string
     */
    protected $date_heure_ouverture;

    /**
     * The value for the agentid_ouverture field.
     * @var        int
     */
    protected $agentid_ouverture;

    /**
     * The value for the agentid_ouverture2 field.
     * @var        int
     */
    protected $agentid_ouverture2;

    /**
     * The value for the date_heure_ouverture_agent2 field.
     * Note: this column has a database default value of: '0000-00-00 00:00:00'
     * @var        string
     */
    protected $date_heure_ouverture_agent2;

    /**
     * The value for the cryptage_reponse field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $cryptage_reponse;

    /**
     * The value for the nom_agent_ouverture field.
     * @var        string
     */
    protected $nom_agent_ouverture;

    /**
     * The value for the agent_telechargement_offre field.
     * @var        int
     */
    protected $agent_telechargement_offre;

    /**
     * The value for the date_telechargement_offre field.
     * @var        string
     */
    protected $date_telechargement_offre;

    /**
     * The value for the repertoire_telechargement_offre field.
     * @var        string
     */
    protected $repertoire_telechargement_offre;

    /**
     * The value for the candidature_id_externe field.
     * @var        int
     */
    protected $candidature_id_externe;

    /**
     * The value for the etat_chiffrement field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $etat_chiffrement;

    /**
     * The value for the erreur_chiffrement field.
     * @var        string
     */
    protected $erreur_chiffrement;

    /**
     * The value for the date_fin_chiffrement field.
     * @var        string
     */
    protected $date_fin_chiffrement;

    /**
     * The value for the date_horodatage field.
     * @var        string
     */
    protected $date_horodatage;

    /**
     * The value for the verification_hotodatage field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $verification_hotodatage;

    /**
     * The value for the verification_signature_offre field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $verification_signature_offre;

    /**
     * The value for the horodatage_hash_fichiers field.
     * @var        string
     */
    protected $horodatage_hash_fichiers;

    /**
     * The value for the id_pdf_echange_accuse field.
     * @var        int
     */
    protected $id_pdf_echange_accuse;

    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;

    /**
     * The value for the uid_response field.
     * @var        string
     */
    protected $uid_response;

    /**
     * The value for the date_depot field.
     * @var        string
     */
    protected $date_depot;

    /**
     * The value for the resultat_verification_hash_all_files field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $resultat_verification_hash_all_files;

    /**
     * The value for the id_blob_horodatage_hash field.
     * @var        int
     */
    protected $id_blob_horodatage_hash;

    /**
     * The value for the id_blob_xml_reponse field.
     * @var        int
     */
    protected $id_blob_xml_reponse;

    /**
     * The value for the consultation_id field.
     * @var        int
     */
    protected $consultation_id;

    /**
     * The value for the plateforme_virtuelle_id field.
     * @var        int
     */
    protected $plateforme_virtuelle_id;

    /**
     * The value for the inscrit_id field.
     * @var        string
     */
    protected $inscrit_id;

    /**
     * The value for the taux_production_france field.
     * @var        int
     */
    protected $taux_production_france;

    /**
     * The value for the taux_production_europe field.
     * @var        int
     */
    protected $taux_production_europe;

    /**
     * @var        CommonBlobOrganismeFile
     */
    protected $aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash;

    /**
     * @var        CommonConsultation
     */
    protected $aCommonConsultation;

    /**
     * @var        CommonInscrit
     */
    protected $aCommonInscrit;

    /**
     * @var        Entreprise
     */
    protected $aEntreprise;

    /**
     * @var        CommonPlateformeVirtuelle
     */
    protected $aCommonPlateformeVirtuelle;

    /**
     * @var        CommonBlobOrganismeFile
     */
    protected $aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse;

    /**
     * @var        PropelObjectCollection|CommonTCandidature[] Collection to store aggregation of CommonTCandidature objects.
     */
    protected $collCommonTCandidatures;
    protected $collCommonTCandidaturesPartial;

    /**
     * @var        PropelObjectCollection|CommonTGroupementEntreprise[] Collection to store aggregation of CommonTGroupementEntreprise objects.
     */
    protected $collCommonTGroupementEntreprises;
    protected $collCommonTGroupementEntreprisesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCandidaturesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTGroupementEntreprisesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->organisme = '';
        $this->consultation_ref = 0;
        $this->entreprise_id = 0;
        $this->mailsignataire = '';
        $this->untrusteddate = '0000-00-00 00:00:00';
        $this->untrustedserial = '';
        $this->envoi_complet = '';
        $this->date_depot_differe = '0000-00-00 00:00:00';
        $this->uid_offre = '';
        $this->offre_selectionnee = 0;
        $this->depot_annule = '0';
        $this->reponse_pas_a_pas = '0';
        $this->date_heure_ouverture = '0000-00-00 00:00:00';
        $this->date_heure_ouverture_agent2 = '0000-00-00 00:00:00';
        $this->cryptage_reponse = '1';
        $this->etat_chiffrement = 1;
        $this->verification_hotodatage = '0';
        $this->verification_signature_offre = '';
        $this->resultat_verification_hash_all_files = '1';
    }

    /**
     * Initializes internal state of BaseCommonOffres object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [consultation_ref] column value.
     *
     * @return int
     */
    public function getConsultationRef()
    {

        return $this->consultation_ref;
    }

    /**
     * Get the [entreprise_id] column value.
     *
     * @return int
     */
    public function getEntrepriseId()
    {

        return $this->entreprise_id;
    }

    /**
     * Get the [id_etablissement] column value.
     *
     * @return int
     */
    public function getIdEtablissement()
    {

        return $this->id_etablissement;
    }

    /**
     * Get the [old_inscrit_id] column value.
     *
     * @return int
     */
    public function getOldInscritId()
    {

        return $this->old_inscrit_id;
    }

    /**
     * Get the [signatureenvxml] column value.
     *
     * @return resource
     */
    public function getSignatureenvxml()
    {

        return $this->signatureenvxml;
    }

    /**
     * Get the [horodatage] column value.
     *
     * @return resource
     */
    public function getHorodatage()
    {

        return $this->horodatage;
    }

    /**
     * Get the [mailsignataire] column value.
     *
     * @return string
     */
    public function getMailsignataire()
    {

        return $this->mailsignataire;
    }

    /**
     * Get the [untrusteddate] column value.
     *
     * @return string
     */
    public function getUntrusteddate()
    {

        return $this->untrusteddate;
    }

    /**
     * Get the [untrustedserial] column value.
     *
     * @return string
     */
    public function getUntrustedserial()
    {

        return $this->untrustedserial;
    }

    /**
     * Get the [envoi_complet] column value.
     *
     * @return string
     */
    public function getEnvoiComplet()
    {

        return $this->envoi_complet;
    }

    /**
     * Get the [date_depot_differe] column value.
     *
     * @return string
     */
    public function getDateDepotDiffere()
    {

        return $this->date_depot_differe;
    }

    /**
     * Get the [horodatage_envoi_differe] column value.
     *
     * @return resource
     */
    public function getHorodatageEnvoiDiffere()
    {

        return $this->horodatage_envoi_differe;
    }

    /**
     * Get the [signatureenvxml_envoi_differe] column value.
     *
     * @return resource
     */
    public function getSignatureenvxmlEnvoiDiffere()
    {

        return $this->signatureenvxml_envoi_differe;
    }

    /**
     * Get the [external_serial] column value.
     *
     * @return string
     */
    public function getExternalSerial()
    {

        return $this->external_serial;
    }

    /**
     * Get the [internal_serial] column value.
     *
     * @return string
     */
    public function getInternalSerial()
    {

        return $this->internal_serial;
    }

    /**
     * Get the [uid_offre] column value.
     *
     * @return string
     */
    public function getUidOffre()
    {

        return $this->uid_offre;
    }

    /**
     * Get the [offre_selectionnee] column value.
     *
     * @return int
     */
    public function getOffreSelectionnee()
    {

        return $this->offre_selectionnee;
    }

    /**
     * Get the [observation] column value.
     *
     * @return string
     */
    public function getObservation()
    {

        return $this->observation;
    }

    /**
     * Get the [xml_string] column value.
     *
     * @return string
     */
    public function getXmlString()
    {

        return $this->xml_string;
    }

    /**
     * Get the [nom_entreprise_inscrit] column value.
     *
     * @return string
     */
    public function getNomEntrepriseInscrit()
    {

        return $this->nom_entreprise_inscrit;
    }

    /**
     * Get the [nom_inscrit] column value.
     *
     * @return string
     */
    public function getNomInscrit()
    {

        return $this->nom_inscrit;
    }

    /**
     * Get the [prenom_inscrit] column value.
     *
     * @return string
     */
    public function getPrenomInscrit()
    {

        return $this->prenom_inscrit;
    }

    /**
     * Get the [adresse_inscrit] column value.
     *
     * @return string
     */
    public function getAdresseInscrit()
    {

        return $this->adresse_inscrit;
    }

    /**
     * Get the [adresse2_inscrit] column value.
     *
     * @return string
     */
    public function getAdresse2Inscrit()
    {

        return $this->adresse2_inscrit;
    }

    /**
     * Get the [telephone_inscrit] column value.
     *
     * @return string
     */
    public function getTelephoneInscrit()
    {

        return $this->telephone_inscrit;
    }

    /**
     * Get the [fax_inscrit] column value.
     *
     * @return string
     */
    public function getFaxInscrit()
    {

        return $this->fax_inscrit;
    }

    /**
     * Get the [code_postal_inscrit] column value.
     *
     * @return string
     */
    public function getCodePostalInscrit()
    {

        return $this->code_postal_inscrit;
    }

    /**
     * Get the [ville_inscrit] column value.
     *
     * @return string
     */
    public function getVilleInscrit()
    {

        return $this->ville_inscrit;
    }

    /**
     * Get the [pays_inscrit] column value.
     *
     * @return string
     */
    public function getPaysInscrit()
    {

        return $this->pays_inscrit;
    }

    /**
     * Get the [acronyme_pays] column value.
     *
     * @return string
     */
    public function getAcronymePays()
    {

        return $this->acronyme_pays;
    }

    /**
     * Get the [siret_entreprise] column value.
     *
     * @return string
     */
    public function getSiretEntreprise()
    {

        return $this->siret_entreprise;
    }

    /**
     * Get the [identifiant_national] column value.
     *
     * @return string
     */
    public function getIdentifiantNational()
    {

        return $this->identifiant_national;
    }

    /**
     * Get the [email_inscrit] column value.
     *
     * @return string
     */
    public function getEmailInscrit()
    {

        return $this->email_inscrit;
    }

    /**
     * Get the [siret_inscrit] column value.
     *
     * @return string
     */
    public function getSiretInscrit()
    {

        return $this->siret_inscrit;
    }

    /**
     * Get the [nom_entreprise] column value.
     *
     * @return string
     */
    public function getNomEntreprise()
    {

        return $this->nom_entreprise;
    }

    /**
     * Get the [horodatage_annulation] column value.
     *
     * @return resource
     */
    public function getHorodatageAnnulation()
    {

        return $this->horodatage_annulation;
    }

    /**
     * Get the [date_annulation] column value.
     *
     * @return string
     */
    public function getDateAnnulation()
    {

        return $this->date_annulation;
    }

    /**
     * Get the [signature_annulation] column value.
     *
     * @return string
     */
    public function getSignatureAnnulation()
    {

        return $this->signature_annulation;
    }

    /**
     * Get the [depot_annule] column value.
     *
     * @return string
     */
    public function getDepotAnnule()
    {

        return $this->depot_annule;
    }

    /**
     * Get the [string_annulation] column value.
     *
     * @return string
     */
    public function getStringAnnulation()
    {

        return $this->string_annulation;
    }

    /**
     * Get the [verification_certificat_annulation] column value.
     *
     * @return string
     */
    public function getVerificationCertificatAnnulation()
    {

        return $this->verification_certificat_annulation;
    }

    /**
     * Get the [offre_variante] column value.
     *
     * @return string
     */
    public function getOffreVariante()
    {

        return $this->offre_variante;
    }

    /**
     * Get the [reponse_pas_a_pas] column value.
     *
     * @return string
     */
    public function getReponsePasAPas()
    {

        return $this->reponse_pas_a_pas;
    }

    /**
     * Get the [numero_reponse] column value.
     *
     * @return int
     */
    public function getNumeroReponse()
    {

        return $this->numero_reponse;
    }

    /**
     * Get the [statut_offres] column value.
     *
     * @return int
     */
    public function getStatutOffres()
    {

        return $this->statut_offres;
    }

    /**
     * Get the [date_heure_ouverture] column value.
     *
     * @return string
     */
    public function getDateHeureOuverture()
    {

        return $this->date_heure_ouverture;
    }

    /**
     * Get the [agentid_ouverture] column value.
     *
     * @return int
     */
    public function getAgentidOuverture()
    {

        return $this->agentid_ouverture;
    }

    /**
     * Get the [agentid_ouverture2] column value.
     *
     * @return int
     */
    public function getAgentidOuverture2()
    {

        return $this->agentid_ouverture2;
    }

    /**
     * Get the [date_heure_ouverture_agent2] column value.
     *
     * @return string
     */
    public function getDateHeureOuvertureAgent2()
    {

        return $this->date_heure_ouverture_agent2;
    }

    /**
     * Get the [cryptage_reponse] column value.
     *
     * @return string
     */
    public function getCryptageReponse()
    {

        return $this->cryptage_reponse;
    }

    /**
     * Get the [nom_agent_ouverture] column value.
     *
     * @return string
     */
    public function getNomAgentOuverture()
    {

        return $this->nom_agent_ouverture;
    }

    /**
     * Get the [agent_telechargement_offre] column value.
     *
     * @return int
     */
    public function getAgentTelechargementOffre()
    {

        return $this->agent_telechargement_offre;
    }

    /**
     * Get the [date_telechargement_offre] column value.
     *
     * @return string
     */
    public function getDateTelechargementOffre()
    {

        return $this->date_telechargement_offre;
    }

    /**
     * Get the [repertoire_telechargement_offre] column value.
     *
     * @return string
     */
    public function getRepertoireTelechargementOffre()
    {

        return $this->repertoire_telechargement_offre;
    }

    /**
     * Get the [candidature_id_externe] column value.
     *
     * @return int
     */
    public function getCandidatureIdExterne()
    {

        return $this->candidature_id_externe;
    }

    /**
     * Get the [etat_chiffrement] column value.
     *
     * @return int
     */
    public function getEtatChiffrement()
    {

        return $this->etat_chiffrement;
    }

    /**
     * Get the [erreur_chiffrement] column value.
     *
     * @return string
     */
    public function getErreurChiffrement()
    {

        return $this->erreur_chiffrement;
    }

    /**
     * Get the [optionally formatted] temporal [date_fin_chiffrement] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateFinChiffrement($format = 'Y-m-d H:i:s')
    {
        if ($this->date_fin_chiffrement === null) {
            return null;
        }

        if ($this->date_fin_chiffrement === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_fin_chiffrement);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_fin_chiffrement, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_horodatage] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateHorodatage($format = 'Y-m-d H:i:s')
    {
        if ($this->date_horodatage === null) {
            return null;
        }

        if ($this->date_horodatage === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_horodatage);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_horodatage, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [verification_hotodatage] column value.
     *
     * @return string
     */
    public function getVerificationHotodatage()
    {

        return $this->verification_hotodatage;
    }

    /**
     * Get the [verification_signature_offre] column value.
     *
     * @return string
     */
    public function getVerificationSignatureOffre()
    {

        return $this->verification_signature_offre;
    }

    /**
     * Get the [horodatage_hash_fichiers] column value.
     *
     * @return string
     */
    public function getHorodatageHashFichiers()
    {

        return $this->horodatage_hash_fichiers;
    }

    /**
     * Get the [id_pdf_echange_accuse] column value.
     *
     * @return int
     */
    public function getIdPdfEchangeAccuse()
    {

        return $this->id_pdf_echange_accuse;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = 'Y-m-d H:i:s')
    {
        if ($this->created_at === null) {
            return null;
        }

        if ($this->created_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [uid_response] column value.
     *
     * @return string
     */
    public function getUidResponse()
    {

        return $this->uid_response;
    }

    /**
     * Get the [optionally formatted] temporal [date_depot] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateDepot($format = 'Y-m-d H:i:s')
    {
        if ($this->date_depot === null) {
            return null;
        }

        if ($this->date_depot === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_depot);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_depot, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [resultat_verification_hash_all_files] column value.
     *
     * @return string
     */
    public function getResultatVerificationHashAllFiles()
    {

        return $this->resultat_verification_hash_all_files;
    }

    /**
     * Get the [id_blob_horodatage_hash] column value.
     *
     * @return int
     */
    public function getIdBlobHorodatageHash()
    {

        return $this->id_blob_horodatage_hash;
    }

    /**
     * Get the [id_blob_xml_reponse] column value.
     *
     * @return int
     */
    public function getIdBlobXmlReponse()
    {

        return $this->id_blob_xml_reponse;
    }

    /**
     * Get the [consultation_id] column value.
     *
     * @return int
     */
    public function getConsultationId()
    {

        return $this->consultation_id;
    }

    /**
     * Get the [plateforme_virtuelle_id] column value.
     *
     * @return int
     */
    public function getPlateformeVirtuelleId()
    {

        return $this->plateforme_virtuelle_id;
    }

    /**
     * Get the [inscrit_id] column value.
     *
     * @return string
     */
    public function getInscritId()
    {

        return $this->inscrit_id;
    }

    /**
     * Get the [taux_production_france] column value.
     *
     * @return int
     */
    public function getTauxProductionFrance()
    {

        return $this->taux_production_france;
    }

    /**
     * Get the [taux_production_europe] column value.
     *
     * @return int
     */
    public function getTauxProductionEurope()
    {

        return $this->taux_production_europe;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [consultation_ref] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setConsultationRef($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_ref !== $v) {
            $this->consultation_ref = $v;
            $this->modifiedColumns[] = CommonOffresPeer::CONSULTATION_REF;
        }


        return $this;
    } // setConsultationRef()

    /**
     * Set the value of [entreprise_id] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setEntrepriseId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->entreprise_id !== $v) {
            $this->entreprise_id = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ENTREPRISE_ID;
        }

        if ($this->aEntreprise !== null && $this->aEntreprise->getId() !== $v) {
            $this->aEntreprise = null;
        }


        return $this;
    } // setEntrepriseId()

    /**
     * Set the value of [id_etablissement] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setIdEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_etablissement !== $v) {
            $this->id_etablissement = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ID_ETABLISSEMENT;
        }


        return $this;
    } // setIdEtablissement()

    /**
     * Set the value of [old_inscrit_id] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setOldInscritId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_inscrit_id !== $v) {
            $this->old_inscrit_id = $v;
            $this->modifiedColumns[] = CommonOffresPeer::OLD_INSCRIT_ID;
        }


        return $this;
    } // setOldInscritId()

    /**
     * Set the value of [signatureenvxml] column.
     *
     * @param resource $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setSignatureenvxml($v)
    {
        // Because BLOB columns are streams in PDO we have to assume that they are
        // always modified when a new value is passed in.  For example, the contents
        // of the stream itself may have changed externally.
        if (!is_resource($v) && $v !== null) {
            $this->signatureenvxml = fopen('php://memory', 'r+');
            fwrite($this->signatureenvxml, $v);
            rewind($this->signatureenvxml);
        } else { // it's already a stream
            $this->signatureenvxml = $v;
        }
        $this->modifiedColumns[] = CommonOffresPeer::SIGNATUREENVXML;


        return $this;
    } // setSignatureenvxml()

    /**
     * Set the value of [horodatage] column.
     *
     * @param resource $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setHorodatage($v)
    {
        // Because BLOB columns are streams in PDO we have to assume that they are
        // always modified when a new value is passed in.  For example, the contents
        // of the stream itself may have changed externally.
        if (!is_resource($v) && $v !== null) {
            $this->horodatage = fopen('php://memory', 'r+');
            fwrite($this->horodatage, $v);
            rewind($this->horodatage);
        } else { // it's already a stream
            $this->horodatage = $v;
        }
        $this->modifiedColumns[] = CommonOffresPeer::HORODATAGE;


        return $this;
    } // setHorodatage()

    /**
     * Set the value of [mailsignataire] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setMailsignataire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mailsignataire !== $v) {
            $this->mailsignataire = $v;
            $this->modifiedColumns[] = CommonOffresPeer::MAILSIGNATAIRE;
        }


        return $this;
    } // setMailsignataire()

    /**
     * Set the value of [untrusteddate] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setUntrusteddate($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->untrusteddate !== $v) {
            $this->untrusteddate = $v;
            $this->modifiedColumns[] = CommonOffresPeer::UNTRUSTEDDATE;
        }


        return $this;
    } // setUntrusteddate()

    /**
     * Set the value of [untrustedserial] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setUntrustedserial($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->untrustedserial !== $v) {
            $this->untrustedserial = $v;
            $this->modifiedColumns[] = CommonOffresPeer::UNTRUSTEDSERIAL;
        }


        return $this;
    } // setUntrustedserial()

    /**
     * Set the value of [envoi_complet] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setEnvoiComplet($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->envoi_complet !== $v) {
            $this->envoi_complet = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ENVOI_COMPLET;
        }


        return $this;
    } // setEnvoiComplet()

    /**
     * Set the value of [date_depot_differe] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setDateDepotDiffere($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_depot_differe !== $v) {
            $this->date_depot_differe = $v;
            $this->modifiedColumns[] = CommonOffresPeer::DATE_DEPOT_DIFFERE;
        }


        return $this;
    } // setDateDepotDiffere()

    /**
     * Set the value of [horodatage_envoi_differe] column.
     *
     * @param resource $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setHorodatageEnvoiDiffere($v)
    {
        // Because BLOB columns are streams in PDO we have to assume that they are
        // always modified when a new value is passed in.  For example, the contents
        // of the stream itself may have changed externally.
        if (!is_resource($v) && $v !== null) {
            $this->horodatage_envoi_differe = fopen('php://memory', 'r+');
            fwrite($this->horodatage_envoi_differe, $v);
            rewind($this->horodatage_envoi_differe);
        } else { // it's already a stream
            $this->horodatage_envoi_differe = $v;
        }
        $this->modifiedColumns[] = CommonOffresPeer::HORODATAGE_ENVOI_DIFFERE;


        return $this;
    } // setHorodatageEnvoiDiffere()

    /**
     * Set the value of [signatureenvxml_envoi_differe] column.
     *
     * @param resource $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setSignatureenvxmlEnvoiDiffere($v)
    {
        // Because BLOB columns are streams in PDO we have to assume that they are
        // always modified when a new value is passed in.  For example, the contents
        // of the stream itself may have changed externally.
        if (!is_resource($v) && $v !== null) {
            $this->signatureenvxml_envoi_differe = fopen('php://memory', 'r+');
            fwrite($this->signatureenvxml_envoi_differe, $v);
            rewind($this->signatureenvxml_envoi_differe);
        } else { // it's already a stream
            $this->signatureenvxml_envoi_differe = $v;
        }
        $this->modifiedColumns[] = CommonOffresPeer::SIGNATUREENVXML_ENVOI_DIFFERE;


        return $this;
    } // setSignatureenvxmlEnvoiDiffere()

    /**
     * Set the value of [external_serial] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setExternalSerial($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->external_serial !== $v) {
            $this->external_serial = $v;
            $this->modifiedColumns[] = CommonOffresPeer::EXTERNAL_SERIAL;
        }


        return $this;
    } // setExternalSerial()

    /**
     * Set the value of [internal_serial] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setInternalSerial($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->internal_serial !== $v) {
            $this->internal_serial = $v;
            $this->modifiedColumns[] = CommonOffresPeer::INTERNAL_SERIAL;
        }


        return $this;
    } // setInternalSerial()

    /**
     * Set the value of [uid_offre] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setUidOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->uid_offre !== $v) {
            $this->uid_offre = $v;
            $this->modifiedColumns[] = CommonOffresPeer::UID_OFFRE;
        }


        return $this;
    } // setUidOffre()

    /**
     * Set the value of [offre_selectionnee] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setOffreSelectionnee($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->offre_selectionnee !== $v) {
            $this->offre_selectionnee = $v;
            $this->modifiedColumns[] = CommonOffresPeer::OFFRE_SELECTIONNEE;
        }


        return $this;
    } // setOffreSelectionnee()

    /**
     * Set the value of [observation] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[] = CommonOffresPeer::OBSERVATION;
        }


        return $this;
    } // setObservation()

    /**
     * Set the value of [xml_string] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setXmlString($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->xml_string !== $v) {
            $this->xml_string = $v;
            $this->modifiedColumns[] = CommonOffresPeer::XML_STRING;
        }


        return $this;
    } // setXmlString()

    /**
     * Set the value of [nom_entreprise_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setNomEntrepriseInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_entreprise_inscrit !== $v) {
            $this->nom_entreprise_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::NOM_ENTREPRISE_INSCRIT;
        }


        return $this;
    } // setNomEntrepriseInscrit()

    /**
     * Set the value of [nom_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setNomInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_inscrit !== $v) {
            $this->nom_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::NOM_INSCRIT;
        }


        return $this;
    } // setNomInscrit()

    /**
     * Set the value of [prenom_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setPrenomInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prenom_inscrit !== $v) {
            $this->prenom_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::PRENOM_INSCRIT;
        }


        return $this;
    } // setPrenomInscrit()

    /**
     * Set the value of [adresse_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setAdresseInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_inscrit !== $v) {
            $this->adresse_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ADRESSE_INSCRIT;
        }


        return $this;
    } // setAdresseInscrit()

    /**
     * Set the value of [adresse2_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setAdresse2Inscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2_inscrit !== $v) {
            $this->adresse2_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ADRESSE2_INSCRIT;
        }


        return $this;
    } // setAdresse2Inscrit()

    /**
     * Set the value of [telephone_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setTelephoneInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telephone_inscrit !== $v) {
            $this->telephone_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::TELEPHONE_INSCRIT;
        }


        return $this;
    } // setTelephoneInscrit()

    /**
     * Set the value of [fax_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setFaxInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->fax_inscrit !== $v) {
            $this->fax_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::FAX_INSCRIT;
        }


        return $this;
    } // setFaxInscrit()

    /**
     * Set the value of [code_postal_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setCodePostalInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_postal_inscrit !== $v) {
            $this->code_postal_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::CODE_POSTAL_INSCRIT;
        }


        return $this;
    } // setCodePostalInscrit()

    /**
     * Set the value of [ville_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setVilleInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_inscrit !== $v) {
            $this->ville_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::VILLE_INSCRIT;
        }


        return $this;
    } // setVilleInscrit()

    /**
     * Set the value of [pays_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setPaysInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_inscrit !== $v) {
            $this->pays_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::PAYS_INSCRIT;
        }


        return $this;
    } // setPaysInscrit()

    /**
     * Set the value of [acronyme_pays] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setAcronymePays($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acronyme_pays !== $v) {
            $this->acronyme_pays = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ACRONYME_PAYS;
        }


        return $this;
    } // setAcronymePays()

    /**
     * Set the value of [siret_entreprise] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setSiretEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siret_entreprise !== $v) {
            $this->siret_entreprise = $v;
            $this->modifiedColumns[] = CommonOffresPeer::SIRET_ENTREPRISE;
        }


        return $this;
    } // setSiretEntreprise()

    /**
     * Set the value of [identifiant_national] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setIdentifiantNational($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->identifiant_national !== $v) {
            $this->identifiant_national = $v;
            $this->modifiedColumns[] = CommonOffresPeer::IDENTIFIANT_NATIONAL;
        }


        return $this;
    } // setIdentifiantNational()

    /**
     * Set the value of [email_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setEmailInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email_inscrit !== $v) {
            $this->email_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::EMAIL_INSCRIT;
        }


        return $this;
    } // setEmailInscrit()

    /**
     * Set the value of [siret_inscrit] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setSiretInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siret_inscrit !== $v) {
            $this->siret_inscrit = $v;
            $this->modifiedColumns[] = CommonOffresPeer::SIRET_INSCRIT;
        }


        return $this;
    } // setSiretInscrit()

    /**
     * Set the value of [nom_entreprise] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setNomEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_entreprise !== $v) {
            $this->nom_entreprise = $v;
            $this->modifiedColumns[] = CommonOffresPeer::NOM_ENTREPRISE;
        }


        return $this;
    } // setNomEntreprise()

    /**
     * Set the value of [horodatage_annulation] column.
     *
     * @param resource $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setHorodatageAnnulation($v)
    {
        // Because BLOB columns are streams in PDO we have to assume that they are
        // always modified when a new value is passed in.  For example, the contents
        // of the stream itself may have changed externally.
        if (!is_resource($v) && $v !== null) {
            $this->horodatage_annulation = fopen('php://memory', 'r+');
            fwrite($this->horodatage_annulation, $v);
            rewind($this->horodatage_annulation);
        } else { // it's already a stream
            $this->horodatage_annulation = $v;
        }
        $this->modifiedColumns[] = CommonOffresPeer::HORODATAGE_ANNULATION;


        return $this;
    } // setHorodatageAnnulation()

    /**
     * Set the value of [date_annulation] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setDateAnnulation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_annulation !== $v) {
            $this->date_annulation = $v;
            $this->modifiedColumns[] = CommonOffresPeer::DATE_ANNULATION;
        }


        return $this;
    } // setDateAnnulation()

    /**
     * Set the value of [signature_annulation] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setSignatureAnnulation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->signature_annulation !== $v) {
            $this->signature_annulation = $v;
            $this->modifiedColumns[] = CommonOffresPeer::SIGNATURE_ANNULATION;
        }


        return $this;
    } // setSignatureAnnulation()

    /**
     * Set the value of [depot_annule] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setDepotAnnule($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->depot_annule !== $v) {
            $this->depot_annule = $v;
            $this->modifiedColumns[] = CommonOffresPeer::DEPOT_ANNULE;
        }


        return $this;
    } // setDepotAnnule()

    /**
     * Set the value of [string_annulation] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setStringAnnulation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->string_annulation !== $v) {
            $this->string_annulation = $v;
            $this->modifiedColumns[] = CommonOffresPeer::STRING_ANNULATION;
        }


        return $this;
    } // setStringAnnulation()

    /**
     * Set the value of [verification_certificat_annulation] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setVerificationCertificatAnnulation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->verification_certificat_annulation !== $v) {
            $this->verification_certificat_annulation = $v;
            $this->modifiedColumns[] = CommonOffresPeer::VERIFICATION_CERTIFICAT_ANNULATION;
        }


        return $this;
    } // setVerificationCertificatAnnulation()

    /**
     * Set the value of [offre_variante] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setOffreVariante($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->offre_variante !== $v) {
            $this->offre_variante = $v;
            $this->modifiedColumns[] = CommonOffresPeer::OFFRE_VARIANTE;
        }


        return $this;
    } // setOffreVariante()

    /**
     * Set the value of [reponse_pas_a_pas] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setReponsePasAPas($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->reponse_pas_a_pas !== $v) {
            $this->reponse_pas_a_pas = $v;
            $this->modifiedColumns[] = CommonOffresPeer::REPONSE_PAS_A_PAS;
        }


        return $this;
    } // setReponsePasAPas()

    /**
     * Set the value of [numero_reponse] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setNumeroReponse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->numero_reponse !== $v) {
            $this->numero_reponse = $v;
            $this->modifiedColumns[] = CommonOffresPeer::NUMERO_REPONSE;
        }


        return $this;
    } // setNumeroReponse()

    /**
     * Set the value of [statut_offres] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setStatutOffres($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->statut_offres !== $v) {
            $this->statut_offres = $v;
            $this->modifiedColumns[] = CommonOffresPeer::STATUT_OFFRES;
        }


        return $this;
    } // setStatutOffres()

    /**
     * Set the value of [date_heure_ouverture] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setDateHeureOuverture($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_heure_ouverture !== $v) {
            $this->date_heure_ouverture = $v;
            $this->modifiedColumns[] = CommonOffresPeer::DATE_HEURE_OUVERTURE;
        }


        return $this;
    } // setDateHeureOuverture()

    /**
     * Set the value of [agentid_ouverture] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setAgentidOuverture($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->agentid_ouverture !== $v) {
            $this->agentid_ouverture = $v;
            $this->modifiedColumns[] = CommonOffresPeer::AGENTID_OUVERTURE;
        }


        return $this;
    } // setAgentidOuverture()

    /**
     * Set the value of [agentid_ouverture2] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setAgentidOuverture2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->agentid_ouverture2 !== $v) {
            $this->agentid_ouverture2 = $v;
            $this->modifiedColumns[] = CommonOffresPeer::AGENTID_OUVERTURE2;
        }


        return $this;
    } // setAgentidOuverture2()

    /**
     * Set the value of [date_heure_ouverture_agent2] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setDateHeureOuvertureAgent2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_heure_ouverture_agent2 !== $v) {
            $this->date_heure_ouverture_agent2 = $v;
            $this->modifiedColumns[] = CommonOffresPeer::DATE_HEURE_OUVERTURE_AGENT2;
        }


        return $this;
    } // setDateHeureOuvertureAgent2()

    /**
     * Set the value of [cryptage_reponse] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setCryptageReponse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->cryptage_reponse !== $v) {
            $this->cryptage_reponse = $v;
            $this->modifiedColumns[] = CommonOffresPeer::CRYPTAGE_REPONSE;
        }


        return $this;
    } // setCryptageReponse()

    /**
     * Set the value of [nom_agent_ouverture] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setNomAgentOuverture($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_agent_ouverture !== $v) {
            $this->nom_agent_ouverture = $v;
            $this->modifiedColumns[] = CommonOffresPeer::NOM_AGENT_OUVERTURE;
        }


        return $this;
    } // setNomAgentOuverture()

    /**
     * Set the value of [agent_telechargement_offre] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setAgentTelechargementOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->agent_telechargement_offre !== $v) {
            $this->agent_telechargement_offre = $v;
            $this->modifiedColumns[] = CommonOffresPeer::AGENT_TELECHARGEMENT_OFFRE;
        }


        return $this;
    } // setAgentTelechargementOffre()

    /**
     * Set the value of [date_telechargement_offre] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setDateTelechargementOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_telechargement_offre !== $v) {
            $this->date_telechargement_offre = $v;
            $this->modifiedColumns[] = CommonOffresPeer::DATE_TELECHARGEMENT_OFFRE;
        }


        return $this;
    } // setDateTelechargementOffre()

    /**
     * Set the value of [repertoire_telechargement_offre] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setRepertoireTelechargementOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->repertoire_telechargement_offre !== $v) {
            $this->repertoire_telechargement_offre = $v;
            $this->modifiedColumns[] = CommonOffresPeer::REPERTOIRE_TELECHARGEMENT_OFFRE;
        }


        return $this;
    } // setRepertoireTelechargementOffre()

    /**
     * Set the value of [candidature_id_externe] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setCandidatureIdExterne($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->candidature_id_externe !== $v) {
            $this->candidature_id_externe = $v;
            $this->modifiedColumns[] = CommonOffresPeer::CANDIDATURE_ID_EXTERNE;
        }


        return $this;
    } // setCandidatureIdExterne()

    /**
     * Set the value of [etat_chiffrement] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setEtatChiffrement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->etat_chiffrement !== $v) {
            $this->etat_chiffrement = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ETAT_CHIFFREMENT;
        }


        return $this;
    } // setEtatChiffrement()

    /**
     * Set the value of [erreur_chiffrement] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setErreurChiffrement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->erreur_chiffrement !== $v) {
            $this->erreur_chiffrement = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ERREUR_CHIFFREMENT;
        }


        return $this;
    } // setErreurChiffrement()

    /**
     * Sets the value of [date_fin_chiffrement] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setDateFinChiffrement($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_fin_chiffrement !== null || $dt !== null) {
            $currentDateAsString = ($this->date_fin_chiffrement !== null && $tmpDt = new DateTime($this->date_fin_chiffrement)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_fin_chiffrement = $newDateAsString;
                $this->modifiedColumns[] = CommonOffresPeer::DATE_FIN_CHIFFREMENT;
            }
        } // if either are not null


        return $this;
    } // setDateFinChiffrement()

    /**
     * Sets the value of [date_horodatage] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setDateHorodatage($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_horodatage !== null || $dt !== null) {
            $currentDateAsString = ($this->date_horodatage !== null && $tmpDt = new DateTime($this->date_horodatage)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_horodatage = $newDateAsString;
                $this->modifiedColumns[] = CommonOffresPeer::DATE_HORODATAGE;
            }
        } // if either are not null


        return $this;
    } // setDateHorodatage()

    /**
     * Set the value of [verification_hotodatage] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setVerificationHotodatage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->verification_hotodatage !== $v) {
            $this->verification_hotodatage = $v;
            $this->modifiedColumns[] = CommonOffresPeer::VERIFICATION_HOTODATAGE;
        }


        return $this;
    } // setVerificationHotodatage()

    /**
     * Set the value of [verification_signature_offre] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setVerificationSignatureOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->verification_signature_offre !== $v) {
            $this->verification_signature_offre = $v;
            $this->modifiedColumns[] = CommonOffresPeer::VERIFICATION_SIGNATURE_OFFRE;
        }


        return $this;
    } // setVerificationSignatureOffre()

    /**
     * Set the value of [horodatage_hash_fichiers] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setHorodatageHashFichiers($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->horodatage_hash_fichiers !== $v) {
            $this->horodatage_hash_fichiers = $v;
            $this->modifiedColumns[] = CommonOffresPeer::HORODATAGE_HASH_FICHIERS;
        }


        return $this;
    } // setHorodatageHashFichiers()

    /**
     * Set the value of [id_pdf_echange_accuse] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setIdPdfEchangeAccuse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_pdf_echange_accuse !== $v) {
            $this->id_pdf_echange_accuse = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ID_PDF_ECHANGE_ACCUSE;
        }


        return $this;
    } // setIdPdfEchangeAccuse()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = CommonOffresPeer::CREATED_AT;
            }
        } // if either are not null


        return $this;
    } // setCreatedAt()

    /**
     * Set the value of [uid_response] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setUidResponse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->uid_response !== $v) {
            $this->uid_response = $v;
            $this->modifiedColumns[] = CommonOffresPeer::UID_RESPONSE;
        }


        return $this;
    } // setUidResponse()

    /**
     * Sets the value of [date_depot] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setDateDepot($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_depot !== null || $dt !== null) {
            $currentDateAsString = ($this->date_depot !== null && $tmpDt = new DateTime($this->date_depot)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_depot = $newDateAsString;
                $this->modifiedColumns[] = CommonOffresPeer::DATE_DEPOT;
            }
        } // if either are not null


        return $this;
    } // setDateDepot()

    /**
     * Set the value of [resultat_verification_hash_all_files] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setResultatVerificationHashAllFiles($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->resultat_verification_hash_all_files !== $v) {
            $this->resultat_verification_hash_all_files = $v;
            $this->modifiedColumns[] = CommonOffresPeer::RESULTAT_VERIFICATION_HASH_ALL_FILES;
        }


        return $this;
    } // setResultatVerificationHashAllFiles()

    /**
     * Set the value of [id_blob_horodatage_hash] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setIdBlobHorodatageHash($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_blob_horodatage_hash !== $v) {
            $this->id_blob_horodatage_hash = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ID_BLOB_HORODATAGE_HASH;
        }

        if ($this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash !== null && $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->getId() !== $v) {
            $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash = null;
        }


        return $this;
    } // setIdBlobHorodatageHash()

    /**
     * Set the value of [id_blob_xml_reponse] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setIdBlobXmlReponse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_blob_xml_reponse !== $v) {
            $this->id_blob_xml_reponse = $v;
            $this->modifiedColumns[] = CommonOffresPeer::ID_BLOB_XML_REPONSE;
        }

        if ($this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse !== null && $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->getId() !== $v) {
            $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse = null;
        }


        return $this;
    } // setIdBlobXmlReponse()

    /**
     * Set the value of [consultation_id] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setConsultationId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_id !== $v) {
            $this->consultation_id = $v;
            $this->modifiedColumns[] = CommonOffresPeer::CONSULTATION_ID;
        }

        if ($this->aCommonConsultation !== null && $this->aCommonConsultation->getId() !== $v) {
            $this->aCommonConsultation = null;
        }


        return $this;
    } // setConsultationId()

    /**
     * Set the value of [plateforme_virtuelle_id] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setPlateformeVirtuelleId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->plateforme_virtuelle_id !== $v) {
            $this->plateforme_virtuelle_id = $v;
            $this->modifiedColumns[] = CommonOffresPeer::PLATEFORME_VIRTUELLE_ID;
        }

        if ($this->aCommonPlateformeVirtuelle !== null && $this->aCommonPlateformeVirtuelle->getId() !== $v) {
            $this->aCommonPlateformeVirtuelle = null;
        }


        return $this;
    } // setPlateformeVirtuelleId()

    /**
     * Set the value of [inscrit_id] column.
     *
     * @param string $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setInscritId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->inscrit_id !== $v) {
            $this->inscrit_id = $v;
            $this->modifiedColumns[] = CommonOffresPeer::INSCRIT_ID;
        }

        if ($this->aCommonInscrit !== null && $this->aCommonInscrit->getId() !== $v) {
            $this->aCommonInscrit = null;
        }


        return $this;
    } // setInscritId()

    /**
     * Set the value of [taux_production_france] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setTauxProductionFrance($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->taux_production_france !== $v) {
            $this->taux_production_france = $v;
            $this->modifiedColumns[] = CommonOffresPeer::TAUX_PRODUCTION_FRANCE;
        }


        return $this;
    } // setTauxProductionFrance()

    /**
     * Set the value of [taux_production_europe] column.
     *
     * @param int $v new value
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setTauxProductionEurope($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->taux_production_europe !== $v) {
            $this->taux_production_europe = $v;
            $this->modifiedColumns[] = CommonOffresPeer::TAUX_PRODUCTION_EUROPE;
        }


        return $this;
    } // setTauxProductionEurope()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->organisme !== '') {
                return false;
            }

            if ($this->consultation_ref !== 0) {
                return false;
            }

            if ($this->entreprise_id !== 0) {
                return false;
            }

            if ($this->mailsignataire !== '') {
                return false;
            }

            if ($this->untrusteddate !== '0000-00-00 00:00:00') {
                return false;
            }

            if ($this->untrustedserial !== '') {
                return false;
            }

            if ($this->envoi_complet !== '') {
                return false;
            }

            if ($this->date_depot_differe !== '0000-00-00 00:00:00') {
                return false;
            }

            if ($this->uid_offre !== '') {
                return false;
            }

            if ($this->offre_selectionnee !== 0) {
                return false;
            }

            if ($this->depot_annule !== '0') {
                return false;
            }

            if ($this->reponse_pas_a_pas !== '0') {
                return false;
            }

            if ($this->date_heure_ouverture !== '0000-00-00 00:00:00') {
                return false;
            }

            if ($this->date_heure_ouverture_agent2 !== '0000-00-00 00:00:00') {
                return false;
            }

            if ($this->cryptage_reponse !== '1') {
                return false;
            }

            if ($this->etat_chiffrement !== 1) {
                return false;
            }

            if ($this->verification_hotodatage !== '0') {
                return false;
            }

            if ($this->verification_signature_offre !== '') {
                return false;
            }

            if ($this->resultat_verification_hash_all_files !== '1') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->organisme = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->consultation_ref = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->entreprise_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->id_etablissement = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->old_inscrit_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            if ($row[$startcol + 6] !== null) {
                $this->signatureenvxml = fopen('php://memory', 'r+');
                fwrite($this->signatureenvxml, $row[$startcol + 6]);
                rewind($this->signatureenvxml);
            } else {
                $this->signatureenvxml = null;
            }
            if ($row[$startcol + 7] !== null) {
                $this->horodatage = fopen('php://memory', 'r+');
                fwrite($this->horodatage, $row[$startcol + 7]);
                rewind($this->horodatage);
            } else {
                $this->horodatage = null;
            }
            $this->mailsignataire = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->untrusteddate = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->untrustedserial = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->envoi_complet = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->date_depot_differe = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            if ($row[$startcol + 13] !== null) {
                $this->horodatage_envoi_differe = fopen('php://memory', 'r+');
                fwrite($this->horodatage_envoi_differe, $row[$startcol + 13]);
                rewind($this->horodatage_envoi_differe);
            } else {
                $this->horodatage_envoi_differe = null;
            }
            if ($row[$startcol + 14] !== null) {
                $this->signatureenvxml_envoi_differe = fopen('php://memory', 'r+');
                fwrite($this->signatureenvxml_envoi_differe, $row[$startcol + 14]);
                rewind($this->signatureenvxml_envoi_differe);
            } else {
                $this->signatureenvxml_envoi_differe = null;
            }
            $this->external_serial = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->internal_serial = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->uid_offre = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->offre_selectionnee = ($row[$startcol + 18] !== null) ? (int) $row[$startcol + 18] : null;
            $this->observation = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->xml_string = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->nom_entreprise_inscrit = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->nom_inscrit = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->prenom_inscrit = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->adresse_inscrit = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->adresse2_inscrit = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->telephone_inscrit = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->fax_inscrit = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->code_postal_inscrit = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->ville_inscrit = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->pays_inscrit = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->acronyme_pays = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->siret_entreprise = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->identifiant_national = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->email_inscrit = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->siret_inscrit = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->nom_entreprise = ($row[$startcol + 36] !== null) ? (string) $row[$startcol + 36] : null;
            if ($row[$startcol + 37] !== null) {
                $this->horodatage_annulation = fopen('php://memory', 'r+');
                fwrite($this->horodatage_annulation, $row[$startcol + 37]);
                rewind($this->horodatage_annulation);
            } else {
                $this->horodatage_annulation = null;
            }
            $this->date_annulation = ($row[$startcol + 38] !== null) ? (string) $row[$startcol + 38] : null;
            $this->signature_annulation = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->depot_annule = ($row[$startcol + 40] !== null) ? (string) $row[$startcol + 40] : null;
            $this->string_annulation = ($row[$startcol + 41] !== null) ? (string) $row[$startcol + 41] : null;
            $this->verification_certificat_annulation = ($row[$startcol + 42] !== null) ? (string) $row[$startcol + 42] : null;
            $this->offre_variante = ($row[$startcol + 43] !== null) ? (string) $row[$startcol + 43] : null;
            $this->reponse_pas_a_pas = ($row[$startcol + 44] !== null) ? (string) $row[$startcol + 44] : null;
            $this->numero_reponse = ($row[$startcol + 45] !== null) ? (int) $row[$startcol + 45] : null;
            $this->statut_offres = ($row[$startcol + 46] !== null) ? (int) $row[$startcol + 46] : null;
            $this->date_heure_ouverture = ($row[$startcol + 47] !== null) ? (string) $row[$startcol + 47] : null;
            $this->agentid_ouverture = ($row[$startcol + 48] !== null) ? (int) $row[$startcol + 48] : null;
            $this->agentid_ouverture2 = ($row[$startcol + 49] !== null) ? (int) $row[$startcol + 49] : null;
            $this->date_heure_ouverture_agent2 = ($row[$startcol + 50] !== null) ? (string) $row[$startcol + 50] : null;
            $this->cryptage_reponse = ($row[$startcol + 51] !== null) ? (string) $row[$startcol + 51] : null;
            $this->nom_agent_ouverture = ($row[$startcol + 52] !== null) ? (string) $row[$startcol + 52] : null;
            $this->agent_telechargement_offre = ($row[$startcol + 53] !== null) ? (int) $row[$startcol + 53] : null;
            $this->date_telechargement_offre = ($row[$startcol + 54] !== null) ? (string) $row[$startcol + 54] : null;
            $this->repertoire_telechargement_offre = ($row[$startcol + 55] !== null) ? (string) $row[$startcol + 55] : null;
            $this->candidature_id_externe = ($row[$startcol + 56] !== null) ? (int) $row[$startcol + 56] : null;
            $this->etat_chiffrement = ($row[$startcol + 57] !== null) ? (int) $row[$startcol + 57] : null;
            $this->erreur_chiffrement = ($row[$startcol + 58] !== null) ? (string) $row[$startcol + 58] : null;
            $this->date_fin_chiffrement = ($row[$startcol + 59] !== null) ? (string) $row[$startcol + 59] : null;
            $this->date_horodatage = ($row[$startcol + 60] !== null) ? (string) $row[$startcol + 60] : null;
            $this->verification_hotodatage = ($row[$startcol + 61] !== null) ? (string) $row[$startcol + 61] : null;
            $this->verification_signature_offre = ($row[$startcol + 62] !== null) ? (string) $row[$startcol + 62] : null;
            $this->horodatage_hash_fichiers = ($row[$startcol + 63] !== null) ? (string) $row[$startcol + 63] : null;
            $this->id_pdf_echange_accuse = ($row[$startcol + 64] !== null) ? (int) $row[$startcol + 64] : null;
            $this->created_at = ($row[$startcol + 65] !== null) ? (string) $row[$startcol + 65] : null;
            $this->uid_response = ($row[$startcol + 66] !== null) ? (string) $row[$startcol + 66] : null;
            $this->date_depot = ($row[$startcol + 67] !== null) ? (string) $row[$startcol + 67] : null;
            $this->resultat_verification_hash_all_files = ($row[$startcol + 68] !== null) ? (string) $row[$startcol + 68] : null;
            $this->id_blob_horodatage_hash = ($row[$startcol + 69] !== null) ? (int) $row[$startcol + 69] : null;
            $this->id_blob_xml_reponse = ($row[$startcol + 70] !== null) ? (int) $row[$startcol + 70] : null;
            $this->consultation_id = ($row[$startcol + 71] !== null) ? (int) $row[$startcol + 71] : null;
            $this->plateforme_virtuelle_id = ($row[$startcol + 72] !== null) ? (int) $row[$startcol + 72] : null;
            $this->inscrit_id = ($row[$startcol + 73] !== null) ? (string) $row[$startcol + 73] : null;
            $this->taux_production_france = ($row[$startcol + 74] !== null) ? (int) $row[$startcol + 74] : null;
            $this->taux_production_europe = ($row[$startcol + 75] !== null) ? (int) $row[$startcol + 75] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 76; // 76 = CommonOffresPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonOffres object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aEntreprise !== null && $this->entreprise_id !== $this->aEntreprise->getId()) {
            $this->aEntreprise = null;
        }
        if ($this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash !== null && $this->id_blob_horodatage_hash !== $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->getId()) {
            $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash = null;
        }
        if ($this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse !== null && $this->id_blob_xml_reponse !== $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->getId()) {
            $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse = null;
        }
        if ($this->aCommonConsultation !== null && $this->consultation_id !== $this->aCommonConsultation->getId()) {
            $this->aCommonConsultation = null;
        }
        if ($this->aCommonPlateformeVirtuelle !== null && $this->plateforme_virtuelle_id !== $this->aCommonPlateformeVirtuelle->getId()) {
            $this->aCommonPlateformeVirtuelle = null;
        }
        if ($this->aCommonInscrit !== null && $this->inscrit_id !== $this->aCommonInscrit->getId()) {
            $this->aCommonInscrit = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonOffresPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash = null;
            $this->aCommonConsultation = null;
            $this->aCommonInscrit = null;
            $this->aEntreprise = null;
            $this->aCommonPlateformeVirtuelle = null;
            $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse = null;
            $this->collCommonTCandidatures = null;

            $this->collCommonTGroupementEntreprises = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonOffresQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonOffresPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonOffresPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash !== null) {
                if ($this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->isModified() || $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->isNew()) {
                    $affectedRows += $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->save($con);
                }
                $this->setCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash($this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash);
            }

            if ($this->aCommonConsultation !== null) {
                if ($this->aCommonConsultation->isModified() || $this->aCommonConsultation->isNew()) {
                    $affectedRows += $this->aCommonConsultation->save($con);
                }
                $this->setCommonConsultation($this->aCommonConsultation);
            }

            if ($this->aCommonInscrit !== null) {
                if ($this->aCommonInscrit->isModified() || $this->aCommonInscrit->isNew()) {
                    $affectedRows += $this->aCommonInscrit->save($con);
                }
                $this->setCommonInscrit($this->aCommonInscrit);
            }

            if ($this->aEntreprise !== null) {
                if ($this->aEntreprise->isModified() || $this->aEntreprise->isNew()) {
                    $affectedRows += $this->aEntreprise->save($con);
                }
                $this->setEntreprise($this->aEntreprise);
            }

            if ($this->aCommonPlateformeVirtuelle !== null) {
                if ($this->aCommonPlateformeVirtuelle->isModified() || $this->aCommonPlateformeVirtuelle->isNew()) {
                    $affectedRows += $this->aCommonPlateformeVirtuelle->save($con);
                }
                $this->setCommonPlateformeVirtuelle($this->aCommonPlateformeVirtuelle);
            }

            if ($this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse !== null) {
                if ($this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->isModified() || $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->isNew()) {
                    $affectedRows += $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->save($con);
                }
                $this->setCommonBlobOrganismeFileRelatedByIdBlobXmlReponse($this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                // Rewind the signatureenvxml LOB column, since PDO does not rewind after inserting value.
                if ($this->signatureenvxml !== null && is_resource($this->signatureenvxml)) {
                    rewind($this->signatureenvxml);
                }

                // Rewind the horodatage LOB column, since PDO does not rewind after inserting value.
                if ($this->horodatage !== null && is_resource($this->horodatage)) {
                    rewind($this->horodatage);
                }

                // Rewind the horodatage_envoi_differe LOB column, since PDO does not rewind after inserting value.
                if ($this->horodatage_envoi_differe !== null && is_resource($this->horodatage_envoi_differe)) {
                    rewind($this->horodatage_envoi_differe);
                }

                // Rewind the signatureenvxml_envoi_differe LOB column, since PDO does not rewind after inserting value.
                if ($this->signatureenvxml_envoi_differe !== null && is_resource($this->signatureenvxml_envoi_differe)) {
                    rewind($this->signatureenvxml_envoi_differe);
                }

                // Rewind the horodatage_annulation LOB column, since PDO does not rewind after inserting value.
                if ($this->horodatage_annulation !== null && is_resource($this->horodatage_annulation)) {
                    rewind($this->horodatage_annulation);
                }

                $this->resetModified();
            }

            if ($this->commonTCandidaturesScheduledForDeletion !== null) {
                if (!$this->commonTCandidaturesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCandidaturesScheduledForDeletion as $commonTCandidature) {
                        // need to save related object because we set the relation to null
                        $commonTCandidature->save($con);
                    }
                    $this->commonTCandidaturesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCandidatures !== null) {
                foreach ($this->collCommonTCandidatures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTGroupementEntreprisesScheduledForDeletion !== null) {
                if (!$this->commonTGroupementEntreprisesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTGroupementEntreprisesScheduledForDeletion as $commonTGroupementEntreprise) {
                        // need to save related object because we set the relation to null
                        $commonTGroupementEntreprise->save($con);
                    }
                    $this->commonTGroupementEntreprisesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTGroupementEntreprises !== null) {
                foreach ($this->collCommonTGroupementEntreprises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonOffresPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonOffresPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonOffresPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonOffresPeer::CONSULTATION_REF)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_ref`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ENTREPRISE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`entreprise_id`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ID_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_etablissement`';
        }
        if ($this->isColumnModified(CommonOffresPeer::OLD_INSCRIT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`old_inscrit_id`';
        }
        if ($this->isColumnModified(CommonOffresPeer::SIGNATUREENVXML)) {
            $modifiedColumns[':p' . $index++]  = '`signatureenvxml`';
        }
        if ($this->isColumnModified(CommonOffresPeer::HORODATAGE)) {
            $modifiedColumns[':p' . $index++]  = '`horodatage`';
        }
        if ($this->isColumnModified(CommonOffresPeer::MAILSIGNATAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`mailsignataire`';
        }
        if ($this->isColumnModified(CommonOffresPeer::UNTRUSTEDDATE)) {
            $modifiedColumns[':p' . $index++]  = '`untrusteddate`';
        }
        if ($this->isColumnModified(CommonOffresPeer::UNTRUSTEDSERIAL)) {
            $modifiedColumns[':p' . $index++]  = '`untrustedserial`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ENVOI_COMPLET)) {
            $modifiedColumns[':p' . $index++]  = '`envoi_complet`';
        }
        if ($this->isColumnModified(CommonOffresPeer::DATE_DEPOT_DIFFERE)) {
            $modifiedColumns[':p' . $index++]  = '`date_depot_differe`';
        }
        if ($this->isColumnModified(CommonOffresPeer::HORODATAGE_ENVOI_DIFFERE)) {
            $modifiedColumns[':p' . $index++]  = '`horodatage_envoi_differe`';
        }
        if ($this->isColumnModified(CommonOffresPeer::SIGNATUREENVXML_ENVOI_DIFFERE)) {
            $modifiedColumns[':p' . $index++]  = '`signatureenvxml_envoi_differe`';
        }
        if ($this->isColumnModified(CommonOffresPeer::EXTERNAL_SERIAL)) {
            $modifiedColumns[':p' . $index++]  = '`external_serial`';
        }
        if ($this->isColumnModified(CommonOffresPeer::INTERNAL_SERIAL)) {
            $modifiedColumns[':p' . $index++]  = '`internal_serial`';
        }
        if ($this->isColumnModified(CommonOffresPeer::UID_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`uid_offre`';
        }
        if ($this->isColumnModified(CommonOffresPeer::OFFRE_SELECTIONNEE)) {
            $modifiedColumns[':p' . $index++]  = '`offre_selectionnee`';
        }
        if ($this->isColumnModified(CommonOffresPeer::OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`Observation`';
        }
        if ($this->isColumnModified(CommonOffresPeer::XML_STRING)) {
            $modifiedColumns[':p' . $index++]  = '`xml_string`';
        }
        if ($this->isColumnModified(CommonOffresPeer::NOM_ENTREPRISE_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`nom_entreprise_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::NOM_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`nom_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::PRENOM_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`prenom_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ADRESSE_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ADRESSE2_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::TELEPHONE_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`telephone_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::FAX_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`fax_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::CODE_POSTAL_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`code_postal_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::VILLE_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`ville_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::PAYS_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`pays_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ACRONYME_PAYS)) {
            $modifiedColumns[':p' . $index++]  = '`acronyme_pays`';
        }
        if ($this->isColumnModified(CommonOffresPeer::SIRET_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`siret_entreprise`';
        }
        if ($this->isColumnModified(CommonOffresPeer::IDENTIFIANT_NATIONAL)) {
            $modifiedColumns[':p' . $index++]  = '`identifiant_national`';
        }
        if ($this->isColumnModified(CommonOffresPeer::EMAIL_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`email_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::SIRET_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`siret_inscrit`';
        }
        if ($this->isColumnModified(CommonOffresPeer::NOM_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`nom_entreprise`';
        }
        if ($this->isColumnModified(CommonOffresPeer::HORODATAGE_ANNULATION)) {
            $modifiedColumns[':p' . $index++]  = '`horodatage_annulation`';
        }
        if ($this->isColumnModified(CommonOffresPeer::DATE_ANNULATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_annulation`';
        }
        if ($this->isColumnModified(CommonOffresPeer::SIGNATURE_ANNULATION)) {
            $modifiedColumns[':p' . $index++]  = '`signature_annulation`';
        }
        if ($this->isColumnModified(CommonOffresPeer::DEPOT_ANNULE)) {
            $modifiedColumns[':p' . $index++]  = '`depot_annule`';
        }
        if ($this->isColumnModified(CommonOffresPeer::STRING_ANNULATION)) {
            $modifiedColumns[':p' . $index++]  = '`string_annulation`';
        }
        if ($this->isColumnModified(CommonOffresPeer::VERIFICATION_CERTIFICAT_ANNULATION)) {
            $modifiedColumns[':p' . $index++]  = '`verification_certificat_annulation`';
        }
        if ($this->isColumnModified(CommonOffresPeer::OFFRE_VARIANTE)) {
            $modifiedColumns[':p' . $index++]  = '`offre_variante`';
        }
        if ($this->isColumnModified(CommonOffresPeer::REPONSE_PAS_A_PAS)) {
            $modifiedColumns[':p' . $index++]  = '`reponse_pas_a_pas`';
        }
        if ($this->isColumnModified(CommonOffresPeer::NUMERO_REPONSE)) {
            $modifiedColumns[':p' . $index++]  = '`numero_reponse`';
        }
        if ($this->isColumnModified(CommonOffresPeer::STATUT_OFFRES)) {
            $modifiedColumns[':p' . $index++]  = '`statut_offres`';
        }
        if ($this->isColumnModified(CommonOffresPeer::DATE_HEURE_OUVERTURE)) {
            $modifiedColumns[':p' . $index++]  = '`date_heure_ouverture`';
        }
        if ($this->isColumnModified(CommonOffresPeer::AGENTID_OUVERTURE)) {
            $modifiedColumns[':p' . $index++]  = '`agentid_ouverture`';
        }
        if ($this->isColumnModified(CommonOffresPeer::AGENTID_OUVERTURE2)) {
            $modifiedColumns[':p' . $index++]  = '`agentid_ouverture2`';
        }
        if ($this->isColumnModified(CommonOffresPeer::DATE_HEURE_OUVERTURE_AGENT2)) {
            $modifiedColumns[':p' . $index++]  = '`date_heure_ouverture_agent2`';
        }
        if ($this->isColumnModified(CommonOffresPeer::CRYPTAGE_REPONSE)) {
            $modifiedColumns[':p' . $index++]  = '`cryptage_reponse`';
        }
        if ($this->isColumnModified(CommonOffresPeer::NOM_AGENT_OUVERTURE)) {
            $modifiedColumns[':p' . $index++]  = '`nom_agent_ouverture`';
        }
        if ($this->isColumnModified(CommonOffresPeer::AGENT_TELECHARGEMENT_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`agent_telechargement_offre`';
        }
        if ($this->isColumnModified(CommonOffresPeer::DATE_TELECHARGEMENT_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`date_telechargement_offre`';
        }
        if ($this->isColumnModified(CommonOffresPeer::REPERTOIRE_TELECHARGEMENT_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`repertoire_telechargement_offre`';
        }
        if ($this->isColumnModified(CommonOffresPeer::CANDIDATURE_ID_EXTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`candidature_id_externe`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ETAT_CHIFFREMENT)) {
            $modifiedColumns[':p' . $index++]  = '`etat_chiffrement`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ERREUR_CHIFFREMENT)) {
            $modifiedColumns[':p' . $index++]  = '`erreur_chiffrement`';
        }
        if ($this->isColumnModified(CommonOffresPeer::DATE_FIN_CHIFFREMENT)) {
            $modifiedColumns[':p' . $index++]  = '`date_fin_chiffrement`';
        }
        if ($this->isColumnModified(CommonOffresPeer::DATE_HORODATAGE)) {
            $modifiedColumns[':p' . $index++]  = '`date_horodatage`';
        }
        if ($this->isColumnModified(CommonOffresPeer::VERIFICATION_HOTODATAGE)) {
            $modifiedColumns[':p' . $index++]  = '`verification_hotodatage`';
        }
        if ($this->isColumnModified(CommonOffresPeer::VERIFICATION_SIGNATURE_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`verification_signature_offre`';
        }
        if ($this->isColumnModified(CommonOffresPeer::HORODATAGE_HASH_FICHIERS)) {
            $modifiedColumns[':p' . $index++]  = '`horodatage_hash_fichiers`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ID_PDF_ECHANGE_ACCUSE)) {
            $modifiedColumns[':p' . $index++]  = '`id_pdf_echange_accuse`';
        }
        if ($this->isColumnModified(CommonOffresPeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(CommonOffresPeer::UID_RESPONSE)) {
            $modifiedColumns[':p' . $index++]  = '`uid_response`';
        }
        if ($this->isColumnModified(CommonOffresPeer::DATE_DEPOT)) {
            $modifiedColumns[':p' . $index++]  = '`date_depot`';
        }
        if ($this->isColumnModified(CommonOffresPeer::RESULTAT_VERIFICATION_HASH_ALL_FILES)) {
            $modifiedColumns[':p' . $index++]  = '`resultat_verification_hash_all_files`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH)) {
            $modifiedColumns[':p' . $index++]  = '`id_blob_horodatage_hash`';
        }
        if ($this->isColumnModified(CommonOffresPeer::ID_BLOB_XML_REPONSE)) {
            $modifiedColumns[':p' . $index++]  = '`id_blob_xml_reponse`';
        }
        if ($this->isColumnModified(CommonOffresPeer::CONSULTATION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_id`';
        }
        if ($this->isColumnModified(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`plateforme_virtuelle_id`';
        }
        if ($this->isColumnModified(CommonOffresPeer::INSCRIT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`inscrit_id`';
        }
        if ($this->isColumnModified(CommonOffresPeer::TAUX_PRODUCTION_FRANCE)) {
            $modifiedColumns[':p' . $index++]  = '`taux_production_france`';
        }
        if ($this->isColumnModified(CommonOffresPeer::TAUX_PRODUCTION_EUROPE)) {
            $modifiedColumns[':p' . $index++]  = '`taux_production_europe`';
        }

        $sql = sprintf(
            'INSERT INTO `Offres` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`consultation_ref`':
                        $stmt->bindValue($identifier, $this->consultation_ref, PDO::PARAM_INT);
                        break;
                    case '`entreprise_id`':
                        $stmt->bindValue($identifier, $this->entreprise_id, PDO::PARAM_INT);
                        break;
                    case '`id_etablissement`':
                        $stmt->bindValue($identifier, $this->id_etablissement, PDO::PARAM_INT);
                        break;
                    case '`old_inscrit_id`':
                        $stmt->bindValue($identifier, $this->old_inscrit_id, PDO::PARAM_INT);
                        break;
                    case '`signatureenvxml`':
                        if (is_resource($this->signatureenvxml)) {
                            rewind($this->signatureenvxml);
                        }
                        $stmt->bindValue($identifier, $this->signatureenvxml, PDO::PARAM_LOB);
                        break;
                    case '`horodatage`':
                        if (is_resource($this->horodatage)) {
                            rewind($this->horodatage);
                        }
                        $stmt->bindValue($identifier, $this->horodatage, PDO::PARAM_LOB);
                        break;
                    case '`mailsignataire`':
                        $stmt->bindValue($identifier, $this->mailsignataire, PDO::PARAM_STR);
                        break;
                    case '`untrusteddate`':
                        $stmt->bindValue($identifier, $this->untrusteddate, PDO::PARAM_STR);
                        break;
                    case '`untrustedserial`':
                        $stmt->bindValue($identifier, $this->untrustedserial, PDO::PARAM_STR);
                        break;
                    case '`envoi_complet`':
                        $stmt->bindValue($identifier, $this->envoi_complet, PDO::PARAM_STR);
                        break;
                    case '`date_depot_differe`':
                        $stmt->bindValue($identifier, $this->date_depot_differe, PDO::PARAM_STR);
                        break;
                    case '`horodatage_envoi_differe`':
                        if (is_resource($this->horodatage_envoi_differe)) {
                            rewind($this->horodatage_envoi_differe);
                        }
                        $stmt->bindValue($identifier, $this->horodatage_envoi_differe, PDO::PARAM_LOB);
                        break;
                    case '`signatureenvxml_envoi_differe`':
                        if (is_resource($this->signatureenvxml_envoi_differe)) {
                            rewind($this->signatureenvxml_envoi_differe);
                        }
                        $stmt->bindValue($identifier, $this->signatureenvxml_envoi_differe, PDO::PARAM_LOB);
                        break;
                    case '`external_serial`':
                        $stmt->bindValue($identifier, $this->external_serial, PDO::PARAM_STR);
                        break;
                    case '`internal_serial`':
                        $stmt->bindValue($identifier, $this->internal_serial, PDO::PARAM_STR);
                        break;
                    case '`uid_offre`':
                        $stmt->bindValue($identifier, $this->uid_offre, PDO::PARAM_STR);
                        break;
                    case '`offre_selectionnee`':
                        $stmt->bindValue($identifier, $this->offre_selectionnee, PDO::PARAM_INT);
                        break;
                    case '`Observation`':
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`xml_string`':
                        $stmt->bindValue($identifier, $this->xml_string, PDO::PARAM_STR);
                        break;
                    case '`nom_entreprise_inscrit`':
                        $stmt->bindValue($identifier, $this->nom_entreprise_inscrit, PDO::PARAM_STR);
                        break;
                    case '`nom_inscrit`':
                        $stmt->bindValue($identifier, $this->nom_inscrit, PDO::PARAM_STR);
                        break;
                    case '`prenom_inscrit`':
                        $stmt->bindValue($identifier, $this->prenom_inscrit, PDO::PARAM_STR);
                        break;
                    case '`adresse_inscrit`':
                        $stmt->bindValue($identifier, $this->adresse_inscrit, PDO::PARAM_STR);
                        break;
                    case '`adresse2_inscrit`':
                        $stmt->bindValue($identifier, $this->adresse2_inscrit, PDO::PARAM_STR);
                        break;
                    case '`telephone_inscrit`':
                        $stmt->bindValue($identifier, $this->telephone_inscrit, PDO::PARAM_STR);
                        break;
                    case '`fax_inscrit`':
                        $stmt->bindValue($identifier, $this->fax_inscrit, PDO::PARAM_STR);
                        break;
                    case '`code_postal_inscrit`':
                        $stmt->bindValue($identifier, $this->code_postal_inscrit, PDO::PARAM_STR);
                        break;
                    case '`ville_inscrit`':
                        $stmt->bindValue($identifier, $this->ville_inscrit, PDO::PARAM_STR);
                        break;
                    case '`pays_inscrit`':
                        $stmt->bindValue($identifier, $this->pays_inscrit, PDO::PARAM_STR);
                        break;
                    case '`acronyme_pays`':
                        $stmt->bindValue($identifier, $this->acronyme_pays, PDO::PARAM_STR);
                        break;
                    case '`siret_entreprise`':
                        $stmt->bindValue($identifier, $this->siret_entreprise, PDO::PARAM_STR);
                        break;
                    case '`identifiant_national`':
                        $stmt->bindValue($identifier, $this->identifiant_national, PDO::PARAM_STR);
                        break;
                    case '`email_inscrit`':
                        $stmt->bindValue($identifier, $this->email_inscrit, PDO::PARAM_STR);
                        break;
                    case '`siret_inscrit`':
                        $stmt->bindValue($identifier, $this->siret_inscrit, PDO::PARAM_STR);
                        break;
                    case '`nom_entreprise`':
                        $stmt->bindValue($identifier, $this->nom_entreprise, PDO::PARAM_STR);
                        break;
                    case '`horodatage_annulation`':
                        if (is_resource($this->horodatage_annulation)) {
                            rewind($this->horodatage_annulation);
                        }
                        $stmt->bindValue($identifier, $this->horodatage_annulation, PDO::PARAM_LOB);
                        break;
                    case '`date_annulation`':
                        $stmt->bindValue($identifier, $this->date_annulation, PDO::PARAM_STR);
                        break;
                    case '`signature_annulation`':
                        $stmt->bindValue($identifier, $this->signature_annulation, PDO::PARAM_STR);
                        break;
                    case '`depot_annule`':
                        $stmt->bindValue($identifier, $this->depot_annule, PDO::PARAM_STR);
                        break;
                    case '`string_annulation`':
                        $stmt->bindValue($identifier, $this->string_annulation, PDO::PARAM_STR);
                        break;
                    case '`verification_certificat_annulation`':
                        $stmt->bindValue($identifier, $this->verification_certificat_annulation, PDO::PARAM_STR);
                        break;
                    case '`offre_variante`':
                        $stmt->bindValue($identifier, $this->offre_variante, PDO::PARAM_STR);
                        break;
                    case '`reponse_pas_a_pas`':
                        $stmt->bindValue($identifier, $this->reponse_pas_a_pas, PDO::PARAM_STR);
                        break;
                    case '`numero_reponse`':
                        $stmt->bindValue($identifier, $this->numero_reponse, PDO::PARAM_INT);
                        break;
                    case '`statut_offres`':
                        $stmt->bindValue($identifier, $this->statut_offres, PDO::PARAM_INT);
                        break;
                    case '`date_heure_ouverture`':
                        $stmt->bindValue($identifier, $this->date_heure_ouverture, PDO::PARAM_STR);
                        break;
                    case '`agentid_ouverture`':
                        $stmt->bindValue($identifier, $this->agentid_ouverture, PDO::PARAM_INT);
                        break;
                    case '`agentid_ouverture2`':
                        $stmt->bindValue($identifier, $this->agentid_ouverture2, PDO::PARAM_INT);
                        break;
                    case '`date_heure_ouverture_agent2`':
                        $stmt->bindValue($identifier, $this->date_heure_ouverture_agent2, PDO::PARAM_STR);
                        break;
                    case '`cryptage_reponse`':
                        $stmt->bindValue($identifier, $this->cryptage_reponse, PDO::PARAM_STR);
                        break;
                    case '`nom_agent_ouverture`':
                        $stmt->bindValue($identifier, $this->nom_agent_ouverture, PDO::PARAM_STR);
                        break;
                    case '`agent_telechargement_offre`':
                        $stmt->bindValue($identifier, $this->agent_telechargement_offre, PDO::PARAM_INT);
                        break;
                    case '`date_telechargement_offre`':
                        $stmt->bindValue($identifier, $this->date_telechargement_offre, PDO::PARAM_STR);
                        break;
                    case '`repertoire_telechargement_offre`':
                        $stmt->bindValue($identifier, $this->repertoire_telechargement_offre, PDO::PARAM_STR);
                        break;
                    case '`candidature_id_externe`':
                        $stmt->bindValue($identifier, $this->candidature_id_externe, PDO::PARAM_INT);
                        break;
                    case '`etat_chiffrement`':
                        $stmt->bindValue($identifier, $this->etat_chiffrement, PDO::PARAM_INT);
                        break;
                    case '`erreur_chiffrement`':
                        $stmt->bindValue($identifier, $this->erreur_chiffrement, PDO::PARAM_STR);
                        break;
                    case '`date_fin_chiffrement`':
                        $stmt->bindValue($identifier, $this->date_fin_chiffrement, PDO::PARAM_STR);
                        break;
                    case '`date_horodatage`':
                        $stmt->bindValue($identifier, $this->date_horodatage, PDO::PARAM_STR);
                        break;
                    case '`verification_hotodatage`':
                        $stmt->bindValue($identifier, $this->verification_hotodatage, PDO::PARAM_STR);
                        break;
                    case '`verification_signature_offre`':
                        $stmt->bindValue($identifier, $this->verification_signature_offre, PDO::PARAM_STR);
                        break;
                    case '`horodatage_hash_fichiers`':
                        $stmt->bindValue($identifier, $this->horodatage_hash_fichiers, PDO::PARAM_STR);
                        break;
                    case '`id_pdf_echange_accuse`':
                        $stmt->bindValue($identifier, $this->id_pdf_echange_accuse, PDO::PARAM_INT);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                    case '`uid_response`':
                        $stmt->bindValue($identifier, $this->uid_response, PDO::PARAM_STR);
                        break;
                    case '`date_depot`':
                        $stmt->bindValue($identifier, $this->date_depot, PDO::PARAM_STR);
                        break;
                    case '`resultat_verification_hash_all_files`':
                        $stmt->bindValue($identifier, $this->resultat_verification_hash_all_files, PDO::PARAM_STR);
                        break;
                    case '`id_blob_horodatage_hash`':
                        $stmt->bindValue($identifier, $this->id_blob_horodatage_hash, PDO::PARAM_INT);
                        break;
                    case '`id_blob_xml_reponse`':
                        $stmt->bindValue($identifier, $this->id_blob_xml_reponse, PDO::PARAM_INT);
                        break;
                    case '`consultation_id`':
                        $stmt->bindValue($identifier, $this->consultation_id, PDO::PARAM_INT);
                        break;
                    case '`plateforme_virtuelle_id`':
                        $stmt->bindValue($identifier, $this->plateforme_virtuelle_id, PDO::PARAM_INT);
                        break;
                    case '`inscrit_id`':
                        $stmt->bindValue($identifier, $this->inscrit_id, PDO::PARAM_STR);
                        break;
                    case '`taux_production_france`':
                        $stmt->bindValue($identifier, $this->taux_production_france, PDO::PARAM_INT);
                        break;
                    case '`taux_production_europe`':
                        $stmt->bindValue($identifier, $this->taux_production_europe, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash !== null) {
                if (!$this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->getValidationFailures());
                }
            }

            if ($this->aCommonConsultation !== null) {
                if (!$this->aCommonConsultation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonConsultation->getValidationFailures());
                }
            }

            if ($this->aCommonInscrit !== null) {
                if (!$this->aCommonInscrit->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonInscrit->getValidationFailures());
                }
            }

            if ($this->aEntreprise !== null) {
                if (!$this->aEntreprise->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aEntreprise->getValidationFailures());
                }
            }

            if ($this->aCommonPlateformeVirtuelle !== null) {
                if (!$this->aCommonPlateformeVirtuelle->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonPlateformeVirtuelle->getValidationFailures());
                }
            }

            if ($this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse !== null) {
                if (!$this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->getValidationFailures());
                }
            }


            if (($retval = CommonOffresPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTCandidatures !== null) {
                    foreach ($this->collCommonTCandidatures as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTGroupementEntreprises !== null) {
                    foreach ($this->collCommonTGroupementEntreprises as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonOffresPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOrganisme();
                break;
            case 2:
                return $this->getConsultationRef();
                break;
            case 3:
                return $this->getEntrepriseId();
                break;
            case 4:
                return $this->getIdEtablissement();
                break;
            case 5:
                return $this->getOldInscritId();
                break;
            case 6:
                return $this->getSignatureenvxml();
                break;
            case 7:
                return $this->getHorodatage();
                break;
            case 8:
                return $this->getMailsignataire();
                break;
            case 9:
                return $this->getUntrusteddate();
                break;
            case 10:
                return $this->getUntrustedserial();
                break;
            case 11:
                return $this->getEnvoiComplet();
                break;
            case 12:
                return $this->getDateDepotDiffere();
                break;
            case 13:
                return $this->getHorodatageEnvoiDiffere();
                break;
            case 14:
                return $this->getSignatureenvxmlEnvoiDiffere();
                break;
            case 15:
                return $this->getExternalSerial();
                break;
            case 16:
                return $this->getInternalSerial();
                break;
            case 17:
                return $this->getUidOffre();
                break;
            case 18:
                return $this->getOffreSelectionnee();
                break;
            case 19:
                return $this->getObservation();
                break;
            case 20:
                return $this->getXmlString();
                break;
            case 21:
                return $this->getNomEntrepriseInscrit();
                break;
            case 22:
                return $this->getNomInscrit();
                break;
            case 23:
                return $this->getPrenomInscrit();
                break;
            case 24:
                return $this->getAdresseInscrit();
                break;
            case 25:
                return $this->getAdresse2Inscrit();
                break;
            case 26:
                return $this->getTelephoneInscrit();
                break;
            case 27:
                return $this->getFaxInscrit();
                break;
            case 28:
                return $this->getCodePostalInscrit();
                break;
            case 29:
                return $this->getVilleInscrit();
                break;
            case 30:
                return $this->getPaysInscrit();
                break;
            case 31:
                return $this->getAcronymePays();
                break;
            case 32:
                return $this->getSiretEntreprise();
                break;
            case 33:
                return $this->getIdentifiantNational();
                break;
            case 34:
                return $this->getEmailInscrit();
                break;
            case 35:
                return $this->getSiretInscrit();
                break;
            case 36:
                return $this->getNomEntreprise();
                break;
            case 37:
                return $this->getHorodatageAnnulation();
                break;
            case 38:
                return $this->getDateAnnulation();
                break;
            case 39:
                return $this->getSignatureAnnulation();
                break;
            case 40:
                return $this->getDepotAnnule();
                break;
            case 41:
                return $this->getStringAnnulation();
                break;
            case 42:
                return $this->getVerificationCertificatAnnulation();
                break;
            case 43:
                return $this->getOffreVariante();
                break;
            case 44:
                return $this->getReponsePasAPas();
                break;
            case 45:
                return $this->getNumeroReponse();
                break;
            case 46:
                return $this->getStatutOffres();
                break;
            case 47:
                return $this->getDateHeureOuverture();
                break;
            case 48:
                return $this->getAgentidOuverture();
                break;
            case 49:
                return $this->getAgentidOuverture2();
                break;
            case 50:
                return $this->getDateHeureOuvertureAgent2();
                break;
            case 51:
                return $this->getCryptageReponse();
                break;
            case 52:
                return $this->getNomAgentOuverture();
                break;
            case 53:
                return $this->getAgentTelechargementOffre();
                break;
            case 54:
                return $this->getDateTelechargementOffre();
                break;
            case 55:
                return $this->getRepertoireTelechargementOffre();
                break;
            case 56:
                return $this->getCandidatureIdExterne();
                break;
            case 57:
                return $this->getEtatChiffrement();
                break;
            case 58:
                return $this->getErreurChiffrement();
                break;
            case 59:
                return $this->getDateFinChiffrement();
                break;
            case 60:
                return $this->getDateHorodatage();
                break;
            case 61:
                return $this->getVerificationHotodatage();
                break;
            case 62:
                return $this->getVerificationSignatureOffre();
                break;
            case 63:
                return $this->getHorodatageHashFichiers();
                break;
            case 64:
                return $this->getIdPdfEchangeAccuse();
                break;
            case 65:
                return $this->getCreatedAt();
                break;
            case 66:
                return $this->getUidResponse();
                break;
            case 67:
                return $this->getDateDepot();
                break;
            case 68:
                return $this->getResultatVerificationHashAllFiles();
                break;
            case 69:
                return $this->getIdBlobHorodatageHash();
                break;
            case 70:
                return $this->getIdBlobXmlReponse();
                break;
            case 71:
                return $this->getConsultationId();
                break;
            case 72:
                return $this->getPlateformeVirtuelleId();
                break;
            case 73:
                return $this->getInscritId();
                break;
            case 74:
                return $this->getTauxProductionFrance();
                break;
            case 75:
                return $this->getTauxProductionEurope();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonOffres'][serialize($this->getPrimaryKey())])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonOffres'][serialize($this->getPrimaryKey())] = true;
        $keys = CommonOffresPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOrganisme(),
            $keys[2] => $this->getConsultationRef(),
            $keys[3] => $this->getEntrepriseId(),
            $keys[4] => $this->getIdEtablissement(),
            $keys[5] => $this->getOldInscritId(),
            $keys[6] => $this->getSignatureenvxml(),
            $keys[7] => $this->getHorodatage(),
            $keys[8] => $this->getMailsignataire(),
            $keys[9] => $this->getUntrusteddate(),
            $keys[10] => $this->getUntrustedserial(),
            $keys[11] => $this->getEnvoiComplet(),
            $keys[12] => $this->getDateDepotDiffere(),
            $keys[13] => $this->getHorodatageEnvoiDiffere(),
            $keys[14] => $this->getSignatureenvxmlEnvoiDiffere(),
            $keys[15] => $this->getExternalSerial(),
            $keys[16] => $this->getInternalSerial(),
            $keys[17] => $this->getUidOffre(),
            $keys[18] => $this->getOffreSelectionnee(),
            $keys[19] => $this->getObservation(),
            $keys[20] => $this->getXmlString(),
            $keys[21] => $this->getNomEntrepriseInscrit(),
            $keys[22] => $this->getNomInscrit(),
            $keys[23] => $this->getPrenomInscrit(),
            $keys[24] => $this->getAdresseInscrit(),
            $keys[25] => $this->getAdresse2Inscrit(),
            $keys[26] => $this->getTelephoneInscrit(),
            $keys[27] => $this->getFaxInscrit(),
            $keys[28] => $this->getCodePostalInscrit(),
            $keys[29] => $this->getVilleInscrit(),
            $keys[30] => $this->getPaysInscrit(),
            $keys[31] => $this->getAcronymePays(),
            $keys[32] => $this->getSiretEntreprise(),
            $keys[33] => $this->getIdentifiantNational(),
            $keys[34] => $this->getEmailInscrit(),
            $keys[35] => $this->getSiretInscrit(),
            $keys[36] => $this->getNomEntreprise(),
            $keys[37] => $this->getHorodatageAnnulation(),
            $keys[38] => $this->getDateAnnulation(),
            $keys[39] => $this->getSignatureAnnulation(),
            $keys[40] => $this->getDepotAnnule(),
            $keys[41] => $this->getStringAnnulation(),
            $keys[42] => $this->getVerificationCertificatAnnulation(),
            $keys[43] => $this->getOffreVariante(),
            $keys[44] => $this->getReponsePasAPas(),
            $keys[45] => $this->getNumeroReponse(),
            $keys[46] => $this->getStatutOffres(),
            $keys[47] => $this->getDateHeureOuverture(),
            $keys[48] => $this->getAgentidOuverture(),
            $keys[49] => $this->getAgentidOuverture2(),
            $keys[50] => $this->getDateHeureOuvertureAgent2(),
            $keys[51] => $this->getCryptageReponse(),
            $keys[52] => $this->getNomAgentOuverture(),
            $keys[53] => $this->getAgentTelechargementOffre(),
            $keys[54] => $this->getDateTelechargementOffre(),
            $keys[55] => $this->getRepertoireTelechargementOffre(),
            $keys[56] => $this->getCandidatureIdExterne(),
            $keys[57] => $this->getEtatChiffrement(),
            $keys[58] => $this->getErreurChiffrement(),
            $keys[59] => $this->getDateFinChiffrement(),
            $keys[60] => $this->getDateHorodatage(),
            $keys[61] => $this->getVerificationHotodatage(),
            $keys[62] => $this->getVerificationSignatureOffre(),
            $keys[63] => $this->getHorodatageHashFichiers(),
            $keys[64] => $this->getIdPdfEchangeAccuse(),
            $keys[65] => $this->getCreatedAt(),
            $keys[66] => $this->getUidResponse(),
            $keys[67] => $this->getDateDepot(),
            $keys[68] => $this->getResultatVerificationHashAllFiles(),
            $keys[69] => $this->getIdBlobHorodatageHash(),
            $keys[70] => $this->getIdBlobXmlReponse(),
            $keys[71] => $this->getConsultationId(),
            $keys[72] => $this->getPlateformeVirtuelleId(),
            $keys[73] => $this->getInscritId(),
            $keys[74] => $this->getTauxProductionFrance(),
            $keys[75] => $this->getTauxProductionEurope(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash) {
                $result['CommonBlobOrganismeFileRelatedByIdBlobHorodatageHash'] = $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonConsultation) {
                $result['CommonConsultation'] = $this->aCommonConsultation->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonInscrit) {
                $result['CommonInscrit'] = $this->aCommonInscrit->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aEntreprise) {
                $result['Entreprise'] = $this->aEntreprise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonPlateformeVirtuelle) {
                $result['CommonPlateformeVirtuelle'] = $this->aCommonPlateformeVirtuelle->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse) {
                $result['CommonBlobOrganismeFileRelatedByIdBlobXmlReponse'] = $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTCandidatures) {
                $result['CommonTCandidatures'] = $this->collCommonTCandidatures->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTGroupementEntreprises) {
                $result['CommonTGroupementEntreprises'] = $this->collCommonTGroupementEntreprises->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonOffresPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOrganisme($value);
                break;
            case 2:
                $this->setConsultationRef($value);
                break;
            case 3:
                $this->setEntrepriseId($value);
                break;
            case 4:
                $this->setIdEtablissement($value);
                break;
            case 5:
                $this->setOldInscritId($value);
                break;
            case 6:
                $this->setSignatureenvxml($value);
                break;
            case 7:
                $this->setHorodatage($value);
                break;
            case 8:
                $this->setMailsignataire($value);
                break;
            case 9:
                $this->setUntrusteddate($value);
                break;
            case 10:
                $this->setUntrustedserial($value);
                break;
            case 11:
                $this->setEnvoiComplet($value);
                break;
            case 12:
                $this->setDateDepotDiffere($value);
                break;
            case 13:
                $this->setHorodatageEnvoiDiffere($value);
                break;
            case 14:
                $this->setSignatureenvxmlEnvoiDiffere($value);
                break;
            case 15:
                $this->setExternalSerial($value);
                break;
            case 16:
                $this->setInternalSerial($value);
                break;
            case 17:
                $this->setUidOffre($value);
                break;
            case 18:
                $this->setOffreSelectionnee($value);
                break;
            case 19:
                $this->setObservation($value);
                break;
            case 20:
                $this->setXmlString($value);
                break;
            case 21:
                $this->setNomEntrepriseInscrit($value);
                break;
            case 22:
                $this->setNomInscrit($value);
                break;
            case 23:
                $this->setPrenomInscrit($value);
                break;
            case 24:
                $this->setAdresseInscrit($value);
                break;
            case 25:
                $this->setAdresse2Inscrit($value);
                break;
            case 26:
                $this->setTelephoneInscrit($value);
                break;
            case 27:
                $this->setFaxInscrit($value);
                break;
            case 28:
                $this->setCodePostalInscrit($value);
                break;
            case 29:
                $this->setVilleInscrit($value);
                break;
            case 30:
                $this->setPaysInscrit($value);
                break;
            case 31:
                $this->setAcronymePays($value);
                break;
            case 32:
                $this->setSiretEntreprise($value);
                break;
            case 33:
                $this->setIdentifiantNational($value);
                break;
            case 34:
                $this->setEmailInscrit($value);
                break;
            case 35:
                $this->setSiretInscrit($value);
                break;
            case 36:
                $this->setNomEntreprise($value);
                break;
            case 37:
                $this->setHorodatageAnnulation($value);
                break;
            case 38:
                $this->setDateAnnulation($value);
                break;
            case 39:
                $this->setSignatureAnnulation($value);
                break;
            case 40:
                $this->setDepotAnnule($value);
                break;
            case 41:
                $this->setStringAnnulation($value);
                break;
            case 42:
                $this->setVerificationCertificatAnnulation($value);
                break;
            case 43:
                $this->setOffreVariante($value);
                break;
            case 44:
                $this->setReponsePasAPas($value);
                break;
            case 45:
                $this->setNumeroReponse($value);
                break;
            case 46:
                $this->setStatutOffres($value);
                break;
            case 47:
                $this->setDateHeureOuverture($value);
                break;
            case 48:
                $this->setAgentidOuverture($value);
                break;
            case 49:
                $this->setAgentidOuverture2($value);
                break;
            case 50:
                $this->setDateHeureOuvertureAgent2($value);
                break;
            case 51:
                $this->setCryptageReponse($value);
                break;
            case 52:
                $this->setNomAgentOuverture($value);
                break;
            case 53:
                $this->setAgentTelechargementOffre($value);
                break;
            case 54:
                $this->setDateTelechargementOffre($value);
                break;
            case 55:
                $this->setRepertoireTelechargementOffre($value);
                break;
            case 56:
                $this->setCandidatureIdExterne($value);
                break;
            case 57:
                $this->setEtatChiffrement($value);
                break;
            case 58:
                $this->setErreurChiffrement($value);
                break;
            case 59:
                $this->setDateFinChiffrement($value);
                break;
            case 60:
                $this->setDateHorodatage($value);
                break;
            case 61:
                $this->setVerificationHotodatage($value);
                break;
            case 62:
                $this->setVerificationSignatureOffre($value);
                break;
            case 63:
                $this->setHorodatageHashFichiers($value);
                break;
            case 64:
                $this->setIdPdfEchangeAccuse($value);
                break;
            case 65:
                $this->setCreatedAt($value);
                break;
            case 66:
                $this->setUidResponse($value);
                break;
            case 67:
                $this->setDateDepot($value);
                break;
            case 68:
                $this->setResultatVerificationHashAllFiles($value);
                break;
            case 69:
                $this->setIdBlobHorodatageHash($value);
                break;
            case 70:
                $this->setIdBlobXmlReponse($value);
                break;
            case 71:
                $this->setConsultationId($value);
                break;
            case 72:
                $this->setPlateformeVirtuelleId($value);
                break;
            case 73:
                $this->setInscritId($value);
                break;
            case 74:
                $this->setTauxProductionFrance($value);
                break;
            case 75:
                $this->setTauxProductionEurope($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonOffresPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOrganisme($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setConsultationRef($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setEntrepriseId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdEtablissement($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setOldInscritId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setSignatureenvxml($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setHorodatage($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setMailsignataire($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setUntrusteddate($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUntrustedserial($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setEnvoiComplet($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setDateDepotDiffere($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setHorodatageEnvoiDiffere($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setSignatureenvxmlEnvoiDiffere($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setExternalSerial($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setInternalSerial($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setUidOffre($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setOffreSelectionnee($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setObservation($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setXmlString($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setNomEntrepriseInscrit($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setNomInscrit($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setPrenomInscrit($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setAdresseInscrit($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setAdresse2Inscrit($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setTelephoneInscrit($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setFaxInscrit($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setCodePostalInscrit($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setVilleInscrit($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setPaysInscrit($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setAcronymePays($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setSiretEntreprise($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setIdentifiantNational($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setEmailInscrit($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setSiretInscrit($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setNomEntreprise($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setHorodatageAnnulation($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setDateAnnulation($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setSignatureAnnulation($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setDepotAnnule($arr[$keys[40]]);
        if (array_key_exists($keys[41], $arr)) $this->setStringAnnulation($arr[$keys[41]]);
        if (array_key_exists($keys[42], $arr)) $this->setVerificationCertificatAnnulation($arr[$keys[42]]);
        if (array_key_exists($keys[43], $arr)) $this->setOffreVariante($arr[$keys[43]]);
        if (array_key_exists($keys[44], $arr)) $this->setReponsePasAPas($arr[$keys[44]]);
        if (array_key_exists($keys[45], $arr)) $this->setNumeroReponse($arr[$keys[45]]);
        if (array_key_exists($keys[46], $arr)) $this->setStatutOffres($arr[$keys[46]]);
        if (array_key_exists($keys[47], $arr)) $this->setDateHeureOuverture($arr[$keys[47]]);
        if (array_key_exists($keys[48], $arr)) $this->setAgentidOuverture($arr[$keys[48]]);
        if (array_key_exists($keys[49], $arr)) $this->setAgentidOuverture2($arr[$keys[49]]);
        if (array_key_exists($keys[50], $arr)) $this->setDateHeureOuvertureAgent2($arr[$keys[50]]);
        if (array_key_exists($keys[51], $arr)) $this->setCryptageReponse($arr[$keys[51]]);
        if (array_key_exists($keys[52], $arr)) $this->setNomAgentOuverture($arr[$keys[52]]);
        if (array_key_exists($keys[53], $arr)) $this->setAgentTelechargementOffre($arr[$keys[53]]);
        if (array_key_exists($keys[54], $arr)) $this->setDateTelechargementOffre($arr[$keys[54]]);
        if (array_key_exists($keys[55], $arr)) $this->setRepertoireTelechargementOffre($arr[$keys[55]]);
        if (array_key_exists($keys[56], $arr)) $this->setCandidatureIdExterne($arr[$keys[56]]);
        if (array_key_exists($keys[57], $arr)) $this->setEtatChiffrement($arr[$keys[57]]);
        if (array_key_exists($keys[58], $arr)) $this->setErreurChiffrement($arr[$keys[58]]);
        if (array_key_exists($keys[59], $arr)) $this->setDateFinChiffrement($arr[$keys[59]]);
        if (array_key_exists($keys[60], $arr)) $this->setDateHorodatage($arr[$keys[60]]);
        if (array_key_exists($keys[61], $arr)) $this->setVerificationHotodatage($arr[$keys[61]]);
        if (array_key_exists($keys[62], $arr)) $this->setVerificationSignatureOffre($arr[$keys[62]]);
        if (array_key_exists($keys[63], $arr)) $this->setHorodatageHashFichiers($arr[$keys[63]]);
        if (array_key_exists($keys[64], $arr)) $this->setIdPdfEchangeAccuse($arr[$keys[64]]);
        if (array_key_exists($keys[65], $arr)) $this->setCreatedAt($arr[$keys[65]]);
        if (array_key_exists($keys[66], $arr)) $this->setUidResponse($arr[$keys[66]]);
        if (array_key_exists($keys[67], $arr)) $this->setDateDepot($arr[$keys[67]]);
        if (array_key_exists($keys[68], $arr)) $this->setResultatVerificationHashAllFiles($arr[$keys[68]]);
        if (array_key_exists($keys[69], $arr)) $this->setIdBlobHorodatageHash($arr[$keys[69]]);
        if (array_key_exists($keys[70], $arr)) $this->setIdBlobXmlReponse($arr[$keys[70]]);
        if (array_key_exists($keys[71], $arr)) $this->setConsultationId($arr[$keys[71]]);
        if (array_key_exists($keys[72], $arr)) $this->setPlateformeVirtuelleId($arr[$keys[72]]);
        if (array_key_exists($keys[73], $arr)) $this->setInscritId($arr[$keys[73]]);
        if (array_key_exists($keys[74], $arr)) $this->setTauxProductionFrance($arr[$keys[74]]);
        if (array_key_exists($keys[75], $arr)) $this->setTauxProductionEurope($arr[$keys[75]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonOffresPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonOffresPeer::ID)) $criteria->add(CommonOffresPeer::ID, $this->id);
        if ($this->isColumnModified(CommonOffresPeer::ORGANISME)) $criteria->add(CommonOffresPeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonOffresPeer::CONSULTATION_REF)) $criteria->add(CommonOffresPeer::CONSULTATION_REF, $this->consultation_ref);
        if ($this->isColumnModified(CommonOffresPeer::ENTREPRISE_ID)) $criteria->add(CommonOffresPeer::ENTREPRISE_ID, $this->entreprise_id);
        if ($this->isColumnModified(CommonOffresPeer::ID_ETABLISSEMENT)) $criteria->add(CommonOffresPeer::ID_ETABLISSEMENT, $this->id_etablissement);
        if ($this->isColumnModified(CommonOffresPeer::OLD_INSCRIT_ID)) $criteria->add(CommonOffresPeer::OLD_INSCRIT_ID, $this->old_inscrit_id);
        if ($this->isColumnModified(CommonOffresPeer::SIGNATUREENVXML)) $criteria->add(CommonOffresPeer::SIGNATUREENVXML, $this->signatureenvxml);
        if ($this->isColumnModified(CommonOffresPeer::HORODATAGE)) $criteria->add(CommonOffresPeer::HORODATAGE, $this->horodatage);
        if ($this->isColumnModified(CommonOffresPeer::MAILSIGNATAIRE)) $criteria->add(CommonOffresPeer::MAILSIGNATAIRE, $this->mailsignataire);
        if ($this->isColumnModified(CommonOffresPeer::UNTRUSTEDDATE)) $criteria->add(CommonOffresPeer::UNTRUSTEDDATE, $this->untrusteddate);
        if ($this->isColumnModified(CommonOffresPeer::UNTRUSTEDSERIAL)) $criteria->add(CommonOffresPeer::UNTRUSTEDSERIAL, $this->untrustedserial);
        if ($this->isColumnModified(CommonOffresPeer::ENVOI_COMPLET)) $criteria->add(CommonOffresPeer::ENVOI_COMPLET, $this->envoi_complet);
        if ($this->isColumnModified(CommonOffresPeer::DATE_DEPOT_DIFFERE)) $criteria->add(CommonOffresPeer::DATE_DEPOT_DIFFERE, $this->date_depot_differe);
        if ($this->isColumnModified(CommonOffresPeer::HORODATAGE_ENVOI_DIFFERE)) $criteria->add(CommonOffresPeer::HORODATAGE_ENVOI_DIFFERE, $this->horodatage_envoi_differe);
        if ($this->isColumnModified(CommonOffresPeer::SIGNATUREENVXML_ENVOI_DIFFERE)) $criteria->add(CommonOffresPeer::SIGNATUREENVXML_ENVOI_DIFFERE, $this->signatureenvxml_envoi_differe);
        if ($this->isColumnModified(CommonOffresPeer::EXTERNAL_SERIAL)) $criteria->add(CommonOffresPeer::EXTERNAL_SERIAL, $this->external_serial);
        if ($this->isColumnModified(CommonOffresPeer::INTERNAL_SERIAL)) $criteria->add(CommonOffresPeer::INTERNAL_SERIAL, $this->internal_serial);
        if ($this->isColumnModified(CommonOffresPeer::UID_OFFRE)) $criteria->add(CommonOffresPeer::UID_OFFRE, $this->uid_offre);
        if ($this->isColumnModified(CommonOffresPeer::OFFRE_SELECTIONNEE)) $criteria->add(CommonOffresPeer::OFFRE_SELECTIONNEE, $this->offre_selectionnee);
        if ($this->isColumnModified(CommonOffresPeer::OBSERVATION)) $criteria->add(CommonOffresPeer::OBSERVATION, $this->observation);
        if ($this->isColumnModified(CommonOffresPeer::XML_STRING)) $criteria->add(CommonOffresPeer::XML_STRING, $this->xml_string);
        if ($this->isColumnModified(CommonOffresPeer::NOM_ENTREPRISE_INSCRIT)) $criteria->add(CommonOffresPeer::NOM_ENTREPRISE_INSCRIT, $this->nom_entreprise_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::NOM_INSCRIT)) $criteria->add(CommonOffresPeer::NOM_INSCRIT, $this->nom_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::PRENOM_INSCRIT)) $criteria->add(CommonOffresPeer::PRENOM_INSCRIT, $this->prenom_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::ADRESSE_INSCRIT)) $criteria->add(CommonOffresPeer::ADRESSE_INSCRIT, $this->adresse_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::ADRESSE2_INSCRIT)) $criteria->add(CommonOffresPeer::ADRESSE2_INSCRIT, $this->adresse2_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::TELEPHONE_INSCRIT)) $criteria->add(CommonOffresPeer::TELEPHONE_INSCRIT, $this->telephone_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::FAX_INSCRIT)) $criteria->add(CommonOffresPeer::FAX_INSCRIT, $this->fax_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::CODE_POSTAL_INSCRIT)) $criteria->add(CommonOffresPeer::CODE_POSTAL_INSCRIT, $this->code_postal_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::VILLE_INSCRIT)) $criteria->add(CommonOffresPeer::VILLE_INSCRIT, $this->ville_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::PAYS_INSCRIT)) $criteria->add(CommonOffresPeer::PAYS_INSCRIT, $this->pays_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::ACRONYME_PAYS)) $criteria->add(CommonOffresPeer::ACRONYME_PAYS, $this->acronyme_pays);
        if ($this->isColumnModified(CommonOffresPeer::SIRET_ENTREPRISE)) $criteria->add(CommonOffresPeer::SIRET_ENTREPRISE, $this->siret_entreprise);
        if ($this->isColumnModified(CommonOffresPeer::IDENTIFIANT_NATIONAL)) $criteria->add(CommonOffresPeer::IDENTIFIANT_NATIONAL, $this->identifiant_national);
        if ($this->isColumnModified(CommonOffresPeer::EMAIL_INSCRIT)) $criteria->add(CommonOffresPeer::EMAIL_INSCRIT, $this->email_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::SIRET_INSCRIT)) $criteria->add(CommonOffresPeer::SIRET_INSCRIT, $this->siret_inscrit);
        if ($this->isColumnModified(CommonOffresPeer::NOM_ENTREPRISE)) $criteria->add(CommonOffresPeer::NOM_ENTREPRISE, $this->nom_entreprise);
        if ($this->isColumnModified(CommonOffresPeer::HORODATAGE_ANNULATION)) $criteria->add(CommonOffresPeer::HORODATAGE_ANNULATION, $this->horodatage_annulation);
        if ($this->isColumnModified(CommonOffresPeer::DATE_ANNULATION)) $criteria->add(CommonOffresPeer::DATE_ANNULATION, $this->date_annulation);
        if ($this->isColumnModified(CommonOffresPeer::SIGNATURE_ANNULATION)) $criteria->add(CommonOffresPeer::SIGNATURE_ANNULATION, $this->signature_annulation);
        if ($this->isColumnModified(CommonOffresPeer::DEPOT_ANNULE)) $criteria->add(CommonOffresPeer::DEPOT_ANNULE, $this->depot_annule);
        if ($this->isColumnModified(CommonOffresPeer::STRING_ANNULATION)) $criteria->add(CommonOffresPeer::STRING_ANNULATION, $this->string_annulation);
        if ($this->isColumnModified(CommonOffresPeer::VERIFICATION_CERTIFICAT_ANNULATION)) $criteria->add(CommonOffresPeer::VERIFICATION_CERTIFICAT_ANNULATION, $this->verification_certificat_annulation);
        if ($this->isColumnModified(CommonOffresPeer::OFFRE_VARIANTE)) $criteria->add(CommonOffresPeer::OFFRE_VARIANTE, $this->offre_variante);
        if ($this->isColumnModified(CommonOffresPeer::REPONSE_PAS_A_PAS)) $criteria->add(CommonOffresPeer::REPONSE_PAS_A_PAS, $this->reponse_pas_a_pas);
        if ($this->isColumnModified(CommonOffresPeer::NUMERO_REPONSE)) $criteria->add(CommonOffresPeer::NUMERO_REPONSE, $this->numero_reponse);
        if ($this->isColumnModified(CommonOffresPeer::STATUT_OFFRES)) $criteria->add(CommonOffresPeer::STATUT_OFFRES, $this->statut_offres);
        if ($this->isColumnModified(CommonOffresPeer::DATE_HEURE_OUVERTURE)) $criteria->add(CommonOffresPeer::DATE_HEURE_OUVERTURE, $this->date_heure_ouverture);
        if ($this->isColumnModified(CommonOffresPeer::AGENTID_OUVERTURE)) $criteria->add(CommonOffresPeer::AGENTID_OUVERTURE, $this->agentid_ouverture);
        if ($this->isColumnModified(CommonOffresPeer::AGENTID_OUVERTURE2)) $criteria->add(CommonOffresPeer::AGENTID_OUVERTURE2, $this->agentid_ouverture2);
        if ($this->isColumnModified(CommonOffresPeer::DATE_HEURE_OUVERTURE_AGENT2)) $criteria->add(CommonOffresPeer::DATE_HEURE_OUVERTURE_AGENT2, $this->date_heure_ouverture_agent2);
        if ($this->isColumnModified(CommonOffresPeer::CRYPTAGE_REPONSE)) $criteria->add(CommonOffresPeer::CRYPTAGE_REPONSE, $this->cryptage_reponse);
        if ($this->isColumnModified(CommonOffresPeer::NOM_AGENT_OUVERTURE)) $criteria->add(CommonOffresPeer::NOM_AGENT_OUVERTURE, $this->nom_agent_ouverture);
        if ($this->isColumnModified(CommonOffresPeer::AGENT_TELECHARGEMENT_OFFRE)) $criteria->add(CommonOffresPeer::AGENT_TELECHARGEMENT_OFFRE, $this->agent_telechargement_offre);
        if ($this->isColumnModified(CommonOffresPeer::DATE_TELECHARGEMENT_OFFRE)) $criteria->add(CommonOffresPeer::DATE_TELECHARGEMENT_OFFRE, $this->date_telechargement_offre);
        if ($this->isColumnModified(CommonOffresPeer::REPERTOIRE_TELECHARGEMENT_OFFRE)) $criteria->add(CommonOffresPeer::REPERTOIRE_TELECHARGEMENT_OFFRE, $this->repertoire_telechargement_offre);
        if ($this->isColumnModified(CommonOffresPeer::CANDIDATURE_ID_EXTERNE)) $criteria->add(CommonOffresPeer::CANDIDATURE_ID_EXTERNE, $this->candidature_id_externe);
        if ($this->isColumnModified(CommonOffresPeer::ETAT_CHIFFREMENT)) $criteria->add(CommonOffresPeer::ETAT_CHIFFREMENT, $this->etat_chiffrement);
        if ($this->isColumnModified(CommonOffresPeer::ERREUR_CHIFFREMENT)) $criteria->add(CommonOffresPeer::ERREUR_CHIFFREMENT, $this->erreur_chiffrement);
        if ($this->isColumnModified(CommonOffresPeer::DATE_FIN_CHIFFREMENT)) $criteria->add(CommonOffresPeer::DATE_FIN_CHIFFREMENT, $this->date_fin_chiffrement);
        if ($this->isColumnModified(CommonOffresPeer::DATE_HORODATAGE)) $criteria->add(CommonOffresPeer::DATE_HORODATAGE, $this->date_horodatage);
        if ($this->isColumnModified(CommonOffresPeer::VERIFICATION_HOTODATAGE)) $criteria->add(CommonOffresPeer::VERIFICATION_HOTODATAGE, $this->verification_hotodatage);
        if ($this->isColumnModified(CommonOffresPeer::VERIFICATION_SIGNATURE_OFFRE)) $criteria->add(CommonOffresPeer::VERIFICATION_SIGNATURE_OFFRE, $this->verification_signature_offre);
        if ($this->isColumnModified(CommonOffresPeer::HORODATAGE_HASH_FICHIERS)) $criteria->add(CommonOffresPeer::HORODATAGE_HASH_FICHIERS, $this->horodatage_hash_fichiers);
        if ($this->isColumnModified(CommonOffresPeer::ID_PDF_ECHANGE_ACCUSE)) $criteria->add(CommonOffresPeer::ID_PDF_ECHANGE_ACCUSE, $this->id_pdf_echange_accuse);
        if ($this->isColumnModified(CommonOffresPeer::CREATED_AT)) $criteria->add(CommonOffresPeer::CREATED_AT, $this->created_at);
        if ($this->isColumnModified(CommonOffresPeer::UID_RESPONSE)) $criteria->add(CommonOffresPeer::UID_RESPONSE, $this->uid_response);
        if ($this->isColumnModified(CommonOffresPeer::DATE_DEPOT)) $criteria->add(CommonOffresPeer::DATE_DEPOT, $this->date_depot);
        if ($this->isColumnModified(CommonOffresPeer::RESULTAT_VERIFICATION_HASH_ALL_FILES)) $criteria->add(CommonOffresPeer::RESULTAT_VERIFICATION_HASH_ALL_FILES, $this->resultat_verification_hash_all_files);
        if ($this->isColumnModified(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH)) $criteria->add(CommonOffresPeer::ID_BLOB_HORODATAGE_HASH, $this->id_blob_horodatage_hash);
        if ($this->isColumnModified(CommonOffresPeer::ID_BLOB_XML_REPONSE)) $criteria->add(CommonOffresPeer::ID_BLOB_XML_REPONSE, $this->id_blob_xml_reponse);
        if ($this->isColumnModified(CommonOffresPeer::CONSULTATION_ID)) $criteria->add(CommonOffresPeer::CONSULTATION_ID, $this->consultation_id);
        if ($this->isColumnModified(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID)) $criteria->add(CommonOffresPeer::PLATEFORME_VIRTUELLE_ID, $this->plateforme_virtuelle_id);
        if ($this->isColumnModified(CommonOffresPeer::INSCRIT_ID)) $criteria->add(CommonOffresPeer::INSCRIT_ID, $this->inscrit_id);
        if ($this->isColumnModified(CommonOffresPeer::TAUX_PRODUCTION_FRANCE)) $criteria->add(CommonOffresPeer::TAUX_PRODUCTION_FRANCE, $this->taux_production_france);
        if ($this->isColumnModified(CommonOffresPeer::TAUX_PRODUCTION_EUROPE)) $criteria->add(CommonOffresPeer::TAUX_PRODUCTION_EUROPE, $this->taux_production_europe);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonOffresPeer::DATABASE_NAME);
        $criteria->add(CommonOffresPeer::ID, $this->id);
        $criteria->add(CommonOffresPeer::ORGANISME, $this->organisme);

        return $criteria;
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getId();
        $pks[1] = $this->getOrganisme();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setId($keys[0]);
        $this->setOrganisme($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return (null === $this->getId()) && (null === $this->getOrganisme());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonOffres (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setConsultationRef($this->getConsultationRef());
        $copyObj->setEntrepriseId($this->getEntrepriseId());
        $copyObj->setIdEtablissement($this->getIdEtablissement());
        $copyObj->setOldInscritId($this->getOldInscritId());
        $copyObj->setSignatureenvxml($this->getSignatureenvxml());
        $copyObj->setHorodatage($this->getHorodatage());
        $copyObj->setMailsignataire($this->getMailsignataire());
        $copyObj->setUntrusteddate($this->getUntrusteddate());
        $copyObj->setUntrustedserial($this->getUntrustedserial());
        $copyObj->setEnvoiComplet($this->getEnvoiComplet());
        $copyObj->setDateDepotDiffere($this->getDateDepotDiffere());
        $copyObj->setHorodatageEnvoiDiffere($this->getHorodatageEnvoiDiffere());
        $copyObj->setSignatureenvxmlEnvoiDiffere($this->getSignatureenvxmlEnvoiDiffere());
        $copyObj->setExternalSerial($this->getExternalSerial());
        $copyObj->setInternalSerial($this->getInternalSerial());
        $copyObj->setUidOffre($this->getUidOffre());
        $copyObj->setOffreSelectionnee($this->getOffreSelectionnee());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setXmlString($this->getXmlString());
        $copyObj->setNomEntrepriseInscrit($this->getNomEntrepriseInscrit());
        $copyObj->setNomInscrit($this->getNomInscrit());
        $copyObj->setPrenomInscrit($this->getPrenomInscrit());
        $copyObj->setAdresseInscrit($this->getAdresseInscrit());
        $copyObj->setAdresse2Inscrit($this->getAdresse2Inscrit());
        $copyObj->setTelephoneInscrit($this->getTelephoneInscrit());
        $copyObj->setFaxInscrit($this->getFaxInscrit());
        $copyObj->setCodePostalInscrit($this->getCodePostalInscrit());
        $copyObj->setVilleInscrit($this->getVilleInscrit());
        $copyObj->setPaysInscrit($this->getPaysInscrit());
        $copyObj->setAcronymePays($this->getAcronymePays());
        $copyObj->setSiretEntreprise($this->getSiretEntreprise());
        $copyObj->setIdentifiantNational($this->getIdentifiantNational());
        $copyObj->setEmailInscrit($this->getEmailInscrit());
        $copyObj->setSiretInscrit($this->getSiretInscrit());
        $copyObj->setNomEntreprise($this->getNomEntreprise());
        $copyObj->setHorodatageAnnulation($this->getHorodatageAnnulation());
        $copyObj->setDateAnnulation($this->getDateAnnulation());
        $copyObj->setSignatureAnnulation($this->getSignatureAnnulation());
        $copyObj->setDepotAnnule($this->getDepotAnnule());
        $copyObj->setStringAnnulation($this->getStringAnnulation());
        $copyObj->setVerificationCertificatAnnulation($this->getVerificationCertificatAnnulation());
        $copyObj->setOffreVariante($this->getOffreVariante());
        $copyObj->setReponsePasAPas($this->getReponsePasAPas());
        $copyObj->setNumeroReponse($this->getNumeroReponse());
        $copyObj->setStatutOffres($this->getStatutOffres());
        $copyObj->setDateHeureOuverture($this->getDateHeureOuverture());
        $copyObj->setAgentidOuverture($this->getAgentidOuverture());
        $copyObj->setAgentidOuverture2($this->getAgentidOuverture2());
        $copyObj->setDateHeureOuvertureAgent2($this->getDateHeureOuvertureAgent2());
        $copyObj->setCryptageReponse($this->getCryptageReponse());
        $copyObj->setNomAgentOuverture($this->getNomAgentOuverture());
        $copyObj->setAgentTelechargementOffre($this->getAgentTelechargementOffre());
        $copyObj->setDateTelechargementOffre($this->getDateTelechargementOffre());
        $copyObj->setRepertoireTelechargementOffre($this->getRepertoireTelechargementOffre());
        $copyObj->setCandidatureIdExterne($this->getCandidatureIdExterne());
        $copyObj->setEtatChiffrement($this->getEtatChiffrement());
        $copyObj->setErreurChiffrement($this->getErreurChiffrement());
        $copyObj->setDateFinChiffrement($this->getDateFinChiffrement());
        $copyObj->setDateHorodatage($this->getDateHorodatage());
        $copyObj->setVerificationHotodatage($this->getVerificationHotodatage());
        $copyObj->setVerificationSignatureOffre($this->getVerificationSignatureOffre());
        $copyObj->setHorodatageHashFichiers($this->getHorodatageHashFichiers());
        $copyObj->setIdPdfEchangeAccuse($this->getIdPdfEchangeAccuse());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUidResponse($this->getUidResponse());
        $copyObj->setDateDepot($this->getDateDepot());
        $copyObj->setResultatVerificationHashAllFiles($this->getResultatVerificationHashAllFiles());
        $copyObj->setIdBlobHorodatageHash($this->getIdBlobHorodatageHash());
        $copyObj->setIdBlobXmlReponse($this->getIdBlobXmlReponse());
        $copyObj->setConsultationId($this->getConsultationId());
        $copyObj->setPlateformeVirtuelleId($this->getPlateformeVirtuelleId());
        $copyObj->setInscritId($this->getInscritId());
        $copyObj->setTauxProductionFrance($this->getTauxProductionFrance());
        $copyObj->setTauxProductionEurope($this->getTauxProductionEurope());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTCandidatures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCandidature($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTGroupementEntreprises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTGroupementEntreprise($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonOffres Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonOffresPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonOffresPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonBlobOrganismeFile object.
     *
     * @param   CommonBlobOrganismeFile $v
     * @return CommonOffres The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash(CommonBlobOrganismeFile $v = null)
    {
        if ($v === null) {
            $this->setIdBlobHorodatageHash(NULL);
        } else {
            $this->setIdBlobHorodatageHash($v->getId());
        }

        $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonBlobOrganismeFile object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonOffresRelatedByIdBlobHorodatageHash($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonBlobOrganismeFile object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonBlobOrganismeFile The associated CommonBlobOrganismeFile object.
     * @throws PropelException
     */
    public function getCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash === null && ($this->id_blob_horodatage_hash !== null) && $doQuery) {
            $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash = CommonBlobOrganismeFileQuery::create()->findPk($this->id_blob_horodatage_hash, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->addCommonOffressRelatedByIdBlobHorodatageHash($this);
             */
        }

        return $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash;
    }

    /**
     * Declares an association between this object and a CommonConsultation object.
     *
     * @param   CommonConsultation $v
     * @return CommonOffres The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonConsultation(CommonConsultation $v = null)
    {
        if ($v === null) {
            $this->setConsultationId(NULL);
        } else {
            $this->setConsultationId($v->getId());
        }

        $this->aCommonConsultation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonConsultation object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonOffres($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonConsultation object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonConsultation The associated CommonConsultation object.
     * @throws PropelException
     */
    public function getCommonConsultation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonConsultation === null && ($this->consultation_id !== null) && $doQuery) {
            $this->aCommonConsultation = CommonConsultationQuery::create()->findPk($this->consultation_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonConsultation->addCommonOffress($this);
             */
        }

        return $this->aCommonConsultation;
    }

    /**
     * Declares an association between this object and a CommonInscrit object.
     *
     * @param   CommonInscrit $v
     * @return CommonOffres The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonInscrit(CommonInscrit $v = null)
    {
        if ($v === null) {
            $this->setInscritId(NULL);
        } else {
            $this->setInscritId($v->getId());
        }

        $this->aCommonInscrit = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonInscrit object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonOffres($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonInscrit object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonInscrit The associated CommonInscrit object.
     * @throws PropelException
     */
    public function getCommonInscrit(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonInscrit === null && (($this->inscrit_id !== "" && $this->inscrit_id !== null)) && $doQuery) {
            $this->aCommonInscrit = CommonInscritQuery::create()->findPk($this->inscrit_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonInscrit->addCommonOffress($this);
             */
        }

        return $this->aCommonInscrit;
    }

    /**
     * Declares an association between this object and a Entreprise object.
     *
     * @param   Entreprise $v
     * @return CommonOffres The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntreprise(Entreprise $v = null)
    {
        if ($v === null) {
            $this->setEntrepriseId(0);
        } else {
            $this->setEntrepriseId($v->getId());
        }

        $this->aEntreprise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Entreprise object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonOffres($this);
        }


        return $this;
    }


    /**
     * Get the associated Entreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Entreprise The associated Entreprise object.
     * @throws PropelException
     */
    public function getEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aEntreprise === null && ($this->entreprise_id !== null) && $doQuery) {
            $this->aEntreprise = EntrepriseQuery::create()->findPk($this->entreprise_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntreprise->addCommonOffress($this);
             */
        }

        return $this->aEntreprise;
    }

    /**
     * Declares an association between this object and a CommonPlateformeVirtuelle object.
     *
     * @param   CommonPlateformeVirtuelle $v
     * @return CommonOffres The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonPlateformeVirtuelle(CommonPlateformeVirtuelle $v = null)
    {
        if ($v === null) {
            $this->setPlateformeVirtuelleId(NULL);
        } else {
            $this->setPlateformeVirtuelleId($v->getId());
        }

        $this->aCommonPlateformeVirtuelle = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonPlateformeVirtuelle object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonOffres($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonPlateformeVirtuelle object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonPlateformeVirtuelle The associated CommonPlateformeVirtuelle object.
     * @throws PropelException
     */
    public function getCommonPlateformeVirtuelle(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonPlateformeVirtuelle === null && ($this->plateforme_virtuelle_id !== null) && $doQuery) {
            $this->aCommonPlateformeVirtuelle = CommonPlateformeVirtuelleQuery::create()->findPk($this->plateforme_virtuelle_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonPlateformeVirtuelle->addCommonOffress($this);
             */
        }

        return $this->aCommonPlateformeVirtuelle;
    }

    /**
     * Declares an association between this object and a CommonBlobOrganismeFile object.
     *
     * @param   CommonBlobOrganismeFile $v
     * @return CommonOffres The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonBlobOrganismeFileRelatedByIdBlobXmlReponse(CommonBlobOrganismeFile $v = null)
    {
        if ($v === null) {
            $this->setIdBlobXmlReponse(NULL);
        } else {
            $this->setIdBlobXmlReponse($v->getId());
        }

        $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonBlobOrganismeFile object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonOffresRelatedByIdBlobXmlReponse($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonBlobOrganismeFile object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonBlobOrganismeFile The associated CommonBlobOrganismeFile object.
     * @throws PropelException
     */
    public function getCommonBlobOrganismeFileRelatedByIdBlobXmlReponse(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse === null && ($this->id_blob_xml_reponse !== null) && $doQuery) {
            $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse = CommonBlobOrganismeFileQuery::create()->findPk($this->id_blob_xml_reponse, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->addCommonOffressRelatedByIdBlobXmlReponse($this);
             */
        }

        return $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTCandidature' == $relationName) {
            $this->initCommonTCandidatures();
        }
        if ('CommonTGroupementEntreprise' == $relationName) {
            $this->initCommonTGroupementEntreprises();
        }
    }

    /**
     * Clears out the collCommonTCandidatures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOffres The current object (for fluent API support)
     * @see        addCommonTCandidatures()
     */
    public function clearCommonTCandidatures()
    {
        $this->collCommonTCandidatures = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCandidaturesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCandidatures collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCandidatures($v = true)
    {
        $this->collCommonTCandidaturesPartial = $v;
    }

    /**
     * Initializes the collCommonTCandidatures collection.
     *
     * By default this just sets the collCommonTCandidatures collection to an empty array (like clearcollCommonTCandidatures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCandidatures($overrideExisting = true)
    {
        if (null !== $this->collCommonTCandidatures && !$overrideExisting) {
            return;
        }
        $this->collCommonTCandidatures = new PropelObjectCollection();
        $this->collCommonTCandidatures->setModel('CommonTCandidature');
    }

    /**
     * Gets an array of CommonTCandidature objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOffres is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     * @throws PropelException
     */
    public function getCommonTCandidatures($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatures || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatures) {
                // return empty collection
                $this->initCommonTCandidatures();
            } else {
                $collCommonTCandidatures = CommonTCandidatureQuery::create(null, $criteria)
                    ->filterByCommonOffres($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCandidaturesPartial && count($collCommonTCandidatures)) {
                      $this->initCommonTCandidatures(false);

                      foreach ($collCommonTCandidatures as $obj) {
                        if (false == $this->collCommonTCandidatures->contains($obj)) {
                          $this->collCommonTCandidatures->append($obj);
                        }
                      }

                      $this->collCommonTCandidaturesPartial = true;
                    }

                    $collCommonTCandidatures->getInternalIterator()->rewind();

                    return $collCommonTCandidatures;
                }

                if ($partial && $this->collCommonTCandidatures) {
                    foreach ($this->collCommonTCandidatures as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCandidatures[] = $obj;
                        }
                    }
                }

                $this->collCommonTCandidatures = $collCommonTCandidatures;
                $this->collCommonTCandidaturesPartial = false;
            }
        }

        return $this->collCommonTCandidatures;
    }

    /**
     * Sets a collection of CommonTCandidature objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCandidatures A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setCommonTCandidatures(PropelCollection $commonTCandidatures, PropelPDO $con = null)
    {
        $commonTCandidaturesToDelete = $this->getCommonTCandidatures(new Criteria(), $con)->diff($commonTCandidatures);


        $this->commonTCandidaturesScheduledForDeletion = $commonTCandidaturesToDelete;

        foreach ($commonTCandidaturesToDelete as $commonTCandidatureRemoved) {
            $commonTCandidatureRemoved->setCommonOffres(null);
        }

        $this->collCommonTCandidatures = null;
        foreach ($commonTCandidatures as $commonTCandidature) {
            $this->addCommonTCandidature($commonTCandidature);
        }

        $this->collCommonTCandidatures = $commonTCandidatures;
        $this->collCommonTCandidaturesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCandidature objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCandidature objects.
     * @throws PropelException
     */
    public function countCommonTCandidatures(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCandidatures());
            }
            $query = CommonTCandidatureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOffres($this)
                ->count($con);
        }

        return count($this->collCommonTCandidatures);
    }

    /**
     * Method called to associate a CommonTCandidature object to this object
     * through the CommonTCandidature foreign key attribute.
     *
     * @param   CommonTCandidature $l CommonTCandidature
     * @return CommonOffres The current object (for fluent API support)
     */
    public function addCommonTCandidature(CommonTCandidature $l)
    {
        if ($this->collCommonTCandidatures === null) {
            $this->initCommonTCandidatures();
            $this->collCommonTCandidaturesPartial = true;
        }
        if (!in_array($l, $this->collCommonTCandidatures->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCandidature($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCandidature $commonTCandidature The commonTCandidature object to add.
     */
    protected function doAddCommonTCandidature($commonTCandidature)
    {
        $this->collCommonTCandidatures[]= $commonTCandidature;
        $commonTCandidature->setCommonOffres($this);
    }

    /**
     * @param	CommonTCandidature $commonTCandidature The commonTCandidature object to remove.
     * @return CommonOffres The current object (for fluent API support)
     */
    public function removeCommonTCandidature($commonTCandidature)
    {
        if ($this->getCommonTCandidatures()->contains($commonTCandidature)) {
            $this->collCommonTCandidatures->remove($this->collCommonTCandidatures->search($commonTCandidature));
            if (null === $this->commonTCandidaturesScheduledForDeletion) {
                $this->commonTCandidaturesScheduledForDeletion = clone $this->collCommonTCandidatures;
                $this->commonTCandidaturesScheduledForDeletion->clear();
            }
            $this->commonTCandidaturesScheduledForDeletion[]= $commonTCandidature;
            $commonTCandidature->setCommonOffres(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOffres is new, it will return
     * an empty collection; or if this CommonOffres has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOffres.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOffres is new, it will return
     * an empty collection; or if this CommonOffres has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOffres.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonTDumeContexte($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonTDumeContexte', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOffres is new, it will return
     * an empty collection; or if this CommonOffres has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOffres.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOffres is new, it will return
     * an empty collection; or if this CommonOffres has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOffres.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }

    /**
     * Clears out the collCommonTGroupementEntreprises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonOffres The current object (for fluent API support)
     * @see        addCommonTGroupementEntreprises()
     */
    public function clearCommonTGroupementEntreprises()
    {
        $this->collCommonTGroupementEntreprises = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTGroupementEntreprisesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTGroupementEntreprises collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTGroupementEntreprises($v = true)
    {
        $this->collCommonTGroupementEntreprisesPartial = $v;
    }

    /**
     * Initializes the collCommonTGroupementEntreprises collection.
     *
     * By default this just sets the collCommonTGroupementEntreprises collection to an empty array (like clearcollCommonTGroupementEntreprises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTGroupementEntreprises($overrideExisting = true)
    {
        if (null !== $this->collCommonTGroupementEntreprises && !$overrideExisting) {
            return;
        }
        $this->collCommonTGroupementEntreprises = new PropelObjectCollection();
        $this->collCommonTGroupementEntreprises->setModel('CommonTGroupementEntreprise');
    }

    /**
     * Gets an array of CommonTGroupementEntreprise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonOffres is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTGroupementEntreprise[] List of CommonTGroupementEntreprise objects
     * @throws PropelException
     */
    public function getCommonTGroupementEntreprises($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTGroupementEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTGroupementEntreprises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTGroupementEntreprises) {
                // return empty collection
                $this->initCommonTGroupementEntreprises();
            } else {
                $collCommonTGroupementEntreprises = CommonTGroupementEntrepriseQuery::create(null, $criteria)
                    ->filterByCommonOffres($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTGroupementEntreprisesPartial && count($collCommonTGroupementEntreprises)) {
                      $this->initCommonTGroupementEntreprises(false);

                      foreach ($collCommonTGroupementEntreprises as $obj) {
                        if (false == $this->collCommonTGroupementEntreprises->contains($obj)) {
                          $this->collCommonTGroupementEntreprises->append($obj);
                        }
                      }

                      $this->collCommonTGroupementEntreprisesPartial = true;
                    }

                    $collCommonTGroupementEntreprises->getInternalIterator()->rewind();

                    return $collCommonTGroupementEntreprises;
                }

                if ($partial && $this->collCommonTGroupementEntreprises) {
                    foreach ($this->collCommonTGroupementEntreprises as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTGroupementEntreprises[] = $obj;
                        }
                    }
                }

                $this->collCommonTGroupementEntreprises = $collCommonTGroupementEntreprises;
                $this->collCommonTGroupementEntreprisesPartial = false;
            }
        }

        return $this->collCommonTGroupementEntreprises;
    }

    /**
     * Sets a collection of CommonTGroupementEntreprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTGroupementEntreprises A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonOffres The current object (for fluent API support)
     */
    public function setCommonTGroupementEntreprises(PropelCollection $commonTGroupementEntreprises, PropelPDO $con = null)
    {
        $commonTGroupementEntreprisesToDelete = $this->getCommonTGroupementEntreprises(new Criteria(), $con)->diff($commonTGroupementEntreprises);


        $this->commonTGroupementEntreprisesScheduledForDeletion = $commonTGroupementEntreprisesToDelete;

        foreach ($commonTGroupementEntreprisesToDelete as $commonTGroupementEntrepriseRemoved) {
            $commonTGroupementEntrepriseRemoved->setCommonOffres(null);
        }

        $this->collCommonTGroupementEntreprises = null;
        foreach ($commonTGroupementEntreprises as $commonTGroupementEntreprise) {
            $this->addCommonTGroupementEntreprise($commonTGroupementEntreprise);
        }

        $this->collCommonTGroupementEntreprises = $commonTGroupementEntreprises;
        $this->collCommonTGroupementEntreprisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTGroupementEntreprise objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTGroupementEntreprise objects.
     * @throws PropelException
     */
    public function countCommonTGroupementEntreprises(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTGroupementEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTGroupementEntreprises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTGroupementEntreprises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTGroupementEntreprises());
            }
            $query = CommonTGroupementEntrepriseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonOffres($this)
                ->count($con);
        }

        return count($this->collCommonTGroupementEntreprises);
    }

    /**
     * Method called to associate a CommonTGroupementEntreprise object to this object
     * through the CommonTGroupementEntreprise foreign key attribute.
     *
     * @param   CommonTGroupementEntreprise $l CommonTGroupementEntreprise
     * @return CommonOffres The current object (for fluent API support)
     */
    public function addCommonTGroupementEntreprise(CommonTGroupementEntreprise $l)
    {
        if ($this->collCommonTGroupementEntreprises === null) {
            $this->initCommonTGroupementEntreprises();
            $this->collCommonTGroupementEntreprisesPartial = true;
        }
        if (!in_array($l, $this->collCommonTGroupementEntreprises->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTGroupementEntreprise($l);
        }

        return $this;
    }

    /**
     * @param	CommonTGroupementEntreprise $commonTGroupementEntreprise The commonTGroupementEntreprise object to add.
     */
    protected function doAddCommonTGroupementEntreprise($commonTGroupementEntreprise)
    {
        $this->collCommonTGroupementEntreprises[]= $commonTGroupementEntreprise;
        $commonTGroupementEntreprise->setCommonOffres($this);
    }

    /**
     * @param	CommonTGroupementEntreprise $commonTGroupementEntreprise The commonTGroupementEntreprise object to remove.
     * @return CommonOffres The current object (for fluent API support)
     */
    public function removeCommonTGroupementEntreprise($commonTGroupementEntreprise)
    {
        if ($this->getCommonTGroupementEntreprises()->contains($commonTGroupementEntreprise)) {
            $this->collCommonTGroupementEntreprises->remove($this->collCommonTGroupementEntreprises->search($commonTGroupementEntreprise));
            if (null === $this->commonTGroupementEntreprisesScheduledForDeletion) {
                $this->commonTGroupementEntreprisesScheduledForDeletion = clone $this->collCommonTGroupementEntreprises;
                $this->commonTGroupementEntreprisesScheduledForDeletion->clear();
            }
            $this->commonTGroupementEntreprisesScheduledForDeletion[]= $commonTGroupementEntreprise;
            $commonTGroupementEntreprise->setCommonOffres(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOffres is new, it will return
     * an empty collection; or if this CommonOffres has previously
     * been saved, it will retrieve related CommonTGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOffres.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTGroupementEntreprise[] List of CommonTGroupementEntreprise objects
     */
    public function getCommonTGroupementEntreprisesJoinCommonTTypeGroupementEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeGroupementEntreprise', $join_behavior);

        return $this->getCommonTGroupementEntreprises($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonOffres is new, it will return
     * an empty collection; or if this CommonOffres has previously
     * been saved, it will retrieve related CommonTGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonOffres.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTGroupementEntreprise[] List of CommonTGroupementEntreprise objects
     */
    public function getCommonTGroupementEntreprisesJoinCommonTCandidature($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTCandidature', $join_behavior);

        return $this->getCommonTGroupementEntreprises($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->organisme = null;
        $this->consultation_ref = null;
        $this->entreprise_id = null;
        $this->id_etablissement = null;
        $this->old_inscrit_id = null;
        $this->signatureenvxml = null;
        $this->horodatage = null;
        $this->mailsignataire = null;
        $this->untrusteddate = null;
        $this->untrustedserial = null;
        $this->envoi_complet = null;
        $this->date_depot_differe = null;
        $this->horodatage_envoi_differe = null;
        $this->signatureenvxml_envoi_differe = null;
        $this->external_serial = null;
        $this->internal_serial = null;
        $this->uid_offre = null;
        $this->offre_selectionnee = null;
        $this->observation = null;
        $this->xml_string = null;
        $this->nom_entreprise_inscrit = null;
        $this->nom_inscrit = null;
        $this->prenom_inscrit = null;
        $this->adresse_inscrit = null;
        $this->adresse2_inscrit = null;
        $this->telephone_inscrit = null;
        $this->fax_inscrit = null;
        $this->code_postal_inscrit = null;
        $this->ville_inscrit = null;
        $this->pays_inscrit = null;
        $this->acronyme_pays = null;
        $this->siret_entreprise = null;
        $this->identifiant_national = null;
        $this->email_inscrit = null;
        $this->siret_inscrit = null;
        $this->nom_entreprise = null;
        $this->horodatage_annulation = null;
        $this->date_annulation = null;
        $this->signature_annulation = null;
        $this->depot_annule = null;
        $this->string_annulation = null;
        $this->verification_certificat_annulation = null;
        $this->offre_variante = null;
        $this->reponse_pas_a_pas = null;
        $this->numero_reponse = null;
        $this->statut_offres = null;
        $this->date_heure_ouverture = null;
        $this->agentid_ouverture = null;
        $this->agentid_ouverture2 = null;
        $this->date_heure_ouverture_agent2 = null;
        $this->cryptage_reponse = null;
        $this->nom_agent_ouverture = null;
        $this->agent_telechargement_offre = null;
        $this->date_telechargement_offre = null;
        $this->repertoire_telechargement_offre = null;
        $this->candidature_id_externe = null;
        $this->etat_chiffrement = null;
        $this->erreur_chiffrement = null;
        $this->date_fin_chiffrement = null;
        $this->date_horodatage = null;
        $this->verification_hotodatage = null;
        $this->verification_signature_offre = null;
        $this->horodatage_hash_fichiers = null;
        $this->id_pdf_echange_accuse = null;
        $this->created_at = null;
        $this->uid_response = null;
        $this->date_depot = null;
        $this->resultat_verification_hash_all_files = null;
        $this->id_blob_horodatage_hash = null;
        $this->id_blob_xml_reponse = null;
        $this->consultation_id = null;
        $this->plateforme_virtuelle_id = null;
        $this->inscrit_id = null;
        $this->taux_production_france = null;
        $this->taux_production_europe = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTCandidatures) {
                foreach ($this->collCommonTCandidatures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTGroupementEntreprises) {
                foreach ($this->collCommonTGroupementEntreprises as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash instanceof Persistent) {
              $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash->clearAllReferences($deep);
            }
            if ($this->aCommonConsultation instanceof Persistent) {
              $this->aCommonConsultation->clearAllReferences($deep);
            }
            if ($this->aCommonInscrit instanceof Persistent) {
              $this->aCommonInscrit->clearAllReferences($deep);
            }
            if ($this->aEntreprise instanceof Persistent) {
              $this->aEntreprise->clearAllReferences($deep);
            }
            if ($this->aCommonPlateformeVirtuelle instanceof Persistent) {
              $this->aCommonPlateformeVirtuelle->clearAllReferences($deep);
            }
            if ($this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse instanceof Persistent) {
              $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTCandidatures instanceof PropelCollection) {
            $this->collCommonTCandidatures->clearIterator();
        }
        $this->collCommonTCandidatures = null;
        if ($this->collCommonTGroupementEntreprises instanceof PropelCollection) {
            $this->collCommonTGroupementEntreprises->clearIterator();
        }
        $this->collCommonTGroupementEntreprises = null;
        $this->aCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash = null;
        $this->aCommonConsultation = null;
        $this->aCommonInscrit = null;
        $this->aEntreprise = null;
        $this->aCommonPlateformeVirtuelle = null;
        $this->aCommonBlobOrganismeFileRelatedByIdBlobXmlReponse = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonOffresPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
