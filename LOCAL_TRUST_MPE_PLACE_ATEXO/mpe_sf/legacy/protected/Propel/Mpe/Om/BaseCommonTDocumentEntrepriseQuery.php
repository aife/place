<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDocumentEntreprise;
use Application\Propel\Mpe\CommonTDocumentEntreprisePeer;
use Application\Propel\Mpe\CommonTDocumentEntrepriseQuery;
use Application\Propel\Mpe\CommonTDocumentType;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersion;
use Application\Propel\Mpe\Entreprise;

/**
 * Base class that represents a query for the 't_document_entreprise' table.
 *
 *
 *
 * @method CommonTDocumentEntrepriseQuery orderByIdDocument($order = Criteria::ASC) Order by the id_document column
 * @method CommonTDocumentEntrepriseQuery orderByIdEntreprise($order = Criteria::ASC) Order by the id_entreprise column
 * @method CommonTDocumentEntrepriseQuery orderByIdEtablissement($order = Criteria::ASC) Order by the id_etablissement column
 * @method CommonTDocumentEntrepriseQuery orderByNomDocument($order = Criteria::ASC) Order by the nom_document column
 * @method CommonTDocumentEntrepriseQuery orderByIdTypeDocument($order = Criteria::ASC) Order by the id_type_document column
 * @method CommonTDocumentEntrepriseQuery orderByIdDerniereVersion($order = Criteria::ASC) Order by the id_derniere_version column
 *
 * @method CommonTDocumentEntrepriseQuery groupByIdDocument() Group by the id_document column
 * @method CommonTDocumentEntrepriseQuery groupByIdEntreprise() Group by the id_entreprise column
 * @method CommonTDocumentEntrepriseQuery groupByIdEtablissement() Group by the id_etablissement column
 * @method CommonTDocumentEntrepriseQuery groupByNomDocument() Group by the nom_document column
 * @method CommonTDocumentEntrepriseQuery groupByIdTypeDocument() Group by the id_type_document column
 * @method CommonTDocumentEntrepriseQuery groupByIdDerniereVersion() Group by the id_derniere_version column
 *
 * @method CommonTDocumentEntrepriseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTDocumentEntrepriseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTDocumentEntrepriseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTDocumentEntrepriseQuery leftJoinEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entreprise relation
 * @method CommonTDocumentEntrepriseQuery rightJoinEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entreprise relation
 * @method CommonTDocumentEntrepriseQuery innerJoinEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the Entreprise relation
 *
 * @method CommonTDocumentEntrepriseQuery leftJoinCommonTDocumentType($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTDocumentType relation
 * @method CommonTDocumentEntrepriseQuery rightJoinCommonTDocumentType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTDocumentType relation
 * @method CommonTDocumentEntrepriseQuery innerJoinCommonTDocumentType($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTDocumentType relation
 *
 * @method CommonTDocumentEntrepriseQuery leftJoinCommonTEntrepriseDocumentVersion($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTEntrepriseDocumentVersion relation
 * @method CommonTDocumentEntrepriseQuery rightJoinCommonTEntrepriseDocumentVersion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTEntrepriseDocumentVersion relation
 * @method CommonTDocumentEntrepriseQuery innerJoinCommonTEntrepriseDocumentVersion($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTEntrepriseDocumentVersion relation
 *
 * @method CommonTDocumentEntreprise findOne(PropelPDO $con = null) Return the first CommonTDocumentEntreprise matching the query
 * @method CommonTDocumentEntreprise findOneOrCreate(PropelPDO $con = null) Return the first CommonTDocumentEntreprise matching the query, or a new CommonTDocumentEntreprise object populated from the query conditions when no match is found
 *
 * @method CommonTDocumentEntreprise findOneByIdEntreprise(int $id_entreprise) Return the first CommonTDocumentEntreprise filtered by the id_entreprise column
 * @method CommonTDocumentEntreprise findOneByIdEtablissement(int $id_etablissement) Return the first CommonTDocumentEntreprise filtered by the id_etablissement column
 * @method CommonTDocumentEntreprise findOneByNomDocument(string $nom_document) Return the first CommonTDocumentEntreprise filtered by the nom_document column
 * @method CommonTDocumentEntreprise findOneByIdTypeDocument(int $id_type_document) Return the first CommonTDocumentEntreprise filtered by the id_type_document column
 * @method CommonTDocumentEntreprise findOneByIdDerniereVersion(int $id_derniere_version) Return the first CommonTDocumentEntreprise filtered by the id_derniere_version column
 *
 * @method array findByIdDocument(int $id_document) Return CommonTDocumentEntreprise objects filtered by the id_document column
 * @method array findByIdEntreprise(int $id_entreprise) Return CommonTDocumentEntreprise objects filtered by the id_entreprise column
 * @method array findByIdEtablissement(int $id_etablissement) Return CommonTDocumentEntreprise objects filtered by the id_etablissement column
 * @method array findByNomDocument(string $nom_document) Return CommonTDocumentEntreprise objects filtered by the nom_document column
 * @method array findByIdTypeDocument(int $id_type_document) Return CommonTDocumentEntreprise objects filtered by the id_type_document column
 * @method array findByIdDerniereVersion(int $id_derniere_version) Return CommonTDocumentEntreprise objects filtered by the id_derniere_version column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDocumentEntrepriseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTDocumentEntrepriseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTDocumentEntreprise', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTDocumentEntrepriseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTDocumentEntrepriseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTDocumentEntrepriseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTDocumentEntrepriseQuery) {
            return $criteria;
        }
        $query = new CommonTDocumentEntrepriseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTDocumentEntreprise|CommonTDocumentEntreprise[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTDocumentEntreprisePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDocumentEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdDocument($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDocumentEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_document`, `id_entreprise`, `id_etablissement`, `nom_document`, `id_type_document`, `id_derniere_version` FROM `t_document_entreprise` WHERE `id_document` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTDocumentEntreprise();
            $obj->hydrate($row);
            CommonTDocumentEntreprisePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTDocumentEntreprise|CommonTDocumentEntreprise[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTDocumentEntreprise[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DOCUMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DOCUMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_document column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDocument(1234); // WHERE id_document = 1234
     * $query->filterByIdDocument(array(12, 34)); // WHERE id_document IN (12, 34)
     * $query->filterByIdDocument(array('min' => 12)); // WHERE id_document >= 12
     * $query->filterByIdDocument(array('max' => 12)); // WHERE id_document <= 12
     * </code>
     *
     * @param     mixed $idDocument The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdDocument($idDocument = null, $comparison = null)
    {
        if (is_array($idDocument)) {
            $useMinMax = false;
            if (isset($idDocument['min'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DOCUMENT, $idDocument['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDocument['max'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DOCUMENT, $idDocument['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DOCUMENT, $idDocument, $comparison);
    }

    /**
     * Filter the query on the id_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntreprise(1234); // WHERE id_entreprise = 1234
     * $query->filterByIdEntreprise(array(12, 34)); // WHERE id_entreprise IN (12, 34)
     * $query->filterByIdEntreprise(array('min' => 12)); // WHERE id_entreprise >= 12
     * $query->filterByIdEntreprise(array('max' => 12)); // WHERE id_entreprise <= 12
     * </code>
     *
     * @see       filterByEntreprise()
     *
     * @param     mixed $idEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdEntreprise($idEntreprise = null, $comparison = null)
    {
        if (is_array($idEntreprise)) {
            $useMinMax = false;
            if (isset($idEntreprise['min'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_ENTREPRISE, $idEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntreprise['max'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_ENTREPRISE, $idEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_ENTREPRISE, $idEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtablissement(1234); // WHERE id_etablissement = 1234
     * $query->filterByIdEtablissement(array(12, 34)); // WHERE id_etablissement IN (12, 34)
     * $query->filterByIdEtablissement(array('min' => 12)); // WHERE id_etablissement >= 12
     * $query->filterByIdEtablissement(array('max' => 12)); // WHERE id_etablissement <= 12
     * </code>
     *
     * @param     mixed $idEtablissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdEtablissement($idEtablissement = null, $comparison = null)
    {
        if (is_array($idEtablissement)) {
            $useMinMax = false;
            if (isset($idEtablissement['min'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_ETABLISSEMENT, $idEtablissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtablissement['max'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_ETABLISSEMENT, $idEtablissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_ETABLISSEMENT, $idEtablissement, $comparison);
    }

    /**
     * Filter the query on the nom_document column
     *
     * Example usage:
     * <code>
     * $query->filterByNomDocument('fooValue');   // WHERE nom_document = 'fooValue'
     * $query->filterByNomDocument('%fooValue%'); // WHERE nom_document LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomDocument The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function filterByNomDocument($nomDocument = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomDocument)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomDocument)) {
                $nomDocument = str_replace('*', '%', $nomDocument);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentEntreprisePeer::NOM_DOCUMENT, $nomDocument, $comparison);
    }

    /**
     * Filter the query on the id_type_document column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeDocument(1234); // WHERE id_type_document = 1234
     * $query->filterByIdTypeDocument(array(12, 34)); // WHERE id_type_document IN (12, 34)
     * $query->filterByIdTypeDocument(array('min' => 12)); // WHERE id_type_document >= 12
     * $query->filterByIdTypeDocument(array('max' => 12)); // WHERE id_type_document <= 12
     * </code>
     *
     * @see       filterByCommonTDocumentType()
     *
     * @param     mixed $idTypeDocument The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdTypeDocument($idTypeDocument = null, $comparison = null)
    {
        if (is_array($idTypeDocument)) {
            $useMinMax = false;
            if (isset($idTypeDocument['min'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_TYPE_DOCUMENT, $idTypeDocument['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeDocument['max'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_TYPE_DOCUMENT, $idTypeDocument['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_TYPE_DOCUMENT, $idTypeDocument, $comparison);
    }

    /**
     * Filter the query on the id_derniere_version column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDerniereVersion(1234); // WHERE id_derniere_version = 1234
     * $query->filterByIdDerniereVersion(array(12, 34)); // WHERE id_derniere_version IN (12, 34)
     * $query->filterByIdDerniereVersion(array('min' => 12)); // WHERE id_derniere_version >= 12
     * $query->filterByIdDerniereVersion(array('max' => 12)); // WHERE id_derniere_version <= 12
     * </code>
     *
     * @param     mixed $idDerniereVersion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdDerniereVersion($idDerniereVersion = null, $comparison = null)
    {
        if (is_array($idDerniereVersion)) {
            $useMinMax = false;
            if (isset($idDerniereVersion['min'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DERNIERE_VERSION, $idDerniereVersion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDerniereVersion['max'])) {
                $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DERNIERE_VERSION, $idDerniereVersion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DERNIERE_VERSION, $idDerniereVersion, $comparison);
    }

    /**
     * Filter the query by a related Entreprise object
     *
     * @param   Entreprise|PropelObjectCollection $entreprise The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDocumentEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByEntreprise($entreprise, $comparison = null)
    {
        if ($entreprise instanceof Entreprise) {
            return $this
                ->addUsingAlias(CommonTDocumentEntreprisePeer::ID_ENTREPRISE, $entreprise->getId(), $comparison);
        } elseif ($entreprise instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTDocumentEntreprisePeer::ID_ENTREPRISE, $entreprise->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEntreprise() only accepts arguments of type Entreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function joinEntreprise($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entreprise');
        }

        return $this;
    }

    /**
     * Use the Entreprise relation Entreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\EntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useEntrepriseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entreprise', '\Application\Propel\Mpe\EntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonTDocumentType object
     *
     * @param   CommonTDocumentType|PropelObjectCollection $commonTDocumentType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDocumentEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTDocumentType($commonTDocumentType, $comparison = null)
    {
        if ($commonTDocumentType instanceof CommonTDocumentType) {
            return $this
                ->addUsingAlias(CommonTDocumentEntreprisePeer::ID_TYPE_DOCUMENT, $commonTDocumentType->getIdTypeDocument(), $comparison);
        } elseif ($commonTDocumentType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTDocumentEntreprisePeer::ID_TYPE_DOCUMENT, $commonTDocumentType->toKeyValue('PrimaryKey', 'IdTypeDocument'), $comparison);
        } else {
            throw new PropelException('filterByCommonTDocumentType() only accepts arguments of type CommonTDocumentType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTDocumentType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTDocumentType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTDocumentType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTDocumentType');
        }

        return $this;
    }

    /**
     * Use the CommonTDocumentType relation CommonTDocumentType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTDocumentTypeQuery A secondary query class using the current class as primary query
     */
    public function useCommonTDocumentTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTDocumentType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTDocumentType', '\Application\Propel\Mpe\CommonTDocumentTypeQuery');
    }

    /**
     * Filter the query by a related CommonTEntrepriseDocumentVersion object
     *
     * @param   CommonTEntrepriseDocumentVersion|PropelObjectCollection $commonTEntrepriseDocumentVersion  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDocumentEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTEntrepriseDocumentVersion($commonTEntrepriseDocumentVersion, $comparison = null)
    {
        if ($commonTEntrepriseDocumentVersion instanceof CommonTEntrepriseDocumentVersion) {
            return $this
                ->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DOCUMENT, $commonTEntrepriseDocumentVersion->getIdDocument(), $comparison);
        } elseif ($commonTEntrepriseDocumentVersion instanceof PropelObjectCollection) {
            return $this
                ->useCommonTEntrepriseDocumentVersionQuery()
                ->filterByPrimaryKeys($commonTEntrepriseDocumentVersion->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTEntrepriseDocumentVersion() only accepts arguments of type CommonTEntrepriseDocumentVersion or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTEntrepriseDocumentVersion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTEntrepriseDocumentVersion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTEntrepriseDocumentVersion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTEntrepriseDocumentVersion');
        }

        return $this;
    }

    /**
     * Use the CommonTEntrepriseDocumentVersion relation CommonTEntrepriseDocumentVersion object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTEntrepriseDocumentVersionQuery A secondary query class using the current class as primary query
     */
    public function useCommonTEntrepriseDocumentVersionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTEntrepriseDocumentVersion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTEntrepriseDocumentVersion', '\Application\Propel\Mpe\CommonTEntrepriseDocumentVersionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTDocumentEntreprise $commonTDocumentEntreprise Object to remove from the list of results
     *
     * @return CommonTDocumentEntrepriseQuery The current query, for fluid interface
     */
    public function prune($commonTDocumentEntreprise = null)
    {
        if ($commonTDocumentEntreprise) {
            $this->addUsingAlias(CommonTDocumentEntreprisePeer::ID_DOCUMENT, $commonTDocumentEntreprise->getIdDocument(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
