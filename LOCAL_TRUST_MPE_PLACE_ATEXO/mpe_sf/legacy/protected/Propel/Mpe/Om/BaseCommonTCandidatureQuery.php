<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidaturePeer;
use Application\Propel\Mpe\CommonTCandidatureQuery;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTListeLotsCandidature;

/**
 * Base class that represents a query for the 't_candidature' table.
 *
 *
 *
 * @method CommonTCandidatureQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTCandidatureQuery orderByRefConsultation($order = Criteria::ASC) Order by the ref_consultation column
 * @method CommonTCandidatureQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTCandidatureQuery orderByOldIdInscrit($order = Criteria::ASC) Order by the old_id_inscrit column
 * @method CommonTCandidatureQuery orderByIdEntreprise($order = Criteria::ASC) Order by the id_entreprise column
 * @method CommonTCandidatureQuery orderByIdEtablissement($order = Criteria::ASC) Order by the id_etablissement column
 * @method CommonTCandidatureQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method CommonTCandidatureQuery orderByTypeCandidature($order = Criteria::ASC) Order by the type_candidature column
 * @method CommonTCandidatureQuery orderByTypeCandidatureDume($order = Criteria::ASC) Order by the type_candidature_dume column
 * @method CommonTCandidatureQuery orderByIdDumeContexte($order = Criteria::ASC) Order by the id_dume_contexte column
 * @method CommonTCandidatureQuery orderByIdOffre($order = Criteria::ASC) Order by the id_offre column
 * @method CommonTCandidatureQuery orderByRoleInscrit($order = Criteria::ASC) Order by the role_inscrit column
 * @method CommonTCandidatureQuery orderByDateDerniereValidationDume($order = Criteria::ASC) Order by the date_derniere_validation_dume column
 * @method CommonTCandidatureQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 * @method CommonTCandidatureQuery orderByIdInscrit($order = Criteria::ASC) Order by the id_inscrit column
 *
 * @method CommonTCandidatureQuery groupById() Group by the id column
 * @method CommonTCandidatureQuery groupByRefConsultation() Group by the ref_consultation column
 * @method CommonTCandidatureQuery groupByOrganisme() Group by the organisme column
 * @method CommonTCandidatureQuery groupByOldIdInscrit() Group by the old_id_inscrit column
 * @method CommonTCandidatureQuery groupByIdEntreprise() Group by the id_entreprise column
 * @method CommonTCandidatureQuery groupByIdEtablissement() Group by the id_etablissement column
 * @method CommonTCandidatureQuery groupByStatus() Group by the status column
 * @method CommonTCandidatureQuery groupByTypeCandidature() Group by the type_candidature column
 * @method CommonTCandidatureQuery groupByTypeCandidatureDume() Group by the type_candidature_dume column
 * @method CommonTCandidatureQuery groupByIdDumeContexte() Group by the id_dume_contexte column
 * @method CommonTCandidatureQuery groupByIdOffre() Group by the id_offre column
 * @method CommonTCandidatureQuery groupByRoleInscrit() Group by the role_inscrit column
 * @method CommonTCandidatureQuery groupByDateDerniereValidationDume() Group by the date_derniere_validation_dume column
 * @method CommonTCandidatureQuery groupByConsultationId() Group by the consultation_id column
 * @method CommonTCandidatureQuery groupByIdInscrit() Group by the id_inscrit column
 *
 * @method CommonTCandidatureQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTCandidatureQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTCandidatureQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTCandidatureQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTCandidatureQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTCandidatureQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonTCandidatureQuery leftJoinCommonTDumeContexte($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTDumeContexte relation
 * @method CommonTCandidatureQuery rightJoinCommonTDumeContexte($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTDumeContexte relation
 * @method CommonTCandidatureQuery innerJoinCommonTDumeContexte($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTDumeContexte relation
 *
 * @method CommonTCandidatureQuery leftJoinCommonInscrit($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonInscrit relation
 * @method CommonTCandidatureQuery rightJoinCommonInscrit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonInscrit relation
 * @method CommonTCandidatureQuery innerJoinCommonInscrit($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonInscrit relation
 *
 * @method CommonTCandidatureQuery leftJoinCommonOffres($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOffres relation
 * @method CommonTCandidatureQuery rightJoinCommonOffres($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOffres relation
 * @method CommonTCandidatureQuery innerJoinCommonOffres($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOffres relation
 *
 * @method CommonTCandidatureQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTCandidatureQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTCandidatureQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonTCandidatureQuery leftJoinCommonTGroupementEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTGroupementEntreprise relation
 * @method CommonTCandidatureQuery rightJoinCommonTGroupementEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTGroupementEntreprise relation
 * @method CommonTCandidatureQuery innerJoinCommonTGroupementEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTGroupementEntreprise relation
 *
 * @method CommonTCandidatureQuery leftJoinCommonTListeLotsCandidature($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTListeLotsCandidature relation
 * @method CommonTCandidatureQuery rightJoinCommonTListeLotsCandidature($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTListeLotsCandidature relation
 * @method CommonTCandidatureQuery innerJoinCommonTListeLotsCandidature($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTListeLotsCandidature relation
 *
 * @method CommonTCandidature findOne(PropelPDO $con = null) Return the first CommonTCandidature matching the query
 * @method CommonTCandidature findOneOrCreate(PropelPDO $con = null) Return the first CommonTCandidature matching the query, or a new CommonTCandidature object populated from the query conditions when no match is found
 *
 * @method CommonTCandidature findOneByRefConsultation(int $ref_consultation) Return the first CommonTCandidature filtered by the ref_consultation column
 * @method CommonTCandidature findOneByOrganisme(string $organisme) Return the first CommonTCandidature filtered by the organisme column
 * @method CommonTCandidature findOneByOldIdInscrit(int $old_id_inscrit) Return the first CommonTCandidature filtered by the old_id_inscrit column
 * @method CommonTCandidature findOneByIdEntreprise(int $id_entreprise) Return the first CommonTCandidature filtered by the id_entreprise column
 * @method CommonTCandidature findOneByIdEtablissement(int $id_etablissement) Return the first CommonTCandidature filtered by the id_etablissement column
 * @method CommonTCandidature findOneByStatus(int $status) Return the first CommonTCandidature filtered by the status column
 * @method CommonTCandidature findOneByTypeCandidature(string $type_candidature) Return the first CommonTCandidature filtered by the type_candidature column
 * @method CommonTCandidature findOneByTypeCandidatureDume(string $type_candidature_dume) Return the first CommonTCandidature filtered by the type_candidature_dume column
 * @method CommonTCandidature findOneByIdDumeContexte(int $id_dume_contexte) Return the first CommonTCandidature filtered by the id_dume_contexte column
 * @method CommonTCandidature findOneByIdOffre(int $id_offre) Return the first CommonTCandidature filtered by the id_offre column
 * @method CommonTCandidature findOneByRoleInscrit(int $role_inscrit) Return the first CommonTCandidature filtered by the role_inscrit column
 * @method CommonTCandidature findOneByDateDerniereValidationDume(string $date_derniere_validation_dume) Return the first CommonTCandidature filtered by the date_derniere_validation_dume column
 * @method CommonTCandidature findOneByConsultationId(int $consultation_id) Return the first CommonTCandidature filtered by the consultation_id column
 * @method CommonTCandidature findOneByIdInscrit(string $id_inscrit) Return the first CommonTCandidature filtered by the id_inscrit column
 *
 * @method array findById(int $id) Return CommonTCandidature objects filtered by the id column
 * @method array findByRefConsultation(int $ref_consultation) Return CommonTCandidature objects filtered by the ref_consultation column
 * @method array findByOrganisme(string $organisme) Return CommonTCandidature objects filtered by the organisme column
 * @method array findByOldIdInscrit(int $old_id_inscrit) Return CommonTCandidature objects filtered by the old_id_inscrit column
 * @method array findByIdEntreprise(int $id_entreprise) Return CommonTCandidature objects filtered by the id_entreprise column
 * @method array findByIdEtablissement(int $id_etablissement) Return CommonTCandidature objects filtered by the id_etablissement column
 * @method array findByStatus(int $status) Return CommonTCandidature objects filtered by the status column
 * @method array findByTypeCandidature(string $type_candidature) Return CommonTCandidature objects filtered by the type_candidature column
 * @method array findByTypeCandidatureDume(string $type_candidature_dume) Return CommonTCandidature objects filtered by the type_candidature_dume column
 * @method array findByIdDumeContexte(int $id_dume_contexte) Return CommonTCandidature objects filtered by the id_dume_contexte column
 * @method array findByIdOffre(int $id_offre) Return CommonTCandidature objects filtered by the id_offre column
 * @method array findByRoleInscrit(int $role_inscrit) Return CommonTCandidature objects filtered by the role_inscrit column
 * @method array findByDateDerniereValidationDume(string $date_derniere_validation_dume) Return CommonTCandidature objects filtered by the date_derniere_validation_dume column
 * @method array findByConsultationId(int $consultation_id) Return CommonTCandidature objects filtered by the consultation_id column
 * @method array findByIdInscrit(string $id_inscrit) Return CommonTCandidature objects filtered by the id_inscrit column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTCandidatureQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTCandidatureQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTCandidature', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTCandidatureQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTCandidatureQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTCandidatureQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTCandidatureQuery) {
            return $criteria;
        }
        $query = new CommonTCandidatureQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTCandidature|CommonTCandidature[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTCandidaturePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTCandidature A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTCandidature A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `ref_consultation`, `organisme`, `old_id_inscrit`, `id_entreprise`, `id_etablissement`, `status`, `type_candidature`, `type_candidature_dume`, `id_dume_contexte`, `id_offre`, `role_inscrit`, `date_derniere_validation_dume`, `consultation_id`, `id_inscrit` FROM `t_candidature` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTCandidature();
            $obj->hydrate($row);
            CommonTCandidaturePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTCandidature|CommonTCandidature[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTCandidature[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTCandidaturePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTCandidaturePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ref_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByRefConsultation(1234); // WHERE ref_consultation = 1234
     * $query->filterByRefConsultation(array(12, 34)); // WHERE ref_consultation IN (12, 34)
     * $query->filterByRefConsultation(array('min' => 12)); // WHERE ref_consultation >= 12
     * $query->filterByRefConsultation(array('max' => 12)); // WHERE ref_consultation <= 12
     * </code>
     *
     * @param     mixed $refConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByRefConsultation($refConsultation = null, $comparison = null)
    {
        if (is_array($refConsultation)) {
            $useMinMax = false;
            if (isset($refConsultation['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::REF_CONSULTATION, $refConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refConsultation['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::REF_CONSULTATION, $refConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::REF_CONSULTATION, $refConsultation, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdInscrit(1234); // WHERE old_id_inscrit = 1234
     * $query->filterByOldIdInscrit(array(12, 34)); // WHERE old_id_inscrit IN (12, 34)
     * $query->filterByOldIdInscrit(array('min' => 12)); // WHERE old_id_inscrit >= 12
     * $query->filterByOldIdInscrit(array('max' => 12)); // WHERE old_id_inscrit <= 12
     * </code>
     *
     * @param     mixed $oldIdInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByOldIdInscrit($oldIdInscrit = null, $comparison = null)
    {
        if (is_array($oldIdInscrit)) {
            $useMinMax = false;
            if (isset($oldIdInscrit['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::OLD_ID_INSCRIT, $oldIdInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdInscrit['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::OLD_ID_INSCRIT, $oldIdInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::OLD_ID_INSCRIT, $oldIdInscrit, $comparison);
    }

    /**
     * Filter the query on the id_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntreprise(1234); // WHERE id_entreprise = 1234
     * $query->filterByIdEntreprise(array(12, 34)); // WHERE id_entreprise IN (12, 34)
     * $query->filterByIdEntreprise(array('min' => 12)); // WHERE id_entreprise >= 12
     * $query->filterByIdEntreprise(array('max' => 12)); // WHERE id_entreprise <= 12
     * </code>
     *
     * @param     mixed $idEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByIdEntreprise($idEntreprise = null, $comparison = null)
    {
        if (is_array($idEntreprise)) {
            $useMinMax = false;
            if (isset($idEntreprise['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_ENTREPRISE, $idEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntreprise['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_ENTREPRISE, $idEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::ID_ENTREPRISE, $idEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtablissement(1234); // WHERE id_etablissement = 1234
     * $query->filterByIdEtablissement(array(12, 34)); // WHERE id_etablissement IN (12, 34)
     * $query->filterByIdEtablissement(array('min' => 12)); // WHERE id_etablissement >= 12
     * $query->filterByIdEtablissement(array('max' => 12)); // WHERE id_etablissement <= 12
     * </code>
     *
     * @param     mixed $idEtablissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByIdEtablissement($idEtablissement = null, $comparison = null)
    {
        if (is_array($idEtablissement)) {
            $useMinMax = false;
            if (isset($idEtablissement['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_ETABLISSEMENT, $idEtablissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtablissement['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_ETABLISSEMENT, $idEtablissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::ID_ETABLISSEMENT, $idEtablissement, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status >= 12
     * $query->filterByStatus(array('max' => 12)); // WHERE status <= 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the type_candidature column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeCandidature('fooValue');   // WHERE type_candidature = 'fooValue'
     * $query->filterByTypeCandidature('%fooValue%'); // WHERE type_candidature LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeCandidature The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByTypeCandidature($typeCandidature = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeCandidature)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeCandidature)) {
                $typeCandidature = str_replace('*', '%', $typeCandidature);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::TYPE_CANDIDATURE, $typeCandidature, $comparison);
    }

    /**
     * Filter the query on the type_candidature_dume column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeCandidatureDume('fooValue');   // WHERE type_candidature_dume = 'fooValue'
     * $query->filterByTypeCandidatureDume('%fooValue%'); // WHERE type_candidature_dume LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeCandidatureDume The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByTypeCandidatureDume($typeCandidatureDume = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeCandidatureDume)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeCandidatureDume)) {
                $typeCandidatureDume = str_replace('*', '%', $typeCandidatureDume);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::TYPE_CANDIDATURE_DUME, $typeCandidatureDume, $comparison);
    }

    /**
     * Filter the query on the id_dume_contexte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDumeContexte(1234); // WHERE id_dume_contexte = 1234
     * $query->filterByIdDumeContexte(array(12, 34)); // WHERE id_dume_contexte IN (12, 34)
     * $query->filterByIdDumeContexte(array('min' => 12)); // WHERE id_dume_contexte >= 12
     * $query->filterByIdDumeContexte(array('max' => 12)); // WHERE id_dume_contexte <= 12
     * </code>
     *
     * @see       filterByCommonTDumeContexte()
     *
     * @param     mixed $idDumeContexte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByIdDumeContexte($idDumeContexte = null, $comparison = null)
    {
        if (is_array($idDumeContexte)) {
            $useMinMax = false;
            if (isset($idDumeContexte['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_DUME_CONTEXTE, $idDumeContexte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDumeContexte['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_DUME_CONTEXTE, $idDumeContexte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::ID_DUME_CONTEXTE, $idDumeContexte, $comparison);
    }

    /**
     * Filter the query on the id_offre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOffre(1234); // WHERE id_offre = 1234
     * $query->filterByIdOffre(array(12, 34)); // WHERE id_offre IN (12, 34)
     * $query->filterByIdOffre(array('min' => 12)); // WHERE id_offre >= 12
     * $query->filterByIdOffre(array('max' => 12)); // WHERE id_offre <= 12
     * </code>
     *
     * @see       filterByCommonOffres()
     *
     * @param     mixed $idOffre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByIdOffre($idOffre = null, $comparison = null)
    {
        if (is_array($idOffre)) {
            $useMinMax = false;
            if (isset($idOffre['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_OFFRE, $idOffre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOffre['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_OFFRE, $idOffre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::ID_OFFRE, $idOffre, $comparison);
    }

    /**
     * Filter the query on the role_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByRoleInscrit(1234); // WHERE role_inscrit = 1234
     * $query->filterByRoleInscrit(array(12, 34)); // WHERE role_inscrit IN (12, 34)
     * $query->filterByRoleInscrit(array('min' => 12)); // WHERE role_inscrit >= 12
     * $query->filterByRoleInscrit(array('max' => 12)); // WHERE role_inscrit <= 12
     * </code>
     *
     * @param     mixed $roleInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByRoleInscrit($roleInscrit = null, $comparison = null)
    {
        if (is_array($roleInscrit)) {
            $useMinMax = false;
            if (isset($roleInscrit['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ROLE_INSCRIT, $roleInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($roleInscrit['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ROLE_INSCRIT, $roleInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::ROLE_INSCRIT, $roleInscrit, $comparison);
    }

    /**
     * Filter the query on the date_derniere_validation_dume column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDerniereValidationDume('2011-03-14'); // WHERE date_derniere_validation_dume = '2011-03-14'
     * $query->filterByDateDerniereValidationDume('now'); // WHERE date_derniere_validation_dume = '2011-03-14'
     * $query->filterByDateDerniereValidationDume(array('max' => 'yesterday')); // WHERE date_derniere_validation_dume > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDerniereValidationDume The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByDateDerniereValidationDume($dateDerniereValidationDume = null, $comparison = null)
    {
        if (is_array($dateDerniereValidationDume)) {
            $useMinMax = false;
            if (isset($dateDerniereValidationDume['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME, $dateDerniereValidationDume['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDerniereValidationDume['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME, $dateDerniereValidationDume['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME, $dateDerniereValidationDume, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultation()
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query on the id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInscrit(1234); // WHERE id_inscrit = 1234
     * $query->filterByIdInscrit(array(12, 34)); // WHERE id_inscrit IN (12, 34)
     * $query->filterByIdInscrit(array('min' => 12)); // WHERE id_inscrit >= 12
     * $query->filterByIdInscrit(array('max' => 12)); // WHERE id_inscrit <= 12
     * </code>
     *
     * @see       filterByCommonInscrit()
     *
     * @param     mixed $idInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function filterByIdInscrit($idInscrit = null, $comparison = null)
    {
        if (is_array($idInscrit)) {
            $useMinMax = false;
            if (isset($idInscrit['min'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_INSCRIT, $idInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInscrit['max'])) {
                $this->addUsingAlias(CommonTCandidaturePeer::ID_INSCRIT, $idInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCandidaturePeer::ID_INSCRIT, $idInscrit, $comparison);
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonTCandidaturePeer::CONSULTATION_ID, $commonConsultation->getId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCandidaturePeer::CONSULTATION_ID, $commonConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Filter the query by a related CommonTDumeContexte object
     *
     * @param   CommonTDumeContexte|PropelObjectCollection $commonTDumeContexte The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTDumeContexte($commonTDumeContexte, $comparison = null)
    {
        if ($commonTDumeContexte instanceof CommonTDumeContexte) {
            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ID_DUME_CONTEXTE, $commonTDumeContexte->getId(), $comparison);
        } elseif ($commonTDumeContexte instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ID_DUME_CONTEXTE, $commonTDumeContexte->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTDumeContexte() only accepts arguments of type CommonTDumeContexte or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTDumeContexte relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonTDumeContexte($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTDumeContexte');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTDumeContexte');
        }

        return $this;
    }

    /**
     * Use the CommonTDumeContexte relation CommonTDumeContexte object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTDumeContexteQuery A secondary query class using the current class as primary query
     */
    public function useCommonTDumeContexteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTDumeContexte($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTDumeContexte', '\Application\Propel\Mpe\CommonTDumeContexteQuery');
    }

    /**
     * Filter the query by a related CommonInscrit object
     *
     * @param   CommonInscrit|PropelObjectCollection $commonInscrit The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonInscrit($commonInscrit, $comparison = null)
    {
        if ($commonInscrit instanceof CommonInscrit) {
            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ID_INSCRIT, $commonInscrit->getId(), $comparison);
        } elseif ($commonInscrit instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ID_INSCRIT, $commonInscrit->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonInscrit() only accepts arguments of type CommonInscrit or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonInscrit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonInscrit($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonInscrit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonInscrit');
        }

        return $this;
    }

    /**
     * Use the CommonInscrit relation CommonInscrit object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonInscritQuery A secondary query class using the current class as primary query
     */
    public function useCommonInscritQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonInscrit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonInscrit', '\Application\Propel\Mpe\CommonInscritQuery');
    }

    /**
     * Filter the query by a related CommonOffres object
     *
     * @param   CommonOffres|PropelObjectCollection $commonOffres The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOffres($commonOffres, $comparison = null)
    {
        if ($commonOffres instanceof CommonOffres) {
            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ID_OFFRE, $commonOffres->getId(), $comparison);
        } elseif ($commonOffres instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ID_OFFRE, $commonOffres->toKeyValue('Id', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonOffres() only accepts arguments of type CommonOffres or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOffres relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonOffres($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOffres');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOffres');
        }

        return $this;
    }

    /**
     * Use the CommonOffres relation CommonOffres object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOffresQuery A secondary query class using the current class as primary query
     */
    public function useCommonOffresQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonOffres($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOffres', '\Application\Propel\Mpe\CommonOffresQuery');
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ORGANISME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ORGANISME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Filter the query by a related CommonTGroupementEntreprise object
     *
     * @param   CommonTGroupementEntreprise|PropelObjectCollection $commonTGroupementEntreprise  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTGroupementEntreprise($commonTGroupementEntreprise, $comparison = null)
    {
        if ($commonTGroupementEntreprise instanceof CommonTGroupementEntreprise) {
            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ID, $commonTGroupementEntreprise->getIdCandidature(), $comparison);
        } elseif ($commonTGroupementEntreprise instanceof PropelObjectCollection) {
            return $this
                ->useCommonTGroupementEntrepriseQuery()
                ->filterByPrimaryKeys($commonTGroupementEntreprise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTGroupementEntreprise() only accepts arguments of type CommonTGroupementEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTGroupementEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonTGroupementEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTGroupementEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTGroupementEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTGroupementEntreprise relation CommonTGroupementEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTGroupementEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTGroupementEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTGroupementEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTGroupementEntreprise', '\Application\Propel\Mpe\CommonTGroupementEntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonTListeLotsCandidature object
     *
     * @param   CommonTListeLotsCandidature|PropelObjectCollection $commonTListeLotsCandidature  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTListeLotsCandidature($commonTListeLotsCandidature, $comparison = null)
    {
        if ($commonTListeLotsCandidature instanceof CommonTListeLotsCandidature) {
            return $this
                ->addUsingAlias(CommonTCandidaturePeer::ID, $commonTListeLotsCandidature->getIdCandidature(), $comparison);
        } elseif ($commonTListeLotsCandidature instanceof PropelObjectCollection) {
            return $this
                ->useCommonTListeLotsCandidatureQuery()
                ->filterByPrimaryKeys($commonTListeLotsCandidature->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTListeLotsCandidature() only accepts arguments of type CommonTListeLotsCandidature or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTListeLotsCandidature relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonTListeLotsCandidature($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTListeLotsCandidature');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTListeLotsCandidature');
        }

        return $this;
    }

    /**
     * Use the CommonTListeLotsCandidature relation CommonTListeLotsCandidature object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTListeLotsCandidatureQuery A secondary query class using the current class as primary query
     */
    public function useCommonTListeLotsCandidatureQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTListeLotsCandidature($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTListeLotsCandidature', '\Application\Propel\Mpe\CommonTListeLotsCandidatureQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTCandidature $commonTCandidature Object to remove from the list of results
     *
     * @return CommonTCandidatureQuery The current query, for fluid interface
     */
    public function prune($commonTCandidature = null)
    {
        if ($commonTCandidature) {
            $this->addUsingAlias(CommonTCandidaturePeer::ID, $commonTCandidature->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
