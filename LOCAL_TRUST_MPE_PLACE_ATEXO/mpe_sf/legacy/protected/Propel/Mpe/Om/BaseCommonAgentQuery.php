<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonAgentTechniqueToken;
use Application\Propel\Mpe\CommonAnnexeFinanciere;
use Application\Propel\Mpe\CommonConsultationFavoris;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocHistorique;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHabilitationAgentWs;
use Application\Propel\Mpe\CommonInvitePermanentTransverse;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTCAOCommissionAgent;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenant;
use Application\Propel\Mpe\CommonTCAOSeanceAgent;
use Application\Propel\Mpe\CommonTCAOSeanceInvite;
use Application\Propel\Mpe\CommonTVisionRmaAgentOrganisme;

/**
 * Base class that represents a query for the 'Agent' table.
 *
 *
 *
 * @method CommonAgentQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonAgentQuery orderByLogin($order = Criteria::ASC) Order by the login column
 * @method CommonAgentQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method CommonAgentQuery orderByMdp($order = Criteria::ASC) Order by the mdp column
 * @method CommonAgentQuery orderByCertificat($order = Criteria::ASC) Order by the certificat column
 * @method CommonAgentQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method CommonAgentQuery orderByPrenom($order = Criteria::ASC) Order by the prenom column
 * @method CommonAgentQuery orderByTentativesMdp($order = Criteria::ASC) Order by the tentatives_mdp column
 * @method CommonAgentQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonAgentQuery orderByOldServiceId($order = Criteria::ASC) Order by the old_service_id column
 * @method CommonAgentQuery orderByRecevoirMail($order = Criteria::ASC) Order by the RECEVOIR_MAIL column
 * @method CommonAgentQuery orderByElu($order = Criteria::ASC) Order by the elu column
 * @method CommonAgentQuery orderByNomFonction($order = Criteria::ASC) Order by the nom_fonction column
 * @method CommonAgentQuery orderByNumTel($order = Criteria::ASC) Order by the num_tel column
 * @method CommonAgentQuery orderByNumFax($order = Criteria::ASC) Order by the num_fax column
 * @method CommonAgentQuery orderByTypeComm($order = Criteria::ASC) Order by the type_comm column
 * @method CommonAgentQuery orderByAdrPostale($order = Criteria::ASC) Order by the adr_postale column
 * @method CommonAgentQuery orderByCivilite($order = Criteria::ASC) Order by the civilite column
 * @method CommonAgentQuery orderByAlerteReponseElectronique($order = Criteria::ASC) Order by the alerte_reponse_electronique column
 * @method CommonAgentQuery orderByAlerteClotureConsultation($order = Criteria::ASC) Order by the alerte_cloture_consultation column
 * @method CommonAgentQuery orderByAlerteReceptionMessage($order = Criteria::ASC) Order by the alerte_reception_message column
 * @method CommonAgentQuery orderByAlertePublicationBoamp($order = Criteria::ASC) Order by the alerte_publication_boamp column
 * @method CommonAgentQuery orderByAlerteEchecPublicationBoamp($order = Criteria::ASC) Order by the alerte_echec_publication_boamp column
 * @method CommonAgentQuery orderByAlerteCreationModificationAgent($order = Criteria::ASC) Order by the alerte_creation_modification_agent column
 * @method CommonAgentQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonAgentQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonAgentQuery orderByIdExterne($order = Criteria::ASC) Order by the id_externe column
 * @method CommonAgentQuery orderByIdProfilSocleExterne($order = Criteria::ASC) Order by the id_profil_socle_externe column
 * @method CommonAgentQuery orderByLieuExecution($order = Criteria::ASC) Order by the lieu_execution column
 * @method CommonAgentQuery orderByAlerteQuestionEntreprise($order = Criteria::ASC) Order by the alerte_question_entreprise column
 * @method CommonAgentQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method CommonAgentQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method CommonAgentQuery orderByCodesNuts($order = Criteria::ASC) Order by the codes_nuts column
 * @method CommonAgentQuery orderByNumCertificat($order = Criteria::ASC) Order by the num_certificat column
 * @method CommonAgentQuery orderByAlerteValidationConsultation($order = Criteria::ASC) Order by the alerte_validation_consultation column
 * @method CommonAgentQuery orderByAlerteChorus($order = Criteria::ASC) Order by the alerte_chorus column
 * @method CommonAgentQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method CommonAgentQuery orderByCodeTheme($order = Criteria::ASC) Order by the code_theme column
 * @method CommonAgentQuery orderByDateConnexion($order = Criteria::ASC) Order by the date_connexion column
 * @method CommonAgentQuery orderByTypeHash($order = Criteria::ASC) Order by the type_hash column
 * @method CommonAgentQuery orderByAlerteMesConsultations($order = Criteria::ASC) Order by the alerte_mes_consultations column
 * @method CommonAgentQuery orderByAlerteConsultationsMonEntite($order = Criteria::ASC) Order by the alerte_consultations_mon_entite column
 * @method CommonAgentQuery orderByAlerteConsultationsDesEntitesDependantes($order = Criteria::ASC) Order by the alerte_consultations_des_entites_dependantes column
 * @method CommonAgentQuery orderByAlerteConsultationsMesEntitesTransverses($order = Criteria::ASC) Order by the alerte_consultations_mes_entites_transverses column
 * @method CommonAgentQuery orderByTechnique($order = Criteria::ASC) Order by the technique column
 * @method CommonAgentQuery orderByServiceId($order = Criteria::ASC) Order by the service_id column
 * @method CommonAgentQuery orderByDateValidationRgpd($order = Criteria::ASC) Order by the date_validation_rgpd column
 * @method CommonAgentQuery orderByRgpdCommunicationPlace($order = Criteria::ASC) Order by the rgpd_communication_place column
 * @method CommonAgentQuery orderByRgpdEnquete($order = Criteria::ASC) Order by the rgpd_enquete column
 *
 * @method CommonAgentQuery groupById() Group by the id column
 * @method CommonAgentQuery groupByLogin() Group by the login column
 * @method CommonAgentQuery groupByEmail() Group by the email column
 * @method CommonAgentQuery groupByMdp() Group by the mdp column
 * @method CommonAgentQuery groupByCertificat() Group by the certificat column
 * @method CommonAgentQuery groupByNom() Group by the nom column
 * @method CommonAgentQuery groupByPrenom() Group by the prenom column
 * @method CommonAgentQuery groupByTentativesMdp() Group by the tentatives_mdp column
 * @method CommonAgentQuery groupByOrganisme() Group by the organisme column
 * @method CommonAgentQuery groupByOldServiceId() Group by the old_service_id column
 * @method CommonAgentQuery groupByRecevoirMail() Group by the RECEVOIR_MAIL column
 * @method CommonAgentQuery groupByElu() Group by the elu column
 * @method CommonAgentQuery groupByNomFonction() Group by the nom_fonction column
 * @method CommonAgentQuery groupByNumTel() Group by the num_tel column
 * @method CommonAgentQuery groupByNumFax() Group by the num_fax column
 * @method CommonAgentQuery groupByTypeComm() Group by the type_comm column
 * @method CommonAgentQuery groupByAdrPostale() Group by the adr_postale column
 * @method CommonAgentQuery groupByCivilite() Group by the civilite column
 * @method CommonAgentQuery groupByAlerteReponseElectronique() Group by the alerte_reponse_electronique column
 * @method CommonAgentQuery groupByAlerteClotureConsultation() Group by the alerte_cloture_consultation column
 * @method CommonAgentQuery groupByAlerteReceptionMessage() Group by the alerte_reception_message column
 * @method CommonAgentQuery groupByAlertePublicationBoamp() Group by the alerte_publication_boamp column
 * @method CommonAgentQuery groupByAlerteEchecPublicationBoamp() Group by the alerte_echec_publication_boamp column
 * @method CommonAgentQuery groupByAlerteCreationModificationAgent() Group by the alerte_creation_modification_agent column
 * @method CommonAgentQuery groupByDateCreation() Group by the date_creation column
 * @method CommonAgentQuery groupByDateModification() Group by the date_modification column
 * @method CommonAgentQuery groupByIdExterne() Group by the id_externe column
 * @method CommonAgentQuery groupByIdProfilSocleExterne() Group by the id_profil_socle_externe column
 * @method CommonAgentQuery groupByLieuExecution() Group by the lieu_execution column
 * @method CommonAgentQuery groupByAlerteQuestionEntreprise() Group by the alerte_question_entreprise column
 * @method CommonAgentQuery groupByActif() Group by the actif column
 * @method CommonAgentQuery groupByDeletedAt() Group by the deleted_at column
 * @method CommonAgentQuery groupByCodesNuts() Group by the codes_nuts column
 * @method CommonAgentQuery groupByNumCertificat() Group by the num_certificat column
 * @method CommonAgentQuery groupByAlerteValidationConsultation() Group by the alerte_validation_consultation column
 * @method CommonAgentQuery groupByAlerteChorus() Group by the alerte_chorus column
 * @method CommonAgentQuery groupByPassword() Group by the password column
 * @method CommonAgentQuery groupByCodeTheme() Group by the code_theme column
 * @method CommonAgentQuery groupByDateConnexion() Group by the date_connexion column
 * @method CommonAgentQuery groupByTypeHash() Group by the type_hash column
 * @method CommonAgentQuery groupByAlerteMesConsultations() Group by the alerte_mes_consultations column
 * @method CommonAgentQuery groupByAlerteConsultationsMonEntite() Group by the alerte_consultations_mon_entite column
 * @method CommonAgentQuery groupByAlerteConsultationsDesEntitesDependantes() Group by the alerte_consultations_des_entites_dependantes column
 * @method CommonAgentQuery groupByAlerteConsultationsMesEntitesTransverses() Group by the alerte_consultations_mes_entites_transverses column
 * @method CommonAgentQuery groupByTechnique() Group by the technique column
 * @method CommonAgentQuery groupByServiceId() Group by the service_id column
 * @method CommonAgentQuery groupByDateValidationRgpd() Group by the date_validation_rgpd column
 * @method CommonAgentQuery groupByRgpdCommunicationPlace() Group by the rgpd_communication_place column
 * @method CommonAgentQuery groupByRgpdEnquete() Group by the rgpd_enquete column
 *
 * @method CommonAgentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonAgentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonAgentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonAgentQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonAgentQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonAgentQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonAgentQuery leftJoinCommonService($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonService relation
 * @method CommonAgentQuery rightJoinCommonService($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonService relation
 * @method CommonAgentQuery innerJoinCommonService($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonService relation
 *
 * @method CommonAgentQuery leftJoinCommonAgentServiceMetier($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgentServiceMetier relation
 * @method CommonAgentQuery rightJoinCommonAgentServiceMetier($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgentServiceMetier relation
 * @method CommonAgentQuery innerJoinCommonAgentServiceMetier($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgentServiceMetier relation
 *
 * @method CommonAgentQuery leftJoinCommonHabilitationAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonHabilitationAgent relation
 * @method CommonAgentQuery rightJoinCommonHabilitationAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonHabilitationAgent relation
 * @method CommonAgentQuery innerJoinCommonHabilitationAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonHabilitationAgent relation
 *
 * @method CommonAgentQuery leftJoinCommonAgentTechniqueToken($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgentTechniqueToken relation
 * @method CommonAgentQuery rightJoinCommonAgentTechniqueToken($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgentTechniqueToken relation
 * @method CommonAgentQuery innerJoinCommonAgentTechniqueToken($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgentTechniqueToken relation
 *
 * @method CommonAgentQuery leftJoinCommonAnnexeFinanciere($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAnnexeFinanciere relation
 * @method CommonAgentQuery rightJoinCommonAnnexeFinanciere($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAnnexeFinanciere relation
 * @method CommonAgentQuery innerJoinCommonAnnexeFinanciere($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAnnexeFinanciere relation
 *
 * @method CommonAgentQuery leftJoinCommonConsultationFavoris($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationFavoris relation
 * @method CommonAgentQuery rightJoinCommonConsultationFavoris($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationFavoris relation
 * @method CommonAgentQuery innerJoinCommonConsultationFavoris($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationFavoris relation
 *
 * @method CommonAgentQuery leftJoinCommonDossierVolumineux($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonDossierVolumineux relation
 * @method CommonAgentQuery rightJoinCommonDossierVolumineux($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonDossierVolumineux relation
 * @method CommonAgentQuery innerJoinCommonDossierVolumineux($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonDossierVolumineux relation
 *
 * @method CommonAgentQuery leftJoinCommonEchangeDoc($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDoc relation
 * @method CommonAgentQuery rightJoinCommonEchangeDoc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDoc relation
 * @method CommonAgentQuery innerJoinCommonEchangeDoc($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDoc relation
 *
 * @method CommonAgentQuery leftJoinCommonEchangeDocHistorique($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocHistorique relation
 * @method CommonAgentQuery rightJoinCommonEchangeDocHistorique($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocHistorique relation
 * @method CommonAgentQuery innerJoinCommonEchangeDocHistorique($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocHistorique relation
 *
 * @method CommonAgentQuery leftJoinCommonHabilitationAgentWs($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonHabilitationAgentWs relation
 * @method CommonAgentQuery rightJoinCommonHabilitationAgentWs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonHabilitationAgentWs relation
 * @method CommonAgentQuery innerJoinCommonHabilitationAgentWs($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonHabilitationAgentWs relation
 *
 * @method CommonAgentQuery leftJoinCommonInvitePermanentTransverse($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonInvitePermanentTransverse relation
 * @method CommonAgentQuery rightJoinCommonInvitePermanentTransverse($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonInvitePermanentTransverse relation
 * @method CommonAgentQuery innerJoinCommonInvitePermanentTransverse($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonInvitePermanentTransverse relation
 *
 * @method CommonAgentQuery leftJoinCommonModificationContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonModificationContrat relation
 * @method CommonAgentQuery rightJoinCommonModificationContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonModificationContrat relation
 * @method CommonAgentQuery innerJoinCommonModificationContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonModificationContrat relation
 *
 * @method CommonAgentQuery leftJoinCommonTCAOCommissionAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCAOCommissionAgent relation
 * @method CommonAgentQuery rightJoinCommonTCAOCommissionAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCAOCommissionAgent relation
 * @method CommonAgentQuery innerJoinCommonTCAOCommissionAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCAOCommissionAgent relation
 *
 * @method CommonAgentQuery leftJoinCommonTCAOOrdreDuJourIntervenant($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCAOOrdreDuJourIntervenant relation
 * @method CommonAgentQuery rightJoinCommonTCAOOrdreDuJourIntervenant($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCAOOrdreDuJourIntervenant relation
 * @method CommonAgentQuery innerJoinCommonTCAOOrdreDuJourIntervenant($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCAOOrdreDuJourIntervenant relation
 *
 * @method CommonAgentQuery leftJoinCommonTCAOSeanceAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCAOSeanceAgent relation
 * @method CommonAgentQuery rightJoinCommonTCAOSeanceAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCAOSeanceAgent relation
 * @method CommonAgentQuery innerJoinCommonTCAOSeanceAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCAOSeanceAgent relation
 *
 * @method CommonAgentQuery leftJoinCommonTCAOSeanceInvite($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCAOSeanceInvite relation
 * @method CommonAgentQuery rightJoinCommonTCAOSeanceInvite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCAOSeanceInvite relation
 * @method CommonAgentQuery innerJoinCommonTCAOSeanceInvite($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCAOSeanceInvite relation
 *
 * @method CommonAgentQuery leftJoinCommonTVisionRmaAgentOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTVisionRmaAgentOrganisme relation
 * @method CommonAgentQuery rightJoinCommonTVisionRmaAgentOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTVisionRmaAgentOrganisme relation
 * @method CommonAgentQuery innerJoinCommonTVisionRmaAgentOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTVisionRmaAgentOrganisme relation
 *
 * @method CommonAgent findOne(PropelPDO $con = null) Return the first CommonAgent matching the query
 * @method CommonAgent findOneOrCreate(PropelPDO $con = null) Return the first CommonAgent matching the query, or a new CommonAgent object populated from the query conditions when no match is found
 *
 * @method CommonAgent findOneByLogin(string $login) Return the first CommonAgent filtered by the login column
 * @method CommonAgent findOneByEmail(string $email) Return the first CommonAgent filtered by the email column
 * @method CommonAgent findOneByMdp(string $mdp) Return the first CommonAgent filtered by the mdp column
 * @method CommonAgent findOneByCertificat(string $certificat) Return the first CommonAgent filtered by the certificat column
 * @method CommonAgent findOneByNom(string $nom) Return the first CommonAgent filtered by the nom column
 * @method CommonAgent findOneByPrenom(string $prenom) Return the first CommonAgent filtered by the prenom column
 * @method CommonAgent findOneByTentativesMdp(int $tentatives_mdp) Return the first CommonAgent filtered by the tentatives_mdp column
 * @method CommonAgent findOneByOrganisme(string $organisme) Return the first CommonAgent filtered by the organisme column
 * @method CommonAgent findOneByOldServiceId(int $old_service_id) Return the first CommonAgent filtered by the old_service_id column
 * @method CommonAgent findOneByRecevoirMail(string $RECEVOIR_MAIL) Return the first CommonAgent filtered by the RECEVOIR_MAIL column
 * @method CommonAgent findOneByElu(string $elu) Return the first CommonAgent filtered by the elu column
 * @method CommonAgent findOneByNomFonction(string $nom_fonction) Return the first CommonAgent filtered by the nom_fonction column
 * @method CommonAgent findOneByNumTel(string $num_tel) Return the first CommonAgent filtered by the num_tel column
 * @method CommonAgent findOneByNumFax(string $num_fax) Return the first CommonAgent filtered by the num_fax column
 * @method CommonAgent findOneByTypeComm(string $type_comm) Return the first CommonAgent filtered by the type_comm column
 * @method CommonAgent findOneByAdrPostale(string $adr_postale) Return the first CommonAgent filtered by the adr_postale column
 * @method CommonAgent findOneByCivilite(string $civilite) Return the first CommonAgent filtered by the civilite column
 * @method CommonAgent findOneByAlerteReponseElectronique(string $alerte_reponse_electronique) Return the first CommonAgent filtered by the alerte_reponse_electronique column
 * @method CommonAgent findOneByAlerteClotureConsultation(string $alerte_cloture_consultation) Return the first CommonAgent filtered by the alerte_cloture_consultation column
 * @method CommonAgent findOneByAlerteReceptionMessage(string $alerte_reception_message) Return the first CommonAgent filtered by the alerte_reception_message column
 * @method CommonAgent findOneByAlertePublicationBoamp(string $alerte_publication_boamp) Return the first CommonAgent filtered by the alerte_publication_boamp column
 * @method CommonAgent findOneByAlerteEchecPublicationBoamp(string $alerte_echec_publication_boamp) Return the first CommonAgent filtered by the alerte_echec_publication_boamp column
 * @method CommonAgent findOneByAlerteCreationModificationAgent(string $alerte_creation_modification_agent) Return the first CommonAgent filtered by the alerte_creation_modification_agent column
 * @method CommonAgent findOneByDateCreation(string $date_creation) Return the first CommonAgent filtered by the date_creation column
 * @method CommonAgent findOneByDateModification(string $date_modification) Return the first CommonAgent filtered by the date_modification column
 * @method CommonAgent findOneByIdExterne(string $id_externe) Return the first CommonAgent filtered by the id_externe column
 * @method CommonAgent findOneByIdProfilSocleExterne(int $id_profil_socle_externe) Return the first CommonAgent filtered by the id_profil_socle_externe column
 * @method CommonAgent findOneByLieuExecution(string $lieu_execution) Return the first CommonAgent filtered by the lieu_execution column
 * @method CommonAgent findOneByAlerteQuestionEntreprise(string $alerte_question_entreprise) Return the first CommonAgent filtered by the alerte_question_entreprise column
 * @method CommonAgent findOneByActif(string $actif) Return the first CommonAgent filtered by the actif column
 * @method CommonAgent findOneByDeletedAt(string $deleted_at) Return the first CommonAgent filtered by the deleted_at column
 * @method CommonAgent findOneByCodesNuts(string $codes_nuts) Return the first CommonAgent filtered by the codes_nuts column
 * @method CommonAgent findOneByNumCertificat(string $num_certificat) Return the first CommonAgent filtered by the num_certificat column
 * @method CommonAgent findOneByAlerteValidationConsultation(string $alerte_validation_consultation) Return the first CommonAgent filtered by the alerte_validation_consultation column
 * @method CommonAgent findOneByAlerteChorus(string $alerte_chorus) Return the first CommonAgent filtered by the alerte_chorus column
 * @method CommonAgent findOneByPassword(string $password) Return the first CommonAgent filtered by the password column
 * @method CommonAgent findOneByCodeTheme(string $code_theme) Return the first CommonAgent filtered by the code_theme column
 * @method CommonAgent findOneByDateConnexion(string $date_connexion) Return the first CommonAgent filtered by the date_connexion column
 * @method CommonAgent findOneByTypeHash(string $type_hash) Return the first CommonAgent filtered by the type_hash column
 * @method CommonAgent findOneByAlerteMesConsultations(string $alerte_mes_consultations) Return the first CommonAgent filtered by the alerte_mes_consultations column
 * @method CommonAgent findOneByAlerteConsultationsMonEntite(string $alerte_consultations_mon_entite) Return the first CommonAgent filtered by the alerte_consultations_mon_entite column
 * @method CommonAgent findOneByAlerteConsultationsDesEntitesDependantes(string $alerte_consultations_des_entites_dependantes) Return the first CommonAgent filtered by the alerte_consultations_des_entites_dependantes column
 * @method CommonAgent findOneByAlerteConsultationsMesEntitesTransverses(string $alerte_consultations_mes_entites_transverses) Return the first CommonAgent filtered by the alerte_consultations_mes_entites_transverses column
 * @method CommonAgent findOneByTechnique(boolean $technique) Return the first CommonAgent filtered by the technique column
 * @method CommonAgent findOneByServiceId(string $service_id) Return the first CommonAgent filtered by the service_id column
 * @method CommonAgent findOneByDateValidationRgpd(string $date_validation_rgpd) Return the first CommonAgent filtered by the date_validation_rgpd column
 * @method CommonAgent findOneByRgpdCommunicationPlace(boolean $rgpd_communication_place) Return the first CommonAgent filtered by the rgpd_communication_place column
 * @method CommonAgent findOneByRgpdEnquete(boolean $rgpd_enquete) Return the first CommonAgent filtered by the rgpd_enquete column
 *
 * @method array findById(int $id) Return CommonAgent objects filtered by the id column
 * @method array findByLogin(string $login) Return CommonAgent objects filtered by the login column
 * @method array findByEmail(string $email) Return CommonAgent objects filtered by the email column
 * @method array findByMdp(string $mdp) Return CommonAgent objects filtered by the mdp column
 * @method array findByCertificat(string $certificat) Return CommonAgent objects filtered by the certificat column
 * @method array findByNom(string $nom) Return CommonAgent objects filtered by the nom column
 * @method array findByPrenom(string $prenom) Return CommonAgent objects filtered by the prenom column
 * @method array findByTentativesMdp(int $tentatives_mdp) Return CommonAgent objects filtered by the tentatives_mdp column
 * @method array findByOrganisme(string $organisme) Return CommonAgent objects filtered by the organisme column
 * @method array findByOldServiceId(int $old_service_id) Return CommonAgent objects filtered by the old_service_id column
 * @method array findByRecevoirMail(string $RECEVOIR_MAIL) Return CommonAgent objects filtered by the RECEVOIR_MAIL column
 * @method array findByElu(string $elu) Return CommonAgent objects filtered by the elu column
 * @method array findByNomFonction(string $nom_fonction) Return CommonAgent objects filtered by the nom_fonction column
 * @method array findByNumTel(string $num_tel) Return CommonAgent objects filtered by the num_tel column
 * @method array findByNumFax(string $num_fax) Return CommonAgent objects filtered by the num_fax column
 * @method array findByTypeComm(string $type_comm) Return CommonAgent objects filtered by the type_comm column
 * @method array findByAdrPostale(string $adr_postale) Return CommonAgent objects filtered by the adr_postale column
 * @method array findByCivilite(string $civilite) Return CommonAgent objects filtered by the civilite column
 * @method array findByAlerteReponseElectronique(string $alerte_reponse_electronique) Return CommonAgent objects filtered by the alerte_reponse_electronique column
 * @method array findByAlerteClotureConsultation(string $alerte_cloture_consultation) Return CommonAgent objects filtered by the alerte_cloture_consultation column
 * @method array findByAlerteReceptionMessage(string $alerte_reception_message) Return CommonAgent objects filtered by the alerte_reception_message column
 * @method array findByAlertePublicationBoamp(string $alerte_publication_boamp) Return CommonAgent objects filtered by the alerte_publication_boamp column
 * @method array findByAlerteEchecPublicationBoamp(string $alerte_echec_publication_boamp) Return CommonAgent objects filtered by the alerte_echec_publication_boamp column
 * @method array findByAlerteCreationModificationAgent(string $alerte_creation_modification_agent) Return CommonAgent objects filtered by the alerte_creation_modification_agent column
 * @method array findByDateCreation(string $date_creation) Return CommonAgent objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonAgent objects filtered by the date_modification column
 * @method array findByIdExterne(string $id_externe) Return CommonAgent objects filtered by the id_externe column
 * @method array findByIdProfilSocleExterne(int $id_profil_socle_externe) Return CommonAgent objects filtered by the id_profil_socle_externe column
 * @method array findByLieuExecution(string $lieu_execution) Return CommonAgent objects filtered by the lieu_execution column
 * @method array findByAlerteQuestionEntreprise(string $alerte_question_entreprise) Return CommonAgent objects filtered by the alerte_question_entreprise column
 * @method array findByActif(string $actif) Return CommonAgent objects filtered by the actif column
 * @method array findByDeletedAt(string $deleted_at) Return CommonAgent objects filtered by the deleted_at column
 * @method array findByCodesNuts(string $codes_nuts) Return CommonAgent objects filtered by the codes_nuts column
 * @method array findByNumCertificat(string $num_certificat) Return CommonAgent objects filtered by the num_certificat column
 * @method array findByAlerteValidationConsultation(string $alerte_validation_consultation) Return CommonAgent objects filtered by the alerte_validation_consultation column
 * @method array findByAlerteChorus(string $alerte_chorus) Return CommonAgent objects filtered by the alerte_chorus column
 * @method array findByPassword(string $password) Return CommonAgent objects filtered by the password column
 * @method array findByCodeTheme(string $code_theme) Return CommonAgent objects filtered by the code_theme column
 * @method array findByDateConnexion(string $date_connexion) Return CommonAgent objects filtered by the date_connexion column
 * @method array findByTypeHash(string $type_hash) Return CommonAgent objects filtered by the type_hash column
 * @method array findByAlerteMesConsultations(string $alerte_mes_consultations) Return CommonAgent objects filtered by the alerte_mes_consultations column
 * @method array findByAlerteConsultationsMonEntite(string $alerte_consultations_mon_entite) Return CommonAgent objects filtered by the alerte_consultations_mon_entite column
 * @method array findByAlerteConsultationsDesEntitesDependantes(string $alerte_consultations_des_entites_dependantes) Return CommonAgent objects filtered by the alerte_consultations_des_entites_dependantes column
 * @method array findByAlerteConsultationsMesEntitesTransverses(string $alerte_consultations_mes_entites_transverses) Return CommonAgent objects filtered by the alerte_consultations_mes_entites_transverses column
 * @method array findByTechnique(boolean $technique) Return CommonAgent objects filtered by the technique column
 * @method array findByServiceId(string $service_id) Return CommonAgent objects filtered by the service_id column
 * @method array findByDateValidationRgpd(string $date_validation_rgpd) Return CommonAgent objects filtered by the date_validation_rgpd column
 * @method array findByRgpdCommunicationPlace(boolean $rgpd_communication_place) Return CommonAgent objects filtered by the rgpd_communication_place column
 * @method array findByRgpdEnquete(boolean $rgpd_enquete) Return CommonAgent objects filtered by the rgpd_enquete column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonAgentQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonAgentQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonAgent', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonAgentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonAgentQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonAgentQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonAgentQuery) {
            return $criteria;
        }
        $query = new CommonAgentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonAgent|CommonAgent[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonAgentPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonAgentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonAgent A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonAgent A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `login`, `email`, `mdp`, `certificat`, `nom`, `prenom`, `tentatives_mdp`, `organisme`, `old_service_id`, `RECEVOIR_MAIL`, `elu`, `nom_fonction`, `num_tel`, `num_fax`, `type_comm`, `adr_postale`, `civilite`, `alerte_reponse_electronique`, `alerte_cloture_consultation`, `alerte_reception_message`, `alerte_publication_boamp`, `alerte_echec_publication_boamp`, `alerte_creation_modification_agent`, `date_creation`, `date_modification`, `id_externe`, `id_profil_socle_externe`, `lieu_execution`, `alerte_question_entreprise`, `actif`, `deleted_at`, `codes_nuts`, `num_certificat`, `alerte_validation_consultation`, `alerte_chorus`, `password`, `code_theme`, `date_connexion`, `type_hash`, `alerte_mes_consultations`, `alerte_consultations_mon_entite`, `alerte_consultations_des_entites_dependantes`, `alerte_consultations_mes_entites_transverses`, `technique`, `service_id`, `date_validation_rgpd`, `rgpd_communication_place`, `rgpd_enquete` FROM `Agent` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonAgent();
            $obj->hydrate($row);
            CommonAgentPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonAgent|CommonAgent[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonAgent[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonAgentPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonAgentPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonAgentPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonAgentPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the login column
     *
     * Example usage:
     * <code>
     * $query->filterByLogin('fooValue');   // WHERE login = 'fooValue'
     * $query->filterByLogin('%fooValue%'); // WHERE login LIKE '%fooValue%'
     * </code>
     *
     * @param     string $login The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByLogin($login = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($login)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $login)) {
                $login = str_replace('*', '%', $login);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::LOGIN, $login, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the mdp column
     *
     * Example usage:
     * <code>
     * $query->filterByMdp('fooValue');   // WHERE mdp = 'fooValue'
     * $query->filterByMdp('%fooValue%'); // WHERE mdp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mdp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByMdp($mdp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mdp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mdp)) {
                $mdp = str_replace('*', '%', $mdp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::MDP, $mdp, $comparison);
    }

    /**
     * Filter the query on the certificat column
     *
     * Example usage:
     * <code>
     * $query->filterByCertificat('fooValue');   // WHERE certificat = 'fooValue'
     * $query->filterByCertificat('%fooValue%'); // WHERE certificat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $certificat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByCertificat($certificat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($certificat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $certificat)) {
                $certificat = str_replace('*', '%', $certificat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::CERTIFICAT, $certificat, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%'); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nom)) {
                $nom = str_replace('*', '%', $nom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the prenom column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenom('fooValue');   // WHERE prenom = 'fooValue'
     * $query->filterByPrenom('%fooValue%'); // WHERE prenom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByPrenom($prenom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prenom)) {
                $prenom = str_replace('*', '%', $prenom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::PRENOM, $prenom, $comparison);
    }

    /**
     * Filter the query on the tentatives_mdp column
     *
     * Example usage:
     * <code>
     * $query->filterByTentativesMdp(1234); // WHERE tentatives_mdp = 1234
     * $query->filterByTentativesMdp(array(12, 34)); // WHERE tentatives_mdp IN (12, 34)
     * $query->filterByTentativesMdp(array('min' => 12)); // WHERE tentatives_mdp >= 12
     * $query->filterByTentativesMdp(array('max' => 12)); // WHERE tentatives_mdp <= 12
     * </code>
     *
     * @param     mixed $tentativesMdp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByTentativesMdp($tentativesMdp = null, $comparison = null)
    {
        if (is_array($tentativesMdp)) {
            $useMinMax = false;
            if (isset($tentativesMdp['min'])) {
                $this->addUsingAlias(CommonAgentPeer::TENTATIVES_MDP, $tentativesMdp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tentativesMdp['max'])) {
                $this->addUsingAlias(CommonAgentPeer::TENTATIVES_MDP, $tentativesMdp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::TENTATIVES_MDP, $tentativesMdp, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldServiceId(1234); // WHERE old_service_id = 1234
     * $query->filterByOldServiceId(array(12, 34)); // WHERE old_service_id IN (12, 34)
     * $query->filterByOldServiceId(array('min' => 12)); // WHERE old_service_id >= 12
     * $query->filterByOldServiceId(array('max' => 12)); // WHERE old_service_id <= 12
     * </code>
     *
     * @param     mixed $oldServiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByOldServiceId($oldServiceId = null, $comparison = null)
    {
        if (is_array($oldServiceId)) {
            $useMinMax = false;
            if (isset($oldServiceId['min'])) {
                $this->addUsingAlias(CommonAgentPeer::OLD_SERVICE_ID, $oldServiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldServiceId['max'])) {
                $this->addUsingAlias(CommonAgentPeer::OLD_SERVICE_ID, $oldServiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::OLD_SERVICE_ID, $oldServiceId, $comparison);
    }

    /**
     * Filter the query on the RECEVOIR_MAIL column
     *
     * Example usage:
     * <code>
     * $query->filterByRecevoirMail('fooValue');   // WHERE RECEVOIR_MAIL = 'fooValue'
     * $query->filterByRecevoirMail('%fooValue%'); // WHERE RECEVOIR_MAIL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $recevoirMail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByRecevoirMail($recevoirMail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($recevoirMail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $recevoirMail)) {
                $recevoirMail = str_replace('*', '%', $recevoirMail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::RECEVOIR_MAIL, $recevoirMail, $comparison);
    }

    /**
     * Filter the query on the elu column
     *
     * Example usage:
     * <code>
     * $query->filterByElu('fooValue');   // WHERE elu = 'fooValue'
     * $query->filterByElu('%fooValue%'); // WHERE elu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $elu The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByElu($elu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($elu)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $elu)) {
                $elu = str_replace('*', '%', $elu);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ELU, $elu, $comparison);
    }

    /**
     * Filter the query on the nom_fonction column
     *
     * Example usage:
     * <code>
     * $query->filterByNomFonction('fooValue');   // WHERE nom_fonction = 'fooValue'
     * $query->filterByNomFonction('%fooValue%'); // WHERE nom_fonction LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomFonction The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByNomFonction($nomFonction = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomFonction)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomFonction)) {
                $nomFonction = str_replace('*', '%', $nomFonction);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::NOM_FONCTION, $nomFonction, $comparison);
    }

    /**
     * Filter the query on the num_tel column
     *
     * Example usage:
     * <code>
     * $query->filterByNumTel('fooValue');   // WHERE num_tel = 'fooValue'
     * $query->filterByNumTel('%fooValue%'); // WHERE num_tel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numTel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByNumTel($numTel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numTel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numTel)) {
                $numTel = str_replace('*', '%', $numTel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::NUM_TEL, $numTel, $comparison);
    }

    /**
     * Filter the query on the num_fax column
     *
     * Example usage:
     * <code>
     * $query->filterByNumFax('fooValue');   // WHERE num_fax = 'fooValue'
     * $query->filterByNumFax('%fooValue%'); // WHERE num_fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numFax The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByNumFax($numFax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numFax)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numFax)) {
                $numFax = str_replace('*', '%', $numFax);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::NUM_FAX, $numFax, $comparison);
    }

    /**
     * Filter the query on the type_comm column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeComm('fooValue');   // WHERE type_comm = 'fooValue'
     * $query->filterByTypeComm('%fooValue%'); // WHERE type_comm LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeComm The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByTypeComm($typeComm = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeComm)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeComm)) {
                $typeComm = str_replace('*', '%', $typeComm);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::TYPE_COMM, $typeComm, $comparison);
    }

    /**
     * Filter the query on the adr_postale column
     *
     * Example usage:
     * <code>
     * $query->filterByAdrPostale('fooValue');   // WHERE adr_postale = 'fooValue'
     * $query->filterByAdrPostale('%fooValue%'); // WHERE adr_postale LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adrPostale The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAdrPostale($adrPostale = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adrPostale)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adrPostale)) {
                $adrPostale = str_replace('*', '%', $adrPostale);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ADR_POSTALE, $adrPostale, $comparison);
    }

    /**
     * Filter the query on the civilite column
     *
     * Example usage:
     * <code>
     * $query->filterByCivilite('fooValue');   // WHERE civilite = 'fooValue'
     * $query->filterByCivilite('%fooValue%'); // WHERE civilite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $civilite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByCivilite($civilite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($civilite)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $civilite)) {
                $civilite = str_replace('*', '%', $civilite);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::CIVILITE, $civilite, $comparison);
    }

    /**
     * Filter the query on the alerte_reponse_electronique column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteReponseElectronique('fooValue');   // WHERE alerte_reponse_electronique = 'fooValue'
     * $query->filterByAlerteReponseElectronique('%fooValue%'); // WHERE alerte_reponse_electronique LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteReponseElectronique The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteReponseElectronique($alerteReponseElectronique = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteReponseElectronique)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteReponseElectronique)) {
                $alerteReponseElectronique = str_replace('*', '%', $alerteReponseElectronique);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_REPONSE_ELECTRONIQUE, $alerteReponseElectronique, $comparison);
    }

    /**
     * Filter the query on the alerte_cloture_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteClotureConsultation('fooValue');   // WHERE alerte_cloture_consultation = 'fooValue'
     * $query->filterByAlerteClotureConsultation('%fooValue%'); // WHERE alerte_cloture_consultation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteClotureConsultation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteClotureConsultation($alerteClotureConsultation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteClotureConsultation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteClotureConsultation)) {
                $alerteClotureConsultation = str_replace('*', '%', $alerteClotureConsultation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_CLOTURE_CONSULTATION, $alerteClotureConsultation, $comparison);
    }

    /**
     * Filter the query on the alerte_reception_message column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteReceptionMessage('fooValue');   // WHERE alerte_reception_message = 'fooValue'
     * $query->filterByAlerteReceptionMessage('%fooValue%'); // WHERE alerte_reception_message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteReceptionMessage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteReceptionMessage($alerteReceptionMessage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteReceptionMessage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteReceptionMessage)) {
                $alerteReceptionMessage = str_replace('*', '%', $alerteReceptionMessage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_RECEPTION_MESSAGE, $alerteReceptionMessage, $comparison);
    }

    /**
     * Filter the query on the alerte_publication_boamp column
     *
     * Example usage:
     * <code>
     * $query->filterByAlertePublicationBoamp('fooValue');   // WHERE alerte_publication_boamp = 'fooValue'
     * $query->filterByAlertePublicationBoamp('%fooValue%'); // WHERE alerte_publication_boamp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alertePublicationBoamp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlertePublicationBoamp($alertePublicationBoamp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alertePublicationBoamp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alertePublicationBoamp)) {
                $alertePublicationBoamp = str_replace('*', '%', $alertePublicationBoamp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_PUBLICATION_BOAMP, $alertePublicationBoamp, $comparison);
    }

    /**
     * Filter the query on the alerte_echec_publication_boamp column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteEchecPublicationBoamp('fooValue');   // WHERE alerte_echec_publication_boamp = 'fooValue'
     * $query->filterByAlerteEchecPublicationBoamp('%fooValue%'); // WHERE alerte_echec_publication_boamp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteEchecPublicationBoamp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteEchecPublicationBoamp($alerteEchecPublicationBoamp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteEchecPublicationBoamp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteEchecPublicationBoamp)) {
                $alerteEchecPublicationBoamp = str_replace('*', '%', $alerteEchecPublicationBoamp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_ECHEC_PUBLICATION_BOAMP, $alerteEchecPublicationBoamp, $comparison);
    }

    /**
     * Filter the query on the alerte_creation_modification_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteCreationModificationAgent('fooValue');   // WHERE alerte_creation_modification_agent = 'fooValue'
     * $query->filterByAlerteCreationModificationAgent('%fooValue%'); // WHERE alerte_creation_modification_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteCreationModificationAgent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteCreationModificationAgent($alerteCreationModificationAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteCreationModificationAgent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteCreationModificationAgent)) {
                $alerteCreationModificationAgent = str_replace('*', '%', $alerteCreationModificationAgent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_CREATION_MODIFICATION_AGENT, $alerteCreationModificationAgent, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('fooValue');   // WHERE date_creation = 'fooValue'
     * $query->filterByDateCreation('%fooValue%'); // WHERE date_creation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dateCreation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dateCreation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dateCreation)) {
                $dateCreation = str_replace('*', '%', $dateCreation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('fooValue');   // WHERE date_modification = 'fooValue'
     * $query->filterByDateModification('%fooValue%'); // WHERE date_modification LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dateModification The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dateModification)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dateModification)) {
                $dateModification = str_replace('*', '%', $dateModification);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the id_externe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdExterne('fooValue');   // WHERE id_externe = 'fooValue'
     * $query->filterByIdExterne('%fooValue%'); // WHERE id_externe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idExterne The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByIdExterne($idExterne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idExterne)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idExterne)) {
                $idExterne = str_replace('*', '%', $idExterne);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ID_EXTERNE, $idExterne, $comparison);
    }

    /**
     * Filter the query on the id_profil_socle_externe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdProfilSocleExterne(1234); // WHERE id_profil_socle_externe = 1234
     * $query->filterByIdProfilSocleExterne(array(12, 34)); // WHERE id_profil_socle_externe IN (12, 34)
     * $query->filterByIdProfilSocleExterne(array('min' => 12)); // WHERE id_profil_socle_externe >= 12
     * $query->filterByIdProfilSocleExterne(array('max' => 12)); // WHERE id_profil_socle_externe <= 12
     * </code>
     *
     * @param     mixed $idProfilSocleExterne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByIdProfilSocleExterne($idProfilSocleExterne = null, $comparison = null)
    {
        if (is_array($idProfilSocleExterne)) {
            $useMinMax = false;
            if (isset($idProfilSocleExterne['min'])) {
                $this->addUsingAlias(CommonAgentPeer::ID_PROFIL_SOCLE_EXTERNE, $idProfilSocleExterne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idProfilSocleExterne['max'])) {
                $this->addUsingAlias(CommonAgentPeer::ID_PROFIL_SOCLE_EXTERNE, $idProfilSocleExterne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ID_PROFIL_SOCLE_EXTERNE, $idProfilSocleExterne, $comparison);
    }

    /**
     * Filter the query on the lieu_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByLieuExecution('fooValue');   // WHERE lieu_execution = 'fooValue'
     * $query->filterByLieuExecution('%fooValue%'); // WHERE lieu_execution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lieuExecution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByLieuExecution($lieuExecution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lieuExecution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lieuExecution)) {
                $lieuExecution = str_replace('*', '%', $lieuExecution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::LIEU_EXECUTION, $lieuExecution, $comparison);
    }

    /**
     * Filter the query on the alerte_question_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteQuestionEntreprise('fooValue');   // WHERE alerte_question_entreprise = 'fooValue'
     * $query->filterByAlerteQuestionEntreprise('%fooValue%'); // WHERE alerte_question_entreprise LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteQuestionEntreprise The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteQuestionEntreprise($alerteQuestionEntreprise = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteQuestionEntreprise)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteQuestionEntreprise)) {
                $alerteQuestionEntreprise = str_replace('*', '%', $alerteQuestionEntreprise);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_QUESTION_ENTREPRISE, $alerteQuestionEntreprise, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif('fooValue');   // WHERE actif = 'fooValue'
     * $query->filterByActif('%fooValue%'); // WHERE actif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $actif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($actif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $actif)) {
                $actif = str_replace('*', '%', $actif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(CommonAgentPeer::DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(CommonAgentPeer::DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the codes_nuts column
     *
     * Example usage:
     * <code>
     * $query->filterByCodesNuts('fooValue');   // WHERE codes_nuts = 'fooValue'
     * $query->filterByCodesNuts('%fooValue%'); // WHERE codes_nuts LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codesNuts The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByCodesNuts($codesNuts = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codesNuts)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codesNuts)) {
                $codesNuts = str_replace('*', '%', $codesNuts);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::CODES_NUTS, $codesNuts, $comparison);
    }

    /**
     * Filter the query on the num_certificat column
     *
     * Example usage:
     * <code>
     * $query->filterByNumCertificat('fooValue');   // WHERE num_certificat = 'fooValue'
     * $query->filterByNumCertificat('%fooValue%'); // WHERE num_certificat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numCertificat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByNumCertificat($numCertificat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numCertificat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numCertificat)) {
                $numCertificat = str_replace('*', '%', $numCertificat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::NUM_CERTIFICAT, $numCertificat, $comparison);
    }

    /**
     * Filter the query on the alerte_validation_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteValidationConsultation('fooValue');   // WHERE alerte_validation_consultation = 'fooValue'
     * $query->filterByAlerteValidationConsultation('%fooValue%'); // WHERE alerte_validation_consultation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteValidationConsultation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteValidationConsultation($alerteValidationConsultation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteValidationConsultation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteValidationConsultation)) {
                $alerteValidationConsultation = str_replace('*', '%', $alerteValidationConsultation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_VALIDATION_CONSULTATION, $alerteValidationConsultation, $comparison);
    }

    /**
     * Filter the query on the alerte_chorus column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteChorus('fooValue');   // WHERE alerte_chorus = 'fooValue'
     * $query->filterByAlerteChorus('%fooValue%'); // WHERE alerte_chorus LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteChorus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteChorus($alerteChorus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteChorus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteChorus)) {
                $alerteChorus = str_replace('*', '%', $alerteChorus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_CHORUS, $alerteChorus, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the code_theme column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeTheme('fooValue');   // WHERE code_theme = 'fooValue'
     * $query->filterByCodeTheme('%fooValue%'); // WHERE code_theme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeTheme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByCodeTheme($codeTheme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeTheme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeTheme)) {
                $codeTheme = str_replace('*', '%', $codeTheme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::CODE_THEME, $codeTheme, $comparison);
    }

    /**
     * Filter the query on the date_connexion column
     *
     * Example usage:
     * <code>
     * $query->filterByDateConnexion('2011-03-14'); // WHERE date_connexion = '2011-03-14'
     * $query->filterByDateConnexion('now'); // WHERE date_connexion = '2011-03-14'
     * $query->filterByDateConnexion(array('max' => 'yesterday')); // WHERE date_connexion > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateConnexion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByDateConnexion($dateConnexion = null, $comparison = null)
    {
        if (is_array($dateConnexion)) {
            $useMinMax = false;
            if (isset($dateConnexion['min'])) {
                $this->addUsingAlias(CommonAgentPeer::DATE_CONNEXION, $dateConnexion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateConnexion['max'])) {
                $this->addUsingAlias(CommonAgentPeer::DATE_CONNEXION, $dateConnexion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::DATE_CONNEXION, $dateConnexion, $comparison);
    }

    /**
     * Filter the query on the type_hash column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeHash('fooValue');   // WHERE type_hash = 'fooValue'
     * $query->filterByTypeHash('%fooValue%'); // WHERE type_hash LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeHash The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByTypeHash($typeHash = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeHash)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeHash)) {
                $typeHash = str_replace('*', '%', $typeHash);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::TYPE_HASH, $typeHash, $comparison);
    }

    /**
     * Filter the query on the alerte_mes_consultations column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteMesConsultations('fooValue');   // WHERE alerte_mes_consultations = 'fooValue'
     * $query->filterByAlerteMesConsultations('%fooValue%'); // WHERE alerte_mes_consultations LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteMesConsultations The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteMesConsultations($alerteMesConsultations = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteMesConsultations)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteMesConsultations)) {
                $alerteMesConsultations = str_replace('*', '%', $alerteMesConsultations);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_MES_CONSULTATIONS, $alerteMesConsultations, $comparison);
    }

    /**
     * Filter the query on the alerte_consultations_mon_entite column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteConsultationsMonEntite('fooValue');   // WHERE alerte_consultations_mon_entite = 'fooValue'
     * $query->filterByAlerteConsultationsMonEntite('%fooValue%'); // WHERE alerte_consultations_mon_entite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteConsultationsMonEntite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteConsultationsMonEntite($alerteConsultationsMonEntite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteConsultationsMonEntite)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteConsultationsMonEntite)) {
                $alerteConsultationsMonEntite = str_replace('*', '%', $alerteConsultationsMonEntite);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_CONSULTATIONS_MON_ENTITE, $alerteConsultationsMonEntite, $comparison);
    }

    /**
     * Filter the query on the alerte_consultations_des_entites_dependantes column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteConsultationsDesEntitesDependantes('fooValue');   // WHERE alerte_consultations_des_entites_dependantes = 'fooValue'
     * $query->filterByAlerteConsultationsDesEntitesDependantes('%fooValue%'); // WHERE alerte_consultations_des_entites_dependantes LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteConsultationsDesEntitesDependantes The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteConsultationsDesEntitesDependantes($alerteConsultationsDesEntitesDependantes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteConsultationsDesEntitesDependantes)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteConsultationsDesEntitesDependantes)) {
                $alerteConsultationsDesEntitesDependantes = str_replace('*', '%', $alerteConsultationsDesEntitesDependantes);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_CONSULTATIONS_DES_ENTITES_DEPENDANTES, $alerteConsultationsDesEntitesDependantes, $comparison);
    }

    /**
     * Filter the query on the alerte_consultations_mes_entites_transverses column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerteConsultationsMesEntitesTransverses('fooValue');   // WHERE alerte_consultations_mes_entites_transverses = 'fooValue'
     * $query->filterByAlerteConsultationsMesEntitesTransverses('%fooValue%'); // WHERE alerte_consultations_mes_entites_transverses LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerteConsultationsMesEntitesTransverses The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByAlerteConsultationsMesEntitesTransverses($alerteConsultationsMesEntitesTransverses = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerteConsultationsMesEntitesTransverses)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerteConsultationsMesEntitesTransverses)) {
                $alerteConsultationsMesEntitesTransverses = str_replace('*', '%', $alerteConsultationsMesEntitesTransverses);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::ALERTE_CONSULTATIONS_MES_ENTITES_TRANSVERSES, $alerteConsultationsMesEntitesTransverses, $comparison);
    }

    /**
     * Filter the query on the technique column
     *
     * Example usage:
     * <code>
     * $query->filterByTechnique(true); // WHERE technique = true
     * $query->filterByTechnique('yes'); // WHERE technique = true
     * </code>
     *
     * @param     boolean|string $technique The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByTechnique($technique = null, $comparison = null)
    {
        if (is_string($technique)) {
            $technique = in_array(strtolower($technique), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonAgentPeer::TECHNIQUE, $technique, $comparison);
    }

    /**
     * Filter the query on the service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceId(1234); // WHERE service_id = 1234
     * $query->filterByServiceId(array(12, 34)); // WHERE service_id IN (12, 34)
     * $query->filterByServiceId(array('min' => 12)); // WHERE service_id >= 12
     * $query->filterByServiceId(array('max' => 12)); // WHERE service_id <= 12
     * </code>
     *
     * @see       filterByCommonService()
     *
     * @param     mixed $serviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByServiceId($serviceId = null, $comparison = null)
    {
        if (is_array($serviceId)) {
            $useMinMax = false;
            if (isset($serviceId['min'])) {
                $this->addUsingAlias(CommonAgentPeer::SERVICE_ID, $serviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceId['max'])) {
                $this->addUsingAlias(CommonAgentPeer::SERVICE_ID, $serviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::SERVICE_ID, $serviceId, $comparison);
    }

    /**
     * Filter the query on the date_validation_rgpd column
     *
     * Example usage:
     * <code>
     * $query->filterByDateValidationRgpd('2011-03-14'); // WHERE date_validation_rgpd = '2011-03-14'
     * $query->filterByDateValidationRgpd('now'); // WHERE date_validation_rgpd = '2011-03-14'
     * $query->filterByDateValidationRgpd(array('max' => 'yesterday')); // WHERE date_validation_rgpd > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateValidationRgpd The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByDateValidationRgpd($dateValidationRgpd = null, $comparison = null)
    {
        if (is_array($dateValidationRgpd)) {
            $useMinMax = false;
            if (isset($dateValidationRgpd['min'])) {
                $this->addUsingAlias(CommonAgentPeer::DATE_VALIDATION_RGPD, $dateValidationRgpd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateValidationRgpd['max'])) {
                $this->addUsingAlias(CommonAgentPeer::DATE_VALIDATION_RGPD, $dateValidationRgpd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAgentPeer::DATE_VALIDATION_RGPD, $dateValidationRgpd, $comparison);
    }

    /**
     * Filter the query on the rgpd_communication_place column
     *
     * Example usage:
     * <code>
     * $query->filterByRgpdCommunicationPlace(true); // WHERE rgpd_communication_place = true
     * $query->filterByRgpdCommunicationPlace('yes'); // WHERE rgpd_communication_place = true
     * </code>
     *
     * @param     boolean|string $rgpdCommunicationPlace The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByRgpdCommunicationPlace($rgpdCommunicationPlace = null, $comparison = null)
    {
        if (is_string($rgpdCommunicationPlace)) {
            $rgpdCommunicationPlace = in_array(strtolower($rgpdCommunicationPlace), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonAgentPeer::RGPD_COMMUNICATION_PLACE, $rgpdCommunicationPlace, $comparison);
    }

    /**
     * Filter the query on the rgpd_enquete column
     *
     * Example usage:
     * <code>
     * $query->filterByRgpdEnquete(true); // WHERE rgpd_enquete = true
     * $query->filterByRgpdEnquete('yes'); // WHERE rgpd_enquete = true
     * </code>
     *
     * @param     boolean|string $rgpdEnquete The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function filterByRgpdEnquete($rgpdEnquete = null, $comparison = null)
    {
        if (is_string($rgpdEnquete)) {
            $rgpdEnquete = in_array(strtolower($rgpdEnquete), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonAgentPeer::RGPD_ENQUETE, $rgpdEnquete, $comparison);
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ORGANISME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonAgentPeer::ORGANISME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Filter the query by a related CommonService object
     *
     * @param   CommonService|PropelObjectCollection $commonService The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonService($commonService, $comparison = null)
    {
        if ($commonService instanceof CommonService) {
            return $this
                ->addUsingAlias(CommonAgentPeer::SERVICE_ID, $commonService->getId(), $comparison);
        } elseif ($commonService instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonAgentPeer::SERVICE_ID, $commonService->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonService() only accepts arguments of type CommonService or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonService relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonService($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonService');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonService');
        }

        return $this;
    }

    /**
     * Use the CommonService relation CommonService object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonServiceQuery A secondary query class using the current class as primary query
     */
    public function useCommonServiceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonService($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonService', '\Application\Propel\Mpe\CommonServiceQuery');
    }

    /**
     * Filter the query by a related CommonAgentServiceMetier object
     *
     * @param   CommonAgentServiceMetier|PropelObjectCollection $commonAgentServiceMetier  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgentServiceMetier($commonAgentServiceMetier, $comparison = null)
    {
        if ($commonAgentServiceMetier instanceof CommonAgentServiceMetier) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonAgentServiceMetier->getIdAgent(), $comparison);
        } elseif ($commonAgentServiceMetier instanceof PropelObjectCollection) {
            return $this
                ->useCommonAgentServiceMetierQuery()
                ->filterByPrimaryKeys($commonAgentServiceMetier->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonAgentServiceMetier() only accepts arguments of type CommonAgentServiceMetier or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgentServiceMetier relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonAgentServiceMetier($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgentServiceMetier');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgentServiceMetier');
        }

        return $this;
    }

    /**
     * Use the CommonAgentServiceMetier relation CommonAgentServiceMetier object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentServiceMetierQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentServiceMetierQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonAgentServiceMetier($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgentServiceMetier', '\Application\Propel\Mpe\CommonAgentServiceMetierQuery');
    }

    /**
     * Filter the query by a related CommonHabilitationAgent object
     *
     * @param   CommonHabilitationAgent|PropelObjectCollection $commonHabilitationAgent  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonHabilitationAgent($commonHabilitationAgent, $comparison = null)
    {
        if ($commonHabilitationAgent instanceof CommonHabilitationAgent) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonHabilitationAgent->getIdAgent(), $comparison);
        } elseif ($commonHabilitationAgent instanceof PropelObjectCollection) {
            return $this
                ->useCommonHabilitationAgentQuery()
                ->filterByPrimaryKeys($commonHabilitationAgent->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonHabilitationAgent() only accepts arguments of type CommonHabilitationAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonHabilitationAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonHabilitationAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonHabilitationAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonHabilitationAgent');
        }

        return $this;
    }

    /**
     * Use the CommonHabilitationAgent relation CommonHabilitationAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonHabilitationAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonHabilitationAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonHabilitationAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonHabilitationAgent', '\Application\Propel\Mpe\CommonHabilitationAgentQuery');
    }

    /**
     * Filter the query by a related CommonAgentTechniqueToken object
     *
     * @param   CommonAgentTechniqueToken|PropelObjectCollection $commonAgentTechniqueToken  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgentTechniqueToken($commonAgentTechniqueToken, $comparison = null)
    {
        if ($commonAgentTechniqueToken instanceof CommonAgentTechniqueToken) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonAgentTechniqueToken->getAgentId(), $comparison);
        } elseif ($commonAgentTechniqueToken instanceof PropelObjectCollection) {
            return $this
                ->useCommonAgentTechniqueTokenQuery()
                ->filterByPrimaryKeys($commonAgentTechniqueToken->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonAgentTechniqueToken() only accepts arguments of type CommonAgentTechniqueToken or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgentTechniqueToken relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonAgentTechniqueToken($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgentTechniqueToken');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgentTechniqueToken');
        }

        return $this;
    }

    /**
     * Use the CommonAgentTechniqueToken relation CommonAgentTechniqueToken object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentTechniqueTokenQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentTechniqueTokenQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonAgentTechniqueToken($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgentTechniqueToken', '\Application\Propel\Mpe\CommonAgentTechniqueTokenQuery');
    }

    /**
     * Filter the query by a related CommonAnnexeFinanciere object
     *
     * @param   CommonAnnexeFinanciere|PropelObjectCollection $commonAnnexeFinanciere  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAnnexeFinanciere($commonAnnexeFinanciere, $comparison = null)
    {
        if ($commonAnnexeFinanciere instanceof CommonAnnexeFinanciere) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonAnnexeFinanciere->getIdAgent(), $comparison);
        } elseif ($commonAnnexeFinanciere instanceof PropelObjectCollection) {
            return $this
                ->useCommonAnnexeFinanciereQuery()
                ->filterByPrimaryKeys($commonAnnexeFinanciere->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonAnnexeFinanciere() only accepts arguments of type CommonAnnexeFinanciere or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAnnexeFinanciere relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonAnnexeFinanciere($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAnnexeFinanciere');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAnnexeFinanciere');
        }

        return $this;
    }

    /**
     * Use the CommonAnnexeFinanciere relation CommonAnnexeFinanciere object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAnnexeFinanciereQuery A secondary query class using the current class as primary query
     */
    public function useCommonAnnexeFinanciereQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonAnnexeFinanciere($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAnnexeFinanciere', '\Application\Propel\Mpe\CommonAnnexeFinanciereQuery');
    }

    /**
     * Filter the query by a related CommonConsultationFavoris object
     *
     * @param   CommonConsultationFavoris|PropelObjectCollection $commonConsultationFavoris  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationFavoris($commonConsultationFavoris, $comparison = null)
    {
        if ($commonConsultationFavoris instanceof CommonConsultationFavoris) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonConsultationFavoris->getIdAgent(), $comparison);
        } elseif ($commonConsultationFavoris instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationFavorisQuery()
                ->filterByPrimaryKeys($commonConsultationFavoris->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultationFavoris() only accepts arguments of type CommonConsultationFavoris or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationFavoris relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonConsultationFavoris($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationFavoris');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationFavoris');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationFavoris relation CommonConsultationFavoris object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationFavorisQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationFavorisQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationFavoris($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationFavoris', '\Application\Propel\Mpe\CommonConsultationFavorisQuery');
    }

    /**
     * Filter the query by a related CommonDossierVolumineux object
     *
     * @param   CommonDossierVolumineux|PropelObjectCollection $commonDossierVolumineux  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonDossierVolumineux($commonDossierVolumineux, $comparison = null)
    {
        if ($commonDossierVolumineux instanceof CommonDossierVolumineux) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonDossierVolumineux->getIdAgent(), $comparison);
        } elseif ($commonDossierVolumineux instanceof PropelObjectCollection) {
            return $this
                ->useCommonDossierVolumineuxQuery()
                ->filterByPrimaryKeys($commonDossierVolumineux->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonDossierVolumineux() only accepts arguments of type CommonDossierVolumineux or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonDossierVolumineux relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonDossierVolumineux($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonDossierVolumineux');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonDossierVolumineux');
        }

        return $this;
    }

    /**
     * Use the CommonDossierVolumineux relation CommonDossierVolumineux object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonDossierVolumineuxQuery A secondary query class using the current class as primary query
     */
    public function useCommonDossierVolumineuxQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonDossierVolumineux($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonDossierVolumineux', '\Application\Propel\Mpe\CommonDossierVolumineuxQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDoc object
     *
     * @param   CommonEchangeDoc|PropelObjectCollection $commonEchangeDoc  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDoc($commonEchangeDoc, $comparison = null)
    {
        if ($commonEchangeDoc instanceof CommonEchangeDoc) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonEchangeDoc->getAgentId(), $comparison);
        } elseif ($commonEchangeDoc instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocQuery()
                ->filterByPrimaryKeys($commonEchangeDoc->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDoc() only accepts arguments of type CommonEchangeDoc or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDoc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDoc($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDoc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDoc');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDoc relation CommonEchangeDoc object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDoc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDoc', '\Application\Propel\Mpe\CommonEchangeDocQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDocHistorique object
     *
     * @param   CommonEchangeDocHistorique|PropelObjectCollection $commonEchangeDocHistorique  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocHistorique($commonEchangeDocHistorique, $comparison = null)
    {
        if ($commonEchangeDocHistorique instanceof CommonEchangeDocHistorique) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonEchangeDocHistorique->getAgentId(), $comparison);
        } elseif ($commonEchangeDocHistorique instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocHistoriqueQuery()
                ->filterByPrimaryKeys($commonEchangeDocHistorique->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDocHistorique() only accepts arguments of type CommonEchangeDocHistorique or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocHistorique relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocHistorique($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocHistorique');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocHistorique');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocHistorique relation CommonEchangeDocHistorique object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocHistoriqueQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocHistoriqueQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocHistorique($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocHistorique', '\Application\Propel\Mpe\CommonEchangeDocHistoriqueQuery');
    }

    /**
     * Filter the query by a related CommonHabilitationAgentWs object
     *
     * @param   CommonHabilitationAgentWs|PropelObjectCollection $commonHabilitationAgentWs  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonHabilitationAgentWs($commonHabilitationAgentWs, $comparison = null)
    {
        if ($commonHabilitationAgentWs instanceof CommonHabilitationAgentWs) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonHabilitationAgentWs->getAgentId(), $comparison);
        } elseif ($commonHabilitationAgentWs instanceof PropelObjectCollection) {
            return $this
                ->useCommonHabilitationAgentWsQuery()
                ->filterByPrimaryKeys($commonHabilitationAgentWs->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonHabilitationAgentWs() only accepts arguments of type CommonHabilitationAgentWs or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonHabilitationAgentWs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonHabilitationAgentWs($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonHabilitationAgentWs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonHabilitationAgentWs');
        }

        return $this;
    }

    /**
     * Use the CommonHabilitationAgentWs relation CommonHabilitationAgentWs object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonHabilitationAgentWsQuery A secondary query class using the current class as primary query
     */
    public function useCommonHabilitationAgentWsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonHabilitationAgentWs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonHabilitationAgentWs', '\Application\Propel\Mpe\CommonHabilitationAgentWsQuery');
    }

    /**
     * Filter the query by a related CommonInvitePermanentTransverse object
     *
     * @param   CommonInvitePermanentTransverse|PropelObjectCollection $commonInvitePermanentTransverse  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonInvitePermanentTransverse($commonInvitePermanentTransverse, $comparison = null)
    {
        if ($commonInvitePermanentTransverse instanceof CommonInvitePermanentTransverse) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonInvitePermanentTransverse->getAgentId(), $comparison);
        } elseif ($commonInvitePermanentTransverse instanceof PropelObjectCollection) {
            return $this
                ->useCommonInvitePermanentTransverseQuery()
                ->filterByPrimaryKeys($commonInvitePermanentTransverse->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonInvitePermanentTransverse() only accepts arguments of type CommonInvitePermanentTransverse or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonInvitePermanentTransverse relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonInvitePermanentTransverse($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonInvitePermanentTransverse');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonInvitePermanentTransverse');
        }

        return $this;
    }

    /**
     * Use the CommonInvitePermanentTransverse relation CommonInvitePermanentTransverse object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonInvitePermanentTransverseQuery A secondary query class using the current class as primary query
     */
    public function useCommonInvitePermanentTransverseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonInvitePermanentTransverse($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonInvitePermanentTransverse', '\Application\Propel\Mpe\CommonInvitePermanentTransverseQuery');
    }

    /**
     * Filter the query by a related CommonModificationContrat object
     *
     * @param   CommonModificationContrat|PropelObjectCollection $commonModificationContrat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonModificationContrat($commonModificationContrat, $comparison = null)
    {
        if ($commonModificationContrat instanceof CommonModificationContrat) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonModificationContrat->getIdAgent(), $comparison);
        } elseif ($commonModificationContrat instanceof PropelObjectCollection) {
            return $this
                ->useCommonModificationContratQuery()
                ->filterByPrimaryKeys($commonModificationContrat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonModificationContrat() only accepts arguments of type CommonModificationContrat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonModificationContrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonModificationContrat($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonModificationContrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonModificationContrat');
        }

        return $this;
    }

    /**
     * Use the CommonModificationContrat relation CommonModificationContrat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonModificationContratQuery A secondary query class using the current class as primary query
     */
    public function useCommonModificationContratQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonModificationContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonModificationContrat', '\Application\Propel\Mpe\CommonModificationContratQuery');
    }

    /**
     * Filter the query by a related CommonTCAOCommissionAgent object
     *
     * @param   CommonTCAOCommissionAgent|PropelObjectCollection $commonTCAOCommissionAgent  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCAOCommissionAgent($commonTCAOCommissionAgent, $comparison = null)
    {
        if ($commonTCAOCommissionAgent instanceof CommonTCAOCommissionAgent) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonTCAOCommissionAgent->getIdAgent(), $comparison);
        } elseif ($commonTCAOCommissionAgent instanceof PropelObjectCollection) {
            return $this
                ->useCommonTCAOCommissionAgentQuery()
                ->filterByPrimaryKeys($commonTCAOCommissionAgent->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTCAOCommissionAgent() only accepts arguments of type CommonTCAOCommissionAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCAOCommissionAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonTCAOCommissionAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCAOCommissionAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCAOCommissionAgent');
        }

        return $this;
    }

    /**
     * Use the CommonTCAOCommissionAgent relation CommonTCAOCommissionAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCAOCommissionAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCAOCommissionAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTCAOCommissionAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCAOCommissionAgent', '\Application\Propel\Mpe\CommonTCAOCommissionAgentQuery');
    }

    /**
     * Filter the query by a related CommonTCAOOrdreDuJourIntervenant object
     *
     * @param   CommonTCAOOrdreDuJourIntervenant|PropelObjectCollection $commonTCAOOrdreDuJourIntervenant  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCAOOrdreDuJourIntervenant($commonTCAOOrdreDuJourIntervenant, $comparison = null)
    {
        if ($commonTCAOOrdreDuJourIntervenant instanceof CommonTCAOOrdreDuJourIntervenant) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonTCAOOrdreDuJourIntervenant->getIdAgent(), $comparison);
        } elseif ($commonTCAOOrdreDuJourIntervenant instanceof PropelObjectCollection) {
            return $this
                ->useCommonTCAOOrdreDuJourIntervenantQuery()
                ->filterByPrimaryKeys($commonTCAOOrdreDuJourIntervenant->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTCAOOrdreDuJourIntervenant() only accepts arguments of type CommonTCAOOrdreDuJourIntervenant or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCAOOrdreDuJourIntervenant relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonTCAOOrdreDuJourIntervenant($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCAOOrdreDuJourIntervenant');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCAOOrdreDuJourIntervenant');
        }

        return $this;
    }

    /**
     * Use the CommonTCAOOrdreDuJourIntervenant relation CommonTCAOOrdreDuJourIntervenant object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenantQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCAOOrdreDuJourIntervenantQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTCAOOrdreDuJourIntervenant($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCAOOrdreDuJourIntervenant', '\Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenantQuery');
    }

    /**
     * Filter the query by a related CommonTCAOSeanceAgent object
     *
     * @param   CommonTCAOSeanceAgent|PropelObjectCollection $commonTCAOSeanceAgent  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCAOSeanceAgent($commonTCAOSeanceAgent, $comparison = null)
    {
        if ($commonTCAOSeanceAgent instanceof CommonTCAOSeanceAgent) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonTCAOSeanceAgent->getIdAgent(), $comparison);
        } elseif ($commonTCAOSeanceAgent instanceof PropelObjectCollection) {
            return $this
                ->useCommonTCAOSeanceAgentQuery()
                ->filterByPrimaryKeys($commonTCAOSeanceAgent->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTCAOSeanceAgent() only accepts arguments of type CommonTCAOSeanceAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCAOSeanceAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonTCAOSeanceAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCAOSeanceAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCAOSeanceAgent');
        }

        return $this;
    }

    /**
     * Use the CommonTCAOSeanceAgent relation CommonTCAOSeanceAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCAOSeanceAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCAOSeanceAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTCAOSeanceAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCAOSeanceAgent', '\Application\Propel\Mpe\CommonTCAOSeanceAgentQuery');
    }

    /**
     * Filter the query by a related CommonTCAOSeanceInvite object
     *
     * @param   CommonTCAOSeanceInvite|PropelObjectCollection $commonTCAOSeanceInvite  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCAOSeanceInvite($commonTCAOSeanceInvite, $comparison = null)
    {
        if ($commonTCAOSeanceInvite instanceof CommonTCAOSeanceInvite) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonTCAOSeanceInvite->getIdAgent(), $comparison);
        } elseif ($commonTCAOSeanceInvite instanceof PropelObjectCollection) {
            return $this
                ->useCommonTCAOSeanceInviteQuery()
                ->filterByPrimaryKeys($commonTCAOSeanceInvite->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTCAOSeanceInvite() only accepts arguments of type CommonTCAOSeanceInvite or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCAOSeanceInvite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonTCAOSeanceInvite($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCAOSeanceInvite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCAOSeanceInvite');
        }

        return $this;
    }

    /**
     * Use the CommonTCAOSeanceInvite relation CommonTCAOSeanceInvite object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCAOSeanceInviteQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCAOSeanceInviteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTCAOSeanceInvite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCAOSeanceInvite', '\Application\Propel\Mpe\CommonTCAOSeanceInviteQuery');
    }

    /**
     * Filter the query by a related CommonTVisionRmaAgentOrganisme object
     *
     * @param   CommonTVisionRmaAgentOrganisme|PropelObjectCollection $commonTVisionRmaAgentOrganisme  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTVisionRmaAgentOrganisme($commonTVisionRmaAgentOrganisme, $comparison = null)
    {
        if ($commonTVisionRmaAgentOrganisme instanceof CommonTVisionRmaAgentOrganisme) {
            return $this
                ->addUsingAlias(CommonAgentPeer::ID, $commonTVisionRmaAgentOrganisme->getIdAgent(), $comparison);
        } elseif ($commonTVisionRmaAgentOrganisme instanceof PropelObjectCollection) {
            return $this
                ->useCommonTVisionRmaAgentOrganismeQuery()
                ->filterByPrimaryKeys($commonTVisionRmaAgentOrganisme->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTVisionRmaAgentOrganisme() only accepts arguments of type CommonTVisionRmaAgentOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTVisionRmaAgentOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function joinCommonTVisionRmaAgentOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTVisionRmaAgentOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTVisionRmaAgentOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonTVisionRmaAgentOrganisme relation CommonTVisionRmaAgentOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTVisionRmaAgentOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonTVisionRmaAgentOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTVisionRmaAgentOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTVisionRmaAgentOrganisme', '\Application\Propel\Mpe\CommonTVisionRmaAgentOrganismeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonAgent $commonAgent Object to remove from the list of results
     *
     * @return CommonAgentQuery The current query, for fluid interface
     */
    public function prune($commonAgent = null)
    {
        if ($commonAgent) {
            $this->addUsingAlias(CommonAgentPeer::ID, $commonAgent->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
