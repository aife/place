<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTOffreSupportPublicite;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\CommonTSupportPublicationPeer;
use Application\Propel\Mpe\CommonTSupportPublicationQuery;

/**
 * Base class that represents a query for the 't_support_publication' table.
 *
 *
 *
 * @method CommonTSupportPublicationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTSupportPublicationQuery orderByImageLogo($order = Criteria::ASC) Order by the image_logo column
 * @method CommonTSupportPublicationQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method CommonTSupportPublicationQuery orderByVisible($order = Criteria::ASC) Order by the visible column
 * @method CommonTSupportPublicationQuery orderByOrdre($order = Criteria::ASC) Order by the ordre column
 * @method CommonTSupportPublicationQuery orderByDefaultValue($order = Criteria::ASC) Order by the default_value column
 * @method CommonTSupportPublicationQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method CommonTSupportPublicationQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method CommonTSupportPublicationQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method CommonTSupportPublicationQuery orderByGroupe($order = Criteria::ASC) Order by the groupe column
 * @method CommonTSupportPublicationQuery orderByTagDebutFinGroupe($order = Criteria::ASC) Order by the tag_debut_fin_groupe column
 * @method CommonTSupportPublicationQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CommonTSupportPublicationQuery orderByNbreTotalGroupe($order = Criteria::ASC) Order by the nbre_total_groupe column
 * @method CommonTSupportPublicationQuery orderByLieuxExecution($order = Criteria::ASC) Order by the lieux_execution column
 * @method CommonTSupportPublicationQuery orderByDetailInfo($order = Criteria::ASC) Order by the detail_info column
 * @method CommonTSupportPublicationQuery orderByTypeInfo($order = Criteria::ASC) Order by the type_info column
 * @method CommonTSupportPublicationQuery orderByAffichageInfos($order = Criteria::ASC) Order by the affichage_infos column
 * @method CommonTSupportPublicationQuery orderByAffichageMessageSupport($order = Criteria::ASC) Order by the affichage_message_support column
 * @method CommonTSupportPublicationQuery orderBySelectionDepartementsParution($order = Criteria::ASC) Order by the selection_departements_parution column
 * @method CommonTSupportPublicationQuery orderByDepartementsParution($order = Criteria::ASC) Order by the departements_parution column
 *
 * @method CommonTSupportPublicationQuery groupById() Group by the id column
 * @method CommonTSupportPublicationQuery groupByImageLogo() Group by the image_logo column
 * @method CommonTSupportPublicationQuery groupByNom() Group by the nom column
 * @method CommonTSupportPublicationQuery groupByVisible() Group by the visible column
 * @method CommonTSupportPublicationQuery groupByOrdre() Group by the ordre column
 * @method CommonTSupportPublicationQuery groupByDefaultValue() Group by the default_value column
 * @method CommonTSupportPublicationQuery groupByCode() Group by the code column
 * @method CommonTSupportPublicationQuery groupByActif() Group by the actif column
 * @method CommonTSupportPublicationQuery groupByUrl() Group by the url column
 * @method CommonTSupportPublicationQuery groupByGroupe() Group by the groupe column
 * @method CommonTSupportPublicationQuery groupByTagDebutFinGroupe() Group by the tag_debut_fin_groupe column
 * @method CommonTSupportPublicationQuery groupByDescription() Group by the description column
 * @method CommonTSupportPublicationQuery groupByNbreTotalGroupe() Group by the nbre_total_groupe column
 * @method CommonTSupportPublicationQuery groupByLieuxExecution() Group by the lieux_execution column
 * @method CommonTSupportPublicationQuery groupByDetailInfo() Group by the detail_info column
 * @method CommonTSupportPublicationQuery groupByTypeInfo() Group by the type_info column
 * @method CommonTSupportPublicationQuery groupByAffichageInfos() Group by the affichage_infos column
 * @method CommonTSupportPublicationQuery groupByAffichageMessageSupport() Group by the affichage_message_support column
 * @method CommonTSupportPublicationQuery groupBySelectionDepartementsParution() Group by the selection_departements_parution column
 * @method CommonTSupportPublicationQuery groupByDepartementsParution() Group by the departements_parution column
 *
 * @method CommonTSupportPublicationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTSupportPublicationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTSupportPublicationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTSupportPublicationQuery leftJoinCommonTOffreSupportPublicite($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTOffreSupportPublicite relation
 * @method CommonTSupportPublicationQuery rightJoinCommonTOffreSupportPublicite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTOffreSupportPublicite relation
 * @method CommonTSupportPublicationQuery innerJoinCommonTOffreSupportPublicite($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTOffreSupportPublicite relation
 *
 * @method CommonTSupportPublicationQuery leftJoinCommonTSupportAnnonceConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTSupportAnnonceConsultation relation
 * @method CommonTSupportPublicationQuery rightJoinCommonTSupportAnnonceConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTSupportAnnonceConsultation relation
 * @method CommonTSupportPublicationQuery innerJoinCommonTSupportAnnonceConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTSupportAnnonceConsultation relation
 *
 * @method CommonTSupportPublication findOne(PropelPDO $con = null) Return the first CommonTSupportPublication matching the query
 * @method CommonTSupportPublication findOneOrCreate(PropelPDO $con = null) Return the first CommonTSupportPublication matching the query, or a new CommonTSupportPublication object populated from the query conditions when no match is found
 *
 * @method CommonTSupportPublication findOneByImageLogo(string $image_logo) Return the first CommonTSupportPublication filtered by the image_logo column
 * @method CommonTSupportPublication findOneByNom(string $nom) Return the first CommonTSupportPublication filtered by the nom column
 * @method CommonTSupportPublication findOneByVisible(string $visible) Return the first CommonTSupportPublication filtered by the visible column
 * @method CommonTSupportPublication findOneByOrdre(int $ordre) Return the first CommonTSupportPublication filtered by the ordre column
 * @method CommonTSupportPublication findOneByDefaultValue(string $default_value) Return the first CommonTSupportPublication filtered by the default_value column
 * @method CommonTSupportPublication findOneByCode(string $code) Return the first CommonTSupportPublication filtered by the code column
 * @method CommonTSupportPublication findOneByActif(string $actif) Return the first CommonTSupportPublication filtered by the actif column
 * @method CommonTSupportPublication findOneByUrl(string $url) Return the first CommonTSupportPublication filtered by the url column
 * @method CommonTSupportPublication findOneByGroupe(string $groupe) Return the first CommonTSupportPublication filtered by the groupe column
 * @method CommonTSupportPublication findOneByTagDebutFinGroupe(string $tag_debut_fin_groupe) Return the first CommonTSupportPublication filtered by the tag_debut_fin_groupe column
 * @method CommonTSupportPublication findOneByDescription(string $description) Return the first CommonTSupportPublication filtered by the description column
 * @method CommonTSupportPublication findOneByNbreTotalGroupe(int $nbre_total_groupe) Return the first CommonTSupportPublication filtered by the nbre_total_groupe column
 * @method CommonTSupportPublication findOneByLieuxExecution(string $lieux_execution) Return the first CommonTSupportPublication filtered by the lieux_execution column
 * @method CommonTSupportPublication findOneByDetailInfo(string $detail_info) Return the first CommonTSupportPublication filtered by the detail_info column
 * @method CommonTSupportPublication findOneByTypeInfo(string $type_info) Return the first CommonTSupportPublication filtered by the type_info column
 * @method CommonTSupportPublication findOneByAffichageInfos(string $affichage_infos) Return the first CommonTSupportPublication filtered by the affichage_infos column
 * @method CommonTSupportPublication findOneByAffichageMessageSupport(string $affichage_message_support) Return the first CommonTSupportPublication filtered by the affichage_message_support column
 * @method CommonTSupportPublication findOneBySelectionDepartementsParution(string $selection_departements_parution) Return the first CommonTSupportPublication filtered by the selection_departements_parution column
 * @method CommonTSupportPublication findOneByDepartementsParution(string $departements_parution) Return the first CommonTSupportPublication filtered by the departements_parution column
 *
 * @method array findById(int $id) Return CommonTSupportPublication objects filtered by the id column
 * @method array findByImageLogo(string $image_logo) Return CommonTSupportPublication objects filtered by the image_logo column
 * @method array findByNom(string $nom) Return CommonTSupportPublication objects filtered by the nom column
 * @method array findByVisible(string $visible) Return CommonTSupportPublication objects filtered by the visible column
 * @method array findByOrdre(int $ordre) Return CommonTSupportPublication objects filtered by the ordre column
 * @method array findByDefaultValue(string $default_value) Return CommonTSupportPublication objects filtered by the default_value column
 * @method array findByCode(string $code) Return CommonTSupportPublication objects filtered by the code column
 * @method array findByActif(string $actif) Return CommonTSupportPublication objects filtered by the actif column
 * @method array findByUrl(string $url) Return CommonTSupportPublication objects filtered by the url column
 * @method array findByGroupe(string $groupe) Return CommonTSupportPublication objects filtered by the groupe column
 * @method array findByTagDebutFinGroupe(string $tag_debut_fin_groupe) Return CommonTSupportPublication objects filtered by the tag_debut_fin_groupe column
 * @method array findByDescription(string $description) Return CommonTSupportPublication objects filtered by the description column
 * @method array findByNbreTotalGroupe(int $nbre_total_groupe) Return CommonTSupportPublication objects filtered by the nbre_total_groupe column
 * @method array findByLieuxExecution(string $lieux_execution) Return CommonTSupportPublication objects filtered by the lieux_execution column
 * @method array findByDetailInfo(string $detail_info) Return CommonTSupportPublication objects filtered by the detail_info column
 * @method array findByTypeInfo(string $type_info) Return CommonTSupportPublication objects filtered by the type_info column
 * @method array findByAffichageInfos(string $affichage_infos) Return CommonTSupportPublication objects filtered by the affichage_infos column
 * @method array findByAffichageMessageSupport(string $affichage_message_support) Return CommonTSupportPublication objects filtered by the affichage_message_support column
 * @method array findBySelectionDepartementsParution(string $selection_departements_parution) Return CommonTSupportPublication objects filtered by the selection_departements_parution column
 * @method array findByDepartementsParution(string $departements_parution) Return CommonTSupportPublication objects filtered by the departements_parution column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTSupportPublicationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTSupportPublicationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTSupportPublication', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTSupportPublicationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTSupportPublicationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTSupportPublicationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTSupportPublicationQuery) {
            return $criteria;
        }
        $query = new CommonTSupportPublicationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTSupportPublication|CommonTSupportPublication[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTSupportPublicationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTSupportPublication A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTSupportPublication A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `image_logo`, `nom`, `visible`, `ordre`, `default_value`, `code`, `actif`, `url`, `groupe`, `tag_debut_fin_groupe`, `description`, `nbre_total_groupe`, `lieux_execution`, `detail_info`, `type_info`, `affichage_infos`, `affichage_message_support`, `selection_departements_parution`, `departements_parution` FROM `t_support_publication` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTSupportPublication();
            $obj->hydrate($row);
            CommonTSupportPublicationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTSupportPublication|CommonTSupportPublication[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTSupportPublication[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTSupportPublicationPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTSupportPublicationPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTSupportPublicationPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTSupportPublicationPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the image_logo column
     *
     * Example usage:
     * <code>
     * $query->filterByImageLogo('fooValue');   // WHERE image_logo = 'fooValue'
     * $query->filterByImageLogo('%fooValue%'); // WHERE image_logo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageLogo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByImageLogo($imageLogo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageLogo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $imageLogo)) {
                $imageLogo = str_replace('*', '%', $imageLogo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::IMAGE_LOGO, $imageLogo, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%'); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nom)) {
                $nom = str_replace('*', '%', $nom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the visible column
     *
     * Example usage:
     * <code>
     * $query->filterByVisible('fooValue');   // WHERE visible = 'fooValue'
     * $query->filterByVisible('%fooValue%'); // WHERE visible LIKE '%fooValue%'
     * </code>
     *
     * @param     string $visible The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByVisible($visible = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($visible)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $visible)) {
                $visible = str_replace('*', '%', $visible);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::VISIBLE, $visible, $comparison);
    }

    /**
     * Filter the query on the ordre column
     *
     * Example usage:
     * <code>
     * $query->filterByOrdre(1234); // WHERE ordre = 1234
     * $query->filterByOrdre(array(12, 34)); // WHERE ordre IN (12, 34)
     * $query->filterByOrdre(array('min' => 12)); // WHERE ordre >= 12
     * $query->filterByOrdre(array('max' => 12)); // WHERE ordre <= 12
     * </code>
     *
     * @param     mixed $ordre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByOrdre($ordre = null, $comparison = null)
    {
        if (is_array($ordre)) {
            $useMinMax = false;
            if (isset($ordre['min'])) {
                $this->addUsingAlias(CommonTSupportPublicationPeer::ORDRE, $ordre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ordre['max'])) {
                $this->addUsingAlias(CommonTSupportPublicationPeer::ORDRE, $ordre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::ORDRE, $ordre, $comparison);
    }

    /**
     * Filter the query on the default_value column
     *
     * Example usage:
     * <code>
     * $query->filterByDefaultValue('fooValue');   // WHERE default_value = 'fooValue'
     * $query->filterByDefaultValue('%fooValue%'); // WHERE default_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $defaultValue The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByDefaultValue($defaultValue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($defaultValue)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $defaultValue)) {
                $defaultValue = str_replace('*', '%', $defaultValue);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::DEFAULT_VALUE, $defaultValue, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif('fooValue');   // WHERE actif = 'fooValue'
     * $query->filterByActif('%fooValue%'); // WHERE actif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $actif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($actif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $actif)) {
                $actif = str_replace('*', '%', $actif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%'); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $url)) {
                $url = str_replace('*', '%', $url);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::URL, $url, $comparison);
    }

    /**
     * Filter the query on the groupe column
     *
     * Example usage:
     * <code>
     * $query->filterByGroupe('fooValue');   // WHERE groupe = 'fooValue'
     * $query->filterByGroupe('%fooValue%'); // WHERE groupe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $groupe The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByGroupe($groupe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($groupe)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $groupe)) {
                $groupe = str_replace('*', '%', $groupe);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::GROUPE, $groupe, $comparison);
    }

    /**
     * Filter the query on the tag_debut_fin_groupe column
     *
     * Example usage:
     * <code>
     * $query->filterByTagDebutFinGroupe('fooValue');   // WHERE tag_debut_fin_groupe = 'fooValue'
     * $query->filterByTagDebutFinGroupe('%fooValue%'); // WHERE tag_debut_fin_groupe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tagDebutFinGroupe The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByTagDebutFinGroupe($tagDebutFinGroupe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tagDebutFinGroupe)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tagDebutFinGroupe)) {
                $tagDebutFinGroupe = str_replace('*', '%', $tagDebutFinGroupe);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::TAG_DEBUT_FIN_GROUPE, $tagDebutFinGroupe, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the nbre_total_groupe column
     *
     * Example usage:
     * <code>
     * $query->filterByNbreTotalGroupe(1234); // WHERE nbre_total_groupe = 1234
     * $query->filterByNbreTotalGroupe(array(12, 34)); // WHERE nbre_total_groupe IN (12, 34)
     * $query->filterByNbreTotalGroupe(array('min' => 12)); // WHERE nbre_total_groupe >= 12
     * $query->filterByNbreTotalGroupe(array('max' => 12)); // WHERE nbre_total_groupe <= 12
     * </code>
     *
     * @param     mixed $nbreTotalGroupe The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByNbreTotalGroupe($nbreTotalGroupe = null, $comparison = null)
    {
        if (is_array($nbreTotalGroupe)) {
            $useMinMax = false;
            if (isset($nbreTotalGroupe['min'])) {
                $this->addUsingAlias(CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE, $nbreTotalGroupe['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbreTotalGroupe['max'])) {
                $this->addUsingAlias(CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE, $nbreTotalGroupe['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE, $nbreTotalGroupe, $comparison);
    }

    /**
     * Filter the query on the lieux_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByLieuxExecution('fooValue');   // WHERE lieux_execution = 'fooValue'
     * $query->filterByLieuxExecution('%fooValue%'); // WHERE lieux_execution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lieuxExecution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByLieuxExecution($lieuxExecution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lieuxExecution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lieuxExecution)) {
                $lieuxExecution = str_replace('*', '%', $lieuxExecution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::LIEUX_EXECUTION, $lieuxExecution, $comparison);
    }

    /**
     * Filter the query on the detail_info column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailInfo('fooValue');   // WHERE detail_info = 'fooValue'
     * $query->filterByDetailInfo('%fooValue%'); // WHERE detail_info LIKE '%fooValue%'
     * </code>
     *
     * @param     string $detailInfo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByDetailInfo($detailInfo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailInfo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $detailInfo)) {
                $detailInfo = str_replace('*', '%', $detailInfo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::DETAIL_INFO, $detailInfo, $comparison);
    }

    /**
     * Filter the query on the type_info column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeInfo('fooValue');   // WHERE type_info = 'fooValue'
     * $query->filterByTypeInfo('%fooValue%'); // WHERE type_info LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeInfo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByTypeInfo($typeInfo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeInfo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeInfo)) {
                $typeInfo = str_replace('*', '%', $typeInfo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::TYPE_INFO, $typeInfo, $comparison);
    }

    /**
     * Filter the query on the affichage_infos column
     *
     * Example usage:
     * <code>
     * $query->filterByAffichageInfos('fooValue');   // WHERE affichage_infos = 'fooValue'
     * $query->filterByAffichageInfos('%fooValue%'); // WHERE affichage_infos LIKE '%fooValue%'
     * </code>
     *
     * @param     string $affichageInfos The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByAffichageInfos($affichageInfos = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($affichageInfos)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $affichageInfos)) {
                $affichageInfos = str_replace('*', '%', $affichageInfos);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::AFFICHAGE_INFOS, $affichageInfos, $comparison);
    }

    /**
     * Filter the query on the affichage_message_support column
     *
     * Example usage:
     * <code>
     * $query->filterByAffichageMessageSupport('fooValue');   // WHERE affichage_message_support = 'fooValue'
     * $query->filterByAffichageMessageSupport('%fooValue%'); // WHERE affichage_message_support LIKE '%fooValue%'
     * </code>
     *
     * @param     string $affichageMessageSupport The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByAffichageMessageSupport($affichageMessageSupport = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($affichageMessageSupport)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $affichageMessageSupport)) {
                $affichageMessageSupport = str_replace('*', '%', $affichageMessageSupport);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT, $affichageMessageSupport, $comparison);
    }

    /**
     * Filter the query on the selection_departements_parution column
     *
     * Example usage:
     * <code>
     * $query->filterBySelectionDepartementsParution('fooValue');   // WHERE selection_departements_parution = 'fooValue'
     * $query->filterBySelectionDepartementsParution('%fooValue%'); // WHERE selection_departements_parution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $selectionDepartementsParution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterBySelectionDepartementsParution($selectionDepartementsParution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($selectionDepartementsParution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $selectionDepartementsParution)) {
                $selectionDepartementsParution = str_replace('*', '%', $selectionDepartementsParution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION, $selectionDepartementsParution, $comparison);
    }

    /**
     * Filter the query on the departements_parution column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartementsParution('fooValue');   // WHERE departements_parution = 'fooValue'
     * $query->filterByDepartementsParution('%fooValue%'); // WHERE departements_parution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departementsParution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByDepartementsParution($departementsParution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departementsParution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $departementsParution)) {
                $departementsParution = str_replace('*', '%', $departementsParution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportPublicationPeer::DEPARTEMENTS_PARUTION, $departementsParution, $comparison);
    }

    /**
     * Filter the query by a related CommonTOffreSupportPublicite object
     *
     * @param   CommonTOffreSupportPublicite|PropelObjectCollection $commonTOffreSupportPublicite  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTSupportPublicationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTOffreSupportPublicite($commonTOffreSupportPublicite, $comparison = null)
    {
        if ($commonTOffreSupportPublicite instanceof CommonTOffreSupportPublicite) {
            return $this
                ->addUsingAlias(CommonTSupportPublicationPeer::ID, $commonTOffreSupportPublicite->getIdSupport(), $comparison);
        } elseif ($commonTOffreSupportPublicite instanceof PropelObjectCollection) {
            return $this
                ->useCommonTOffreSupportPubliciteQuery()
                ->filterByPrimaryKeys($commonTOffreSupportPublicite->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTOffreSupportPublicite() only accepts arguments of type CommonTOffreSupportPublicite or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTOffreSupportPublicite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function joinCommonTOffreSupportPublicite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTOffreSupportPublicite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTOffreSupportPublicite');
        }

        return $this;
    }

    /**
     * Use the CommonTOffreSupportPublicite relation CommonTOffreSupportPublicite object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTOffreSupportPubliciteQuery A secondary query class using the current class as primary query
     */
    public function useCommonTOffreSupportPubliciteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTOffreSupportPublicite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTOffreSupportPublicite', '\Application\Propel\Mpe\CommonTOffreSupportPubliciteQuery');
    }

    /**
     * Filter the query by a related CommonTSupportAnnonceConsultation object
     *
     * @param   CommonTSupportAnnonceConsultation|PropelObjectCollection $commonTSupportAnnonceConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTSupportPublicationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTSupportAnnonceConsultation($commonTSupportAnnonceConsultation, $comparison = null)
    {
        if ($commonTSupportAnnonceConsultation instanceof CommonTSupportAnnonceConsultation) {
            return $this
                ->addUsingAlias(CommonTSupportPublicationPeer::ID, $commonTSupportAnnonceConsultation->getIdSupport(), $comparison);
        } elseif ($commonTSupportAnnonceConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonTSupportAnnonceConsultationQuery()
                ->filterByPrimaryKeys($commonTSupportAnnonceConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTSupportAnnonceConsultation() only accepts arguments of type CommonTSupportAnnonceConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTSupportAnnonceConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function joinCommonTSupportAnnonceConsultation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTSupportAnnonceConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTSupportAnnonceConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonTSupportAnnonceConsultation relation CommonTSupportAnnonceConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTSupportAnnonceConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonTSupportAnnonceConsultationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTSupportAnnonceConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTSupportAnnonceConsultation', '\Application\Propel\Mpe\CommonTSupportAnnonceConsultationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTSupportPublication $commonTSupportPublication Object to remove from the list of results
     *
     * @return CommonTSupportPublicationQuery The current query, for fluid interface
     */
    public function prune($commonTSupportPublication = null)
    {
        if ($commonTSupportPublication) {
            $this->addUsingAlias(CommonTSupportPublicationPeer::ID, $commonTSupportPublication->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
