<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonJmsJobRelatedEntities;
use Application\Propel\Mpe\CommonJmsJobRelatedEntitiesPeer;
use Application\Propel\Mpe\CommonJmsJobRelatedEntitiesQuery;
use Application\Propel\Mpe\CommonJmsJobs;

/**
 * Base class that represents a query for the 'jms_job_related_entities' table.
 *
 *
 *
 * @method CommonJmsJobRelatedEntitiesQuery orderByJobId($order = Criteria::ASC) Order by the job_id column
 * @method CommonJmsJobRelatedEntitiesQuery orderByRelatedClass($order = Criteria::ASC) Order by the related_class column
 * @method CommonJmsJobRelatedEntitiesQuery orderByRelatedId($order = Criteria::ASC) Order by the related_id column
 *
 * @method CommonJmsJobRelatedEntitiesQuery groupByJobId() Group by the job_id column
 * @method CommonJmsJobRelatedEntitiesQuery groupByRelatedClass() Group by the related_class column
 * @method CommonJmsJobRelatedEntitiesQuery groupByRelatedId() Group by the related_id column
 *
 * @method CommonJmsJobRelatedEntitiesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonJmsJobRelatedEntitiesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonJmsJobRelatedEntitiesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonJmsJobRelatedEntitiesQuery leftJoinCommonJmsJobs($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonJmsJobs relation
 * @method CommonJmsJobRelatedEntitiesQuery rightJoinCommonJmsJobs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonJmsJobs relation
 * @method CommonJmsJobRelatedEntitiesQuery innerJoinCommonJmsJobs($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonJmsJobs relation
 *
 * @method CommonJmsJobRelatedEntities findOne(PropelPDO $con = null) Return the first CommonJmsJobRelatedEntities matching the query
 * @method CommonJmsJobRelatedEntities findOneOrCreate(PropelPDO $con = null) Return the first CommonJmsJobRelatedEntities matching the query, or a new CommonJmsJobRelatedEntities object populated from the query conditions when no match is found
 *
 * @method CommonJmsJobRelatedEntities findOneByJobId(string $job_id) Return the first CommonJmsJobRelatedEntities filtered by the job_id column
 * @method CommonJmsJobRelatedEntities findOneByRelatedClass(string $related_class) Return the first CommonJmsJobRelatedEntities filtered by the related_class column
 * @method CommonJmsJobRelatedEntities findOneByRelatedId(string $related_id) Return the first CommonJmsJobRelatedEntities filtered by the related_id column
 *
 * @method array findByJobId(string $job_id) Return CommonJmsJobRelatedEntities objects filtered by the job_id column
 * @method array findByRelatedClass(string $related_class) Return CommonJmsJobRelatedEntities objects filtered by the related_class column
 * @method array findByRelatedId(string $related_id) Return CommonJmsJobRelatedEntities objects filtered by the related_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonJmsJobRelatedEntitiesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonJmsJobRelatedEntitiesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonJmsJobRelatedEntities', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonJmsJobRelatedEntitiesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonJmsJobRelatedEntitiesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonJmsJobRelatedEntitiesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonJmsJobRelatedEntitiesQuery) {
            return $criteria;
        }
        $query = new CommonJmsJobRelatedEntitiesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$job_id, $related_class, $related_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonJmsJobRelatedEntities|CommonJmsJobRelatedEntities[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonJmsJobRelatedEntitiesPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1], (string) $key[2]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobRelatedEntitiesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonJmsJobRelatedEntities A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `job_id`, `related_class`, `related_id` FROM `jms_job_related_entities` WHERE `job_id` = :p0 AND `related_class` = :p1 AND `related_id` = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonJmsJobRelatedEntities();
            $obj->hydrate($row);
            CommonJmsJobRelatedEntitiesPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1], (string) $key[2])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonJmsJobRelatedEntities|CommonJmsJobRelatedEntities[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonJmsJobRelatedEntities[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonJmsJobRelatedEntitiesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::JOB_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::RELATED_CLASS, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::RELATED_ID, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonJmsJobRelatedEntitiesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(CommonJmsJobRelatedEntitiesPeer::JOB_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(CommonJmsJobRelatedEntitiesPeer::RELATED_CLASS, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(CommonJmsJobRelatedEntitiesPeer::RELATED_ID, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the job_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJobId(1234); // WHERE job_id = 1234
     * $query->filterByJobId(array(12, 34)); // WHERE job_id IN (12, 34)
     * $query->filterByJobId(array('min' => 12)); // WHERE job_id >= 12
     * $query->filterByJobId(array('max' => 12)); // WHERE job_id <= 12
     * </code>
     *
     * @see       filterByCommonJmsJobs()
     *
     * @param     mixed $jobId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobRelatedEntitiesQuery The current query, for fluid interface
     */
    public function filterByJobId($jobId = null, $comparison = null)
    {
        if (is_array($jobId)) {
            $useMinMax = false;
            if (isset($jobId['min'])) {
                $this->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::JOB_ID, $jobId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jobId['max'])) {
                $this->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::JOB_ID, $jobId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::JOB_ID, $jobId, $comparison);
    }

    /**
     * Filter the query on the related_class column
     *
     * Example usage:
     * <code>
     * $query->filterByRelatedClass('fooValue');   // WHERE related_class = 'fooValue'
     * $query->filterByRelatedClass('%fooValue%'); // WHERE related_class LIKE '%fooValue%'
     * </code>
     *
     * @param     string $relatedClass The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobRelatedEntitiesQuery The current query, for fluid interface
     */
    public function filterByRelatedClass($relatedClass = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($relatedClass)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $relatedClass)) {
                $relatedClass = str_replace('*', '%', $relatedClass);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::RELATED_CLASS, $relatedClass, $comparison);
    }

    /**
     * Filter the query on the related_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRelatedId('fooValue');   // WHERE related_id = 'fooValue'
     * $query->filterByRelatedId('%fooValue%'); // WHERE related_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $relatedId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobRelatedEntitiesQuery The current query, for fluid interface
     */
    public function filterByRelatedId($relatedId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($relatedId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $relatedId)) {
                $relatedId = str_replace('*', '%', $relatedId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::RELATED_ID, $relatedId, $comparison);
    }

    /**
     * Filter the query by a related CommonJmsJobs object
     *
     * @param   CommonJmsJobs|PropelObjectCollection $commonJmsJobs The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonJmsJobRelatedEntitiesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonJmsJobs($commonJmsJobs, $comparison = null)
    {
        if ($commonJmsJobs instanceof CommonJmsJobs) {
            return $this
                ->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::JOB_ID, $commonJmsJobs->getId(), $comparison);
        } elseif ($commonJmsJobs instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonJmsJobRelatedEntitiesPeer::JOB_ID, $commonJmsJobs->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonJmsJobs() only accepts arguments of type CommonJmsJobs or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonJmsJobs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonJmsJobRelatedEntitiesQuery The current query, for fluid interface
     */
    public function joinCommonJmsJobs($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonJmsJobs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonJmsJobs');
        }

        return $this;
    }

    /**
     * Use the CommonJmsJobs relation CommonJmsJobs object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonJmsJobsQuery A secondary query class using the current class as primary query
     */
    public function useCommonJmsJobsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonJmsJobs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonJmsJobs', '\Application\Propel\Mpe\CommonJmsJobsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonJmsJobRelatedEntities $commonJmsJobRelatedEntities Object to remove from the list of results
     *
     * @return CommonJmsJobRelatedEntitiesQuery The current query, for fluid interface
     */
    public function prune($commonJmsJobRelatedEntities = null)
    {
        if ($commonJmsJobRelatedEntities) {
            $this->addCond('pruneCond0', $this->getAliasedColName(CommonJmsJobRelatedEntitiesPeer::JOB_ID), $commonJmsJobRelatedEntities->getJobId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(CommonJmsJobRelatedEntitiesPeer::RELATED_CLASS), $commonJmsJobRelatedEntities->getRelatedClass(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(CommonJmsJobRelatedEntitiesPeer::RELATED_ID), $commonJmsJobRelatedEntities->getRelatedId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
