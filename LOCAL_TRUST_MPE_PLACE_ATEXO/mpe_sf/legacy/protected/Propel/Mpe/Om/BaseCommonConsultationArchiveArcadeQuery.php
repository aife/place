<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonArchiveArcade;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveArcade;
use Application\Propel\Mpe\CommonConsultationArchiveArcadePeer;
use Application\Propel\Mpe\CommonConsultationArchiveArcadeQuery;

/**
 * Base class that represents a query for the 'consultation_archive_arcade' table.
 *
 *
 *
 * @method CommonConsultationArchiveArcadeQuery orderByConsultationArchiveId($order = Criteria::ASC) Order by the consultation_archive_id column
 * @method CommonConsultationArchiveArcadeQuery orderByArchiveArcadeId($order = Criteria::ASC) Order by the archive_arcade_id column
 *
 * @method CommonConsultationArchiveArcadeQuery groupByConsultationArchiveId() Group by the consultation_archive_id column
 * @method CommonConsultationArchiveArcadeQuery groupByArchiveArcadeId() Group by the archive_arcade_id column
 *
 * @method CommonConsultationArchiveArcadeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConsultationArchiveArcadeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConsultationArchiveArcadeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConsultationArchiveArcadeQuery leftJoinCommonArchiveArcade($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonArchiveArcade relation
 * @method CommonConsultationArchiveArcadeQuery rightJoinCommonArchiveArcade($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonArchiveArcade relation
 * @method CommonConsultationArchiveArcadeQuery innerJoinCommonArchiveArcade($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonArchiveArcade relation
 *
 * @method CommonConsultationArchiveArcadeQuery leftJoinCommonConsultationArchive($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationArchive relation
 * @method CommonConsultationArchiveArcadeQuery rightJoinCommonConsultationArchive($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationArchive relation
 * @method CommonConsultationArchiveArcadeQuery innerJoinCommonConsultationArchive($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationArchive relation
 *
 * @method CommonConsultationArchiveArcade findOne(PropelPDO $con = null) Return the first CommonConsultationArchiveArcade matching the query
 * @method CommonConsultationArchiveArcade findOneOrCreate(PropelPDO $con = null) Return the first CommonConsultationArchiveArcade matching the query, or a new CommonConsultationArchiveArcade object populated from the query conditions when no match is found
 *
 * @method CommonConsultationArchiveArcade findOneByArchiveArcadeId(int $archive_arcade_id) Return the first CommonConsultationArchiveArcade filtered by the archive_arcade_id column
 *
 * @method array findByConsultationArchiveId(int $consultation_archive_id) Return CommonConsultationArchiveArcade objects filtered by the consultation_archive_id column
 * @method array findByArchiveArcadeId(int $archive_arcade_id) Return CommonConsultationArchiveArcade objects filtered by the archive_arcade_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationArchiveArcadeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConsultationArchiveArcadeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConsultationArchiveArcade', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConsultationArchiveArcadeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConsultationArchiveArcadeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConsultationArchiveArcadeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConsultationArchiveArcadeQuery) {
            return $criteria;
        }
        $query = new CommonConsultationArchiveArcadeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConsultationArchiveArcade|CommonConsultationArchiveArcade[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConsultationArchiveArcadePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchiveArcadePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchiveArcade A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByConsultationArchiveId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchiveArcade A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `consultation_archive_id`, `archive_arcade_id` FROM `consultation_archive_arcade` WHERE `consultation_archive_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConsultationArchiveArcade();
            $obj->hydrate($row);
            CommonConsultationArchiveArcadePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConsultationArchiveArcade|CommonConsultationArchiveArcade[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConsultationArchiveArcade[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConsultationArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConsultationArchiveArcadePeer::CONSULTATION_ARCHIVE_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConsultationArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConsultationArchiveArcadePeer::CONSULTATION_ARCHIVE_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the consultation_archive_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationArchiveId(1234); // WHERE consultation_archive_id = 1234
     * $query->filterByConsultationArchiveId(array(12, 34)); // WHERE consultation_archive_id IN (12, 34)
     * $query->filterByConsultationArchiveId(array('min' => 12)); // WHERE consultation_archive_id >= 12
     * $query->filterByConsultationArchiveId(array('max' => 12)); // WHERE consultation_archive_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultationArchive()
     *
     * @param     mixed $consultationArchiveId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByConsultationArchiveId($consultationArchiveId = null, $comparison = null)
    {
        if (is_array($consultationArchiveId)) {
            $useMinMax = false;
            if (isset($consultationArchiveId['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveArcadePeer::CONSULTATION_ARCHIVE_ID, $consultationArchiveId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationArchiveId['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveArcadePeer::CONSULTATION_ARCHIVE_ID, $consultationArchiveId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveArcadePeer::CONSULTATION_ARCHIVE_ID, $consultationArchiveId, $comparison);
    }

    /**
     * Filter the query on the archive_arcade_id column
     *
     * Example usage:
     * <code>
     * $query->filterByArchiveArcadeId(1234); // WHERE archive_arcade_id = 1234
     * $query->filterByArchiveArcadeId(array(12, 34)); // WHERE archive_arcade_id IN (12, 34)
     * $query->filterByArchiveArcadeId(array('min' => 12)); // WHERE archive_arcade_id >= 12
     * $query->filterByArchiveArcadeId(array('max' => 12)); // WHERE archive_arcade_id <= 12
     * </code>
     *
     * @see       filterByCommonArchiveArcade()
     *
     * @param     mixed $archiveArcadeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByArchiveArcadeId($archiveArcadeId = null, $comparison = null)
    {
        if (is_array($archiveArcadeId)) {
            $useMinMax = false;
            if (isset($archiveArcadeId['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveArcadePeer::ARCHIVE_ARCADE_ID, $archiveArcadeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archiveArcadeId['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveArcadePeer::ARCHIVE_ARCADE_ID, $archiveArcadeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveArcadePeer::ARCHIVE_ARCADE_ID, $archiveArcadeId, $comparison);
    }

    /**
     * Filter the query by a related CommonArchiveArcade object
     *
     * @param   CommonArchiveArcade|PropelObjectCollection $commonArchiveArcade The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationArchiveArcadeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonArchiveArcade($commonArchiveArcade, $comparison = null)
    {
        if ($commonArchiveArcade instanceof CommonArchiveArcade) {
            return $this
                ->addUsingAlias(CommonConsultationArchiveArcadePeer::ARCHIVE_ARCADE_ID, $commonArchiveArcade->getId(), $comparison);
        } elseif ($commonArchiveArcade instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationArchiveArcadePeer::ARCHIVE_ARCADE_ID, $commonArchiveArcade->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonArchiveArcade() only accepts arguments of type CommonArchiveArcade or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonArchiveArcade relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationArchiveArcadeQuery The current query, for fluid interface
     */
    public function joinCommonArchiveArcade($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonArchiveArcade');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonArchiveArcade');
        }

        return $this;
    }

    /**
     * Use the CommonArchiveArcade relation CommonArchiveArcade object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonArchiveArcadeQuery A secondary query class using the current class as primary query
     */
    public function useCommonArchiveArcadeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonArchiveArcade($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonArchiveArcade', '\Application\Propel\Mpe\CommonArchiveArcadeQuery');
    }

    /**
     * Filter the query by a related CommonConsultationArchive object
     *
     * @param   CommonConsultationArchive|PropelObjectCollection $commonConsultationArchive The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationArchiveArcadeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationArchive($commonConsultationArchive, $comparison = null)
    {
        if ($commonConsultationArchive instanceof CommonConsultationArchive) {
            return $this
                ->addUsingAlias(CommonConsultationArchiveArcadePeer::CONSULTATION_ARCHIVE_ID, $commonConsultationArchive->getId(), $comparison);
        } elseif ($commonConsultationArchive instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationArchiveArcadePeer::CONSULTATION_ARCHIVE_ID, $commonConsultationArchive->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultationArchive() only accepts arguments of type CommonConsultationArchive or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationArchive relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationArchiveArcadeQuery The current query, for fluid interface
     */
    public function joinCommonConsultationArchive($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationArchive');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationArchive');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationArchive relation CommonConsultationArchive object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationArchiveQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationArchiveQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationArchive($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationArchive', '\Application\Propel\Mpe\CommonConsultationArchiveQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConsultationArchiveArcade $commonConsultationArchiveArcade Object to remove from the list of results
     *
     * @return CommonConsultationArchiveArcadeQuery The current query, for fluid interface
     */
    public function prune($commonConsultationArchiveArcade = null)
    {
        if ($commonConsultationArchiveArcade) {
            $this->addUsingAlias(CommonConsultationArchiveArcadePeer::CONSULTATION_ARCHIVE_ID, $commonConsultationArchiveArcade->getConsultationArchiveId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
