<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTDonneesConsultation;
use Application\Propel\Mpe\CommonTDonneesConsultationPeer;
use Application\Propel\Mpe\CommonTDonneesConsultationQuery;

/**
 * Base class that represents a row from the 't_donnees_consultation' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDonneesConsultation extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTDonneesConsultationPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTDonneesConsultationPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_donnees_consultation field.
     * @var        int
     */
    protected $id_donnees_consultation;

    /**
     * The value for the reference_consultation field.
     * @var        int
     */
    protected $reference_consultation;

    /**
     * The value for the id_contrat_titulaire field.
     * @var        int
     */
    protected $id_contrat_titulaire;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the old_service_id field.
     * @var        int
     */
    protected $old_service_id;

    /**
     * The value for the id_type_procedure field.
     * @var        int
     */
    protected $id_type_procedure;

    /**
     * The value for the libelle_type_procedure field.
     * @var        string
     */
    protected $libelle_type_procedure;

    /**
     * The value for the nbre_offres_recues field.
     * @var        int
     */
    protected $nbre_offres_recues;

    /**
     * The value for the nbre_offres_dematerialisees field.
     * @var        int
     */
    protected $nbre_offres_dematerialisees;

    /**
     * The value for the signature_offre field.
     * @var        string
     */
    protected $signature_offre;

    /**
     * The value for the service_id field.
     * @var        string
     */
    protected $service_id;

    /**
     * The value for the uuid_externe_exec field.
     * @var        string
     */
    protected $uuid_externe_exec;

    /**
     * @var        CommonTContratTitulaire
     */
    protected $aCommonTContratTitulaire;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [id_donnees_consultation] column value.
     *
     * @return int
     */
    public function getIdDonneesConsultation()
    {

        return $this->id_donnees_consultation;
    }

    /**
     * Get the [reference_consultation] column value.
     *
     * @return int
     */
    public function getReferenceConsultation()
    {

        return $this->reference_consultation;
    }

    /**
     * Get the [id_contrat_titulaire] column value.
     *
     * @return int
     */
    public function getIdContratTitulaire()
    {

        return $this->id_contrat_titulaire;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [old_service_id] column value.
     *
     * @return int
     */
    public function getOldServiceId()
    {

        return $this->old_service_id;
    }

    /**
     * Get the [id_type_procedure] column value.
     *
     * @return int
     */
    public function getIdTypeProcedure()
    {

        return $this->id_type_procedure;
    }

    /**
     * Get the [libelle_type_procedure] column value.
     *
     * @return string
     */
    public function getLibelleTypeProcedure()
    {

        return $this->libelle_type_procedure;
    }

    /**
     * Get the [nbre_offres_recues] column value.
     *
     * @return int
     */
    public function getNbreOffresRecues()
    {

        return $this->nbre_offres_recues;
    }

    /**
     * Get the [nbre_offres_dematerialisees] column value.
     *
     * @return int
     */
    public function getNbreOffresDematerialisees()
    {

        return $this->nbre_offres_dematerialisees;
    }

    /**
     * Get the [signature_offre] column value.
     *
     * @return string
     */
    public function getSignatureOffre()
    {

        return $this->signature_offre;
    }

    /**
     * Get the [service_id] column value.
     *
     * @return string
     */
    public function getServiceId()
    {

        return $this->service_id;
    }

    /**
     * Get the [uuid_externe_exec] column value.
     *
     * @return string
     */
    public function getUuidExterneExec()
    {

        return $this->uuid_externe_exec;
    }

    /**
     * Set the value of [id_donnees_consultation] column.
     *
     * @param int $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setIdDonneesConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_donnees_consultation !== $v) {
            $this->id_donnees_consultation = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION;
        }


        return $this;
    } // setIdDonneesConsultation()

    /**
     * Set the value of [reference_consultation] column.
     *
     * @param int $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setReferenceConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->reference_consultation !== $v) {
            $this->reference_consultation = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::REFERENCE_CONSULTATION;
        }


        return $this;
    } // setReferenceConsultation()

    /**
     * Set the value of [id_contrat_titulaire] column.
     *
     * @param int $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setIdContratTitulaire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_contrat_titulaire !== $v) {
            $this->id_contrat_titulaire = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE;
        }

        if ($this->aCommonTContratTitulaire !== null && $this->aCommonTContratTitulaire->getIdContratTitulaire() !== $v) {
            $this->aCommonTContratTitulaire = null;
        }


        return $this;
    } // setIdContratTitulaire()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [old_service_id] column.
     *
     * @param int $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setOldServiceId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_service_id !== $v) {
            $this->old_service_id = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::OLD_SERVICE_ID;
        }


        return $this;
    } // setOldServiceId()

    /**
     * Set the value of [id_type_procedure] column.
     *
     * @param int $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setIdTypeProcedure($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_procedure !== $v) {
            $this->id_type_procedure = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::ID_TYPE_PROCEDURE;
        }


        return $this;
    } // setIdTypeProcedure()

    /**
     * Set the value of [libelle_type_procedure] column.
     *
     * @param string $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setLibelleTypeProcedure($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_type_procedure !== $v) {
            $this->libelle_type_procedure = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::LIBELLE_TYPE_PROCEDURE;
        }


        return $this;
    } // setLibelleTypeProcedure()

    /**
     * Set the value of [nbre_offres_recues] column.
     *
     * @param int $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setNbreOffresRecues($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->nbre_offres_recues !== $v) {
            $this->nbre_offres_recues = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::NBRE_OFFRES_RECUES;
        }


        return $this;
    } // setNbreOffresRecues()

    /**
     * Set the value of [nbre_offres_dematerialisees] column.
     *
     * @param int $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setNbreOffresDematerialisees($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->nbre_offres_dematerialisees !== $v) {
            $this->nbre_offres_dematerialisees = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::NBRE_OFFRES_DEMATERIALISEES;
        }


        return $this;
    } // setNbreOffresDematerialisees()

    /**
     * Set the value of [signature_offre] column.
     *
     * @param string $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setSignatureOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->signature_offre !== $v) {
            $this->signature_offre = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::SIGNATURE_OFFRE;
        }


        return $this;
    } // setSignatureOffre()

    /**
     * Set the value of [service_id] column.
     *
     * @param string $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setServiceId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->service_id !== $v) {
            $this->service_id = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::SERVICE_ID;
        }


        return $this;
    } // setServiceId()

    /**
     * Set the value of [uuid_externe_exec] column.
     *
     * @param string $v new value
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     */
    public function setUuidExterneExec($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->uuid_externe_exec !== $v) {
            $this->uuid_externe_exec = $v;
            $this->modifiedColumns[] = CommonTDonneesConsultationPeer::UUID_EXTERNE_EXEC;
        }


        return $this;
    } // setUuidExterneExec()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_donnees_consultation = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->reference_consultation = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_contrat_titulaire = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->organisme = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->old_service_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->id_type_procedure = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->libelle_type_procedure = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->nbre_offres_recues = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->nbre_offres_dematerialisees = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->signature_offre = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->service_id = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->uuid_externe_exec = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 12; // 12 = CommonTDonneesConsultationPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTDonneesConsultation object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTContratTitulaire !== null && $this->id_contrat_titulaire !== $this->aCommonTContratTitulaire->getIdContratTitulaire()) {
            $this->aCommonTContratTitulaire = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDonneesConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTDonneesConsultationPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTContratTitulaire = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDonneesConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTDonneesConsultationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDonneesConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTDonneesConsultationPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTContratTitulaire !== null) {
                if ($this->aCommonTContratTitulaire->isModified() || $this->aCommonTContratTitulaire->isNew()) {
                    $affectedRows += $this->aCommonTContratTitulaire->save($con);
                }
                $this->setCommonTContratTitulaire($this->aCommonTContratTitulaire);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION;
        if (null !== $this->id_donnees_consultation) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`id_donnees_consultation`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::REFERENCE_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`reference_consultation`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_contrat_titulaire`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::OLD_SERVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`old_service_id`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::ID_TYPE_PROCEDURE)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_procedure`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::LIBELLE_TYPE_PROCEDURE)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_type_procedure`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::NBRE_OFFRES_RECUES)) {
            $modifiedColumns[':p' . $index++]  = '`nbre_offres_recues`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::NBRE_OFFRES_DEMATERIALISEES)) {
            $modifiedColumns[':p' . $index++]  = '`nbre_offres_dematerialisees`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::SIGNATURE_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`signature_offre`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::SERVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`service_id`';
        }
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::UUID_EXTERNE_EXEC)) {
            $modifiedColumns[':p' . $index++]  = '`uuid_externe_exec`';
        }

        $sql = sprintf(
            'INSERT INTO `t_donnees_consultation` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_donnees_consultation`':
                        $stmt->bindValue($identifier, $this->id_donnees_consultation, PDO::PARAM_INT);
                        break;
                    case '`reference_consultation`':
                        $stmt->bindValue($identifier, $this->reference_consultation, PDO::PARAM_INT);
                        break;
                    case '`id_contrat_titulaire`':
                        $stmt->bindValue($identifier, $this->id_contrat_titulaire, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`old_service_id`':
                        $stmt->bindValue($identifier, $this->old_service_id, PDO::PARAM_INT);
                        break;
                    case '`id_type_procedure`':
                        $stmt->bindValue($identifier, $this->id_type_procedure, PDO::PARAM_INT);
                        break;
                    case '`libelle_type_procedure`':
                        $stmt->bindValue($identifier, $this->libelle_type_procedure, PDO::PARAM_STR);
                        break;
                    case '`nbre_offres_recues`':
                        $stmt->bindValue($identifier, $this->nbre_offres_recues, PDO::PARAM_INT);
                        break;
                    case '`nbre_offres_dematerialisees`':
                        $stmt->bindValue($identifier, $this->nbre_offres_dematerialisees, PDO::PARAM_INT);
                        break;
                    case '`signature_offre`':
                        $stmt->bindValue($identifier, $this->signature_offre, PDO::PARAM_STR);
                        break;
                    case '`service_id`':
                        $stmt->bindValue($identifier, $this->service_id, PDO::PARAM_STR);
                        break;
                    case '`uuid_externe_exec`':
                        $stmt->bindValue($identifier, $this->uuid_externe_exec, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdDonneesConsultation($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTContratTitulaire !== null) {
                if (!$this->aCommonTContratTitulaire->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTContratTitulaire->getValidationFailures());
                }
            }


            if (($retval = CommonTDonneesConsultationPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDonneesConsultationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdDonneesConsultation();
                break;
            case 1:
                return $this->getReferenceConsultation();
                break;
            case 2:
                return $this->getIdContratTitulaire();
                break;
            case 3:
                return $this->getOrganisme();
                break;
            case 4:
                return $this->getOldServiceId();
                break;
            case 5:
                return $this->getIdTypeProcedure();
                break;
            case 6:
                return $this->getLibelleTypeProcedure();
                break;
            case 7:
                return $this->getNbreOffresRecues();
                break;
            case 8:
                return $this->getNbreOffresDematerialisees();
                break;
            case 9:
                return $this->getSignatureOffre();
                break;
            case 10:
                return $this->getServiceId();
                break;
            case 11:
                return $this->getUuidExterneExec();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTDonneesConsultation'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTDonneesConsultation'][$this->getPrimaryKey()] = true;
        $keys = CommonTDonneesConsultationPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdDonneesConsultation(),
            $keys[1] => $this->getReferenceConsultation(),
            $keys[2] => $this->getIdContratTitulaire(),
            $keys[3] => $this->getOrganisme(),
            $keys[4] => $this->getOldServiceId(),
            $keys[5] => $this->getIdTypeProcedure(),
            $keys[6] => $this->getLibelleTypeProcedure(),
            $keys[7] => $this->getNbreOffresRecues(),
            $keys[8] => $this->getNbreOffresDematerialisees(),
            $keys[9] => $this->getSignatureOffre(),
            $keys[10] => $this->getServiceId(),
            $keys[11] => $this->getUuidExterneExec(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTContratTitulaire) {
                $result['CommonTContratTitulaire'] = $this->aCommonTContratTitulaire->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDonneesConsultationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdDonneesConsultation($value);
                break;
            case 1:
                $this->setReferenceConsultation($value);
                break;
            case 2:
                $this->setIdContratTitulaire($value);
                break;
            case 3:
                $this->setOrganisme($value);
                break;
            case 4:
                $this->setOldServiceId($value);
                break;
            case 5:
                $this->setIdTypeProcedure($value);
                break;
            case 6:
                $this->setLibelleTypeProcedure($value);
                break;
            case 7:
                $this->setNbreOffresRecues($value);
                break;
            case 8:
                $this->setNbreOffresDematerialisees($value);
                break;
            case 9:
                $this->setSignatureOffre($value);
                break;
            case 10:
                $this->setServiceId($value);
                break;
            case 11:
                $this->setUuidExterneExec($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTDonneesConsultationPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdDonneesConsultation($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setReferenceConsultation($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdContratTitulaire($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setOrganisme($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setOldServiceId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setIdTypeProcedure($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLibelleTypeProcedure($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setNbreOffresRecues($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setNbreOffresDematerialisees($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setSignatureOffre($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setServiceId($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setUuidExterneExec($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTDonneesConsultationPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION)) $criteria->add(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION, $this->id_donnees_consultation);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::REFERENCE_CONSULTATION)) $criteria->add(CommonTDonneesConsultationPeer::REFERENCE_CONSULTATION, $this->reference_consultation);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE)) $criteria->add(CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE, $this->id_contrat_titulaire);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::ORGANISME)) $criteria->add(CommonTDonneesConsultationPeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::OLD_SERVICE_ID)) $criteria->add(CommonTDonneesConsultationPeer::OLD_SERVICE_ID, $this->old_service_id);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::ID_TYPE_PROCEDURE)) $criteria->add(CommonTDonneesConsultationPeer::ID_TYPE_PROCEDURE, $this->id_type_procedure);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::LIBELLE_TYPE_PROCEDURE)) $criteria->add(CommonTDonneesConsultationPeer::LIBELLE_TYPE_PROCEDURE, $this->libelle_type_procedure);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::NBRE_OFFRES_RECUES)) $criteria->add(CommonTDonneesConsultationPeer::NBRE_OFFRES_RECUES, $this->nbre_offres_recues);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::NBRE_OFFRES_DEMATERIALISEES)) $criteria->add(CommonTDonneesConsultationPeer::NBRE_OFFRES_DEMATERIALISEES, $this->nbre_offres_dematerialisees);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::SIGNATURE_OFFRE)) $criteria->add(CommonTDonneesConsultationPeer::SIGNATURE_OFFRE, $this->signature_offre);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::SERVICE_ID)) $criteria->add(CommonTDonneesConsultationPeer::SERVICE_ID, $this->service_id);
        if ($this->isColumnModified(CommonTDonneesConsultationPeer::UUID_EXTERNE_EXEC)) $criteria->add(CommonTDonneesConsultationPeer::UUID_EXTERNE_EXEC, $this->uuid_externe_exec);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTDonneesConsultationPeer::DATABASE_NAME);
        $criteria->add(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION, $this->id_donnees_consultation);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdDonneesConsultation();
    }

    /**
     * Generic method to set the primary key (id_donnees_consultation column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdDonneesConsultation($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdDonneesConsultation();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTDonneesConsultation (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setReferenceConsultation($this->getReferenceConsultation());
        $copyObj->setIdContratTitulaire($this->getIdContratTitulaire());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setOldServiceId($this->getOldServiceId());
        $copyObj->setIdTypeProcedure($this->getIdTypeProcedure());
        $copyObj->setLibelleTypeProcedure($this->getLibelleTypeProcedure());
        $copyObj->setNbreOffresRecues($this->getNbreOffresRecues());
        $copyObj->setNbreOffresDematerialisees($this->getNbreOffresDematerialisees());
        $copyObj->setSignatureOffre($this->getSignatureOffre());
        $copyObj->setServiceId($this->getServiceId());
        $copyObj->setUuidExterneExec($this->getUuidExterneExec());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdDonneesConsultation(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTDonneesConsultation Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTDonneesConsultationPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTDonneesConsultationPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonTContratTitulaire object.
     *
     * @param   CommonTContratTitulaire $v
     * @return CommonTDonneesConsultation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTContratTitulaire(CommonTContratTitulaire $v = null)
    {
        if ($v === null) {
            $this->setIdContratTitulaire(NULL);
        } else {
            $this->setIdContratTitulaire($v->getIdContratTitulaire());
        }

        $this->aCommonTContratTitulaire = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTContratTitulaire object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTDonneesConsultation($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTContratTitulaire object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTContratTitulaire The associated CommonTContratTitulaire object.
     * @throws PropelException
     */
    public function getCommonTContratTitulaire(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTContratTitulaire === null && ($this->id_contrat_titulaire !== null) && $doQuery) {
            $this->aCommonTContratTitulaire = CommonTContratTitulaireQuery::create()->findPk($this->id_contrat_titulaire, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTContratTitulaire->addCommonTDonneesConsultations($this);
             */
        }

        return $this->aCommonTContratTitulaire;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_donnees_consultation = null;
        $this->reference_consultation = null;
        $this->id_contrat_titulaire = null;
        $this->organisme = null;
        $this->old_service_id = null;
        $this->id_type_procedure = null;
        $this->libelle_type_procedure = null;
        $this->nbre_offres_recues = null;
        $this->nbre_offres_dematerialisees = null;
        $this->signature_offre = null;
        $this->service_id = null;
        $this->uuid_externe_exec = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonTContratTitulaire instanceof Persistent) {
              $this->aCommonTContratTitulaire->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonTContratTitulaire = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTDonneesConsultationPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
