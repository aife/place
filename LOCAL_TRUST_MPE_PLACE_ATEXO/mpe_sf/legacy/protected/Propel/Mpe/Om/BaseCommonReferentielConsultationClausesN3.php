<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationClausesN3;
use Application\Propel\Mpe\CommonConsultationClausesN3Query;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN2ClausesN3;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN2ClausesN3Query;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN3;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN3Peer;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN3Query;

/**
 * Base class that represents a row from the 'referentiel_consultation_clauses_n3' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonReferentielConsultationClausesN3 extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonReferentielConsultationClausesN3Peer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonReferentielConsultationClausesN3Peer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the label field.
     * @var        string
     */
    protected $label;

    /**
     * The value for the slug field.
     * @var        string
     */
    protected $slug;

    /**
     * @var        PropelObjectCollection|CommonConsultationClausesN3[] Collection to store aggregation of CommonConsultationClausesN3 objects.
     */
    protected $collCommonConsultationClausesN3s;
    protected $collCommonConsultationClausesN3sPartial;

    /**
     * @var        PropelObjectCollection|CommonReferentielConsultationClausesN2ClausesN3[] Collection to store aggregation of CommonReferentielConsultationClausesN2ClausesN3 objects.
     */
    protected $collCommonReferentielConsultationClausesN2ClausesN3s;
    protected $collCommonReferentielConsultationClausesN2ClausesN3sPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationClausesN3sScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [label] column value.
     *
     * @return string
     */
    public function getLabel()
    {

        return $this->label;
    }

    /**
     * Get the [slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {

        return $this->slug;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonReferentielConsultationClausesN3Peer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [label] column.
     *
     * @param string $v new value
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     */
    public function setLabel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->label !== $v) {
            $this->label = $v;
            $this->modifiedColumns[] = CommonReferentielConsultationClausesN3Peer::LABEL;
        }


        return $this;
    } // setLabel()

    /**
     * Set the value of [slug] column.
     *
     * @param string $v new value
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->slug !== $v) {
            $this->slug = $v;
            $this->modifiedColumns[] = CommonReferentielConsultationClausesN3Peer::SLUG;
        }


        return $this;
    } // setSlug()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->label = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->slug = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 3; // 3 = CommonReferentielConsultationClausesN3Peer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonReferentielConsultationClausesN3 object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonReferentielConsultationClausesN3Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonReferentielConsultationClausesN3Peer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonConsultationClausesN3s = null;

            $this->collCommonReferentielConsultationClausesN2ClausesN3s = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonReferentielConsultationClausesN3Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonReferentielConsultationClausesN3Query::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonReferentielConsultationClausesN3Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonReferentielConsultationClausesN3Peer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonConsultationClausesN3sScheduledForDeletion !== null) {
                if (!$this->commonConsultationClausesN3sScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationClausesN3sScheduledForDeletion as $commonConsultationClausesN3) {
                        // need to save related object because we set the relation to null
                        $commonConsultationClausesN3->save($con);
                    }
                    $this->commonConsultationClausesN3sScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultationClausesN3s !== null) {
                foreach ($this->collCommonConsultationClausesN3s as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion !== null) {
                if (!$this->commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonReferentielConsultationClausesN2ClausesN3Query::create()
                        ->filterByPrimaryKeys($this->commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion = null;
                }
            }

            if ($this->collCommonReferentielConsultationClausesN2ClausesN3s !== null) {
                foreach ($this->collCommonReferentielConsultationClausesN2ClausesN3s as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonReferentielConsultationClausesN3Peer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonReferentielConsultationClausesN3Peer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonReferentielConsultationClausesN3Peer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonReferentielConsultationClausesN3Peer::LABEL)) {
            $modifiedColumns[':p' . $index++]  = '`label`';
        }
        if ($this->isColumnModified(CommonReferentielConsultationClausesN3Peer::SLUG)) {
            $modifiedColumns[':p' . $index++]  = '`slug`';
        }

        $sql = sprintf(
            'INSERT INTO `referentiel_consultation_clauses_n3` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`label`':
                        $stmt->bindValue($identifier, $this->label, PDO::PARAM_STR);
                        break;
                    case '`slug`':
                        $stmt->bindValue($identifier, $this->slug, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonReferentielConsultationClausesN3Peer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonConsultationClausesN3s !== null) {
                    foreach ($this->collCommonConsultationClausesN3s as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonReferentielConsultationClausesN2ClausesN3s !== null) {
                    foreach ($this->collCommonReferentielConsultationClausesN2ClausesN3s as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonReferentielConsultationClausesN3Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getLabel();
                break;
            case 2:
                return $this->getSlug();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonReferentielConsultationClausesN3'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonReferentielConsultationClausesN3'][$this->getPrimaryKey()] = true;
        $keys = CommonReferentielConsultationClausesN3Peer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getLabel(),
            $keys[2] => $this->getSlug(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonConsultationClausesN3s) {
                $result['CommonConsultationClausesN3s'] = $this->collCommonConsultationClausesN3s->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonReferentielConsultationClausesN2ClausesN3s) {
                $result['CommonReferentielConsultationClausesN2ClausesN3s'] = $this->collCommonReferentielConsultationClausesN2ClausesN3s->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonReferentielConsultationClausesN3Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setLabel($value);
                break;
            case 2:
                $this->setSlug($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonReferentielConsultationClausesN3Peer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setLabel($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSlug($arr[$keys[2]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonReferentielConsultationClausesN3Peer::DATABASE_NAME);

        if ($this->isColumnModified(CommonReferentielConsultationClausesN3Peer::ID)) $criteria->add(CommonReferentielConsultationClausesN3Peer::ID, $this->id);
        if ($this->isColumnModified(CommonReferentielConsultationClausesN3Peer::LABEL)) $criteria->add(CommonReferentielConsultationClausesN3Peer::LABEL, $this->label);
        if ($this->isColumnModified(CommonReferentielConsultationClausesN3Peer::SLUG)) $criteria->add(CommonReferentielConsultationClausesN3Peer::SLUG, $this->slug);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonReferentielConsultationClausesN3Peer::DATABASE_NAME);
        $criteria->add(CommonReferentielConsultationClausesN3Peer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonReferentielConsultationClausesN3 (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLabel($this->getLabel());
        $copyObj->setSlug($this->getSlug());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonConsultationClausesN3s() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultationClausesN3($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonReferentielConsultationClausesN2ClausesN3s() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonReferentielConsultationClausesN2ClausesN3($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonReferentielConsultationClausesN3 Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonReferentielConsultationClausesN3Peer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonReferentielConsultationClausesN3Peer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonConsultationClausesN3' == $relationName) {
            $this->initCommonConsultationClausesN3s();
        }
        if ('CommonReferentielConsultationClausesN2ClausesN3' == $relationName) {
            $this->initCommonReferentielConsultationClausesN2ClausesN3s();
        }
    }

    /**
     * Clears out the collCommonConsultationClausesN3s collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     * @see        addCommonConsultationClausesN3s()
     */
    public function clearCommonConsultationClausesN3s()
    {
        $this->collCommonConsultationClausesN3s = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationClausesN3sPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultationClausesN3s collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultationClausesN3s($v = true)
    {
        $this->collCommonConsultationClausesN3sPartial = $v;
    }

    /**
     * Initializes the collCommonConsultationClausesN3s collection.
     *
     * By default this just sets the collCommonConsultationClausesN3s collection to an empty array (like clearcollCommonConsultationClausesN3s());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultationClausesN3s($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultationClausesN3s && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultationClausesN3s = new PropelObjectCollection();
        $this->collCommonConsultationClausesN3s->setModel('CommonConsultationClausesN3');
    }

    /**
     * Gets an array of CommonConsultationClausesN3 objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonReferentielConsultationClausesN3 is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultationClausesN3[] List of CommonConsultationClausesN3 objects
     * @throws PropelException
     */
    public function getCommonConsultationClausesN3s($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationClausesN3sPartial && !$this->isNew();
        if (null === $this->collCommonConsultationClausesN3s || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationClausesN3s) {
                // return empty collection
                $this->initCommonConsultationClausesN3s();
            } else {
                $collCommonConsultationClausesN3s = CommonConsultationClausesN3Query::create(null, $criteria)
                    ->filterByCommonReferentielConsultationClausesN3($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationClausesN3sPartial && count($collCommonConsultationClausesN3s)) {
                      $this->initCommonConsultationClausesN3s(false);

                      foreach ($collCommonConsultationClausesN3s as $obj) {
                        if (false == $this->collCommonConsultationClausesN3s->contains($obj)) {
                          $this->collCommonConsultationClausesN3s->append($obj);
                        }
                      }

                      $this->collCommonConsultationClausesN3sPartial = true;
                    }

                    $collCommonConsultationClausesN3s->getInternalIterator()->rewind();

                    return $collCommonConsultationClausesN3s;
                }

                if ($partial && $this->collCommonConsultationClausesN3s) {
                    foreach ($this->collCommonConsultationClausesN3s as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultationClausesN3s[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultationClausesN3s = $collCommonConsultationClausesN3s;
                $this->collCommonConsultationClausesN3sPartial = false;
            }
        }

        return $this->collCommonConsultationClausesN3s;
    }

    /**
     * Sets a collection of CommonConsultationClausesN3 objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultationClausesN3s A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     */
    public function setCommonConsultationClausesN3s(PropelCollection $commonConsultationClausesN3s, PropelPDO $con = null)
    {
        $commonConsultationClausesN3sToDelete = $this->getCommonConsultationClausesN3s(new Criteria(), $con)->diff($commonConsultationClausesN3s);


        $this->commonConsultationClausesN3sScheduledForDeletion = $commonConsultationClausesN3sToDelete;

        foreach ($commonConsultationClausesN3sToDelete as $commonConsultationClausesN3Removed) {
            $commonConsultationClausesN3Removed->setCommonReferentielConsultationClausesN3(null);
        }

        $this->collCommonConsultationClausesN3s = null;
        foreach ($commonConsultationClausesN3s as $commonConsultationClausesN3) {
            $this->addCommonConsultationClausesN3($commonConsultationClausesN3);
        }

        $this->collCommonConsultationClausesN3s = $commonConsultationClausesN3s;
        $this->collCommonConsultationClausesN3sPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultationClausesN3 objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultationClausesN3 objects.
     * @throws PropelException
     */
    public function countCommonConsultationClausesN3s(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationClausesN3sPartial && !$this->isNew();
        if (null === $this->collCommonConsultationClausesN3s || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationClausesN3s) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultationClausesN3s());
            }
            $query = CommonConsultationClausesN3Query::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonReferentielConsultationClausesN3($this)
                ->count($con);
        }

        return count($this->collCommonConsultationClausesN3s);
    }

    /**
     * Method called to associate a CommonConsultationClausesN3 object to this object
     * through the CommonConsultationClausesN3 foreign key attribute.
     *
     * @param   CommonConsultationClausesN3 $l CommonConsultationClausesN3
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     */
    public function addCommonConsultationClausesN3(CommonConsultationClausesN3 $l)
    {
        if ($this->collCommonConsultationClausesN3s === null) {
            $this->initCommonConsultationClausesN3s();
            $this->collCommonConsultationClausesN3sPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultationClausesN3s->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultationClausesN3($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultationClausesN3 $commonConsultationClausesN3 The commonConsultationClausesN3 object to add.
     */
    protected function doAddCommonConsultationClausesN3($commonConsultationClausesN3)
    {
        $this->collCommonConsultationClausesN3s[]= $commonConsultationClausesN3;
        $commonConsultationClausesN3->setCommonReferentielConsultationClausesN3($this);
    }

    /**
     * @param	CommonConsultationClausesN3 $commonConsultationClausesN3 The commonConsultationClausesN3 object to remove.
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     */
    public function removeCommonConsultationClausesN3($commonConsultationClausesN3)
    {
        if ($this->getCommonConsultationClausesN3s()->contains($commonConsultationClausesN3)) {
            $this->collCommonConsultationClausesN3s->remove($this->collCommonConsultationClausesN3s->search($commonConsultationClausesN3));
            if (null === $this->commonConsultationClausesN3sScheduledForDeletion) {
                $this->commonConsultationClausesN3sScheduledForDeletion = clone $this->collCommonConsultationClausesN3s;
                $this->commonConsultationClausesN3sScheduledForDeletion->clear();
            }
            $this->commonConsultationClausesN3sScheduledForDeletion[]= clone $commonConsultationClausesN3;
            $commonConsultationClausesN3->setCommonReferentielConsultationClausesN3(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonReferentielConsultationClausesN3 is new, it will return
     * an empty collection; or if this CommonReferentielConsultationClausesN3 has previously
     * been saved, it will retrieve related CommonConsultationClausesN3s from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonReferentielConsultationClausesN3.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultationClausesN3[] List of CommonConsultationClausesN3 objects
     */
    public function getCommonConsultationClausesN3sJoinCommonConsultationClausesN2($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationClausesN3Query::create(null, $criteria);
        $query->joinWith('CommonConsultationClausesN2', $join_behavior);

        return $this->getCommonConsultationClausesN3s($query, $con);
    }

    /**
     * Clears out the collCommonReferentielConsultationClausesN2ClausesN3s collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     * @see        addCommonReferentielConsultationClausesN2ClausesN3s()
     */
    public function clearCommonReferentielConsultationClausesN2ClausesN3s()
    {
        $this->collCommonReferentielConsultationClausesN2ClausesN3s = null; // important to set this to null since that means it is uninitialized
        $this->collCommonReferentielConsultationClausesN2ClausesN3sPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonReferentielConsultationClausesN2ClausesN3s collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonReferentielConsultationClausesN2ClausesN3s($v = true)
    {
        $this->collCommonReferentielConsultationClausesN2ClausesN3sPartial = $v;
    }

    /**
     * Initializes the collCommonReferentielConsultationClausesN2ClausesN3s collection.
     *
     * By default this just sets the collCommonReferentielConsultationClausesN2ClausesN3s collection to an empty array (like clearcollCommonReferentielConsultationClausesN2ClausesN3s());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonReferentielConsultationClausesN2ClausesN3s($overrideExisting = true)
    {
        if (null !== $this->collCommonReferentielConsultationClausesN2ClausesN3s && !$overrideExisting) {
            return;
        }
        $this->collCommonReferentielConsultationClausesN2ClausesN3s = new PropelObjectCollection();
        $this->collCommonReferentielConsultationClausesN2ClausesN3s->setModel('CommonReferentielConsultationClausesN2ClausesN3');
    }

    /**
     * Gets an array of CommonReferentielConsultationClausesN2ClausesN3 objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonReferentielConsultationClausesN3 is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonReferentielConsultationClausesN2ClausesN3[] List of CommonReferentielConsultationClausesN2ClausesN3 objects
     * @throws PropelException
     */
    public function getCommonReferentielConsultationClausesN2ClausesN3s($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonReferentielConsultationClausesN2ClausesN3sPartial && !$this->isNew();
        if (null === $this->collCommonReferentielConsultationClausesN2ClausesN3s || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonReferentielConsultationClausesN2ClausesN3s) {
                // return empty collection
                $this->initCommonReferentielConsultationClausesN2ClausesN3s();
            } else {
                $collCommonReferentielConsultationClausesN2ClausesN3s = CommonReferentielConsultationClausesN2ClausesN3Query::create(null, $criteria)
                    ->filterByCommonReferentielConsultationClausesN3($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonReferentielConsultationClausesN2ClausesN3sPartial && count($collCommonReferentielConsultationClausesN2ClausesN3s)) {
                      $this->initCommonReferentielConsultationClausesN2ClausesN3s(false);

                      foreach ($collCommonReferentielConsultationClausesN2ClausesN3s as $obj) {
                        if (false == $this->collCommonReferentielConsultationClausesN2ClausesN3s->contains($obj)) {
                          $this->collCommonReferentielConsultationClausesN2ClausesN3s->append($obj);
                        }
                      }

                      $this->collCommonReferentielConsultationClausesN2ClausesN3sPartial = true;
                    }

                    $collCommonReferentielConsultationClausesN2ClausesN3s->getInternalIterator()->rewind();

                    return $collCommonReferentielConsultationClausesN2ClausesN3s;
                }

                if ($partial && $this->collCommonReferentielConsultationClausesN2ClausesN3s) {
                    foreach ($this->collCommonReferentielConsultationClausesN2ClausesN3s as $obj) {
                        if ($obj->isNew()) {
                            $collCommonReferentielConsultationClausesN2ClausesN3s[] = $obj;
                        }
                    }
                }

                $this->collCommonReferentielConsultationClausesN2ClausesN3s = $collCommonReferentielConsultationClausesN2ClausesN3s;
                $this->collCommonReferentielConsultationClausesN2ClausesN3sPartial = false;
            }
        }

        return $this->collCommonReferentielConsultationClausesN2ClausesN3s;
    }

    /**
     * Sets a collection of CommonReferentielConsultationClausesN2ClausesN3 objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonReferentielConsultationClausesN2ClausesN3s A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     */
    public function setCommonReferentielConsultationClausesN2ClausesN3s(PropelCollection $commonReferentielConsultationClausesN2ClausesN3s, PropelPDO $con = null)
    {
        $commonReferentielConsultationClausesN2ClausesN3sToDelete = $this->getCommonReferentielConsultationClausesN2ClausesN3s(new Criteria(), $con)->diff($commonReferentielConsultationClausesN2ClausesN3s);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion = clone $commonReferentielConsultationClausesN2ClausesN3sToDelete;

        foreach ($commonReferentielConsultationClausesN2ClausesN3sToDelete as $commonReferentielConsultationClausesN2ClausesN3Removed) {
            $commonReferentielConsultationClausesN2ClausesN3Removed->setCommonReferentielConsultationClausesN3(null);
        }

        $this->collCommonReferentielConsultationClausesN2ClausesN3s = null;
        foreach ($commonReferentielConsultationClausesN2ClausesN3s as $commonReferentielConsultationClausesN2ClausesN3) {
            $this->addCommonReferentielConsultationClausesN2ClausesN3($commonReferentielConsultationClausesN2ClausesN3);
        }

        $this->collCommonReferentielConsultationClausesN2ClausesN3s = $commonReferentielConsultationClausesN2ClausesN3s;
        $this->collCommonReferentielConsultationClausesN2ClausesN3sPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonReferentielConsultationClausesN2ClausesN3 objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonReferentielConsultationClausesN2ClausesN3 objects.
     * @throws PropelException
     */
    public function countCommonReferentielConsultationClausesN2ClausesN3s(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonReferentielConsultationClausesN2ClausesN3sPartial && !$this->isNew();
        if (null === $this->collCommonReferentielConsultationClausesN2ClausesN3s || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonReferentielConsultationClausesN2ClausesN3s) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonReferentielConsultationClausesN2ClausesN3s());
            }
            $query = CommonReferentielConsultationClausesN2ClausesN3Query::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonReferentielConsultationClausesN3($this)
                ->count($con);
        }

        return count($this->collCommonReferentielConsultationClausesN2ClausesN3s);
    }

    /**
     * Method called to associate a CommonReferentielConsultationClausesN2ClausesN3 object to this object
     * through the CommonReferentielConsultationClausesN2ClausesN3 foreign key attribute.
     *
     * @param   CommonReferentielConsultationClausesN2ClausesN3 $l CommonReferentielConsultationClausesN2ClausesN3
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     */
    public function addCommonReferentielConsultationClausesN2ClausesN3(CommonReferentielConsultationClausesN2ClausesN3 $l)
    {
        if ($this->collCommonReferentielConsultationClausesN2ClausesN3s === null) {
            $this->initCommonReferentielConsultationClausesN2ClausesN3s();
            $this->collCommonReferentielConsultationClausesN2ClausesN3sPartial = true;
        }
        if (!in_array($l, $this->collCommonReferentielConsultationClausesN2ClausesN3s->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonReferentielConsultationClausesN2ClausesN3($l);
        }

        return $this;
    }

    /**
     * @param	CommonReferentielConsultationClausesN2ClausesN3 $commonReferentielConsultationClausesN2ClausesN3 The commonReferentielConsultationClausesN2ClausesN3 object to add.
     */
    protected function doAddCommonReferentielConsultationClausesN2ClausesN3($commonReferentielConsultationClausesN2ClausesN3)
    {
        $this->collCommonReferentielConsultationClausesN2ClausesN3s[]= $commonReferentielConsultationClausesN2ClausesN3;
        $commonReferentielConsultationClausesN2ClausesN3->setCommonReferentielConsultationClausesN3($this);
    }

    /**
     * @param	CommonReferentielConsultationClausesN2ClausesN3 $commonReferentielConsultationClausesN2ClausesN3 The commonReferentielConsultationClausesN2ClausesN3 object to remove.
     * @return CommonReferentielConsultationClausesN3 The current object (for fluent API support)
     */
    public function removeCommonReferentielConsultationClausesN2ClausesN3($commonReferentielConsultationClausesN2ClausesN3)
    {
        if ($this->getCommonReferentielConsultationClausesN2ClausesN3s()->contains($commonReferentielConsultationClausesN2ClausesN3)) {
            $this->collCommonReferentielConsultationClausesN2ClausesN3s->remove($this->collCommonReferentielConsultationClausesN2ClausesN3s->search($commonReferentielConsultationClausesN2ClausesN3));
            if (null === $this->commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion) {
                $this->commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion = clone $this->collCommonReferentielConsultationClausesN2ClausesN3s;
                $this->commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion->clear();
            }
            $this->commonReferentielConsultationClausesN2ClausesN3sScheduledForDeletion[]= clone $commonReferentielConsultationClausesN2ClausesN3;
            $commonReferentielConsultationClausesN2ClausesN3->setCommonReferentielConsultationClausesN3(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonReferentielConsultationClausesN3 is new, it will return
     * an empty collection; or if this CommonReferentielConsultationClausesN3 has previously
     * been saved, it will retrieve related CommonReferentielConsultationClausesN2ClausesN3s from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonReferentielConsultationClausesN3.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonReferentielConsultationClausesN2ClausesN3[] List of CommonReferentielConsultationClausesN2ClausesN3 objects
     */
    public function getCommonReferentielConsultationClausesN2ClausesN3sJoinCommonReferentielConsultationClausesN2($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonReferentielConsultationClausesN2ClausesN3Query::create(null, $criteria);
        $query->joinWith('CommonReferentielConsultationClausesN2', $join_behavior);

        return $this->getCommonReferentielConsultationClausesN2ClausesN3s($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->label = null;
        $this->slug = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonConsultationClausesN3s) {
                foreach ($this->collCommonConsultationClausesN3s as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonReferentielConsultationClausesN2ClausesN3s) {
                foreach ($this->collCommonReferentielConsultationClausesN2ClausesN3s as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonConsultationClausesN3s instanceof PropelCollection) {
            $this->collCommonConsultationClausesN3s->clearIterator();
        }
        $this->collCommonConsultationClausesN3s = null;
        if ($this->collCommonReferentielConsultationClausesN2ClausesN3s instanceof PropelCollection) {
            $this->collCommonReferentielConsultationClausesN2ClausesN3s->clearIterator();
        }
        $this->collCommonReferentielConsultationClausesN2ClausesN3s = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonReferentielConsultationClausesN3Peer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
