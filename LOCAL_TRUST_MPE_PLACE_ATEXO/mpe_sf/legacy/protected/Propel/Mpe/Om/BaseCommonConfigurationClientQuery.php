<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConfigurationClient;
use Application\Propel\Mpe\CommonConfigurationClientPeer;
use Application\Propel\Mpe\CommonConfigurationClientQuery;

/**
 * Base class that represents a query for the 'configuration_client' table.
 *
 *
 *
 * @method CommonConfigurationClientQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonConfigurationClientQuery orderByParameter($order = Criteria::ASC) Order by the parameter column
 * @method CommonConfigurationClientQuery orderByValue($order = Criteria::ASC) Order by the value column
 * @method CommonConfigurationClientQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method CommonConfigurationClientQuery groupById() Group by the id column
 * @method CommonConfigurationClientQuery groupByParameter() Group by the parameter column
 * @method CommonConfigurationClientQuery groupByValue() Group by the value column
 * @method CommonConfigurationClientQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method CommonConfigurationClientQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConfigurationClientQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConfigurationClientQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConfigurationClient findOne(PropelPDO $con = null) Return the first CommonConfigurationClient matching the query
 * @method CommonConfigurationClient findOneOrCreate(PropelPDO $con = null) Return the first CommonConfigurationClient matching the query, or a new CommonConfigurationClient object populated from the query conditions when no match is found
 *
 * @method CommonConfigurationClient findOneByParameter(string $parameter) Return the first CommonConfigurationClient filtered by the parameter column
 * @method CommonConfigurationClient findOneByValue(string $value) Return the first CommonConfigurationClient filtered by the value column
 * @method CommonConfigurationClient findOneByUpdatedAt(string $updated_at) Return the first CommonConfigurationClient filtered by the updated_at column
 *
 * @method array findById(int $id) Return CommonConfigurationClient objects filtered by the id column
 * @method array findByParameter(string $parameter) Return CommonConfigurationClient objects filtered by the parameter column
 * @method array findByValue(string $value) Return CommonConfigurationClient objects filtered by the value column
 * @method array findByUpdatedAt(string $updated_at) Return CommonConfigurationClient objects filtered by the updated_at column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConfigurationClientQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConfigurationClientQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConfigurationClient', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConfigurationClientQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConfigurationClientQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConfigurationClientQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConfigurationClientQuery) {
            return $criteria;
        }
        $query = new CommonConfigurationClientQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConfigurationClient|CommonConfigurationClient[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConfigurationClientPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationClientPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConfigurationClient A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConfigurationClient A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `parameter`, `value`, `updated_at` FROM `configuration_client` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConfigurationClient();
            $obj->hydrate($row);
            CommonConfigurationClientPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConfigurationClient|CommonConfigurationClient[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConfigurationClient[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConfigurationClientQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConfigurationClientPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConfigurationClientQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConfigurationClientPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationClientQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonConfigurationClientPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonConfigurationClientPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConfigurationClientPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the parameter column
     *
     * Example usage:
     * <code>
     * $query->filterByParameter('fooValue');   // WHERE parameter = 'fooValue'
     * $query->filterByParameter('%fooValue%'); // WHERE parameter LIKE '%fooValue%'
     * </code>
     *
     * @param     string $parameter The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationClientQuery The current query, for fluid interface
     */
    public function filterByParameter($parameter = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($parameter)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $parameter)) {
                $parameter = str_replace('*', '%', $parameter);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationClientPeer::PARAMETER, $parameter, $comparison);
    }

    /**
     * Filter the query on the value column
     *
     * Example usage:
     * <code>
     * $query->filterByValue('fooValue');   // WHERE value = 'fooValue'
     * $query->filterByValue('%fooValue%'); // WHERE value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $value The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationClientQuery The current query, for fluid interface
     */
    public function filterByValue($value = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($value)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $value)) {
                $value = str_replace('*', '%', $value);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationClientPeer::VALUE, $value, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationClientQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CommonConfigurationClientPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CommonConfigurationClientPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConfigurationClientPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConfigurationClient $commonConfigurationClient Object to remove from the list of results
     *
     * @return CommonConfigurationClientQuery The current query, for fluid interface
     */
    public function prune($commonConfigurationClient = null)
    {
        if ($commonConfigurationClient) {
            $this->addUsingAlias(CommonConfigurationClientPeer::ID, $commonConfigurationClient->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
