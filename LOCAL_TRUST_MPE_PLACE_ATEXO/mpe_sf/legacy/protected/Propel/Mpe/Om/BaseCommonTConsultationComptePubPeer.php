<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTConsultationComptePub;
use Application\Propel\Mpe\CommonTConsultationComptePubPeer;
use Application\Propel\Mpe\CommonTDonneeComplementairePeer;
use Application\Propel\Mpe\Map\CommonTConsultationComptePubTableMap;

/**
 * Base static class for performing query and update operations on the 't_consultation_compte_pub' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTConsultationComptePubPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_consultation_compte_pub';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTConsultationComptePub';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTConsultationComptePubTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 25;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 25;

    /** the column name for the id field */
    const ID = 't_consultation_compte_pub.id';

    /** the column name for the id_donnees_complementaires field */
    const ID_DONNEES_COMPLEMENTAIRES = 't_consultation_compte_pub.id_donnees_complementaires';

    /** the column name for the organisme field */
    const ORGANISME = 't_consultation_compte_pub.organisme';

    /** the column name for the id_compte_pub field */
    const ID_COMPTE_PUB = 't_consultation_compte_pub.id_compte_pub';

    /** the column name for the boamp_login field */
    const BOAMP_LOGIN = 't_consultation_compte_pub.boamp_login';

    /** the column name for the boamp_password field */
    const BOAMP_PASSWORD = 't_consultation_compte_pub.boamp_password';

    /** the column name for the boamp_mail field */
    const BOAMP_MAIL = 't_consultation_compte_pub.boamp_mail';

    /** the column name for the boamp_target field */
    const BOAMP_TARGET = 't_consultation_compte_pub.boamp_target';

    /** the column name for the denomination field */
    const DENOMINATION = 't_consultation_compte_pub.denomination';

    /** the column name for the prm field */
    const PRM = 't_consultation_compte_pub.prm';

    /** the column name for the adresse field */
    const ADRESSE = 't_consultation_compte_pub.adresse';

    /** the column name for the cp field */
    const CP = 't_consultation_compte_pub.cp';

    /** the column name for the ville field */
    const VILLE = 't_consultation_compte_pub.ville';

    /** the column name for the url field */
    const URL = 't_consultation_compte_pub.url';

    /** the column name for the facture_denomination field */
    const FACTURE_DENOMINATION = 't_consultation_compte_pub.facture_denomination';

    /** the column name for the facture_adresse field */
    const FACTURE_ADRESSE = 't_consultation_compte_pub.facture_adresse';

    /** the column name for the facture_cp field */
    const FACTURE_CP = 't_consultation_compte_pub.facture_cp';

    /** the column name for the facture_ville field */
    const FACTURE_VILLE = 't_consultation_compte_pub.facture_ville';

    /** the column name for the instance_recours_organisme field */
    const INSTANCE_RECOURS_ORGANISME = 't_consultation_compte_pub.instance_recours_organisme';

    /** the column name for the instance_recours_adresse field */
    const INSTANCE_RECOURS_ADRESSE = 't_consultation_compte_pub.instance_recours_adresse';

    /** the column name for the instance_recours_cp field */
    const INSTANCE_RECOURS_CP = 't_consultation_compte_pub.instance_recours_cp';

    /** the column name for the instance_recours_ville field */
    const INSTANCE_RECOURS_VILLE = 't_consultation_compte_pub.instance_recours_ville';

    /** the column name for the instance_recours_url field */
    const INSTANCE_RECOURS_URL = 't_consultation_compte_pub.instance_recours_url';

    /** the column name for the telephone field */
    const TELEPHONE = 't_consultation_compte_pub.telephone';

    /** the column name for the email field */
    const EMAIL = 't_consultation_compte_pub.email';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTConsultationComptePub objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTConsultationComptePub[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTConsultationComptePubPeer::$fieldNames[CommonTConsultationComptePubPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'IdDonneesComplementaires', 'Organisme', 'IdComptePub', 'BoampLogin', 'BoampPassword', 'BoampMail', 'BoampTarget', 'Denomination', 'Prm', 'Adresse', 'Cp', 'Ville', 'Url', 'FactureDenomination', 'FactureAdresse', 'FactureCp', 'FactureVille', 'InstanceRecoursOrganisme', 'InstanceRecoursAdresse', 'InstanceRecoursCp', 'InstanceRecoursVille', 'InstanceRecoursUrl', 'Telephone', 'Email', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'idDonneesComplementaires', 'organisme', 'idComptePub', 'boampLogin', 'boampPassword', 'boampMail', 'boampTarget', 'denomination', 'prm', 'adresse', 'cp', 'ville', 'url', 'factureDenomination', 'factureAdresse', 'factureCp', 'factureVille', 'instanceRecoursOrganisme', 'instanceRecoursAdresse', 'instanceRecoursCp', 'instanceRecoursVille', 'instanceRecoursUrl', 'telephone', 'email', ),
        BasePeer::TYPE_COLNAME => array (CommonTConsultationComptePubPeer::ID, CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES, CommonTConsultationComptePubPeer::ORGANISME, CommonTConsultationComptePubPeer::ID_COMPTE_PUB, CommonTConsultationComptePubPeer::BOAMP_LOGIN, CommonTConsultationComptePubPeer::BOAMP_PASSWORD, CommonTConsultationComptePubPeer::BOAMP_MAIL, CommonTConsultationComptePubPeer::BOAMP_TARGET, CommonTConsultationComptePubPeer::DENOMINATION, CommonTConsultationComptePubPeer::PRM, CommonTConsultationComptePubPeer::ADRESSE, CommonTConsultationComptePubPeer::CP, CommonTConsultationComptePubPeer::VILLE, CommonTConsultationComptePubPeer::URL, CommonTConsultationComptePubPeer::FACTURE_DENOMINATION, CommonTConsultationComptePubPeer::FACTURE_ADRESSE, CommonTConsultationComptePubPeer::FACTURE_CP, CommonTConsultationComptePubPeer::FACTURE_VILLE, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ORGANISME, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ADRESSE, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_CP, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_VILLE, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_URL, CommonTConsultationComptePubPeer::TELEPHONE, CommonTConsultationComptePubPeer::EMAIL, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ID_DONNEES_COMPLEMENTAIRES', 'ORGANISME', 'ID_COMPTE_PUB', 'BOAMP_LOGIN', 'BOAMP_PASSWORD', 'BOAMP_MAIL', 'BOAMP_TARGET', 'DENOMINATION', 'PRM', 'ADRESSE', 'CP', 'VILLE', 'URL', 'FACTURE_DENOMINATION', 'FACTURE_ADRESSE', 'FACTURE_CP', 'FACTURE_VILLE', 'INSTANCE_RECOURS_ORGANISME', 'INSTANCE_RECOURS_ADRESSE', 'INSTANCE_RECOURS_CP', 'INSTANCE_RECOURS_VILLE', 'INSTANCE_RECOURS_URL', 'TELEPHONE', 'EMAIL', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'id_donnees_complementaires', 'organisme', 'id_compte_pub', 'boamp_login', 'boamp_password', 'boamp_mail', 'boamp_target', 'denomination', 'prm', 'adresse', 'cp', 'ville', 'url', 'facture_denomination', 'facture_adresse', 'facture_cp', 'facture_ville', 'instance_recours_organisme', 'instance_recours_adresse', 'instance_recours_cp', 'instance_recours_ville', 'instance_recours_url', 'telephone', 'email', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTConsultationComptePubPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdDonneesComplementaires' => 1, 'Organisme' => 2, 'IdComptePub' => 3, 'BoampLogin' => 4, 'BoampPassword' => 5, 'BoampMail' => 6, 'BoampTarget' => 7, 'Denomination' => 8, 'Prm' => 9, 'Adresse' => 10, 'Cp' => 11, 'Ville' => 12, 'Url' => 13, 'FactureDenomination' => 14, 'FactureAdresse' => 15, 'FactureCp' => 16, 'FactureVille' => 17, 'InstanceRecoursOrganisme' => 18, 'InstanceRecoursAdresse' => 19, 'InstanceRecoursCp' => 20, 'InstanceRecoursVille' => 21, 'InstanceRecoursUrl' => 22, 'Telephone' => 23, 'Email' => 24, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'idDonneesComplementaires' => 1, 'organisme' => 2, 'idComptePub' => 3, 'boampLogin' => 4, 'boampPassword' => 5, 'boampMail' => 6, 'boampTarget' => 7, 'denomination' => 8, 'prm' => 9, 'adresse' => 10, 'cp' => 11, 'ville' => 12, 'url' => 13, 'factureDenomination' => 14, 'factureAdresse' => 15, 'factureCp' => 16, 'factureVille' => 17, 'instanceRecoursOrganisme' => 18, 'instanceRecoursAdresse' => 19, 'instanceRecoursCp' => 20, 'instanceRecoursVille' => 21, 'instanceRecoursUrl' => 22, 'telephone' => 23, 'email' => 24, ),
        BasePeer::TYPE_COLNAME => array (CommonTConsultationComptePubPeer::ID => 0, CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES => 1, CommonTConsultationComptePubPeer::ORGANISME => 2, CommonTConsultationComptePubPeer::ID_COMPTE_PUB => 3, CommonTConsultationComptePubPeer::BOAMP_LOGIN => 4, CommonTConsultationComptePubPeer::BOAMP_PASSWORD => 5, CommonTConsultationComptePubPeer::BOAMP_MAIL => 6, CommonTConsultationComptePubPeer::BOAMP_TARGET => 7, CommonTConsultationComptePubPeer::DENOMINATION => 8, CommonTConsultationComptePubPeer::PRM => 9, CommonTConsultationComptePubPeer::ADRESSE => 10, CommonTConsultationComptePubPeer::CP => 11, CommonTConsultationComptePubPeer::VILLE => 12, CommonTConsultationComptePubPeer::URL => 13, CommonTConsultationComptePubPeer::FACTURE_DENOMINATION => 14, CommonTConsultationComptePubPeer::FACTURE_ADRESSE => 15, CommonTConsultationComptePubPeer::FACTURE_CP => 16, CommonTConsultationComptePubPeer::FACTURE_VILLE => 17, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ORGANISME => 18, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ADRESSE => 19, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_CP => 20, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_VILLE => 21, CommonTConsultationComptePubPeer::INSTANCE_RECOURS_URL => 22, CommonTConsultationComptePubPeer::TELEPHONE => 23, CommonTConsultationComptePubPeer::EMAIL => 24, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ID_DONNEES_COMPLEMENTAIRES' => 1, 'ORGANISME' => 2, 'ID_COMPTE_PUB' => 3, 'BOAMP_LOGIN' => 4, 'BOAMP_PASSWORD' => 5, 'BOAMP_MAIL' => 6, 'BOAMP_TARGET' => 7, 'DENOMINATION' => 8, 'PRM' => 9, 'ADRESSE' => 10, 'CP' => 11, 'VILLE' => 12, 'URL' => 13, 'FACTURE_DENOMINATION' => 14, 'FACTURE_ADRESSE' => 15, 'FACTURE_CP' => 16, 'FACTURE_VILLE' => 17, 'INSTANCE_RECOURS_ORGANISME' => 18, 'INSTANCE_RECOURS_ADRESSE' => 19, 'INSTANCE_RECOURS_CP' => 20, 'INSTANCE_RECOURS_VILLE' => 21, 'INSTANCE_RECOURS_URL' => 22, 'TELEPHONE' => 23, 'EMAIL' => 24, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_donnees_complementaires' => 1, 'organisme' => 2, 'id_compte_pub' => 3, 'boamp_login' => 4, 'boamp_password' => 5, 'boamp_mail' => 6, 'boamp_target' => 7, 'denomination' => 8, 'prm' => 9, 'adresse' => 10, 'cp' => 11, 'ville' => 12, 'url' => 13, 'facture_denomination' => 14, 'facture_adresse' => 15, 'facture_cp' => 16, 'facture_ville' => 17, 'instance_recours_organisme' => 18, 'instance_recours_adresse' => 19, 'instance_recours_cp' => 20, 'instance_recours_ville' => 21, 'instance_recours_url' => 22, 'telephone' => 23, 'email' => 24, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTConsultationComptePubPeer::getFieldNames($toType);
        $key = isset(CommonTConsultationComptePubPeer::$fieldKeys[$fromType][$name]) ? CommonTConsultationComptePubPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTConsultationComptePubPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTConsultationComptePubPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTConsultationComptePubPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTConsultationComptePubPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTConsultationComptePubPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::ID);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::ORGANISME);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::ID_COMPTE_PUB);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::BOAMP_LOGIN);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::BOAMP_PASSWORD);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::BOAMP_MAIL);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::BOAMP_TARGET);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::DENOMINATION);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::PRM);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::ADRESSE);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::CP);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::VILLE);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::URL);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::FACTURE_DENOMINATION);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::FACTURE_ADRESSE);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::FACTURE_CP);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::FACTURE_VILLE);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ORGANISME);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ADRESSE);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_CP);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_VILLE);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_URL);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::TELEPHONE);
            $criteria->addSelectColumn(CommonTConsultationComptePubPeer::EMAIL);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.id_donnees_complementaires');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.id_compte_pub');
            $criteria->addSelectColumn($alias . '.boamp_login');
            $criteria->addSelectColumn($alias . '.boamp_password');
            $criteria->addSelectColumn($alias . '.boamp_mail');
            $criteria->addSelectColumn($alias . '.boamp_target');
            $criteria->addSelectColumn($alias . '.denomination');
            $criteria->addSelectColumn($alias . '.prm');
            $criteria->addSelectColumn($alias . '.adresse');
            $criteria->addSelectColumn($alias . '.cp');
            $criteria->addSelectColumn($alias . '.ville');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.facture_denomination');
            $criteria->addSelectColumn($alias . '.facture_adresse');
            $criteria->addSelectColumn($alias . '.facture_cp');
            $criteria->addSelectColumn($alias . '.facture_ville');
            $criteria->addSelectColumn($alias . '.instance_recours_organisme');
            $criteria->addSelectColumn($alias . '.instance_recours_adresse');
            $criteria->addSelectColumn($alias . '.instance_recours_cp');
            $criteria->addSelectColumn($alias . '.instance_recours_ville');
            $criteria->addSelectColumn($alias . '.instance_recours_url');
            $criteria->addSelectColumn($alias . '.telephone');
            $criteria->addSelectColumn($alias . '.email');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTConsultationComptePubPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTConsultationComptePubPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTConsultationComptePubPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTConsultationComptePub
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTConsultationComptePubPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTConsultationComptePubPeer::populateObjects(CommonTConsultationComptePubPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTConsultationComptePubPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTConsultationComptePubPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTConsultationComptePub $obj A CommonTConsultationComptePub object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTConsultationComptePubPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTConsultationComptePub object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTConsultationComptePub) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTConsultationComptePub object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTConsultationComptePubPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTConsultationComptePub Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTConsultationComptePubPeer::$instances[$key])) {
                return CommonTConsultationComptePubPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTConsultationComptePubPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTConsultationComptePubPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_consultation_compte_pub
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTConsultationComptePubPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTConsultationComptePubPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTConsultationComptePubPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTConsultationComptePubPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTConsultationComptePub object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTConsultationComptePubPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTConsultationComptePubPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTConsultationComptePubPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTConsultationComptePubPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTConsultationComptePubPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTDonneeComplementaire table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTDonneeComplementaire(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTConsultationComptePubPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTConsultationComptePubPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTConsultationComptePubPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES, CommonTDonneeComplementairePeer::ID_DONNEE_COMPLEMENTAIRE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTConsultationComptePub objects pre-filled with their CommonTDonneeComplementaire objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTConsultationComptePub objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTDonneeComplementaire(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTConsultationComptePubPeer::DATABASE_NAME);
        }

        CommonTConsultationComptePubPeer::addSelectColumns($criteria);
        $startcol = CommonTConsultationComptePubPeer::NUM_HYDRATE_COLUMNS;
        CommonTDonneeComplementairePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES, CommonTDonneeComplementairePeer::ID_DONNEE_COMPLEMENTAIRE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTConsultationComptePubPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTConsultationComptePubPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTConsultationComptePubPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTConsultationComptePubPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTDonneeComplementairePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTDonneeComplementairePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTDonneeComplementairePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTDonneeComplementairePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTConsultationComptePub) to $obj2 (CommonTDonneeComplementaire)
                $obj2->addCommonTConsultationComptePub($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTConsultationComptePubPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTConsultationComptePubPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTConsultationComptePubPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES, CommonTDonneeComplementairePeer::ID_DONNEE_COMPLEMENTAIRE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTConsultationComptePub objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTConsultationComptePub objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTConsultationComptePubPeer::DATABASE_NAME);
        }

        CommonTConsultationComptePubPeer::addSelectColumns($criteria);
        $startcol2 = CommonTConsultationComptePubPeer::NUM_HYDRATE_COLUMNS;

        CommonTDonneeComplementairePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTDonneeComplementairePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES, CommonTDonneeComplementairePeer::ID_DONNEE_COMPLEMENTAIRE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTConsultationComptePubPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTConsultationComptePubPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTConsultationComptePubPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTConsultationComptePubPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonTDonneeComplementaire rows

            $key2 = CommonTDonneeComplementairePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonTDonneeComplementairePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTDonneeComplementairePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTDonneeComplementairePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTConsultationComptePub) to the collection in $obj2 (CommonTDonneeComplementaire)
                $obj2->addCommonTConsultationComptePub($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTConsultationComptePubPeer::DATABASE_NAME)->getTable(CommonTConsultationComptePubPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTConsultationComptePubPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTConsultationComptePubPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTConsultationComptePubTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTConsultationComptePubPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTConsultationComptePub or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTConsultationComptePub object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTConsultationComptePub object
        }

        if ($criteria->containsKey(CommonTConsultationComptePubPeer::ID) && $criteria->keyContainsValue(CommonTConsultationComptePubPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTConsultationComptePubPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTConsultationComptePubPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTConsultationComptePub or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTConsultationComptePub object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTConsultationComptePubPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTConsultationComptePubPeer::ID);
            $value = $criteria->remove(CommonTConsultationComptePubPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTConsultationComptePubPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTConsultationComptePubPeer::TABLE_NAME);
            }

        } else { // $values is CommonTConsultationComptePub object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTConsultationComptePubPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_consultation_compte_pub table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTConsultationComptePubPeer::TABLE_NAME, $con, CommonTConsultationComptePubPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTConsultationComptePubPeer::clearInstancePool();
            CommonTConsultationComptePubPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTConsultationComptePub or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTConsultationComptePub object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTConsultationComptePubPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTConsultationComptePub) { // it's a model object
            // invalidate the cache for this single object
            CommonTConsultationComptePubPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTConsultationComptePubPeer::DATABASE_NAME);
            $criteria->add(CommonTConsultationComptePubPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTConsultationComptePubPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTConsultationComptePubPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTConsultationComptePubPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTConsultationComptePub object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTConsultationComptePub $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTConsultationComptePubPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTConsultationComptePubPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTConsultationComptePubPeer::DATABASE_NAME, CommonTConsultationComptePubPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTConsultationComptePub
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTConsultationComptePubPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTConsultationComptePubPeer::DATABASE_NAME);
        $criteria->add(CommonTConsultationComptePubPeer::ID, $pk);

        $v = CommonTConsultationComptePubPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTConsultationComptePub[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTConsultationComptePubPeer::DATABASE_NAME);
            $criteria->add(CommonTConsultationComptePubPeer::ID, $pks, Criteria::IN);
            $objs = CommonTConsultationComptePubPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTConsultationComptePubPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTConsultationComptePubPeer::buildTableMap();

