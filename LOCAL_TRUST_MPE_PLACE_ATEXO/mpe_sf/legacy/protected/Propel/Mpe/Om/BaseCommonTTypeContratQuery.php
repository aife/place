<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratPeer;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonTechniqueAchat;

/**
 * Base class that represents a query for the 't_type_contrat' table.
 *
 *
 *
 * @method CommonTTypeContratQuery orderByIdTypeContrat($order = Criteria::ASC) Order by the id_type_contrat column
 * @method CommonTTypeContratQuery orderByLibelleTypeContrat($order = Criteria::ASC) Order by the libelle_type_contrat column
 * @method CommonTTypeContratQuery orderByAbreviationTypeContrat($order = Criteria::ASC) Order by the abreviation_type_contrat column
 * @method CommonTTypeContratQuery orderByTypeContratStatistique($order = Criteria::ASC) Order by the type_contrat_statistique column
 * @method CommonTTypeContratQuery orderByMulti($order = Criteria::ASC) Order by the multi column
 * @method CommonTTypeContratQuery orderByAccordCadreSad($order = Criteria::ASC) Order by the accord_cadre_sad column
 * @method CommonTTypeContratQuery orderByAvecChapeau($order = Criteria::ASC) Order by the avec_chapeau column
 * @method CommonTTypeContratQuery orderByAvecMontant($order = Criteria::ASC) Order by the avec_montant column
 * @method CommonTTypeContratQuery orderByModeEchangeChorus($order = Criteria::ASC) Order by the mode_echange_chorus column
 * @method CommonTTypeContratQuery orderByMarcheSubsequent($order = Criteria::ASC) Order by the marche_subsequent column
 * @method CommonTTypeContratQuery orderByAvecMontantMax($order = Criteria::ASC) Order by the avec_montant_max column
 * @method CommonTTypeContratQuery orderByOrdreAffichage($order = Criteria::ASC) Order by the ordre_affichage column
 * @method CommonTTypeContratQuery orderByArticle133($order = Criteria::ASC) Order by the article_133 column
 * @method CommonTTypeContratQuery orderByCodeDume($order = Criteria::ASC) Order by the code_dume column
 * @method CommonTTypeContratQuery orderByConcession($order = Criteria::ASC) Order by the concession column
 * @method CommonTTypeContratQuery orderByIdExterne($order = Criteria::ASC) Order by the id_externe column
 * @method CommonTTypeContratQuery orderByTechniqueAchat($order = Criteria::ASC) Order by the technique_achat column
 *
 * @method CommonTTypeContratQuery groupByIdTypeContrat() Group by the id_type_contrat column
 * @method CommonTTypeContratQuery groupByLibelleTypeContrat() Group by the libelle_type_contrat column
 * @method CommonTTypeContratQuery groupByAbreviationTypeContrat() Group by the abreviation_type_contrat column
 * @method CommonTTypeContratQuery groupByTypeContratStatistique() Group by the type_contrat_statistique column
 * @method CommonTTypeContratQuery groupByMulti() Group by the multi column
 * @method CommonTTypeContratQuery groupByAccordCadreSad() Group by the accord_cadre_sad column
 * @method CommonTTypeContratQuery groupByAvecChapeau() Group by the avec_chapeau column
 * @method CommonTTypeContratQuery groupByAvecMontant() Group by the avec_montant column
 * @method CommonTTypeContratQuery groupByModeEchangeChorus() Group by the mode_echange_chorus column
 * @method CommonTTypeContratQuery groupByMarcheSubsequent() Group by the marche_subsequent column
 * @method CommonTTypeContratQuery groupByAvecMontantMax() Group by the avec_montant_max column
 * @method CommonTTypeContratQuery groupByOrdreAffichage() Group by the ordre_affichage column
 * @method CommonTTypeContratQuery groupByArticle133() Group by the article_133 column
 * @method CommonTTypeContratQuery groupByCodeDume() Group by the code_dume column
 * @method CommonTTypeContratQuery groupByConcession() Group by the concession column
 * @method CommonTTypeContratQuery groupByIdExterne() Group by the id_externe column
 * @method CommonTTypeContratQuery groupByTechniqueAchat() Group by the technique_achat column
 *
 * @method CommonTTypeContratQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTTypeContratQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTTypeContratQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTTypeContratQuery leftJoinCommonTechniqueAchat($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTechniqueAchat relation
 * @method CommonTTypeContratQuery rightJoinCommonTechniqueAchat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTechniqueAchat relation
 * @method CommonTTypeContratQuery innerJoinCommonTechniqueAchat($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTechniqueAchat relation
 *
 * @method CommonTTypeContratQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTTypeContratQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTTypeContratQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonTTypeContratQuery leftJoinCommonTContratTitulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonTTypeContratQuery rightJoinCommonTContratTitulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonTTypeContratQuery innerJoinCommonTContratTitulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContratTitulaire relation
 *
 * @method CommonTTypeContrat findOne(PropelPDO $con = null) Return the first CommonTTypeContrat matching the query
 * @method CommonTTypeContrat findOneOrCreate(PropelPDO $con = null) Return the first CommonTTypeContrat matching the query, or a new CommonTTypeContrat object populated from the query conditions when no match is found
 *
 * @method CommonTTypeContrat findOneByLibelleTypeContrat(string $libelle_type_contrat) Return the first CommonTTypeContrat filtered by the libelle_type_contrat column
 * @method CommonTTypeContrat findOneByAbreviationTypeContrat(string $abreviation_type_contrat) Return the first CommonTTypeContrat filtered by the abreviation_type_contrat column
 * @method CommonTTypeContrat findOneByTypeContratStatistique(int $type_contrat_statistique) Return the first CommonTTypeContrat filtered by the type_contrat_statistique column
 * @method CommonTTypeContrat findOneByMulti(string $multi) Return the first CommonTTypeContrat filtered by the multi column
 * @method CommonTTypeContrat findOneByAccordCadreSad(string $accord_cadre_sad) Return the first CommonTTypeContrat filtered by the accord_cadre_sad column
 * @method CommonTTypeContrat findOneByAvecChapeau(string $avec_chapeau) Return the first CommonTTypeContrat filtered by the avec_chapeau column
 * @method CommonTTypeContrat findOneByAvecMontant(string $avec_montant) Return the first CommonTTypeContrat filtered by the avec_montant column
 * @method CommonTTypeContrat findOneByModeEchangeChorus(string $mode_echange_chorus) Return the first CommonTTypeContrat filtered by the mode_echange_chorus column
 * @method CommonTTypeContrat findOneByMarcheSubsequent(string $marche_subsequent) Return the first CommonTTypeContrat filtered by the marche_subsequent column
 * @method CommonTTypeContrat findOneByAvecMontantMax(string $avec_montant_max) Return the first CommonTTypeContrat filtered by the avec_montant_max column
 * @method CommonTTypeContrat findOneByOrdreAffichage(int $ordre_affichage) Return the first CommonTTypeContrat filtered by the ordre_affichage column
 * @method CommonTTypeContrat findOneByArticle133(string $article_133) Return the first CommonTTypeContrat filtered by the article_133 column
 * @method CommonTTypeContrat findOneByCodeDume(string $code_dume) Return the first CommonTTypeContrat filtered by the code_dume column
 * @method CommonTTypeContrat findOneByConcession(boolean $concession) Return the first CommonTTypeContrat filtered by the concession column
 * @method CommonTTypeContrat findOneByIdExterne(string $id_externe) Return the first CommonTTypeContrat filtered by the id_externe column
 * @method CommonTTypeContrat findOneByTechniqueAchat(int $technique_achat) Return the first CommonTTypeContrat filtered by the technique_achat column
 *
 * @method array findByIdTypeContrat(int $id_type_contrat) Return CommonTTypeContrat objects filtered by the id_type_contrat column
 * @method array findByLibelleTypeContrat(string $libelle_type_contrat) Return CommonTTypeContrat objects filtered by the libelle_type_contrat column
 * @method array findByAbreviationTypeContrat(string $abreviation_type_contrat) Return CommonTTypeContrat objects filtered by the abreviation_type_contrat column
 * @method array findByTypeContratStatistique(int $type_contrat_statistique) Return CommonTTypeContrat objects filtered by the type_contrat_statistique column
 * @method array findByMulti(string $multi) Return CommonTTypeContrat objects filtered by the multi column
 * @method array findByAccordCadreSad(string $accord_cadre_sad) Return CommonTTypeContrat objects filtered by the accord_cadre_sad column
 * @method array findByAvecChapeau(string $avec_chapeau) Return CommonTTypeContrat objects filtered by the avec_chapeau column
 * @method array findByAvecMontant(string $avec_montant) Return CommonTTypeContrat objects filtered by the avec_montant column
 * @method array findByModeEchangeChorus(string $mode_echange_chorus) Return CommonTTypeContrat objects filtered by the mode_echange_chorus column
 * @method array findByMarcheSubsequent(string $marche_subsequent) Return CommonTTypeContrat objects filtered by the marche_subsequent column
 * @method array findByAvecMontantMax(string $avec_montant_max) Return CommonTTypeContrat objects filtered by the avec_montant_max column
 * @method array findByOrdreAffichage(int $ordre_affichage) Return CommonTTypeContrat objects filtered by the ordre_affichage column
 * @method array findByArticle133(string $article_133) Return CommonTTypeContrat objects filtered by the article_133 column
 * @method array findByCodeDume(string $code_dume) Return CommonTTypeContrat objects filtered by the code_dume column
 * @method array findByConcession(boolean $concession) Return CommonTTypeContrat objects filtered by the concession column
 * @method array findByIdExterne(string $id_externe) Return CommonTTypeContrat objects filtered by the id_externe column
 * @method array findByTechniqueAchat(int $technique_achat) Return CommonTTypeContrat objects filtered by the technique_achat column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTypeContratQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTTypeContratQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTTypeContrat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTTypeContratQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTTypeContratQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTTypeContratQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTTypeContratQuery) {
            return $criteria;
        }
        $query = new CommonTTypeContratQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTTypeContrat|CommonTTypeContrat[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTTypeContratPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeContrat A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdTypeContrat($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeContrat A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_type_contrat`, `libelle_type_contrat`, `abreviation_type_contrat`, `type_contrat_statistique`, `multi`, `accord_cadre_sad`, `avec_chapeau`, `avec_montant`, `mode_echange_chorus`, `marche_subsequent`, `avec_montant_max`, `ordre_affichage`, `article_133`, `code_dume`, `concession`, `id_externe`, `technique_achat` FROM `t_type_contrat` WHERE `id_type_contrat` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTTypeContrat();
            $obj->hydrate($row);
            CommonTTypeContratPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTTypeContrat|CommonTTypeContrat[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTTypeContrat[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_type_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeContrat(1234); // WHERE id_type_contrat = 1234
     * $query->filterByIdTypeContrat(array(12, 34)); // WHERE id_type_contrat IN (12, 34)
     * $query->filterByIdTypeContrat(array('min' => 12)); // WHERE id_type_contrat >= 12
     * $query->filterByIdTypeContrat(array('max' => 12)); // WHERE id_type_contrat <= 12
     * </code>
     *
     * @param     mixed $idTypeContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByIdTypeContrat($idTypeContrat = null, $comparison = null)
    {
        if (is_array($idTypeContrat)) {
            $useMinMax = false;
            if (isset($idTypeContrat['min'])) {
                $this->addUsingAlias(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $idTypeContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeContrat['max'])) {
                $this->addUsingAlias(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $idTypeContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $idTypeContrat, $comparison);
    }

    /**
     * Filter the query on the libelle_type_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleTypeContrat('fooValue');   // WHERE libelle_type_contrat = 'fooValue'
     * $query->filterByLibelleTypeContrat('%fooValue%'); // WHERE libelle_type_contrat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleTypeContrat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByLibelleTypeContrat($libelleTypeContrat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleTypeContrat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleTypeContrat)) {
                $libelleTypeContrat = str_replace('*', '%', $libelleTypeContrat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::LIBELLE_TYPE_CONTRAT, $libelleTypeContrat, $comparison);
    }

    /**
     * Filter the query on the abreviation_type_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByAbreviationTypeContrat('fooValue');   // WHERE abreviation_type_contrat = 'fooValue'
     * $query->filterByAbreviationTypeContrat('%fooValue%'); // WHERE abreviation_type_contrat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $abreviationTypeContrat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByAbreviationTypeContrat($abreviationTypeContrat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($abreviationTypeContrat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $abreviationTypeContrat)) {
                $abreviationTypeContrat = str_replace('*', '%', $abreviationTypeContrat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::ABREVIATION_TYPE_CONTRAT, $abreviationTypeContrat, $comparison);
    }

    /**
     * Filter the query on the type_contrat_statistique column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeContratStatistique(1234); // WHERE type_contrat_statistique = 1234
     * $query->filterByTypeContratStatistique(array(12, 34)); // WHERE type_contrat_statistique IN (12, 34)
     * $query->filterByTypeContratStatistique(array('min' => 12)); // WHERE type_contrat_statistique >= 12
     * $query->filterByTypeContratStatistique(array('max' => 12)); // WHERE type_contrat_statistique <= 12
     * </code>
     *
     * @param     mixed $typeContratStatistique The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByTypeContratStatistique($typeContratStatistique = null, $comparison = null)
    {
        if (is_array($typeContratStatistique)) {
            $useMinMax = false;
            if (isset($typeContratStatistique['min'])) {
                $this->addUsingAlias(CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE, $typeContratStatistique['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeContratStatistique['max'])) {
                $this->addUsingAlias(CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE, $typeContratStatistique['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE, $typeContratStatistique, $comparison);
    }

    /**
     * Filter the query on the multi column
     *
     * Example usage:
     * <code>
     * $query->filterByMulti('fooValue');   // WHERE multi = 'fooValue'
     * $query->filterByMulti('%fooValue%'); // WHERE multi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $multi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByMulti($multi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($multi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $multi)) {
                $multi = str_replace('*', '%', $multi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::MULTI, $multi, $comparison);
    }

    /**
     * Filter the query on the accord_cadre_sad column
     *
     * Example usage:
     * <code>
     * $query->filterByAccordCadreSad('fooValue');   // WHERE accord_cadre_sad = 'fooValue'
     * $query->filterByAccordCadreSad('%fooValue%'); // WHERE accord_cadre_sad LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accordCadreSad The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByAccordCadreSad($accordCadreSad = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accordCadreSad)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $accordCadreSad)) {
                $accordCadreSad = str_replace('*', '%', $accordCadreSad);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::ACCORD_CADRE_SAD, $accordCadreSad, $comparison);
    }

    /**
     * Filter the query on the avec_chapeau column
     *
     * Example usage:
     * <code>
     * $query->filterByAvecChapeau('fooValue');   // WHERE avec_chapeau = 'fooValue'
     * $query->filterByAvecChapeau('%fooValue%'); // WHERE avec_chapeau LIKE '%fooValue%'
     * </code>
     *
     * @param     string $avecChapeau The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByAvecChapeau($avecChapeau = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($avecChapeau)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $avecChapeau)) {
                $avecChapeau = str_replace('*', '%', $avecChapeau);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::AVEC_CHAPEAU, $avecChapeau, $comparison);
    }

    /**
     * Filter the query on the avec_montant column
     *
     * Example usage:
     * <code>
     * $query->filterByAvecMontant('fooValue');   // WHERE avec_montant = 'fooValue'
     * $query->filterByAvecMontant('%fooValue%'); // WHERE avec_montant LIKE '%fooValue%'
     * </code>
     *
     * @param     string $avecMontant The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByAvecMontant($avecMontant = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($avecMontant)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $avecMontant)) {
                $avecMontant = str_replace('*', '%', $avecMontant);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::AVEC_MONTANT, $avecMontant, $comparison);
    }

    /**
     * Filter the query on the mode_echange_chorus column
     *
     * Example usage:
     * <code>
     * $query->filterByModeEchangeChorus('fooValue');   // WHERE mode_echange_chorus = 'fooValue'
     * $query->filterByModeEchangeChorus('%fooValue%'); // WHERE mode_echange_chorus LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modeEchangeChorus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByModeEchangeChorus($modeEchangeChorus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modeEchangeChorus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modeEchangeChorus)) {
                $modeEchangeChorus = str_replace('*', '%', $modeEchangeChorus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::MODE_ECHANGE_CHORUS, $modeEchangeChorus, $comparison);
    }

    /**
     * Filter the query on the marche_subsequent column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcheSubsequent('fooValue');   // WHERE marche_subsequent = 'fooValue'
     * $query->filterByMarcheSubsequent('%fooValue%'); // WHERE marche_subsequent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $marcheSubsequent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByMarcheSubsequent($marcheSubsequent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($marcheSubsequent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $marcheSubsequent)) {
                $marcheSubsequent = str_replace('*', '%', $marcheSubsequent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::MARCHE_SUBSEQUENT, $marcheSubsequent, $comparison);
    }

    /**
     * Filter the query on the avec_montant_max column
     *
     * Example usage:
     * <code>
     * $query->filterByAvecMontantMax('fooValue');   // WHERE avec_montant_max = 'fooValue'
     * $query->filterByAvecMontantMax('%fooValue%'); // WHERE avec_montant_max LIKE '%fooValue%'
     * </code>
     *
     * @param     string $avecMontantMax The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByAvecMontantMax($avecMontantMax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($avecMontantMax)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $avecMontantMax)) {
                $avecMontantMax = str_replace('*', '%', $avecMontantMax);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::AVEC_MONTANT_MAX, $avecMontantMax, $comparison);
    }

    /**
     * Filter the query on the ordre_affichage column
     *
     * Example usage:
     * <code>
     * $query->filterByOrdreAffichage(1234); // WHERE ordre_affichage = 1234
     * $query->filterByOrdreAffichage(array(12, 34)); // WHERE ordre_affichage IN (12, 34)
     * $query->filterByOrdreAffichage(array('min' => 12)); // WHERE ordre_affichage >= 12
     * $query->filterByOrdreAffichage(array('max' => 12)); // WHERE ordre_affichage <= 12
     * </code>
     *
     * @param     mixed $ordreAffichage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByOrdreAffichage($ordreAffichage = null, $comparison = null)
    {
        if (is_array($ordreAffichage)) {
            $useMinMax = false;
            if (isset($ordreAffichage['min'])) {
                $this->addUsingAlias(CommonTTypeContratPeer::ORDRE_AFFICHAGE, $ordreAffichage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ordreAffichage['max'])) {
                $this->addUsingAlias(CommonTTypeContratPeer::ORDRE_AFFICHAGE, $ordreAffichage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::ORDRE_AFFICHAGE, $ordreAffichage, $comparison);
    }

    /**
     * Filter the query on the article_133 column
     *
     * Example usage:
     * <code>
     * $query->filterByArticle133('fooValue');   // WHERE article_133 = 'fooValue'
     * $query->filterByArticle133('%fooValue%'); // WHERE article_133 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $article133 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByArticle133($article133 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($article133)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $article133)) {
                $article133 = str_replace('*', '%', $article133);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::ARTICLE_133, $article133, $comparison);
    }

    /**
     * Filter the query on the code_dume column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeDume('fooValue');   // WHERE code_dume = 'fooValue'
     * $query->filterByCodeDume('%fooValue%'); // WHERE code_dume LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeDume The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByCodeDume($codeDume = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeDume)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeDume)) {
                $codeDume = str_replace('*', '%', $codeDume);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::CODE_DUME, $codeDume, $comparison);
    }

    /**
     * Filter the query on the concession column
     *
     * Example usage:
     * <code>
     * $query->filterByConcession(true); // WHERE concession = true
     * $query->filterByConcession('yes'); // WHERE concession = true
     * </code>
     *
     * @param     boolean|string $concession The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByConcession($concession = null, $comparison = null)
    {
        if (is_string($concession)) {
            $concession = in_array(strtolower($concession), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::CONCESSION, $concession, $comparison);
    }

    /**
     * Filter the query on the id_externe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdExterne('fooValue');   // WHERE id_externe = 'fooValue'
     * $query->filterByIdExterne('%fooValue%'); // WHERE id_externe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idExterne The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByIdExterne($idExterne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idExterne)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idExterne)) {
                $idExterne = str_replace('*', '%', $idExterne);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::ID_EXTERNE, $idExterne, $comparison);
    }

    /**
     * Filter the query on the technique_achat column
     *
     * Example usage:
     * <code>
     * $query->filterByTechniqueAchat(1234); // WHERE technique_achat = 1234
     * $query->filterByTechniqueAchat(array(12, 34)); // WHERE technique_achat IN (12, 34)
     * $query->filterByTechniqueAchat(array('min' => 12)); // WHERE technique_achat >= 12
     * $query->filterByTechniqueAchat(array('max' => 12)); // WHERE technique_achat <= 12
     * </code>
     *
     * @see       filterByCommonTechniqueAchat()
     *
     * @param     mixed $techniqueAchat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function filterByTechniqueAchat($techniqueAchat = null, $comparison = null)
    {
        if (is_array($techniqueAchat)) {
            $useMinMax = false;
            if (isset($techniqueAchat['min'])) {
                $this->addUsingAlias(CommonTTypeContratPeer::TECHNIQUE_ACHAT, $techniqueAchat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($techniqueAchat['max'])) {
                $this->addUsingAlias(CommonTTypeContratPeer::TECHNIQUE_ACHAT, $techniqueAchat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeContratPeer::TECHNIQUE_ACHAT, $techniqueAchat, $comparison);
    }

    /**
     * Filter the query by a related CommonTechniqueAchat object
     *
     * @param   CommonTechniqueAchat|PropelObjectCollection $commonTechniqueAchat The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTTypeContratQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTechniqueAchat($commonTechniqueAchat, $comparison = null)
    {
        if ($commonTechniqueAchat instanceof CommonTechniqueAchat) {
            return $this
                ->addUsingAlias(CommonTTypeContratPeer::TECHNIQUE_ACHAT, $commonTechniqueAchat->getIdTechniqueAchat(), $comparison);
        } elseif ($commonTechniqueAchat instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTTypeContratPeer::TECHNIQUE_ACHAT, $commonTechniqueAchat->toKeyValue('PrimaryKey', 'IdTechniqueAchat'), $comparison);
        } else {
            throw new PropelException('filterByCommonTechniqueAchat() only accepts arguments of type CommonTechniqueAchat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTechniqueAchat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function joinCommonTechniqueAchat($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTechniqueAchat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTechniqueAchat');
        }

        return $this;
    }

    /**
     * Use the CommonTechniqueAchat relation CommonTechniqueAchat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTechniqueAchatQuery A secondary query class using the current class as primary query
     */
    public function useCommonTechniqueAchatQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTechniqueAchat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTechniqueAchat', '\Application\Propel\Mpe\CommonTechniqueAchatQuery');
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTTypeContratQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $commonConsultation->getTypeMarche(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationQuery()
                ->filterByPrimaryKeys($commonConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Filter the query by a related CommonTContratTitulaire object
     *
     * @param   CommonTContratTitulaire|PropelObjectCollection $commonTContratTitulaire  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTTypeContratQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContratTitulaire($commonTContratTitulaire, $comparison = null)
    {
        if ($commonTContratTitulaire instanceof CommonTContratTitulaire) {
            return $this
                ->addUsingAlias(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $commonTContratTitulaire->getIdTypeContrat(), $comparison);
        } elseif ($commonTContratTitulaire instanceof PropelObjectCollection) {
            return $this
                ->useCommonTContratTitulaireQuery()
                ->filterByPrimaryKeys($commonTContratTitulaire->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTContratTitulaire() only accepts arguments of type CommonTContratTitulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContratTitulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function joinCommonTContratTitulaire($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContratTitulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContratTitulaire');
        }

        return $this;
    }

    /**
     * Use the CommonTContratTitulaire relation CommonTContratTitulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContratTitulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContratTitulaireQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContratTitulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContratTitulaire', '\Application\Propel\Mpe\CommonTContratTitulaireQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTTypeContrat $commonTTypeContrat Object to remove from the list of results
     *
     * @return CommonTTypeContratQuery The current query, for fluid interface
     */
    public function prune($commonTTypeContrat = null)
    {
        if ($commonTTypeContrat) {
            $this->addUsingAlias(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $commonTTypeContrat->getIdTypeContrat(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
