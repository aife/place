<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratPeer;
use Application\Propel\Mpe\CommonTechniqueAchatPeer;
use Application\Propel\Mpe\Map\CommonTTypeContratTableMap;

/**
 * Base static class for performing query and update operations on the 't_type_contrat' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTypeContratPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_type_contrat';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTTypeContrat';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTTypeContratTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 17;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 17;

    /** the column name for the id_type_contrat field */
    const ID_TYPE_CONTRAT = 't_type_contrat.id_type_contrat';

    /** the column name for the libelle_type_contrat field */
    const LIBELLE_TYPE_CONTRAT = 't_type_contrat.libelle_type_contrat';

    /** the column name for the abreviation_type_contrat field */
    const ABREVIATION_TYPE_CONTRAT = 't_type_contrat.abreviation_type_contrat';

    /** the column name for the type_contrat_statistique field */
    const TYPE_CONTRAT_STATISTIQUE = 't_type_contrat.type_contrat_statistique';

    /** the column name for the multi field */
    const MULTI = 't_type_contrat.multi';

    /** the column name for the accord_cadre_sad field */
    const ACCORD_CADRE_SAD = 't_type_contrat.accord_cadre_sad';

    /** the column name for the avec_chapeau field */
    const AVEC_CHAPEAU = 't_type_contrat.avec_chapeau';

    /** the column name for the avec_montant field */
    const AVEC_MONTANT = 't_type_contrat.avec_montant';

    /** the column name for the mode_echange_chorus field */
    const MODE_ECHANGE_CHORUS = 't_type_contrat.mode_echange_chorus';

    /** the column name for the marche_subsequent field */
    const MARCHE_SUBSEQUENT = 't_type_contrat.marche_subsequent';

    /** the column name for the avec_montant_max field */
    const AVEC_MONTANT_MAX = 't_type_contrat.avec_montant_max';

    /** the column name for the ordre_affichage field */
    const ORDRE_AFFICHAGE = 't_type_contrat.ordre_affichage';

    /** the column name for the article_133 field */
    const ARTICLE_133 = 't_type_contrat.article_133';

    /** the column name for the code_dume field */
    const CODE_DUME = 't_type_contrat.code_dume';

    /** the column name for the concession field */
    const CONCESSION = 't_type_contrat.concession';

    /** the column name for the id_externe field */
    const ID_EXTERNE = 't_type_contrat.id_externe';

    /** the column name for the technique_achat field */
    const TECHNIQUE_ACHAT = 't_type_contrat.technique_achat';

    /** The enumerated values for the multi field */
    const MULTI_0 = '0';
    const MULTI_1 = '1';

    /** The enumerated values for the accord_cadre_sad field */
    const ACCORD_CADRE_SAD_0 = '0';
    const ACCORD_CADRE_SAD_1 = '1';
    const ACCORD_CADRE_SAD_2 = '2';

    /** The enumerated values for the avec_chapeau field */
    const AVEC_CHAPEAU_0 = '0';
    const AVEC_CHAPEAU_1 = '1';

    /** The enumerated values for the avec_montant field */
    const AVEC_MONTANT_0 = '0';
    const AVEC_MONTANT_1 = '1';

    /** The enumerated values for the avec_montant_max field */
    const AVEC_MONTANT_MAX_0 = '0';
    const AVEC_MONTANT_MAX_1 = '1';

    /** The enumerated values for the article_133 field */
    const ARTICLE_133_0 = '0';
    const ARTICLE_133_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTTypeContrat objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTTypeContrat[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTTypeContratPeer::$fieldNames[CommonTTypeContratPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdTypeContrat', 'LibelleTypeContrat', 'AbreviationTypeContrat', 'TypeContratStatistique', 'Multi', 'AccordCadreSad', 'AvecChapeau', 'AvecMontant', 'ModeEchangeChorus', 'MarcheSubsequent', 'AvecMontantMax', 'OrdreAffichage', 'Article133', 'CodeDume', 'Concession', 'IdExterne', 'TechniqueAchat', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idTypeContrat', 'libelleTypeContrat', 'abreviationTypeContrat', 'typeContratStatistique', 'multi', 'accordCadreSad', 'avecChapeau', 'avecMontant', 'modeEchangeChorus', 'marcheSubsequent', 'avecMontantMax', 'ordreAffichage', 'article133', 'codeDume', 'concession', 'idExterne', 'techniqueAchat', ),
        BasePeer::TYPE_COLNAME => array (CommonTTypeContratPeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::LIBELLE_TYPE_CONTRAT, CommonTTypeContratPeer::ABREVIATION_TYPE_CONTRAT, CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE, CommonTTypeContratPeer::MULTI, CommonTTypeContratPeer::ACCORD_CADRE_SAD, CommonTTypeContratPeer::AVEC_CHAPEAU, CommonTTypeContratPeer::AVEC_MONTANT, CommonTTypeContratPeer::MODE_ECHANGE_CHORUS, CommonTTypeContratPeer::MARCHE_SUBSEQUENT, CommonTTypeContratPeer::AVEC_MONTANT_MAX, CommonTTypeContratPeer::ORDRE_AFFICHAGE, CommonTTypeContratPeer::ARTICLE_133, CommonTTypeContratPeer::CODE_DUME, CommonTTypeContratPeer::CONCESSION, CommonTTypeContratPeer::ID_EXTERNE, CommonTTypeContratPeer::TECHNIQUE_ACHAT, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_TYPE_CONTRAT', 'LIBELLE_TYPE_CONTRAT', 'ABREVIATION_TYPE_CONTRAT', 'TYPE_CONTRAT_STATISTIQUE', 'MULTI', 'ACCORD_CADRE_SAD', 'AVEC_CHAPEAU', 'AVEC_MONTANT', 'MODE_ECHANGE_CHORUS', 'MARCHE_SUBSEQUENT', 'AVEC_MONTANT_MAX', 'ORDRE_AFFICHAGE', 'ARTICLE_133', 'CODE_DUME', 'CONCESSION', 'ID_EXTERNE', 'TECHNIQUE_ACHAT', ),
        BasePeer::TYPE_FIELDNAME => array ('id_type_contrat', 'libelle_type_contrat', 'abreviation_type_contrat', 'type_contrat_statistique', 'multi', 'accord_cadre_sad', 'avec_chapeau', 'avec_montant', 'mode_echange_chorus', 'marche_subsequent', 'avec_montant_max', 'ordre_affichage', 'article_133', 'code_dume', 'concession', 'id_externe', 'technique_achat', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTTypeContratPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdTypeContrat' => 0, 'LibelleTypeContrat' => 1, 'AbreviationTypeContrat' => 2, 'TypeContratStatistique' => 3, 'Multi' => 4, 'AccordCadreSad' => 5, 'AvecChapeau' => 6, 'AvecMontant' => 7, 'ModeEchangeChorus' => 8, 'MarcheSubsequent' => 9, 'AvecMontantMax' => 10, 'OrdreAffichage' => 11, 'Article133' => 12, 'CodeDume' => 13, 'Concession' => 14, 'IdExterne' => 15, 'TechniqueAchat' => 16, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idTypeContrat' => 0, 'libelleTypeContrat' => 1, 'abreviationTypeContrat' => 2, 'typeContratStatistique' => 3, 'multi' => 4, 'accordCadreSad' => 5, 'avecChapeau' => 6, 'avecMontant' => 7, 'modeEchangeChorus' => 8, 'marcheSubsequent' => 9, 'avecMontantMax' => 10, 'ordreAffichage' => 11, 'article133' => 12, 'codeDume' => 13, 'concession' => 14, 'idExterne' => 15, 'techniqueAchat' => 16, ),
        BasePeer::TYPE_COLNAME => array (CommonTTypeContratPeer::ID_TYPE_CONTRAT => 0, CommonTTypeContratPeer::LIBELLE_TYPE_CONTRAT => 1, CommonTTypeContratPeer::ABREVIATION_TYPE_CONTRAT => 2, CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE => 3, CommonTTypeContratPeer::MULTI => 4, CommonTTypeContratPeer::ACCORD_CADRE_SAD => 5, CommonTTypeContratPeer::AVEC_CHAPEAU => 6, CommonTTypeContratPeer::AVEC_MONTANT => 7, CommonTTypeContratPeer::MODE_ECHANGE_CHORUS => 8, CommonTTypeContratPeer::MARCHE_SUBSEQUENT => 9, CommonTTypeContratPeer::AVEC_MONTANT_MAX => 10, CommonTTypeContratPeer::ORDRE_AFFICHAGE => 11, CommonTTypeContratPeer::ARTICLE_133 => 12, CommonTTypeContratPeer::CODE_DUME => 13, CommonTTypeContratPeer::CONCESSION => 14, CommonTTypeContratPeer::ID_EXTERNE => 15, CommonTTypeContratPeer::TECHNIQUE_ACHAT => 16, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_TYPE_CONTRAT' => 0, 'LIBELLE_TYPE_CONTRAT' => 1, 'ABREVIATION_TYPE_CONTRAT' => 2, 'TYPE_CONTRAT_STATISTIQUE' => 3, 'MULTI' => 4, 'ACCORD_CADRE_SAD' => 5, 'AVEC_CHAPEAU' => 6, 'AVEC_MONTANT' => 7, 'MODE_ECHANGE_CHORUS' => 8, 'MARCHE_SUBSEQUENT' => 9, 'AVEC_MONTANT_MAX' => 10, 'ORDRE_AFFICHAGE' => 11, 'ARTICLE_133' => 12, 'CODE_DUME' => 13, 'CONCESSION' => 14, 'ID_EXTERNE' => 15, 'TECHNIQUE_ACHAT' => 16, ),
        BasePeer::TYPE_FIELDNAME => array ('id_type_contrat' => 0, 'libelle_type_contrat' => 1, 'abreviation_type_contrat' => 2, 'type_contrat_statistique' => 3, 'multi' => 4, 'accord_cadre_sad' => 5, 'avec_chapeau' => 6, 'avec_montant' => 7, 'mode_echange_chorus' => 8, 'marche_subsequent' => 9, 'avec_montant_max' => 10, 'ordre_affichage' => 11, 'article_133' => 12, 'code_dume' => 13, 'concession' => 14, 'id_externe' => 15, 'technique_achat' => 16, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTTypeContratPeer::MULTI => array(
            CommonTTypeContratPeer::MULTI_0,
            CommonTTypeContratPeer::MULTI_1,
        ),
        CommonTTypeContratPeer::ACCORD_CADRE_SAD => array(
            CommonTTypeContratPeer::ACCORD_CADRE_SAD_0,
            CommonTTypeContratPeer::ACCORD_CADRE_SAD_1,
            CommonTTypeContratPeer::ACCORD_CADRE_SAD_2,
        ),
        CommonTTypeContratPeer::AVEC_CHAPEAU => array(
            CommonTTypeContratPeer::AVEC_CHAPEAU_0,
            CommonTTypeContratPeer::AVEC_CHAPEAU_1,
        ),
        CommonTTypeContratPeer::AVEC_MONTANT => array(
            CommonTTypeContratPeer::AVEC_MONTANT_0,
            CommonTTypeContratPeer::AVEC_MONTANT_1,
        ),
        CommonTTypeContratPeer::AVEC_MONTANT_MAX => array(
            CommonTTypeContratPeer::AVEC_MONTANT_MAX_0,
            CommonTTypeContratPeer::AVEC_MONTANT_MAX_1,
        ),
        CommonTTypeContratPeer::ARTICLE_133 => array(
            CommonTTypeContratPeer::ARTICLE_133_0,
            CommonTTypeContratPeer::ARTICLE_133_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTTypeContratPeer::getFieldNames($toType);
        $key = isset(CommonTTypeContratPeer::$fieldKeys[$fromType][$name]) ? CommonTTypeContratPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTTypeContratPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTTypeContratPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTTypeContratPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTTypeContratPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTTypeContratPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTTypeContratPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTTypeContratPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTTypeContratPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTTypeContratPeer::ID_TYPE_CONTRAT);
            $criteria->addSelectColumn(CommonTTypeContratPeer::LIBELLE_TYPE_CONTRAT);
            $criteria->addSelectColumn(CommonTTypeContratPeer::ABREVIATION_TYPE_CONTRAT);
            $criteria->addSelectColumn(CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE);
            $criteria->addSelectColumn(CommonTTypeContratPeer::MULTI);
            $criteria->addSelectColumn(CommonTTypeContratPeer::ACCORD_CADRE_SAD);
            $criteria->addSelectColumn(CommonTTypeContratPeer::AVEC_CHAPEAU);
            $criteria->addSelectColumn(CommonTTypeContratPeer::AVEC_MONTANT);
            $criteria->addSelectColumn(CommonTTypeContratPeer::MODE_ECHANGE_CHORUS);
            $criteria->addSelectColumn(CommonTTypeContratPeer::MARCHE_SUBSEQUENT);
            $criteria->addSelectColumn(CommonTTypeContratPeer::AVEC_MONTANT_MAX);
            $criteria->addSelectColumn(CommonTTypeContratPeer::ORDRE_AFFICHAGE);
            $criteria->addSelectColumn(CommonTTypeContratPeer::ARTICLE_133);
            $criteria->addSelectColumn(CommonTTypeContratPeer::CODE_DUME);
            $criteria->addSelectColumn(CommonTTypeContratPeer::CONCESSION);
            $criteria->addSelectColumn(CommonTTypeContratPeer::ID_EXTERNE);
            $criteria->addSelectColumn(CommonTTypeContratPeer::TECHNIQUE_ACHAT);
        } else {
            $criteria->addSelectColumn($alias . '.id_type_contrat');
            $criteria->addSelectColumn($alias . '.libelle_type_contrat');
            $criteria->addSelectColumn($alias . '.abreviation_type_contrat');
            $criteria->addSelectColumn($alias . '.type_contrat_statistique');
            $criteria->addSelectColumn($alias . '.multi');
            $criteria->addSelectColumn($alias . '.accord_cadre_sad');
            $criteria->addSelectColumn($alias . '.avec_chapeau');
            $criteria->addSelectColumn($alias . '.avec_montant');
            $criteria->addSelectColumn($alias . '.mode_echange_chorus');
            $criteria->addSelectColumn($alias . '.marche_subsequent');
            $criteria->addSelectColumn($alias . '.avec_montant_max');
            $criteria->addSelectColumn($alias . '.ordre_affichage');
            $criteria->addSelectColumn($alias . '.article_133');
            $criteria->addSelectColumn($alias . '.code_dume');
            $criteria->addSelectColumn($alias . '.concession');
            $criteria->addSelectColumn($alias . '.id_externe');
            $criteria->addSelectColumn($alias . '.technique_achat');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTTypeContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTTypeContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTTypeContratPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTTypeContrat
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTTypeContratPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTTypeContratPeer::populateObjects(CommonTTypeContratPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTTypeContratPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTTypeContratPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTTypeContrat $obj A CommonTTypeContrat object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getIdTypeContrat();
            } // if key === null
            CommonTTypeContratPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTTypeContrat object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTTypeContrat) {
                $key = (string) $value->getIdTypeContrat();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTTypeContrat object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTTypeContratPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTTypeContrat Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTTypeContratPeer::$instances[$key])) {
                return CommonTTypeContratPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTTypeContratPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTTypeContratPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_type_contrat
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTTypeContratPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTTypeContratPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTTypeContratPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTTypeContrat object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTTypeContratPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTTypeContratPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTTypeContratPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTechniqueAchat table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTechniqueAchat(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTTypeContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTTypeContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTTypeContratPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTTypeContratPeer::TECHNIQUE_ACHAT, CommonTechniqueAchatPeer::ID_TECHNIQUE_ACHAT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTTypeContrat objects pre-filled with their CommonTechniqueAchat objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTTypeContrat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTechniqueAchat(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTTypeContratPeer::DATABASE_NAME);
        }

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol = CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;
        CommonTechniqueAchatPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTTypeContratPeer::TECHNIQUE_ACHAT, CommonTechniqueAchatPeer::ID_TECHNIQUE_ACHAT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTTypeContratPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTTypeContratPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTTypeContratPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTechniqueAchatPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTechniqueAchatPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTechniqueAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTechniqueAchatPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTTypeContrat) to $obj2 (CommonTechniqueAchat)
                $obj2->addCommonTTypeContrat($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTTypeContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTTypeContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTTypeContratPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTTypeContratPeer::TECHNIQUE_ACHAT, CommonTechniqueAchatPeer::ID_TECHNIQUE_ACHAT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTTypeContrat objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTTypeContrat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTTypeContratPeer::DATABASE_NAME);
        }

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol2 = CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTechniqueAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTechniqueAchatPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTTypeContratPeer::TECHNIQUE_ACHAT, CommonTechniqueAchatPeer::ID_TECHNIQUE_ACHAT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTTypeContratPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTTypeContratPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTTypeContratPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonTechniqueAchat rows

            $key2 = CommonTechniqueAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonTechniqueAchatPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTechniqueAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTechniqueAchatPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTTypeContrat) to the collection in $obj2 (CommonTechniqueAchat)
                $obj2->addCommonTTypeContrat($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTTypeContratPeer::DATABASE_NAME)->getTable(CommonTTypeContratPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTTypeContratPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTTypeContratPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTTypeContratTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTTypeContratPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTTypeContrat or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTTypeContrat object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTTypeContrat object
        }

        if ($criteria->containsKey(CommonTTypeContratPeer::ID_TYPE_CONTRAT) && $criteria->keyContainsValue(CommonTTypeContratPeer::ID_TYPE_CONTRAT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTTypeContratPeer::ID_TYPE_CONTRAT.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTTypeContratPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTTypeContrat or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTTypeContrat object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTTypeContratPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTTypeContratPeer::ID_TYPE_CONTRAT);
            $value = $criteria->remove(CommonTTypeContratPeer::ID_TYPE_CONTRAT);
            if ($value) {
                $selectCriteria->add(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTTypeContratPeer::TABLE_NAME);
            }

        } else { // $values is CommonTTypeContrat object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTTypeContratPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_type_contrat table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTTypeContratPeer::TABLE_NAME, $con, CommonTTypeContratPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTTypeContratPeer::clearInstancePool();
            CommonTTypeContratPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTTypeContrat or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTTypeContrat object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTTypeContratPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTTypeContrat) { // it's a model object
            // invalidate the cache for this single object
            CommonTTypeContratPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTTypeContratPeer::DATABASE_NAME);
            $criteria->add(CommonTTypeContratPeer::ID_TYPE_CONTRAT, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTTypeContratPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTTypeContratPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTTypeContratPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTTypeContrat object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTTypeContrat $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTTypeContratPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTTypeContratPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTTypeContratPeer::DATABASE_NAME, CommonTTypeContratPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTTypeContrat
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTTypeContratPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTTypeContratPeer::DATABASE_NAME);
        $criteria->add(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $pk);

        $v = CommonTTypeContratPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTTypeContrat[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTTypeContratPeer::DATABASE_NAME);
            $criteria->add(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $pks, Criteria::IN);
            $objs = CommonTTypeContratPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTTypeContratPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTTypeContratPeer::buildTableMap();

