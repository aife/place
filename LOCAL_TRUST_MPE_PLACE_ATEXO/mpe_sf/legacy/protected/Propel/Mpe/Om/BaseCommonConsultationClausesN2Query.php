<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationClausesN1;
use Application\Propel\Mpe\CommonConsultationClausesN2;
use Application\Propel\Mpe\CommonConsultationClausesN2Peer;
use Application\Propel\Mpe\CommonConsultationClausesN2Query;
use Application\Propel\Mpe\CommonConsultationClausesN3;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN2;

/**
 * Base class that represents a query for the 'consultation_clauses_n2' table.
 *
 *
 *
 * @method CommonConsultationClausesN2Query orderById($order = Criteria::ASC) Order by the id column
 * @method CommonConsultationClausesN2Query orderByClauseN1Id($order = Criteria::ASC) Order by the clause_n1_id column
 * @method CommonConsultationClausesN2Query orderByReferentielClauseN2Id($order = Criteria::ASC) Order by the referentiel_clause_n2_id column
 *
 * @method CommonConsultationClausesN2Query groupById() Group by the id column
 * @method CommonConsultationClausesN2Query groupByClauseN1Id() Group by the clause_n1_id column
 * @method CommonConsultationClausesN2Query groupByReferentielClauseN2Id() Group by the referentiel_clause_n2_id column
 *
 * @method CommonConsultationClausesN2Query leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConsultationClausesN2Query rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConsultationClausesN2Query innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConsultationClausesN2Query leftJoinCommonReferentielConsultationClausesN2($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonReferentielConsultationClausesN2 relation
 * @method CommonConsultationClausesN2Query rightJoinCommonReferentielConsultationClausesN2($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonReferentielConsultationClausesN2 relation
 * @method CommonConsultationClausesN2Query innerJoinCommonReferentielConsultationClausesN2($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonReferentielConsultationClausesN2 relation
 *
 * @method CommonConsultationClausesN2Query leftJoinCommonConsultationClausesN1($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationClausesN1 relation
 * @method CommonConsultationClausesN2Query rightJoinCommonConsultationClausesN1($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationClausesN1 relation
 * @method CommonConsultationClausesN2Query innerJoinCommonConsultationClausesN1($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationClausesN1 relation
 *
 * @method CommonConsultationClausesN2Query leftJoinCommonConsultationClausesN3($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationClausesN3 relation
 * @method CommonConsultationClausesN2Query rightJoinCommonConsultationClausesN3($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationClausesN3 relation
 * @method CommonConsultationClausesN2Query innerJoinCommonConsultationClausesN3($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationClausesN3 relation
 *
 * @method CommonConsultationClausesN2 findOne(PropelPDO $con = null) Return the first CommonConsultationClausesN2 matching the query
 * @method CommonConsultationClausesN2 findOneOrCreate(PropelPDO $con = null) Return the first CommonConsultationClausesN2 matching the query, or a new CommonConsultationClausesN2 object populated from the query conditions when no match is found
 *
 * @method CommonConsultationClausesN2 findOneByClauseN1Id(int $clause_n1_id) Return the first CommonConsultationClausesN2 filtered by the clause_n1_id column
 * @method CommonConsultationClausesN2 findOneByReferentielClauseN2Id(int $referentiel_clause_n2_id) Return the first CommonConsultationClausesN2 filtered by the referentiel_clause_n2_id column
 *
 * @method array findById(int $id) Return CommonConsultationClausesN2 objects filtered by the id column
 * @method array findByClauseN1Id(int $clause_n1_id) Return CommonConsultationClausesN2 objects filtered by the clause_n1_id column
 * @method array findByReferentielClauseN2Id(int $referentiel_clause_n2_id) Return CommonConsultationClausesN2 objects filtered by the referentiel_clause_n2_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationClausesN2Query extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConsultationClausesN2Query object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConsultationClausesN2', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConsultationClausesN2Query object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConsultationClausesN2Query|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConsultationClausesN2Query
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConsultationClausesN2Query) {
            return $criteria;
        }
        $query = new CommonConsultationClausesN2Query();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConsultationClausesN2|CommonConsultationClausesN2[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConsultationClausesN2Peer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN2Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationClausesN2 A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationClausesN2 A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `clause_n1_id`, `referentiel_clause_n2_id` FROM `consultation_clauses_n2` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConsultationClausesN2();
            $obj->hydrate($row);
            CommonConsultationClausesN2Peer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConsultationClausesN2|CommonConsultationClausesN2[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConsultationClausesN2[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConsultationClausesN2Peer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConsultationClausesN2Peer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN2Peer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN2Peer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN2Peer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the clause_n1_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseN1Id(1234); // WHERE clause_n1_id = 1234
     * $query->filterByClauseN1Id(array(12, 34)); // WHERE clause_n1_id IN (12, 34)
     * $query->filterByClauseN1Id(array('min' => 12)); // WHERE clause_n1_id >= 12
     * $query->filterByClauseN1Id(array('max' => 12)); // WHERE clause_n1_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultationClausesN1()
     *
     * @param     mixed $clauseN1Id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterByClauseN1Id($clauseN1Id = null, $comparison = null)
    {
        if (is_array($clauseN1Id)) {
            $useMinMax = false;
            if (isset($clauseN1Id['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN2Peer::CLAUSE_N1_ID, $clauseN1Id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clauseN1Id['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN2Peer::CLAUSE_N1_ID, $clauseN1Id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN2Peer::CLAUSE_N1_ID, $clauseN1Id, $comparison);
    }

    /**
     * Filter the query on the referentiel_clause_n2_id column
     *
     * Example usage:
     * <code>
     * $query->filterByReferentielClauseN2Id(1234); // WHERE referentiel_clause_n2_id = 1234
     * $query->filterByReferentielClauseN2Id(array(12, 34)); // WHERE referentiel_clause_n2_id IN (12, 34)
     * $query->filterByReferentielClauseN2Id(array('min' => 12)); // WHERE referentiel_clause_n2_id >= 12
     * $query->filterByReferentielClauseN2Id(array('max' => 12)); // WHERE referentiel_clause_n2_id <= 12
     * </code>
     *
     * @see       filterByCommonReferentielConsultationClausesN2()
     *
     * @param     mixed $referentielClauseN2Id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterByReferentielClauseN2Id($referentielClauseN2Id = null, $comparison = null)
    {
        if (is_array($referentielClauseN2Id)) {
            $useMinMax = false;
            if (isset($referentielClauseN2Id['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN2Peer::REFERENTIEL_CLAUSE_N2_ID, $referentielClauseN2Id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($referentielClauseN2Id['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN2Peer::REFERENTIEL_CLAUSE_N2_ID, $referentielClauseN2Id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN2Peer::REFERENTIEL_CLAUSE_N2_ID, $referentielClauseN2Id, $comparison);
    }

    /**
     * Filter the query by a related CommonReferentielConsultationClausesN2 object
     *
     * @param   CommonReferentielConsultationClausesN2|PropelObjectCollection $commonReferentielConsultationClausesN2 The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN2Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonReferentielConsultationClausesN2($commonReferentielConsultationClausesN2, $comparison = null)
    {
        if ($commonReferentielConsultationClausesN2 instanceof CommonReferentielConsultationClausesN2) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN2Peer::REFERENTIEL_CLAUSE_N2_ID, $commonReferentielConsultationClausesN2->getId(), $comparison);
        } elseif ($commonReferentielConsultationClausesN2 instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationClausesN2Peer::REFERENTIEL_CLAUSE_N2_ID, $commonReferentielConsultationClausesN2->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonReferentielConsultationClausesN2() only accepts arguments of type CommonReferentielConsultationClausesN2 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonReferentielConsultationClausesN2 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN2Query The current query, for fluid interface
     */
    public function joinCommonReferentielConsultationClausesN2($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonReferentielConsultationClausesN2');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonReferentielConsultationClausesN2');
        }

        return $this;
    }

    /**
     * Use the CommonReferentielConsultationClausesN2 relation CommonReferentielConsultationClausesN2 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonReferentielConsultationClausesN2Query A secondary query class using the current class as primary query
     */
    public function useCommonReferentielConsultationClausesN2Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonReferentielConsultationClausesN2($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonReferentielConsultationClausesN2', '\Application\Propel\Mpe\CommonReferentielConsultationClausesN2Query');
    }

    /**
     * Filter the query by a related CommonConsultationClausesN1 object
     *
     * @param   CommonConsultationClausesN1|PropelObjectCollection $commonConsultationClausesN1 The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN2Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationClausesN1($commonConsultationClausesN1, $comparison = null)
    {
        if ($commonConsultationClausesN1 instanceof CommonConsultationClausesN1) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN2Peer::CLAUSE_N1_ID, $commonConsultationClausesN1->getId(), $comparison);
        } elseif ($commonConsultationClausesN1 instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationClausesN2Peer::CLAUSE_N1_ID, $commonConsultationClausesN1->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultationClausesN1() only accepts arguments of type CommonConsultationClausesN1 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationClausesN1 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN2Query The current query, for fluid interface
     */
    public function joinCommonConsultationClausesN1($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationClausesN1');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationClausesN1');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationClausesN1 relation CommonConsultationClausesN1 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationClausesN1Query A secondary query class using the current class as primary query
     */
    public function useCommonConsultationClausesN1Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationClausesN1($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationClausesN1', '\Application\Propel\Mpe\CommonConsultationClausesN1Query');
    }

    /**
     * Filter the query by a related CommonConsultationClausesN3 object
     *
     * @param   CommonConsultationClausesN3|PropelObjectCollection $commonConsultationClausesN3  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN2Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationClausesN3($commonConsultationClausesN3, $comparison = null)
    {
        if ($commonConsultationClausesN3 instanceof CommonConsultationClausesN3) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN2Peer::ID, $commonConsultationClausesN3->getClauseN2Id(), $comparison);
        } elseif ($commonConsultationClausesN3 instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationClausesN3Query()
                ->filterByPrimaryKeys($commonConsultationClausesN3->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultationClausesN3() only accepts arguments of type CommonConsultationClausesN3 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationClausesN3 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN2Query The current query, for fluid interface
     */
    public function joinCommonConsultationClausesN3($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationClausesN3');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationClausesN3');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationClausesN3 relation CommonConsultationClausesN3 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationClausesN3Query A secondary query class using the current class as primary query
     */
    public function useCommonConsultationClausesN3Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationClausesN3($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationClausesN3', '\Application\Propel\Mpe\CommonConsultationClausesN3Query');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConsultationClausesN2 $commonConsultationClausesN2 Object to remove from the list of results
     *
     * @return CommonConsultationClausesN2Query The current query, for fluid interface
     */
    public function prune($commonConsultationClausesN2 = null)
    {
        if ($commonConsultationClausesN2) {
            $this->addUsingAlias(CommonConsultationClausesN2Peer::ID, $commonConsultationClausesN2->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
