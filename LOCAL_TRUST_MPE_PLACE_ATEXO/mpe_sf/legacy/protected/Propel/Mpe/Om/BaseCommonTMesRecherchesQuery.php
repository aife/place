<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonTMesRecherches;
use Application\Propel\Mpe\CommonTMesRecherchesPeer;
use Application\Propel\Mpe\CommonTMesRecherchesQuery;

/**
 * Base class that represents a query for the 'T_MesRecherches' table.
 *
 *
 *
 * @method CommonTMesRecherchesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTMesRecherchesQuery orderByIdCreateur($order = Criteria::ASC) Order by the id_createur column
 * @method CommonTMesRecherchesQuery orderByTypeCreateur($order = Criteria::ASC) Order by the type_createur column
 * @method CommonTMesRecherchesQuery orderByDenomination($order = Criteria::ASC) Order by the denomination column
 * @method CommonTMesRecherchesQuery orderByPeriodicite($order = Criteria::ASC) Order by the periodicite column
 * @method CommonTMesRecherchesQuery orderByXmlcriteria($order = Criteria::ASC) Order by the xmlCriteria column
 * @method CommonTMesRecherchesQuery orderByCategorie($order = Criteria::ASC) Order by the categorie column
 * @method CommonTMesRecherchesQuery orderByIdInitial($order = Criteria::ASC) Order by the id_initial column
 * @method CommonTMesRecherchesQuery orderByFormat($order = Criteria::ASC) Order by the format column
 * @method CommonTMesRecherchesQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTMesRecherchesQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonTMesRecherchesQuery orderByRecherche($order = Criteria::ASC) Order by the recherche column
 * @method CommonTMesRecherchesQuery orderByAlerte($order = Criteria::ASC) Order by the alerte column
 * @method CommonTMesRecherchesQuery orderByTypeAvis($order = Criteria::ASC) Order by the type_avis column
 * @method CommonTMesRecherchesQuery orderByPlateformeVirtuelleId($order = Criteria::ASC) Order by the plateforme_virtuelle_id column
 * @method CommonTMesRecherchesQuery orderByCriteria($order = Criteria::ASC) Order by the criteria column
 * @method CommonTMesRecherchesQuery orderByHashedCriteria($order = Criteria::ASC) Order by the hashed_criteria column
 *
 * @method CommonTMesRecherchesQuery groupById() Group by the id column
 * @method CommonTMesRecherchesQuery groupByIdCreateur() Group by the id_createur column
 * @method CommonTMesRecherchesQuery groupByTypeCreateur() Group by the type_createur column
 * @method CommonTMesRecherchesQuery groupByDenomination() Group by the denomination column
 * @method CommonTMesRecherchesQuery groupByPeriodicite() Group by the periodicite column
 * @method CommonTMesRecherchesQuery groupByXmlcriteria() Group by the xmlCriteria column
 * @method CommonTMesRecherchesQuery groupByCategorie() Group by the categorie column
 * @method CommonTMesRecherchesQuery groupByIdInitial() Group by the id_initial column
 * @method CommonTMesRecherchesQuery groupByFormat() Group by the format column
 * @method CommonTMesRecherchesQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTMesRecherchesQuery groupByDateModification() Group by the date_modification column
 * @method CommonTMesRecherchesQuery groupByRecherche() Group by the recherche column
 * @method CommonTMesRecherchesQuery groupByAlerte() Group by the alerte column
 * @method CommonTMesRecherchesQuery groupByTypeAvis() Group by the type_avis column
 * @method CommonTMesRecherchesQuery groupByPlateformeVirtuelleId() Group by the plateforme_virtuelle_id column
 * @method CommonTMesRecherchesQuery groupByCriteria() Group by the criteria column
 * @method CommonTMesRecherchesQuery groupByHashedCriteria() Group by the hashed_criteria column
 *
 * @method CommonTMesRecherchesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTMesRecherchesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTMesRecherchesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTMesRecherchesQuery leftJoinCommonPlateformeVirtuelle($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonPlateformeVirtuelle relation
 * @method CommonTMesRecherchesQuery rightJoinCommonPlateformeVirtuelle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonPlateformeVirtuelle relation
 * @method CommonTMesRecherchesQuery innerJoinCommonPlateformeVirtuelle($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonPlateformeVirtuelle relation
 *
 * @method CommonTMesRecherches findOne(PropelPDO $con = null) Return the first CommonTMesRecherches matching the query
 * @method CommonTMesRecherches findOneOrCreate(PropelPDO $con = null) Return the first CommonTMesRecherches matching the query, or a new CommonTMesRecherches object populated from the query conditions when no match is found
 *
 * @method CommonTMesRecherches findOneByIdCreateur(int $id_createur) Return the first CommonTMesRecherches filtered by the id_createur column
 * @method CommonTMesRecherches findOneByTypeCreateur(string $type_createur) Return the first CommonTMesRecherches filtered by the type_createur column
 * @method CommonTMesRecherches findOneByDenomination(string $denomination) Return the first CommonTMesRecherches filtered by the denomination column
 * @method CommonTMesRecherches findOneByPeriodicite(string $periodicite) Return the first CommonTMesRecherches filtered by the periodicite column
 * @method CommonTMesRecherches findOneByXmlcriteria(string $xmlCriteria) Return the first CommonTMesRecherches filtered by the xmlCriteria column
 * @method CommonTMesRecherches findOneByCategorie(string $categorie) Return the first CommonTMesRecherches filtered by the categorie column
 * @method CommonTMesRecherches findOneByIdInitial(int $id_initial) Return the first CommonTMesRecherches filtered by the id_initial column
 * @method CommonTMesRecherches findOneByFormat(string $format) Return the first CommonTMesRecherches filtered by the format column
 * @method CommonTMesRecherches findOneByDateCreation(string $date_creation) Return the first CommonTMesRecherches filtered by the date_creation column
 * @method CommonTMesRecherches findOneByDateModification(string $date_modification) Return the first CommonTMesRecherches filtered by the date_modification column
 * @method CommonTMesRecherches findOneByRecherche(string $recherche) Return the first CommonTMesRecherches filtered by the recherche column
 * @method CommonTMesRecherches findOneByAlerte(string $alerte) Return the first CommonTMesRecherches filtered by the alerte column
 * @method CommonTMesRecherches findOneByTypeAvis(int $type_avis) Return the first CommonTMesRecherches filtered by the type_avis column
 * @method CommonTMesRecherches findOneByPlateformeVirtuelleId(int $plateforme_virtuelle_id) Return the first CommonTMesRecherches filtered by the plateforme_virtuelle_id column
 * @method CommonTMesRecherches findOneByCriteria(string $criteria) Return the first CommonTMesRecherches filtered by the criteria column
 * @method CommonTMesRecherches findOneByHashedCriteria(string $hashed_criteria) Return the first CommonTMesRecherches filtered by the hashed_criteria column
 *
 * @method array findById(int $id) Return CommonTMesRecherches objects filtered by the id column
 * @method array findByIdCreateur(int $id_createur) Return CommonTMesRecherches objects filtered by the id_createur column
 * @method array findByTypeCreateur(string $type_createur) Return CommonTMesRecherches objects filtered by the type_createur column
 * @method array findByDenomination(string $denomination) Return CommonTMesRecherches objects filtered by the denomination column
 * @method array findByPeriodicite(string $periodicite) Return CommonTMesRecherches objects filtered by the periodicite column
 * @method array findByXmlcriteria(string $xmlCriteria) Return CommonTMesRecherches objects filtered by the xmlCriteria column
 * @method array findByCategorie(string $categorie) Return CommonTMesRecherches objects filtered by the categorie column
 * @method array findByIdInitial(int $id_initial) Return CommonTMesRecherches objects filtered by the id_initial column
 * @method array findByFormat(string $format) Return CommonTMesRecherches objects filtered by the format column
 * @method array findByDateCreation(string $date_creation) Return CommonTMesRecherches objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonTMesRecherches objects filtered by the date_modification column
 * @method array findByRecherche(string $recherche) Return CommonTMesRecherches objects filtered by the recherche column
 * @method array findByAlerte(string $alerte) Return CommonTMesRecherches objects filtered by the alerte column
 * @method array findByTypeAvis(int $type_avis) Return CommonTMesRecherches objects filtered by the type_avis column
 * @method array findByPlateformeVirtuelleId(int $plateforme_virtuelle_id) Return CommonTMesRecherches objects filtered by the plateforme_virtuelle_id column
 * @method array findByCriteria(string $criteria) Return CommonTMesRecherches objects filtered by the criteria column
 * @method array findByHashedCriteria(string $hashed_criteria) Return CommonTMesRecherches objects filtered by the hashed_criteria column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTMesRecherchesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTMesRecherchesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTMesRecherches', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTMesRecherchesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTMesRecherchesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTMesRecherchesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTMesRecherchesQuery) {
            return $criteria;
        }
        $query = new CommonTMesRecherchesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTMesRecherches|CommonTMesRecherches[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTMesRecherchesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTMesRecherches A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTMesRecherches A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_createur`, `type_createur`, `denomination`, `periodicite`, `xmlCriteria`, `categorie`, `id_initial`, `format`, `date_creation`, `date_modification`, `recherche`, `alerte`, `type_avis`, `plateforme_virtuelle_id`, `criteria`, `hashed_criteria` FROM `T_MesRecherches` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTMesRecherches();
            $obj->hydrate($row);
            CommonTMesRecherchesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTMesRecherches|CommonTMesRecherches[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTMesRecherches[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTMesRecherchesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTMesRecherchesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_createur column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCreateur(1234); // WHERE id_createur = 1234
     * $query->filterByIdCreateur(array(12, 34)); // WHERE id_createur IN (12, 34)
     * $query->filterByIdCreateur(array('min' => 12)); // WHERE id_createur >= 12
     * $query->filterByIdCreateur(array('max' => 12)); // WHERE id_createur <= 12
     * </code>
     *
     * @param     mixed $idCreateur The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByIdCreateur($idCreateur = null, $comparison = null)
    {
        if (is_array($idCreateur)) {
            $useMinMax = false;
            if (isset($idCreateur['min'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::ID_CREATEUR, $idCreateur['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCreateur['max'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::ID_CREATEUR, $idCreateur['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::ID_CREATEUR, $idCreateur, $comparison);
    }

    /**
     * Filter the query on the type_createur column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeCreateur('fooValue');   // WHERE type_createur = 'fooValue'
     * $query->filterByTypeCreateur('%fooValue%'); // WHERE type_createur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeCreateur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByTypeCreateur($typeCreateur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeCreateur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeCreateur)) {
                $typeCreateur = str_replace('*', '%', $typeCreateur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::TYPE_CREATEUR, $typeCreateur, $comparison);
    }

    /**
     * Filter the query on the denomination column
     *
     * Example usage:
     * <code>
     * $query->filterByDenomination('fooValue');   // WHERE denomination = 'fooValue'
     * $query->filterByDenomination('%fooValue%'); // WHERE denomination LIKE '%fooValue%'
     * </code>
     *
     * @param     string $denomination The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByDenomination($denomination = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($denomination)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $denomination)) {
                $denomination = str_replace('*', '%', $denomination);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::DENOMINATION, $denomination, $comparison);
    }

    /**
     * Filter the query on the periodicite column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodicite('fooValue');   // WHERE periodicite = 'fooValue'
     * $query->filterByPeriodicite('%fooValue%'); // WHERE periodicite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $periodicite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByPeriodicite($periodicite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($periodicite)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $periodicite)) {
                $periodicite = str_replace('*', '%', $periodicite);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::PERIODICITE, $periodicite, $comparison);
    }

    /**
     * Filter the query on the xmlCriteria column
     *
     * Example usage:
     * <code>
     * $query->filterByXmlcriteria('fooValue');   // WHERE xmlCriteria = 'fooValue'
     * $query->filterByXmlcriteria('%fooValue%'); // WHERE xmlCriteria LIKE '%fooValue%'
     * </code>
     *
     * @param     string $xmlcriteria The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByXmlcriteria($xmlcriteria = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($xmlcriteria)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $xmlcriteria)) {
                $xmlcriteria = str_replace('*', '%', $xmlcriteria);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::XMLCRITERIA, $xmlcriteria, $comparison);
    }

    /**
     * Filter the query on the categorie column
     *
     * Example usage:
     * <code>
     * $query->filterByCategorie('fooValue');   // WHERE categorie = 'fooValue'
     * $query->filterByCategorie('%fooValue%'); // WHERE categorie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $categorie The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByCategorie($categorie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($categorie)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $categorie)) {
                $categorie = str_replace('*', '%', $categorie);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::CATEGORIE, $categorie, $comparison);
    }

    /**
     * Filter the query on the id_initial column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInitial(1234); // WHERE id_initial = 1234
     * $query->filterByIdInitial(array(12, 34)); // WHERE id_initial IN (12, 34)
     * $query->filterByIdInitial(array('min' => 12)); // WHERE id_initial >= 12
     * $query->filterByIdInitial(array('max' => 12)); // WHERE id_initial <= 12
     * </code>
     *
     * @param     mixed $idInitial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByIdInitial($idInitial = null, $comparison = null)
    {
        if (is_array($idInitial)) {
            $useMinMax = false;
            if (isset($idInitial['min'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::ID_INITIAL, $idInitial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInitial['max'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::ID_INITIAL, $idInitial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::ID_INITIAL, $idInitial, $comparison);
    }

    /**
     * Filter the query on the format column
     *
     * Example usage:
     * <code>
     * $query->filterByFormat('fooValue');   // WHERE format = 'fooValue'
     * $query->filterByFormat('%fooValue%'); // WHERE format LIKE '%fooValue%'
     * </code>
     *
     * @param     string $format The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByFormat($format = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($format)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $format)) {
                $format = str_replace('*', '%', $format);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::FORMAT, $format, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the recherche column
     *
     * Example usage:
     * <code>
     * $query->filterByRecherche('fooValue');   // WHERE recherche = 'fooValue'
     * $query->filterByRecherche('%fooValue%'); // WHERE recherche LIKE '%fooValue%'
     * </code>
     *
     * @param     string $recherche The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByRecherche($recherche = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($recherche)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $recherche)) {
                $recherche = str_replace('*', '%', $recherche);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::RECHERCHE, $recherche, $comparison);
    }

    /**
     * Filter the query on the alerte column
     *
     * Example usage:
     * <code>
     * $query->filterByAlerte('fooValue');   // WHERE alerte = 'fooValue'
     * $query->filterByAlerte('%fooValue%'); // WHERE alerte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alerte The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByAlerte($alerte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alerte)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alerte)) {
                $alerte = str_replace('*', '%', $alerte);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::ALERTE, $alerte, $comparison);
    }

    /**
     * Filter the query on the type_avis column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeAvis(1234); // WHERE type_avis = 1234
     * $query->filterByTypeAvis(array(12, 34)); // WHERE type_avis IN (12, 34)
     * $query->filterByTypeAvis(array('min' => 12)); // WHERE type_avis >= 12
     * $query->filterByTypeAvis(array('max' => 12)); // WHERE type_avis <= 12
     * </code>
     *
     * @param     mixed $typeAvis The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByTypeAvis($typeAvis = null, $comparison = null)
    {
        if (is_array($typeAvis)) {
            $useMinMax = false;
            if (isset($typeAvis['min'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::TYPE_AVIS, $typeAvis['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeAvis['max'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::TYPE_AVIS, $typeAvis['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::TYPE_AVIS, $typeAvis, $comparison);
    }

    /**
     * Filter the query on the plateforme_virtuelle_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPlateformeVirtuelleId(1234); // WHERE plateforme_virtuelle_id = 1234
     * $query->filterByPlateformeVirtuelleId(array(12, 34)); // WHERE plateforme_virtuelle_id IN (12, 34)
     * $query->filterByPlateformeVirtuelleId(array('min' => 12)); // WHERE plateforme_virtuelle_id >= 12
     * $query->filterByPlateformeVirtuelleId(array('max' => 12)); // WHERE plateforme_virtuelle_id <= 12
     * </code>
     *
     * @see       filterByCommonPlateformeVirtuelle()
     *
     * @param     mixed $plateformeVirtuelleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByPlateformeVirtuelleId($plateformeVirtuelleId = null, $comparison = null)
    {
        if (is_array($plateformeVirtuelleId)) {
            $useMinMax = false;
            if (isset($plateformeVirtuelleId['min'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, $plateformeVirtuelleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($plateformeVirtuelleId['max'])) {
                $this->addUsingAlias(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, $plateformeVirtuelleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, $plateformeVirtuelleId, $comparison);
    }

    /**
     * Filter the query on the criteria column
     *
     * Example usage:
     * <code>
     * $query->filterByCriteria('fooValue');   // WHERE criteria = 'fooValue'
     * $query->filterByCriteria('%fooValue%'); // WHERE criteria LIKE '%fooValue%'
     * </code>
     *
     * @param     string $criteria The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByCriteria($criteria = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($criteria)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $criteria)) {
                $criteria = str_replace('*', '%', $criteria);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::CRITERIA, $criteria, $comparison);
    }

    /**
     * Filter the query on the hashed_criteria column
     *
     * Example usage:
     * <code>
     * $query->filterByHashedCriteria('fooValue');   // WHERE hashed_criteria = 'fooValue'
     * $query->filterByHashedCriteria('%fooValue%'); // WHERE hashed_criteria LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hashedCriteria The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function filterByHashedCriteria($hashedCriteria = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hashedCriteria)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $hashedCriteria)) {
                $hashedCriteria = str_replace('*', '%', $hashedCriteria);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMesRecherchesPeer::HASHED_CRITERIA, $hashedCriteria, $comparison);
    }

    /**
     * Filter the query by a related CommonPlateformeVirtuelle object
     *
     * @param   CommonPlateformeVirtuelle|PropelObjectCollection $commonPlateformeVirtuelle The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTMesRecherchesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonPlateformeVirtuelle($commonPlateformeVirtuelle, $comparison = null)
    {
        if ($commonPlateformeVirtuelle instanceof CommonPlateformeVirtuelle) {
            return $this
                ->addUsingAlias(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, $commonPlateformeVirtuelle->getId(), $comparison);
        } elseif ($commonPlateformeVirtuelle instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, $commonPlateformeVirtuelle->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonPlateformeVirtuelle() only accepts arguments of type CommonPlateformeVirtuelle or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonPlateformeVirtuelle relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function joinCommonPlateformeVirtuelle($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonPlateformeVirtuelle');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonPlateformeVirtuelle');
        }

        return $this;
    }

    /**
     * Use the CommonPlateformeVirtuelle relation CommonPlateformeVirtuelle object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonPlateformeVirtuelleQuery A secondary query class using the current class as primary query
     */
    public function useCommonPlateformeVirtuelleQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonPlateformeVirtuelle($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonPlateformeVirtuelle', '\Application\Propel\Mpe\CommonPlateformeVirtuelleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTMesRecherches $commonTMesRecherches Object to remove from the list of results
     *
     * @return CommonTMesRecherchesQuery The current query, for fluid interface
     */
    public function prune($commonTMesRecherches = null)
    {
        if ($commonTMesRecherches) {
            $this->addUsingAlias(CommonTMesRecherchesPeer::ID, $commonTMesRecherches->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
