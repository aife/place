<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonHistoriqueNbrConsultationsPubliees;
use Application\Propel\Mpe\CommonHistoriqueNbrConsultationsPublieesPeer;
use Application\Propel\Mpe\CommonHistoriqueNbrConsultationsPublieesQuery;

/**
 * Base class that represents a query for the 'HistoriqueNbrConsultationsPubliees' table.
 *
 *
 *
 * @method CommonHistoriqueNbrConsultationsPublieesQuery orderByIdHistoriqueNbrCons($order = Criteria::ASC) Order by the id_historique_nbr_cons column
 * @method CommonHistoriqueNbrConsultationsPublieesQuery orderByDatePublication($order = Criteria::ASC) Order by the date_publication column
 * @method CommonHistoriqueNbrConsultationsPublieesQuery orderByNbrConsultationsPubliees($order = Criteria::ASC) Order by the nbr_consultations_publiees column
 *
 * @method CommonHistoriqueNbrConsultationsPublieesQuery groupByIdHistoriqueNbrCons() Group by the id_historique_nbr_cons column
 * @method CommonHistoriqueNbrConsultationsPublieesQuery groupByDatePublication() Group by the date_publication column
 * @method CommonHistoriqueNbrConsultationsPublieesQuery groupByNbrConsultationsPubliees() Group by the nbr_consultations_publiees column
 *
 * @method CommonHistoriqueNbrConsultationsPublieesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonHistoriqueNbrConsultationsPublieesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonHistoriqueNbrConsultationsPublieesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonHistoriqueNbrConsultationsPubliees findOne(PropelPDO $con = null) Return the first CommonHistoriqueNbrConsultationsPubliees matching the query
 * @method CommonHistoriqueNbrConsultationsPubliees findOneOrCreate(PropelPDO $con = null) Return the first CommonHistoriqueNbrConsultationsPubliees matching the query, or a new CommonHistoriqueNbrConsultationsPubliees object populated from the query conditions when no match is found
 *
 * @method CommonHistoriqueNbrConsultationsPubliees findOneByDatePublication(string $date_publication) Return the first CommonHistoriqueNbrConsultationsPubliees filtered by the date_publication column
 * @method CommonHistoriqueNbrConsultationsPubliees findOneByNbrConsultationsPubliees(int $nbr_consultations_publiees) Return the first CommonHistoriqueNbrConsultationsPubliees filtered by the nbr_consultations_publiees column
 *
 * @method array findByIdHistoriqueNbrCons(int $id_historique_nbr_cons) Return CommonHistoriqueNbrConsultationsPubliees objects filtered by the id_historique_nbr_cons column
 * @method array findByDatePublication(string $date_publication) Return CommonHistoriqueNbrConsultationsPubliees objects filtered by the date_publication column
 * @method array findByNbrConsultationsPubliees(int $nbr_consultations_publiees) Return CommonHistoriqueNbrConsultationsPubliees objects filtered by the nbr_consultations_publiees column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonHistoriqueNbrConsultationsPublieesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonHistoriqueNbrConsultationsPublieesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonHistoriqueNbrConsultationsPubliees', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonHistoriqueNbrConsultationsPublieesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonHistoriqueNbrConsultationsPublieesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonHistoriqueNbrConsultationsPublieesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonHistoriqueNbrConsultationsPublieesQuery) {
            return $criteria;
        }
        $query = new CommonHistoriqueNbrConsultationsPublieesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonHistoriqueNbrConsultationsPubliees|CommonHistoriqueNbrConsultationsPubliees[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonHistoriqueNbrConsultationsPublieesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonHistoriqueNbrConsultationsPublieesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonHistoriqueNbrConsultationsPubliees A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdHistoriqueNbrCons($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonHistoriqueNbrConsultationsPubliees A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_historique_nbr_cons`, `date_publication`, `nbr_consultations_publiees` FROM `HistoriqueNbrConsultationsPubliees` WHERE `id_historique_nbr_cons` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonHistoriqueNbrConsultationsPubliees();
            $obj->hydrate($row);
            CommonHistoriqueNbrConsultationsPublieesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonHistoriqueNbrConsultationsPubliees|CommonHistoriqueNbrConsultationsPubliees[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonHistoriqueNbrConsultationsPubliees[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonHistoriqueNbrConsultationsPublieesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::ID_HISTORIQUE_NBR_CONS, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonHistoriqueNbrConsultationsPublieesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::ID_HISTORIQUE_NBR_CONS, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_historique_nbr_cons column
     *
     * Example usage:
     * <code>
     * $query->filterByIdHistoriqueNbrCons(1234); // WHERE id_historique_nbr_cons = 1234
     * $query->filterByIdHistoriqueNbrCons(array(12, 34)); // WHERE id_historique_nbr_cons IN (12, 34)
     * $query->filterByIdHistoriqueNbrCons(array('min' => 12)); // WHERE id_historique_nbr_cons >= 12
     * $query->filterByIdHistoriqueNbrCons(array('max' => 12)); // WHERE id_historique_nbr_cons <= 12
     * </code>
     *
     * @param     mixed $idHistoriqueNbrCons The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHistoriqueNbrConsultationsPublieesQuery The current query, for fluid interface
     */
    public function filterByIdHistoriqueNbrCons($idHistoriqueNbrCons = null, $comparison = null)
    {
        if (is_array($idHistoriqueNbrCons)) {
            $useMinMax = false;
            if (isset($idHistoriqueNbrCons['min'])) {
                $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::ID_HISTORIQUE_NBR_CONS, $idHistoriqueNbrCons['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idHistoriqueNbrCons['max'])) {
                $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::ID_HISTORIQUE_NBR_CONS, $idHistoriqueNbrCons['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::ID_HISTORIQUE_NBR_CONS, $idHistoriqueNbrCons, $comparison);
    }

    /**
     * Filter the query on the date_publication column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePublication('2011-03-14'); // WHERE date_publication = '2011-03-14'
     * $query->filterByDatePublication('now'); // WHERE date_publication = '2011-03-14'
     * $query->filterByDatePublication(array('max' => 'yesterday')); // WHERE date_publication > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePublication The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHistoriqueNbrConsultationsPublieesQuery The current query, for fluid interface
     */
    public function filterByDatePublication($datePublication = null, $comparison = null)
    {
        if (is_array($datePublication)) {
            $useMinMax = false;
            if (isset($datePublication['min'])) {
                $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::DATE_PUBLICATION, $datePublication['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePublication['max'])) {
                $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::DATE_PUBLICATION, $datePublication['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::DATE_PUBLICATION, $datePublication, $comparison);
    }

    /**
     * Filter the query on the nbr_consultations_publiees column
     *
     * Example usage:
     * <code>
     * $query->filterByNbrConsultationsPubliees(1234); // WHERE nbr_consultations_publiees = 1234
     * $query->filterByNbrConsultationsPubliees(array(12, 34)); // WHERE nbr_consultations_publiees IN (12, 34)
     * $query->filterByNbrConsultationsPubliees(array('min' => 12)); // WHERE nbr_consultations_publiees >= 12
     * $query->filterByNbrConsultationsPubliees(array('max' => 12)); // WHERE nbr_consultations_publiees <= 12
     * </code>
     *
     * @param     mixed $nbrConsultationsPubliees The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHistoriqueNbrConsultationsPublieesQuery The current query, for fluid interface
     */
    public function filterByNbrConsultationsPubliees($nbrConsultationsPubliees = null, $comparison = null)
    {
        if (is_array($nbrConsultationsPubliees)) {
            $useMinMax = false;
            if (isset($nbrConsultationsPubliees['min'])) {
                $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::NBR_CONSULTATIONS_PUBLIEES, $nbrConsultationsPubliees['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbrConsultationsPubliees['max'])) {
                $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::NBR_CONSULTATIONS_PUBLIEES, $nbrConsultationsPubliees['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::NBR_CONSULTATIONS_PUBLIEES, $nbrConsultationsPubliees, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonHistoriqueNbrConsultationsPubliees $commonHistoriqueNbrConsultationsPubliees Object to remove from the list of results
     *
     * @return CommonHistoriqueNbrConsultationsPublieesQuery The current query, for fluid interface
     */
    public function prune($commonHistoriqueNbrConsultationsPubliees = null)
    {
        if ($commonHistoriqueNbrConsultationsPubliees) {
            $this->addUsingAlias(CommonHistoriqueNbrConsultationsPublieesPeer::ID_HISTORIQUE_NBR_CONS, $commonHistoriqueNbrConsultationsPubliees->getIdHistoriqueNbrCons(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
