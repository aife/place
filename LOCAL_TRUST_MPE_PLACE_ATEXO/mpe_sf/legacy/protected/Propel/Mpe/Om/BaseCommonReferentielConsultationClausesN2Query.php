<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationClausesN2;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN1;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN2;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN2ClausesN3;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN2Peer;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN2Query;

/**
 * Base class that represents a query for the 'referentiel_consultation_clauses_n2' table.
 *
 *
 *
 * @method CommonReferentielConsultationClausesN2Query orderById($order = Criteria::ASC) Order by the id column
 * @method CommonReferentielConsultationClausesN2Query orderByClauseN1Id($order = Criteria::ASC) Order by the clause_n1_id column
 * @method CommonReferentielConsultationClausesN2Query orderByLabel($order = Criteria::ASC) Order by the label column
 * @method CommonReferentielConsultationClausesN2Query orderBySlug($order = Criteria::ASC) Order by the slug column
 *
 * @method CommonReferentielConsultationClausesN2Query groupById() Group by the id column
 * @method CommonReferentielConsultationClausesN2Query groupByClauseN1Id() Group by the clause_n1_id column
 * @method CommonReferentielConsultationClausesN2Query groupByLabel() Group by the label column
 * @method CommonReferentielConsultationClausesN2Query groupBySlug() Group by the slug column
 *
 * @method CommonReferentielConsultationClausesN2Query leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonReferentielConsultationClausesN2Query rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonReferentielConsultationClausesN2Query innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonReferentielConsultationClausesN2Query leftJoinCommonReferentielConsultationClausesN1($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonReferentielConsultationClausesN1 relation
 * @method CommonReferentielConsultationClausesN2Query rightJoinCommonReferentielConsultationClausesN1($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonReferentielConsultationClausesN1 relation
 * @method CommonReferentielConsultationClausesN2Query innerJoinCommonReferentielConsultationClausesN1($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonReferentielConsultationClausesN1 relation
 *
 * @method CommonReferentielConsultationClausesN2Query leftJoinCommonConsultationClausesN2($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationClausesN2 relation
 * @method CommonReferentielConsultationClausesN2Query rightJoinCommonConsultationClausesN2($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationClausesN2 relation
 * @method CommonReferentielConsultationClausesN2Query innerJoinCommonConsultationClausesN2($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationClausesN2 relation
 *
 * @method CommonReferentielConsultationClausesN2Query leftJoinCommonReferentielConsultationClausesN2ClausesN3($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonReferentielConsultationClausesN2ClausesN3 relation
 * @method CommonReferentielConsultationClausesN2Query rightJoinCommonReferentielConsultationClausesN2ClausesN3($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonReferentielConsultationClausesN2ClausesN3 relation
 * @method CommonReferentielConsultationClausesN2Query innerJoinCommonReferentielConsultationClausesN2ClausesN3($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonReferentielConsultationClausesN2ClausesN3 relation
 *
 * @method CommonReferentielConsultationClausesN2 findOne(PropelPDO $con = null) Return the first CommonReferentielConsultationClausesN2 matching the query
 * @method CommonReferentielConsultationClausesN2 findOneOrCreate(PropelPDO $con = null) Return the first CommonReferentielConsultationClausesN2 matching the query, or a new CommonReferentielConsultationClausesN2 object populated from the query conditions when no match is found
 *
 * @method CommonReferentielConsultationClausesN2 findOneByClauseN1Id(int $clause_n1_id) Return the first CommonReferentielConsultationClausesN2 filtered by the clause_n1_id column
 * @method CommonReferentielConsultationClausesN2 findOneByLabel(string $label) Return the first CommonReferentielConsultationClausesN2 filtered by the label column
 * @method CommonReferentielConsultationClausesN2 findOneBySlug(string $slug) Return the first CommonReferentielConsultationClausesN2 filtered by the slug column
 *
 * @method array findById(int $id) Return CommonReferentielConsultationClausesN2 objects filtered by the id column
 * @method array findByClauseN1Id(int $clause_n1_id) Return CommonReferentielConsultationClausesN2 objects filtered by the clause_n1_id column
 * @method array findByLabel(string $label) Return CommonReferentielConsultationClausesN2 objects filtered by the label column
 * @method array findBySlug(string $slug) Return CommonReferentielConsultationClausesN2 objects filtered by the slug column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonReferentielConsultationClausesN2Query extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonReferentielConsultationClausesN2Query object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonReferentielConsultationClausesN2', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonReferentielConsultationClausesN2Query object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonReferentielConsultationClausesN2Query|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonReferentielConsultationClausesN2Query
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonReferentielConsultationClausesN2Query) {
            return $criteria;
        }
        $query = new CommonReferentielConsultationClausesN2Query();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonReferentielConsultationClausesN2|CommonReferentielConsultationClausesN2[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonReferentielConsultationClausesN2Peer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonReferentielConsultationClausesN2Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonReferentielConsultationClausesN2 A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonReferentielConsultationClausesN2 A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `clause_n1_id`, `label`, `slug` FROM `referentiel_consultation_clauses_n2` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonReferentielConsultationClausesN2();
            $obj->hydrate($row);
            CommonReferentielConsultationClausesN2Peer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonReferentielConsultationClausesN2|CommonReferentielConsultationClausesN2[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonReferentielConsultationClausesN2[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the clause_n1_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseN1Id(1234); // WHERE clause_n1_id = 1234
     * $query->filterByClauseN1Id(array(12, 34)); // WHERE clause_n1_id IN (12, 34)
     * $query->filterByClauseN1Id(array('min' => 12)); // WHERE clause_n1_id >= 12
     * $query->filterByClauseN1Id(array('max' => 12)); // WHERE clause_n1_id <= 12
     * </code>
     *
     * @see       filterByCommonReferentielConsultationClausesN1()
     *
     * @param     mixed $clauseN1Id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterByClauseN1Id($clauseN1Id = null, $comparison = null)
    {
        if (is_array($clauseN1Id)) {
            $useMinMax = false;
            if (isset($clauseN1Id['min'])) {
                $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::CLAUSE_N1_ID, $clauseN1Id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clauseN1Id['max'])) {
                $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::CLAUSE_N1_ID, $clauseN1Id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::CLAUSE_N1_ID, $clauseN1Id, $comparison);
    }

    /**
     * Filter the query on the label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE label = 'fooValue'
     * $query->filterByLabel('%fooValue%'); // WHERE label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $label)) {
                $label = str_replace('*', '%', $label);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%'); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $slug)) {
                $slug = str_replace('*', '%', $slug);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::SLUG, $slug, $comparison);
    }

    /**
     * Filter the query by a related CommonReferentielConsultationClausesN1 object
     *
     * @param   CommonReferentielConsultationClausesN1|PropelObjectCollection $commonReferentielConsultationClausesN1 The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonReferentielConsultationClausesN1($commonReferentielConsultationClausesN1, $comparison = null)
    {
        if ($commonReferentielConsultationClausesN1 instanceof CommonReferentielConsultationClausesN1) {
            return $this
                ->addUsingAlias(CommonReferentielConsultationClausesN2Peer::CLAUSE_N1_ID, $commonReferentielConsultationClausesN1->getId(), $comparison);
        } elseif ($commonReferentielConsultationClausesN1 instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonReferentielConsultationClausesN2Peer::CLAUSE_N1_ID, $commonReferentielConsultationClausesN1->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonReferentielConsultationClausesN1() only accepts arguments of type CommonReferentielConsultationClausesN1 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonReferentielConsultationClausesN1 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function joinCommonReferentielConsultationClausesN1($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonReferentielConsultationClausesN1');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonReferentielConsultationClausesN1');
        }

        return $this;
    }

    /**
     * Use the CommonReferentielConsultationClausesN1 relation CommonReferentielConsultationClausesN1 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonReferentielConsultationClausesN1Query A secondary query class using the current class as primary query
     */
    public function useCommonReferentielConsultationClausesN1Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonReferentielConsultationClausesN1($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonReferentielConsultationClausesN1', '\Application\Propel\Mpe\CommonReferentielConsultationClausesN1Query');
    }

    /**
     * Filter the query by a related CommonConsultationClausesN2 object
     *
     * @param   CommonConsultationClausesN2|PropelObjectCollection $commonConsultationClausesN2  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationClausesN2($commonConsultationClausesN2, $comparison = null)
    {
        if ($commonConsultationClausesN2 instanceof CommonConsultationClausesN2) {
            return $this
                ->addUsingAlias(CommonReferentielConsultationClausesN2Peer::ID, $commonConsultationClausesN2->getReferentielClauseN2Id(), $comparison);
        } elseif ($commonConsultationClausesN2 instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationClausesN2Query()
                ->filterByPrimaryKeys($commonConsultationClausesN2->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultationClausesN2() only accepts arguments of type CommonConsultationClausesN2 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationClausesN2 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function joinCommonConsultationClausesN2($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationClausesN2');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationClausesN2');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationClausesN2 relation CommonConsultationClausesN2 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationClausesN2Query A secondary query class using the current class as primary query
     */
    public function useCommonConsultationClausesN2Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationClausesN2($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationClausesN2', '\Application\Propel\Mpe\CommonConsultationClausesN2Query');
    }

    /**
     * Filter the query by a related CommonReferentielConsultationClausesN2ClausesN3 object
     *
     * @param   CommonReferentielConsultationClausesN2ClausesN3|PropelObjectCollection $commonReferentielConsultationClausesN2ClausesN3  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonReferentielConsultationClausesN2ClausesN3($commonReferentielConsultationClausesN2ClausesN3, $comparison = null)
    {
        if ($commonReferentielConsultationClausesN2ClausesN3 instanceof CommonReferentielConsultationClausesN2ClausesN3) {
            return $this
                ->addUsingAlias(CommonReferentielConsultationClausesN2Peer::ID, $commonReferentielConsultationClausesN2ClausesN3->getClausesN2Id(), $comparison);
        } elseif ($commonReferentielConsultationClausesN2ClausesN3 instanceof PropelObjectCollection) {
            return $this
                ->useCommonReferentielConsultationClausesN2ClausesN3Query()
                ->filterByPrimaryKeys($commonReferentielConsultationClausesN2ClausesN3->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonReferentielConsultationClausesN2ClausesN3() only accepts arguments of type CommonReferentielConsultationClausesN2ClausesN3 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonReferentielConsultationClausesN2ClausesN3 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function joinCommonReferentielConsultationClausesN2ClausesN3($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonReferentielConsultationClausesN2ClausesN3');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonReferentielConsultationClausesN2ClausesN3');
        }

        return $this;
    }

    /**
     * Use the CommonReferentielConsultationClausesN2ClausesN3 relation CommonReferentielConsultationClausesN2ClausesN3 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonReferentielConsultationClausesN2ClausesN3Query A secondary query class using the current class as primary query
     */
    public function useCommonReferentielConsultationClausesN2ClausesN3Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonReferentielConsultationClausesN2ClausesN3($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonReferentielConsultationClausesN2ClausesN3', '\Application\Propel\Mpe\CommonReferentielConsultationClausesN2ClausesN3Query');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonReferentielConsultationClausesN2 $commonReferentielConsultationClausesN2 Object to remove from the list of results
     *
     * @return CommonReferentielConsultationClausesN2Query The current query, for fluid interface
     */
    public function prune($commonReferentielConsultationClausesN2 = null)
    {
        if ($commonReferentielConsultationClausesN2) {
            $this->addUsingAlias(CommonReferentielConsultationClausesN2Peer::ID, $commonReferentielConsultationClausesN2->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
