<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratPeer;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonTechniqueAchat;
use Application\Propel\Mpe\CommonTechniqueAchatQuery;

/**
 * Base class that represents a row from the 't_type_contrat' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTypeContrat extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTTypeContratPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTTypeContratPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_type_contrat field.
     * @var        int
     */
    protected $id_type_contrat;

    /**
     * The value for the libelle_type_contrat field.
     * @var        string
     */
    protected $libelle_type_contrat;

    /**
     * The value for the abreviation_type_contrat field.
     * @var        string
     */
    protected $abreviation_type_contrat;

    /**
     * The value for the type_contrat_statistique field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $type_contrat_statistique;

    /**
     * The value for the multi field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $multi;

    /**
     * The value for the accord_cadre_sad field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $accord_cadre_sad;

    /**
     * The value for the avec_chapeau field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $avec_chapeau;

    /**
     * The value for the avec_montant field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $avec_montant;

    /**
     * The value for the mode_echange_chorus field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $mode_echange_chorus;

    /**
     * The value for the marche_subsequent field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $marche_subsequent;

    /**
     * The value for the avec_montant_max field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $avec_montant_max;

    /**
     * The value for the ordre_affichage field.
     * @var        int
     */
    protected $ordre_affichage;

    /**
     * The value for the article_133 field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $article_133;

    /**
     * The value for the code_dume field.
     * @var        string
     */
    protected $code_dume;

    /**
     * The value for the concession field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $concession;

    /**
     * The value for the id_externe field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $id_externe;

    /**
     * The value for the technique_achat field.
     * @var        int
     */
    protected $technique_achat;

    /**
     * @var        CommonTechniqueAchat
     */
    protected $aCommonTechniqueAchat;

    /**
     * @var        PropelObjectCollection|CommonConsultation[] Collection to store aggregation of CommonConsultation objects.
     */
    protected $collCommonConsultations;
    protected $collCommonConsultationsPartial;

    /**
     * @var        PropelObjectCollection|CommonTContratTitulaire[] Collection to store aggregation of CommonTContratTitulaire objects.
     */
    protected $collCommonTContratTitulaires;
    protected $collCommonTContratTitulairesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTContratTitulairesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->type_contrat_statistique = 0;
        $this->multi = '0';
        $this->accord_cadre_sad = '0';
        $this->avec_chapeau = '0';
        $this->avec_montant = '0';
        $this->mode_echange_chorus = '0';
        $this->marche_subsequent = '0';
        $this->avec_montant_max = '0';
        $this->article_133 = '0';
        $this->concession = false;
        $this->id_externe = '';
    }

    /**
     * Initializes internal state of BaseCommonTTypeContrat object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_type_contrat] column value.
     *
     * @return int
     */
    public function getIdTypeContrat()
    {

        return $this->id_type_contrat;
    }

    /**
     * Get the [libelle_type_contrat] column value.
     *
     * @return string
     */
    public function getLibelleTypeContrat()
    {

        return $this->libelle_type_contrat;
    }

    /**
     * Get the [abreviation_type_contrat] column value.
     *
     * @return string
     */
    public function getAbreviationTypeContrat()
    {

        return $this->abreviation_type_contrat;
    }

    /**
     * Get the [type_contrat_statistique] column value.
     *
     * @return int
     */
    public function getTypeContratStatistique()
    {

        return $this->type_contrat_statistique;
    }

    /**
     * Get the [multi] column value.
     *
     * @return string
     */
    public function getMulti()
    {

        return $this->multi;
    }

    /**
     * Get the [accord_cadre_sad] column value.
     *
     * @return string
     */
    public function getAccordCadreSad()
    {

        return $this->accord_cadre_sad;
    }

    /**
     * Get the [avec_chapeau] column value.
     *
     * @return string
     */
    public function getAvecChapeau()
    {

        return $this->avec_chapeau;
    }

    /**
     * Get the [avec_montant] column value.
     *
     * @return string
     */
    public function getAvecMontant()
    {

        return $this->avec_montant;
    }

    /**
     * Get the [mode_echange_chorus] column value.
     *
     * @return string
     */
    public function getModeEchangeChorus()
    {

        return $this->mode_echange_chorus;
    }

    /**
     * Get the [marche_subsequent] column value.
     *
     * @return string
     */
    public function getMarcheSubsequent()
    {

        return $this->marche_subsequent;
    }

    /**
     * Get the [avec_montant_max] column value.
     *
     * @return string
     */
    public function getAvecMontantMax()
    {

        return $this->avec_montant_max;
    }

    /**
     * Get the [ordre_affichage] column value.
     *
     * @return int
     */
    public function getOrdreAffichage()
    {

        return $this->ordre_affichage;
    }

    /**
     * Get the [article_133] column value.
     *
     * @return string
     */
    public function getArticle133()
    {

        return $this->article_133;
    }

    /**
     * Get the [code_dume] column value.
     *
     * @return string
     */
    public function getCodeDume()
    {

        return $this->code_dume;
    }

    /**
     * Get the [concession] column value.
     *
     * @return boolean
     */
    public function getConcession()
    {

        return $this->concession;
    }

    /**
     * Get the [id_externe] column value.
     *
     * @return string
     */
    public function getIdExterne()
    {

        return $this->id_externe;
    }

    /**
     * Get the [technique_achat] column value.
     *
     * @return int
     */
    public function getTechniqueAchat()
    {

        return $this->technique_achat;
    }

    /**
     * Set the value of [id_type_contrat] column.
     *
     * @param int $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setIdTypeContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_contrat !== $v) {
            $this->id_type_contrat = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::ID_TYPE_CONTRAT;
        }


        return $this;
    } // setIdTypeContrat()

    /**
     * Set the value of [libelle_type_contrat] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setLibelleTypeContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_type_contrat !== $v) {
            $this->libelle_type_contrat = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::LIBELLE_TYPE_CONTRAT;
        }


        return $this;
    } // setLibelleTypeContrat()

    /**
     * Set the value of [abreviation_type_contrat] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setAbreviationTypeContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->abreviation_type_contrat !== $v) {
            $this->abreviation_type_contrat = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::ABREVIATION_TYPE_CONTRAT;
        }


        return $this;
    } // setAbreviationTypeContrat()

    /**
     * Set the value of [type_contrat_statistique] column.
     *
     * @param int $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setTypeContratStatistique($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->type_contrat_statistique !== $v) {
            $this->type_contrat_statistique = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE;
        }


        return $this;
    } // setTypeContratStatistique()

    /**
     * Set the value of [multi] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setMulti($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->multi !== $v) {
            $this->multi = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::MULTI;
        }


        return $this;
    } // setMulti()

    /**
     * Set the value of [accord_cadre_sad] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setAccordCadreSad($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->accord_cadre_sad !== $v) {
            $this->accord_cadre_sad = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::ACCORD_CADRE_SAD;
        }


        return $this;
    } // setAccordCadreSad()

    /**
     * Set the value of [avec_chapeau] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setAvecChapeau($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->avec_chapeau !== $v) {
            $this->avec_chapeau = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::AVEC_CHAPEAU;
        }


        return $this;
    } // setAvecChapeau()

    /**
     * Set the value of [avec_montant] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setAvecMontant($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->avec_montant !== $v) {
            $this->avec_montant = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::AVEC_MONTANT;
        }


        return $this;
    } // setAvecMontant()

    /**
     * Set the value of [mode_echange_chorus] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setModeEchangeChorus($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mode_echange_chorus !== $v) {
            $this->mode_echange_chorus = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::MODE_ECHANGE_CHORUS;
        }


        return $this;
    } // setModeEchangeChorus()

    /**
     * Set the value of [marche_subsequent] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setMarcheSubsequent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->marche_subsequent !== $v) {
            $this->marche_subsequent = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::MARCHE_SUBSEQUENT;
        }


        return $this;
    } // setMarcheSubsequent()

    /**
     * Set the value of [avec_montant_max] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setAvecMontantMax($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->avec_montant_max !== $v) {
            $this->avec_montant_max = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::AVEC_MONTANT_MAX;
        }


        return $this;
    } // setAvecMontantMax()

    /**
     * Set the value of [ordre_affichage] column.
     *
     * @param int $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setOrdreAffichage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ordre_affichage !== $v) {
            $this->ordre_affichage = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::ORDRE_AFFICHAGE;
        }


        return $this;
    } // setOrdreAffichage()

    /**
     * Set the value of [article_133] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setArticle133($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->article_133 !== $v) {
            $this->article_133 = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::ARTICLE_133;
        }


        return $this;
    } // setArticle133()

    /**
     * Set the value of [code_dume] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setCodeDume($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_dume !== $v) {
            $this->code_dume = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::CODE_DUME;
        }


        return $this;
    } // setCodeDume()

    /**
     * Sets the value of the [concession] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setConcession($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->concession !== $v) {
            $this->concession = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::CONCESSION;
        }


        return $this;
    } // setConcession()

    /**
     * Set the value of [id_externe] column.
     *
     * @param string $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setIdExterne($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_externe !== $v) {
            $this->id_externe = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::ID_EXTERNE;
        }


        return $this;
    } // setIdExterne()

    /**
     * Set the value of [technique_achat] column.
     *
     * @param int $v new value
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setTechniqueAchat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->technique_achat !== $v) {
            $this->technique_achat = $v;
            $this->modifiedColumns[] = CommonTTypeContratPeer::TECHNIQUE_ACHAT;
        }

        if ($this->aCommonTechniqueAchat !== null && $this->aCommonTechniqueAchat->getIdTechniqueAchat() !== $v) {
            $this->aCommonTechniqueAchat = null;
        }


        return $this;
    } // setTechniqueAchat()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->type_contrat_statistique !== 0) {
                return false;
            }

            if ($this->multi !== '0') {
                return false;
            }

            if ($this->accord_cadre_sad !== '0') {
                return false;
            }

            if ($this->avec_chapeau !== '0') {
                return false;
            }

            if ($this->avec_montant !== '0') {
                return false;
            }

            if ($this->mode_echange_chorus !== '0') {
                return false;
            }

            if ($this->marche_subsequent !== '0') {
                return false;
            }

            if ($this->avec_montant_max !== '0') {
                return false;
            }

            if ($this->article_133 !== '0') {
                return false;
            }

            if ($this->concession !== false) {
                return false;
            }

            if ($this->id_externe !== '') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_type_contrat = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->libelle_type_contrat = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->abreviation_type_contrat = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->type_contrat_statistique = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->multi = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->accord_cadre_sad = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->avec_chapeau = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->avec_montant = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->mode_echange_chorus = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->marche_subsequent = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->avec_montant_max = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->ordre_affichage = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->article_133 = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->code_dume = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->concession = ($row[$startcol + 14] !== null) ? (boolean) $row[$startcol + 14] : null;
            $this->id_externe = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->technique_achat = ($row[$startcol + 16] !== null) ? (int) $row[$startcol + 16] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 17; // 17 = CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTTypeContrat object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTechniqueAchat !== null && $this->technique_achat !== $this->aCommonTechniqueAchat->getIdTechniqueAchat()) {
            $this->aCommonTechniqueAchat = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTTypeContratPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTechniqueAchat = null;
            $this->collCommonConsultations = null;

            $this->collCommonTContratTitulaires = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTTypeContratQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTTypeContratPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTechniqueAchat !== null) {
                if ($this->aCommonTechniqueAchat->isModified() || $this->aCommonTechniqueAchat->isNew()) {
                    $affectedRows += $this->aCommonTechniqueAchat->save($con);
                }
                $this->setCommonTechniqueAchat($this->aCommonTechniqueAchat);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonConsultationsScheduledForDeletion !== null) {
                if (!$this->commonConsultationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationsScheduledForDeletion as $commonConsultation) {
                        // need to save related object because we set the relation to null
                        $commonConsultation->save($con);
                    }
                    $this->commonConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultations !== null) {
                foreach ($this->collCommonConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTContratTitulairesScheduledForDeletion !== null) {
                if (!$this->commonTContratTitulairesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTContratTitulairesScheduledForDeletion as $commonTContratTitulaire) {
                        // need to save related object because we set the relation to null
                        $commonTContratTitulaire->save($con);
                    }
                    $this->commonTContratTitulairesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTContratTitulaires !== null) {
                foreach ($this->collCommonTContratTitulaires as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTTypeContratPeer::ID_TYPE_CONTRAT;
        if (null !== $this->id_type_contrat) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTTypeContratPeer::ID_TYPE_CONTRAT . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTTypeContratPeer::ID_TYPE_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_contrat`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::LIBELLE_TYPE_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_type_contrat`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::ABREVIATION_TYPE_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`abreviation_type_contrat`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`type_contrat_statistique`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::MULTI)) {
            $modifiedColumns[':p' . $index++]  = '`multi`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::ACCORD_CADRE_SAD)) {
            $modifiedColumns[':p' . $index++]  = '`accord_cadre_sad`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::AVEC_CHAPEAU)) {
            $modifiedColumns[':p' . $index++]  = '`avec_chapeau`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::AVEC_MONTANT)) {
            $modifiedColumns[':p' . $index++]  = '`avec_montant`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::MODE_ECHANGE_CHORUS)) {
            $modifiedColumns[':p' . $index++]  = '`mode_echange_chorus`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::MARCHE_SUBSEQUENT)) {
            $modifiedColumns[':p' . $index++]  = '`marche_subsequent`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::AVEC_MONTANT_MAX)) {
            $modifiedColumns[':p' . $index++]  = '`avec_montant_max`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::ORDRE_AFFICHAGE)) {
            $modifiedColumns[':p' . $index++]  = '`ordre_affichage`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::ARTICLE_133)) {
            $modifiedColumns[':p' . $index++]  = '`article_133`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::CODE_DUME)) {
            $modifiedColumns[':p' . $index++]  = '`code_dume`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::CONCESSION)) {
            $modifiedColumns[':p' . $index++]  = '`concession`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::ID_EXTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`id_externe`';
        }
        if ($this->isColumnModified(CommonTTypeContratPeer::TECHNIQUE_ACHAT)) {
            $modifiedColumns[':p' . $index++]  = '`technique_achat`';
        }

        $sql = sprintf(
            'INSERT INTO `t_type_contrat` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_type_contrat`':
                        $stmt->bindValue($identifier, $this->id_type_contrat, PDO::PARAM_INT);
                        break;
                    case '`libelle_type_contrat`':
                        $stmt->bindValue($identifier, $this->libelle_type_contrat, PDO::PARAM_STR);
                        break;
                    case '`abreviation_type_contrat`':
                        $stmt->bindValue($identifier, $this->abreviation_type_contrat, PDO::PARAM_STR);
                        break;
                    case '`type_contrat_statistique`':
                        $stmt->bindValue($identifier, $this->type_contrat_statistique, PDO::PARAM_INT);
                        break;
                    case '`multi`':
                        $stmt->bindValue($identifier, $this->multi, PDO::PARAM_STR);
                        break;
                    case '`accord_cadre_sad`':
                        $stmt->bindValue($identifier, $this->accord_cadre_sad, PDO::PARAM_STR);
                        break;
                    case '`avec_chapeau`':
                        $stmt->bindValue($identifier, $this->avec_chapeau, PDO::PARAM_STR);
                        break;
                    case '`avec_montant`':
                        $stmt->bindValue($identifier, $this->avec_montant, PDO::PARAM_STR);
                        break;
                    case '`mode_echange_chorus`':
                        $stmt->bindValue($identifier, $this->mode_echange_chorus, PDO::PARAM_STR);
                        break;
                    case '`marche_subsequent`':
                        $stmt->bindValue($identifier, $this->marche_subsequent, PDO::PARAM_STR);
                        break;
                    case '`avec_montant_max`':
                        $stmt->bindValue($identifier, $this->avec_montant_max, PDO::PARAM_STR);
                        break;
                    case '`ordre_affichage`':
                        $stmt->bindValue($identifier, $this->ordre_affichage, PDO::PARAM_INT);
                        break;
                    case '`article_133`':
                        $stmt->bindValue($identifier, $this->article_133, PDO::PARAM_STR);
                        break;
                    case '`code_dume`':
                        $stmt->bindValue($identifier, $this->code_dume, PDO::PARAM_STR);
                        break;
                    case '`concession`':
                        $stmt->bindValue($identifier, (int) $this->concession, PDO::PARAM_INT);
                        break;
                    case '`id_externe`':
                        $stmt->bindValue($identifier, $this->id_externe, PDO::PARAM_STR);
                        break;
                    case '`technique_achat`':
                        $stmt->bindValue($identifier, $this->technique_achat, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdTypeContrat($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTechniqueAchat !== null) {
                if (!$this->aCommonTechniqueAchat->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTechniqueAchat->getValidationFailures());
                }
            }


            if (($retval = CommonTTypeContratPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonConsultations !== null) {
                    foreach ($this->collCommonConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTContratTitulaires !== null) {
                    foreach ($this->collCommonTContratTitulaires as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTTypeContratPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdTypeContrat();
                break;
            case 1:
                return $this->getLibelleTypeContrat();
                break;
            case 2:
                return $this->getAbreviationTypeContrat();
                break;
            case 3:
                return $this->getTypeContratStatistique();
                break;
            case 4:
                return $this->getMulti();
                break;
            case 5:
                return $this->getAccordCadreSad();
                break;
            case 6:
                return $this->getAvecChapeau();
                break;
            case 7:
                return $this->getAvecMontant();
                break;
            case 8:
                return $this->getModeEchangeChorus();
                break;
            case 9:
                return $this->getMarcheSubsequent();
                break;
            case 10:
                return $this->getAvecMontantMax();
                break;
            case 11:
                return $this->getOrdreAffichage();
                break;
            case 12:
                return $this->getArticle133();
                break;
            case 13:
                return $this->getCodeDume();
                break;
            case 14:
                return $this->getConcession();
                break;
            case 15:
                return $this->getIdExterne();
                break;
            case 16:
                return $this->getTechniqueAchat();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTTypeContrat'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTTypeContrat'][$this->getPrimaryKey()] = true;
        $keys = CommonTTypeContratPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdTypeContrat(),
            $keys[1] => $this->getLibelleTypeContrat(),
            $keys[2] => $this->getAbreviationTypeContrat(),
            $keys[3] => $this->getTypeContratStatistique(),
            $keys[4] => $this->getMulti(),
            $keys[5] => $this->getAccordCadreSad(),
            $keys[6] => $this->getAvecChapeau(),
            $keys[7] => $this->getAvecMontant(),
            $keys[8] => $this->getModeEchangeChorus(),
            $keys[9] => $this->getMarcheSubsequent(),
            $keys[10] => $this->getAvecMontantMax(),
            $keys[11] => $this->getOrdreAffichage(),
            $keys[12] => $this->getArticle133(),
            $keys[13] => $this->getCodeDume(),
            $keys[14] => $this->getConcession(),
            $keys[15] => $this->getIdExterne(),
            $keys[16] => $this->getTechniqueAchat(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTechniqueAchat) {
                $result['CommonTechniqueAchat'] = $this->aCommonTechniqueAchat->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonConsultations) {
                $result['CommonConsultations'] = $this->collCommonConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTContratTitulaires) {
                $result['CommonTContratTitulaires'] = $this->collCommonTContratTitulaires->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTTypeContratPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdTypeContrat($value);
                break;
            case 1:
                $this->setLibelleTypeContrat($value);
                break;
            case 2:
                $this->setAbreviationTypeContrat($value);
                break;
            case 3:
                $this->setTypeContratStatistique($value);
                break;
            case 4:
                $this->setMulti($value);
                break;
            case 5:
                $this->setAccordCadreSad($value);
                break;
            case 6:
                $this->setAvecChapeau($value);
                break;
            case 7:
                $this->setAvecMontant($value);
                break;
            case 8:
                $this->setModeEchangeChorus($value);
                break;
            case 9:
                $this->setMarcheSubsequent($value);
                break;
            case 10:
                $this->setAvecMontantMax($value);
                break;
            case 11:
                $this->setOrdreAffichage($value);
                break;
            case 12:
                $this->setArticle133($value);
                break;
            case 13:
                $this->setCodeDume($value);
                break;
            case 14:
                $this->setConcession($value);
                break;
            case 15:
                $this->setIdExterne($value);
                break;
            case 16:
                $this->setTechniqueAchat($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTTypeContratPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdTypeContrat($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setLibelleTypeContrat($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setAbreviationTypeContrat($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTypeContratStatistique($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMulti($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setAccordCadreSad($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAvecChapeau($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setAvecMontant($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setModeEchangeChorus($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setMarcheSubsequent($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setAvecMontantMax($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setOrdreAffichage($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setArticle133($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setCodeDume($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setConcession($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setIdExterne($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setTechniqueAchat($arr[$keys[16]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTTypeContratPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTTypeContratPeer::ID_TYPE_CONTRAT)) $criteria->add(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $this->id_type_contrat);
        if ($this->isColumnModified(CommonTTypeContratPeer::LIBELLE_TYPE_CONTRAT)) $criteria->add(CommonTTypeContratPeer::LIBELLE_TYPE_CONTRAT, $this->libelle_type_contrat);
        if ($this->isColumnModified(CommonTTypeContratPeer::ABREVIATION_TYPE_CONTRAT)) $criteria->add(CommonTTypeContratPeer::ABREVIATION_TYPE_CONTRAT, $this->abreviation_type_contrat);
        if ($this->isColumnModified(CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE)) $criteria->add(CommonTTypeContratPeer::TYPE_CONTRAT_STATISTIQUE, $this->type_contrat_statistique);
        if ($this->isColumnModified(CommonTTypeContratPeer::MULTI)) $criteria->add(CommonTTypeContratPeer::MULTI, $this->multi);
        if ($this->isColumnModified(CommonTTypeContratPeer::ACCORD_CADRE_SAD)) $criteria->add(CommonTTypeContratPeer::ACCORD_CADRE_SAD, $this->accord_cadre_sad);
        if ($this->isColumnModified(CommonTTypeContratPeer::AVEC_CHAPEAU)) $criteria->add(CommonTTypeContratPeer::AVEC_CHAPEAU, $this->avec_chapeau);
        if ($this->isColumnModified(CommonTTypeContratPeer::AVEC_MONTANT)) $criteria->add(CommonTTypeContratPeer::AVEC_MONTANT, $this->avec_montant);
        if ($this->isColumnModified(CommonTTypeContratPeer::MODE_ECHANGE_CHORUS)) $criteria->add(CommonTTypeContratPeer::MODE_ECHANGE_CHORUS, $this->mode_echange_chorus);
        if ($this->isColumnModified(CommonTTypeContratPeer::MARCHE_SUBSEQUENT)) $criteria->add(CommonTTypeContratPeer::MARCHE_SUBSEQUENT, $this->marche_subsequent);
        if ($this->isColumnModified(CommonTTypeContratPeer::AVEC_MONTANT_MAX)) $criteria->add(CommonTTypeContratPeer::AVEC_MONTANT_MAX, $this->avec_montant_max);
        if ($this->isColumnModified(CommonTTypeContratPeer::ORDRE_AFFICHAGE)) $criteria->add(CommonTTypeContratPeer::ORDRE_AFFICHAGE, $this->ordre_affichage);
        if ($this->isColumnModified(CommonTTypeContratPeer::ARTICLE_133)) $criteria->add(CommonTTypeContratPeer::ARTICLE_133, $this->article_133);
        if ($this->isColumnModified(CommonTTypeContratPeer::CODE_DUME)) $criteria->add(CommonTTypeContratPeer::CODE_DUME, $this->code_dume);
        if ($this->isColumnModified(CommonTTypeContratPeer::CONCESSION)) $criteria->add(CommonTTypeContratPeer::CONCESSION, $this->concession);
        if ($this->isColumnModified(CommonTTypeContratPeer::ID_EXTERNE)) $criteria->add(CommonTTypeContratPeer::ID_EXTERNE, $this->id_externe);
        if ($this->isColumnModified(CommonTTypeContratPeer::TECHNIQUE_ACHAT)) $criteria->add(CommonTTypeContratPeer::TECHNIQUE_ACHAT, $this->technique_achat);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTTypeContratPeer::DATABASE_NAME);
        $criteria->add(CommonTTypeContratPeer::ID_TYPE_CONTRAT, $this->id_type_contrat);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdTypeContrat();
    }

    /**
     * Generic method to set the primary key (id_type_contrat column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdTypeContrat($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdTypeContrat();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTTypeContrat (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLibelleTypeContrat($this->getLibelleTypeContrat());
        $copyObj->setAbreviationTypeContrat($this->getAbreviationTypeContrat());
        $copyObj->setTypeContratStatistique($this->getTypeContratStatistique());
        $copyObj->setMulti($this->getMulti());
        $copyObj->setAccordCadreSad($this->getAccordCadreSad());
        $copyObj->setAvecChapeau($this->getAvecChapeau());
        $copyObj->setAvecMontant($this->getAvecMontant());
        $copyObj->setModeEchangeChorus($this->getModeEchangeChorus());
        $copyObj->setMarcheSubsequent($this->getMarcheSubsequent());
        $copyObj->setAvecMontantMax($this->getAvecMontantMax());
        $copyObj->setOrdreAffichage($this->getOrdreAffichage());
        $copyObj->setArticle133($this->getArticle133());
        $copyObj->setCodeDume($this->getCodeDume());
        $copyObj->setConcession($this->getConcession());
        $copyObj->setIdExterne($this->getIdExterne());
        $copyObj->setTechniqueAchat($this->getTechniqueAchat());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTContratTitulaires() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTContratTitulaire($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdTypeContrat(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTTypeContrat Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTTypeContratPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTTypeContratPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonTechniqueAchat object.
     *
     * @param   CommonTechniqueAchat $v
     * @return CommonTTypeContrat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTechniqueAchat(CommonTechniqueAchat $v = null)
    {
        if ($v === null) {
            $this->setTechniqueAchat(NULL);
        } else {
            $this->setTechniqueAchat($v->getIdTechniqueAchat());
        }

        $this->aCommonTechniqueAchat = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTechniqueAchat object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTTypeContrat($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTechniqueAchat object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTechniqueAchat The associated CommonTechniqueAchat object.
     * @throws PropelException
     */
    public function getCommonTechniqueAchat(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTechniqueAchat === null && ($this->technique_achat !== null) && $doQuery) {
            $this->aCommonTechniqueAchat = CommonTechniqueAchatQuery::create()->findPk($this->technique_achat, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTechniqueAchat->addCommonTTypeContrats($this);
             */
        }

        return $this->aCommonTechniqueAchat;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonConsultation' == $relationName) {
            $this->initCommonConsultations();
        }
        if ('CommonTContratTitulaire' == $relationName) {
            $this->initCommonTContratTitulaires();
        }
    }

    /**
     * Clears out the collCommonConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTTypeContrat The current object (for fluent API support)
     * @see        addCommonConsultations()
     */
    public function clearCommonConsultations()
    {
        $this->collCommonConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultations($v = true)
    {
        $this->collCommonConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonConsultations collection.
     *
     * By default this just sets the collCommonConsultations collection to an empty array (like clearcollCommonConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultations = new PropelObjectCollection();
        $this->collCommonConsultations->setModel('CommonConsultation');
    }

    /**
     * Gets an array of CommonConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTTypeContrat is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     * @throws PropelException
     */
    public function getCommonConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultations) {
                // return empty collection
                $this->initCommonConsultations();
            } else {
                $collCommonConsultations = CommonConsultationQuery::create(null, $criteria)
                    ->filterByCommonTTypeContrat($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationsPartial && count($collCommonConsultations)) {
                      $this->initCommonConsultations(false);

                      foreach ($collCommonConsultations as $obj) {
                        if (false == $this->collCommonConsultations->contains($obj)) {
                          $this->collCommonConsultations->append($obj);
                        }
                      }

                      $this->collCommonConsultationsPartial = true;
                    }

                    $collCommonConsultations->getInternalIterator()->rewind();

                    return $collCommonConsultations;
                }

                if ($partial && $this->collCommonConsultations) {
                    foreach ($this->collCommonConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultations = $collCommonConsultations;
                $this->collCommonConsultationsPartial = false;
            }
        }

        return $this->collCommonConsultations;
    }

    /**
     * Sets a collection of CommonConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setCommonConsultations(PropelCollection $commonConsultations, PropelPDO $con = null)
    {
        $commonConsultationsToDelete = $this->getCommonConsultations(new Criteria(), $con)->diff($commonConsultations);


        $this->commonConsultationsScheduledForDeletion = $commonConsultationsToDelete;

        foreach ($commonConsultationsToDelete as $commonConsultationRemoved) {
            $commonConsultationRemoved->setCommonTTypeContrat(null);
        }

        $this->collCommonConsultations = null;
        foreach ($commonConsultations as $commonConsultation) {
            $this->addCommonConsultation($commonConsultation);
        }

        $this->collCommonConsultations = $commonConsultations;
        $this->collCommonConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultation objects.
     * @throws PropelException
     */
    public function countCommonConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultations());
            }
            $query = CommonConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTTypeContrat($this)
                ->count($con);
        }

        return count($this->collCommonConsultations);
    }

    /**
     * Method called to associate a CommonConsultation object to this object
     * through the CommonConsultation foreign key attribute.
     *
     * @param   CommonConsultation $l CommonConsultation
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function addCommonConsultation(CommonConsultation $l)
    {
        if ($this->collCommonConsultations === null) {
            $this->initCommonConsultations();
            $this->collCommonConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultation $commonConsultation The commonConsultation object to add.
     */
    protected function doAddCommonConsultation($commonConsultation)
    {
        $this->collCommonConsultations[]= $commonConsultation;
        $commonConsultation->setCommonTTypeContrat($this);
    }

    /**
     * @param	CommonConsultation $commonConsultation The commonConsultation object to remove.
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function removeCommonConsultation($commonConsultation)
    {
        if ($this->getCommonConsultations()->contains($commonConsultation)) {
            $this->collCommonConsultations->remove($this->collCommonConsultations->search($commonConsultation));
            if (null === $this->commonConsultationsScheduledForDeletion) {
                $this->commonConsultationsScheduledForDeletion = clone $this->collCommonConsultations;
                $this->commonConsultationsScheduledForDeletion->clear();
            }
            $this->commonConsultationsScheduledForDeletion[]= $commonConsultation;
            $commonConsultation->setCommonTTypeContrat(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceValidation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceValidation', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceId', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonConsultationRelatedByReferenceConsultationInit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonConsultationRelatedByReferenceConsultationInit', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceValidationIntermediaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceValidationIntermediaire', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonOperations($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOperations', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonDossierVolumineux($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonDossierVolumineux', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }

    /**
     * Clears out the collCommonTContratTitulaires collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTTypeContrat The current object (for fluent API support)
     * @see        addCommonTContratTitulaires()
     */
    public function clearCommonTContratTitulaires()
    {
        $this->collCommonTContratTitulaires = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTContratTitulairesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTContratTitulaires collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTContratTitulaires($v = true)
    {
        $this->collCommonTContratTitulairesPartial = $v;
    }

    /**
     * Initializes the collCommonTContratTitulaires collection.
     *
     * By default this just sets the collCommonTContratTitulaires collection to an empty array (like clearcollCommonTContratTitulaires());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTContratTitulaires($overrideExisting = true)
    {
        if (null !== $this->collCommonTContratTitulaires && !$overrideExisting) {
            return;
        }
        $this->collCommonTContratTitulaires = new PropelObjectCollection();
        $this->collCommonTContratTitulaires->setModel('CommonTContratTitulaire');
    }

    /**
     * Gets an array of CommonTContratTitulaire objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTTypeContrat is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     * @throws PropelException
     */
    public function getCommonTContratTitulaires($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTContratTitulairesPartial && !$this->isNew();
        if (null === $this->collCommonTContratTitulaires || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTContratTitulaires) {
                // return empty collection
                $this->initCommonTContratTitulaires();
            } else {
                $collCommonTContratTitulaires = CommonTContratTitulaireQuery::create(null, $criteria)
                    ->filterByCommonTTypeContrat($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTContratTitulairesPartial && count($collCommonTContratTitulaires)) {
                      $this->initCommonTContratTitulaires(false);

                      foreach ($collCommonTContratTitulaires as $obj) {
                        if (false == $this->collCommonTContratTitulaires->contains($obj)) {
                          $this->collCommonTContratTitulaires->append($obj);
                        }
                      }

                      $this->collCommonTContratTitulairesPartial = true;
                    }

                    $collCommonTContratTitulaires->getInternalIterator()->rewind();

                    return $collCommonTContratTitulaires;
                }

                if ($partial && $this->collCommonTContratTitulaires) {
                    foreach ($this->collCommonTContratTitulaires as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTContratTitulaires[] = $obj;
                        }
                    }
                }

                $this->collCommonTContratTitulaires = $collCommonTContratTitulaires;
                $this->collCommonTContratTitulairesPartial = false;
            }
        }

        return $this->collCommonTContratTitulaires;
    }

    /**
     * Sets a collection of CommonTContratTitulaire objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTContratTitulaires A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function setCommonTContratTitulaires(PropelCollection $commonTContratTitulaires, PropelPDO $con = null)
    {
        $commonTContratTitulairesToDelete = $this->getCommonTContratTitulaires(new Criteria(), $con)->diff($commonTContratTitulaires);


        $this->commonTContratTitulairesScheduledForDeletion = $commonTContratTitulairesToDelete;

        foreach ($commonTContratTitulairesToDelete as $commonTContratTitulaireRemoved) {
            $commonTContratTitulaireRemoved->setCommonTTypeContrat(null);
        }

        $this->collCommonTContratTitulaires = null;
        foreach ($commonTContratTitulaires as $commonTContratTitulaire) {
            $this->addCommonTContratTitulaire($commonTContratTitulaire);
        }

        $this->collCommonTContratTitulaires = $commonTContratTitulaires;
        $this->collCommonTContratTitulairesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTContratTitulaire objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTContratTitulaire objects.
     * @throws PropelException
     */
    public function countCommonTContratTitulaires(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTContratTitulairesPartial && !$this->isNew();
        if (null === $this->collCommonTContratTitulaires || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTContratTitulaires) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTContratTitulaires());
            }
            $query = CommonTContratTitulaireQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTTypeContrat($this)
                ->count($con);
        }

        return count($this->collCommonTContratTitulaires);
    }

    /**
     * Method called to associate a BaseCommonTContratTitulaire object to this object
     * through the BaseCommonTContratTitulaire foreign key attribute.
     *
     * @param   BaseCommonTContratTitulaire $l BaseCommonTContratTitulaire
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function addCommonTContratTitulaire(BaseCommonTContratTitulaire $l)
    {
        if ($this->collCommonTContratTitulaires === null) {
            $this->initCommonTContratTitulaires();
            $this->collCommonTContratTitulairesPartial = true;
        }
        if (!in_array($l, $this->collCommonTContratTitulaires->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTContratTitulaire($l);
        }

        return $this;
    }

    /**
     * @param	CommonTContratTitulaire $commonTContratTitulaire The commonTContratTitulaire object to add.
     */
    protected function doAddCommonTContratTitulaire($commonTContratTitulaire)
    {
        $this->collCommonTContratTitulaires[]= $commonTContratTitulaire;
        $commonTContratTitulaire->setCommonTTypeContrat($this);
    }

    /**
     * @param	CommonTContratTitulaire $commonTContratTitulaire The commonTContratTitulaire object to remove.
     * @return CommonTTypeContrat The current object (for fluent API support)
     */
    public function removeCommonTContratTitulaire($commonTContratTitulaire)
    {
        if ($this->getCommonTContratTitulaires()->contains($commonTContratTitulaire)) {
            $this->collCommonTContratTitulaires->remove($this->collCommonTContratTitulaires->search($commonTContratTitulaire));
            if (null === $this->commonTContratTitulairesScheduledForDeletion) {
                $this->commonTContratTitulairesScheduledForDeletion = clone $this->collCommonTContratTitulaires;
                $this->commonTContratTitulairesScheduledForDeletion->clear();
            }
            $this->commonTContratTitulairesScheduledForDeletion[]= $commonTContratTitulaire;
            $commonTContratTitulaire->setCommonTTypeContrat(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTypeContratConcessionPivot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeContratConcessionPivot', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTypeContratPivot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeContratPivot', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTypeProcedureConcessionPivot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeProcedureConcessionPivot', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTContactContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTContactContrat', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTContratTitulaireRelatedByIdContratMulti($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTContratTitulaireRelatedByIdContratMulti', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTTypeContrat is new, it will return
     * an empty collection; or if this CommonTTypeContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTTypeContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTypeProcedure($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeProcedure', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_type_contrat = null;
        $this->libelle_type_contrat = null;
        $this->abreviation_type_contrat = null;
        $this->type_contrat_statistique = null;
        $this->multi = null;
        $this->accord_cadre_sad = null;
        $this->avec_chapeau = null;
        $this->avec_montant = null;
        $this->mode_echange_chorus = null;
        $this->marche_subsequent = null;
        $this->avec_montant_max = null;
        $this->ordre_affichage = null;
        $this->article_133 = null;
        $this->code_dume = null;
        $this->concession = null;
        $this->id_externe = null;
        $this->technique_achat = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonConsultations) {
                foreach ($this->collCommonConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTContratTitulaires) {
                foreach ($this->collCommonTContratTitulaires as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonTechniqueAchat instanceof Persistent) {
              $this->aCommonTechniqueAchat->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonConsultations instanceof PropelCollection) {
            $this->collCommonConsultations->clearIterator();
        }
        $this->collCommonConsultations = null;
        if ($this->collCommonTContratTitulaires instanceof PropelCollection) {
            $this->collCommonTContratTitulaires->clearIterator();
        }
        $this->collCommonTContratTitulaires = null;
        $this->aCommonTechniqueAchat = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTTypeContratPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
