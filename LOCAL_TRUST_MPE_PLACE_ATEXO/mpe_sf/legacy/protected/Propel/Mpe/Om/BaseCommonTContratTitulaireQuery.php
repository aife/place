<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcession;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonMarche;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTDonneesConsultation;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTypeContratConcessionPivot;
use Application\Propel\Mpe\CommonTypeContratPivot;
use Application\Propel\Mpe\CommonTypeProcedure;
use Application\Propel\Mpe\CommonTypeProcedureConcessionPivot;

/**
 * Base class that represents a query for the 't_contrat_titulaire' table.
 *
 *
 *
 * @method CommonTContratTitulaireQuery orderByIdContratTitulaire($order = Criteria::ASC) Order by the id_contrat_titulaire column
 * @method CommonTContratTitulaireQuery orderByIdTypeContrat($order = Criteria::ASC) Order by the id_type_contrat column
 * @method CommonTContratTitulaireQuery orderByIdContactContrat($order = Criteria::ASC) Order by the id_contact_contrat column
 * @method CommonTContratTitulaireQuery orderByNumeroContrat($order = Criteria::ASC) Order by the numero_contrat column
 * @method CommonTContratTitulaireQuery orderByLienAcSad($order = Criteria::ASC) Order by the lien_AC_SAD column
 * @method CommonTContratTitulaireQuery orderByIdContratMulti($order = Criteria::ASC) Order by the id_contrat_multi column
 * @method CommonTContratTitulaireQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTContratTitulaireQuery orderByOldServiceId($order = Criteria::ASC) Order by the old_service_id column
 * @method CommonTContratTitulaireQuery orderByIdTitulaire($order = Criteria::ASC) Order by the id_titulaire column
 * @method CommonTContratTitulaireQuery orderByIdTitulaireEtab($order = Criteria::ASC) Order by the id_titulaire_etab column
 * @method CommonTContratTitulaireQuery orderByIdOffre($order = Criteria::ASC) Order by the id_offre column
 * @method CommonTContratTitulaireQuery orderByTypeDepotReponse($order = Criteria::ASC) Order by the type_depot_reponse column
 * @method CommonTContratTitulaireQuery orderByObjetContrat($order = Criteria::ASC) Order by the objet_contrat column
 * @method CommonTContratTitulaireQuery orderByIntitule($order = Criteria::ASC) Order by the intitule column
 * @method CommonTContratTitulaireQuery orderByMontantContrat($order = Criteria::ASC) Order by the montant_contrat column
 * @method CommonTContratTitulaireQuery orderByIdTrancheBudgetaire($order = Criteria::ASC) Order by the id_tranche_budgetaire column
 * @method CommonTContratTitulaireQuery orderByMontantMaxEstime($order = Criteria::ASC) Order by the montant_max_estime column
 * @method CommonTContratTitulaireQuery orderByPublicationMontant($order = Criteria::ASC) Order by the publication_montant column
 * @method CommonTContratTitulaireQuery orderByIdMotifNonPublicationMontant($order = Criteria::ASC) Order by the id_motif_non_publication_montant column
 * @method CommonTContratTitulaireQuery orderByDescMotifNonPublicationMontant($order = Criteria::ASC) Order by the desc_motif_non_publication_montant column
 * @method CommonTContratTitulaireQuery orderByPublicationContrat($order = Criteria::ASC) Order by the publication_contrat column
 * @method CommonTContratTitulaireQuery orderByNumEj($order = Criteria::ASC) Order by the num_EJ column
 * @method CommonTContratTitulaireQuery orderByStatutej($order = Criteria::ASC) Order by the statutEJ column
 * @method CommonTContratTitulaireQuery orderByNumLongOeap($order = Criteria::ASC) Order by the num_long_OEAP column
 * @method CommonTContratTitulaireQuery orderByReferenceLibre($order = Criteria::ASC) Order by the reference_libre column
 * @method CommonTContratTitulaireQuery orderByStatutContrat($order = Criteria::ASC) Order by the statut_contrat column
 * @method CommonTContratTitulaireQuery orderByCategorie($order = Criteria::ASC) Order by the categorie column
 * @method CommonTContratTitulaireQuery orderByCcagApplicable($order = Criteria::ASC) Order by the ccag_applicable column
 * @method CommonTContratTitulaireQuery orderByClauseSociale($order = Criteria::ASC) Order by the clause_sociale column
 * @method CommonTContratTitulaireQuery orderByClauseSocialeConditionExecution($order = Criteria::ASC) Order by the clause_sociale_condition_execution column
 * @method CommonTContratTitulaireQuery orderByClauseSocialeInsertion($order = Criteria::ASC) Order by the clause_sociale_insertion column
 * @method CommonTContratTitulaireQuery orderByClauseSocialeAteliersProteges($order = Criteria::ASC) Order by the clause_sociale_ateliers_proteges column
 * @method CommonTContratTitulaireQuery orderByClauseSocialeSiae($order = Criteria::ASC) Order by the clause_sociale_siae column
 * @method CommonTContratTitulaireQuery orderByClauseSocialeEss($order = Criteria::ASC) Order by the clause_sociale_ess column
 * @method CommonTContratTitulaireQuery orderByClauseEnvironnementale($order = Criteria::ASC) Order by the clause_environnementale column
 * @method CommonTContratTitulaireQuery orderByClauseEnvSpecsTechniques($order = Criteria::ASC) Order by the clause_env_specs_techniques column
 * @method CommonTContratTitulaireQuery orderByClauseEnvCondExecution($order = Criteria::ASC) Order by the clause_env_cond_execution column
 * @method CommonTContratTitulaireQuery orderByClauseEnvCriteresSelect($order = Criteria::ASC) Order by the clause_env_criteres_select column
 * @method CommonTContratTitulaireQuery orderByDatePrevueNotification($order = Criteria::ASC) Order by the date_prevue_notification column
 * @method CommonTContratTitulaireQuery orderByDatePrevueFinContrat($order = Criteria::ASC) Order by the date_prevue_fin_contrat column
 * @method CommonTContratTitulaireQuery orderByDatePrevueMaxFinContrat($order = Criteria::ASC) Order by the date_prevue_max_fin_contrat column
 * @method CommonTContratTitulaireQuery orderByDateNotification($order = Criteria::ASC) Order by the date_notification column
 * @method CommonTContratTitulaireQuery orderByDateFinContrat($order = Criteria::ASC) Order by the date_fin_contrat column
 * @method CommonTContratTitulaireQuery orderByDateMaxFinContrat($order = Criteria::ASC) Order by the date_max_fin_contrat column
 * @method CommonTContratTitulaireQuery orderByDateAttribution($order = Criteria::ASC) Order by the date_attribution column
 * @method CommonTContratTitulaireQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTContratTitulaireQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonTContratTitulaireQuery orderByEnvoiInterface($order = Criteria::ASC) Order by the envoi_interface column
 * @method CommonTContratTitulaireQuery orderByContratClassKey($order = Criteria::ASC) Order by the contrat_class_key column
 * @method CommonTContratTitulaireQuery orderByPmePmi($order = Criteria::ASC) Order by the pme_pmi column
 * @method CommonTContratTitulaireQuery orderByReferenceConsultation($order = Criteria::ASC) Order by the reference_consultation column
 * @method CommonTContratTitulaireQuery orderByHorsPassation($order = Criteria::ASC) Order by the hors_passation column
 * @method CommonTContratTitulaireQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonTContratTitulaireQuery orderByNomAgent($order = Criteria::ASC) Order by the nom_agent column
 * @method CommonTContratTitulaireQuery orderByPrenomAgent($order = Criteria::ASC) Order by the prenom_agent column
 * @method CommonTContratTitulaireQuery orderByLieuExecution($order = Criteria::ASC) Order by the lieu_execution column
 * @method CommonTContratTitulaireQuery orderByCodeCpv1($order = Criteria::ASC) Order by the code_cpv_1 column
 * @method CommonTContratTitulaireQuery orderByCodeCpv2($order = Criteria::ASC) Order by the code_cpv_2 column
 * @method CommonTContratTitulaireQuery orderByMarcheInsertion($order = Criteria::ASC) Order by the marche_insertion column
 * @method CommonTContratTitulaireQuery orderByClauseSpecificationTechnique($order = Criteria::ASC) Order by the clause_specification_technique column
 * @method CommonTContratTitulaireQuery orderByProcedurePassationPivot($order = Criteria::ASC) Order by the procedure_passation_pivot column
 * @method CommonTContratTitulaireQuery orderByNomLieuPrincipalExecution($order = Criteria::ASC) Order by the nom_lieu_principal_execution column
 * @method CommonTContratTitulaireQuery orderByCodeLieuPrincipalExecution($order = Criteria::ASC) Order by the code_lieu_principal_execution column
 * @method CommonTContratTitulaireQuery orderByTypeCodeLieuPrincipalExecution($order = Criteria::ASC) Order by the type_code_lieu_principal_execution column
 * @method CommonTContratTitulaireQuery orderByDureeInitialeContrat($order = Criteria::ASC) Order by the duree_initiale_contrat column
 * @method CommonTContratTitulaireQuery orderByFormePrix($order = Criteria::ASC) Order by the forme_prix column
 * @method CommonTContratTitulaireQuery orderByDatePublicationInitialeDe($order = Criteria::ASC) Order by the date_publication_initiale_de column
 * @method CommonTContratTitulaireQuery orderByNumIdUniqueMarchePublic($order = Criteria::ASC) Order by the num_id_unique_marche_public column
 * @method CommonTContratTitulaireQuery orderByLibelleTypeContratPivot($order = Criteria::ASC) Order by the libelle_type_contrat_pivot column
 * @method CommonTContratTitulaireQuery orderBySiretPaAccordCadre($order = Criteria::ASC) Order by the siret_pa_accord_cadre column
 * @method CommonTContratTitulaireQuery orderByAcMarcheSubsequent($order = Criteria::ASC) Order by the ac_marche_subsequent column
 * @method CommonTContratTitulaireQuery orderByLibelleTypeProcedureMpe($order = Criteria::ASC) Order by the libelle_type_procedure_mpe column
 * @method CommonTContratTitulaireQuery orderByNbTotalPropositionsLot($order = Criteria::ASC) Order by the nb_total_propositions_lot column
 * @method CommonTContratTitulaireQuery orderByNbTotalPropositionsDematLot($order = Criteria::ASC) Order by the nb_total_propositions_demat_lot column
 * @method CommonTContratTitulaireQuery orderByMarcheDefense($order = Criteria::ASC) Order by the marche_defense column
 * @method CommonTContratTitulaireQuery orderBySiret($order = Criteria::ASC) Order by the siret column
 * @method CommonTContratTitulaireQuery orderByNomEntiteAcheteur($order = Criteria::ASC) Order by the nom_entite_acheteur column
 * @method CommonTContratTitulaireQuery orderByStatutPublicationSn($order = Criteria::ASC) Order by the statut_publication_sn column
 * @method CommonTContratTitulaireQuery orderByDatePublicationSn($order = Criteria::ASC) Order by the date_publication_sn column
 * @method CommonTContratTitulaireQuery orderByErreurSn($order = Criteria::ASC) Order by the erreur_sn column
 * @method CommonTContratTitulaireQuery orderByDateModificationSn($order = Criteria::ASC) Order by the date_modification_sn column
 * @method CommonTContratTitulaireQuery orderByIdTypeProcedurePivot($order = Criteria::ASC) Order by the id_type_procedure_pivot column
 * @method CommonTContratTitulaireQuery orderByIdTypeProcedureConcessionPivot($order = Criteria::ASC) Order by the id_type_procedure_concession_pivot column
 * @method CommonTContratTitulaireQuery orderByIdTypeContratPivot($order = Criteria::ASC) Order by the id_type_contrat_pivot column
 * @method CommonTContratTitulaireQuery orderByIdTypeContratConcessionPivot($order = Criteria::ASC) Order by the id_type_contrat_concession_pivot column
 * @method CommonTContratTitulaireQuery orderByMontantSubventionPublique($order = Criteria::ASC) Order by the montant_subvention_publique column
 * @method CommonTContratTitulaireQuery orderByDateDebutExecution($order = Criteria::ASC) Order by the date_debut_execution column
 * @method CommonTContratTitulaireQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method CommonTContratTitulaireQuery orderByMarcheInnovant($order = Criteria::ASC) Order by the marche_innovant column
 * @method CommonTContratTitulaireQuery orderByServiceId($order = Criteria::ASC) Order by the service_id column
 *
 * @method CommonTContratTitulaireQuery groupByIdContratTitulaire() Group by the id_contrat_titulaire column
 * @method CommonTContratTitulaireQuery groupByIdTypeContrat() Group by the id_type_contrat column
 * @method CommonTContratTitulaireQuery groupByIdContactContrat() Group by the id_contact_contrat column
 * @method CommonTContratTitulaireQuery groupByNumeroContrat() Group by the numero_contrat column
 * @method CommonTContratTitulaireQuery groupByLienAcSad() Group by the lien_AC_SAD column
 * @method CommonTContratTitulaireQuery groupByIdContratMulti() Group by the id_contrat_multi column
 * @method CommonTContratTitulaireQuery groupByOrganisme() Group by the organisme column
 * @method CommonTContratTitulaireQuery groupByOldServiceId() Group by the old_service_id column
 * @method CommonTContratTitulaireQuery groupByIdTitulaire() Group by the id_titulaire column
 * @method CommonTContratTitulaireQuery groupByIdTitulaireEtab() Group by the id_titulaire_etab column
 * @method CommonTContratTitulaireQuery groupByIdOffre() Group by the id_offre column
 * @method CommonTContratTitulaireQuery groupByTypeDepotReponse() Group by the type_depot_reponse column
 * @method CommonTContratTitulaireQuery groupByObjetContrat() Group by the objet_contrat column
 * @method CommonTContratTitulaireQuery groupByIntitule() Group by the intitule column
 * @method CommonTContratTitulaireQuery groupByMontantContrat() Group by the montant_contrat column
 * @method CommonTContratTitulaireQuery groupByIdTrancheBudgetaire() Group by the id_tranche_budgetaire column
 * @method CommonTContratTitulaireQuery groupByMontantMaxEstime() Group by the montant_max_estime column
 * @method CommonTContratTitulaireQuery groupByPublicationMontant() Group by the publication_montant column
 * @method CommonTContratTitulaireQuery groupByIdMotifNonPublicationMontant() Group by the id_motif_non_publication_montant column
 * @method CommonTContratTitulaireQuery groupByDescMotifNonPublicationMontant() Group by the desc_motif_non_publication_montant column
 * @method CommonTContratTitulaireQuery groupByPublicationContrat() Group by the publication_contrat column
 * @method CommonTContratTitulaireQuery groupByNumEj() Group by the num_EJ column
 * @method CommonTContratTitulaireQuery groupByStatutej() Group by the statutEJ column
 * @method CommonTContratTitulaireQuery groupByNumLongOeap() Group by the num_long_OEAP column
 * @method CommonTContratTitulaireQuery groupByReferenceLibre() Group by the reference_libre column
 * @method CommonTContratTitulaireQuery groupByStatutContrat() Group by the statut_contrat column
 * @method CommonTContratTitulaireQuery groupByCategorie() Group by the categorie column
 * @method CommonTContratTitulaireQuery groupByCcagApplicable() Group by the ccag_applicable column
 * @method CommonTContratTitulaireQuery groupByClauseSociale() Group by the clause_sociale column
 * @method CommonTContratTitulaireQuery groupByClauseSocialeConditionExecution() Group by the clause_sociale_condition_execution column
 * @method CommonTContratTitulaireQuery groupByClauseSocialeInsertion() Group by the clause_sociale_insertion column
 * @method CommonTContratTitulaireQuery groupByClauseSocialeAteliersProteges() Group by the clause_sociale_ateliers_proteges column
 * @method CommonTContratTitulaireQuery groupByClauseSocialeSiae() Group by the clause_sociale_siae column
 * @method CommonTContratTitulaireQuery groupByClauseSocialeEss() Group by the clause_sociale_ess column
 * @method CommonTContratTitulaireQuery groupByClauseEnvironnementale() Group by the clause_environnementale column
 * @method CommonTContratTitulaireQuery groupByClauseEnvSpecsTechniques() Group by the clause_env_specs_techniques column
 * @method CommonTContratTitulaireQuery groupByClauseEnvCondExecution() Group by the clause_env_cond_execution column
 * @method CommonTContratTitulaireQuery groupByClauseEnvCriteresSelect() Group by the clause_env_criteres_select column
 * @method CommonTContratTitulaireQuery groupByDatePrevueNotification() Group by the date_prevue_notification column
 * @method CommonTContratTitulaireQuery groupByDatePrevueFinContrat() Group by the date_prevue_fin_contrat column
 * @method CommonTContratTitulaireQuery groupByDatePrevueMaxFinContrat() Group by the date_prevue_max_fin_contrat column
 * @method CommonTContratTitulaireQuery groupByDateNotification() Group by the date_notification column
 * @method CommonTContratTitulaireQuery groupByDateFinContrat() Group by the date_fin_contrat column
 * @method CommonTContratTitulaireQuery groupByDateMaxFinContrat() Group by the date_max_fin_contrat column
 * @method CommonTContratTitulaireQuery groupByDateAttribution() Group by the date_attribution column
 * @method CommonTContratTitulaireQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTContratTitulaireQuery groupByDateModification() Group by the date_modification column
 * @method CommonTContratTitulaireQuery groupByEnvoiInterface() Group by the envoi_interface column
 * @method CommonTContratTitulaireQuery groupByContratClassKey() Group by the contrat_class_key column
 * @method CommonTContratTitulaireQuery groupByPmePmi() Group by the pme_pmi column
 * @method CommonTContratTitulaireQuery groupByReferenceConsultation() Group by the reference_consultation column
 * @method CommonTContratTitulaireQuery groupByHorsPassation() Group by the hors_passation column
 * @method CommonTContratTitulaireQuery groupByIdAgent() Group by the id_agent column
 * @method CommonTContratTitulaireQuery groupByNomAgent() Group by the nom_agent column
 * @method CommonTContratTitulaireQuery groupByPrenomAgent() Group by the prenom_agent column
 * @method CommonTContratTitulaireQuery groupByLieuExecution() Group by the lieu_execution column
 * @method CommonTContratTitulaireQuery groupByCodeCpv1() Group by the code_cpv_1 column
 * @method CommonTContratTitulaireQuery groupByCodeCpv2() Group by the code_cpv_2 column
 * @method CommonTContratTitulaireQuery groupByMarcheInsertion() Group by the marche_insertion column
 * @method CommonTContratTitulaireQuery groupByClauseSpecificationTechnique() Group by the clause_specification_technique column
 * @method CommonTContratTitulaireQuery groupByProcedurePassationPivot() Group by the procedure_passation_pivot column
 * @method CommonTContratTitulaireQuery groupByNomLieuPrincipalExecution() Group by the nom_lieu_principal_execution column
 * @method CommonTContratTitulaireQuery groupByCodeLieuPrincipalExecution() Group by the code_lieu_principal_execution column
 * @method CommonTContratTitulaireQuery groupByTypeCodeLieuPrincipalExecution() Group by the type_code_lieu_principal_execution column
 * @method CommonTContratTitulaireQuery groupByDureeInitialeContrat() Group by the duree_initiale_contrat column
 * @method CommonTContratTitulaireQuery groupByFormePrix() Group by the forme_prix column
 * @method CommonTContratTitulaireQuery groupByDatePublicationInitialeDe() Group by the date_publication_initiale_de column
 * @method CommonTContratTitulaireQuery groupByNumIdUniqueMarchePublic() Group by the num_id_unique_marche_public column
 * @method CommonTContratTitulaireQuery groupByLibelleTypeContratPivot() Group by the libelle_type_contrat_pivot column
 * @method CommonTContratTitulaireQuery groupBySiretPaAccordCadre() Group by the siret_pa_accord_cadre column
 * @method CommonTContratTitulaireQuery groupByAcMarcheSubsequent() Group by the ac_marche_subsequent column
 * @method CommonTContratTitulaireQuery groupByLibelleTypeProcedureMpe() Group by the libelle_type_procedure_mpe column
 * @method CommonTContratTitulaireQuery groupByNbTotalPropositionsLot() Group by the nb_total_propositions_lot column
 * @method CommonTContratTitulaireQuery groupByNbTotalPropositionsDematLot() Group by the nb_total_propositions_demat_lot column
 * @method CommonTContratTitulaireQuery groupByMarcheDefense() Group by the marche_defense column
 * @method CommonTContratTitulaireQuery groupBySiret() Group by the siret column
 * @method CommonTContratTitulaireQuery groupByNomEntiteAcheteur() Group by the nom_entite_acheteur column
 * @method CommonTContratTitulaireQuery groupByStatutPublicationSn() Group by the statut_publication_sn column
 * @method CommonTContratTitulaireQuery groupByDatePublicationSn() Group by the date_publication_sn column
 * @method CommonTContratTitulaireQuery groupByErreurSn() Group by the erreur_sn column
 * @method CommonTContratTitulaireQuery groupByDateModificationSn() Group by the date_modification_sn column
 * @method CommonTContratTitulaireQuery groupByIdTypeProcedurePivot() Group by the id_type_procedure_pivot column
 * @method CommonTContratTitulaireQuery groupByIdTypeProcedureConcessionPivot() Group by the id_type_procedure_concession_pivot column
 * @method CommonTContratTitulaireQuery groupByIdTypeContratPivot() Group by the id_type_contrat_pivot column
 * @method CommonTContratTitulaireQuery groupByIdTypeContratConcessionPivot() Group by the id_type_contrat_concession_pivot column
 * @method CommonTContratTitulaireQuery groupByMontantSubventionPublique() Group by the montant_subvention_publique column
 * @method CommonTContratTitulaireQuery groupByDateDebutExecution() Group by the date_debut_execution column
 * @method CommonTContratTitulaireQuery groupByUuid() Group by the uuid column
 * @method CommonTContratTitulaireQuery groupByMarcheInnovant() Group by the marche_innovant column
 * @method CommonTContratTitulaireQuery groupByServiceId() Group by the service_id column
 *
 * @method CommonTContratTitulaireQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTContratTitulaireQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTContratTitulaireQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTypeContratConcessionPivot($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTypeContratConcessionPivot relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTypeContratConcessionPivot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTypeContratConcessionPivot relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTypeContratConcessionPivot($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTypeContratConcessionPivot relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTypeContratPivot($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTypeContratPivot relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTypeContratPivot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTypeContratPivot relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTypeContratPivot($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTypeContratPivot relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTypeProcedureConcessionPivot($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTypeProcedureConcessionPivot relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTypeProcedureConcessionPivot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTypeProcedureConcessionPivot relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTypeProcedureConcessionPivot($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTypeProcedureConcessionPivot relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTContactContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContactContrat relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTContactContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContactContrat relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTContactContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContactContrat relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTContratTitulaireRelatedByIdContratMulti($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContratTitulaireRelatedByIdContratMulti relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTContratTitulaireRelatedByIdContratMulti($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContratTitulaireRelatedByIdContratMulti relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTContratTitulaireRelatedByIdContratMulti($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContratTitulaireRelatedByIdContratMulti relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTTypeContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTTypeContrat relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTTypeContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTTypeContrat relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTTypeContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTTypeContrat relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTypeProcedure($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTypeProcedure relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTypeProcedure($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTypeProcedure relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTypeProcedure($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTypeProcedure relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonMarche($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonMarche relation
 * @method CommonTContratTitulaireQuery rightJoinCommonMarche($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonMarche relation
 * @method CommonTContratTitulaireQuery innerJoinCommonMarche($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonMarche relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonDonneesAnnuellesConcession($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonDonneesAnnuellesConcession relation
 * @method CommonTContratTitulaireQuery rightJoinCommonDonneesAnnuellesConcession($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonDonneesAnnuellesConcession relation
 * @method CommonTContratTitulaireQuery innerJoinCommonDonneesAnnuellesConcession($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonDonneesAnnuellesConcession relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonEchangeDoc($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDoc relation
 * @method CommonTContratTitulaireQuery rightJoinCommonEchangeDoc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDoc relation
 * @method CommonTContratTitulaireQuery innerJoinCommonEchangeDoc($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDoc relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonModificationContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonModificationContrat relation
 * @method CommonTContratTitulaireQuery rightJoinCommonModificationContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonModificationContrat relation
 * @method CommonTContratTitulaireQuery innerJoinCommonModificationContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonModificationContrat relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTConsLotContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTConsLotContrat relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTConsLotContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTConsLotContrat relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTConsLotContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTConsLotContrat relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTContratTitulaireRelatedByIdContratTitulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContratTitulaireRelatedByIdContratTitulaire relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTContratTitulaireRelatedByIdContratTitulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContratTitulaireRelatedByIdContratTitulaire relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTContratTitulaireRelatedByIdContratTitulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContratTitulaireRelatedByIdContratTitulaire relation
 *
 * @method CommonTContratTitulaireQuery leftJoinCommonTDonneesConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTDonneesConsultation relation
 * @method CommonTContratTitulaireQuery rightJoinCommonTDonneesConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTDonneesConsultation relation
 * @method CommonTContratTitulaireQuery innerJoinCommonTDonneesConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTDonneesConsultation relation
 *
 * @method CommonTContratTitulaire findOne(PropelPDO $con = null) Return the first CommonTContratTitulaire matching the query
 * @method CommonTContratTitulaire findOneOrCreate(PropelPDO $con = null) Return the first CommonTContratTitulaire matching the query, or a new CommonTContratTitulaire object populated from the query conditions when no match is found
 *
 * @method CommonTContratTitulaire findOneByIdTypeContrat(int $id_type_contrat) Return the first CommonTContratTitulaire filtered by the id_type_contrat column
 * @method CommonTContratTitulaire findOneByIdContactContrat(int $id_contact_contrat) Return the first CommonTContratTitulaire filtered by the id_contact_contrat column
 * @method CommonTContratTitulaire findOneByNumeroContrat(string $numero_contrat) Return the first CommonTContratTitulaire filtered by the numero_contrat column
 * @method CommonTContratTitulaire findOneByLienAcSad(int $lien_AC_SAD) Return the first CommonTContratTitulaire filtered by the lien_AC_SAD column
 * @method CommonTContratTitulaire findOneByIdContratMulti(int $id_contrat_multi) Return the first CommonTContratTitulaire filtered by the id_contrat_multi column
 * @method CommonTContratTitulaire findOneByOrganisme(string $organisme) Return the first CommonTContratTitulaire filtered by the organisme column
 * @method CommonTContratTitulaire findOneByOldServiceId(int $old_service_id) Return the first CommonTContratTitulaire filtered by the old_service_id column
 * @method CommonTContratTitulaire findOneByIdTitulaire(int $id_titulaire) Return the first CommonTContratTitulaire filtered by the id_titulaire column
 * @method CommonTContratTitulaire findOneByIdTitulaireEtab(int $id_titulaire_etab) Return the first CommonTContratTitulaire filtered by the id_titulaire_etab column
 * @method CommonTContratTitulaire findOneByIdOffre(int $id_offre) Return the first CommonTContratTitulaire filtered by the id_offre column
 * @method CommonTContratTitulaire findOneByTypeDepotReponse(string $type_depot_reponse) Return the first CommonTContratTitulaire filtered by the type_depot_reponse column
 * @method CommonTContratTitulaire findOneByObjetContrat(string $objet_contrat) Return the first CommonTContratTitulaire filtered by the objet_contrat column
 * @method CommonTContratTitulaire findOneByIntitule(string $intitule) Return the first CommonTContratTitulaire filtered by the intitule column
 * @method CommonTContratTitulaire findOneByMontantContrat(double $montant_contrat) Return the first CommonTContratTitulaire filtered by the montant_contrat column
 * @method CommonTContratTitulaire findOneByIdTrancheBudgetaire(int $id_tranche_budgetaire) Return the first CommonTContratTitulaire filtered by the id_tranche_budgetaire column
 * @method CommonTContratTitulaire findOneByMontantMaxEstime(double $montant_max_estime) Return the first CommonTContratTitulaire filtered by the montant_max_estime column
 * @method CommonTContratTitulaire findOneByPublicationMontant(int $publication_montant) Return the first CommonTContratTitulaire filtered by the publication_montant column
 * @method CommonTContratTitulaire findOneByIdMotifNonPublicationMontant(int $id_motif_non_publication_montant) Return the first CommonTContratTitulaire filtered by the id_motif_non_publication_montant column
 * @method CommonTContratTitulaire findOneByDescMotifNonPublicationMontant(string $desc_motif_non_publication_montant) Return the first CommonTContratTitulaire filtered by the desc_motif_non_publication_montant column
 * @method CommonTContratTitulaire findOneByPublicationContrat(int $publication_contrat) Return the first CommonTContratTitulaire filtered by the publication_contrat column
 * @method CommonTContratTitulaire findOneByNumEj(string $num_EJ) Return the first CommonTContratTitulaire filtered by the num_EJ column
 * @method CommonTContratTitulaire findOneByStatutej(string $statutEJ) Return the first CommonTContratTitulaire filtered by the statutEJ column
 * @method CommonTContratTitulaire findOneByNumLongOeap(string $num_long_OEAP) Return the first CommonTContratTitulaire filtered by the num_long_OEAP column
 * @method CommonTContratTitulaire findOneByReferenceLibre(string $reference_libre) Return the first CommonTContratTitulaire filtered by the reference_libre column
 * @method CommonTContratTitulaire findOneByStatutContrat(int $statut_contrat) Return the first CommonTContratTitulaire filtered by the statut_contrat column
 * @method CommonTContratTitulaire findOneByCategorie(int $categorie) Return the first CommonTContratTitulaire filtered by the categorie column
 * @method CommonTContratTitulaire findOneByCcagApplicable(int $ccag_applicable) Return the first CommonTContratTitulaire filtered by the ccag_applicable column
 * @method CommonTContratTitulaire findOneByClauseSociale(string $clause_sociale) Return the first CommonTContratTitulaire filtered by the clause_sociale column
 * @method CommonTContratTitulaire findOneByClauseSocialeConditionExecution(string $clause_sociale_condition_execution) Return the first CommonTContratTitulaire filtered by the clause_sociale_condition_execution column
 * @method CommonTContratTitulaire findOneByClauseSocialeInsertion(string $clause_sociale_insertion) Return the first CommonTContratTitulaire filtered by the clause_sociale_insertion column
 * @method CommonTContratTitulaire findOneByClauseSocialeAteliersProteges(string $clause_sociale_ateliers_proteges) Return the first CommonTContratTitulaire filtered by the clause_sociale_ateliers_proteges column
 * @method CommonTContratTitulaire findOneByClauseSocialeSiae(string $clause_sociale_siae) Return the first CommonTContratTitulaire filtered by the clause_sociale_siae column
 * @method CommonTContratTitulaire findOneByClauseSocialeEss(string $clause_sociale_ess) Return the first CommonTContratTitulaire filtered by the clause_sociale_ess column
 * @method CommonTContratTitulaire findOneByClauseEnvironnementale(string $clause_environnementale) Return the first CommonTContratTitulaire filtered by the clause_environnementale column
 * @method CommonTContratTitulaire findOneByClauseEnvSpecsTechniques(string $clause_env_specs_techniques) Return the first CommonTContratTitulaire filtered by the clause_env_specs_techniques column
 * @method CommonTContratTitulaire findOneByClauseEnvCondExecution(string $clause_env_cond_execution) Return the first CommonTContratTitulaire filtered by the clause_env_cond_execution column
 * @method CommonTContratTitulaire findOneByClauseEnvCriteresSelect(string $clause_env_criteres_select) Return the first CommonTContratTitulaire filtered by the clause_env_criteres_select column
 * @method CommonTContratTitulaire findOneByDatePrevueNotification(string $date_prevue_notification) Return the first CommonTContratTitulaire filtered by the date_prevue_notification column
 * @method CommonTContratTitulaire findOneByDatePrevueFinContrat(string $date_prevue_fin_contrat) Return the first CommonTContratTitulaire filtered by the date_prevue_fin_contrat column
 * @method CommonTContratTitulaire findOneByDatePrevueMaxFinContrat(string $date_prevue_max_fin_contrat) Return the first CommonTContratTitulaire filtered by the date_prevue_max_fin_contrat column
 * @method CommonTContratTitulaire findOneByDateNotification(string $date_notification) Return the first CommonTContratTitulaire filtered by the date_notification column
 * @method CommonTContratTitulaire findOneByDateFinContrat(string $date_fin_contrat) Return the first CommonTContratTitulaire filtered by the date_fin_contrat column
 * @method CommonTContratTitulaire findOneByDateMaxFinContrat(string $date_max_fin_contrat) Return the first CommonTContratTitulaire filtered by the date_max_fin_contrat column
 * @method CommonTContratTitulaire findOneByDateAttribution(string $date_attribution) Return the first CommonTContratTitulaire filtered by the date_attribution column
 * @method CommonTContratTitulaire findOneByDateCreation(string $date_creation) Return the first CommonTContratTitulaire filtered by the date_creation column
 * @method CommonTContratTitulaire findOneByDateModification(string $date_modification) Return the first CommonTContratTitulaire filtered by the date_modification column
 * @method CommonTContratTitulaire findOneByEnvoiInterface(string $envoi_interface) Return the first CommonTContratTitulaire filtered by the envoi_interface column
 * @method CommonTContratTitulaire findOneByContratClassKey(int $contrat_class_key) Return the first CommonTContratTitulaire filtered by the contrat_class_key column
 * @method CommonTContratTitulaire findOneByPmePmi(int $pme_pmi) Return the first CommonTContratTitulaire filtered by the pme_pmi column
 * @method CommonTContratTitulaire findOneByReferenceConsultation(string $reference_consultation) Return the first CommonTContratTitulaire filtered by the reference_consultation column
 * @method CommonTContratTitulaire findOneByHorsPassation(int $hors_passation) Return the first CommonTContratTitulaire filtered by the hors_passation column
 * @method CommonTContratTitulaire findOneByIdAgent(int $id_agent) Return the first CommonTContratTitulaire filtered by the id_agent column
 * @method CommonTContratTitulaire findOneByNomAgent(string $nom_agent) Return the first CommonTContratTitulaire filtered by the nom_agent column
 * @method CommonTContratTitulaire findOneByPrenomAgent(string $prenom_agent) Return the first CommonTContratTitulaire filtered by the prenom_agent column
 * @method CommonTContratTitulaire findOneByLieuExecution(string $lieu_execution) Return the first CommonTContratTitulaire filtered by the lieu_execution column
 * @method CommonTContratTitulaire findOneByCodeCpv1(string $code_cpv_1) Return the first CommonTContratTitulaire filtered by the code_cpv_1 column
 * @method CommonTContratTitulaire findOneByCodeCpv2(string $code_cpv_2) Return the first CommonTContratTitulaire filtered by the code_cpv_2 column
 * @method CommonTContratTitulaire findOneByMarcheInsertion(boolean $marche_insertion) Return the first CommonTContratTitulaire filtered by the marche_insertion column
 * @method CommonTContratTitulaire findOneByClauseSpecificationTechnique(string $clause_specification_technique) Return the first CommonTContratTitulaire filtered by the clause_specification_technique column
 * @method CommonTContratTitulaire findOneByProcedurePassationPivot(string $procedure_passation_pivot) Return the first CommonTContratTitulaire filtered by the procedure_passation_pivot column
 * @method CommonTContratTitulaire findOneByNomLieuPrincipalExecution(string $nom_lieu_principal_execution) Return the first CommonTContratTitulaire filtered by the nom_lieu_principal_execution column
 * @method CommonTContratTitulaire findOneByCodeLieuPrincipalExecution(string $code_lieu_principal_execution) Return the first CommonTContratTitulaire filtered by the code_lieu_principal_execution column
 * @method CommonTContratTitulaire findOneByTypeCodeLieuPrincipalExecution(string $type_code_lieu_principal_execution) Return the first CommonTContratTitulaire filtered by the type_code_lieu_principal_execution column
 * @method CommonTContratTitulaire findOneByDureeInitialeContrat(int $duree_initiale_contrat) Return the first CommonTContratTitulaire filtered by the duree_initiale_contrat column
 * @method CommonTContratTitulaire findOneByFormePrix(string $forme_prix) Return the first CommonTContratTitulaire filtered by the forme_prix column
 * @method CommonTContratTitulaire findOneByDatePublicationInitialeDe(string $date_publication_initiale_de) Return the first CommonTContratTitulaire filtered by the date_publication_initiale_de column
 * @method CommonTContratTitulaire findOneByNumIdUniqueMarchePublic(string $num_id_unique_marche_public) Return the first CommonTContratTitulaire filtered by the num_id_unique_marche_public column
 * @method CommonTContratTitulaire findOneByLibelleTypeContratPivot(string $libelle_type_contrat_pivot) Return the first CommonTContratTitulaire filtered by the libelle_type_contrat_pivot column
 * @method CommonTContratTitulaire findOneBySiretPaAccordCadre(string $siret_pa_accord_cadre) Return the first CommonTContratTitulaire filtered by the siret_pa_accord_cadre column
 * @method CommonTContratTitulaire findOneByAcMarcheSubsequent(boolean $ac_marche_subsequent) Return the first CommonTContratTitulaire filtered by the ac_marche_subsequent column
 * @method CommonTContratTitulaire findOneByLibelleTypeProcedureMpe(string $libelle_type_procedure_mpe) Return the first CommonTContratTitulaire filtered by the libelle_type_procedure_mpe column
 * @method CommonTContratTitulaire findOneByNbTotalPropositionsLot(int $nb_total_propositions_lot) Return the first CommonTContratTitulaire filtered by the nb_total_propositions_lot column
 * @method CommonTContratTitulaire findOneByNbTotalPropositionsDematLot(int $nb_total_propositions_demat_lot) Return the first CommonTContratTitulaire filtered by the nb_total_propositions_demat_lot column
 * @method CommonTContratTitulaire findOneByMarcheDefense(string $marche_defense) Return the first CommonTContratTitulaire filtered by the marche_defense column
 * @method CommonTContratTitulaire findOneBySiret(string $siret) Return the first CommonTContratTitulaire filtered by the siret column
 * @method CommonTContratTitulaire findOneByNomEntiteAcheteur(string $nom_entite_acheteur) Return the first CommonTContratTitulaire filtered by the nom_entite_acheteur column
 * @method CommonTContratTitulaire findOneByStatutPublicationSn(int $statut_publication_sn) Return the first CommonTContratTitulaire filtered by the statut_publication_sn column
 * @method CommonTContratTitulaire findOneByDatePublicationSn(string $date_publication_sn) Return the first CommonTContratTitulaire filtered by the date_publication_sn column
 * @method CommonTContratTitulaire findOneByErreurSn(string $erreur_sn) Return the first CommonTContratTitulaire filtered by the erreur_sn column
 * @method CommonTContratTitulaire findOneByDateModificationSn(string $date_modification_sn) Return the first CommonTContratTitulaire filtered by the date_modification_sn column
 * @method CommonTContratTitulaire findOneByIdTypeProcedurePivot(int $id_type_procedure_pivot) Return the first CommonTContratTitulaire filtered by the id_type_procedure_pivot column
 * @method CommonTContratTitulaire findOneByIdTypeProcedureConcessionPivot(int $id_type_procedure_concession_pivot) Return the first CommonTContratTitulaire filtered by the id_type_procedure_concession_pivot column
 * @method CommonTContratTitulaire findOneByIdTypeContratPivot(int $id_type_contrat_pivot) Return the first CommonTContratTitulaire filtered by the id_type_contrat_pivot column
 * @method CommonTContratTitulaire findOneByIdTypeContratConcessionPivot(int $id_type_contrat_concession_pivot) Return the first CommonTContratTitulaire filtered by the id_type_contrat_concession_pivot column
 * @method CommonTContratTitulaire findOneByMontantSubventionPublique(double $montant_subvention_publique) Return the first CommonTContratTitulaire filtered by the montant_subvention_publique column
 * @method CommonTContratTitulaire findOneByDateDebutExecution(string $date_debut_execution) Return the first CommonTContratTitulaire filtered by the date_debut_execution column
 * @method CommonTContratTitulaire findOneByUuid(string $uuid) Return the first CommonTContratTitulaire filtered by the uuid column
 * @method CommonTContratTitulaire findOneByMarcheInnovant(boolean $marche_innovant) Return the first CommonTContratTitulaire filtered by the marche_innovant column
 * @method CommonTContratTitulaire findOneByServiceId(string $service_id) Return the first CommonTContratTitulaire filtered by the service_id column
 *
 * @method array findByIdContratTitulaire(int $id_contrat_titulaire) Return CommonTContratTitulaire objects filtered by the id_contrat_titulaire column
 * @method array findByIdTypeContrat(int $id_type_contrat) Return CommonTContratTitulaire objects filtered by the id_type_contrat column
 * @method array findByIdContactContrat(int $id_contact_contrat) Return CommonTContratTitulaire objects filtered by the id_contact_contrat column
 * @method array findByNumeroContrat(string $numero_contrat) Return CommonTContratTitulaire objects filtered by the numero_contrat column
 * @method array findByLienAcSad(int $lien_AC_SAD) Return CommonTContratTitulaire objects filtered by the lien_AC_SAD column
 * @method array findByIdContratMulti(int $id_contrat_multi) Return CommonTContratTitulaire objects filtered by the id_contrat_multi column
 * @method array findByOrganisme(string $organisme) Return CommonTContratTitulaire objects filtered by the organisme column
 * @method array findByOldServiceId(int $old_service_id) Return CommonTContratTitulaire objects filtered by the old_service_id column
 * @method array findByIdTitulaire(int $id_titulaire) Return CommonTContratTitulaire objects filtered by the id_titulaire column
 * @method array findByIdTitulaireEtab(int $id_titulaire_etab) Return CommonTContratTitulaire objects filtered by the id_titulaire_etab column
 * @method array findByIdOffre(int $id_offre) Return CommonTContratTitulaire objects filtered by the id_offre column
 * @method array findByTypeDepotReponse(string $type_depot_reponse) Return CommonTContratTitulaire objects filtered by the type_depot_reponse column
 * @method array findByObjetContrat(string $objet_contrat) Return CommonTContratTitulaire objects filtered by the objet_contrat column
 * @method array findByIntitule(string $intitule) Return CommonTContratTitulaire objects filtered by the intitule column
 * @method array findByMontantContrat(double $montant_contrat) Return CommonTContratTitulaire objects filtered by the montant_contrat column
 * @method array findByIdTrancheBudgetaire(int $id_tranche_budgetaire) Return CommonTContratTitulaire objects filtered by the id_tranche_budgetaire column
 * @method array findByMontantMaxEstime(double $montant_max_estime) Return CommonTContratTitulaire objects filtered by the montant_max_estime column
 * @method array findByPublicationMontant(int $publication_montant) Return CommonTContratTitulaire objects filtered by the publication_montant column
 * @method array findByIdMotifNonPublicationMontant(int $id_motif_non_publication_montant) Return CommonTContratTitulaire objects filtered by the id_motif_non_publication_montant column
 * @method array findByDescMotifNonPublicationMontant(string $desc_motif_non_publication_montant) Return CommonTContratTitulaire objects filtered by the desc_motif_non_publication_montant column
 * @method array findByPublicationContrat(int $publication_contrat) Return CommonTContratTitulaire objects filtered by the publication_contrat column
 * @method array findByNumEj(string $num_EJ) Return CommonTContratTitulaire objects filtered by the num_EJ column
 * @method array findByStatutej(string $statutEJ) Return CommonTContratTitulaire objects filtered by the statutEJ column
 * @method array findByNumLongOeap(string $num_long_OEAP) Return CommonTContratTitulaire objects filtered by the num_long_OEAP column
 * @method array findByReferenceLibre(string $reference_libre) Return CommonTContratTitulaire objects filtered by the reference_libre column
 * @method array findByStatutContrat(int $statut_contrat) Return CommonTContratTitulaire objects filtered by the statut_contrat column
 * @method array findByCategorie(int $categorie) Return CommonTContratTitulaire objects filtered by the categorie column
 * @method array findByCcagApplicable(int $ccag_applicable) Return CommonTContratTitulaire objects filtered by the ccag_applicable column
 * @method array findByClauseSociale(string $clause_sociale) Return CommonTContratTitulaire objects filtered by the clause_sociale column
 * @method array findByClauseSocialeConditionExecution(string $clause_sociale_condition_execution) Return CommonTContratTitulaire objects filtered by the clause_sociale_condition_execution column
 * @method array findByClauseSocialeInsertion(string $clause_sociale_insertion) Return CommonTContratTitulaire objects filtered by the clause_sociale_insertion column
 * @method array findByClauseSocialeAteliersProteges(string $clause_sociale_ateliers_proteges) Return CommonTContratTitulaire objects filtered by the clause_sociale_ateliers_proteges column
 * @method array findByClauseSocialeSiae(string $clause_sociale_siae) Return CommonTContratTitulaire objects filtered by the clause_sociale_siae column
 * @method array findByClauseSocialeEss(string $clause_sociale_ess) Return CommonTContratTitulaire objects filtered by the clause_sociale_ess column
 * @method array findByClauseEnvironnementale(string $clause_environnementale) Return CommonTContratTitulaire objects filtered by the clause_environnementale column
 * @method array findByClauseEnvSpecsTechniques(string $clause_env_specs_techniques) Return CommonTContratTitulaire objects filtered by the clause_env_specs_techniques column
 * @method array findByClauseEnvCondExecution(string $clause_env_cond_execution) Return CommonTContratTitulaire objects filtered by the clause_env_cond_execution column
 * @method array findByClauseEnvCriteresSelect(string $clause_env_criteres_select) Return CommonTContratTitulaire objects filtered by the clause_env_criteres_select column
 * @method array findByDatePrevueNotification(string $date_prevue_notification) Return CommonTContratTitulaire objects filtered by the date_prevue_notification column
 * @method array findByDatePrevueFinContrat(string $date_prevue_fin_contrat) Return CommonTContratTitulaire objects filtered by the date_prevue_fin_contrat column
 * @method array findByDatePrevueMaxFinContrat(string $date_prevue_max_fin_contrat) Return CommonTContratTitulaire objects filtered by the date_prevue_max_fin_contrat column
 * @method array findByDateNotification(string $date_notification) Return CommonTContratTitulaire objects filtered by the date_notification column
 * @method array findByDateFinContrat(string $date_fin_contrat) Return CommonTContratTitulaire objects filtered by the date_fin_contrat column
 * @method array findByDateMaxFinContrat(string $date_max_fin_contrat) Return CommonTContratTitulaire objects filtered by the date_max_fin_contrat column
 * @method array findByDateAttribution(string $date_attribution) Return CommonTContratTitulaire objects filtered by the date_attribution column
 * @method array findByDateCreation(string $date_creation) Return CommonTContratTitulaire objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonTContratTitulaire objects filtered by the date_modification column
 * @method array findByEnvoiInterface(string $envoi_interface) Return CommonTContratTitulaire objects filtered by the envoi_interface column
 * @method array findByContratClassKey(int $contrat_class_key) Return CommonTContratTitulaire objects filtered by the contrat_class_key column
 * @method array findByPmePmi(int $pme_pmi) Return CommonTContratTitulaire objects filtered by the pme_pmi column
 * @method array findByReferenceConsultation(string $reference_consultation) Return CommonTContratTitulaire objects filtered by the reference_consultation column
 * @method array findByHorsPassation(int $hors_passation) Return CommonTContratTitulaire objects filtered by the hors_passation column
 * @method array findByIdAgent(int $id_agent) Return CommonTContratTitulaire objects filtered by the id_agent column
 * @method array findByNomAgent(string $nom_agent) Return CommonTContratTitulaire objects filtered by the nom_agent column
 * @method array findByPrenomAgent(string $prenom_agent) Return CommonTContratTitulaire objects filtered by the prenom_agent column
 * @method array findByLieuExecution(string $lieu_execution) Return CommonTContratTitulaire objects filtered by the lieu_execution column
 * @method array findByCodeCpv1(string $code_cpv_1) Return CommonTContratTitulaire objects filtered by the code_cpv_1 column
 * @method array findByCodeCpv2(string $code_cpv_2) Return CommonTContratTitulaire objects filtered by the code_cpv_2 column
 * @method array findByMarcheInsertion(boolean $marche_insertion) Return CommonTContratTitulaire objects filtered by the marche_insertion column
 * @method array findByClauseSpecificationTechnique(string $clause_specification_technique) Return CommonTContratTitulaire objects filtered by the clause_specification_technique column
 * @method array findByProcedurePassationPivot(string $procedure_passation_pivot) Return CommonTContratTitulaire objects filtered by the procedure_passation_pivot column
 * @method array findByNomLieuPrincipalExecution(string $nom_lieu_principal_execution) Return CommonTContratTitulaire objects filtered by the nom_lieu_principal_execution column
 * @method array findByCodeLieuPrincipalExecution(string $code_lieu_principal_execution) Return CommonTContratTitulaire objects filtered by the code_lieu_principal_execution column
 * @method array findByTypeCodeLieuPrincipalExecution(string $type_code_lieu_principal_execution) Return CommonTContratTitulaire objects filtered by the type_code_lieu_principal_execution column
 * @method array findByDureeInitialeContrat(int $duree_initiale_contrat) Return CommonTContratTitulaire objects filtered by the duree_initiale_contrat column
 * @method array findByFormePrix(string $forme_prix) Return CommonTContratTitulaire objects filtered by the forme_prix column
 * @method array findByDatePublicationInitialeDe(string $date_publication_initiale_de) Return CommonTContratTitulaire objects filtered by the date_publication_initiale_de column
 * @method array findByNumIdUniqueMarchePublic(string $num_id_unique_marche_public) Return CommonTContratTitulaire objects filtered by the num_id_unique_marche_public column
 * @method array findByLibelleTypeContratPivot(string $libelle_type_contrat_pivot) Return CommonTContratTitulaire objects filtered by the libelle_type_contrat_pivot column
 * @method array findBySiretPaAccordCadre(string $siret_pa_accord_cadre) Return CommonTContratTitulaire objects filtered by the siret_pa_accord_cadre column
 * @method array findByAcMarcheSubsequent(boolean $ac_marche_subsequent) Return CommonTContratTitulaire objects filtered by the ac_marche_subsequent column
 * @method array findByLibelleTypeProcedureMpe(string $libelle_type_procedure_mpe) Return CommonTContratTitulaire objects filtered by the libelle_type_procedure_mpe column
 * @method array findByNbTotalPropositionsLot(int $nb_total_propositions_lot) Return CommonTContratTitulaire objects filtered by the nb_total_propositions_lot column
 * @method array findByNbTotalPropositionsDematLot(int $nb_total_propositions_demat_lot) Return CommonTContratTitulaire objects filtered by the nb_total_propositions_demat_lot column
 * @method array findByMarcheDefense(string $marche_defense) Return CommonTContratTitulaire objects filtered by the marche_defense column
 * @method array findBySiret(string $siret) Return CommonTContratTitulaire objects filtered by the siret column
 * @method array findByNomEntiteAcheteur(string $nom_entite_acheteur) Return CommonTContratTitulaire objects filtered by the nom_entite_acheteur column
 * @method array findByStatutPublicationSn(int $statut_publication_sn) Return CommonTContratTitulaire objects filtered by the statut_publication_sn column
 * @method array findByDatePublicationSn(string $date_publication_sn) Return CommonTContratTitulaire objects filtered by the date_publication_sn column
 * @method array findByErreurSn(string $erreur_sn) Return CommonTContratTitulaire objects filtered by the erreur_sn column
 * @method array findByDateModificationSn(string $date_modification_sn) Return CommonTContratTitulaire objects filtered by the date_modification_sn column
 * @method array findByIdTypeProcedurePivot(int $id_type_procedure_pivot) Return CommonTContratTitulaire objects filtered by the id_type_procedure_pivot column
 * @method array findByIdTypeProcedureConcessionPivot(int $id_type_procedure_concession_pivot) Return CommonTContratTitulaire objects filtered by the id_type_procedure_concession_pivot column
 * @method array findByIdTypeContratPivot(int $id_type_contrat_pivot) Return CommonTContratTitulaire objects filtered by the id_type_contrat_pivot column
 * @method array findByIdTypeContratConcessionPivot(int $id_type_contrat_concession_pivot) Return CommonTContratTitulaire objects filtered by the id_type_contrat_concession_pivot column
 * @method array findByMontantSubventionPublique(double $montant_subvention_publique) Return CommonTContratTitulaire objects filtered by the montant_subvention_publique column
 * @method array findByDateDebutExecution(string $date_debut_execution) Return CommonTContratTitulaire objects filtered by the date_debut_execution column
 * @method array findByUuid(string $uuid) Return CommonTContratTitulaire objects filtered by the uuid column
 * @method array findByMarcheInnovant(boolean $marche_innovant) Return CommonTContratTitulaire objects filtered by the marche_innovant column
 * @method array findByServiceId(string $service_id) Return CommonTContratTitulaire objects filtered by the service_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTContratTitulaireQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTContratTitulaireQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTContratTitulaire', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTContratTitulaireQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTContratTitulaireQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTContratTitulaireQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTContratTitulaireQuery) {
            return $criteria;
        }
        $query = new CommonTContratTitulaireQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTContratTitulaire|CommonTContratTitulaire[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTContratTitulairePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTContratTitulaire A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdContratTitulaire($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTContratTitulaire A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_contrat_titulaire`, `id_type_contrat`, `id_contact_contrat`, `numero_contrat`, `lien_AC_SAD`, `id_contrat_multi`, `organisme`, `old_service_id`, `id_titulaire`, `id_titulaire_etab`, `id_offre`, `type_depot_reponse`, `objet_contrat`, `intitule`, `montant_contrat`, `id_tranche_budgetaire`, `montant_max_estime`, `publication_montant`, `id_motif_non_publication_montant`, `desc_motif_non_publication_montant`, `publication_contrat`, `num_EJ`, `statutEJ`, `num_long_OEAP`, `reference_libre`, `statut_contrat`, `categorie`, `ccag_applicable`, `clause_sociale`, `clause_sociale_condition_execution`, `clause_sociale_insertion`, `clause_sociale_ateliers_proteges`, `clause_sociale_siae`, `clause_sociale_ess`, `clause_environnementale`, `clause_env_specs_techniques`, `clause_env_cond_execution`, `clause_env_criteres_select`, `date_prevue_notification`, `date_prevue_fin_contrat`, `date_prevue_max_fin_contrat`, `date_notification`, `date_fin_contrat`, `date_max_fin_contrat`, `date_attribution`, `date_creation`, `date_modification`, `envoi_interface`, `contrat_class_key`, `pme_pmi`, `reference_consultation`, `hors_passation`, `id_agent`, `nom_agent`, `prenom_agent`, `lieu_execution`, `code_cpv_1`, `code_cpv_2`, `marche_insertion`, `clause_specification_technique`, `procedure_passation_pivot`, `nom_lieu_principal_execution`, `code_lieu_principal_execution`, `type_code_lieu_principal_execution`, `duree_initiale_contrat`, `forme_prix`, `date_publication_initiale_de`, `num_id_unique_marche_public`, `libelle_type_contrat_pivot`, `siret_pa_accord_cadre`, `ac_marche_subsequent`, `libelle_type_procedure_mpe`, `nb_total_propositions_lot`, `nb_total_propositions_demat_lot`, `marche_defense`, `siret`, `nom_entite_acheteur`, `statut_publication_sn`, `date_publication_sn`, `erreur_sn`, `date_modification_sn`, `id_type_procedure_pivot`, `id_type_procedure_concession_pivot`, `id_type_contrat_pivot`, `id_type_contrat_concession_pivot`, `montant_subvention_publique`, `date_debut_execution`, `uuid`, `marche_innovant`, `service_id` FROM `t_contrat_titulaire` WHERE `id_contrat_titulaire` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $cls = CommonTContratTitulairePeer::getOMClass($row, 0);
            $obj = new $cls();
            $obj->hydrate($row);
            CommonTContratTitulairePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTContratTitulaire|CommonTContratTitulaire[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTContratTitulaire[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_contrat_titulaire column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContratTitulaire(1234); // WHERE id_contrat_titulaire = 1234
     * $query->filterByIdContratTitulaire(array(12, 34)); // WHERE id_contrat_titulaire IN (12, 34)
     * $query->filterByIdContratTitulaire(array('min' => 12)); // WHERE id_contrat_titulaire >= 12
     * $query->filterByIdContratTitulaire(array('max' => 12)); // WHERE id_contrat_titulaire <= 12
     * </code>
     *
     * @param     mixed $idContratTitulaire The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdContratTitulaire($idContratTitulaire = null, $comparison = null)
    {
        if (is_array($idContratTitulaire)) {
            $useMinMax = false;
            if (isset($idContratTitulaire['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContratTitulaire['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire, $comparison);
    }

    /**
     * Filter the query on the id_type_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeContrat(1234); // WHERE id_type_contrat = 1234
     * $query->filterByIdTypeContrat(array(12, 34)); // WHERE id_type_contrat IN (12, 34)
     * $query->filterByIdTypeContrat(array('min' => 12)); // WHERE id_type_contrat >= 12
     * $query->filterByIdTypeContrat(array('max' => 12)); // WHERE id_type_contrat <= 12
     * </code>
     *
     * @see       filterByCommonTTypeContrat()
     *
     * @param     mixed $idTypeContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdTypeContrat($idTypeContrat = null, $comparison = null)
    {
        if (is_array($idTypeContrat)) {
            $useMinMax = false;
            if (isset($idTypeContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, $idTypeContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, $idTypeContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, $idTypeContrat, $comparison);
    }

    /**
     * Filter the query on the id_contact_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContactContrat(1234); // WHERE id_contact_contrat = 1234
     * $query->filterByIdContactContrat(array(12, 34)); // WHERE id_contact_contrat IN (12, 34)
     * $query->filterByIdContactContrat(array('min' => 12)); // WHERE id_contact_contrat >= 12
     * $query->filterByIdContactContrat(array('max' => 12)); // WHERE id_contact_contrat <= 12
     * </code>
     *
     * @see       filterByCommonTContactContrat()
     *
     * @param     mixed $idContactContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdContactContrat($idContactContrat = null, $comparison = null)
    {
        if (is_array($idContactContrat)) {
            $useMinMax = false;
            if (isset($idContactContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, $idContactContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContactContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, $idContactContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, $idContactContrat, $comparison);
    }

    /**
     * Filter the query on the numero_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroContrat('fooValue');   // WHERE numero_contrat = 'fooValue'
     * $query->filterByNumeroContrat('%fooValue%'); // WHERE numero_contrat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numeroContrat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByNumeroContrat($numeroContrat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numeroContrat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numeroContrat)) {
                $numeroContrat = str_replace('*', '%', $numeroContrat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::NUMERO_CONTRAT, $numeroContrat, $comparison);
    }

    /**
     * Filter the query on the lien_AC_SAD column
     *
     * Example usage:
     * <code>
     * $query->filterByLienAcSad(1234); // WHERE lien_AC_SAD = 1234
     * $query->filterByLienAcSad(array(12, 34)); // WHERE lien_AC_SAD IN (12, 34)
     * $query->filterByLienAcSad(array('min' => 12)); // WHERE lien_AC_SAD >= 12
     * $query->filterByLienAcSad(array('max' => 12)); // WHERE lien_AC_SAD <= 12
     * </code>
     *
     * @param     mixed $lienAcSad The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByLienAcSad($lienAcSad = null, $comparison = null)
    {
        if (is_array($lienAcSad)) {
            $useMinMax = false;
            if (isset($lienAcSad['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::LIEN_AC_SAD, $lienAcSad['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lienAcSad['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::LIEN_AC_SAD, $lienAcSad['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::LIEN_AC_SAD, $lienAcSad, $comparison);
    }

    /**
     * Filter the query on the id_contrat_multi column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContratMulti(1234); // WHERE id_contrat_multi = 1234
     * $query->filterByIdContratMulti(array(12, 34)); // WHERE id_contrat_multi IN (12, 34)
     * $query->filterByIdContratMulti(array('min' => 12)); // WHERE id_contrat_multi >= 12
     * $query->filterByIdContratMulti(array('max' => 12)); // WHERE id_contrat_multi <= 12
     * </code>
     *
     * @see       filterByCommonTContratTitulaireRelatedByIdContratMulti()
     *
     * @param     mixed $idContratMulti The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdContratMulti($idContratMulti = null, $comparison = null)
    {
        if (is_array($idContratMulti)) {
            $useMinMax = false;
            if (isset($idContratMulti['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_MULTI, $idContratMulti['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContratMulti['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_MULTI, $idContratMulti['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_MULTI, $idContratMulti, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldServiceId(1234); // WHERE old_service_id = 1234
     * $query->filterByOldServiceId(array(12, 34)); // WHERE old_service_id IN (12, 34)
     * $query->filterByOldServiceId(array('min' => 12)); // WHERE old_service_id >= 12
     * $query->filterByOldServiceId(array('max' => 12)); // WHERE old_service_id <= 12
     * </code>
     *
     * @param     mixed $oldServiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByOldServiceId($oldServiceId = null, $comparison = null)
    {
        if (is_array($oldServiceId)) {
            $useMinMax = false;
            if (isset($oldServiceId['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::OLD_SERVICE_ID, $oldServiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldServiceId['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::OLD_SERVICE_ID, $oldServiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::OLD_SERVICE_ID, $oldServiceId, $comparison);
    }

    /**
     * Filter the query on the id_titulaire column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTitulaire(1234); // WHERE id_titulaire = 1234
     * $query->filterByIdTitulaire(array(12, 34)); // WHERE id_titulaire IN (12, 34)
     * $query->filterByIdTitulaire(array('min' => 12)); // WHERE id_titulaire >= 12
     * $query->filterByIdTitulaire(array('max' => 12)); // WHERE id_titulaire <= 12
     * </code>
     *
     * @param     mixed $idTitulaire The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdTitulaire($idTitulaire = null, $comparison = null)
    {
        if (is_array($idTitulaire)) {
            $useMinMax = false;
            if (isset($idTitulaire['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TITULAIRE, $idTitulaire['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTitulaire['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TITULAIRE, $idTitulaire['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_TITULAIRE, $idTitulaire, $comparison);
    }

    /**
     * Filter the query on the id_titulaire_etab column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTitulaireEtab(1234); // WHERE id_titulaire_etab = 1234
     * $query->filterByIdTitulaireEtab(array(12, 34)); // WHERE id_titulaire_etab IN (12, 34)
     * $query->filterByIdTitulaireEtab(array('min' => 12)); // WHERE id_titulaire_etab >= 12
     * $query->filterByIdTitulaireEtab(array('max' => 12)); // WHERE id_titulaire_etab <= 12
     * </code>
     *
     * @param     mixed $idTitulaireEtab The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdTitulaireEtab($idTitulaireEtab = null, $comparison = null)
    {
        if (is_array($idTitulaireEtab)) {
            $useMinMax = false;
            if (isset($idTitulaireEtab['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TITULAIRE_ETAB, $idTitulaireEtab['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTitulaireEtab['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TITULAIRE_ETAB, $idTitulaireEtab['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_TITULAIRE_ETAB, $idTitulaireEtab, $comparison);
    }

    /**
     * Filter the query on the id_offre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOffre(1234); // WHERE id_offre = 1234
     * $query->filterByIdOffre(array(12, 34)); // WHERE id_offre IN (12, 34)
     * $query->filterByIdOffre(array('min' => 12)); // WHERE id_offre >= 12
     * $query->filterByIdOffre(array('max' => 12)); // WHERE id_offre <= 12
     * </code>
     *
     * @param     mixed $idOffre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdOffre($idOffre = null, $comparison = null)
    {
        if (is_array($idOffre)) {
            $useMinMax = false;
            if (isset($idOffre['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_OFFRE, $idOffre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOffre['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_OFFRE, $idOffre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_OFFRE, $idOffre, $comparison);
    }

    /**
     * Filter the query on the type_depot_reponse column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeDepotReponse('fooValue');   // WHERE type_depot_reponse = 'fooValue'
     * $query->filterByTypeDepotReponse('%fooValue%'); // WHERE type_depot_reponse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeDepotReponse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByTypeDepotReponse($typeDepotReponse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeDepotReponse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeDepotReponse)) {
                $typeDepotReponse = str_replace('*', '%', $typeDepotReponse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::TYPE_DEPOT_REPONSE, $typeDepotReponse, $comparison);
    }

    /**
     * Filter the query on the objet_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByObjetContrat('fooValue');   // WHERE objet_contrat = 'fooValue'
     * $query->filterByObjetContrat('%fooValue%'); // WHERE objet_contrat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objetContrat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByObjetContrat($objetContrat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objetContrat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $objetContrat)) {
                $objetContrat = str_replace('*', '%', $objetContrat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::OBJET_CONTRAT, $objetContrat, $comparison);
    }

    /**
     * Filter the query on the intitule column
     *
     * Example usage:
     * <code>
     * $query->filterByIntitule('fooValue');   // WHERE intitule = 'fooValue'
     * $query->filterByIntitule('%fooValue%'); // WHERE intitule LIKE '%fooValue%'
     * </code>
     *
     * @param     string $intitule The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIntitule($intitule = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($intitule)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $intitule)) {
                $intitule = str_replace('*', '%', $intitule);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::INTITULE, $intitule, $comparison);
    }

    /**
     * Filter the query on the montant_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantContrat(1234); // WHERE montant_contrat = 1234
     * $query->filterByMontantContrat(array(12, 34)); // WHERE montant_contrat IN (12, 34)
     * $query->filterByMontantContrat(array('min' => 12)); // WHERE montant_contrat >= 12
     * $query->filterByMontantContrat(array('max' => 12)); // WHERE montant_contrat <= 12
     * </code>
     *
     * @param     mixed $montantContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByMontantContrat($montantContrat = null, $comparison = null)
    {
        if (is_array($montantContrat)) {
            $useMinMax = false;
            if (isset($montantContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::MONTANT_CONTRAT, $montantContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::MONTANT_CONTRAT, $montantContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::MONTANT_CONTRAT, $montantContrat, $comparison);
    }

    /**
     * Filter the query on the id_tranche_budgetaire column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTrancheBudgetaire(1234); // WHERE id_tranche_budgetaire = 1234
     * $query->filterByIdTrancheBudgetaire(array(12, 34)); // WHERE id_tranche_budgetaire IN (12, 34)
     * $query->filterByIdTrancheBudgetaire(array('min' => 12)); // WHERE id_tranche_budgetaire >= 12
     * $query->filterByIdTrancheBudgetaire(array('max' => 12)); // WHERE id_tranche_budgetaire <= 12
     * </code>
     *
     * @param     mixed $idTrancheBudgetaire The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdTrancheBudgetaire($idTrancheBudgetaire = null, $comparison = null)
    {
        if (is_array($idTrancheBudgetaire)) {
            $useMinMax = false;
            if (isset($idTrancheBudgetaire['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE, $idTrancheBudgetaire['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTrancheBudgetaire['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE, $idTrancheBudgetaire['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE, $idTrancheBudgetaire, $comparison);
    }

    /**
     * Filter the query on the montant_max_estime column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantMaxEstime(1234); // WHERE montant_max_estime = 1234
     * $query->filterByMontantMaxEstime(array(12, 34)); // WHERE montant_max_estime IN (12, 34)
     * $query->filterByMontantMaxEstime(array('min' => 12)); // WHERE montant_max_estime >= 12
     * $query->filterByMontantMaxEstime(array('max' => 12)); // WHERE montant_max_estime <= 12
     * </code>
     *
     * @param     mixed $montantMaxEstime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByMontantMaxEstime($montantMaxEstime = null, $comparison = null)
    {
        if (is_array($montantMaxEstime)) {
            $useMinMax = false;
            if (isset($montantMaxEstime['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::MONTANT_MAX_ESTIME, $montantMaxEstime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantMaxEstime['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::MONTANT_MAX_ESTIME, $montantMaxEstime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::MONTANT_MAX_ESTIME, $montantMaxEstime, $comparison);
    }

    /**
     * Filter the query on the publication_montant column
     *
     * Example usage:
     * <code>
     * $query->filterByPublicationMontant(1234); // WHERE publication_montant = 1234
     * $query->filterByPublicationMontant(array(12, 34)); // WHERE publication_montant IN (12, 34)
     * $query->filterByPublicationMontant(array('min' => 12)); // WHERE publication_montant >= 12
     * $query->filterByPublicationMontant(array('max' => 12)); // WHERE publication_montant <= 12
     * </code>
     *
     * @param     mixed $publicationMontant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByPublicationMontant($publicationMontant = null, $comparison = null)
    {
        if (is_array($publicationMontant)) {
            $useMinMax = false;
            if (isset($publicationMontant['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::PUBLICATION_MONTANT, $publicationMontant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($publicationMontant['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::PUBLICATION_MONTANT, $publicationMontant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::PUBLICATION_MONTANT, $publicationMontant, $comparison);
    }

    /**
     * Filter the query on the id_motif_non_publication_montant column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMotifNonPublicationMontant(1234); // WHERE id_motif_non_publication_montant = 1234
     * $query->filterByIdMotifNonPublicationMontant(array(12, 34)); // WHERE id_motif_non_publication_montant IN (12, 34)
     * $query->filterByIdMotifNonPublicationMontant(array('min' => 12)); // WHERE id_motif_non_publication_montant >= 12
     * $query->filterByIdMotifNonPublicationMontant(array('max' => 12)); // WHERE id_motif_non_publication_montant <= 12
     * </code>
     *
     * @param     mixed $idMotifNonPublicationMontant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdMotifNonPublicationMontant($idMotifNonPublicationMontant = null, $comparison = null)
    {
        if (is_array($idMotifNonPublicationMontant)) {
            $useMinMax = false;
            if (isset($idMotifNonPublicationMontant['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT, $idMotifNonPublicationMontant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMotifNonPublicationMontant['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT, $idMotifNonPublicationMontant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT, $idMotifNonPublicationMontant, $comparison);
    }

    /**
     * Filter the query on the desc_motif_non_publication_montant column
     *
     * Example usage:
     * <code>
     * $query->filterByDescMotifNonPublicationMontant('fooValue');   // WHERE desc_motif_non_publication_montant = 'fooValue'
     * $query->filterByDescMotifNonPublicationMontant('%fooValue%'); // WHERE desc_motif_non_publication_montant LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descMotifNonPublicationMontant The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDescMotifNonPublicationMontant($descMotifNonPublicationMontant = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descMotifNonPublicationMontant)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descMotifNonPublicationMontant)) {
                $descMotifNonPublicationMontant = str_replace('*', '%', $descMotifNonPublicationMontant);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DESC_MOTIF_NON_PUBLICATION_MONTANT, $descMotifNonPublicationMontant, $comparison);
    }

    /**
     * Filter the query on the publication_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByPublicationContrat(1234); // WHERE publication_contrat = 1234
     * $query->filterByPublicationContrat(array(12, 34)); // WHERE publication_contrat IN (12, 34)
     * $query->filterByPublicationContrat(array('min' => 12)); // WHERE publication_contrat >= 12
     * $query->filterByPublicationContrat(array('max' => 12)); // WHERE publication_contrat <= 12
     * </code>
     *
     * @param     mixed $publicationContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByPublicationContrat($publicationContrat = null, $comparison = null)
    {
        if (is_array($publicationContrat)) {
            $useMinMax = false;
            if (isset($publicationContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::PUBLICATION_CONTRAT, $publicationContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($publicationContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::PUBLICATION_CONTRAT, $publicationContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::PUBLICATION_CONTRAT, $publicationContrat, $comparison);
    }

    /**
     * Filter the query on the num_EJ column
     *
     * Example usage:
     * <code>
     * $query->filterByNumEj('fooValue');   // WHERE num_EJ = 'fooValue'
     * $query->filterByNumEj('%fooValue%'); // WHERE num_EJ LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numEj The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByNumEj($numEj = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numEj)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numEj)) {
                $numEj = str_replace('*', '%', $numEj);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::NUM_EJ, $numEj, $comparison);
    }

    /**
     * Filter the query on the statutEJ column
     *
     * Example usage:
     * <code>
     * $query->filterByStatutej('fooValue');   // WHERE statutEJ = 'fooValue'
     * $query->filterByStatutej('%fooValue%'); // WHERE statutEJ LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statutej The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByStatutej($statutej = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statutej)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $statutej)) {
                $statutej = str_replace('*', '%', $statutej);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::STATUTEJ, $statutej, $comparison);
    }

    /**
     * Filter the query on the num_long_OEAP column
     *
     * Example usage:
     * <code>
     * $query->filterByNumLongOeap('fooValue');   // WHERE num_long_OEAP = 'fooValue'
     * $query->filterByNumLongOeap('%fooValue%'); // WHERE num_long_OEAP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numLongOeap The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByNumLongOeap($numLongOeap = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numLongOeap)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numLongOeap)) {
                $numLongOeap = str_replace('*', '%', $numLongOeap);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::NUM_LONG_OEAP, $numLongOeap, $comparison);
    }

    /**
     * Filter the query on the reference_libre column
     *
     * Example usage:
     * <code>
     * $query->filterByReferenceLibre('fooValue');   // WHERE reference_libre = 'fooValue'
     * $query->filterByReferenceLibre('%fooValue%'); // WHERE reference_libre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $referenceLibre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByReferenceLibre($referenceLibre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($referenceLibre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $referenceLibre)) {
                $referenceLibre = str_replace('*', '%', $referenceLibre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::REFERENCE_LIBRE, $referenceLibre, $comparison);
    }

    /**
     * Filter the query on the statut_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByStatutContrat(1234); // WHERE statut_contrat = 1234
     * $query->filterByStatutContrat(array(12, 34)); // WHERE statut_contrat IN (12, 34)
     * $query->filterByStatutContrat(array('min' => 12)); // WHERE statut_contrat >= 12
     * $query->filterByStatutContrat(array('max' => 12)); // WHERE statut_contrat <= 12
     * </code>
     *
     * @param     mixed $statutContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByStatutContrat($statutContrat = null, $comparison = null)
    {
        if (is_array($statutContrat)) {
            $useMinMax = false;
            if (isset($statutContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::STATUT_CONTRAT, $statutContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statutContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::STATUT_CONTRAT, $statutContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::STATUT_CONTRAT, $statutContrat, $comparison);
    }

    /**
     * Filter the query on the categorie column
     *
     * Example usage:
     * <code>
     * $query->filterByCategorie(1234); // WHERE categorie = 1234
     * $query->filterByCategorie(array(12, 34)); // WHERE categorie IN (12, 34)
     * $query->filterByCategorie(array('min' => 12)); // WHERE categorie >= 12
     * $query->filterByCategorie(array('max' => 12)); // WHERE categorie <= 12
     * </code>
     *
     * @param     mixed $categorie The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByCategorie($categorie = null, $comparison = null)
    {
        if (is_array($categorie)) {
            $useMinMax = false;
            if (isset($categorie['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::CATEGORIE, $categorie['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categorie['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::CATEGORIE, $categorie['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CATEGORIE, $categorie, $comparison);
    }

    /**
     * Filter the query on the ccag_applicable column
     *
     * Example usage:
     * <code>
     * $query->filterByCcagApplicable(1234); // WHERE ccag_applicable = 1234
     * $query->filterByCcagApplicable(array(12, 34)); // WHERE ccag_applicable IN (12, 34)
     * $query->filterByCcagApplicable(array('min' => 12)); // WHERE ccag_applicable >= 12
     * $query->filterByCcagApplicable(array('max' => 12)); // WHERE ccag_applicable <= 12
     * </code>
     *
     * @param     mixed $ccagApplicable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByCcagApplicable($ccagApplicable = null, $comparison = null)
    {
        if (is_array($ccagApplicable)) {
            $useMinMax = false;
            if (isset($ccagApplicable['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::CCAG_APPLICABLE, $ccagApplicable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ccagApplicable['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::CCAG_APPLICABLE, $ccagApplicable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CCAG_APPLICABLE, $ccagApplicable, $comparison);
    }

    /**
     * Filter the query on the clause_sociale column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseSociale('fooValue');   // WHERE clause_sociale = 'fooValue'
     * $query->filterByClauseSociale('%fooValue%'); // WHERE clause_sociale LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseSociale The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseSociale($clauseSociale = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseSociale)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseSociale)) {
                $clauseSociale = str_replace('*', '%', $clauseSociale);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_SOCIALE, $clauseSociale, $comparison);
    }

    /**
     * Filter the query on the clause_sociale_condition_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseSocialeConditionExecution('fooValue');   // WHERE clause_sociale_condition_execution = 'fooValue'
     * $query->filterByClauseSocialeConditionExecution('%fooValue%'); // WHERE clause_sociale_condition_execution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseSocialeConditionExecution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseSocialeConditionExecution($clauseSocialeConditionExecution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseSocialeConditionExecution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseSocialeConditionExecution)) {
                $clauseSocialeConditionExecution = str_replace('*', '%', $clauseSocialeConditionExecution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_SOCIALE_CONDITION_EXECUTION, $clauseSocialeConditionExecution, $comparison);
    }

    /**
     * Filter the query on the clause_sociale_insertion column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseSocialeInsertion('fooValue');   // WHERE clause_sociale_insertion = 'fooValue'
     * $query->filterByClauseSocialeInsertion('%fooValue%'); // WHERE clause_sociale_insertion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseSocialeInsertion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseSocialeInsertion($clauseSocialeInsertion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseSocialeInsertion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseSocialeInsertion)) {
                $clauseSocialeInsertion = str_replace('*', '%', $clauseSocialeInsertion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_SOCIALE_INSERTION, $clauseSocialeInsertion, $comparison);
    }

    /**
     * Filter the query on the clause_sociale_ateliers_proteges column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseSocialeAteliersProteges('fooValue');   // WHERE clause_sociale_ateliers_proteges = 'fooValue'
     * $query->filterByClauseSocialeAteliersProteges('%fooValue%'); // WHERE clause_sociale_ateliers_proteges LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseSocialeAteliersProteges The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseSocialeAteliersProteges($clauseSocialeAteliersProteges = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseSocialeAteliersProteges)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseSocialeAteliersProteges)) {
                $clauseSocialeAteliersProteges = str_replace('*', '%', $clauseSocialeAteliersProteges);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES, $clauseSocialeAteliersProteges, $comparison);
    }

    /**
     * Filter the query on the clause_sociale_siae column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseSocialeSiae('fooValue');   // WHERE clause_sociale_siae = 'fooValue'
     * $query->filterByClauseSocialeSiae('%fooValue%'); // WHERE clause_sociale_siae LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseSocialeSiae The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseSocialeSiae($clauseSocialeSiae = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseSocialeSiae)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseSocialeSiae)) {
                $clauseSocialeSiae = str_replace('*', '%', $clauseSocialeSiae);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_SOCIALE_SIAE, $clauseSocialeSiae, $comparison);
    }

    /**
     * Filter the query on the clause_sociale_ess column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseSocialeEss('fooValue');   // WHERE clause_sociale_ess = 'fooValue'
     * $query->filterByClauseSocialeEss('%fooValue%'); // WHERE clause_sociale_ess LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseSocialeEss The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseSocialeEss($clauseSocialeEss = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseSocialeEss)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseSocialeEss)) {
                $clauseSocialeEss = str_replace('*', '%', $clauseSocialeEss);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ESS, $clauseSocialeEss, $comparison);
    }

    /**
     * Filter the query on the clause_environnementale column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseEnvironnementale('fooValue');   // WHERE clause_environnementale = 'fooValue'
     * $query->filterByClauseEnvironnementale('%fooValue%'); // WHERE clause_environnementale LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseEnvironnementale The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseEnvironnementale($clauseEnvironnementale = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseEnvironnementale)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseEnvironnementale)) {
                $clauseEnvironnementale = str_replace('*', '%', $clauseEnvironnementale);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_ENVIRONNEMENTALE, $clauseEnvironnementale, $comparison);
    }

    /**
     * Filter the query on the clause_env_specs_techniques column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseEnvSpecsTechniques('fooValue');   // WHERE clause_env_specs_techniques = 'fooValue'
     * $query->filterByClauseEnvSpecsTechniques('%fooValue%'); // WHERE clause_env_specs_techniques LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseEnvSpecsTechniques The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseEnvSpecsTechniques($clauseEnvSpecsTechniques = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseEnvSpecsTechniques)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseEnvSpecsTechniques)) {
                $clauseEnvSpecsTechniques = str_replace('*', '%', $clauseEnvSpecsTechniques);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_ENV_SPECS_TECHNIQUES, $clauseEnvSpecsTechniques, $comparison);
    }

    /**
     * Filter the query on the clause_env_cond_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseEnvCondExecution('fooValue');   // WHERE clause_env_cond_execution = 'fooValue'
     * $query->filterByClauseEnvCondExecution('%fooValue%'); // WHERE clause_env_cond_execution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseEnvCondExecution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseEnvCondExecution($clauseEnvCondExecution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseEnvCondExecution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseEnvCondExecution)) {
                $clauseEnvCondExecution = str_replace('*', '%', $clauseEnvCondExecution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_ENV_COND_EXECUTION, $clauseEnvCondExecution, $comparison);
    }

    /**
     * Filter the query on the clause_env_criteres_select column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseEnvCriteresSelect('fooValue');   // WHERE clause_env_criteres_select = 'fooValue'
     * $query->filterByClauseEnvCriteresSelect('%fooValue%'); // WHERE clause_env_criteres_select LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseEnvCriteresSelect The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseEnvCriteresSelect($clauseEnvCriteresSelect = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseEnvCriteresSelect)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseEnvCriteresSelect)) {
                $clauseEnvCriteresSelect = str_replace('*', '%', $clauseEnvCriteresSelect);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_ENV_CRITERES_SELECT, $clauseEnvCriteresSelect, $comparison);
    }

    /**
     * Filter the query on the date_prevue_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePrevueNotification('2011-03-14'); // WHERE date_prevue_notification = '2011-03-14'
     * $query->filterByDatePrevueNotification('now'); // WHERE date_prevue_notification = '2011-03-14'
     * $query->filterByDatePrevueNotification(array('max' => 'yesterday')); // WHERE date_prevue_notification > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePrevueNotification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDatePrevueNotification($datePrevueNotification = null, $comparison = null)
    {
        if (is_array($datePrevueNotification)) {
            $useMinMax = false;
            if (isset($datePrevueNotification['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION, $datePrevueNotification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePrevueNotification['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION, $datePrevueNotification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION, $datePrevueNotification, $comparison);
    }

    /**
     * Filter the query on the date_prevue_fin_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePrevueFinContrat('2011-03-14'); // WHERE date_prevue_fin_contrat = '2011-03-14'
     * $query->filterByDatePrevueFinContrat('now'); // WHERE date_prevue_fin_contrat = '2011-03-14'
     * $query->filterByDatePrevueFinContrat(array('max' => 'yesterday')); // WHERE date_prevue_fin_contrat > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePrevueFinContrat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDatePrevueFinContrat($datePrevueFinContrat = null, $comparison = null)
    {
        if (is_array($datePrevueFinContrat)) {
            $useMinMax = false;
            if (isset($datePrevueFinContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT, $datePrevueFinContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePrevueFinContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT, $datePrevueFinContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT, $datePrevueFinContrat, $comparison);
    }

    /**
     * Filter the query on the date_prevue_max_fin_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePrevueMaxFinContrat('2011-03-14'); // WHERE date_prevue_max_fin_contrat = '2011-03-14'
     * $query->filterByDatePrevueMaxFinContrat('now'); // WHERE date_prevue_max_fin_contrat = '2011-03-14'
     * $query->filterByDatePrevueMaxFinContrat(array('max' => 'yesterday')); // WHERE date_prevue_max_fin_contrat > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePrevueMaxFinContrat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDatePrevueMaxFinContrat($datePrevueMaxFinContrat = null, $comparison = null)
    {
        if (is_array($datePrevueMaxFinContrat)) {
            $useMinMax = false;
            if (isset($datePrevueMaxFinContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT, $datePrevueMaxFinContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePrevueMaxFinContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT, $datePrevueMaxFinContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT, $datePrevueMaxFinContrat, $comparison);
    }

    /**
     * Filter the query on the date_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateNotification('2011-03-14'); // WHERE date_notification = '2011-03-14'
     * $query->filterByDateNotification('now'); // WHERE date_notification = '2011-03-14'
     * $query->filterByDateNotification(array('max' => 'yesterday')); // WHERE date_notification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateNotification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDateNotification($dateNotification = null, $comparison = null)
    {
        if (is_array($dateNotification)) {
            $useMinMax = false;
            if (isset($dateNotification['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_NOTIFICATION, $dateNotification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateNotification['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_NOTIFICATION, $dateNotification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_NOTIFICATION, $dateNotification, $comparison);
    }

    /**
     * Filter the query on the date_fin_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFinContrat('2011-03-14'); // WHERE date_fin_contrat = '2011-03-14'
     * $query->filterByDateFinContrat('now'); // WHERE date_fin_contrat = '2011-03-14'
     * $query->filterByDateFinContrat(array('max' => 'yesterday')); // WHERE date_fin_contrat > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFinContrat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDateFinContrat($dateFinContrat = null, $comparison = null)
    {
        if (is_array($dateFinContrat)) {
            $useMinMax = false;
            if (isset($dateFinContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_FIN_CONTRAT, $dateFinContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFinContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_FIN_CONTRAT, $dateFinContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_FIN_CONTRAT, $dateFinContrat, $comparison);
    }

    /**
     * Filter the query on the date_max_fin_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByDateMaxFinContrat('2011-03-14'); // WHERE date_max_fin_contrat = '2011-03-14'
     * $query->filterByDateMaxFinContrat('now'); // WHERE date_max_fin_contrat = '2011-03-14'
     * $query->filterByDateMaxFinContrat(array('max' => 'yesterday')); // WHERE date_max_fin_contrat > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateMaxFinContrat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDateMaxFinContrat($dateMaxFinContrat = null, $comparison = null)
    {
        if (is_array($dateMaxFinContrat)) {
            $useMinMax = false;
            if (isset($dateMaxFinContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT, $dateMaxFinContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateMaxFinContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT, $dateMaxFinContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT, $dateMaxFinContrat, $comparison);
    }

    /**
     * Filter the query on the date_attribution column
     *
     * Example usage:
     * <code>
     * $query->filterByDateAttribution('2011-03-14'); // WHERE date_attribution = '2011-03-14'
     * $query->filterByDateAttribution('now'); // WHERE date_attribution = '2011-03-14'
     * $query->filterByDateAttribution(array('max' => 'yesterday')); // WHERE date_attribution > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateAttribution The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDateAttribution($dateAttribution = null, $comparison = null)
    {
        if (is_array($dateAttribution)) {
            $useMinMax = false;
            if (isset($dateAttribution['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_ATTRIBUTION, $dateAttribution['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateAttribution['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_ATTRIBUTION, $dateAttribution['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_ATTRIBUTION, $dateAttribution, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the envoi_interface column
     *
     * Example usage:
     * <code>
     * $query->filterByEnvoiInterface('fooValue');   // WHERE envoi_interface = 'fooValue'
     * $query->filterByEnvoiInterface('%fooValue%'); // WHERE envoi_interface LIKE '%fooValue%'
     * </code>
     *
     * @param     string $envoiInterface The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByEnvoiInterface($envoiInterface = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($envoiInterface)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $envoiInterface)) {
                $envoiInterface = str_replace('*', '%', $envoiInterface);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ENVOI_INTERFACE, $envoiInterface, $comparison);
    }

    /**
     * Filter the query on the contrat_class_key column
     *
     * Example usage:
     * <code>
     * $query->filterByContratClassKey(1234); // WHERE contrat_class_key = 1234
     * $query->filterByContratClassKey(array(12, 34)); // WHERE contrat_class_key IN (12, 34)
     * $query->filterByContratClassKey(array('min' => 12)); // WHERE contrat_class_key >= 12
     * $query->filterByContratClassKey(array('max' => 12)); // WHERE contrat_class_key <= 12
     * </code>
     *
     * @param     mixed $contratClassKey The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByContratClassKey($contratClassKey = null, $comparison = null)
    {
        if (is_array($contratClassKey)) {
            $useMinMax = false;
            if (isset($contratClassKey['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::CONTRAT_CLASS_KEY, $contratClassKey['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contratClassKey['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::CONTRAT_CLASS_KEY, $contratClassKey['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CONTRAT_CLASS_KEY, $contratClassKey, $comparison);
    }

    /**
     * Filter the query on the pme_pmi column
     *
     * Example usage:
     * <code>
     * $query->filterByPmePmi(1234); // WHERE pme_pmi = 1234
     * $query->filterByPmePmi(array(12, 34)); // WHERE pme_pmi IN (12, 34)
     * $query->filterByPmePmi(array('min' => 12)); // WHERE pme_pmi >= 12
     * $query->filterByPmePmi(array('max' => 12)); // WHERE pme_pmi <= 12
     * </code>
     *
     * @param     mixed $pmePmi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByPmePmi($pmePmi = null, $comparison = null)
    {
        if (is_array($pmePmi)) {
            $useMinMax = false;
            if (isset($pmePmi['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::PME_PMI, $pmePmi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pmePmi['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::PME_PMI, $pmePmi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::PME_PMI, $pmePmi, $comparison);
    }

    /**
     * Filter the query on the reference_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByReferenceConsultation('fooValue');   // WHERE reference_consultation = 'fooValue'
     * $query->filterByReferenceConsultation('%fooValue%'); // WHERE reference_consultation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $referenceConsultation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByReferenceConsultation($referenceConsultation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($referenceConsultation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $referenceConsultation)) {
                $referenceConsultation = str_replace('*', '%', $referenceConsultation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::REFERENCE_CONSULTATION, $referenceConsultation, $comparison);
    }

    /**
     * Filter the query on the hors_passation column
     *
     * Example usage:
     * <code>
     * $query->filterByHorsPassation(1234); // WHERE hors_passation = 1234
     * $query->filterByHorsPassation(array(12, 34)); // WHERE hors_passation IN (12, 34)
     * $query->filterByHorsPassation(array('min' => 12)); // WHERE hors_passation >= 12
     * $query->filterByHorsPassation(array('max' => 12)); // WHERE hors_passation <= 12
     * </code>
     *
     * @param     mixed $horsPassation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByHorsPassation($horsPassation = null, $comparison = null)
    {
        if (is_array($horsPassation)) {
            $useMinMax = false;
            if (isset($horsPassation['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::HORS_PASSATION, $horsPassation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($horsPassation['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::HORS_PASSATION, $horsPassation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::HORS_PASSATION, $horsPassation, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the nom_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByNomAgent('fooValue');   // WHERE nom_agent = 'fooValue'
     * $query->filterByNomAgent('%fooValue%'); // WHERE nom_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomAgent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByNomAgent($nomAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomAgent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomAgent)) {
                $nomAgent = str_replace('*', '%', $nomAgent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::NOM_AGENT, $nomAgent, $comparison);
    }

    /**
     * Filter the query on the prenom_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenomAgent('fooValue');   // WHERE prenom_agent = 'fooValue'
     * $query->filterByPrenomAgent('%fooValue%'); // WHERE prenom_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenomAgent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByPrenomAgent($prenomAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenomAgent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prenomAgent)) {
                $prenomAgent = str_replace('*', '%', $prenomAgent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::PRENOM_AGENT, $prenomAgent, $comparison);
    }

    /**
     * Filter the query on the lieu_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByLieuExecution('fooValue');   // WHERE lieu_execution = 'fooValue'
     * $query->filterByLieuExecution('%fooValue%'); // WHERE lieu_execution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lieuExecution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByLieuExecution($lieuExecution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lieuExecution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lieuExecution)) {
                $lieuExecution = str_replace('*', '%', $lieuExecution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::LIEU_EXECUTION, $lieuExecution, $comparison);
    }

    /**
     * Filter the query on the code_cpv_1 column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeCpv1('fooValue');   // WHERE code_cpv_1 = 'fooValue'
     * $query->filterByCodeCpv1('%fooValue%'); // WHERE code_cpv_1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeCpv1 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByCodeCpv1($codeCpv1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeCpv1)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeCpv1)) {
                $codeCpv1 = str_replace('*', '%', $codeCpv1);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CODE_CPV_1, $codeCpv1, $comparison);
    }

    /**
     * Filter the query on the code_cpv_2 column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeCpv2('fooValue');   // WHERE code_cpv_2 = 'fooValue'
     * $query->filterByCodeCpv2('%fooValue%'); // WHERE code_cpv_2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeCpv2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByCodeCpv2($codeCpv2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeCpv2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeCpv2)) {
                $codeCpv2 = str_replace('*', '%', $codeCpv2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CODE_CPV_2, $codeCpv2, $comparison);
    }

    /**
     * Filter the query on the marche_insertion column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcheInsertion(true); // WHERE marche_insertion = true
     * $query->filterByMarcheInsertion('yes'); // WHERE marche_insertion = true
     * </code>
     *
     * @param     boolean|string $marcheInsertion The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByMarcheInsertion($marcheInsertion = null, $comparison = null)
    {
        if (is_string($marcheInsertion)) {
            $marcheInsertion = in_array(strtolower($marcheInsertion), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::MARCHE_INSERTION, $marcheInsertion, $comparison);
    }

    /**
     * Filter the query on the clause_specification_technique column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseSpecificationTechnique('fooValue');   // WHERE clause_specification_technique = 'fooValue'
     * $query->filterByClauseSpecificationTechnique('%fooValue%'); // WHERE clause_specification_technique LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseSpecificationTechnique The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByClauseSpecificationTechnique($clauseSpecificationTechnique = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseSpecificationTechnique)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseSpecificationTechnique)) {
                $clauseSpecificationTechnique = str_replace('*', '%', $clauseSpecificationTechnique);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CLAUSE_SPECIFICATION_TECHNIQUE, $clauseSpecificationTechnique, $comparison);
    }

    /**
     * Filter the query on the procedure_passation_pivot column
     *
     * Example usage:
     * <code>
     * $query->filterByProcedurePassationPivot('fooValue');   // WHERE procedure_passation_pivot = 'fooValue'
     * $query->filterByProcedurePassationPivot('%fooValue%'); // WHERE procedure_passation_pivot LIKE '%fooValue%'
     * </code>
     *
     * @param     string $procedurePassationPivot The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByProcedurePassationPivot($procedurePassationPivot = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($procedurePassationPivot)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $procedurePassationPivot)) {
                $procedurePassationPivot = str_replace('*', '%', $procedurePassationPivot);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::PROCEDURE_PASSATION_PIVOT, $procedurePassationPivot, $comparison);
    }

    /**
     * Filter the query on the nom_lieu_principal_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByNomLieuPrincipalExecution('fooValue');   // WHERE nom_lieu_principal_execution = 'fooValue'
     * $query->filterByNomLieuPrincipalExecution('%fooValue%'); // WHERE nom_lieu_principal_execution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomLieuPrincipalExecution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByNomLieuPrincipalExecution($nomLieuPrincipalExecution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomLieuPrincipalExecution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomLieuPrincipalExecution)) {
                $nomLieuPrincipalExecution = str_replace('*', '%', $nomLieuPrincipalExecution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::NOM_LIEU_PRINCIPAL_EXECUTION, $nomLieuPrincipalExecution, $comparison);
    }

    /**
     * Filter the query on the code_lieu_principal_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeLieuPrincipalExecution('fooValue');   // WHERE code_lieu_principal_execution = 'fooValue'
     * $query->filterByCodeLieuPrincipalExecution('%fooValue%'); // WHERE code_lieu_principal_execution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeLieuPrincipalExecution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByCodeLieuPrincipalExecution($codeLieuPrincipalExecution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeLieuPrincipalExecution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeLieuPrincipalExecution)) {
                $codeLieuPrincipalExecution = str_replace('*', '%', $codeLieuPrincipalExecution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::CODE_LIEU_PRINCIPAL_EXECUTION, $codeLieuPrincipalExecution, $comparison);
    }

    /**
     * Filter the query on the type_code_lieu_principal_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeCodeLieuPrincipalExecution('fooValue');   // WHERE type_code_lieu_principal_execution = 'fooValue'
     * $query->filterByTypeCodeLieuPrincipalExecution('%fooValue%'); // WHERE type_code_lieu_principal_execution LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeCodeLieuPrincipalExecution The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByTypeCodeLieuPrincipalExecution($typeCodeLieuPrincipalExecution = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeCodeLieuPrincipalExecution)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeCodeLieuPrincipalExecution)) {
                $typeCodeLieuPrincipalExecution = str_replace('*', '%', $typeCodeLieuPrincipalExecution);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::TYPE_CODE_LIEU_PRINCIPAL_EXECUTION, $typeCodeLieuPrincipalExecution, $comparison);
    }

    /**
     * Filter the query on the duree_initiale_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByDureeInitialeContrat(1234); // WHERE duree_initiale_contrat = 1234
     * $query->filterByDureeInitialeContrat(array(12, 34)); // WHERE duree_initiale_contrat IN (12, 34)
     * $query->filterByDureeInitialeContrat(array('min' => 12)); // WHERE duree_initiale_contrat >= 12
     * $query->filterByDureeInitialeContrat(array('max' => 12)); // WHERE duree_initiale_contrat <= 12
     * </code>
     *
     * @param     mixed $dureeInitialeContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDureeInitialeContrat($dureeInitialeContrat = null, $comparison = null)
    {
        if (is_array($dureeInitialeContrat)) {
            $useMinMax = false;
            if (isset($dureeInitialeContrat['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT, $dureeInitialeContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dureeInitialeContrat['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT, $dureeInitialeContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT, $dureeInitialeContrat, $comparison);
    }

    /**
     * Filter the query on the forme_prix column
     *
     * Example usage:
     * <code>
     * $query->filterByFormePrix('fooValue');   // WHERE forme_prix = 'fooValue'
     * $query->filterByFormePrix('%fooValue%'); // WHERE forme_prix LIKE '%fooValue%'
     * </code>
     *
     * @param     string $formePrix The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByFormePrix($formePrix = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($formePrix)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $formePrix)) {
                $formePrix = str_replace('*', '%', $formePrix);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::FORME_PRIX, $formePrix, $comparison);
    }

    /**
     * Filter the query on the date_publication_initiale_de column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePublicationInitialeDe('2011-03-14'); // WHERE date_publication_initiale_de = '2011-03-14'
     * $query->filterByDatePublicationInitialeDe('now'); // WHERE date_publication_initiale_de = '2011-03-14'
     * $query->filterByDatePublicationInitialeDe(array('max' => 'yesterday')); // WHERE date_publication_initiale_de > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePublicationInitialeDe The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDatePublicationInitialeDe($datePublicationInitialeDe = null, $comparison = null)
    {
        if (is_array($datePublicationInitialeDe)) {
            $useMinMax = false;
            if (isset($datePublicationInitialeDe['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE, $datePublicationInitialeDe['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePublicationInitialeDe['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE, $datePublicationInitialeDe['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE, $datePublicationInitialeDe, $comparison);
    }

    /**
     * Filter the query on the num_id_unique_marche_public column
     *
     * Example usage:
     * <code>
     * $query->filterByNumIdUniqueMarchePublic('fooValue');   // WHERE num_id_unique_marche_public = 'fooValue'
     * $query->filterByNumIdUniqueMarchePublic('%fooValue%'); // WHERE num_id_unique_marche_public LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numIdUniqueMarchePublic The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByNumIdUniqueMarchePublic($numIdUniqueMarchePublic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numIdUniqueMarchePublic)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numIdUniqueMarchePublic)) {
                $numIdUniqueMarchePublic = str_replace('*', '%', $numIdUniqueMarchePublic);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::NUM_ID_UNIQUE_MARCHE_PUBLIC, $numIdUniqueMarchePublic, $comparison);
    }

    /**
     * Filter the query on the libelle_type_contrat_pivot column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleTypeContratPivot('fooValue');   // WHERE libelle_type_contrat_pivot = 'fooValue'
     * $query->filterByLibelleTypeContratPivot('%fooValue%'); // WHERE libelle_type_contrat_pivot LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleTypeContratPivot The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByLibelleTypeContratPivot($libelleTypeContratPivot = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleTypeContratPivot)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleTypeContratPivot)) {
                $libelleTypeContratPivot = str_replace('*', '%', $libelleTypeContratPivot);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::LIBELLE_TYPE_CONTRAT_PIVOT, $libelleTypeContratPivot, $comparison);
    }

    /**
     * Filter the query on the siret_pa_accord_cadre column
     *
     * Example usage:
     * <code>
     * $query->filterBySiretPaAccordCadre('fooValue');   // WHERE siret_pa_accord_cadre = 'fooValue'
     * $query->filterBySiretPaAccordCadre('%fooValue%'); // WHERE siret_pa_accord_cadre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siretPaAccordCadre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterBySiretPaAccordCadre($siretPaAccordCadre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siretPaAccordCadre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $siretPaAccordCadre)) {
                $siretPaAccordCadre = str_replace('*', '%', $siretPaAccordCadre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::SIRET_PA_ACCORD_CADRE, $siretPaAccordCadre, $comparison);
    }

    /**
     * Filter the query on the ac_marche_subsequent column
     *
     * Example usage:
     * <code>
     * $query->filterByAcMarcheSubsequent(true); // WHERE ac_marche_subsequent = true
     * $query->filterByAcMarcheSubsequent('yes'); // WHERE ac_marche_subsequent = true
     * </code>
     *
     * @param     boolean|string $acMarcheSubsequent The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByAcMarcheSubsequent($acMarcheSubsequent = null, $comparison = null)
    {
        if (is_string($acMarcheSubsequent)) {
            $acMarcheSubsequent = in_array(strtolower($acMarcheSubsequent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::AC_MARCHE_SUBSEQUENT, $acMarcheSubsequent, $comparison);
    }

    /**
     * Filter the query on the libelle_type_procedure_mpe column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleTypeProcedureMpe('fooValue');   // WHERE libelle_type_procedure_mpe = 'fooValue'
     * $query->filterByLibelleTypeProcedureMpe('%fooValue%'); // WHERE libelle_type_procedure_mpe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleTypeProcedureMpe The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByLibelleTypeProcedureMpe($libelleTypeProcedureMpe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleTypeProcedureMpe)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleTypeProcedureMpe)) {
                $libelleTypeProcedureMpe = str_replace('*', '%', $libelleTypeProcedureMpe);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::LIBELLE_TYPE_PROCEDURE_MPE, $libelleTypeProcedureMpe, $comparison);
    }

    /**
     * Filter the query on the nb_total_propositions_lot column
     *
     * Example usage:
     * <code>
     * $query->filterByNbTotalPropositionsLot(1234); // WHERE nb_total_propositions_lot = 1234
     * $query->filterByNbTotalPropositionsLot(array(12, 34)); // WHERE nb_total_propositions_lot IN (12, 34)
     * $query->filterByNbTotalPropositionsLot(array('min' => 12)); // WHERE nb_total_propositions_lot >= 12
     * $query->filterByNbTotalPropositionsLot(array('max' => 12)); // WHERE nb_total_propositions_lot <= 12
     * </code>
     *
     * @param     mixed $nbTotalPropositionsLot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByNbTotalPropositionsLot($nbTotalPropositionsLot = null, $comparison = null)
    {
        if (is_array($nbTotalPropositionsLot)) {
            $useMinMax = false;
            if (isset($nbTotalPropositionsLot['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT, $nbTotalPropositionsLot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbTotalPropositionsLot['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT, $nbTotalPropositionsLot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT, $nbTotalPropositionsLot, $comparison);
    }

    /**
     * Filter the query on the nb_total_propositions_demat_lot column
     *
     * Example usage:
     * <code>
     * $query->filterByNbTotalPropositionsDematLot(1234); // WHERE nb_total_propositions_demat_lot = 1234
     * $query->filterByNbTotalPropositionsDematLot(array(12, 34)); // WHERE nb_total_propositions_demat_lot IN (12, 34)
     * $query->filterByNbTotalPropositionsDematLot(array('min' => 12)); // WHERE nb_total_propositions_demat_lot >= 12
     * $query->filterByNbTotalPropositionsDematLot(array('max' => 12)); // WHERE nb_total_propositions_demat_lot <= 12
     * </code>
     *
     * @param     mixed $nbTotalPropositionsDematLot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByNbTotalPropositionsDematLot($nbTotalPropositionsDematLot = null, $comparison = null)
    {
        if (is_array($nbTotalPropositionsDematLot)) {
            $useMinMax = false;
            if (isset($nbTotalPropositionsDematLot['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT, $nbTotalPropositionsDematLot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbTotalPropositionsDematLot['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT, $nbTotalPropositionsDematLot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT, $nbTotalPropositionsDematLot, $comparison);
    }

    /**
     * Filter the query on the marche_defense column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcheDefense('fooValue');   // WHERE marche_defense = 'fooValue'
     * $query->filterByMarcheDefense('%fooValue%'); // WHERE marche_defense LIKE '%fooValue%'
     * </code>
     *
     * @param     string $marcheDefense The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByMarcheDefense($marcheDefense = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($marcheDefense)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $marcheDefense)) {
                $marcheDefense = str_replace('*', '%', $marcheDefense);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::MARCHE_DEFENSE, $marcheDefense, $comparison);
    }

    /**
     * Filter the query on the siret column
     *
     * Example usage:
     * <code>
     * $query->filterBySiret('fooValue');   // WHERE siret = 'fooValue'
     * $query->filterBySiret('%fooValue%'); // WHERE siret LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siret The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterBySiret($siret = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siret)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $siret)) {
                $siret = str_replace('*', '%', $siret);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::SIRET, $siret, $comparison);
    }

    /**
     * Filter the query on the nom_entite_acheteur column
     *
     * Example usage:
     * <code>
     * $query->filterByNomEntiteAcheteur('fooValue');   // WHERE nom_entite_acheteur = 'fooValue'
     * $query->filterByNomEntiteAcheteur('%fooValue%'); // WHERE nom_entite_acheteur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomEntiteAcheteur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByNomEntiteAcheteur($nomEntiteAcheteur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomEntiteAcheteur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomEntiteAcheteur)) {
                $nomEntiteAcheteur = str_replace('*', '%', $nomEntiteAcheteur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::NOM_ENTITE_ACHETEUR, $nomEntiteAcheteur, $comparison);
    }

    /**
     * Filter the query on the statut_publication_sn column
     *
     * Example usage:
     * <code>
     * $query->filterByStatutPublicationSn(1234); // WHERE statut_publication_sn = 1234
     * $query->filterByStatutPublicationSn(array(12, 34)); // WHERE statut_publication_sn IN (12, 34)
     * $query->filterByStatutPublicationSn(array('min' => 12)); // WHERE statut_publication_sn >= 12
     * $query->filterByStatutPublicationSn(array('max' => 12)); // WHERE statut_publication_sn <= 12
     * </code>
     *
     * @param     mixed $statutPublicationSn The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByStatutPublicationSn($statutPublicationSn = null, $comparison = null)
    {
        if (is_array($statutPublicationSn)) {
            $useMinMax = false;
            if (isset($statutPublicationSn['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::STATUT_PUBLICATION_SN, $statutPublicationSn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statutPublicationSn['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::STATUT_PUBLICATION_SN, $statutPublicationSn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::STATUT_PUBLICATION_SN, $statutPublicationSn, $comparison);
    }

    /**
     * Filter the query on the date_publication_sn column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePublicationSn('2011-03-14'); // WHERE date_publication_sn = '2011-03-14'
     * $query->filterByDatePublicationSn('now'); // WHERE date_publication_sn = '2011-03-14'
     * $query->filterByDatePublicationSn(array('max' => 'yesterday')); // WHERE date_publication_sn > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePublicationSn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDatePublicationSn($datePublicationSn = null, $comparison = null)
    {
        if (is_array($datePublicationSn)) {
            $useMinMax = false;
            if (isset($datePublicationSn['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PUBLICATION_SN, $datePublicationSn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePublicationSn['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PUBLICATION_SN, $datePublicationSn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_PUBLICATION_SN, $datePublicationSn, $comparison);
    }

    /**
     * Filter the query on the erreur_sn column
     *
     * Example usage:
     * <code>
     * $query->filterByErreurSn('fooValue');   // WHERE erreur_sn = 'fooValue'
     * $query->filterByErreurSn('%fooValue%'); // WHERE erreur_sn LIKE '%fooValue%'
     * </code>
     *
     * @param     string $erreurSn The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByErreurSn($erreurSn = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($erreurSn)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $erreurSn)) {
                $erreurSn = str_replace('*', '%', $erreurSn);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ERREUR_SN, $erreurSn, $comparison);
    }

    /**
     * Filter the query on the date_modification_sn column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModificationSn('2011-03-14'); // WHERE date_modification_sn = '2011-03-14'
     * $query->filterByDateModificationSn('now'); // WHERE date_modification_sn = '2011-03-14'
     * $query->filterByDateModificationSn(array('max' => 'yesterday')); // WHERE date_modification_sn > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModificationSn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDateModificationSn($dateModificationSn = null, $comparison = null)
    {
        if (is_array($dateModificationSn)) {
            $useMinMax = false;
            if (isset($dateModificationSn['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_MODIFICATION_SN, $dateModificationSn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModificationSn['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_MODIFICATION_SN, $dateModificationSn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_MODIFICATION_SN, $dateModificationSn, $comparison);
    }

    /**
     * Filter the query on the id_type_procedure_pivot column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeProcedurePivot(1234); // WHERE id_type_procedure_pivot = 1234
     * $query->filterByIdTypeProcedurePivot(array(12, 34)); // WHERE id_type_procedure_pivot IN (12, 34)
     * $query->filterByIdTypeProcedurePivot(array('min' => 12)); // WHERE id_type_procedure_pivot >= 12
     * $query->filterByIdTypeProcedurePivot(array('max' => 12)); // WHERE id_type_procedure_pivot <= 12
     * </code>
     *
     * @see       filterByCommonTypeProcedure()
     *
     * @param     mixed $idTypeProcedurePivot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdTypeProcedurePivot($idTypeProcedurePivot = null, $comparison = null)
    {
        if (is_array($idTypeProcedurePivot)) {
            $useMinMax = false;
            if (isset($idTypeProcedurePivot['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, $idTypeProcedurePivot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeProcedurePivot['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, $idTypeProcedurePivot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, $idTypeProcedurePivot, $comparison);
    }

    /**
     * Filter the query on the id_type_procedure_concession_pivot column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeProcedureConcessionPivot(1234); // WHERE id_type_procedure_concession_pivot = 1234
     * $query->filterByIdTypeProcedureConcessionPivot(array(12, 34)); // WHERE id_type_procedure_concession_pivot IN (12, 34)
     * $query->filterByIdTypeProcedureConcessionPivot(array('min' => 12)); // WHERE id_type_procedure_concession_pivot >= 12
     * $query->filterByIdTypeProcedureConcessionPivot(array('max' => 12)); // WHERE id_type_procedure_concession_pivot <= 12
     * </code>
     *
     * @see       filterByCommonTypeProcedureConcessionPivot()
     *
     * @param     mixed $idTypeProcedureConcessionPivot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdTypeProcedureConcessionPivot($idTypeProcedureConcessionPivot = null, $comparison = null)
    {
        if (is_array($idTypeProcedureConcessionPivot)) {
            $useMinMax = false;
            if (isset($idTypeProcedureConcessionPivot['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, $idTypeProcedureConcessionPivot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeProcedureConcessionPivot['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, $idTypeProcedureConcessionPivot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, $idTypeProcedureConcessionPivot, $comparison);
    }

    /**
     * Filter the query on the id_type_contrat_pivot column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeContratPivot(1234); // WHERE id_type_contrat_pivot = 1234
     * $query->filterByIdTypeContratPivot(array(12, 34)); // WHERE id_type_contrat_pivot IN (12, 34)
     * $query->filterByIdTypeContratPivot(array('min' => 12)); // WHERE id_type_contrat_pivot >= 12
     * $query->filterByIdTypeContratPivot(array('max' => 12)); // WHERE id_type_contrat_pivot <= 12
     * </code>
     *
     * @see       filterByCommonTypeContratPivot()
     *
     * @param     mixed $idTypeContratPivot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdTypeContratPivot($idTypeContratPivot = null, $comparison = null)
    {
        if (is_array($idTypeContratPivot)) {
            $useMinMax = false;
            if (isset($idTypeContratPivot['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, $idTypeContratPivot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeContratPivot['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, $idTypeContratPivot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, $idTypeContratPivot, $comparison);
    }

    /**
     * Filter the query on the id_type_contrat_concession_pivot column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeContratConcessionPivot(1234); // WHERE id_type_contrat_concession_pivot = 1234
     * $query->filterByIdTypeContratConcessionPivot(array(12, 34)); // WHERE id_type_contrat_concession_pivot IN (12, 34)
     * $query->filterByIdTypeContratConcessionPivot(array('min' => 12)); // WHERE id_type_contrat_concession_pivot >= 12
     * $query->filterByIdTypeContratConcessionPivot(array('max' => 12)); // WHERE id_type_contrat_concession_pivot <= 12
     * </code>
     *
     * @see       filterByCommonTypeContratConcessionPivot()
     *
     * @param     mixed $idTypeContratConcessionPivot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByIdTypeContratConcessionPivot($idTypeContratConcessionPivot = null, $comparison = null)
    {
        if (is_array($idTypeContratConcessionPivot)) {
            $useMinMax = false;
            if (isset($idTypeContratConcessionPivot['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, $idTypeContratConcessionPivot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeContratConcessionPivot['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, $idTypeContratConcessionPivot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, $idTypeContratConcessionPivot, $comparison);
    }

    /**
     * Filter the query on the montant_subvention_publique column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantSubventionPublique(1234); // WHERE montant_subvention_publique = 1234
     * $query->filterByMontantSubventionPublique(array(12, 34)); // WHERE montant_subvention_publique IN (12, 34)
     * $query->filterByMontantSubventionPublique(array('min' => 12)); // WHERE montant_subvention_publique >= 12
     * $query->filterByMontantSubventionPublique(array('max' => 12)); // WHERE montant_subvention_publique <= 12
     * </code>
     *
     * @param     mixed $montantSubventionPublique The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByMontantSubventionPublique($montantSubventionPublique = null, $comparison = null)
    {
        if (is_array($montantSubventionPublique)) {
            $useMinMax = false;
            if (isset($montantSubventionPublique['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE, $montantSubventionPublique['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantSubventionPublique['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE, $montantSubventionPublique['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE, $montantSubventionPublique, $comparison);
    }

    /**
     * Filter the query on the date_debut_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDebutExecution('2011-03-14'); // WHERE date_debut_execution = '2011-03-14'
     * $query->filterByDateDebutExecution('now'); // WHERE date_debut_execution = '2011-03-14'
     * $query->filterByDateDebutExecution(array('max' => 'yesterday')); // WHERE date_debut_execution > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDebutExecution The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByDateDebutExecution($dateDebutExecution = null, $comparison = null)
    {
        if (is_array($dateDebutExecution)) {
            $useMinMax = false;
            if (isset($dateDebutExecution['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION, $dateDebutExecution['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDebutExecution['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION, $dateDebutExecution['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION, $dateDebutExecution, $comparison);
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%'); // WHERE uuid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuid The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $uuid)) {
                $uuid = str_replace('*', '%', $uuid);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::UUID, $uuid, $comparison);
    }

    /**
     * Filter the query on the marche_innovant column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcheInnovant(true); // WHERE marche_innovant = true
     * $query->filterByMarcheInnovant('yes'); // WHERE marche_innovant = true
     * </code>
     *
     * @param     boolean|string $marcheInnovant The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByMarcheInnovant($marcheInnovant = null, $comparison = null)
    {
        if (is_string($marcheInnovant)) {
            $marcheInnovant = in_array(strtolower($marcheInnovant), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::MARCHE_INNOVANT, $marcheInnovant, $comparison);
    }

    /**
     * Filter the query on the service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceId(1234); // WHERE service_id = 1234
     * $query->filterByServiceId(array(12, 34)); // WHERE service_id IN (12, 34)
     * $query->filterByServiceId(array('min' => 12)); // WHERE service_id >= 12
     * $query->filterByServiceId(array('max' => 12)); // WHERE service_id <= 12
     * </code>
     *
     * @param     mixed $serviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function filterByServiceId($serviceId = null, $comparison = null)
    {
        if (is_array($serviceId)) {
            $useMinMax = false;
            if (isset($serviceId['min'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::SERVICE_ID, $serviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceId['max'])) {
                $this->addUsingAlias(CommonTContratTitulairePeer::SERVICE_ID, $serviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulairePeer::SERVICE_ID, $serviceId, $comparison);
    }

    /**
     * Filter the query by a related CommonTypeContratConcessionPivot object
     *
     * @param   CommonTypeContratConcessionPivot|PropelObjectCollection $commonTypeContratConcessionPivot The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTypeContratConcessionPivot($commonTypeContratConcessionPivot, $comparison = null)
    {
        if ($commonTypeContratConcessionPivot instanceof CommonTypeContratConcessionPivot) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, $commonTypeContratConcessionPivot->getId(), $comparison);
        } elseif ($commonTypeContratConcessionPivot instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, $commonTypeContratConcessionPivot->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTypeContratConcessionPivot() only accepts arguments of type CommonTypeContratConcessionPivot or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTypeContratConcessionPivot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTypeContratConcessionPivot($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTypeContratConcessionPivot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTypeContratConcessionPivot');
        }

        return $this;
    }

    /**
     * Use the CommonTypeContratConcessionPivot relation CommonTypeContratConcessionPivot object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTypeContratConcessionPivotQuery A secondary query class using the current class as primary query
     */
    public function useCommonTypeContratConcessionPivotQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTypeContratConcessionPivot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTypeContratConcessionPivot', '\Application\Propel\Mpe\CommonTypeContratConcessionPivotQuery');
    }

    /**
     * Filter the query by a related CommonTypeContratPivot object
     *
     * @param   CommonTypeContratPivot|PropelObjectCollection $commonTypeContratPivot The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTypeContratPivot($commonTypeContratPivot, $comparison = null)
    {
        if ($commonTypeContratPivot instanceof CommonTypeContratPivot) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, $commonTypeContratPivot->getId(), $comparison);
        } elseif ($commonTypeContratPivot instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, $commonTypeContratPivot->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTypeContratPivot() only accepts arguments of type CommonTypeContratPivot or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTypeContratPivot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTypeContratPivot($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTypeContratPivot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTypeContratPivot');
        }

        return $this;
    }

    /**
     * Use the CommonTypeContratPivot relation CommonTypeContratPivot object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTypeContratPivotQuery A secondary query class using the current class as primary query
     */
    public function useCommonTypeContratPivotQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTypeContratPivot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTypeContratPivot', '\Application\Propel\Mpe\CommonTypeContratPivotQuery');
    }

    /**
     * Filter the query by a related CommonTypeProcedureConcessionPivot object
     *
     * @param   CommonTypeProcedureConcessionPivot|PropelObjectCollection $commonTypeProcedureConcessionPivot The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTypeProcedureConcessionPivot($commonTypeProcedureConcessionPivot, $comparison = null)
    {
        if ($commonTypeProcedureConcessionPivot instanceof CommonTypeProcedureConcessionPivot) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, $commonTypeProcedureConcessionPivot->getId(), $comparison);
        } elseif ($commonTypeProcedureConcessionPivot instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, $commonTypeProcedureConcessionPivot->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTypeProcedureConcessionPivot() only accepts arguments of type CommonTypeProcedureConcessionPivot or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTypeProcedureConcessionPivot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTypeProcedureConcessionPivot($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTypeProcedureConcessionPivot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTypeProcedureConcessionPivot');
        }

        return $this;
    }

    /**
     * Use the CommonTypeProcedureConcessionPivot relation CommonTypeProcedureConcessionPivot object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTypeProcedureConcessionPivotQuery A secondary query class using the current class as primary query
     */
    public function useCommonTypeProcedureConcessionPivotQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTypeProcedureConcessionPivot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTypeProcedureConcessionPivot', '\Application\Propel\Mpe\CommonTypeProcedureConcessionPivotQuery');
    }

    /**
     * Filter the query by a related CommonTContactContrat object
     *
     * @param   CommonTContactContrat|PropelObjectCollection $commonTContactContrat The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContactContrat($commonTContactContrat, $comparison = null)
    {
        if ($commonTContactContrat instanceof CommonTContactContrat) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, $commonTContactContrat->getIdContactContrat(), $comparison);
        } elseif ($commonTContactContrat instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, $commonTContactContrat->toKeyValue('PrimaryKey', 'IdContactContrat'), $comparison);
        } else {
            throw new PropelException('filterByCommonTContactContrat() only accepts arguments of type CommonTContactContrat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContactContrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTContactContrat($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContactContrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContactContrat');
        }

        return $this;
    }

    /**
     * Use the CommonTContactContrat relation CommonTContactContrat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContactContratQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContactContratQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContactContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContactContrat', '\Application\Propel\Mpe\CommonTContactContratQuery');
    }

    /**
     * Filter the query by a related CommonTContratTitulaire object
     *
     * @param   CommonTContratTitulaire|PropelObjectCollection $commonTContratTitulaire The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContratTitulaireRelatedByIdContratMulti($commonTContratTitulaire, $comparison = null)
    {
        if ($commonTContratTitulaire instanceof CommonTContratTitulaire) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_MULTI, $commonTContratTitulaire->getIdContratTitulaire(), $comparison);
        } elseif ($commonTContratTitulaire instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_MULTI, $commonTContratTitulaire->toKeyValue('PrimaryKey', 'IdContratTitulaire'), $comparison);
        } else {
            throw new PropelException('filterByCommonTContratTitulaireRelatedByIdContratMulti() only accepts arguments of type CommonTContratTitulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContratTitulaireRelatedByIdContratMulti relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTContratTitulaireRelatedByIdContratMulti($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContratTitulaireRelatedByIdContratMulti');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContratTitulaireRelatedByIdContratMulti');
        }

        return $this;
    }

    /**
     * Use the CommonTContratTitulaireRelatedByIdContratMulti relation CommonTContratTitulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContratTitulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContratTitulaireRelatedByIdContratMultiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContratTitulaireRelatedByIdContratMulti($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContratTitulaireRelatedByIdContratMulti', '\Application\Propel\Mpe\CommonTContratTitulaireQuery');
    }

    /**
     * Filter the query by a related CommonTTypeContrat object
     *
     * @param   CommonTTypeContrat|PropelObjectCollection $commonTTypeContrat The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTTypeContrat($commonTTypeContrat, $comparison = null)
    {
        if ($commonTTypeContrat instanceof CommonTTypeContrat) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, $commonTTypeContrat->getIdTypeContrat(), $comparison);
        } elseif ($commonTTypeContrat instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, $commonTTypeContrat->toKeyValue('PrimaryKey', 'IdTypeContrat'), $comparison);
        } else {
            throw new PropelException('filterByCommonTTypeContrat() only accepts arguments of type CommonTTypeContrat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTTypeContrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTTypeContrat($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTTypeContrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTTypeContrat');
        }

        return $this;
    }

    /**
     * Use the CommonTTypeContrat relation CommonTTypeContrat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTTypeContratQuery A secondary query class using the current class as primary query
     */
    public function useCommonTTypeContratQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTTypeContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTTypeContrat', '\Application\Propel\Mpe\CommonTTypeContratQuery');
    }

    /**
     * Filter the query by a related CommonTypeProcedure object
     *
     * @param   CommonTypeProcedure|PropelObjectCollection $commonTypeProcedure The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTypeProcedure($commonTypeProcedure, $comparison = null)
    {
        if ($commonTypeProcedure instanceof CommonTypeProcedure) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, $commonTypeProcedure->getIdTypeProcedure(), $comparison);
        } elseif ($commonTypeProcedure instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, $commonTypeProcedure->toKeyValue('PrimaryKey', 'IdTypeProcedure'), $comparison);
        } else {
            throw new PropelException('filterByCommonTypeProcedure() only accepts arguments of type CommonTypeProcedure or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTypeProcedure relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTypeProcedure($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTypeProcedure');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTypeProcedure');
        }

        return $this;
    }

    /**
     * Use the CommonTypeProcedure relation CommonTypeProcedure object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTypeProcedureQuery A secondary query class using the current class as primary query
     */
    public function useCommonTypeProcedureQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTypeProcedure($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTypeProcedure', '\Application\Propel\Mpe\CommonTypeProcedureQuery');
    }

    /**
     * Filter the query by a related CommonMarche object
     *
     * @param   CommonMarche|PropelObjectCollection $commonMarche  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonMarche($commonMarche, $comparison = null)
    {
        if ($commonMarche instanceof CommonMarche) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $commonMarche->getIdContratTitulaire(), $comparison);
        } elseif ($commonMarche instanceof PropelObjectCollection) {
            return $this
                ->useCommonMarcheQuery()
                ->filterByPrimaryKeys($commonMarche->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonMarche() only accepts arguments of type CommonMarche or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonMarche relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonMarche($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonMarche');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonMarche');
        }

        return $this;
    }

    /**
     * Use the CommonMarche relation CommonMarche object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonMarcheQuery A secondary query class using the current class as primary query
     */
    public function useCommonMarcheQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonMarche($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonMarche', '\Application\Propel\Mpe\CommonMarcheQuery');
    }

    /**
     * Filter the query by a related CommonDonneesAnnuellesConcession object
     *
     * @param   CommonDonneesAnnuellesConcession|PropelObjectCollection $commonDonneesAnnuellesConcession  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonDonneesAnnuellesConcession($commonDonneesAnnuellesConcession, $comparison = null)
    {
        if ($commonDonneesAnnuellesConcession instanceof CommonDonneesAnnuellesConcession) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $commonDonneesAnnuellesConcession->getIdContrat(), $comparison);
        } elseif ($commonDonneesAnnuellesConcession instanceof PropelObjectCollection) {
            return $this
                ->useCommonDonneesAnnuellesConcessionQuery()
                ->filterByPrimaryKeys($commonDonneesAnnuellesConcession->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonDonneesAnnuellesConcession() only accepts arguments of type CommonDonneesAnnuellesConcession or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonDonneesAnnuellesConcession relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonDonneesAnnuellesConcession($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonDonneesAnnuellesConcession');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonDonneesAnnuellesConcession');
        }

        return $this;
    }

    /**
     * Use the CommonDonneesAnnuellesConcession relation CommonDonneesAnnuellesConcession object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonDonneesAnnuellesConcessionQuery A secondary query class using the current class as primary query
     */
    public function useCommonDonneesAnnuellesConcessionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonDonneesAnnuellesConcession($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonDonneesAnnuellesConcession', '\Application\Propel\Mpe\CommonDonneesAnnuellesConcessionQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDoc object
     *
     * @param   CommonEchangeDoc|PropelObjectCollection $commonEchangeDoc  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDoc($commonEchangeDoc, $comparison = null)
    {
        if ($commonEchangeDoc instanceof CommonEchangeDoc) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $commonEchangeDoc->getIdContratTitulaire(), $comparison);
        } elseif ($commonEchangeDoc instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocQuery()
                ->filterByPrimaryKeys($commonEchangeDoc->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDoc() only accepts arguments of type CommonEchangeDoc or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDoc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDoc($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDoc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDoc');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDoc relation CommonEchangeDoc object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDoc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDoc', '\Application\Propel\Mpe\CommonEchangeDocQuery');
    }

    /**
     * Filter the query by a related CommonModificationContrat object
     *
     * @param   CommonModificationContrat|PropelObjectCollection $commonModificationContrat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonModificationContrat($commonModificationContrat, $comparison = null)
    {
        if ($commonModificationContrat instanceof CommonModificationContrat) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $commonModificationContrat->getIdContratTitulaire(), $comparison);
        } elseif ($commonModificationContrat instanceof PropelObjectCollection) {
            return $this
                ->useCommonModificationContratQuery()
                ->filterByPrimaryKeys($commonModificationContrat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonModificationContrat() only accepts arguments of type CommonModificationContrat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonModificationContrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonModificationContrat($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonModificationContrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonModificationContrat');
        }

        return $this;
    }

    /**
     * Use the CommonModificationContrat relation CommonModificationContrat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonModificationContratQuery A secondary query class using the current class as primary query
     */
    public function useCommonModificationContratQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonModificationContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonModificationContrat', '\Application\Propel\Mpe\CommonModificationContratQuery');
    }

    /**
     * Filter the query by a related CommonTConsLotContrat object
     *
     * @param   CommonTConsLotContrat|PropelObjectCollection $commonTConsLotContrat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTConsLotContrat($commonTConsLotContrat, $comparison = null)
    {
        if ($commonTConsLotContrat instanceof CommonTConsLotContrat) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $commonTConsLotContrat->getIdContratTitulaire(), $comparison);
        } elseif ($commonTConsLotContrat instanceof PropelObjectCollection) {
            return $this
                ->useCommonTConsLotContratQuery()
                ->filterByPrimaryKeys($commonTConsLotContrat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTConsLotContrat() only accepts arguments of type CommonTConsLotContrat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTConsLotContrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTConsLotContrat($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTConsLotContrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTConsLotContrat');
        }

        return $this;
    }

    /**
     * Use the CommonTConsLotContrat relation CommonTConsLotContrat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTConsLotContratQuery A secondary query class using the current class as primary query
     */
    public function useCommonTConsLotContratQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTConsLotContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTConsLotContrat', '\Application\Propel\Mpe\CommonTConsLotContratQuery');
    }

    /**
     * Filter the query by a related CommonTContratTitulaire object
     *
     * @param   CommonTContratTitulaire|PropelObjectCollection $commonTContratTitulaire  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContratTitulaireRelatedByIdContratTitulaire($commonTContratTitulaire, $comparison = null)
    {
        if ($commonTContratTitulaire instanceof CommonTContratTitulaire) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $commonTContratTitulaire->getIdContratMulti(), $comparison);
        } elseif ($commonTContratTitulaire instanceof PropelObjectCollection) {
            return $this
                ->useCommonTContratTitulaireRelatedByIdContratTitulaireQuery()
                ->filterByPrimaryKeys($commonTContratTitulaire->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTContratTitulaireRelatedByIdContratTitulaire() only accepts arguments of type CommonTContratTitulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContratTitulaireRelatedByIdContratTitulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTContratTitulaireRelatedByIdContratTitulaire($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContratTitulaireRelatedByIdContratTitulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContratTitulaireRelatedByIdContratTitulaire');
        }

        return $this;
    }

    /**
     * Use the CommonTContratTitulaireRelatedByIdContratTitulaire relation CommonTContratTitulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContratTitulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContratTitulaireRelatedByIdContratTitulaireQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContratTitulaireRelatedByIdContratTitulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContratTitulaireRelatedByIdContratTitulaire', '\Application\Propel\Mpe\CommonTContratTitulaireQuery');
    }

    /**
     * Filter the query by a related CommonTDonneesConsultation object
     *
     * @param   CommonTDonneesConsultation|PropelObjectCollection $commonTDonneesConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTContratTitulaireQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTDonneesConsultation($commonTDonneesConsultation, $comparison = null)
    {
        if ($commonTDonneesConsultation instanceof CommonTDonneesConsultation) {
            return $this
                ->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $commonTDonneesConsultation->getIdContratTitulaire(), $comparison);
        } elseif ($commonTDonneesConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonTDonneesConsultationQuery()
                ->filterByPrimaryKeys($commonTDonneesConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTDonneesConsultation() only accepts arguments of type CommonTDonneesConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTDonneesConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function joinCommonTDonneesConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTDonneesConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTDonneesConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonTDonneesConsultation relation CommonTDonneesConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTDonneesConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonTDonneesConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTDonneesConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTDonneesConsultation', '\Application\Propel\Mpe\CommonTDonneesConsultationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTContratTitulaire $commonTContratTitulaire Object to remove from the list of results
     *
     * @return CommonTContratTitulaireQuery The current query, for fluid interface
     */
    public function prune($commonTContratTitulaire = null)
    {
        if ($commonTContratTitulaire) {
            $this->addUsingAlias(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $commonTContratTitulaire->getIdContratTitulaire(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
