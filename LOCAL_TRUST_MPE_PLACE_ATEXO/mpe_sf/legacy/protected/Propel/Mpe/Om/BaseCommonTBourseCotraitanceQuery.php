<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTBourseCotraitance;
use Application\Propel\Mpe\CommonTBourseCotraitancePeer;
use Application\Propel\Mpe\CommonTBourseCotraitanceQuery;

/**
 * Base class that represents a query for the 't_bourse_cotraitance' table.
 *
 *
 *
 * @method CommonTBourseCotraitanceQuery orderByIdAutoBc($order = Criteria::ASC) Order by the id_auto_BC column
 * @method CommonTBourseCotraitanceQuery orderByReferenceConsultation($order = Criteria::ASC) Order by the reference_consultation column
 * @method CommonTBourseCotraitanceQuery orderByIdEntreprise($order = Criteria::ASC) Order by the id_entreprise column
 * @method CommonTBourseCotraitanceQuery orderByIdEtablissementInscrite($order = Criteria::ASC) Order by the id_etablissement_inscrite column
 * @method CommonTBourseCotraitanceQuery orderByNomInscrit($order = Criteria::ASC) Order by the nom_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByPrenomInscrit($order = Criteria::ASC) Order by the prenom_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByAdresseInscrit($order = Criteria::ASC) Order by the adresse_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByAdresse2Incsrit($order = Criteria::ASC) Order by the adresse2_incsrit column
 * @method CommonTBourseCotraitanceQuery orderByCpInscrit($order = Criteria::ASC) Order by the cp_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByVilleInscrit($order = Criteria::ASC) Order by the ville_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByPaysInscrit($order = Criteria::ASC) Order by the pays_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByFonctionInscrit($order = Criteria::ASC) Order by the fonction_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByEmailInscrit($order = Criteria::ASC) Order by the email_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByTelFixeInscrit($order = Criteria::ASC) Order by the tel_fixe_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByTelMobileInscrit($order = Criteria::ASC) Order by the tel_mobile_inscrit column
 * @method CommonTBourseCotraitanceQuery orderByMandataireGroupement($order = Criteria::ASC) Order by the mandataire_groupement column
 * @method CommonTBourseCotraitanceQuery orderByCotraitantSolidaire($order = Criteria::ASC) Order by the cotraitant_solidaire column
 * @method CommonTBourseCotraitanceQuery orderByCotraitantConjoint($order = Criteria::ASC) Order by the cotraitant_conjoint column
 * @method CommonTBourseCotraitanceQuery orderByDescMonApportMarche($order = Criteria::ASC) Order by the desc_mon_apport_marche column
 * @method CommonTBourseCotraitanceQuery orderByDescTypeCotraitanceRecherche($order = Criteria::ASC) Order by the desc_type_cotraitance_recherche column
 * @method CommonTBourseCotraitanceQuery orderByClauseSocial($order = Criteria::ASC) Order by the clause_social column
 * @method CommonTBourseCotraitanceQuery orderByEntrepriseAdapte($order = Criteria::ASC) Order by the entreprise_adapte column
 * @method CommonTBourseCotraitanceQuery orderByLong($order = Criteria::ASC) Order by the long column
 * @method CommonTBourseCotraitanceQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method CommonTBourseCotraitanceQuery orderByMajLongLat($order = Criteria::ASC) Order by the maj_long_lat column
 * @method CommonTBourseCotraitanceQuery orderBySousTraitant($order = Criteria::ASC) Order by the sous_traitant column
 *
 * @method CommonTBourseCotraitanceQuery groupByIdAutoBc() Group by the id_auto_BC column
 * @method CommonTBourseCotraitanceQuery groupByReferenceConsultation() Group by the reference_consultation column
 * @method CommonTBourseCotraitanceQuery groupByIdEntreprise() Group by the id_entreprise column
 * @method CommonTBourseCotraitanceQuery groupByIdEtablissementInscrite() Group by the id_etablissement_inscrite column
 * @method CommonTBourseCotraitanceQuery groupByNomInscrit() Group by the nom_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByPrenomInscrit() Group by the prenom_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByAdresseInscrit() Group by the adresse_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByAdresse2Incsrit() Group by the adresse2_incsrit column
 * @method CommonTBourseCotraitanceQuery groupByCpInscrit() Group by the cp_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByVilleInscrit() Group by the ville_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByPaysInscrit() Group by the pays_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByFonctionInscrit() Group by the fonction_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByEmailInscrit() Group by the email_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByTelFixeInscrit() Group by the tel_fixe_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByTelMobileInscrit() Group by the tel_mobile_inscrit column
 * @method CommonTBourseCotraitanceQuery groupByMandataireGroupement() Group by the mandataire_groupement column
 * @method CommonTBourseCotraitanceQuery groupByCotraitantSolidaire() Group by the cotraitant_solidaire column
 * @method CommonTBourseCotraitanceQuery groupByCotraitantConjoint() Group by the cotraitant_conjoint column
 * @method CommonTBourseCotraitanceQuery groupByDescMonApportMarche() Group by the desc_mon_apport_marche column
 * @method CommonTBourseCotraitanceQuery groupByDescTypeCotraitanceRecherche() Group by the desc_type_cotraitance_recherche column
 * @method CommonTBourseCotraitanceQuery groupByClauseSocial() Group by the clause_social column
 * @method CommonTBourseCotraitanceQuery groupByEntrepriseAdapte() Group by the entreprise_adapte column
 * @method CommonTBourseCotraitanceQuery groupByLong() Group by the long column
 * @method CommonTBourseCotraitanceQuery groupByLat() Group by the lat column
 * @method CommonTBourseCotraitanceQuery groupByMajLongLat() Group by the maj_long_lat column
 * @method CommonTBourseCotraitanceQuery groupBySousTraitant() Group by the sous_traitant column
 *
 * @method CommonTBourseCotraitanceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTBourseCotraitanceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTBourseCotraitanceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTBourseCotraitance findOne(PropelPDO $con = null) Return the first CommonTBourseCotraitance matching the query
 * @method CommonTBourseCotraitance findOneOrCreate(PropelPDO $con = null) Return the first CommonTBourseCotraitance matching the query, or a new CommonTBourseCotraitance object populated from the query conditions when no match is found
 *
 * @method CommonTBourseCotraitance findOneByReferenceConsultation(int $reference_consultation) Return the first CommonTBourseCotraitance filtered by the reference_consultation column
 * @method CommonTBourseCotraitance findOneByIdEntreprise(int $id_entreprise) Return the first CommonTBourseCotraitance filtered by the id_entreprise column
 * @method CommonTBourseCotraitance findOneByIdEtablissementInscrite(int $id_etablissement_inscrite) Return the first CommonTBourseCotraitance filtered by the id_etablissement_inscrite column
 * @method CommonTBourseCotraitance findOneByNomInscrit(string $nom_inscrit) Return the first CommonTBourseCotraitance filtered by the nom_inscrit column
 * @method CommonTBourseCotraitance findOneByPrenomInscrit(string $prenom_inscrit) Return the first CommonTBourseCotraitance filtered by the prenom_inscrit column
 * @method CommonTBourseCotraitance findOneByAdresseInscrit(string $adresse_inscrit) Return the first CommonTBourseCotraitance filtered by the adresse_inscrit column
 * @method CommonTBourseCotraitance findOneByAdresse2Incsrit(string $adresse2_incsrit) Return the first CommonTBourseCotraitance filtered by the adresse2_incsrit column
 * @method CommonTBourseCotraitance findOneByCpInscrit(string $cp_inscrit) Return the first CommonTBourseCotraitance filtered by the cp_inscrit column
 * @method CommonTBourseCotraitance findOneByVilleInscrit(string $ville_inscrit) Return the first CommonTBourseCotraitance filtered by the ville_inscrit column
 * @method CommonTBourseCotraitance findOneByPaysInscrit(string $pays_inscrit) Return the first CommonTBourseCotraitance filtered by the pays_inscrit column
 * @method CommonTBourseCotraitance findOneByFonctionInscrit(string $fonction_inscrit) Return the first CommonTBourseCotraitance filtered by the fonction_inscrit column
 * @method CommonTBourseCotraitance findOneByEmailInscrit(string $email_inscrit) Return the first CommonTBourseCotraitance filtered by the email_inscrit column
 * @method CommonTBourseCotraitance findOneByTelFixeInscrit(string $tel_fixe_inscrit) Return the first CommonTBourseCotraitance filtered by the tel_fixe_inscrit column
 * @method CommonTBourseCotraitance findOneByTelMobileInscrit(string $tel_mobile_inscrit) Return the first CommonTBourseCotraitance filtered by the tel_mobile_inscrit column
 * @method CommonTBourseCotraitance findOneByMandataireGroupement(string $mandataire_groupement) Return the first CommonTBourseCotraitance filtered by the mandataire_groupement column
 * @method CommonTBourseCotraitance findOneByCotraitantSolidaire(string $cotraitant_solidaire) Return the first CommonTBourseCotraitance filtered by the cotraitant_solidaire column
 * @method CommonTBourseCotraitance findOneByCotraitantConjoint(string $cotraitant_conjoint) Return the first CommonTBourseCotraitance filtered by the cotraitant_conjoint column
 * @method CommonTBourseCotraitance findOneByDescMonApportMarche(string $desc_mon_apport_marche) Return the first CommonTBourseCotraitance filtered by the desc_mon_apport_marche column
 * @method CommonTBourseCotraitance findOneByDescTypeCotraitanceRecherche(string $desc_type_cotraitance_recherche) Return the first CommonTBourseCotraitance filtered by the desc_type_cotraitance_recherche column
 * @method CommonTBourseCotraitance findOneByClauseSocial(string $clause_social) Return the first CommonTBourseCotraitance filtered by the clause_social column
 * @method CommonTBourseCotraitance findOneByEntrepriseAdapte(string $entreprise_adapte) Return the first CommonTBourseCotraitance filtered by the entreprise_adapte column
 * @method CommonTBourseCotraitance findOneByLong(double $long) Return the first CommonTBourseCotraitance filtered by the long column
 * @method CommonTBourseCotraitance findOneByLat(double $lat) Return the first CommonTBourseCotraitance filtered by the lat column
 * @method CommonTBourseCotraitance findOneByMajLongLat(string $maj_long_lat) Return the first CommonTBourseCotraitance filtered by the maj_long_lat column
 * @method CommonTBourseCotraitance findOneBySousTraitant(string $sous_traitant) Return the first CommonTBourseCotraitance filtered by the sous_traitant column
 *
 * @method array findByIdAutoBc(int $id_auto_BC) Return CommonTBourseCotraitance objects filtered by the id_auto_BC column
 * @method array findByReferenceConsultation(int $reference_consultation) Return CommonTBourseCotraitance objects filtered by the reference_consultation column
 * @method array findByIdEntreprise(int $id_entreprise) Return CommonTBourseCotraitance objects filtered by the id_entreprise column
 * @method array findByIdEtablissementInscrite(int $id_etablissement_inscrite) Return CommonTBourseCotraitance objects filtered by the id_etablissement_inscrite column
 * @method array findByNomInscrit(string $nom_inscrit) Return CommonTBourseCotraitance objects filtered by the nom_inscrit column
 * @method array findByPrenomInscrit(string $prenom_inscrit) Return CommonTBourseCotraitance objects filtered by the prenom_inscrit column
 * @method array findByAdresseInscrit(string $adresse_inscrit) Return CommonTBourseCotraitance objects filtered by the adresse_inscrit column
 * @method array findByAdresse2Incsrit(string $adresse2_incsrit) Return CommonTBourseCotraitance objects filtered by the adresse2_incsrit column
 * @method array findByCpInscrit(string $cp_inscrit) Return CommonTBourseCotraitance objects filtered by the cp_inscrit column
 * @method array findByVilleInscrit(string $ville_inscrit) Return CommonTBourseCotraitance objects filtered by the ville_inscrit column
 * @method array findByPaysInscrit(string $pays_inscrit) Return CommonTBourseCotraitance objects filtered by the pays_inscrit column
 * @method array findByFonctionInscrit(string $fonction_inscrit) Return CommonTBourseCotraitance objects filtered by the fonction_inscrit column
 * @method array findByEmailInscrit(string $email_inscrit) Return CommonTBourseCotraitance objects filtered by the email_inscrit column
 * @method array findByTelFixeInscrit(string $tel_fixe_inscrit) Return CommonTBourseCotraitance objects filtered by the tel_fixe_inscrit column
 * @method array findByTelMobileInscrit(string $tel_mobile_inscrit) Return CommonTBourseCotraitance objects filtered by the tel_mobile_inscrit column
 * @method array findByMandataireGroupement(string $mandataire_groupement) Return CommonTBourseCotraitance objects filtered by the mandataire_groupement column
 * @method array findByCotraitantSolidaire(string $cotraitant_solidaire) Return CommonTBourseCotraitance objects filtered by the cotraitant_solidaire column
 * @method array findByCotraitantConjoint(string $cotraitant_conjoint) Return CommonTBourseCotraitance objects filtered by the cotraitant_conjoint column
 * @method array findByDescMonApportMarche(string $desc_mon_apport_marche) Return CommonTBourseCotraitance objects filtered by the desc_mon_apport_marche column
 * @method array findByDescTypeCotraitanceRecherche(string $desc_type_cotraitance_recherche) Return CommonTBourseCotraitance objects filtered by the desc_type_cotraitance_recherche column
 * @method array findByClauseSocial(string $clause_social) Return CommonTBourseCotraitance objects filtered by the clause_social column
 * @method array findByEntrepriseAdapte(string $entreprise_adapte) Return CommonTBourseCotraitance objects filtered by the entreprise_adapte column
 * @method array findByLong(double $long) Return CommonTBourseCotraitance objects filtered by the long column
 * @method array findByLat(double $lat) Return CommonTBourseCotraitance objects filtered by the lat column
 * @method array findByMajLongLat(string $maj_long_lat) Return CommonTBourseCotraitance objects filtered by the maj_long_lat column
 * @method array findBySousTraitant(string $sous_traitant) Return CommonTBourseCotraitance objects filtered by the sous_traitant column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTBourseCotraitanceQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTBourseCotraitanceQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTBourseCotraitance', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTBourseCotraitanceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTBourseCotraitanceQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTBourseCotraitanceQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTBourseCotraitanceQuery) {
            return $criteria;
        }
        $query = new CommonTBourseCotraitanceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTBourseCotraitance|CommonTBourseCotraitance[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTBourseCotraitancePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTBourseCotraitancePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTBourseCotraitance A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdAutoBc($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTBourseCotraitance A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_auto_BC`, `reference_consultation`, `id_entreprise`, `id_etablissement_inscrite`, `nom_inscrit`, `prenom_inscrit`, `adresse_inscrit`, `adresse2_incsrit`, `cp_inscrit`, `ville_inscrit`, `pays_inscrit`, `fonction_inscrit`, `email_inscrit`, `tel_fixe_inscrit`, `tel_mobile_inscrit`, `mandataire_groupement`, `cotraitant_solidaire`, `cotraitant_conjoint`, `desc_mon_apport_marche`, `desc_type_cotraitance_recherche`, `clause_social`, `entreprise_adapte`, `long`, `lat`, `maj_long_lat`, `sous_traitant` FROM `t_bourse_cotraitance` WHERE `id_auto_BC` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTBourseCotraitance();
            $obj->hydrate($row);
            CommonTBourseCotraitancePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTBourseCotraitance|CommonTBourseCotraitance[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTBourseCotraitance[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_AUTO_BC, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_AUTO_BC, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_auto_BC column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAutoBc(1234); // WHERE id_auto_BC = 1234
     * $query->filterByIdAutoBc(array(12, 34)); // WHERE id_auto_BC IN (12, 34)
     * $query->filterByIdAutoBc(array('min' => 12)); // WHERE id_auto_BC >= 12
     * $query->filterByIdAutoBc(array('max' => 12)); // WHERE id_auto_BC <= 12
     * </code>
     *
     * @param     mixed $idAutoBc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByIdAutoBc($idAutoBc = null, $comparison = null)
    {
        if (is_array($idAutoBc)) {
            $useMinMax = false;
            if (isset($idAutoBc['min'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_AUTO_BC, $idAutoBc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAutoBc['max'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_AUTO_BC, $idAutoBc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_AUTO_BC, $idAutoBc, $comparison);
    }

    /**
     * Filter the query on the reference_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByReferenceConsultation(1234); // WHERE reference_consultation = 1234
     * $query->filterByReferenceConsultation(array(12, 34)); // WHERE reference_consultation IN (12, 34)
     * $query->filterByReferenceConsultation(array('min' => 12)); // WHERE reference_consultation >= 12
     * $query->filterByReferenceConsultation(array('max' => 12)); // WHERE reference_consultation <= 12
     * </code>
     *
     * @param     mixed $referenceConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByReferenceConsultation($referenceConsultation = null, $comparison = null)
    {
        if (is_array($referenceConsultation)) {
            $useMinMax = false;
            if (isset($referenceConsultation['min'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION, $referenceConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($referenceConsultation['max'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION, $referenceConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::REFERENCE_CONSULTATION, $referenceConsultation, $comparison);
    }

    /**
     * Filter the query on the id_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntreprise(1234); // WHERE id_entreprise = 1234
     * $query->filterByIdEntreprise(array(12, 34)); // WHERE id_entreprise IN (12, 34)
     * $query->filterByIdEntreprise(array('min' => 12)); // WHERE id_entreprise >= 12
     * $query->filterByIdEntreprise(array('max' => 12)); // WHERE id_entreprise <= 12
     * </code>
     *
     * @param     mixed $idEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByIdEntreprise($idEntreprise = null, $comparison = null)
    {
        if (is_array($idEntreprise)) {
            $useMinMax = false;
            if (isset($idEntreprise['min'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_ENTREPRISE, $idEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntreprise['max'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_ENTREPRISE, $idEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_ENTREPRISE, $idEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_etablissement_inscrite column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtablissementInscrite(1234); // WHERE id_etablissement_inscrite = 1234
     * $query->filterByIdEtablissementInscrite(array(12, 34)); // WHERE id_etablissement_inscrite IN (12, 34)
     * $query->filterByIdEtablissementInscrite(array('min' => 12)); // WHERE id_etablissement_inscrite >= 12
     * $query->filterByIdEtablissementInscrite(array('max' => 12)); // WHERE id_etablissement_inscrite <= 12
     * </code>
     *
     * @param     mixed $idEtablissementInscrite The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByIdEtablissementInscrite($idEtablissementInscrite = null, $comparison = null)
    {
        if (is_array($idEtablissementInscrite)) {
            $useMinMax = false;
            if (isset($idEtablissementInscrite['min'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE, $idEtablissementInscrite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtablissementInscrite['max'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE, $idEtablissementInscrite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_ETABLISSEMENT_INSCRITE, $idEtablissementInscrite, $comparison);
    }

    /**
     * Filter the query on the nom_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByNomInscrit('fooValue');   // WHERE nom_inscrit = 'fooValue'
     * $query->filterByNomInscrit('%fooValue%'); // WHERE nom_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByNomInscrit($nomInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomInscrit)) {
                $nomInscrit = str_replace('*', '%', $nomInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::NOM_INSCRIT, $nomInscrit, $comparison);
    }

    /**
     * Filter the query on the prenom_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenomInscrit('fooValue');   // WHERE prenom_inscrit = 'fooValue'
     * $query->filterByPrenomInscrit('%fooValue%'); // WHERE prenom_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenomInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByPrenomInscrit($prenomInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenomInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prenomInscrit)) {
                $prenomInscrit = str_replace('*', '%', $prenomInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::PRENOM_INSCRIT, $prenomInscrit, $comparison);
    }

    /**
     * Filter the query on the adresse_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresseInscrit('fooValue');   // WHERE adresse_inscrit = 'fooValue'
     * $query->filterByAdresseInscrit('%fooValue%'); // WHERE adresse_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresseInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByAdresseInscrit($adresseInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresseInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adresseInscrit)) {
                $adresseInscrit = str_replace('*', '%', $adresseInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::ADRESSE_INSCRIT, $adresseInscrit, $comparison);
    }

    /**
     * Filter the query on the adresse2_incsrit column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse2Incsrit('fooValue');   // WHERE adresse2_incsrit = 'fooValue'
     * $query->filterByAdresse2Incsrit('%fooValue%'); // WHERE adresse2_incsrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresse2Incsrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByAdresse2Incsrit($adresse2Incsrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse2Incsrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adresse2Incsrit)) {
                $adresse2Incsrit = str_replace('*', '%', $adresse2Incsrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::ADRESSE2_INCSRIT, $adresse2Incsrit, $comparison);
    }

    /**
     * Filter the query on the cp_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByCpInscrit('fooValue');   // WHERE cp_inscrit = 'fooValue'
     * $query->filterByCpInscrit('%fooValue%'); // WHERE cp_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cpInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByCpInscrit($cpInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cpInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cpInscrit)) {
                $cpInscrit = str_replace('*', '%', $cpInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::CP_INSCRIT, $cpInscrit, $comparison);
    }

    /**
     * Filter the query on the ville_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByVilleInscrit('fooValue');   // WHERE ville_inscrit = 'fooValue'
     * $query->filterByVilleInscrit('%fooValue%'); // WHERE ville_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $villeInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByVilleInscrit($villeInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($villeInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $villeInscrit)) {
                $villeInscrit = str_replace('*', '%', $villeInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::VILLE_INSCRIT, $villeInscrit, $comparison);
    }

    /**
     * Filter the query on the pays_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByPaysInscrit('fooValue');   // WHERE pays_inscrit = 'fooValue'
     * $query->filterByPaysInscrit('%fooValue%'); // WHERE pays_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paysInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByPaysInscrit($paysInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paysInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paysInscrit)) {
                $paysInscrit = str_replace('*', '%', $paysInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::PAYS_INSCRIT, $paysInscrit, $comparison);
    }

    /**
     * Filter the query on the fonction_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByFonctionInscrit('fooValue');   // WHERE fonction_inscrit = 'fooValue'
     * $query->filterByFonctionInscrit('%fooValue%'); // WHERE fonction_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fonctionInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByFonctionInscrit($fonctionInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fonctionInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fonctionInscrit)) {
                $fonctionInscrit = str_replace('*', '%', $fonctionInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::FONCTION_INSCRIT, $fonctionInscrit, $comparison);
    }

    /**
     * Filter the query on the email_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailInscrit('fooValue');   // WHERE email_inscrit = 'fooValue'
     * $query->filterByEmailInscrit('%fooValue%'); // WHERE email_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByEmailInscrit($emailInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $emailInscrit)) {
                $emailInscrit = str_replace('*', '%', $emailInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::EMAIL_INSCRIT, $emailInscrit, $comparison);
    }

    /**
     * Filter the query on the tel_fixe_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByTelFixeInscrit('fooValue');   // WHERE tel_fixe_inscrit = 'fooValue'
     * $query->filterByTelFixeInscrit('%fooValue%'); // WHERE tel_fixe_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telFixeInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByTelFixeInscrit($telFixeInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telFixeInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $telFixeInscrit)) {
                $telFixeInscrit = str_replace('*', '%', $telFixeInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::TEL_FIXE_INSCRIT, $telFixeInscrit, $comparison);
    }

    /**
     * Filter the query on the tel_mobile_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByTelMobileInscrit('fooValue');   // WHERE tel_mobile_inscrit = 'fooValue'
     * $query->filterByTelMobileInscrit('%fooValue%'); // WHERE tel_mobile_inscrit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telMobileInscrit The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByTelMobileInscrit($telMobileInscrit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telMobileInscrit)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $telMobileInscrit)) {
                $telMobileInscrit = str_replace('*', '%', $telMobileInscrit);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::TEL_MOBILE_INSCRIT, $telMobileInscrit, $comparison);
    }

    /**
     * Filter the query on the mandataire_groupement column
     *
     * Example usage:
     * <code>
     * $query->filterByMandataireGroupement('fooValue');   // WHERE mandataire_groupement = 'fooValue'
     * $query->filterByMandataireGroupement('%fooValue%'); // WHERE mandataire_groupement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mandataireGroupement The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByMandataireGroupement($mandataireGroupement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mandataireGroupement)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mandataireGroupement)) {
                $mandataireGroupement = str_replace('*', '%', $mandataireGroupement);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::MANDATAIRE_GROUPEMENT, $mandataireGroupement, $comparison);
    }

    /**
     * Filter the query on the cotraitant_solidaire column
     *
     * Example usage:
     * <code>
     * $query->filterByCotraitantSolidaire('fooValue');   // WHERE cotraitant_solidaire = 'fooValue'
     * $query->filterByCotraitantSolidaire('%fooValue%'); // WHERE cotraitant_solidaire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cotraitantSolidaire The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByCotraitantSolidaire($cotraitantSolidaire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cotraitantSolidaire)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cotraitantSolidaire)) {
                $cotraitantSolidaire = str_replace('*', '%', $cotraitantSolidaire);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::COTRAITANT_SOLIDAIRE, $cotraitantSolidaire, $comparison);
    }

    /**
     * Filter the query on the cotraitant_conjoint column
     *
     * Example usage:
     * <code>
     * $query->filterByCotraitantConjoint('fooValue');   // WHERE cotraitant_conjoint = 'fooValue'
     * $query->filterByCotraitantConjoint('%fooValue%'); // WHERE cotraitant_conjoint LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cotraitantConjoint The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByCotraitantConjoint($cotraitantConjoint = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cotraitantConjoint)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cotraitantConjoint)) {
                $cotraitantConjoint = str_replace('*', '%', $cotraitantConjoint);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::COTRAITANT_CONJOINT, $cotraitantConjoint, $comparison);
    }

    /**
     * Filter the query on the desc_mon_apport_marche column
     *
     * Example usage:
     * <code>
     * $query->filterByDescMonApportMarche('fooValue');   // WHERE desc_mon_apport_marche = 'fooValue'
     * $query->filterByDescMonApportMarche('%fooValue%'); // WHERE desc_mon_apport_marche LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descMonApportMarche The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByDescMonApportMarche($descMonApportMarche = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descMonApportMarche)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descMonApportMarche)) {
                $descMonApportMarche = str_replace('*', '%', $descMonApportMarche);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::DESC_MON_APPORT_MARCHE, $descMonApportMarche, $comparison);
    }

    /**
     * Filter the query on the desc_type_cotraitance_recherche column
     *
     * Example usage:
     * <code>
     * $query->filterByDescTypeCotraitanceRecherche('fooValue');   // WHERE desc_type_cotraitance_recherche = 'fooValue'
     * $query->filterByDescTypeCotraitanceRecherche('%fooValue%'); // WHERE desc_type_cotraitance_recherche LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descTypeCotraitanceRecherche The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByDescTypeCotraitanceRecherche($descTypeCotraitanceRecherche = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descTypeCotraitanceRecherche)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descTypeCotraitanceRecherche)) {
                $descTypeCotraitanceRecherche = str_replace('*', '%', $descTypeCotraitanceRecherche);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::DESC_TYPE_COTRAITANCE_RECHERCHE, $descTypeCotraitanceRecherche, $comparison);
    }

    /**
     * Filter the query on the clause_social column
     *
     * Example usage:
     * <code>
     * $query->filterByClauseSocial('fooValue');   // WHERE clause_social = 'fooValue'
     * $query->filterByClauseSocial('%fooValue%'); // WHERE clause_social LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clauseSocial The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByClauseSocial($clauseSocial = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clauseSocial)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $clauseSocial)) {
                $clauseSocial = str_replace('*', '%', $clauseSocial);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::CLAUSE_SOCIAL, $clauseSocial, $comparison);
    }

    /**
     * Filter the query on the entreprise_adapte column
     *
     * Example usage:
     * <code>
     * $query->filterByEntrepriseAdapte('fooValue');   // WHERE entreprise_adapte = 'fooValue'
     * $query->filterByEntrepriseAdapte('%fooValue%'); // WHERE entreprise_adapte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $entrepriseAdapte The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByEntrepriseAdapte($entrepriseAdapte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($entrepriseAdapte)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $entrepriseAdapte)) {
                $entrepriseAdapte = str_replace('*', '%', $entrepriseAdapte);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::ENTREPRISE_ADAPTE, $entrepriseAdapte, $comparison);
    }

    /**
     * Filter the query on the long column
     *
     * Example usage:
     * <code>
     * $query->filterByLong(1234); // WHERE long = 1234
     * $query->filterByLong(array(12, 34)); // WHERE long IN (12, 34)
     * $query->filterByLong(array('min' => 12)); // WHERE long >= 12
     * $query->filterByLong(array('max' => 12)); // WHERE long <= 12
     * </code>
     *
     * @param     mixed $long The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByLong($long = null, $comparison = null)
    {
        if (is_array($long)) {
            $useMinMax = false;
            if (isset($long['min'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::LONG, $long['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($long['max'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::LONG, $long['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::LONG, $long, $comparison);
    }

    /**
     * Filter the query on the lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE lat >= 12
     * $query->filterByLat(array('max' => 12)); // WHERE lat <= 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the maj_long_lat column
     *
     * Example usage:
     * <code>
     * $query->filterByMajLongLat('2011-03-14'); // WHERE maj_long_lat = '2011-03-14'
     * $query->filterByMajLongLat('now'); // WHERE maj_long_lat = '2011-03-14'
     * $query->filterByMajLongLat(array('max' => 'yesterday')); // WHERE maj_long_lat > '2011-03-13'
     * </code>
     *
     * @param     mixed $majLongLat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterByMajLongLat($majLongLat = null, $comparison = null)
    {
        if (is_array($majLongLat)) {
            $useMinMax = false;
            if (isset($majLongLat['min'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::MAJ_LONG_LAT, $majLongLat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($majLongLat['max'])) {
                $this->addUsingAlias(CommonTBourseCotraitancePeer::MAJ_LONG_LAT, $majLongLat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::MAJ_LONG_LAT, $majLongLat, $comparison);
    }

    /**
     * Filter the query on the sous_traitant column
     *
     * Example usage:
     * <code>
     * $query->filterBySousTraitant('fooValue');   // WHERE sous_traitant = 'fooValue'
     * $query->filterBySousTraitant('%fooValue%'); // WHERE sous_traitant LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sousTraitant The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function filterBySousTraitant($sousTraitant = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sousTraitant)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sousTraitant)) {
                $sousTraitant = str_replace('*', '%', $sousTraitant);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTBourseCotraitancePeer::SOUS_TRAITANT, $sousTraitant, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTBourseCotraitance $commonTBourseCotraitance Object to remove from the list of results
     *
     * @return CommonTBourseCotraitanceQuery The current query, for fluid interface
     */
    public function prune($commonTBourseCotraitance = null)
    {
        if ($commonTBourseCotraitance) {
            $this->addUsingAlias(CommonTBourseCotraitancePeer::ID_AUTO_BC, $commonTBourseCotraitance->getIdAutoBc(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
