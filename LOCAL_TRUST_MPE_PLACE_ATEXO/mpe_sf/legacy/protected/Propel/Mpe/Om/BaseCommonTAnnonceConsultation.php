<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTAnnonceConsultationQuery;
use Application\Propel\Mpe\CommonTHistoriqueAnnonce;
use Application\Propel\Mpe\CommonTHistoriqueAnnonceQuery;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationQuery;
use Application\Propel\Mpe\CommonTTypeAnnonceConsultation;
use Application\Propel\Mpe\CommonTTypeAnnonceConsultationQuery;

/**
 * Base class that represents a row from the 't_annonce_consultation' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTAnnonceConsultation extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTAnnonceConsultationPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTAnnonceConsultationPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the ref_consultation field.
     * @var        int
     */
    protected $ref_consultation;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the id_dossier_sub field.
     * @var        int
     */
    protected $id_dossier_sub;

    /**
     * The value for the id_dispositif field.
     * @var        int
     */
    protected $id_dispositif;

    /**
     * The value for the id_compte_boamp field.
     * @var        int
     */
    protected $id_compte_boamp;

    /**
     * The value for the statut field.
     * @var        string
     */
    protected $statut;

    /**
     * The value for the id_type_annonce field.
     * @var        int
     */
    protected $id_type_annonce;

    /**
     * The value for the id_agent field.
     * @var        int
     */
    protected $id_agent;

    /**
     * The value for the prenom_nom_agent field.
     * @var        string
     */
    protected $prenom_nom_agent;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the date_statut field.
     * @var        string
     */
    protected $date_statut;

    /**
     * The value for the motif_statut field.
     * @var        string
     */
    protected $motif_statut;

    /**
     * The value for the id_dossier_parent field.
     * @var        int
     */
    protected $id_dossier_parent;

    /**
     * The value for the id_dispositif_parent field.
     * @var        int
     */
    protected $id_dispositif_parent;

    /**
     * The value for the publicite_simplifie field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $publicite_simplifie;

    /**
     * The value for the consultation_id field.
     * @var        int
     */
    protected $consultation_id;

    /**
     * @var        CommonConsultation
     */
    protected $aCommonConsultation;

    /**
     * @var        CommonTTypeAnnonceConsultation
     */
    protected $aCommonTTypeAnnonceConsultation;

    /**
     * @var        PropelObjectCollection|CommonTHistoriqueAnnonce[] Collection to store aggregation of CommonTHistoriqueAnnonce objects.
     */
    protected $collCommonTHistoriqueAnnonces;
    protected $collCommonTHistoriqueAnnoncesPartial;

    /**
     * @var        PropelObjectCollection|CommonTSupportAnnonceConsultation[] Collection to store aggregation of CommonTSupportAnnonceConsultation objects.
     */
    protected $collCommonTSupportAnnonceConsultations;
    protected $collCommonTSupportAnnonceConsultationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTHistoriqueAnnoncesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTSupportAnnonceConsultationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->publicite_simplifie = '0';
    }

    /**
     * Initializes internal state of BaseCommonTAnnonceConsultation object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [ref_consultation] column value.
     *
     * @return int
     */
    public function getRefConsultation()
    {

        return $this->ref_consultation;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [id_dossier_sub] column value.
     *
     * @return int
     */
    public function getIdDossierSub()
    {

        return $this->id_dossier_sub;
    }

    /**
     * Get the [id_dispositif] column value.
     *
     * @return int
     */
    public function getIdDispositif()
    {

        return $this->id_dispositif;
    }

    /**
     * Get the [id_compte_boamp] column value.
     *
     * @return int
     */
    public function getIdCompteBoamp()
    {

        return $this->id_compte_boamp;
    }

    /**
     * Get the [statut] column value.
     *
     * @return string
     */
    public function getStatut()
    {

        return $this->statut;
    }

    /**
     * Get the [id_type_annonce] column value.
     *
     * @return int
     */
    public function getIdTypeAnnonce()
    {

        return $this->id_type_annonce;
    }

    /**
     * Get the [id_agent] column value.
     *
     * @return int
     */
    public function getIdAgent()
    {

        return $this->id_agent;
    }

    /**
     * Get the [prenom_nom_agent] column value.
     *
     * @return string
     */
    public function getPrenomNomAgent()
    {

        return $this->prenom_nom_agent;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_modification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModification($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification === null) {
            return null;
        }

        if ($this->date_modification === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_statut] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateStatut($format = 'Y-m-d H:i:s')
    {
        if ($this->date_statut === null) {
            return null;
        }

        if ($this->date_statut === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_statut);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_statut, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [motif_statut] column value.
     *
     * @return string
     */
    public function getMotifStatut()
    {

        return $this->motif_statut;
    }

    /**
     * Get the [id_dossier_parent] column value.
     *
     * @return int
     */
    public function getIdDossierParent()
    {

        return $this->id_dossier_parent;
    }

    /**
     * Get the [id_dispositif_parent] column value.
     *
     * @return int
     */
    public function getIdDispositifParent()
    {

        return $this->id_dispositif_parent;
    }

    /**
     * Get the [publicite_simplifie] column value.
     *
     * @return string
     */
    public function getPubliciteSimplifie()
    {

        return $this->publicite_simplifie;
    }

    /**
     * Get the [consultation_id] column value.
     *
     * @return int
     */
    public function getConsultationId()
    {

        return $this->consultation_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [ref_consultation] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setRefConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ref_consultation !== $v) {
            $this->ref_consultation = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::REF_CONSULTATION;
        }


        return $this;
    } // setRefConsultation()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [id_dossier_sub] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdDossierSub($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_dossier_sub !== $v) {
            $this->id_dossier_sub = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB;
        }


        return $this;
    } // setIdDossierSub()

    /**
     * Set the value of [id_dispositif] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdDispositif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_dispositif !== $v) {
            $this->id_dispositif = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ID_DISPOSITIF;
        }


        return $this;
    } // setIdDispositif()

    /**
     * Set the value of [id_compte_boamp] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdCompteBoamp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_compte_boamp !== $v) {
            $this->id_compte_boamp = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP;
        }


        return $this;
    } // setIdCompteBoamp()

    /**
     * Set the value of [statut] column.
     *
     * @param string $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setStatut($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->statut !== $v) {
            $this->statut = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::STATUT;
        }


        return $this;
    } // setStatut()

    /**
     * Set the value of [id_type_annonce] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdTypeAnnonce($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_annonce !== $v) {
            $this->id_type_annonce = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE;
        }

        if ($this->aCommonTTypeAnnonceConsultation !== null && $this->aCommonTTypeAnnonceConsultation->getId() !== $v) {
            $this->aCommonTTypeAnnonceConsultation = null;
        }


        return $this;
    } // setIdTypeAnnonce()

    /**
     * Set the value of [id_agent] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_agent !== $v) {
            $this->id_agent = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ID_AGENT;
        }


        return $this;
    } // setIdAgent()

    /**
     * Set the value of [prenom_nom_agent] column.
     *
     * @param string $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setPrenomNomAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prenom_nom_agent !== $v) {
            $this->prenom_nom_agent = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::PRENOM_NOM_AGENT;
        }


        return $this;
    } // setPrenomNomAgent()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Sets the value of [date_modification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification !== null && $tmpDt = new DateTime($this->date_modification)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification = $newDateAsString;
                $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::DATE_MODIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDateModification()

    /**
     * Sets the value of [date_statut] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setDateStatut($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_statut !== null || $dt !== null) {
            $currentDateAsString = ($this->date_statut !== null && $tmpDt = new DateTime($this->date_statut)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_statut = $newDateAsString;
                $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::DATE_STATUT;
            }
        } // if either are not null


        return $this;
    } // setDateStatut()

    /**
     * Set the value of [motif_statut] column.
     *
     * @param string $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setMotifStatut($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->motif_statut !== $v) {
            $this->motif_statut = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::MOTIF_STATUT;
        }


        return $this;
    } // setMotifStatut()

    /**
     * Set the value of [id_dossier_parent] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdDossierParent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_dossier_parent !== $v) {
            $this->id_dossier_parent = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT;
        }


        return $this;
    } // setIdDossierParent()

    /**
     * Set the value of [id_dispositif_parent] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdDispositifParent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_dispositif_parent !== $v) {
            $this->id_dispositif_parent = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT;
        }


        return $this;
    } // setIdDispositifParent()

    /**
     * Set the value of [publicite_simplifie] column.
     *
     * @param string $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setPubliciteSimplifie($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->publicite_simplifie !== $v) {
            $this->publicite_simplifie = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE;
        }


        return $this;
    } // setPubliciteSimplifie()

    /**
     * Set the value of [consultation_id] column.
     *
     * @param int $v new value
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setConsultationId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_id !== $v) {
            $this->consultation_id = $v;
            $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::CONSULTATION_ID;
        }

        if ($this->aCommonConsultation !== null && $this->aCommonConsultation->getId() !== $v) {
            $this->aCommonConsultation = null;
        }


        return $this;
    } // setConsultationId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->publicite_simplifie !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->ref_consultation = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->organisme = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->id_dossier_sub = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->id_dispositif = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->id_compte_boamp = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->statut = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->id_type_annonce = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->id_agent = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->prenom_nom_agent = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->date_creation = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->date_modification = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->date_statut = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->motif_statut = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->id_dossier_parent = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->id_dispositif_parent = ($row[$startcol + 15] !== null) ? (int) $row[$startcol + 15] : null;
            $this->publicite_simplifie = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->consultation_id = ($row[$startcol + 17] !== null) ? (int) $row[$startcol + 17] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 18; // 18 = CommonTAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTAnnonceConsultation object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTTypeAnnonceConsultation !== null && $this->id_type_annonce !== $this->aCommonTTypeAnnonceConsultation->getId()) {
            $this->aCommonTTypeAnnonceConsultation = null;
        }
        if ($this->aCommonConsultation !== null && $this->consultation_id !== $this->aCommonConsultation->getId()) {
            $this->aCommonConsultation = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTAnnonceConsultationPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonConsultation = null;
            $this->aCommonTTypeAnnonceConsultation = null;
            $this->collCommonTHistoriqueAnnonces = null;

            $this->collCommonTSupportAnnonceConsultations = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTAnnonceConsultationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTAnnonceConsultationPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonConsultation !== null) {
                if ($this->aCommonConsultation->isModified() || $this->aCommonConsultation->isNew()) {
                    $affectedRows += $this->aCommonConsultation->save($con);
                }
                $this->setCommonConsultation($this->aCommonConsultation);
            }

            if ($this->aCommonTTypeAnnonceConsultation !== null) {
                if ($this->aCommonTTypeAnnonceConsultation->isModified() || $this->aCommonTTypeAnnonceConsultation->isNew()) {
                    $affectedRows += $this->aCommonTTypeAnnonceConsultation->save($con);
                }
                $this->setCommonTTypeAnnonceConsultation($this->aCommonTTypeAnnonceConsultation);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTHistoriqueAnnoncesScheduledForDeletion !== null) {
                if (!$this->commonTHistoriqueAnnoncesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTHistoriqueAnnonceQuery::create()
                        ->filterByPrimaryKeys($this->commonTHistoriqueAnnoncesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTHistoriqueAnnoncesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTHistoriqueAnnonces !== null) {
                foreach ($this->collCommonTHistoriqueAnnonces as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTSupportAnnonceConsultationsScheduledForDeletion !== null) {
                if (!$this->commonTSupportAnnonceConsultationsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTSupportAnnonceConsultationQuery::create()
                        ->filterByPrimaryKeys($this->commonTSupportAnnonceConsultationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTSupportAnnonceConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTSupportAnnonceConsultations !== null) {
                foreach ($this->collCommonTSupportAnnonceConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTAnnonceConsultationPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTAnnonceConsultationPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::REF_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`ref_consultation`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB)) {
            $modifiedColumns[':p' . $index++]  = '`id_dossier_sub`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_DISPOSITIF)) {
            $modifiedColumns[':p' . $index++]  = '`id_dispositif`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP)) {
            $modifiedColumns[':p' . $index++]  = '`id_compte_boamp`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`statut`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_annonce`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_agent`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::PRENOM_NOM_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`prenom_nom_agent`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::DATE_STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`date_statut`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::MOTIF_STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`motif_statut`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_dossier_parent`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_dispositif_parent`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE)) {
            $modifiedColumns[':p' . $index++]  = '`publicite_simplifie`';
        }
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::CONSULTATION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_id`';
        }

        $sql = sprintf(
            'INSERT INTO `t_annonce_consultation` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`ref_consultation`':
                        $stmt->bindValue($identifier, $this->ref_consultation, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`id_dossier_sub`':
                        $stmt->bindValue($identifier, $this->id_dossier_sub, PDO::PARAM_INT);
                        break;
                    case '`id_dispositif`':
                        $stmt->bindValue($identifier, $this->id_dispositif, PDO::PARAM_INT);
                        break;
                    case '`id_compte_boamp`':
                        $stmt->bindValue($identifier, $this->id_compte_boamp, PDO::PARAM_INT);
                        break;
                    case '`statut`':
                        $stmt->bindValue($identifier, $this->statut, PDO::PARAM_STR);
                        break;
                    case '`id_type_annonce`':
                        $stmt->bindValue($identifier, $this->id_type_annonce, PDO::PARAM_INT);
                        break;
                    case '`id_agent`':
                        $stmt->bindValue($identifier, $this->id_agent, PDO::PARAM_INT);
                        break;
                    case '`prenom_nom_agent`':
                        $stmt->bindValue($identifier, $this->prenom_nom_agent, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`date_statut`':
                        $stmt->bindValue($identifier, $this->date_statut, PDO::PARAM_STR);
                        break;
                    case '`motif_statut`':
                        $stmt->bindValue($identifier, $this->motif_statut, PDO::PARAM_STR);
                        break;
                    case '`id_dossier_parent`':
                        $stmt->bindValue($identifier, $this->id_dossier_parent, PDO::PARAM_INT);
                        break;
                    case '`id_dispositif_parent`':
                        $stmt->bindValue($identifier, $this->id_dispositif_parent, PDO::PARAM_INT);
                        break;
                    case '`publicite_simplifie`':
                        $stmt->bindValue($identifier, $this->publicite_simplifie, PDO::PARAM_STR);
                        break;
                    case '`consultation_id`':
                        $stmt->bindValue($identifier, $this->consultation_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonConsultation !== null) {
                if (!$this->aCommonConsultation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonConsultation->getValidationFailures());
                }
            }

            if ($this->aCommonTTypeAnnonceConsultation !== null) {
                if (!$this->aCommonTTypeAnnonceConsultation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTTypeAnnonceConsultation->getValidationFailures());
                }
            }


            if (($retval = CommonTAnnonceConsultationPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTHistoriqueAnnonces !== null) {
                    foreach ($this->collCommonTHistoriqueAnnonces as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTSupportAnnonceConsultations !== null) {
                    foreach ($this->collCommonTSupportAnnonceConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTAnnonceConsultationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getRefConsultation();
                break;
            case 2:
                return $this->getOrganisme();
                break;
            case 3:
                return $this->getIdDossierSub();
                break;
            case 4:
                return $this->getIdDispositif();
                break;
            case 5:
                return $this->getIdCompteBoamp();
                break;
            case 6:
                return $this->getStatut();
                break;
            case 7:
                return $this->getIdTypeAnnonce();
                break;
            case 8:
                return $this->getIdAgent();
                break;
            case 9:
                return $this->getPrenomNomAgent();
                break;
            case 10:
                return $this->getDateCreation();
                break;
            case 11:
                return $this->getDateModification();
                break;
            case 12:
                return $this->getDateStatut();
                break;
            case 13:
                return $this->getMotifStatut();
                break;
            case 14:
                return $this->getIdDossierParent();
                break;
            case 15:
                return $this->getIdDispositifParent();
                break;
            case 16:
                return $this->getPubliciteSimplifie();
                break;
            case 17:
                return $this->getConsultationId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTAnnonceConsultation'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTAnnonceConsultation'][$this->getPrimaryKey()] = true;
        $keys = CommonTAnnonceConsultationPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getRefConsultation(),
            $keys[2] => $this->getOrganisme(),
            $keys[3] => $this->getIdDossierSub(),
            $keys[4] => $this->getIdDispositif(),
            $keys[5] => $this->getIdCompteBoamp(),
            $keys[6] => $this->getStatut(),
            $keys[7] => $this->getIdTypeAnnonce(),
            $keys[8] => $this->getIdAgent(),
            $keys[9] => $this->getPrenomNomAgent(),
            $keys[10] => $this->getDateCreation(),
            $keys[11] => $this->getDateModification(),
            $keys[12] => $this->getDateStatut(),
            $keys[13] => $this->getMotifStatut(),
            $keys[14] => $this->getIdDossierParent(),
            $keys[15] => $this->getIdDispositifParent(),
            $keys[16] => $this->getPubliciteSimplifie(),
            $keys[17] => $this->getConsultationId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonConsultation) {
                $result['CommonConsultation'] = $this->aCommonConsultation->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTTypeAnnonceConsultation) {
                $result['CommonTTypeAnnonceConsultation'] = $this->aCommonTTypeAnnonceConsultation->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTHistoriqueAnnonces) {
                $result['CommonTHistoriqueAnnonces'] = $this->collCommonTHistoriqueAnnonces->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTSupportAnnonceConsultations) {
                $result['CommonTSupportAnnonceConsultations'] = $this->collCommonTSupportAnnonceConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTAnnonceConsultationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setRefConsultation($value);
                break;
            case 2:
                $this->setOrganisme($value);
                break;
            case 3:
                $this->setIdDossierSub($value);
                break;
            case 4:
                $this->setIdDispositif($value);
                break;
            case 5:
                $this->setIdCompteBoamp($value);
                break;
            case 6:
                $this->setStatut($value);
                break;
            case 7:
                $this->setIdTypeAnnonce($value);
                break;
            case 8:
                $this->setIdAgent($value);
                break;
            case 9:
                $this->setPrenomNomAgent($value);
                break;
            case 10:
                $this->setDateCreation($value);
                break;
            case 11:
                $this->setDateModification($value);
                break;
            case 12:
                $this->setDateStatut($value);
                break;
            case 13:
                $this->setMotifStatut($value);
                break;
            case 14:
                $this->setIdDossierParent($value);
                break;
            case 15:
                $this->setIdDispositifParent($value);
                break;
            case 16:
                $this->setPubliciteSimplifie($value);
                break;
            case 17:
                $this->setConsultationId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTAnnonceConsultationPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setRefConsultation($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setOrganisme($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIdDossierSub($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdDispositif($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setIdCompteBoamp($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setStatut($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setIdTypeAnnonce($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setIdAgent($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setPrenomNomAgent($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setDateCreation($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setDateModification($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setDateStatut($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setMotifStatut($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setIdDossierParent($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setIdDispositifParent($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setPubliciteSimplifie($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setConsultationId($arr[$keys[17]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTAnnonceConsultationPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID)) $criteria->add(CommonTAnnonceConsultationPeer::ID, $this->id);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::REF_CONSULTATION)) $criteria->add(CommonTAnnonceConsultationPeer::REF_CONSULTATION, $this->ref_consultation);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ORGANISME)) $criteria->add(CommonTAnnonceConsultationPeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB)) $criteria->add(CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB, $this->id_dossier_sub);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_DISPOSITIF)) $criteria->add(CommonTAnnonceConsultationPeer::ID_DISPOSITIF, $this->id_dispositif);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP)) $criteria->add(CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP, $this->id_compte_boamp);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::STATUT)) $criteria->add(CommonTAnnonceConsultationPeer::STATUT, $this->statut);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE)) $criteria->add(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, $this->id_type_annonce);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_AGENT)) $criteria->add(CommonTAnnonceConsultationPeer::ID_AGENT, $this->id_agent);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::PRENOM_NOM_AGENT)) $criteria->add(CommonTAnnonceConsultationPeer::PRENOM_NOM_AGENT, $this->prenom_nom_agent);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::DATE_CREATION)) $criteria->add(CommonTAnnonceConsultationPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::DATE_MODIFICATION)) $criteria->add(CommonTAnnonceConsultationPeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::DATE_STATUT)) $criteria->add(CommonTAnnonceConsultationPeer::DATE_STATUT, $this->date_statut);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::MOTIF_STATUT)) $criteria->add(CommonTAnnonceConsultationPeer::MOTIF_STATUT, $this->motif_statut);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT)) $criteria->add(CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT, $this->id_dossier_parent);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT)) $criteria->add(CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT, $this->id_dispositif_parent);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE)) $criteria->add(CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE, $this->publicite_simplifie);
        if ($this->isColumnModified(CommonTAnnonceConsultationPeer::CONSULTATION_ID)) $criteria->add(CommonTAnnonceConsultationPeer::CONSULTATION_ID, $this->consultation_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTAnnonceConsultationPeer::DATABASE_NAME);
        $criteria->add(CommonTAnnonceConsultationPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTAnnonceConsultation (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setRefConsultation($this->getRefConsultation());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setIdDossierSub($this->getIdDossierSub());
        $copyObj->setIdDispositif($this->getIdDispositif());
        $copyObj->setIdCompteBoamp($this->getIdCompteBoamp());
        $copyObj->setStatut($this->getStatut());
        $copyObj->setIdTypeAnnonce($this->getIdTypeAnnonce());
        $copyObj->setIdAgent($this->getIdAgent());
        $copyObj->setPrenomNomAgent($this->getPrenomNomAgent());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setDateStatut($this->getDateStatut());
        $copyObj->setMotifStatut($this->getMotifStatut());
        $copyObj->setIdDossierParent($this->getIdDossierParent());
        $copyObj->setIdDispositifParent($this->getIdDispositifParent());
        $copyObj->setPubliciteSimplifie($this->getPubliciteSimplifie());
        $copyObj->setConsultationId($this->getConsultationId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTHistoriqueAnnonces() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTHistoriqueAnnonce($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTSupportAnnonceConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTSupportAnnonceConsultation($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTAnnonceConsultation Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTAnnonceConsultationPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTAnnonceConsultationPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonConsultation object.
     *
     * @param   CommonConsultation $v
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonConsultation(CommonConsultation $v = null)
    {
        if ($v === null) {
            $this->setConsultationId(NULL);
        } else {
            $this->setConsultationId($v->getId());
        }

        $this->aCommonConsultation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonConsultation object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTAnnonceConsultation($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonConsultation object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonConsultation The associated CommonConsultation object.
     * @throws PropelException
     */
    public function getCommonConsultation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonConsultation === null && ($this->consultation_id !== null) && $doQuery) {
            $this->aCommonConsultation = CommonConsultationQuery::create()->findPk($this->consultation_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonConsultation->addCommonTAnnonceConsultations($this);
             */
        }

        return $this->aCommonConsultation;
    }

    /**
     * Declares an association between this object and a CommonTTypeAnnonceConsultation object.
     *
     * @param   CommonTTypeAnnonceConsultation $v
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTTypeAnnonceConsultation(CommonTTypeAnnonceConsultation $v = null)
    {
        if ($v === null) {
            $this->setIdTypeAnnonce(NULL);
        } else {
            $this->setIdTypeAnnonce($v->getId());
        }

        $this->aCommonTTypeAnnonceConsultation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTTypeAnnonceConsultation object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTAnnonceConsultation($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTTypeAnnonceConsultation object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTTypeAnnonceConsultation The associated CommonTTypeAnnonceConsultation object.
     * @throws PropelException
     */
    public function getCommonTTypeAnnonceConsultation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTTypeAnnonceConsultation === null && ($this->id_type_annonce !== null) && $doQuery) {
            $this->aCommonTTypeAnnonceConsultation = CommonTTypeAnnonceConsultationQuery::create()->findPk($this->id_type_annonce, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTTypeAnnonceConsultation->addCommonTAnnonceConsultations($this);
             */
        }

        return $this->aCommonTTypeAnnonceConsultation;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTHistoriqueAnnonce' == $relationName) {
            $this->initCommonTHistoriqueAnnonces();
        }
        if ('CommonTSupportAnnonceConsultation' == $relationName) {
            $this->initCommonTSupportAnnonceConsultations();
        }
    }

    /**
     * Clears out the collCommonTHistoriqueAnnonces collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     * @see        addCommonTHistoriqueAnnonces()
     */
    public function clearCommonTHistoriqueAnnonces()
    {
        $this->collCommonTHistoriqueAnnonces = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTHistoriqueAnnoncesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTHistoriqueAnnonces collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTHistoriqueAnnonces($v = true)
    {
        $this->collCommonTHistoriqueAnnoncesPartial = $v;
    }

    /**
     * Initializes the collCommonTHistoriqueAnnonces collection.
     *
     * By default this just sets the collCommonTHistoriqueAnnonces collection to an empty array (like clearcollCommonTHistoriqueAnnonces());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTHistoriqueAnnonces($overrideExisting = true)
    {
        if (null !== $this->collCommonTHistoriqueAnnonces && !$overrideExisting) {
            return;
        }
        $this->collCommonTHistoriqueAnnonces = new PropelObjectCollection();
        $this->collCommonTHistoriqueAnnonces->setModel('CommonTHistoriqueAnnonce');
    }

    /**
     * Gets an array of CommonTHistoriqueAnnonce objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTAnnonceConsultation is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTHistoriqueAnnonce[] List of CommonTHistoriqueAnnonce objects
     * @throws PropelException
     */
    public function getCommonTHistoriqueAnnonces($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTHistoriqueAnnoncesPartial && !$this->isNew();
        if (null === $this->collCommonTHistoriqueAnnonces || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTHistoriqueAnnonces) {
                // return empty collection
                $this->initCommonTHistoriqueAnnonces();
            } else {
                $collCommonTHistoriqueAnnonces = CommonTHistoriqueAnnonceQuery::create(null, $criteria)
                    ->filterByCommonTAnnonceConsultation($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTHistoriqueAnnoncesPartial && count($collCommonTHistoriqueAnnonces)) {
                      $this->initCommonTHistoriqueAnnonces(false);

                      foreach ($collCommonTHistoriqueAnnonces as $obj) {
                        if (false == $this->collCommonTHistoriqueAnnonces->contains($obj)) {
                          $this->collCommonTHistoriqueAnnonces->append($obj);
                        }
                      }

                      $this->collCommonTHistoriqueAnnoncesPartial = true;
                    }

                    $collCommonTHistoriqueAnnonces->getInternalIterator()->rewind();

                    return $collCommonTHistoriqueAnnonces;
                }

                if ($partial && $this->collCommonTHistoriqueAnnonces) {
                    foreach ($this->collCommonTHistoriqueAnnonces as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTHistoriqueAnnonces[] = $obj;
                        }
                    }
                }

                $this->collCommonTHistoriqueAnnonces = $collCommonTHistoriqueAnnonces;
                $this->collCommonTHistoriqueAnnoncesPartial = false;
            }
        }

        return $this->collCommonTHistoriqueAnnonces;
    }

    /**
     * Sets a collection of CommonTHistoriqueAnnonce objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTHistoriqueAnnonces A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setCommonTHistoriqueAnnonces(PropelCollection $commonTHistoriqueAnnonces, PropelPDO $con = null)
    {
        $commonTHistoriqueAnnoncesToDelete = $this->getCommonTHistoriqueAnnonces(new Criteria(), $con)->diff($commonTHistoriqueAnnonces);


        $this->commonTHistoriqueAnnoncesScheduledForDeletion = $commonTHistoriqueAnnoncesToDelete;

        foreach ($commonTHistoriqueAnnoncesToDelete as $commonTHistoriqueAnnonceRemoved) {
            $commonTHistoriqueAnnonceRemoved->setCommonTAnnonceConsultation(null);
        }

        $this->collCommonTHistoriqueAnnonces = null;
        foreach ($commonTHistoriqueAnnonces as $commonTHistoriqueAnnonce) {
            $this->addCommonTHistoriqueAnnonce($commonTHistoriqueAnnonce);
        }

        $this->collCommonTHistoriqueAnnonces = $commonTHistoriqueAnnonces;
        $this->collCommonTHistoriqueAnnoncesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTHistoriqueAnnonce objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTHistoriqueAnnonce objects.
     * @throws PropelException
     */
    public function countCommonTHistoriqueAnnonces(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTHistoriqueAnnoncesPartial && !$this->isNew();
        if (null === $this->collCommonTHistoriqueAnnonces || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTHistoriqueAnnonces) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTHistoriqueAnnonces());
            }
            $query = CommonTHistoriqueAnnonceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTAnnonceConsultation($this)
                ->count($con);
        }

        return count($this->collCommonTHistoriqueAnnonces);
    }

    /**
     * Method called to associate a CommonTHistoriqueAnnonce object to this object
     * through the CommonTHistoriqueAnnonce foreign key attribute.
     *
     * @param   CommonTHistoriqueAnnonce $l CommonTHistoriqueAnnonce
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function addCommonTHistoriqueAnnonce(CommonTHistoriqueAnnonce $l)
    {
        if ($this->collCommonTHistoriqueAnnonces === null) {
            $this->initCommonTHistoriqueAnnonces();
            $this->collCommonTHistoriqueAnnoncesPartial = true;
        }
        if (!in_array($l, $this->collCommonTHistoriqueAnnonces->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTHistoriqueAnnonce($l);
        }

        return $this;
    }

    /**
     * @param	CommonTHistoriqueAnnonce $commonTHistoriqueAnnonce The commonTHistoriqueAnnonce object to add.
     */
    protected function doAddCommonTHistoriqueAnnonce($commonTHistoriqueAnnonce)
    {
        $this->collCommonTHistoriqueAnnonces[]= $commonTHistoriqueAnnonce;
        $commonTHistoriqueAnnonce->setCommonTAnnonceConsultation($this);
    }

    /**
     * @param	CommonTHistoriqueAnnonce $commonTHistoriqueAnnonce The commonTHistoriqueAnnonce object to remove.
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function removeCommonTHistoriqueAnnonce($commonTHistoriqueAnnonce)
    {
        if ($this->getCommonTHistoriqueAnnonces()->contains($commonTHistoriqueAnnonce)) {
            $this->collCommonTHistoriqueAnnonces->remove($this->collCommonTHistoriqueAnnonces->search($commonTHistoriqueAnnonce));
            if (null === $this->commonTHistoriqueAnnoncesScheduledForDeletion) {
                $this->commonTHistoriqueAnnoncesScheduledForDeletion = clone $this->collCommonTHistoriqueAnnonces;
                $this->commonTHistoriqueAnnoncesScheduledForDeletion->clear();
            }
            $this->commonTHistoriqueAnnoncesScheduledForDeletion[]= clone $commonTHistoriqueAnnonce;
            $commonTHistoriqueAnnonce->setCommonTAnnonceConsultation(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTSupportAnnonceConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     * @see        addCommonTSupportAnnonceConsultations()
     */
    public function clearCommonTSupportAnnonceConsultations()
    {
        $this->collCommonTSupportAnnonceConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTSupportAnnonceConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTSupportAnnonceConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTSupportAnnonceConsultations($v = true)
    {
        $this->collCommonTSupportAnnonceConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonTSupportAnnonceConsultations collection.
     *
     * By default this just sets the collCommonTSupportAnnonceConsultations collection to an empty array (like clearcollCommonTSupportAnnonceConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTSupportAnnonceConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonTSupportAnnonceConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonTSupportAnnonceConsultations = new PropelObjectCollection();
        $this->collCommonTSupportAnnonceConsultations->setModel('CommonTSupportAnnonceConsultation');
    }

    /**
     * Gets an array of CommonTSupportAnnonceConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTAnnonceConsultation is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTSupportAnnonceConsultation[] List of CommonTSupportAnnonceConsultation objects
     * @throws PropelException
     */
    public function getCommonTSupportAnnonceConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTSupportAnnonceConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonTSupportAnnonceConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTSupportAnnonceConsultations) {
                // return empty collection
                $this->initCommonTSupportAnnonceConsultations();
            } else {
                $collCommonTSupportAnnonceConsultations = CommonTSupportAnnonceConsultationQuery::create(null, $criteria)
                    ->filterByCommonTAnnonceConsultation($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTSupportAnnonceConsultationsPartial && count($collCommonTSupportAnnonceConsultations)) {
                      $this->initCommonTSupportAnnonceConsultations(false);

                      foreach ($collCommonTSupportAnnonceConsultations as $obj) {
                        if (false == $this->collCommonTSupportAnnonceConsultations->contains($obj)) {
                          $this->collCommonTSupportAnnonceConsultations->append($obj);
                        }
                      }

                      $this->collCommonTSupportAnnonceConsultationsPartial = true;
                    }

                    $collCommonTSupportAnnonceConsultations->getInternalIterator()->rewind();

                    return $collCommonTSupportAnnonceConsultations;
                }

                if ($partial && $this->collCommonTSupportAnnonceConsultations) {
                    foreach ($this->collCommonTSupportAnnonceConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTSupportAnnonceConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonTSupportAnnonceConsultations = $collCommonTSupportAnnonceConsultations;
                $this->collCommonTSupportAnnonceConsultationsPartial = false;
            }
        }

        return $this->collCommonTSupportAnnonceConsultations;
    }

    /**
     * Sets a collection of CommonTSupportAnnonceConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTSupportAnnonceConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function setCommonTSupportAnnonceConsultations(PropelCollection $commonTSupportAnnonceConsultations, PropelPDO $con = null)
    {
        $commonTSupportAnnonceConsultationsToDelete = $this->getCommonTSupportAnnonceConsultations(new Criteria(), $con)->diff($commonTSupportAnnonceConsultations);


        $this->commonTSupportAnnonceConsultationsScheduledForDeletion = $commonTSupportAnnonceConsultationsToDelete;

        foreach ($commonTSupportAnnonceConsultationsToDelete as $commonTSupportAnnonceConsultationRemoved) {
            $commonTSupportAnnonceConsultationRemoved->setCommonTAnnonceConsultation(null);
        }

        $this->collCommonTSupportAnnonceConsultations = null;
        foreach ($commonTSupportAnnonceConsultations as $commonTSupportAnnonceConsultation) {
            $this->addCommonTSupportAnnonceConsultation($commonTSupportAnnonceConsultation);
        }

        $this->collCommonTSupportAnnonceConsultations = $commonTSupportAnnonceConsultations;
        $this->collCommonTSupportAnnonceConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTSupportAnnonceConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTSupportAnnonceConsultation objects.
     * @throws PropelException
     */
    public function countCommonTSupportAnnonceConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTSupportAnnonceConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonTSupportAnnonceConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTSupportAnnonceConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTSupportAnnonceConsultations());
            }
            $query = CommonTSupportAnnonceConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTAnnonceConsultation($this)
                ->count($con);
        }

        return count($this->collCommonTSupportAnnonceConsultations);
    }

    /**
     * Method called to associate a CommonTSupportAnnonceConsultation object to this object
     * through the CommonTSupportAnnonceConsultation foreign key attribute.
     *
     * @param   CommonTSupportAnnonceConsultation $l CommonTSupportAnnonceConsultation
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function addCommonTSupportAnnonceConsultation(CommonTSupportAnnonceConsultation $l)
    {
        if ($this->collCommonTSupportAnnonceConsultations === null) {
            $this->initCommonTSupportAnnonceConsultations();
            $this->collCommonTSupportAnnonceConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonTSupportAnnonceConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTSupportAnnonceConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonTSupportAnnonceConsultation $commonTSupportAnnonceConsultation The commonTSupportAnnonceConsultation object to add.
     */
    protected function doAddCommonTSupportAnnonceConsultation($commonTSupportAnnonceConsultation)
    {
        $this->collCommonTSupportAnnonceConsultations[]= $commonTSupportAnnonceConsultation;
        $commonTSupportAnnonceConsultation->setCommonTAnnonceConsultation($this);
    }

    /**
     * @param	CommonTSupportAnnonceConsultation $commonTSupportAnnonceConsultation The commonTSupportAnnonceConsultation object to remove.
     * @return CommonTAnnonceConsultation The current object (for fluent API support)
     */
    public function removeCommonTSupportAnnonceConsultation($commonTSupportAnnonceConsultation)
    {
        if ($this->getCommonTSupportAnnonceConsultations()->contains($commonTSupportAnnonceConsultation)) {
            $this->collCommonTSupportAnnonceConsultations->remove($this->collCommonTSupportAnnonceConsultations->search($commonTSupportAnnonceConsultation));
            if (null === $this->commonTSupportAnnonceConsultationsScheduledForDeletion) {
                $this->commonTSupportAnnonceConsultationsScheduledForDeletion = clone $this->collCommonTSupportAnnonceConsultations;
                $this->commonTSupportAnnonceConsultationsScheduledForDeletion->clear();
            }
            $this->commonTSupportAnnonceConsultationsScheduledForDeletion[]= clone $commonTSupportAnnonceConsultation;
            $commonTSupportAnnonceConsultation->setCommonTAnnonceConsultation(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTAnnonceConsultation is new, it will return
     * an empty collection; or if this CommonTAnnonceConsultation has previously
     * been saved, it will retrieve related CommonTSupportAnnonceConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTAnnonceConsultation.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTSupportAnnonceConsultation[] List of CommonTSupportAnnonceConsultation objects
     */
    public function getCommonTSupportAnnonceConsultationsJoinCommonTSupportPublication($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTSupportAnnonceConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonTSupportPublication', $join_behavior);

        return $this->getCommonTSupportAnnonceConsultations($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->ref_consultation = null;
        $this->organisme = null;
        $this->id_dossier_sub = null;
        $this->id_dispositif = null;
        $this->id_compte_boamp = null;
        $this->statut = null;
        $this->id_type_annonce = null;
        $this->id_agent = null;
        $this->prenom_nom_agent = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->date_statut = null;
        $this->motif_statut = null;
        $this->id_dossier_parent = null;
        $this->id_dispositif_parent = null;
        $this->publicite_simplifie = null;
        $this->consultation_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTHistoriqueAnnonces) {
                foreach ($this->collCommonTHistoriqueAnnonces as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTSupportAnnonceConsultations) {
                foreach ($this->collCommonTSupportAnnonceConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonConsultation instanceof Persistent) {
              $this->aCommonConsultation->clearAllReferences($deep);
            }
            if ($this->aCommonTTypeAnnonceConsultation instanceof Persistent) {
              $this->aCommonTTypeAnnonceConsultation->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTHistoriqueAnnonces instanceof PropelCollection) {
            $this->collCommonTHistoriqueAnnonces->clearIterator();
        }
        $this->collCommonTHistoriqueAnnonces = null;
        if ($this->collCommonTSupportAnnonceConsultations instanceof PropelCollection) {
            $this->collCommonTSupportAnnonceConsultations->clearIterator();
        }
        $this->collCommonTSupportAnnonceConsultations = null;
        $this->aCommonConsultation = null;
        $this->aCommonTTypeAnnonceConsultation = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTAnnonceConsultationPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
