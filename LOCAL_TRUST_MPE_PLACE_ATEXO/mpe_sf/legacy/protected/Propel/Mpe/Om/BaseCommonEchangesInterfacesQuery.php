<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonCodeRetour;
use Application\Propel\Mpe\BaseCommonEchangesInterfaces;
use Application\Propel\Mpe\CommonEchangesInterfacesPeer;
use Application\Propel\Mpe\CommonEchangesInterfacesQuery;

/**
 * Base class that represents a query for the 'echanges_interfaces' table.
 *
 *
 *
 * @method CommonEchangesInterfacesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonEchangesInterfacesQuery orderByCodeRetour($order = Criteria::ASC) Order by the code_retour column
 * @method CommonEchangesInterfacesQuery orderByService($order = Criteria::ASC) Order by the service column
 * @method CommonEchangesInterfacesQuery orderByNomBatch($order = Criteria::ASC) Order by the nom_batch column
 * @method CommonEchangesInterfacesQuery orderByVariablesEntree($order = Criteria::ASC) Order by the variables_entree column
 * @method CommonEchangesInterfacesQuery orderByResultat($order = Criteria::ASC) Order by the resultat column
 * @method CommonEchangesInterfacesQuery orderByTypeFlux($order = Criteria::ASC) Order by the type_flux column
 * @method CommonEchangesInterfacesQuery orderByPoids($order = Criteria::ASC) Order by the poids column
 * @method CommonEchangesInterfacesQuery orderByInformationMetier($order = Criteria::ASC) Order by the information_metier column
 * @method CommonEchangesInterfacesQuery orderByNbFlux($order = Criteria::ASC) Order by the nb_flux column
 * @method CommonEchangesInterfacesQuery orderByDebutExecution($order = Criteria::ASC) Order by the debut_execution column
 * @method CommonEchangesInterfacesQuery orderByFinExecution($order = Criteria::ASC) Order by the fin_execution column
 * @method CommonEchangesInterfacesQuery orderByIdEchangesInterfaces($order = Criteria::ASC) Order by the id_echanges_interfaces column
 * @method CommonEchangesInterfacesQuery orderByNomInterface($order = Criteria::ASC) Order by the nom_interface column
 *
 * @method CommonEchangesInterfacesQuery groupById() Group by the id column
 * @method CommonEchangesInterfacesQuery groupByCodeRetour() Group by the code_retour column
 * @method CommonEchangesInterfacesQuery groupByService() Group by the service column
 * @method CommonEchangesInterfacesQuery groupByNomBatch() Group by the nom_batch column
 * @method CommonEchangesInterfacesQuery groupByVariablesEntree() Group by the variables_entree column
 * @method CommonEchangesInterfacesQuery groupByResultat() Group by the resultat column
 * @method CommonEchangesInterfacesQuery groupByTypeFlux() Group by the type_flux column
 * @method CommonEchangesInterfacesQuery groupByPoids() Group by the poids column
 * @method CommonEchangesInterfacesQuery groupByInformationMetier() Group by the information_metier column
 * @method CommonEchangesInterfacesQuery groupByNbFlux() Group by the nb_flux column
 * @method CommonEchangesInterfacesQuery groupByDebutExecution() Group by the debut_execution column
 * @method CommonEchangesInterfacesQuery groupByFinExecution() Group by the fin_execution column
 * @method CommonEchangesInterfacesQuery groupByIdEchangesInterfaces() Group by the id_echanges_interfaces column
 * @method CommonEchangesInterfacesQuery groupByNomInterface() Group by the nom_interface column
 *
 * @method CommonEchangesInterfacesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonEchangesInterfacesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonEchangesInterfacesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonEchangesInterfacesQuery leftJoinCommonCodeRetour($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonCodeRetour relation
 * @method CommonEchangesInterfacesQuery rightJoinCommonCodeRetour($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonCodeRetour relation
 * @method CommonEchangesInterfacesQuery innerJoinCommonCodeRetour($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonCodeRetour relation
 *
 * @method CommonEchangesInterfacesQuery leftJoinCommonEchangesInterfacesRelatedByIdEchangesInterfaces($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangesInterfacesRelatedByIdEchangesInterfaces relation
 * @method CommonEchangesInterfacesQuery rightJoinCommonEchangesInterfacesRelatedByIdEchangesInterfaces($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangesInterfacesRelatedByIdEchangesInterfaces relation
 * @method CommonEchangesInterfacesQuery innerJoinCommonEchangesInterfacesRelatedByIdEchangesInterfaces($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangesInterfacesRelatedByIdEchangesInterfaces relation
 *
 * @method CommonEchangesInterfacesQuery leftJoinCommonEchangesInterfacesRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangesInterfacesRelatedById relation
 * @method CommonEchangesInterfacesQuery rightJoinCommonEchangesInterfacesRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangesInterfacesRelatedById relation
 * @method CommonEchangesInterfacesQuery innerJoinCommonEchangesInterfacesRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangesInterfacesRelatedById relation
 *
 * @method CommonEchangesInterfaces findOne(PropelPDO $con = null) Return the first CommonEchangesInterfaces matching the query
 * @method CommonEchangesInterfaces findOneOrCreate(PropelPDO $con = null) Return the first CommonEchangesInterfaces matching the query, or a new CommonEchangesInterfaces object populated from the query conditions when no match is found
 *
 * @method CommonEchangesInterfaces findOneByCodeRetour(int $code_retour) Return the first CommonEchangesInterfaces filtered by the code_retour column
 * @method CommonEchangesInterfaces findOneByService(string $service) Return the first CommonEchangesInterfaces filtered by the service column
 * @method CommonEchangesInterfaces findOneByNomBatch(string $nom_batch) Return the first CommonEchangesInterfaces filtered by the nom_batch column
 * @method CommonEchangesInterfaces findOneByVariablesEntree(string $variables_entree) Return the first CommonEchangesInterfaces filtered by the variables_entree column
 * @method CommonEchangesInterfaces findOneByResultat(string $resultat) Return the first CommonEchangesInterfaces filtered by the resultat column
 * @method CommonEchangesInterfaces findOneByTypeFlux(string $type_flux) Return the first CommonEchangesInterfaces filtered by the type_flux column
 * @method CommonEchangesInterfaces findOneByPoids(int $poids) Return the first CommonEchangesInterfaces filtered by the poids column
 * @method CommonEchangesInterfaces findOneByInformationMetier(string $information_metier) Return the first CommonEchangesInterfaces filtered by the information_metier column
 * @method CommonEchangesInterfaces findOneByNbFlux(int $nb_flux) Return the first CommonEchangesInterfaces filtered by the nb_flux column
 * @method CommonEchangesInterfaces findOneByDebutExecution(string $debut_execution) Return the first CommonEchangesInterfaces filtered by the debut_execution column
 * @method CommonEchangesInterfaces findOneByFinExecution(string $fin_execution) Return the first CommonEchangesInterfaces filtered by the fin_execution column
 * @method CommonEchangesInterfaces findOneByIdEchangesInterfaces(int $id_echanges_interfaces) Return the first CommonEchangesInterfaces filtered by the id_echanges_interfaces column
 * @method CommonEchangesInterfaces findOneByNomInterface(string $nom_interface) Return the first CommonEchangesInterfaces filtered by the nom_interface column
 *
 * @method array findById(int $id) Return CommonEchangesInterfaces objects filtered by the id column
 * @method array findByCodeRetour(int $code_retour) Return CommonEchangesInterfaces objects filtered by the code_retour column
 * @method array findByService(string $service) Return CommonEchangesInterfaces objects filtered by the service column
 * @method array findByNomBatch(string $nom_batch) Return CommonEchangesInterfaces objects filtered by the nom_batch column
 * @method array findByVariablesEntree(string $variables_entree) Return CommonEchangesInterfaces objects filtered by the variables_entree column
 * @method array findByResultat(string $resultat) Return CommonEchangesInterfaces objects filtered by the resultat column
 * @method array findByTypeFlux(string $type_flux) Return CommonEchangesInterfaces objects filtered by the type_flux column
 * @method array findByPoids(int $poids) Return CommonEchangesInterfaces objects filtered by the poids column
 * @method array findByInformationMetier(string $information_metier) Return CommonEchangesInterfaces objects filtered by the information_metier column
 * @method array findByNbFlux(int $nb_flux) Return CommonEchangesInterfaces objects filtered by the nb_flux column
 * @method array findByDebutExecution(string $debut_execution) Return CommonEchangesInterfaces objects filtered by the debut_execution column
 * @method array findByFinExecution(string $fin_execution) Return CommonEchangesInterfaces objects filtered by the fin_execution column
 * @method array findByIdEchangesInterfaces(int $id_echanges_interfaces) Return CommonEchangesInterfaces objects filtered by the id_echanges_interfaces column
 * @method array findByNomInterface(string $nom_interface) Return CommonEchangesInterfaces objects filtered by the nom_interface column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangesInterfacesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonEchangesInterfacesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonEchangesInterfaces', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonEchangesInterfacesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonEchangesInterfacesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonEchangesInterfacesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonEchangesInterfacesQuery) {
            return $criteria;
        }
        $query = new CommonEchangesInterfacesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonEchangesInterfaces|CommonEchangesInterfaces[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonEchangesInterfacesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangesInterfaces A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangesInterfaces A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `code_retour`, `service`, `nom_batch`, `variables_entree`, `resultat`, `type_flux`, `poids`, `information_metier`, `nb_flux`, `debut_execution`, `fin_execution`, `id_echanges_interfaces`, `nom_interface` FROM `echanges_interfaces` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonEchangesInterfaces();
            $obj->hydrate($row);
            CommonEchangesInterfacesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonEchangesInterfaces|CommonEchangesInterfaces[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonEchangesInterfaces[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the code_retour column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeRetour(1234); // WHERE code_retour = 1234
     * $query->filterByCodeRetour(array(12, 34)); // WHERE code_retour IN (12, 34)
     * $query->filterByCodeRetour(array('min' => 12)); // WHERE code_retour >= 12
     * $query->filterByCodeRetour(array('max' => 12)); // WHERE code_retour <= 12
     * </code>
     *
     * @see       filterByCommonCodeRetour()
     *
     * @param     mixed $codeRetour The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByCodeRetour($codeRetour = null, $comparison = null)
    {
        if (is_array($codeRetour)) {
            $useMinMax = false;
            if (isset($codeRetour['min'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::CODE_RETOUR, $codeRetour['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($codeRetour['max'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::CODE_RETOUR, $codeRetour['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::CODE_RETOUR, $codeRetour, $comparison);
    }

    /**
     * Filter the query on the service column
     *
     * Example usage:
     * <code>
     * $query->filterByService('fooValue');   // WHERE service = 'fooValue'
     * $query->filterByService('%fooValue%'); // WHERE service LIKE '%fooValue%'
     * </code>
     *
     * @param     string $service The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByService($service = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($service)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $service)) {
                $service = str_replace('*', '%', $service);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::SERVICE, $service, $comparison);
    }

    /**
     * Filter the query on the nom_batch column
     *
     * Example usage:
     * <code>
     * $query->filterByNomBatch('fooValue');   // WHERE nom_batch = 'fooValue'
     * $query->filterByNomBatch('%fooValue%'); // WHERE nom_batch LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomBatch The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByNomBatch($nomBatch = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomBatch)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomBatch)) {
                $nomBatch = str_replace('*', '%', $nomBatch);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::NOM_BATCH, $nomBatch, $comparison);
    }

    /**
     * Filter the query on the variables_entree column
     *
     * Example usage:
     * <code>
     * $query->filterByVariablesEntree('fooValue');   // WHERE variables_entree = 'fooValue'
     * $query->filterByVariablesEntree('%fooValue%'); // WHERE variables_entree LIKE '%fooValue%'
     * </code>
     *
     * @param     string $variablesEntree The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByVariablesEntree($variablesEntree = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($variablesEntree)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $variablesEntree)) {
                $variablesEntree = str_replace('*', '%', $variablesEntree);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::VARIABLES_ENTREE, $variablesEntree, $comparison);
    }

    /**
     * Filter the query on the resultat column
     *
     * Example usage:
     * <code>
     * $query->filterByResultat('fooValue');   // WHERE resultat = 'fooValue'
     * $query->filterByResultat('%fooValue%'); // WHERE resultat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $resultat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByResultat($resultat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($resultat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $resultat)) {
                $resultat = str_replace('*', '%', $resultat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::RESULTAT, $resultat, $comparison);
    }

    /**
     * Filter the query on the type_flux column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeFlux('fooValue');   // WHERE type_flux = 'fooValue'
     * $query->filterByTypeFlux('%fooValue%'); // WHERE type_flux LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeFlux The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByTypeFlux($typeFlux = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeFlux)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeFlux)) {
                $typeFlux = str_replace('*', '%', $typeFlux);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::TYPE_FLUX, $typeFlux, $comparison);
    }

    /**
     * Filter the query on the poids column
     *
     * Example usage:
     * <code>
     * $query->filterByPoids(1234); // WHERE poids = 1234
     * $query->filterByPoids(array(12, 34)); // WHERE poids IN (12, 34)
     * $query->filterByPoids(array('min' => 12)); // WHERE poids >= 12
     * $query->filterByPoids(array('max' => 12)); // WHERE poids <= 12
     * </code>
     *
     * @param     mixed $poids The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByPoids($poids = null, $comparison = null)
    {
        if (is_array($poids)) {
            $useMinMax = false;
            if (isset($poids['min'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::POIDS, $poids['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($poids['max'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::POIDS, $poids['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::POIDS, $poids, $comparison);
    }

    /**
     * Filter the query on the information_metier column
     *
     * Example usage:
     * <code>
     * $query->filterByInformationMetier('fooValue');   // WHERE information_metier = 'fooValue'
     * $query->filterByInformationMetier('%fooValue%'); // WHERE information_metier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $informationMetier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByInformationMetier($informationMetier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($informationMetier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $informationMetier)) {
                $informationMetier = str_replace('*', '%', $informationMetier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::INFORMATION_METIER, $informationMetier, $comparison);
    }

    /**
     * Filter the query on the nb_flux column
     *
     * Example usage:
     * <code>
     * $query->filterByNbFlux(1234); // WHERE nb_flux = 1234
     * $query->filterByNbFlux(array(12, 34)); // WHERE nb_flux IN (12, 34)
     * $query->filterByNbFlux(array('min' => 12)); // WHERE nb_flux >= 12
     * $query->filterByNbFlux(array('max' => 12)); // WHERE nb_flux <= 12
     * </code>
     *
     * @param     mixed $nbFlux The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByNbFlux($nbFlux = null, $comparison = null)
    {
        if (is_array($nbFlux)) {
            $useMinMax = false;
            if (isset($nbFlux['min'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::NB_FLUX, $nbFlux['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbFlux['max'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::NB_FLUX, $nbFlux['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::NB_FLUX, $nbFlux, $comparison);
    }

    /**
     * Filter the query on the debut_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByDebutExecution('2011-03-14'); // WHERE debut_execution = '2011-03-14'
     * $query->filterByDebutExecution('now'); // WHERE debut_execution = '2011-03-14'
     * $query->filterByDebutExecution(array('max' => 'yesterday')); // WHERE debut_execution > '2011-03-13'
     * </code>
     *
     * @param     mixed $debutExecution The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByDebutExecution($debutExecution = null, $comparison = null)
    {
        if (is_array($debutExecution)) {
            $useMinMax = false;
            if (isset($debutExecution['min'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::DEBUT_EXECUTION, $debutExecution['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($debutExecution['max'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::DEBUT_EXECUTION, $debutExecution['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::DEBUT_EXECUTION, $debutExecution, $comparison);
    }

    /**
     * Filter the query on the fin_execution column
     *
     * Example usage:
     * <code>
     * $query->filterByFinExecution('2011-03-14'); // WHERE fin_execution = '2011-03-14'
     * $query->filterByFinExecution('now'); // WHERE fin_execution = '2011-03-14'
     * $query->filterByFinExecution(array('max' => 'yesterday')); // WHERE fin_execution > '2011-03-13'
     * </code>
     *
     * @param     mixed $finExecution The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByFinExecution($finExecution = null, $comparison = null)
    {
        if (is_array($finExecution)) {
            $useMinMax = false;
            if (isset($finExecution['min'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::FIN_EXECUTION, $finExecution['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($finExecution['max'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::FIN_EXECUTION, $finExecution['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::FIN_EXECUTION, $finExecution, $comparison);
    }

    /**
     * Filter the query on the id_echanges_interfaces column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEchangesInterfaces(1234); // WHERE id_echanges_interfaces = 1234
     * $query->filterByIdEchangesInterfaces(array(12, 34)); // WHERE id_echanges_interfaces IN (12, 34)
     * $query->filterByIdEchangesInterfaces(array('min' => 12)); // WHERE id_echanges_interfaces >= 12
     * $query->filterByIdEchangesInterfaces(array('max' => 12)); // WHERE id_echanges_interfaces <= 12
     * </code>
     *
     * @see       filterByCommonEchangesInterfacesRelatedByIdEchangesInterfaces()
     *
     * @param     mixed $idEchangesInterfaces The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByIdEchangesInterfaces($idEchangesInterfaces = null, $comparison = null)
    {
        if (is_array($idEchangesInterfaces)) {
            $useMinMax = false;
            if (isset($idEchangesInterfaces['min'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::ID_ECHANGES_INTERFACES, $idEchangesInterfaces['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEchangesInterfaces['max'])) {
                $this->addUsingAlias(CommonEchangesInterfacesPeer::ID_ECHANGES_INTERFACES, $idEchangesInterfaces['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::ID_ECHANGES_INTERFACES, $idEchangesInterfaces, $comparison);
    }

    /**
     * Filter the query on the nom_interface column
     *
     * Example usage:
     * <code>
     * $query->filterByNomInterface('fooValue');   // WHERE nom_interface = 'fooValue'
     * $query->filterByNomInterface('%fooValue%'); // WHERE nom_interface LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomInterface The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function filterByNomInterface($nomInterface = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomInterface)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomInterface)) {
                $nomInterface = str_replace('*', '%', $nomInterface);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangesInterfacesPeer::NOM_INTERFACE, $nomInterface, $comparison);
    }

    /**
     * Filter the query by a related CommonCodeRetour object
     *
     * @param   CommonCodeRetour|PropelObjectCollection $commonCodeRetour The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangesInterfacesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonCodeRetour($commonCodeRetour, $comparison = null)
    {
        if ($commonCodeRetour instanceof CommonCodeRetour) {
            return $this
                ->addUsingAlias(CommonEchangesInterfacesPeer::CODE_RETOUR, $commonCodeRetour->getCode(), $comparison);
        } elseif ($commonCodeRetour instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangesInterfacesPeer::CODE_RETOUR, $commonCodeRetour->toKeyValue('PrimaryKey', 'Code'), $comparison);
        } else {
            throw new PropelException('filterByCommonCodeRetour() only accepts arguments of type CommonCodeRetour or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonCodeRetour relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function joinCommonCodeRetour($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonCodeRetour');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonCodeRetour');
        }

        return $this;
    }

    /**
     * Use the CommonCodeRetour relation CommonCodeRetour object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonCodeRetourQuery A secondary query class using the current class as primary query
     */
    public function useCommonCodeRetourQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonCodeRetour($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonCodeRetour', '\Application\Propel\Mpe\CommonCodeRetourQuery');
    }

    /**
     * Filter the query by a related CommonEchangesInterfaces object
     *
     * @param   CommonEchangesInterfaces|PropelObjectCollection $commonEchangesInterfaces The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangesInterfacesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangesInterfacesRelatedByIdEchangesInterfaces($commonEchangesInterfaces, $comparison = null)
    {
        if ($commonEchangesInterfaces instanceof CommonEchangesInterfaces) {
            return $this
                ->addUsingAlias(CommonEchangesInterfacesPeer::ID_ECHANGES_INTERFACES, $commonEchangesInterfaces->getId(), $comparison);
        } elseif ($commonEchangesInterfaces instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangesInterfacesPeer::ID_ECHANGES_INTERFACES, $commonEchangesInterfaces->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonEchangesInterfacesRelatedByIdEchangesInterfaces() only accepts arguments of type CommonEchangesInterfaces or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangesInterfacesRelatedByIdEchangesInterfaces relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function joinCommonEchangesInterfacesRelatedByIdEchangesInterfaces($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangesInterfacesRelatedByIdEchangesInterfaces');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangesInterfacesRelatedByIdEchangesInterfaces');
        }

        return $this;
    }

    /**
     * Use the CommonEchangesInterfacesRelatedByIdEchangesInterfaces relation CommonEchangesInterfaces object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangesInterfacesQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangesInterfacesRelatedByIdEchangesInterfacesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangesInterfacesRelatedByIdEchangesInterfaces($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangesInterfacesRelatedByIdEchangesInterfaces', '\Application\Propel\Mpe\CommonEchangesInterfacesQuery');
    }

    /**
     * Filter the query by a related CommonEchangesInterfaces object
     *
     * @param   CommonEchangesInterfaces|PropelObjectCollection $commonEchangesInterfaces  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangesInterfacesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangesInterfacesRelatedById($commonEchangesInterfaces, $comparison = null)
    {
        if ($commonEchangesInterfaces instanceof CommonEchangesInterfaces) {
            return $this
                ->addUsingAlias(CommonEchangesInterfacesPeer::ID, $commonEchangesInterfaces->getIdEchangesInterfaces(), $comparison);
        } elseif ($commonEchangesInterfaces instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangesInterfacesRelatedByIdQuery()
                ->filterByPrimaryKeys($commonEchangesInterfaces->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangesInterfacesRelatedById() only accepts arguments of type CommonEchangesInterfaces or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangesInterfacesRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function joinCommonEchangesInterfacesRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangesInterfacesRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangesInterfacesRelatedById');
        }

        return $this;
    }

    /**
     * Use the CommonEchangesInterfacesRelatedById relation CommonEchangesInterfaces object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangesInterfacesQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangesInterfacesRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangesInterfacesRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangesInterfacesRelatedById', '\Application\Propel\Mpe\CommonEchangesInterfacesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonEchangesInterfaces $commonEchangesInterfaces Object to remove from the list of results
     *
     * @return CommonEchangesInterfacesQuery The current query, for fluid interface
     */
    public function prune($commonEchangesInterfaces = null)
    {
        if ($commonEchangesInterfaces) {
            $this->addUsingAlias(CommonEchangesInterfacesPeer::ID, $commonEchangesInterfaces->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
