<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDocument;
use Application\Propel\Mpe\CommonTDocumentPeer;
use Application\Propel\Mpe\CommonTDocumentQuery;

/**
 * Base class that represents a query for the 't_document' table.
 *
 *
 *
 * @method CommonTDocumentQuery orderByIdDocument($order = Criteria::ASC) Order by the id_document column
 * @method CommonTDocumentQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method CommonTDocumentQuery orderByTaille($order = Criteria::ASC) Order by the taille column
 * @method CommonTDocumentQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTDocumentQuery orderByConsultationRef($order = Criteria::ASC) Order by the consultation_ref column
 * @method CommonTDocumentQuery orderByLot($order = Criteria::ASC) Order by the lot column
 * @method CommonTDocumentQuery orderByEmpreinte($order = Criteria::ASC) Order by the empreinte column
 * @method CommonTDocumentQuery orderByTypeEmpreinte($order = Criteria::ASC) Order by the type_empreinte column
 * @method CommonTDocumentQuery orderByIdTypeDocument($order = Criteria::ASC) Order by the id_type_document column
 * @method CommonTDocumentQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTDocumentQuery orderByIdFichier($order = Criteria::ASC) Order by the id_fichier column
 * @method CommonTDocumentQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 *
 * @method CommonTDocumentQuery groupByIdDocument() Group by the id_document column
 * @method CommonTDocumentQuery groupByNom() Group by the nom column
 * @method CommonTDocumentQuery groupByTaille() Group by the taille column
 * @method CommonTDocumentQuery groupByOrganisme() Group by the organisme column
 * @method CommonTDocumentQuery groupByConsultationRef() Group by the consultation_ref column
 * @method CommonTDocumentQuery groupByLot() Group by the lot column
 * @method CommonTDocumentQuery groupByEmpreinte() Group by the empreinte column
 * @method CommonTDocumentQuery groupByTypeEmpreinte() Group by the type_empreinte column
 * @method CommonTDocumentQuery groupByIdTypeDocument() Group by the id_type_document column
 * @method CommonTDocumentQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTDocumentQuery groupByIdFichier() Group by the id_fichier column
 * @method CommonTDocumentQuery groupByConsultationId() Group by the consultation_id column
 *
 * @method CommonTDocumentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTDocumentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTDocumentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTDocument findOne(PropelPDO $con = null) Return the first CommonTDocument matching the query
 * @method CommonTDocument findOneOrCreate(PropelPDO $con = null) Return the first CommonTDocument matching the query, or a new CommonTDocument object populated from the query conditions when no match is found
 *
 * @method CommonTDocument findOneByNom(string $nom) Return the first CommonTDocument filtered by the nom column
 * @method CommonTDocument findOneByTaille(string $taille) Return the first CommonTDocument filtered by the taille column
 * @method CommonTDocument findOneByOrganisme(string $organisme) Return the first CommonTDocument filtered by the organisme column
 * @method CommonTDocument findOneByConsultationRef(int $consultation_ref) Return the first CommonTDocument filtered by the consultation_ref column
 * @method CommonTDocument findOneByLot(int $lot) Return the first CommonTDocument filtered by the lot column
 * @method CommonTDocument findOneByEmpreinte(string $empreinte) Return the first CommonTDocument filtered by the empreinte column
 * @method CommonTDocument findOneByTypeEmpreinte(string $type_empreinte) Return the first CommonTDocument filtered by the type_empreinte column
 * @method CommonTDocument findOneByIdTypeDocument(int $id_type_document) Return the first CommonTDocument filtered by the id_type_document column
 * @method CommonTDocument findOneByDateCreation(string $date_creation) Return the first CommonTDocument filtered by the date_creation column
 * @method CommonTDocument findOneByIdFichier(int $id_fichier) Return the first CommonTDocument filtered by the id_fichier column
 * @method CommonTDocument findOneByConsultationId(int $consultation_id) Return the first CommonTDocument filtered by the consultation_id column
 *
 * @method array findByIdDocument(int $id_document) Return CommonTDocument objects filtered by the id_document column
 * @method array findByNom(string $nom) Return CommonTDocument objects filtered by the nom column
 * @method array findByTaille(string $taille) Return CommonTDocument objects filtered by the taille column
 * @method array findByOrganisme(string $organisme) Return CommonTDocument objects filtered by the organisme column
 * @method array findByConsultationRef(int $consultation_ref) Return CommonTDocument objects filtered by the consultation_ref column
 * @method array findByLot(int $lot) Return CommonTDocument objects filtered by the lot column
 * @method array findByEmpreinte(string $empreinte) Return CommonTDocument objects filtered by the empreinte column
 * @method array findByTypeEmpreinte(string $type_empreinte) Return CommonTDocument objects filtered by the type_empreinte column
 * @method array findByIdTypeDocument(int $id_type_document) Return CommonTDocument objects filtered by the id_type_document column
 * @method array findByDateCreation(string $date_creation) Return CommonTDocument objects filtered by the date_creation column
 * @method array findByIdFichier(int $id_fichier) Return CommonTDocument objects filtered by the id_fichier column
 * @method array findByConsultationId(int $consultation_id) Return CommonTDocument objects filtered by the consultation_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDocumentQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTDocumentQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTDocument', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTDocumentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTDocumentQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTDocumentQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTDocumentQuery) {
            return $criteria;
        }
        $query = new CommonTDocumentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTDocument|CommonTDocument[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTDocumentPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDocument A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdDocument($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDocument A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_document`, `nom`, `taille`, `organisme`, `consultation_ref`, `lot`, `empreinte`, `type_empreinte`, `id_type_document`, `date_creation`, `id_fichier`, `consultation_id` FROM `t_document` WHERE `id_document` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTDocument();
            $obj->hydrate($row);
            CommonTDocumentPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTDocument|CommonTDocument[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTDocument[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTDocumentPeer::ID_DOCUMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTDocumentPeer::ID_DOCUMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_document column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDocument(1234); // WHERE id_document = 1234
     * $query->filterByIdDocument(array(12, 34)); // WHERE id_document IN (12, 34)
     * $query->filterByIdDocument(array('min' => 12)); // WHERE id_document >= 12
     * $query->filterByIdDocument(array('max' => 12)); // WHERE id_document <= 12
     * </code>
     *
     * @param     mixed $idDocument The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByIdDocument($idDocument = null, $comparison = null)
    {
        if (is_array($idDocument)) {
            $useMinMax = false;
            if (isset($idDocument['min'])) {
                $this->addUsingAlias(CommonTDocumentPeer::ID_DOCUMENT, $idDocument['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDocument['max'])) {
                $this->addUsingAlias(CommonTDocumentPeer::ID_DOCUMENT, $idDocument['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::ID_DOCUMENT, $idDocument, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%'); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nom)) {
                $nom = str_replace('*', '%', $nom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the taille column
     *
     * Example usage:
     * <code>
     * $query->filterByTaille('fooValue');   // WHERE taille = 'fooValue'
     * $query->filterByTaille('%fooValue%'); // WHERE taille LIKE '%fooValue%'
     * </code>
     *
     * @param     string $taille The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByTaille($taille = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($taille)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $taille)) {
                $taille = str_replace('*', '%', $taille);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::TAILLE, $taille, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the consultation_ref column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationRef(1234); // WHERE consultation_ref = 1234
     * $query->filterByConsultationRef(array(12, 34)); // WHERE consultation_ref IN (12, 34)
     * $query->filterByConsultationRef(array('min' => 12)); // WHERE consultation_ref >= 12
     * $query->filterByConsultationRef(array('max' => 12)); // WHERE consultation_ref <= 12
     * </code>
     *
     * @param     mixed $consultationRef The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByConsultationRef($consultationRef = null, $comparison = null)
    {
        if (is_array($consultationRef)) {
            $useMinMax = false;
            if (isset($consultationRef['min'])) {
                $this->addUsingAlias(CommonTDocumentPeer::CONSULTATION_REF, $consultationRef['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationRef['max'])) {
                $this->addUsingAlias(CommonTDocumentPeer::CONSULTATION_REF, $consultationRef['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::CONSULTATION_REF, $consultationRef, $comparison);
    }

    /**
     * Filter the query on the lot column
     *
     * Example usage:
     * <code>
     * $query->filterByLot(1234); // WHERE lot = 1234
     * $query->filterByLot(array(12, 34)); // WHERE lot IN (12, 34)
     * $query->filterByLot(array('min' => 12)); // WHERE lot >= 12
     * $query->filterByLot(array('max' => 12)); // WHERE lot <= 12
     * </code>
     *
     * @param     mixed $lot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByLot($lot = null, $comparison = null)
    {
        if (is_array($lot)) {
            $useMinMax = false;
            if (isset($lot['min'])) {
                $this->addUsingAlias(CommonTDocumentPeer::LOT, $lot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lot['max'])) {
                $this->addUsingAlias(CommonTDocumentPeer::LOT, $lot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::LOT, $lot, $comparison);
    }

    /**
     * Filter the query on the empreinte column
     *
     * Example usage:
     * <code>
     * $query->filterByEmpreinte('fooValue');   // WHERE empreinte = 'fooValue'
     * $query->filterByEmpreinte('%fooValue%'); // WHERE empreinte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $empreinte The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByEmpreinte($empreinte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($empreinte)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $empreinte)) {
                $empreinte = str_replace('*', '%', $empreinte);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::EMPREINTE, $empreinte, $comparison);
    }

    /**
     * Filter the query on the type_empreinte column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeEmpreinte('fooValue');   // WHERE type_empreinte = 'fooValue'
     * $query->filterByTypeEmpreinte('%fooValue%'); // WHERE type_empreinte LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeEmpreinte The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByTypeEmpreinte($typeEmpreinte = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeEmpreinte)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeEmpreinte)) {
                $typeEmpreinte = str_replace('*', '%', $typeEmpreinte);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::TYPE_EMPREINTE, $typeEmpreinte, $comparison);
    }

    /**
     * Filter the query on the id_type_document column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeDocument(1234); // WHERE id_type_document = 1234
     * $query->filterByIdTypeDocument(array(12, 34)); // WHERE id_type_document IN (12, 34)
     * $query->filterByIdTypeDocument(array('min' => 12)); // WHERE id_type_document >= 12
     * $query->filterByIdTypeDocument(array('max' => 12)); // WHERE id_type_document <= 12
     * </code>
     *
     * @param     mixed $idTypeDocument The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByIdTypeDocument($idTypeDocument = null, $comparison = null)
    {
        if (is_array($idTypeDocument)) {
            $useMinMax = false;
            if (isset($idTypeDocument['min'])) {
                $this->addUsingAlias(CommonTDocumentPeer::ID_TYPE_DOCUMENT, $idTypeDocument['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeDocument['max'])) {
                $this->addUsingAlias(CommonTDocumentPeer::ID_TYPE_DOCUMENT, $idTypeDocument['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::ID_TYPE_DOCUMENT, $idTypeDocument, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTDocumentPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTDocumentPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the id_fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByIdFichier(1234); // WHERE id_fichier = 1234
     * $query->filterByIdFichier(array(12, 34)); // WHERE id_fichier IN (12, 34)
     * $query->filterByIdFichier(array('min' => 12)); // WHERE id_fichier >= 12
     * $query->filterByIdFichier(array('max' => 12)); // WHERE id_fichier <= 12
     * </code>
     *
     * @param     mixed $idFichier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByIdFichier($idFichier = null, $comparison = null)
    {
        if (is_array($idFichier)) {
            $useMinMax = false;
            if (isset($idFichier['min'])) {
                $this->addUsingAlias(CommonTDocumentPeer::ID_FICHIER, $idFichier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idFichier['max'])) {
                $this->addUsingAlias(CommonTDocumentPeer::ID_FICHIER, $idFichier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::ID_FICHIER, $idFichier, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonTDocumentPeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonTDocumentPeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentPeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTDocument $commonTDocument Object to remove from the list of results
     *
     * @return CommonTDocumentQuery The current query, for fluid interface
     */
    public function prune($commonTDocument = null)
    {
        if ($commonTDocument) {
            $this->addUsingAlias(CommonTDocumentPeer::ID_DOCUMENT, $commonTDocument->getIdDocument(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
