<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocApplication;
use Application\Propel\Mpe\CommonEchangeDocApplicationClient;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganisme;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientPeer;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientQuery;

/**
 * Base class that represents a query for the 'echange_doc_application_client' table.
 *
 *
 *
 * @method CommonEchangeDocApplicationClientQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonEchangeDocApplicationClientQuery orderByEchangeDocApplicationId($order = Criteria::ASC) Order by the echange_doc_application_id column
 * @method CommonEchangeDocApplicationClientQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method CommonEchangeDocApplicationClientQuery orderByLibelle($order = Criteria::ASC) Order by the libelle column
 * @method CommonEchangeDocApplicationClientQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method CommonEchangeDocApplicationClientQuery orderByCheminementSignature($order = Criteria::ASC) Order by the cheminement_signature column
 * @method CommonEchangeDocApplicationClientQuery orderByCheminementGed($order = Criteria::ASC) Order by the cheminement_ged column
 * @method CommonEchangeDocApplicationClientQuery orderByCheminementSae($order = Criteria::ASC) Order by the cheminement_sae column
 * @method CommonEchangeDocApplicationClientQuery orderByCheminementTdt($order = Criteria::ASC) Order by the cheminement_tdt column
 * @method CommonEchangeDocApplicationClientQuery orderByClassification1($order = Criteria::ASC) Order by the classification_1 column
 * @method CommonEchangeDocApplicationClientQuery orderByClassification2($order = Criteria::ASC) Order by the classification_2 column
 * @method CommonEchangeDocApplicationClientQuery orderByClassification3($order = Criteria::ASC) Order by the classification_3 column
 * @method CommonEchangeDocApplicationClientQuery orderByClassification4($order = Criteria::ASC) Order by the classification_4 column
 * @method CommonEchangeDocApplicationClientQuery orderByClassification5($order = Criteria::ASC) Order by the classification_5 column
 * @method CommonEchangeDocApplicationClientQuery orderByMultiDocsPrincipaux($order = Criteria::ASC) Order by the multi_docs_principaux column
 * @method CommonEchangeDocApplicationClientQuery orderByEnvoiAutoArchivage($order = Criteria::ASC) Order by the envoi_auto_archivage column
 *
 * @method CommonEchangeDocApplicationClientQuery groupById() Group by the id column
 * @method CommonEchangeDocApplicationClientQuery groupByEchangeDocApplicationId() Group by the echange_doc_application_id column
 * @method CommonEchangeDocApplicationClientQuery groupByCode() Group by the code column
 * @method CommonEchangeDocApplicationClientQuery groupByLibelle() Group by the libelle column
 * @method CommonEchangeDocApplicationClientQuery groupByActif() Group by the actif column
 * @method CommonEchangeDocApplicationClientQuery groupByCheminementSignature() Group by the cheminement_signature column
 * @method CommonEchangeDocApplicationClientQuery groupByCheminementGed() Group by the cheminement_ged column
 * @method CommonEchangeDocApplicationClientQuery groupByCheminementSae() Group by the cheminement_sae column
 * @method CommonEchangeDocApplicationClientQuery groupByCheminementTdt() Group by the cheminement_tdt column
 * @method CommonEchangeDocApplicationClientQuery groupByClassification1() Group by the classification_1 column
 * @method CommonEchangeDocApplicationClientQuery groupByClassification2() Group by the classification_2 column
 * @method CommonEchangeDocApplicationClientQuery groupByClassification3() Group by the classification_3 column
 * @method CommonEchangeDocApplicationClientQuery groupByClassification4() Group by the classification_4 column
 * @method CommonEchangeDocApplicationClientQuery groupByClassification5() Group by the classification_5 column
 * @method CommonEchangeDocApplicationClientQuery groupByMultiDocsPrincipaux() Group by the multi_docs_principaux column
 * @method CommonEchangeDocApplicationClientQuery groupByEnvoiAutoArchivage() Group by the envoi_auto_archivage column
 *
 * @method CommonEchangeDocApplicationClientQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonEchangeDocApplicationClientQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonEchangeDocApplicationClientQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonEchangeDocApplicationClientQuery leftJoinCommonEchangeDocApplication($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocApplication relation
 * @method CommonEchangeDocApplicationClientQuery rightJoinCommonEchangeDocApplication($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocApplication relation
 * @method CommonEchangeDocApplicationClientQuery innerJoinCommonEchangeDocApplication($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocApplication relation
 *
 * @method CommonEchangeDocApplicationClientQuery leftJoinCommonEchangeDoc($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDoc relation
 * @method CommonEchangeDocApplicationClientQuery rightJoinCommonEchangeDoc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDoc relation
 * @method CommonEchangeDocApplicationClientQuery innerJoinCommonEchangeDoc($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDoc relation
 *
 * @method CommonEchangeDocApplicationClientQuery leftJoinCommonEchangeDocApplicationClientOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocApplicationClientOrganisme relation
 * @method CommonEchangeDocApplicationClientQuery rightJoinCommonEchangeDocApplicationClientOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocApplicationClientOrganisme relation
 * @method CommonEchangeDocApplicationClientQuery innerJoinCommonEchangeDocApplicationClientOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocApplicationClientOrganisme relation
 *
 * @method CommonEchangeDocApplicationClient findOne(PropelPDO $con = null) Return the first CommonEchangeDocApplicationClient matching the query
 * @method CommonEchangeDocApplicationClient findOneOrCreate(PropelPDO $con = null) Return the first CommonEchangeDocApplicationClient matching the query, or a new CommonEchangeDocApplicationClient object populated from the query conditions when not found
 *
 * @method CommonEchangeDocApplicationClient findOneByEchangeDocApplicationId(int $echange_doc_application_id) Return the first CommonEchangeDocApplicationClient filtered by the echange_doc_application_id column
 * @method CommonEchangeDocApplicationClient findOneByCode(string $code) Return the first CommonEchangeDocApplicationClient filtered by the code column
 * @method CommonEchangeDocApplicationClient findOneByLibelle(string $libelle) Return the first CommonEchangeDocApplicationClient filtered by the libelle column
 * @method CommonEchangeDocApplicationClient findOneByActif(boolean $actif) Return the first CommonEchangeDocApplicationClient filtered by the actif column
 * @method CommonEchangeDocApplicationClient findOneByCheminementSignature(boolean $cheminement_signature) Return the first CommonEchangeDocApplicationClient filtered by the cheminement_signature column
 * @method CommonEchangeDocApplicationClient findOneByCheminementGed(boolean $cheminement_ged) Return the first CommonEchangeDocApplicationClient filtered by the cheminement_ged column
 * @method CommonEchangeDocApplicationClient findOneByCheminementSae(boolean $cheminement_sae) Return the first CommonEchangeDocApplicationClient filtered by the cheminement_sae column
 * @method CommonEchangeDocApplicationClient findOneByCheminementTdt(boolean $cheminement_tdt) Return the first CommonEchangeDocApplicationClient filtered by the cheminement_tdt column
 * @method CommonEchangeDocApplicationClient findOneByClassification1(string $classification_1) Return the first CommonEchangeDocApplicationClient filtered by the classification_1 column
 * @method CommonEchangeDocApplicationClient findOneByClassification2(string $classification_2) Return the first CommonEchangeDocApplicationClient filtered by the classification_2 column
 * @method CommonEchangeDocApplicationClient findOneByClassification3(string $classification_3) Return the first CommonEchangeDocApplicationClient filtered by the classification_3 column
 * @method CommonEchangeDocApplicationClient findOneByClassification4(string $classification_4) Return the first CommonEchangeDocApplicationClient filtered by the classification_4 column
 * @method CommonEchangeDocApplicationClient findOneByClassification5(string $classification_5) Return the first CommonEchangeDocApplicationClient filtered by the classification_5 column
 * @method CommonEchangeDocApplicationClient findOneByMultiDocsPrincipaux(boolean $multi_docs_principaux) Return the first CommonEchangeDocApplicationClient filtered by the multi_docs_principaux column
 * @method CommonEchangeDocApplicationClient findOneByEnvoiAutoArchivage(boolean $envoi_auto_archivage) Return the first CommonEchangeDocApplicationClient filtered by the envoi_auto_archivage column
 *
 * @method array findById(int $id) Return CommonEchangeDocApplicationClient objects filtered by the id column
 * @method array findByEchangeDocApplicationId(int $echange_doc_application_id) Return CommonEchangeDocApplicationClient objects filtered by the echange_doc_application_id column
 * @method array findByCode(string $code) Return CommonEchangeDocApplicationClient objects filtered by the code column
 * @method array findByLibelle(string $libelle) Return CommonEchangeDocApplicationClient objects filtered by the libelle column
 * @method array findByActif(boolean $actif) Return CommonEchangeDocApplicationClient objects filtered by the actif column
 * @method array findByCheminementSignature(boolean $cheminement_signature) Return CommonEchangeDocApplicationClient objects filtered by the cheminement_signature column
 * @method array findByCheminementGed(boolean $cheminement_ged) Return CommonEchangeDocApplicationClient objects filtered by the cheminement_ged column
 * @method array findByCheminementSae(boolean $cheminement_sae) Return CommonEchangeDocApplicationClient objects filtered by the cheminement_sae column
 * @method array findByCheminementTdt(boolean $cheminement_tdt) Return CommonEchangeDocApplicationClient objects filtered by the cheminement_tdt column
 * @method array findByClassification1(string $classification_1) Return CommonEchangeDocApplicationClient objects filtered by the classification_1 column
 * @method array findByClassification2(string $classification_2) Return CommonEchangeDocApplicationClient objects filtered by the classification_2 column
 * @method array findByClassification3(string $classification_3) Return CommonEchangeDocApplicationClient objects filtered by the classification_3 column
 * @method array findByClassification4(string $classification_4) Return CommonEchangeDocApplicationClient objects filtered by the classification_4 column
 * @method array findByClassification5(string $classification_5) Return CommonEchangeDocApplicationClient objects filtered by the classification_5 column
 * @method array findByMultiDocsPrincipaux(boolean $multi_docs_principaux) Return CommonEchangeDocApplicationClient objects filtered by the multi_docs_principaux column
 * @method array findByEnvoiAutoArchivage(boolean $envoi_auto_archivage) Return CommonEchangeDocApplicationClient objects filtered by the envoi_auto_archivage column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocApplicationClientQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonEchangeDocApplicationClientQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonEchangeDocApplicationClient', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonEchangeDocApplicationClientQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonEchangeDocApplicationClientQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonEchangeDocApplicationClientQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonEchangeDocApplicationClientQuery) {
            return $criteria;
        }
        $query = new CommonEchangeDocApplicationClientQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonEchangeDocApplicationClient|CommonEchangeDocApplicationClient[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonEchangeDocApplicationClientPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDocApplicationClient A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDocApplicationClient A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `echange_doc_application_id`, `code`, `libelle`, `actif`, `cheminement_signature`, `cheminement_ged`, `cheminement_sae`, `cheminement_tdt`, `classification_1`, `classification_2`, `classification_3`, `classification_4`, `classification_5`, `multi_docs_principaux`, `envoi_auto_archivage` FROM `echange_doc_application_client` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonEchangeDocApplicationClient();
            $obj->hydrate($row);
            CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonEchangeDocApplicationClient|CommonEchangeDocApplicationClient[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonEchangeDocApplicationClient[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the echange_doc_application_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEchangeDocApplicationId(1234); // WHERE echange_doc_application_id = 1234
     * $query->filterByEchangeDocApplicationId(array(12, 34)); // WHERE echange_doc_application_id IN (12, 34)
     * $query->filterByEchangeDocApplicationId(array('min' => 12)); // WHERE echange_doc_application_id >= 12
     * $query->filterByEchangeDocApplicationId(array('max' => 12)); // WHERE echange_doc_application_id <= 12
     * </code>
     *
     * @see       filterByCommonEchangeDocApplication()
     *
     * @param     mixed $echangeDocApplicationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByEchangeDocApplicationId($echangeDocApplicationId = null, $comparison = null)
    {
        if (is_array($echangeDocApplicationId)) {
            $useMinMax = false;
            if (isset($echangeDocApplicationId['min'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, $echangeDocApplicationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($echangeDocApplicationId['max'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, $echangeDocApplicationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, $echangeDocApplicationId, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the libelle column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelle('fooValue');   // WHERE libelle = 'fooValue'
     * $query->filterByLibelle('%fooValue%'); // WHERE libelle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByLibelle($libelle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelle)) {
                $libelle = str_replace('*', '%', $libelle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::LIBELLE, $libelle, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif(true); // WHERE actif = true
     * $query->filterByActif('yes'); // WHERE actif = true
     * </code>
     *
     * @param     boolean|string $actif The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (is_string($actif)) {
            $actif = in_array(strtolower($actif), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the cheminement_signature column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminementSignature(true); // WHERE cheminement_signature = true
     * $query->filterByCheminementSignature('yes'); // WHERE cheminement_signature = true
     * </code>
     *
     * @param     boolean|string $cheminementSignature The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByCheminementSignature($cheminementSignature = null, $comparison = null)
    {
        if (is_string($cheminementSignature)) {
            $cheminementSignature = in_array(strtolower($cheminementSignature), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SIGNATURE, $cheminementSignature, $comparison);
    }

    /**
     * Filter the query on the cheminement_ged column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminementGed(true); // WHERE cheminement_ged = true
     * $query->filterByCheminementGed('yes'); // WHERE cheminement_ged = true
     * </code>
     *
     * @param     boolean|string $cheminementGed The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByCheminementGed($cheminementGed = null, $comparison = null)
    {
        if (is_string($cheminementGed)) {
            $cheminementGed = in_array(strtolower($cheminementGed), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_GED, $cheminementGed, $comparison);
    }

    /**
     * Filter the query on the cheminement_sae column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminementSae(true); // WHERE cheminement_sae = true
     * $query->filterByCheminementSae('yes'); // WHERE cheminement_sae = true
     * </code>
     *
     * @param     boolean|string $cheminementSae The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByCheminementSae($cheminementSae = null, $comparison = null)
    {
        if (is_string($cheminementSae)) {
            $cheminementSae = in_array(strtolower($cheminementSae), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SAE, $cheminementSae, $comparison);
    }

    /**
     * Filter the query on the cheminement_tdt column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminementTdt(true); // WHERE cheminement_tdt = true
     * $query->filterByCheminementTdt('yes'); // WHERE cheminement_tdt = true
     * </code>
     *
     * @param     boolean|string $cheminementTdt The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByCheminementTdt($cheminementTdt = null, $comparison = null)
    {
        if (is_string($cheminementTdt)) {
            $cheminementTdt = in_array(strtolower($cheminementTdt), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_TDT, $cheminementTdt, $comparison);
    }

    /**
     * Filter the query on the classification_1 column
     *
     * Example usage:
     * <code>
     * $query->filterByClassification1('fooValue');   // WHERE classification_1 = 'fooValue'
     * $query->filterByClassification1('%fooValue%'); // WHERE classification_1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classification1 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByClassification1($classification1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classification1)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $classification1)) {
                $classification1 = str_replace('*', '%', $classification1);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_1, $classification1, $comparison);
    }

    /**
     * Filter the query on the classification_2 column
     *
     * Example usage:
     * <code>
     * $query->filterByClassification2('fooValue');   // WHERE classification_2 = 'fooValue'
     * $query->filterByClassification2('%fooValue%'); // WHERE classification_2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classification2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByClassification2($classification2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classification2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $classification2)) {
                $classification2 = str_replace('*', '%', $classification2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_2, $classification2, $comparison);
    }

    /**
     * Filter the query on the classification_3 column
     *
     * Example usage:
     * <code>
     * $query->filterByClassification3('fooValue');   // WHERE classification_3 = 'fooValue'
     * $query->filterByClassification3('%fooValue%'); // WHERE classification_3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classification3 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByClassification3($classification3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classification3)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $classification3)) {
                $classification3 = str_replace('*', '%', $classification3);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_3, $classification3, $comparison);
    }

    /**
     * Filter the query on the classification_4 column
     *
     * Example usage:
     * <code>
     * $query->filterByClassification4('fooValue');   // WHERE classification_4 = 'fooValue'
     * $query->filterByClassification4('%fooValue%'); // WHERE classification_4 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classification4 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByClassification4($classification4 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classification4)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $classification4)) {
                $classification4 = str_replace('*', '%', $classification4);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_4, $classification4, $comparison);
    }

    /**
     * Filter the query on the classification_5 column
     *
     * Example usage:
     * <code>
     * $query->filterByClassification5('fooValue');   // WHERE classification_5 = 'fooValue'
     * $query->filterByClassification5('%fooValue%'); // WHERE classification_5 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $classification5 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByClassification5($classification5 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($classification5)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $classification5)) {
                $classification5 = str_replace('*', '%', $classification5);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_5, $classification5, $comparison);
    }

    /**
     * Filter the query on the multi_docs_principaux column
     *
     * Example usage:
     * <code>
     * $query->filterByMultiDocsPrincipaux(true); // WHERE multi_docs_principaux = true
     * $query->filterByMultiDocsPrincipaux('yes'); // WHERE multi_docs_principaux = true
     * </code>
     *
     * @param     boolean|string $multiDocsPrincipaux The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByMultiDocsPrincipaux($multiDocsPrincipaux = null, $comparison = null)
    {
        if (is_string($multiDocsPrincipaux)) {
            $multiDocsPrincipaux = in_array(strtolower($multiDocsPrincipaux), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::MULTI_DOCS_PRINCIPAUX, $multiDocsPrincipaux, $comparison);
    }

    /**
     * Filter the query on the envoi_auto_archivage column
     *
     * Example usage:
     * <code>
     * $query->filterByEnvoiAutoArchivage(true); // WHERE envoi_auto_archivage = true
     * $query->filterByEnvoiAutoArchivage('yes'); // WHERE envoi_auto_archivage = true
     * </code>
     *
     * @param     boolean|string $envoiAutoArchivage The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function filterByEnvoiAutoArchivage($envoiAutoArchivage = null, $comparison = null)
    {
        if (is_string($envoiAutoArchivage)) {
            $envoiAutoArchivage = in_array(strtolower($envoiAutoArchivage), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ENVOI_AUTO_ARCHIVAGE, $envoiAutoArchivage, $comparison);
    }

    /**
     * Filter the query by a related CommonEchangeDocApplication object
     *
     * @param   CommonEchangeDocApplication|PropelObjectCollection $commonEchangeDocApplication The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocApplication($commonEchangeDocApplication, $comparison = null)
    {
        if ($commonEchangeDocApplication instanceof CommonEchangeDocApplication) {
            return $this
                ->addUsingAlias(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, $commonEchangeDocApplication->getId(), $comparison);
        } elseif ($commonEchangeDocApplication instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, $commonEchangeDocApplication->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonEchangeDocApplication() only accepts arguments of type CommonEchangeDocApplication or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocApplication relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocApplication($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocApplication');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocApplication');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocApplication relation CommonEchangeDocApplication object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocApplicationQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocApplicationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocApplication($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocApplication', '\Application\Propel\Mpe\CommonEchangeDocApplicationQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDoc object
     *
     * @param   CommonEchangeDoc|PropelObjectCollection $commonEchangeDoc  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDoc($commonEchangeDoc, $comparison = null)
    {
        if ($commonEchangeDoc instanceof CommonEchangeDoc) {
            return $this
                ->addUsingAlias(CommonEchangeDocApplicationClientPeer::ID, $commonEchangeDoc->getEchangeDocApplicationClientId(), $comparison);
        } elseif ($commonEchangeDoc instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocQuery()
                ->filterByPrimaryKeys($commonEchangeDoc->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDoc() only accepts arguments of type CommonEchangeDoc or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDoc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDoc($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDoc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDoc');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDoc relation CommonEchangeDoc object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDoc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDoc', '\Application\Propel\Mpe\CommonEchangeDocQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDocApplicationClientOrganisme object
     *
     * @param   CommonEchangeDocApplicationClientOrganisme|PropelObjectCollection $commonEchangeDocApplicationClientOrganisme  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocApplicationClientOrganisme($commonEchangeDocApplicationClientOrganisme, $comparison = null)
    {
        if ($commonEchangeDocApplicationClientOrganisme instanceof CommonEchangeDocApplicationClientOrganisme) {
            return $this
                ->addUsingAlias(CommonEchangeDocApplicationClientPeer::ID, $commonEchangeDocApplicationClientOrganisme->getEchangeDocApplicationClientId(), $comparison);
        } elseif ($commonEchangeDocApplicationClientOrganisme instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocApplicationClientOrganismeQuery()
                ->filterByPrimaryKeys($commonEchangeDocApplicationClientOrganisme->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDocApplicationClientOrganisme() only accepts arguments of type CommonEchangeDocApplicationClientOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocApplicationClientOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocApplicationClientOrganisme($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocApplicationClientOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocApplicationClientOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocApplicationClientOrganisme relation CommonEchangeDocApplicationClientOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocApplicationClientOrganismeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocApplicationClientOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocApplicationClientOrganisme', '\Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganismeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonEchangeDocApplicationClient $commonEchangeDocApplicationClient Object to remove from the list of results
     *
     * @return CommonEchangeDocApplicationClientQuery The current query, for fluid interface
     */
    public function prune($commonEchangeDocApplicationClient = null)
    {
        if ($commonEchangeDocApplicationClient) {
            $this->addUsingAlias(CommonEchangeDocApplicationClientPeer::ID, $commonEchangeDocApplicationClient->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
