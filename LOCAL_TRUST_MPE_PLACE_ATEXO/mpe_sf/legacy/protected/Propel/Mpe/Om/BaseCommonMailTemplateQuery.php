<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonMailTemplate;
use Application\Propel\Mpe\CommonMailTemplatePeer;
use Application\Propel\Mpe\CommonMailTemplateQuery;
use Application\Propel\Mpe\CommonMailType;

/**
 * Base class that represents a query for the 'mail_template' table.
 *
 *
 *
 * @method CommonMailTemplateQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonMailTemplateQuery orderByMailTypeId($order = Criteria::ASC) Order by the mail_type_id column
 * @method CommonMailTemplateQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method CommonMailTemplateQuery orderByObjet($order = Criteria::ASC) Order by the objet column
 * @method CommonMailTemplateQuery orderByCorps($order = Criteria::ASC) Order by the corps column
 * @method CommonMailTemplateQuery orderByOrdreAffichage($order = Criteria::ASC) Order by the ordre_affichage column
 * @method CommonMailTemplateQuery orderByEnvoiModalite($order = Criteria::ASC) Order by the envoi_modalite column
 * @method CommonMailTemplateQuery orderByEnvoiModaliteFigee($order = Criteria::ASC) Order by the envoi_modalite_figee column
 * @method CommonMailTemplateQuery orderByReponseAttendue($order = Criteria::ASC) Order by the reponse_attendue column
 * @method CommonMailTemplateQuery orderByReponseAttendueFigee($order = Criteria::ASC) Order by the reponse_attendue_figee column
 *
 * @method CommonMailTemplateQuery groupById() Group by the id column
 * @method CommonMailTemplateQuery groupByMailTypeId() Group by the mail_type_id column
 * @method CommonMailTemplateQuery groupByCode() Group by the code column
 * @method CommonMailTemplateQuery groupByObjet() Group by the objet column
 * @method CommonMailTemplateQuery groupByCorps() Group by the corps column
 * @method CommonMailTemplateQuery groupByOrdreAffichage() Group by the ordre_affichage column
 * @method CommonMailTemplateQuery groupByEnvoiModalite() Group by the envoi_modalite column
 * @method CommonMailTemplateQuery groupByEnvoiModaliteFigee() Group by the envoi_modalite_figee column
 * @method CommonMailTemplateQuery groupByReponseAttendue() Group by the reponse_attendue column
 * @method CommonMailTemplateQuery groupByReponseAttendueFigee() Group by the reponse_attendue_figee column
 *
 * @method CommonMailTemplateQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonMailTemplateQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonMailTemplateQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonMailTemplateQuery leftJoinCommonMailType($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonMailType relation
 * @method CommonMailTemplateQuery rightJoinCommonMailType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonMailType relation
 * @method CommonMailTemplateQuery innerJoinCommonMailType($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonMailType relation
 *
 * @method CommonMailTemplate findOne(PropelPDO $con = null) Return the first CommonMailTemplate matching the query
 * @method CommonMailTemplate findOneOrCreate(PropelPDO $con = null) Return the first CommonMailTemplate matching the query, or a new CommonMailTemplate object populated from the query conditions when no match is found
 *
 * @method CommonMailTemplate findOneByMailTypeId(int $mail_type_id) Return the first CommonMailTemplate filtered by the mail_type_id column
 * @method CommonMailTemplate findOneByCode(string $code) Return the first CommonMailTemplate filtered by the code column
 * @method CommonMailTemplate findOneByObjet(string $objet) Return the first CommonMailTemplate filtered by the objet column
 * @method CommonMailTemplate findOneByCorps(string $corps) Return the first CommonMailTemplate filtered by the corps column
 * @method CommonMailTemplate findOneByOrdreAffichage(int $ordre_affichage) Return the first CommonMailTemplate filtered by the ordre_affichage column
 * @method CommonMailTemplate findOneByEnvoiModalite(string $envoi_modalite) Return the first CommonMailTemplate filtered by the envoi_modalite column
 * @method CommonMailTemplate findOneByEnvoiModaliteFigee(boolean $envoi_modalite_figee) Return the first CommonMailTemplate filtered by the envoi_modalite_figee column
 * @method CommonMailTemplate findOneByReponseAttendue(boolean $reponse_attendue) Return the first CommonMailTemplate filtered by the reponse_attendue column
 * @method CommonMailTemplate findOneByReponseAttendueFigee(boolean $reponse_attendue_figee) Return the first CommonMailTemplate filtered by the reponse_attendue_figee column
 *
 * @method array findById(int $id) Return CommonMailTemplate objects filtered by the id column
 * @method array findByMailTypeId(int $mail_type_id) Return CommonMailTemplate objects filtered by the mail_type_id column
 * @method array findByCode(string $code) Return CommonMailTemplate objects filtered by the code column
 * @method array findByObjet(string $objet) Return CommonMailTemplate objects filtered by the objet column
 * @method array findByCorps(string $corps) Return CommonMailTemplate objects filtered by the corps column
 * @method array findByOrdreAffichage(int $ordre_affichage) Return CommonMailTemplate objects filtered by the ordre_affichage column
 * @method array findByEnvoiModalite(string $envoi_modalite) Return CommonMailTemplate objects filtered by the envoi_modalite column
 * @method array findByEnvoiModaliteFigee(boolean $envoi_modalite_figee) Return CommonMailTemplate objects filtered by the envoi_modalite_figee column
 * @method array findByReponseAttendue(boolean $reponse_attendue) Return CommonMailTemplate objects filtered by the reponse_attendue column
 * @method array findByReponseAttendueFigee(boolean $reponse_attendue_figee) Return CommonMailTemplate objects filtered by the reponse_attendue_figee column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonMailTemplateQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonMailTemplateQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonMailTemplate', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonMailTemplateQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonMailTemplateQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonMailTemplateQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonMailTemplateQuery) {
            return $criteria;
        }
        $query = new CommonMailTemplateQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonMailTemplate|CommonMailTemplate[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonMailTemplatePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonMailTemplatePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonMailTemplate A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonMailTemplate A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `mail_type_id`, `code`, `objet`, `corps`, `ordre_affichage`, `envoi_modalite`, `envoi_modalite_figee`, `reponse_attendue`, `reponse_attendue_figee` FROM `mail_template` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonMailTemplate();
            $obj->hydrate($row);
            CommonMailTemplatePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonMailTemplate|CommonMailTemplate[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonMailTemplate[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonMailTemplatePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonMailTemplatePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonMailTemplatePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonMailTemplatePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the mail_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMailTypeId(1234); // WHERE mail_type_id = 1234
     * $query->filterByMailTypeId(array(12, 34)); // WHERE mail_type_id IN (12, 34)
     * $query->filterByMailTypeId(array('min' => 12)); // WHERE mail_type_id >= 12
     * $query->filterByMailTypeId(array('max' => 12)); // WHERE mail_type_id <= 12
     * </code>
     *
     * @see       filterByCommonMailType()
     *
     * @param     mixed $mailTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByMailTypeId($mailTypeId = null, $comparison = null)
    {
        if (is_array($mailTypeId)) {
            $useMinMax = false;
            if (isset($mailTypeId['min'])) {
                $this->addUsingAlias(CommonMailTemplatePeer::MAIL_TYPE_ID, $mailTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mailTypeId['max'])) {
                $this->addUsingAlias(CommonMailTemplatePeer::MAIL_TYPE_ID, $mailTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::MAIL_TYPE_ID, $mailTypeId, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the objet column
     *
     * Example usage:
     * <code>
     * $query->filterByObjet('fooValue');   // WHERE objet = 'fooValue'
     * $query->filterByObjet('%fooValue%'); // WHERE objet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objet The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByObjet($objet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objet)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $objet)) {
                $objet = str_replace('*', '%', $objet);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::OBJET, $objet, $comparison);
    }

    /**
     * Filter the query on the corps column
     *
     * Example usage:
     * <code>
     * $query->filterByCorps('fooValue');   // WHERE corps = 'fooValue'
     * $query->filterByCorps('%fooValue%'); // WHERE corps LIKE '%fooValue%'
     * </code>
     *
     * @param     string $corps The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByCorps($corps = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($corps)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $corps)) {
                $corps = str_replace('*', '%', $corps);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::CORPS, $corps, $comparison);
    }

    /**
     * Filter the query on the ordre_affichage column
     *
     * Example usage:
     * <code>
     * $query->filterByOrdreAffichage(1234); // WHERE ordre_affichage = 1234
     * $query->filterByOrdreAffichage(array(12, 34)); // WHERE ordre_affichage IN (12, 34)
     * $query->filterByOrdreAffichage(array('min' => 12)); // WHERE ordre_affichage >= 12
     * $query->filterByOrdreAffichage(array('max' => 12)); // WHERE ordre_affichage <= 12
     * </code>
     *
     * @param     mixed $ordreAffichage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByOrdreAffichage($ordreAffichage = null, $comparison = null)
    {
        if (is_array($ordreAffichage)) {
            $useMinMax = false;
            if (isset($ordreAffichage['min'])) {
                $this->addUsingAlias(CommonMailTemplatePeer::ORDRE_AFFICHAGE, $ordreAffichage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ordreAffichage['max'])) {
                $this->addUsingAlias(CommonMailTemplatePeer::ORDRE_AFFICHAGE, $ordreAffichage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::ORDRE_AFFICHAGE, $ordreAffichage, $comparison);
    }

    /**
     * Filter the query on the envoi_modalite column
     *
     * Example usage:
     * <code>
     * $query->filterByEnvoiModalite('fooValue');   // WHERE envoi_modalite = 'fooValue'
     * $query->filterByEnvoiModalite('%fooValue%'); // WHERE envoi_modalite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $envoiModalite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByEnvoiModalite($envoiModalite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($envoiModalite)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $envoiModalite)) {
                $envoiModalite = str_replace('*', '%', $envoiModalite);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::ENVOI_MODALITE, $envoiModalite, $comparison);
    }

    /**
     * Filter the query on the envoi_modalite_figee column
     *
     * Example usage:
     * <code>
     * $query->filterByEnvoiModaliteFigee(true); // WHERE envoi_modalite_figee = true
     * $query->filterByEnvoiModaliteFigee('yes'); // WHERE envoi_modalite_figee = true
     * </code>
     *
     * @param     boolean|string $envoiModaliteFigee The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByEnvoiModaliteFigee($envoiModaliteFigee = null, $comparison = null)
    {
        if (is_string($envoiModaliteFigee)) {
            $envoiModaliteFigee = in_array(strtolower($envoiModaliteFigee), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::ENVOI_MODALITE_FIGEE, $envoiModaliteFigee, $comparison);
    }

    /**
     * Filter the query on the reponse_attendue column
     *
     * Example usage:
     * <code>
     * $query->filterByReponseAttendue(true); // WHERE reponse_attendue = true
     * $query->filterByReponseAttendue('yes'); // WHERE reponse_attendue = true
     * </code>
     *
     * @param     boolean|string $reponseAttendue The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByReponseAttendue($reponseAttendue = null, $comparison = null)
    {
        if (is_string($reponseAttendue)) {
            $reponseAttendue = in_array(strtolower($reponseAttendue), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::REPONSE_ATTENDUE, $reponseAttendue, $comparison);
    }

    /**
     * Filter the query on the reponse_attendue_figee column
     *
     * Example usage:
     * <code>
     * $query->filterByReponseAttendueFigee(true); // WHERE reponse_attendue_figee = true
     * $query->filterByReponseAttendueFigee('yes'); // WHERE reponse_attendue_figee = true
     * </code>
     *
     * @param     boolean|string $reponseAttendueFigee The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function filterByReponseAttendueFigee($reponseAttendueFigee = null, $comparison = null)
    {
        if (is_string($reponseAttendueFigee)) {
            $reponseAttendueFigee = in_array(strtolower($reponseAttendueFigee), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonMailTemplatePeer::REPONSE_ATTENDUE_FIGEE, $reponseAttendueFigee, $comparison);
    }

    /**
     * Filter the query by a related CommonMailType object
     *
     * @param   CommonMailType|PropelObjectCollection $commonMailType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonMailTemplateQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonMailType($commonMailType, $comparison = null)
    {
        if ($commonMailType instanceof CommonMailType) {
            return $this
                ->addUsingAlias(CommonMailTemplatePeer::MAIL_TYPE_ID, $commonMailType->getId(), $comparison);
        } elseif ($commonMailType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonMailTemplatePeer::MAIL_TYPE_ID, $commonMailType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonMailType() only accepts arguments of type CommonMailType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonMailType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function joinCommonMailType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonMailType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonMailType');
        }

        return $this;
    }

    /**
     * Use the CommonMailType relation CommonMailType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonMailTypeQuery A secondary query class using the current class as primary query
     */
    public function useCommonMailTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonMailType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonMailType', '\Application\Propel\Mpe\CommonMailTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonMailTemplate $commonMailTemplate Object to remove from the list of results
     *
     * @return CommonMailTemplateQuery The current query, for fluid interface
     */
    public function prune($commonMailTemplate = null)
    {
        if ($commonMailTemplate) {
            $this->addUsingAlias(CommonMailTemplatePeer::ID, $commonMailTemplate->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
