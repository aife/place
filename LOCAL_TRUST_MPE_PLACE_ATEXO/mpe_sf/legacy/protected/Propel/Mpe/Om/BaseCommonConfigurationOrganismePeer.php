<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConfigurationOrganismePeer;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\Map\CommonConfigurationOrganismeTableMap;

/**
 * Base static class for performing query and update operations on the 'configuration_organisme' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConfigurationOrganismePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'configuration_organisme';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonConfigurationOrganisme';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonConfigurationOrganismeTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 85;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 85;

    /** the column name for the organisme field */
    const ORGANISME = 'configuration_organisme.organisme';

    /** the column name for the encheres field */
    const ENCHERES = 'configuration_organisme.encheres';

    /** the column name for the consultation_pj_autres_pieces_telechargeables field */
    const CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES = 'configuration_organisme.consultation_pj_autres_pieces_telechargeables';

    /** the column name for the no_activex field */
    const NO_ACTIVEX = 'configuration_organisme.no_activex';

    /** the column name for the gestion_mapa field */
    const GESTION_MAPA = 'configuration_organisme.gestion_mapa';

    /** the column name for the article_133_upload_fichier field */
    const ARTICLE_133_UPLOAD_FICHIER = 'configuration_organisme.article_133_upload_fichier';

    /** the column name for the centrale_publication field */
    const CENTRALE_PUBLICATION = 'configuration_organisme.centrale_publication';

    /** the column name for the organisation_centralisee field */
    const ORGANISATION_CENTRALISEE = 'configuration_organisme.organisation_centralisee';

    /** the column name for the presence_elu field */
    const PRESENCE_ELU = 'configuration_organisme.presence_elu';

    /** the column name for the traduire_consultation field */
    const TRADUIRE_CONSULTATION = 'configuration_organisme.traduire_consultation';

    /** the column name for the suivi_passation field */
    const SUIVI_PASSATION = 'configuration_organisme.suivi_passation';

    /** the column name for the numerotation_ref_cons field */
    const NUMEROTATION_REF_CONS = 'configuration_organisme.numerotation_ref_cons';

    /** the column name for the pmi_lien_portail_defense_agent field */
    const PMI_LIEN_PORTAIL_DEFENSE_AGENT = 'configuration_organisme.pmi_lien_portail_defense_agent';

    /** the column name for the interface_archive_arcade_pmi field */
    const INTERFACE_ARCHIVE_ARCADE_PMI = 'configuration_organisme.interface_archive_arcade_pmi';

    /** the column name for the desarchivage_consultation field */
    const DESARCHIVAGE_CONSULTATION = 'configuration_organisme.desarchivage_consultation';

    /** the column name for the alimentation_automatique_liste_invites field */
    const ALIMENTATION_AUTOMATIQUE_LISTE_INVITES = 'configuration_organisme.alimentation_automatique_liste_invites';

    /** the column name for the interface_chorus_pmi field */
    const INTERFACE_CHORUS_PMI = 'configuration_organisme.interface_chorus_pmi';

    /** the column name for the archivage_consultation_sur_pf field */
    const ARCHIVAGE_CONSULTATION_SUR_PF = 'configuration_organisme.archivage_consultation_sur_pf';

    /** the column name for the autoriser_modification_apres_phase_consultation field */
    const AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION = 'configuration_organisme.autoriser_modification_apres_phase_consultation';

    /** the column name for the importer_enveloppe field */
    const IMPORTER_ENVELOPPE = 'configuration_organisme.importer_enveloppe';

    /** the column name for the export_marches_notifies field */
    const EXPORT_MARCHES_NOTIFIES = 'configuration_organisme.export_marches_notifies';

    /** the column name for the acces_agents_cfe_bd_fournisseur field */
    const ACCES_AGENTS_CFE_BD_FOURNISSEUR = 'configuration_organisme.acces_agents_cfe_bd_fournisseur';

    /** the column name for the acces_agents_cfe_ouverture_analyse field */
    const ACCES_AGENTS_CFE_OUVERTURE_ANALYSE = 'configuration_organisme.acces_agents_cfe_ouverture_analyse';

    /** the column name for the utiliser_parametrage_encheres field */
    const UTILISER_PARAMETRAGE_ENCHERES = 'configuration_organisme.utiliser_parametrage_encheres';

    /** the column name for the verifier_compte_boamp field */
    const VERIFIER_COMPTE_BOAMP = 'configuration_organisme.verifier_compte_boamp';

    /** the column name for the gestion_mandataire field */
    const GESTION_MANDATAIRE = 'configuration_organisme.gestion_mandataire';

    /** the column name for the four_eyes field */
    const FOUR_EYES = 'configuration_organisme.four_eyes';

    /** the column name for the interface_module_rsem field */
    const INTERFACE_MODULE_RSEM = 'configuration_organisme.interface_module_rsem';

    /** the column name for the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE field */
    const ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE = 'configuration_organisme.ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE';

    /** the column name for the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE field */
    const ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE = 'configuration_organisme.ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE';

    /** the column name for the agent_verification_certificat_peppol field */
    const AGENT_VERIFICATION_CERTIFICAT_PEPPOL = 'configuration_organisme.agent_verification_certificat_peppol';

    /** the column name for the fuseau_horaire field */
    const FUSEAU_HORAIRE = 'configuration_organisme.fuseau_horaire';

    /** the column name for the fiche_weka field */
    const FICHE_WEKA = 'configuration_organisme.fiche_weka';

    /** the column name for the mise_disposition_pieces_marche field */
    const MISE_DISPOSITION_PIECES_MARCHE = 'configuration_organisme.mise_disposition_pieces_marche';

    /** the column name for the base_dce field */
    const BASE_DCE = 'configuration_organisme.base_dce';

    /** the column name for the avis_membres_commision field */
    const AVIS_MEMBRES_COMMISION = 'configuration_organisme.avis_membres_commision';

    /** the column name for the Donnees_Redac field */
    const DONNEES_REDAC = 'configuration_organisme.Donnees_Redac';

    /** the column name for the Personnaliser_Affichage_Theme_Et_Illustration field */
    const PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION = 'configuration_organisme.Personnaliser_Affichage_Theme_Et_Illustration';

    /** the column name for the type_contrat field */
    const TYPE_CONTRAT = 'configuration_organisme.type_contrat';

    /** the column name for the entite_adjudicatrice field */
    const ENTITE_ADJUDICATRICE = 'configuration_organisme.entite_adjudicatrice';

    /** the column name for the calendrier_de_la_consultation field */
    const CALENDRIER_DE_LA_CONSULTATION = 'configuration_organisme.calendrier_de_la_consultation';

    /** the column name for the donnees_complementaires field */
    const DONNEES_COMPLEMENTAIRES = 'configuration_organisme.donnees_complementaires';

    /** the column name for the espace_collaboratif field */
    const ESPACE_COLLABORATIF = 'configuration_organisme.espace_collaboratif';

    /** the column name for the historique_navigation_inscrits field */
    const HISTORIQUE_NAVIGATION_INSCRITS = 'configuration_organisme.historique_navigation_inscrits';

    /** the column name for the Identification_contrat field */
    const IDENTIFICATION_CONTRAT = 'configuration_organisme.Identification_contrat';

    /** the column name for the extraction_accords_cadres field */
    const EXTRACTION_ACCORDS_CADRES = 'configuration_organisme.extraction_accords_cadres';

    /** the column name for the gestion_operations field */
    const GESTION_OPERATIONS = 'configuration_organisme.gestion_operations';

    /** the column name for the extraction_siret_acheteur field */
    const EXTRACTION_SIRET_ACHETEUR = 'configuration_organisme.extraction_siret_acheteur';

    /** the column name for the marche_public_simplifie field */
    const MARCHE_PUBLIC_SIMPLIFIE = 'configuration_organisme.marche_public_simplifie';

    /** the column name for the recherches_favorites_agent field */
    const RECHERCHES_FAVORITES_AGENT = 'configuration_organisme.recherches_favorites_agent';

    /** the column name for the profil_rma field */
    const PROFIL_RMA = 'configuration_organisme.profil_rma';

    /** the column name for the filtre_contrat_ac_sad field */
    const FILTRE_CONTRAT_AC_SAD = 'configuration_organisme.filtre_contrat_ac_sad';

    /** the column name for the affichage_nom_service_pere field */
    const AFFICHAGE_NOM_SERVICE_PERE = 'configuration_organisme.affichage_nom_service_pere';

    /** the column name for the mode_applet field */
    const MODE_APPLET = 'configuration_organisme.mode_applet';

    /** the column name for the marche_defense field */
    const MARCHE_DEFENSE = 'configuration_organisme.marche_defense';

    /** the column name for the num_donnees_essentielles_manuel field */
    const NUM_DONNEES_ESSENTIELLES_MANUEL = 'configuration_organisme.num_donnees_essentielles_manuel';

    /** the column name for the espace_documentaire field */
    const ESPACE_DOCUMENTAIRE = 'configuration_organisme.espace_documentaire';

    /** the column name for the numero_projet_achat field */
    const NUMERO_PROJET_ACHAT = 'configuration_organisme.numero_projet_achat';

    /** the column name for the module_exec field */
    const MODULE_EXEC = 'configuration_organisme.module_exec';

    /** the column name for the echanges_documents field */
    const ECHANGES_DOCUMENTS = 'configuration_organisme.echanges_documents';

    /** the column name for the heure_limite_de_remise_de_plis_par_defaut field */
    const HEURE_LIMITE_DE_REMISE_DE_PLIS_PAR_DEFAUT = 'configuration_organisme.heure_limite_de_remise_de_plis_par_defaut';

    /** the column name for the saisie_manuelle_id_externe field */
    const SAISIE_MANUELLE_ID_EXTERNE = 'configuration_organisme.saisie_manuelle_id_externe';

    /** the column name for the module_sourcing field */
    const MODULE_SOURCING = 'configuration_organisme.module_sourcing';

    /** the column name for the module_recensement_programmation field */
    const MODULE_RECENSEMENT_PROGRAMMATION = 'configuration_organisme.module_recensement_programmation';

    /** the column name for the module_envol field */
    const MODULE_ENVOL = 'configuration_organisme.module_envol';

    /** the column name for the module_bi_premium field */
    const MODULE_BI_PREMIUM = 'configuration_organisme.module_bi_premium';

    /** the column name for the analyse_offres field */
    const ANALYSE_OFFRES = 'configuration_organisme.analyse_offres';

    /** the column name for the cao field */
    const CAO = 'configuration_organisme.cao';

    /** the column name for the module_BI field */
    const MODULE_BI = 'configuration_organisme.module_BI';

    /** the column name for the cms_actif field */
    const CMS_ACTIF = 'configuration_organisme.cms_actif';

    /** the column name for the dce_restreint field */
    const DCE_RESTREINT = 'configuration_organisme.dce_restreint';

    /** the column name for the activer_mon_assistant_marches_publics field */
    const ACTIVER_MON_ASSISTANT_MARCHES_PUBLICS = 'configuration_organisme.activer_mon_assistant_marches_publics';

    /** the column name for the gestion_contrat_dans_exec field */
    const GESTION_CONTRAT_DANS_EXEC = 'configuration_organisme.gestion_contrat_dans_exec';

    /** the column name for the consultation_simplifiee field */
    const CONSULTATION_SIMPLIFIEE = 'configuration_organisme.consultation_simplifiee';

    /** the column name for the typage_jo2024 field */
    const TYPAGE_JO2024 = 'configuration_organisme.typage_jo2024';

    /** the column name for the module_tncp field */
    const MODULE_TNCP = 'configuration_organisme.module_tncp';

    /** the column name for the acces_module_spaser field */
    const ACCES_MODULE_SPASER = 'configuration_organisme.acces_module_spaser';

    /** the column name for the pub_tncp field */
    const PUB_TNCP = 'configuration_organisme.pub_tncp';

    /** the column name for the pub_mol field */
    const PUB_MOL = 'configuration_organisme.pub_mol';

    /** the column name for the pub_jal_fr field */
    const PUB_JAL_FR = 'configuration_organisme.pub_jal_fr';

    /** the column name for the pub_jal_lux field */
    const PUB_JAL_LUX = 'configuration_organisme.pub_jal_lux';

    /** the column name for the pub_joue field */
    const PUB_JOUE = 'configuration_organisme.pub_joue';

    /** the column name for the module_eco_sip field */
    const MODULE_ECO_SIP = 'configuration_organisme.module_eco_sip';

    /** the column name for the module_mpe_pub field */
    const MODULE_MPE_PUB = 'configuration_organisme.module_mpe_pub';

    /** the column name for the module_administration_document field */
    const MODULE_ADMINISTRATION_DOCUMENT = 'configuration_organisme.module_administration_document';

    /** The enumerated values for the encheres field */
    const ENCHERES_0 = '0';
    const ENCHERES_1 = '1';

    /** The enumerated values for the consultation_pj_autres_pieces_telechargeables field */
    const CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES_0 = '0';
    const CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES_1 = '1';

    /** The enumerated values for the no_activex field */
    const NO_ACTIVEX_0 = '0';
    const NO_ACTIVEX_1 = '1';

    /** The enumerated values for the gestion_mapa field */
    const GESTION_MAPA_0 = '0';
    const GESTION_MAPA_1 = '1';

    /** The enumerated values for the article_133_upload_fichier field */
    const ARTICLE_133_UPLOAD_FICHIER_0 = '0';
    const ARTICLE_133_UPLOAD_FICHIER_1 = '1';

    /** The enumerated values for the centrale_publication field */
    const CENTRALE_PUBLICATION_0 = '0';
    const CENTRALE_PUBLICATION_1 = '1';

    /** The enumerated values for the organisation_centralisee field */
    const ORGANISATION_CENTRALISEE_0 = '0';
    const ORGANISATION_CENTRALISEE_1 = '1';

    /** The enumerated values for the presence_elu field */
    const PRESENCE_ELU_0 = '0';
    const PRESENCE_ELU_1 = '1';

    /** The enumerated values for the traduire_consultation field */
    const TRADUIRE_CONSULTATION_0 = '0';
    const TRADUIRE_CONSULTATION_1 = '1';

    /** The enumerated values for the suivi_passation field */
    const SUIVI_PASSATION_0 = '0';
    const SUIVI_PASSATION_1 = '1';

    /** The enumerated values for the numerotation_ref_cons field */
    const NUMEROTATION_REF_CONS_0 = '0';
    const NUMEROTATION_REF_CONS_1 = '1';

    /** The enumerated values for the pmi_lien_portail_defense_agent field */
    const PMI_LIEN_PORTAIL_DEFENSE_AGENT_0 = '0';
    const PMI_LIEN_PORTAIL_DEFENSE_AGENT_1 = '1';

    /** The enumerated values for the interface_archive_arcade_pmi field */
    const INTERFACE_ARCHIVE_ARCADE_PMI_0 = '0';
    const INTERFACE_ARCHIVE_ARCADE_PMI_1 = '1';

    /** The enumerated values for the desarchivage_consultation field */
    const DESARCHIVAGE_CONSULTATION_0 = '0';
    const DESARCHIVAGE_CONSULTATION_1 = '1';

    /** The enumerated values for the alimentation_automatique_liste_invites field */
    const ALIMENTATION_AUTOMATIQUE_LISTE_INVITES_0 = '0';
    const ALIMENTATION_AUTOMATIQUE_LISTE_INVITES_1 = '1';

    /** The enumerated values for the interface_chorus_pmi field */
    const INTERFACE_CHORUS_PMI_0 = '0';
    const INTERFACE_CHORUS_PMI_1 = '1';

    /** The enumerated values for the archivage_consultation_sur_pf field */
    const ARCHIVAGE_CONSULTATION_SUR_PF_0 = '0';
    const ARCHIVAGE_CONSULTATION_SUR_PF_1 = '1';

    /** The enumerated values for the autoriser_modification_apres_phase_consultation field */
    const AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION_0 = '0';
    const AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION_1 = '1';

    /** The enumerated values for the importer_enveloppe field */
    const IMPORTER_ENVELOPPE_0 = '0';
    const IMPORTER_ENVELOPPE_1 = '1';

    /** The enumerated values for the export_marches_notifies field */
    const EXPORT_MARCHES_NOTIFIES_0 = '0';
    const EXPORT_MARCHES_NOTIFIES_1 = '1';

    /** The enumerated values for the acces_agents_cfe_bd_fournisseur field */
    const ACCES_AGENTS_CFE_BD_FOURNISSEUR_0 = '0';
    const ACCES_AGENTS_CFE_BD_FOURNISSEUR_1 = '1';

    /** The enumerated values for the acces_agents_cfe_ouverture_analyse field */
    const ACCES_AGENTS_CFE_OUVERTURE_ANALYSE_0 = '0';
    const ACCES_AGENTS_CFE_OUVERTURE_ANALYSE_1 = '1';

    /** The enumerated values for the utiliser_parametrage_encheres field */
    const UTILISER_PARAMETRAGE_ENCHERES_0 = '0';
    const UTILISER_PARAMETRAGE_ENCHERES_1 = '1';

    /** The enumerated values for the verifier_compte_boamp field */
    const VERIFIER_COMPTE_BOAMP_0 = '0';
    const VERIFIER_COMPTE_BOAMP_1 = '1';

    /** The enumerated values for the gestion_mandataire field */
    const GESTION_MANDATAIRE_0 = '0';
    const GESTION_MANDATAIRE_1 = '1';

    /** The enumerated values for the four_eyes field */
    const FOUR_EYES_0 = '0';
    const FOUR_EYES_1 = '1';

    /** The enumerated values for the interface_module_rsem field */
    const INTERFACE_MODULE_RSEM_0 = '0';
    const INTERFACE_MODULE_RSEM_1 = '1';

    /** The enumerated values for the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE field */
    const ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE_0 = '0';
    const ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE_1 = '1';

    /** The enumerated values for the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE field */
    const ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE_0 = '0';
    const ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE_1 = '1';

    /** The enumerated values for the agent_verification_certificat_peppol field */
    const AGENT_VERIFICATION_CERTIFICAT_PEPPOL_0 = '0';
    const AGENT_VERIFICATION_CERTIFICAT_PEPPOL_1 = '1';

    /** The enumerated values for the fuseau_horaire field */
    const FUSEAU_HORAIRE_0 = '0';
    const FUSEAU_HORAIRE_1 = '1';

    /** The enumerated values for the fiche_weka field */
    const FICHE_WEKA_0 = '0';
    const FICHE_WEKA_1 = '1';

    /** The enumerated values for the mise_disposition_pieces_marche field */
    const MISE_DISPOSITION_PIECES_MARCHE_0 = '0';
    const MISE_DISPOSITION_PIECES_MARCHE_1 = '1';

    /** The enumerated values for the base_dce field */
    const BASE_DCE_0 = '0';
    const BASE_DCE_1 = '1';

    /** The enumerated values for the avis_membres_commision field */
    const AVIS_MEMBRES_COMMISION_0 = '0';
    const AVIS_MEMBRES_COMMISION_1 = '1';

    /** The enumerated values for the Donnees_Redac field */
    const DONNEES_REDAC_0 = '0';
    const DONNEES_REDAC_1 = '1';

    /** The enumerated values for the Personnaliser_Affichage_Theme_Et_Illustration field */
    const PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION_0 = '0';
    const PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION_1 = '1';

    /** The enumerated values for the type_contrat field */
    const TYPE_CONTRAT_0 = '0';
    const TYPE_CONTRAT_1 = '1';

    /** The enumerated values for the entite_adjudicatrice field */
    const ENTITE_ADJUDICATRICE_0 = '0';
    const ENTITE_ADJUDICATRICE_1 = '1';

    /** The enumerated values for the calendrier_de_la_consultation field */
    const CALENDRIER_DE_LA_CONSULTATION_0 = '0';
    const CALENDRIER_DE_LA_CONSULTATION_1 = '1';

    /** The enumerated values for the donnees_complementaires field */
    const DONNEES_COMPLEMENTAIRES_0 = '0';
    const DONNEES_COMPLEMENTAIRES_1 = '1';

    /** The enumerated values for the espace_collaboratif field */
    const ESPACE_COLLABORATIF_0 = '0';
    const ESPACE_COLLABORATIF_1 = '1';

    /** The enumerated values for the historique_navigation_inscrits field */
    const HISTORIQUE_NAVIGATION_INSCRITS_0 = '0';
    const HISTORIQUE_NAVIGATION_INSCRITS_1 = '1';

    /** The enumerated values for the Identification_contrat field */
    const IDENTIFICATION_CONTRAT_0 = '0';
    const IDENTIFICATION_CONTRAT_1 = '1';

    /** The enumerated values for the extraction_accords_cadres field */
    const EXTRACTION_ACCORDS_CADRES_0 = '0';
    const EXTRACTION_ACCORDS_CADRES_1 = '1';

    /** The enumerated values for the gestion_operations field */
    const GESTION_OPERATIONS_0 = '0';
    const GESTION_OPERATIONS_1 = '1';

    /** The enumerated values for the extraction_siret_acheteur field */
    const EXTRACTION_SIRET_ACHETEUR_0 = '0';
    const EXTRACTION_SIRET_ACHETEUR_1 = '1';

    /** The enumerated values for the marche_public_simplifie field */
    const MARCHE_PUBLIC_SIMPLIFIE_0 = '0';
    const MARCHE_PUBLIC_SIMPLIFIE_1 = '1';

    /** The enumerated values for the recherches_favorites_agent field */
    const RECHERCHES_FAVORITES_AGENT_0 = '0';
    const RECHERCHES_FAVORITES_AGENT_1 = '1';

    /** The enumerated values for the profil_rma field */
    const PROFIL_RMA_0 = '0';
    const PROFIL_RMA_1 = '1';

    /** The enumerated values for the filtre_contrat_ac_sad field */
    const FILTRE_CONTRAT_AC_SAD_0 = '0';
    const FILTRE_CONTRAT_AC_SAD_1 = '1';

    /** The enumerated values for the affichage_nom_service_pere field */
    const AFFICHAGE_NOM_SERVICE_PERE_0 = '0';
    const AFFICHAGE_NOM_SERVICE_PERE_1 = '1';

    /** The enumerated values for the mode_applet field */
    const MODE_APPLET_0 = '0';
    const MODE_APPLET_1 = '1';

    /** The enumerated values for the marche_defense field */
    const MARCHE_DEFENSE_0 = '0';
    const MARCHE_DEFENSE_1 = '1';

    /** The enumerated values for the num_donnees_essentielles_manuel field */
    const NUM_DONNEES_ESSENTIELLES_MANUEL_0 = '0';
    const NUM_DONNEES_ESSENTIELLES_MANUEL_1 = '1';

    /** The enumerated values for the numero_projet_achat field */
    const NUMERO_PROJET_ACHAT_0 = '0';
    const NUMERO_PROJET_ACHAT_1 = '1';

    /** The enumerated values for the module_exec field */
    const MODULE_EXEC_0 = '0';
    const MODULE_EXEC_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonConfigurationOrganisme objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonConfigurationOrganisme[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonConfigurationOrganismePeer::$fieldNames[CommonConfigurationOrganismePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Organisme', 'Encheres', 'ConsultationPjAutresPiecesTelechargeables', 'NoActivex', 'GestionMapa', 'Article133UploadFichier', 'CentralePublication', 'OrganisationCentralisee', 'PresenceElu', 'TraduireConsultation', 'SuiviPassation', 'NumerotationRefCons', 'PmiLienPortailDefenseAgent', 'InterfaceArchiveArcadePmi', 'DesarchivageConsultation', 'AlimentationAutomatiqueListeInvites', 'InterfaceChorusPmi', 'ArchivageConsultationSurPf', 'AutoriserModificationApresPhaseConsultation', 'ImporterEnveloppe', 'ExportMarchesNotifies', 'AccesAgentsCfeBdFournisseur', 'AccesAgentsCfeOuvertureAnalyse', 'UtiliserParametrageEncheres', 'VerifierCompteBoamp', 'GestionMandataire', 'FourEyes', 'InterfaceModuleRsem', 'ArchivageConsultationSaeExterneEnvoiArchive', 'ArchivageConsultationSaeExterneTelechargementArchive', 'AgentVerificationCertificatPeppol', 'FuseauHoraire', 'FicheWeka', 'MiseDispositionPiecesMarche', 'BaseDce', 'AvisMembresCommision', 'DonneesRedac', 'PersonnaliserAffichageThemeEtIllustration', 'TypeContrat', 'EntiteAdjudicatrice', 'CalendrierDeLaConsultation', 'DonneesComplementaires', 'EspaceCollaboratif', 'HistoriqueNavigationInscrits', 'IdentificationContrat', 'ExtractionAccordsCadres', 'GestionOperations', 'ExtractionSiretAcheteur', 'MarchePublicSimplifie', 'RecherchesFavoritesAgent', 'ProfilRma', 'FiltreContratAcSad', 'AffichageNomServicePere', 'ModeApplet', 'MarcheDefense', 'NumDonneesEssentiellesManuel', 'EspaceDocumentaire', 'NumeroProjetAchat', 'ModuleExec', 'EchangesDocuments', 'HeureLimiteDeRemiseDePlisParDefaut', 'SaisieManuelleIdExterne', 'ModuleSourcing', 'ModuleRecensementProgrammation', 'ModuleEnvol', 'ModuleBiPremium', 'AnalyseOffres', 'Cao', 'ModuleBi', 'CmsActif', 'DceRestreint', 'ActiverMonAssistantMarchesPublics', 'GestionContratDansExec', 'ConsultationSimplifiee', 'TypageJo2024', 'ModuleTncp', 'AccesModuleSpaser', 'PubTncp', 'PubMol', 'PubJalFr', 'PubJalLux', 'PubJoue', 'ModuleEcoSip', 'ModuleMpePub', 'ModuleAdministrationDocument', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('organisme', 'encheres', 'consultationPjAutresPiecesTelechargeables', 'noActivex', 'gestionMapa', 'article133UploadFichier', 'centralePublication', 'organisationCentralisee', 'presenceElu', 'traduireConsultation', 'suiviPassation', 'numerotationRefCons', 'pmiLienPortailDefenseAgent', 'interfaceArchiveArcadePmi', 'desarchivageConsultation', 'alimentationAutomatiqueListeInvites', 'interfaceChorusPmi', 'archivageConsultationSurPf', 'autoriserModificationApresPhaseConsultation', 'importerEnveloppe', 'exportMarchesNotifies', 'accesAgentsCfeBdFournisseur', 'accesAgentsCfeOuvertureAnalyse', 'utiliserParametrageEncheres', 'verifierCompteBoamp', 'gestionMandataire', 'fourEyes', 'interfaceModuleRsem', 'archivageConsultationSaeExterneEnvoiArchive', 'archivageConsultationSaeExterneTelechargementArchive', 'agentVerificationCertificatPeppol', 'fuseauHoraire', 'ficheWeka', 'miseDispositionPiecesMarche', 'baseDce', 'avisMembresCommision', 'donneesRedac', 'personnaliserAffichageThemeEtIllustration', 'typeContrat', 'entiteAdjudicatrice', 'calendrierDeLaConsultation', 'donneesComplementaires', 'espaceCollaboratif', 'historiqueNavigationInscrits', 'identificationContrat', 'extractionAccordsCadres', 'gestionOperations', 'extractionSiretAcheteur', 'marchePublicSimplifie', 'recherchesFavoritesAgent', 'profilRma', 'filtreContratAcSad', 'affichageNomServicePere', 'modeApplet', 'marcheDefense', 'numDonneesEssentiellesManuel', 'espaceDocumentaire', 'numeroProjetAchat', 'moduleExec', 'echangesDocuments', 'heureLimiteDeRemiseDePlisParDefaut', 'saisieManuelleIdExterne', 'moduleSourcing', 'moduleRecensementProgrammation', 'moduleEnvol', 'moduleBiPremium', 'analyseOffres', 'cao', 'moduleBi', 'cmsActif', 'dceRestreint', 'activerMonAssistantMarchesPublics', 'gestionContratDansExec', 'consultationSimplifiee', 'typageJo2024', 'moduleTncp', 'accesModuleSpaser', 'pubTncp', 'pubMol', 'pubJalFr', 'pubJalLux', 'pubJoue', 'moduleEcoSip', 'moduleMpePub', 'moduleAdministrationDocument', ),
        BasePeer::TYPE_COLNAME => array (CommonConfigurationOrganismePeer::ORGANISME, CommonConfigurationOrganismePeer::ENCHERES, CommonConfigurationOrganismePeer::CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES, CommonConfigurationOrganismePeer::NO_ACTIVEX, CommonConfigurationOrganismePeer::GESTION_MAPA, CommonConfigurationOrganismePeer::ARTICLE_133_UPLOAD_FICHIER, CommonConfigurationOrganismePeer::CENTRALE_PUBLICATION, CommonConfigurationOrganismePeer::ORGANISATION_CENTRALISEE, CommonConfigurationOrganismePeer::PRESENCE_ELU, CommonConfigurationOrganismePeer::TRADUIRE_CONSULTATION, CommonConfigurationOrganismePeer::SUIVI_PASSATION, CommonConfigurationOrganismePeer::NUMEROTATION_REF_CONS, CommonConfigurationOrganismePeer::PMI_LIEN_PORTAIL_DEFENSE_AGENT, CommonConfigurationOrganismePeer::INTERFACE_ARCHIVE_ARCADE_PMI, CommonConfigurationOrganismePeer::DESARCHIVAGE_CONSULTATION, CommonConfigurationOrganismePeer::ALIMENTATION_AUTOMATIQUE_LISTE_INVITES, CommonConfigurationOrganismePeer::INTERFACE_CHORUS_PMI, CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SUR_PF, CommonConfigurationOrganismePeer::AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION, CommonConfigurationOrganismePeer::IMPORTER_ENVELOPPE, CommonConfigurationOrganismePeer::EXPORT_MARCHES_NOTIFIES, CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_BD_FOURNISSEUR, CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_OUVERTURE_ANALYSE, CommonConfigurationOrganismePeer::UTILISER_PARAMETRAGE_ENCHERES, CommonConfigurationOrganismePeer::VERIFIER_COMPTE_BOAMP, CommonConfigurationOrganismePeer::GESTION_MANDATAIRE, CommonConfigurationOrganismePeer::FOUR_EYES, CommonConfigurationOrganismePeer::INTERFACE_MODULE_RSEM, CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE, CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE, CommonConfigurationOrganismePeer::AGENT_VERIFICATION_CERTIFICAT_PEPPOL, CommonConfigurationOrganismePeer::FUSEAU_HORAIRE, CommonConfigurationOrganismePeer::FICHE_WEKA, CommonConfigurationOrganismePeer::MISE_DISPOSITION_PIECES_MARCHE, CommonConfigurationOrganismePeer::BASE_DCE, CommonConfigurationOrganismePeer::AVIS_MEMBRES_COMMISION, CommonConfigurationOrganismePeer::DONNEES_REDAC, CommonConfigurationOrganismePeer::PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION, CommonConfigurationOrganismePeer::TYPE_CONTRAT, CommonConfigurationOrganismePeer::ENTITE_ADJUDICATRICE, CommonConfigurationOrganismePeer::CALENDRIER_DE_LA_CONSULTATION, CommonConfigurationOrganismePeer::DONNEES_COMPLEMENTAIRES, CommonConfigurationOrganismePeer::ESPACE_COLLABORATIF, CommonConfigurationOrganismePeer::HISTORIQUE_NAVIGATION_INSCRITS, CommonConfigurationOrganismePeer::IDENTIFICATION_CONTRAT, CommonConfigurationOrganismePeer::EXTRACTION_ACCORDS_CADRES, CommonConfigurationOrganismePeer::GESTION_OPERATIONS, CommonConfigurationOrganismePeer::EXTRACTION_SIRET_ACHETEUR, CommonConfigurationOrganismePeer::MARCHE_PUBLIC_SIMPLIFIE, CommonConfigurationOrganismePeer::RECHERCHES_FAVORITES_AGENT, CommonConfigurationOrganismePeer::PROFIL_RMA, CommonConfigurationOrganismePeer::FILTRE_CONTRAT_AC_SAD, CommonConfigurationOrganismePeer::AFFICHAGE_NOM_SERVICE_PERE, CommonConfigurationOrganismePeer::MODE_APPLET, CommonConfigurationOrganismePeer::MARCHE_DEFENSE, CommonConfigurationOrganismePeer::NUM_DONNEES_ESSENTIELLES_MANUEL, CommonConfigurationOrganismePeer::ESPACE_DOCUMENTAIRE, CommonConfigurationOrganismePeer::NUMERO_PROJET_ACHAT, CommonConfigurationOrganismePeer::MODULE_EXEC, CommonConfigurationOrganismePeer::ECHANGES_DOCUMENTS, CommonConfigurationOrganismePeer::HEURE_LIMITE_DE_REMISE_DE_PLIS_PAR_DEFAUT, CommonConfigurationOrganismePeer::SAISIE_MANUELLE_ID_EXTERNE, CommonConfigurationOrganismePeer::MODULE_SOURCING, CommonConfigurationOrganismePeer::MODULE_RECENSEMENT_PROGRAMMATION, CommonConfigurationOrganismePeer::MODULE_ENVOL, CommonConfigurationOrganismePeer::MODULE_BI_PREMIUM, CommonConfigurationOrganismePeer::ANALYSE_OFFRES, CommonConfigurationOrganismePeer::CAO, CommonConfigurationOrganismePeer::MODULE_BI, CommonConfigurationOrganismePeer::CMS_ACTIF, CommonConfigurationOrganismePeer::DCE_RESTREINT, CommonConfigurationOrganismePeer::ACTIVER_MON_ASSISTANT_MARCHES_PUBLICS, CommonConfigurationOrganismePeer::GESTION_CONTRAT_DANS_EXEC, CommonConfigurationOrganismePeer::CONSULTATION_SIMPLIFIEE, CommonConfigurationOrganismePeer::TYPAGE_JO2024, CommonConfigurationOrganismePeer::MODULE_TNCP, CommonConfigurationOrganismePeer::ACCES_MODULE_SPASER, CommonConfigurationOrganismePeer::PUB_TNCP, CommonConfigurationOrganismePeer::PUB_MOL, CommonConfigurationOrganismePeer::PUB_JAL_FR, CommonConfigurationOrganismePeer::PUB_JAL_LUX, CommonConfigurationOrganismePeer::PUB_JOUE, CommonConfigurationOrganismePeer::MODULE_ECO_SIP, CommonConfigurationOrganismePeer::MODULE_MPE_PUB, CommonConfigurationOrganismePeer::MODULE_ADMINISTRATION_DOCUMENT, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ORGANISME', 'ENCHERES', 'CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES', 'NO_ACTIVEX', 'GESTION_MAPA', 'ARTICLE_133_UPLOAD_FICHIER', 'CENTRALE_PUBLICATION', 'ORGANISATION_CENTRALISEE', 'PRESENCE_ELU', 'TRADUIRE_CONSULTATION', 'SUIVI_PASSATION', 'NUMEROTATION_REF_CONS', 'PMI_LIEN_PORTAIL_DEFENSE_AGENT', 'INTERFACE_ARCHIVE_ARCADE_PMI', 'DESARCHIVAGE_CONSULTATION', 'ALIMENTATION_AUTOMATIQUE_LISTE_INVITES', 'INTERFACE_CHORUS_PMI', 'ARCHIVAGE_CONSULTATION_SUR_PF', 'AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION', 'IMPORTER_ENVELOPPE', 'EXPORT_MARCHES_NOTIFIES', 'ACCES_AGENTS_CFE_BD_FOURNISSEUR', 'ACCES_AGENTS_CFE_OUVERTURE_ANALYSE', 'UTILISER_PARAMETRAGE_ENCHERES', 'VERIFIER_COMPTE_BOAMP', 'GESTION_MANDATAIRE', 'FOUR_EYES', 'INTERFACE_MODULE_RSEM', 'ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE', 'ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE', 'AGENT_VERIFICATION_CERTIFICAT_PEPPOL', 'FUSEAU_HORAIRE', 'FICHE_WEKA', 'MISE_DISPOSITION_PIECES_MARCHE', 'BASE_DCE', 'AVIS_MEMBRES_COMMISION', 'DONNEES_REDAC', 'PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION', 'TYPE_CONTRAT', 'ENTITE_ADJUDICATRICE', 'CALENDRIER_DE_LA_CONSULTATION', 'DONNEES_COMPLEMENTAIRES', 'ESPACE_COLLABORATIF', 'HISTORIQUE_NAVIGATION_INSCRITS', 'IDENTIFICATION_CONTRAT', 'EXTRACTION_ACCORDS_CADRES', 'GESTION_OPERATIONS', 'EXTRACTION_SIRET_ACHETEUR', 'MARCHE_PUBLIC_SIMPLIFIE', 'RECHERCHES_FAVORITES_AGENT', 'PROFIL_RMA', 'FILTRE_CONTRAT_AC_SAD', 'AFFICHAGE_NOM_SERVICE_PERE', 'MODE_APPLET', 'MARCHE_DEFENSE', 'NUM_DONNEES_ESSENTIELLES_MANUEL', 'ESPACE_DOCUMENTAIRE', 'NUMERO_PROJET_ACHAT', 'MODULE_EXEC', 'ECHANGES_DOCUMENTS', 'HEURE_LIMITE_DE_REMISE_DE_PLIS_PAR_DEFAUT', 'SAISIE_MANUELLE_ID_EXTERNE', 'MODULE_SOURCING', 'MODULE_RECENSEMENT_PROGRAMMATION', 'MODULE_ENVOL', 'MODULE_BI_PREMIUM', 'ANALYSE_OFFRES', 'CAO', 'MODULE_BI', 'CMS_ACTIF', 'DCE_RESTREINT', 'ACTIVER_MON_ASSISTANT_MARCHES_PUBLICS', 'GESTION_CONTRAT_DANS_EXEC', 'CONSULTATION_SIMPLIFIEE', 'TYPAGE_JO2024', 'MODULE_TNCP', 'ACCES_MODULE_SPASER', 'PUB_TNCP', 'PUB_MOL', 'PUB_JAL_FR', 'PUB_JAL_LUX', 'PUB_JOUE', 'MODULE_ECO_SIP', 'MODULE_MPE_PUB', 'MODULE_ADMINISTRATION_DOCUMENT', ),
        BasePeer::TYPE_FIELDNAME => array ('organisme', 'encheres', 'consultation_pj_autres_pieces_telechargeables', 'no_activex', 'gestion_mapa', 'article_133_upload_fichier', 'centrale_publication', 'organisation_centralisee', 'presence_elu', 'traduire_consultation', 'suivi_passation', 'numerotation_ref_cons', 'pmi_lien_portail_defense_agent', 'interface_archive_arcade_pmi', 'desarchivage_consultation', 'alimentation_automatique_liste_invites', 'interface_chorus_pmi', 'archivage_consultation_sur_pf', 'autoriser_modification_apres_phase_consultation', 'importer_enveloppe', 'export_marches_notifies', 'acces_agents_cfe_bd_fournisseur', 'acces_agents_cfe_ouverture_analyse', 'utiliser_parametrage_encheres', 'verifier_compte_boamp', 'gestion_mandataire', 'four_eyes', 'interface_module_rsem', 'ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE', 'ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE', 'agent_verification_certificat_peppol', 'fuseau_horaire', 'fiche_weka', 'mise_disposition_pieces_marche', 'base_dce', 'avis_membres_commision', 'Donnees_Redac', 'Personnaliser_Affichage_Theme_Et_Illustration', 'type_contrat', 'entite_adjudicatrice', 'calendrier_de_la_consultation', 'donnees_complementaires', 'espace_collaboratif', 'historique_navigation_inscrits', 'Identification_contrat', 'extraction_accords_cadres', 'gestion_operations', 'extraction_siret_acheteur', 'marche_public_simplifie', 'recherches_favorites_agent', 'profil_rma', 'filtre_contrat_ac_sad', 'affichage_nom_service_pere', 'mode_applet', 'marche_defense', 'num_donnees_essentielles_manuel', 'espace_documentaire', 'numero_projet_achat', 'module_exec', 'echanges_documents', 'heure_limite_de_remise_de_plis_par_defaut', 'saisie_manuelle_id_externe', 'module_sourcing', 'module_recensement_programmation', 'module_envol', 'module_bi_premium', 'analyse_offres', 'cao', 'module_BI', 'cms_actif', 'dce_restreint', 'activer_mon_assistant_marches_publics', 'gestion_contrat_dans_exec', 'consultation_simplifiee', 'typage_jo2024', 'module_tncp', 'acces_module_spaser', 'pub_tncp', 'pub_mol', 'pub_jal_fr', 'pub_jal_lux', 'pub_joue', 'module_eco_sip', 'module_mpe_pub', 'module_administration_document', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonConfigurationOrganismePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Organisme' => 0, 'Encheres' => 1, 'ConsultationPjAutresPiecesTelechargeables' => 2, 'NoActivex' => 3, 'GestionMapa' => 4, 'Article133UploadFichier' => 5, 'CentralePublication' => 6, 'OrganisationCentralisee' => 7, 'PresenceElu' => 8, 'TraduireConsultation' => 9, 'SuiviPassation' => 10, 'NumerotationRefCons' => 11, 'PmiLienPortailDefenseAgent' => 12, 'InterfaceArchiveArcadePmi' => 13, 'DesarchivageConsultation' => 14, 'AlimentationAutomatiqueListeInvites' => 15, 'InterfaceChorusPmi' => 16, 'ArchivageConsultationSurPf' => 17, 'AutoriserModificationApresPhaseConsultation' => 18, 'ImporterEnveloppe' => 19, 'ExportMarchesNotifies' => 20, 'AccesAgentsCfeBdFournisseur' => 21, 'AccesAgentsCfeOuvertureAnalyse' => 22, 'UtiliserParametrageEncheres' => 23, 'VerifierCompteBoamp' => 24, 'GestionMandataire' => 25, 'FourEyes' => 26, 'InterfaceModuleRsem' => 27, 'ArchivageConsultationSaeExterneEnvoiArchive' => 28, 'ArchivageConsultationSaeExterneTelechargementArchive' => 29, 'AgentVerificationCertificatPeppol' => 30, 'FuseauHoraire' => 31, 'FicheWeka' => 32, 'MiseDispositionPiecesMarche' => 33, 'BaseDce' => 34, 'AvisMembresCommision' => 35, 'DonneesRedac' => 36, 'PersonnaliserAffichageThemeEtIllustration' => 37, 'TypeContrat' => 38, 'EntiteAdjudicatrice' => 39, 'CalendrierDeLaConsultation' => 40, 'DonneesComplementaires' => 41, 'EspaceCollaboratif' => 42, 'HistoriqueNavigationInscrits' => 43, 'IdentificationContrat' => 44, 'ExtractionAccordsCadres' => 45, 'GestionOperations' => 46, 'ExtractionSiretAcheteur' => 47, 'MarchePublicSimplifie' => 48, 'RecherchesFavoritesAgent' => 49, 'ProfilRma' => 50, 'FiltreContratAcSad' => 51, 'AffichageNomServicePere' => 52, 'ModeApplet' => 53, 'MarcheDefense' => 54, 'NumDonneesEssentiellesManuel' => 55, 'EspaceDocumentaire' => 56, 'NumeroProjetAchat' => 57, 'ModuleExec' => 58, 'EchangesDocuments' => 59, 'HeureLimiteDeRemiseDePlisParDefaut' => 60, 'SaisieManuelleIdExterne' => 61, 'ModuleSourcing' => 62, 'ModuleRecensementProgrammation' => 63, 'ModuleEnvol' => 64, 'ModuleBiPremium' => 65, 'AnalyseOffres' => 66, 'Cao' => 67, 'ModuleBi' => 68, 'CmsActif' => 69, 'DceRestreint' => 70, 'ActiverMonAssistantMarchesPublics' => 71, 'GestionContratDansExec' => 72, 'ConsultationSimplifiee' => 73, 'TypageJo2024' => 74, 'ModuleTncp' => 75, 'AccesModuleSpaser' => 76, 'PubTncp' => 77, 'PubMol' => 78, 'PubJalFr' => 79, 'PubJalLux' => 80, 'PubJoue' => 81, 'ModuleEcoSip' => 82, 'ModuleMpePub' => 83, 'ModuleAdministrationDocument' => 84, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('organisme' => 0, 'encheres' => 1, 'consultationPjAutresPiecesTelechargeables' => 2, 'noActivex' => 3, 'gestionMapa' => 4, 'article133UploadFichier' => 5, 'centralePublication' => 6, 'organisationCentralisee' => 7, 'presenceElu' => 8, 'traduireConsultation' => 9, 'suiviPassation' => 10, 'numerotationRefCons' => 11, 'pmiLienPortailDefenseAgent' => 12, 'interfaceArchiveArcadePmi' => 13, 'desarchivageConsultation' => 14, 'alimentationAutomatiqueListeInvites' => 15, 'interfaceChorusPmi' => 16, 'archivageConsultationSurPf' => 17, 'autoriserModificationApresPhaseConsultation' => 18, 'importerEnveloppe' => 19, 'exportMarchesNotifies' => 20, 'accesAgentsCfeBdFournisseur' => 21, 'accesAgentsCfeOuvertureAnalyse' => 22, 'utiliserParametrageEncheres' => 23, 'verifierCompteBoamp' => 24, 'gestionMandataire' => 25, 'fourEyes' => 26, 'interfaceModuleRsem' => 27, 'archivageConsultationSaeExterneEnvoiArchive' => 28, 'archivageConsultationSaeExterneTelechargementArchive' => 29, 'agentVerificationCertificatPeppol' => 30, 'fuseauHoraire' => 31, 'ficheWeka' => 32, 'miseDispositionPiecesMarche' => 33, 'baseDce' => 34, 'avisMembresCommision' => 35, 'donneesRedac' => 36, 'personnaliserAffichageThemeEtIllustration' => 37, 'typeContrat' => 38, 'entiteAdjudicatrice' => 39, 'calendrierDeLaConsultation' => 40, 'donneesComplementaires' => 41, 'espaceCollaboratif' => 42, 'historiqueNavigationInscrits' => 43, 'identificationContrat' => 44, 'extractionAccordsCadres' => 45, 'gestionOperations' => 46, 'extractionSiretAcheteur' => 47, 'marchePublicSimplifie' => 48, 'recherchesFavoritesAgent' => 49, 'profilRma' => 50, 'filtreContratAcSad' => 51, 'affichageNomServicePere' => 52, 'modeApplet' => 53, 'marcheDefense' => 54, 'numDonneesEssentiellesManuel' => 55, 'espaceDocumentaire' => 56, 'numeroProjetAchat' => 57, 'moduleExec' => 58, 'echangesDocuments' => 59, 'heureLimiteDeRemiseDePlisParDefaut' => 60, 'saisieManuelleIdExterne' => 61, 'moduleSourcing' => 62, 'moduleRecensementProgrammation' => 63, 'moduleEnvol' => 64, 'moduleBiPremium' => 65, 'analyseOffres' => 66, 'cao' => 67, 'moduleBi' => 68, 'cmsActif' => 69, 'dceRestreint' => 70, 'activerMonAssistantMarchesPublics' => 71, 'gestionContratDansExec' => 72, 'consultationSimplifiee' => 73, 'typageJo2024' => 74, 'moduleTncp' => 75, 'accesModuleSpaser' => 76, 'pubTncp' => 77, 'pubMol' => 78, 'pubJalFr' => 79, 'pubJalLux' => 80, 'pubJoue' => 81, 'moduleEcoSip' => 82, 'moduleMpePub' => 83, 'moduleAdministrationDocument' => 84, ),
        BasePeer::TYPE_COLNAME => array (CommonConfigurationOrganismePeer::ORGANISME => 0, CommonConfigurationOrganismePeer::ENCHERES => 1, CommonConfigurationOrganismePeer::CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES => 2, CommonConfigurationOrganismePeer::NO_ACTIVEX => 3, CommonConfigurationOrganismePeer::GESTION_MAPA => 4, CommonConfigurationOrganismePeer::ARTICLE_133_UPLOAD_FICHIER => 5, CommonConfigurationOrganismePeer::CENTRALE_PUBLICATION => 6, CommonConfigurationOrganismePeer::ORGANISATION_CENTRALISEE => 7, CommonConfigurationOrganismePeer::PRESENCE_ELU => 8, CommonConfigurationOrganismePeer::TRADUIRE_CONSULTATION => 9, CommonConfigurationOrganismePeer::SUIVI_PASSATION => 10, CommonConfigurationOrganismePeer::NUMEROTATION_REF_CONS => 11, CommonConfigurationOrganismePeer::PMI_LIEN_PORTAIL_DEFENSE_AGENT => 12, CommonConfigurationOrganismePeer::INTERFACE_ARCHIVE_ARCADE_PMI => 13, CommonConfigurationOrganismePeer::DESARCHIVAGE_CONSULTATION => 14, CommonConfigurationOrganismePeer::ALIMENTATION_AUTOMATIQUE_LISTE_INVITES => 15, CommonConfigurationOrganismePeer::INTERFACE_CHORUS_PMI => 16, CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SUR_PF => 17, CommonConfigurationOrganismePeer::AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION => 18, CommonConfigurationOrganismePeer::IMPORTER_ENVELOPPE => 19, CommonConfigurationOrganismePeer::EXPORT_MARCHES_NOTIFIES => 20, CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_BD_FOURNISSEUR => 21, CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_OUVERTURE_ANALYSE => 22, CommonConfigurationOrganismePeer::UTILISER_PARAMETRAGE_ENCHERES => 23, CommonConfigurationOrganismePeer::VERIFIER_COMPTE_BOAMP => 24, CommonConfigurationOrganismePeer::GESTION_MANDATAIRE => 25, CommonConfigurationOrganismePeer::FOUR_EYES => 26, CommonConfigurationOrganismePeer::INTERFACE_MODULE_RSEM => 27, CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE => 28, CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE => 29, CommonConfigurationOrganismePeer::AGENT_VERIFICATION_CERTIFICAT_PEPPOL => 30, CommonConfigurationOrganismePeer::FUSEAU_HORAIRE => 31, CommonConfigurationOrganismePeer::FICHE_WEKA => 32, CommonConfigurationOrganismePeer::MISE_DISPOSITION_PIECES_MARCHE => 33, CommonConfigurationOrganismePeer::BASE_DCE => 34, CommonConfigurationOrganismePeer::AVIS_MEMBRES_COMMISION => 35, CommonConfigurationOrganismePeer::DONNEES_REDAC => 36, CommonConfigurationOrganismePeer::PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION => 37, CommonConfigurationOrganismePeer::TYPE_CONTRAT => 38, CommonConfigurationOrganismePeer::ENTITE_ADJUDICATRICE => 39, CommonConfigurationOrganismePeer::CALENDRIER_DE_LA_CONSULTATION => 40, CommonConfigurationOrganismePeer::DONNEES_COMPLEMENTAIRES => 41, CommonConfigurationOrganismePeer::ESPACE_COLLABORATIF => 42, CommonConfigurationOrganismePeer::HISTORIQUE_NAVIGATION_INSCRITS => 43, CommonConfigurationOrganismePeer::IDENTIFICATION_CONTRAT => 44, CommonConfigurationOrganismePeer::EXTRACTION_ACCORDS_CADRES => 45, CommonConfigurationOrganismePeer::GESTION_OPERATIONS => 46, CommonConfigurationOrganismePeer::EXTRACTION_SIRET_ACHETEUR => 47, CommonConfigurationOrganismePeer::MARCHE_PUBLIC_SIMPLIFIE => 48, CommonConfigurationOrganismePeer::RECHERCHES_FAVORITES_AGENT => 49, CommonConfigurationOrganismePeer::PROFIL_RMA => 50, CommonConfigurationOrganismePeer::FILTRE_CONTRAT_AC_SAD => 51, CommonConfigurationOrganismePeer::AFFICHAGE_NOM_SERVICE_PERE => 52, CommonConfigurationOrganismePeer::MODE_APPLET => 53, CommonConfigurationOrganismePeer::MARCHE_DEFENSE => 54, CommonConfigurationOrganismePeer::NUM_DONNEES_ESSENTIELLES_MANUEL => 55, CommonConfigurationOrganismePeer::ESPACE_DOCUMENTAIRE => 56, CommonConfigurationOrganismePeer::NUMERO_PROJET_ACHAT => 57, CommonConfigurationOrganismePeer::MODULE_EXEC => 58, CommonConfigurationOrganismePeer::ECHANGES_DOCUMENTS => 59, CommonConfigurationOrganismePeer::HEURE_LIMITE_DE_REMISE_DE_PLIS_PAR_DEFAUT => 60, CommonConfigurationOrganismePeer::SAISIE_MANUELLE_ID_EXTERNE => 61, CommonConfigurationOrganismePeer::MODULE_SOURCING => 62, CommonConfigurationOrganismePeer::MODULE_RECENSEMENT_PROGRAMMATION => 63, CommonConfigurationOrganismePeer::MODULE_ENVOL => 64, CommonConfigurationOrganismePeer::MODULE_BI_PREMIUM => 65, CommonConfigurationOrganismePeer::ANALYSE_OFFRES => 66, CommonConfigurationOrganismePeer::CAO => 67, CommonConfigurationOrganismePeer::MODULE_BI => 68, CommonConfigurationOrganismePeer::CMS_ACTIF => 69, CommonConfigurationOrganismePeer::DCE_RESTREINT => 70, CommonConfigurationOrganismePeer::ACTIVER_MON_ASSISTANT_MARCHES_PUBLICS => 71, CommonConfigurationOrganismePeer::GESTION_CONTRAT_DANS_EXEC => 72, CommonConfigurationOrganismePeer::CONSULTATION_SIMPLIFIEE => 73, CommonConfigurationOrganismePeer::TYPAGE_JO2024 => 74, CommonConfigurationOrganismePeer::MODULE_TNCP => 75, CommonConfigurationOrganismePeer::ACCES_MODULE_SPASER => 76, CommonConfigurationOrganismePeer::PUB_TNCP => 77, CommonConfigurationOrganismePeer::PUB_MOL => 78, CommonConfigurationOrganismePeer::PUB_JAL_FR => 79, CommonConfigurationOrganismePeer::PUB_JAL_LUX => 80, CommonConfigurationOrganismePeer::PUB_JOUE => 81, CommonConfigurationOrganismePeer::MODULE_ECO_SIP => 82, CommonConfigurationOrganismePeer::MODULE_MPE_PUB => 83, CommonConfigurationOrganismePeer::MODULE_ADMINISTRATION_DOCUMENT => 84, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ORGANISME' => 0, 'ENCHERES' => 1, 'CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES' => 2, 'NO_ACTIVEX' => 3, 'GESTION_MAPA' => 4, 'ARTICLE_133_UPLOAD_FICHIER' => 5, 'CENTRALE_PUBLICATION' => 6, 'ORGANISATION_CENTRALISEE' => 7, 'PRESENCE_ELU' => 8, 'TRADUIRE_CONSULTATION' => 9, 'SUIVI_PASSATION' => 10, 'NUMEROTATION_REF_CONS' => 11, 'PMI_LIEN_PORTAIL_DEFENSE_AGENT' => 12, 'INTERFACE_ARCHIVE_ARCADE_PMI' => 13, 'DESARCHIVAGE_CONSULTATION' => 14, 'ALIMENTATION_AUTOMATIQUE_LISTE_INVITES' => 15, 'INTERFACE_CHORUS_PMI' => 16, 'ARCHIVAGE_CONSULTATION_SUR_PF' => 17, 'AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION' => 18, 'IMPORTER_ENVELOPPE' => 19, 'EXPORT_MARCHES_NOTIFIES' => 20, 'ACCES_AGENTS_CFE_BD_FOURNISSEUR' => 21, 'ACCES_AGENTS_CFE_OUVERTURE_ANALYSE' => 22, 'UTILISER_PARAMETRAGE_ENCHERES' => 23, 'VERIFIER_COMPTE_BOAMP' => 24, 'GESTION_MANDATAIRE' => 25, 'FOUR_EYES' => 26, 'INTERFACE_MODULE_RSEM' => 27, 'ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE' => 28, 'ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE' => 29, 'AGENT_VERIFICATION_CERTIFICAT_PEPPOL' => 30, 'FUSEAU_HORAIRE' => 31, 'FICHE_WEKA' => 32, 'MISE_DISPOSITION_PIECES_MARCHE' => 33, 'BASE_DCE' => 34, 'AVIS_MEMBRES_COMMISION' => 35, 'DONNEES_REDAC' => 36, 'PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION' => 37, 'TYPE_CONTRAT' => 38, 'ENTITE_ADJUDICATRICE' => 39, 'CALENDRIER_DE_LA_CONSULTATION' => 40, 'DONNEES_COMPLEMENTAIRES' => 41, 'ESPACE_COLLABORATIF' => 42, 'HISTORIQUE_NAVIGATION_INSCRITS' => 43, 'IDENTIFICATION_CONTRAT' => 44, 'EXTRACTION_ACCORDS_CADRES' => 45, 'GESTION_OPERATIONS' => 46, 'EXTRACTION_SIRET_ACHETEUR' => 47, 'MARCHE_PUBLIC_SIMPLIFIE' => 48, 'RECHERCHES_FAVORITES_AGENT' => 49, 'PROFIL_RMA' => 50, 'FILTRE_CONTRAT_AC_SAD' => 51, 'AFFICHAGE_NOM_SERVICE_PERE' => 52, 'MODE_APPLET' => 53, 'MARCHE_DEFENSE' => 54, 'NUM_DONNEES_ESSENTIELLES_MANUEL' => 55, 'ESPACE_DOCUMENTAIRE' => 56, 'NUMERO_PROJET_ACHAT' => 57, 'MODULE_EXEC' => 58, 'ECHANGES_DOCUMENTS' => 59, 'HEURE_LIMITE_DE_REMISE_DE_PLIS_PAR_DEFAUT' => 60, 'SAISIE_MANUELLE_ID_EXTERNE' => 61, 'MODULE_SOURCING' => 62, 'MODULE_RECENSEMENT_PROGRAMMATION' => 63, 'MODULE_ENVOL' => 64, 'MODULE_BI_PREMIUM' => 65, 'ANALYSE_OFFRES' => 66, 'CAO' => 67, 'MODULE_BI' => 68, 'CMS_ACTIF' => 69, 'DCE_RESTREINT' => 70, 'ACTIVER_MON_ASSISTANT_MARCHES_PUBLICS' => 71, 'GESTION_CONTRAT_DANS_EXEC' => 72, 'CONSULTATION_SIMPLIFIEE' => 73, 'TYPAGE_JO2024' => 74, 'MODULE_TNCP' => 75, 'ACCES_MODULE_SPASER' => 76, 'PUB_TNCP' => 77, 'PUB_MOL' => 78, 'PUB_JAL_FR' => 79, 'PUB_JAL_LUX' => 80, 'PUB_JOUE' => 81, 'MODULE_ECO_SIP' => 82, 'MODULE_MPE_PUB' => 83, 'MODULE_ADMINISTRATION_DOCUMENT' => 84, ),
        BasePeer::TYPE_FIELDNAME => array ('organisme' => 0, 'encheres' => 1, 'consultation_pj_autres_pieces_telechargeables' => 2, 'no_activex' => 3, 'gestion_mapa' => 4, 'article_133_upload_fichier' => 5, 'centrale_publication' => 6, 'organisation_centralisee' => 7, 'presence_elu' => 8, 'traduire_consultation' => 9, 'suivi_passation' => 10, 'numerotation_ref_cons' => 11, 'pmi_lien_portail_defense_agent' => 12, 'interface_archive_arcade_pmi' => 13, 'desarchivage_consultation' => 14, 'alimentation_automatique_liste_invites' => 15, 'interface_chorus_pmi' => 16, 'archivage_consultation_sur_pf' => 17, 'autoriser_modification_apres_phase_consultation' => 18, 'importer_enveloppe' => 19, 'export_marches_notifies' => 20, 'acces_agents_cfe_bd_fournisseur' => 21, 'acces_agents_cfe_ouverture_analyse' => 22, 'utiliser_parametrage_encheres' => 23, 'verifier_compte_boamp' => 24, 'gestion_mandataire' => 25, 'four_eyes' => 26, 'interface_module_rsem' => 27, 'ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE' => 28, 'ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE' => 29, 'agent_verification_certificat_peppol' => 30, 'fuseau_horaire' => 31, 'fiche_weka' => 32, 'mise_disposition_pieces_marche' => 33, 'base_dce' => 34, 'avis_membres_commision' => 35, 'Donnees_Redac' => 36, 'Personnaliser_Affichage_Theme_Et_Illustration' => 37, 'type_contrat' => 38, 'entite_adjudicatrice' => 39, 'calendrier_de_la_consultation' => 40, 'donnees_complementaires' => 41, 'espace_collaboratif' => 42, 'historique_navigation_inscrits' => 43, 'Identification_contrat' => 44, 'extraction_accords_cadres' => 45, 'gestion_operations' => 46, 'extraction_siret_acheteur' => 47, 'marche_public_simplifie' => 48, 'recherches_favorites_agent' => 49, 'profil_rma' => 50, 'filtre_contrat_ac_sad' => 51, 'affichage_nom_service_pere' => 52, 'mode_applet' => 53, 'marche_defense' => 54, 'num_donnees_essentielles_manuel' => 55, 'espace_documentaire' => 56, 'numero_projet_achat' => 57, 'module_exec' => 58, 'echanges_documents' => 59, 'heure_limite_de_remise_de_plis_par_defaut' => 60, 'saisie_manuelle_id_externe' => 61, 'module_sourcing' => 62, 'module_recensement_programmation' => 63, 'module_envol' => 64, 'module_bi_premium' => 65, 'analyse_offres' => 66, 'cao' => 67, 'module_BI' => 68, 'cms_actif' => 69, 'dce_restreint' => 70, 'activer_mon_assistant_marches_publics' => 71, 'gestion_contrat_dans_exec' => 72, 'consultation_simplifiee' => 73, 'typage_jo2024' => 74, 'module_tncp' => 75, 'acces_module_spaser' => 76, 'pub_tncp' => 77, 'pub_mol' => 78, 'pub_jal_fr' => 79, 'pub_jal_lux' => 80, 'pub_joue' => 81, 'module_eco_sip' => 82, 'module_mpe_pub' => 83, 'module_administration_document' => 84, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonConfigurationOrganismePeer::ENCHERES => array(
            CommonConfigurationOrganismePeer::ENCHERES_0,
            CommonConfigurationOrganismePeer::ENCHERES_1,
        ),
        CommonConfigurationOrganismePeer::CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES => array(
            CommonConfigurationOrganismePeer::CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES_0,
            CommonConfigurationOrganismePeer::CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES_1,
        ),
        CommonConfigurationOrganismePeer::NO_ACTIVEX => array(
            CommonConfigurationOrganismePeer::NO_ACTIVEX_0,
            CommonConfigurationOrganismePeer::NO_ACTIVEX_1,
        ),
        CommonConfigurationOrganismePeer::GESTION_MAPA => array(
            CommonConfigurationOrganismePeer::GESTION_MAPA_0,
            CommonConfigurationOrganismePeer::GESTION_MAPA_1,
        ),
        CommonConfigurationOrganismePeer::ARTICLE_133_UPLOAD_FICHIER => array(
            CommonConfigurationOrganismePeer::ARTICLE_133_UPLOAD_FICHIER_0,
            CommonConfigurationOrganismePeer::ARTICLE_133_UPLOAD_FICHIER_1,
        ),
        CommonConfigurationOrganismePeer::CENTRALE_PUBLICATION => array(
            CommonConfigurationOrganismePeer::CENTRALE_PUBLICATION_0,
            CommonConfigurationOrganismePeer::CENTRALE_PUBLICATION_1,
        ),
        CommonConfigurationOrganismePeer::ORGANISATION_CENTRALISEE => array(
            CommonConfigurationOrganismePeer::ORGANISATION_CENTRALISEE_0,
            CommonConfigurationOrganismePeer::ORGANISATION_CENTRALISEE_1,
        ),
        CommonConfigurationOrganismePeer::PRESENCE_ELU => array(
            CommonConfigurationOrganismePeer::PRESENCE_ELU_0,
            CommonConfigurationOrganismePeer::PRESENCE_ELU_1,
        ),
        CommonConfigurationOrganismePeer::TRADUIRE_CONSULTATION => array(
            CommonConfigurationOrganismePeer::TRADUIRE_CONSULTATION_0,
            CommonConfigurationOrganismePeer::TRADUIRE_CONSULTATION_1,
        ),
        CommonConfigurationOrganismePeer::SUIVI_PASSATION => array(
            CommonConfigurationOrganismePeer::SUIVI_PASSATION_0,
            CommonConfigurationOrganismePeer::SUIVI_PASSATION_1,
        ),
        CommonConfigurationOrganismePeer::NUMEROTATION_REF_CONS => array(
            CommonConfigurationOrganismePeer::NUMEROTATION_REF_CONS_0,
            CommonConfigurationOrganismePeer::NUMEROTATION_REF_CONS_1,
        ),
        CommonConfigurationOrganismePeer::PMI_LIEN_PORTAIL_DEFENSE_AGENT => array(
            CommonConfigurationOrganismePeer::PMI_LIEN_PORTAIL_DEFENSE_AGENT_0,
            CommonConfigurationOrganismePeer::PMI_LIEN_PORTAIL_DEFENSE_AGENT_1,
        ),
        CommonConfigurationOrganismePeer::INTERFACE_ARCHIVE_ARCADE_PMI => array(
            CommonConfigurationOrganismePeer::INTERFACE_ARCHIVE_ARCADE_PMI_0,
            CommonConfigurationOrganismePeer::INTERFACE_ARCHIVE_ARCADE_PMI_1,
        ),
        CommonConfigurationOrganismePeer::DESARCHIVAGE_CONSULTATION => array(
            CommonConfigurationOrganismePeer::DESARCHIVAGE_CONSULTATION_0,
            CommonConfigurationOrganismePeer::DESARCHIVAGE_CONSULTATION_1,
        ),
        CommonConfigurationOrganismePeer::ALIMENTATION_AUTOMATIQUE_LISTE_INVITES => array(
            CommonConfigurationOrganismePeer::ALIMENTATION_AUTOMATIQUE_LISTE_INVITES_0,
            CommonConfigurationOrganismePeer::ALIMENTATION_AUTOMATIQUE_LISTE_INVITES_1,
        ),
        CommonConfigurationOrganismePeer::INTERFACE_CHORUS_PMI => array(
            CommonConfigurationOrganismePeer::INTERFACE_CHORUS_PMI_0,
            CommonConfigurationOrganismePeer::INTERFACE_CHORUS_PMI_1,
        ),
        CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SUR_PF => array(
            CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SUR_PF_0,
            CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SUR_PF_1,
        ),
        CommonConfigurationOrganismePeer::AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION => array(
            CommonConfigurationOrganismePeer::AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION_0,
            CommonConfigurationOrganismePeer::AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION_1,
        ),
        CommonConfigurationOrganismePeer::IMPORTER_ENVELOPPE => array(
            CommonConfigurationOrganismePeer::IMPORTER_ENVELOPPE_0,
            CommonConfigurationOrganismePeer::IMPORTER_ENVELOPPE_1,
        ),
        CommonConfigurationOrganismePeer::EXPORT_MARCHES_NOTIFIES => array(
            CommonConfigurationOrganismePeer::EXPORT_MARCHES_NOTIFIES_0,
            CommonConfigurationOrganismePeer::EXPORT_MARCHES_NOTIFIES_1,
        ),
        CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_BD_FOURNISSEUR => array(
            CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_BD_FOURNISSEUR_0,
            CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_BD_FOURNISSEUR_1,
        ),
        CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_OUVERTURE_ANALYSE => array(
            CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_OUVERTURE_ANALYSE_0,
            CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_OUVERTURE_ANALYSE_1,
        ),
        CommonConfigurationOrganismePeer::UTILISER_PARAMETRAGE_ENCHERES => array(
            CommonConfigurationOrganismePeer::UTILISER_PARAMETRAGE_ENCHERES_0,
            CommonConfigurationOrganismePeer::UTILISER_PARAMETRAGE_ENCHERES_1,
        ),
        CommonConfigurationOrganismePeer::VERIFIER_COMPTE_BOAMP => array(
            CommonConfigurationOrganismePeer::VERIFIER_COMPTE_BOAMP_0,
            CommonConfigurationOrganismePeer::VERIFIER_COMPTE_BOAMP_1,
        ),
        CommonConfigurationOrganismePeer::GESTION_MANDATAIRE => array(
            CommonConfigurationOrganismePeer::GESTION_MANDATAIRE_0,
            CommonConfigurationOrganismePeer::GESTION_MANDATAIRE_1,
        ),
        CommonConfigurationOrganismePeer::FOUR_EYES => array(
            CommonConfigurationOrganismePeer::FOUR_EYES_0,
            CommonConfigurationOrganismePeer::FOUR_EYES_1,
        ),
        CommonConfigurationOrganismePeer::INTERFACE_MODULE_RSEM => array(
            CommonConfigurationOrganismePeer::INTERFACE_MODULE_RSEM_0,
            CommonConfigurationOrganismePeer::INTERFACE_MODULE_RSEM_1,
        ),
        CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE => array(
            CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE_0,
            CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE_1,
        ),
        CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE => array(
            CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE_0,
            CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE_1,
        ),
        CommonConfigurationOrganismePeer::AGENT_VERIFICATION_CERTIFICAT_PEPPOL => array(
            CommonConfigurationOrganismePeer::AGENT_VERIFICATION_CERTIFICAT_PEPPOL_0,
            CommonConfigurationOrganismePeer::AGENT_VERIFICATION_CERTIFICAT_PEPPOL_1,
        ),
        CommonConfigurationOrganismePeer::FUSEAU_HORAIRE => array(
            CommonConfigurationOrganismePeer::FUSEAU_HORAIRE_0,
            CommonConfigurationOrganismePeer::FUSEAU_HORAIRE_1,
        ),
        CommonConfigurationOrganismePeer::FICHE_WEKA => array(
            CommonConfigurationOrganismePeer::FICHE_WEKA_0,
            CommonConfigurationOrganismePeer::FICHE_WEKA_1,
        ),
        CommonConfigurationOrganismePeer::MISE_DISPOSITION_PIECES_MARCHE => array(
            CommonConfigurationOrganismePeer::MISE_DISPOSITION_PIECES_MARCHE_0,
            CommonConfigurationOrganismePeer::MISE_DISPOSITION_PIECES_MARCHE_1,
        ),
        CommonConfigurationOrganismePeer::BASE_DCE => array(
            CommonConfigurationOrganismePeer::BASE_DCE_0,
            CommonConfigurationOrganismePeer::BASE_DCE_1,
        ),
        CommonConfigurationOrganismePeer::AVIS_MEMBRES_COMMISION => array(
            CommonConfigurationOrganismePeer::AVIS_MEMBRES_COMMISION_0,
            CommonConfigurationOrganismePeer::AVIS_MEMBRES_COMMISION_1,
        ),
        CommonConfigurationOrganismePeer::DONNEES_REDAC => array(
            CommonConfigurationOrganismePeer::DONNEES_REDAC_0,
            CommonConfigurationOrganismePeer::DONNEES_REDAC_1,
        ),
        CommonConfigurationOrganismePeer::PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION => array(
            CommonConfigurationOrganismePeer::PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION_0,
            CommonConfigurationOrganismePeer::PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION_1,
        ),
        CommonConfigurationOrganismePeer::TYPE_CONTRAT => array(
            CommonConfigurationOrganismePeer::TYPE_CONTRAT_0,
            CommonConfigurationOrganismePeer::TYPE_CONTRAT_1,
        ),
        CommonConfigurationOrganismePeer::ENTITE_ADJUDICATRICE => array(
            CommonConfigurationOrganismePeer::ENTITE_ADJUDICATRICE_0,
            CommonConfigurationOrganismePeer::ENTITE_ADJUDICATRICE_1,
        ),
        CommonConfigurationOrganismePeer::CALENDRIER_DE_LA_CONSULTATION => array(
            CommonConfigurationOrganismePeer::CALENDRIER_DE_LA_CONSULTATION_0,
            CommonConfigurationOrganismePeer::CALENDRIER_DE_LA_CONSULTATION_1,
        ),
        CommonConfigurationOrganismePeer::DONNEES_COMPLEMENTAIRES => array(
            CommonConfigurationOrganismePeer::DONNEES_COMPLEMENTAIRES_0,
            CommonConfigurationOrganismePeer::DONNEES_COMPLEMENTAIRES_1,
        ),
        CommonConfigurationOrganismePeer::ESPACE_COLLABORATIF => array(
            CommonConfigurationOrganismePeer::ESPACE_COLLABORATIF_0,
            CommonConfigurationOrganismePeer::ESPACE_COLLABORATIF_1,
        ),
        CommonConfigurationOrganismePeer::HISTORIQUE_NAVIGATION_INSCRITS => array(
            CommonConfigurationOrganismePeer::HISTORIQUE_NAVIGATION_INSCRITS_0,
            CommonConfigurationOrganismePeer::HISTORIQUE_NAVIGATION_INSCRITS_1,
        ),
        CommonConfigurationOrganismePeer::IDENTIFICATION_CONTRAT => array(
            CommonConfigurationOrganismePeer::IDENTIFICATION_CONTRAT_0,
            CommonConfigurationOrganismePeer::IDENTIFICATION_CONTRAT_1,
        ),
        CommonConfigurationOrganismePeer::EXTRACTION_ACCORDS_CADRES => array(
            CommonConfigurationOrganismePeer::EXTRACTION_ACCORDS_CADRES_0,
            CommonConfigurationOrganismePeer::EXTRACTION_ACCORDS_CADRES_1,
        ),
        CommonConfigurationOrganismePeer::GESTION_OPERATIONS => array(
            CommonConfigurationOrganismePeer::GESTION_OPERATIONS_0,
            CommonConfigurationOrganismePeer::GESTION_OPERATIONS_1,
        ),
        CommonConfigurationOrganismePeer::EXTRACTION_SIRET_ACHETEUR => array(
            CommonConfigurationOrganismePeer::EXTRACTION_SIRET_ACHETEUR_0,
            CommonConfigurationOrganismePeer::EXTRACTION_SIRET_ACHETEUR_1,
        ),
        CommonConfigurationOrganismePeer::MARCHE_PUBLIC_SIMPLIFIE => array(
            CommonConfigurationOrganismePeer::MARCHE_PUBLIC_SIMPLIFIE_0,
            CommonConfigurationOrganismePeer::MARCHE_PUBLIC_SIMPLIFIE_1,
        ),
        CommonConfigurationOrganismePeer::RECHERCHES_FAVORITES_AGENT => array(
            CommonConfigurationOrganismePeer::RECHERCHES_FAVORITES_AGENT_0,
            CommonConfigurationOrganismePeer::RECHERCHES_FAVORITES_AGENT_1,
        ),
        CommonConfigurationOrganismePeer::PROFIL_RMA => array(
            CommonConfigurationOrganismePeer::PROFIL_RMA_0,
            CommonConfigurationOrganismePeer::PROFIL_RMA_1,
        ),
        CommonConfigurationOrganismePeer::FILTRE_CONTRAT_AC_SAD => array(
            CommonConfigurationOrganismePeer::FILTRE_CONTRAT_AC_SAD_0,
            CommonConfigurationOrganismePeer::FILTRE_CONTRAT_AC_SAD_1,
        ),
        CommonConfigurationOrganismePeer::AFFICHAGE_NOM_SERVICE_PERE => array(
            CommonConfigurationOrganismePeer::AFFICHAGE_NOM_SERVICE_PERE_0,
            CommonConfigurationOrganismePeer::AFFICHAGE_NOM_SERVICE_PERE_1,
        ),
        CommonConfigurationOrganismePeer::MODE_APPLET => array(
            CommonConfigurationOrganismePeer::MODE_APPLET_0,
            CommonConfigurationOrganismePeer::MODE_APPLET_1,
        ),
        CommonConfigurationOrganismePeer::MARCHE_DEFENSE => array(
            CommonConfigurationOrganismePeer::MARCHE_DEFENSE_0,
            CommonConfigurationOrganismePeer::MARCHE_DEFENSE_1,
        ),
        CommonConfigurationOrganismePeer::NUM_DONNEES_ESSENTIELLES_MANUEL => array(
            CommonConfigurationOrganismePeer::NUM_DONNEES_ESSENTIELLES_MANUEL_0,
            CommonConfigurationOrganismePeer::NUM_DONNEES_ESSENTIELLES_MANUEL_1,
        ),
        CommonConfigurationOrganismePeer::NUMERO_PROJET_ACHAT => array(
            CommonConfigurationOrganismePeer::NUMERO_PROJET_ACHAT_0,
            CommonConfigurationOrganismePeer::NUMERO_PROJET_ACHAT_1,
        ),
        CommonConfigurationOrganismePeer::MODULE_EXEC => array(
            CommonConfigurationOrganismePeer::MODULE_EXEC_0,
            CommonConfigurationOrganismePeer::MODULE_EXEC_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonConfigurationOrganismePeer::getFieldNames($toType);
        $key = isset(CommonConfigurationOrganismePeer::$fieldKeys[$fromType][$name]) ? CommonConfigurationOrganismePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonConfigurationOrganismePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonConfigurationOrganismePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonConfigurationOrganismePeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonConfigurationOrganismePeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonConfigurationOrganismePeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonConfigurationOrganismePeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonConfigurationOrganismePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonConfigurationOrganismePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ORGANISME);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ENCHERES);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::NO_ACTIVEX);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::GESTION_MAPA);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ARTICLE_133_UPLOAD_FICHIER);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::CENTRALE_PUBLICATION);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ORGANISATION_CENTRALISEE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::PRESENCE_ELU);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::TRADUIRE_CONSULTATION);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::SUIVI_PASSATION);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::NUMEROTATION_REF_CONS);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::PMI_LIEN_PORTAIL_DEFENSE_AGENT);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::INTERFACE_ARCHIVE_ARCADE_PMI);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::DESARCHIVAGE_CONSULTATION);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ALIMENTATION_AUTOMATIQUE_LISTE_INVITES);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::INTERFACE_CHORUS_PMI);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SUR_PF);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::IMPORTER_ENVELOPPE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::EXPORT_MARCHES_NOTIFIES);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_BD_FOURNISSEUR);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_OUVERTURE_ANALYSE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::UTILISER_PARAMETRAGE_ENCHERES);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::VERIFIER_COMPTE_BOAMP);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::GESTION_MANDATAIRE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::FOUR_EYES);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::INTERFACE_MODULE_RSEM);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::AGENT_VERIFICATION_CERTIFICAT_PEPPOL);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::FUSEAU_HORAIRE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::FICHE_WEKA);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MISE_DISPOSITION_PIECES_MARCHE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::BASE_DCE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::AVIS_MEMBRES_COMMISION);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::DONNEES_REDAC);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::TYPE_CONTRAT);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ENTITE_ADJUDICATRICE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::CALENDRIER_DE_LA_CONSULTATION);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::DONNEES_COMPLEMENTAIRES);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ESPACE_COLLABORATIF);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::HISTORIQUE_NAVIGATION_INSCRITS);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::IDENTIFICATION_CONTRAT);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::EXTRACTION_ACCORDS_CADRES);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::GESTION_OPERATIONS);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::EXTRACTION_SIRET_ACHETEUR);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MARCHE_PUBLIC_SIMPLIFIE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::RECHERCHES_FAVORITES_AGENT);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::PROFIL_RMA);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::FILTRE_CONTRAT_AC_SAD);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::AFFICHAGE_NOM_SERVICE_PERE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODE_APPLET);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MARCHE_DEFENSE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::NUM_DONNEES_ESSENTIELLES_MANUEL);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ESPACE_DOCUMENTAIRE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::NUMERO_PROJET_ACHAT);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_EXEC);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ECHANGES_DOCUMENTS);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::HEURE_LIMITE_DE_REMISE_DE_PLIS_PAR_DEFAUT);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::SAISIE_MANUELLE_ID_EXTERNE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_SOURCING);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_RECENSEMENT_PROGRAMMATION);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_ENVOL);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_BI_PREMIUM);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ANALYSE_OFFRES);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::CAO);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_BI);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::CMS_ACTIF);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::DCE_RESTREINT);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ACTIVER_MON_ASSISTANT_MARCHES_PUBLICS);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::GESTION_CONTRAT_DANS_EXEC);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::CONSULTATION_SIMPLIFIEE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::TYPAGE_JO2024);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_TNCP);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::ACCES_MODULE_SPASER);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::PUB_TNCP);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::PUB_MOL);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::PUB_JAL_FR);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::PUB_JAL_LUX);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::PUB_JOUE);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_ECO_SIP);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_MPE_PUB);
            $criteria->addSelectColumn(CommonConfigurationOrganismePeer::MODULE_ADMINISTRATION_DOCUMENT);
        } else {
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.encheres');
            $criteria->addSelectColumn($alias . '.consultation_pj_autres_pieces_telechargeables');
            $criteria->addSelectColumn($alias . '.no_activex');
            $criteria->addSelectColumn($alias . '.gestion_mapa');
            $criteria->addSelectColumn($alias . '.article_133_upload_fichier');
            $criteria->addSelectColumn($alias . '.centrale_publication');
            $criteria->addSelectColumn($alias . '.organisation_centralisee');
            $criteria->addSelectColumn($alias . '.presence_elu');
            $criteria->addSelectColumn($alias . '.traduire_consultation');
            $criteria->addSelectColumn($alias . '.suivi_passation');
            $criteria->addSelectColumn($alias . '.numerotation_ref_cons');
            $criteria->addSelectColumn($alias . '.pmi_lien_portail_defense_agent');
            $criteria->addSelectColumn($alias . '.interface_archive_arcade_pmi');
            $criteria->addSelectColumn($alias . '.desarchivage_consultation');
            $criteria->addSelectColumn($alias . '.alimentation_automatique_liste_invites');
            $criteria->addSelectColumn($alias . '.interface_chorus_pmi');
            $criteria->addSelectColumn($alias . '.archivage_consultation_sur_pf');
            $criteria->addSelectColumn($alias . '.autoriser_modification_apres_phase_consultation');
            $criteria->addSelectColumn($alias . '.importer_enveloppe');
            $criteria->addSelectColumn($alias . '.export_marches_notifies');
            $criteria->addSelectColumn($alias . '.acces_agents_cfe_bd_fournisseur');
            $criteria->addSelectColumn($alias . '.acces_agents_cfe_ouverture_analyse');
            $criteria->addSelectColumn($alias . '.utiliser_parametrage_encheres');
            $criteria->addSelectColumn($alias . '.verifier_compte_boamp');
            $criteria->addSelectColumn($alias . '.gestion_mandataire');
            $criteria->addSelectColumn($alias . '.four_eyes');
            $criteria->addSelectColumn($alias . '.interface_module_rsem');
            $criteria->addSelectColumn($alias . '.ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE');
            $criteria->addSelectColumn($alias . '.ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE');
            $criteria->addSelectColumn($alias . '.agent_verification_certificat_peppol');
            $criteria->addSelectColumn($alias . '.fuseau_horaire');
            $criteria->addSelectColumn($alias . '.fiche_weka');
            $criteria->addSelectColumn($alias . '.mise_disposition_pieces_marche');
            $criteria->addSelectColumn($alias . '.base_dce');
            $criteria->addSelectColumn($alias . '.avis_membres_commision');
            $criteria->addSelectColumn($alias . '.Donnees_Redac');
            $criteria->addSelectColumn($alias . '.Personnaliser_Affichage_Theme_Et_Illustration');
            $criteria->addSelectColumn($alias . '.type_contrat');
            $criteria->addSelectColumn($alias . '.entite_adjudicatrice');
            $criteria->addSelectColumn($alias . '.calendrier_de_la_consultation');
            $criteria->addSelectColumn($alias . '.donnees_complementaires');
            $criteria->addSelectColumn($alias . '.espace_collaboratif');
            $criteria->addSelectColumn($alias . '.historique_navigation_inscrits');
            $criteria->addSelectColumn($alias . '.Identification_contrat');
            $criteria->addSelectColumn($alias . '.extraction_accords_cadres');
            $criteria->addSelectColumn($alias . '.gestion_operations');
            $criteria->addSelectColumn($alias . '.extraction_siret_acheteur');
            $criteria->addSelectColumn($alias . '.marche_public_simplifie');
            $criteria->addSelectColumn($alias . '.recherches_favorites_agent');
            $criteria->addSelectColumn($alias . '.profil_rma');
            $criteria->addSelectColumn($alias . '.filtre_contrat_ac_sad');
            $criteria->addSelectColumn($alias . '.affichage_nom_service_pere');
            $criteria->addSelectColumn($alias . '.mode_applet');
            $criteria->addSelectColumn($alias . '.marche_defense');
            $criteria->addSelectColumn($alias . '.num_donnees_essentielles_manuel');
            $criteria->addSelectColumn($alias . '.espace_documentaire');
            $criteria->addSelectColumn($alias . '.numero_projet_achat');
            $criteria->addSelectColumn($alias . '.module_exec');
            $criteria->addSelectColumn($alias . '.echanges_documents');
            $criteria->addSelectColumn($alias . '.heure_limite_de_remise_de_plis_par_defaut');
            $criteria->addSelectColumn($alias . '.saisie_manuelle_id_externe');
            $criteria->addSelectColumn($alias . '.module_sourcing');
            $criteria->addSelectColumn($alias . '.module_recensement_programmation');
            $criteria->addSelectColumn($alias . '.module_envol');
            $criteria->addSelectColumn($alias . '.module_bi_premium');
            $criteria->addSelectColumn($alias . '.analyse_offres');
            $criteria->addSelectColumn($alias . '.cao');
            $criteria->addSelectColumn($alias . '.module_BI');
            $criteria->addSelectColumn($alias . '.cms_actif');
            $criteria->addSelectColumn($alias . '.dce_restreint');
            $criteria->addSelectColumn($alias . '.activer_mon_assistant_marches_publics');
            $criteria->addSelectColumn($alias . '.gestion_contrat_dans_exec');
            $criteria->addSelectColumn($alias . '.consultation_simplifiee');
            $criteria->addSelectColumn($alias . '.typage_jo2024');
            $criteria->addSelectColumn($alias . '.module_tncp');
            $criteria->addSelectColumn($alias . '.acces_module_spaser');
            $criteria->addSelectColumn($alias . '.pub_tncp');
            $criteria->addSelectColumn($alias . '.pub_mol');
            $criteria->addSelectColumn($alias . '.pub_jal_fr');
            $criteria->addSelectColumn($alias . '.pub_jal_lux');
            $criteria->addSelectColumn($alias . '.pub_joue');
            $criteria->addSelectColumn($alias . '.module_eco_sip');
            $criteria->addSelectColumn($alias . '.module_mpe_pub');
            $criteria->addSelectColumn($alias . '.module_administration_document');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConfigurationOrganismePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConfigurationOrganismePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonConfigurationOrganismePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonConfigurationOrganisme
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonConfigurationOrganismePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonConfigurationOrganismePeer::populateObjects(CommonConfigurationOrganismePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonConfigurationOrganismePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonConfigurationOrganismePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonConfigurationOrganisme $obj A CommonConfigurationOrganisme object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getOrganisme();
            } // if key === null
            CommonConfigurationOrganismePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonConfigurationOrganisme object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonConfigurationOrganisme) {
                $key = (string) $value->getOrganisme();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonConfigurationOrganisme object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonConfigurationOrganismePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonConfigurationOrganisme Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonConfigurationOrganismePeer::$instances[$key])) {
                return CommonConfigurationOrganismePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonConfigurationOrganismePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonConfigurationOrganismePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to configuration_organisme
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (string) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonConfigurationOrganismePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonConfigurationOrganismePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonConfigurationOrganismePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonConfigurationOrganismePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonConfigurationOrganisme object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonConfigurationOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonConfigurationOrganismePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonConfigurationOrganismePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonConfigurationOrganismePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonConfigurationOrganismePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOrganisme table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonOrganisme(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConfigurationOrganismePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConfigurationOrganismePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConfigurationOrganismePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConfigurationOrganismePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonConfigurationOrganisme objects pre-filled with their CommonOrganisme objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConfigurationOrganisme objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonOrganisme(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConfigurationOrganismePeer::DATABASE_NAME);
        }

        CommonConfigurationOrganismePeer::addSelectColumns($criteria);
        $startcol = CommonConfigurationOrganismePeer::NUM_HYDRATE_COLUMNS;
        CommonOrganismePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConfigurationOrganismePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConfigurationOrganismePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConfigurationOrganismePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConfigurationOrganismePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConfigurationOrganismePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonOrganismePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonOrganismePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConfigurationOrganisme) to $obj2 (CommonOrganisme)
                // one to one relationship
                $obj1->setCommonOrganisme($obj2);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConfigurationOrganismePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConfigurationOrganismePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConfigurationOrganismePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConfigurationOrganismePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonConfigurationOrganisme objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConfigurationOrganisme objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConfigurationOrganismePeer::DATABASE_NAME);
        }

        CommonConfigurationOrganismePeer::addSelectColumns($criteria);
        $startcol2 = CommonConfigurationOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConfigurationOrganismePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConfigurationOrganismePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConfigurationOrganismePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConfigurationOrganismePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConfigurationOrganismePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonOrganisme rows

            $key2 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonOrganismePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonOrganismePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonConfigurationOrganisme) to the collection in $obj2 (CommonOrganisme)
                $obj1->setCommonOrganisme($obj2);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonConfigurationOrganismePeer::DATABASE_NAME)->getTable(CommonConfigurationOrganismePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonConfigurationOrganismePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonConfigurationOrganismePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonConfigurationOrganismeTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonConfigurationOrganismePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonConfigurationOrganisme or Criteria object.
     *
     * @param      mixed $values Criteria or CommonConfigurationOrganisme object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonConfigurationOrganisme object
        }


        // Set the correct dbName
        $criteria->setDbName(CommonConfigurationOrganismePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonConfigurationOrganisme or Criteria object.
     *
     * @param      mixed $values Criteria or CommonConfigurationOrganisme object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonConfigurationOrganismePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonConfigurationOrganismePeer::ORGANISME);
            $value = $criteria->remove(CommonConfigurationOrganismePeer::ORGANISME);
            if ($value) {
                $selectCriteria->add(CommonConfigurationOrganismePeer::ORGANISME, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonConfigurationOrganismePeer::TABLE_NAME);
            }

        } else { // $values is CommonConfigurationOrganisme object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonConfigurationOrganismePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the configuration_organisme table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonConfigurationOrganismePeer::TABLE_NAME, $con, CommonConfigurationOrganismePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonConfigurationOrganismePeer::clearInstancePool();
            CommonConfigurationOrganismePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonConfigurationOrganisme or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonConfigurationOrganisme object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonConfigurationOrganismePeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonConfigurationOrganisme) { // it's a model object
            // invalidate the cache for this single object
            CommonConfigurationOrganismePeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonConfigurationOrganismePeer::DATABASE_NAME);
            $criteria->add(CommonConfigurationOrganismePeer::ORGANISME, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonConfigurationOrganismePeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonConfigurationOrganismePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonConfigurationOrganismePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonConfigurationOrganisme object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonConfigurationOrganisme $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonConfigurationOrganismePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonConfigurationOrganismePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonConfigurationOrganismePeer::DATABASE_NAME, CommonConfigurationOrganismePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      string $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonConfigurationOrganisme
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonConfigurationOrganismePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonConfigurationOrganismePeer::DATABASE_NAME);
        $criteria->add(CommonConfigurationOrganismePeer::ORGANISME, $pk);

        $v = CommonConfigurationOrganismePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonConfigurationOrganisme[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonConfigurationOrganismePeer::DATABASE_NAME);
            $criteria->add(CommonConfigurationOrganismePeer::ORGANISME, $pks, Criteria::IN);
            $objs = CommonConfigurationOrganismePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonConfigurationOrganismePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonConfigurationOrganismePeer::buildTableMap();

