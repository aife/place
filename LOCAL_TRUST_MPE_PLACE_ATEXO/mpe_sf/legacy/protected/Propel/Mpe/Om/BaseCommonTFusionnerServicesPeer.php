<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTFusionnerServices;
use Application\Propel\Mpe\CommonTFusionnerServicesPeer;
use Application\Propel\Mpe\Map\CommonTFusionnerServicesTableMap;

/**
 * Base static class for performing query and update operations on the 't_fusionner_services' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTFusionnerServicesPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_fusionner_services';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTFusionnerServices';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTFusionnerServicesTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 10;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 10;

    /** the column name for the id field */
    const ID = 't_fusionner_services.id';

    /** the column name for the id_service_source field */
    const ID_SERVICE_SOURCE = 't_fusionner_services.id_service_source';

    /** the column name for the id_service_cible field */
    const ID_SERVICE_CIBLE = 't_fusionner_services.id_service_cible';

    /** the column name for the old_id_service_source field */
    const OLD_ID_SERVICE_SOURCE = 't_fusionner_services.old_id_service_source';

    /** the column name for the old_id_service_cible field */
    const OLD_ID_SERVICE_CIBLE = 't_fusionner_services.old_id_service_cible';

    /** the column name for the organisme field */
    const ORGANISME = 't_fusionner_services.organisme';

    /** the column name for the id_agent field */
    const ID_AGENT = 't_fusionner_services.id_agent';

    /** the column name for the date_creation field */
    const DATE_CREATION = 't_fusionner_services.date_creation';

    /** the column name for the date_fusion field */
    const DATE_FUSION = 't_fusionner_services.date_fusion';

    /** the column name for the donnees_fusionnees field */
    const DONNEES_FUSIONNEES = 't_fusionner_services.donnees_fusionnees';

    /** The enumerated values for the donnees_fusionnees field */
    const DONNEES_FUSIONNEES_0 = '0';
    const DONNEES_FUSIONNEES_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTFusionnerServices objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTFusionnerServices[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTFusionnerServicesPeer::$fieldNames[CommonTFusionnerServicesPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'IdServiceSource', 'IdServiceCible', 'OldIdServiceSource', 'OldIdServiceCible', 'Organisme', 'IdAgent', 'DateCreation', 'DateFusion', 'DonneesFusionnees', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'idServiceSource', 'idServiceCible', 'oldIdServiceSource', 'oldIdServiceCible', 'organisme', 'idAgent', 'dateCreation', 'dateFusion', 'donneesFusionnees', ),
        BasePeer::TYPE_COLNAME => array (CommonTFusionnerServicesPeer::ID, CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE, CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE, CommonTFusionnerServicesPeer::ORGANISME, CommonTFusionnerServicesPeer::ID_AGENT, CommonTFusionnerServicesPeer::DATE_CREATION, CommonTFusionnerServicesPeer::DATE_FUSION, CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ID_SERVICE_SOURCE', 'ID_SERVICE_CIBLE', 'OLD_ID_SERVICE_SOURCE', 'OLD_ID_SERVICE_CIBLE', 'ORGANISME', 'ID_AGENT', 'DATE_CREATION', 'DATE_FUSION', 'DONNEES_FUSIONNEES', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'id_service_source', 'id_service_cible', 'old_id_service_source', 'old_id_service_cible', 'organisme', 'id_agent', 'date_creation', 'date_fusion', 'donnees_fusionnees', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTFusionnerServicesPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdServiceSource' => 1, 'IdServiceCible' => 2, 'OldIdServiceSource' => 3, 'OldIdServiceCible' => 4, 'Organisme' => 5, 'IdAgent' => 6, 'DateCreation' => 7, 'DateFusion' => 8, 'DonneesFusionnees' => 9, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'idServiceSource' => 1, 'idServiceCible' => 2, 'oldIdServiceSource' => 3, 'oldIdServiceCible' => 4, 'organisme' => 5, 'idAgent' => 6, 'dateCreation' => 7, 'dateFusion' => 8, 'donneesFusionnees' => 9, ),
        BasePeer::TYPE_COLNAME => array (CommonTFusionnerServicesPeer::ID => 0, CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE => 1, CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE => 2, CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE => 3, CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE => 4, CommonTFusionnerServicesPeer::ORGANISME => 5, CommonTFusionnerServicesPeer::ID_AGENT => 6, CommonTFusionnerServicesPeer::DATE_CREATION => 7, CommonTFusionnerServicesPeer::DATE_FUSION => 8, CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES => 9, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ID_SERVICE_SOURCE' => 1, 'ID_SERVICE_CIBLE' => 2, 'OLD_ID_SERVICE_SOURCE' => 3, 'OLD_ID_SERVICE_CIBLE' => 4, 'ORGANISME' => 5, 'ID_AGENT' => 6, 'DATE_CREATION' => 7, 'DATE_FUSION' => 8, 'DONNEES_FUSIONNEES' => 9, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_service_source' => 1, 'id_service_cible' => 2, 'old_id_service_source' => 3, 'old_id_service_cible' => 4, 'organisme' => 5, 'id_agent' => 6, 'date_creation' => 7, 'date_fusion' => 8, 'donnees_fusionnees' => 9, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES => array(
            CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES_0,
            CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTFusionnerServicesPeer::getFieldNames($toType);
        $key = isset(CommonTFusionnerServicesPeer::$fieldKeys[$fromType][$name]) ? CommonTFusionnerServicesPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTFusionnerServicesPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTFusionnerServicesPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTFusionnerServicesPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTFusionnerServicesPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTFusionnerServicesPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTFusionnerServicesPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTFusionnerServicesPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTFusionnerServicesPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::ID);
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE);
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE);
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE);
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE);
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::ORGANISME);
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::ID_AGENT);
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::DATE_FUSION);
            $criteria->addSelectColumn(CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.id_service_source');
            $criteria->addSelectColumn($alias . '.id_service_cible');
            $criteria->addSelectColumn($alias . '.old_id_service_source');
            $criteria->addSelectColumn($alias . '.old_id_service_cible');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.id_agent');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_fusion');
            $criteria->addSelectColumn($alias . '.donnees_fusionnees');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTFusionnerServicesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTFusionnerServices
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTFusionnerServicesPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTFusionnerServicesPeer::populateObjects(CommonTFusionnerServicesPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTFusionnerServices $obj A CommonTFusionnerServices object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTFusionnerServicesPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTFusionnerServices object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTFusionnerServices) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTFusionnerServices object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTFusionnerServicesPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTFusionnerServices Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTFusionnerServicesPeer::$instances[$key])) {
                return CommonTFusionnerServicesPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTFusionnerServicesPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTFusionnerServicesPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_fusionner_services
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTFusionnerServicesPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTFusionnerServicesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTFusionnerServicesPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTFusionnerServicesPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTFusionnerServices object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTFusionnerServicesPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTFusionnerServicesPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTFusionnerServicesPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTFusionnerServicesPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTFusionnerServicesPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByIdServiceCible table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonServiceRelatedByIdServiceCible(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTFusionnerServicesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByIdServiceSource table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonServiceRelatedByIdServiceSource(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTFusionnerServicesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTFusionnerServices objects pre-filled with their CommonService objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTFusionnerServices objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonServiceRelatedByIdServiceCible(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);
        }

        CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        $startcol = CommonTFusionnerServicesPeer::NUM_HYDRATE_COLUMNS;
        CommonServicePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTFusionnerServicesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTFusionnerServicesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTFusionnerServicesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTFusionnerServicesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonServicePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonServicePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTFusionnerServices) to $obj2 (CommonService)
                $obj2->addCommonTFusionnerServicesRelatedByIdServiceCible($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTFusionnerServices objects pre-filled with their CommonService objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTFusionnerServices objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonServiceRelatedByIdServiceSource(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);
        }

        CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        $startcol = CommonTFusionnerServicesPeer::NUM_HYDRATE_COLUMNS;
        CommonServicePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTFusionnerServicesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTFusionnerServicesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTFusionnerServicesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTFusionnerServicesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonServicePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonServicePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTFusionnerServices) to $obj2 (CommonService)
                $obj2->addCommonTFusionnerServicesRelatedByIdServiceSource($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTFusionnerServicesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTFusionnerServices objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTFusionnerServices objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);
        }

        CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        $startcol2 = CommonTFusionnerServicesPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTFusionnerServicesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTFusionnerServicesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTFusionnerServicesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTFusionnerServicesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonService rows

            $key2 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonServicePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonServicePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTFusionnerServices) to the collection in $obj2 (CommonService)
                $obj2->addCommonTFusionnerServicesRelatedByIdServiceCible($obj1);
            } // if joined row not null

            // Add objects for joined CommonService rows

            $key3 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonServicePeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonServicePeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonTFusionnerServices) to the collection in $obj3 (CommonService)
                $obj3->addCommonTFusionnerServicesRelatedByIdServiceSource($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByIdServiceCible table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonServiceRelatedByIdServiceCible(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTFusionnerServicesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByIdServiceSource table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonServiceRelatedByIdServiceSource(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTFusionnerServicesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTFusionnerServices objects pre-filled with all related objects except CommonServiceRelatedByIdServiceCible.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTFusionnerServices objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonServiceRelatedByIdServiceCible(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);
        }

        CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        $startcol2 = CommonTFusionnerServicesPeer::NUM_HYDRATE_COLUMNS;


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTFusionnerServicesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTFusionnerServicesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTFusionnerServicesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTFusionnerServicesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTFusionnerServices objects pre-filled with all related objects except CommonServiceRelatedByIdServiceSource.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTFusionnerServices objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonServiceRelatedByIdServiceSource(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);
        }

        CommonTFusionnerServicesPeer::addSelectColumns($criteria);
        $startcol2 = CommonTFusionnerServicesPeer::NUM_HYDRATE_COLUMNS;


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTFusionnerServicesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTFusionnerServicesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTFusionnerServicesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTFusionnerServicesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTFusionnerServicesPeer::DATABASE_NAME)->getTable(CommonTFusionnerServicesPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTFusionnerServicesPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTFusionnerServicesPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTFusionnerServicesTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTFusionnerServicesPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTFusionnerServices or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTFusionnerServices object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTFusionnerServices object
        }

        if ($criteria->containsKey(CommonTFusionnerServicesPeer::ID) && $criteria->keyContainsValue(CommonTFusionnerServicesPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTFusionnerServicesPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTFusionnerServices or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTFusionnerServices object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTFusionnerServicesPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTFusionnerServicesPeer::ID);
            $value = $criteria->remove(CommonTFusionnerServicesPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTFusionnerServicesPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTFusionnerServicesPeer::TABLE_NAME);
            }

        } else { // $values is CommonTFusionnerServices object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_fusionner_services table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTFusionnerServicesPeer::TABLE_NAME, $con, CommonTFusionnerServicesPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTFusionnerServicesPeer::clearInstancePool();
            CommonTFusionnerServicesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTFusionnerServices or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTFusionnerServices object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTFusionnerServicesPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTFusionnerServices) { // it's a model object
            // invalidate the cache for this single object
            CommonTFusionnerServicesPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTFusionnerServicesPeer::DATABASE_NAME);
            $criteria->add(CommonTFusionnerServicesPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTFusionnerServicesPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTFusionnerServicesPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTFusionnerServicesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTFusionnerServices object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTFusionnerServices $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTFusionnerServicesPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTFusionnerServicesPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTFusionnerServicesPeer::DATABASE_NAME, CommonTFusionnerServicesPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTFusionnerServices
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTFusionnerServicesPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTFusionnerServicesPeer::DATABASE_NAME);
        $criteria->add(CommonTFusionnerServicesPeer::ID, $pk);

        $v = CommonTFusionnerServicesPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTFusionnerServices[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTFusionnerServicesPeer::DATABASE_NAME);
            $criteria->add(CommonTFusionnerServicesPeer::ID, $pks, Criteria::IN);
            $objs = CommonTFusionnerServicesPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTFusionnerServicesPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTFusionnerServicesPeer::buildTableMap();

