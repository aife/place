<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonPlateformeVirtuelleQuery;
use Application\Propel\Mpe\CommonTMesRecherches;
use Application\Propel\Mpe\CommonTMesRecherchesPeer;
use Application\Propel\Mpe\CommonTMesRecherchesQuery;

/**
 * Base class that represents a row from the 'T_MesRecherches' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTMesRecherches extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTMesRecherchesPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTMesRecherchesPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the id_createur field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $id_createur;

    /**
     * The value for the type_createur field.
     * @var        string
     */
    protected $type_createur;

    /**
     * The value for the denomination field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination;

    /**
     * The value for the periodicite field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $periodicite;

    /**
     * The value for the xmlcriteria field.
     * @var        string
     */
    protected $xmlcriteria;

    /**
     * The value for the categorie field.
     * @var        string
     */
    protected $categorie;

    /**
     * The value for the id_initial field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $id_initial;

    /**
     * The value for the format field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $format;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the recherche field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $recherche;

    /**
     * The value for the alerte field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte;

    /**
     * The value for the type_avis field.
     * Note: this column has a database default value of: 3
     * @var        int
     */
    protected $type_avis;

    /**
     * The value for the plateforme_virtuelle_id field.
     * @var        int
     */
    protected $plateforme_virtuelle_id;

    /**
     * The value for the criteria field.
     * @var        string
     */
    protected $criteria;

    /**
     * The value for the hashed_criteria field.
     * @var        string
     */
    protected $hashed_criteria;

    /**
     * @var        CommonPlateformeVirtuelle
     */
    protected $aCommonPlateformeVirtuelle;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->id_createur = 0;
        $this->denomination = '';
        $this->periodicite = '';
        $this->id_initial = 0;
        $this->format = '1';
        $this->recherche = '0';
        $this->alerte = '0';
        $this->type_avis = 3;
    }

    /**
     * Initializes internal state of BaseCommonTMesRecherches object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [id_createur] column value.
     *
     * @return int
     */
    public function getIdCreateur()
    {

        return $this->id_createur;
    }

    /**
     * Get the [type_createur] column value.
     *
     * @return string
     */
    public function getTypeCreateur()
    {

        return $this->type_createur;
    }

    /**
     * Get the [denomination] column value.
     *
     * @return string
     */
    public function getDenomination()
    {

        return $this->denomination;
    }

    /**
     * Get the [periodicite] column value.
     *
     * @return string
     */
    public function getPeriodicite()
    {

        return $this->periodicite;
    }

    /**
     * Get the [xmlcriteria] column value.
     *
     * @return string
     */
    public function getXmlcriteria()
    {

        return $this->xmlcriteria;
    }

    /**
     * Get the [categorie] column value.
     *
     * @return string
     */
    public function getCategorie()
    {

        return $this->categorie;
    }

    /**
     * Get the [id_initial] column value.
     *
     * @return int
     */
    public function getIdInitial()
    {

        return $this->id_initial;
    }

    /**
     * Get the [format] column value.
     *
     * @return string
     */
    public function getFormat()
    {

        return $this->format;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_modification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModification($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification === null) {
            return null;
        }

        if ($this->date_modification === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [recherche] column value.
     *
     * @return string
     */
    public function getRecherche()
    {

        return $this->recherche;
    }

    /**
     * Get the [alerte] column value.
     *
     * @return string
     */
    public function getAlerte()
    {

        return $this->alerte;
    }

    /**
     * Get the [type_avis] column value.
     *
     * @return int
     */
    public function getTypeAvis()
    {

        return $this->type_avis;
    }

    /**
     * Get the [plateforme_virtuelle_id] column value.
     *
     * @return int
     */
    public function getPlateformeVirtuelleId()
    {

        return $this->plateforme_virtuelle_id;
    }

    /**
     * Get the [criteria] column value.
     *
     * @return string
     */
    public function getCriteria()
    {

        return $this->criteria;
    }

    /**
     * Get the [hashed_criteria] column value.
     *
     * @return string
     */
    public function getHashedCriteria()
    {

        return $this->hashed_criteria;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [id_createur] column.
     *
     * @param int $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setIdCreateur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_createur !== $v) {
            $this->id_createur = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::ID_CREATEUR;
        }


        return $this;
    } // setIdCreateur()

    /**
     * Set the value of [type_createur] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setTypeCreateur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_createur !== $v) {
            $this->type_createur = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::TYPE_CREATEUR;
        }


        return $this;
    } // setTypeCreateur()

    /**
     * Set the value of [denomination] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setDenomination($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination !== $v) {
            $this->denomination = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::DENOMINATION;
        }


        return $this;
    } // setDenomination()

    /**
     * Set the value of [periodicite] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setPeriodicite($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->periodicite !== $v) {
            $this->periodicite = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::PERIODICITE;
        }


        return $this;
    } // setPeriodicite()

    /**
     * Set the value of [xmlcriteria] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setXmlcriteria($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->xmlcriteria !== $v) {
            $this->xmlcriteria = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::XMLCRITERIA;
        }


        return $this;
    } // setXmlcriteria()

    /**
     * Set the value of [categorie] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setCategorie($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->categorie !== $v) {
            $this->categorie = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::CATEGORIE;
        }


        return $this;
    } // setCategorie()

    /**
     * Set the value of [id_initial] column.
     *
     * @param int $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setIdInitial($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_initial !== $v) {
            $this->id_initial = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::ID_INITIAL;
        }


        return $this;
    } // setIdInitial()

    /**
     * Set the value of [format] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setFormat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->format !== $v) {
            $this->format = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::FORMAT;
        }


        return $this;
    } // setFormat()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTMesRecherchesPeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Sets the value of [date_modification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification !== null && $tmpDt = new DateTime($this->date_modification)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification = $newDateAsString;
                $this->modifiedColumns[] = CommonTMesRecherchesPeer::DATE_MODIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDateModification()

    /**
     * Set the value of [recherche] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setRecherche($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->recherche !== $v) {
            $this->recherche = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::RECHERCHE;
        }


        return $this;
    } // setRecherche()

    /**
     * Set the value of [alerte] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setAlerte($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte !== $v) {
            $this->alerte = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::ALERTE;
        }


        return $this;
    } // setAlerte()

    /**
     * Set the value of [type_avis] column.
     *
     * @param int $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setTypeAvis($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->type_avis !== $v) {
            $this->type_avis = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::TYPE_AVIS;
        }


        return $this;
    } // setTypeAvis()

    /**
     * Set the value of [plateforme_virtuelle_id] column.
     *
     * @param int $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setPlateformeVirtuelleId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->plateforme_virtuelle_id !== $v) {
            $this->plateforme_virtuelle_id = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID;
        }

        if ($this->aCommonPlateformeVirtuelle !== null && $this->aCommonPlateformeVirtuelle->getId() !== $v) {
            $this->aCommonPlateformeVirtuelle = null;
        }


        return $this;
    } // setPlateformeVirtuelleId()

    /**
     * Set the value of [criteria] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setCriteria($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->criteria !== $v) {
            $this->criteria = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::CRITERIA;
        }


        return $this;
    } // setCriteria()

    /**
     * Set the value of [hashed_criteria] column.
     *
     * @param string $v new value
     * @return CommonTMesRecherches The current object (for fluent API support)
     */
    public function setHashedCriteria($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->hashed_criteria !== $v) {
            $this->hashed_criteria = $v;
            $this->modifiedColumns[] = CommonTMesRecherchesPeer::HASHED_CRITERIA;
        }


        return $this;
    } // setHashedCriteria()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->id_createur !== 0) {
                return false;
            }

            if ($this->denomination !== '') {
                return false;
            }

            if ($this->periodicite !== '') {
                return false;
            }

            if ($this->id_initial !== 0) {
                return false;
            }

            if ($this->format !== '1') {
                return false;
            }

            if ($this->recherche !== '0') {
                return false;
            }

            if ($this->alerte !== '0') {
                return false;
            }

            if ($this->type_avis !== 3) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_createur = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->type_createur = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->denomination = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->periodicite = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->xmlcriteria = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->categorie = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->id_initial = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->format = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->date_creation = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->date_modification = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->recherche = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->alerte = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->type_avis = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->plateforme_virtuelle_id = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->criteria = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->hashed_criteria = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 17; // 17 = CommonTMesRecherchesPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTMesRecherches object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonPlateformeVirtuelle !== null && $this->plateforme_virtuelle_id !== $this->aCommonPlateformeVirtuelle->getId()) {
            $this->aCommonPlateformeVirtuelle = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTMesRecherchesPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonPlateformeVirtuelle = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTMesRecherchesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTMesRecherchesPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonPlateformeVirtuelle !== null) {
                if ($this->aCommonPlateformeVirtuelle->isModified() || $this->aCommonPlateformeVirtuelle->isNew()) {
                    $affectedRows += $this->aCommonPlateformeVirtuelle->save($con);
                }
                $this->setCommonPlateformeVirtuelle($this->aCommonPlateformeVirtuelle);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTMesRecherchesPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTMesRecherchesPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTMesRecherchesPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::ID_CREATEUR)) {
            $modifiedColumns[':p' . $index++]  = '`id_createur`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::TYPE_CREATEUR)) {
            $modifiedColumns[':p' . $index++]  = '`type_createur`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::DENOMINATION)) {
            $modifiedColumns[':p' . $index++]  = '`denomination`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::PERIODICITE)) {
            $modifiedColumns[':p' . $index++]  = '`periodicite`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::XMLCRITERIA)) {
            $modifiedColumns[':p' . $index++]  = '`xmlCriteria`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::CATEGORIE)) {
            $modifiedColumns[':p' . $index++]  = '`categorie`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::ID_INITIAL)) {
            $modifiedColumns[':p' . $index++]  = '`id_initial`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::FORMAT)) {
            $modifiedColumns[':p' . $index++]  = '`format`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::RECHERCHE)) {
            $modifiedColumns[':p' . $index++]  = '`recherche`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::ALERTE)) {
            $modifiedColumns[':p' . $index++]  = '`alerte`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::TYPE_AVIS)) {
            $modifiedColumns[':p' . $index++]  = '`type_avis`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`plateforme_virtuelle_id`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::CRITERIA)) {
            $modifiedColumns[':p' . $index++]  = '`criteria`';
        }
        if ($this->isColumnModified(CommonTMesRecherchesPeer::HASHED_CRITERIA)) {
            $modifiedColumns[':p' . $index++]  = '`hashed_criteria`';
        }

        $sql = sprintf(
            'INSERT INTO `T_MesRecherches` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`id_createur`':
                        $stmt->bindValue($identifier, $this->id_createur, PDO::PARAM_INT);
                        break;
                    case '`type_createur`':
                        $stmt->bindValue($identifier, $this->type_createur, PDO::PARAM_STR);
                        break;
                    case '`denomination`':
                        $stmt->bindValue($identifier, $this->denomination, PDO::PARAM_STR);
                        break;
                    case '`periodicite`':
                        $stmt->bindValue($identifier, $this->periodicite, PDO::PARAM_STR);
                        break;
                    case '`xmlCriteria`':
                        $stmt->bindValue($identifier, $this->xmlcriteria, PDO::PARAM_STR);
                        break;
                    case '`categorie`':
                        $stmt->bindValue($identifier, $this->categorie, PDO::PARAM_STR);
                        break;
                    case '`id_initial`':
                        $stmt->bindValue($identifier, $this->id_initial, PDO::PARAM_INT);
                        break;
                    case '`format`':
                        $stmt->bindValue($identifier, $this->format, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`recherche`':
                        $stmt->bindValue($identifier, $this->recherche, PDO::PARAM_STR);
                        break;
                    case '`alerte`':
                        $stmt->bindValue($identifier, $this->alerte, PDO::PARAM_STR);
                        break;
                    case '`type_avis`':
                        $stmt->bindValue($identifier, $this->type_avis, PDO::PARAM_INT);
                        break;
                    case '`plateforme_virtuelle_id`':
                        $stmt->bindValue($identifier, $this->plateforme_virtuelle_id, PDO::PARAM_INT);
                        break;
                    case '`criteria`':
                        $stmt->bindValue($identifier, $this->criteria, PDO::PARAM_STR);
                        break;
                    case '`hashed_criteria`':
                        $stmt->bindValue($identifier, $this->hashed_criteria, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonPlateformeVirtuelle !== null) {
                if (!$this->aCommonPlateformeVirtuelle->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonPlateformeVirtuelle->getValidationFailures());
                }
            }


            if (($retval = CommonTMesRecherchesPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTMesRecherchesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIdCreateur();
                break;
            case 2:
                return $this->getTypeCreateur();
                break;
            case 3:
                return $this->getDenomination();
                break;
            case 4:
                return $this->getPeriodicite();
                break;
            case 5:
                return $this->getXmlcriteria();
                break;
            case 6:
                return $this->getCategorie();
                break;
            case 7:
                return $this->getIdInitial();
                break;
            case 8:
                return $this->getFormat();
                break;
            case 9:
                return $this->getDateCreation();
                break;
            case 10:
                return $this->getDateModification();
                break;
            case 11:
                return $this->getRecherche();
                break;
            case 12:
                return $this->getAlerte();
                break;
            case 13:
                return $this->getTypeAvis();
                break;
            case 14:
                return $this->getPlateformeVirtuelleId();
                break;
            case 15:
                return $this->getCriteria();
                break;
            case 16:
                return $this->getHashedCriteria();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTMesRecherches'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTMesRecherches'][$this->getPrimaryKey()] = true;
        $keys = CommonTMesRecherchesPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIdCreateur(),
            $keys[2] => $this->getTypeCreateur(),
            $keys[3] => $this->getDenomination(),
            $keys[4] => $this->getPeriodicite(),
            $keys[5] => $this->getXmlcriteria(),
            $keys[6] => $this->getCategorie(),
            $keys[7] => $this->getIdInitial(),
            $keys[8] => $this->getFormat(),
            $keys[9] => $this->getDateCreation(),
            $keys[10] => $this->getDateModification(),
            $keys[11] => $this->getRecherche(),
            $keys[12] => $this->getAlerte(),
            $keys[13] => $this->getTypeAvis(),
            $keys[14] => $this->getPlateformeVirtuelleId(),
            $keys[15] => $this->getCriteria(),
            $keys[16] => $this->getHashedCriteria(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonPlateformeVirtuelle) {
                $result['CommonPlateformeVirtuelle'] = $this->aCommonPlateformeVirtuelle->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTMesRecherchesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIdCreateur($value);
                break;
            case 2:
                $this->setTypeCreateur($value);
                break;
            case 3:
                $this->setDenomination($value);
                break;
            case 4:
                $this->setPeriodicite($value);
                break;
            case 5:
                $this->setXmlcriteria($value);
                break;
            case 6:
                $this->setCategorie($value);
                break;
            case 7:
                $this->setIdInitial($value);
                break;
            case 8:
                $this->setFormat($value);
                break;
            case 9:
                $this->setDateCreation($value);
                break;
            case 10:
                $this->setDateModification($value);
                break;
            case 11:
                $this->setRecherche($value);
                break;
            case 12:
                $this->setAlerte($value);
                break;
            case 13:
                $this->setTypeAvis($value);
                break;
            case 14:
                $this->setPlateformeVirtuelleId($value);
                break;
            case 15:
                $this->setCriteria($value);
                break;
            case 16:
                $this->setHashedCriteria($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTMesRecherchesPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdCreateur($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTypeCreateur($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDenomination($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPeriodicite($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setXmlcriteria($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCategorie($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setIdInitial($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setFormat($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setDateCreation($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setDateModification($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setRecherche($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setAlerte($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setTypeAvis($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setPlateformeVirtuelleId($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setCriteria($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setHashedCriteria($arr[$keys[16]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTMesRecherchesPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTMesRecherchesPeer::ID)) $criteria->add(CommonTMesRecherchesPeer::ID, $this->id);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::ID_CREATEUR)) $criteria->add(CommonTMesRecherchesPeer::ID_CREATEUR, $this->id_createur);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::TYPE_CREATEUR)) $criteria->add(CommonTMesRecherchesPeer::TYPE_CREATEUR, $this->type_createur);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::DENOMINATION)) $criteria->add(CommonTMesRecherchesPeer::DENOMINATION, $this->denomination);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::PERIODICITE)) $criteria->add(CommonTMesRecherchesPeer::PERIODICITE, $this->periodicite);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::XMLCRITERIA)) $criteria->add(CommonTMesRecherchesPeer::XMLCRITERIA, $this->xmlcriteria);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::CATEGORIE)) $criteria->add(CommonTMesRecherchesPeer::CATEGORIE, $this->categorie);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::ID_INITIAL)) $criteria->add(CommonTMesRecherchesPeer::ID_INITIAL, $this->id_initial);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::FORMAT)) $criteria->add(CommonTMesRecherchesPeer::FORMAT, $this->format);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::DATE_CREATION)) $criteria->add(CommonTMesRecherchesPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::DATE_MODIFICATION)) $criteria->add(CommonTMesRecherchesPeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::RECHERCHE)) $criteria->add(CommonTMesRecherchesPeer::RECHERCHE, $this->recherche);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::ALERTE)) $criteria->add(CommonTMesRecherchesPeer::ALERTE, $this->alerte);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::TYPE_AVIS)) $criteria->add(CommonTMesRecherchesPeer::TYPE_AVIS, $this->type_avis);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID)) $criteria->add(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, $this->plateforme_virtuelle_id);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::CRITERIA)) $criteria->add(CommonTMesRecherchesPeer::CRITERIA, $this->criteria);
        if ($this->isColumnModified(CommonTMesRecherchesPeer::HASHED_CRITERIA)) $criteria->add(CommonTMesRecherchesPeer::HASHED_CRITERIA, $this->hashed_criteria);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTMesRecherchesPeer::DATABASE_NAME);
        $criteria->add(CommonTMesRecherchesPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTMesRecherches (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdCreateur($this->getIdCreateur());
        $copyObj->setTypeCreateur($this->getTypeCreateur());
        $copyObj->setDenomination($this->getDenomination());
        $copyObj->setPeriodicite($this->getPeriodicite());
        $copyObj->setXmlcriteria($this->getXmlcriteria());
        $copyObj->setCategorie($this->getCategorie());
        $copyObj->setIdInitial($this->getIdInitial());
        $copyObj->setFormat($this->getFormat());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setRecherche($this->getRecherche());
        $copyObj->setAlerte($this->getAlerte());
        $copyObj->setTypeAvis($this->getTypeAvis());
        $copyObj->setPlateformeVirtuelleId($this->getPlateformeVirtuelleId());
        $copyObj->setCriteria($this->getCriteria());
        $copyObj->setHashedCriteria($this->getHashedCriteria());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTMesRecherches Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTMesRecherchesPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTMesRecherchesPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonPlateformeVirtuelle object.
     *
     * @param   CommonPlateformeVirtuelle $v
     * @return CommonTMesRecherches The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonPlateformeVirtuelle(CommonPlateformeVirtuelle $v = null)
    {
        if ($v === null) {
            $this->setPlateformeVirtuelleId(NULL);
        } else {
            $this->setPlateformeVirtuelleId($v->getId());
        }

        $this->aCommonPlateformeVirtuelle = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonPlateformeVirtuelle object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTMesRecherches($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonPlateformeVirtuelle object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonPlateformeVirtuelle The associated CommonPlateformeVirtuelle object.
     * @throws PropelException
     */
    public function getCommonPlateformeVirtuelle(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonPlateformeVirtuelle === null && ($this->plateforme_virtuelle_id !== null) && $doQuery) {
            $this->aCommonPlateformeVirtuelle = CommonPlateformeVirtuelleQuery::create()->findPk($this->plateforme_virtuelle_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonPlateformeVirtuelle->addCommonTMesRecherchess($this);
             */
        }

        return $this->aCommonPlateformeVirtuelle;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->id_createur = null;
        $this->type_createur = null;
        $this->denomination = null;
        $this->periodicite = null;
        $this->xmlcriteria = null;
        $this->categorie = null;
        $this->id_initial = null;
        $this->format = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->recherche = null;
        $this->alerte = null;
        $this->type_avis = null;
        $this->plateforme_virtuelle_id = null;
        $this->criteria = null;
        $this->hashed_criteria = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonPlateformeVirtuelle instanceof Persistent) {
              $this->aCommonPlateformeVirtuelle->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonPlateformeVirtuelle = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTMesRecherchesPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
