<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTDumeContexteQuery;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Propel\Mpe\CommonTDumeNumeroPeer;
use Application\Propel\Mpe\CommonTDumeNumeroQuery;

/**
 * Base class that represents a row from the 't_dume_numero' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDumeNumero extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTDumeNumeroPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTDumeNumeroPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the numero_dume_national field.
     * @var        string
     */
    protected $numero_dume_national;

    /**
     * The value for the blob_id field.
     * @var        int
     */
    protected $blob_id;

    /**
     * The value for the list_lot field.
     * @var        string
     */
    protected $list_lot;

    /**
     * The value for the id_dume_contexte field.
     * @var        int
     */
    protected $id_dume_contexte;

    /**
     * The value for the date_recuperation_pdf field.
     * @var        string
     */
    protected $date_recuperation_pdf;

    /**
     * The value for the blob_id_xml field.
     * @var        int
     */
    protected $blob_id_xml;

    /**
     * The value for the date_recuperation_xml field.
     * @var        string
     */
    protected $date_recuperation_xml;

    /**
     * @var        CommonTDumeContexte
     */
    protected $aCommonTDumeContexte;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [numero_dume_national] column value.
     *
     * @return string
     */
    public function getNumeroDumeNational()
    {

        return $this->numero_dume_national;
    }

    /**
     * Get the [blob_id] column value.
     *
     * @return int
     */
    public function getBlobId()
    {

        return $this->blob_id;
    }

    /**
     * Get the [list_lot] column value.
     *
     * @return string
     */
    public function getListLot()
    {

        return $this->list_lot;
    }

    /**
     * Get the [id_dume_contexte] column value.
     *
     * @return int
     */
    public function getIdDumeContexte()
    {

        return $this->id_dume_contexte;
    }

    /**
     * Get the [optionally formatted] temporal [date_recuperation_pdf] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateRecuperationPdf($format = 'Y-m-d H:i:s')
    {
        if ($this->date_recuperation_pdf === null) {
            return null;
        }

        if ($this->date_recuperation_pdf === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_recuperation_pdf);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_recuperation_pdf, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [blob_id_xml] column value.
     *
     * @return int
     */
    public function getBlobIdXml()
    {

        return $this->blob_id_xml;
    }

    /**
     * Get the [optionally formatted] temporal [date_recuperation_xml] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateRecuperationXml($format = 'Y-m-d H:i:s')
    {
        if ($this->date_recuperation_xml === null) {
            return null;
        }

        if ($this->date_recuperation_xml === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_recuperation_xml);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_recuperation_xml, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTDumeNumero The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTDumeNumeroPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [numero_dume_national] column.
     *
     * @param string $v new value
     * @return CommonTDumeNumero The current object (for fluent API support)
     */
    public function setNumeroDumeNational($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->numero_dume_national !== $v) {
            $this->numero_dume_national = $v;
            $this->modifiedColumns[] = CommonTDumeNumeroPeer::NUMERO_DUME_NATIONAL;
        }


        return $this;
    } // setNumeroDumeNational()

    /**
     * Set the value of [blob_id] column.
     *
     * @param int $v new value
     * @return CommonTDumeNumero The current object (for fluent API support)
     */
    public function setBlobId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->blob_id !== $v) {
            $this->blob_id = $v;
            $this->modifiedColumns[] = CommonTDumeNumeroPeer::BLOB_ID;
        }


        return $this;
    } // setBlobId()

    /**
     * Set the value of [list_lot] column.
     *
     * @param string $v new value
     * @return CommonTDumeNumero The current object (for fluent API support)
     */
    public function setListLot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->list_lot !== $v) {
            $this->list_lot = $v;
            $this->modifiedColumns[] = CommonTDumeNumeroPeer::LIST_LOT;
        }


        return $this;
    } // setListLot()

    /**
     * Set the value of [id_dume_contexte] column.
     *
     * @param int $v new value
     * @return CommonTDumeNumero The current object (for fluent API support)
     */
    public function setIdDumeContexte($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_dume_contexte !== $v) {
            $this->id_dume_contexte = $v;
            $this->modifiedColumns[] = CommonTDumeNumeroPeer::ID_DUME_CONTEXTE;
        }

        if ($this->aCommonTDumeContexte !== null && $this->aCommonTDumeContexte->getId() !== $v) {
            $this->aCommonTDumeContexte = null;
        }


        return $this;
    } // setIdDumeContexte()

    /**
     * Sets the value of [date_recuperation_pdf] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTDumeNumero The current object (for fluent API support)
     */
    public function setDateRecuperationPdf($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_recuperation_pdf !== null || $dt !== null) {
            $currentDateAsString = ($this->date_recuperation_pdf !== null && $tmpDt = new DateTime($this->date_recuperation_pdf)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_recuperation_pdf = $newDateAsString;
                $this->modifiedColumns[] = CommonTDumeNumeroPeer::DATE_RECUPERATION_PDF;
            }
        } // if either are not null


        return $this;
    } // setDateRecuperationPdf()

    /**
     * Set the value of [blob_id_xml] column.
     *
     * @param int $v new value
     * @return CommonTDumeNumero The current object (for fluent API support)
     */
    public function setBlobIdXml($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->blob_id_xml !== $v) {
            $this->blob_id_xml = $v;
            $this->modifiedColumns[] = CommonTDumeNumeroPeer::BLOB_ID_XML;
        }


        return $this;
    } // setBlobIdXml()

    /**
     * Sets the value of [date_recuperation_xml] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTDumeNumero The current object (for fluent API support)
     */
    public function setDateRecuperationXml($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_recuperation_xml !== null || $dt !== null) {
            $currentDateAsString = ($this->date_recuperation_xml !== null && $tmpDt = new DateTime($this->date_recuperation_xml)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_recuperation_xml = $newDateAsString;
                $this->modifiedColumns[] = CommonTDumeNumeroPeer::DATE_RECUPERATION_XML;
            }
        } // if either are not null


        return $this;
    } // setDateRecuperationXml()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->numero_dume_national = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->blob_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->list_lot = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->id_dume_contexte = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->date_recuperation_pdf = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->blob_id_xml = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->date_recuperation_xml = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 8; // 8 = CommonTDumeNumeroPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTDumeNumero object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTDumeContexte !== null && $this->id_dume_contexte !== $this->aCommonTDumeContexte->getId()) {
            $this->aCommonTDumeContexte = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDumeNumeroPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTDumeNumeroPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTDumeContexte = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDumeNumeroPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTDumeNumeroQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDumeNumeroPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTDumeNumeroPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTDumeContexte !== null) {
                if ($this->aCommonTDumeContexte->isModified() || $this->aCommonTDumeContexte->isNew()) {
                    $affectedRows += $this->aCommonTDumeContexte->save($con);
                }
                $this->setCommonTDumeContexte($this->aCommonTDumeContexte);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTDumeNumeroPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTDumeNumeroPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTDumeNumeroPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTDumeNumeroPeer::NUMERO_DUME_NATIONAL)) {
            $modifiedColumns[':p' . $index++]  = '`numero_dume_national`';
        }
        if ($this->isColumnModified(CommonTDumeNumeroPeer::BLOB_ID)) {
            $modifiedColumns[':p' . $index++]  = '`blob_id`';
        }
        if ($this->isColumnModified(CommonTDumeNumeroPeer::LIST_LOT)) {
            $modifiedColumns[':p' . $index++]  = '`list_lot`';
        }
        if ($this->isColumnModified(CommonTDumeNumeroPeer::ID_DUME_CONTEXTE)) {
            $modifiedColumns[':p' . $index++]  = '`id_dume_contexte`';
        }
        if ($this->isColumnModified(CommonTDumeNumeroPeer::DATE_RECUPERATION_PDF)) {
            $modifiedColumns[':p' . $index++]  = '`date_recuperation_pdf`';
        }
        if ($this->isColumnModified(CommonTDumeNumeroPeer::BLOB_ID_XML)) {
            $modifiedColumns[':p' . $index++]  = '`blob_id_xml`';
        }
        if ($this->isColumnModified(CommonTDumeNumeroPeer::DATE_RECUPERATION_XML)) {
            $modifiedColumns[':p' . $index++]  = '`date_recuperation_xml`';
        }

        $sql = sprintf(
            'INSERT INTO `t_dume_numero` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`numero_dume_national`':
                        $stmt->bindValue($identifier, $this->numero_dume_national, PDO::PARAM_STR);
                        break;
                    case '`blob_id`':
                        $stmt->bindValue($identifier, $this->blob_id, PDO::PARAM_INT);
                        break;
                    case '`list_lot`':
                        $stmt->bindValue($identifier, $this->list_lot, PDO::PARAM_STR);
                        break;
                    case '`id_dume_contexte`':
                        $stmt->bindValue($identifier, $this->id_dume_contexte, PDO::PARAM_INT);
                        break;
                    case '`date_recuperation_pdf`':
                        $stmt->bindValue($identifier, $this->date_recuperation_pdf, PDO::PARAM_STR);
                        break;
                    case '`blob_id_xml`':
                        $stmt->bindValue($identifier, $this->blob_id_xml, PDO::PARAM_INT);
                        break;
                    case '`date_recuperation_xml`':
                        $stmt->bindValue($identifier, $this->date_recuperation_xml, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTDumeContexte !== null) {
                if (!$this->aCommonTDumeContexte->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTDumeContexte->getValidationFailures());
                }
            }


            if (($retval = CommonTDumeNumeroPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDumeNumeroPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNumeroDumeNational();
                break;
            case 2:
                return $this->getBlobId();
                break;
            case 3:
                return $this->getListLot();
                break;
            case 4:
                return $this->getIdDumeContexte();
                break;
            case 5:
                return $this->getDateRecuperationPdf();
                break;
            case 6:
                return $this->getBlobIdXml();
                break;
            case 7:
                return $this->getDateRecuperationXml();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTDumeNumero'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTDumeNumero'][$this->getPrimaryKey()] = true;
        $keys = CommonTDumeNumeroPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNumeroDumeNational(),
            $keys[2] => $this->getBlobId(),
            $keys[3] => $this->getListLot(),
            $keys[4] => $this->getIdDumeContexte(),
            $keys[5] => $this->getDateRecuperationPdf(),
            $keys[6] => $this->getBlobIdXml(),
            $keys[7] => $this->getDateRecuperationXml(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTDumeContexte) {
                $result['CommonTDumeContexte'] = $this->aCommonTDumeContexte->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDumeNumeroPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNumeroDumeNational($value);
                break;
            case 2:
                $this->setBlobId($value);
                break;
            case 3:
                $this->setListLot($value);
                break;
            case 4:
                $this->setIdDumeContexte($value);
                break;
            case 5:
                $this->setDateRecuperationPdf($value);
                break;
            case 6:
                $this->setBlobIdXml($value);
                break;
            case 7:
                $this->setDateRecuperationXml($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTDumeNumeroPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNumeroDumeNational($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setBlobId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setListLot($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdDumeContexte($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDateRecuperationPdf($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setBlobIdXml($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDateRecuperationXml($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTDumeNumeroPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTDumeNumeroPeer::ID)) $criteria->add(CommonTDumeNumeroPeer::ID, $this->id);
        if ($this->isColumnModified(CommonTDumeNumeroPeer::NUMERO_DUME_NATIONAL)) $criteria->add(CommonTDumeNumeroPeer::NUMERO_DUME_NATIONAL, $this->numero_dume_national);
        if ($this->isColumnModified(CommonTDumeNumeroPeer::BLOB_ID)) $criteria->add(CommonTDumeNumeroPeer::BLOB_ID, $this->blob_id);
        if ($this->isColumnModified(CommonTDumeNumeroPeer::LIST_LOT)) $criteria->add(CommonTDumeNumeroPeer::LIST_LOT, $this->list_lot);
        if ($this->isColumnModified(CommonTDumeNumeroPeer::ID_DUME_CONTEXTE)) $criteria->add(CommonTDumeNumeroPeer::ID_DUME_CONTEXTE, $this->id_dume_contexte);
        if ($this->isColumnModified(CommonTDumeNumeroPeer::DATE_RECUPERATION_PDF)) $criteria->add(CommonTDumeNumeroPeer::DATE_RECUPERATION_PDF, $this->date_recuperation_pdf);
        if ($this->isColumnModified(CommonTDumeNumeroPeer::BLOB_ID_XML)) $criteria->add(CommonTDumeNumeroPeer::BLOB_ID_XML, $this->blob_id_xml);
        if ($this->isColumnModified(CommonTDumeNumeroPeer::DATE_RECUPERATION_XML)) $criteria->add(CommonTDumeNumeroPeer::DATE_RECUPERATION_XML, $this->date_recuperation_xml);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTDumeNumeroPeer::DATABASE_NAME);
        $criteria->add(CommonTDumeNumeroPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTDumeNumero (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNumeroDumeNational($this->getNumeroDumeNational());
        $copyObj->setBlobId($this->getBlobId());
        $copyObj->setListLot($this->getListLot());
        $copyObj->setIdDumeContexte($this->getIdDumeContexte());
        $copyObj->setDateRecuperationPdf($this->getDateRecuperationPdf());
        $copyObj->setBlobIdXml($this->getBlobIdXml());
        $copyObj->setDateRecuperationXml($this->getDateRecuperationXml());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTDumeNumero Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTDumeNumeroPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTDumeNumeroPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonTDumeContexte object.
     *
     * @param   CommonTDumeContexte $v
     * @return CommonTDumeNumero The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTDumeContexte(CommonTDumeContexte $v = null)
    {
        if ($v === null) {
            $this->setIdDumeContexte(NULL);
        } else {
            $this->setIdDumeContexte($v->getId());
        }

        $this->aCommonTDumeContexte = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTDumeContexte object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTDumeNumero($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTDumeContexte object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTDumeContexte The associated CommonTDumeContexte object.
     * @throws PropelException
     */
    public function getCommonTDumeContexte(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTDumeContexte === null && ($this->id_dume_contexte !== null) && $doQuery) {
            $this->aCommonTDumeContexte = CommonTDumeContexteQuery::create()->findPk($this->id_dume_contexte, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTDumeContexte->addCommonTDumeNumeros($this);
             */
        }

        return $this->aCommonTDumeContexte;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->numero_dume_national = null;
        $this->blob_id = null;
        $this->list_lot = null;
        $this->id_dume_contexte = null;
        $this->date_recuperation_pdf = null;
        $this->blob_id_xml = null;
        $this->date_recuperation_xml = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonTDumeContexte instanceof Persistent) {
              $this->aCommonTDumeContexte->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonTDumeContexte = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTDumeNumeroPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
