<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonPlateformeVirtuelleOrganisme;
use Application\Propel\Mpe\CommonPlateformeVirtuelleOrganismePeer;
use Application\Propel\Mpe\CommonPlateformeVirtuelleOrganismeQuery;

/**
 * Base class that represents a query for the 'plateforme_virtuelle_organisme' table.
 *
 *
 *
 * @method CommonPlateformeVirtuelleOrganismeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonPlateformeVirtuelleOrganismeQuery orderByPlateformeId($order = Criteria::ASC) Order by the plateforme_id column
 * @method CommonPlateformeVirtuelleOrganismeQuery orderByOrganismeAcronyme($order = Criteria::ASC) Order by the organisme_acronyme column
 *
 * @method CommonPlateformeVirtuelleOrganismeQuery groupById() Group by the id column
 * @method CommonPlateformeVirtuelleOrganismeQuery groupByPlateformeId() Group by the plateforme_id column
 * @method CommonPlateformeVirtuelleOrganismeQuery groupByOrganismeAcronyme() Group by the organisme_acronyme column
 *
 * @method CommonPlateformeVirtuelleOrganismeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonPlateformeVirtuelleOrganismeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonPlateformeVirtuelleOrganismeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonPlateformeVirtuelleOrganismeQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonPlateformeVirtuelleOrganismeQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonPlateformeVirtuelleOrganismeQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonPlateformeVirtuelleOrganismeQuery leftJoinCommonPlateformeVirtuelle($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonPlateformeVirtuelle relation
 * @method CommonPlateformeVirtuelleOrganismeQuery rightJoinCommonPlateformeVirtuelle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonPlateformeVirtuelle relation
 * @method CommonPlateformeVirtuelleOrganismeQuery innerJoinCommonPlateformeVirtuelle($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonPlateformeVirtuelle relation
 *
 * @method CommonPlateformeVirtuelleOrganisme findOne(PropelPDO $con = null) Return the first CommonPlateformeVirtuelleOrganisme matching the query
 * @method CommonPlateformeVirtuelleOrganisme findOneOrCreate(PropelPDO $con = null) Return the first CommonPlateformeVirtuelleOrganisme matching the query, or a new CommonPlateformeVirtuelleOrganisme object populated from the query conditions when no match is found
 *
 * @method CommonPlateformeVirtuelleOrganisme findOneByPlateformeId(int $plateforme_id) Return the first CommonPlateformeVirtuelleOrganisme filtered by the plateforme_id column
 * @method CommonPlateformeVirtuelleOrganisme findOneByOrganismeAcronyme(string $organisme_acronyme) Return the first CommonPlateformeVirtuelleOrganisme filtered by the organisme_acronyme column
 *
 * @method array findById(int $id) Return CommonPlateformeVirtuelleOrganisme objects filtered by the id column
 * @method array findByPlateformeId(int $plateforme_id) Return CommonPlateformeVirtuelleOrganisme objects filtered by the plateforme_id column
 * @method array findByOrganismeAcronyme(string $organisme_acronyme) Return CommonPlateformeVirtuelleOrganisme objects filtered by the organisme_acronyme column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonPlateformeVirtuelleOrganismeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonPlateformeVirtuelleOrganismeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonPlateformeVirtuelleOrganisme', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonPlateformeVirtuelleOrganismeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonPlateformeVirtuelleOrganismeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonPlateformeVirtuelleOrganismeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonPlateformeVirtuelleOrganismeQuery) {
            return $criteria;
        }
        $query = new CommonPlateformeVirtuelleOrganismeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonPlateformeVirtuelleOrganisme|CommonPlateformeVirtuelleOrganisme[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonPlateformeVirtuelleOrganismePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonPlateformeVirtuelleOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonPlateformeVirtuelleOrganisme A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonPlateformeVirtuelleOrganisme A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `plateforme_id`, `organisme_acronyme` FROM `plateforme_virtuelle_organisme` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonPlateformeVirtuelleOrganisme();
            $obj->hydrate($row);
            CommonPlateformeVirtuelleOrganismePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonPlateformeVirtuelleOrganisme|CommonPlateformeVirtuelleOrganisme[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonPlateformeVirtuelleOrganisme[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the plateforme_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPlateformeId(1234); // WHERE plateforme_id = 1234
     * $query->filterByPlateformeId(array(12, 34)); // WHERE plateforme_id IN (12, 34)
     * $query->filterByPlateformeId(array('min' => 12)); // WHERE plateforme_id >= 12
     * $query->filterByPlateformeId(array('max' => 12)); // WHERE plateforme_id <= 12
     * </code>
     *
     * @see       filterByCommonPlateformeVirtuelle()
     *
     * @param     mixed $plateformeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     */
    public function filterByPlateformeId($plateformeId = null, $comparison = null)
    {
        if (is_array($plateformeId)) {
            $useMinMax = false;
            if (isset($plateformeId['min'])) {
                $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::PLATEFORME_ID, $plateformeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($plateformeId['max'])) {
                $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::PLATEFORME_ID, $plateformeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::PLATEFORME_ID, $plateformeId, $comparison);
    }

    /**
     * Filter the query on the organisme_acronyme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganismeAcronyme('fooValue');   // WHERE organisme_acronyme = 'fooValue'
     * $query->filterByOrganismeAcronyme('%fooValue%'); // WHERE organisme_acronyme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organismeAcronyme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     */
    public function filterByOrganismeAcronyme($organismeAcronyme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organismeAcronyme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organismeAcronyme)) {
                $organismeAcronyme = str_replace('*', '%', $organismeAcronyme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::ORGANISME_ACRONYME, $organismeAcronyme, $comparison);
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::ORGANISME_ACRONYME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::ORGANISME_ACRONYME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Filter the query by a related CommonPlateformeVirtuelle object
     *
     * @param   CommonPlateformeVirtuelle|PropelObjectCollection $commonPlateformeVirtuelle The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonPlateformeVirtuelle($commonPlateformeVirtuelle, $comparison = null)
    {
        if ($commonPlateformeVirtuelle instanceof CommonPlateformeVirtuelle) {
            return $this
                ->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::PLATEFORME_ID, $commonPlateformeVirtuelle->getId(), $comparison);
        } elseif ($commonPlateformeVirtuelle instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::PLATEFORME_ID, $commonPlateformeVirtuelle->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonPlateformeVirtuelle() only accepts arguments of type CommonPlateformeVirtuelle or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonPlateformeVirtuelle relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     */
    public function joinCommonPlateformeVirtuelle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonPlateformeVirtuelle');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonPlateformeVirtuelle');
        }

        return $this;
    }

    /**
     * Use the CommonPlateformeVirtuelle relation CommonPlateformeVirtuelle object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonPlateformeVirtuelleQuery A secondary query class using the current class as primary query
     */
    public function useCommonPlateformeVirtuelleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonPlateformeVirtuelle($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonPlateformeVirtuelle', '\Application\Propel\Mpe\CommonPlateformeVirtuelleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonPlateformeVirtuelleOrganisme $commonPlateformeVirtuelleOrganisme Object to remove from the list of results
     *
     * @return CommonPlateformeVirtuelleOrganismeQuery The current query, for fluid interface
     */
    public function prune($commonPlateformeVirtuelleOrganisme = null)
    {
        if ($commonPlateformeVirtuelleOrganisme) {
            $this->addUsingAlias(CommonPlateformeVirtuelleOrganismePeer::ID, $commonPlateformeVirtuelleOrganisme->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
