<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonMailTemplate;
use Application\Propel\Mpe\CommonMailType;
use Application\Propel\Mpe\CommonMailTypeGroup;
use Application\Propel\Mpe\CommonMailTypePeer;
use Application\Propel\Mpe\CommonMailTypeQuery;

/**
 * Base class that represents a query for the 'mail_type' table.
 *
 *
 *
 * @method CommonMailTypeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonMailTypeQuery orderByMailTypeGroupId($order = Criteria::ASC) Order by the mail_type_group_id column
 * @method CommonMailTypeQuery orderByLabel($order = Criteria::ASC) Order by the label column
 * @method CommonMailTypeQuery orderByCode($order = Criteria::ASC) Order by the code column
 *
 * @method CommonMailTypeQuery groupById() Group by the id column
 * @method CommonMailTypeQuery groupByMailTypeGroupId() Group by the mail_type_group_id column
 * @method CommonMailTypeQuery groupByLabel() Group by the label column
 * @method CommonMailTypeQuery groupByCode() Group by the code column
 *
 * @method CommonMailTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonMailTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonMailTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonMailTypeQuery leftJoinCommonMailTypeGroup($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonMailTypeGroup relation
 * @method CommonMailTypeQuery rightJoinCommonMailTypeGroup($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonMailTypeGroup relation
 * @method CommonMailTypeQuery innerJoinCommonMailTypeGroup($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonMailTypeGroup relation
 *
 * @method CommonMailTypeQuery leftJoinCommonMailTemplate($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonMailTemplate relation
 * @method CommonMailTypeQuery rightJoinCommonMailTemplate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonMailTemplate relation
 * @method CommonMailTypeQuery innerJoinCommonMailTemplate($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonMailTemplate relation
 *
 * @method CommonMailType findOne(PropelPDO $con = null) Return the first CommonMailType matching the query
 * @method CommonMailType findOneOrCreate(PropelPDO $con = null) Return the first CommonMailType matching the query, or a new CommonMailType object populated from the query conditions when no match is found
 *
 * @method CommonMailType findOneByMailTypeGroupId(int $mail_type_group_id) Return the first CommonMailType filtered by the mail_type_group_id column
 * @method CommonMailType findOneByLabel(string $label) Return the first CommonMailType filtered by the label column
 * @method CommonMailType findOneByCode(string $code) Return the first CommonMailType filtered by the code column
 *
 * @method array findById(int $id) Return CommonMailType objects filtered by the id column
 * @method array findByMailTypeGroupId(int $mail_type_group_id) Return CommonMailType objects filtered by the mail_type_group_id column
 * @method array findByLabel(string $label) Return CommonMailType objects filtered by the label column
 * @method array findByCode(string $code) Return CommonMailType objects filtered by the code column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonMailTypeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonMailTypeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonMailType', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonMailTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonMailTypeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonMailTypeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonMailTypeQuery) {
            return $criteria;
        }
        $query = new CommonMailTypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonMailType|CommonMailType[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonMailTypePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonMailTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonMailType A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonMailType A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `mail_type_group_id`, `label`, `code` FROM `mail_type` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonMailType();
            $obj->hydrate($row);
            CommonMailTypePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonMailType|CommonMailType[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonMailType[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonMailTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonMailTypePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonMailTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonMailTypePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTypeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonMailTypePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonMailTypePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonMailTypePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the mail_type_group_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMailTypeGroupId(1234); // WHERE mail_type_group_id = 1234
     * $query->filterByMailTypeGroupId(array(12, 34)); // WHERE mail_type_group_id IN (12, 34)
     * $query->filterByMailTypeGroupId(array('min' => 12)); // WHERE mail_type_group_id >= 12
     * $query->filterByMailTypeGroupId(array('max' => 12)); // WHERE mail_type_group_id <= 12
     * </code>
     *
     * @see       filterByCommonMailTypeGroup()
     *
     * @param     mixed $mailTypeGroupId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTypeQuery The current query, for fluid interface
     */
    public function filterByMailTypeGroupId($mailTypeGroupId = null, $comparison = null)
    {
        if (is_array($mailTypeGroupId)) {
            $useMinMax = false;
            if (isset($mailTypeGroupId['min'])) {
                $this->addUsingAlias(CommonMailTypePeer::MAIL_TYPE_GROUP_ID, $mailTypeGroupId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mailTypeGroupId['max'])) {
                $this->addUsingAlias(CommonMailTypePeer::MAIL_TYPE_GROUP_ID, $mailTypeGroupId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonMailTypePeer::MAIL_TYPE_GROUP_ID, $mailTypeGroupId, $comparison);
    }

    /**
     * Filter the query on the label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE label = 'fooValue'
     * $query->filterByLabel('%fooValue%'); // WHERE label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTypeQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $label)) {
                $label = str_replace('*', '%', $label);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonMailTypePeer::LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonMailTypeQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonMailTypePeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query by a related CommonMailTypeGroup object
     *
     * @param   CommonMailTypeGroup|PropelObjectCollection $commonMailTypeGroup The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonMailTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonMailTypeGroup($commonMailTypeGroup, $comparison = null)
    {
        if ($commonMailTypeGroup instanceof CommonMailTypeGroup) {
            return $this
                ->addUsingAlias(CommonMailTypePeer::MAIL_TYPE_GROUP_ID, $commonMailTypeGroup->getId(), $comparison);
        } elseif ($commonMailTypeGroup instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonMailTypePeer::MAIL_TYPE_GROUP_ID, $commonMailTypeGroup->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonMailTypeGroup() only accepts arguments of type CommonMailTypeGroup or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonMailTypeGroup relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonMailTypeQuery The current query, for fluid interface
     */
    public function joinCommonMailTypeGroup($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonMailTypeGroup');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonMailTypeGroup');
        }

        return $this;
    }

    /**
     * Use the CommonMailTypeGroup relation CommonMailTypeGroup object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonMailTypeGroupQuery A secondary query class using the current class as primary query
     */
    public function useCommonMailTypeGroupQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonMailTypeGroup($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonMailTypeGroup', '\Application\Propel\Mpe\CommonMailTypeGroupQuery');
    }

    /**
     * Filter the query by a related CommonMailTemplate object
     *
     * @param   CommonMailTemplate|PropelObjectCollection $commonMailTemplate  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonMailTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonMailTemplate($commonMailTemplate, $comparison = null)
    {
        if ($commonMailTemplate instanceof CommonMailTemplate) {
            return $this
                ->addUsingAlias(CommonMailTypePeer::ID, $commonMailTemplate->getMailTypeId(), $comparison);
        } elseif ($commonMailTemplate instanceof PropelObjectCollection) {
            return $this
                ->useCommonMailTemplateQuery()
                ->filterByPrimaryKeys($commonMailTemplate->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonMailTemplate() only accepts arguments of type CommonMailTemplate or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonMailTemplate relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonMailTypeQuery The current query, for fluid interface
     */
    public function joinCommonMailTemplate($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonMailTemplate');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonMailTemplate');
        }

        return $this;
    }

    /**
     * Use the CommonMailTemplate relation CommonMailTemplate object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonMailTemplateQuery A secondary query class using the current class as primary query
     */
    public function useCommonMailTemplateQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonMailTemplate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonMailTemplate', '\Application\Propel\Mpe\CommonMailTemplateQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonMailType $commonMailType Object to remove from the list of results
     *
     * @return CommonMailTypeQuery The current query, for fluid interface
     */
    public function prune($commonMailType = null)
    {
        if ($commonMailType) {
            $this->addUsingAlias(CommonMailTypePeer::ID, $commonMailType->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
