<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcession;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionPeer;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionQuery;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionTarif;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionTarifQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;

/**
 * Base class that represents a row from the 'donnees_annuelles_concession' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonDonneesAnnuellesConcession extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonDonneesAnnuellesConcessionPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonDonneesAnnuellesConcessionPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the id_contrat field.
     * @var        int
     */
    protected $id_contrat;

    /**
     * The value for the valeur_depense field.
     * @var        double
     */
    protected $valeur_depense;

    /**
     * The value for the date_saisie field.
     * @var        string
     */
    protected $date_saisie;

    /**
     * The value for the num_ordre field.
     * @var        int
     */
    protected $num_ordre;

    /**
     * The value for the suivi_publication_sn field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $suivi_publication_sn;

    /**
     * @var        CommonTContratTitulaire
     */
    protected $aCommonTContratTitulaire;

    /**
     * @var        PropelObjectCollection|CommonDonneesAnnuellesConcessionTarif[] Collection to store aggregation of CommonDonneesAnnuellesConcessionTarif objects.
     */
    protected $collCommonDonneesAnnuellesConcessionTarifs;
    protected $collCommonDonneesAnnuellesConcessionTarifsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonDonneesAnnuellesConcessionTarifsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->suivi_publication_sn = 0;
    }

    /**
     * Initializes internal state of BaseCommonDonneesAnnuellesConcession object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [id_contrat] column value.
     *
     * @return int
     */
    public function getIdContrat()
    {

        return $this->id_contrat;
    }

    /**
     * Get the [valeur_depense] column value.
     *
     * @return double
     */
    public function getValeurDepense()
    {

        return $this->valeur_depense;
    }

    /**
     * Get the [optionally formatted] temporal [date_saisie] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateSaisie($format = 'Y-m-d H:i:s')
    {
        if ($this->date_saisie === null) {
            return null;
        }

        if ($this->date_saisie === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_saisie);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_saisie, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [num_ordre] column value.
     *
     * @return int
     */
    public function getNumOrdre()
    {

        return $this->num_ordre;
    }

    /**
     * Get the [suivi_publication_sn] column value.
     *
     * @return int
     */
    public function getSuiviPublicationSn()
    {

        return $this->suivi_publication_sn;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonDonneesAnnuellesConcessionPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [id_contrat] column.
     *
     * @param int $v new value
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     */
    public function setIdContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_contrat !== $v) {
            $this->id_contrat = $v;
            $this->modifiedColumns[] = CommonDonneesAnnuellesConcessionPeer::ID_CONTRAT;
        }

        if ($this->aCommonTContratTitulaire !== null && $this->aCommonTContratTitulaire->getIdContratTitulaire() !== $v) {
            $this->aCommonTContratTitulaire = null;
        }


        return $this;
    } // setIdContrat()

    /**
     * Set the value of [valeur_depense] column.
     *
     * @param double $v new value
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     */
    public function setValeurDepense($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->valeur_depense !== $v) {
            $this->valeur_depense = $v;
            $this->modifiedColumns[] = CommonDonneesAnnuellesConcessionPeer::VALEUR_DEPENSE;
        }


        return $this;
    } // setValeurDepense()

    /**
     * Sets the value of [date_saisie] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     */
    public function setDateSaisie($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_saisie !== null || $dt !== null) {
            $currentDateAsString = ($this->date_saisie !== null && $tmpDt = new DateTime($this->date_saisie)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_saisie = $newDateAsString;
                $this->modifiedColumns[] = CommonDonneesAnnuellesConcessionPeer::DATE_SAISIE;
            }
        } // if either are not null


        return $this;
    } // setDateSaisie()

    /**
     * Set the value of [num_ordre] column.
     *
     * @param int $v new value
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     */
    public function setNumOrdre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->num_ordre !== $v) {
            $this->num_ordre = $v;
            $this->modifiedColumns[] = CommonDonneesAnnuellesConcessionPeer::NUM_ORDRE;
        }


        return $this;
    } // setNumOrdre()

    /**
     * Set the value of [suivi_publication_sn] column.
     *
     * @param int $v new value
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     */
    public function setSuiviPublicationSn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->suivi_publication_sn !== $v) {
            $this->suivi_publication_sn = $v;
            $this->modifiedColumns[] = CommonDonneesAnnuellesConcessionPeer::SUIVI_PUBLICATION_SN;
        }


        return $this;
    } // setSuiviPublicationSn()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->suivi_publication_sn !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_contrat = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->valeur_depense = ($row[$startcol + 2] !== null) ? (double) $row[$startcol + 2] : null;
            $this->date_saisie = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->num_ordre = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->suivi_publication_sn = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 6; // 6 = CommonDonneesAnnuellesConcessionPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonDonneesAnnuellesConcession object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTContratTitulaire !== null && $this->id_contrat !== $this->aCommonTContratTitulaire->getIdContratTitulaire()) {
            $this->aCommonTContratTitulaire = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonDonneesAnnuellesConcessionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonDonneesAnnuellesConcessionPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTContratTitulaire = null;
            $this->collCommonDonneesAnnuellesConcessionTarifs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonDonneesAnnuellesConcessionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonDonneesAnnuellesConcessionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonDonneesAnnuellesConcessionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonDonneesAnnuellesConcessionPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTContratTitulaire !== null) {
                if ($this->aCommonTContratTitulaire->isModified() || $this->aCommonTContratTitulaire->isNew()) {
                    $affectedRows += $this->aCommonTContratTitulaire->save($con);
                }
                $this->setCommonTContratTitulaire($this->aCommonTContratTitulaire);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonDonneesAnnuellesConcessionTarifsScheduledForDeletion !== null) {
                if (!$this->commonDonneesAnnuellesConcessionTarifsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonDonneesAnnuellesConcessionTarifsScheduledForDeletion as $commonDonneesAnnuellesConcessionTarif) {
                        // need to save related object because we set the relation to null
                        $commonDonneesAnnuellesConcessionTarif->save($con);
                    }
                    $this->commonDonneesAnnuellesConcessionTarifsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonDonneesAnnuellesConcessionTarifs !== null) {
                foreach ($this->collCommonDonneesAnnuellesConcessionTarifs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonDonneesAnnuellesConcessionPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonDonneesAnnuellesConcessionPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::ID_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`id_contrat`';
        }
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::VALEUR_DEPENSE)) {
            $modifiedColumns[':p' . $index++]  = '`valeur_depense`';
        }
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::DATE_SAISIE)) {
            $modifiedColumns[':p' . $index++]  = '`date_saisie`';
        }
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::NUM_ORDRE)) {
            $modifiedColumns[':p' . $index++]  = '`num_ordre`';
        }
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::SUIVI_PUBLICATION_SN)) {
            $modifiedColumns[':p' . $index++]  = '`suivi_publication_sn`';
        }

        $sql = sprintf(
            'INSERT INTO `donnees_annuelles_concession` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`id_contrat`':
                        $stmt->bindValue($identifier, $this->id_contrat, PDO::PARAM_INT);
                        break;
                    case '`valeur_depense`':
                        $stmt->bindValue($identifier, $this->valeur_depense, PDO::PARAM_STR);
                        break;
                    case '`date_saisie`':
                        $stmt->bindValue($identifier, $this->date_saisie, PDO::PARAM_STR);
                        break;
                    case '`num_ordre`':
                        $stmt->bindValue($identifier, $this->num_ordre, PDO::PARAM_INT);
                        break;
                    case '`suivi_publication_sn`':
                        $stmt->bindValue($identifier, $this->suivi_publication_sn, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTContratTitulaire !== null) {
                if (!$this->aCommonTContratTitulaire->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTContratTitulaire->getValidationFailures());
                }
            }


            if (($retval = CommonDonneesAnnuellesConcessionPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonDonneesAnnuellesConcessionTarifs !== null) {
                    foreach ($this->collCommonDonneesAnnuellesConcessionTarifs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonDonneesAnnuellesConcessionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIdContrat();
                break;
            case 2:
                return $this->getValeurDepense();
                break;
            case 3:
                return $this->getDateSaisie();
                break;
            case 4:
                return $this->getNumOrdre();
                break;
            case 5:
                return $this->getSuiviPublicationSn();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonDonneesAnnuellesConcession'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonDonneesAnnuellesConcession'][$this->getPrimaryKey()] = true;
        $keys = CommonDonneesAnnuellesConcessionPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIdContrat(),
            $keys[2] => $this->getValeurDepense(),
            $keys[3] => $this->getDateSaisie(),
            $keys[4] => $this->getNumOrdre(),
            $keys[5] => $this->getSuiviPublicationSn(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTContratTitulaire) {
                $result['CommonTContratTitulaire'] = $this->aCommonTContratTitulaire->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonDonneesAnnuellesConcessionTarifs) {
                $result['CommonDonneesAnnuellesConcessionTarifs'] = $this->collCommonDonneesAnnuellesConcessionTarifs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonDonneesAnnuellesConcessionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIdContrat($value);
                break;
            case 2:
                $this->setValeurDepense($value);
                break;
            case 3:
                $this->setDateSaisie($value);
                break;
            case 4:
                $this->setNumOrdre($value);
                break;
            case 5:
                $this->setSuiviPublicationSn($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonDonneesAnnuellesConcessionPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdContrat($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setValeurDepense($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDateSaisie($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNumOrdre($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSuiviPublicationSn($arr[$keys[5]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonDonneesAnnuellesConcessionPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::ID)) $criteria->add(CommonDonneesAnnuellesConcessionPeer::ID, $this->id);
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::ID_CONTRAT)) $criteria->add(CommonDonneesAnnuellesConcessionPeer::ID_CONTRAT, $this->id_contrat);
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::VALEUR_DEPENSE)) $criteria->add(CommonDonneesAnnuellesConcessionPeer::VALEUR_DEPENSE, $this->valeur_depense);
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::DATE_SAISIE)) $criteria->add(CommonDonneesAnnuellesConcessionPeer::DATE_SAISIE, $this->date_saisie);
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::NUM_ORDRE)) $criteria->add(CommonDonneesAnnuellesConcessionPeer::NUM_ORDRE, $this->num_ordre);
        if ($this->isColumnModified(CommonDonneesAnnuellesConcessionPeer::SUIVI_PUBLICATION_SN)) $criteria->add(CommonDonneesAnnuellesConcessionPeer::SUIVI_PUBLICATION_SN, $this->suivi_publication_sn);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonDonneesAnnuellesConcessionPeer::DATABASE_NAME);
        $criteria->add(CommonDonneesAnnuellesConcessionPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonDonneesAnnuellesConcession (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdContrat($this->getIdContrat());
        $copyObj->setValeurDepense($this->getValeurDepense());
        $copyObj->setDateSaisie($this->getDateSaisie());
        $copyObj->setNumOrdre($this->getNumOrdre());
        $copyObj->setSuiviPublicationSn($this->getSuiviPublicationSn());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonDonneesAnnuellesConcessionTarifs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonDonneesAnnuellesConcessionTarif($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonDonneesAnnuellesConcession Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonDonneesAnnuellesConcessionPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonDonneesAnnuellesConcessionPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonTContratTitulaire object.
     *
     * @param   CommonTContratTitulaire $v
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTContratTitulaire(CommonTContratTitulaire $v = null)
    {
        if ($v === null) {
            $this->setIdContrat(NULL);
        } else {
            $this->setIdContrat($v->getIdContratTitulaire());
        }

        $this->aCommonTContratTitulaire = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTContratTitulaire object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonDonneesAnnuellesConcession($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTContratTitulaire object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTContratTitulaire The associated CommonTContratTitulaire object.
     * @throws PropelException
     */
    public function getCommonTContratTitulaire(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTContratTitulaire === null && ($this->id_contrat !== null) && $doQuery) {
            $this->aCommonTContratTitulaire = CommonTContratTitulaireQuery::create()->findPk($this->id_contrat, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTContratTitulaire->addCommonDonneesAnnuellesConcessions($this);
             */
        }

        return $this->aCommonTContratTitulaire;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonDonneesAnnuellesConcessionTarif' == $relationName) {
            $this->initCommonDonneesAnnuellesConcessionTarifs();
        }
    }

    /**
     * Clears out the collCommonDonneesAnnuellesConcessionTarifs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     * @see        addCommonDonneesAnnuellesConcessionTarifs()
     */
    public function clearCommonDonneesAnnuellesConcessionTarifs()
    {
        $this->collCommonDonneesAnnuellesConcessionTarifs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonDonneesAnnuellesConcessionTarifsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonDonneesAnnuellesConcessionTarifs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonDonneesAnnuellesConcessionTarifs($v = true)
    {
        $this->collCommonDonneesAnnuellesConcessionTarifsPartial = $v;
    }

    /**
     * Initializes the collCommonDonneesAnnuellesConcessionTarifs collection.
     *
     * By default this just sets the collCommonDonneesAnnuellesConcessionTarifs collection to an empty array (like clearcollCommonDonneesAnnuellesConcessionTarifs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonDonneesAnnuellesConcessionTarifs($overrideExisting = true)
    {
        if (null !== $this->collCommonDonneesAnnuellesConcessionTarifs && !$overrideExisting) {
            return;
        }
        $this->collCommonDonneesAnnuellesConcessionTarifs = new PropelObjectCollection();
        $this->collCommonDonneesAnnuellesConcessionTarifs->setModel('CommonDonneesAnnuellesConcessionTarif');
    }

    /**
     * Gets an array of CommonDonneesAnnuellesConcessionTarif objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonDonneesAnnuellesConcession is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonDonneesAnnuellesConcessionTarif[] List of CommonDonneesAnnuellesConcessionTarif objects
     * @throws PropelException
     */
    public function getCommonDonneesAnnuellesConcessionTarifs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonDonneesAnnuellesConcessionTarifsPartial && !$this->isNew();
        if (null === $this->collCommonDonneesAnnuellesConcessionTarifs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonDonneesAnnuellesConcessionTarifs) {
                // return empty collection
                $this->initCommonDonneesAnnuellesConcessionTarifs();
            } else {
                $collCommonDonneesAnnuellesConcessionTarifs = CommonDonneesAnnuellesConcessionTarifQuery::create(null, $criteria)
                    ->filterByCommonDonneesAnnuellesConcession($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonDonneesAnnuellesConcessionTarifsPartial && count($collCommonDonneesAnnuellesConcessionTarifs)) {
                      $this->initCommonDonneesAnnuellesConcessionTarifs(false);

                      foreach ($collCommonDonneesAnnuellesConcessionTarifs as $obj) {
                        if (false == $this->collCommonDonneesAnnuellesConcessionTarifs->contains($obj)) {
                          $this->collCommonDonneesAnnuellesConcessionTarifs->append($obj);
                        }
                      }

                      $this->collCommonDonneesAnnuellesConcessionTarifsPartial = true;
                    }

                    $collCommonDonneesAnnuellesConcessionTarifs->getInternalIterator()->rewind();

                    return $collCommonDonneesAnnuellesConcessionTarifs;
                }

                if ($partial && $this->collCommonDonneesAnnuellesConcessionTarifs) {
                    foreach ($this->collCommonDonneesAnnuellesConcessionTarifs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonDonneesAnnuellesConcessionTarifs[] = $obj;
                        }
                    }
                }

                $this->collCommonDonneesAnnuellesConcessionTarifs = $collCommonDonneesAnnuellesConcessionTarifs;
                $this->collCommonDonneesAnnuellesConcessionTarifsPartial = false;
            }
        }

        return $this->collCommonDonneesAnnuellesConcessionTarifs;
    }

    /**
     * Sets a collection of CommonDonneesAnnuellesConcessionTarif objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonDonneesAnnuellesConcessionTarifs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     */
    public function setCommonDonneesAnnuellesConcessionTarifs(PropelCollection $commonDonneesAnnuellesConcessionTarifs, PropelPDO $con = null)
    {
        $commonDonneesAnnuellesConcessionTarifsToDelete = $this->getCommonDonneesAnnuellesConcessionTarifs(new Criteria(), $con)->diff($commonDonneesAnnuellesConcessionTarifs);


        $this->commonDonneesAnnuellesConcessionTarifsScheduledForDeletion = $commonDonneesAnnuellesConcessionTarifsToDelete;

        foreach ($commonDonneesAnnuellesConcessionTarifsToDelete as $commonDonneesAnnuellesConcessionTarifRemoved) {
            $commonDonneesAnnuellesConcessionTarifRemoved->setCommonDonneesAnnuellesConcession(null);
        }

        $this->collCommonDonneesAnnuellesConcessionTarifs = null;
        foreach ($commonDonneesAnnuellesConcessionTarifs as $commonDonneesAnnuellesConcessionTarif) {
            $this->addCommonDonneesAnnuellesConcessionTarif($commonDonneesAnnuellesConcessionTarif);
        }

        $this->collCommonDonneesAnnuellesConcessionTarifs = $commonDonneesAnnuellesConcessionTarifs;
        $this->collCommonDonneesAnnuellesConcessionTarifsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonDonneesAnnuellesConcessionTarif objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonDonneesAnnuellesConcessionTarif objects.
     * @throws PropelException
     */
    public function countCommonDonneesAnnuellesConcessionTarifs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonDonneesAnnuellesConcessionTarifsPartial && !$this->isNew();
        if (null === $this->collCommonDonneesAnnuellesConcessionTarifs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonDonneesAnnuellesConcessionTarifs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonDonneesAnnuellesConcessionTarifs());
            }
            $query = CommonDonneesAnnuellesConcessionTarifQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonDonneesAnnuellesConcession($this)
                ->count($con);
        }

        return count($this->collCommonDonneesAnnuellesConcessionTarifs);
    }

    /**
     * Method called to associate a CommonDonneesAnnuellesConcessionTarif object to this object
     * through the CommonDonneesAnnuellesConcessionTarif foreign key attribute.
     *
     * @param   CommonDonneesAnnuellesConcessionTarif $l CommonDonneesAnnuellesConcessionTarif
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     */
    public function addCommonDonneesAnnuellesConcessionTarif(CommonDonneesAnnuellesConcessionTarif $l)
    {
        if ($this->collCommonDonneesAnnuellesConcessionTarifs === null) {
            $this->initCommonDonneesAnnuellesConcessionTarifs();
            $this->collCommonDonneesAnnuellesConcessionTarifsPartial = true;
        }
        if (!in_array($l, $this->collCommonDonneesAnnuellesConcessionTarifs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonDonneesAnnuellesConcessionTarif($l);
        }

        return $this;
    }

    /**
     * @param	CommonDonneesAnnuellesConcessionTarif $commonDonneesAnnuellesConcessionTarif The commonDonneesAnnuellesConcessionTarif object to add.
     */
    protected function doAddCommonDonneesAnnuellesConcessionTarif($commonDonneesAnnuellesConcessionTarif)
    {
        $this->collCommonDonneesAnnuellesConcessionTarifs[]= $commonDonneesAnnuellesConcessionTarif;
        $commonDonneesAnnuellesConcessionTarif->setCommonDonneesAnnuellesConcession($this);
    }

    /**
     * @param	CommonDonneesAnnuellesConcessionTarif $commonDonneesAnnuellesConcessionTarif The commonDonneesAnnuellesConcessionTarif object to remove.
     * @return CommonDonneesAnnuellesConcession The current object (for fluent API support)
     */
    public function removeCommonDonneesAnnuellesConcessionTarif($commonDonneesAnnuellesConcessionTarif)
    {
        if ($this->getCommonDonneesAnnuellesConcessionTarifs()->contains($commonDonneesAnnuellesConcessionTarif)) {
            $this->collCommonDonneesAnnuellesConcessionTarifs->remove($this->collCommonDonneesAnnuellesConcessionTarifs->search($commonDonneesAnnuellesConcessionTarif));
            if (null === $this->commonDonneesAnnuellesConcessionTarifsScheduledForDeletion) {
                $this->commonDonneesAnnuellesConcessionTarifsScheduledForDeletion = clone $this->collCommonDonneesAnnuellesConcessionTarifs;
                $this->commonDonneesAnnuellesConcessionTarifsScheduledForDeletion->clear();
            }
            $this->commonDonneesAnnuellesConcessionTarifsScheduledForDeletion[]= clone $commonDonneesAnnuellesConcessionTarif;
            $commonDonneesAnnuellesConcessionTarif->setCommonDonneesAnnuellesConcession(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->id_contrat = null;
        $this->valeur_depense = null;
        $this->date_saisie = null;
        $this->num_ordre = null;
        $this->suivi_publication_sn = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonDonneesAnnuellesConcessionTarifs) {
                foreach ($this->collCommonDonneesAnnuellesConcessionTarifs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonTContratTitulaire instanceof Persistent) {
              $this->aCommonTContratTitulaire->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonDonneesAnnuellesConcessionTarifs instanceof PropelCollection) {
            $this->collCommonDonneesAnnuellesConcessionTarifs->clearIterator();
        }
        $this->collCommonDonneesAnnuellesConcessionTarifs = null;
        $this->aCommonTContratTitulaire = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonDonneesAnnuellesConcessionPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
