<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTProcedureEquivalenceDume;
use Application\Propel\Mpe\CommonTProcedureEquivalenceDumePeer;
use Application\Propel\Mpe\CommonTProcedureEquivalenceDumeQuery;

/**
 * Base class that represents a query for the 't_procedure_equivalence_dume' table.
 *
 *
 *
 * @method CommonTProcedureEquivalenceDumeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTProcedureEquivalenceDumeQuery orderByIdTypeProcedure($order = Criteria::ASC) Order by the id_type_procedure column
 * @method CommonTProcedureEquivalenceDumeQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTProcedureEquivalenceDumeQuery orderByIdTypeProcedureDume($order = Criteria::ASC) Order by the id_type_procedure_dume column
 * @method CommonTProcedureEquivalenceDumeQuery orderByAfficher($order = Criteria::ASC) Order by the afficher column
 * @method CommonTProcedureEquivalenceDumeQuery orderByFiger($order = Criteria::ASC) Order by the figer column
 * @method CommonTProcedureEquivalenceDumeQuery orderBySelectionner($order = Criteria::ASC) Order by the selectionner column
 *
 * @method CommonTProcedureEquivalenceDumeQuery groupById() Group by the id column
 * @method CommonTProcedureEquivalenceDumeQuery groupByIdTypeProcedure() Group by the id_type_procedure column
 * @method CommonTProcedureEquivalenceDumeQuery groupByOrganisme() Group by the organisme column
 * @method CommonTProcedureEquivalenceDumeQuery groupByIdTypeProcedureDume() Group by the id_type_procedure_dume column
 * @method CommonTProcedureEquivalenceDumeQuery groupByAfficher() Group by the afficher column
 * @method CommonTProcedureEquivalenceDumeQuery groupByFiger() Group by the figer column
 * @method CommonTProcedureEquivalenceDumeQuery groupBySelectionner() Group by the selectionner column
 *
 * @method CommonTProcedureEquivalenceDumeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTProcedureEquivalenceDumeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTProcedureEquivalenceDumeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTProcedureEquivalenceDume findOne(PropelPDO $con = null) Return the first CommonTProcedureEquivalenceDume matching the query
 * @method CommonTProcedureEquivalenceDume findOneOrCreate(PropelPDO $con = null) Return the first CommonTProcedureEquivalenceDume matching the query, or a new CommonTProcedureEquivalenceDume object populated from the query conditions when no match is found
 *
 * @method CommonTProcedureEquivalenceDume findOneByIdTypeProcedure(int $id_type_procedure) Return the first CommonTProcedureEquivalenceDume filtered by the id_type_procedure column
 * @method CommonTProcedureEquivalenceDume findOneByOrganisme(string $organisme) Return the first CommonTProcedureEquivalenceDume filtered by the organisme column
 * @method CommonTProcedureEquivalenceDume findOneByIdTypeProcedureDume(int $id_type_procedure_dume) Return the first CommonTProcedureEquivalenceDume filtered by the id_type_procedure_dume column
 * @method CommonTProcedureEquivalenceDume findOneByAfficher(string $afficher) Return the first CommonTProcedureEquivalenceDume filtered by the afficher column
 * @method CommonTProcedureEquivalenceDume findOneByFiger(string $figer) Return the first CommonTProcedureEquivalenceDume filtered by the figer column
 * @method CommonTProcedureEquivalenceDume findOneBySelectionner(string $selectionner) Return the first CommonTProcedureEquivalenceDume filtered by the selectionner column
 *
 * @method array findById(int $id) Return CommonTProcedureEquivalenceDume objects filtered by the id column
 * @method array findByIdTypeProcedure(int $id_type_procedure) Return CommonTProcedureEquivalenceDume objects filtered by the id_type_procedure column
 * @method array findByOrganisme(string $organisme) Return CommonTProcedureEquivalenceDume objects filtered by the organisme column
 * @method array findByIdTypeProcedureDume(int $id_type_procedure_dume) Return CommonTProcedureEquivalenceDume objects filtered by the id_type_procedure_dume column
 * @method array findByAfficher(string $afficher) Return CommonTProcedureEquivalenceDume objects filtered by the afficher column
 * @method array findByFiger(string $figer) Return CommonTProcedureEquivalenceDume objects filtered by the figer column
 * @method array findBySelectionner(string $selectionner) Return CommonTProcedureEquivalenceDume objects filtered by the selectionner column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTProcedureEquivalenceDumeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTProcedureEquivalenceDumeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTProcedureEquivalenceDume', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTProcedureEquivalenceDumeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTProcedureEquivalenceDumeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTProcedureEquivalenceDumeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTProcedureEquivalenceDumeQuery) {
            return $criteria;
        }
        $query = new CommonTProcedureEquivalenceDumeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTProcedureEquivalenceDume|CommonTProcedureEquivalenceDume[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTProcedureEquivalenceDumePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTProcedureEquivalenceDumePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTProcedureEquivalenceDume A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTProcedureEquivalenceDume A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_type_procedure`, `organisme`, `id_type_procedure_dume`, `afficher`, `figer`, `selectionner` FROM `t_procedure_equivalence_dume` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTProcedureEquivalenceDume();
            $obj->hydrate($row);
            CommonTProcedureEquivalenceDumePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTProcedureEquivalenceDume|CommonTProcedureEquivalenceDume[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTProcedureEquivalenceDume[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_type_procedure column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeProcedure(1234); // WHERE id_type_procedure = 1234
     * $query->filterByIdTypeProcedure(array(12, 34)); // WHERE id_type_procedure IN (12, 34)
     * $query->filterByIdTypeProcedure(array('min' => 12)); // WHERE id_type_procedure >= 12
     * $query->filterByIdTypeProcedure(array('max' => 12)); // WHERE id_type_procedure <= 12
     * </code>
     *
     * @param     mixed $idTypeProcedure The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function filterByIdTypeProcedure($idTypeProcedure = null, $comparison = null)
    {
        if (is_array($idTypeProcedure)) {
            $useMinMax = false;
            if (isset($idTypeProcedure['min'])) {
                $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID_TYPE_PROCEDURE, $idTypeProcedure['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeProcedure['max'])) {
                $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID_TYPE_PROCEDURE, $idTypeProcedure['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID_TYPE_PROCEDURE, $idTypeProcedure, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the id_type_procedure_dume column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeProcedureDume(1234); // WHERE id_type_procedure_dume = 1234
     * $query->filterByIdTypeProcedureDume(array(12, 34)); // WHERE id_type_procedure_dume IN (12, 34)
     * $query->filterByIdTypeProcedureDume(array('min' => 12)); // WHERE id_type_procedure_dume >= 12
     * $query->filterByIdTypeProcedureDume(array('max' => 12)); // WHERE id_type_procedure_dume <= 12
     * </code>
     *
     * @param     mixed $idTypeProcedureDume The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function filterByIdTypeProcedureDume($idTypeProcedureDume = null, $comparison = null)
    {
        if (is_array($idTypeProcedureDume)) {
            $useMinMax = false;
            if (isset($idTypeProcedureDume['min'])) {
                $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID_TYPE_PROCEDURE_DUME, $idTypeProcedureDume['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeProcedureDume['max'])) {
                $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID_TYPE_PROCEDURE_DUME, $idTypeProcedureDume['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID_TYPE_PROCEDURE_DUME, $idTypeProcedureDume, $comparison);
    }

    /**
     * Filter the query on the afficher column
     *
     * Example usage:
     * <code>
     * $query->filterByAfficher('fooValue');   // WHERE afficher = 'fooValue'
     * $query->filterByAfficher('%fooValue%'); // WHERE afficher LIKE '%fooValue%'
     * </code>
     *
     * @param     string $afficher The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function filterByAfficher($afficher = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($afficher)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $afficher)) {
                $afficher = str_replace('*', '%', $afficher);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::AFFICHER, $afficher, $comparison);
    }

    /**
     * Filter the query on the figer column
     *
     * Example usage:
     * <code>
     * $query->filterByFiger('fooValue');   // WHERE figer = 'fooValue'
     * $query->filterByFiger('%fooValue%'); // WHERE figer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $figer The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function filterByFiger($figer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($figer)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $figer)) {
                $figer = str_replace('*', '%', $figer);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::FIGER, $figer, $comparison);
    }

    /**
     * Filter the query on the selectionner column
     *
     * Example usage:
     * <code>
     * $query->filterBySelectionner('fooValue');   // WHERE selectionner = 'fooValue'
     * $query->filterBySelectionner('%fooValue%'); // WHERE selectionner LIKE '%fooValue%'
     * </code>
     *
     * @param     string $selectionner The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function filterBySelectionner($selectionner = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($selectionner)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $selectionner)) {
                $selectionner = str_replace('*', '%', $selectionner);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::SELECTIONNER, $selectionner, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTProcedureEquivalenceDume $commonTProcedureEquivalenceDume Object to remove from the list of results
     *
     * @return CommonTProcedureEquivalenceDumeQuery The current query, for fluid interface
     */
    public function prune($commonTProcedureEquivalenceDume = null)
    {
        if ($commonTProcedureEquivalenceDume) {
            $this->addUsingAlias(CommonTProcedureEquivalenceDumePeer::ID, $commonTProcedureEquivalenceDume->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
