<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonJmsJobDependencies;
use Application\Propel\Mpe\CommonJmsJobDependenciesQuery;
use Application\Propel\Mpe\CommonJmsJobRelatedEntities;
use Application\Propel\Mpe\CommonJmsJobRelatedEntitiesQuery;
use Application\Propel\Mpe\CommonJmsJobs;
use Application\Propel\Mpe\CommonJmsJobsPeer;
use Application\Propel\Mpe\CommonJmsJobsQuery;

/**
 * Base class that represents a row from the 'jms_jobs' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonJmsJobs extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonJmsJobsPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonJmsJobsPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        string
     */
    protected $id;

    /**
     * The value for the state field.
     * @var        string
     */
    protected $state;

    /**
     * The value for the queue field.
     * @var        string
     */
    protected $queue;

    /**
     * The value for the priority field.
     * @var        int
     */
    protected $priority;

    /**
     * The value for the createdat field.
     * @var        string
     */
    protected $createdat;

    /**
     * The value for the startedat field.
     * @var        string
     */
    protected $startedat;

    /**
     * The value for the checkedat field.
     * @var        string
     */
    protected $checkedat;

    /**
     * The value for the workername field.
     * @var        string
     */
    protected $workername;

    /**
     * The value for the executeafter field.
     * @var        string
     */
    protected $executeafter;

    /**
     * The value for the closedat field.
     * @var        string
     */
    protected $closedat;

    /**
     * The value for the command field.
     * @var        string
     */
    protected $command;

    /**
     * The value for the args field.
     * @var        string
     */
    protected $args;

    /**
     * The value for the output field.
     * @var        string
     */
    protected $output;

    /**
     * The value for the erroroutput field.
     * @var        string
     */
    protected $erroroutput;

    /**
     * The value for the exitcode field.
     * @var        int
     */
    protected $exitcode;

    /**
     * The value for the maxruntime field.
     * @var        int
     */
    protected $maxruntime;

    /**
     * The value for the maxretries field.
     * @var        int
     */
    protected $maxretries;

    /**
     * The value for the stacktrace field.
     * @var        resource
     */
    protected $stacktrace;

    /**
     * The value for the runtime field.
     * @var        int
     */
    protected $runtime;

    /**
     * The value for the memoryusage field.
     * @var        int
     */
    protected $memoryusage;

    /**
     * The value for the memoryusagereal field.
     * @var        int
     */
    protected $memoryusagereal;

    /**
     * The value for the originaljob_id field.
     * @var        string
     */
    protected $originaljob_id;

    /**
     * @var        CommonJmsJobs
     */
    protected $aCommonJmsJobsRelatedByOriginaljobId;

    /**
     * @var        PropelObjectCollection|CommonJmsJobDependencies[] Collection to store aggregation of CommonJmsJobDependencies objects.
     */
    protected $collCommonJmsJobDependenciessRelatedByDestJobId;
    protected $collCommonJmsJobDependenciessRelatedByDestJobIdPartial;

    /**
     * @var        PropelObjectCollection|CommonJmsJobDependencies[] Collection to store aggregation of CommonJmsJobDependencies objects.
     */
    protected $collCommonJmsJobDependenciessRelatedBySourceJobId;
    protected $collCommonJmsJobDependenciessRelatedBySourceJobIdPartial;

    /**
     * @var        PropelObjectCollection|CommonJmsJobRelatedEntities[] Collection to store aggregation of CommonJmsJobRelatedEntities objects.
     */
    protected $collCommonJmsJobRelatedEntitiess;
    protected $collCommonJmsJobRelatedEntitiessPartial;

    /**
     * @var        PropelObjectCollection|CommonJmsJobs[] Collection to store aggregation of CommonJmsJobs objects.
     */
    protected $collCommonJmsJobssRelatedById;
    protected $collCommonJmsJobssRelatedByIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonJmsJobRelatedEntitiessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonJmsJobssRelatedByIdScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [state] column value.
     *
     * @return string
     */
    public function getState()
    {

        return $this->state;
    }

    /**
     * Get the [queue] column value.
     *
     * @return string
     */
    public function getQueue()
    {

        return $this->queue;
    }

    /**
     * Get the [priority] column value.
     *
     * @return int
     */
    public function getPriority()
    {

        return $this->priority;
    }

    /**
     * Get the [optionally formatted] temporal [createdat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedat($format = 'Y-m-d H:i:s')
    {
        if ($this->createdat === null) {
            return null;
        }

        if ($this->createdat === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->createdat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->createdat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [startedat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStartedat($format = 'Y-m-d H:i:s')
    {
        if ($this->startedat === null) {
            return null;
        }

        if ($this->startedat === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->startedat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->startedat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [checkedat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCheckedat($format = 'Y-m-d H:i:s')
    {
        if ($this->checkedat === null) {
            return null;
        }

        if ($this->checkedat === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->checkedat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->checkedat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [workername] column value.
     *
     * @return string
     */
    public function getWorkername()
    {

        return $this->workername;
    }

    /**
     * Get the [optionally formatted] temporal [executeafter] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExecuteafter($format = 'Y-m-d H:i:s')
    {
        if ($this->executeafter === null) {
            return null;
        }

        if ($this->executeafter === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->executeafter);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->executeafter, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [closedat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getClosedat($format = 'Y-m-d H:i:s')
    {
        if ($this->closedat === null) {
            return null;
        }

        if ($this->closedat === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->closedat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->closedat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [command] column value.
     *
     * @return string
     */
    public function getCommand()
    {

        return $this->command;
    }

    /**
     * Get the [args] column value.
     *
     * @return string
     */
    public function getArgs()
    {

        return $this->args;
    }

    /**
     * Get the [output] column value.
     *
     * @return string
     */
    public function getOutput()
    {

        return $this->output;
    }

    /**
     * Get the [erroroutput] column value.
     *
     * @return string
     */
    public function getErroroutput()
    {

        return $this->erroroutput;
    }

    /**
     * Get the [exitcode] column value.
     *
     * @return int
     */
    public function getExitcode()
    {

        return $this->exitcode;
    }

    /**
     * Get the [maxruntime] column value.
     *
     * @return int
     */
    public function getMaxruntime()
    {

        return $this->maxruntime;
    }

    /**
     * Get the [maxretries] column value.
     *
     * @return int
     */
    public function getMaxretries()
    {

        return $this->maxretries;
    }

    /**
     * Get the [stacktrace] column value.
     *
     * @return resource
     */
    public function getStacktrace()
    {

        return $this->stacktrace;
    }

    /**
     * Get the [runtime] column value.
     *
     * @return int
     */
    public function getRuntime()
    {

        return $this->runtime;
    }

    /**
     * Get the [memoryusage] column value.
     *
     * @return int
     */
    public function getMemoryusage()
    {

        return $this->memoryusage;
    }

    /**
     * Get the [memoryusagereal] column value.
     *
     * @return int
     */
    public function getMemoryusagereal()
    {

        return $this->memoryusagereal;
    }

    /**
     * Get the [originaljob_id] column value.
     *
     * @return string
     */
    public function getOriginaljobId()
    {

        return $this->originaljob_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [state] column.
     *
     * @param string $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::STATE;
        }


        return $this;
    } // setState()

    /**
     * Set the value of [queue] column.
     *
     * @param string $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setQueue($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->queue !== $v) {
            $this->queue = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::QUEUE;
        }


        return $this;
    } // setQueue()

    /**
     * Set the value of [priority] column.
     *
     * @param int $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setPriority($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->priority !== $v) {
            $this->priority = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::PRIORITY;
        }


        return $this;
    } // setPriority()

    /**
     * Sets the value of [createdat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setCreatedat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->createdat !== null || $dt !== null) {
            $currentDateAsString = ($this->createdat !== null && $tmpDt = new DateTime($this->createdat)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->createdat = $newDateAsString;
                $this->modifiedColumns[] = CommonJmsJobsPeer::CREATEDAT;
            }
        } // if either are not null


        return $this;
    } // setCreatedat()

    /**
     * Sets the value of [startedat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setStartedat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->startedat !== null || $dt !== null) {
            $currentDateAsString = ($this->startedat !== null && $tmpDt = new DateTime($this->startedat)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->startedat = $newDateAsString;
                $this->modifiedColumns[] = CommonJmsJobsPeer::STARTEDAT;
            }
        } // if either are not null


        return $this;
    } // setStartedat()

    /**
     * Sets the value of [checkedat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setCheckedat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->checkedat !== null || $dt !== null) {
            $currentDateAsString = ($this->checkedat !== null && $tmpDt = new DateTime($this->checkedat)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->checkedat = $newDateAsString;
                $this->modifiedColumns[] = CommonJmsJobsPeer::CHECKEDAT;
            }
        } // if either are not null


        return $this;
    } // setCheckedat()

    /**
     * Set the value of [workername] column.
     *
     * @param string $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setWorkername($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->workername !== $v) {
            $this->workername = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::WORKERNAME;
        }


        return $this;
    } // setWorkername()

    /**
     * Sets the value of [executeafter] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setExecuteafter($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->executeafter !== null || $dt !== null) {
            $currentDateAsString = ($this->executeafter !== null && $tmpDt = new DateTime($this->executeafter)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->executeafter = $newDateAsString;
                $this->modifiedColumns[] = CommonJmsJobsPeer::EXECUTEAFTER;
            }
        } // if either are not null


        return $this;
    } // setExecuteafter()

    /**
     * Sets the value of [closedat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setClosedat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->closedat !== null || $dt !== null) {
            $currentDateAsString = ($this->closedat !== null && $tmpDt = new DateTime($this->closedat)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->closedat = $newDateAsString;
                $this->modifiedColumns[] = CommonJmsJobsPeer::CLOSEDAT;
            }
        } // if either are not null


        return $this;
    } // setClosedat()

    /**
     * Set the value of [command] column.
     *
     * @param string $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setCommand($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->command !== $v) {
            $this->command = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::COMMAND;
        }


        return $this;
    } // setCommand()

    /**
     * Set the value of [args] column.
     *
     * @param string $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setArgs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->args !== $v) {
            $this->args = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::ARGS;
        }


        return $this;
    } // setArgs()

    /**
     * Set the value of [output] column.
     *
     * @param string $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setOutput($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->output !== $v) {
            $this->output = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::OUTPUT;
        }


        return $this;
    } // setOutput()

    /**
     * Set the value of [erroroutput] column.
     *
     * @param string $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setErroroutput($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->erroroutput !== $v) {
            $this->erroroutput = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::ERROROUTPUT;
        }


        return $this;
    } // setErroroutput()

    /**
     * Set the value of [exitcode] column.
     *
     * @param int $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setExitcode($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->exitcode !== $v) {
            $this->exitcode = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::EXITCODE;
        }


        return $this;
    } // setExitcode()

    /**
     * Set the value of [maxruntime] column.
     *
     * @param int $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setMaxruntime($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->maxruntime !== $v) {
            $this->maxruntime = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::MAXRUNTIME;
        }


        return $this;
    } // setMaxruntime()

    /**
     * Set the value of [maxretries] column.
     *
     * @param int $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setMaxretries($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->maxretries !== $v) {
            $this->maxretries = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::MAXRETRIES;
        }


        return $this;
    } // setMaxretries()

    /**
     * Set the value of [stacktrace] column.
     *
     * @param resource $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setStacktrace($v)
    {
        // Because BLOB columns are streams in PDO we have to assume that they are
        // always modified when a new value is passed in.  For example, the contents
        // of the stream itself may have changed externally.
        if (!is_resource($v) && $v !== null) {
            $this->stacktrace = fopen('php://memory', 'r+');
            fwrite($this->stacktrace, $v);
            rewind($this->stacktrace);
        } else { // it's already a stream
            $this->stacktrace = $v;
        }
        $this->modifiedColumns[] = CommonJmsJobsPeer::STACKTRACE;


        return $this;
    } // setStacktrace()

    /**
     * Set the value of [runtime] column.
     *
     * @param int $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setRuntime($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->runtime !== $v) {
            $this->runtime = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::RUNTIME;
        }


        return $this;
    } // setRuntime()

    /**
     * Set the value of [memoryusage] column.
     *
     * @param int $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setMemoryusage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->memoryusage !== $v) {
            $this->memoryusage = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::MEMORYUSAGE;
        }


        return $this;
    } // setMemoryusage()

    /**
     * Set the value of [memoryusagereal] column.
     *
     * @param int $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setMemoryusagereal($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->memoryusagereal !== $v) {
            $this->memoryusagereal = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::MEMORYUSAGEREAL;
        }


        return $this;
    } // setMemoryusagereal()

    /**
     * Set the value of [originaljob_id] column.
     *
     * @param string $v new value
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setOriginaljobId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->originaljob_id !== $v) {
            $this->originaljob_id = $v;
            $this->modifiedColumns[] = CommonJmsJobsPeer::ORIGINALJOB_ID;
        }

        if ($this->aCommonJmsJobsRelatedByOriginaljobId !== null && $this->aCommonJmsJobsRelatedByOriginaljobId->getId() !== $v) {
            $this->aCommonJmsJobsRelatedByOriginaljobId = null;
        }


        return $this;
    } // setOriginaljobId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->state = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->queue = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->priority = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->createdat = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->startedat = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->checkedat = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->workername = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->executeafter = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->closedat = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->command = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->args = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->output = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->erroroutput = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->exitcode = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->maxruntime = ($row[$startcol + 15] !== null) ? (int) $row[$startcol + 15] : null;
            $this->maxretries = ($row[$startcol + 16] !== null) ? (int) $row[$startcol + 16] : null;
            if ($row[$startcol + 17] !== null) {
                $this->stacktrace = fopen('php://memory', 'r+');
                fwrite($this->stacktrace, $row[$startcol + 17]);
                rewind($this->stacktrace);
            } else {
                $this->stacktrace = null;
            }
            $this->runtime = ($row[$startcol + 18] !== null) ? (int) $row[$startcol + 18] : null;
            $this->memoryusage = ($row[$startcol + 19] !== null) ? (int) $row[$startcol + 19] : null;
            $this->memoryusagereal = ($row[$startcol + 20] !== null) ? (int) $row[$startcol + 20] : null;
            $this->originaljob_id = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 22; // 22 = CommonJmsJobsPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonJmsJobs object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonJmsJobsRelatedByOriginaljobId !== null && $this->originaljob_id !== $this->aCommonJmsJobsRelatedByOriginaljobId->getId()) {
            $this->aCommonJmsJobsRelatedByOriginaljobId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonJmsJobsPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonJmsJobsRelatedByOriginaljobId = null;
            $this->collCommonJmsJobDependenciessRelatedByDestJobId = null;

            $this->collCommonJmsJobDependenciessRelatedBySourceJobId = null;

            $this->collCommonJmsJobRelatedEntitiess = null;

            $this->collCommonJmsJobssRelatedById = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonJmsJobsQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonJmsJobsPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonJmsJobsRelatedByOriginaljobId !== null) {
                if ($this->aCommonJmsJobsRelatedByOriginaljobId->isModified() || $this->aCommonJmsJobsRelatedByOriginaljobId->isNew()) {
                    $affectedRows += $this->aCommonJmsJobsRelatedByOriginaljobId->save($con);
                }
                $this->setCommonJmsJobsRelatedByOriginaljobId($this->aCommonJmsJobsRelatedByOriginaljobId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                // Rewind the stacktrace LOB column, since PDO does not rewind after inserting value.
                if ($this->stacktrace !== null && is_resource($this->stacktrace)) {
                    rewind($this->stacktrace);
                }

                $this->resetModified();
            }

            if ($this->commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion !== null) {
                if (!$this->commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion as $commonJmsJobDependenciesRelatedByDestJobId) {
                        // need to save related object because we set the relation to null
                        $commonJmsJobDependenciesRelatedByDestJobId->save($con);
                    }
                    $this->commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion = null;
                }
            }

            if ($this->collCommonJmsJobDependenciessRelatedByDestJobId !== null) {
                foreach ($this->collCommonJmsJobDependenciessRelatedByDestJobId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion !== null) {
                if (!$this->commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion as $commonJmsJobDependenciesRelatedBySourceJobId) {
                        // need to save related object because we set the relation to null
                        $commonJmsJobDependenciesRelatedBySourceJobId->save($con);
                    }
                    $this->commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion = null;
                }
            }

            if ($this->collCommonJmsJobDependenciessRelatedBySourceJobId !== null) {
                foreach ($this->collCommonJmsJobDependenciessRelatedBySourceJobId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonJmsJobRelatedEntitiessScheduledForDeletion !== null) {
                if (!$this->commonJmsJobRelatedEntitiessScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonJmsJobRelatedEntitiessScheduledForDeletion as $commonJmsJobRelatedEntities) {
                        // need to save related object because we set the relation to null
                        $commonJmsJobRelatedEntities->save($con);
                    }
                    $this->commonJmsJobRelatedEntitiessScheduledForDeletion = null;
                }
            }

            if ($this->collCommonJmsJobRelatedEntitiess !== null) {
                foreach ($this->collCommonJmsJobRelatedEntitiess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonJmsJobssRelatedByIdScheduledForDeletion !== null) {
                if (!$this->commonJmsJobssRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonJmsJobssRelatedByIdScheduledForDeletion as $commonJmsJobsRelatedById) {
                        // need to save related object because we set the relation to null
                        $commonJmsJobsRelatedById->save($con);
                    }
                    $this->commonJmsJobssRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collCommonJmsJobssRelatedById !== null) {
                foreach ($this->collCommonJmsJobssRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonJmsJobsPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonJmsJobsPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonJmsJobsPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::STATE)) {
            $modifiedColumns[':p' . $index++]  = '`state`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::QUEUE)) {
            $modifiedColumns[':p' . $index++]  = '`queue`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::PRIORITY)) {
            $modifiedColumns[':p' . $index++]  = '`priority`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::CREATEDAT)) {
            $modifiedColumns[':p' . $index++]  = '`createdAt`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::STARTEDAT)) {
            $modifiedColumns[':p' . $index++]  = '`startedAt`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::CHECKEDAT)) {
            $modifiedColumns[':p' . $index++]  = '`checkedAt`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::WORKERNAME)) {
            $modifiedColumns[':p' . $index++]  = '`workerName`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::EXECUTEAFTER)) {
            $modifiedColumns[':p' . $index++]  = '`executeAfter`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::CLOSEDAT)) {
            $modifiedColumns[':p' . $index++]  = '`closedAt`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::COMMAND)) {
            $modifiedColumns[':p' . $index++]  = '`command`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::ARGS)) {
            $modifiedColumns[':p' . $index++]  = '`args`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::OUTPUT)) {
            $modifiedColumns[':p' . $index++]  = '`output`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::ERROROUTPUT)) {
            $modifiedColumns[':p' . $index++]  = '`errorOutput`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::EXITCODE)) {
            $modifiedColumns[':p' . $index++]  = '`exitCode`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::MAXRUNTIME)) {
            $modifiedColumns[':p' . $index++]  = '`maxRuntime`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::MAXRETRIES)) {
            $modifiedColumns[':p' . $index++]  = '`maxRetries`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::STACKTRACE)) {
            $modifiedColumns[':p' . $index++]  = '`stackTrace`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::RUNTIME)) {
            $modifiedColumns[':p' . $index++]  = '`runtime`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::MEMORYUSAGE)) {
            $modifiedColumns[':p' . $index++]  = '`memoryUsage`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::MEMORYUSAGEREAL)) {
            $modifiedColumns[':p' . $index++]  = '`memoryUsageReal`';
        }
        if ($this->isColumnModified(CommonJmsJobsPeer::ORIGINALJOB_ID)) {
            $modifiedColumns[':p' . $index++]  = '`originalJob_id`';
        }

        $sql = sprintf(
            'INSERT INTO `jms_jobs` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                    case '`state`':
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_STR);
                        break;
                    case '`queue`':
                        $stmt->bindValue($identifier, $this->queue, PDO::PARAM_STR);
                        break;
                    case '`priority`':
                        $stmt->bindValue($identifier, $this->priority, PDO::PARAM_INT);
                        break;
                    case '`createdAt`':
                        $stmt->bindValue($identifier, $this->createdat, PDO::PARAM_STR);
                        break;
                    case '`startedAt`':
                        $stmt->bindValue($identifier, $this->startedat, PDO::PARAM_STR);
                        break;
                    case '`checkedAt`':
                        $stmt->bindValue($identifier, $this->checkedat, PDO::PARAM_STR);
                        break;
                    case '`workerName`':
                        $stmt->bindValue($identifier, $this->workername, PDO::PARAM_STR);
                        break;
                    case '`executeAfter`':
                        $stmt->bindValue($identifier, $this->executeafter, PDO::PARAM_STR);
                        break;
                    case '`closedAt`':
                        $stmt->bindValue($identifier, $this->closedat, PDO::PARAM_STR);
                        break;
                    case '`command`':
                        $stmt->bindValue($identifier, $this->command, PDO::PARAM_STR);
                        break;
                    case '`args`':
                        $stmt->bindValue($identifier, $this->args, PDO::PARAM_STR);
                        break;
                    case '`output`':
                        $stmt->bindValue($identifier, $this->output, PDO::PARAM_STR);
                        break;
                    case '`errorOutput`':
                        $stmt->bindValue($identifier, $this->erroroutput, PDO::PARAM_STR);
                        break;
                    case '`exitCode`':
                        $stmt->bindValue($identifier, $this->exitcode, PDO::PARAM_INT);
                        break;
                    case '`maxRuntime`':
                        $stmt->bindValue($identifier, $this->maxruntime, PDO::PARAM_INT);
                        break;
                    case '`maxRetries`':
                        $stmt->bindValue($identifier, $this->maxretries, PDO::PARAM_INT);
                        break;
                    case '`stackTrace`':
                        if (is_resource($this->stacktrace)) {
                            rewind($this->stacktrace);
                        }
                        $stmt->bindValue($identifier, $this->stacktrace, PDO::PARAM_LOB);
                        break;
                    case '`runtime`':
                        $stmt->bindValue($identifier, $this->runtime, PDO::PARAM_INT);
                        break;
                    case '`memoryUsage`':
                        $stmt->bindValue($identifier, $this->memoryusage, PDO::PARAM_INT);
                        break;
                    case '`memoryUsageReal`':
                        $stmt->bindValue($identifier, $this->memoryusagereal, PDO::PARAM_INT);
                        break;
                    case '`originalJob_id`':
                        $stmt->bindValue($identifier, $this->originaljob_id, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonJmsJobsRelatedByOriginaljobId !== null) {
                if (!$this->aCommonJmsJobsRelatedByOriginaljobId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonJmsJobsRelatedByOriginaljobId->getValidationFailures());
                }
            }


            if (($retval = CommonJmsJobsPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonJmsJobDependenciessRelatedByDestJobId !== null) {
                    foreach ($this->collCommonJmsJobDependenciessRelatedByDestJobId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonJmsJobDependenciessRelatedBySourceJobId !== null) {
                    foreach ($this->collCommonJmsJobDependenciessRelatedBySourceJobId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonJmsJobRelatedEntitiess !== null) {
                    foreach ($this->collCommonJmsJobRelatedEntitiess as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonJmsJobssRelatedById !== null) {
                    foreach ($this->collCommonJmsJobssRelatedById as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonJmsJobsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getState();
                break;
            case 2:
                return $this->getQueue();
                break;
            case 3:
                return $this->getPriority();
                break;
            case 4:
                return $this->getCreatedat();
                break;
            case 5:
                return $this->getStartedat();
                break;
            case 6:
                return $this->getCheckedat();
                break;
            case 7:
                return $this->getWorkername();
                break;
            case 8:
                return $this->getExecuteafter();
                break;
            case 9:
                return $this->getClosedat();
                break;
            case 10:
                return $this->getCommand();
                break;
            case 11:
                return $this->getArgs();
                break;
            case 12:
                return $this->getOutput();
                break;
            case 13:
                return $this->getErroroutput();
                break;
            case 14:
                return $this->getExitcode();
                break;
            case 15:
                return $this->getMaxruntime();
                break;
            case 16:
                return $this->getMaxretries();
                break;
            case 17:
                return $this->getStacktrace();
                break;
            case 18:
                return $this->getRuntime();
                break;
            case 19:
                return $this->getMemoryusage();
                break;
            case 20:
                return $this->getMemoryusagereal();
                break;
            case 21:
                return $this->getOriginaljobId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonJmsJobs'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonJmsJobs'][$this->getPrimaryKey()] = true;
        $keys = CommonJmsJobsPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getState(),
            $keys[2] => $this->getQueue(),
            $keys[3] => $this->getPriority(),
            $keys[4] => $this->getCreatedat(),
            $keys[5] => $this->getStartedat(),
            $keys[6] => $this->getCheckedat(),
            $keys[7] => $this->getWorkername(),
            $keys[8] => $this->getExecuteafter(),
            $keys[9] => $this->getClosedat(),
            $keys[10] => $this->getCommand(),
            $keys[11] => $this->getArgs(),
            $keys[12] => $this->getOutput(),
            $keys[13] => $this->getErroroutput(),
            $keys[14] => $this->getExitcode(),
            $keys[15] => $this->getMaxruntime(),
            $keys[16] => $this->getMaxretries(),
            $keys[17] => $this->getStacktrace(),
            $keys[18] => $this->getRuntime(),
            $keys[19] => $this->getMemoryusage(),
            $keys[20] => $this->getMemoryusagereal(),
            $keys[21] => $this->getOriginaljobId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonJmsJobsRelatedByOriginaljobId) {
                $result['CommonJmsJobsRelatedByOriginaljobId'] = $this->aCommonJmsJobsRelatedByOriginaljobId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonJmsJobDependenciessRelatedByDestJobId) {
                $result['CommonJmsJobDependenciessRelatedByDestJobId'] = $this->collCommonJmsJobDependenciessRelatedByDestJobId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonJmsJobDependenciessRelatedBySourceJobId) {
                $result['CommonJmsJobDependenciessRelatedBySourceJobId'] = $this->collCommonJmsJobDependenciessRelatedBySourceJobId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonJmsJobRelatedEntitiess) {
                $result['CommonJmsJobRelatedEntitiess'] = $this->collCommonJmsJobRelatedEntitiess->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonJmsJobssRelatedById) {
                $result['CommonJmsJobssRelatedById'] = $this->collCommonJmsJobssRelatedById->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonJmsJobsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setState($value);
                break;
            case 2:
                $this->setQueue($value);
                break;
            case 3:
                $this->setPriority($value);
                break;
            case 4:
                $this->setCreatedat($value);
                break;
            case 5:
                $this->setStartedat($value);
                break;
            case 6:
                $this->setCheckedat($value);
                break;
            case 7:
                $this->setWorkername($value);
                break;
            case 8:
                $this->setExecuteafter($value);
                break;
            case 9:
                $this->setClosedat($value);
                break;
            case 10:
                $this->setCommand($value);
                break;
            case 11:
                $this->setArgs($value);
                break;
            case 12:
                $this->setOutput($value);
                break;
            case 13:
                $this->setErroroutput($value);
                break;
            case 14:
                $this->setExitcode($value);
                break;
            case 15:
                $this->setMaxruntime($value);
                break;
            case 16:
                $this->setMaxretries($value);
                break;
            case 17:
                $this->setStacktrace($value);
                break;
            case 18:
                $this->setRuntime($value);
                break;
            case 19:
                $this->setMemoryusage($value);
                break;
            case 20:
                $this->setMemoryusagereal($value);
                break;
            case 21:
                $this->setOriginaljobId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonJmsJobsPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setState($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setQueue($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPriority($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setCreatedat($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setStartedat($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCheckedat($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setWorkername($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setExecuteafter($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setClosedat($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setCommand($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setArgs($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setOutput($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setErroroutput($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setExitcode($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setMaxruntime($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setMaxretries($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setStacktrace($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setRuntime($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setMemoryusage($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setMemoryusagereal($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setOriginaljobId($arr[$keys[21]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonJmsJobsPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonJmsJobsPeer::ID)) $criteria->add(CommonJmsJobsPeer::ID, $this->id);
        if ($this->isColumnModified(CommonJmsJobsPeer::STATE)) $criteria->add(CommonJmsJobsPeer::STATE, $this->state);
        if ($this->isColumnModified(CommonJmsJobsPeer::QUEUE)) $criteria->add(CommonJmsJobsPeer::QUEUE, $this->queue);
        if ($this->isColumnModified(CommonJmsJobsPeer::PRIORITY)) $criteria->add(CommonJmsJobsPeer::PRIORITY, $this->priority);
        if ($this->isColumnModified(CommonJmsJobsPeer::CREATEDAT)) $criteria->add(CommonJmsJobsPeer::CREATEDAT, $this->createdat);
        if ($this->isColumnModified(CommonJmsJobsPeer::STARTEDAT)) $criteria->add(CommonJmsJobsPeer::STARTEDAT, $this->startedat);
        if ($this->isColumnModified(CommonJmsJobsPeer::CHECKEDAT)) $criteria->add(CommonJmsJobsPeer::CHECKEDAT, $this->checkedat);
        if ($this->isColumnModified(CommonJmsJobsPeer::WORKERNAME)) $criteria->add(CommonJmsJobsPeer::WORKERNAME, $this->workername);
        if ($this->isColumnModified(CommonJmsJobsPeer::EXECUTEAFTER)) $criteria->add(CommonJmsJobsPeer::EXECUTEAFTER, $this->executeafter);
        if ($this->isColumnModified(CommonJmsJobsPeer::CLOSEDAT)) $criteria->add(CommonJmsJobsPeer::CLOSEDAT, $this->closedat);
        if ($this->isColumnModified(CommonJmsJobsPeer::COMMAND)) $criteria->add(CommonJmsJobsPeer::COMMAND, $this->command);
        if ($this->isColumnModified(CommonJmsJobsPeer::ARGS)) $criteria->add(CommonJmsJobsPeer::ARGS, $this->args);
        if ($this->isColumnModified(CommonJmsJobsPeer::OUTPUT)) $criteria->add(CommonJmsJobsPeer::OUTPUT, $this->output);
        if ($this->isColumnModified(CommonJmsJobsPeer::ERROROUTPUT)) $criteria->add(CommonJmsJobsPeer::ERROROUTPUT, $this->erroroutput);
        if ($this->isColumnModified(CommonJmsJobsPeer::EXITCODE)) $criteria->add(CommonJmsJobsPeer::EXITCODE, $this->exitcode);
        if ($this->isColumnModified(CommonJmsJobsPeer::MAXRUNTIME)) $criteria->add(CommonJmsJobsPeer::MAXRUNTIME, $this->maxruntime);
        if ($this->isColumnModified(CommonJmsJobsPeer::MAXRETRIES)) $criteria->add(CommonJmsJobsPeer::MAXRETRIES, $this->maxretries);
        if ($this->isColumnModified(CommonJmsJobsPeer::STACKTRACE)) $criteria->add(CommonJmsJobsPeer::STACKTRACE, $this->stacktrace);
        if ($this->isColumnModified(CommonJmsJobsPeer::RUNTIME)) $criteria->add(CommonJmsJobsPeer::RUNTIME, $this->runtime);
        if ($this->isColumnModified(CommonJmsJobsPeer::MEMORYUSAGE)) $criteria->add(CommonJmsJobsPeer::MEMORYUSAGE, $this->memoryusage);
        if ($this->isColumnModified(CommonJmsJobsPeer::MEMORYUSAGEREAL)) $criteria->add(CommonJmsJobsPeer::MEMORYUSAGEREAL, $this->memoryusagereal);
        if ($this->isColumnModified(CommonJmsJobsPeer::ORIGINALJOB_ID)) $criteria->add(CommonJmsJobsPeer::ORIGINALJOB_ID, $this->originaljob_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonJmsJobsPeer::DATABASE_NAME);
        $criteria->add(CommonJmsJobsPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonJmsJobs (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setState($this->getState());
        $copyObj->setQueue($this->getQueue());
        $copyObj->setPriority($this->getPriority());
        $copyObj->setCreatedat($this->getCreatedat());
        $copyObj->setStartedat($this->getStartedat());
        $copyObj->setCheckedat($this->getCheckedat());
        $copyObj->setWorkername($this->getWorkername());
        $copyObj->setExecuteafter($this->getExecuteafter());
        $copyObj->setClosedat($this->getClosedat());
        $copyObj->setCommand($this->getCommand());
        $copyObj->setArgs($this->getArgs());
        $copyObj->setOutput($this->getOutput());
        $copyObj->setErroroutput($this->getErroroutput());
        $copyObj->setExitcode($this->getExitcode());
        $copyObj->setMaxruntime($this->getMaxruntime());
        $copyObj->setMaxretries($this->getMaxretries());
        $copyObj->setStacktrace($this->getStacktrace());
        $copyObj->setRuntime($this->getRuntime());
        $copyObj->setMemoryusage($this->getMemoryusage());
        $copyObj->setMemoryusagereal($this->getMemoryusagereal());
        $copyObj->setOriginaljobId($this->getOriginaljobId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonJmsJobDependenciessRelatedByDestJobId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonJmsJobDependenciesRelatedByDestJobId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonJmsJobDependenciessRelatedBySourceJobId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonJmsJobDependenciesRelatedBySourceJobId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonJmsJobRelatedEntitiess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonJmsJobRelatedEntities($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonJmsJobssRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonJmsJobsRelatedById($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonJmsJobs Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonJmsJobsPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonJmsJobsPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonJmsJobs object.
     *
     * @param   CommonJmsJobs $v
     * @return CommonJmsJobs The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonJmsJobsRelatedByOriginaljobId(CommonJmsJobs $v = null)
    {
        if ($v === null) {
            $this->setOriginaljobId(NULL);
        } else {
            $this->setOriginaljobId($v->getId());
        }

        $this->aCommonJmsJobsRelatedByOriginaljobId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonJmsJobs object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonJmsJobsRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonJmsJobs object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonJmsJobs The associated CommonJmsJobs object.
     * @throws PropelException
     */
    public function getCommonJmsJobsRelatedByOriginaljobId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonJmsJobsRelatedByOriginaljobId === null && (($this->originaljob_id !== "" && $this->originaljob_id !== null)) && $doQuery) {
            $this->aCommonJmsJobsRelatedByOriginaljobId = CommonJmsJobsQuery::create()->findPk($this->originaljob_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonJmsJobsRelatedByOriginaljobId->addCommonJmsJobssRelatedById($this);
             */
        }

        return $this->aCommonJmsJobsRelatedByOriginaljobId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonJmsJobDependenciesRelatedByDestJobId' == $relationName) {
            $this->initCommonJmsJobDependenciessRelatedByDestJobId();
        }
        if ('CommonJmsJobDependenciesRelatedBySourceJobId' == $relationName) {
            $this->initCommonJmsJobDependenciessRelatedBySourceJobId();
        }
        if ('CommonJmsJobRelatedEntities' == $relationName) {
            $this->initCommonJmsJobRelatedEntitiess();
        }
        if ('CommonJmsJobsRelatedById' == $relationName) {
            $this->initCommonJmsJobssRelatedById();
        }
    }

    /**
     * Clears out the collCommonJmsJobDependenciessRelatedByDestJobId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonJmsJobs The current object (for fluent API support)
     * @see        addCommonJmsJobDependenciessRelatedByDestJobId()
     */
    public function clearCommonJmsJobDependenciessRelatedByDestJobId()
    {
        $this->collCommonJmsJobDependenciessRelatedByDestJobId = null; // important to set this to null since that means it is uninitialized
        $this->collCommonJmsJobDependenciessRelatedByDestJobIdPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonJmsJobDependenciessRelatedByDestJobId collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonJmsJobDependenciessRelatedByDestJobId($v = true)
    {
        $this->collCommonJmsJobDependenciessRelatedByDestJobIdPartial = $v;
    }

    /**
     * Initializes the collCommonJmsJobDependenciessRelatedByDestJobId collection.
     *
     * By default this just sets the collCommonJmsJobDependenciessRelatedByDestJobId collection to an empty array (like clearcollCommonJmsJobDependenciessRelatedByDestJobId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonJmsJobDependenciessRelatedByDestJobId($overrideExisting = true)
    {
        if (null !== $this->collCommonJmsJobDependenciessRelatedByDestJobId && !$overrideExisting) {
            return;
        }
        $this->collCommonJmsJobDependenciessRelatedByDestJobId = new PropelObjectCollection();
        $this->collCommonJmsJobDependenciessRelatedByDestJobId->setModel('CommonJmsJobDependencies');
    }

    /**
     * Gets an array of CommonJmsJobDependencies objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonJmsJobs is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonJmsJobDependencies[] List of CommonJmsJobDependencies objects
     * @throws PropelException
     */
    public function getCommonJmsJobDependenciessRelatedByDestJobId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonJmsJobDependenciessRelatedByDestJobIdPartial && !$this->isNew();
        if (null === $this->collCommonJmsJobDependenciessRelatedByDestJobId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonJmsJobDependenciessRelatedByDestJobId) {
                // return empty collection
                $this->initCommonJmsJobDependenciessRelatedByDestJobId();
            } else {
                $collCommonJmsJobDependenciessRelatedByDestJobId = CommonJmsJobDependenciesQuery::create(null, $criteria)
                    ->filterByCommonJmsJobsRelatedByDestJobId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonJmsJobDependenciessRelatedByDestJobIdPartial && count($collCommonJmsJobDependenciessRelatedByDestJobId)) {
                      $this->initCommonJmsJobDependenciessRelatedByDestJobId(false);

                      foreach ($collCommonJmsJobDependenciessRelatedByDestJobId as $obj) {
                        if (false == $this->collCommonJmsJobDependenciessRelatedByDestJobId->contains($obj)) {
                          $this->collCommonJmsJobDependenciessRelatedByDestJobId->append($obj);
                        }
                      }

                      $this->collCommonJmsJobDependenciessRelatedByDestJobIdPartial = true;
                    }

                    $collCommonJmsJobDependenciessRelatedByDestJobId->getInternalIterator()->rewind();

                    return $collCommonJmsJobDependenciessRelatedByDestJobId;
                }

                if ($partial && $this->collCommonJmsJobDependenciessRelatedByDestJobId) {
                    foreach ($this->collCommonJmsJobDependenciessRelatedByDestJobId as $obj) {
                        if ($obj->isNew()) {
                            $collCommonJmsJobDependenciessRelatedByDestJobId[] = $obj;
                        }
                    }
                }

                $this->collCommonJmsJobDependenciessRelatedByDestJobId = $collCommonJmsJobDependenciessRelatedByDestJobId;
                $this->collCommonJmsJobDependenciessRelatedByDestJobIdPartial = false;
            }
        }

        return $this->collCommonJmsJobDependenciessRelatedByDestJobId;
    }

    /**
     * Sets a collection of CommonJmsJobDependenciesRelatedByDestJobId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonJmsJobDependenciessRelatedByDestJobId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setCommonJmsJobDependenciessRelatedByDestJobId(PropelCollection $commonJmsJobDependenciessRelatedByDestJobId, PropelPDO $con = null)
    {
        $commonJmsJobDependenciessRelatedByDestJobIdToDelete = $this->getCommonJmsJobDependenciessRelatedByDestJobId(new Criteria(), $con)->diff($commonJmsJobDependenciessRelatedByDestJobId);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion = clone $commonJmsJobDependenciessRelatedByDestJobIdToDelete;

        foreach ($commonJmsJobDependenciessRelatedByDestJobIdToDelete as $commonJmsJobDependenciesRelatedByDestJobIdRemoved) {
            $commonJmsJobDependenciesRelatedByDestJobIdRemoved->setCommonJmsJobsRelatedByDestJobId(null);
        }

        $this->collCommonJmsJobDependenciessRelatedByDestJobId = null;
        foreach ($commonJmsJobDependenciessRelatedByDestJobId as $commonJmsJobDependenciesRelatedByDestJobId) {
            $this->addCommonJmsJobDependenciesRelatedByDestJobId($commonJmsJobDependenciesRelatedByDestJobId);
        }

        $this->collCommonJmsJobDependenciessRelatedByDestJobId = $commonJmsJobDependenciessRelatedByDestJobId;
        $this->collCommonJmsJobDependenciessRelatedByDestJobIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonJmsJobDependencies objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonJmsJobDependencies objects.
     * @throws PropelException
     */
    public function countCommonJmsJobDependenciessRelatedByDestJobId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonJmsJobDependenciessRelatedByDestJobIdPartial && !$this->isNew();
        if (null === $this->collCommonJmsJobDependenciessRelatedByDestJobId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonJmsJobDependenciessRelatedByDestJobId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonJmsJobDependenciessRelatedByDestJobId());
            }
            $query = CommonJmsJobDependenciesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonJmsJobsRelatedByDestJobId($this)
                ->count($con);
        }

        return count($this->collCommonJmsJobDependenciessRelatedByDestJobId);
    }

    /**
     * Method called to associate a CommonJmsJobDependencies object to this object
     * through the CommonJmsJobDependencies foreign key attribute.
     *
     * @param   CommonJmsJobDependencies $l CommonJmsJobDependencies
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function addCommonJmsJobDependenciesRelatedByDestJobId(CommonJmsJobDependencies $l)
    {
        if ($this->collCommonJmsJobDependenciessRelatedByDestJobId === null) {
            $this->initCommonJmsJobDependenciessRelatedByDestJobId();
            $this->collCommonJmsJobDependenciessRelatedByDestJobIdPartial = true;
        }
        if (!in_array($l, $this->collCommonJmsJobDependenciessRelatedByDestJobId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonJmsJobDependenciesRelatedByDestJobId($l);
        }

        return $this;
    }

    /**
     * @param	CommonJmsJobDependenciesRelatedByDestJobId $commonJmsJobDependenciesRelatedByDestJobId The commonJmsJobDependenciesRelatedByDestJobId object to add.
     */
    protected function doAddCommonJmsJobDependenciesRelatedByDestJobId($commonJmsJobDependenciesRelatedByDestJobId)
    {
        $this->collCommonJmsJobDependenciessRelatedByDestJobId[]= $commonJmsJobDependenciesRelatedByDestJobId;
        $commonJmsJobDependenciesRelatedByDestJobId->setCommonJmsJobsRelatedByDestJobId($this);
    }

    /**
     * @param	CommonJmsJobDependenciesRelatedByDestJobId $commonJmsJobDependenciesRelatedByDestJobId The commonJmsJobDependenciesRelatedByDestJobId object to remove.
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function removeCommonJmsJobDependenciesRelatedByDestJobId($commonJmsJobDependenciesRelatedByDestJobId)
    {
        if ($this->getCommonJmsJobDependenciessRelatedByDestJobId()->contains($commonJmsJobDependenciesRelatedByDestJobId)) {
            $this->collCommonJmsJobDependenciessRelatedByDestJobId->remove($this->collCommonJmsJobDependenciessRelatedByDestJobId->search($commonJmsJobDependenciesRelatedByDestJobId));
            if (null === $this->commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion) {
                $this->commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion = clone $this->collCommonJmsJobDependenciessRelatedByDestJobId;
                $this->commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion->clear();
            }
            $this->commonJmsJobDependenciessRelatedByDestJobIdScheduledForDeletion[]= clone $commonJmsJobDependenciesRelatedByDestJobId;
            $commonJmsJobDependenciesRelatedByDestJobId->setCommonJmsJobsRelatedByDestJobId(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonJmsJobDependenciessRelatedBySourceJobId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonJmsJobs The current object (for fluent API support)
     * @see        addCommonJmsJobDependenciessRelatedBySourceJobId()
     */
    public function clearCommonJmsJobDependenciessRelatedBySourceJobId()
    {
        $this->collCommonJmsJobDependenciessRelatedBySourceJobId = null; // important to set this to null since that means it is uninitialized
        $this->collCommonJmsJobDependenciessRelatedBySourceJobIdPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonJmsJobDependenciessRelatedBySourceJobId collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonJmsJobDependenciessRelatedBySourceJobId($v = true)
    {
        $this->collCommonJmsJobDependenciessRelatedBySourceJobIdPartial = $v;
    }

    /**
     * Initializes the collCommonJmsJobDependenciessRelatedBySourceJobId collection.
     *
     * By default this just sets the collCommonJmsJobDependenciessRelatedBySourceJobId collection to an empty array (like clearcollCommonJmsJobDependenciessRelatedBySourceJobId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonJmsJobDependenciessRelatedBySourceJobId($overrideExisting = true)
    {
        if (null !== $this->collCommonJmsJobDependenciessRelatedBySourceJobId && !$overrideExisting) {
            return;
        }
        $this->collCommonJmsJobDependenciessRelatedBySourceJobId = new PropelObjectCollection();
        $this->collCommonJmsJobDependenciessRelatedBySourceJobId->setModel('CommonJmsJobDependencies');
    }

    /**
     * Gets an array of CommonJmsJobDependencies objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonJmsJobs is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonJmsJobDependencies[] List of CommonJmsJobDependencies objects
     * @throws PropelException
     */
    public function getCommonJmsJobDependenciessRelatedBySourceJobId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonJmsJobDependenciessRelatedBySourceJobIdPartial && !$this->isNew();
        if (null === $this->collCommonJmsJobDependenciessRelatedBySourceJobId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonJmsJobDependenciessRelatedBySourceJobId) {
                // return empty collection
                $this->initCommonJmsJobDependenciessRelatedBySourceJobId();
            } else {
                $collCommonJmsJobDependenciessRelatedBySourceJobId = CommonJmsJobDependenciesQuery::create(null, $criteria)
                    ->filterByCommonJmsJobsRelatedBySourceJobId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonJmsJobDependenciessRelatedBySourceJobIdPartial && count($collCommonJmsJobDependenciessRelatedBySourceJobId)) {
                      $this->initCommonJmsJobDependenciessRelatedBySourceJobId(false);

                      foreach ($collCommonJmsJobDependenciessRelatedBySourceJobId as $obj) {
                        if (false == $this->collCommonJmsJobDependenciessRelatedBySourceJobId->contains($obj)) {
                          $this->collCommonJmsJobDependenciessRelatedBySourceJobId->append($obj);
                        }
                      }

                      $this->collCommonJmsJobDependenciessRelatedBySourceJobIdPartial = true;
                    }

                    $collCommonJmsJobDependenciessRelatedBySourceJobId->getInternalIterator()->rewind();

                    return $collCommonJmsJobDependenciessRelatedBySourceJobId;
                }

                if ($partial && $this->collCommonJmsJobDependenciessRelatedBySourceJobId) {
                    foreach ($this->collCommonJmsJobDependenciessRelatedBySourceJobId as $obj) {
                        if ($obj->isNew()) {
                            $collCommonJmsJobDependenciessRelatedBySourceJobId[] = $obj;
                        }
                    }
                }

                $this->collCommonJmsJobDependenciessRelatedBySourceJobId = $collCommonJmsJobDependenciessRelatedBySourceJobId;
                $this->collCommonJmsJobDependenciessRelatedBySourceJobIdPartial = false;
            }
        }

        return $this->collCommonJmsJobDependenciessRelatedBySourceJobId;
    }

    /**
     * Sets a collection of CommonJmsJobDependenciesRelatedBySourceJobId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonJmsJobDependenciessRelatedBySourceJobId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setCommonJmsJobDependenciessRelatedBySourceJobId(PropelCollection $commonJmsJobDependenciessRelatedBySourceJobId, PropelPDO $con = null)
    {
        $commonJmsJobDependenciessRelatedBySourceJobIdToDelete = $this->getCommonJmsJobDependenciessRelatedBySourceJobId(new Criteria(), $con)->diff($commonJmsJobDependenciessRelatedBySourceJobId);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion = clone $commonJmsJobDependenciessRelatedBySourceJobIdToDelete;

        foreach ($commonJmsJobDependenciessRelatedBySourceJobIdToDelete as $commonJmsJobDependenciesRelatedBySourceJobIdRemoved) {
            $commonJmsJobDependenciesRelatedBySourceJobIdRemoved->setCommonJmsJobsRelatedBySourceJobId(null);
        }

        $this->collCommonJmsJobDependenciessRelatedBySourceJobId = null;
        foreach ($commonJmsJobDependenciessRelatedBySourceJobId as $commonJmsJobDependenciesRelatedBySourceJobId) {
            $this->addCommonJmsJobDependenciesRelatedBySourceJobId($commonJmsJobDependenciesRelatedBySourceJobId);
        }

        $this->collCommonJmsJobDependenciessRelatedBySourceJobId = $commonJmsJobDependenciessRelatedBySourceJobId;
        $this->collCommonJmsJobDependenciessRelatedBySourceJobIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonJmsJobDependencies objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonJmsJobDependencies objects.
     * @throws PropelException
     */
    public function countCommonJmsJobDependenciessRelatedBySourceJobId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonJmsJobDependenciessRelatedBySourceJobIdPartial && !$this->isNew();
        if (null === $this->collCommonJmsJobDependenciessRelatedBySourceJobId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonJmsJobDependenciessRelatedBySourceJobId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonJmsJobDependenciessRelatedBySourceJobId());
            }
            $query = CommonJmsJobDependenciesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonJmsJobsRelatedBySourceJobId($this)
                ->count($con);
        }

        return count($this->collCommonJmsJobDependenciessRelatedBySourceJobId);
    }

    /**
     * Method called to associate a CommonJmsJobDependencies object to this object
     * through the CommonJmsJobDependencies foreign key attribute.
     *
     * @param   CommonJmsJobDependencies $l CommonJmsJobDependencies
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function addCommonJmsJobDependenciesRelatedBySourceJobId(CommonJmsJobDependencies $l)
    {
        if ($this->collCommonJmsJobDependenciessRelatedBySourceJobId === null) {
            $this->initCommonJmsJobDependenciessRelatedBySourceJobId();
            $this->collCommonJmsJobDependenciessRelatedBySourceJobIdPartial = true;
        }
        if (!in_array($l, $this->collCommonJmsJobDependenciessRelatedBySourceJobId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonJmsJobDependenciesRelatedBySourceJobId($l);
        }

        return $this;
    }

    /**
     * @param	CommonJmsJobDependenciesRelatedBySourceJobId $commonJmsJobDependenciesRelatedBySourceJobId The commonJmsJobDependenciesRelatedBySourceJobId object to add.
     */
    protected function doAddCommonJmsJobDependenciesRelatedBySourceJobId($commonJmsJobDependenciesRelatedBySourceJobId)
    {
        $this->collCommonJmsJobDependenciessRelatedBySourceJobId[]= $commonJmsJobDependenciesRelatedBySourceJobId;
        $commonJmsJobDependenciesRelatedBySourceJobId->setCommonJmsJobsRelatedBySourceJobId($this);
    }

    /**
     * @param	CommonJmsJobDependenciesRelatedBySourceJobId $commonJmsJobDependenciesRelatedBySourceJobId The commonJmsJobDependenciesRelatedBySourceJobId object to remove.
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function removeCommonJmsJobDependenciesRelatedBySourceJobId($commonJmsJobDependenciesRelatedBySourceJobId)
    {
        if ($this->getCommonJmsJobDependenciessRelatedBySourceJobId()->contains($commonJmsJobDependenciesRelatedBySourceJobId)) {
            $this->collCommonJmsJobDependenciessRelatedBySourceJobId->remove($this->collCommonJmsJobDependenciessRelatedBySourceJobId->search($commonJmsJobDependenciesRelatedBySourceJobId));
            if (null === $this->commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion) {
                $this->commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion = clone $this->collCommonJmsJobDependenciessRelatedBySourceJobId;
                $this->commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion->clear();
            }
            $this->commonJmsJobDependenciessRelatedBySourceJobIdScheduledForDeletion[]= clone $commonJmsJobDependenciesRelatedBySourceJobId;
            $commonJmsJobDependenciesRelatedBySourceJobId->setCommonJmsJobsRelatedBySourceJobId(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonJmsJobRelatedEntitiess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonJmsJobs The current object (for fluent API support)
     * @see        addCommonJmsJobRelatedEntitiess()
     */
    public function clearCommonJmsJobRelatedEntitiess()
    {
        $this->collCommonJmsJobRelatedEntitiess = null; // important to set this to null since that means it is uninitialized
        $this->collCommonJmsJobRelatedEntitiessPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonJmsJobRelatedEntitiess collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonJmsJobRelatedEntitiess($v = true)
    {
        $this->collCommonJmsJobRelatedEntitiessPartial = $v;
    }

    /**
     * Initializes the collCommonJmsJobRelatedEntitiess collection.
     *
     * By default this just sets the collCommonJmsJobRelatedEntitiess collection to an empty array (like clearcollCommonJmsJobRelatedEntitiess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonJmsJobRelatedEntitiess($overrideExisting = true)
    {
        if (null !== $this->collCommonJmsJobRelatedEntitiess && !$overrideExisting) {
            return;
        }
        $this->collCommonJmsJobRelatedEntitiess = new PropelObjectCollection();
        $this->collCommonJmsJobRelatedEntitiess->setModel('CommonJmsJobRelatedEntities');
    }

    /**
     * Gets an array of CommonJmsJobRelatedEntities objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonJmsJobs is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonJmsJobRelatedEntities[] List of CommonJmsJobRelatedEntities objects
     * @throws PropelException
     */
    public function getCommonJmsJobRelatedEntitiess($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonJmsJobRelatedEntitiessPartial && !$this->isNew();
        if (null === $this->collCommonJmsJobRelatedEntitiess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonJmsJobRelatedEntitiess) {
                // return empty collection
                $this->initCommonJmsJobRelatedEntitiess();
            } else {
                $collCommonJmsJobRelatedEntitiess = CommonJmsJobRelatedEntitiesQuery::create(null, $criteria)
                    ->filterByCommonJmsJobs($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonJmsJobRelatedEntitiessPartial && count($collCommonJmsJobRelatedEntitiess)) {
                      $this->initCommonJmsJobRelatedEntitiess(false);

                      foreach ($collCommonJmsJobRelatedEntitiess as $obj) {
                        if (false == $this->collCommonJmsJobRelatedEntitiess->contains($obj)) {
                          $this->collCommonJmsJobRelatedEntitiess->append($obj);
                        }
                      }

                      $this->collCommonJmsJobRelatedEntitiessPartial = true;
                    }

                    $collCommonJmsJobRelatedEntitiess->getInternalIterator()->rewind();

                    return $collCommonJmsJobRelatedEntitiess;
                }

                if ($partial && $this->collCommonJmsJobRelatedEntitiess) {
                    foreach ($this->collCommonJmsJobRelatedEntitiess as $obj) {
                        if ($obj->isNew()) {
                            $collCommonJmsJobRelatedEntitiess[] = $obj;
                        }
                    }
                }

                $this->collCommonJmsJobRelatedEntitiess = $collCommonJmsJobRelatedEntitiess;
                $this->collCommonJmsJobRelatedEntitiessPartial = false;
            }
        }

        return $this->collCommonJmsJobRelatedEntitiess;
    }

    /**
     * Sets a collection of CommonJmsJobRelatedEntities objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonJmsJobRelatedEntitiess A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setCommonJmsJobRelatedEntitiess(PropelCollection $commonJmsJobRelatedEntitiess, PropelPDO $con = null)
    {
        $commonJmsJobRelatedEntitiessToDelete = $this->getCommonJmsJobRelatedEntitiess(new Criteria(), $con)->diff($commonJmsJobRelatedEntitiess);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonJmsJobRelatedEntitiessScheduledForDeletion = clone $commonJmsJobRelatedEntitiessToDelete;

        foreach ($commonJmsJobRelatedEntitiessToDelete as $commonJmsJobRelatedEntitiesRemoved) {
            $commonJmsJobRelatedEntitiesRemoved->setCommonJmsJobs(null);
        }

        $this->collCommonJmsJobRelatedEntitiess = null;
        foreach ($commonJmsJobRelatedEntitiess as $commonJmsJobRelatedEntities) {
            $this->addCommonJmsJobRelatedEntities($commonJmsJobRelatedEntities);
        }

        $this->collCommonJmsJobRelatedEntitiess = $commonJmsJobRelatedEntitiess;
        $this->collCommonJmsJobRelatedEntitiessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonJmsJobRelatedEntities objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonJmsJobRelatedEntities objects.
     * @throws PropelException
     */
    public function countCommonJmsJobRelatedEntitiess(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonJmsJobRelatedEntitiessPartial && !$this->isNew();
        if (null === $this->collCommonJmsJobRelatedEntitiess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonJmsJobRelatedEntitiess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonJmsJobRelatedEntitiess());
            }
            $query = CommonJmsJobRelatedEntitiesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonJmsJobs($this)
                ->count($con);
        }

        return count($this->collCommonJmsJobRelatedEntitiess);
    }

    /**
     * Method called to associate a CommonJmsJobRelatedEntities object to this object
     * through the CommonJmsJobRelatedEntities foreign key attribute.
     *
     * @param   CommonJmsJobRelatedEntities $l CommonJmsJobRelatedEntities
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function addCommonJmsJobRelatedEntities(CommonJmsJobRelatedEntities $l)
    {
        if ($this->collCommonJmsJobRelatedEntitiess === null) {
            $this->initCommonJmsJobRelatedEntitiess();
            $this->collCommonJmsJobRelatedEntitiessPartial = true;
        }
        if (!in_array($l, $this->collCommonJmsJobRelatedEntitiess->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonJmsJobRelatedEntities($l);
        }

        return $this;
    }

    /**
     * @param	CommonJmsJobRelatedEntities $commonJmsJobRelatedEntities The commonJmsJobRelatedEntities object to add.
     */
    protected function doAddCommonJmsJobRelatedEntities($commonJmsJobRelatedEntities)
    {
        $this->collCommonJmsJobRelatedEntitiess[]= $commonJmsJobRelatedEntities;
        $commonJmsJobRelatedEntities->setCommonJmsJobs($this);
    }

    /**
     * @param	CommonJmsJobRelatedEntities $commonJmsJobRelatedEntities The commonJmsJobRelatedEntities object to remove.
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function removeCommonJmsJobRelatedEntities($commonJmsJobRelatedEntities)
    {
        if ($this->getCommonJmsJobRelatedEntitiess()->contains($commonJmsJobRelatedEntities)) {
            $this->collCommonJmsJobRelatedEntitiess->remove($this->collCommonJmsJobRelatedEntitiess->search($commonJmsJobRelatedEntities));
            if (null === $this->commonJmsJobRelatedEntitiessScheduledForDeletion) {
                $this->commonJmsJobRelatedEntitiessScheduledForDeletion = clone $this->collCommonJmsJobRelatedEntitiess;
                $this->commonJmsJobRelatedEntitiessScheduledForDeletion->clear();
            }
            $this->commonJmsJobRelatedEntitiessScheduledForDeletion[]= clone $commonJmsJobRelatedEntities;
            $commonJmsJobRelatedEntities->setCommonJmsJobs(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonJmsJobssRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonJmsJobs The current object (for fluent API support)
     * @see        addCommonJmsJobssRelatedById()
     */
    public function clearCommonJmsJobssRelatedById()
    {
        $this->collCommonJmsJobssRelatedById = null; // important to set this to null since that means it is uninitialized
        $this->collCommonJmsJobssRelatedByIdPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonJmsJobssRelatedById collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonJmsJobssRelatedById($v = true)
    {
        $this->collCommonJmsJobssRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collCommonJmsJobssRelatedById collection.
     *
     * By default this just sets the collCommonJmsJobssRelatedById collection to an empty array (like clearcollCommonJmsJobssRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonJmsJobssRelatedById($overrideExisting = true)
    {
        if (null !== $this->collCommonJmsJobssRelatedById && !$overrideExisting) {
            return;
        }
        $this->collCommonJmsJobssRelatedById = new PropelObjectCollection();
        $this->collCommonJmsJobssRelatedById->setModel('CommonJmsJobs');
    }

    /**
     * Gets an array of CommonJmsJobs objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonJmsJobs is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonJmsJobs[] List of CommonJmsJobs objects
     * @throws PropelException
     */
    public function getCommonJmsJobssRelatedById($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonJmsJobssRelatedByIdPartial && !$this->isNew();
        if (null === $this->collCommonJmsJobssRelatedById || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonJmsJobssRelatedById) {
                // return empty collection
                $this->initCommonJmsJobssRelatedById();
            } else {
                $collCommonJmsJobssRelatedById = CommonJmsJobsQuery::create(null, $criteria)
                    ->filterByCommonJmsJobsRelatedByOriginaljobId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonJmsJobssRelatedByIdPartial && count($collCommonJmsJobssRelatedById)) {
                      $this->initCommonJmsJobssRelatedById(false);

                      foreach ($collCommonJmsJobssRelatedById as $obj) {
                        if (false == $this->collCommonJmsJobssRelatedById->contains($obj)) {
                          $this->collCommonJmsJobssRelatedById->append($obj);
                        }
                      }

                      $this->collCommonJmsJobssRelatedByIdPartial = true;
                    }

                    $collCommonJmsJobssRelatedById->getInternalIterator()->rewind();

                    return $collCommonJmsJobssRelatedById;
                }

                if ($partial && $this->collCommonJmsJobssRelatedById) {
                    foreach ($this->collCommonJmsJobssRelatedById as $obj) {
                        if ($obj->isNew()) {
                            $collCommonJmsJobssRelatedById[] = $obj;
                        }
                    }
                }

                $this->collCommonJmsJobssRelatedById = $collCommonJmsJobssRelatedById;
                $this->collCommonJmsJobssRelatedByIdPartial = false;
            }
        }

        return $this->collCommonJmsJobssRelatedById;
    }

    /**
     * Sets a collection of CommonJmsJobsRelatedById objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonJmsJobssRelatedById A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function setCommonJmsJobssRelatedById(PropelCollection $commonJmsJobssRelatedById, PropelPDO $con = null)
    {
        $commonJmsJobssRelatedByIdToDelete = $this->getCommonJmsJobssRelatedById(new Criteria(), $con)->diff($commonJmsJobssRelatedById);


        $this->commonJmsJobssRelatedByIdScheduledForDeletion = $commonJmsJobssRelatedByIdToDelete;

        foreach ($commonJmsJobssRelatedByIdToDelete as $commonJmsJobsRelatedByIdRemoved) {
            $commonJmsJobsRelatedByIdRemoved->setCommonJmsJobsRelatedByOriginaljobId(null);
        }

        $this->collCommonJmsJobssRelatedById = null;
        foreach ($commonJmsJobssRelatedById as $commonJmsJobsRelatedById) {
            $this->addCommonJmsJobsRelatedById($commonJmsJobsRelatedById);
        }

        $this->collCommonJmsJobssRelatedById = $commonJmsJobssRelatedById;
        $this->collCommonJmsJobssRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonJmsJobs objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonJmsJobs objects.
     * @throws PropelException
     */
    public function countCommonJmsJobssRelatedById(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonJmsJobssRelatedByIdPartial && !$this->isNew();
        if (null === $this->collCommonJmsJobssRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonJmsJobssRelatedById) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonJmsJobssRelatedById());
            }
            $query = CommonJmsJobsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonJmsJobsRelatedByOriginaljobId($this)
                ->count($con);
        }

        return count($this->collCommonJmsJobssRelatedById);
    }

    /**
     * Method called to associate a CommonJmsJobs object to this object
     * through the CommonJmsJobs foreign key attribute.
     *
     * @param   CommonJmsJobs $l CommonJmsJobs
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function addCommonJmsJobsRelatedById(CommonJmsJobs $l)
    {
        if ($this->collCommonJmsJobssRelatedById === null) {
            $this->initCommonJmsJobssRelatedById();
            $this->collCommonJmsJobssRelatedByIdPartial = true;
        }
        if (!in_array($l, $this->collCommonJmsJobssRelatedById->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonJmsJobsRelatedById($l);
        }

        return $this;
    }

    /**
     * @param	CommonJmsJobsRelatedById $commonJmsJobsRelatedById The commonJmsJobsRelatedById object to add.
     */
    protected function doAddCommonJmsJobsRelatedById($commonJmsJobsRelatedById)
    {
        $this->collCommonJmsJobssRelatedById[]= $commonJmsJobsRelatedById;
        $commonJmsJobsRelatedById->setCommonJmsJobsRelatedByOriginaljobId($this);
    }

    /**
     * @param	CommonJmsJobsRelatedById $commonJmsJobsRelatedById The commonJmsJobsRelatedById object to remove.
     * @return CommonJmsJobs The current object (for fluent API support)
     */
    public function removeCommonJmsJobsRelatedById($commonJmsJobsRelatedById)
    {
        if ($this->getCommonJmsJobssRelatedById()->contains($commonJmsJobsRelatedById)) {
            $this->collCommonJmsJobssRelatedById->remove($this->collCommonJmsJobssRelatedById->search($commonJmsJobsRelatedById));
            if (null === $this->commonJmsJobssRelatedByIdScheduledForDeletion) {
                $this->commonJmsJobssRelatedByIdScheduledForDeletion = clone $this->collCommonJmsJobssRelatedById;
                $this->commonJmsJobssRelatedByIdScheduledForDeletion->clear();
            }
            $this->commonJmsJobssRelatedByIdScheduledForDeletion[]= $commonJmsJobsRelatedById;
            $commonJmsJobsRelatedById->setCommonJmsJobsRelatedByOriginaljobId(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->state = null;
        $this->queue = null;
        $this->priority = null;
        $this->createdat = null;
        $this->startedat = null;
        $this->checkedat = null;
        $this->workername = null;
        $this->executeafter = null;
        $this->closedat = null;
        $this->command = null;
        $this->args = null;
        $this->output = null;
        $this->erroroutput = null;
        $this->exitcode = null;
        $this->maxruntime = null;
        $this->maxretries = null;
        $this->stacktrace = null;
        $this->runtime = null;
        $this->memoryusage = null;
        $this->memoryusagereal = null;
        $this->originaljob_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonJmsJobDependenciessRelatedByDestJobId) {
                foreach ($this->collCommonJmsJobDependenciessRelatedByDestJobId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonJmsJobDependenciessRelatedBySourceJobId) {
                foreach ($this->collCommonJmsJobDependenciessRelatedBySourceJobId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonJmsJobRelatedEntitiess) {
                foreach ($this->collCommonJmsJobRelatedEntitiess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonJmsJobssRelatedById) {
                foreach ($this->collCommonJmsJobssRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonJmsJobsRelatedByOriginaljobId instanceof Persistent) {
              $this->aCommonJmsJobsRelatedByOriginaljobId->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonJmsJobDependenciessRelatedByDestJobId instanceof PropelCollection) {
            $this->collCommonJmsJobDependenciessRelatedByDestJobId->clearIterator();
        }
        $this->collCommonJmsJobDependenciessRelatedByDestJobId = null;
        if ($this->collCommonJmsJobDependenciessRelatedBySourceJobId instanceof PropelCollection) {
            $this->collCommonJmsJobDependenciessRelatedBySourceJobId->clearIterator();
        }
        $this->collCommonJmsJobDependenciessRelatedBySourceJobId = null;
        if ($this->collCommonJmsJobRelatedEntitiess instanceof PropelCollection) {
            $this->collCommonJmsJobRelatedEntitiess->clearIterator();
        }
        $this->collCommonJmsJobRelatedEntitiess = null;
        if ($this->collCommonJmsJobssRelatedById instanceof PropelCollection) {
            $this->collCommonJmsJobssRelatedById->clearIterator();
        }
        $this->collCommonJmsJobssRelatedById = null;
        $this->aCommonJmsJobsRelatedByOriginaljobId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonJmsJobsPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
