<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonTCAOCommissionConsultationPeer;
use Application\Propel\Mpe\CommonTCAOCommissionPeer;
use Application\Propel\Mpe\CommonTCAOOrdreDuJour;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourPeer;
use Application\Propel\Mpe\CommonTCAOSeancePeer;
use Application\Propel\Mpe\Map\CommonTCAOOrdreDuJourTableMap;

/**
 * Base static class for performing query and update operations on the 't_CAO_Ordre_Du_Jour' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTCAOOrdreDuJourPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_CAO_Ordre_Du_Jour';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTCAOOrdreDuJour';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTCAOOrdreDuJourTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 10;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 10;

    /** the column name for the id_ordre_du_jour field */
    const ID_ORDRE_DU_JOUR = 't_CAO_Ordre_Du_Jour.id_ordre_du_jour';

    /** the column name for the organisme field */
    const ORGANISME = 't_CAO_Ordre_Du_Jour.organisme';

    /** the column name for the id_seance field */
    const ID_SEANCE = 't_CAO_Ordre_Du_Jour.id_seance';

    /** the column name for the id_commission field */
    const ID_COMMISSION = 't_CAO_Ordre_Du_Jour.id_commission';

    /** the column name for the id_commission_consultation field */
    const ID_COMMISSION_CONSULTATION = 't_CAO_Ordre_Du_Jour.id_commission_consultation';

    /** the column name for the numero field */
    const NUMERO = 't_CAO_Ordre_Du_Jour.numero';

    /** the column name for the id_ref_org_val_etape field */
    const ID_REF_ORG_VAL_ETAPE = 't_CAO_Ordre_Du_Jour.id_ref_org_val_etape';

    /** the column name for the intitule field */
    const INTITULE = 't_CAO_Ordre_Du_Jour.intitule';

    /** the column name for the date_seance field */
    const DATE_SEANCE = 't_CAO_Ordre_Du_Jour.date_seance';

    /** the column name for the heure_passage field */
    const HEURE_PASSAGE = 't_CAO_Ordre_Du_Jour.heure_passage';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTCAOOrdreDuJour objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTCAOOrdreDuJour[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTCAOOrdreDuJourPeer::$fieldNames[CommonTCAOOrdreDuJourPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdOrdreDuJour', 'Organisme', 'IdSeance', 'IdCommission', 'IdCommissionConsultation', 'Numero', 'IdRefOrgValEtape', 'Intitule', 'DateSeance', 'HeurePassage', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idOrdreDuJour', 'organisme', 'idSeance', 'idCommission', 'idCommissionConsultation', 'numero', 'idRefOrgValEtape', 'intitule', 'dateSeance', 'heurePassage', ),
        BasePeer::TYPE_COLNAME => array (CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR, CommonTCAOOrdreDuJourPeer::ORGANISME, CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOOrdreDuJourPeer::NUMERO, CommonTCAOOrdreDuJourPeer::ID_REF_ORG_VAL_ETAPE, CommonTCAOOrdreDuJourPeer::INTITULE, CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOOrdreDuJourPeer::HEURE_PASSAGE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_ORDRE_DU_JOUR', 'ORGANISME', 'ID_SEANCE', 'ID_COMMISSION', 'ID_COMMISSION_CONSULTATION', 'NUMERO', 'ID_REF_ORG_VAL_ETAPE', 'INTITULE', 'DATE_SEANCE', 'HEURE_PASSAGE', ),
        BasePeer::TYPE_FIELDNAME => array ('id_ordre_du_jour', 'organisme', 'id_seance', 'id_commission', 'id_commission_consultation', 'numero', 'id_ref_org_val_etape', 'intitule', 'date_seance', 'heure_passage', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTCAOOrdreDuJourPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdOrdreDuJour' => 0, 'Organisme' => 1, 'IdSeance' => 2, 'IdCommission' => 3, 'IdCommissionConsultation' => 4, 'Numero' => 5, 'IdRefOrgValEtape' => 6, 'Intitule' => 7, 'DateSeance' => 8, 'HeurePassage' => 9, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idOrdreDuJour' => 0, 'organisme' => 1, 'idSeance' => 2, 'idCommission' => 3, 'idCommissionConsultation' => 4, 'numero' => 5, 'idRefOrgValEtape' => 6, 'intitule' => 7, 'dateSeance' => 8, 'heurePassage' => 9, ),
        BasePeer::TYPE_COLNAME => array (CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR => 0, CommonTCAOOrdreDuJourPeer::ORGANISME => 1, CommonTCAOOrdreDuJourPeer::ID_SEANCE => 2, CommonTCAOOrdreDuJourPeer::ID_COMMISSION => 3, CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION => 4, CommonTCAOOrdreDuJourPeer::NUMERO => 5, CommonTCAOOrdreDuJourPeer::ID_REF_ORG_VAL_ETAPE => 6, CommonTCAOOrdreDuJourPeer::INTITULE => 7, CommonTCAOOrdreDuJourPeer::DATE_SEANCE => 8, CommonTCAOOrdreDuJourPeer::HEURE_PASSAGE => 9, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_ORDRE_DU_JOUR' => 0, 'ORGANISME' => 1, 'ID_SEANCE' => 2, 'ID_COMMISSION' => 3, 'ID_COMMISSION_CONSULTATION' => 4, 'NUMERO' => 5, 'ID_REF_ORG_VAL_ETAPE' => 6, 'INTITULE' => 7, 'DATE_SEANCE' => 8, 'HEURE_PASSAGE' => 9, ),
        BasePeer::TYPE_FIELDNAME => array ('id_ordre_du_jour' => 0, 'organisme' => 1, 'id_seance' => 2, 'id_commission' => 3, 'id_commission_consultation' => 4, 'numero' => 5, 'id_ref_org_val_etape' => 6, 'intitule' => 7, 'date_seance' => 8, 'heure_passage' => 9, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTCAOOrdreDuJourPeer::getFieldNames($toType);
        $key = isset(CommonTCAOOrdreDuJourPeer::$fieldKeys[$fromType][$name]) ? CommonTCAOOrdreDuJourPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTCAOOrdreDuJourPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTCAOOrdreDuJourPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTCAOOrdreDuJourPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTCAOOrdreDuJourPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTCAOOrdreDuJourPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR);
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::ORGANISME);
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::ID_SEANCE);
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::ID_COMMISSION);
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION);
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::NUMERO);
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::ID_REF_ORG_VAL_ETAPE);
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::INTITULE);
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::DATE_SEANCE);
            $criteria->addSelectColumn(CommonTCAOOrdreDuJourPeer::HEURE_PASSAGE);
        } else {
            $criteria->addSelectColumn($alias . '.id_ordre_du_jour');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.id_seance');
            $criteria->addSelectColumn($alias . '.id_commission');
            $criteria->addSelectColumn($alias . '.id_commission_consultation');
            $criteria->addSelectColumn($alias . '.numero');
            $criteria->addSelectColumn($alias . '.id_ref_org_val_etape');
            $criteria->addSelectColumn($alias . '.intitule');
            $criteria->addSelectColumn($alias . '.date_seance');
            $criteria->addSelectColumn($alias . '.heure_passage');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTCAOOrdreDuJour
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTCAOOrdreDuJourPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTCAOOrdreDuJourPeer::populateObjects(CommonTCAOOrdreDuJourPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTCAOOrdreDuJour $obj A CommonTCAOOrdreDuJour object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getIdOrdreDuJour(), (string) $obj->getOrganisme()));
            } // if key === null
            CommonTCAOOrdreDuJourPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTCAOOrdreDuJour object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTCAOOrdreDuJour) {
                $key = serialize(array((string) $value->getIdOrdreDuJour(), (string) $value->getOrganisme()));
            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTCAOOrdreDuJour object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTCAOOrdreDuJourPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTCAOOrdreDuJour Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTCAOOrdreDuJourPeer::$instances[$key])) {
                return CommonTCAOOrdreDuJourPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTCAOOrdreDuJourPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTCAOOrdreDuJourPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_CAO_Ordre_Du_Jour
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((string) $row[$startcol], (string) $row[$startcol + 1]);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTCAOOrdreDuJourPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTCAOOrdreDuJour object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTCAOOrdreDuJourPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCAOSeanceRelatedByDateSeance table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTCAOSeanceRelatedByDateSeance(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCAOCommissionConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTCAOCommissionConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCAOCommission table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTCAOCommission(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCAOSeanceRelatedByIdSeance table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTCAOSeanceRelatedByIdSeance(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOrganisme table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonOrganisme(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with their CommonTCAOSeance objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTCAOSeanceRelatedByDateSeance(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;
        CommonTCAOSeancePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTCAOSeancePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTCAOSeancePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to $obj2 (CommonTCAOSeance)
                $obj2->addCommonTCAOOrdreDuJourRelatedByDateSeance($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with their CommonTCAOCommissionConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTCAOCommissionConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;
        CommonTCAOCommissionConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTCAOCommissionConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTCAOCommissionConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTCAOCommissionConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTCAOCommissionConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to $obj2 (CommonTCAOCommissionConsultation)
                $obj2->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with their CommonTCAOCommission objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTCAOCommission(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;
        CommonTCAOCommissionPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTCAOCommissionPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTCAOCommissionPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTCAOCommissionPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTCAOCommissionPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to $obj2 (CommonTCAOCommission)
                $obj2->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with their CommonTCAOSeance objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTCAOSeanceRelatedByIdSeance(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;
        CommonTCAOSeancePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTCAOSeancePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTCAOSeancePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to $obj2 (CommonTCAOSeance)
                $obj2->addCommonTCAOOrdreDuJourRelatedByIdSeance($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with their CommonOrganisme objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonOrganisme(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;
        CommonOrganismePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonOrganismePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonOrganismePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to $obj2 (CommonOrganisme)
                $obj2->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol2 = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOSeancePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCAOSeancePeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTCAOCommissionConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTCAOCommissionPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOSeancePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTCAOSeancePeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonTCAOSeance rows

            $key2 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonTCAOSeancePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCAOSeancePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj2 (CommonTCAOSeance)
                $obj2->addCommonTCAOOrdreDuJourRelatedByDateSeance($obj1);
            } // if joined row not null

            // Add objects for joined CommonTCAOCommissionConsultation rows

            $key3 = CommonTCAOCommissionConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonTCAOCommissionConsultationPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonTCAOCommissionConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTCAOCommissionConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj3 (CommonTCAOCommissionConsultation)
                $obj3->addCommonTCAOOrdreDuJour($obj1);
            } // if joined row not null

            // Add objects for joined CommonTCAOCommission rows

            $key4 = CommonTCAOCommissionPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonTCAOCommissionPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonTCAOCommissionPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTCAOCommissionPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj4 (CommonTCAOCommission)
                $obj4->addCommonTCAOOrdreDuJour($obj1);
            } // if joined row not null

            // Add objects for joined CommonTCAOSeance rows

            $key5 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = CommonTCAOSeancePeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTCAOSeancePeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj5 (CommonTCAOSeance)
                $obj5->addCommonTCAOOrdreDuJourRelatedByIdSeance($obj1);
            } // if joined row not null

            // Add objects for joined CommonOrganisme rows

            $key6 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = CommonOrganismePeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonOrganismePeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj6 (CommonOrganisme)
                $obj6->addCommonTCAOOrdreDuJour($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCAOSeanceRelatedByDateSeance table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTCAOSeanceRelatedByDateSeance(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCAOCommissionConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTCAOCommissionConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCAOCommission table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTCAOCommission(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCAOSeanceRelatedByIdSeance table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTCAOSeanceRelatedByIdSeance(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOrganisme table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonOrganisme(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with all related objects except CommonTCAOSeanceRelatedByDateSeance.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTCAOSeanceRelatedByDateSeance(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol2 = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCAOCommissionConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTCAOCommissionPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTCAOCommissionConsultation rows

                $key2 = CommonTCAOCommissionConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTCAOCommissionConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTCAOCommissionConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCAOCommissionConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj2 (CommonTCAOCommissionConsultation)
                $obj2->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTCAOCommission rows

                $key3 = CommonTCAOCommissionPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTCAOCommissionPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTCAOCommissionPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTCAOCommissionPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj3 (CommonTCAOCommission)
                $obj3->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key4 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonOrganismePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonOrganismePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj4 (CommonOrganisme)
                $obj4->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with all related objects except CommonTCAOCommissionConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTCAOCommissionConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol2 = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOSeancePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCAOSeancePeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTCAOCommissionPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOSeancePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTCAOSeancePeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTCAOSeance rows

                $key2 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTCAOSeancePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCAOSeancePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj2 (CommonTCAOSeance)
                $obj2->addCommonTCAOOrdreDuJourRelatedByDateSeance($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTCAOCommission rows

                $key3 = CommonTCAOCommissionPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTCAOCommissionPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTCAOCommissionPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTCAOCommissionPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj3 (CommonTCAOCommission)
                $obj3->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTCAOSeance rows

                $key4 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTCAOSeancePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTCAOSeancePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj4 (CommonTCAOSeance)
                $obj4->addCommonTCAOOrdreDuJourRelatedByIdSeance($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key5 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOrganismePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOrganismePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj5 (CommonOrganisme)
                $obj5->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with all related objects except CommonTCAOCommission.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTCAOCommission(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol2 = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOSeancePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCAOSeancePeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTCAOCommissionConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOSeancePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTCAOSeancePeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTCAOSeance rows

                $key2 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTCAOSeancePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCAOSeancePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj2 (CommonTCAOSeance)
                $obj2->addCommonTCAOOrdreDuJourRelatedByDateSeance($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTCAOCommissionConsultation rows

                $key3 = CommonTCAOCommissionConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTCAOCommissionConsultationPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTCAOCommissionConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTCAOCommissionConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj3 (CommonTCAOCommissionConsultation)
                $obj3->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTCAOSeance rows

                $key4 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTCAOSeancePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTCAOSeancePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj4 (CommonTCAOSeance)
                $obj4->addCommonTCAOOrdreDuJourRelatedByIdSeance($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key5 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOrganismePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOrganismePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj5 (CommonOrganisme)
                $obj5->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with all related objects except CommonTCAOSeanceRelatedByIdSeance.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTCAOSeanceRelatedByIdSeance(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol2 = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCAOCommissionConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTCAOCommissionPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTCAOCommissionConsultation rows

                $key2 = CommonTCAOCommissionConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTCAOCommissionConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTCAOCommissionConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCAOCommissionConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj2 (CommonTCAOCommissionConsultation)
                $obj2->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTCAOCommission rows

                $key3 = CommonTCAOCommissionPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTCAOCommissionPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTCAOCommissionPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTCAOCommissionPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj3 (CommonTCAOCommission)
                $obj3->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key4 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonOrganismePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonOrganismePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj4 (CommonOrganisme)
                $obj4->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTCAOOrdreDuJour objects pre-filled with all related objects except CommonOrganisme.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTCAOOrdreDuJour objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonOrganisme(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        }

        CommonTCAOOrdreDuJourPeer::addSelectColumns($criteria);
        $startcol2 = CommonTCAOOrdreDuJourPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOSeancePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCAOSeancePeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTCAOCommissionConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOCommissionPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTCAOCommissionPeer::NUM_HYDRATE_COLUMNS;

        CommonTCAOSeancePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTCAOSeancePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::DATE_SEANCE, CommonTCAOSeancePeer::DATE, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION_CONSULTATION, CommonTCAOCommissionConsultationPeer::ID_COMMISSION_CONSULTATION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_COMMISSION, CommonTCAOCommissionPeer::ID_COMMISSION, $join_behavior);

        $criteria->addJoin(CommonTCAOOrdreDuJourPeer::ID_SEANCE, CommonTCAOSeancePeer::ID_SEANCE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTCAOOrdreDuJourPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTCAOOrdreDuJourPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTCAOOrdreDuJourPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTCAOSeance rows

                $key2 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTCAOSeancePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCAOSeancePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj2 (CommonTCAOSeance)
                $obj2->addCommonTCAOOrdreDuJourRelatedByDateSeance($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTCAOCommissionConsultation rows

                $key3 = CommonTCAOCommissionConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTCAOCommissionConsultationPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTCAOCommissionConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTCAOCommissionConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj3 (CommonTCAOCommissionConsultation)
                $obj3->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTCAOCommission rows

                $key4 = CommonTCAOCommissionPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTCAOCommissionPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTCAOCommissionPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTCAOCommissionPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj4 (CommonTCAOCommission)
                $obj4->addCommonTCAOOrdreDuJour($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTCAOSeance rows

                $key5 = CommonTCAOSeancePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTCAOSeancePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTCAOSeancePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTCAOSeancePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTCAOOrdreDuJour) to the collection in $obj5 (CommonTCAOSeance)
                $obj5->addCommonTCAOOrdreDuJourRelatedByIdSeance($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTCAOOrdreDuJourPeer::DATABASE_NAME)->getTable(CommonTCAOOrdreDuJourPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTCAOOrdreDuJourPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTCAOOrdreDuJourPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTCAOOrdreDuJourTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTCAOOrdreDuJourPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTCAOOrdreDuJour or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTCAOOrdreDuJour object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTCAOOrdreDuJour object
        }

        if ($criteria->containsKey(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR) && $criteria->keyContainsValue(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTCAOOrdreDuJour or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTCAOOrdreDuJour object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR);
            $value = $criteria->remove(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR);
            if ($value) {
                $selectCriteria->add(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(CommonTCAOOrdreDuJourPeer::ORGANISME);
            $value = $criteria->remove(CommonTCAOOrdreDuJourPeer::ORGANISME);
            if ($value) {
                $selectCriteria->add(CommonTCAOOrdreDuJourPeer::ORGANISME, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTCAOOrdreDuJourPeer::TABLE_NAME);
            }

        } else { // $values is CommonTCAOOrdreDuJour object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_CAO_Ordre_Du_Jour table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTCAOOrdreDuJourPeer::TABLE_NAME, $con, CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTCAOOrdreDuJourPeer::clearInstancePool();
            CommonTCAOOrdreDuJourPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTCAOOrdreDuJour or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTCAOOrdreDuJour object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTCAOOrdreDuJourPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTCAOOrdreDuJour) { // it's a model object
            // invalidate the cache for this single object
            CommonTCAOOrdreDuJourPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(CommonTCAOOrdreDuJourPeer::ORGANISME, $value[1]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                CommonTCAOOrdreDuJourPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTCAOOrdreDuJourPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTCAOOrdreDuJour object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTCAOOrdreDuJour $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTCAOOrdreDuJourPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, CommonTCAOOrdreDuJourPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   string $id_ordre_du_jour
     * @param   string $organisme
     * @param      PropelPDO $con
     * @return   CommonTCAOOrdreDuJour
     */
    public static function retrieveByPK($id_ordre_du_jour, $organisme, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $id_ordre_du_jour, (string) $organisme));
         if (null !== ($obj = CommonTCAOOrdreDuJourPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(CommonTCAOOrdreDuJourPeer::DATABASE_NAME);
        $criteria->add(CommonTCAOOrdreDuJourPeer::ID_ORDRE_DU_JOUR, $id_ordre_du_jour);
        $criteria->add(CommonTCAOOrdreDuJourPeer::ORGANISME, $organisme);
        $v = CommonTCAOOrdreDuJourPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BaseCommonTCAOOrdreDuJourPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTCAOOrdreDuJourPeer::buildTableMap();

