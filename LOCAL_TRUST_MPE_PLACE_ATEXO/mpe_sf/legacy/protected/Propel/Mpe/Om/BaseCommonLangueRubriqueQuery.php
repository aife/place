<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonLangue;
use Application\Propel\Mpe\CommonLangueRubrique;
use Application\Propel\Mpe\CommonLangueRubriquePeer;
use Application\Propel\Mpe\CommonLangueRubriqueQuery;
use Application\Propel\Mpe\CommonRubrique;

/**
 * Base class that represents a query for the 'langue_rubrique' table.
 *
 *
 *
 * @method CommonLangueRubriqueQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonLangueRubriqueQuery orderByLangueId($order = Criteria::ASC) Order by the langue_id column
 * @method CommonLangueRubriqueQuery orderByRubriqueId($order = Criteria::ASC) Order by the rubrique_id column
 * @method CommonLangueRubriqueQuery orderByNom($order = Criteria::ASC) Order by the nom column
 *
 * @method CommonLangueRubriqueQuery groupById() Group by the id column
 * @method CommonLangueRubriqueQuery groupByLangueId() Group by the langue_id column
 * @method CommonLangueRubriqueQuery groupByRubriqueId() Group by the rubrique_id column
 * @method CommonLangueRubriqueQuery groupByNom() Group by the nom column
 *
 * @method CommonLangueRubriqueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonLangueRubriqueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonLangueRubriqueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonLangueRubriqueQuery leftJoinCommonLangue($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonLangue relation
 * @method CommonLangueRubriqueQuery rightJoinCommonLangue($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonLangue relation
 * @method CommonLangueRubriqueQuery innerJoinCommonLangue($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonLangue relation
 *
 * @method CommonLangueRubriqueQuery leftJoinCommonRubrique($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonRubrique relation
 * @method CommonLangueRubriqueQuery rightJoinCommonRubrique($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonRubrique relation
 * @method CommonLangueRubriqueQuery innerJoinCommonRubrique($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonRubrique relation
 *
 * @method CommonLangueRubrique findOne(PropelPDO $con = null) Return the first CommonLangueRubrique matching the query
 * @method CommonLangueRubrique findOneOrCreate(PropelPDO $con = null) Return the first CommonLangueRubrique matching the query, or a new CommonLangueRubrique object populated from the query conditions when no match is found
 *
 * @method CommonLangueRubrique findOneByLangueId(int $langue_id) Return the first CommonLangueRubrique filtered by the langue_id column
 * @method CommonLangueRubrique findOneByRubriqueId(int $rubrique_id) Return the first CommonLangueRubrique filtered by the rubrique_id column
 * @method CommonLangueRubrique findOneByNom(string $nom) Return the first CommonLangueRubrique filtered by the nom column
 *
 * @method array findById(int $id) Return CommonLangueRubrique objects filtered by the id column
 * @method array findByLangueId(int $langue_id) Return CommonLangueRubrique objects filtered by the langue_id column
 * @method array findByRubriqueId(int $rubrique_id) Return CommonLangueRubrique objects filtered by the rubrique_id column
 * @method array findByNom(string $nom) Return CommonLangueRubrique objects filtered by the nom column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonLangueRubriqueQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonLangueRubriqueQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonLangueRubrique', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonLangueRubriqueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonLangueRubriqueQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonLangueRubriqueQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonLangueRubriqueQuery) {
            return $criteria;
        }
        $query = new CommonLangueRubriqueQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonLangueRubrique|CommonLangueRubrique[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonLangueRubriquePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonLangueRubriquePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonLangueRubrique A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonLangueRubrique A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `langue_id`, `rubrique_id`, `nom` FROM `langue_rubrique` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonLangueRubrique();
            $obj->hydrate($row);
            CommonLangueRubriquePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonLangueRubrique|CommonLangueRubrique[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonLangueRubrique[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonLangueRubriqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonLangueRubriquePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonLangueRubriqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonLangueRubriquePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonLangueRubriqueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonLangueRubriquePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonLangueRubriquePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonLangueRubriquePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the langue_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLangueId(1234); // WHERE langue_id = 1234
     * $query->filterByLangueId(array(12, 34)); // WHERE langue_id IN (12, 34)
     * $query->filterByLangueId(array('min' => 12)); // WHERE langue_id >= 12
     * $query->filterByLangueId(array('max' => 12)); // WHERE langue_id <= 12
     * </code>
     *
     * @see       filterByCommonLangue()
     *
     * @param     mixed $langueId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonLangueRubriqueQuery The current query, for fluid interface
     */
    public function filterByLangueId($langueId = null, $comparison = null)
    {
        if (is_array($langueId)) {
            $useMinMax = false;
            if (isset($langueId['min'])) {
                $this->addUsingAlias(CommonLangueRubriquePeer::LANGUE_ID, $langueId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($langueId['max'])) {
                $this->addUsingAlias(CommonLangueRubriquePeer::LANGUE_ID, $langueId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonLangueRubriquePeer::LANGUE_ID, $langueId, $comparison);
    }

    /**
     * Filter the query on the rubrique_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRubriqueId(1234); // WHERE rubrique_id = 1234
     * $query->filterByRubriqueId(array(12, 34)); // WHERE rubrique_id IN (12, 34)
     * $query->filterByRubriqueId(array('min' => 12)); // WHERE rubrique_id >= 12
     * $query->filterByRubriqueId(array('max' => 12)); // WHERE rubrique_id <= 12
     * </code>
     *
     * @see       filterByCommonRubrique()
     *
     * @param     mixed $rubriqueId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonLangueRubriqueQuery The current query, for fluid interface
     */
    public function filterByRubriqueId($rubriqueId = null, $comparison = null)
    {
        if (is_array($rubriqueId)) {
            $useMinMax = false;
            if (isset($rubriqueId['min'])) {
                $this->addUsingAlias(CommonLangueRubriquePeer::RUBRIQUE_ID, $rubriqueId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rubriqueId['max'])) {
                $this->addUsingAlias(CommonLangueRubriquePeer::RUBRIQUE_ID, $rubriqueId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonLangueRubriquePeer::RUBRIQUE_ID, $rubriqueId, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%'); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonLangueRubriqueQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nom)) {
                $nom = str_replace('*', '%', $nom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonLangueRubriquePeer::NOM, $nom, $comparison);
    }

    /**
     * Filter the query by a related CommonLangue object
     *
     * @param   CommonLangue|PropelObjectCollection $commonLangue The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonLangueRubriqueQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonLangue($commonLangue, $comparison = null)
    {
        if ($commonLangue instanceof CommonLangue) {
            return $this
                ->addUsingAlias(CommonLangueRubriquePeer::LANGUE_ID, $commonLangue->getIdLangue(), $comparison);
        } elseif ($commonLangue instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonLangueRubriquePeer::LANGUE_ID, $commonLangue->toKeyValue('PrimaryKey', 'IdLangue'), $comparison);
        } else {
            throw new PropelException('filterByCommonLangue() only accepts arguments of type CommonLangue or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonLangue relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonLangueRubriqueQuery The current query, for fluid interface
     */
    public function joinCommonLangue($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonLangue');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonLangue');
        }

        return $this;
    }

    /**
     * Use the CommonLangue relation CommonLangue object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonLangueQuery A secondary query class using the current class as primary query
     */
    public function useCommonLangueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonLangue($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonLangue', '\Application\Propel\Mpe\CommonLangueQuery');
    }

    /**
     * Filter the query by a related CommonRubrique object
     *
     * @param   CommonRubrique|PropelObjectCollection $commonRubrique The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonLangueRubriqueQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonRubrique($commonRubrique, $comparison = null)
    {
        if ($commonRubrique instanceof CommonRubrique) {
            return $this
                ->addUsingAlias(CommonLangueRubriquePeer::RUBRIQUE_ID, $commonRubrique->getId(), $comparison);
        } elseif ($commonRubrique instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonLangueRubriquePeer::RUBRIQUE_ID, $commonRubrique->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonRubrique() only accepts arguments of type CommonRubrique or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonRubrique relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonLangueRubriqueQuery The current query, for fluid interface
     */
    public function joinCommonRubrique($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonRubrique');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonRubrique');
        }

        return $this;
    }

    /**
     * Use the CommonRubrique relation CommonRubrique object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonRubriqueQuery A secondary query class using the current class as primary query
     */
    public function useCommonRubriqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonRubrique($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonRubrique', '\Application\Propel\Mpe\CommonRubriqueQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonLangueRubrique $commonLangueRubrique Object to remove from the list of results
     *
     * @return CommonLangueRubriqueQuery The current query, for fluid interface
     */
    public function prune($commonLangueRubrique = null)
    {
        if ($commonLangueRubrique) {
            $this->addUsingAlias(CommonLangueRubriquePeer::ID, $commonLangueRubrique->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
