<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonPanierEntreprise;
use Application\Propel\Mpe\CommonPanierEntreprisePeer;
use Application\Propel\Mpe\CommonPanierEntrepriseQuery;
use Application\Propel\Mpe\Entreprise;

/**
 * Base class that represents a query for the 'Panier_Entreprise' table.
 *
 *
 *
 * @method CommonPanierEntrepriseQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonPanierEntrepriseQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonPanierEntrepriseQuery orderByRefConsultation($order = Criteria::ASC) Order by the ref_consultation column
 * @method CommonPanierEntrepriseQuery orderByIdEntreprise($order = Criteria::ASC) Order by the id_entreprise column
 * @method CommonPanierEntrepriseQuery orderByOldIdInscrit($order = Criteria::ASC) Order by the old_id_inscrit column
 * @method CommonPanierEntrepriseQuery orderByDateAjout($order = Criteria::ASC) Order by the date_ajout column
 * @method CommonPanierEntrepriseQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 * @method CommonPanierEntrepriseQuery orderByIdInscrit($order = Criteria::ASC) Order by the id_inscrit column
 *
 * @method CommonPanierEntrepriseQuery groupById() Group by the id column
 * @method CommonPanierEntrepriseQuery groupByOrganisme() Group by the organisme column
 * @method CommonPanierEntrepriseQuery groupByRefConsultation() Group by the ref_consultation column
 * @method CommonPanierEntrepriseQuery groupByIdEntreprise() Group by the id_entreprise column
 * @method CommonPanierEntrepriseQuery groupByOldIdInscrit() Group by the old_id_inscrit column
 * @method CommonPanierEntrepriseQuery groupByDateAjout() Group by the date_ajout column
 * @method CommonPanierEntrepriseQuery groupByConsultationId() Group by the consultation_id column
 * @method CommonPanierEntrepriseQuery groupByIdInscrit() Group by the id_inscrit column
 *
 * @method CommonPanierEntrepriseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonPanierEntrepriseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonPanierEntrepriseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonPanierEntrepriseQuery leftJoinCommonInscrit($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonInscrit relation
 * @method CommonPanierEntrepriseQuery rightJoinCommonInscrit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonInscrit relation
 * @method CommonPanierEntrepriseQuery innerJoinCommonInscrit($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonInscrit relation
 *
 * @method CommonPanierEntrepriseQuery leftJoinEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entreprise relation
 * @method CommonPanierEntrepriseQuery rightJoinEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entreprise relation
 * @method CommonPanierEntrepriseQuery innerJoinEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the Entreprise relation
 *
 * @method CommonPanierEntrepriseQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonPanierEntrepriseQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonPanierEntrepriseQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonPanierEntreprise findOne(PropelPDO $con = null) Return the first CommonPanierEntreprise matching the query
 * @method CommonPanierEntreprise findOneOrCreate(PropelPDO $con = null) Return the first CommonPanierEntreprise matching the query, or a new CommonPanierEntreprise object populated from the query conditions when no match is found
 *
 * @method CommonPanierEntreprise findOneByOrganisme(string $organisme) Return the first CommonPanierEntreprise filtered by the organisme column
 * @method CommonPanierEntreprise findOneByRefConsultation(int $ref_consultation) Return the first CommonPanierEntreprise filtered by the ref_consultation column
 * @method CommonPanierEntreprise findOneByIdEntreprise(int $id_entreprise) Return the first CommonPanierEntreprise filtered by the id_entreprise column
 * @method CommonPanierEntreprise findOneByOldIdInscrit(int $old_id_inscrit) Return the first CommonPanierEntreprise filtered by the old_id_inscrit column
 * @method CommonPanierEntreprise findOneByDateAjout(string $date_ajout) Return the first CommonPanierEntreprise filtered by the date_ajout column
 * @method CommonPanierEntreprise findOneByConsultationId(int $consultation_id) Return the first CommonPanierEntreprise filtered by the consultation_id column
 * @method CommonPanierEntreprise findOneByIdInscrit(string $id_inscrit) Return the first CommonPanierEntreprise filtered by the id_inscrit column
 *
 * @method array findById(int $id) Return CommonPanierEntreprise objects filtered by the id column
 * @method array findByOrganisme(string $organisme) Return CommonPanierEntreprise objects filtered by the organisme column
 * @method array findByRefConsultation(int $ref_consultation) Return CommonPanierEntreprise objects filtered by the ref_consultation column
 * @method array findByIdEntreprise(int $id_entreprise) Return CommonPanierEntreprise objects filtered by the id_entreprise column
 * @method array findByOldIdInscrit(int $old_id_inscrit) Return CommonPanierEntreprise objects filtered by the old_id_inscrit column
 * @method array findByDateAjout(string $date_ajout) Return CommonPanierEntreprise objects filtered by the date_ajout column
 * @method array findByConsultationId(int $consultation_id) Return CommonPanierEntreprise objects filtered by the consultation_id column
 * @method array findByIdInscrit(string $id_inscrit) Return CommonPanierEntreprise objects filtered by the id_inscrit column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonPanierEntrepriseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonPanierEntrepriseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonPanierEntreprise', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonPanierEntrepriseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonPanierEntrepriseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonPanierEntrepriseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonPanierEntrepriseQuery) {
            return $criteria;
        }
        $query = new CommonPanierEntrepriseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonPanierEntreprise|CommonPanierEntreprise[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonPanierEntreprisePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonPanierEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonPanierEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonPanierEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `organisme`, `ref_consultation`, `id_entreprise`, `old_id_inscrit`, `date_ajout`, `consultation_id`, `id_inscrit` FROM `Panier_Entreprise` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonPanierEntreprise();
            $obj->hydrate($row);
            CommonPanierEntreprisePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonPanierEntreprise|CommonPanierEntreprise[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonPanierEntreprise[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonPanierEntreprisePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonPanierEntreprisePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonPanierEntreprisePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPanierEntreprisePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the ref_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByRefConsultation(1234); // WHERE ref_consultation = 1234
     * $query->filterByRefConsultation(array(12, 34)); // WHERE ref_consultation IN (12, 34)
     * $query->filterByRefConsultation(array('min' => 12)); // WHERE ref_consultation >= 12
     * $query->filterByRefConsultation(array('max' => 12)); // WHERE ref_consultation <= 12
     * </code>
     *
     * @param     mixed $refConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterByRefConsultation($refConsultation = null, $comparison = null)
    {
        if (is_array($refConsultation)) {
            $useMinMax = false;
            if (isset($refConsultation['min'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::REF_CONSULTATION, $refConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refConsultation['max'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::REF_CONSULTATION, $refConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonPanierEntreprisePeer::REF_CONSULTATION, $refConsultation, $comparison);
    }

    /**
     * Filter the query on the id_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntreprise(1234); // WHERE id_entreprise = 1234
     * $query->filterByIdEntreprise(array(12, 34)); // WHERE id_entreprise IN (12, 34)
     * $query->filterByIdEntreprise(array('min' => 12)); // WHERE id_entreprise >= 12
     * $query->filterByIdEntreprise(array('max' => 12)); // WHERE id_entreprise <= 12
     * </code>
     *
     * @see       filterByEntreprise()
     *
     * @param     mixed $idEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdEntreprise($idEntreprise = null, $comparison = null)
    {
        if (is_array($idEntreprise)) {
            $useMinMax = false;
            if (isset($idEntreprise['min'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::ID_ENTREPRISE, $idEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntreprise['max'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::ID_ENTREPRISE, $idEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonPanierEntreprisePeer::ID_ENTREPRISE, $idEntreprise, $comparison);
    }

    /**
     * Filter the query on the old_id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdInscrit(1234); // WHERE old_id_inscrit = 1234
     * $query->filterByOldIdInscrit(array(12, 34)); // WHERE old_id_inscrit IN (12, 34)
     * $query->filterByOldIdInscrit(array('min' => 12)); // WHERE old_id_inscrit >= 12
     * $query->filterByOldIdInscrit(array('max' => 12)); // WHERE old_id_inscrit <= 12
     * </code>
     *
     * @param     mixed $oldIdInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterByOldIdInscrit($oldIdInscrit = null, $comparison = null)
    {
        if (is_array($oldIdInscrit)) {
            $useMinMax = false;
            if (isset($oldIdInscrit['min'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::OLD_ID_INSCRIT, $oldIdInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdInscrit['max'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::OLD_ID_INSCRIT, $oldIdInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonPanierEntreprisePeer::OLD_ID_INSCRIT, $oldIdInscrit, $comparison);
    }

    /**
     * Filter the query on the date_ajout column
     *
     * Example usage:
     * <code>
     * $query->filterByDateAjout('fooValue');   // WHERE date_ajout = 'fooValue'
     * $query->filterByDateAjout('%fooValue%'); // WHERE date_ajout LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dateAjout The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterByDateAjout($dateAjout = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dateAjout)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dateAjout)) {
                $dateAjout = str_replace('*', '%', $dateAjout);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonPanierEntreprisePeer::DATE_AJOUT, $dateAjout, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultation()
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonPanierEntreprisePeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query on the id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInscrit(1234); // WHERE id_inscrit = 1234
     * $query->filterByIdInscrit(array(12, 34)); // WHERE id_inscrit IN (12, 34)
     * $query->filterByIdInscrit(array('min' => 12)); // WHERE id_inscrit >= 12
     * $query->filterByIdInscrit(array('max' => 12)); // WHERE id_inscrit <= 12
     * </code>
     *
     * @see       filterByCommonInscrit()
     *
     * @param     mixed $idInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdInscrit($idInscrit = null, $comparison = null)
    {
        if (is_array($idInscrit)) {
            $useMinMax = false;
            if (isset($idInscrit['min'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::ID_INSCRIT, $idInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInscrit['max'])) {
                $this->addUsingAlias(CommonPanierEntreprisePeer::ID_INSCRIT, $idInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonPanierEntreprisePeer::ID_INSCRIT, $idInscrit, $comparison);
    }

    /**
     * Filter the query by a related CommonInscrit object
     *
     * @param   CommonInscrit|PropelObjectCollection $commonInscrit The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPanierEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonInscrit($commonInscrit, $comparison = null)
    {
        if ($commonInscrit instanceof CommonInscrit) {
            return $this
                ->addUsingAlias(CommonPanierEntreprisePeer::ID_INSCRIT, $commonInscrit->getId(), $comparison);
        } elseif ($commonInscrit instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonPanierEntreprisePeer::ID_INSCRIT, $commonInscrit->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonInscrit() only accepts arguments of type CommonInscrit or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonInscrit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonInscrit($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonInscrit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonInscrit');
        }

        return $this;
    }

    /**
     * Use the CommonInscrit relation CommonInscrit object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonInscritQuery A secondary query class using the current class as primary query
     */
    public function useCommonInscritQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonInscrit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonInscrit', '\Application\Propel\Mpe\CommonInscritQuery');
    }

    /**
     * Filter the query by a related Entreprise object
     *
     * @param   Entreprise|PropelObjectCollection $entreprise The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPanierEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByEntreprise($entreprise, $comparison = null)
    {
        if ($entreprise instanceof Entreprise) {
            return $this
                ->addUsingAlias(CommonPanierEntreprisePeer::ID_ENTREPRISE, $entreprise->getId(), $comparison);
        } elseif ($entreprise instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonPanierEntreprisePeer::ID_ENTREPRISE, $entreprise->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEntreprise() only accepts arguments of type Entreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function joinEntreprise($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entreprise');
        }

        return $this;
    }

    /**
     * Use the Entreprise relation Entreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\EntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useEntrepriseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entreprise', '\Application\Propel\Mpe\EntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonPanierEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonPanierEntreprisePeer::CONSULTATION_ID, $commonConsultation->getId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonPanierEntreprisePeer::CONSULTATION_ID, $commonConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonPanierEntreprise $commonPanierEntreprise Object to remove from the list of results
     *
     * @return CommonPanierEntrepriseQuery The current query, for fluid interface
     */
    public function prune($commonPanierEntreprise = null)
    {
        if ($commonPanierEntreprise) {
            $this->addUsingAlias(CommonPanierEntreprisePeer::ID, $commonPanierEntreprise->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
