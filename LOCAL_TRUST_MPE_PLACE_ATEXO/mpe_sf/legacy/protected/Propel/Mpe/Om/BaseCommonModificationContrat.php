<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonModificationContratPeer;
use Application\Propel\Mpe\CommonModificationContratQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;

/**
 * Base class that represents a row from the 'modification_contrat' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonModificationContrat extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonModificationContratPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonModificationContratPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the id_contrat_titulaire field.
     * @var        int
     */
    protected $id_contrat_titulaire;

    /**
     * The value for the num_ordre field.
     * @var        int
     */
    protected $num_ordre;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the id_agent field.
     * @var        int
     */
    protected $id_agent;

    /**
     * The value for the objet_modification field.
     * @var        string
     */
    protected $objet_modification;

    /**
     * The value for the date_signature field.
     * @var        string
     */
    protected $date_signature;

    /**
     * The value for the montant field.
     * @var        string
     */
    protected $montant;

    /**
     * The value for the id_etablissement field.
     * @var        int
     */
    protected $id_etablissement;

    /**
     * The value for the duree_marche field.
     * @var        int
     */
    protected $duree_marche;

    /**
     * The value for the statut_publication_sn field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $statut_publication_sn;

    /**
     * The value for the date_publication_sn field.
     * @var        string
     */
    protected $date_publication_sn;

    /**
     * The value for the erreur_sn field.
     * @var        string
     */
    protected $erreur_sn;

    /**
     * The value for the date_modification_sn field.
     * @var        string
     */
    protected $date_modification_sn;

    /**
     * @var        CommonAgent
     */
    protected $aCommonAgent;

    /**
     * @var        CommonTContratTitulaire
     */
    protected $aCommonTContratTitulaire;

    /**
     * @var        CommonTEtablissement
     */
    protected $aCommonTEtablissement;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->statut_publication_sn = 0;
    }

    /**
     * Initializes internal state of BaseCommonModificationContrat object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [id_contrat_titulaire] column value.
     *
     * @return int
     */
    public function getIdContratTitulaire()
    {

        return $this->id_contrat_titulaire;
    }

    /**
     * Get the [num_ordre] column value.
     *
     * @return int
     */
    public function getNumOrdre()
    {

        return $this->num_ordre;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_modification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModification($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification === null) {
            return null;
        }

        if ($this->date_modification === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [id_agent] column value.
     *
     * @return int
     */
    public function getIdAgent()
    {

        return $this->id_agent;
    }

    /**
     * Get the [objet_modification] column value.
     *
     * @return string
     */
    public function getObjetModification()
    {

        return $this->objet_modification;
    }

    /**
     * Get the [optionally formatted] temporal [date_signature] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateSignature($format = 'Y-m-d H:i:s')
    {
        if ($this->date_signature === null) {
            return null;
        }

        if ($this->date_signature === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_signature);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_signature, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [montant] column value.
     *
     * @return string
     */
    public function getMontant()
    {

        return $this->montant;
    }

    /**
     * Get the [id_etablissement] column value.
     *
     * @return int
     */
    public function getIdEtablissement()
    {

        return $this->id_etablissement;
    }

    /**
     * Get the [duree_marche] column value.
     *
     * @return int
     */
    public function getDureeMarche()
    {

        return $this->duree_marche;
    }

    /**
     * Get the [statut_publication_sn] column value.
     *
     * @return int
     */
    public function getStatutPublicationSn()
    {

        return $this->statut_publication_sn;
    }

    /**
     * Get the [optionally formatted] temporal [date_publication_sn] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatePublicationSn($format = 'Y-m-d H:i:s')
    {
        if ($this->date_publication_sn === null) {
            return null;
        }

        if ($this->date_publication_sn === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_publication_sn);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_publication_sn, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [erreur_sn] column value.
     *
     * @return string
     */
    public function getErreurSn()
    {

        return $this->erreur_sn;
    }

    /**
     * Get the [optionally formatted] temporal [date_modification_sn] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModificationSn($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification_sn === null) {
            return null;
        }

        if ($this->date_modification_sn === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification_sn);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification_sn, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [id_contrat_titulaire] column.
     *
     * @param int $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setIdContratTitulaire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_contrat_titulaire !== $v) {
            $this->id_contrat_titulaire = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::ID_CONTRAT_TITULAIRE;
        }

        if ($this->aCommonTContratTitulaire !== null && $this->aCommonTContratTitulaire->getIdContratTitulaire() !== $v) {
            $this->aCommonTContratTitulaire = null;
        }


        return $this;
    } // setIdContratTitulaire()

    /**
     * Set the value of [num_ordre] column.
     *
     * @param int $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setNumOrdre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->num_ordre !== $v) {
            $this->num_ordre = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::NUM_ORDRE;
        }


        return $this;
    } // setNumOrdre()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonModificationContratPeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Sets the value of [date_modification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification !== null && $tmpDt = new DateTime($this->date_modification)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification = $newDateAsString;
                $this->modifiedColumns[] = CommonModificationContratPeer::DATE_MODIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDateModification()

    /**
     * Set the value of [id_agent] column.
     *
     * @param int $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setIdAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_agent !== $v) {
            $this->id_agent = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::ID_AGENT;
        }

        if ($this->aCommonAgent !== null && $this->aCommonAgent->getId() !== $v) {
            $this->aCommonAgent = null;
        }


        return $this;
    } // setIdAgent()

    /**
     * Set the value of [objet_modification] column.
     *
     * @param string $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setObjetModification($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->objet_modification !== $v) {
            $this->objet_modification = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::OBJET_MODIFICATION;
        }


        return $this;
    } // setObjetModification()

    /**
     * Sets the value of [date_signature] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setDateSignature($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_signature !== null || $dt !== null) {
            $currentDateAsString = ($this->date_signature !== null && $tmpDt = new DateTime($this->date_signature)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_signature = $newDateAsString;
                $this->modifiedColumns[] = CommonModificationContratPeer::DATE_SIGNATURE;
            }
        } // if either are not null


        return $this;
    } // setDateSignature()

    /**
     * Set the value of [montant] column.
     *
     * @param string $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setMontant($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->montant !== $v) {
            $this->montant = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::MONTANT;
        }


        return $this;
    } // setMontant()

    /**
     * Set the value of [id_etablissement] column.
     *
     * @param int $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setIdEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_etablissement !== $v) {
            $this->id_etablissement = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::ID_ETABLISSEMENT;
        }

        if ($this->aCommonTEtablissement !== null && $this->aCommonTEtablissement->getIdEtablissement() !== $v) {
            $this->aCommonTEtablissement = null;
        }


        return $this;
    } // setIdEtablissement()

    /**
     * Set the value of [duree_marche] column.
     *
     * @param int $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setDureeMarche($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->duree_marche !== $v) {
            $this->duree_marche = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::DUREE_MARCHE;
        }


        return $this;
    } // setDureeMarche()

    /**
     * Set the value of [statut_publication_sn] column.
     *
     * @param int $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setStatutPublicationSn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->statut_publication_sn !== $v) {
            $this->statut_publication_sn = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::STATUT_PUBLICATION_SN;
        }


        return $this;
    } // setStatutPublicationSn()

    /**
     * Sets the value of [date_publication_sn] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setDatePublicationSn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_publication_sn !== null || $dt !== null) {
            $currentDateAsString = ($this->date_publication_sn !== null && $tmpDt = new DateTime($this->date_publication_sn)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_publication_sn = $newDateAsString;
                $this->modifiedColumns[] = CommonModificationContratPeer::DATE_PUBLICATION_SN;
            }
        } // if either are not null


        return $this;
    } // setDatePublicationSn()

    /**
     * Set the value of [erreur_sn] column.
     *
     * @param string $v new value
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setErreurSn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->erreur_sn !== $v) {
            $this->erreur_sn = $v;
            $this->modifiedColumns[] = CommonModificationContratPeer::ERREUR_SN;
        }


        return $this;
    } // setErreurSn()

    /**
     * Sets the value of [date_modification_sn] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonModificationContrat The current object (for fluent API support)
     */
    public function setDateModificationSn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification_sn !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification_sn !== null && $tmpDt = new DateTime($this->date_modification_sn)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification_sn = $newDateAsString;
                $this->modifiedColumns[] = CommonModificationContratPeer::DATE_MODIFICATION_SN;
            }
        } // if either are not null


        return $this;
    } // setDateModificationSn()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->statut_publication_sn !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_contrat_titulaire = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->num_ordre = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->date_creation = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->date_modification = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->id_agent = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->objet_modification = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->date_signature = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->montant = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->id_etablissement = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->duree_marche = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->statut_publication_sn = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->date_publication_sn = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->erreur_sn = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->date_modification_sn = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 15; // 15 = CommonModificationContratPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonModificationContrat object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTContratTitulaire !== null && $this->id_contrat_titulaire !== $this->aCommonTContratTitulaire->getIdContratTitulaire()) {
            $this->aCommonTContratTitulaire = null;
        }
        if ($this->aCommonAgent !== null && $this->id_agent !== $this->aCommonAgent->getId()) {
            $this->aCommonAgent = null;
        }
        if ($this->aCommonTEtablissement !== null && $this->id_etablissement !== $this->aCommonTEtablissement->getIdEtablissement()) {
            $this->aCommonTEtablissement = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonModificationContratPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonAgent = null;
            $this->aCommonTContratTitulaire = null;
            $this->aCommonTEtablissement = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonModificationContratQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonModificationContratPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonAgent !== null) {
                if ($this->aCommonAgent->isModified() || $this->aCommonAgent->isNew()) {
                    $affectedRows += $this->aCommonAgent->save($con);
                }
                $this->setCommonAgent($this->aCommonAgent);
            }

            if ($this->aCommonTContratTitulaire !== null) {
                if ($this->aCommonTContratTitulaire->isModified() || $this->aCommonTContratTitulaire->isNew()) {
                    $affectedRows += $this->aCommonTContratTitulaire->save($con);
                }
                $this->setCommonTContratTitulaire($this->aCommonTContratTitulaire);
            }

            if ($this->aCommonTEtablissement !== null) {
                if ($this->aCommonTEtablissement->isModified() || $this->aCommonTEtablissement->isNew()) {
                    $affectedRows += $this->aCommonTEtablissement->save($con);
                }
                $this->setCommonTEtablissement($this->aCommonTEtablissement);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonModificationContratPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonModificationContratPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonModificationContratPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_contrat_titulaire`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::NUM_ORDRE)) {
            $modifiedColumns[':p' . $index++]  = '`num_ordre`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::ID_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_agent`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::OBJET_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`objet_modification`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_SIGNATURE)) {
            $modifiedColumns[':p' . $index++]  = '`date_signature`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::MONTANT)) {
            $modifiedColumns[':p' . $index++]  = '`montant`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::ID_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_etablissement`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::DUREE_MARCHE)) {
            $modifiedColumns[':p' . $index++]  = '`duree_marche`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::STATUT_PUBLICATION_SN)) {
            $modifiedColumns[':p' . $index++]  = '`statut_publication_sn`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_PUBLICATION_SN)) {
            $modifiedColumns[':p' . $index++]  = '`date_publication_sn`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::ERREUR_SN)) {
            $modifiedColumns[':p' . $index++]  = '`erreur_sn`';
        }
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_MODIFICATION_SN)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification_sn`';
        }

        $sql = sprintf(
            'INSERT INTO `modification_contrat` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`id_contrat_titulaire`':
                        $stmt->bindValue($identifier, $this->id_contrat_titulaire, PDO::PARAM_INT);
                        break;
                    case '`num_ordre`':
                        $stmt->bindValue($identifier, $this->num_ordre, PDO::PARAM_INT);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`id_agent`':
                        $stmt->bindValue($identifier, $this->id_agent, PDO::PARAM_INT);
                        break;
                    case '`objet_modification`':
                        $stmt->bindValue($identifier, $this->objet_modification, PDO::PARAM_STR);
                        break;
                    case '`date_signature`':
                        $stmt->bindValue($identifier, $this->date_signature, PDO::PARAM_STR);
                        break;
                    case '`montant`':
                        $stmt->bindValue($identifier, $this->montant, PDO::PARAM_STR);
                        break;
                    case '`id_etablissement`':
                        $stmt->bindValue($identifier, $this->id_etablissement, PDO::PARAM_INT);
                        break;
                    case '`duree_marche`':
                        $stmt->bindValue($identifier, $this->duree_marche, PDO::PARAM_INT);
                        break;
                    case '`statut_publication_sn`':
                        $stmt->bindValue($identifier, $this->statut_publication_sn, PDO::PARAM_INT);
                        break;
                    case '`date_publication_sn`':
                        $stmt->bindValue($identifier, $this->date_publication_sn, PDO::PARAM_STR);
                        break;
                    case '`erreur_sn`':
                        $stmt->bindValue($identifier, $this->erreur_sn, PDO::PARAM_STR);
                        break;
                    case '`date_modification_sn`':
                        $stmt->bindValue($identifier, $this->date_modification_sn, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonAgent !== null) {
                if (!$this->aCommonAgent->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonAgent->getValidationFailures());
                }
            }

            if ($this->aCommonTContratTitulaire !== null) {
                if (!$this->aCommonTContratTitulaire->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTContratTitulaire->getValidationFailures());
                }
            }

            if ($this->aCommonTEtablissement !== null) {
                if (!$this->aCommonTEtablissement->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTEtablissement->getValidationFailures());
                }
            }


            if (($retval = CommonModificationContratPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonModificationContratPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIdContratTitulaire();
                break;
            case 2:
                return $this->getNumOrdre();
                break;
            case 3:
                return $this->getDateCreation();
                break;
            case 4:
                return $this->getDateModification();
                break;
            case 5:
                return $this->getIdAgent();
                break;
            case 6:
                return $this->getObjetModification();
                break;
            case 7:
                return $this->getDateSignature();
                break;
            case 8:
                return $this->getMontant();
                break;
            case 9:
                return $this->getIdEtablissement();
                break;
            case 10:
                return $this->getDureeMarche();
                break;
            case 11:
                return $this->getStatutPublicationSn();
                break;
            case 12:
                return $this->getDatePublicationSn();
                break;
            case 13:
                return $this->getErreurSn();
                break;
            case 14:
                return $this->getDateModificationSn();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonModificationContrat'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonModificationContrat'][$this->getPrimaryKey()] = true;
        $keys = CommonModificationContratPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIdContratTitulaire(),
            $keys[2] => $this->getNumOrdre(),
            $keys[3] => $this->getDateCreation(),
            $keys[4] => $this->getDateModification(),
            $keys[5] => $this->getIdAgent(),
            $keys[6] => $this->getObjetModification(),
            $keys[7] => $this->getDateSignature(),
            $keys[8] => $this->getMontant(),
            $keys[9] => $this->getIdEtablissement(),
            $keys[10] => $this->getDureeMarche(),
            $keys[11] => $this->getStatutPublicationSn(),
            $keys[12] => $this->getDatePublicationSn(),
            $keys[13] => $this->getErreurSn(),
            $keys[14] => $this->getDateModificationSn(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonAgent) {
                $result['CommonAgent'] = $this->aCommonAgent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTContratTitulaire) {
                $result['CommonTContratTitulaire'] = $this->aCommonTContratTitulaire->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTEtablissement) {
                $result['CommonTEtablissement'] = $this->aCommonTEtablissement->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonModificationContratPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIdContratTitulaire($value);
                break;
            case 2:
                $this->setNumOrdre($value);
                break;
            case 3:
                $this->setDateCreation($value);
                break;
            case 4:
                $this->setDateModification($value);
                break;
            case 5:
                $this->setIdAgent($value);
                break;
            case 6:
                $this->setObjetModification($value);
                break;
            case 7:
                $this->setDateSignature($value);
                break;
            case 8:
                $this->setMontant($value);
                break;
            case 9:
                $this->setIdEtablissement($value);
                break;
            case 10:
                $this->setDureeMarche($value);
                break;
            case 11:
                $this->setStatutPublicationSn($value);
                break;
            case 12:
                $this->setDatePublicationSn($value);
                break;
            case 13:
                $this->setErreurSn($value);
                break;
            case 14:
                $this->setDateModificationSn($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonModificationContratPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdContratTitulaire($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNumOrdre($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDateCreation($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDateModification($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setIdAgent($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setObjetModification($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDateSignature($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setMontant($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setIdEtablissement($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setDureeMarche($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setStatutPublicationSn($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setDatePublicationSn($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setErreurSn($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setDateModificationSn($arr[$keys[14]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonModificationContratPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonModificationContratPeer::ID)) $criteria->add(CommonModificationContratPeer::ID, $this->id);
        if ($this->isColumnModified(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE)) $criteria->add(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, $this->id_contrat_titulaire);
        if ($this->isColumnModified(CommonModificationContratPeer::NUM_ORDRE)) $criteria->add(CommonModificationContratPeer::NUM_ORDRE, $this->num_ordre);
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_CREATION)) $criteria->add(CommonModificationContratPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_MODIFICATION)) $criteria->add(CommonModificationContratPeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonModificationContratPeer::ID_AGENT)) $criteria->add(CommonModificationContratPeer::ID_AGENT, $this->id_agent);
        if ($this->isColumnModified(CommonModificationContratPeer::OBJET_MODIFICATION)) $criteria->add(CommonModificationContratPeer::OBJET_MODIFICATION, $this->objet_modification);
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_SIGNATURE)) $criteria->add(CommonModificationContratPeer::DATE_SIGNATURE, $this->date_signature);
        if ($this->isColumnModified(CommonModificationContratPeer::MONTANT)) $criteria->add(CommonModificationContratPeer::MONTANT, $this->montant);
        if ($this->isColumnModified(CommonModificationContratPeer::ID_ETABLISSEMENT)) $criteria->add(CommonModificationContratPeer::ID_ETABLISSEMENT, $this->id_etablissement);
        if ($this->isColumnModified(CommonModificationContratPeer::DUREE_MARCHE)) $criteria->add(CommonModificationContratPeer::DUREE_MARCHE, $this->duree_marche);
        if ($this->isColumnModified(CommonModificationContratPeer::STATUT_PUBLICATION_SN)) $criteria->add(CommonModificationContratPeer::STATUT_PUBLICATION_SN, $this->statut_publication_sn);
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_PUBLICATION_SN)) $criteria->add(CommonModificationContratPeer::DATE_PUBLICATION_SN, $this->date_publication_sn);
        if ($this->isColumnModified(CommonModificationContratPeer::ERREUR_SN)) $criteria->add(CommonModificationContratPeer::ERREUR_SN, $this->erreur_sn);
        if ($this->isColumnModified(CommonModificationContratPeer::DATE_MODIFICATION_SN)) $criteria->add(CommonModificationContratPeer::DATE_MODIFICATION_SN, $this->date_modification_sn);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonModificationContratPeer::DATABASE_NAME);
        $criteria->add(CommonModificationContratPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonModificationContrat (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdContratTitulaire($this->getIdContratTitulaire());
        $copyObj->setNumOrdre($this->getNumOrdre());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setIdAgent($this->getIdAgent());
        $copyObj->setObjetModification($this->getObjetModification());
        $copyObj->setDateSignature($this->getDateSignature());
        $copyObj->setMontant($this->getMontant());
        $copyObj->setIdEtablissement($this->getIdEtablissement());
        $copyObj->setDureeMarche($this->getDureeMarche());
        $copyObj->setStatutPublicationSn($this->getStatutPublicationSn());
        $copyObj->setDatePublicationSn($this->getDatePublicationSn());
        $copyObj->setErreurSn($this->getErreurSn());
        $copyObj->setDateModificationSn($this->getDateModificationSn());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonModificationContrat Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonModificationContratPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonModificationContratPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonAgent object.
     *
     * @param   CommonAgent $v
     * @return CommonModificationContrat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonAgent(CommonAgent $v = null)
    {
        if ($v === null) {
            $this->setIdAgent(NULL);
        } else {
            $this->setIdAgent($v->getId());
        }

        $this->aCommonAgent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonAgent object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonModificationContrat($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonAgent object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonAgent The associated CommonAgent object.
     * @throws PropelException
     */
    public function getCommonAgent(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonAgent === null && ($this->id_agent !== null) && $doQuery) {
            $this->aCommonAgent = CommonAgentQuery::create()->findPk($this->id_agent, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonAgent->addCommonModificationContrats($this);
             */
        }

        return $this->aCommonAgent;
    }

    /**
     * Declares an association between this object and a CommonTContratTitulaire object.
     *
     * @param   CommonTContratTitulaire $v
     * @return CommonModificationContrat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTContratTitulaire(CommonTContratTitulaire $v = null)
    {
        if ($v === null) {
            $this->setIdContratTitulaire(NULL);
        } else {
            $this->setIdContratTitulaire($v->getIdContratTitulaire());
        }

        $this->aCommonTContratTitulaire = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTContratTitulaire object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonModificationContrat($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTContratTitulaire object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTContratTitulaire The associated CommonTContratTitulaire object.
     * @throws PropelException
     */
    public function getCommonTContratTitulaire(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTContratTitulaire === null && ($this->id_contrat_titulaire !== null) && $doQuery) {
            $this->aCommonTContratTitulaire = CommonTContratTitulaireQuery::create()->findPk($this->id_contrat_titulaire, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTContratTitulaire->addCommonModificationContrats($this);
             */
        }

        return $this->aCommonTContratTitulaire;
    }

    /**
     * Declares an association between this object and a CommonTEtablissement object.
     *
     * @param   CommonTEtablissement $v
     * @return CommonModificationContrat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTEtablissement(CommonTEtablissement $v = null)
    {
        if ($v === null) {
            $this->setIdEtablissement(NULL);
        } else {
            $this->setIdEtablissement($v->getIdEtablissement());
        }

        $this->aCommonTEtablissement = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTEtablissement object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonModificationContrat($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTEtablissement object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTEtablissement The associated CommonTEtablissement object.
     * @throws PropelException
     */
    public function getCommonTEtablissement(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTEtablissement === null && ($this->id_etablissement !== null) && $doQuery) {
            $this->aCommonTEtablissement = CommonTEtablissementQuery::create()->findPk($this->id_etablissement, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTEtablissement->addCommonModificationContrats($this);
             */
        }

        return $this->aCommonTEtablissement;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->id_contrat_titulaire = null;
        $this->num_ordre = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->id_agent = null;
        $this->objet_modification = null;
        $this->date_signature = null;
        $this->montant = null;
        $this->id_etablissement = null;
        $this->duree_marche = null;
        $this->statut_publication_sn = null;
        $this->date_publication_sn = null;
        $this->erreur_sn = null;
        $this->date_modification_sn = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonAgent instanceof Persistent) {
              $this->aCommonAgent->clearAllReferences($deep);
            }
            if ($this->aCommonTContratTitulaire instanceof Persistent) {
              $this->aCommonTContratTitulaire->clearAllReferences($deep);
            }
            if ($this->aCommonTEtablissement instanceof Persistent) {
              $this->aCommonTEtablissement->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonAgent = null;
        $this->aCommonTContratTitulaire = null;
        $this->aCommonTEtablissement = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonModificationContratPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
