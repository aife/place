<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonEnchereEntreprisePmi;
use Application\Propel\Mpe\CommonEnchereOffre;
use Application\Propel\Mpe\CommonEnchereOffrePeer;
use Application\Propel\Mpe\CommonEnchereOffreQuery;
use Application\Propel\Mpe\CommonEnchereOffreReference;
use Application\Propel\Mpe\CommonEncherePmi;

/**
 * Base class that represents a query for the 'EnchereOffre' table.
 *
 *
 *
 * @method CommonEnchereOffreQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method CommonEnchereOffreQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonEnchereOffreQuery orderByOldIdEnchere($order = Criteria::ASC) Order by the old_id_enchere column
 * @method CommonEnchereOffreQuery orderByOldIdEnchereEntreprise($order = Criteria::ASC) Order by the old_id_enchere_entreprise column
 * @method CommonEnchereOffreQuery orderByDate($order = Criteria::ASC) Order by the date column
 * @method CommonEnchereOffreQuery orderByValeurtic($order = Criteria::ASC) Order by the valeurTIC column
 * @method CommonEnchereOffreQuery orderByValeurtc($order = Criteria::ASC) Order by the valeurTC column
 * @method CommonEnchereOffreQuery orderByValeurnetc($order = Criteria::ASC) Order by the valeurNETC column
 * @method CommonEnchereOffreQuery orderByValeurngc($order = Criteria::ASC) Order by the valeurNGC column
 * @method CommonEnchereOffreQuery orderByRang($order = Criteria::ASC) Order by the rang column
 * @method CommonEnchereOffreQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonEnchereOffreQuery orderByIdenchere($order = Criteria::ASC) Order by the idEnchere column
 * @method CommonEnchereOffreQuery orderByIdenchereentreprise($order = Criteria::ASC) Order by the idEnchereEntreprise column
 *
 * @method CommonEnchereOffreQuery groupByOldId() Group by the old_id column
 * @method CommonEnchereOffreQuery groupByOrganisme() Group by the organisme column
 * @method CommonEnchereOffreQuery groupByOldIdEnchere() Group by the old_id_enchere column
 * @method CommonEnchereOffreQuery groupByOldIdEnchereEntreprise() Group by the old_id_enchere_entreprise column
 * @method CommonEnchereOffreQuery groupByDate() Group by the date column
 * @method CommonEnchereOffreQuery groupByValeurtic() Group by the valeurTIC column
 * @method CommonEnchereOffreQuery groupByValeurtc() Group by the valeurTC column
 * @method CommonEnchereOffreQuery groupByValeurnetc() Group by the valeurNETC column
 * @method CommonEnchereOffreQuery groupByValeurngc() Group by the valeurNGC column
 * @method CommonEnchereOffreQuery groupByRang() Group by the rang column
 * @method CommonEnchereOffreQuery groupById() Group by the id column
 * @method CommonEnchereOffreQuery groupByIdenchere() Group by the idEnchere column
 * @method CommonEnchereOffreQuery groupByIdenchereentreprise() Group by the idEnchereEntreprise column
 *
 * @method CommonEnchereOffreQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonEnchereOffreQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonEnchereOffreQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonEnchereOffreQuery leftJoinCommonEncherePmi($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEncherePmi relation
 * @method CommonEnchereOffreQuery rightJoinCommonEncherePmi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEncherePmi relation
 * @method CommonEnchereOffreQuery innerJoinCommonEncherePmi($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEncherePmi relation
 *
 * @method CommonEnchereOffreQuery leftJoinCommonEnchereEntreprisePmi($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEnchereEntreprisePmi relation
 * @method CommonEnchereOffreQuery rightJoinCommonEnchereEntreprisePmi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEnchereEntreprisePmi relation
 * @method CommonEnchereOffreQuery innerJoinCommonEnchereEntreprisePmi($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEnchereEntreprisePmi relation
 *
 * @method CommonEnchereOffreQuery leftJoinCommonEnchereOffreReference($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEnchereOffreReference relation
 * @method CommonEnchereOffreQuery rightJoinCommonEnchereOffreReference($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEnchereOffreReference relation
 * @method CommonEnchereOffreQuery innerJoinCommonEnchereOffreReference($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEnchereOffreReference relation
 *
 * @method CommonEnchereOffre findOne(PropelPDO $con = null) Return the first CommonEnchereOffre matching the query
 * @method CommonEnchereOffre findOneOrCreate(PropelPDO $con = null) Return the first CommonEnchereOffre matching the query, or a new CommonEnchereOffre object populated from the query conditions when no match is found
 *
 * @method CommonEnchereOffre findOneByOldId(int $old_id) Return the first CommonEnchereOffre filtered by the old_id column
 * @method CommonEnchereOffre findOneByOrganisme(string $organisme) Return the first CommonEnchereOffre filtered by the organisme column
 * @method CommonEnchereOffre findOneByOldIdEnchere(int $old_id_enchere) Return the first CommonEnchereOffre filtered by the old_id_enchere column
 * @method CommonEnchereOffre findOneByOldIdEnchereEntreprise(int $old_id_enchere_entreprise) Return the first CommonEnchereOffre filtered by the old_id_enchere_entreprise column
 * @method CommonEnchereOffre findOneByDate(string $date) Return the first CommonEnchereOffre filtered by the date column
 * @method CommonEnchereOffre findOneByValeurtic(double $valeurTIC) Return the first CommonEnchereOffre filtered by the valeurTIC column
 * @method CommonEnchereOffre findOneByValeurtc(double $valeurTC) Return the first CommonEnchereOffre filtered by the valeurTC column
 * @method CommonEnchereOffre findOneByValeurnetc(double $valeurNETC) Return the first CommonEnchereOffre filtered by the valeurNETC column
 * @method CommonEnchereOffre findOneByValeurngc(double $valeurNGC) Return the first CommonEnchereOffre filtered by the valeurNGC column
 * @method CommonEnchereOffre findOneByRang(int $rang) Return the first CommonEnchereOffre filtered by the rang column
 * @method CommonEnchereOffre findOneByIdenchere(string $idEnchere) Return the first CommonEnchereOffre filtered by the idEnchere column
 * @method CommonEnchereOffre findOneByIdenchereentreprise(string $idEnchereEntreprise) Return the first CommonEnchereOffre filtered by the idEnchereEntreprise column
 *
 * @method array findByOldId(int $old_id) Return CommonEnchereOffre objects filtered by the old_id column
 * @method array findByOrganisme(string $organisme) Return CommonEnchereOffre objects filtered by the organisme column
 * @method array findByOldIdEnchere(int $old_id_enchere) Return CommonEnchereOffre objects filtered by the old_id_enchere column
 * @method array findByOldIdEnchereEntreprise(int $old_id_enchere_entreprise) Return CommonEnchereOffre objects filtered by the old_id_enchere_entreprise column
 * @method array findByDate(string $date) Return CommonEnchereOffre objects filtered by the date column
 * @method array findByValeurtic(double $valeurTIC) Return CommonEnchereOffre objects filtered by the valeurTIC column
 * @method array findByValeurtc(double $valeurTC) Return CommonEnchereOffre objects filtered by the valeurTC column
 * @method array findByValeurnetc(double $valeurNETC) Return CommonEnchereOffre objects filtered by the valeurNETC column
 * @method array findByValeurngc(double $valeurNGC) Return CommonEnchereOffre objects filtered by the valeurNGC column
 * @method array findByRang(int $rang) Return CommonEnchereOffre objects filtered by the rang column
 * @method array findById(string $id) Return CommonEnchereOffre objects filtered by the id column
 * @method array findByIdenchere(string $idEnchere) Return CommonEnchereOffre objects filtered by the idEnchere column
 * @method array findByIdenchereentreprise(string $idEnchereEntreprise) Return CommonEnchereOffre objects filtered by the idEnchereEntreprise column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEnchereOffreQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonEnchereOffreQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonEnchereOffre', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonEnchereOffreQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonEnchereOffreQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonEnchereOffreQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonEnchereOffreQuery) {
            return $criteria;
        }
        $query = new CommonEnchereOffreQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonEnchereOffre|CommonEnchereOffre[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonEnchereOffrePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonEnchereOffrePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEnchereOffre A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEnchereOffre A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `old_id`, `organisme`, `old_id_enchere`, `old_id_enchere_entreprise`, `date`, `valeurTIC`, `valeurTC`, `valeurNETC`, `valeurNGC`, `rang`, `id`, `idEnchere`, `idEnchereEntreprise` FROM `EnchereOffre` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonEnchereOffre();
            $obj->hydrate($row);
            CommonEnchereOffrePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonEnchereOffre|CommonEnchereOffre[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonEnchereOffre[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonEnchereOffrePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonEnchereOffrePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId(1234); // WHERE old_id = 1234
     * $query->filterByOldId(array(12, 34)); // WHERE old_id IN (12, 34)
     * $query->filterByOldId(array('min' => 12)); // WHERE old_id >= 12
     * $query->filterByOldId(array('max' => 12)); // WHERE old_id <= 12
     * </code>
     *
     * @param     mixed $oldId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (is_array($oldId)) {
            $useMinMax = false;
            if (isset($oldId['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::OLD_ID, $oldId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldId['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::OLD_ID, $oldId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_id_enchere column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdEnchere(1234); // WHERE old_id_enchere = 1234
     * $query->filterByOldIdEnchere(array(12, 34)); // WHERE old_id_enchere IN (12, 34)
     * $query->filterByOldIdEnchere(array('min' => 12)); // WHERE old_id_enchere >= 12
     * $query->filterByOldIdEnchere(array('max' => 12)); // WHERE old_id_enchere <= 12
     * </code>
     *
     * @param     mixed $oldIdEnchere The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByOldIdEnchere($oldIdEnchere = null, $comparison = null)
    {
        if (is_array($oldIdEnchere)) {
            $useMinMax = false;
            if (isset($oldIdEnchere['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::OLD_ID_ENCHERE, $oldIdEnchere['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdEnchere['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::OLD_ID_ENCHERE, $oldIdEnchere['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::OLD_ID_ENCHERE, $oldIdEnchere, $comparison);
    }

    /**
     * Filter the query on the old_id_enchere_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdEnchereEntreprise(1234); // WHERE old_id_enchere_entreprise = 1234
     * $query->filterByOldIdEnchereEntreprise(array(12, 34)); // WHERE old_id_enchere_entreprise IN (12, 34)
     * $query->filterByOldIdEnchereEntreprise(array('min' => 12)); // WHERE old_id_enchere_entreprise >= 12
     * $query->filterByOldIdEnchereEntreprise(array('max' => 12)); // WHERE old_id_enchere_entreprise <= 12
     * </code>
     *
     * @param     mixed $oldIdEnchereEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByOldIdEnchereEntreprise($oldIdEnchereEntreprise = null, $comparison = null)
    {
        if (is_array($oldIdEnchereEntreprise)) {
            $useMinMax = false;
            if (isset($oldIdEnchereEntreprise['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::OLD_ID_ENCHERE_ENTREPRISE, $oldIdEnchereEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdEnchereEntreprise['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::OLD_ID_ENCHERE_ENTREPRISE, $oldIdEnchereEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::OLD_ID_ENCHERE_ENTREPRISE, $oldIdEnchereEntreprise, $comparison);
    }

    /**
     * Filter the query on the date column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('fooValue');   // WHERE date = 'fooValue'
     * $query->filterByDate('%fooValue%'); // WHERE date LIKE '%fooValue%'
     * </code>
     *
     * @param     string $date The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($date)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $date)) {
                $date = str_replace('*', '%', $date);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::DATE, $date, $comparison);
    }

    /**
     * Filter the query on the valeurTIC column
     *
     * Example usage:
     * <code>
     * $query->filterByValeurtic(1234); // WHERE valeurTIC = 1234
     * $query->filterByValeurtic(array(12, 34)); // WHERE valeurTIC IN (12, 34)
     * $query->filterByValeurtic(array('min' => 12)); // WHERE valeurTIC >= 12
     * $query->filterByValeurtic(array('max' => 12)); // WHERE valeurTIC <= 12
     * </code>
     *
     * @param     mixed $valeurtic The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByValeurtic($valeurtic = null, $comparison = null)
    {
        if (is_array($valeurtic)) {
            $useMinMax = false;
            if (isset($valeurtic['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::VALEURTIC, $valeurtic['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valeurtic['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::VALEURTIC, $valeurtic['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::VALEURTIC, $valeurtic, $comparison);
    }

    /**
     * Filter the query on the valeurTC column
     *
     * Example usage:
     * <code>
     * $query->filterByValeurtc(1234); // WHERE valeurTC = 1234
     * $query->filterByValeurtc(array(12, 34)); // WHERE valeurTC IN (12, 34)
     * $query->filterByValeurtc(array('min' => 12)); // WHERE valeurTC >= 12
     * $query->filterByValeurtc(array('max' => 12)); // WHERE valeurTC <= 12
     * </code>
     *
     * @param     mixed $valeurtc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByValeurtc($valeurtc = null, $comparison = null)
    {
        if (is_array($valeurtc)) {
            $useMinMax = false;
            if (isset($valeurtc['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::VALEURTC, $valeurtc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valeurtc['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::VALEURTC, $valeurtc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::VALEURTC, $valeurtc, $comparison);
    }

    /**
     * Filter the query on the valeurNETC column
     *
     * Example usage:
     * <code>
     * $query->filterByValeurnetc(1234); // WHERE valeurNETC = 1234
     * $query->filterByValeurnetc(array(12, 34)); // WHERE valeurNETC IN (12, 34)
     * $query->filterByValeurnetc(array('min' => 12)); // WHERE valeurNETC >= 12
     * $query->filterByValeurnetc(array('max' => 12)); // WHERE valeurNETC <= 12
     * </code>
     *
     * @param     mixed $valeurnetc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByValeurnetc($valeurnetc = null, $comparison = null)
    {
        if (is_array($valeurnetc)) {
            $useMinMax = false;
            if (isset($valeurnetc['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::VALEURNETC, $valeurnetc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valeurnetc['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::VALEURNETC, $valeurnetc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::VALEURNETC, $valeurnetc, $comparison);
    }

    /**
     * Filter the query on the valeurNGC column
     *
     * Example usage:
     * <code>
     * $query->filterByValeurngc(1234); // WHERE valeurNGC = 1234
     * $query->filterByValeurngc(array(12, 34)); // WHERE valeurNGC IN (12, 34)
     * $query->filterByValeurngc(array('min' => 12)); // WHERE valeurNGC >= 12
     * $query->filterByValeurngc(array('max' => 12)); // WHERE valeurNGC <= 12
     * </code>
     *
     * @param     mixed $valeurngc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByValeurngc($valeurngc = null, $comparison = null)
    {
        if (is_array($valeurngc)) {
            $useMinMax = false;
            if (isset($valeurngc['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::VALEURNGC, $valeurngc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valeurngc['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::VALEURNGC, $valeurngc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::VALEURNGC, $valeurngc, $comparison);
    }

    /**
     * Filter the query on the rang column
     *
     * Example usage:
     * <code>
     * $query->filterByRang(1234); // WHERE rang = 1234
     * $query->filterByRang(array(12, 34)); // WHERE rang IN (12, 34)
     * $query->filterByRang(array('min' => 12)); // WHERE rang >= 12
     * $query->filterByRang(array('max' => 12)); // WHERE rang <= 12
     * </code>
     *
     * @param     mixed $rang The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByRang($rang = null, $comparison = null)
    {
        if (is_array($rang)) {
            $useMinMax = false;
            if (isset($rang['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::RANG, $rang['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rang['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::RANG, $rang['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::RANG, $rang, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the idEnchere column
     *
     * Example usage:
     * <code>
     * $query->filterByIdenchere(1234); // WHERE idEnchere = 1234
     * $query->filterByIdenchere(array(12, 34)); // WHERE idEnchere IN (12, 34)
     * $query->filterByIdenchere(array('min' => 12)); // WHERE idEnchere >= 12
     * $query->filterByIdenchere(array('max' => 12)); // WHERE idEnchere <= 12
     * </code>
     *
     * @see       filterByCommonEncherePmi()
     *
     * @param     mixed $idenchere The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByIdenchere($idenchere = null, $comparison = null)
    {
        if (is_array($idenchere)) {
            $useMinMax = false;
            if (isset($idenchere['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::IDENCHERE, $idenchere['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idenchere['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::IDENCHERE, $idenchere['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::IDENCHERE, $idenchere, $comparison);
    }

    /**
     * Filter the query on the idEnchereEntreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdenchereentreprise(1234); // WHERE idEnchereEntreprise = 1234
     * $query->filterByIdenchereentreprise(array(12, 34)); // WHERE idEnchereEntreprise IN (12, 34)
     * $query->filterByIdenchereentreprise(array('min' => 12)); // WHERE idEnchereEntreprise >= 12
     * $query->filterByIdenchereentreprise(array('max' => 12)); // WHERE idEnchereEntreprise <= 12
     * </code>
     *
     * @see       filterByCommonEnchereEntreprisePmi()
     *
     * @param     mixed $idenchereentreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function filterByIdenchereentreprise($idenchereentreprise = null, $comparison = null)
    {
        if (is_array($idenchereentreprise)) {
            $useMinMax = false;
            if (isset($idenchereentreprise['min'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::IDENCHEREENTREPRISE, $idenchereentreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idenchereentreprise['max'])) {
                $this->addUsingAlias(CommonEnchereOffrePeer::IDENCHEREENTREPRISE, $idenchereentreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEnchereOffrePeer::IDENCHEREENTREPRISE, $idenchereentreprise, $comparison);
    }

    /**
     * Filter the query by a related CommonEncherePmi object
     *
     * @param   CommonEncherePmi|PropelObjectCollection $commonEncherePmi The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEnchereOffreQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEncherePmi($commonEncherePmi, $comparison = null)
    {
        if ($commonEncherePmi instanceof CommonEncherePmi) {
            return $this
                ->addUsingAlias(CommonEnchereOffrePeer::IDENCHERE, $commonEncherePmi->getId(), $comparison);
        } elseif ($commonEncherePmi instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEnchereOffrePeer::IDENCHERE, $commonEncherePmi->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonEncherePmi() only accepts arguments of type CommonEncherePmi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEncherePmi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function joinCommonEncherePmi($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEncherePmi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEncherePmi');
        }

        return $this;
    }

    /**
     * Use the CommonEncherePmi relation CommonEncherePmi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEncherePmiQuery A secondary query class using the current class as primary query
     */
    public function useCommonEncherePmiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEncherePmi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEncherePmi', '\Application\Propel\Mpe\CommonEncherePmiQuery');
    }

    /**
     * Filter the query by a related CommonEnchereEntreprisePmi object
     *
     * @param   CommonEnchereEntreprisePmi|PropelObjectCollection $commonEnchereEntreprisePmi The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEnchereOffreQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEnchereEntreprisePmi($commonEnchereEntreprisePmi, $comparison = null)
    {
        if ($commonEnchereEntreprisePmi instanceof CommonEnchereEntreprisePmi) {
            return $this
                ->addUsingAlias(CommonEnchereOffrePeer::IDENCHEREENTREPRISE, $commonEnchereEntreprisePmi->getId(), $comparison);
        } elseif ($commonEnchereEntreprisePmi instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEnchereOffrePeer::IDENCHEREENTREPRISE, $commonEnchereEntreprisePmi->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonEnchereEntreprisePmi() only accepts arguments of type CommonEnchereEntreprisePmi or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEnchereEntreprisePmi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function joinCommonEnchereEntreprisePmi($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEnchereEntreprisePmi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEnchereEntreprisePmi');
        }

        return $this;
    }

    /**
     * Use the CommonEnchereEntreprisePmi relation CommonEnchereEntreprisePmi object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEnchereEntreprisePmiQuery A secondary query class using the current class as primary query
     */
    public function useCommonEnchereEntreprisePmiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEnchereEntreprisePmi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEnchereEntreprisePmi', '\Application\Propel\Mpe\CommonEnchereEntreprisePmiQuery');
    }

    /**
     * Filter the query by a related CommonEnchereOffreReference object
     *
     * @param   CommonEnchereOffreReference|PropelObjectCollection $commonEnchereOffreReference  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEnchereOffreQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEnchereOffreReference($commonEnchereOffreReference, $comparison = null)
    {
        if ($commonEnchereOffreReference instanceof CommonEnchereOffreReference) {
            return $this
                ->addUsingAlias(CommonEnchereOffrePeer::ID, $commonEnchereOffreReference->getIdEnchereOffre(), $comparison);
        } elseif ($commonEnchereOffreReference instanceof PropelObjectCollection) {
            return $this
                ->useCommonEnchereOffreReferenceQuery()
                ->filterByPrimaryKeys($commonEnchereOffreReference->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEnchereOffreReference() only accepts arguments of type CommonEnchereOffreReference or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEnchereOffreReference relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function joinCommonEnchereOffreReference($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEnchereOffreReference');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEnchereOffreReference');
        }

        return $this;
    }

    /**
     * Use the CommonEnchereOffreReference relation CommonEnchereOffreReference object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEnchereOffreReferenceQuery A secondary query class using the current class as primary query
     */
    public function useCommonEnchereOffreReferenceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEnchereOffreReference($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEnchereOffreReference', '\Application\Propel\Mpe\CommonEnchereOffreReferenceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonEnchereOffre $commonEnchereOffre Object to remove from the list of results
     *
     * @return CommonEnchereOffreQuery The current query, for fluid interface
     */
    public function prune($commonEnchereOffre = null)
    {
        if ($commonEnchereOffre) {
            $this->addUsingAlias(CommonEnchereOffrePeer::ID, $commonEnchereOffre->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
