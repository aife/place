<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonMarchePeer;
use Application\Propel\Mpe\CommonTConsLotContratPeer;
use Application\Propel\Mpe\CommonTContactContratPeer;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Propel\Mpe\CommonTDonneesConsultationPeer;
use Application\Propel\Mpe\CommonTTypeContratPeer;
use Application\Propel\Mpe\CommonTypeContratConcessionPivotPeer;
use Application\Propel\Mpe\CommonTypeContratPivotPeer;
use Application\Propel\Mpe\CommonTypeProcedureConcessionPivotPeer;
use Application\Propel\Mpe\CommonTypeProcedurePeer;
use Application\Propel\Mpe\Map\CommonTContratTitulaireTableMap;

/**
 * Base static class for performing query and update operations on the 't_contrat_titulaire' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTContratTitulairePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_contrat_titulaire';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTContratTitulaire';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTContratTitulaireTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 90;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 90;

    /** the column name for the id_contrat_titulaire field */
    const ID_CONTRAT_TITULAIRE = 't_contrat_titulaire.id_contrat_titulaire';

    /** the column name for the id_type_contrat field */
    const ID_TYPE_CONTRAT = 't_contrat_titulaire.id_type_contrat';

    /** the column name for the id_contact_contrat field */
    const ID_CONTACT_CONTRAT = 't_contrat_titulaire.id_contact_contrat';

    /** the column name for the numero_contrat field */
    const NUMERO_CONTRAT = 't_contrat_titulaire.numero_contrat';

    /** the column name for the lien_AC_SAD field */
    const LIEN_AC_SAD = 't_contrat_titulaire.lien_AC_SAD';

    /** the column name for the id_contrat_multi field */
    const ID_CONTRAT_MULTI = 't_contrat_titulaire.id_contrat_multi';

    /** the column name for the organisme field */
    const ORGANISME = 't_contrat_titulaire.organisme';

    /** the column name for the old_service_id field */
    const OLD_SERVICE_ID = 't_contrat_titulaire.old_service_id';

    /** the column name for the id_titulaire field */
    const ID_TITULAIRE = 't_contrat_titulaire.id_titulaire';

    /** the column name for the id_titulaire_etab field */
    const ID_TITULAIRE_ETAB = 't_contrat_titulaire.id_titulaire_etab';

    /** the column name for the id_offre field */
    const ID_OFFRE = 't_contrat_titulaire.id_offre';

    /** the column name for the type_depot_reponse field */
    const TYPE_DEPOT_REPONSE = 't_contrat_titulaire.type_depot_reponse';

    /** the column name for the objet_contrat field */
    const OBJET_CONTRAT = 't_contrat_titulaire.objet_contrat';

    /** the column name for the intitule field */
    const INTITULE = 't_contrat_titulaire.intitule';

    /** the column name for the montant_contrat field */
    const MONTANT_CONTRAT = 't_contrat_titulaire.montant_contrat';

    /** the column name for the id_tranche_budgetaire field */
    const ID_TRANCHE_BUDGETAIRE = 't_contrat_titulaire.id_tranche_budgetaire';

    /** the column name for the montant_max_estime field */
    const MONTANT_MAX_ESTIME = 't_contrat_titulaire.montant_max_estime';

    /** the column name for the publication_montant field */
    const PUBLICATION_MONTANT = 't_contrat_titulaire.publication_montant';

    /** the column name for the id_motif_non_publication_montant field */
    const ID_MOTIF_NON_PUBLICATION_MONTANT = 't_contrat_titulaire.id_motif_non_publication_montant';

    /** the column name for the desc_motif_non_publication_montant field */
    const DESC_MOTIF_NON_PUBLICATION_MONTANT = 't_contrat_titulaire.desc_motif_non_publication_montant';

    /** the column name for the publication_contrat field */
    const PUBLICATION_CONTRAT = 't_contrat_titulaire.publication_contrat';

    /** the column name for the num_EJ field */
    const NUM_EJ = 't_contrat_titulaire.num_EJ';

    /** the column name for the statutEJ field */
    const STATUTEJ = 't_contrat_titulaire.statutEJ';

    /** the column name for the num_long_OEAP field */
    const NUM_LONG_OEAP = 't_contrat_titulaire.num_long_OEAP';

    /** the column name for the reference_libre field */
    const REFERENCE_LIBRE = 't_contrat_titulaire.reference_libre';

    /** the column name for the statut_contrat field */
    const STATUT_CONTRAT = 't_contrat_titulaire.statut_contrat';

    /** the column name for the categorie field */
    const CATEGORIE = 't_contrat_titulaire.categorie';

    /** the column name for the ccag_applicable field */
    const CCAG_APPLICABLE = 't_contrat_titulaire.ccag_applicable';

    /** the column name for the clause_sociale field */
    const CLAUSE_SOCIALE = 't_contrat_titulaire.clause_sociale';

    /** the column name for the clause_sociale_condition_execution field */
    const CLAUSE_SOCIALE_CONDITION_EXECUTION = 't_contrat_titulaire.clause_sociale_condition_execution';

    /** the column name for the clause_sociale_insertion field */
    const CLAUSE_SOCIALE_INSERTION = 't_contrat_titulaire.clause_sociale_insertion';

    /** the column name for the clause_sociale_ateliers_proteges field */
    const CLAUSE_SOCIALE_ATELIERS_PROTEGES = 't_contrat_titulaire.clause_sociale_ateliers_proteges';

    /** the column name for the clause_sociale_siae field */
    const CLAUSE_SOCIALE_SIAE = 't_contrat_titulaire.clause_sociale_siae';

    /** the column name for the clause_sociale_ess field */
    const CLAUSE_SOCIALE_ESS = 't_contrat_titulaire.clause_sociale_ess';

    /** the column name for the clause_environnementale field */
    const CLAUSE_ENVIRONNEMENTALE = 't_contrat_titulaire.clause_environnementale';

    /** the column name for the clause_env_specs_techniques field */
    const CLAUSE_ENV_SPECS_TECHNIQUES = 't_contrat_titulaire.clause_env_specs_techniques';

    /** the column name for the clause_env_cond_execution field */
    const CLAUSE_ENV_COND_EXECUTION = 't_contrat_titulaire.clause_env_cond_execution';

    /** the column name for the clause_env_criteres_select field */
    const CLAUSE_ENV_CRITERES_SELECT = 't_contrat_titulaire.clause_env_criteres_select';

    /** the column name for the date_prevue_notification field */
    const DATE_PREVUE_NOTIFICATION = 't_contrat_titulaire.date_prevue_notification';

    /** the column name for the date_prevue_fin_contrat field */
    const DATE_PREVUE_FIN_CONTRAT = 't_contrat_titulaire.date_prevue_fin_contrat';

    /** the column name for the date_prevue_max_fin_contrat field */
    const DATE_PREVUE_MAX_FIN_CONTRAT = 't_contrat_titulaire.date_prevue_max_fin_contrat';

    /** the column name for the date_notification field */
    const DATE_NOTIFICATION = 't_contrat_titulaire.date_notification';

    /** the column name for the date_fin_contrat field */
    const DATE_FIN_CONTRAT = 't_contrat_titulaire.date_fin_contrat';

    /** the column name for the date_max_fin_contrat field */
    const DATE_MAX_FIN_CONTRAT = 't_contrat_titulaire.date_max_fin_contrat';

    /** the column name for the date_attribution field */
    const DATE_ATTRIBUTION = 't_contrat_titulaire.date_attribution';

    /** the column name for the date_creation field */
    const DATE_CREATION = 't_contrat_titulaire.date_creation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 't_contrat_titulaire.date_modification';

    /** the column name for the envoi_interface field */
    const ENVOI_INTERFACE = 't_contrat_titulaire.envoi_interface';

    /** the column name for the contrat_class_key field */
    const CONTRAT_CLASS_KEY = 't_contrat_titulaire.contrat_class_key';

    /** the column name for the pme_pmi field */
    const PME_PMI = 't_contrat_titulaire.pme_pmi';

    /** the column name for the reference_consultation field */
    const REFERENCE_CONSULTATION = 't_contrat_titulaire.reference_consultation';

    /** the column name for the hors_passation field */
    const HORS_PASSATION = 't_contrat_titulaire.hors_passation';

    /** the column name for the id_agent field */
    const ID_AGENT = 't_contrat_titulaire.id_agent';

    /** the column name for the nom_agent field */
    const NOM_AGENT = 't_contrat_titulaire.nom_agent';

    /** the column name for the prenom_agent field */
    const PRENOM_AGENT = 't_contrat_titulaire.prenom_agent';

    /** the column name for the lieu_execution field */
    const LIEU_EXECUTION = 't_contrat_titulaire.lieu_execution';

    /** the column name for the code_cpv_1 field */
    const CODE_CPV_1 = 't_contrat_titulaire.code_cpv_1';

    /** the column name for the code_cpv_2 field */
    const CODE_CPV_2 = 't_contrat_titulaire.code_cpv_2';

    /** the column name for the marche_insertion field */
    const MARCHE_INSERTION = 't_contrat_titulaire.marche_insertion';

    /** the column name for the clause_specification_technique field */
    const CLAUSE_SPECIFICATION_TECHNIQUE = 't_contrat_titulaire.clause_specification_technique';

    /** the column name for the procedure_passation_pivot field */
    const PROCEDURE_PASSATION_PIVOT = 't_contrat_titulaire.procedure_passation_pivot';

    /** the column name for the nom_lieu_principal_execution field */
    const NOM_LIEU_PRINCIPAL_EXECUTION = 't_contrat_titulaire.nom_lieu_principal_execution';

    /** the column name for the code_lieu_principal_execution field */
    const CODE_LIEU_PRINCIPAL_EXECUTION = 't_contrat_titulaire.code_lieu_principal_execution';

    /** the column name for the type_code_lieu_principal_execution field */
    const TYPE_CODE_LIEU_PRINCIPAL_EXECUTION = 't_contrat_titulaire.type_code_lieu_principal_execution';

    /** the column name for the duree_initiale_contrat field */
    const DUREE_INITIALE_CONTRAT = 't_contrat_titulaire.duree_initiale_contrat';

    /** the column name for the forme_prix field */
    const FORME_PRIX = 't_contrat_titulaire.forme_prix';

    /** the column name for the date_publication_initiale_de field */
    const DATE_PUBLICATION_INITIALE_DE = 't_contrat_titulaire.date_publication_initiale_de';

    /** the column name for the num_id_unique_marche_public field */
    const NUM_ID_UNIQUE_MARCHE_PUBLIC = 't_contrat_titulaire.num_id_unique_marche_public';

    /** the column name for the libelle_type_contrat_pivot field */
    const LIBELLE_TYPE_CONTRAT_PIVOT = 't_contrat_titulaire.libelle_type_contrat_pivot';

    /** the column name for the siret_pa_accord_cadre field */
    const SIRET_PA_ACCORD_CADRE = 't_contrat_titulaire.siret_pa_accord_cadre';

    /** the column name for the ac_marche_subsequent field */
    const AC_MARCHE_SUBSEQUENT = 't_contrat_titulaire.ac_marche_subsequent';

    /** the column name for the libelle_type_procedure_mpe field */
    const LIBELLE_TYPE_PROCEDURE_MPE = 't_contrat_titulaire.libelle_type_procedure_mpe';

    /** the column name for the nb_total_propositions_lot field */
    const NB_TOTAL_PROPOSITIONS_LOT = 't_contrat_titulaire.nb_total_propositions_lot';

    /** the column name for the nb_total_propositions_demat_lot field */
    const NB_TOTAL_PROPOSITIONS_DEMAT_LOT = 't_contrat_titulaire.nb_total_propositions_demat_lot';

    /** the column name for the marche_defense field */
    const MARCHE_DEFENSE = 't_contrat_titulaire.marche_defense';

    /** the column name for the siret field */
    const SIRET = 't_contrat_titulaire.siret';

    /** the column name for the nom_entite_acheteur field */
    const NOM_ENTITE_ACHETEUR = 't_contrat_titulaire.nom_entite_acheteur';

    /** the column name for the statut_publication_sn field */
    const STATUT_PUBLICATION_SN = 't_contrat_titulaire.statut_publication_sn';

    /** the column name for the date_publication_sn field */
    const DATE_PUBLICATION_SN = 't_contrat_titulaire.date_publication_sn';

    /** the column name for the erreur_sn field */
    const ERREUR_SN = 't_contrat_titulaire.erreur_sn';

    /** the column name for the date_modification_sn field */
    const DATE_MODIFICATION_SN = 't_contrat_titulaire.date_modification_sn';

    /** the column name for the id_type_procedure_pivot field */
    const ID_TYPE_PROCEDURE_PIVOT = 't_contrat_titulaire.id_type_procedure_pivot';

    /** the column name for the id_type_procedure_concession_pivot field */
    const ID_TYPE_PROCEDURE_CONCESSION_PIVOT = 't_contrat_titulaire.id_type_procedure_concession_pivot';

    /** the column name for the id_type_contrat_pivot field */
    const ID_TYPE_CONTRAT_PIVOT = 't_contrat_titulaire.id_type_contrat_pivot';

    /** the column name for the id_type_contrat_concession_pivot field */
    const ID_TYPE_CONTRAT_CONCESSION_PIVOT = 't_contrat_titulaire.id_type_contrat_concession_pivot';

    /** the column name for the montant_subvention_publique field */
    const MONTANT_SUBVENTION_PUBLIQUE = 't_contrat_titulaire.montant_subvention_publique';

    /** the column name for the date_debut_execution field */
    const DATE_DEBUT_EXECUTION = 't_contrat_titulaire.date_debut_execution';

    /** the column name for the uuid field */
    const UUID = 't_contrat_titulaire.uuid';

    /** the column name for the marche_innovant field */
    const MARCHE_INNOVANT = 't_contrat_titulaire.marche_innovant';

    /** the column name for the service_id field */
    const SERVICE_ID = 't_contrat_titulaire.service_id';

    /** A key representing a particular subclass */
    const CLASSKEY_1 = '1';

    /** A key representing a particular subclass */
    const CLASSKEY_COMMONTCONTRATTITULAIRE = '1';

    /** A class that can be returned by this peer. */
    const CLASSNAME_1 = 'Application\\Propel\\Mpe\\CommonTContratTitulaire';

    /** A key representing a particular subclass */
    const CLASSKEY_2 = '2';

    /** A key representing a particular subclass */
    const CLASSKEY_COMMONTCONTRATMULTI = '2';

    /** A class that can be returned by this peer. */
    const CLASSNAME_2 = 'Application\\Propel\\Mpe\\CommonTContratMulti';

    /** The enumerated values for the envoi_interface field */
    const ENVOI_INTERFACE_0 = '0';
    const ENVOI_INTERFACE_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTContratTitulaire objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTContratTitulaire[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTContratTitulairePeer::$fieldNames[CommonTContratTitulairePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdContratTitulaire', 'IdTypeContrat', 'IdContactContrat', 'NumeroContrat', 'LienAcSad', 'IdContratMulti', 'Organisme', 'OldServiceId', 'IdTitulaire', 'IdTitulaireEtab', 'IdOffre', 'TypeDepotReponse', 'ObjetContrat', 'Intitule', 'MontantContrat', 'IdTrancheBudgetaire', 'MontantMaxEstime', 'PublicationMontant', 'IdMotifNonPublicationMontant', 'DescMotifNonPublicationMontant', 'PublicationContrat', 'NumEj', 'Statutej', 'NumLongOeap', 'ReferenceLibre', 'StatutContrat', 'Categorie', 'CcagApplicable', 'ClauseSociale', 'ClauseSocialeConditionExecution', 'ClauseSocialeInsertion', 'ClauseSocialeAteliersProteges', 'ClauseSocialeSiae', 'ClauseSocialeEss', 'ClauseEnvironnementale', 'ClauseEnvSpecsTechniques', 'ClauseEnvCondExecution', 'ClauseEnvCriteresSelect', 'DatePrevueNotification', 'DatePrevueFinContrat', 'DatePrevueMaxFinContrat', 'DateNotification', 'DateFinContrat', 'DateMaxFinContrat', 'DateAttribution', 'DateCreation', 'DateModification', 'EnvoiInterface', 'ContratClassKey', 'PmePmi', 'ReferenceConsultation', 'HorsPassation', 'IdAgent', 'NomAgent', 'PrenomAgent', 'LieuExecution', 'CodeCpv1', 'CodeCpv2', 'MarcheInsertion', 'ClauseSpecificationTechnique', 'ProcedurePassationPivot', 'NomLieuPrincipalExecution', 'CodeLieuPrincipalExecution', 'TypeCodeLieuPrincipalExecution', 'DureeInitialeContrat', 'FormePrix', 'DatePublicationInitialeDe', 'NumIdUniqueMarchePublic', 'LibelleTypeContratPivot', 'SiretPaAccordCadre', 'AcMarcheSubsequent', 'LibelleTypeProcedureMpe', 'NbTotalPropositionsLot', 'NbTotalPropositionsDematLot', 'MarcheDefense', 'Siret', 'NomEntiteAcheteur', 'StatutPublicationSn', 'DatePublicationSn', 'ErreurSn', 'DateModificationSn', 'IdTypeProcedurePivot', 'IdTypeProcedureConcessionPivot', 'IdTypeContratPivot', 'IdTypeContratConcessionPivot', 'MontantSubventionPublique', 'DateDebutExecution', 'Uuid', 'MarcheInnovant', 'ServiceId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idContratTitulaire', 'idTypeContrat', 'idContactContrat', 'numeroContrat', 'lienAcSad', 'idContratMulti', 'organisme', 'oldServiceId', 'idTitulaire', 'idTitulaireEtab', 'idOffre', 'typeDepotReponse', 'objetContrat', 'intitule', 'montantContrat', 'idTrancheBudgetaire', 'montantMaxEstime', 'publicationMontant', 'idMotifNonPublicationMontant', 'descMotifNonPublicationMontant', 'publicationContrat', 'numEj', 'statutej', 'numLongOeap', 'referenceLibre', 'statutContrat', 'categorie', 'ccagApplicable', 'clauseSociale', 'clauseSocialeConditionExecution', 'clauseSocialeInsertion', 'clauseSocialeAteliersProteges', 'clauseSocialeSiae', 'clauseSocialeEss', 'clauseEnvironnementale', 'clauseEnvSpecsTechniques', 'clauseEnvCondExecution', 'clauseEnvCriteresSelect', 'datePrevueNotification', 'datePrevueFinContrat', 'datePrevueMaxFinContrat', 'dateNotification', 'dateFinContrat', 'dateMaxFinContrat', 'dateAttribution', 'dateCreation', 'dateModification', 'envoiInterface', 'contratClassKey', 'pmePmi', 'referenceConsultation', 'horsPassation', 'idAgent', 'nomAgent', 'prenomAgent', 'lieuExecution', 'codeCpv1', 'codeCpv2', 'marcheInsertion', 'clauseSpecificationTechnique', 'procedurePassationPivot', 'nomLieuPrincipalExecution', 'codeLieuPrincipalExecution', 'typeCodeLieuPrincipalExecution', 'dureeInitialeContrat', 'formePrix', 'datePublicationInitialeDe', 'numIdUniqueMarchePublic', 'libelleTypeContratPivot', 'siretPaAccordCadre', 'acMarcheSubsequent', 'libelleTypeProcedureMpe', 'nbTotalPropositionsLot', 'nbTotalPropositionsDematLot', 'marcheDefense', 'siret', 'nomEntiteAcheteur', 'statutPublicationSn', 'datePublicationSn', 'erreurSn', 'dateModificationSn', 'idTypeProcedurePivot', 'idTypeProcedureConcessionPivot', 'idTypeContratPivot', 'idTypeContratConcessionPivot', 'montantSubventionPublique', 'dateDebutExecution', 'uuid', 'marcheInnovant', 'serviceId', ),
        BasePeer::TYPE_COLNAME => array (CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContratTitulairePeer::NUMERO_CONTRAT, CommonTContratTitulairePeer::LIEN_AC_SAD, CommonTContratTitulairePeer::ID_CONTRAT_MULTI, CommonTContratTitulairePeer::ORGANISME, CommonTContratTitulairePeer::OLD_SERVICE_ID, CommonTContratTitulairePeer::ID_TITULAIRE, CommonTContratTitulairePeer::ID_TITULAIRE_ETAB, CommonTContratTitulairePeer::ID_OFFRE, CommonTContratTitulairePeer::TYPE_DEPOT_REPONSE, CommonTContratTitulairePeer::OBJET_CONTRAT, CommonTContratTitulairePeer::INTITULE, CommonTContratTitulairePeer::MONTANT_CONTRAT, CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE, CommonTContratTitulairePeer::MONTANT_MAX_ESTIME, CommonTContratTitulairePeer::PUBLICATION_MONTANT, CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT, CommonTContratTitulairePeer::DESC_MOTIF_NON_PUBLICATION_MONTANT, CommonTContratTitulairePeer::PUBLICATION_CONTRAT, CommonTContratTitulairePeer::NUM_EJ, CommonTContratTitulairePeer::STATUTEJ, CommonTContratTitulairePeer::NUM_LONG_OEAP, CommonTContratTitulairePeer::REFERENCE_LIBRE, CommonTContratTitulairePeer::STATUT_CONTRAT, CommonTContratTitulairePeer::CATEGORIE, CommonTContratTitulairePeer::CCAG_APPLICABLE, CommonTContratTitulairePeer::CLAUSE_SOCIALE, CommonTContratTitulairePeer::CLAUSE_SOCIALE_CONDITION_EXECUTION, CommonTContratTitulairePeer::CLAUSE_SOCIALE_INSERTION, CommonTContratTitulairePeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES, CommonTContratTitulairePeer::CLAUSE_SOCIALE_SIAE, CommonTContratTitulairePeer::CLAUSE_SOCIALE_ESS, CommonTContratTitulairePeer::CLAUSE_ENVIRONNEMENTALE, CommonTContratTitulairePeer::CLAUSE_ENV_SPECS_TECHNIQUES, CommonTContratTitulairePeer::CLAUSE_ENV_COND_EXECUTION, CommonTContratTitulairePeer::CLAUSE_ENV_CRITERES_SELECT, CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION, CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT, CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT, CommonTContratTitulairePeer::DATE_NOTIFICATION, CommonTContratTitulairePeer::DATE_FIN_CONTRAT, CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT, CommonTContratTitulairePeer::DATE_ATTRIBUTION, CommonTContratTitulairePeer::DATE_CREATION, CommonTContratTitulairePeer::DATE_MODIFICATION, CommonTContratTitulairePeer::ENVOI_INTERFACE, CommonTContratTitulairePeer::CONTRAT_CLASS_KEY, CommonTContratTitulairePeer::PME_PMI, CommonTContratTitulairePeer::REFERENCE_CONSULTATION, CommonTContratTitulairePeer::HORS_PASSATION, CommonTContratTitulairePeer::ID_AGENT, CommonTContratTitulairePeer::NOM_AGENT, CommonTContratTitulairePeer::PRENOM_AGENT, CommonTContratTitulairePeer::LIEU_EXECUTION, CommonTContratTitulairePeer::CODE_CPV_1, CommonTContratTitulairePeer::CODE_CPV_2, CommonTContratTitulairePeer::MARCHE_INSERTION, CommonTContratTitulairePeer::CLAUSE_SPECIFICATION_TECHNIQUE, CommonTContratTitulairePeer::PROCEDURE_PASSATION_PIVOT, CommonTContratTitulairePeer::NOM_LIEU_PRINCIPAL_EXECUTION, CommonTContratTitulairePeer::CODE_LIEU_PRINCIPAL_EXECUTION, CommonTContratTitulairePeer::TYPE_CODE_LIEU_PRINCIPAL_EXECUTION, CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT, CommonTContratTitulairePeer::FORME_PRIX, CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE, CommonTContratTitulairePeer::NUM_ID_UNIQUE_MARCHE_PUBLIC, CommonTContratTitulairePeer::LIBELLE_TYPE_CONTRAT_PIVOT, CommonTContratTitulairePeer::SIRET_PA_ACCORD_CADRE, CommonTContratTitulairePeer::AC_MARCHE_SUBSEQUENT, CommonTContratTitulairePeer::LIBELLE_TYPE_PROCEDURE_MPE, CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT, CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT, CommonTContratTitulairePeer::MARCHE_DEFENSE, CommonTContratTitulairePeer::SIRET, CommonTContratTitulairePeer::NOM_ENTITE_ACHETEUR, CommonTContratTitulairePeer::STATUT_PUBLICATION_SN, CommonTContratTitulairePeer::DATE_PUBLICATION_SN, CommonTContratTitulairePeer::ERREUR_SN, CommonTContratTitulairePeer::DATE_MODIFICATION_SN, CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE, CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION, CommonTContratTitulairePeer::UUID, CommonTContratTitulairePeer::MARCHE_INNOVANT, CommonTContratTitulairePeer::SERVICE_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_CONTRAT_TITULAIRE', 'ID_TYPE_CONTRAT', 'ID_CONTACT_CONTRAT', 'NUMERO_CONTRAT', 'LIEN_AC_SAD', 'ID_CONTRAT_MULTI', 'ORGANISME', 'OLD_SERVICE_ID', 'ID_TITULAIRE', 'ID_TITULAIRE_ETAB', 'ID_OFFRE', 'TYPE_DEPOT_REPONSE', 'OBJET_CONTRAT', 'INTITULE', 'MONTANT_CONTRAT', 'ID_TRANCHE_BUDGETAIRE', 'MONTANT_MAX_ESTIME', 'PUBLICATION_MONTANT', 'ID_MOTIF_NON_PUBLICATION_MONTANT', 'DESC_MOTIF_NON_PUBLICATION_MONTANT', 'PUBLICATION_CONTRAT', 'NUM_EJ', 'STATUTEJ', 'NUM_LONG_OEAP', 'REFERENCE_LIBRE', 'STATUT_CONTRAT', 'CATEGORIE', 'CCAG_APPLICABLE', 'CLAUSE_SOCIALE', 'CLAUSE_SOCIALE_CONDITION_EXECUTION', 'CLAUSE_SOCIALE_INSERTION', 'CLAUSE_SOCIALE_ATELIERS_PROTEGES', 'CLAUSE_SOCIALE_SIAE', 'CLAUSE_SOCIALE_ESS', 'CLAUSE_ENVIRONNEMENTALE', 'CLAUSE_ENV_SPECS_TECHNIQUES', 'CLAUSE_ENV_COND_EXECUTION', 'CLAUSE_ENV_CRITERES_SELECT', 'DATE_PREVUE_NOTIFICATION', 'DATE_PREVUE_FIN_CONTRAT', 'DATE_PREVUE_MAX_FIN_CONTRAT', 'DATE_NOTIFICATION', 'DATE_FIN_CONTRAT', 'DATE_MAX_FIN_CONTRAT', 'DATE_ATTRIBUTION', 'DATE_CREATION', 'DATE_MODIFICATION', 'ENVOI_INTERFACE', 'CONTRAT_CLASS_KEY', 'PME_PMI', 'REFERENCE_CONSULTATION', 'HORS_PASSATION', 'ID_AGENT', 'NOM_AGENT', 'PRENOM_AGENT', 'LIEU_EXECUTION', 'CODE_CPV_1', 'CODE_CPV_2', 'MARCHE_INSERTION', 'CLAUSE_SPECIFICATION_TECHNIQUE', 'PROCEDURE_PASSATION_PIVOT', 'NOM_LIEU_PRINCIPAL_EXECUTION', 'CODE_LIEU_PRINCIPAL_EXECUTION', 'TYPE_CODE_LIEU_PRINCIPAL_EXECUTION', 'DUREE_INITIALE_CONTRAT', 'FORME_PRIX', 'DATE_PUBLICATION_INITIALE_DE', 'NUM_ID_UNIQUE_MARCHE_PUBLIC', 'LIBELLE_TYPE_CONTRAT_PIVOT', 'SIRET_PA_ACCORD_CADRE', 'AC_MARCHE_SUBSEQUENT', 'LIBELLE_TYPE_PROCEDURE_MPE', 'NB_TOTAL_PROPOSITIONS_LOT', 'NB_TOTAL_PROPOSITIONS_DEMAT_LOT', 'MARCHE_DEFENSE', 'SIRET', 'NOM_ENTITE_ACHETEUR', 'STATUT_PUBLICATION_SN', 'DATE_PUBLICATION_SN', 'ERREUR_SN', 'DATE_MODIFICATION_SN', 'ID_TYPE_PROCEDURE_PIVOT', 'ID_TYPE_PROCEDURE_CONCESSION_PIVOT', 'ID_TYPE_CONTRAT_PIVOT', 'ID_TYPE_CONTRAT_CONCESSION_PIVOT', 'MONTANT_SUBVENTION_PUBLIQUE', 'DATE_DEBUT_EXECUTION', 'UUID', 'MARCHE_INNOVANT', 'SERVICE_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('id_contrat_titulaire', 'id_type_contrat', 'id_contact_contrat', 'numero_contrat', 'lien_AC_SAD', 'id_contrat_multi', 'organisme', 'old_service_id', 'id_titulaire', 'id_titulaire_etab', 'id_offre', 'type_depot_reponse', 'objet_contrat', 'intitule', 'montant_contrat', 'id_tranche_budgetaire', 'montant_max_estime', 'publication_montant', 'id_motif_non_publication_montant', 'desc_motif_non_publication_montant', 'publication_contrat', 'num_EJ', 'statutEJ', 'num_long_OEAP', 'reference_libre', 'statut_contrat', 'categorie', 'ccag_applicable', 'clause_sociale', 'clause_sociale_condition_execution', 'clause_sociale_insertion', 'clause_sociale_ateliers_proteges', 'clause_sociale_siae', 'clause_sociale_ess', 'clause_environnementale', 'clause_env_specs_techniques', 'clause_env_cond_execution', 'clause_env_criteres_select', 'date_prevue_notification', 'date_prevue_fin_contrat', 'date_prevue_max_fin_contrat', 'date_notification', 'date_fin_contrat', 'date_max_fin_contrat', 'date_attribution', 'date_creation', 'date_modification', 'envoi_interface', 'contrat_class_key', 'pme_pmi', 'reference_consultation', 'hors_passation', 'id_agent', 'nom_agent', 'prenom_agent', 'lieu_execution', 'code_cpv_1', 'code_cpv_2', 'marche_insertion', 'clause_specification_technique', 'procedure_passation_pivot', 'nom_lieu_principal_execution', 'code_lieu_principal_execution', 'type_code_lieu_principal_execution', 'duree_initiale_contrat', 'forme_prix', 'date_publication_initiale_de', 'num_id_unique_marche_public', 'libelle_type_contrat_pivot', 'siret_pa_accord_cadre', 'ac_marche_subsequent', 'libelle_type_procedure_mpe', 'nb_total_propositions_lot', 'nb_total_propositions_demat_lot', 'marche_defense', 'siret', 'nom_entite_acheteur', 'statut_publication_sn', 'date_publication_sn', 'erreur_sn', 'date_modification_sn', 'id_type_procedure_pivot', 'id_type_procedure_concession_pivot', 'id_type_contrat_pivot', 'id_type_contrat_concession_pivot', 'montant_subvention_publique', 'date_debut_execution', 'uuid', 'marche_innovant', 'service_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTContratTitulairePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdContratTitulaire' => 0, 'IdTypeContrat' => 1, 'IdContactContrat' => 2, 'NumeroContrat' => 3, 'LienAcSad' => 4, 'IdContratMulti' => 5, 'Organisme' => 6, 'OldServiceId' => 7, 'IdTitulaire' => 8, 'IdTitulaireEtab' => 9, 'IdOffre' => 10, 'TypeDepotReponse' => 11, 'ObjetContrat' => 12, 'Intitule' => 13, 'MontantContrat' => 14, 'IdTrancheBudgetaire' => 15, 'MontantMaxEstime' => 16, 'PublicationMontant' => 17, 'IdMotifNonPublicationMontant' => 18, 'DescMotifNonPublicationMontant' => 19, 'PublicationContrat' => 20, 'NumEj' => 21, 'Statutej' => 22, 'NumLongOeap' => 23, 'ReferenceLibre' => 24, 'StatutContrat' => 25, 'Categorie' => 26, 'CcagApplicable' => 27, 'ClauseSociale' => 28, 'ClauseSocialeConditionExecution' => 29, 'ClauseSocialeInsertion' => 30, 'ClauseSocialeAteliersProteges' => 31, 'ClauseSocialeSiae' => 32, 'ClauseSocialeEss' => 33, 'ClauseEnvironnementale' => 34, 'ClauseEnvSpecsTechniques' => 35, 'ClauseEnvCondExecution' => 36, 'ClauseEnvCriteresSelect' => 37, 'DatePrevueNotification' => 38, 'DatePrevueFinContrat' => 39, 'DatePrevueMaxFinContrat' => 40, 'DateNotification' => 41, 'DateFinContrat' => 42, 'DateMaxFinContrat' => 43, 'DateAttribution' => 44, 'DateCreation' => 45, 'DateModification' => 46, 'EnvoiInterface' => 47, 'ContratClassKey' => 48, 'PmePmi' => 49, 'ReferenceConsultation' => 50, 'HorsPassation' => 51, 'IdAgent' => 52, 'NomAgent' => 53, 'PrenomAgent' => 54, 'LieuExecution' => 55, 'CodeCpv1' => 56, 'CodeCpv2' => 57, 'MarcheInsertion' => 58, 'ClauseSpecificationTechnique' => 59, 'ProcedurePassationPivot' => 60, 'NomLieuPrincipalExecution' => 61, 'CodeLieuPrincipalExecution' => 62, 'TypeCodeLieuPrincipalExecution' => 63, 'DureeInitialeContrat' => 64, 'FormePrix' => 65, 'DatePublicationInitialeDe' => 66, 'NumIdUniqueMarchePublic' => 67, 'LibelleTypeContratPivot' => 68, 'SiretPaAccordCadre' => 69, 'AcMarcheSubsequent' => 70, 'LibelleTypeProcedureMpe' => 71, 'NbTotalPropositionsLot' => 72, 'NbTotalPropositionsDematLot' => 73, 'MarcheDefense' => 74, 'Siret' => 75, 'NomEntiteAcheteur' => 76, 'StatutPublicationSn' => 77, 'DatePublicationSn' => 78, 'ErreurSn' => 79, 'DateModificationSn' => 80, 'IdTypeProcedurePivot' => 81, 'IdTypeProcedureConcessionPivot' => 82, 'IdTypeContratPivot' => 83, 'IdTypeContratConcessionPivot' => 84, 'MontantSubventionPublique' => 85, 'DateDebutExecution' => 86, 'Uuid' => 87, 'MarcheInnovant' => 88, 'ServiceId' => 89, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idContratTitulaire' => 0, 'idTypeContrat' => 1, 'idContactContrat' => 2, 'numeroContrat' => 3, 'lienAcSad' => 4, 'idContratMulti' => 5, 'organisme' => 6, 'oldServiceId' => 7, 'idTitulaire' => 8, 'idTitulaireEtab' => 9, 'idOffre' => 10, 'typeDepotReponse' => 11, 'objetContrat' => 12, 'intitule' => 13, 'montantContrat' => 14, 'idTrancheBudgetaire' => 15, 'montantMaxEstime' => 16, 'publicationMontant' => 17, 'idMotifNonPublicationMontant' => 18, 'descMotifNonPublicationMontant' => 19, 'publicationContrat' => 20, 'numEj' => 21, 'statutej' => 22, 'numLongOeap' => 23, 'referenceLibre' => 24, 'statutContrat' => 25, 'categorie' => 26, 'ccagApplicable' => 27, 'clauseSociale' => 28, 'clauseSocialeConditionExecution' => 29, 'clauseSocialeInsertion' => 30, 'clauseSocialeAteliersProteges' => 31, 'clauseSocialeSiae' => 32, 'clauseSocialeEss' => 33, 'clauseEnvironnementale' => 34, 'clauseEnvSpecsTechniques' => 35, 'clauseEnvCondExecution' => 36, 'clauseEnvCriteresSelect' => 37, 'datePrevueNotification' => 38, 'datePrevueFinContrat' => 39, 'datePrevueMaxFinContrat' => 40, 'dateNotification' => 41, 'dateFinContrat' => 42, 'dateMaxFinContrat' => 43, 'dateAttribution' => 44, 'dateCreation' => 45, 'dateModification' => 46, 'envoiInterface' => 47, 'contratClassKey' => 48, 'pmePmi' => 49, 'referenceConsultation' => 50, 'horsPassation' => 51, 'idAgent' => 52, 'nomAgent' => 53, 'prenomAgent' => 54, 'lieuExecution' => 55, 'codeCpv1' => 56, 'codeCpv2' => 57, 'marcheInsertion' => 58, 'clauseSpecificationTechnique' => 59, 'procedurePassationPivot' => 60, 'nomLieuPrincipalExecution' => 61, 'codeLieuPrincipalExecution' => 62, 'typeCodeLieuPrincipalExecution' => 63, 'dureeInitialeContrat' => 64, 'formePrix' => 65, 'datePublicationInitialeDe' => 66, 'numIdUniqueMarchePublic' => 67, 'libelleTypeContratPivot' => 68, 'siretPaAccordCadre' => 69, 'acMarcheSubsequent' => 70, 'libelleTypeProcedureMpe' => 71, 'nbTotalPropositionsLot' => 72, 'nbTotalPropositionsDematLot' => 73, 'marcheDefense' => 74, 'siret' => 75, 'nomEntiteAcheteur' => 76, 'statutPublicationSn' => 77, 'datePublicationSn' => 78, 'erreurSn' => 79, 'dateModificationSn' => 80, 'idTypeProcedurePivot' => 81, 'idTypeProcedureConcessionPivot' => 82, 'idTypeContratPivot' => 83, 'idTypeContratConcessionPivot' => 84, 'montantSubventionPublique' => 85, 'dateDebutExecution' => 86, 'uuid' => 87, 'marcheInnovant' => 88, 'serviceId' => 89, ),
        BasePeer::TYPE_COLNAME => array (CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE => 0, CommonTContratTitulairePeer::ID_TYPE_CONTRAT => 1, CommonTContratTitulairePeer::ID_CONTACT_CONTRAT => 2, CommonTContratTitulairePeer::NUMERO_CONTRAT => 3, CommonTContratTitulairePeer::LIEN_AC_SAD => 4, CommonTContratTitulairePeer::ID_CONTRAT_MULTI => 5, CommonTContratTitulairePeer::ORGANISME => 6, CommonTContratTitulairePeer::OLD_SERVICE_ID => 7, CommonTContratTitulairePeer::ID_TITULAIRE => 8, CommonTContratTitulairePeer::ID_TITULAIRE_ETAB => 9, CommonTContratTitulairePeer::ID_OFFRE => 10, CommonTContratTitulairePeer::TYPE_DEPOT_REPONSE => 11, CommonTContratTitulairePeer::OBJET_CONTRAT => 12, CommonTContratTitulairePeer::INTITULE => 13, CommonTContratTitulairePeer::MONTANT_CONTRAT => 14, CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE => 15, CommonTContratTitulairePeer::MONTANT_MAX_ESTIME => 16, CommonTContratTitulairePeer::PUBLICATION_MONTANT => 17, CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT => 18, CommonTContratTitulairePeer::DESC_MOTIF_NON_PUBLICATION_MONTANT => 19, CommonTContratTitulairePeer::PUBLICATION_CONTRAT => 20, CommonTContratTitulairePeer::NUM_EJ => 21, CommonTContratTitulairePeer::STATUTEJ => 22, CommonTContratTitulairePeer::NUM_LONG_OEAP => 23, CommonTContratTitulairePeer::REFERENCE_LIBRE => 24, CommonTContratTitulairePeer::STATUT_CONTRAT => 25, CommonTContratTitulairePeer::CATEGORIE => 26, CommonTContratTitulairePeer::CCAG_APPLICABLE => 27, CommonTContratTitulairePeer::CLAUSE_SOCIALE => 28, CommonTContratTitulairePeer::CLAUSE_SOCIALE_CONDITION_EXECUTION => 29, CommonTContratTitulairePeer::CLAUSE_SOCIALE_INSERTION => 30, CommonTContratTitulairePeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES => 31, CommonTContratTitulairePeer::CLAUSE_SOCIALE_SIAE => 32, CommonTContratTitulairePeer::CLAUSE_SOCIALE_ESS => 33, CommonTContratTitulairePeer::CLAUSE_ENVIRONNEMENTALE => 34, CommonTContratTitulairePeer::CLAUSE_ENV_SPECS_TECHNIQUES => 35, CommonTContratTitulairePeer::CLAUSE_ENV_COND_EXECUTION => 36, CommonTContratTitulairePeer::CLAUSE_ENV_CRITERES_SELECT => 37, CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION => 38, CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT => 39, CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT => 40, CommonTContratTitulairePeer::DATE_NOTIFICATION => 41, CommonTContratTitulairePeer::DATE_FIN_CONTRAT => 42, CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT => 43, CommonTContratTitulairePeer::DATE_ATTRIBUTION => 44, CommonTContratTitulairePeer::DATE_CREATION => 45, CommonTContratTitulairePeer::DATE_MODIFICATION => 46, CommonTContratTitulairePeer::ENVOI_INTERFACE => 47, CommonTContratTitulairePeer::CONTRAT_CLASS_KEY => 48, CommonTContratTitulairePeer::PME_PMI => 49, CommonTContratTitulairePeer::REFERENCE_CONSULTATION => 50, CommonTContratTitulairePeer::HORS_PASSATION => 51, CommonTContratTitulairePeer::ID_AGENT => 52, CommonTContratTitulairePeer::NOM_AGENT => 53, CommonTContratTitulairePeer::PRENOM_AGENT => 54, CommonTContratTitulairePeer::LIEU_EXECUTION => 55, CommonTContratTitulairePeer::CODE_CPV_1 => 56, CommonTContratTitulairePeer::CODE_CPV_2 => 57, CommonTContratTitulairePeer::MARCHE_INSERTION => 58, CommonTContratTitulairePeer::CLAUSE_SPECIFICATION_TECHNIQUE => 59, CommonTContratTitulairePeer::PROCEDURE_PASSATION_PIVOT => 60, CommonTContratTitulairePeer::NOM_LIEU_PRINCIPAL_EXECUTION => 61, CommonTContratTitulairePeer::CODE_LIEU_PRINCIPAL_EXECUTION => 62, CommonTContratTitulairePeer::TYPE_CODE_LIEU_PRINCIPAL_EXECUTION => 63, CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT => 64, CommonTContratTitulairePeer::FORME_PRIX => 65, CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE => 66, CommonTContratTitulairePeer::NUM_ID_UNIQUE_MARCHE_PUBLIC => 67, CommonTContratTitulairePeer::LIBELLE_TYPE_CONTRAT_PIVOT => 68, CommonTContratTitulairePeer::SIRET_PA_ACCORD_CADRE => 69, CommonTContratTitulairePeer::AC_MARCHE_SUBSEQUENT => 70, CommonTContratTitulairePeer::LIBELLE_TYPE_PROCEDURE_MPE => 71, CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT => 72, CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT => 73, CommonTContratTitulairePeer::MARCHE_DEFENSE => 74, CommonTContratTitulairePeer::SIRET => 75, CommonTContratTitulairePeer::NOM_ENTITE_ACHETEUR => 76, CommonTContratTitulairePeer::STATUT_PUBLICATION_SN => 77, CommonTContratTitulairePeer::DATE_PUBLICATION_SN => 78, CommonTContratTitulairePeer::ERREUR_SN => 79, CommonTContratTitulairePeer::DATE_MODIFICATION_SN => 80, CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT => 81, CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT => 82, CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT => 83, CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT => 84, CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE => 85, CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION => 86, CommonTContratTitulairePeer::UUID => 87, CommonTContratTitulairePeer::MARCHE_INNOVANT => 88, CommonTContratTitulairePeer::SERVICE_ID => 89, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_CONTRAT_TITULAIRE' => 0, 'ID_TYPE_CONTRAT' => 1, 'ID_CONTACT_CONTRAT' => 2, 'NUMERO_CONTRAT' => 3, 'LIEN_AC_SAD' => 4, 'ID_CONTRAT_MULTI' => 5, 'ORGANISME' => 6, 'OLD_SERVICE_ID' => 7, 'ID_TITULAIRE' => 8, 'ID_TITULAIRE_ETAB' => 9, 'ID_OFFRE' => 10, 'TYPE_DEPOT_REPONSE' => 11, 'OBJET_CONTRAT' => 12, 'INTITULE' => 13, 'MONTANT_CONTRAT' => 14, 'ID_TRANCHE_BUDGETAIRE' => 15, 'MONTANT_MAX_ESTIME' => 16, 'PUBLICATION_MONTANT' => 17, 'ID_MOTIF_NON_PUBLICATION_MONTANT' => 18, 'DESC_MOTIF_NON_PUBLICATION_MONTANT' => 19, 'PUBLICATION_CONTRAT' => 20, 'NUM_EJ' => 21, 'STATUTEJ' => 22, 'NUM_LONG_OEAP' => 23, 'REFERENCE_LIBRE' => 24, 'STATUT_CONTRAT' => 25, 'CATEGORIE' => 26, 'CCAG_APPLICABLE' => 27, 'CLAUSE_SOCIALE' => 28, 'CLAUSE_SOCIALE_CONDITION_EXECUTION' => 29, 'CLAUSE_SOCIALE_INSERTION' => 30, 'CLAUSE_SOCIALE_ATELIERS_PROTEGES' => 31, 'CLAUSE_SOCIALE_SIAE' => 32, 'CLAUSE_SOCIALE_ESS' => 33, 'CLAUSE_ENVIRONNEMENTALE' => 34, 'CLAUSE_ENV_SPECS_TECHNIQUES' => 35, 'CLAUSE_ENV_COND_EXECUTION' => 36, 'CLAUSE_ENV_CRITERES_SELECT' => 37, 'DATE_PREVUE_NOTIFICATION' => 38, 'DATE_PREVUE_FIN_CONTRAT' => 39, 'DATE_PREVUE_MAX_FIN_CONTRAT' => 40, 'DATE_NOTIFICATION' => 41, 'DATE_FIN_CONTRAT' => 42, 'DATE_MAX_FIN_CONTRAT' => 43, 'DATE_ATTRIBUTION' => 44, 'DATE_CREATION' => 45, 'DATE_MODIFICATION' => 46, 'ENVOI_INTERFACE' => 47, 'CONTRAT_CLASS_KEY' => 48, 'PME_PMI' => 49, 'REFERENCE_CONSULTATION' => 50, 'HORS_PASSATION' => 51, 'ID_AGENT' => 52, 'NOM_AGENT' => 53, 'PRENOM_AGENT' => 54, 'LIEU_EXECUTION' => 55, 'CODE_CPV_1' => 56, 'CODE_CPV_2' => 57, 'MARCHE_INSERTION' => 58, 'CLAUSE_SPECIFICATION_TECHNIQUE' => 59, 'PROCEDURE_PASSATION_PIVOT' => 60, 'NOM_LIEU_PRINCIPAL_EXECUTION' => 61, 'CODE_LIEU_PRINCIPAL_EXECUTION' => 62, 'TYPE_CODE_LIEU_PRINCIPAL_EXECUTION' => 63, 'DUREE_INITIALE_CONTRAT' => 64, 'FORME_PRIX' => 65, 'DATE_PUBLICATION_INITIALE_DE' => 66, 'NUM_ID_UNIQUE_MARCHE_PUBLIC' => 67, 'LIBELLE_TYPE_CONTRAT_PIVOT' => 68, 'SIRET_PA_ACCORD_CADRE' => 69, 'AC_MARCHE_SUBSEQUENT' => 70, 'LIBELLE_TYPE_PROCEDURE_MPE' => 71, 'NB_TOTAL_PROPOSITIONS_LOT' => 72, 'NB_TOTAL_PROPOSITIONS_DEMAT_LOT' => 73, 'MARCHE_DEFENSE' => 74, 'SIRET' => 75, 'NOM_ENTITE_ACHETEUR' => 76, 'STATUT_PUBLICATION_SN' => 77, 'DATE_PUBLICATION_SN' => 78, 'ERREUR_SN' => 79, 'DATE_MODIFICATION_SN' => 80, 'ID_TYPE_PROCEDURE_PIVOT' => 81, 'ID_TYPE_PROCEDURE_CONCESSION_PIVOT' => 82, 'ID_TYPE_CONTRAT_PIVOT' => 83, 'ID_TYPE_CONTRAT_CONCESSION_PIVOT' => 84, 'MONTANT_SUBVENTION_PUBLIQUE' => 85, 'DATE_DEBUT_EXECUTION' => 86, 'UUID' => 87, 'MARCHE_INNOVANT' => 88, 'SERVICE_ID' => 89, ),
        BasePeer::TYPE_FIELDNAME => array ('id_contrat_titulaire' => 0, 'id_type_contrat' => 1, 'id_contact_contrat' => 2, 'numero_contrat' => 3, 'lien_AC_SAD' => 4, 'id_contrat_multi' => 5, 'organisme' => 6, 'old_service_id' => 7, 'id_titulaire' => 8, 'id_titulaire_etab' => 9, 'id_offre' => 10, 'type_depot_reponse' => 11, 'objet_contrat' => 12, 'intitule' => 13, 'montant_contrat' => 14, 'id_tranche_budgetaire' => 15, 'montant_max_estime' => 16, 'publication_montant' => 17, 'id_motif_non_publication_montant' => 18, 'desc_motif_non_publication_montant' => 19, 'publication_contrat' => 20, 'num_EJ' => 21, 'statutEJ' => 22, 'num_long_OEAP' => 23, 'reference_libre' => 24, 'statut_contrat' => 25, 'categorie' => 26, 'ccag_applicable' => 27, 'clause_sociale' => 28, 'clause_sociale_condition_execution' => 29, 'clause_sociale_insertion' => 30, 'clause_sociale_ateliers_proteges' => 31, 'clause_sociale_siae' => 32, 'clause_sociale_ess' => 33, 'clause_environnementale' => 34, 'clause_env_specs_techniques' => 35, 'clause_env_cond_execution' => 36, 'clause_env_criteres_select' => 37, 'date_prevue_notification' => 38, 'date_prevue_fin_contrat' => 39, 'date_prevue_max_fin_contrat' => 40, 'date_notification' => 41, 'date_fin_contrat' => 42, 'date_max_fin_contrat' => 43, 'date_attribution' => 44, 'date_creation' => 45, 'date_modification' => 46, 'envoi_interface' => 47, 'contrat_class_key' => 48, 'pme_pmi' => 49, 'reference_consultation' => 50, 'hors_passation' => 51, 'id_agent' => 52, 'nom_agent' => 53, 'prenom_agent' => 54, 'lieu_execution' => 55, 'code_cpv_1' => 56, 'code_cpv_2' => 57, 'marche_insertion' => 58, 'clause_specification_technique' => 59, 'procedure_passation_pivot' => 60, 'nom_lieu_principal_execution' => 61, 'code_lieu_principal_execution' => 62, 'type_code_lieu_principal_execution' => 63, 'duree_initiale_contrat' => 64, 'forme_prix' => 65, 'date_publication_initiale_de' => 66, 'num_id_unique_marche_public' => 67, 'libelle_type_contrat_pivot' => 68, 'siret_pa_accord_cadre' => 69, 'ac_marche_subsequent' => 70, 'libelle_type_procedure_mpe' => 71, 'nb_total_propositions_lot' => 72, 'nb_total_propositions_demat_lot' => 73, 'marche_defense' => 74, 'siret' => 75, 'nom_entite_acheteur' => 76, 'statut_publication_sn' => 77, 'date_publication_sn' => 78, 'erreur_sn' => 79, 'date_modification_sn' => 80, 'id_type_procedure_pivot' => 81, 'id_type_procedure_concession_pivot' => 82, 'id_type_contrat_pivot' => 83, 'id_type_contrat_concession_pivot' => 84, 'montant_subvention_publique' => 85, 'date_debut_execution' => 86, 'uuid' => 87, 'marche_innovant' => 88, 'service_id' => 89, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTContratTitulairePeer::ENVOI_INTERFACE => array(
            CommonTContratTitulairePeer::ENVOI_INTERFACE_0,
            CommonTContratTitulairePeer::ENVOI_INTERFACE_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTContratTitulairePeer::getFieldNames($toType);
        $key = isset(CommonTContratTitulairePeer::$fieldKeys[$fromType][$name]) ? CommonTContratTitulairePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTContratTitulairePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTContratTitulairePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTContratTitulairePeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTContratTitulairePeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTContratTitulairePeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTContratTitulairePeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTContratTitulairePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTContratTitulairePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_TYPE_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::NUMERO_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::LIEN_AC_SAD);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_CONTRAT_MULTI);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ORGANISME);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::OLD_SERVICE_ID);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_TITULAIRE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_TITULAIRE_ETAB);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_OFFRE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::TYPE_DEPOT_REPONSE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::OBJET_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::INTITULE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::MONTANT_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::MONTANT_MAX_ESTIME);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::PUBLICATION_MONTANT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DESC_MOTIF_NON_PUBLICATION_MONTANT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::PUBLICATION_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::NUM_EJ);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::STATUTEJ);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::NUM_LONG_OEAP);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::REFERENCE_LIBRE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::STATUT_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CATEGORIE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CCAG_APPLICABLE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_SOCIALE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_SOCIALE_CONDITION_EXECUTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_SOCIALE_INSERTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_SOCIALE_SIAE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ESS);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_ENVIRONNEMENTALE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_ENV_SPECS_TECHNIQUES);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_ENV_COND_EXECUTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_ENV_CRITERES_SELECT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_NOTIFICATION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_FIN_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_ATTRIBUTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_MODIFICATION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ENVOI_INTERFACE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CONTRAT_CLASS_KEY);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::PME_PMI);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::REFERENCE_CONSULTATION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::HORS_PASSATION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_AGENT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::NOM_AGENT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::PRENOM_AGENT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::LIEU_EXECUTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CODE_CPV_1);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CODE_CPV_2);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::MARCHE_INSERTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CLAUSE_SPECIFICATION_TECHNIQUE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::PROCEDURE_PASSATION_PIVOT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::NOM_LIEU_PRINCIPAL_EXECUTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::CODE_LIEU_PRINCIPAL_EXECUTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::TYPE_CODE_LIEU_PRINCIPAL_EXECUTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::FORME_PRIX);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::NUM_ID_UNIQUE_MARCHE_PUBLIC);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::LIBELLE_TYPE_CONTRAT_PIVOT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::SIRET_PA_ACCORD_CADRE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::AC_MARCHE_SUBSEQUENT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::LIBELLE_TYPE_PROCEDURE_MPE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::MARCHE_DEFENSE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::SIRET);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::NOM_ENTITE_ACHETEUR);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::STATUT_PUBLICATION_SN);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_PUBLICATION_SN);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ERREUR_SN);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_MODIFICATION_SN);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::UUID);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::MARCHE_INNOVANT);
            $criteria->addSelectColumn(CommonTContratTitulairePeer::SERVICE_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id_contrat_titulaire');
            $criteria->addSelectColumn($alias . '.id_type_contrat');
            $criteria->addSelectColumn($alias . '.id_contact_contrat');
            $criteria->addSelectColumn($alias . '.numero_contrat');
            $criteria->addSelectColumn($alias . '.lien_AC_SAD');
            $criteria->addSelectColumn($alias . '.id_contrat_multi');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.old_service_id');
            $criteria->addSelectColumn($alias . '.id_titulaire');
            $criteria->addSelectColumn($alias . '.id_titulaire_etab');
            $criteria->addSelectColumn($alias . '.id_offre');
            $criteria->addSelectColumn($alias . '.type_depot_reponse');
            $criteria->addSelectColumn($alias . '.objet_contrat');
            $criteria->addSelectColumn($alias . '.intitule');
            $criteria->addSelectColumn($alias . '.montant_contrat');
            $criteria->addSelectColumn($alias . '.id_tranche_budgetaire');
            $criteria->addSelectColumn($alias . '.montant_max_estime');
            $criteria->addSelectColumn($alias . '.publication_montant');
            $criteria->addSelectColumn($alias . '.id_motif_non_publication_montant');
            $criteria->addSelectColumn($alias . '.desc_motif_non_publication_montant');
            $criteria->addSelectColumn($alias . '.publication_contrat');
            $criteria->addSelectColumn($alias . '.num_EJ');
            $criteria->addSelectColumn($alias . '.statutEJ');
            $criteria->addSelectColumn($alias . '.num_long_OEAP');
            $criteria->addSelectColumn($alias . '.reference_libre');
            $criteria->addSelectColumn($alias . '.statut_contrat');
            $criteria->addSelectColumn($alias . '.categorie');
            $criteria->addSelectColumn($alias . '.ccag_applicable');
            $criteria->addSelectColumn($alias . '.clause_sociale');
            $criteria->addSelectColumn($alias . '.clause_sociale_condition_execution');
            $criteria->addSelectColumn($alias . '.clause_sociale_insertion');
            $criteria->addSelectColumn($alias . '.clause_sociale_ateliers_proteges');
            $criteria->addSelectColumn($alias . '.clause_sociale_siae');
            $criteria->addSelectColumn($alias . '.clause_sociale_ess');
            $criteria->addSelectColumn($alias . '.clause_environnementale');
            $criteria->addSelectColumn($alias . '.clause_env_specs_techniques');
            $criteria->addSelectColumn($alias . '.clause_env_cond_execution');
            $criteria->addSelectColumn($alias . '.clause_env_criteres_select');
            $criteria->addSelectColumn($alias . '.date_prevue_notification');
            $criteria->addSelectColumn($alias . '.date_prevue_fin_contrat');
            $criteria->addSelectColumn($alias . '.date_prevue_max_fin_contrat');
            $criteria->addSelectColumn($alias . '.date_notification');
            $criteria->addSelectColumn($alias . '.date_fin_contrat');
            $criteria->addSelectColumn($alias . '.date_max_fin_contrat');
            $criteria->addSelectColumn($alias . '.date_attribution');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_modification');
            $criteria->addSelectColumn($alias . '.envoi_interface');
            $criteria->addSelectColumn($alias . '.contrat_class_key');
            $criteria->addSelectColumn($alias . '.pme_pmi');
            $criteria->addSelectColumn($alias . '.reference_consultation');
            $criteria->addSelectColumn($alias . '.hors_passation');
            $criteria->addSelectColumn($alias . '.id_agent');
            $criteria->addSelectColumn($alias . '.nom_agent');
            $criteria->addSelectColumn($alias . '.prenom_agent');
            $criteria->addSelectColumn($alias . '.lieu_execution');
            $criteria->addSelectColumn($alias . '.code_cpv_1');
            $criteria->addSelectColumn($alias . '.code_cpv_2');
            $criteria->addSelectColumn($alias . '.marche_insertion');
            $criteria->addSelectColumn($alias . '.clause_specification_technique');
            $criteria->addSelectColumn($alias . '.procedure_passation_pivot');
            $criteria->addSelectColumn($alias . '.nom_lieu_principal_execution');
            $criteria->addSelectColumn($alias . '.code_lieu_principal_execution');
            $criteria->addSelectColumn($alias . '.type_code_lieu_principal_execution');
            $criteria->addSelectColumn($alias . '.duree_initiale_contrat');
            $criteria->addSelectColumn($alias . '.forme_prix');
            $criteria->addSelectColumn($alias . '.date_publication_initiale_de');
            $criteria->addSelectColumn($alias . '.num_id_unique_marche_public');
            $criteria->addSelectColumn($alias . '.libelle_type_contrat_pivot');
            $criteria->addSelectColumn($alias . '.siret_pa_accord_cadre');
            $criteria->addSelectColumn($alias . '.ac_marche_subsequent');
            $criteria->addSelectColumn($alias . '.libelle_type_procedure_mpe');
            $criteria->addSelectColumn($alias . '.nb_total_propositions_lot');
            $criteria->addSelectColumn($alias . '.nb_total_propositions_demat_lot');
            $criteria->addSelectColumn($alias . '.marche_defense');
            $criteria->addSelectColumn($alias . '.siret');
            $criteria->addSelectColumn($alias . '.nom_entite_acheteur');
            $criteria->addSelectColumn($alias . '.statut_publication_sn');
            $criteria->addSelectColumn($alias . '.date_publication_sn');
            $criteria->addSelectColumn($alias . '.erreur_sn');
            $criteria->addSelectColumn($alias . '.date_modification_sn');
            $criteria->addSelectColumn($alias . '.id_type_procedure_pivot');
            $criteria->addSelectColumn($alias . '.id_type_procedure_concession_pivot');
            $criteria->addSelectColumn($alias . '.id_type_contrat_pivot');
            $criteria->addSelectColumn($alias . '.id_type_contrat_concession_pivot');
            $criteria->addSelectColumn($alias . '.montant_subvention_publique');
            $criteria->addSelectColumn($alias . '.date_debut_execution');
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.marche_innovant');
            $criteria->addSelectColumn($alias . '.service_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTContratTitulaire
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTContratTitulairePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTContratTitulairePeer::populateObjects(CommonTContratTitulairePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTContratTitulaire $obj A CommonTContratTitulaire object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getIdContratTitulaire();
            } // if key === null
            CommonTContratTitulairePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTContratTitulaire object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTContratTitulaire) {
                $key = (string) $value->getIdContratTitulaire();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTContratTitulaire object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTContratTitulairePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTContratTitulaire Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTContratTitulairePeer::$instances[$key])) {
                return CommonTContratTitulairePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTContratTitulairePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTContratTitulairePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_contrat_titulaire
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in CommonMarchePeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonMarchePeer::clearInstancePool();
        // Invalidate objects in CommonTConsLotContratPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonTConsLotContratPeer::clearInstancePool();
        // Invalidate objects in CommonTContratTitulairePeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonTContratTitulairePeer::clearInstancePool();
        // Invalidate objects in CommonTDonneesConsultationPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonTDonneesConsultationPeer::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTContratTitulairePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                // class must be set each time from the record row
                $cls = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$cls, strrpos('.'.$cls, '.') + 1);
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTContratTitulairePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTContratTitulaire object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTContratTitulairePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTContratTitulairePeer::getOMClass($row, $startcol);
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTContratTitulairePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTypeContratConcessionPivot table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTypeContratConcessionPivot(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTypeContratPivot table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTypeContratPivot(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTypeProcedureConcessionPivot table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTypeProcedureConcessionPivot(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTContactContrat table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTContactContrat(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTTypeContrat table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTTypeContrat(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTypeProcedure table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTypeProcedure(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with their CommonTypeContratConcessionPivot objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTypeContratConcessionPivot(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;
        CommonTypeContratConcessionPivotPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTypeContratConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTypeContratConcessionPivotPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTypeContratConcessionPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTypeContratConcessionPivotPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to $obj2 (CommonTypeContratConcessionPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with their CommonTypeContratPivot objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTypeContratPivot(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;
        CommonTypeContratPivotPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTypeContratPivotPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTypeContratPivotPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTypeContratPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTypeContratPivotPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to $obj2 (CommonTypeContratPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with their CommonTypeProcedureConcessionPivot objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTypeProcedureConcessionPivot(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;
        CommonTypeProcedureConcessionPivotPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTypeProcedureConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTypeProcedureConcessionPivotPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTypeProcedureConcessionPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTypeProcedureConcessionPivotPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to $obj2 (CommonTypeProcedureConcessionPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with their CommonTContactContrat objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTContactContrat(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;
        CommonTContactContratPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTContactContratPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTContactContratPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTContactContratPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTContactContratPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to $obj2 (CommonTContactContrat)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with their CommonTTypeContrat objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTTypeContrat(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;
        CommonTTypeContratPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTTypeContratPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTTypeContratPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTTypeContratPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to $obj2 (CommonTTypeContrat)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with their CommonTypeProcedure objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTypeProcedure(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;
        CommonTypeProcedurePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTypeProcedurePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTypeProcedurePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTypeProcedurePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTypeProcedurePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to $obj2 (CommonTypeProcedure)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol2 = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratConcessionPivotPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTypeContratConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratPivotPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTypeContratPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedureConcessionPivotPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTypeProcedureConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTContactContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTContactContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedurePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonTypeProcedurePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
        $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonTypeContratConcessionPivot rows

            $key2 = CommonTypeContratConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonTypeContratConcessionPivotPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTypeContratConcessionPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTypeContratConcessionPivotPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj2 (CommonTypeContratConcessionPivot)
                $obj2->addCommonTContratTitulaire($obj1);
            } // if joined row not null

            // Add objects for joined CommonTypeContratPivot rows

            $key3 = CommonTypeContratPivotPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonTypeContratPivotPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonTypeContratPivotPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTypeContratPivotPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj3 (CommonTypeContratPivot)
                $obj3->addCommonTContratTitulaire($obj1);
            } // if joined row not null

            // Add objects for joined CommonTypeProcedureConcessionPivot rows

            $key4 = CommonTypeProcedureConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonTypeProcedureConcessionPivotPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonTypeProcedureConcessionPivotPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTypeProcedureConcessionPivotPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj4 (CommonTypeProcedureConcessionPivot)
                $obj4->addCommonTContratTitulaire($obj1);
            } // if joined row not null

            // Add objects for joined CommonTContactContrat rows

            $key5 = CommonTContactContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = CommonTContactContratPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = CommonTContactContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTContactContratPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj5 (CommonTContactContrat)
                $obj5->addCommonTContratTitulaire($obj1);
            } // if joined row not null

            // Add objects for joined CommonTTypeContrat rows

            $key6 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = CommonTTypeContratPeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = CommonTTypeContratPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonTTypeContratPeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj6 (CommonTTypeContrat)
                $obj6->addCommonTContratTitulaire($obj1);
            } // if joined row not null

            // Add objects for joined CommonTypeProcedure rows

            $key7 = CommonTypeProcedurePeer::getPrimaryKeyHashFromRow($row, $startcol7);
            if ($key7 !== null) {
                $obj7 = CommonTypeProcedurePeer::getInstanceFromPool($key7);
                if (!$obj7) {

                    $cls = CommonTypeProcedurePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonTypeProcedurePeer::addInstanceToPool($obj7, $key7);
                } // if obj7 loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj7 (CommonTypeProcedure)
                $obj7->addCommonTContratTitulaire($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTypeContratConcessionPivot table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTypeContratConcessionPivot(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTypeContratPivot table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTypeContratPivot(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTypeProcedureConcessionPivot table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTypeProcedureConcessionPivot(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTContactContrat table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTContactContrat(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTContratTitulaireRelatedByIdContratMulti table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTContratTitulaireRelatedByIdContratMulti(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTTypeContrat table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTTypeContrat(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTypeProcedure table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTypeProcedure(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTContratTitulairePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with all related objects except CommonTypeContratConcessionPivot.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTypeContratConcessionPivot(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol2 = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratPivotPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTypeContratPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedureConcessionPivotPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTypeProcedureConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTContactContratPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTContactContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedurePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonTypeProcedurePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTypeContratPivot rows

                $key2 = CommonTypeContratPivotPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTypeContratPivotPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTypeContratPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTypeContratPivotPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj2 (CommonTypeContratPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedureConcessionPivot rows

                $key3 = CommonTypeProcedureConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTypeProcedureConcessionPivotPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTypeProcedureConcessionPivotPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTypeProcedureConcessionPivotPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj3 (CommonTypeProcedureConcessionPivot)
                $obj3->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContactContrat rows

                $key4 = CommonTContactContratPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTContactContratPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTContactContratPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTContactContratPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj4 (CommonTContactContrat)
                $obj4->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedure rows

                $key6 = CommonTypeProcedurePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonTypeProcedurePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonTypeProcedurePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonTypeProcedurePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj6 (CommonTypeProcedure)
                $obj6->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with all related objects except CommonTypeContratPivot.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTypeContratPivot(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol2 = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratConcessionPivotPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTypeContratConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedureConcessionPivotPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTypeProcedureConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTContactContratPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTContactContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedurePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonTypeProcedurePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTypeContratConcessionPivot rows

                $key2 = CommonTypeContratConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTypeContratConcessionPivotPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTypeContratConcessionPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTypeContratConcessionPivotPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj2 (CommonTypeContratConcessionPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedureConcessionPivot rows

                $key3 = CommonTypeProcedureConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTypeProcedureConcessionPivotPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTypeProcedureConcessionPivotPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTypeProcedureConcessionPivotPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj3 (CommonTypeProcedureConcessionPivot)
                $obj3->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContactContrat rows

                $key4 = CommonTContactContratPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTContactContratPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTContactContratPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTContactContratPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj4 (CommonTContactContrat)
                $obj4->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedure rows

                $key6 = CommonTypeProcedurePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonTypeProcedurePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonTypeProcedurePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonTypeProcedurePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj6 (CommonTypeProcedure)
                $obj6->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with all related objects except CommonTypeProcedureConcessionPivot.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTypeProcedureConcessionPivot(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol2 = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratConcessionPivotPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTypeContratConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratPivotPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTypeContratPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTContactContratPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTContactContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedurePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonTypeProcedurePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTypeContratConcessionPivot rows

                $key2 = CommonTypeContratConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTypeContratConcessionPivotPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTypeContratConcessionPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTypeContratConcessionPivotPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj2 (CommonTypeContratConcessionPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeContratPivot rows

                $key3 = CommonTypeContratPivotPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTypeContratPivotPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTypeContratPivotPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTypeContratPivotPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj3 (CommonTypeContratPivot)
                $obj3->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContactContrat rows

                $key4 = CommonTContactContratPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTContactContratPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTContactContratPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTContactContratPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj4 (CommonTContactContrat)
                $obj4->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedure rows

                $key6 = CommonTypeProcedurePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonTypeProcedurePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonTypeProcedurePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonTypeProcedurePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj6 (CommonTypeProcedure)
                $obj6->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with all related objects except CommonTContactContrat.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTContactContrat(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol2 = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratConcessionPivotPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTypeContratConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratPivotPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTypeContratPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedureConcessionPivotPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTypeProcedureConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedurePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonTypeProcedurePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTypeContratConcessionPivot rows

                $key2 = CommonTypeContratConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTypeContratConcessionPivotPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTypeContratConcessionPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTypeContratConcessionPivotPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj2 (CommonTypeContratConcessionPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeContratPivot rows

                $key3 = CommonTypeContratPivotPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTypeContratPivotPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTypeContratPivotPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTypeContratPivotPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj3 (CommonTypeContratPivot)
                $obj3->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedureConcessionPivot rows

                $key4 = CommonTypeProcedureConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTypeProcedureConcessionPivotPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTypeProcedureConcessionPivotPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTypeProcedureConcessionPivotPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj4 (CommonTypeProcedureConcessionPivot)
                $obj4->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedure rows

                $key6 = CommonTypeProcedurePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonTypeProcedurePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonTypeProcedurePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonTypeProcedurePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj6 (CommonTypeProcedure)
                $obj6->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with all related objects except CommonTContratTitulaireRelatedByIdContratMulti.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTContratTitulaireRelatedByIdContratMulti(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol2 = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratConcessionPivotPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTypeContratConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratPivotPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTypeContratPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedureConcessionPivotPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTypeProcedureConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTContactContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTContactContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedurePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonTypeProcedurePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTypeContratConcessionPivot rows

                $key2 = CommonTypeContratConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTypeContratConcessionPivotPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTypeContratConcessionPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTypeContratConcessionPivotPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj2 (CommonTypeContratConcessionPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeContratPivot rows

                $key3 = CommonTypeContratPivotPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTypeContratPivotPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTypeContratPivotPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTypeContratPivotPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj3 (CommonTypeContratPivot)
                $obj3->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedureConcessionPivot rows

                $key4 = CommonTypeProcedureConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTypeProcedureConcessionPivotPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTypeProcedureConcessionPivotPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTypeProcedureConcessionPivotPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj4 (CommonTypeProcedureConcessionPivot)
                $obj4->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContactContrat rows

                $key5 = CommonTContactContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTContactContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTContactContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTContactContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj5 (CommonTContactContrat)
                $obj5->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key6 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonTTypeContratPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonTTypeContratPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj6 (CommonTTypeContrat)
                $obj6->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedure rows

                $key7 = CommonTypeProcedurePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonTypeProcedurePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonTypeProcedurePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonTypeProcedurePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj7 (CommonTypeProcedure)
                $obj7->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with all related objects except CommonTTypeContrat.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTTypeContrat(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol2 = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratConcessionPivotPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTypeContratConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratPivotPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTypeContratPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedureConcessionPivotPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTypeProcedureConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTContactContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTContactContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedurePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonTypeProcedurePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTypeContratConcessionPivot rows

                $key2 = CommonTypeContratConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTypeContratConcessionPivotPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTypeContratConcessionPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTypeContratConcessionPivotPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj2 (CommonTypeContratConcessionPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeContratPivot rows

                $key3 = CommonTypeContratPivotPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTypeContratPivotPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTypeContratPivotPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTypeContratPivotPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj3 (CommonTypeContratPivot)
                $obj3->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedureConcessionPivot rows

                $key4 = CommonTypeProcedureConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTypeProcedureConcessionPivotPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTypeProcedureConcessionPivotPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTypeProcedureConcessionPivotPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj4 (CommonTypeProcedureConcessionPivot)
                $obj4->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContactContrat rows

                $key5 = CommonTContactContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTContactContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTContactContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTContactContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj5 (CommonTContactContrat)
                $obj5->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedure rows

                $key6 = CommonTypeProcedurePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonTypeProcedurePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonTypeProcedurePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonTypeProcedurePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj6 (CommonTypeProcedure)
                $obj6->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTContratTitulaire objects pre-filled with all related objects except CommonTypeProcedure.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTContratTitulaire objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTypeProcedure(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);
        }

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol2 = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratConcessionPivotPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTypeContratConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeContratPivotPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTypeContratPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTypeProcedureConcessionPivotPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTypeProcedureConcessionPivotPeer::NUM_HYDRATE_COLUMNS;

        CommonTContactContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTContactContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, CommonTypeContratConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, CommonTypeContratPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, CommonTypeProcedureConcessionPivotPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, CommonTContactContratPeer::ID_CONTACT_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTContratTitulairePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $omClass = CommonTContratTitulairePeer::getOMClass($row, 0);
                $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTContratTitulairePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTypeContratConcessionPivot rows

                $key2 = CommonTypeContratConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTypeContratConcessionPivotPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTypeContratConcessionPivotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTypeContratConcessionPivotPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj2 (CommonTypeContratConcessionPivot)
                $obj2->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeContratPivot rows

                $key3 = CommonTypeContratPivotPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTypeContratPivotPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTypeContratPivotPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTypeContratPivotPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj3 (CommonTypeContratPivot)
                $obj3->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTypeProcedureConcessionPivot rows

                $key4 = CommonTypeProcedureConcessionPivotPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTypeProcedureConcessionPivotPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTypeProcedureConcessionPivotPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTypeProcedureConcessionPivotPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj4 (CommonTypeProcedureConcessionPivot)
                $obj4->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContactContrat rows

                $key5 = CommonTContactContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTContactContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTContactContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTContactContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj5 (CommonTContactContrat)
                $obj5->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key6 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonTTypeContratPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonTTypeContratPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonTContratTitulaire) to the collection in $obj6 (CommonTTypeContrat)
                $obj6->addCommonTContratTitulaire($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTContratTitulairePeer::DATABASE_NAME)->getTable(CommonTContratTitulairePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTContratTitulairePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTContratTitulairePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTContratTitulaireTableMap());
      }
    }

    /**
     * The returned Class will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param      array $row PropelPDO result row.
     * @param      int $colnum Column to examine for OM class information (first is 0).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        try {

            $omClass = null;
            $classKey = $row[$colnum + 48];

            switch ($classKey) {

                case CommonTContratTitulairePeer::CLASSKEY_1:
                    $omClass = CommonTContratTitulairePeer::CLASSNAME_1;
                    break;

                case CommonTContratTitulairePeer::CLASSKEY_2:
                    $omClass = CommonTContratTitulairePeer::CLASSNAME_2;
                    break;

                default:
                    $omClass = CommonTContratTitulairePeer::OM_CLASS;

            } // switch

        } catch (Exception $e) {
            throw new PropelException('Unable to get OM class.', $e);
        }

        return $omClass;
    }

    /**
     * Performs an INSERT on the database, given a CommonTContratTitulaire or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTContratTitulaire object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTContratTitulaire object
        }

        if ($criteria->containsKey(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE) && $criteria->keyContainsValue(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTContratTitulaire or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTContratTitulaire object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE);
            $value = $criteria->remove(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE);
            if ($value) {
                $selectCriteria->add(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTContratTitulairePeer::TABLE_NAME);
            }

        } else { // $values is CommonTContratTitulaire object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_contrat_titulaire table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += CommonTContratTitulairePeer::doOnDeleteCascade(new Criteria(CommonTContratTitulairePeer::DATABASE_NAME), $con);
            $affectedRows += BasePeer::doDeleteAll(CommonTContratTitulairePeer::TABLE_NAME, $con, CommonTContratTitulairePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTContratTitulairePeer::clearInstancePool();
            CommonTContratTitulairePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTContratTitulaire or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTContratTitulaire object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTContratTitulaire) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTContratTitulairePeer::DATABASE_NAME);
            $criteria->add(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, (array) $values, Criteria::IN);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTContratTitulairePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += CommonTContratTitulairePeer::doOnDeleteCascade($c, $con);

            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            if ($values instanceof Criteria) {
                CommonTContratTitulairePeer::clearInstancePool();
            } elseif ($values instanceof CommonTContratTitulaire) { // it's a model object
                CommonTContratTitulairePeer::removeInstanceFromPool($values);
            } else { // it's a primary key, or an array of pks
                foreach ((array) $values as $singleval) {
                    CommonTContratTitulairePeer::removeInstanceFromPool($singleval);
                }
            }

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTContratTitulairePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected static function doOnDeleteCascade(Criteria $criteria, PropelPDO $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $criteria
        $objects = CommonTContratTitulairePeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {


            // delete related CommonMarche objects
            $criteria = new Criteria(CommonMarchePeer::DATABASE_NAME);

            $criteria->add(CommonMarchePeer::ID_CONTRAT_TITULAIRE, $obj->getIdContratTitulaire());
            $affectedRows += CommonMarchePeer::doDelete($criteria, $con);

            // delete related CommonTConsLotContrat objects
            $criteria = new Criteria(CommonTConsLotContratPeer::DATABASE_NAME);

            $criteria->add(CommonTConsLotContratPeer::ID_CONTRAT_TITULAIRE, $obj->getIdContratTitulaire());
            $affectedRows += CommonTConsLotContratPeer::doDelete($criteria, $con);

            // delete related CommonTContratTitulaire objects
            $criteria = new Criteria(CommonTContratTitulairePeer::DATABASE_NAME);

            $criteria->add(CommonTContratTitulairePeer::ID_CONTRAT_MULTI, $obj->getIdContratTitulaire());
            $affectedRows += CommonTContratTitulairePeer::doDelete($criteria, $con);

            // delete related CommonTDonneesConsultation objects
            $criteria = new Criteria(CommonTDonneesConsultationPeer::DATABASE_NAME);

            $criteria->add(CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE, $obj->getIdContratTitulaire());
            $affectedRows += CommonTDonneesConsultationPeer::doDelete($criteria, $con);
        }

        return $affectedRows;
    }

    /**
     * Validates all modified columns of given CommonTContratTitulaire object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTContratTitulaire $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTContratTitulairePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTContratTitulairePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTContratTitulairePeer::DATABASE_NAME, CommonTContratTitulairePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTContratTitulaire
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTContratTitulairePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTContratTitulairePeer::DATABASE_NAME);
        $criteria->add(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $pk);

        $v = CommonTContratTitulairePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTContratTitulaire[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTContratTitulairePeer::DATABASE_NAME);
            $criteria->add(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $pks, Criteria::IN);
            $objs = CommonTContratTitulairePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTContratTitulairePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTContratTitulairePeer::buildTableMap();

