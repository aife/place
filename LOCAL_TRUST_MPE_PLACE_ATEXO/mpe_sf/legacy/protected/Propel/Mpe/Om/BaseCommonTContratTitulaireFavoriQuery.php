<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTContratTitulaireFavori;
use Application\Propel\Mpe\CommonTContratTitulaireFavoriPeer;
use Application\Propel\Mpe\CommonTContratTitulaireFavoriQuery;

/**
 * Base class that represents a query for the 't_contrat_titulaire_favori' table.
 *
 *
 *
 * @method CommonTContratTitulaireFavoriQuery orderByIdContratTitulaireFavori($order = Criteria::ASC) Order by the id_contrat_titulaire_favori column
 * @method CommonTContratTitulaireFavoriQuery orderByIdContratTitulaire($order = Criteria::ASC) Order by the id_contrat_titulaire column
 * @method CommonTContratTitulaireFavoriQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTContratTitulaireFavoriQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 *
 * @method CommonTContratTitulaireFavoriQuery groupByIdContratTitulaireFavori() Group by the id_contrat_titulaire_favori column
 * @method CommonTContratTitulaireFavoriQuery groupByIdContratTitulaire() Group by the id_contrat_titulaire column
 * @method CommonTContratTitulaireFavoriQuery groupByOrganisme() Group by the organisme column
 * @method CommonTContratTitulaireFavoriQuery groupByIdAgent() Group by the id_agent column
 *
 * @method CommonTContratTitulaireFavoriQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTContratTitulaireFavoriQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTContratTitulaireFavoriQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTContratTitulaireFavori findOne(PropelPDO $con = null) Return the first CommonTContratTitulaireFavori matching the query
 * @method CommonTContratTitulaireFavori findOneOrCreate(PropelPDO $con = null) Return the first CommonTContratTitulaireFavori matching the query, or a new CommonTContratTitulaireFavori object populated from the query conditions when no match is found
 *
 * @method CommonTContratTitulaireFavori findOneByIdContratTitulaire(int $id_contrat_titulaire) Return the first CommonTContratTitulaireFavori filtered by the id_contrat_titulaire column
 * @method CommonTContratTitulaireFavori findOneByOrganisme(string $organisme) Return the first CommonTContratTitulaireFavori filtered by the organisme column
 * @method CommonTContratTitulaireFavori findOneByIdAgent(int $id_agent) Return the first CommonTContratTitulaireFavori filtered by the id_agent column
 *
 * @method array findByIdContratTitulaireFavori(int $id_contrat_titulaire_favori) Return CommonTContratTitulaireFavori objects filtered by the id_contrat_titulaire_favori column
 * @method array findByIdContratTitulaire(int $id_contrat_titulaire) Return CommonTContratTitulaireFavori objects filtered by the id_contrat_titulaire column
 * @method array findByOrganisme(string $organisme) Return CommonTContratTitulaireFavori objects filtered by the organisme column
 * @method array findByIdAgent(int $id_agent) Return CommonTContratTitulaireFavori objects filtered by the id_agent column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTContratTitulaireFavoriQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTContratTitulaireFavoriQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTContratTitulaireFavori', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTContratTitulaireFavoriQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTContratTitulaireFavoriQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTContratTitulaireFavoriQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTContratTitulaireFavoriQuery) {
            return $criteria;
        }
        $query = new CommonTContratTitulaireFavoriQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTContratTitulaireFavori|CommonTContratTitulaireFavori[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTContratTitulaireFavoriPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulaireFavoriPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTContratTitulaireFavori A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdContratTitulaireFavori($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTContratTitulaireFavori A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_contrat_titulaire_favori`, `id_contrat_titulaire`, `organisme`, `id_agent` FROM `t_contrat_titulaire_favori` WHERE `id_contrat_titulaire_favori` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTContratTitulaireFavori();
            $obj->hydrate($row);
            CommonTContratTitulaireFavoriPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTContratTitulaireFavori|CommonTContratTitulaireFavori[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTContratTitulaireFavori[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTContratTitulaireFavoriQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_CONTRAT_TITULAIRE_FAVORI, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTContratTitulaireFavoriQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_CONTRAT_TITULAIRE_FAVORI, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_contrat_titulaire_favori column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContratTitulaireFavori(1234); // WHERE id_contrat_titulaire_favori = 1234
     * $query->filterByIdContratTitulaireFavori(array(12, 34)); // WHERE id_contrat_titulaire_favori IN (12, 34)
     * $query->filterByIdContratTitulaireFavori(array('min' => 12)); // WHERE id_contrat_titulaire_favori >= 12
     * $query->filterByIdContratTitulaireFavori(array('max' => 12)); // WHERE id_contrat_titulaire_favori <= 12
     * </code>
     *
     * @param     mixed $idContratTitulaireFavori The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireFavoriQuery The current query, for fluid interface
     */
    public function filterByIdContratTitulaireFavori($idContratTitulaireFavori = null, $comparison = null)
    {
        if (is_array($idContratTitulaireFavori)) {
            $useMinMax = false;
            if (isset($idContratTitulaireFavori['min'])) {
                $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_CONTRAT_TITULAIRE_FAVORI, $idContratTitulaireFavori['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContratTitulaireFavori['max'])) {
                $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_CONTRAT_TITULAIRE_FAVORI, $idContratTitulaireFavori['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_CONTRAT_TITULAIRE_FAVORI, $idContratTitulaireFavori, $comparison);
    }

    /**
     * Filter the query on the id_contrat_titulaire column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContratTitulaire(1234); // WHERE id_contrat_titulaire = 1234
     * $query->filterByIdContratTitulaire(array(12, 34)); // WHERE id_contrat_titulaire IN (12, 34)
     * $query->filterByIdContratTitulaire(array('min' => 12)); // WHERE id_contrat_titulaire >= 12
     * $query->filterByIdContratTitulaire(array('max' => 12)); // WHERE id_contrat_titulaire <= 12
     * </code>
     *
     * @param     mixed $idContratTitulaire The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireFavoriQuery The current query, for fluid interface
     */
    public function filterByIdContratTitulaire($idContratTitulaire = null, $comparison = null)
    {
        if (is_array($idContratTitulaire)) {
            $useMinMax = false;
            if (isset($idContratTitulaire['min'])) {
                $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContratTitulaire['max'])) {
                $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireFavoriQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTContratTitulaireFavoriQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTContratTitulaireFavori $commonTContratTitulaireFavori Object to remove from the list of results
     *
     * @return CommonTContratTitulaireFavoriQuery The current query, for fluid interface
     */
    public function prune($commonTContratTitulaireFavori = null)
    {
        if ($commonTContratTitulaireFavori) {
            $this->addUsingAlias(CommonTContratTitulaireFavoriPeer::ID_CONTRAT_TITULAIRE_FAVORI, $commonTContratTitulaireFavori->getIdContratTitulaireFavori(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
